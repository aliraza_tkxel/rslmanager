﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class HrNoteViewModel
    {
        public List<JobDetailNoteViewModel> notes { get; set; }
    }

    public class JobDetailNoteViewModel
    {
        public int jobDetailNoteID { get; set; }
        public int employeeId { get; set; }
        public DateTime _createdDate { get; set; }
        public string createdDate { get; set; }
        public int createdBy { get; set; }
        public string createdByName { get; set; }
        public string note { get; set; }
        public string subNote
        {
            get
            {
                if (note.Length > 150)
                {
                    return note.Substring(0, 150);
                }
                return note;
            }
        }
    }
}