﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BHGHRModuleWeb.ViewModels
{
    public class PostViewModel
    {
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please Select Job Role")]
        [Display(Name = "Job Role:")]
        public int? jobRoleId { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please Select Team")]
        [Display(Name = "Team:")]
        public int? team { get; set; }

        public JobDetailViewModel jobDetail { get; set; } = new JobDetailViewModel();
        public LookUpsViewModel postTabLookups { get; set; } = new LookUpsViewModel();

        #region >>> Lookups <<<
        public List<SelectListItem> genders
        {
            get
            {
                var list = new List<SelectListItem>();
                list.Add(new SelectListItem() { Text = "Male", Value = "Male" });
                list.Add(new SelectListItem() { Text = "Female", Value = "Female" });
                list.Add(new SelectListItem() { Text = "Prefer not to say", Value = "Prefer not to say" });
                list.Insert(0, new SelectListItem() { Text = "Please Select Gender", Value = "0", Selected = true });
                return list;
            }
        }
        public List<SelectListItem> teams
        {
            get
            {
                var list = new List<SelectListItem>();
                list = postTabLookups.team.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Insert(0, new SelectListItem() { Text = "Please Select Team", Value = "0", Selected = true });
                return list;
            }
            set { }
        }
        public List<SelectListItem> jobRole
        {
            get
            {
                var list = new List<SelectListItem>();
                list = postTabLookups.jobRole.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Insert(0, new SelectListItem() { Text = "Please Select Job Role", Value = "0", Selected = true });
                return list;
            }
            set { }
        }
        public List<SelectListItem> holidayRule
        {
            get
            {
                var list = new List<SelectListItem>();
                list = postTabLookups.holidayRule.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Insert(0, new SelectListItem() { Text = "Please Select Holiday Rule", Value = "0", Selected = true });
                
                return list;
            }
            set { }
        }
        public List<SelectListItem> lineManager
        {
            get
            {
                var list = new List<SelectListItem>();
                list = postTabLookups.lineManager.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();        
                list=list.OrderBy(x => x.Text).ToList();
                list.Insert(0, new SelectListItem() { Text = "Please Select Line Manager", Value = "0", Selected = true });
                return list;
            }
            set { }
        }
        public List<SelectListItem> contractType
        {
            get
            {
                var list = new List<SelectListItem>();
                list = postTabLookups.contractType.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Insert(0, new SelectListItem() { Text = "Please Select Contract Type", Value = "0", Selected = true });
                return list;
            }
            set { }
        }
        public List<SelectListItem> placeOfWork
        {
            get
            {
                var list = new List<SelectListItem>();
                list = postTabLookups.placeOfWork.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Insert(0, new SelectListItem() { Text = "Please Select Place Of Work", Value = "0", Selected = true });
                return list;
            }
            set { }
        }
        public List<SelectListItem> grade
        {
            get
            {
                var list = new List<SelectListItem>();
                list = postTabLookups.grade.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Insert(0, new SelectListItem() { Text = "Please Select Grade", Value = "0", Selected = true });
                return list;
            }
            set { }
        }
        public List<SelectListItem> gradePoint
        {
            get
            {
                var list = new List<SelectListItem>();
                list = postTabLookups.gradePoint.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Insert(0, new SelectListItem() { Text = "Please Select Point", Value = "0", Selected = true });
                return list;
            }
            set { }
        }
        public List<SelectListItem> dayHour
        {
            get
            {
                var list = new List<SelectListItem>();
                list = postTabLookups.dayHour.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Insert(0, new SelectListItem() { Text = "Please Select", Value = "0", Selected = true });
                return list;
            }
            set { }
        }
        public List<SelectListItem> partFullTime
        {
            get
            {
                var list = new List<SelectListItem>();
                list = postTabLookups.partFullTime.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Insert(0, new SelectListItem() { Text = "Please Select", Value = "0", Selected = true });
                return list;
            }
            set { }
        }
        public List<SelectListItem> noticePeriod
        {
            get
            {
                var list = new List<SelectListItem>();
                list = postTabLookups.noticePeriod.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Insert(0, new SelectListItem() { Text = "Please Select Notice Period", Value = "0", Selected = true });
                return list;
            }
            set { }
        }
        public List<SelectListItem> probationPeriod
        {
            get
            {
                var list = new List<SelectListItem>();
                list = postTabLookups.probationPeriod.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Insert(0, new SelectListItem() { Text = "Please Select Probation Period", Value = "0", Selected = true });
                return list;
            }
            set { }
        }

        public List<SelectListItem> BRSBHA
        {
            get
            {
                var list = new List<SelectListItem>();
                list.Add(new SelectListItem() { Text = "BHA", Value = "0" });
                list.Add(new SelectListItem() { Text = "BRS", Value = "1" });
                list.Insert(0, new SelectListItem() { Text = "Please Select", Value = "null", Selected = true });
                return list;
            }
            set { }
        }
        #endregion
    }

    public class SaleryAmendment
    {
        public int employeeId { get; set; }
        public int salaryAmendmentId { get; set; }
        public string fileName { get; set; }
        public string filePath { get; set; }
    }

    public class JobRoleHistoryViewModel
    {
        public int employeeId { get; set; }
        public List<HistoryViewModel> history { get; set; } = new List<HistoryViewModel>();
    }

    public class HistoryViewModel
    {
        public DateTime createdDate { get; set; }
        public string jobRole { get; set; }
        public string team { get; set; }
        public string createdby { get; set; }
        public string date { get { return createdDate.ToString("dd/MM/yyyy"); } set { } }
    }

    public class WorkingHoursViewModel
    {
        public int employeeId { get; set; }
        public string requestType { get; set; }
        public int? lastActionUser { get; set; }
        public decimal? total { get; set; }
        public string unit { get; set; }
        public List<WorkingHoursHistoryViewModel> workingHours { get; set; } = new List<WorkingHoursHistoryViewModel>();

        public List<WorkingHoursHistoryViewModel> workingHoursHistory { get; set; } = new List<WorkingHoursHistoryViewModel>();

        public List<WorkingHoursGroupModel> workingHoursHistoryGrouping
        {
            get
            {
                if (workingHoursHistory != null && workingHoursHistory.Count > 0)
                {
                    return workingHoursHistory.GroupBy(e => e.groupNumber).Select(e => new WorkingHoursGroupModel() { item = e.Key, EmployeesWorkingHours= e }).ToList();
                }
                return null;
            }
        }
    }
    public class DayTimingViewModel
    {
        public int employeeId { get; set; }
        public int weekNumber { get; set; }
        public string day { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public double duration { get; set; }
        public string LunchStartTime { get; set; }
        public string LunchEndTime { get; set; }
        public string LunchTimeInMinutes { get; set; }
    }
    
    public class WorkingHoursGroupModel
    {
        public int? item { get; set; }
        public IEnumerable<WorkingHoursHistoryViewModel> EmployeesWorkingHours { get; set; }
    }

    public class WorkingHoursHistoryViewModel
    {
        public int employeeId { get; set; }
        public int wId { get; set; }
        public double mon { get; set; } = 0;
        public double tue { get; set; } = 0;
        public double wed { get; set; } = 0;
        public double thu { get; set; } = 0;        
        public double fri { get; set; } = 0;
        public double sat { get; set; } = 0;
        public double sun { get; set; } = 0;
        public string startDate { get; set; }
        public string endDate { get; set; }
        public int? createdBy { get; set; }
        public DateTime? createdOn { get; set; }
        public string createdByName { get; set; }
        public int? lastActionUser { get; set; }
        public double total { get; set; }
        public int? groupNumber { get; set; }

        public string start { get { return startDate; } }
        public string end { get { return endDate; } }
        public string created { get { return createdOn.HasValue ? createdOn.Value.ToString("dd/MM/yyyy") : ""; } }

        public bool isEmpty { get; set; } = false;
        public string unit { get; set; }
        public string MonDayTimings { get; set; }
        public string TueDayTimings { get; set; }
        public string WedDayTimings { get; set; }
        public string ThuDayTimings { get; set; } 
        public string FriDayTimings { get; set; } 
        public string SatDayTimings { get; set; } 
        public string SunDayTimings { get; set; }

        public string DayTimings { get { return string.Format("{0}_{1}_{2}_{3}_{4}_{5}_{6}", MonDayTimings, TueDayTimings, WedDayTimings, ThuDayTimings, FriDayTimings, SatDayTimings, SunDayTimings); } }
        public string createdByFname { get; set; }
        public string createdByLname { get; set; }

    }
}