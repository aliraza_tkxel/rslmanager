﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BHGHRModuleWeb.ViewModels
{
    public class JobDetailViewModel
    {
        public int jobDetailsId { get; set; }

        public int employeeId { get; set; }

        public int? lastActionUser { get; set; }

        public string lastActiontime { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please Select Team")]
        [Display(Name = "Team:")]
        public int? team { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please Select Job Role")]
        [Display(Name = "Job Role:")]
        public int? jobRoleId { get; set; }

        [Required]
        [Display(Name = "Start Date:")]
        public string startDate { get; set; }

        [Required]
        [Display(Name = "AL Start Date:")]
        public string alStartDate { get; set; }

        [Display(Name = "Probation Review Date:")]
        public string reviewDate { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please Select Holiday Rule")]
        [Display(Name = "Holiday Allocation:")]
        public int? holidayRule { get; set; }

        [Display(Name = "Full Time Equivalent:")]
        //[Range(0.01, double.MaxValue, ErrorMessage = "Please Select Holiday Rule")]
        public double? fte { get; set; }

        [Display(Name = "Details of Role:")]
        public string detailsOfRole { get; set; }

        [Display(Name = "Job Description:")]
        public string roleFile { get; set; }

        [Display(Name = "Employee Limit:")]
        [Range(0, double.MaxValue)]
        public double? employeeLimit { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please Select Line Manager")]
        [Display(Name = "Line Manager:")]
        public int? lineManager { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please Select Contract Type")]
        [Display(Name = "T&Cs:")]
        public int? contractType { get; set; }

        [Display(Name = "Place of Work:")]
        public int? officeLocation { get; set; }

        [Display(Name = "Team Manager:")]
        public int? isManager { get; set; }

        [Display(Name = "Employment Contracts:")]
        public string employeeContract { get; set; }

        [Display(Name = "Salary Amendments:")]
        public string salaryAmendments { get; set; }

        [Display(Name = "Grade:")]
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please Select Grade")]
        public int? grade { get; set; }

        [Display(Name = "Point:")]
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please Select Point")]
        public int? gradePoint { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please Select Full/Part Time")]
        [Display(Name = "Full/Part Time:")]
        public int? partFullTime { get; set; }

        [Display(Name = "Fixed Term Contract:")]
        public int fixedTermContract { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please Select")]
        public int? holidayIndicator { get; set; }

        [Required]
        [Display(Name = "Hours (per week):")]
        public double? hours { get; set; }

        [Display(Name = "Gross Salary:")]
        public decimal? salary { get; set; }

        [Display(Name = "Foreign National Number:")]
        public string foreignNationalNumber { get; set; }

        [Display(Name = "Tax Office:")]
        public string taxOffice { get; set; }

        [Display(Name = "Tax Code:")]
        public string taxCode { get; set; }

        [Display(Name = "Payroll Number:")]
        public string payrollNumber { get; set; }

        [Display(Name = "Max Holiday Entitlement:")]
        public double? maxHolidayEntitlementDays { get; set; }

        public double? maxHolidayEntitlementHours { get; set; }

        [Display(Name = "Actual Holiday Entitlement:")]
        public double? holidayEntitlementDays { get; set; }

        public double? holidayEntitlementHours { get; set; }

        [Display(Name = "Bank Holiday:")]
        public double? bankHoliday { get; set; }
        public double? _bankHoliday { get; set; }
        

        [Display(Name = "Allowance:")]
        public double? autoHolidayAdjustment { get; set; }

        [Display(Name = "Carry Forward:")]
        public double? carryForward { get; set; }
        public double? carryForwardHours { get; set; }

        [Display(Name = "Total Leave:")]
        public double? totalLeave { get; set; }
        public double? totalLeaveHours { get; set; }

        [Display(Name = "TUPE In:")]
        public string tupeInDate { get; set; }

        [Display(Name = "TUPE Out:")]
        public string tupeOutDate { get; set; }

        [Required]
        [Display(Name = "Pay Point Review Date:")]
        public string payPointReviewDate { get; set; }

        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Please Select")]
        [Display(Name = "Notice Period:")]
        public int? noticePeriod { get; set; }

        [Display(Name = "Date of Notice:")]
        public string dateOfNotice { get; set; }

        [Display(Name = "Probation Period:")]
        public int? probationPeriod { get; set; }

        [Display(Name = "Director:")]
        public int? isDirector { get; set; }

        [Display(Name = "Active:")]
        public int? active { get; set; } = 0;

        [Display(Name = "End Date:")]
        public string endDate { get; set; }

        [Display(Name = "Reason for Leaving:")]
        public int leavingReason { get; set; }

        [Display(Name = "Director:")]
        public string teamDirectorName { get; set; }
        public int noOfWeeks { get; set; } = 1;
        public string annualLeaveEndDate { get; set; }

        [Display(Name = "Business Unit:")]
        public int? isBRS { get; set; } = 1;
        public int noteCount { get; set; }
        public string hrNote { get; set; }
        public string ModelDateFormate { get; set; } = "{0:dd/MM/yyyy}";

        public List<SaleryAmendment> saleryAmendments { get; set; }
        public WorkingHoursViewModel workingPattern { get; set; } = new WorkingHoursViewModel();
        public List<JobDetailNoteViewModel> notes { get; set; } = new List<JobDetailNoteViewModel>();
        public List<SelectListItem> jobRole { get; set; } = new List<SelectListItem>();
        public List<SelectListItem> teams { get; set; } = new List<SelectListItem>();
    }
}