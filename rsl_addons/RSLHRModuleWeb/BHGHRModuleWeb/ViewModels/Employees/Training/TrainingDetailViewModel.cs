﻿using BHGHRModuleWeb.ViewModels.Employees.Training;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{

    [Validator(typeof(TrainingDetailValidator))]
    public class TrainingDetailViewModel : TrainingDetailValidator
    {
        public TrainingDetailViewModel()
        {
            //approvedTraining = new List<ApprovedTraining>();
        }
        public int trainingId { get; set; }
        [Display(Name = "Training Need Justification:")]
        public string justification { get; set; }
        [Display(Name = "Training Course:")]
        public string course { get; set; }
        [Display(Name = "Start Date:")]
        public string startDate { get; set; }
        [Display(Name = "End Date:")]
        public string endDate { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = "Total number of days must be greater than 0")]
        [Display(Name = "Total Number Of Days:")]
        public int? totalNumberOfDays { get; set; }
        [Display(Name = "Provider Name:")]
        public string providerName { get; set; }
        [Display(Name = "Provider Website:")]
        public string providerWebsite { get; set; }
        [Display(Name = "Location:")]
        public string location { get; set; }

        [Display(Name = "Postcode:")]
        public string postcode { get; set; }

        [Display(Name = "Venue:")]
        public string venue { get; set; } = "";

        [Display(Name = "Total Cost(inc VAT):")]
        [Range(0.01, double.MaxValue, ErrorMessage = "Total Cost must be a valid number greater than 0")]
        public decimal? totalCost { get; set; }
        [Display(Name = "Submitted By:")]
        public int? isSubmittedBy { get; set; }
        [Display(Name = "Mandatory Training:")]
        public bool isMandatoryTraining { get; set; }
        [Display(Name = "Renew:")]
        public string expiry { get; set; }
        [Display(Name = "Additional Notes:")]
        public string additionalNotes { get; set; }
        public string status { get; set; }
        public int? employeeId { get; set; }
        public int? createdby { get; set; }
        public string createdByName { get; set; }
        public string appTrainings { get; set; }

        [Display(Name = "Approved Training")]
        public string approvedtraining { get; set; }

        [Display(Name = "Remuneration")]
        public bool? isRemuneration { get; set; }

        [Display(Name = "£")]
        [Range(0.01, double.MaxValue)]
        public decimal? remunerationCost { get; set; }
        //public List<ApprovedTraining> approvedTraining { get; set; }
        public bool isActive { get; set; }

        public string renewDate { get; set; }

        public string recordedDate { get; set; }
        public string createdDate { get; set; } = DateTime.Now.ToShortDateString();
        public int? professionalQualification { get; set; }

        [Display(Name = "Add Employees")]
        public List<SelectEmployee> employees { get; set; }

        public WorkingHoursViewModel workingPattern { get; set; }
        public string employeeName { get; set; }
        //public List<ApprovedTraining> approvedTraining { get; set; }

    }
    public class SelectEmployee
    {
        public string employeeId { get; set; }
        [Display(Name = "Approved Training")]
        public string employeeName { get; set; }
        [Display(Name = "Remuneration")]
        public bool team { get; set; }
    }
}