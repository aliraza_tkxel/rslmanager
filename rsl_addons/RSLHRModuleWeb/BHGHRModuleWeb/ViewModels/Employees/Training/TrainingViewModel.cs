﻿using System;
using System.Collections.Generic;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class TrainingViewModel
    {
        public TrainingViewModel()
        {
            detail = new TrainingDetailViewModel();
        }

        public TrainingDetailViewModel detail { get; set; }
        public TrainingGridViewModel trainingList { get; set; } = new TrainingGridViewModel();

    }
}