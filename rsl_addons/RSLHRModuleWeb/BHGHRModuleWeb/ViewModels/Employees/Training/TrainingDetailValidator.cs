﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels.Employees.Training
{
    public class TrainingDetailValidator : AbstractValidator<TrainingDetailViewModel>
    {
        public TrainingDetailValidator()
        {
            RuleFor(x => x.startDate).NotEmpty().WithMessage("Start Date is required");
            RuleFor(x => x.endDate).NotEmpty().WithMessage("End Date is required");
            RuleFor(x => x.course).NotEmpty().WithMessage("Training Course is required");
            RuleFor(x => x.providerName).NotEmpty().WithMessage("Provider name is required");
            RuleFor(x => x.postcode).NotEmpty().WithMessage("Postcode is required");
            RuleFor(x => x.postcode).Matches("^([A-PR-UWYZa-pr-uwyz](([0-9](([0-9]|[A-HJKSTUWa-hjkstuw])?)?)|([A-HK-Ya-hk-y][0-9]([0-9]|[ABEHMNPRVWXYabehmnprvwxy])?)) [0-9][ABD-HJLNP-UW-Zabd-hjlnp-uw-z]{2})");
        }
    }
}