﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class TrainingGridViewModel
    {
        public List<TrainingDetailViewModel> trainingList { get; set; } = new List<TrainingDetailViewModel>();
        public PaginationViewModel pagination { get; set; } = new PaginationViewModel();
        public TrainingDetailViewModel detail { get; set; } = new TrainingDetailViewModel();
    }
}