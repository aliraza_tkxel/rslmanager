﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class EmployeesListViewModel
    {
        public int teamId { get; set; } = 0;
        public List<EmployeesViewModel> employees { get; set; }
        public PaginationViewModel pagination { get; set; } = new PaginationViewModel();
    }
    public class EmployeesViewModel
    {
        public int employeeId { get; set; }
        public string name { get; set; }
        public string team { get; set; }
        public string jobTitle { get; set; }
        public string grade { get; set; }
        public bool isSelected { get; set; } = false;
    }
}