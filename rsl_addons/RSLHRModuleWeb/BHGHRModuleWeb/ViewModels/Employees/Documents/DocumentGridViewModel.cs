﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class DocumentGridViewModel
    {
     public   DocumentGridViewModel() {
            documentList = new List<DocumentUploadViewModel>();
        }
        public List<DocumentUploadViewModel> documentList { get; set; }
        public PaginationViewModel pagination { get; set; } = new PaginationViewModel();
    }
}