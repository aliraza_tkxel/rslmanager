﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class DocumentSearchViewModel
    {
        [Display(Name = "Type:")]
        public string documetTypeId { get; set; }
        [Display(Name = "Title:")]
        public string title { get; set; }
        [Display(Name = "Keywords:")]
        public string keywords { get; set; }
        public int employeeId { get; set; }
    }
}