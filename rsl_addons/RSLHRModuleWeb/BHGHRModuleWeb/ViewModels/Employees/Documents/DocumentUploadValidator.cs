﻿using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels.Employees.Documents
{
    public class DocumentUploadValidator : AbstractValidator<DocumentUploadViewModel>
    {
        public DocumentUploadValidator()
        {
            RuleFor(x => x.title).NotEmpty().WithMessage("Title is required");
            RuleFor(x => x.documentDate).NotEmpty().WithMessage("Document Date is required");
            RuleFor(x => x.documentExpiry).NotEmpty().WithMessage("Expiry Date is required");
            RuleFor(x => x.file).NotEmpty().WithMessage("Upload a file");
        }
    }
}