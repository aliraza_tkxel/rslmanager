﻿using BHGHRModuleWeb.ViewModels.Employees.Documents;
using FluentValidation.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
   
    public class DocumentUploadViewModel 
    {
        public int documentId { get; set; }
        [Display(Name = "Type:")]
        [Required(ErrorMessage = "Document Type is required")]
        public int documentTypeId { get; set; }
        public int? documentVisibilityId { get; set; }
        public string documentType { get; set; }
        [Display(Name = "Title:")]
        [Required(ErrorMessage = "Document title is required")]
        public string title { get; set; }
        [Display(Name = "Date of Document:")]
        [Required(ErrorMessage = "Document date is required")]
        public string documentDate { get; set; }
        [Display(Name = "Expiry:")]
        public string documentExpiry { get; set; }
        public string  documentDateTime { get; set; }
        public string documentExpiryTime { get; set; }
        public int employeeId { get; set; }
        public string createdDate { get; set; }
        public int createdBy { get; set; }
        public string createdByName { get; set; }
        public string documentPath { get; set; }
        public bool? employeeVisibility { get; set; }
        public bool employeeVisibi { get; set; } = true;
        [Display(Name = "Keywords:")]
        public string keywords { get; set; }
        [Required(ErrorMessage = "Document is required")]
        public HttpPostedFileBase file { get; set; }

    }
}