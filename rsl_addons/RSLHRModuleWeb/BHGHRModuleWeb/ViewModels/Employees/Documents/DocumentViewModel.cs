﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BHGHRModuleWeb.ViewModels
{
    public class DocumentViewModel
    {   
        public DocumentViewModel()
        {
            docSearch= new DocumentSearchViewModel();
            docUpload= new DocumentUploadViewModel();
        }
        public DocumentSearchViewModel docSearch { get; set; }
        public DocumentUploadViewModel docUpload { get; set; }
        public DocumentGridViewModel documentList { get; set; }
        public LookUpsViewModel lookUps { get; set; }
        public bool isHR { get; set; }
        public List<SelectListItem> documentType
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.documentType.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                return list;
            }
            set { }
        }
        public List<SelectListItem> documentVisibility
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.documentVisibility.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                return list;
            }
            set { }
        }
    }
}