﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BHGHRModuleWeb.ViewModels
{
    public class SkillsDetailViewModel
    {
        [Display(Name = "qualificationId")]
        public int qualificationId { get; set; }

        [Display(Name = "employeeId")]
        public int? employeeId { get; set; }

        [Display(Name = "Level:")]
        [Required(ErrorMessage = "Please select level")]
        public int? qualLevelID { get; set; }

        [Display(Name = "Level")]
        public string levelDescription { get; set; }

        [Display(Name = "Subject:")]
        [Required(ErrorMessage = "The subject is required")]
        public string subject { get; set; }

        [Display(Name = "Result:")]
        [Required(ErrorMessage = "The result is required")]
        public string result { get; set; }

        [Display(Name = "Date Completed:")]
        [Required(ErrorMessage = "Please select Date Completed")]
        public string qualificationDate { get; set; }

        [Display(Name = "Professional Accreditation:")]
        public List<string> professionalQualification { get; set; }

        public string pQualification { get; set; }       

        [Display(Name = "createdBy")]
        public int? createdBy { get; set; }  

        [Display(Name = "NQF Equivalent Level:")]
        public int? equivalentLevel { get; set; }
    }
    public class LanguageSkillsViewModel
    {
        
        public string language { get; set; }
        public string spokenWritten { get; set; }        
        public bool isSpoken { get; set; }
        
        public int languageId { get; set; }
        public bool isWritten { get; set; }
        public int? employeeId { get; set; }
        public int? createdBy { get; set; }
        [Display(Name = "Language:")]
        [Required]
        public int? languageTypeId { get; set; }
        [Display(Name = "Other:")]
    
        public string languageOther { get; set; }

    }
}