﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BHGHRModuleWeb.ViewModels
{
    public class SkillsResponseViewModel
    {
        public SkillsDetailViewModel skillDetail { get; set; } = new SkillsDetailViewModel();
        public SkillsGridViewModel skills { get; set; } = new SkillsGridViewModel(); 
        public LookUpsViewModel lookUps { get; set; }
        public LanguageSkillsViewModel language { get; set; }
        public List<LanguageSkillsViewModel> languageSkills { get; set; } = new List<LanguageSkillsViewModel>();
        public List<SelectList> spokenLanguageLookUp { get; set; }
        public List<SelectListItem> qualificationLevel
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.qualificationLevel.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                // list.Insert(0, new SelectListItem() { Text = "Please Select", Value = "0", Selected = true });
                // list.Add(new SelectListItem() { Text = "Please Select Marital Status", Value = "0", Selected = true });
                return list;
            }
            set { }
        }
        public List<SelectListItem> equivalentLevelLookUP
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.equivalentLevel.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                // list.Insert(0, new SelectListItem() { Text = "Please Select", Value = "0", Selected = true });
                // list.Add(new SelectListItem() { Text = "Please Select Marital Status", Value = "0", Selected = true });
                return list;
            }
            set { }
        }
        public List<SelectListItem> languageTypesLookUP
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.languageTypes.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();                
                return list;
            }
            set { }
        }
    }
}