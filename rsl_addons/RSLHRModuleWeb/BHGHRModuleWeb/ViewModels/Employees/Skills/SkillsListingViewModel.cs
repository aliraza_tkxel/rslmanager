﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BHGHRModuleWeb.ViewModels
{
    public class SkillsListingViewModel
    {
        public SkillsDetailViewModel skill {get; set;}
        public SkillsGridViewModel skillsList { get; set; }
        public PaginationViewModel pagination { get; set; } = new PaginationViewModel();

    }
}