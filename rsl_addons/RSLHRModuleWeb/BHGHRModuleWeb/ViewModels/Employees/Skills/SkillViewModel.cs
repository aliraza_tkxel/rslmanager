﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BHGHRModuleWeb.ViewModels
{
    public class SkillViewModel
    {
        [DisplayName("Type:")]
        public SelectList type { get; set; }
        [DisplayName("Level:")]
        public SelectList level { get; set; }
        [DisplayName("Subject:")]
        public string subject { get; set; }
        [DisplayName("Result:")]
        public string result { get; set; }
        [DisplayName("Date:")]
        public string date { get; set; }
        [DisplayName("Specialised Training:")]
        public string specialisedTraining { get; set; }
        [DisplayName("Professional Accreditation:")]
        public string professionalQualification { get; set; }
        [DisplayName("Language:")]
        public string languageSkills { get; set; }
        [DisplayName("Other Spoken/Written Language:")]
        public SelectList otherSpokenWrittenLanguage { get; set; }
        [DisplayName("Mandatory Training:")]
        public string mandatoryTraining { get; set; }
        [DisplayName("Expiry:")]
        public string expiry { get; set; }
        [DisplayName("Approved Training:")]
        public string approvedTraining { get; set; }
        [DisplayName("Yes")]
        public bool yes { get; set; }
        [DisplayName("No")]
        public bool no { get; set; }
        [DisplayName("$")]
        public decimal pound { get; set; }
        public EmployeesListViewModel employees { get; set; }
        public string modelDateFormat { get; set; }
    }
}