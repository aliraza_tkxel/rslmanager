﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class SkillsGridViewModel
    {
        public List<SkillsDetailViewModel> skillsList { get; set; }
        public PaginationViewModel pagination { get; set; } = new PaginationViewModel();
    }
}