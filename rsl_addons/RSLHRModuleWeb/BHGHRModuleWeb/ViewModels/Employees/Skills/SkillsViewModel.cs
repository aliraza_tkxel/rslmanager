﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels.Employees.Skills
{
    public class SkillsViewModel
    {
        public int qualificationId { get; set; }
        public int employeeId { get; set; }
        public int? qualLevelID { get; set; }
        public string levelDescription { get; set; }
        public string subject { get; set; }
        public string result { get; set; }
        public string qualificationDate { get; set; }
        public string professionalQualification { get; set; }
        public string languageSkills { get; set; }
        public string spokenLanguage { get; set; }
        public string spokenLanguageOther { get; set; }
        public string mandatoryTraining { get; set; }
        public string mandatoryTrainingExpiry { get; set; }
        public string approvedTraining { get; set; }
        public bool? remuneration { get; set; }
        public decimal remunerationCost { get; set; }
        public int? createdBy { get; set; }
    }
}