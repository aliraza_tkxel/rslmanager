﻿using BHGHRModuleWeb.Helpers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BHGHRModuleWeb.ViewModels
{
    public class BenefitsViewModel
    {
        public BenefitsViewModel()
        {
            vehiclelst =  new List<BenefitVehicleViewModel>();
            generallst = new List<BenefitGeneralViewModel>();
        }
        public string requestType { get; set; }
        [Display(Name = "Benefit Type:")]
        public int benefitId { get; set; }
        public int? employeeId { get; set; }
        [Display(Name = "Vehicle:")]
        public int? vehicleId { get; set; }
        public int benefitType { get; set; }
        //Accomodation
        [Display(Name = "Rent £:")]
        [DataType(DataType.Currency)]
        public decimal? accomodationRent { get; set; }
        [Display(Name = "Council Tax £:")]
        [DataType(DataType.Currency)]
        public decimal? councilTax { get; set; }
        [Display(Name = "Heating £:")]
        [DataType(DataType.Currency)]
        public decimal? heating { get; set; }
        [Display(Name = "Phone Rental £:")]
        [DataType(DataType.Currency)]
        public decimal? lineRental { get; set; }
        //Company Car
        [Display(Name = "Make:")]
        [DataType(DataType.Text)]
        public string carMake { get; set; }
        [Display(Name = "Model:")]
        [DataType(DataType.Text)]
        public string model { get; set; }
        [Display(Name = "List Price £:")]
        [DataType(DataType.Currency)]
        public decimal? listPrice { get; set; }
        [Display(Name = "Contract Hire Charge £:")]
        [DataType(DataType.Currency)]
        public decimal? contHireCharge { get; set; }
        [Display(Name = "Employees Contribution £:")]
        [DataType(DataType.Currency)]
        public decimal? empContribution { get; set; }
        [Display(Name = "Excess £:")]
        [DataType(DataType.Currency)]
        public decimal? excessContribution { get; set; }
        [Display(Name = "Start Date:")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? carStartDate { get; set; }
        [Display(Name = "End Date:")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? carEndDate { get; set; }
        [Display(Name = "Insurance Policy Renewal:")]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? insuranceRenewalDate { get; set; }
        [Display(Name = "CO2 Emissions:")]
        [DataType(DataType.Currency)]
        public double? co2Emissions { get; set; }
        [Display(Name = "Fuel Type:")]
        [DataType(DataType.Text)]
        public string fuel { get; set; }
        [Display(Name = "Employers Contribution £:")]
        [DataType(DataType.Currency)]
        public decimal? compEmpContribution { get; set; }
        [Display(Name = "Driving Licence Number:")]
        [DataType(DataType.Text)]
        public string drivingLicenceNo { get; set; }
        public string imagePath
        {
            get
            {
                //  return Path.Combine(HttpContext.Current.Request.Url.Authority + "/" + "EmployeeDocuments" + "/" + employeeId.ToString() + "/", drivingLicenceImage == null ? "" : drivingLicenceImage);
                return Path.Combine(HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority) + "/EmployeeDocuments/" + employeeId.ToString() + "/", drivingLicenceImage == null ? "" : drivingLicenceImage);
            }
        }
        [Display(Name = "MOT certificate Number:")]
        [DataType(DataType.Text)]
        public string motCertNo { get; set; }
        [Display(Name = "Insurance Policy Number:")]
        [DataType(DataType.Text)]
        public string insuranceNo { get; set; }
        [Display(Name = "Driving Licence Image:")]
        public string drivingLicenceImage { get; set; }
        // Essential User
        [Display(Name = "Engine Size(CC):")]
        [Range(0, int.MaxValue, ErrorMessage = "Please enter valid integer Number")]
        public string engineSize { get; set; }
        [Display(Name = "Car Allowance:")]
        [DataType(DataType.Currency)]
        public decimal? carAllowance { get; set; }
        [Display(Name = "Car Loan:")]
        public int? carLoan { get; set; }
        [Display(Name = "ECU Top Up £:")]
        [Range(0, double.MaxValue, ErrorMessage = "Please enter valid  Number")]
        public decimal? ecuTopUp { get; set; }
        [Display(Name = "MOT Renewal:")]
        [DataType(DataType.Date)]
        public DateTime? motRenewalDate { get; set; }
        [Display(Name = "Tool Allowance £:")]
        [Range(0, double.MaxValue, ErrorMessage = "Please enter valid  Number")]
        public decimal? toolAllowance { get; set; }
        [Display(Name = "First Aid £:")]
        [Range(0, double.MaxValue, ErrorMessage = "Please enter valid  Number")]
        public decimal? firstAid { get; set; }
        [Display(Name = "Union Subscription £:")]
        [Range(0, double.MaxValue, ErrorMessage = "Please enter valid  Number")]
        public decimal? unionSubscription { get; set; }
        [Display(Name = "Life Assurance £:")]
        [Range(0, double.MaxValue, ErrorMessage = "Please enter valid  Number")]
        public decimal? lifeAssurance { get; set; }
        [Display(Name = "Enhanced Holiday Entitlement:")]
        [Range(0, int.MaxValue, ErrorMessage = "Please enter valid  Number")]
        public int? enhancedHolidayEntitlement { get; set; }
        [Display(Name = "Car Parking Facilities £:")]
        [Range(0, double.MaxValue, ErrorMessage = "Please enter valid  Number")]
        public decimal? carParkingFacilities { get; set; }
        [Display(Name = "Eye Care Assistance £:")]
        [Range(0, double.MaxValue, ErrorMessage = "Please enter valid  Number")]
        public decimal? eyeCareAssistance { get; set; }
        [Display(Name = "Employee Assistance Programme:")]
        [DataType(DataType.Text)]
        public string employeeAssistanceProgramme { get; set; }
        [Display(Name = "Enhanced Sick Pay £:")]
        [Range(0, double.MaxValue, ErrorMessage = "Please enter valid  Number")]
        public decimal? enhancedSickPay { get; set; }
        [Display(Name = "Learning and Development £:")]
        [Range(0, double.MaxValue, ErrorMessage = "Please enter valid  Number")]
        public decimal? learningAndDevelopment { get; set; }
        [Display(Name = "Professional Subscriptions £:")]
        [Range(0, double.MaxValue, ErrorMessage = "Please enter valid  Number")]
        public decimal? professionalSubscriptions { get; set; }
        [Display(Name = "Childcare Vouchers £:")]
        [Range(0, double.MaxValue, ErrorMessage = "Please enter valid  Number")]
        public decimal? childcareVouchers { get; set; }
        [Display(Name = "Flu & Hep B Jabs £:")]
        [Range(0, double.MaxValue, ErrorMessage = "Please enter valid  Number")]
        public decimal? fluAndHepBJabs { get; set; }
        //---------General------------------
        [Display(Name = "Professional Subscription £:")]
        [DataType(DataType.Currency)]
        public decimal? professionalFees { get; set; }
        [Display(Name = "Telephone Allowance £:")]
        [DataType(DataType.Currency)]
        public decimal? telephoneAllowance { get; set; }
        [Display(Name = "First Aider Allowance £:")]
        [DataType(DataType.Currency)]
        public decimal? firstAidAllowance { get; set; }
        [Display(Name = "Call Out Payments £:")]
        [DataType(DataType.Currency)]
        public decimal? callOutAllowance { get; set; }
        /////////////////////// BUPA Medical Insurance///////////////
        [Display(Name = "Name:")]
        [Required]
        public int? medicalOrgId { get; set; }
        [Display(Name = "Taxable Benefit £:")]
        [DataType(DataType.Currency)]
        public decimal? taxableBenefit { get; set; }
        [Display(Name = "Annual Premium £:")]
        [DataType(DataType.Currency)]
        public decimal? annualPremium { get; set; }
        [Display(Name = "Additional Members:")]
        [DataType(DataType.Text)]
        public string additionalMembers { get; set; }
        [Display(Name = "Additional Members2:")]
        [DataType(DataType.Text)]
        public string additionalMembers2 { get; set; }
        [Display(Name = "Additional Members3:")]
        [DataType(DataType.Text)]
        public string additionalMembers3 { get; set; }
        [Display(Name = "Additional Members4:")]
        [DataType(DataType.Text)]
        public string additionalMembers4 { get; set; }
        [Display(Name = "Call Out Payments £:")]
        [DataType(DataType.Text)]
        public string additionalMembers5 { get; set; }
        [Display(Name = "Group Scheme Ref:")]
        [DataType(DataType.Text)]
        public string groupSchemeRef { get; set; }
        [Display(Name = "Membership No:")]
        [DataType(DataType.Text)]
        public string membershipNo { get; set; }
        /////////////////////// Pension/////////
        [Display(Name = "Membership No:")]
        [DataType(DataType.Text)]
        public string memNumber { get; set; }
        [Display(Name = "Scheme Name:")]
        [Required]
        public int? scheme { get; set; }
        [Display(Name = "Scheme Number:")]
        [DataType(DataType.Text)]
        public string schemeNumber { get; set; }
        [Display(Name = "Salary %:")]
        [Range(0, double.MaxValue, ErrorMessage = "Please enter valid  Number")]
        public double? salaryPercent { get; set; }
        [Display(Name = "Employee Contribution:")]
        [Range(0, double.MaxValue, ErrorMessage = "Please enter valid  Number")]
        public decimal? employeeContribution { get; set; }
        [Display(Name = "Employer Contribution:")]
        [Range(0, double.MaxValue, ErrorMessage = "Please enter valid  Number")]
        public decimal? employerContribution { get; set; }
        [Display(Name = "AVC £:")]
        [Range(0, double.MaxValue, ErrorMessage = "Please enter valid  Number")]
        public decimal? avc { get; set; }
        [Display(Name = "Contracted Out:")]
        [Range(0, int.MaxValue, ErrorMessage = "Please enter valid  Number")]
        public int? contractedOut { get; set; }
        public int? lastActionUser { get; set; }

        public string ModelDateFormate { get; set; } = "{0:dd/MM/yyyy}";

        [Display(Name = "Vehicle Registration:")]
        public string carRegistration { get; set; }

        [Display(Name = "Insurance Company:")]
        public string insuranceCompany { get; set; }

        [Display(Name = "Insurance Reimbursement Value £:")]
       
        public decimal?  insuranceReimbursement { get; set; }

        [Display(Name = "Policy Summary Upload:")]
        public string policySummaryPath { get; set; }

        [Display(Name = "DVLA Online Check:")]
        public bool?  dvlaOnline { get; set; }
        [Display(Name = "Checked:")]
        [DataType(DataType.Date)]
        public DateTime? dvlaOnlineDate { get; set; }
        [Display(Name = "V5 Upload:")]
        public string V5Path { get; set; }
        public BenefitVehicleViewModel vehicle { get; set; } = new BenefitVehicleViewModel();
        public List<BenefitVehicleViewModel> vehiclelst { get; set; }
        public BenefitGeneralViewModel general { get; set; } = new BenefitGeneralViewModel();
        public List<BenefitGeneralViewModel> generallst { get; set; }

        public BenefitHealthViewModel health { get; set; } = new BenefitHealthViewModel();
        public BenefitPensionViewModel pension { get; set; } = new BenefitPensionViewModel();
        public BenefitPRPViewModel prp { get; set; } = new BenefitPRPViewModel();
        public BenefitSubscriptionViewModel subscription { get; set; } = new BenefitSubscriptionViewModel();
        public List<BenefitSubscriptionViewModel> subscriptionlst { get; set; } = new List<BenefitSubscriptionViewModel>();

    }
}