﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class BenefitSubscriptionViewModel
    {

         public int benefitSubscriptionId { get; set; }
        [Display(Name = "Subscriptions For Tax Year:")]
        public int    fiscalYear           {get; set;}
         public int   benefitId             {get; set;}
         public int?   employeeId            {get; set;}
        [Display(Name = "Subscription Title:")]
        public string   subscriptionTitle     {get; set;}
        [Display(Name = "Subscription Cost £:")]
        public decimal   subscriptionCost      {get; set;}
        [Display(Name = "Total Subscriptions £:")]
        public decimal totalSubscription { get; set; }

        public string pSubscription { get; set; }
    }
}