﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BHGHRModuleWeb.ViewModels
{
    public class BenefitsResponseViewModel
    {
        public BenefitsResponseViewModel()
        {
            benefits=new  BenefitsViewModel();
        }
        public BenefitsViewModel benefits { get; set; } = new BenefitsViewModel();
        public LookUpsViewModel lookUps { get; set; } = new LookUpsViewModel();
        public List<SelectListItem> benefitTypeList
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.benefitTypeList.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Add(new SelectListItem() { Text = "Vehicle", Value = "Vehicle" });
                list.RemoveAll(x => x.Text == "Essential User" || x.Text == "Company Car" || x.Text == "Accomodation");
                list.Insert(0, new SelectListItem() { Value = "0", Text = "Please Select", Selected = true });
                list.RemoveAll(x => x.Text == "PRP");
                list.Add(new SelectListItem() { Text = "PRP", Value = "PRP" });
                return list;
            }
            set { }
        }
        public List<SelectListItem> fiscalYearList
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.fiscalYearList.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).OrderByDescending(x=>x.Text).ToList();
                return list;
            }
            set { }
        }
        public List<SelectListItem> schemesList
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.schemesList.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Insert(0, new SelectListItem() { Value = "0", Text = "Please Select", Selected = true });
                return list;
            }
            set { }
        }
        public List<SelectListItem> medicalOrganizationList
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.medicalOrganizationList.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                return list;
            }
            set { }
        }
        public List<SelectListItem> medicalTypeList
        {
            get
            {
                var list = new List<SelectListItem>();
                list = lookUps.medicalTypeList.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
                list.Insert(0, new SelectListItem() { Value = "0", Text = "Please Select", Selected = true });
                return list;
            }
            set { }
        }
        public SelectList fuelCar { get; set; }
        public SelectList vehicle { get; set; } 


       
    }
}