﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class BenefitPensionViewModel
    {
        public int benefitPensionId { get; set; }
        public int benefitId { get; set; }
        public int? employeeId { get; set; }

        [Display(Name = "Pension Year:")]
        public int fiscalYear { get; set; }

        [Display(Name = "Rent £:")]
        [DataType(DataType.Currency)]
        public decimal? accomodationRent { get; set; }

        [Display(Name = "Council Tax £:")]
        [DataType(DataType.Currency)]
        public decimal? councilTax { get; set; }

        [Display(Name = "Phone Rental £:")]
        [DataType(DataType.Currency)]
        public decimal? lineRental { get; set; }

        [Display(Name = "Heating £:")]
        [DataType(DataType.Currency)]
        public decimal? heating { get; set; }

        [Display(Name = "Salary %:")]
        [Range(0, double.MaxValue, ErrorMessage = "Please enter valid  Number")]
        public double? salaryPercent { get; set; }

        [Display(Name = "Employee Contribution:")]
        [Range(0, double.MaxValue, ErrorMessage = "Please enter valid  Number")]
        public decimal? employeeContribution { get; set; }

        [Display(Name = "Employer Contribution:")]
        [Range(0, double.MaxValue, ErrorMessage = "Please enter valid  Number")]
        public decimal? employerContribution { get; set; }

        [Display(Name = "AVC £:")]
        [Range(0, double.MaxValue, ErrorMessage = "Please enter valid  Number")]
        public decimal? avc { get; set; }

        [Display(Name = "Contracted Out:")]
        [Range(0, int.MaxValue, ErrorMessage = "Please enter valid  Number")]
        public int? contractedOut { get; set; }


    }
}