﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class BenefitVehicleViewModel
    {



        public int benefitVehicleId { get; set; }
        [Display(Name = "Fiscal Year:")]
        public int fiscalYear { get; set; }
        public int? employeeId { get; set; }
        public int benefitId { get; set; }

        [Display(Name = "Type:")]
        public int? Type { get; set; }

        [Display(Name = "Start Date:")]
        public string carStartDate { get; set; }
        [Display(Name = "End Date:")]
        public string carEndDate { get; set; }
        [Display(Name = "Vehicle Registration:")]
        public string carRegistration { get; set; }
        [Display(Name = "Make:")]
        [DataType(DataType.Text)]
        public string carMake { get; set; }
        [Display(Name = "Model:")]
        [DataType(DataType.Text)]
        public string model { get; set; }
        [Display(Name = "Fuel Type:")]
        [DataType(DataType.Text)]
        public string fuel { get; set; }
        [Display(Name = "Engine Size(CC):")]
        public string engineSize { get; set; }
        [Display(Name = "Additional Driver:")]
        public string additionalDriver { get; set; }
        [Display(Name = "MOT Renewal:")]
        public string motRenewalDate { get; set; }
        [Display(Name = "V5 Upload:")]
        public string V5Path { get; set; }
        [Display(Name = "Insurance Company:")]
        public string insuranceCompany { get; set; }
        [Display(Name = "Insurance Policy Renewal:")]
        public string insuranceRenewalDate { get; set; }
        [Display(Name = "Insurance Reimbursement Value £:")]
        public decimal? insuranceReimbursement { get; set; }
        [Display(Name = "Policy Summary Upload:")]
        public string policySummaryPath { get; set; }
        [Display(Name = "DVLA Online Check:")]
        public bool? dvlaOnline { get; set; }
        [Display(Name = "Checked:")]
        public string dvlaOnlineDate { get; set; }
        public bool isReadOnly { get; set; }
        public string V5doc { get; set; }
        public string Summarydoc { get; set; }

}
}