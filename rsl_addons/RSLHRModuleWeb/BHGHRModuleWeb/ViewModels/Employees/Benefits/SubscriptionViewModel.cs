﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class SubscriptionViewModel
    {
         public string   title    {get; set;}
         public int   year     {get; set;}
         public decimal   amount   {get; set;}
    }
}