﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class BenefitHealthViewModel
    {
         public int     benefitHealthId     {get; set;}
         public int     benefitId           {get; set;}
         public int?     employeeId          {get; set;}
        [Display(Name = "Health Cover/Plan For:")]
        public int     fiscalYear          {get; set;}
        [Display(Name = "Provider:")]
        public int     medicalOrgId        {get; set;}

        [Required]
        [Range(1, int.MaxValue)]
        [Display(Name = "Type:")]
        public int?     isPrivateMedical   {get; set;}

        [Display(Name = "Excess Paid £:")]
        public decimal?     excessPaid      {get; set;}

        [Required(ErrorMessage = "End Date is Required")]
        [Display(Name = "Effective From:")]
        public string     effectiveFrom    {get; set;}

        [Required(ErrorMessage = "End Date is Required")]
        [Display(Name = "End Date:")]
        public string endDate { get; set; }

        [Display(Name = "Annual Premium:")]
        public decimal?     annualPremium   {get; set;}
        [Display(Name = "Membership Number:")]
        public string membershipNumber { get; set; }

    }

    public class BenefitHealthHistoryViewModel
    {
        public int benefitHealthId { get; set; }
        public List<BenefitHealthHistory> history { get; set; } = new List<BenefitHealthHistory>();
    }

    public class BenefitHealthHistory
    {
        public int benefitId { get; set; }
        public int? employeeId { get; set; }
        public int fiscalYear { get; set; }
        public string fiscalYears { get; set; }
        public int medicalOrgId { get; set; }
        public int? isPrivateMedical { get; set; }
        public string Type { get; set; }
        public string Provider { get; set; }
        public decimal? excessPaid { get; set; }
        public string effectiveFrom { get; set; }
        public string endDate { get; set; }
        public decimal? annualPremium { get; set; }
        public string membershipNumber { get; set; }
    }
}