﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class BenefitPRPViewModel
    {
        public int benefitPRPId { get; set; }
        public int benefitId { get; set; }
        public int? employeeId { get; set; }
        [Display(Name = "PRP Year:")]
        public int fiscalYear { get; set; }

        [Display(Name = "Q1 £:")]
        [DataType(DataType.Currency)]
        public decimal? q1 { get; set; }

        [Display(Name = "Q2 £:")]
        [DataType(DataType.Currency)]
        public decimal? q2 { get; set; }

        [Display(Name = "Q3 £:")]
        [DataType(DataType.Currency)]
        public decimal? q3 { get; set; }

        [Display(Name = "Q4 £:")]
        [DataType(DataType.Currency)]
        public decimal? q4 { get; set; }


    }
}