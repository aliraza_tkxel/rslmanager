﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class BenefitGeneralViewModel
    {
        public int benefitGeneralId { get; set; }
        [Display(Name = "General Allowances for Tax Year:")]
        public int fiscalYear { get; set; }
        public int? employeeId { get; set; }
        public int benefitId { get; set; }
        [Display(Name = "ECU £:")]
        [DataType(DataType.Currency)]
        public decimal? ECU { get; set; }
        [Display(Name = "Enhanced Family Leave £:")]
        [DataType(DataType.Currency)]
        public decimal? EnhancedFamilyLeave { get; set; }
        [Display(Name = "PHI £:")]
        [DataType(DataType.Currency)]
        public decimal? PHI { get; set; }
        [Display(Name = "Other £:")]
        [DataType(DataType.Currency)]
        public decimal? Other { get; set; }
        [Display(Name = "Overtime £:")]
        [DataType(DataType.Currency)]
        public decimal? Overtime { get; set; }
        [Display(Name = "Stand By Payments £:")]
        [DataType(DataType.Currency)]
        public decimal? callOutAllowance { get; set; }

        [Display(Name = "Birthday Leave £:")]
        [DataType(DataType.Currency)]
        public decimal?   birthdayLeave                { get; set; }
        [Display(Name = "Car Parking Facilities £:")]
        [DataType(DataType.Currency)]
        public decimal?   carParking                   { get; set; }
        [Display(Name = "Childcare Vouchers £:")]
        [DataType(DataType.Currency)]
        public decimal?   childCareVouchers            { get; set; }
        [Display(Name = "EAP £:")]
        [DataType(DataType.Currency)]
        public decimal?   employeeAssistance           { get; set; }
        [Display(Name = "Enhanced HolidayEntitlement £:")]
        [DataType(DataType.Currency)]
        public decimal?   enhancedHoliday              { get; set; }
        [Display(Name = "Enhanced Sick Pay £:")]
        [DataType(DataType.Currency)]
        public decimal?   enhancedSickPay              { get; set; }
        [Display(Name = "Eye Care Assistance £:")]
        [DataType(DataType.Currency)]
        public decimal?   eyeCareAssistance            { get; set; }
        [Display(Name = "First Aider Allowance £:")]
        [DataType(DataType.Currency)]
        public decimal?   firstAiderAllowance          { get; set; }
        [Display(Name = "Flu & Hep B Jabs £:")]
        [DataType(DataType.Currency)]
        public decimal?   fluHepBJab                  { get; set; }
        [Display(Name = "Learning & Development £:")]
        [DataType(DataType.Currency)]
        public decimal?   learningAndDevelopment       { get; set; }
        [Display(Name = "Life Assurance £:")]
        [DataType(DataType.Currency)]
        public decimal?   lifeAssurance                { get; set; }
        [Display(Name = "Personal Day £:")]
        [DataType(DataType.Currency)]
        public decimal? personalDay                   { get; set; }
        [Display(Name = "Voluntary Day £:")]
        [DataType(DataType.Currency)]
        public decimal? VoluntaryDay                { get; set; }
        public string    birthdayLeaveDate            { get; set; }
        public string    carParkingDate               { get; set; }
        public string    childCareVouchersDate        { get; set; }
        public string    employeeAssistanceDate       { get; set; }
        public string    enhancedHolidayDate          { get; set; }
        public string    enhancedSickPayDate          { get; set; }
        public string    eyeCareAssistanceDate        { get; set; }
        public string    firstAiderAllowanceDate      { get; set; }
        public string    fluHepBJabDate              { get; set; }
        public string    learningAndDevelopmentDate   { get; set; }
        public string    lifeAssuranceDate            { get; set; }
        public string    personalDayDate              { get; set; }
        public string    VoluntaryDayDate             { get; set; }
        public string callOutAllowanceDate { get; set; }
        public string ECUDate { get; set; }
        public string EnhancedFamilyLeaveDate { get; set; }
        public string PHIDate { get; set; }
        public string OtherDate { get; set; }
        public string OvertimeDate { get; set; }

        public decimal totalAllowance { get; set; }
        public bool isReadOnly { get; set; }
       
    }
}