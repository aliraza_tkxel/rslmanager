﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.ViewModels
{
    public class GeneralRequestViewModel
    {
        public int fiscalYear { get; set; }
        public int? employeeId { get; set; }
        public int benefitId { get; set; } 

    }
}