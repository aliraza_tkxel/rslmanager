﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.Helpers
{
    public class GeneralHelper
    {
        public static string LoginPath = ConfigurationManager.AppSettings["LoginPath"].ToString();

        protected string GetBaseUrl()
        {
            string BASE_URL = ConfigurationManager.AppSettings["URL"].ToString();
            string domain = HttpContext.Current.Request.Url.AbsoluteUri;
            string servername = domain.Substring((domain.IndexOf("//") + 2), domain.Substring((domain.IndexOf("//") + 2), domain.Substring((domain.IndexOf("//") + 2)).IndexOf("/")).Length);
            return "https://" + servername + BASE_URL;

            // return "https://" + servername; // For Development only. Do not remove
        }
    }
}