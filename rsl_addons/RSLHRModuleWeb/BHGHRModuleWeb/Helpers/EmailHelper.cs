﻿using BusinessModule.Constants;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace BHGHRModuleWeb.Helpers
{
    public class EmailHelper
    {
        public static void sendHtmlFormattedEmail(String recepientName, String recepientEmail, String subject, String body)
        {
            try
            {
                MailMessage mailMessage = new MailMessage();
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = true;
                mailMessage.To.Add(new MailAddress(recepientEmail, recepientName));

                //The SmtpClient gets configuration from Web.Config
                SmtpClient smtp = new SmtpClient();
                smtp.Send(mailMessage);
            }
            catch (IOException)
            {
                throw new ArgumentException(UserMessageConstants.ErrorSendingEmailMsg);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public static bool SendEmail(Stream ms, string receipentAddress, string emailBody, string receiverName, string subject)
        {
            try
            {
               emailBody = "<font size='3' face='arial'>" + emailBody + "</font>";
                MailMessage mailMessage = new MailMessage();
                mailMessage.Subject = subject;
                mailMessage.Body = emailBody;
                mailMessage.IsBodyHtml = true;
                mailMessage.Attachments.Add(new Attachment(ms, "Remittance_Advice.pdf", "application/pdf"));
                mailMessage.To.Add(new MailAddress(receipentAddress, receiverName));

                //The SmtpClient gets configuration from Web.Config
                SmtpClient smtp = new SmtpClient();
                smtp.Send(mailMessage);
                return true;



            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}