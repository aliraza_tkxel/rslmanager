﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;

namespace BHGHRModuleWeb.Helpers
{
    public class FileHelper
    {
        public static void WriteFileFromStream(Stream stream, string filePath)
        {
            using (FileStream fileToSave = new FileStream(filePath, FileMode.Create))
            {
                stream.CopyTo(fileToSave);
            }
        }

        public static string GetProfilePicUploadPath()
        {
            string imageUploadPath = ConfigurationManager.AppSettings["ProfilePicUploadPath"].ToString();
            //return HttpContext.Current.Server.MapPath(imageUploadPath);
            return imageUploadPath;
        }

        public static string GetRefDocUploadPath()
        {
            string documentUploadPath = ConfigurationManager.AppSettings["DocumentUploadPath"].ToString();
            //return HttpContext.Current.Server.MapPath(imageUploadPath);
            return documentUploadPath;
        }

        public static string GetRoleFileUploadPath()
        {
            string roleFileUploadPath = ConfigurationManager.AppSettings["RoleFileUploadPath"].ToString();
            //return HttpContext.Current.Server.MapPath(imageUploadPath);
            return roleFileUploadPath;
        }

        public static string GetEmploymentContractUploadPath()
        {
            string employmentContractUploadPath = ConfigurationManager.AppSettings["EmploymentContractUploadPath"].ToString();
            //return HttpContext.Current.Server.MapPath(imageUploadPath);
            return employmentContractUploadPath;
        }

        public static string GetSalaryLetterUploadPath()
        {
            string salaryLetterUploadPath = ConfigurationManager.AppSettings["SalaryLetterUploadPath"].ToString();
            //return HttpContext.Current.Server.MapPath(imageUploadPath);
            return salaryLetterUploadPath;
        }


        public static string GetDisabilityHealthDocUploadPath()
        {
            string disabilityHealthUploadPath = ConfigurationManager.AppSettings["DocumentUploadPath"].ToString();
            //return HttpContext.Current.Server.MapPath(imageUploadPath);
            return disabilityHealthUploadPath;
        }

        #region "getUniqueFileName"

        /// <summary>
        /// This function returns the unique filename
        /// </summary>
        /// <param name="pfileName"></param>
        /// <returns></returns>
        public static string getUniqueFileName(string pfileName)
        {
            string fileName = Path.GetFileNameWithoutExtension(pfileName);
            string ext = Path.GetExtension(pfileName);
            string uniqueString = DateTime.Now.ToString().GetHashCode().ToString("x");

            if (fileName.Length > 35)
            {
                fileName = fileName.Substring(0, 35);
            }

            fileName = fileName + uniqueString;

            return fileName + ext;
        }

        public static string getFileNameFromPath(string path)
        {
            var arrPath = path.Split('/');
            return arrPath[arrPath.Length - 1];
        }

        #region saving customer profile image on disk

        public static void saveImageOnDisk(string srcImagePath, string dstImagePath, long fileSize)
        {
            FileStream inStream = new FileStream(srcImagePath, FileMode.Open);
            FileStream outStream = File.Open(dstImagePath, FileMode.Create, FileAccess.Write);
            //read from the input stream in 4K chunks and save to output stream

            int bufferLen = (int)fileSize;
            byte[] buffer = new byte[bufferLen];
            int count = 0;
            while ((count = inStream.Read(buffer, 0, bufferLen)) > 0)
            {
                outStream.Write(buffer, 0, count);
            }
            outStream.Close();
            inStream.Close();
        }
        #endregion

        #endregion

        public static void createDirectory(string path)
        {
            try
            {
                if (!Directory.Exists(@path))
                {
                    Directory.CreateDirectory(@path);
                }

            }
            catch (IOException ex)
            {
                throw ex;
            }
        }

        public static string getLogicalEmployeeProfileImagePath(string _imagePath)
        {
            var requestURL = HttpContext.Current.Request.Url;
            string imagePath = requestURL.GetLeftPart(UriPartial.Authority) + "/EmployeeImage/" + _imagePath;

            return imagePath;
        }


        public static string getLogicalDocumentPath(string _Path, int employeeId)
        {
            var requestURL = HttpContext.Current.Request.Url;
            string imagePath = requestURL.GetLeftPart(UriPartial.Authority) + "/EmployeeDocuments/" + employeeId.ToString() + "/" + _Path;
            return imagePath;
        }

        public static string getLogicalCustomerProfileThumbPath()
        {
            var requestURL = HttpContext.Current.Request.Url;
            string imagePath = requestURL.GetLeftPart(UriPartial.Authority) + "/CustomerProfilePic/Thumbs/";
            return imagePath;
        }
        #region Delete file        
        public static void deleteImage(string filePath)
        {
            if (File.Exists(filePath) == true)
            {
                File.Delete(filePath);
            }
        }
        #endregion
    }
}