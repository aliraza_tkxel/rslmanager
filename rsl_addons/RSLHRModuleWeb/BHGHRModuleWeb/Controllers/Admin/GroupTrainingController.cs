﻿using AutoMapper;
using BHGHRModuleWeb.ViewModels;
using BHGHRModuleWeb.ViewModels.Admin.Training;
using BusinessModule.Constants;
using BusinessModule.NetworkManagers;
using BusinessModule.NetworkObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace BHGHRModuleWeb.Controllers
{
    public class GroupTrainingController : ABaseController
    {
        #region >>> Helpers <<<

        private GroupTrainingViewModel GroupTrainingsPopulate()
        {
            int status = 1;
            string searchText = string.Empty;
            if (Request["txtSearchText"] != null)
            {
                searchText = Request["txtSearchText"].ToString();
                ViewBag.searchText = searchText;
                Session["searchText"] = searchText;
            }
            var manager = new GroupTrainingManager();
            var dataRequest = new GroupTrainingListingDataRequest();
            dataRequest.pagination = new BusinessModule.NetworkObjects.PaginationDataRequest();
            if (Request["directorate"] != null && Request["directorate"] != "")
            {
                dataRequest.directorateId = Convert.ToInt32(Request["directorate"]);
            }
            if (Request["status"] != null && Request["status"] != "")
            {
                dataRequest.status = Convert.ToInt32(Request["status"]);
            }
            if (Request["team"] != null && Request["team"] != "")
            {
                dataRequest.teamId = Convert.ToInt32(Request["team"]);
            }
            dataRequest.searchText = searchText;
            if (Session["searchText"] != null)
            {
                dataRequest.searchText = Session["searchText"].ToString();
            }
            dataRequest.sortBy = "recordedDate";
            dataRequest.sortOrder = "DESC";
            int pageNo = 1;
            if (HttpContext.Request.QueryString["page"] != null && HttpContext.Request.QueryString["page"] != "0")
            {
                pageNo = Convert.ToInt32(HttpContext.Request.QueryString["page"]);
            }
            if (HttpContext.Request.QueryString["sort"] != null && HttpContext.Request.QueryString["sortdir"] != null)
            {
                dataRequest.sortBy = HttpContext.Request.QueryString["sort"].ToString();
                dataRequest.sortOrder = HttpContext.Request.QueryString["sortdir"].ToString();
            }

            dataRequest.pagination.pageSize = _ReportPageSize;
            dataRequest.pagination.pageNo = pageNo;
            var response = manager.GetGroupTrainingsList(dataRequest);
            GroupTrainingViewModel model = new JavaScriptSerializer().Deserialize<GroupTrainingViewModel>(response);
            Session["__Teams__"] = model.teams;
            Session["__Directorates__"] = model.directorates;
            if (model.groupTrainingList.pagination != null)
            {
                ViewBag.TotalRows = model.groupTrainingList.pagination.totalRows;
                ViewBag.PageSize = model.groupTrainingList.pagination.pageSize;
            }
            else
            {
                ViewBag.TotalRows = 0;
            }
            return model;
        }

        private EmployeesSearchViewModel EmployeesListPopulate()
        {
            int status = 1;
            string searchText = string.Empty;
            if (Request["txtSearchText"] != null)
            {
                searchText = Request["txtSearchText"].ToString();
                ViewBag.searchText = searchText;
                Session["searchText"] = searchText;
            }
            var manager = new GroupTrainingManager();
            var dataRequest = new EmployeeSearchDataRequest();
            dataRequest.pagination = new BusinessModule.NetworkObjects.PaginationDataRequest();
            if (Request["directorate"] != null && Request["directorate"] != "")
            {
                dataRequest.directorateId = Convert.ToInt32(Request["directorate"]);
            }
            if (Request["status"] != null && Request["status"] != "")
            {
                dataRequest.status = Convert.ToInt32(Request["status"]);
            }
            if (Request["team"] != null && Request["team"] != "")
            {
                dataRequest.teamId = Convert.ToInt32(Request["team"]);
            }
            dataRequest.searchText = searchText;
            if (Session["searchText"] != null)
            {
                dataRequest.searchText = Session["searchText"].ToString();
            }
            dataRequest.sortBy = "name";
            dataRequest.sortOrder = "ASC";
            int pageNo = 1;
            if (HttpContext.Request.QueryString["page"] != null && HttpContext.Request.QueryString["page"] != "0" && HttpContext.Request.QueryString["page"] != "undefined")
            {
                pageNo = Convert.ToInt32(HttpContext.Request.QueryString["page"]);
            }
            if (HttpContext.Request.QueryString["sort"] != null && HttpContext.Request.QueryString["sortdir"] != null)
            {
                dataRequest.sortBy = HttpContext.Request.QueryString["sort"].ToString();
                dataRequest.sortOrder = HttpContext.Request.QueryString["sortdir"].ToString();
            }

            dataRequest.pagination.pageSize = _ExportPageSize;
            dataRequest.pagination.pageNo = pageNo;
            var response = manager.GetEmployeesList(dataRequest);
            EmployeesSearchViewModel model = new JavaScriptSerializer().Deserialize<EmployeesSearchViewModel>(response);
            if (model.pagination != null)
            {
                ViewBag.TotalRows = model.pagination.totalRows;
                ViewBag.PageSize = model.pagination.pageSize;
            }
            else
            {
                ViewBag.TotalRows = 0;
            }

            if (Session["__Teams__"] != null)
            {
                model.teams = (List<SelectListItem>)Session["__Teams__"];
                model.directorates = (List<SelectListItem>)Session["__Directorates__"];
            }

            if (Request["employeeIds"] != null && Request["employeeIds"] != "")
            {
                model.employeeIds = Request["employeeIds"].ToString();
                var lstEmployeeId = StringToIntList(Request["employeeIds"].ToString());
                foreach (var item in lstEmployeeId)
                {
                    if (model.employees.FirstOrDefault(e => e.employeeId == item) != null)
                    {
                        model.employees.FirstOrDefault(e => e.employeeId == item).isSelected = true;
                    }
                }
            }
            

            return model;
        }

        public JsonResult TeamSelectListJson(string Origin, string Target, string Value)
        {
            var manager = new CommonManager();
            var directorateId = int.Parse(Value);
            var response = manager.GetTeamIdForDirectorate(directorateId);
            var model = new JavaScriptSerializer().Deserialize<List<LookUpFields>>(response);

            var jobDetail_jobRole = new List<SelectListItem>();
            jobDetail_jobRole = model.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
            jobDetail_jobRole.Insert(0, new SelectListItem() { Text = "Please Select Team", Value = "0", Selected = true });

            return Json(jobDetail_jobRole);
        }

        

        #endregion

        #region >>> Action Methods <<<

        // GET: Training
        public ActionResult Index()
        {
            GroupTrainingViewModel model = GroupTrainingsPopulate();

            return PartialView("~/Views/Admin/GroupTraining/_GroupTraining.cshtml", model);
        }

        public ActionResult SearchGroupTraining()
        {
            GroupTrainingViewModel model = GroupTrainingsPopulate();
            return PartialView("~/Views/Admin/GroupTraining/_TrainingListing.cshtml", model.groupTrainingList);
        }

        public ActionResult GetGroupDetails(int id)
        {
            try
            {
                var manager = new GroupTrainingManager();
                var response = manager.GetGroupDetails(id);
                GroupTrainingDetailViewModel model = new JavaScriptSerializer().Deserialize<GroupTrainingDetailViewModel>(response);
                var employeeIds = "";
                if (model.employeeDetails != null && model.employeeDetails.Count > 0)
                {
                    foreach (var item in model.employeeDetails)
                    {
                        employeeIds += item.employeeId.ToString() + ",";
                    }
                }
                employeeIds = employeeIds.Remove(employeeIds.LastIndexOf(','));
                model.employeeIds = employeeIds;
                return PartialView("~/Views/Admin/GroupTraining/_TrainingDetail.cshtml", model);
            }
            catch (Exception ex)
            {
                return PartialView("~/Views/Admin/GroupTraining/_TrainingDetail.cshtml");
            }
        }

        public ActionResult ResetDetails()
        {
            GroupTrainingDetailViewModel model = new GroupTrainingDetailViewModel();
            return PartialView("~/Views/Admin/GroupTraining/_TrainingDetail.cshtml", model);
        }

        public ActionResult GetEmployeeList(string employeeIds)
        {
            try
            {
                EmployeesSearchViewModel model = EmployeesListPopulate();
                return PartialView("~/Views/Admin/GroupTraining/_EmployeeSelectPopup.cshtml", model);
            }
            catch (Exception ex)
            {
                return PartialView("~/Views/Admin/GroupTraining/_EmployeeSelectPopup.cshtml");
            }
        }

        public ActionResult SearchEmployeeList()
        {
            try
            {
                EmployeesSearchViewModel model = EmployeesListPopulate();
                return PartialView("~/Views/Admin/GroupTraining/_EmployeeList.cshtml", model);
            }
            catch (Exception ex)
            {
                return PartialView("~/Views/Admin/GroupTraining/_EmployeeList.cshtml");
            }
        }

        public ActionResult AddGroupTraining(GroupTrainingDetailViewModel model)
        {
            var response = "";
            var manager = new GroupTrainingManager();
            var dataRequest = new GroupTrainingDataRequest();
            var responseModel = new FailerResponseViewModel();

            if (ModelState.IsValid)
            {
                model.createdby = GetUserIdFromSession();
                model.approvedTraining = "";
                model.employeeId = StringToIntList(model.employeeIds);
                dataRequest = Mapper.Map<GroupTrainingDetailViewModel, GroupTrainingDataRequest>(model);
                response = manager.AddGroupTraining(dataRequest);
                if (response == UserMessageConstants.GroupTrainingSuccessMsg || response == UserMessageConstants.GroupTrainingUpdateSuccessMsg)
                {
                    responseModel.isSuccessFul = true;
                    responseModel.message = response;
                }
                else
                {
                    responseModel.isSuccessFul = false;
                    responseModel.message = response;
                }
                return Json(responseModel.ToString(), JsonRequestBehavior.AllowGet);
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        


        #endregion
    }
}