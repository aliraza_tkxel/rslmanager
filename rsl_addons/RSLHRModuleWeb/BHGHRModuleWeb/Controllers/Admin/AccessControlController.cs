﻿using AutoMapper;
using BHGHRModuleWeb.ViewModels;
using BusinessModule.NetworkManagers;
using BusinessModule.NetworkObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace BHGHRModuleWeb.Controllers
{
    public class AccessControlController : ABaseController
    {
        #region >>> helpers <<<

        private EmployeesListViewModel getEmployeesList()
        {
            int status = 1;
            int teamId = 0;
            string searchText = string.Empty;
            string requestList = string.Empty;
            if (Request["txtSearchText"] != null)
            {
                searchText = Request["txtSearchText"].ToString();
                ViewBag.searchText = searchText;
                Session["searchText"] = searchText;
            }

            if (Request["requestList"] != null)
            {
                requestList = Request["requestList"].ToString();
                ViewBag.requestList = requestList;
                Session["requestList"] = requestList;
            }

            if (Request["rdBtnstatus"] != null)
            {
                status = Convert.ToInt32(Request["rdBtnstatus"].ToString());
            }

            if (Request["teamId"] != null)
            {
                teamId = Convert.ToInt32(Request["teamId"].ToString());
            }

            var employeesManager = new EmployeesManager();
            var employeesDataRequest = new EmployeesDataRequest();
            employeesDataRequest.status = status;
            ViewBag.status = status;
            employeesDataRequest.teamId = teamId;
            employeesDataRequest.searchText = searchText;
            if (Session["searchText"] != null)
            {
                employeesDataRequest.searchText = Session["searchText"].ToString();
            }

            employeesDataRequest.requestList = requestList;
            if (Session["requestList"] != null)
            {
                employeesDataRequest.requestList = Session["requestList"].ToString();
            }
           
            employeesDataRequest.sortBy = "name";
            employeesDataRequest.sortOrder = "ASC";
            int pageNo = 1;
            if (HttpContext.Request.QueryString["page"] != null && HttpContext.Request.QueryString["page"] != "0")
            {
                pageNo = Convert.ToInt32(HttpContext.Request.QueryString["page"]);
            }
            if (HttpContext.Request.QueryString["sort"] != null && HttpContext.Request.QueryString["sortdir"] != null)
            {
                employeesDataRequest.sortBy = HttpContext.Request.QueryString["sort"].ToString();
                employeesDataRequest.sortOrder = HttpContext.Request.QueryString["sortdir"].ToString();
            }

            employeesDataRequest.pagination.pageSize = _PageSize;
            employeesDataRequest.pagination.pageNo = pageNo;

            var response = employeesManager.getEmployeesList(employeesDataRequest);
            EmployeesListViewModel model = new JavaScriptSerializer().Deserialize<EmployeesListViewModel>(response);
            ViewBag.TotalRows = model.pagination.totalRows;
            ViewBag.PageSize = model.pagination.pageSize;
            model.teamId = teamId;

            return model;
        }

        #endregion

        #region >>> Action Methods <<<

        // GET: Employees
        public ActionResult Index()
        {
            EmployeesListViewModel model = getEmployeesList();
            return View("~/Views/Admin/AccessControl/AccessControl.cshtml", model);
        }

        public ActionResult SearchEmployees()
        {
            EmployeesListViewModel model = getEmployeesList();
            return PartialView("~/Views/Admin/AccessControl/_ACEmployeesList.cshtml", model);
        }

        public ActionResult EmployeeProfile(int id = 0)
        {
            var manager = new AccessControlManager();
            var request = new AccessControlDataRequest();
            request.employeeId = id;
            var response = manager.getEmployeeProfile(request);
            AccessControlViewModel model = new JavaScriptSerializer().Deserialize<AccessControlViewModel>(response);
            model.employeeId = id;
            model.empId = id;
            return PartialView("~/Views/Admin/AccessControl/_EmployeeProfile.cshtml", model);
        }
        [HttpPost]
        public ActionResult UpdateLogin(AccessControlViewModel loginDetail)
        {
            var response = "";
            var manager = new AccessControlManager();
            var request = new AccessControlSaveRequest();
            request = Mapper.Map<AccessControlViewModel, AccessControlSaveRequest>(loginDetail);
            request.currentUserId = GetUserIdFromSession();
            //Encode password to Base64
            var encodedPassword = Encoding.UTF8.GetBytes(request.password);
            request.password = Convert.ToBase64String(encodedPassword);

            response = manager.updateLogin(request);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UnlockAccount(AccessControlViewModel loginDetail)
        {
            var response = "";
            var manager = new AccessControlManager();
            response = manager.unlockAccount(loginDetail.empId);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        #endregion

    }
}