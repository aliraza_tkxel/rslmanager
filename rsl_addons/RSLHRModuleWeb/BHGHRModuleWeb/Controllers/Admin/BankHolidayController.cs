﻿using AutoMapper;
using BHGHRModuleWeb.ViewModels;
using BusinessModule.NetworkManagers;
using BusinessModule.NetworkObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace BHGHRModuleWeb.Controllers
{
    public class BankHolidayController : ABaseController
    {
        #region >>> helpers <<<

        #endregion

        #region >>> Action Methods <<<

        // Index: 
        public ActionResult Index()
        {
            return View("~/Views/Admin/BankHoliday/BankHoliday.cshtml");
        }
        #endregion

    }
}