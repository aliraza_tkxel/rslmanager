﻿using AutoMapper;
using BHGHRModuleWeb.ViewModels;
using BusinessModule.NetworkManagers;
using BusinessModule.NetworkObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace BHGHRModuleWeb.Controllers
{
    public class JobRoleController : ABaseController
    {

        // GET: JobRole
        public ActionResult Index()
        {
            return View("~/Views/Admin/JobRole/JobRole.cshtml");
        }
    }
}