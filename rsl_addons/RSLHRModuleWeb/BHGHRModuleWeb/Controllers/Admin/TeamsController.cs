﻿using AutoMapper;
using BHGHRModuleWeb.ViewModels;
using BusinessModule.NetworkManagers;
using BusinessModule.NetworkObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace BHGHRModuleWeb.Controllers
{
    public class TeamsController : ABaseController
    {
        #region >>> helpers <<<

        private TeamListViewModel getTeamsList()
        {
            var teamsManager = new TeamsManager();
            var teamsDataRequest = new TeamsDataRequest();

            int status = 1;

            teamsDataRequest.sortBy = "teamName";
            teamsDataRequest.sortOrder = "ASC";
            int pageNo = 1;
            if (HttpContext.Request.QueryString["page"] != null && HttpContext.Request.QueryString["page"] != "0")
            {
                pageNo = Convert.ToInt32(HttpContext.Request.QueryString["page"]);
            }
            if (HttpContext.Request.QueryString["sort"] != null && HttpContext.Request.QueryString["sortdir"] != null)
            {
                teamsDataRequest.sortBy = HttpContext.Request.QueryString["sort"].ToString();
                teamsDataRequest.sortOrder = HttpContext.Request.QueryString["sortdir"].ToString();
            }
            if (Request["rdBtnstatus"] != null && Request["rdBtnstatus"] != "" && Request["rdBtnstatus"] != "undefined")
            {
                status = Convert.ToInt32(Request["rdBtnstatus"].ToString());
            }

            ViewBag.status = status;

            teamsDataRequest.status = status;
            teamsDataRequest.pagination.pageSize = _PageSize;
            teamsDataRequest.pagination.pageNo = pageNo;
            
            var response = teamsManager.getTeamsList(teamsDataRequest);
           TeamListViewModel model = new JavaScriptSerializer().Deserialize<TeamListViewModel>(response);

            ViewBag.TotalRows = model.pagination.totalRows;
            ViewBag.PageSize = model.pagination.pageSize;

            return model;
        }

        #endregion

        #region >>> Action Methods<<<
        // GET: Teams
        public ActionResult Index()
        {
            TeamListViewModel model = getTeamsList();
            return View("~/Views/Admin/Teams/Teams.cshtml", model);
        }
        public ActionResult SearchTeams()
        {
            TeamListViewModel model = getTeamsList();
            return PartialView("~/Views/Admin/Teams/_TeamsList.cshtml", model);
        }
        public ActionResult TeamCreate(int id = 0)
        {
            TeamCreateViewModel model = new TeamCreateViewModel();
            model.team = new TeamViewModel();
            if (id == 0)
            {
                ViewBag.isEdit = "create";
            }
            else
            {
                ViewBag.isEdit = "edit";
            }
            var teamsManager = new TeamsManager();
            var teamsDataRequest = new TeamsDataRequest();
            teamsDataRequest.teamId = id;
            var response = teamsManager.getTeamDetail(teamsDataRequest);
            TeamListViewModel teamVM = new JavaScriptSerializer().Deserialize<TeamListViewModel>(response);
            if (teamVM != null)
            {
                model.team.teamId = id;
                if (teamVM.teams.Count==0)
                {
                    model.team.teamManager = "";
                    model.team.active = 1;
                    model.team.description = "";
                    model.team.directorate = "";
                    model.team.directorateId = 0;
                    model.team.mainFunction = "";
                    model.team.managerId = 0;
                    model.team.teamDirector = "";
                    model.team.teamName = "";
                    model.team.directorId = 0;
                }
                else
                {
                    model.team.teamManager = teamVM.teams.First().teamManager;
                    model.team.active = teamVM.teams.First().active;
                    model.team.activeCheck = model.team.active==1;
                    model.team.description = teamVM.teams.First().description;
                    model.team.directorate = teamVM.teams.First().directorate;
                    model.team.directorateId = teamVM.teams.First().directorateId;
                    model.team.mainFunction = teamVM.teams.First().mainFunction;
                    model.team.managerId = teamVM.teams.First().managerId;
                    model.team.teamDirector = teamVM.teams.First().teamDirector;
                    model.team.teamName = teamVM.teams.First().teamName;
                    model.team.directorId = teamVM.teams.First().directorId;                    
                }
                model.lookUps = teamVM.DMLookUps;
            }
            return View("~/Views/Admin/Teams/TeamCreate.cshtml", model);
        }
        [HttpPost]
        public ActionResult AddTeam(TeamCreateViewModel teamDetail)
        {
            teamDetail.team.active = teamDetail.team.activeCheck? 1 : 0;
            var response = "";
            var teamsManager = new TeamsManager();
            var teamSaveDataRequest = new TeamSaveDataRequest();
            teamSaveDataRequest = Mapper.Map<TeamViewModel, TeamSaveDataRequest>(teamDetail.team);
            response = teamsManager.saveTeam(teamSaveDataRequest);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        #endregion

    }
}