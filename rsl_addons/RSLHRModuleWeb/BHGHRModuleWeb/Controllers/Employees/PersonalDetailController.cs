﻿using AutoMapper;
using BHGHRModuleWeb.ViewModels;
using BusinessModule.NetworkManagers;
using BusinessModule.NetworkObjects;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using SysIO = System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using BHGHRModuleWeb.Helpers;
using Rotativa;

namespace BHGHRModuleWeb.Controllers.Employees
{
    public class PersonalDetailController : ABaseController
    {
        #region >>> Helpers <<<

        private EmployeesListViewModel getEmployeesList()
        {
            int status = 1;
            string searchText = string.Empty;
            if (Request["txtSearchText"] != null)
            {
                searchText = Request["txtSearchText"].ToString();
                ViewBag.searchText = searchText;
                Session["searchText"] = searchText;
            }

            if (Request["rdBtnstatus"] != null)
            {
                status = Convert.ToInt32(Request["rdBtnstatus"].ToString());
            }

            var employeesManager = new EmployeesManager();
            var employeesDataRequest = new EmployeesDataRequest();
            employeesDataRequest.status = status;
            ViewBag.status = status;
            employeesDataRequest.searchText = searchText;
            if (Session["searchText"] != null)
            {
                employeesDataRequest.searchText = Session["searchText"].ToString();
            }

            employeesDataRequest.sortBy = "name";
            employeesDataRequest.sortOrder = "ASC";
            int pageNo = 1;
            if (HttpContext.Request.QueryString["page"] != null && HttpContext.Request.QueryString["page"] != "0")
            {
                pageNo = Convert.ToInt32(HttpContext.Request.QueryString["page"]);
            }
            if (HttpContext.Request.QueryString["sort"] != null && HttpContext.Request.QueryString["sortdir"] != null)
            {
                employeesDataRequest.sortBy = HttpContext.Request.QueryString["sort"].ToString();
                employeesDataRequest.sortOrder = HttpContext.Request.QueryString["sortdir"].ToString();
            }

            employeesDataRequest.pagination.pageSize = _PageSize;
            employeesDataRequest.pagination.pageNo = pageNo;

            var response = employeesManager.getEmployeesList(employeesDataRequest);
            EmployeesListViewModel model = new JavaScriptSerializer().Deserialize<EmployeesListViewModel>(response);
            ViewBag.TotalRows = model.pagination.totalRows;
            ViewBag.PageSize = model.pagination.pageSize;

            return model;
        }

        private ViewModels.GiftAndHospitalityViewModel GetEmployeesGifts(int id, int giftId = 0)
        {
            var manager = new PersonalDetailManager();
            var request = new GetPersonalDetailDataRequest();
            var model = new ViewModels.GiftAndHospitalityViewModel();
            var gifts = new List<GiftViewModel>();
            request.employeeId = id;

            var response = manager.getEmployeeGifts(request);
            if (response != "[]")
            {
                model.gifts = new JavaScriptSerializer().Deserialize<List<GiftViewModel>>(response);
            }

            model.employeeId = id;
            if (giftId > 0)
            {
                model.received = model.lstReceived.Where(e => e.giftId == giftId).FirstOrDefault();
                if (model.received != null)
                {
                    model.received.offeredBy = model.received.offerToBy;
                }
                model.given = model.lstGiven.Where(e => e.giftId == giftId).FirstOrDefault();
                if (model.given != null)
                {
                    model.given.offeredTo = model.given.offerToBy;
                }
            }
            model.isTeam = IsTeam();
            return model;
        }

        #endregion

        #region >>> Action Methods <<<

        #region >>> Personal Detail <<<

        // GET: PersonalDetail
        public ActionResult Index(int id = 0)
        {
            var manager = new PersonalDetailManager();
            var request = new GetPersonalDetailDataRequest();
            request.employeeId = id;
            Session["gifts"] = null;

            if (id != 0)
            {
                ViewBag.isEdit = "edit";
            }
            else
            {
                ViewBag.isEdit = "create";
            }

            var response = manager.getPersonalDetail(request);
            PersonalDetailViewModel model = new JavaScriptSerializer().Deserialize<PersonalDetailViewModel>(response);
            if (model.employeeDetail == null)
            {
                model.employeeDetail = new EmployeeDetailViewModel();
            }
            else
            {
                if(!string.IsNullOrEmpty(model.employeeDetail.dbsDate))
                {
                    // model.employeeDetail.dbsDate 
                    DateTime dbsDate = GetDateTimeFromString(model.employeeDetail.dbsDate);
                    model.employeeDetail.dbsReminderDate = dbsDate.AddMonths(30).ToShortDateString();

                }
            }

            if (HttpContext.Request.QueryString["isDirector"] != null)
            {
                model.isDirector = Convert.ToBoolean(HttpContext.Request.QueryString["isDirector"].ToString());
            }

            if (HttpContext.Request.QueryString["isGift"] != null)
            {
                model.isGift = Convert.ToBoolean(HttpContext.Request.QueryString["isGift"].ToString());
            }

            return PartialView("~/Views/Employees/PersonalDetail/_PersonalDetail.cshtml", model);
        }

        [HttpPost]
        public ActionResult Save(HttpPostedFileBase file, PersonalDetailViewModel model)
        {
            var response = "";
            if (ModelState.IsValid)
            {
                PersonalDetailDataRequest personalDetailDR = new PersonalDetailDataRequest();
                // auto mapper to map view model value to network object.
                var manager = new PersonalDetailManager();

                personalDetailDR = Mapper.Map<EmployeeDetailViewModel, PersonalDetailDataRequest>(model.employeeDetail);

                if (file != null && file.ContentLength > 0)
                    try
                    {
                        FileHelper.createDirectory(FileHelper.GetProfilePicUploadPath());
                        string fileName = FileHelper.getUniqueFileName(file.FileName);
                        string path = Path.Combine(FileHelper.GetProfilePicUploadPath(), fileName);
                        file.SaveAs(path);
                        ViewBag.Message = "File uploaded successfully";
                        personalDetailDR.imagePath = fileName;
                    }
                    catch (Exception ex)
                    {
                        ViewBag.Message = "ERROR:" + ex.Message.ToString();
                    }
                else
                {
                    if (model.employeeDetail.imagePath != null)
                    {
                        personalDetailDR.imagePath = getFileNameFromPath(model.employeeDetail.imagePath);
                    }
                    ViewBag.Message = "You have not specified a file.";
                }

                response = manager.savePersonalDetail(personalDetailDR);
                var obj = new JavaScriptSerializer().Deserialize<PersonalDetailResponseViewModel>(response);
                if (obj.isSuccessFul)
                {
                    Session.Add("employeeId", obj.personalDetail.employeeId);
                }

                return Json(response, JsonRequestBehavior.AllowGet);
            }

            var errorList = (from item in ModelState.Values
                             from error in item.Errors
                             select error.ErrorMessage).ToList();
            var message = "";
            for (int i = 0; i < errorList.Count; i++)
            {
                message = message + errorList[i].ToString() + "<br />";
            }
            var failerResponse = new FailerResponseViewModel();
            failerResponse.message = message;
            //"{'isSuccessFul':true,'message':'Personal detail is updated successfully.'}"
            response = "{'isSuccessFul':true,'message':'" + message + "'}";
            return Json(failerResponse.ToString(), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region >>> Contact Details <<<

        public ActionResult ContactDetail(int id = 0)
        {
            var manager = new PersonalDetailManager();
            var request = new GetPersonalDetailDataRequest();
            request.employeeId = id;
            var response = manager.getContactDetail(request);
            ContactDetailViewModel model = new JavaScriptSerializer().Deserialize<ContactDetailViewModel>(response);
            model.employeeId = id;
            return PartialView("~/Views/Employees/PersonalDetail/_ContactDetail.cshtml", model);
        }

        [HttpPost]
        public ActionResult SaveContact(ContactDetailViewModel model)
        {
            if (ModelState.IsValid)
            {
                ContactDetailDataRequest contactDetailDR = new ContactDetailDataRequest();
                // auto mapper to map view model value to network object.
                var manager = new PersonalDetailManager();
                contactDetailDR = Mapper.Map<ContactDetailViewModel, ContactDetailDataRequest>(model);

                var response = manager.saveContactDetail(contactDetailDR);

                return Json(response, JsonRequestBehavior.AllowGet);
            }

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region >>> Documents <<<

        public ActionResult ReferenceDocuments(int id = 0, string requestType = "edit")
        {
            var manager = new PersonalDetailManager();
            var request = new GetPersonalDetailDataRequest();
            var model = new ReferenceDocumentViewModel();
            request.employeeId = id;
            var response = manager.getEmployeeReferenceDocuments(request);
            if (response != "[]")
            {
                //JavaScriptSerializer json_serializer = new JavaScriptSerializer();
                //model = (ReferenceDocumentViewModel)json_serializer.Deserialize(response, typeof(ReferenceDocumentViewModel));
                model.Documents = new JavaScriptSerializer().Deserialize<List<DocumentsViewModel>>(response);


            }
            model.employeeId = id;
            model.requestType = requestType;
            return PartialView("~/Views/Employees/PersonalDetail/_ReferenceDocument.cshtml", model);
        }

        [HttpPost]
        public ActionResult UpdateEmployeeImage(HttpPostedFileBase pictureUrl, PersonalDetailViewModel model)
        {
            var response = "";
            var updateEmployeeImageDR = new UpdateEmployeeImageDataRequest();
            updateEmployeeImageDR.employeeId = model.employeeDetail.employeeId;
            updateEmployeeImageDR.profile = model.employeeDetail.profile;

            var manager = new PersonalDetailManager();

            if (pictureUrl != null && pictureUrl.ContentLength > 0)
            {
                FileHelper.createDirectory(FileHelper.GetProfilePicUploadPath());
                string fileName = FileHelper.getUniqueFileName(pictureUrl.FileName);
                string path = Path.Combine(FileHelper.GetProfilePicUploadPath(), fileName);
                pictureUrl.SaveAs(path);
                updateEmployeeImageDR.imageName = fileName;
            }
              
            response = manager.updateEmployeeImage(updateEmployeeImageDR);

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateEmployeeReferenceDocument(HttpPostedFileBase reference, ReferenceDocumentViewModel model)
        {
            var response = "";
            var updateRefDocumentDR = new UpdateReferenceDocumentDataRequest();
            var manager = new PersonalDetailManager();

            if (reference != null && reference.ContentLength > 0)
                try
                {
                    var url = FileHelper.GetRefDocUploadPath() + model.employeeId.ToString();
                    FileHelper.createDirectory(url);
                    string fileName = FileHelper.getUniqueFileName(reference.FileName);

                    string path = Path.Combine(url, fileName);
                    reference.SaveAs(path);
                    updateRefDocumentDR.employeeId = model.employeeId;
                    updateRefDocumentDR.documentPath = fileName;
                    updateRefDocumentDR.createdBy = GetUserIdFromSession();
                    response = manager.updateEmployeeReferenceDocument(updateRefDocumentDR);
                }
                catch (Exception ex)
                {
                    ViewBag.Message = "ERROR:" + ex.Message.ToString();
                }
            else
            {
                ViewBag.Message = "You have not specified a file.";
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region >>> Declaration Of Interest <<<

        public ActionResult Interest(int id = 0)
        {
            var manager = new PersonalDetailManager();
            var request = new GetPersonalDetailDataRequest();
            request.employeeId = id;

            var response = manager.GetDeclarationOfInterest(request);
            DeclarationViewModel model = new JavaScriptSerializer().Deserialize<DeclarationViewModel>(response);
            model.declarationOfInterest.employeeId = request.employeeId;
            if (model.declarationOfInterest.page1.isSecondaryEmployment.HasValue)
            {
                model.declarationOfInterest.page1.isEmployment = model.declarationOfInterest.page1.isSecondaryEmployment.Value;
            }

            if (model.declarationOfInterest.page2.isConApprovedByLeadership.HasValue)
            {
                model.declarationOfInterest.page2.isLeadership = model.declarationOfInterest.page2.isConApprovedByLeadership.Value;
            }

            if (model.declarationOfInterest.page2.isDirectorP8.HasValue)
            {
                model.declarationOfInterest.page2.isTempDirectorP8 = model.declarationOfInterest.page2.isDirectorP8.Value;
            }

            if (model.declarationOfInterest.page2.isDirectorP9.HasValue)
            {
                model.declarationOfInterest.page2.isTempDirectorP9 = model.declarationOfInterest.page2.isDirectorP9.Value;
            }

            if (model.declarationOfInterest.page3.isInterested.HasValue)
            {
                model.declarationOfInterest.page3.isTempInterested = model.declarationOfInterest.page3.isInterested.Value;
            }

            return PartialView("~/Views/Employees/PersonalDetail/_Interests.cshtml", model);
        }

        [HttpPost]
        public ActionResult SaveInterest(DeclarationViewModel model)
        {
            var response = "";
            DeclarationOfInterestDataRequest doiDR = new DeclarationOfInterestDataRequest();
            var manager = new PersonalDetailManager();

            // auto mapper to map view model value to network object.
            doiDR = Mapper.Map<DeclarationOfInterestViewModel, DeclarationOfInterestDataRequest>(model.declarationOfInterest);
            doiDR.createdBy = GetUserIdFromSession();
            doiDR.createdOn = DateTime.Now;
            if (!model.declarationOfInterest.isActive.HasValue)
            {
                doiDR.isActive = true;
            }

            if (model.declarationOfInterest.page1 != null)
            {
                doiDR.page1.isSecondaryEmployment = model.declarationOfInterest.page1.isEmployment;
            }

            if (model.declarationOfInterest.page2 != null)
            {
                doiDR.page2.isConApprovedByLeadership = model.declarationOfInterest.page2.isLeadership;
                doiDR.page2.isDirectorP8 = model.declarationOfInterest.page2.isTempDirectorP8;
                doiDR.page2.isDirectorP9 = model.declarationOfInterest.page2.isTempDirectorP9;
            }

            if (model.declarationOfInterest.page3 != null)
            {
                doiDR.page3.isInterested = model.declarationOfInterest.page3.isTempInterested;

                // save PDF file start
                var request = new GetPersonalDetailDataRequest();
                request.employeeId = model.declarationOfInterest.employeeId;
                var fileResponse = manager.GetDeclarationOfInterest(request);
                DeclarationViewModel fileModel = new JavaScriptSerializer().Deserialize<DeclarationViewModel>(fileResponse);
                fileModel.declarationOfInterest.employeeId = request.employeeId;

                if (fileModel.declarationOfInterest.page1.isSecondaryEmployment.HasValue)
                {
                    fileModel.declarationOfInterest.page1.isEmployment = fileModel.declarationOfInterest.page1.isSecondaryEmployment.Value;
                }

                if (fileModel.declarationOfInterest.page2.isConApprovedByLeadership.HasValue)
                {
                    fileModel.declarationOfInterest.page2.isLeadership = fileModel.declarationOfInterest.page2.isConApprovedByLeadership.Value;
                }

                if (fileModel.declarationOfInterest.page2.isDirectorP8.HasValue)
                {
                    fileModel.declarationOfInterest.page2.isTempDirectorP8 = fileModel.declarationOfInterest.page2.isDirectorP8.Value;
                }

                if (fileModel.declarationOfInterest.page2.isDirectorP9.HasValue)
                {
                    fileModel.declarationOfInterest.page2.isTempDirectorP9 = fileModel.declarationOfInterest.page2.isDirectorP9.Value;
                }
                fileModel.declarationOfInterest.page3 = model.declarationOfInterest.page3;

                string fileName = FileHelper.getUniqueFileName("DeclarationOfInterest.pdf");
                try
                {
                    var pdfOutput = new ViewAsPdf("~/Views/Employees/PersonalDetail/_InterestsPDF.cshtml", fileModel)
                    {
                        FileName = fileName,
                        CustomSwitches = "--footer-spacing \"5\" --footer-font-size \"8\" --footer-center \"Page [page] of [toPage]\" --footer-right \"PDF Created " + DateTime.Now.ToString() + "\" ",
                        PageMargins = { Top = 15, Bottom = 20 },
                        PageSize = Rotativa.Options.Size.A4
                    };

                    var url = FileHelper.GetRefDocUploadPath() + model.declarationOfInterest.employeeId.ToString();
                    FileHelper.createDirectory(url);

                    string path = Path.Combine(url, fileName);

                    if (SysIO.File.Exists(path))
                    {
                        SysIO.File.Delete(path);
                    }
                    SysIO.File.WriteAllBytes(path, pdfOutput.BuildPdf(ControllerContext));
                    doiDR.page3.documentPath = fileName;
                }
                catch (Exception ex)
                {

                }

                // save PDF file end
            }

            response = manager.SaveInterest(doiDR);
            return Json(response, JsonRequestBehavior.AllowGet);
        }


        #endregion

        #region >>> Gift & Hospitality <<<


        public ActionResult GiftMaster(int id)
        {

            var manager = new PersonalDetailManager();
            var request = new GiftMasterDateRequest();
            var model = new GiftMasterViewModel();
            request.employeeId = id;

            var response = manager.giftMaster(request);
            if (response != "[]")
            {
                model = new JavaScriptSerializer().Deserialize<GiftMasterViewModel>(response);
            }

            if (HttpContext.Request.QueryString["isGift"] != null)
            {
                model.isGift = Convert.ToBoolean(HttpContext.Request.QueryString["isGift"].ToString());
            }

            model.employeeId = id;
            return PartialView("~/Views/Employees/PersonalDetail/_GiftMaster.cshtml", model);
        }


        public ActionResult GiftHospitality(int id, int giftId = 0)
        {
            var model = GetEmployeesGifts(id, giftId);
            return PartialView("~/Views/Employees/PersonalDetail/_Gift.cshtml", model);
        }

        [HttpPost]
        public ActionResult SearchGiftHospitality(GiftMasterViewModel requestModel)
        {

            var manager = new PersonalDetailManager();
            var request = new GiftMasterDateRequest();
            var model = new GiftMasterViewModel();
            request.employeeId = requestModel.employeeId;
            request.from = requestModel.from;
            request.to = requestModel.to;
            request.typeId = requestModel.typeId;
            request.statusId = requestModel.statusId;

            var response = manager.giftMaster(request);
            if (response != "")
            {
                model = new JavaScriptSerializer().Deserialize<GiftMasterViewModel>(response);
            }
            model.employeeId = requestModel.employeeId;
            model.from = requestModel.from;
            model.to = requestModel.to;
            model.typeId = requestModel.typeId;
            model.statusId = requestModel.statusId;

            return PartialView("~/Views/Employees/PersonalDetail/_GiftMasterList.cshtml", model);
        }

        [HttpPost]
        public ActionResult UpdateGiftStatus(GiftViewModel model)
        {
            var giftDR = new Gift();
            var manager = new PersonalDetailManager();
            var response = "";

            // auto mapper to map view model value to network object.

            giftDR = Mapper.Map<GiftViewModel, Gift>(model);
            giftDR.updatedBy  = GetUserIdFromSession();
            giftDR.updatedDate = DateTime.Now;
            
            response = manager.updateGiftStatus(giftDR);

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveGiftHospitality(int id = 0)
        {
            var model = new GiftAndHospitalityViewModel();
            var giftDR = new GiftDataRequest();
            var manager = new PersonalDetailManager();
            var response = "";

            if (Session["gifts"] != null)
            {
                model.gifts = (List<GiftViewModel>)Session["gifts"];
            }

            model.employeeId = id;
            model.createdBy = GetUserIdFromSession();

            // auto mapper to map view model value to network object.

            giftDR = Mapper.Map<GiftAndHospitalityViewModel, GiftDataRequest>(model);

            // requirment no more valid 
            //// save PDF file start
            //try
            //{
            //    string fileName = FileHelper.getUniqueFileName("Gift.pdf");
            //    var pdfOutput = new ViewAsPdf("~/Views/Employees/PersonalDetail/_GiftPDF.cshtml", model)
            //    {
            //        FileName = fileName,
            //        CustomSwitches = "--footer-spacing \"5\" --footer-font-size \"8\" --footer-center \"Page [page] of [toPage]\" --footer-right \"PDF Created " + DateTime.Now.ToString() + "\" ",
            //        PageMargins = { Top = 15, Bottom = 20 },
            //        PageSize = Rotativa.Options.Size.A4
            //    };

            //    var url = FileHelper.GetRefDocUploadPath() + model.employeeId.ToString();
            //    FileHelper.createDirectory(url);

            //    string path = Path.Combine(url, fileName);

            //    if (SysIO.File.Exists(path))
            //    {
            //        SysIO.File.Delete(path);
            //    }
            //    SysIO.File.WriteAllBytes(path, pdfOutput.BuildPdf(ControllerContext));
            //    giftDR.documentPath = fileName;
            //}
            //catch (Exception ex)
            //{

            //}
            //// save PDF file end

            response = manager.saveGift(giftDR);
            
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveGiftReceived(GiftAndHospitalityViewModel model, string nextBtn)
        {
            switch (nextBtn)
            {
                case "Approve":
                    // update status call 
                    break;
                case "Reject":
                    // update status call with rejection note
                    break;
                default:
                    //var response = "";
                    //var lstReceived = new List<GiftViewModel>();
                    //if (Session["gifts"] != null)
                    //{
                    //    lstReceived = (List<GiftViewModel>)Session["gifts"];
                    //}
                    //model.received.isGivenReceived = 1; // 1 for received
                    //model.received.createdDate = DateTime.Now;
                    //model.received.offerToBy = model.received.offeredBy;
                    //model.received.dateGivenReceived = model.received.dateRevieved;
                    //lstReceived.Add(model.received);

                    //Session["gifts"] = lstReceived;
                    //model.gifts = lstReceived;

                    ////////
                    var savingModel = new GiftAndHospitalityViewModel();
                    savingModel.gifts = new List<GiftViewModel>();
                    var giftDR = new GiftDataRequest();
                    var manager = new PersonalDetailManager();
                    var response = "";

                    if (Session["gifts"] != null)
                    {
                        model.gifts = (List<GiftViewModel>)Session["gifts"];
                    }

                    model.received.isGivenReceived = 1; // 1 for received
                    model.received.createdDate = DateTime.Now;
                    model.received.dateOfDeclaration = DateTime.Now;
                    model.received.offerToBy = model.received.offeredBy;
                    model.received.dateGivenReceived = model.received.dateRevieved;

                    savingModel.employeeId = model.received.employeeId;
                    savingModel.createdBy = GetUserIdFromSession();
                    savingModel.gifts.Add(model.received);
                    giftDR = Mapper.Map<GiftAndHospitalityViewModel, GiftDataRequest>(savingModel);
                    response = manager.saveGift(giftDR);

                    return Json(response, JsonRequestBehavior.AllowGet);


                    break;
            }
            

            return PartialView("~/Views/Employees/PersonalDetail/_GiftReceivedList.cshtml", model);
        }

        [HttpPost]
        public ActionResult SaveGiftGiven(GiftAndHospitalityViewModel model)
        {
            var savingModel = new GiftAndHospitalityViewModel();
            savingModel.gifts = new List<GiftViewModel>();
            var giftDR = new GiftDataRequest();
            var manager = new PersonalDetailManager();
            var response = "";

            model.given.isGivenReceived = 2; // 2 for given
            model.given.createdDate = DateTime.Now;
            model.given.dateOfDeclaration = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
            model.given.offerToBy = model.given.offeredTo;
            model.given.dateGivenReceived = model.given.dateGiven;

            savingModel.employeeId = model.given.employeeId;
            savingModel.createdBy = GetUserIdFromSession();
            savingModel.gifts.Add(model.given);
            giftDR = Mapper.Map<GiftAndHospitalityViewModel, GiftDataRequest>(savingModel);
            response = manager.saveGift(giftDR);

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region >>> Salary Information <<<

        public ActionResult SalaryInformation(int id = 0)
        {
            var manager = new PersonalDetailManager();
            var request = new GetPersonalDetailDataRequest();
           
            request.employeeId = id;
            
            var response = manager.getSalaryInformation(request);
            SalaryViewModel model = new JavaScriptSerializer().Deserialize<SalaryViewModel>(response);
            if (model.payPoint != null)
            {
                model.payPoint.employeeId = id;
                if (model.payPoint.authorizedBy != null)
                {
                    model.payPoint.authorizedByName = GetEmployeeNameById(model.payPoint.authorizedBy.Value);
                }

                if (model.payPoint.supportedBy != null)
                {
                    model.payPoint.supportedByName = GetEmployeeNameById(model.payPoint.supportedBy.Value);
                }

                if (model.payPoint.approvedBy != null)
                {
                    model.payPoint.approvedByName = GetEmployeeNameById(model.payPoint.approvedBy.Value);
                }
            }
            model.loginUserId = GetUserIdFromSession();
            return PartialView("~/Views/Employees/PersonalDetail/_Salary.cshtml", model);
        }

        [HttpPost]
        public ActionResult SaveSalaryInformation(SalaryViewModel model)
        {
            var manager = new PersonalDetailManager();
            var salaryDR = new SalaryInformationDataRequest();
            salaryDR = Mapper.Map<ProposedSalaryViewModel, SalaryInformationDataRequest>(model.payPoint);

            if (model.payPoint.isWaiting)
            {
                salaryDR.waiting = true;
                salaryDR.waitingDate = DateTime.Now.Date.ToString();
                salaryDR.waitingBy = GetUserIdFromSession();
                salaryDR.reason = model.payPoint.waitingReason;
            }
            else
            {
                salaryDR.approved = true;
                salaryDR.approvedDate = DateTime.Now.Date.ToString();
                salaryDR.approvedBy = GetUserIdFromSession();
            }

            var response = manager.SaveSalaryInformation(salaryDR);

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetGradePointSalary(GradePointSalaryViewModel model)
        {
            var manager = new PersonalDetailManager();
            var salaryDR = new GradePointSalaryDataRequest();
            salaryDR = Mapper.Map<GradePointSalaryViewModel, GradePointSalaryDataRequest>(model);

            var response = manager.GetGradePointSalary(salaryDR);

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetPayPointRequest(int Id)
        {
            ReportsManager rptManager = new ReportsManager();
            var response = rptManager.GetEmployeePaypointSubmission(Id);
            PayPointSupportViewModel model = new JavaScriptSerializer().Deserialize<PayPointSupportViewModel>(response);
            // for director 
            model.isCeo = 0;

            return PartialView("~/Views/Employees/PersonalDetail/_PayPointDirectorPopUp.cshtml", model);
        }

        #endregion

        #region >>> Driving Licence<<<

        public ActionResult DrivingLicence(int id = 0)
        {
            var manager = new PersonalDetailManager();
            var request = new GetPersonalDetailDataRequest();
            request.employeeId = id;

            //var response = manager.GetDeclarationOfInterest(request);
            //DrivingLicenceResponseViewModel model = new DrivingLicenceResponseViewModel();// new JavaScriptSerializer().Deserialize<DrivingLicenceResponseViewModel>(response);
            var response = manager.GetDrivingLicence(request);
            DrivingLicenceResponseViewModel model = new JavaScriptSerializer().Deserialize<DrivingLicenceResponseViewModel>(response);
            model.drivingLicence.employeeId = request.employeeId;
            if (model.disqualificationsList != null && model.disqualificationsList.Count> 0)
            {
                model.disqualifications = new JavaScriptSerializer().Serialize(model.disqualificationsList);
            }
            if (model.drivingLicence.categories != null && model.drivingLicence.categories.Count > 0)
            {
                model.drivingLicence.categorys = new JavaScriptSerializer().Serialize(model.drivingLicence.categories);
            }

            if (model.drivingLicence.expiryDate.HasValue)
            {
                model.drivingLicence.expiry = model.drivingLicence.expiryDate.Value.ToShortDateString();
            }

            return PartialView("~/Views/Employees/PersonalDetail/_Licence.cshtml", model);
        }

        [HttpPost]
        public ActionResult SaveDrivingLicence(DrivingLicenceResponseViewModel model)
        {
            var response = "";
            var manager = new PersonalDetailManager();
            LicenceDataRequest licenceDR = new LicenceDataRequest();
            try
            {
                JavaScriptSerializer js = new JavaScriptSerializer();

                if (!string.IsNullOrEmpty(model.disqualifications))
                {
                    List<LicenceDisqualificationsDataRequest> list = js.Deserialize<List<LicenceDisqualificationsDataRequest>>(model.disqualifications);
                    licenceDR.disqualificationsList = list;
                }
                if (!string.IsNullOrEmpty(model.drivingLicence.categorys))
                {
                    List<DrivingLicenceCategoriesDataRequest> listCate = js.Deserialize<List<DrivingLicenceCategoriesDataRequest>>(model.drivingLicence.categorys);
                    licenceDR.drivingLicence.categories = listCate;
                }

                if (!string.IsNullOrEmpty(model.drivingLicence.expiry))
                {
                    licenceDR.drivingLicence.expiry = model.drivingLicence.expiry;
                }

                licenceDR.drivingLicence.drivingLicenceId = model.drivingLicence.drivingLicenceId;
                licenceDR.drivingLicence.employeeId = model.drivingLicence.employeeId;
                licenceDR.drivingLicence.drivingLicenceType = model.drivingLicence.drivingLicenceType;
                licenceDR.drivingLicence.drivingLicenceNumber = model.drivingLicence.drivingLicenceNumber;
                //licenceDR.drivingLicence.drivingLicenceIssueNumber = model.drivingLicence.drivingLicenceIssueNumber;
                licenceDR.drivingLicence.drivingLicenceIssueNumber = "";

                licenceDR.drivingLicence.medicalHistory = model.drivingLicence.medicalHistory;
                licenceDR.drivingLicence.createdBy = GetUserIdFromSession();
                response = manager.SaveDrivingLicence(licenceDR);
            }
            catch (Exception ex)
            {

            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UploadDrivingLicence(HttpPostedFileBase liceneUrl, DrivingLicenceResponseViewModel model)
        {
            var response = "";
            var uploadicence = new UploadDrivingLicenceImageDataRequest();
            var manager = new PersonalDetailManager();

            if (liceneUrl != null && liceneUrl.ContentLength > 0)
                try
                {
                    FileHelper.createDirectory(FileHelper.GetRefDocUploadPath() + model.drivingLicence.employeeId.ToString());
                    string fileName = FileHelper.getUniqueFileName(liceneUrl.FileName);
                    string path = Path.Combine(FileHelper.GetRefDocUploadPath() + model.drivingLicence.employeeId.ToString(), fileName);
                    liceneUrl.SaveAs(path);

                    uploadicence.employeeId = (int)model.drivingLicence.employeeId;
                    uploadicence.filePath = fileName;
                    uploadicence.updatedBy = GetUserIdFromSession();
                    response = manager.updateDrivingLicenceImage(uploadicence);

                    if (response.Equals("\"Driving licence Image is added successfully.\""))
                    {
                        var imagePath = "https://" + Request.Url.Authority + "/EmployeeDocuments/"+ model.drivingLicence.employeeId.ToString()+ "/" + fileName;

                        response = response + " __imagePath__" + imagePath;
                    }
                }
                catch (Exception ex)
                {
                    ViewBag.Message = "ERROR:" + ex.Message.ToString();
                }
            else
            {
                ViewBag.Message = "You have not specified a file.";
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #endregion

    }
}
