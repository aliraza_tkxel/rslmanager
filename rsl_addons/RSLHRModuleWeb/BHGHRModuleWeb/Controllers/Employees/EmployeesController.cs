﻿using BHGHRModuleWeb.ViewModels;
using BusinessModule.Constants;
using BusinessModule.NetworkManagers;
using BusinessModule.NetworkObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace BHGHRModuleWeb.Controllers
{
    public class EmployeesController : ABaseController
    {
        #region >>> helpers <<<

        private EmployeesListViewModel getEmployeesList()
        {
            int status = 1;
            int teamId = 0;
            string searchText = string.Empty;
            string requestList = string.Empty;
            if (Request["txtSearchText"] != null)
            {
                searchText = Request["txtSearchText"].ToString();
                ViewBag.searchText = searchText;
                Session["searchText"] = searchText;
            }

            if (Request["requestList"] != null)
            {
                requestList = Request["requestList"].ToString();
                ViewBag.requestList = requestList;
                Session["requestList"] = requestList;
            }

            if (Request["rdBtnstatus"] != null)
            {
                status = Convert.ToInt32(Request["rdBtnstatus"].ToString());
            }

            if (Request["teamId"] != null)
            {
                teamId = Convert.ToInt32(Request["teamId"].ToString());
            }

            var employeesManager = new EmployeesManager();
            var employeesDataRequest = new EmployeesDataRequest();
            employeesDataRequest.status = status;
            ViewBag.status = status;
            employeesDataRequest.teamId = teamId;
            employeesDataRequest.searchText = searchText;
            if (Session["searchText"] != null)
            {
                employeesDataRequest.searchText = Session["searchText"].ToString();
            }

            employeesDataRequest.requestList = requestList;
            if (Session["requestList"] != null)
            {
                employeesDataRequest.requestList = Session["requestList"].ToString();
            }
           
            employeesDataRequest.sortBy = "name";
            employeesDataRequest.sortOrder = "ASC";
            int pageNo = 1;
            if (HttpContext.Request.QueryString["page"] != null && HttpContext.Request.QueryString["page"] != "0")
            {
                pageNo = Convert.ToInt32(HttpContext.Request.QueryString["page"]);
            }
            if (HttpContext.Request.QueryString["sort"] != null && HttpContext.Request.QueryString["sortdir"] != null)
            {
                employeesDataRequest.sortBy = HttpContext.Request.QueryString["sort"].ToString();
                employeesDataRequest.sortOrder = HttpContext.Request.QueryString["sortdir"].ToString();
            }

            employeesDataRequest.pagination.pageSize = _PageSize;
            employeesDataRequest.pagination.pageNo = pageNo;

            var response = employeesManager.getEmployeesList(employeesDataRequest);
            EmployeesListViewModel model = new JavaScriptSerializer().Deserialize<EmployeesListViewModel>(response);
            ViewBag.TotalRows = model.pagination.totalRows;
            ViewBag.PageSize = model.pagination.pageSize;
            model.teamId = teamId;

            return model;
        }

        #endregion

        #region >>> Action Methods <<<

        // GET: Employees
        public ActionResult EmployeesList()
        {
            EmployeesListViewModel model = getEmployeesList();
            return View(model);
        }

        public ActionResult SearchEmployees()
        {
            EmployeesListViewModel model = getEmployeesList();
            return PartialView("_EmployeesList", model);
        }

        public ActionResult EmployeeCreate(int id = 0, bool isTeam = false, string activeTab = "", bool isDirector = false, bool isGift = false, int trainingid = 0)
        {
            if (Request.QueryString["Module"] != null)
            {
                Session["Module"] = Request.QueryString["Module"].ToString();
            }

            TempData["isTeam"] = isTeam;
            TempData["isDirector"] = isDirector;
            TempData["isGift"] = isGift;

            if (isTeam)
            {
                TempData["backToMyStaffURL"] = HttpContext.Request.UrlReferrer.ToString() + "&redirectTab="+ activeTab;
            }
            if(trainingid != 0)
            {
                Session[ApplicationConstants.trainingId] = trainingid;
            }
            else
            {
                Session[ApplicationConstants.trainingId] = null;
            }
            EmployeesCreateViewModel model = new EmployeesCreateViewModel();
            if (id == 0)
            {
                ViewBag.isEdit = "create";
            }
            else
            {
                ViewBag.isEdit = "edit";
                var manager = new PersonalDetailManager();
                var request = new GetPersonalDetailDataRequest();
                request.employeeId = id;
                var response = manager.getPersonalDetail(request);
                PersonalDetailViewModel personalDetailVM = new JavaScriptSerializer().Deserialize<PersonalDetailViewModel>(response);
                if (personalDetailVM.employeeDetail != null)
                {
                    model.employeeId = id;
                    model.firstName = personalDetailVM.employeeDetail.firstName;
                    model.lastName = personalDetailVM.employeeDetail.lastName;
                    model.lastName = personalDetailVM.employeeDetail.lastName;
                    model.trainingCount = personalDetailVM.employeeDetail.trainingCount;
                    Session["IsManager"] = model.isManager = personalDetailVM.employeeDetail.isManager;

                    if (Request.QueryString["itemId"] != null)
                    {
                        TempData["__GiftId__"] = Request.QueryString["itemId"].ToString();
                    }

                    personalDetailVM.employeeDetail = new EmployeeDetailViewModel();
                }
            }

            return View(model);
        }

        #endregion

    }
}