﻿using BHGHRModuleWeb.ViewModels;
using BusinessModule.NetworkManagers;
using BusinessModule.NetworkObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace BHGHRModuleWeb.Controllers.Employees
{
    public class MyTeamController : ABaseController
    {
        // GET: MyTeam
        public ActionResult Index(int Id = 0)
        {
            MyTeamGridViewModel team = new MyTeamGridViewModel();
            team = GetMyTeams(Id);
            return PartialView("~/Views/Employees/MyTeam/_MyTeam.cshtml", team);
        }
        public ActionResult GetTeamDetail(int teamId, int employeeId)
        {
            MyTeamDetailGridViewModel teamDetail = new MyTeamDetailGridViewModel();
            teamDetail = GetMyTeamDetail(teamId, employeeId);
            return PartialView("~/Views/Employees/MyTeam/_MyTeamDetail.cshtml", teamDetail);
        }
        public MyTeamGridViewModel GetMyTeams(int Id = 0)
        {
            var manager = new MyTeamManager();
            var dataRequest = new MyTeamListingDataRequest();
            dataRequest.employeeId = Id;
            dataRequest.sortBy = "teamId";
            dataRequest.sortOrder = "ASC";
            int pageNo = 1;
            if (HttpContext.Request.QueryString["page"] != null && HttpContext.Request.QueryString["page"] != "0")
            {
                pageNo = Convert.ToInt32(HttpContext.Request.QueryString["page"]);
            }
            if (HttpContext.Request.QueryString["sort"] != null && HttpContext.Request.QueryString["sortdir"] != null)
            {
                dataRequest.sortBy = HttpContext.Request.QueryString["sort"].ToString();
                dataRequest.sortOrder = HttpContext.Request.QueryString["sortdir"].ToString();
            }
            dataRequest.pagination.pageSize = _PageSize;
            dataRequest.pagination.pageNo = pageNo;
            var response = manager.GetMyTeams(dataRequest);
            MyTeamGridViewModel model = new JavaScriptSerializer().Deserialize<MyTeamGridViewModel>(response);
            if (model.teamList != null)
            {
                foreach (MyTeams team in model.teamList)
                {
                    team.employeeId = int.Parse(RouteData.Values["id"].ToString());
                }
            }
            if (model.pagination != null)
            {
                ViewBag.TotalRows = model.pagination.totalRows;
                ViewBag.PageSize = model.pagination.pageSize;

            }
            else
            {
                ViewBag.TotalRows = 0;
                ViewBag.PageSize = _ReportPageSize;
                model.teamList = new List<MyTeams>();
                model.pagination = new PaginationViewModel();
            }
            return model;
        }
        public MyTeamDetailGridViewModel GetMyTeamDetail(int teamId, int employeeId)
        {
            var manager = new MyTeamManager();
            var dataRequest = new TeamEmployeeDataRequest();
            if (RouteData.Values["id"] != null)
            {
                dataRequest.employeeId = int.Parse(RouteData.Values["id"].ToString());
            }
            else
            {
                if (Session["employeeId"] != null)
                {
                    dataRequest.employeeId = int.Parse(Session["employeeId"].ToString());
                }
            }
            dataRequest.teamId = teamId;
            dataRequest.employeeId = employeeId;
            dataRequest.sortBy = "teamName";
            dataRequest.sortOrder = "ASC";
            int pageNo = 1;
            if (HttpContext.Request.QueryString["page"] != null && HttpContext.Request.QueryString["page"] != "0")
            {
                pageNo = Convert.ToInt32(HttpContext.Request.QueryString["page"]);
            }
            if (HttpContext.Request.QueryString["sort"] != null && HttpContext.Request.QueryString["sortdir"] != null)
            {
                dataRequest.sortBy = HttpContext.Request.QueryString["sort"].ToString();
                dataRequest.sortOrder = HttpContext.Request.QueryString["sortdir"].ToString();
            }
            dataRequest.pagination.pageSize = _PageSize;
            dataRequest.pagination.pageNo = pageNo;
            var response = manager.GetEmployeeByTeam(dataRequest);

            MyTeamDetailGridViewModel model = new JavaScriptSerializer().Deserialize<MyTeamDetailGridViewModel>(response);
            ViewBag.PageSize = _PageSize;
            if (model.pagination != null)
            {
                model.teamId = teamId;
                model.employeeId = employeeId;
                ViewBag.TotalRows = model.pagination.totalRows;
                ViewBag.PageSize = model.pagination.pageSize;
            }
            else
            {
                ViewBag.TotalRows = 0;
            }
            return model;
        }
        public ActionResult SearchMyTeam(int id = 0)
        {
            MyTeamGridViewModel model = GetMyTeams(id);
            return PartialView("~/Views/Employees/MyTeam/_MyTeamGrid.cshtml", model);
        }

        public ActionResult SearchMyTeamDetail(int teamId, int employeeId)
        {

            if (RouteData.Values["id"] != null)
            {
                employeeId = int.Parse(RouteData.Values["id"].ToString());
            }
            else
            {
                if (Session["employeeId"] != null)
                {
                    employeeId = int.Parse(Session["employeeId"].ToString());
                }
            }
            MyTeamDetailGridViewModel model = GetMyTeamDetail(teamId, employeeId);
            return PartialView("~/Views/Employees/MyTeam/_MyTeamDetailGrid.cshtml", model);
        }
    }
}