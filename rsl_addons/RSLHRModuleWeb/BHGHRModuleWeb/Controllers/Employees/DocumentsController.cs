﻿using AutoMapper;
using BHGHRModuleWeb.Helpers;
using BHGHRModuleWeb.ViewModels;
using BusinessModule.NetworkManagers;
using BusinessModule.NetworkObjects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace BHGHRModuleWeb.Controllers.Employees
{
    public class DocumentsController : ABaseController
    {
        // GET: Documents
        public ActionResult Index(int id = 0)
        {
            DocumentViewModel documents = GetDocuments(id);
            return PartialView("~/Views/Employees/Documents/_Document.cshtml", documents);
        }
        public ActionResult SearchDocument(int id = 0)
        {
            DocumentViewModel docs = GetDocuments(id);
            return PartialView("~/Views/Employees/Documents/_DocumentListing.cshtml", docs.documentList);
        }
        public JsonResult GetDocumentById(int Id)
        {
            DocumentViewModel model = new DocumentViewModel();
            DocumentUploadViewModel detail = new DocumentUploadViewModel();
            model = (DocumentViewModel)Session["Documents"];
            detail = model.documentList.documentList.SingleOrDefault(c => c.documentId == Id);
            return Json(detail, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AddAmendDocuments(DocumentViewModel docDetail)
        {
            var response = "";
            var documentsManager = new DocumentsManager();
            var documentDataRequest = new DocumentDataRequest();
            if (docDetail.docUpload.file != null && docDetail.docUpload.file.ContentLength > 0)
                try
                {
                    FileHelper.createDirectory(FileHelper.GetRefDocUploadPath() + "\\" + docDetail.docUpload.employeeId.ToString());
                    string fileName = FileHelper.getUniqueFileName(docDetail.docUpload.file.FileName);
                    string path = Path.Combine(FileHelper.GetRefDocUploadPath() + "\\" + docDetail.docUpload.employeeId.ToString(), fileName);
                    docDetail.docUpload.file.SaveAs(path);
                    ViewBag.Message = "File uploaded successfully";
                    docDetail.docUpload.documentPath = fileName;
                    docDetail.docUpload.createdBy = GetUserIdFromSession();
                    documentDataRequest = Mapper.Map<DocumentUploadViewModel, DocumentDataRequest>(docDetail.docUpload);
                    documentDataRequest.documentPath = fileName;
                    documentDataRequest.employeeVisibility = docDetail.docUpload.employeeVisibi;
                    response = documentsManager.AddDocumentRecord(documentDataRequest);
                }
                catch (Exception ex)
                {
                    ViewBag.Message = "ERROR:" + ex.Message.ToString();
                }
            else
            {
                ViewBag.Message = "You have not specified a file.";
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public DocumentViewModel GetDocuments(int id = 0)
        {
            var manager = new DocumentsManager();
            var dataRequest = new DocumentsListingDataRequest();
            dataRequest.employeeId = id;
            dataRequest.sortBy = "documentId";
            dataRequest.sortOrder = "DESC";
            int pageNo = 1;
            if (HttpContext.Request.QueryString["page"] != null && HttpContext.Request.QueryString["page"] != "0")
            {
                pageNo = Convert.ToInt32(HttpContext.Request.QueryString["page"]);
            }
            if (HttpContext.Request.QueryString["sort"] != null && HttpContext.Request.QueryString["sortdir"] != null)
            {
                dataRequest.sortBy = HttpContext.Request.QueryString["sort"].ToString();
                dataRequest.sortOrder = HttpContext.Request.QueryString["sortdir"].ToString();
            }
            if (Request["documetTypeId"] != null && Request["documetTypeId"] != "" && Request["documetTypeId"] != "0")
            {
                dataRequest.documetTypeId = Convert.ToInt32(Request["documetTypeId"]);
            }
            if (Request["title"] != null && Request["title"] != string.Empty)
            {
                dataRequest.title = Request["title"].ToString();
            }

            if (Request["keyWords"] != null && Request["keyWords"] != string.Empty)
            {
                dataRequest.keyWords = Request["keyWords"].ToString();
            }
            dataRequest.pagination.pageSize = _PageSize;
            dataRequest.pagination.pageNo = pageNo;

            dataRequest.loggedInUser = int.Parse(Session["UserId"].ToString());
            // for employee view (condition apply 'employeeVisibility = true') other vise get all documents 

            var isMyJob = false;
            var isDisabled = "false";

            if (Session["Module"] != null && Session["Module"].ToString() == "MyJob")
            {
                isMyJob = true;
            }

            var isTeam = false;
            if (TempData.ContainsKey("isTeam"))
            {
                isTeam = bool.Parse(TempData["isTeam"].ToString());
            }
            TempData.Keep("isTeam");

            if (isMyJob)
            {
                isDisabled = "true";
            }

            if (isTeam)
            {
                isDisabled = "false";
            }

            if (isDisabled == "false")
            {
                dataRequest.employeeVisibility = false;
            }
            else
            {
                dataRequest.employeeVisibility = true;
            }

            var response = manager.GetEmployeeDocuments(dataRequest);
            DocumentViewModel model = new JavaScriptSerializer().Deserialize<DocumentViewModel>(response);
            if (string.IsNullOrEmpty(response))
            {
                model = new DocumentViewModel();
                ViewBag.TotalRows = 0;
                ViewBag.PageSize = _PageSize;
            }
            else
            {
                ViewBag.TotalRows = model.documentList.pagination.totalRows;
                ViewBag.PageSize = model.documentList.pagination.pageSize;
            }
            model.isHR = IsHR();
            model.docSearch.employeeId = model.docUpload.employeeId = id;
            Session["Documents"] = model;
            return model;
        }

        public string RenderRazorViewToString(string viewName, object model)
        {
            ViewData.Model = model;
            using (var sw = new StringWriter())
            {
                var viewResult = ViewEngines.Engines.FindPartialView(ControllerContext,
                                                                         viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View,
                                             ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                return sw.GetStringBuilder().ToString();
            }
        }

        public ActionResult RemoveDocumentById(int Id)
        {
            var response = "";
            var manager = new DocumentsManager();
            var dataRequest = new DocumentDataRequest();
            dataRequest.documentId = Id;
            dataRequest.updatedBy = GetUserIdFromSession();
            response = manager.deleteDocument(dataRequest);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}