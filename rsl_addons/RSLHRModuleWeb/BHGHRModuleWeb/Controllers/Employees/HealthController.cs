﻿using AutoMapper;
using BHGHRModuleWeb.Helpers;
using BHGHRModuleWeb.ViewModels;
using BusinessModule.Constants;
using BusinessModule.NetworkManagers;
using BusinessModule.NetworkObjects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace BHGHRModuleWeb.Controllers.Employees
{
    public class HealthController : ABaseController
    {
        // GET: Health
        public ActionResult Index()
        {
            if (RouteData.Values["id"] != null)
            {
                ViewBag.isEdit = "edit";
            }
            else
            {
                if (Session["employeeId"] != null)
                {
                    ViewBag.isEdit = "create";
                }
            }
            return View();
        }
        public ActionResult GetDifficultiesAndDisablities(int id = 0)
        {
            HealthListViewModel model = new HealthListViewModel();
            var healthManager = new HealthManager();
            var healthDataRequest = new HealthDataRequest();
            healthDataRequest.loggedBy = GetUserIdFromSession();
            if (id != 0)
            {
                ViewBag.isEdit = "edit";
                healthDataRequest.employeeId = id;
            }
            else if (Session["employeeId"] != null)
            {
                ViewBag.isEdit = "create";
                healthDataRequest.employeeId = int.Parse(Session["employeeId"].ToString());
            }
            
            var response = healthManager.getDifficultiesAndDisablities(healthDataRequest);
            model = new JavaScriptSerializer().Deserialize<HealthListViewModel>(response);
            model.healthDetail = new HealthDetailViewModel();
            model.healthDetail.employeeId = healthDataRequest.employeeId;
            foreach (var item in model.Disablities)
            {
                item.employeeId = healthDataRequest.employeeId;
            }
            model.healthDetail.IsHR = IsHR();
            Session["disabilities"] = model;
            return PartialView("~/Views/Employees/Health/_HealthMaster.cshtml", model);
        }

        #region >>> helpers <<<
        
        [HttpPost]
        public ActionResult AddAmendHealthRecord(HealthDetailViewModel healthDetail, HttpPostedFileBase file)
        {
            var response = "";
            var healthManager = new HealthManager();
            var healthDataRequest = new HealthDataRequest();

            if (ModelState.IsValid)
            {
                if (RouteData.Values["id"] != null)
                {
                    healthDetail.employeeId = int.Parse(RouteData.Values["id"].ToString());
                }
                else
                {
                    if (Session["employeeId"] != null)
                    {
                        healthDetail.employeeId = int.Parse(Session["employeeId"].ToString());
                    }
                }
                if (file != null && file.ContentLength > 0)
                {
                    try
                    {
                        FileHelper.createDirectory(FileHelper.GetDisabilityHealthDocUploadPath());
                        string fileName = FileHelper.getUniqueFileName(file.FileName);

                        string path = Path.Combine(FileHelper.GetDisabilityHealthDocUploadPath() + "/" + healthDetail.employeeId.ToString(), fileName);
                        if (!Directory.Exists(Path.Combine(FileHelper.GetDisabilityHealthDocUploadPath() + "/" + healthDetail.employeeId.ToString())))
                        {
                            Directory.CreateDirectory(Path.Combine(FileHelper.GetDisabilityHealthDocUploadPath() + "/" + healthDetail.employeeId.ToString()));
                        }

                        file.SaveAs(path);
                        ViewBag.Message = "File uploaded successfully";
                        healthDetail.docPath = fileName;
                    }
                    catch (Exception ex)
                    {
                        ViewBag.Message = "ERROR:" + ex.Message.ToString();
                    }
                }
                else if (!string.IsNullOrEmpty(healthDetail.docPath))
                {
                    healthDetail.docPath = getFileNameFromPath(healthDetail.docPath);
                }

                healthDetail.loggedBy = GetUserIdFromSession();
                
                healthDataRequest = Mapper.Map<HealthDetailViewModel, HealthDataRequest>(healthDetail);
                response = healthManager.addAmendHealthRecord(healthDataRequest);
                if (response == UserMessageConstants.successMessage)
                {
                    var responseModel = new FailerResponseViewModel();
                    responseModel.isSuccessFul = true;
                    responseModel.message = response;
                    return Json(responseModel.ToString(), JsonRequestBehavior.AllowGet);
                }
                response = "{'isSuccessFul':true,'message':'" + response + "'}";
            }
            var errorList = (from item in ModelState.Values
                             from error in item.Errors
                             select error.ErrorMessage).ToList();
            var message = "";
            for (int i = 0; i < errorList.Count; i++)
            {
                message = message + errorList[i].ToString() + "<br />";
            }
            var failerResponse = new FailerResponseViewModel();
            failerResponse.message = message;
            //"{'isSuccessFul':true,'message':'Personal detail is updated successfully.'}"
            response = "{'isSuccessFul':false,'message':'" + message + "'}";
            return Json(failerResponse.ToString(), JsonRequestBehavior.AllowGet);
        }


       
        public ActionResult DeleteDisabilityHealthRecord(HealthDetailViewModel healthDetail)
        {
            var response = "";
            var healthManager = new HealthManager();
            var healthDataRequest = new HealthDataRequest();
                healthDetail.loggedBy = GetUserIdFromSession();
                if (RouteData.Values["id"] != null)
                {
                    healthDetail.employeeId = int.Parse(RouteData.Values["id"].ToString());
                }
                else
                {
                    if (Session["employeeId"] != null)
                    {
                        healthDetail.employeeId = int.Parse(Session["employeeId"].ToString());
                    }
                }
                healthDataRequest = Mapper.Map<HealthDetailViewModel, HealthDataRequest>(healthDetail);
                response = healthManager.deleteDisabilityHealthRecord(healthDataRequest);
                return Json(response, JsonRequestBehavior.AllowGet);
           
           
        }


        public ActionResult GetDisability(int Id)
        {
            HealthListViewModel model = new HealthListViewModel();
            model = (HealthListViewModel)Session["disabilities"];
            model.healthDetail = model.Disablities.SingleOrDefault(c => c.diffDisabilityId == Id);
            model.healthDetail.IsHR = IsHR();
            return PartialView("~/Views/Employees/Health/_HealthDetail.cshtml", model);
        }
        //public JsonResult GetLearningDifficulty(int Id)
        //{
        //    HealthListViewModel model = new HealthListViewModel();
        //    HealthDetailViewModel detail = new HealthDetailViewModel();
        //    model = (HealthListViewModel)Session["disabilities"];
        //    detail = model.learningDifficulties.SingleOrDefault(c => c.diffDisabilityId == Id);
        //    return Json(detail, JsonRequestBehavior.AllowGet);
        //}
        #endregion
    }
}