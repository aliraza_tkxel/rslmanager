﻿using AutoMapper;
using BHGHRModuleWeb.ViewModels;
using BusinessModule.NetworkManagers;
using BusinessModule.NetworkObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace BHGHRModuleWeb.Controllers.Employees
{
    public class MyStaffController : ABaseController
    {

        #region >>> Helper <<<

        private MyStaffGridViewModel GetMyStaff(int Id = 0)
        {
            _PageSize = 25;
            var manager = new MyStaffManager();
            var dataRequest = new MyStaffDataRequest();
            dataRequest.employeeId = Id;
            if (HttpContext.Request.QueryString["isDirector"] != null)
            {
                dataRequest.isDirector = Convert.ToBoolean(HttpContext.Request.QueryString["isDirector"].ToString());
            }

            int pageNo = 1;
            if (HttpContext.Request.QueryString["page"] != null && HttpContext.Request.QueryString["page"] != "0")
            {
                pageNo = Convert.ToInt32(HttpContext.Request.QueryString["page"]);
            }
            if (HttpContext.Request.QueryString["sort"] != null && HttpContext.Request.QueryString["sortdir"] != null)
            {
                dataRequest.sortBy = HttpContext.Request.QueryString["sort"].ToString();
                dataRequest.sortOrder = HttpContext.Request.QueryString["sortdir"].ToString();
            }
            dataRequest.pagination.pageSize = _PageSize;
            dataRequest.pagination.pageNo = pageNo;
            var response = manager.GetMyStaff(dataRequest);
            MyStaffGridViewModel model = new JavaScriptSerializer().Deserialize<MyStaffGridViewModel>(response);
            model.isDirector = dataRequest.isDirector;
            if (model.pagination != null)
            {
                ViewBag.TotalRows = model.pagination.totalRows;
                ViewBag.PageSize = model.pagination.pageSize;
            }
            else
            {
                ViewBag.TotalRows = 1;
                ViewBag.PageSize = 1;
            }
            _PageSize = 15;
            return model;
        }

        #endregion

        #region >>> Action Method <<<

        #region >>> DOI <<<

        public ActionResult GetDeclarationOfInterestsRequest(int Id)
        {
            var manager = new ReportsManager();
            var response = manager.GetDeclarationOfInterestById(Id);

            DeclarationViewModel model = new JavaScriptSerializer().Deserialize<DeclarationViewModel>(response);

            if (model.declarationOfInterest.page1.isSecondaryEmployment.HasValue)
            {
                model.declarationOfInterest.page1.isEmployment = model.declarationOfInterest.page1.isSecondaryEmployment.Value;
            }

            if (model.declarationOfInterest.page2.isConApprovedByLeadership.HasValue)
            {
                model.declarationOfInterest.page2.isLeadership = model.declarationOfInterest.page2.isConApprovedByLeadership.Value;
            }

            if (model.declarationOfInterest.page2.isDirectorP8.HasValue)
            {
                model.declarationOfInterest.page2.isTempDirectorP8 = model.declarationOfInterest.page2.isDirectorP8.Value;
            }

            if (model.declarationOfInterest.page2.isDirectorP9.HasValue)
            {
                model.declarationOfInterest.page2.isTempDirectorP9 = model.declarationOfInterest.page2.isDirectorP9.Value;
            }
            model.declarationOfInterest.page3 = model.declarationOfInterest.page3;

            return PartialView("~/Views/Employees/MyStaff/_DeclarationOfInterestReviewPopup.cshtml", model);
        }

        public ActionResult UpdateDeclarationOfInterestStatus(DeclarationOfInterestReviewStatusViewModel data)
        {
            ReportsManager rptManager = new ReportsManager();
            DeclarationOfInterestStatusDataRequest dataRequest = new DeclarationOfInterestStatusDataRequest();
            dataRequest = Mapper.Map<DeclarationOfInterestReviewStatusViewModel, DeclarationOfInterestStatusDataRequest>(data);
            dataRequest.lastActionUser = GetUserIdFromSession();
            var response = rptManager.UpdateDeclarationOfInterestStatus(dataRequest);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        #endregion

        // GET: MyStaff
        public ActionResult Index(int Id = 0)
        {
            MyStaffGridViewModel model = new MyStaffGridViewModel();
            model = GetMyStaff(Id);
            return PartialView("~/Views/Employees/MyStaff/_MyStaff.cshtml", model);
        }

        public ActionResult SearchMyStaff(int id = 0)
        {
            MyStaffGridViewModel model = GetMyStaff(id);
            return PartialView("~/Views/Employees/MyStaff/_MyStaffGrid.cshtml", model);
        }

        

        #endregion


    }
}