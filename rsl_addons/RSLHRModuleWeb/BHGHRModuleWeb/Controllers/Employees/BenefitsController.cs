﻿using AutoMapper;
using BHGHRModuleWeb.ViewModels;
using BusinessModule.NetworkManagers;
using BusinessModule.NetworkObjects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using BHGHRModuleWeb.Helpers;
using Newtonsoft.Json;

namespace BHGHRModuleWeb.Controllers.Employees
{
    public class BenefitsController : ABaseController
    {
        // GET: Benefits
        public ActionResult Index()
        {
            BenefitsResponseViewModel model = getEmployeeBenefits();
            if (RouteData.Values["id"] != null)
            {
                ViewBag.isEdit = "edit";
                model.benefits.employeeId = int.Parse(RouteData.Values["id"].ToString());
            }
            else
            {
                if (Session["employeeId"] != null)
                {
                    ViewBag.isEdit = "create";
                    model.benefits.employeeId = int.Parse(Session["employeeId"].ToString());
                }
            }
            Session["Benefits"] = model;
            List<SelectViewModel> vehicle = new List<SelectViewModel>
            {
                new SelectViewModel(){ Name = "Company Car", ID="CompanyCar" },
                new SelectViewModel(){ Name = "Essential User", ID="EssentialUser" },
            };
            List<SelectViewModel> fuel = new List<SelectViewModel>
            {
                new SelectViewModel(){ Name = "Diesel", ID="Diesel" },
                new SelectViewModel(){ Name = "Electric", ID="Electric" },
                new SelectViewModel(){ Name = "Hybrid", ID="Hybrid" },
                new SelectViewModel(){ Name = "LPG", ID="LPG" },
                new SelectViewModel(){ Name = "Petrol", ID="Petrol" }
                
            };
            if (model.benefitTypeList != null)
                model.benefitTypeList.Add(new SelectListItem() { Text = "Vehicle", Value = "Vehicle" });
            model.fuelCar = new SelectList(fuel, "ID", "Name");
            model.vehicle = new SelectList(vehicle, "ID", "Name");

            var manager = new JobDetailManager();
            var loggedInUserTeam = new JavaScriptSerializer().Deserialize<string>(manager.getLoggedInUserTeam(int.Parse(Session["UserId"].ToString())));
            ViewBag.IsHR = false;
            if (loggedInUserTeam.ToString() == "HR Services")
            {
                ViewBag.IsHR = true;
            }
            return PartialView("~/Views/Employees/Benefits/_Benefits.cshtml", model);
        }
        [HttpPost]
        public ActionResult AddAmendBenefitRecord(BenefitsResponseViewModel bvm, HttpPostedFileBase file1 , HttpPostedFileBase file2)
        {
            var response = "";
            var benefitId = 0;
            if (RouteData.Values["id"] != null)
            {
                bvm.benefits.employeeId = int.Parse(RouteData.Values["id"].ToString());
            }
            else
            {
                if (Session["employeeId"] != null)
                {
                    bvm.benefits.employeeId = int.Parse(Session["employeeId"].ToString());
                }
            }
            BenefitsDataRequest benefitsDetailDR = new BenefitsDataRequest();
            var manager = new BenefitsManager();
            if (bvm.benefits.benefitId > 0)
            {
                benefitId = bvm.benefits.benefitId;
                BenefitsResponseViewModel model = new BenefitsResponseViewModel();
                model.benefits.benefitId= bvm.benefits.benefitId;
                model.benefits.employeeId = bvm.benefits.employeeId;
                // model = (BenefitsResponseViewModel)Session["Benefits"];
                if (bvm.benefits.requestType == "accomodation")
                {
                    model.benefits.accomodationRent = bvm.benefits.accomodationRent;
                    model.benefits.councilTax = bvm.benefits.councilTax;
                    model.benefits.heating = bvm.benefits.heating;
                    model.benefits.lineRental = bvm.benefits.lineRental;
                }
                else if (bvm.benefits.requestType == "vehicle")
                {

                    if (bvm.benefits.vehicle.fiscalYear == 0 || String.IsNullOrEmpty(bvm.benefits.vehicle.carStartDate) || String.IsNullOrEmpty(bvm.benefits.vehicle.carRegistration) || String.IsNullOrEmpty(bvm.benefits.vehicle.fuel) || String.IsNullOrEmpty(bvm.benefits.vehicle.engineSize))
                    {
                        return Json("\"" + "Please Select Required Fields" + "\"", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {

                        model.benefits.vehicle.benefitVehicleId = bvm.benefits.vehicle.benefitVehicleId;
                        model.benefits.vehicle.benefitId = bvm.benefits.benefitId;
                        model.benefits.vehicle.employeeId = bvm.benefits.employeeId;
                        model.benefits.vehicle.fiscalYear = bvm.benefits.vehicle.fiscalYear;
                        model.benefits.vehicle.carStartDate = bvm.benefits.vehicle.carStartDate;
                        model.benefits.vehicle.carEndDate = bvm.benefits.vehicle.carEndDate;
                        model.benefits.vehicle.carRegistration = bvm.benefits.vehicle.carRegistration;
                        model.benefits.vehicle.carMake = bvm.benefits.vehicle.carMake;
                        model.benefits.vehicle.model = bvm.benefits.vehicle.model;
                        model.benefits.vehicle.fuel = bvm.benefits.vehicle.fuel;
                        model.benefits.vehicle.engineSize = bvm.benefits.vehicle.engineSize;
                        model.benefits.vehicle.motRenewalDate = bvm.benefits.vehicle.motRenewalDate;
                        model.benefits.vehicle.insuranceCompany = bvm.benefits.vehicle.insuranceCompany;
                        model.benefits.vehicle.insuranceRenewalDate = bvm.benefits.vehicle.insuranceRenewalDate;
                        model.benefits.vehicle.insuranceReimbursement = bvm.benefits.vehicle.insuranceReimbursement;
                        model.benefits.vehicle.dvlaOnline = bvm.benefits.vehicle.dvlaOnline;
                        model.benefits.vehicle.dvlaOnlineDate = bvm.benefits.vehicle.dvlaOnlineDate;
                        model.benefits.vehicle.additionalDriver = bvm.benefits.vehicle.additionalDriver;
                        model.benefits.vehicle.Type = bvm.benefits.vehicle.Type;
                        if (file1 != null)
                        {
                            if (file1 != null && file1.ContentLength > 0)
                            {
                                //do processing of first file
                                FileHelper.createDirectory(FileHelper.GetRefDocUploadPath() + bvm.benefits.employeeId + "\\BenefitVehicle\\");
                                string fileName = FileHelper.getUniqueFileName(file1.FileName);
                                string path = Path.Combine(FileHelper.GetRefDocUploadPath() + bvm.benefits.employeeId + "\\BenefitVehicle\\", fileName);
                                file1.SaveAs(path);
                                ViewBag.Message = "File uploaded successfully";
                                model.benefits.vehicle.V5Path = fileName;
                            }
                        }
                        else
                        {
                            model.benefits.vehicle.V5Path = bvm.benefits.vehicle.V5doc;
                        }
                        //Let's take the second one now.
                        if (file2 != null)
                        {
                            if (file2 != null && file2.ContentLength > 0)
                            {
                                //do processing of second file here
                                FileHelper.createDirectory(FileHelper.GetRefDocUploadPath() + bvm.benefits.employeeId + "\\BenefitVehicle\\");
                                string fileName = FileHelper.getUniqueFileName(file2.FileName);
                                string path = Path.Combine(FileHelper.GetRefDocUploadPath() + bvm.benefits.employeeId + "\\BenefitVehicle\\", fileName);
                                file2.SaveAs(path);
                                ViewBag.Message = "File uploaded successfully";
                                model.benefits.vehicle.policySummaryPath = fileName;
                            }
                        }
                        else
                        {
                            model.benefits.vehicle.policySummaryPath = bvm.benefits.vehicle.Summarydoc;
                        }
                    }
                }
                else if (bvm.benefits.requestType == "general")
                {
                    if (bvm.benefits.general.fiscalYear == 0)
                    {
                        return Json("\"" + "Please Select Required Fields" + "\"", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        model.benefits.general.benefitGeneralId = bvm.benefits.general.benefitGeneralId;
                        model.benefits.general.fiscalYear = bvm.benefits.general.fiscalYear;
                        model.benefits.general.employeeId = bvm.benefits.employeeId;
                        model.benefits.general.benefitId = bvm.benefits.benefitId;
                        model.benefits.general.callOutAllowance = bvm.benefits.general.callOutAllowance;
                        model.benefits.general.birthdayLeave = bvm.benefits.general.birthdayLeave;
                        model.benefits.general.carParking = bvm.benefits.general.carParking;
                        model.benefits.general.childCareVouchers = bvm.benefits.general.childCareVouchers;
                        model.benefits.general.employeeAssistance = bvm.benefits.general.employeeAssistance;
                        model.benefits.general.enhancedHoliday = bvm.benefits.general.enhancedHoliday;
                        model.benefits.general.enhancedSickPay = bvm.benefits.general.enhancedSickPay;
                        model.benefits.general.eyeCareAssistance = bvm.benefits.general.eyeCareAssistance;
                        model.benefits.general.firstAiderAllowance = bvm.benefits.general.firstAiderAllowance;
                        model.benefits.general.fluHepBJab = bvm.benefits.general.fluHepBJab;
                        model.benefits.general.learningAndDevelopment = bvm.benefits.general.learningAndDevelopment;
                        model.benefits.general.lifeAssurance = bvm.benefits.general.lifeAssurance;
                        model.benefits.general.personalDay = bvm.benefits.general.personalDay;
                        model.benefits.general.VoluntaryDay = bvm.benefits.general.VoluntaryDay;
                        model.benefits.general.birthdayLeaveDate = bvm.benefits.general.birthdayLeaveDate;
                        model.benefits.general.ECU = bvm.benefits.general.ECU;
                        model.benefits.general.EnhancedFamilyLeave = bvm.benefits.general.EnhancedFamilyLeave;
                        model.benefits.general.PHI = bvm.benefits.general.PHI;
                        model.benefits.general.Other = bvm.benefits.general.Other;
                        model.benefits.general.Overtime = bvm.benefits.general.Overtime;
                        model.benefits.general.carParkingDate = bvm.benefits.general.carParkingDate;
                        model.benefits.general.childCareVouchersDate = bvm.benefits.general.childCareVouchersDate;
                        model.benefits.general.employeeAssistanceDate = bvm.benefits.general.employeeAssistanceDate;
                        model.benefits.general.enhancedHolidayDate = bvm.benefits.general.enhancedHolidayDate;
                        model.benefits.general.enhancedSickPayDate = bvm.benefits.general.enhancedSickPayDate;
                        model.benefits.general.eyeCareAssistanceDate = bvm.benefits.general.eyeCareAssistanceDate;
                        model.benefits.general.firstAiderAllowanceDate = bvm.benefits.general.firstAiderAllowanceDate;
                        model.benefits.general.fluHepBJabDate = bvm.benefits.general.fluHepBJabDate;
                        model.benefits.general.learningAndDevelopmentDate = bvm.benefits.general.learningAndDevelopmentDate;
                        model.benefits.general.lifeAssuranceDate = bvm.benefits.general.lifeAssuranceDate;
                        model.benefits.general.personalDayDate = bvm.benefits.general.personalDayDate;
                        model.benefits.general.VoluntaryDayDate = bvm.benefits.general.VoluntaryDayDate;
                        model.benefits.general.callOutAllowanceDate = bvm.benefits.general.callOutAllowanceDate;
                        model.benefits.general.ECUDate = bvm.benefits.general.ECUDate;
                        model.benefits.general.EnhancedFamilyLeaveDate = bvm.benefits.general.EnhancedFamilyLeaveDate;
                        model.benefits.general.PHIDate = bvm.benefits.general.PHIDate;
                        model.benefits.general.OtherDate = bvm.benefits.general.OtherDate;
                        model.benefits.general.OvertimeDate = bvm.benefits.general.OvertimeDate;
                    }
                }

                else if (bvm.benefits.requestType == "health")
                {

                    if (bvm.benefits.health.fiscalYear == 0 || bvm.benefits.health.medicalOrgId==0 || String.IsNullOrEmpty(bvm.benefits.health.effectiveFrom) || String.IsNullOrEmpty(bvm.benefits.health.endDate) || (bvm.benefits.health.isPrivateMedical == 1 && bvm.benefits.health.excessPaid<=0))
                    {
                        return Json("\"" + "Please Select Required Fields" + "\"", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        model.benefits.health.benefitHealthId = bvm.benefits.health.benefitHealthId;
                        model.benefits.health.fiscalYear = bvm.benefits.health.fiscalYear;
                        model.benefits.health.employeeId = bvm.benefits.employeeId;
                        model.benefits.health.benefitId = bvm.benefits.benefitId;
                        model.benefits.health.effectiveFrom = bvm.benefits.health.effectiveFrom;
                        model.benefits.health.endDate = bvm.benefits.health.endDate;
                        model.benefits.health.excessPaid = bvm.benefits.health.excessPaid;
                        model.benefits.health.annualPremium = bvm.benefits.health.annualPremium;
                        model.benefits.health.membershipNumber = bvm.benefits.health.membershipNumber;
                        model.benefits.health.isPrivateMedical = bvm.benefits.health.isPrivateMedical;
                        model.benefits.health.medicalOrgId = bvm.benefits.health.medicalOrgId;
                    }
                }
                else if (bvm.benefits.requestType == "subscription")
                {
                    if (bvm.benefits.subscription.pSubscription == null || bvm.benefits.subscription.pSubscription == "" )
                    {
                        return Json("\"" + "Please Select Required Fields" + "\"", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        JavaScriptSerializer js = new JavaScriptSerializer();
                    SubscriptionViewModel[] list =
                        js.Deserialize<SubscriptionViewModel[]>(bvm.benefits.subscription.pSubscription);
                    List<BenefitSubscriptionViewModel> sublst = new List<BenefitSubscriptionViewModel>();
                    foreach (SubscriptionViewModel sub in list)
                    {
                        BenefitSubscriptionViewModel bSubscript = new BenefitSubscriptionViewModel();
                        bSubscript.employeeId = bvm.benefits.employeeId;
                        bSubscript.benefitId = bvm.benefits.benefitId;
                        bSubscript.fiscalYear = sub.year;
                        bSubscript.subscriptionTitle = sub.title;
                        bSubscript.subscriptionCost = sub.amount;
                        sublst.Add(bSubscript);
                    }
                        model.benefits.subscriptionlst = sublst;
                    }
                }

                else if (bvm.benefits.requestType == "medicalInsurance")
                {
                    model.benefits.medicalOrgId = bvm.benefits.medicalOrgId;
                    model.benefits.taxableBenefit = bvm.benefits.taxableBenefit;
                    model.benefits.annualPremium = bvm.benefits.annualPremium;
                    model.benefits.additionalMembers = bvm.benefits.additionalMembers;
                    model.benefits.additionalMembers2 = bvm.benefits.additionalMembers2;
                    model.benefits.additionalMembers3 = bvm.benefits.additionalMembers3;
                    model.benefits.additionalMembers4 = bvm.benefits.additionalMembers4;
                    model.benefits.additionalMembers5 = bvm.benefits.additionalMembers5;
                    model.benefits.groupSchemeRef = bvm.benefits.groupSchemeRef;
                    model.benefits.membershipNo = bvm.benefits.membershipNo;
                }
                else if (bvm.benefits.requestType == "pension")
                {
                        model.benefits.pension.benefitPensionId = bvm.benefits.pension.benefitPensionId;
                        model.benefits.pension.fiscalYear = bvm.benefits.pension.fiscalYear;
                        model.benefits.pension.employeeId = bvm.benefits.employeeId;
                        model.benefits.pension.benefitId = bvm.benefits.benefitId;
                        model.benefits.pension.accomodationRent = bvm.benefits.pension.accomodationRent;
                        model.benefits.pension.councilTax = bvm.benefits.pension.councilTax;
                        model.benefits.pension.lineRental = bvm.benefits.pension.lineRental;
                        model.benefits.pension.heating = bvm.benefits.pension.heating;
                        model.benefits.pension.salaryPercent = bvm.benefits.pension.salaryPercent;
                        model.benefits.pension.employeeContribution = bvm.benefits.pension.employeeContribution;
                        model.benefits.pension.employerContribution = bvm.benefits.pension.employerContribution;
                        model.benefits.pension.avc = bvm.benefits.pension.avc;
                        model.benefits.pension.contractedOut = bvm.benefits.pension.contractedOut;
                }
                else if (bvm.benefits.requestType == "PRP")
                {
                    model.benefits.prp.benefitPRPId = bvm.benefits.prp.benefitPRPId;
                    model.benefits.prp.fiscalYear = bvm.benefits.prp.fiscalYear;
                    model.benefits.prp.employeeId = bvm.benefits.employeeId;
                    model.benefits.prp.benefitId = bvm.benefits.benefitId;
                    model.benefits.prp.q1 = bvm.benefits.prp.q1;
                    model.benefits.prp.q2 = bvm.benefits.prp.q2;
                    model.benefits.prp.q3 = bvm.benefits.prp.q3;
                    model.benefits.prp.q4 = bvm.benefits.prp.q4;
                }

                benefitsDetailDR = Mapper.Map<BenefitsViewModel, BenefitsDataRequest>(model.benefits);
               benefitsDetailDR.requestType = bvm.benefits.requestType;
                Session["Benefits"] = model;
            }
            else
            {
                if (bvm.benefits.requestType == "pension")
                {
                    var pension = new BenefitPensionViewModel();
                    pension.benefitPensionId = bvm.benefits.pension.benefitPensionId;
                    pension.fiscalYear = bvm.benefits.pension.fiscalYear;
                    pension.employeeId = bvm.benefits.employeeId;
                    pension.benefitId = bvm.benefits.benefitId;
                    pension.accomodationRent = bvm.benefits.pension.accomodationRent;
                    pension.councilTax = bvm.benefits.pension.councilTax;
                    pension.lineRental = bvm.benefits.pension.lineRental;
                    pension.heating = bvm.benefits.pension.heating;
                    pension.salaryPercent = bvm.benefits.pension.salaryPercent;
                    pension.employeeContribution = bvm.benefits.pension.employeeContribution;
                    pension.employerContribution = bvm.benefits.pension.employerContribution;
                    pension.avc = bvm.benefits.pension.avc;
                    pension.contractedOut = bvm.benefits.pension.contractedOut;
                    bvm.benefits.pension = pension;
                }

                if (bvm.benefits.requestType == "PRP")
                {
                    var prp = new BenefitPRPViewModel();
                    prp.benefitPRPId = bvm.benefits.prp.benefitPRPId;
                    prp.fiscalYear = bvm.benefits.prp.fiscalYear;
                    prp.employeeId = bvm.benefits.employeeId;
                    prp.benefitId = bvm.benefits.benefitId;
                    prp.q1 = bvm.benefits.prp.q1;
                    prp.q2 = bvm.benefits.prp.q2;
                    prp.q3 = bvm.benefits.prp.q3;
                    prp.q4 = bvm.benefits.prp.q4;
                    bvm.benefits.prp = prp;
                }
                //if (bvm.benefits.requestType == "companyCar")
                //    bvm.benefits.drivingLicenceImage = SaveFile(file, bvm.benefits.employeeId);
                if (bvm.benefits.requestType == "vehicle")
                {
                    if (bvm.benefits.vehicle.fiscalYear == 0 || String.IsNullOrEmpty(bvm.benefits.vehicle.carStartDate) || String.IsNullOrEmpty(bvm.benefits.vehicle.carEndDate) || String.IsNullOrEmpty(bvm.benefits.vehicle.carRegistration))
                    {
                        return Json("\"" + "Please Select Required Fields" + "\"", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var vehicle = new BenefitVehicleViewModel();
                        vehicle.benefitVehicleId = bvm.benefits.vehicle.benefitVehicleId;
                        vehicle.benefitId = bvm.benefits.benefitId;
                        vehicle.employeeId = bvm.benefits.employeeId;
                        vehicle.fiscalYear = bvm.benefits.vehicle.fiscalYear;
                        vehicle.carStartDate = bvm.benefits.vehicle.carStartDate;
                        vehicle.carEndDate = bvm.benefits.vehicle.carEndDate;
                        vehicle.carRegistration = bvm.benefits.vehicle.carRegistration;
                        vehicle.carMake = bvm.benefits.vehicle.carMake;
                        vehicle.model = bvm.benefits.vehicle.model;
                        vehicle.fuel = bvm.benefits.vehicle.fuel;
                        vehicle.engineSize = bvm.benefits.vehicle.engineSize;
                        vehicle.additionalDriver = bvm.benefits.vehicle.additionalDriver;
                        vehicle.motRenewalDate = bvm.benefits.vehicle.motRenewalDate;
                        vehicle.insuranceCompany = bvm.benefits.vehicle.insuranceCompany;
                        vehicle.insuranceRenewalDate = bvm.benefits.vehicle.insuranceRenewalDate;
                        vehicle.insuranceReimbursement = bvm.benefits.vehicle.insuranceReimbursement;
                        vehicle.dvlaOnline = bvm.benefits.vehicle.dvlaOnline;
                        vehicle.dvlaOnlineDate = bvm.benefits.vehicle.dvlaOnlineDate;
                        vehicle.Type = bvm.benefits.vehicle.Type;
                        if (file1 != null)
                        {
                            if (file1 != null && file1.ContentLength > 0)
                            {
                                //do processing of first file
                                FileHelper.createDirectory(FileHelper.GetRefDocUploadPath() + bvm.benefits.employeeId + "/BenefitVehicle/");
                                string fileName = FileHelper.getUniqueFileName(file1.FileName);
                                string path = Path.Combine(FileHelper.GetRefDocUploadPath() + bvm.benefits.employeeId + "/BenefitVehicle", fileName);
                                file1.SaveAs(path);
                                ViewBag.Message = "File uploaded successfully";
                                vehicle.V5Path = fileName;
                            }
                        }
                        //Let's take the second one now.
                        if (file2 != null)
                        {
                            if (file2 != null && file2.ContentLength > 0)
                            {
                                //do processing of second file here
                                FileHelper.createDirectory(FileHelper.GetRefDocUploadPath() + bvm.benefits.employeeId + "/BenefitVehicle/");
                                string fileName = FileHelper.getUniqueFileName(file2.FileName);
                                string path = Path.Combine(FileHelper.GetRefDocUploadPath() + bvm.benefits.employeeId + "/BenefitVehicle/", fileName);
                                file2.SaveAs(path);
                                ViewBag.Message = "File uploaded successfully";
                                vehicle.policySummaryPath = fileName;
                            }
                        }
                        bvm.benefits.vehicle = vehicle;
                    }
                }
                if (bvm.benefits.requestType == "general")
                {
                    if (bvm.benefits.general.fiscalYear == 0 )
                    {
                        return Json("\"" + "Please Select Required Fields" + "\"", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var general = new BenefitGeneralViewModel();
                        general.benefitGeneralId = bvm.benefits.general.benefitGeneralId;
                        general.fiscalYear = bvm.benefits.general.fiscalYear;
                        general.employeeId = bvm.benefits.employeeId;
                        general.benefitId = bvm.benefits.benefitId;
                        general.callOutAllowance = bvm.benefits.general.callOutAllowance;
                        general.birthdayLeave = bvm.benefits.general.birthdayLeave;
                        general.carParking = bvm.benefits.general.carParking;
                        general.childCareVouchers = bvm.benefits.general.childCareVouchers;
                        general.employeeAssistance = bvm.benefits.general.employeeAssistance;
                        general.enhancedHoliday = bvm.benefits.general.enhancedHoliday;
                        general.enhancedSickPay = bvm.benefits.general.enhancedSickPay;
                        general.eyeCareAssistance = bvm.benefits.general.eyeCareAssistance;
                        general.firstAiderAllowance = bvm.benefits.general.firstAiderAllowance;
                        general.fluHepBJab = bvm.benefits.general.fluHepBJab;
                        general.learningAndDevelopment = bvm.benefits.general.learningAndDevelopment;
                        general.lifeAssurance = bvm.benefits.general.lifeAssurance;
                        general.personalDay = bvm.benefits.general.personalDay;
                        general.VoluntaryDay = bvm.benefits.general.VoluntaryDay;
                        general.ECU = bvm.benefits.general.ECU;
                        general.EnhancedFamilyLeave = bvm.benefits.general.EnhancedFamilyLeave;
                        general.PHI = bvm.benefits.general.PHI;
                        general.Other = bvm.benefits.general.Other;
                        general.Overtime = bvm.benefits.general.Overtime;
                        general.birthdayLeaveDate = bvm.benefits.general.birthdayLeaveDate;
                        general.carParkingDate = bvm.benefits.general.carParkingDate;
                        general.childCareVouchersDate = bvm.benefits.general.childCareVouchersDate;
                        general.employeeAssistanceDate = bvm.benefits.general.employeeAssistanceDate;
                        general.enhancedHolidayDate = bvm.benefits.general.enhancedHolidayDate;
                        general.enhancedSickPayDate = bvm.benefits.general.enhancedSickPayDate;
                        general.eyeCareAssistanceDate = bvm.benefits.general.eyeCareAssistanceDate;
                        general.firstAiderAllowanceDate = bvm.benefits.general.firstAiderAllowanceDate;
                        general.fluHepBJabDate = bvm.benefits.general.fluHepBJabDate;
                        general.learningAndDevelopmentDate = bvm.benefits.general.learningAndDevelopmentDate;
                        general.lifeAssuranceDate = bvm.benefits.general.lifeAssuranceDate;
                        general.personalDayDate = bvm.benefits.general.personalDayDate;
                        general.VoluntaryDayDate = bvm.benefits.general.VoluntaryDayDate;
                        general.callOutAllowanceDate = bvm.benefits.general.callOutAllowanceDate;
                        general.ECUDate = bvm.benefits.general.ECUDate;
                        general.EnhancedFamilyLeaveDate = bvm.benefits.general.EnhancedFamilyLeaveDate;
                        general.PHIDate = bvm.benefits.general.PHIDate;
                        general.OtherDate = bvm.benefits.general.OtherDate;
                        general.OvertimeDate = bvm.benefits.general.OvertimeDate;
                        bvm.benefits.general = general;
                    }
                }
                if (bvm.benefits.requestType == "health")
                {
                    if (bvm.benefits.health.fiscalYear == 0 || bvm.benefits.health.medicalOrgId == 0 || String.IsNullOrEmpty(bvm.benefits.health.effectiveFrom) || String.IsNullOrEmpty(bvm.benefits.health.endDate) || (bvm.benefits.health.isPrivateMedical == 1 && bvm.benefits.health.excessPaid <= 0))
                    {
                        return Json("\"" + "Please Select Required Fields" + "\"", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var health = new BenefitHealthViewModel();
                            health.benefitHealthId = bvm.benefits.health.benefitHealthId;
                            health.fiscalYear = bvm.benefits.health.fiscalYear;
                            health.employeeId = bvm.benefits.employeeId;
                            health.benefitId = bvm.benefits.benefitId;
                            health.effectiveFrom = bvm.benefits.health.effectiveFrom;
                            health.endDate = bvm.benefits.health.endDate;
                            health.excessPaid = bvm.benefits.health.excessPaid;
                            health.annualPremium = bvm.benefits.health.annualPremium;
                            health.membershipNumber = bvm.benefits.health.membershipNumber;
                            health.isPrivateMedical = bvm.benefits.health.isPrivateMedical;
                            health.medicalOrgId = bvm.benefits.health.medicalOrgId;
                            bvm.benefits.health = health;
                    }
                }
                if (bvm.benefits.requestType == "subscription")
                {
                    if (bvm.benefits.subscription.pSubscription == null || bvm.benefits.subscription.pSubscription == "")
                    {
                        return Json("\"" + "Please Select Required Fields" + "\"", JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        JavaScriptSerializer js = new JavaScriptSerializer();
                        SubscriptionViewModel[] list =
                            js.Deserialize<SubscriptionViewModel[]>(bvm.benefits.subscription.pSubscription);
                        List<BenefitSubscriptionViewModel> sublst = new List<BenefitSubscriptionViewModel>();
                        foreach (SubscriptionViewModel sub in list)
                        {
                            BenefitSubscriptionViewModel bSubscript = new BenefitSubscriptionViewModel();
                            bSubscript.employeeId = bvm.benefits.employeeId;
                            bSubscript.benefitId = bvm.benefits.benefitId;
                            bSubscript.fiscalYear = sub.year;
                            bSubscript.subscriptionTitle = sub.title;
                            bSubscript.subscriptionCost = sub.amount;
                            sublst.Add(bSubscript);
                        }
                        bvm.benefits.subscriptionlst = sublst;
                    }
                }
                benefitsDetailDR = Mapper.Map<BenefitsViewModel, BenefitsDataRequest>(bvm.benefits);
                benefitsDetailDR.requestType= bvm.benefits.requestType;
            }
            benefitsDetailDR.lastActionUser = GetUserIdFromSession();
            response = manager.addAmendBenefitsRecord(benefitsDetailDR);
            if (benefitId == 0)
            {
                var responseModel = getEmployeeBenefits();
                Session["Benefits"] = responseModel;
                benefitId = responseModel.benefits.benefitId;
            }
            return Json(response + "__benefitId__" + benefitId, JsonRequestBehavior.AllowGet);
        }
        public string SaveFile(HttpPostedFileBase file, int? employeeId)
        {
            string fileName = "";
            if (file != null && file.ContentLength > 0)
                try
                {
                    FileHelper.createDirectory(FileHelper.GetRefDocUploadPath() + "\\" + employeeId.ToString());
                    fileName = FileHelper.getUniqueFileName(file.FileName);
                    string path = Path.Combine(FileHelper.GetRefDocUploadPath() + "\\" + employeeId.ToString(), fileName);
                    file.SaveAs(path);
                    ViewBag.Message = "File uploaded successfully";
                }
                catch (Exception ex)
                {
                    ViewBag.Message = "ERROR:" + ex.Message.ToString();
                }
            else
            {
                ViewBag.Message = "You have not specified a file.";

            }
            return fileName;
        }
        public BenefitsResponseViewModel getEmployeeBenefits()
        {
            var benefitManager = new BenefitsManager();
            var benefitDataRequest = new BenefitsDataRequest();
            benefitDataRequest.lastActionUser = GetUserIdFromSession();
            if (RouteData.Values["id"] != null)
            {
                benefitDataRequest.employeeId = int.Parse(RouteData.Values["id"].ToString());
            }
            else
            {
                if (Session["employeeId"] != null)
                {
                    benefitDataRequest.employeeId = int.Parse(Session["employeeId"].ToString());
                }
            }
            var response = benefitManager.getEmployeeBenefits(benefitDataRequest);
            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };

            BenefitsResponseViewModel model = JsonConvert.DeserializeObject<BenefitsResponseViewModel>(response, settings);
            // BenefitsResponseViewModel model = new JavaScriptSerializer().Deserialize<BenefitsResponseViewModel>(response,);
            if (model == null)
            {
                model = new BenefitsResponseViewModel();
                model.benefits = new BenefitsViewModel();
            }
            if (model.benefits.vehiclelst == null)
                model.benefits.vehiclelst = new List<BenefitVehicleViewModel>();
            return model;
        }
        public JsonResult GetBenefitVehicle(int Id)
         {
            var benefitManager = new BenefitsManager();
           // var benefitDataRequest = new BenefitVehicleDataRequest();
          //  benefitDataRequest.benefitVehicleId = Id;
            var  vehicle = benefitManager.getBenefitVehicle(Id);
            return Json(vehicle, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetBenefitHealth(GeneralRequestViewModel data)
        {
            var benefitManager = new BenefitsManager();
            GeneralDataRequest general =   new  GeneralDataRequest();
            general = Mapper.Map<GeneralRequestViewModel, GeneralDataRequest>(data);
            var vehicle = benefitManager.getBenefitHealth(general);
            return Json(vehicle, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetBenefitPension(GeneralRequestViewModel data)
        {
            var benefitManager = new BenefitsManager();
            GeneralDataRequest general = new GeneralDataRequest();
            general = Mapper.Map<GeneralRequestViewModel, GeneralDataRequest>(data);
            var pension = benefitManager.getBenefitPension(general);
            return Json(pension, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetBenefitPRP(GeneralRequestViewModel data)
        {
            var benefitManager = new BenefitsManager();
            GeneralDataRequest general = new GeneralDataRequest();
            general = Mapper.Map<GeneralRequestViewModel, GeneralDataRequest>(data);
            var prp = benefitManager.getBenefitPRP(general);
            return Json(prp, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetBenefitGeneral(GeneralRequestViewModel data)
        {
            var benefitManager = new BenefitsManager();
            GeneralDataRequest general = new GeneralDataRequest();
            general = Mapper.Map<GeneralRequestViewModel, GeneralDataRequest>(data);
            var vehicle = benefitManager.getBenefitGeneral(general);
            return Json(vehicle, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetBenefitVehicles(GeneralRequestViewModel data)
        {
            var benefitManager = new BenefitsManager();
            GeneralDataRequest general = new GeneralDataRequest();
            general = Mapper.Map<GeneralRequestViewModel, GeneralDataRequest>(data);
           
            var vehicle = benefitManager.getBenefitVehicles(general);
            var model = new JavaScriptSerializer().Deserialize<List<BenefitVehicleViewModel>>(vehicle);
            return PartialView("~/Views/Employees/Benefits/_Vehicle.cshtml", model);
        }
        public JsonResult GetBenefitSubscription(GeneralRequestViewModel data)
        {
            var benefitManager = new BenefitsManager();
            GeneralDataRequest general = new GeneralDataRequest();
            general = Mapper.Map<GeneralRequestViewModel, GeneralDataRequest>(data);
            var vehicle = benefitManager.getBenefitSubscription(general);
            return Json(vehicle, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteV5Doc(BenefitsResponseViewModel bvm)
        {
            var response = "";
            var benefitManager = new BenefitsManager();
            var healthDataRequest = new BenefitsResponseViewModel();
            BenefitsDataRequest benefitsDetailDR = new BenefitsDataRequest();
            if (RouteData.Values["id"] != null)
            {
                bvm.benefits.employeeId = int.Parse(RouteData.Values["id"].ToString());
            }
            else
            {
                if (Session["employeeId"] != null)
                {
                    bvm.benefits.employeeId = int.Parse(Session["employeeId"].ToString());
                }
            }
            benefitsDetailDR = Mapper.Map<BenefitsViewModel, BenefitsDataRequest>(bvm.benefits);
            response = benefitManager.deleteV5Doc(benefitsDetailDR);
            return Json(response, JsonRequestBehavior.AllowGet);


        }
        public JsonResult GetCurrentFinancialYear(int Id)
        {
            var benefitManager = new BenefitsManager();
            var fData = benefitManager.getCurrentFinancialYear(Id);
            return Json(fData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult HealthBenefitHistory(int benefitHealthId = 0)
        {
            var benefitManager = new BenefitsManager();
            var model = new BenefitHealthHistoryViewModel();
            var response = benefitManager.getBenefitsHealthHistory(benefitHealthId);
            if (response != "[]")
            {
                model.history = new JavaScriptSerializer().Deserialize<List<BenefitHealthHistory>>(response);
            }
            model.benefitHealthId = benefitHealthId;
            return PartialView("~/Views/Employees/Benefits/_ViewHistoryHealth.cshtml", model);
        }

    }
}