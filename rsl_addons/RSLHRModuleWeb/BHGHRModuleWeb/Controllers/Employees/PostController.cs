﻿using AutoMapper;
using BHGHRModuleWeb.ViewModels;
using BusinessModule.NetworkManagers;
using BusinessModule.NetworkObjects;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using BHGHRModuleWeb.Helpers;
using System.Globalization;

namespace BHGHRModuleWeb.Controllers.Employees
{
    public class PostController : ABaseController
    {

        #region >>> Helpers <<<
        private DateTime GetDateTimeFromString(string dt)
        {
            string[] formats = { "dd/MM/yyyy" };
            var dateTime = DateTime.ParseExact(dt, formats, new CultureInfo("en-US"), DateTimeStyles.None);
            return dateTime;
        }

        public JsonResult JobRoleSelectListJson(string Origin, string Target, string Value)
        {
            var manager = new JobDetailManager();
            var request = new GetJobRolesForTeamDataRequest();
            request.teamId = int.Parse(Value);
            var response = manager.getJobRolesForTeamId(request);
            var model = new JavaScriptSerializer().Deserialize<List<LookUpFields>>(response);

            var jobDetail_jobRole = new List<SelectListItem>();
            jobDetail_jobRole = model.Select(e => new SelectListItem() { Text = e.lookUpDescription, Value = e.lookUpId.ToString() }).ToList();
            jobDetail_jobRole.Add(new SelectListItem() { Text = "Please Select Job Role", Value = "0", Selected = true });

            return Json(jobDetail_jobRole);
        }

        #endregion

        #region >>> Action Methods <<<

        // GET: Job Detail
        public ActionResult Index(int id = 0)
        {
            var manager = new JobDetailManager();
            var request = new GetPersonalDetailDataRequest();
            PostViewModel model = new PostViewModel();
            request.employeeId = id;
            if (id != 0)
            {
                ViewBag.isEdit = "edit";
            }
            else
            {
                ViewBag.isEdit = "create";
                if (Session["employeeId"] != null)
                {
                    request.employeeId = int.Parse(Session["employeeId"].ToString());
                }
            }

            if (id == 0)
            {
                var newJobDetail = "";

                JobDetailDataRequest jobDetailDR = new JobDetailDataRequest();
                // auto mapper to map view model value to network object.
                model.jobDetail.employeeId = request.employeeId;
                jobDetailDR = Mapper.Map<JobDetailViewModel, JobDetailDataRequest>(model.jobDetail);
                jobDetailDR.team = model.team;
                jobDetailDR.jobRoleId = model.jobRoleId;
                jobDetailDR.startDate = model.jobDetail.startDate;
                newJobDetail = manager.saveJobDetail(jobDetailDR);
            }

            var response = manager.getJobDetail(request);
            
            model = new JavaScriptSerializer().Deserialize<PostViewModel>(response);
            if (model.jobDetail == null)
            {
                model.jobDetail = new JobDetailViewModel();
            }

            if (!string.IsNullOrEmpty(model.jobDetail.startDate) && string.IsNullOrEmpty(model.jobDetail.reviewDate))
            {
                var defaultMonthsForReviewDate = 6;
                var tempDate = GetDateTimeFromString(model.jobDetail.startDate);
                model.jobDetail.reviewDate = tempDate.AddMonths(defaultMonthsForReviewDate).ToShortDateString();
            }

            model.jobDetail._bankHoliday = model.jobDetail.bankHoliday;

            if (model.jobDetail.holidayIndicator.HasValue)
            {
                if (model.jobDetail.holidayIndicator.Value == 2)
                {
                    if (model.jobDetail.isBRS.HasValue)
                    {
                        if (model.jobDetail.isBRS == 1)
                        {
                            model.jobDetail.bankHoliday = model.jobDetail.bankHoliday * 8;
                        }

                        if (model.jobDetail.isBRS == 0)
                        {
                            model.jobDetail.bankHoliday = model.jobDetail.bankHoliday * 7.4;
                        }
                    }
                }
            }

            //TODO:
            model.jobDetail.employeeId = request.employeeId;
            model.team = model.jobDetail.team;
            model.jobRoleId = model.jobDetail.jobRoleId;
            model.jobDetail.teams = model.teams;
            model.jobDetail.jobRole = model.jobRole;
            if (model.jobDetail.workingPattern.workingHours.Count() == 0)
            {
                model.jobDetail.workingPattern.workingHours.Add(new WorkingHoursHistoryViewModel());
            }
            model.jobDetail.noOfWeeks = model.jobDetail.workingPattern.workingHours.Count();

            if (model.jobDetail.noOfWeeks > 1)
            {
                for (int i = 0; i < model.jobDetail.workingPattern.workingHours.Count(); i++)
                {
                    if ((i+1) < model.jobDetail.workingPattern.workingHours.Count())
                    {
                        var startdate = GetDateTimeFromString(model.jobDetail.workingPattern.workingHours[i + 1].startDate);
                        model.jobDetail.workingPattern.workingHours[i].endDate = startdate.AddDays(-1).ToShortDateString();
                    }
                }
            }

            ViewBag.IsHR = IsHR();
           
            return PartialView("~/Views/Employees/Post/_Post.cshtml", model);
        }

        public ActionResult JobRoleHistory(int id = 0)
        {
            var manager = new JobDetailManager();
            var request = new GetPersonalDetailDataRequest();
            var model = new JobRoleHistoryViewModel();
            request.employeeId = id;
            var response = manager.getJobRoleHistory(request);
            if (response != "[]")
            {
                model.history = new JavaScriptSerializer().Deserialize<List<HistoryViewModel>>(response);
            }
            model.employeeId = id;
            return PartialView("~/Views/Employees/Post/_ViewHistory.cshtml", model);
        }

        public ActionResult WorkingHours(int id = 0, string requestType = "Edit")
        {
            var manager = new JobDetailManager();
            var request = new GetPersonalDetailDataRequest();
            var model = new WorkingHoursViewModel();
            request.employeeId = id;
            var response = manager.getWorkingHours(request);
            if (response != "[]")
            {
                model = new JavaScriptSerializer().Deserialize<WorkingHoursViewModel>(response);
                //if (model.workingHours == null)
                //{
                //    model.workingHours = new WorkingHoursHistoryViewModel() { employeeId = id };
                //    model.workingHours.isEmpty = true;
                //    model.workingHours.startDate = DateTime.Now;
                //}
            }
            model.employeeId = id;
            model.requestType = requestType;
            return PartialView("~/Views/Employees/Post/_WorkingHours.cshtml", model);
        }
        public ActionResult DaysTimings(int id = 0,int weekId=0,string day="",int weekNum=0)
        {
            var manager = new JobDetailManager();
            var request = new GetDayTimingDataRequest();
            var model = new DayTimingViewModel();
            request.employeeId = id;
            request.wId = weekId;
            request.day = day;
            var response = manager.getDaysTimings(request);
            if (response != "[]" && response!=null && response != "null" && response != "")
            {
                model = new JavaScriptSerializer().Deserialize<DayTimingViewModel>(response);
            }
            model.employeeId = id;
            model.day = day;
            model.weekNumber = weekNum;
            if (string.IsNullOrEmpty(model.StartTime) || string.IsNullOrEmpty(model.EndTime))
            {
                if (model.day.Equals("sat") || model.day.Equals("sun"))
                {
                    model.StartTime = "00:00";
                    model.EndTime = "00:00";
                    model.LunchTimeInMinutes = "0";
                    model.duration = 0;
                }
                else
                {
                    model.StartTime = "09:00";
                    model.EndTime = "17:30";
                    model.LunchTimeInMinutes = "60";
                    model.duration = 7.5;
                }
            }
            
            return PartialView("~/Views/Employees/Post/_DaysTimings.cshtml", model);
        }

        public ActionResult ExportData(int id = 0)
        {
            var manager = new JobDetailManager();
            var request = new GetPersonalDetailDataRequest();
            var model = new JobRoleHistoryViewModel();
            request.employeeId = id;
            var response = manager.getJobRoleHistory(request);
            if (response != "[]")
            {
                model.history = new JavaScriptSerializer().Deserialize<List<HistoryViewModel>>(response);

                model.employeeId = id;
                var fileName = "JobRoleAdministration_{0}_{1}_{2}.xls";
                fileName = string.Format(fileName, DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
                ExportXLS(model.history.Select(e => new { Date = e.date, Team = e.team, JobRole = e.jobRole, CreatedBy = e.createdby }).ToList(), fileName);
                return Json(true, JsonRequestBehavior.AllowGet);
            }

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Save(PostViewModel model)
        {
            var response = "";

            JobDetailDataRequest jobDetailDR = new JobDetailDataRequest();
            // auto mapper to map view model value to network object.
            var manager = new JobDetailManager();

            jobDetailDR = Mapper.Map<JobDetailViewModel, JobDetailDataRequest>(model.jobDetail);
            jobDetailDR.team = model.team;
            jobDetailDR.jobRoleId = model.jobRoleId;
            jobDetailDR.startDate = model.jobDetail.startDate;
            jobDetailDR.lastActionUser = GetUserIdFromSession();
            response = manager.saveJobDetail(jobDetailDR);

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveWorkingHours(WorkingHoursViewModel model)
        {
            var response = "";
            EmployeeWorkingHoursDataRequest workingHoursDR = new EmployeeWorkingHoursDataRequest();
            // auto mapper to map view model value to network object.
            var manager = new JobDetailManager();

            workingHoursDR = Mapper.Map<WorkingHoursViewModel, EmployeeWorkingHoursDataRequest>(model);
            foreach (var item in workingHoursDR.workingHours)
            {
                item.startDate = item.startDate;
                item.createdBy = GetUserIdFromSession();
                item.createdOn = DateTime.Now;
                item.lastActionUser = GetUserIdFromSession();
            }
            
            response = manager.saveWorkingHours(workingHoursDR);
            var request = new GetPersonalDetailDataRequest();
            var postViewModel = new PostViewModel();
            request.employeeId = model.employeeId;

            var Postresponse = manager.getJobDetail(request);

            postViewModel = new JavaScriptSerializer().Deserialize<PostViewModel>(Postresponse);

            if (postViewModel.jobDetail == null)
            {
                postViewModel.jobDetail = new JobDetailViewModel();
            }

            postViewModel.jobDetail.noOfWeeks = postViewModel.jobDetail.workingPattern.workingHours.Count();
            if (postViewModel.jobDetail.noOfWeeks > 1)
            {
                for (int i = 0; i < postViewModel.jobDetail.workingPattern.workingHours.Count(); i++)
                {
                    if ((i + 1) < postViewModel.jobDetail.workingPattern.workingHours.Count())
                    {
                        var startdate = GetDateTimeFromString(postViewModel.jobDetail.workingPattern.workingHours[i + 1].startDate);
                        postViewModel.jobDetail.workingPattern.workingHours[i].endDate = startdate.AddDays(-1).ToShortDateString();
                    }
                }
            }

            ViewBag.IsHR = IsHR();
            
            return PartialView("~/Views/Employees/Post/_Post.cshtml", postViewModel);
            //if (response != "[]")
            //{
            //    responseModel = new JavaScriptSerializer().Deserialize<WorkingHoursViewModel>(response);
            //    responseModel.employeeId = workingHoursDR.employeeId;
            //    var request = new GetPersonalDetailDataRequest();
            //    request.employeeId = model.employeeId;
            //    response = manager.getWorkingHours(request);
            //    if (response != "[]")
            //    {
            //        responseModel = new JavaScriptSerializer().Deserialize<WorkingHoursViewModel>(response);
            //        if (responseModel.workingHours == null)
            //        {
            //            responseModel.workingHours = new List<WorkingHoursHistoryViewModel>();
            //        }
            //    }
            //}
            //return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UploadRoleFile(HttpPostedFileBase roleFileUrl, PostViewModel model)
        {
            var response = "";
            var uploadRoleFileDR = new UploadRoleFileDataRequest();
            var manager = new JobDetailManager();

            if (roleFileUrl != null && roleFileUrl.ContentLength > 0)
                try
                {
                    FileHelper.createDirectory(FileHelper.GetRoleFileUploadPath());
                    string fileName = FileHelper.getUniqueFileName(roleFileUrl.FileName);
                    string path = Path.Combine(FileHelper.GetRoleFileUploadPath(), fileName);
                    roleFileUrl.SaveAs(path);
                    uploadRoleFileDR.employeeId = model.jobDetail.employeeId;
                    uploadRoleFileDR.rolePath = fileName;
                    if (!string.IsNullOrEmpty(model.jobDetail.employeeContract))
                    {
                        uploadRoleFileDR.filePath = FileHelper.getFileNameFromPath(model.jobDetail.employeeContract);
                    }

                    response = manager.updateEmployeeContractRoleDocument(uploadRoleFileDR);

                    if (response.Equals("\"Document is added successfully.\""))
                    {
                        var imagePath = "https://" + Request.Url.Authority + "/RoleFile/" + fileName;

                        response = response + " __imagePath__" + imagePath;
                    }
                }
                catch (Exception ex)
                {
                    ViewBag.Message = "ERROR:" + ex.Message.ToString();
                }
            else
            {
                ViewBag.Message = "You have not specified a file.";
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UploadEmploymentContract(HttpPostedFileBase employmentContractUrl, PostViewModel model)
        {
            var response = "";
            var uploadEmploymentContractDR = new UploadRoleFileDataRequest();
            var manager = new JobDetailManager();

            if (employmentContractUrl != null && employmentContractUrl.ContentLength > 0)
                try
                {
                    FileHelper.createDirectory(FileHelper.GetEmploymentContractUploadPath());
                    string fileName = FileHelper.getUniqueFileName(employmentContractUrl.FileName);
                    string path = Path.Combine(FileHelper.GetEmploymentContractUploadPath(), fileName);
                    employmentContractUrl.SaveAs(path);
                    uploadEmploymentContractDR.employeeId = model.jobDetail.employeeId;
                    uploadEmploymentContractDR.filePath = fileName;
                    if (!string.IsNullOrEmpty(model.jobDetail.roleFile))
                    {
                        uploadEmploymentContractDR.rolePath = FileHelper.getFileNameFromPath(model.jobDetail.roleFile);
                    }
                    uploadEmploymentContractDR.rolePath = FileHelper.getFileNameFromPath(model.jobDetail.roleFile);

                    response = manager.updateEmployeeContractRoleDocument(uploadEmploymentContractDR);

                    if (response.Equals("\"Document is added successfully.\""))
                    {
                        var imagePath = "https://"+ Request.Url.Authority + "/RoleFile/EmploymentContract/" + fileName;

                        response = response + " __imagePath__" + imagePath;
                    }
                }
                catch (Exception ex)
                {
                    ViewBag.Message = "ERROR:" + ex.Message.ToString();
                }
            else
            {
                ViewBag.Message = "You have not specified a file.";
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UploadSalaryLetter(HttpPostedFileBase salaryLetterUrl, PostViewModel model)
        {
            var response = "";
            var uploadSalaryLetterDR = new SalaryAmmendmentDataRequest();
            var manager = new JobDetailManager();

            if (salaryLetterUrl != null && salaryLetterUrl.ContentLength > 0)
                try
                {
                    FileHelper.createDirectory(FileHelper.GetSalaryLetterUploadPath());
                    string fileName = FileHelper.getUniqueFileName(salaryLetterUrl.FileName);
                    string path = Path.Combine(FileHelper.GetSalaryLetterUploadPath(), fileName);
                    salaryLetterUrl.SaveAs(path);
                    uploadSalaryLetterDR.employeeId = model.jobDetail.employeeId;
                    uploadSalaryLetterDR.salaryAmendmentId = 0;
                    uploadSalaryLetterDR.fileName = fileName;

                    response = manager.uploadSalaryLetter(uploadSalaryLetterDR);

                    if (response.Equals("\"Salary Amendment is added successfully.\""))
                    {
                        var imagePath = "";
                        var request = new GetPersonalDetailDataRequest();
                        request.employeeId = model.jobDetail.employeeId;

                        var responseJob = manager.getJobDetail(request);
                        PostViewModel modelPost = new JavaScriptSerializer().Deserialize<PostViewModel>(responseJob);
                        if (modelPost.jobDetail != null && modelPost.jobDetail.saleryAmendments != null)
                        {
                            imagePath = "<div class='padding-bottom-5'></div>";
                            foreach (var item in modelPost.jobDetail.saleryAmendments)
                            {
                                imagePath = imagePath + "<div class='row row-" + item.salaryAmendmentId + "'>";
                                imagePath = imagePath + "<div class='col-md-12'><div class='col-md-9'>";
                                if (item.fileName.Length > 35)
                                {
                                    imagePath = imagePath + "<a href = " + item.filePath + "/" + item.fileName + " target='_blank' title=" + item.fileName + ">" + item.fileName.Substring(0, 35) + "...</a>";
                                }
                                else
                                {
                                    imagePath = imagePath + "<a href = " + item.filePath +"/"+ item.fileName + " target='_blank' title="+ item.fileName+">" + item.fileName + "</a>";
                                }
                                imagePath = imagePath + "</div><div class='col-md-2'>";
                                imagePath = imagePath + " <a href = 'JavaScript://' onclick='RemoveSalaryAmendment(" + item.salaryAmendmentId + ")' style='text-decoration: none;color:Red; text-align:right;'>Remove</a>";
                                imagePath = imagePath + "</div></div></div>";
                             }
                        }
                        response = response + " __imagePath__" + imagePath;
                    }
                }
                catch (Exception ex)
                {
                    ViewBag.Message = "ERROR:" + ex.Message.ToString();
                }
            else
            {
                ViewBag.Message = "You have not specified a file.";
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RemoveSalaryLetter(int id = 0)
        {
            var response = "";
            var manager = new JobDetailManager();
            response = manager.removeSalaryLetter(id);

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetTeamDirector(int id = 0)
        {
            var response = "";
            var manager = new JobDetailManager();
            var request = new GetJobRolesForTeamDataRequest();
            request.teamId = id;
            response = manager.GetTeamDirector(request);

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetHolidayRule(int id = 0)
        {
            var response = "";
            var manager = new JobDetailManager();
            var request = new HolidayRuleDataRequest();
            request.eid = id;
            response = manager.GetHolidayRule(request);

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetGradePointLookup(int id = 0, int isBRS = 0)
        {
            var response = "";
            var manager = new JobDetailManager();
            var request = new GradePointLookupDataRequest();
            request.eid = id;
            request.isBRS = isBRS;
            response = manager.GetGradePointLookup(request);

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetJobDetailNote(int id = 0)
        {
            var manager = new JobDetailManager();
            var request = new GetPersonalDetailDataRequest();
            HrNoteViewModel model = new HrNoteViewModel();
            request.employeeId = id;
            var response = manager.getJobDetailNote(request);

            if (response != "[]")
            {
                model.notes = new JavaScriptSerializer().Deserialize<List<JobDetailNoteViewModel>>(response);
            }

            return PartialView("~/Views/Employees/Post/_JobDetailNote.cshtml", model);
        }

        [HttpPost]
        public ActionResult SaveJobDetailNote(JobDetailNoteViewModel model)
        {
            var response = "";
            PostViewModel responseModel = new PostViewModel();
            //List<JobDetailNoteDataRequest> responseModel = new List<JobDetailNoteDataRequest>();
            JobDetailNoteDataRequest workingHoursDR = new JobDetailNoteDataRequest();
            // auto mapper to map view model value to network object.
            var manager = new JobDetailManager();
            model.createdBy = GetUserIdFromSession();
            model.createdDate = DateTime.Now.ToString();
            model._createdDate = DateTime.Now;
            workingHoursDR = Mapper.Map<JobDetailNoteViewModel, JobDetailNoteDataRequest>(model);
            response = manager.saveJobDetailNote(workingHoursDR);

            responseModel.jobDetail.notes = new JavaScriptSerializer().Deserialize<List<JobDetailNoteViewModel>>(response);
            responseModel.jobDetail.noteCount = responseModel.jobDetail.notes.Count;
            return PartialView("~/Views/Employees/Post/_JobDetailNote.cshtml", responseModel);
        }

        #endregion
    }
}