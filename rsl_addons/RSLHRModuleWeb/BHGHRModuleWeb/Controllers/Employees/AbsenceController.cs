﻿using BHGHRModuleWeb.ViewModels;
using BusinessModule.NetworkManagers;
using BusinessModule.NetworkObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace BHGHRModuleWeb.Controllers.Employees
{
    public class AbsenceController : ABaseController
    {

        #region >>> Constants <<<

        public const string _toilRecorded = "BRS TOIL Recorded";
        public const string _toilInLieu = "BRS Time Off in Lieu";
        public const string _toilAvailableFailure = "\"You cannot add greater than your toil limit.\"";
        public const string _annualAvailableFailure = "\"Your requested leave duration exceeds your remaining leave quota of {0}. If you have any concerns, please discuss them with your manager.\"";
        

        #endregion

        #region >>> Helpers <<<

        private bool IsToilAvailable(double count)
        {
            var toilDuration = 0.0;
            if (TempData.ContainsKey("ToilAvailable"))
                toilDuration = double.Parse(TempData["ToilAvailable"].ToString());
            TempData.Keep("ToilAvailable");

            if (count > toilDuration)
            {
                return false;
            }

            return true;
        }

        private bool IsAnnualAvailable(double count)
        {
            var annualDuration = 0.0;
            if (TempData.ContainsKey("AnnualAvailable"))
                annualDuration = double.Parse(TempData["AnnualAvailable"].ToString());
            TempData.Keep("AnnualAvailable");

            if (count > annualDuration)
            {
                return false;
            }

            return true;
        }

        private string SaveLeave(LeaveEditViewModel model, LeaveType leaveType)
        {
            var manager = new AbsenceManager();
            var request = new SaveAbsenceDataRequest();
            var response = "";

            request.employeeId = model.employeeId;
            request.startDate = model.start;
            request.endDate = model.end;
            request.duration = model.duration;
            request.notes = model.notes;
            request.holType = model.holtype;
            request.from = model.from;
            request.to = model.to;
            request.title = model.title;
            request.recordedBy = GetUserIdFromSession();
            // leave of absence 
            request.natureId = model.nature;
            // sickness 
            request.reasonId = model.reason;
            if (leaveType.Equals(LeaveType.Sickness))
            {
                if (model.anticipated!=null && model.anticipated!="")
                {
                    request.anticipatedReturnDate = model.anticipated;
                }
            }

            // Toil
            // HR and Line manager can add quota for employee in toil.
            if (IsTeam())
            {
                request.leaveType = _toilRecorded;
            }
            else
            {
                if (Session["Module"] != null && Session["Module"].ToString() == "MyJob")
                {
                    request.leaveType = _toilInLieu;
                }
                else
                {
                    request.leaveType = _toilRecorded;
                }
            }
            
            response = manager.SaveLeave(request, leaveType);

            if (response.Contains("isSuccessFul"))
            {
                var responseModel = new JavaScriptSerializer().Deserialize<FailerResponseViewModel>(response);

                if (responseModel.isSuccessFul == false)
                {
                    return "__failure__" + responseModel.message;
                }
            }

            if (response == "\"You have already booked the date(s).\"")
            {
                return "__failure__" + response;
            }
            if (response == "\"You have already booked the date(s) for Sickness Leave(s).\"")
            {
                return "__failure__" + response;
            }
            if (response.Contains("Please enter a return date smaller than the start date of the current non returned leave."))
            {
                return "__failure__" + response;
            }
            return response;
        }

        private FailerResponseViewModel SaveLeaveStatus(MyTeamLeavesResponseViewModel model, LeaveType leaveType)
        {
            var manager = new AbsenceManager();
            var request = new LeaveStatusChangeRequest();
            var leave = model.myTeamLeavesPending[0];
            var response = "";
            request.absenceHistoryId = leave.absenceHistoryId.Value;
            request.actionBy = GetUserIdFromSession();
            request.actionId = leave.statusId;
            request.notes = leave.notes;

            response = manager.ChangeAbsenceStatus(request, leaveType);

            var responseModel = new JavaScriptSerializer().Deserialize<FailerResponseViewModel>(response);

            return responseModel;
        }

        private FailerResponseViewModel SaveSicknessStatus(PendingSicknessViewModel model)
        {
            var manager = new AbsenceManager();
            var request = new LeaveSicknessStatusChangeRequest();
            var response = "";
            request.absenceHistoryId = model.absenceHistoryId.Value;
            request.actionBy = GetUserIdFromSession();
            request.actionId = model.statusId ?? model.statusId.Value;
            request.certNo = model.certNo;
            request.drName = model.drName;
            request.notes = model.notes;
            request.reasonId = model.reasonId;
            request.startDate = model.start;
            request.endDate = model.end;
            request.duration = model.absentDays;
            request.notes = model.notes;
            request.holtype = model.holtype;
            if (model.anticipated != null && model.anticipated != "")
            {
                request.anticipatedReturnDate = model.anticipated;
            }

            response = manager.ChangeSicknessStatus(request);
            var responseModel = new JavaScriptSerializer().Deserialize<FailerResponseViewModel>(response);
            if (responseModel.isSuccessFul == false)
            {
                responseModel.message= "__failure__" + responseModel.message;
            }
            return responseModel;
        }

        private FailerResponseViewModel SaveInternalStatus(PendingInternalLeaveViewModel model)
        {
            var manager = new AbsenceManager();
            var request = new InternalLeaveStatusChangeRequest();
            var response = "";
            request.absenceHistoryId = model.absenceHistoryId.Value;
            request.actionBy = GetUserIdFromSession();
            request.actionId = model.statusId;
            request.startDate = model.startDate.Value.ToShortDateString();
            request.endDate = model.endDate.Value.ToShortDateString();
            request.duration = model.duration.Value;
            request.from = model.from;
            request.to = model.to;
            request.title = model.title;
            request.notes = model.notes;

            response = manager.ChangeInternalStatus(request);
            var responseModel = new JavaScriptSerializer().Deserialize<FailerResponseViewModel>(response);
            if (responseModel.isSuccessFul == false)
            {
                responseModel.message = responseModel.message;
            }
            return responseModel;
        }

        private AbsenceViewModel InternalMeetingList (int id)
        {
            var manager = new AbsenceManager();
            var request = new AbsenceDataRequest();
            var model = new AbsenceViewModel();

            model.employeeId = request.employeeId = id;
            model.internalMeetingsList.employeeId = model.employeeId;
            model.internalMeetingsList.isTeam = IsTeam();
            model.internalMeetingsList.isHr = IsHR();
            model.internalMeetingsList.currentlyLoggedEmployeeId = GetUserIdFromSession();
            var responseInternalMeeting = manager.GetInternalMeetingAbsence(request);

            model.internalMeetingsList.absentees = new JavaScriptSerializer().Deserialize<List<AbsenteeViewModel>>(responseInternalMeeting);

            if (model.internalMeetingsList.absentees == null)
            {
                model.internalMeetingsList.absentees = new List<AbsenteeViewModel>();
            }

            return model;
        }

        private string GetUnit()
        {
            return Session["__unit__"].ToString();
        }

        private void SetUnit(string unit)
        {
            Session["__unit__"] = unit;
        }

        #endregion

        #region >>> Action Methods <<<

        // GET: Absence
        public ActionResult Index(int id = 0)
        {
            var manager = new AbsenceManager();
            var request = new AbsenceDataRequest();
            var model = new AbsenceViewModel();
            model.employeeId = request.employeeId = id;
          
            var responseAnnualLeave = manager.GetAnnualLeaveDetail(request);
            var responseSickness = manager.GetSicknessAbsence(request);
            var responseAbsence = manager.GetLeaveOfAbsence(request);
            var responseToil = manager.GetToilAbsence(request);
            var responseInternalMeeting = manager.GetInternalMeetingAbsence(request);

            model.annualList = new JavaScriptSerializer().Deserialize<AnnualLeaveListViewModel>(responseAnnualLeave);
            model.sicknessList = new JavaScriptSerializer().Deserialize<SicknessListViewModel>(responseSickness);
            model.absenceList = new JavaScriptSerializer().Deserialize<AbsenceListViewModel>(responseAbsence);
            model.toilList = new JavaScriptSerializer().Deserialize<ToilListViewModel>(responseToil);
            model.internalMeetingsList.absentees = new JavaScriptSerializer().Deserialize<List<AbsenteeViewModel>>(responseInternalMeeting);
            
            //model.maternityPaternityList.absentees = model.absenceList.absentees.Where(e => e.nature == "Paternity" || e.nature == "Maternity" || e.nature == "Maternity - Add").ToList();
            //model.internalMeetingsList.absentees = model.absenceList.absentees.Where(e => e.nature == "Internal Meeting").ToList();
            model.absenceList.absentees.RemoveAll(e => e.nature == "Paternity" || 
                                                       e.nature == "Maternity" || 
                                                       e.nature == "Maternity - Add" || 
                                                       e.nature == "Internal Meeting");
            model.absenceList.leavesNatures.RemoveAll(e => e.lookUpDescription == "Maternity - Add");

            if (model.annualList == null)
            {
                model.annualList = new AnnualLeaveListViewModel();
            }
            else
            {
                TempData["ToilAvailable"] = model.annualList.leaveDetail.timeOfInLieuOwed;
                TempData["AnnualAvailable"] = model.annualList.leaveDetail.leavesRemaining;
                if (model.annualList.workingPattren.workingHours != null)
                {
                    model.annualList.workingPattren.unit = model.annualList.leaveDetail.unit;
                    SetUnit(model.annualList.leaveDetail.unit);
                }
                Session["__WorkingPattern__"] = model.annualList.workingPattren.workingHours;
            }

            if (model.annualList != null)
            {
                model.annualList.isTeam = IsTeam();
                model.annualList.isHr = IsHR();
                model.annualList.currentlyLoggedEmployeeId = GetUserIdFromSession();
                model.annualList.employeeId = request.employeeId;
                //model.fiscalYearLookUp 

                if (!string.IsNullOrEmpty(model.annualList.leaveYearStart))
                {
                    var list = new List<SelectListItem>();
                    var startDate = GetDateTimeFromString(model.annualList.leaveYearStart);
                    var endDate = GetDateTimeFromString(model.annualList.leaveYearEnd);

                    do
                    {
                        
                        list.Add(new SelectListItem() { Text = startDate.ToShortDateString() + " - " + endDate.ToShortDateString(), Value = startDate.ToShortDateString() });

                        startDate = startDate.AddYears(-1);
                        endDate = endDate.AddYears(-1);

                    } while (endDate.Year >= 2004);

                    list.Insert(0, new SelectListItem()
                    {
                        Text = "Select Year",
                        Value = "0"
                    });
                    model.fiscalYearLookUp = list;
                }
                else
                {
                    model.fiscalYearLookUp = new List<SelectListItem>() { new SelectListItem() { Text = "Select Year", Value = "0" } };
                }
               
                //Session["__LeaveYearStart__"] = model.annualList.leaveYearStart;
                //Session["__LeaveYearEnd__"] = model.annualList.leaveYearEnd;
            }

            if (model.sicknessList != null)
            {
                Session["absenceReasons"] = model.sicknessList.absenceReasons;
                model.sicknessList.isTeam = IsTeam();
                model.sicknessList.isHR = IsHR();
                model.sicknessList.employeeId = request.employeeId;
            }

            if (model.absenceList != null)
            {
                Session["bankHolidays"] = string.Join(",", model.absenceList.bankHolidays);
                Session["leavsNatures"] = model.absenceList.leavesNatures;
                model.absenceList.isTeam = IsTeam();
                model.absenceList.isHr = IsHR();
                model.absenceList.currentlyLoggedEmployeeId = GetUserIdFromSession();
                model.absenceList.employeeId = request.employeeId;
            }

            if (model.toilList != null)
            {
                model.toilList.isTeam = IsTeam();
                model.toilList.employeeId = request.employeeId;
            }
            else
            {
                model.toilList = new ToilListViewModel();
            }

            if (model.internalMeetingsList != null)
            {
                model.internalMeetingsList.isTeam = IsTeam();
                model.internalMeetingsList.isHr = IsHR();
                model.internalMeetingsList.currentlyLoggedEmployeeId = GetUserIdFromSession();
                model.internalMeetingsList.employeeId = request.employeeId;
            }

            model.isManager = (bool)Session["IsManager"];
            if (model.sicknessList == null)
            {
                model.sicknessList = new SicknessListViewModel();
            }
            return PartialView("~/Views/Employees/Absence/_Absence.cshtml", model);
        }

        [HttpPost]
        public ActionResult Index(AbsenceViewModel model)
        {
            var manager = new AbsenceManager();
            var request = new AbsenceDataRequest();
            model.employeeId = request.employeeId = model.employeeId;
            request.fiscalYearDate = model.fiscalYearId;

            var responseAnnualLeave = manager.GetAnnualLeaveDetail(request);
            var responseSickness = manager.GetSicknessAbsence(request);
            var responseAbsence = manager.GetLeaveOfAbsence(request);
            var responseToil = manager.GetToilAbsence(request);
            var responseInternalMeeting = manager.GetInternalMeetingAbsence(request);

            model.annualList = new JavaScriptSerializer().Deserialize<AnnualLeaveListViewModel>(responseAnnualLeave);
            model.sicknessList = new JavaScriptSerializer().Deserialize<SicknessListViewModel>(responseSickness);
            model.absenceList = new JavaScriptSerializer().Deserialize<AbsenceListViewModel>(responseAbsence);
            model.toilList = new JavaScriptSerializer().Deserialize<ToilListViewModel>(responseToil);
            model.internalMeetingsList.absentees = new JavaScriptSerializer().Deserialize<List<AbsenteeViewModel>>(responseInternalMeeting);

            //model.maternityPaternityList.absentees = model.absenceList.absentees.Where(e => e.nature == "Paternity" || e.nature == "Maternity" || e.nature == "Maternity - Add").ToList();
            //model.internalMeetingsList.absentees = model.absenceList.absentees.Where(e => e.nature == "Internal Meeting").ToList();

            if (model.annualList == null)
            {
                model.annualList = new AnnualLeaveListViewModel();
            }
            else
            {
                TempData["ToilAvailable"] = model.annualList.leaveDetail.timeOfInLieuOwed;
                TempData["AnnualAvailable"] = model.annualList.leaveDetail.leavesRemaining;
                if (model.annualList.workingPattren.workingHours != null)
                {
                    model.annualList.workingPattren.unit = model.annualList.leaveDetail.unit;
                    SetUnit(model.annualList.leaveDetail.unit);
                }
                Session["__WorkingPattern__"] = model.annualList.workingPattren.workingHours;
            }

            if (model.annualList != null)
            {
                model.annualList.isTeam = IsTeam();
                model.annualList.isHr = IsHR();
                model.annualList.currentlyLoggedEmployeeId = GetUserIdFromSession();
                model.annualList.employeeId = request.employeeId;
                //model.fiscalYearLookUp 

                if (!string.IsNullOrEmpty(model.annualList.leaveYearStart))
                {
                    var list = new List<SelectListItem>();
                    var startDate = GetDateTimeFromString(model.annualList.leaveYearStart);
                    var endDate = GetDateTimeFromString(model.annualList.leaveYearEnd);

                    do
                    {
                        list.Add(new SelectListItem() { Text = startDate.ToShortDateString() + " - " + endDate.ToShortDateString(), Value = startDate.ToShortDateString() });

                        startDate = startDate.AddYears(-1);
                        endDate = endDate.AddYears(-1);

                    } while (endDate.Year >= 2004);

                    model.fiscalYearLookUp = list;
                }
                else
                {
                    model.fiscalYearLookUp = new List<SelectListItem>() { new SelectListItem() { Text = "Select Year", Value = "0" } };
                }
            }
            if (model.sicknessList != null)
            {
                Session["absenceReasons"] = model.sicknessList.absenceReasons;
                model.sicknessList.isTeam = IsTeam();
                model.sicknessList.isHR = IsHR();
                model.sicknessList.employeeId = request.employeeId;
            }

            if (model.absenceList != null)
            {
                model.absenceList.absentees.RemoveAll(e => e.nature == "Paternity" ||
                                                      e.nature == "Maternity" ||
                                                      e.nature == "Maternity - Add" ||
                                                      e.nature == "Internal Meeting");
                model.absenceList.leavesNatures.RemoveAll(e => e.lookUpDescription == "Maternity - Add");

                Session["bankHolidays"] = string.Join(",", model.absenceList.bankHolidays);
                Session["leavsNatures"] = model.absenceList.leavesNatures;
                model.absenceList.isTeam = IsTeam();
                model.absenceList.isHr = IsHR();
                model.absenceList.currentlyLoggedEmployeeId = GetUserIdFromSession();
                model.absenceList.employeeId = request.employeeId;
            }

            if (model.toilList != null)
            {
                model.toilList.isTeam = IsTeam();
                model.toilList.employeeId = request.employeeId;
            }
            else
            {
                model.toilList = new ToilListViewModel();
            }

            if (model.internalMeetingsList != null)
            {
                model.internalMeetingsList.isTeam = IsTeam();
                model.internalMeetingsList.isHr = IsHR();
                model.internalMeetingsList.currentlyLoggedEmployeeId = GetUserIdFromSession();
                model.internalMeetingsList.employeeId = request.employeeId;
            }

            model.isManager = (bool)Session["IsManager"];
            if (model.sicknessList == null)
            {
                model.sicknessList = new SicknessListViewModel();
            }
            return PartialView("~/Views/Employees/Absence/_Absence.cshtml", model);
        }

        #region >>> Annual <<<

        public ActionResult LoadAnnualLeave(int id = 0)
        {
            var manager = new AbsenceManager();
            var request = new AbsenceDataRequest();
            var model = new AnnualLeaveListViewModel();
            request.employeeId = id;
            var responseAnnualLeave = manager.GetAnnualLeaveDetail(request);

            model = new JavaScriptSerializer().Deserialize<AnnualLeaveListViewModel>(responseAnnualLeave);

            if (model == null)
            {
                model = new AnnualLeaveListViewModel();
            }
            else
            {
                TempData["ToilAvailable"] = model.leaveDetail.timeOfInLieuOwed;
                TempData["AnnualAvailable"] = model.leaveDetail.leavesRemaining;
                Session["__WorkingPattern__"] = model.workingPattren;
            }

            model.employeeId = request.employeeId;
            model.isManager = (bool)Session["IsManager"];
            model.isTeam = IsTeam();
            model.isHr = IsHR();
            model.currentlyLoggedEmployeeId = GetUserIdFromSession();
            return PartialView("~/Views/Employees/Absence/_AnnualLeaveList.cshtml", model);
        }

        [HttpPost]
        public ActionResult GetAnnualLeave(int id)
        {
            //Getting Working Pattern
            var manager = new TrainingManager();
            var workingPatternResponse = manager.GetWorkingPattern(id);
            var workingPattern = new JavaScriptSerializer().Deserialize<WorkingHoursViewModel>(workingPatternResponse);

            LeaveEditViewModel model = new LeaveEditViewModel();
            model.bankHolidays = Session["bankHolidays"].ToString();
            model.employeeId = id;
            model.recordedBy = GetLoginUserName();
            model.unit = GetUnit();
            if (workingPattern.workingHours != null)
            {
                model.workingHours = workingPattern.workingHours; 
            }
            else
            {
                model.workingHours = new List<WorkingHoursHistoryViewModel>();
            }
            return PartialView("~/Views/Employees/Absence/_AnnualLeaveEdit.cshtml", model);
        }

        [HttpPost]
        public ActionResult SaveAnnualLeave(LeaveEditViewModel model, string submitButton)
        {
            var manager = new AbsenceManager();
            var response = "";
            // save functionality here 
            if (submitButton == "Add")
            {
                if (!IsAnnualAvailable(model.duration))
                {
                    var annualDuration = 0.0;
                    if (TempData.ContainsKey("AnnualAvailable"))
                        annualDuration = double.Parse(TempData["AnnualAvailable"].ToString());
                    TempData.Keep("AnnualAvailable");
                    var message = string.Format(_annualAvailableFailure, annualDuration);
                    response = "__failure__ " + message;
                    return Json(response, JsonRequestBehavior.AllowGet);
                }

                response = SaveLeave(model, LeaveType.Annual);
                if (response != "" && response.Contains("__failure__"))
                {
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
            }

            // get list record 
            var request = new AbsenceDataRequest();
            request.employeeId = model.employeeId;
            var responseAnnualLeave = manager.GetAnnualLeaveDetail(request);
            AnnualLeaveListViewModel lstmodel = new JavaScriptSerializer().Deserialize<AnnualLeaveListViewModel>(responseAnnualLeave);
            lstmodel.employeeId = model.employeeId;
            lstmodel.isTeam = IsTeam();
            lstmodel.isHr = IsHR();
            lstmodel.currentlyLoggedEmployeeId = GetUserIdFromSession();
            TempData["AnnualAvailable"] = lstmodel.leaveDetail.leavesRemaining;
            return PartialView("~/Views/Employees/Absence/_AnnualLeaveList.cshtml", lstmodel);
        }

        public ActionResult AnnualLeaveDetail(int id = 0)
        {
            var manager = new AbsenceManager();
            var model = new MyTeamLeavesResponseViewModel();
            var absenceDetailResponse = manager.GetAbsenceDetailByAbsenseHistoryId(id);

            model = new JavaScriptSerializer().Deserialize<MyTeamLeavesResponseViewModel> (absenceDetailResponse);

            if (model == null)
            {
                model = new MyTeamLeavesResponseViewModel();
            }

            // Employee can only cancel leave, manager can perform multiple actions
            var isTeam = IsTeam();
            var isHr = IsHR();
            var leaveDetail = model.myTeamLeavesPending.FirstOrDefault(e => e.absenceHistoryId == id);

            if (isTeam || (isHr && leaveDetail.applicantId != GetUserIdFromSession()))
            {
                ViewBag.HeaderTitle = "Annual Leave Approval";
                var excludeStatuses = new List<string>(){ "Apply","Last Day of Absence", "Log" };                
                model.leaveActions = model.leaveActions.Where(x => !(excludeStatuses.Contains(x.lookUpDescription)) ).ToList();
            }
            else
            {
                if (leaveDetail.status != "Cancelled" && leaveDetail.status != "Declined")
                {
                    model.leaveActions = model.leaveActions.Where(x => x.lookUpDescription.Equals("Cancel")).ToList();
                }
                ViewBag.HeaderTitle = "Update Annual Leave Status";
            }

            model.myTeamLeavesPending = model.myTeamLeavesPending.Where(e => e.absenceHistoryId == id).ToList();            

            if (leaveDetail.status == "Cancelled" || leaveDetail.status == "Declined")
            {
                if (leaveDetail.status == "Cancelled")
                {
                    model.leaveActions = model.leaveActions.Where(x => x.lookUpDescription.Equals("Cancel")).ToList();
                }
                else
                {
                    model.leaveActions = model.leaveActions.Where(x => x.lookUpDescription.Equals("Decline")).ToList();
                }

                return PartialView("~/Views/Employees/Absence/_AnnualLeaveApprovalDisabled.cshtml", model);
            }

            return PartialView("~/Views/Employees/Absence/_AnnualLeaveApproval.cshtml", model);
        }

        public ActionResult UpdateAnnualLeaveStatus(MyTeamLeavesResponseViewModel model)
        {
            var manager = new AbsenceManager();
            var response = new FailerResponseViewModel();

            response = SaveLeaveStatus(model, LeaveType.Annual);

            return Json(response.ToString(), JsonRequestBehavior.AllowGet);
        }
        public ActionResult UpdateToilLeaveStatus(MyTeamLeavesResponseViewModel model)
        {
            var manager = new AbsenceManager();
            var response = new FailerResponseViewModel();

            response = SaveLeaveStatus(model, LeaveType.Toil);

            return Json(response.ToString(), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region >>> Sickness <<<

        [HttpPost]
        public ActionResult GetSicknessLeave(int id)
        {
            //Getting Working Pattern
            var manager = new TrainingManager();
            var workingPatternResponse = manager.GetWorkingPattern(id);
            var workingPattern = new JavaScriptSerializer().Deserialize<WorkingHoursViewModel>(workingPatternResponse);

            LeaveEditViewModel model = new LeaveEditViewModel();
            model.employeeId = id;
            model.recordedBy = GetLoginUserName();
            model.absenceReasons = Session["absenceReasons"] as List<LookUpFields>;
            model.bankHolidays = Session["bankHolidays"].ToString();
            model.isTeam = IsTeam();
            model.unit = GetUnit();

            if (workingPattern.workingHours != null)
            {
                model.workingHours = workingPattern.workingHours;
            }
            else
            {
                model.workingHours = new List<WorkingHoursHistoryViewModel>();
            }

            return PartialView("~/Views/Employees/Absence/_SicknessLeaveEdit.cshtml", model);
        }

        [HttpPost]
        public ActionResult SaveSicknessAbsence(LeaveEditViewModel model, string submitButton)
        {
            var manager = new AbsenceManager();
            var response = "";
            // save functionality here 
            if (submitButton == "Add")
            {
                response = SaveLeave(model, LeaveType.Sickness);
                if (response != "" && response.Contains("__failure__"))
                {
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
            }

            // get list record 
            var request = new AbsenceDataRequest();
            request.employeeId = model.employeeId;
            var responseSickness = manager.GetSicknessAbsence(request);
            SicknessListViewModel lstmodel = new JavaScriptSerializer().Deserialize<SicknessListViewModel>(responseSickness);
            lstmodel.employeeId = model.employeeId;
            lstmodel.isTeam = IsTeam();
            return PartialView("~/Views/Employees/Absence/_SicknessLeaveList.cshtml", lstmodel);
        }

        public ActionResult SicknessDetail(int id = 0)
        {
            var manager = new AbsenceManager();
            var request = new AbsenceDataRequest();
            request.employeeId = GetUserIdFromSession();
            var responseSickness = manager.GetSicknessAbsence(request);
            var model = new JavaScriptSerializer().Deserialize<SicknessListViewModel>(responseSickness);
            if(model.staffMembers.Where(e => e.absenceHistoryId == id).Count()>0)
            {
                var editModel = model.staffMembers.Where(e => e.absenceHistoryId == id).Select(e => new PendingSicknessViewModel()
                {
                    staffMemberFName = e.staffMemberFName,
                    staffMemberLName = e.staffMemberLName,
                    staffMemberId = e.staffMemberId,
                    lineManagerId = e.lineManagerId,
                    startDate = e.startDate,
                    statusId = e.statusId,
                    status = e.status,
                    reasonId = e.reasonId,
                    absentDays = e.absentDays,
                    anticipatedReturnDate = e.anticipatedReturnDate,
                    anticipated = e.anticipatedReturnDate?.ToString("dd/MM/yyyy"),
                    endDate = e.endDate,
                    end = e.endDate?.ToString("dd/MM/yyyy"),
                    holidayType = e.holidayType,
                    nature = e.nature,
                    natureId = e.natureId,
                    reason = e.reason,
                    absenceHistoryId = e.absenceHistoryId,
                    notes = e.notes,
                    duration= e.duration != null ? e.duration : 0,
                    unit=e.unit

                }).FirstOrDefault();
                if (editModel.holidayType == null)
                {
                    editModel.holidayType = "F";
                }
                editModel.absenceReasons = model.absenceReasons;
                editModel.sicknessActions = model.sicknessActions;
                editModel.bankHolidays = string.Join(",", model.bankHolidays);

                //Getting Working Pattern
                var CurrentEmployeeId = int.Parse(editModel.staffMemberId.ToString());
                var TrainingManager = new TrainingManager();
                var workingPatternResponse = TrainingManager.GetWorkingPattern(CurrentEmployeeId);
                var workingPattern = new JavaScriptSerializer().Deserialize<WorkingHoursViewModel>(workingPatternResponse);

                if (workingPattern.workingHours != null)
                {
                    editModel.workingHours = workingPattern.workingHours;
                }
                else
                {
                    editModel.workingHours = new List<WorkingHoursHistoryViewModel>();
                }

                return PartialView("~/Views/Employees/Absence/_SicknessLeaveApproval.cshtml", editModel);
            }
            else
            {
                var responseSicknessLeave = manager.GetOwnSicknessAbsence(id);
                var SicknessModel = new JavaScriptSerializer().Deserialize<SicknessListViewModel>(responseSicknessLeave);
                var editModel = SicknessModel.staffMembers.Where(e => e.absenceHistoryId == id).Select(e => new PendingSicknessViewModel()
                {
                    staffMemberFName = e.staffMemberFName,
                    staffMemberLName = e.staffMemberLName,
                    staffMemberId = e.staffMemberId,
                    lineManagerId = e.lineManagerId,
                    startDate = e.startDate,
                    statusId = e.statusId,
                    status = e.status,
                    reasonId = e.reasonId,
                    absentDays = e.absentDays,
                    anticipatedReturnDate = e.anticipatedReturnDate,
                    anticipated = e.anticipatedReturnDate?.ToString("dd/MM/yyyy"),
                    endDate = e.endDate,
                    end = e.endDate?.ToString("dd/MM/yyyy"),
                    holidayType = e.holidayType,
                    nature = e.nature,
                    natureId = e.natureId,
                    reason = e.reason,
                    absenceHistoryId = e.absenceHistoryId,
                    notes = e.notes,
                    duration = e.duration != null ? e.duration : 0,                 
                    unit=e.unit
                }).FirstOrDefault();
                if(editModel.holidayType==null)
                {
                    editModel.holidayType = "F";
                }
                editModel.absenceReasons = SicknessModel.absenceReasons;
                editModel.sicknessActions = SicknessModel.sicknessActions;
                editModel.bankHolidays = string.Join(",", SicknessModel.bankHolidays);

                //Getting Working Pattern
                var CurrentEmployeeId = int.Parse(editModel.staffMemberId.ToString());
                var TrainingManager = new TrainingManager();
                var workingPatternResponse = TrainingManager.GetWorkingPattern(CurrentEmployeeId);
                var workingPattern = new JavaScriptSerializer().Deserialize<WorkingHoursViewModel>(workingPatternResponse);

                if (workingPattern.workingHours != null)
                {
                    editModel.workingHours = workingPattern.workingHours;
                }
                else
                {
                    editModel.workingHours = new List<WorkingHoursHistoryViewModel>();
                }

                if ((Session["Module"] != null && Session["Module"].ToString() == "MyJob") && !IsTeam())
                {
                    return PartialView("~/Views/Employees/Absence/_SicknessLeaveApprovalDisabled.cshtml", editModel);
                }
                else
                {
                    return PartialView("~/Views/Employees/Absence/_SicknessLeaveApproval.cshtml", editModel);
                }
            }
        }

        public ActionResult UpdateSicknessStatus(PendingSicknessViewModel model)
        {
            var manager = new AbsenceManager();
            var response = new FailerResponseViewModel();

            response = SaveSicknessStatus(model);

            return Json(response.message, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region >>> Leave of Absence <<<

        [HttpPost]
        public ActionResult GetAbsenceLeave(int id)
        {
            //Getting Working Pattern
            var manager = new TrainingManager();
            var workingPatternResponse = manager.GetWorkingPattern(id);
            var workingPattern = new JavaScriptSerializer().Deserialize<WorkingHoursViewModel>(workingPatternResponse);

            LeaveEditViewModel model = new LeaveEditViewModel();
            model.bankHolidays = Session["bankHolidays"].ToString();
            model.employeeId = id;
            model.recordedBy = GetLoginUserName();
            model.unit = GetUnit();
            model.leavesNatures = Session["leavsNatures"] as List<LookUpFields>;
            model.isTeam = IsTeam();

            if (workingPattern.workingHours != null)
            {
                model.workingHours = workingPattern.workingHours;
            }
            else
            {
                model.workingHours = new List<WorkingHoursHistoryViewModel>();
            }

            return PartialView("~/Views/Employees/Absence/_AbsenceLeaveEdit.cshtml", model);
        }

        [HttpPost]
        public ActionResult SaveAbsenceLeave(LeaveEditViewModel model, string submitButton)
        {
            var manager = new AbsenceManager();
            var response = "";
            // save functionality here 
            if (submitButton == "Add")
            {
                if (model.requested == false) // false means single day leave
                {
                    model.endDate = model.startDate;
                }

                response = SaveLeave(model, LeaveType.LeaveOfAbsence);

                if (response != "" && response.Contains("__failure__"))
                {
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
            }

            // get list record 
            var request = new AbsenceDataRequest();
            request.employeeId = model.employeeId;
            var responseAbsence = manager.GetLeaveOfAbsence(request);

            AbsenceListViewModel lstmodel = new JavaScriptSerializer().Deserialize<AbsenceListViewModel>(responseAbsence);
            lstmodel.absentees.RemoveAll(e => e.nature == "Maternity - Add" || e.nature == "Internal Meeting");
            lstmodel.employeeId = model.employeeId;
            lstmodel.isTeam = IsTeam();
            lstmodel.isHr = IsHR();
            lstmodel.currentlyLoggedEmployeeId = GetUserIdFromSession();
            return PartialView("~/Views/Employees/Absence/_AbsenceLeaveList.cshtml", lstmodel);
        }


        public ActionResult AbsenceLeaveDetail(int id = 0)
        {
            var manager = new AbsenceManager();
            var model = new MyTeamLeavesResponseViewModel();
            var response = manager.GetAbsenceDetailByAbsenseHistoryId(id);

            model = new JavaScriptSerializer().Deserialize<MyTeamLeavesResponseViewModel>(response);

            if (model == null)
            {
                model = new MyTeamLeavesResponseViewModel();
            }

            // Employee can only cancel leave, manager can perform multiple actions
            var isTeam = IsTeam();
            var isHr = IsHR();
            var leaveDetail = model.myTeamLeavesPending.FirstOrDefault(e => e.absenceHistoryId == id);

            if (isTeam || (isHr && leaveDetail.applicantId != GetUserIdFromSession()))
            {
                ViewBag.HeaderTitle = "Other Leave Approval";
                model.leaveActions = model.leaveActions.Where(x => x.lookUpDescription != "Apply").ToList();
            }
            else {
                if (leaveDetail.status != "Cancelled" && leaveDetail.status != "Declined")
                {
                    model.leaveActions = model.leaveActions.Where(x => x.lookUpDescription.Equals("Cancel")).ToList();
                }
                ViewBag.HeaderTitle = "Update Other Leave Status";
            }
            
            model.myTeamLeavesPending = model.myTeamLeavesPending.Where(e => e.absenceHistoryId == id).ToList();
            model.pendingLeaves = model.myTeamLeavesPending.FirstOrDefault(e => e.absenceHistoryId == id);
            model.absenceNature = Session["leavsNatures"] as List<LookUpFields>;

            if (leaveDetail.status == "Cancelled" || leaveDetail.status == "Declined")
            {
                if (leaveDetail.status == "Cancelled")
                {
                    model.leaveActions = model.leaveActions.Where(x => x.lookUpDescription.Equals("Cancel")).ToList();
                }
                else
                {
                    model.leaveActions = model.leaveActions.Where(x => x.lookUpDescription.Equals("Decline")).ToList();
                }

                return PartialView("~/Views/Employees/Absence/_AbsenceLeaveApprovalDisabled.cshtml", model);
            }

            return PartialView("~/Views/Employees/Absence/_AbsenceLeaveApproval.cshtml", model);
        }

        public ActionResult UpdateAbsenceLeaveStatus(MyTeamLeavesResponseViewModel model)
        {
            var manager = new AbsenceManager();
            var response = new FailerResponseViewModel();
            response = SaveLeaveStatus(model, LeaveType.LeaveOfAbsence);

            return Json(response.ToString(), JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region >>> Maternity Peternity <<<

        public ActionResult GetMaternityPeternityLeave(int id)
        {
            var manager = new AbsenceManager();
            var request = new AbsenceDataRequest();
            var model = new AbsenceViewModel();
            request.employeeId = id;

            var responseAbsence = manager.GetLeaveOfAbsence(request);

            model.absenceList = new JavaScriptSerializer().Deserialize<AbsenceListViewModel>(responseAbsence);

            if (model.absenceList.absentees == null)
            {
                model.absenceList.absentees = new List<AbsenteeViewModel>();
            }

            model.maternityPaternityList.absentees = model.absenceList.absentees.Where(e => e.nature == "Paternity" || e.nature == "Maternity" || e.nature == "Maternity - Add").ToList();

            return PartialView("~/Views/Employees/Absence/_MaternityPaternityList.cshtml", model.maternityPaternityList);
        }

        #endregion

        #region >>> Internal Meeting <<<

        [HttpPost]
        public ActionResult GetInternalMeetings(int id)
        {
            //Getting Working Pattern
            var manager = new TrainingManager();
            var workingPatternResponse = manager.GetWorkingPattern(id);
            var workingPattern = new JavaScriptSerializer().Deserialize<WorkingHoursViewModel>(workingPatternResponse);

            LeaveEditViewModel model = new LeaveEditViewModel();
            model.employeeId = id;
            model.recordedBy = GetLoginUserName();
            model.leavesNatures = Session["leavsNatures"] as List<LookUpFields>;
            model.bankHolidays = Session["bankHolidays"].ToString();
            model.isTeam = IsTeam();

            if (workingPattern.workingHours != null)
            {
                model.workingHours = workingPattern.workingHours;
            }
            else
            {
                model.workingHours = new List<WorkingHoursHistoryViewModel>();
            }

            return PartialView("~/Views/Employees/Absence/_InternalMeetingsEdit.cshtml", model);
        }

        public ActionResult GetInternalLeaveDetail(int id = 0)
        {
            var manager = new AbsenceManager();
            var model = new MyTeamLeavesResponseViewModel();
            var response = manager.GetAbsenceDetailByAbsenseHistoryId(id);

            model = new JavaScriptSerializer().Deserialize<MyTeamLeavesResponseViewModel>(response);

            if (model == null)
            {
                model = new MyTeamLeavesResponseViewModel();
            }

            // Employee can only cancel leave, manager can perform multiple actions
            var isTeam = IsTeam();
            var isHr = IsHR();
            var leaveDetail = model.myTeamLeavesPending.FirstOrDefault(e => e.absenceHistoryId == id);

            // internal meeting all fields can update. US789
            var trainingManager = new TrainingManager();
            var workingPatternResponse = trainingManager.GetWorkingPattern(leaveDetail.applicantId.Value);
            var workingPattern = new JavaScriptSerializer().Deserialize<WorkingHoursViewModel>(workingPatternResponse);

            model.leavesNatures = Session["leavsNatures"] as List<LookUpFields>;
            model.bankHolidays = Session["bankHolidays"].ToString();
            model.isTeam = IsTeam();
            model.employeeId = leaveDetail.applicantId.Value;

            if (workingPattern.workingHours != null)
            {
                model.workingHours = workingPattern.workingHours;
            }
            else
            {
                model.workingHours = new List<WorkingHoursHistoryViewModel>();
            }

            if (isTeam || (isHr && leaveDetail.applicantId != GetUserIdFromSession()))
            {
                ViewBag.HeaderTitle = "Internal Meeting Leave Approval";
                model.leaveActions = model.leaveActions.Where(x => x.lookUpDescription != "Apply" && x.lookUpDescription != "Log" && x.lookUpDescription != "Return Date").ToList();
            }
            else
            {
                if (leaveDetail.status != "Cancelled" && leaveDetail.status != "Declined")
                {
                    model.leaveActions = model.leaveActions.Where(x => x.lookUpDescription.Equals("Cancel") || x.lookUpDescription.Equals("Apply")).ToList();
                }
                ViewBag.HeaderTitle = "Update Internal Meeting Leave Status";
            }

            model.myTeamLeavesPending = model.myTeamLeavesPending.Where(e => e.absenceHistoryId == id).ToList();
            model.pendingLeaves = model.myTeamLeavesPending.FirstOrDefault(e => e.absenceHistoryId == id);
            model.absenceNature = Session["leavsNatures"] as List<LookUpFields>;
            model.absenceNature.Add(new LookUpFields() { lookUpDescription = "Internal Meeting", lookUpId = 48 });

            if (leaveDetail.status == "Cancelled" || leaveDetail.status == "Declined")
            {
                if (leaveDetail.status == "Cancelled")
                {
                    model.leaveActions = model.leaveActions.Where(x => x.lookUpDescription.Equals("Cancel")).ToList();
                }
                else
                {
                    model.leaveActions = model.leaveActions.Where(x => x.lookUpDescription.Equals("Decline")).ToList();
                }

                return PartialView("~/Views/Employees/Absence/_InternalLeaveApprovalDisabled.cshtml", model);
            }

            if (model.myTeamLeavesPending[0].startDate <= DateTime.Now)
            {
                return PartialView("~/Views/Employees/Absence/_InternalLeaveApprovalDisabled.cshtml", model);   
            }

            var pendingInternalLeaveVM = new PendingInternalLeaveViewModel();
            pendingInternalLeaveVM.absenceHistoryId = model.myTeamLeavesPending[0].absenceHistoryId;
            pendingInternalLeaveVM.applicantId = model.myTeamLeavesPending[0].applicantId;
            pendingInternalLeaveVM.applicantName = model.myTeamLeavesPending[0].applicantName;
            pendingInternalLeaveVM.duration = model.myTeamLeavesPending[0].duration;
            pendingInternalLeaveVM.employeeId = model.myTeamLeavesPending[0].employeeId;
            pendingInternalLeaveVM.endDate = model.myTeamLeavesPending[0].endDate;
            pendingInternalLeaveVM.from = model.myTeamLeavesPending[0].from;
            pendingInternalLeaveVM.holType = model.myTeamLeavesPending[0].holType;
            pendingInternalLeaveVM.leaveDescription = model.myTeamLeavesPending[0].leaveDescription;
            pendingInternalLeaveVM.natureId = model.myTeamLeavesPending[0].natureId;
            pendingInternalLeaveVM.notes = model.myTeamLeavesPending[0].notes;
            pendingInternalLeaveVM.startDate = model.myTeamLeavesPending[0].startDate;
            pendingInternalLeaveVM.status = model.myTeamLeavesPending[0].status;
            pendingInternalLeaveVM.statusId = model.myTeamLeavesPending[0].statusId;
            pendingInternalLeaveVM.title = model.myTeamLeavesPending[0].title;
            pendingInternalLeaveVM.to = model.myTeamLeavesPending[0].to;
            pendingInternalLeaveVM.unit = model.myTeamLeavesPending[0].unit;
            pendingInternalLeaveVM._notes = model.myTeamLeavesPending[0]._notes;
            pendingInternalLeaveVM._reason = model.myTeamLeavesPending[0]._reason;
            pendingInternalLeaveVM.absenceReasons = model.absenceReasons;
            pendingInternalLeaveVM.bankHolidays = model.bankHolidays;
            pendingInternalLeaveVM.leavesNatures = model.leavesNatures;
            pendingInternalLeaveVM.natures = model.natures;
            pendingInternalLeaveVM.reasons = model.reasons;
            pendingInternalLeaveVM.statusLookUps = model.statusLookUps;
            pendingInternalLeaveVM.workingHours = model.workingHours;
            pendingInternalLeaveVM.isTeam = model.isTeam;


            return PartialView("~/Views/Employees/Absence/_InternalLeaveApproval.cshtml", pendingInternalLeaveVM);
        }

        [HttpPost]
        public ActionResult SaveInternalLeave(LeaveEditViewModel model, string submitButton)
        {
            var manager = new AbsenceManager();
            var response = "";
            // save functionality here 
            if (submitButton == "Add")
            {
                if (model.requested == false) // false means single day leave
                {
                    model.endDate = model.startDate;
                }

                response = SaveLeave(model, LeaveType.InternalMeeting);
                
                if (response != "" && response.Contains("__failure__"))
                {
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
            }

            // get list record 
            var responseModel = InternalMeetingList(model.employeeId);
            return PartialView("~/Views/Employees/Absence/_InternalMeetingsList.cshtml", responseModel.internalMeetingsList);
        }

        public ActionResult UpdateInternalStatus(PendingInternalLeaveViewModel model)
        {
            var manager = new AbsenceManager();
            var response = new FailerResponseViewModel();

            response = SaveInternalStatus(model);

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region >>> Toil <<<

        [HttpGet]
        public ActionResult GetToilLeave(int id)
        {
            //Getting Working Pattern
            var manager = new TrainingManager();
            var workingPatternResponse = manager.GetWorkingPattern(id);
            var workingPattern = new JavaScriptSerializer().Deserialize<WorkingHoursViewModel>(workingPatternResponse);

            LeaveEditViewModel model = new LeaveEditViewModel();
            model.bankHolidays = Session["bankHolidays"].ToString();
            model.employeeId = id;
            model.recordedBy = GetLoginUserName();
            model.isTeam = IsTeam();
            model.duration = 0;

            if (workingPattern.workingHours != null)
            {
                model.workingHours = workingPattern.workingHours;
            }
            else
            {
                model.workingHours = new List<WorkingHoursHistoryViewModel>();
            }

            return PartialView("~/Views/Employees/Absence/_ToilLeaveEdit.cshtml", model);
        }

        [HttpPost]
        public ActionResult SaveToilLeave(LeaveEditViewModel model, string submitButton)
        {
            var manager = new AbsenceManager();
            var response = "";
            // save functionality here 
            if (submitButton == "Add")
            {
                // HR and Line manager can add quota for employee in toil.
                if (!IsTeam())
                {
                    // only employee can apply/avail toil quota
                    if (Session["Module"] != null && Session["Module"].ToString() == "MyJob")
                    {
                        if (!IsToilAvailable(model.duration))
                        {
                            response = "__failure__ "+ _toilAvailableFailure;
                            return Json(response, JsonRequestBehavior.AllowGet);
                        }
                    
                        var from = model.startDate.Add(TimeSpan.Parse(model.from));
                        from = from.AddHours(model.duration);
                        model.to = from.ToString("HH:mm");
                    }
                }

                response = SaveLeave(model, LeaveType.Toil);
                if (response != "" && response.Contains("__failure__"))
                {
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
            }

            // get list record 
            var request = new AbsenceDataRequest();
            request.employeeId = model.employeeId;
            var responseToilLeave = manager.GetToilAbsence(request);
            ToilListViewModel lstmodel = new JavaScriptSerializer().Deserialize<ToilListViewModel>(responseToilLeave);
            lstmodel.employeeId = model.employeeId;
            lstmodel.isTeam = IsTeam();
            return PartialView("~/Views/Employees/Absence/_ToilLeaveList.cshtml", lstmodel);
        }

        public ActionResult ToilLeaveDetail(int id = 0)
        {
            var manager = new AbsenceManager();
            var model = new MyTeamLeavesResponseViewModel();
            var absenceDetailResponse = manager.GetAbsenceDetailByAbsenseHistoryId(id);

            model = new JavaScriptSerializer().Deserialize<MyTeamLeavesResponseViewModel>(absenceDetailResponse);

            if (model == null)
            {
                model = new MyTeamLeavesResponseViewModel();
            }

            // Employee can only cancel leave, manager can perform multiple actions
            var isTeam = IsTeam();
            var isHr = IsHR();
            var leaveDetail = model.myTeamLeavesPending.FirstOrDefault(e => e.absenceHistoryId == id);

            if (isTeam || (isHr && leaveDetail.applicantId != GetUserIdFromSession()))
            {
                ViewBag.HeaderTitle = "Toil Leave Approval";
                var excludeStatuses = new List<string>() { "Apply", "Last Day of Absence", "Log" };
                model.leaveActions = model.leaveActions.Where(x => !(excludeStatuses.Contains(x.lookUpDescription))).ToList();
            }
            else
            {
                if (leaveDetail.status != "Cancelled" && leaveDetail.status != "Declined")
                {
                    model.leaveActions = model.leaveActions.Where(x => x.lookUpDescription.Equals("Cancel")).ToList();
                }
                ViewBag.HeaderTitle = "Update Toil Leave Status";
            }

            model.myTeamLeavesPending = model.myTeamLeavesPending.Where(e => e.absenceHistoryId == id).ToList();

            if (leaveDetail.status == "Cancelled" || leaveDetail.status == "Declined")
            {
                if (leaveDetail.status == "Cancelled")
                {
                    model.leaveActions = model.leaveActions.Where(x => x.lookUpDescription.Equals("Cancel")).ToList();
                }
                else
                {
                    model.leaveActions = model.leaveActions.Where(x => x.lookUpDescription.Equals("Decline")).ToList();
                }

                return PartialView("~/Views/Employees/Absence/_ToilLeaveApprovalDisabled.cshtml", model);
            }

            return PartialView("~/Views/Employees/Absence/_ToilLeaveApproval.cshtml", model);
        }

        #endregion

        [AllowAnonymous]
        [HttpPost]
        public ActionResult CheckExistingEmail(DateTime startDate)
        {
            try
            {
                return Json(true);
            }
            catch (Exception ex)
            {
                return Json(false);
            }
        }

        #endregion

    }
}