﻿using AutoMapper;
using BHGHRModuleWeb.ViewModels;
using BusinessModule.NetworkManagers;
using BusinessModule.NetworkObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace BHGHRModuleWeb.Controllers.Employees
{
    public class SkillsController : ABaseController
    {
        // GET: Skills
        public ActionResult Index(int id = 0)
        {
            if (id == 0)
            {
                if (Session["employeeId"] != null)
                {
                    ViewBag.isEdit = "create";
                    id = Convert.ToInt32(Session["employeeId"]);
                }
            }
            else
            {
                ViewBag.isEdit = "edit";
            }

            SkillsResponseViewModel skillListing = GetSkills(id);
            skillListing.skillDetail.employeeId = id;
            return View("~/Views/Employees/Skills/_Skills.cshtml", skillListing);
        }
        public SkillsResponseViewModel GetSkills(int id = 0)
        {
            var skillsManager = new SkillsManager();
            var skillsDataRequest = new SkillsListingDataRequest();
            skillsDataRequest.employeeId = id;
            skillsDataRequest.sortBy = "qualificationDateTime";
            skillsDataRequest.sortOrder = "DESC";
            int pageNo = 1;
            if (HttpContext.Request.QueryString["page"] != null && HttpContext.Request.QueryString["page"] != "0")
            {
                pageNo = Convert.ToInt32(HttpContext.Request.QueryString["page"]);
            }
            if (HttpContext.Request.QueryString["sort"] != null && HttpContext.Request.QueryString["sortdir"] != null)
            {
                skillsDataRequest.sortBy = HttpContext.Request.QueryString["sort"].ToString();
                skillsDataRequest.sortOrder = HttpContext.Request.QueryString["sortdir"].ToString();
            }
            skillsDataRequest.pagination.pageSize = _PageSize;
            skillsDataRequest.pagination.pageNo = pageNo;
            var response = skillsManager.getEmployeesSkills(skillsDataRequest);
            SkillsResponseViewModel model = new JavaScriptSerializer().Deserialize<SkillsResponseViewModel>(response);
            if (model != null)
            {
                ViewBag.TotalRows = model.skills.pagination.totalRows;
                ViewBag.PageSize = model.skills.pagination.pageSize;
                Session["Skills"] = model;
            }

            return model;
        }
        [HttpPost]
        public ActionResult AddAmendEmployeeSkills(SkillsDetailViewModel skillDetail)
        {
            var response = "";
            var responses = "";
            // remove required validator from "Professional Accreditation"
            //if(skillDetail.pQualification == null || skillDetail.pQualification == string.Empty)
            //    return Json("\"" + "Please add atleast 1 Professional Accreditation" + "\"", JsonRequestBehavior.AllowGet);
            var skillsManager = new SkillsManager();
            var skillDataRequest = new SkillsDataRequest();
            if (skillDetail.pQualification != null && skillDetail.pQualification != "")
                skillDetail.professionalQualification = skillDetail.pQualification.Split(',').Select(t => Convert.ToString(t)).ToList();
            skillDetail.createdBy = GetUserIdFromSession();
            if (RouteData.Values["id"] != null)
            {
                skillDetail.employeeId = int.Parse(RouteData.Values["id"].ToString());
            }
            else
            {
                if (Session["employeeId"] != null)
                {
                    skillDetail.employeeId = int.Parse(Session["employeeId"].ToString());
                }
            }

            skillDataRequest = Mapper.Map<SkillsDetailViewModel, SkillsDataRequest>(skillDetail);
            response = skillsManager.addAmendSkillsRecord(skillDataRequest);

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetSkillById(int Id)
        {
            SkillsResponseViewModel model = new SkillsResponseViewModel();
            SkillsDetailViewModel detail = new SkillsDetailViewModel();
            model = (SkillsResponseViewModel)Session["Skills"];
            detail = model.skills.skillsList.SingleOrDefault(c => c.qualificationId == Id);
            return Json(detail, JsonRequestBehavior.AllowGet);
        }
        public ActionResult RemoveSkillById(int Id)
        {
            var response = "";
            var skillsManager = new SkillsManager();
            var skillDataRequest = new SkillsDataRequest();
            var skillsDataRequest = new SkillsListingDataRequest();
            skillDataRequest.qualificationId = Id;
            skillDataRequest.updatedBy = GetUserIdFromSession();
            response = skillsManager.deleteSkillsRecord(skillDataRequest);
            var model = (SkillsResponseViewModel)Session["Skills"];
            var detail = model.skills.skillsList.SingleOrDefault(c => c.qualificationId == Id);
            model.skills.skillsList.Remove(detail);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SearchSkills(int id = 0)
        {
            SkillsResponseViewModel model = GetSkills(id);
            return PartialView("~/Views/Employees/Skills/_SkillListing.cshtml", model.skills);
        }

        [HttpPost]
        public ActionResult AddEmployeeLanguageSkills(LanguageSkillsViewModel language)
        {
            SkillsResponseViewModel model = new SkillsResponseViewModel();
            model = (SkillsResponseViewModel)Session["Skills"];
            if (language.isWritten == true || language.isSpoken == true)
            {
                if (model.languageSkills.Count() > 0)
                {
                    var highestlanguageId = (from c in model.languageSkills orderby c.languageId descending select c.languageId).FirstOrDefault();
                    language.languageId = highestlanguageId + 1;
                }
                else
                {
                    language.languageId = 1;
                }

                if (language.isWritten == true && language.isSpoken == true)
                {
                    language.spokenWritten = "Spoken/Written";
                }
                else if (language.isSpoken == true)
                {
                    language.spokenWritten = "Spoken";
                }
                else if (language.isWritten == true)
                {
                    language.spokenWritten = "Written";
                }
                if (language.languageTypeId > 0)
                {
                    var languageText = (from c in model.lookUps.languageTypes where c.lookUpId == language.languageTypeId select c.lookUpDescription).FirstOrDefault();
                    if (languageText == "Other")
                    {
                        language.language = language.languageOther;
                    }
                    else
                    {
                        language.language = languageText;
                    }
                }
                language.employeeId = model.skillDetail.employeeId;
                language.createdBy = GetUserIdFromSession();
                model.languageSkills.Add(language);
                Session["Skills"] = model;
            }
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public ActionResult RemovelanguageById(int Id)
        {
            var model = (SkillsResponseViewModel)Session["Skills"];
            var detail = model.languageSkills.SingleOrDefault(c => c.languageId == Id);
            model.languageSkills.Remove(detail);
            Session["Skills"] = model;
            return Json(model, JsonRequestBehavior.AllowGet);
        }


        public ActionResult SaveEmployeeLanguageSkills()
        {
            var model = (SkillsResponseViewModel)Session["Skills"];
            var skillsManager = new SkillsManager();
            var dataRequest = new LanguageSkillsRequest();
            dataRequest.languages = Mapper.Map<List<LanguageSkillsViewModel>, List<LanguageSkillsDataRequest>>(model.languageSkills);
            string response = skillsManager.addAmendLanguageSkills(dataRequest);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

    }
}