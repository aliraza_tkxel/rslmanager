﻿using AutoMapper;
using BHGHRModuleWeb.ViewModels;
using TVM = BHGHRModuleWeb.ViewModels.Admin.Training;
using BusinessModule.NetworkManagers;
using BusinessModule.NetworkObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace BHGHRModuleWeb.Controllers.Employees
{
    public class TrainingController : ABaseController
    {
        #region >>> Helpers <<<

        private bool IsTeam()
        {
            var isTeam = false;
            if (TempData.ContainsKey("isTeam"))
                isTeam = bool.Parse(TempData["isTeam"].ToString());
            TempData.Keep("isTeam");

            return isTeam;
        }

        public TrainingViewModel GetTrainings(int id = 0)
        {
            var manager = new TrainingManager();
            var dataRequest = new TrainingListingDataRequest();
            var model = new TrainingViewModel();

            try
            {
                dataRequest.employeeId = id;
                dataRequest.sortBy = "trainingId";
                dataRequest.sortOrder = "DESC";
                int pageNo = 1;
                if (HttpContext.Request.QueryString["page"] != null && HttpContext.Request.QueryString["page"] != "0")
                {
                    pageNo = Convert.ToInt32(HttpContext.Request.QueryString["page"]);
                }
                if (HttpContext.Request.QueryString["sort"] != null && HttpContext.Request.QueryString["sortdir"] != null)
                {
                    dataRequest.sortBy = HttpContext.Request.QueryString["sort"].ToString();
                    dataRequest.sortOrder = HttpContext.Request.QueryString["sortdir"].ToString();
                }
                dataRequest.pagination.pageSize = _PageSize;
                dataRequest.pagination.pageNo = pageNo;
                var response = manager.GetEmployeesTrainings(dataRequest);
                model.trainingList = new JavaScriptSerializer().Deserialize<TrainingGridViewModel>(response);
                ViewBag.IsTeam = IsTeam();
            }
            catch (Exception ex)
            {

            }
            
            
            model.detail.employeeId = id;

            if (model.trainingList != null)
            {
                ViewBag.TotalRows = model.trainingList.pagination.totalRows;
                ViewBag.PageSize = model.trainingList.pagination.pageSize;
                Session["Trainings"] = model;
            }
            else
            {
                model.trainingList = new TrainingGridViewModel();
                model.trainingList.pagination = new PaginationViewModel();
                model.trainingList.pagination.totalRows = ViewBag.TotalRows = 0;
                model.trainingList.pagination.pageSize = ViewBag.PageSize = _PageSize;
            }
            var workingPatternResponse= manager.GetWorkingPattern(id);
            model.detail.workingPattern = new JavaScriptSerializer().Deserialize<WorkingHoursViewModel>(workingPatternResponse);

            if (model.detail.workingPattern.workingHours == null)
            {
                model.detail.workingPattern.workingHours = new List<WorkingHoursHistoryViewModel>();
            }
            return model;
        }

        #endregion

        #region >>> Action Methods <<<

        // GET: Training
        public ActionResult Index(int Id = 0)
        {
            TrainingViewModel training = GetTrainings(Id);

            return PartialView("~/Views/Employees/Training/_Training.cshtml", training);
        }

        [HttpPost]
        public ActionResult AddEmployeeTrainings(TrainingDetailViewModel detail)
        {
            var response = "";
            var manager = new TrainingManager();
            var dataRequest = new TrainingDataRequest();
            if (ModelState.IsValid)
            {
                JavaScriptSerializer js = new JavaScriptSerializer();
                if (detail.appTrainings != null)
                {
                    TVM.ApprovedTrainingViewModel[] list = js.Deserialize<TVM.ApprovedTrainingViewModel[]>(detail.appTrainings);
                    //detail.approvedTraining = list.ToList();
                }
                detail.createdby = GetUserIdFromSession();
                //detail.approvedtraining = "";
                dataRequest = Mapper.Map<TrainingDetailViewModel, TrainingDataRequest>(detail);
                response = manager.AddEmployeeTrainings(dataRequest);
            }
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetTrainingById(int Id)
        {
            TrainingViewModel model = new TrainingViewModel();
            TrainingDetailViewModel detail = new TrainingDetailViewModel();
            model = (TrainingViewModel)Session["Trainings"];
            detail = model.trainingList.trainingList.SingleOrDefault(c => c.trainingId == Id);
            return PartialView("~/Views/Employees/Training/_TrainingDetailEdit.cshtml", detail);
        }
        public ActionResult UpdateEmployeeTraining(TrainingUpdateViewModel training)
        {
            var manager = new TrainingManager();
            var dataRequest = new TrainingUpdateDataRequest();
            //if (training.status == "Approve")
            //    training.status = "Approved";
            //else
            //    training.status = "Declined";
            training.updatedBy = GetUserIdFromSession();
            dataRequest = Mapper.Map<TrainingUpdateViewModel, TrainingUpdateDataRequest>(training);
            var response = manager.UpdateEmployeeTrainings(dataRequest);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SearchTrainings(int id = 0)
        {
            TrainingViewModel model = GetTrainings(id);
            return PartialView("~/Views/Employees/Training/_TrainingListing.cshtml", model.trainingList);
        }

        #endregion

    }
}