﻿using BHGHRModuleWeb.ViewModels;
using BusinessModule.NetworkManagers;
using BusinessModule.NetworkObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace BHGHRModuleWeb.Controllers.Employees
{
    public class JournalController : ABaseController
    {
        // GET: Journal
        public ActionResult Index()
        {
            return View();
        }
        #region journal report
        public ActionResult JournalReportPopulate()
        {
            JournalGridViewModel model = GetJournalReportPopulate();
            return View(model);
        }
        private JournalGridViewModel GetJournalReportPopulate(int Id = 0)
        {
            int status = 1;
            if (Request["rdBtnstatus"] != null)
            {
                status = Convert.ToInt32(Request["rdBtnstatus"].ToString());
            }
            var manager = new JournalManager();
            var dataRequest = new JournalDataRequest();
            dataRequest.pagination = new BusinessModule.NetworkObjects.PaginationDataRequest();
            if (Request["fromDate"] != null && Request["fromDate"] != "")
            {
                dataRequest.fromDate = Request["fromDate"].ToString();
            }
            if (Request["toDate"] != null && Request["toDate"] != "")
            {
                dataRequest.toDate = Request["toDate"].ToString();
            }
            if (Request["itemType"] != null && Request["itemType"] != "")
            {
                dataRequest.itemType = Request["itemType"].ToString();
            }
            
            dataRequest.employeeId = Id;
            dataRequest.sortBy = "lastActionDateString";
            dataRequest.sortOrder = "DESC";
            int pageNo = 1;
            if (HttpContext.Request.QueryString["page"] != null && HttpContext.Request.QueryString["page"] != "0" && HttpContext.Request.QueryString["page"] != "undefined")
            {
                pageNo = Convert.ToInt32(HttpContext.Request.QueryString["page"]);
            }
            if (HttpContext.Request.QueryString["sort"] != null && HttpContext.Request.QueryString["sortdir"] != null)
            {
                dataRequest.sortBy = HttpContext.Request.QueryString["sort"].ToString();
                dataRequest.sortOrder = HttpContext.Request.QueryString["sortdir"].ToString();
            }
            dataRequest.pagination.pageSize = _PageSize;
            dataRequest.pagination.pageNo = pageNo;
            var response = manager.GetJournalReport(dataRequest);
            JournalGridViewModel model = new JavaScriptSerializer().Deserialize<JournalGridViewModel>(response);
            if (model.pagination != null)
            {
                ViewBag.TotalRows = model.pagination.totalRows;
                ViewBag.PageSize = model.pagination.pageSize;
            }
            else
            {
                ViewBag.TotalRows = 0;
                ViewBag.PageSize = 0;
            }
            return model;
        }
        public ActionResult JournalReport(int Id = 0)
        {

            JournalManager manager = new JournalManager();
            var dataRequest = new JournalDataRequest();
            dataRequest.pagination = new BusinessModule.NetworkObjects.PaginationDataRequest();

            dataRequest.sortBy = "lastActionDateString";
            dataRequest.sortOrder = "DESC";
            dataRequest.itemType = "";
            dataRequest.searchText = "";
            dataRequest.employeeId = Id;

            int pageNo = 1;
            if (HttpContext.Request.QueryString["page"] != null && HttpContext.Request.QueryString["page"] != "0")
            {
                pageNo = Convert.ToInt32(HttpContext.Request.QueryString["page"]);
            }
            if (HttpContext.Request.QueryString["sort"] != null && HttpContext.Request.QueryString["sortdir"] != null)
            {
                dataRequest.sortBy = HttpContext.Request.QueryString["sort"].ToString();
                dataRequest.sortOrder = HttpContext.Request.QueryString["sortdir"].ToString();
            }

            dataRequest.pagination.pageSize = _PageSize;
            dataRequest.pagination.pageNo = pageNo;
            var response = manager.GetJournalReport(dataRequest);
            //var model = new JournalGridViewModel (){ 

            //pagination = new PaginationViewModel() {pageNo=1,pageSize=10,totalPages=1,totalRows=5 },
            //reportListing = new List<JournalViewModel>() {
            //    new JournalViewModel() {employeeId=423,itemType="Benefits",journalDate="30/10/1900",nature="Good",notes="Benefits added",status="well",title="Hello world" },
            //    new JournalViewModel() {employeeId=423,itemType="Health",journalDate="30/10/1900",nature="Good",notes="Benefits added",status="well",title="Hello world" },
            //    new JournalViewModel() {employeeId=423,itemType="Skills",journalDate="30/10/1900",nature="Good",notes="Benefits added",status="well",title="Hello world" },
            //    new JournalViewModel() {employeeId=423,itemType="Remunerations",journalDate="30/10/1900",nature="Good",notes="Benefits added",status="well",title="Hello world" },
            //    new JournalViewModel() {employeeId=423,itemType="My Team",journalDate="30/10/1900",nature="Good",notes="Benefits added",status="well",title="Hello world" }
            //     }

            //};
            JournalGridViewModel model = new JavaScriptSerializer().Deserialize<JournalGridViewModel>(response);
            model.employeeId = Id;

            if (model.employeeList != null)
            {
                ViewBag.TotalRows = model.pagination.totalRows;
                ViewBag.PageSize = model.pagination.pageSize;
            }
            else
            {
                ViewBag.TotalRows = _PageSize;
                ViewBag.PageSize = pageNo;
            }

            return PartialView("~/Views/Employees/Journal/_JournalReport.cshtml", model);
        }
        public ActionResult SearchJournal(int Id = 0)
        {
            JournalGridViewModel model = GetJournalReportPopulate(Id);
            return PartialView("~/Views/Employees/Journal/_JournalGrid.cshtml", model);
        }
        public ActionResult JournalReportCriteria(JournalDataRequest dataRequest)
        {
            JournalManager manager = new JournalManager();
            dataRequest.pagination = new BusinessModule.NetworkObjects.PaginationDataRequest();
            dataRequest.sortBy = "lastActionDateString";
            dataRequest.sortOrder = "DESC";
            dataRequest.searchText = "";
            dataRequest.pagination.pageSize = _ReportPageSize;
            var response = manager.GetJournalReport(dataRequest);
            JournalGridViewModel model = new JavaScriptSerializer().Deserialize<JournalGridViewModel>(response);
            if (model.pagination != null)
            {
                ViewBag.TotalRows = model.pagination.totalRows;
                ViewBag.PageSize = model.pagination.pageSize;
            }
            else
            {
                ViewBag.TotalRows = 0;
                ViewBag.PageSize = 0;
            }
            return PartialView("~/Views/Employees/Journal/_JournalReport.cshtml", model);
        }
        public ActionResult ExportData(JournalDataRequest request)
        {
            var manager = new JournalManager();
            var model = new JournalGridViewModel();
            request.sortBy = "lastActionDateString";
            request.sortOrder = "DESC";
            request.itemType = "";
            request.searchText = "";
            request.pagination = new PaginationDataRequest();
            request.pagination.pageNo = 1;
            request.pagination.pageSize = _ExportPageSize;
            var response = manager.GetJournalReport(request);
            if (response != "[]")
            {
                model = new JavaScriptSerializer().Deserialize<JournalGridViewModel>(response);
            }
            var fileName = "Journal_{0}_{1}_{2}.xls";
            fileName = string.Format(fileName, DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            PrintExcel(model.employeeList, fileName);
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        public void PrintExcel(List<JournalViewModel> objSource, string fileName)
        {
             WebGrid grid = new WebGrid(source: objSource, canPage: false, canSort: false);
             string gridData = grid.GetHtml(
             tableStyle: "table table-responsive backcolor",
             headerStyle: "wedgrid-header",
             footerStyle: "webgrid-footer",
             alternatingRowStyle: "webgrid-alternating-row",
             rowStyle: "webgrid-row-style",
             columns: grid.Columns(
                grid.Column(columnName: "creationDate", header: "Created:"),
                grid.Column(columnName: "lastActionDate", header: "Updated:"),
                grid.Column(columnName: "item", header: "Item:"),
                grid.Column(columnName: "nature", header: "Nature:"),
                grid.Column(columnName: "title", header: "Title:"),
                grid.Column(columnName: "notes", header: "Notes:"),
                grid.Column(columnName: "status", header: "Status:")

                            )).ToString();
            Response.ClearContent();
            //give name to excel sheet.
            Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            //specify content type
            Response.ContentType = "application/excel";
            //write excel data using this method 
            Response.Write(gridData);
            Response.End();
        }
        #endregion
    }
}