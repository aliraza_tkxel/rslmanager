﻿using BHGHRModuleWeb.ViewModels;
using BusinessModule.NetworkManagers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace BHGHRModuleWeb.Controllers.Dashboard
{
    public class DashboardController : Controller
    {
        // GET: Dashboard
        public ActionResult Index()
        {
            var model = new DashboardViewModel();
            var dashboardManager = new DashboardManager();
            var response = dashboardManager.getVacancyList();
            var teamLookup = dashboardManager.GetTeamLookUp();
            var lstVacancy = new JavaScriptSerializer().Deserialize<List<VacancyViewModel>>(response);
            model.teamLookup = new JavaScriptSerializer().Deserialize<List<LookUpFields>>(teamLookup);
            model.vacancies = lstVacancy;// empModel.employees.Select(e => new VacancyViewModel() { teamName = e.team, vacancyName = e.name, vacancyId = e.employeeId }).ToList();
            //model.disciplinery = lstVacancy.Select(e => new DisciplineryViewModel() { name = e.title, disciplineryId = e.jobId, typeName = e.jobType, removalDue = e.startDate }).ToList();

            return View(model);
        }

        [HttpPost]
        public ActionResult Index(DashboardViewModel model)
        {
            return PartialView("~/Views/Dashboard/_Index.cshtml", model);
        }

        public int GetAlertCount(string boxName = "", int teamId = 0)
        {
            var manager = new DashboardManager();
            var response = manager.GetAlertCount(boxName, teamId);
            if (response == "" || boxName == "OverdueAppraisals")
            {
                return 0;
            }
            else
            {
                return int.Parse(response);
            }
        }
    }
}