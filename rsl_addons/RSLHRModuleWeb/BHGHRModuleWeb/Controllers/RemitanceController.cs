﻿using BHGHRModuleWeb.Helpers;
using BHGHRModuleWeb.ViewModels;
using BusinessModule.NetworkManagers;
using Rotativa;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace BHGHRModuleWeb.Controllers
{
    public class RemitanceController : Controller
    {
        protected readonly log4net.ILog log = log4net.LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        // GET: Remitance
        public ActionResult SendEmail(string receipentAddress, string emailBody, string receiverName, int supplierId, string subject, string processDate, int paymentTypeId, string paymentmethod)
        {
            log.Debug("Enter: Send Email");
            ViewAsPdf pdfOutput = null;
            RemitanceViewModel model = new RemitanceViewModel();
            RemitanceManager remManager = new RemitanceManager();
            try
            {
                emailBody = string.Format("Dear Sir or Madam,<br><br>Please find attached the remittance advice from our payment run date {0}.<br><br>Funds will be in your account in the next 3-5 working days.<br><br>If you have any queries, please contact payments@broadlandgroup.org.<br><br>Kind Regards <br><br>The Finance Team<br>Broadland Housing Association <br>NCFC, The Jarrold Stand Carrow Road,<br>  Norwich, NR1 1HU", processDate);
                log.Debug("Enter: get Invoice Detail");
                var invoices = remManager.getInvoiceDetail(supplierId, processDate, paymentTypeId);
                model.invoices = new JavaScriptSerializer().Deserialize<List<InvoiceDetailViewModel>>(invoices);

                log.Debug("Enter: get Supplier Data");
                var org = remManager.getSupplierData(supplierId);
                model.organization = new JavaScriptSerializer().Deserialize<OrganizationViewModel>(org);

                model.paymentMethod = paymentmethod;
                model.receiverName = receiverName;
                model.processDate = processDate;
                model.paymentMethod = paymentmethod;

                pdfOutput = new ViewAsPdf("~/Views/Remitance/Remitance_PDF.cshtml", model)
                {
                    FileName = "Remittance_Advice.pdf",
                    CustomSwitches = "--footer-spacing \"5\" --footer-font-size \"8\" --footer-center \"Page [page] of [toPage]\" --footer-right \"PDF Created " + DateTime.Now.ToString() + "\" ",
                    PageMargins = { Top = 15, Bottom = 20 },
                    PageSize = Rotativa.Options.Size.A4
                };

                //convert the pdf into array of bytes 
                byte[] bytes = pdfOutput.BuildPdf(ControllerContext);
                string fileName = string.Format("DeviceActivationRequest_{0}.pdf", DateTime.Now.ToString("dd_MMM_yy_hh:mm"));
                bool response = false;


                log.Debug("Enter: going to send email");
                response = EmailHelper.SendEmail(new MemoryStream(bytes), receipentAddress, emailBody, receiverName, subject);

                log.Debug("Return: View");
                return View("~/Views/Remitance/Remitance_PDF.cshtml", model);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Invoice Detail--------------------------");
                log.Error(ex.ToString());
                return View();
            }
        }
        public ActionResult PrintRemitanceAdvice(string receiverName, int supplierId, string processDate, int paymentTypeId, string paymentmethod)
        {
            RemitanceViewModel model = new RemitanceViewModel();
            RemitanceManager remManager = new RemitanceManager();
            try
            {
                log.Debug("Enter: get Invoice Detail");
                var invoices = remManager.getInvoiceDetail(supplierId, processDate, paymentTypeId);
                model.invoices = new JavaScriptSerializer().Deserialize<List<InvoiceDetailViewModel>>(invoices);

                log.Debug("Enter: get Supplier Data");

                var org = remManager.getSupplierData(supplierId);
                model.organization = new JavaScriptSerializer().Deserialize<OrganizationViewModel>(org);

                model.paymentMethod = paymentmethod;
                model.receiverName = receiverName;
                model.processDate = processDate;
                model.paymentMethod = paymentmethod;
                log.Debug("Return: View");
                return new ViewAsPdf("~/Views/Remitance/RemitancePrint.cshtml", model)
                {
                    FileName = "Remittance_Advice.pdf",
                    CustomSwitches = "--footer-spacing \"5\" --footer-font-size \"8\" --footer-center \"Page [page] of [toPage]\" --footer-right \"PDF Created " + DateTime.Now.ToString() + "\" ",
                    PageMargins = { Top = 15, Bottom = 20 },
                    PageSize = Rotativa.Options.Size.A5

                };
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Invoice Detail--------------------------");
                log.Error(ex.StackTrace);
                return View();
            }
        }
    }
}