﻿using BusinessModule.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BHGHRModuleWeb.Controllers
{
    public class BridgeController : Controller
    {
        // GET: Bridge
        public ActionResult Index()
        {
            int userId = int.Parse(Request.QueryString["UserId"]);

            Session["UserId"] = userId;
            if (Request.QueryString["redirectTab"] != null && Request.QueryString["Module"] != null && Request.QueryString["IsDirector"] != null)
            {
                Session["Module"] = Request.QueryString["Module"].ToString();
                return RedirectToAction("EmployeeCreate", "Employees", new { id = userId, module = Request.QueryString["Module"].ToString(), redirectTab = Request.QueryString["redirectTab"].ToString(), isDirector = Request.QueryString["IsDirector"].ToString() });
            }
            else if (Request.QueryString["redirectTab"] != null && Request.QueryString["Module"] != null)
            {
                Session["Module"] = Request.QueryString["Module"].ToString();
                return RedirectToAction("EmployeeCreate", "Employees", new { id = userId, module = Request.QueryString["Module"].ToString(), redirectTab = Request.QueryString["redirectTab"].ToString() });
            }
            else if (Request.QueryString["Module"] != null)
            {
                Session["Module"] = Request.QueryString["Module"].ToString();
                return RedirectToAction("EmployeeCreate", "Employees", new { id = userId, module = Request.QueryString["Module"].ToString() });
            }
            else if (Request.QueryString["Reports"] != null)
            {
                if (Request.QueryString["Reports"].ToString() == ApplicationConstants.doi)
                {
                    Session["__Report__"] = ApplicationConstants.doi;
                    return RedirectToAction("Index", "Reports", new { report = ApplicationConstants.doi });
                }

                if (Request.QueryString["Reports"].ToString() == ApplicationConstants.groupTraining)
                {
                    Session["__Report__"] = ApplicationConstants.groupTraining;
                    return RedirectToAction("Index", "Reports", new { report = ApplicationConstants.groupTraining, HR = Request.QueryString["HR"], Director = Request.QueryString["Director"] });
                }
            }
            else
            {
                Session["Module"] = null;
            }
            return RedirectToAction("Index", "Dashboard");
        }
    }
}