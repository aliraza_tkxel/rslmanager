﻿using BHGHRModuleWeb.Helpers;
using BHGHRModuleWeb.ViewModels;
using BusinessModule.Constants;
using BusinessModule.NetworkManagers;
using BusinessModule.NetworkObjects;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BHGHRModuleWeb.Controllers
{
    public class ABaseController : Controller
    {
        protected readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public int _PageSize = 15;
        public int _ReportPageSize = 25;
        public int _ExportPageSize = 200000;
        public DateTime _FiscalYearStartDate = new DateTime(1999, 4, 1);
        public DateTime _FiscalYearEndDate = new DateTime(2000, 3, 31);

        //protected string DEFAULT_DATE_FORMAT = ConfigurationManager.AppSettings[ApplicationConstants.KeyDateFormat].ToString();
        //protected string DATE_FORMAT_FOR_MODEL = ConfigurationManager.AppSettings[ApplicationConstants.KeyDateFormatModel].ToString();
        public ABaseController()
        {

        }
        protected override void OnException(ExceptionContext filterContext)
        {
            try
            {
                if (filterContext.ExceptionHandled)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ex.StackTrace + ex.InnerException);
                HandleException(filterContext, ex.Message);
            }
        }
        private void HandleException(ExceptionContext context, string Message)
        {
            context.Result = new ViewResult
            {
                ViewData = new ViewDataDictionary(),
                ViewName = "ErrorView"
            };
            ViewBag.ErrorMessage = Message;
            context.ExceptionHandled = true;
        }

        protected override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            if (!filterContext.Controller.ToString().Contains("EmployeesController"))
            {
                Session["searchText"] = null;
                Session["requestList"] = null;
            }
            if (!filterContext.Controller.ToString().Contains("PersonalDetailController"))
            {
                Session["gifts"] = null;
            }
            if (!filterContext.Controller.ToString().Contains("ReportsController") && !filterContext.Controller.ToString().Contains("NavigationController"))
            {
                Session["__Report__"] = null;
            }
            HttpContext.Response.Cache.SetExpires(DateTime.UtcNow.AddDays(-1));
            HttpContext.Response.Cache.SetValidUntilExpires(false);
            HttpContext.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
            HttpContext.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            HttpContext.Response.Cache.SetNoStore();
            filterContext.HttpContext.Response.Cache.SetExpires(DateTime.UtcNow.AddDays(-1));
            filterContext.HttpContext.Response.Cache.SetValidUntilExpires(false);
            filterContext.HttpContext.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
            filterContext.HttpContext.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            filterContext.HttpContext.Response.Cache.SetNoStore();
            base.OnResultExecuting(filterContext);
        }
        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            var userId = GetUserIdFromSession();
            //if (objBridge.LoadUser(userId) == false)
            //{
            //    throw new Exception();
            //}
            if (userId == 0)
            {
                redirectToLoginPage();
            }
        }

        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var userId = GetUserIdFromSession();
            //if (objBridge.LoadUser(userId) == false)
            //{
            //    throw new Exception();
            //}
            if (userId == 0)
            {
                redirectToLoginPage();
            }
        }
        /// <summary>
        /// Get user id from session
        /// </summary>
        /// <returns>UserId</returns>
        /// <exception cref="Exception">If Session variable is not set then set UserId = 0</exception>
        protected int GetUserIdFromSession()
        {
            //TODO:
            int userId = 0;
            try
            {
                userId = int.Parse(Session["UserId"].ToString());
            }
            catch
            {
                userId = 0;
            }
            return userId;
        }

        protected int[] StringToIntList(string str)
        {
            List<int> myIntegers = new List<int>();
            Array.ForEach(str.Split(",".ToCharArray()), s =>
            {
                int currentInt;
                if (Int32.TryParse(s, out currentInt))
                    myIntegers.Add(currentInt);
            });
            return myIntegers.ToArray();
        }

        protected string GetLoginUserName()
        {
            //TODO:
            return GetEmployeeNameById(GetUserIdFromSession());
        }

        protected string GetEmployeeNameById(int id)
        {
            //TODO:
            var manager = new PersonalDetailManager();
            var request = new GetPersonalDetailDataRequest();
            request.employeeId = id;
            var response = manager.getPersonalDetail(request);
            PersonalDetailViewModel model = new JavaScriptSerializer().Deserialize<PersonalDetailViewModel>(response);
            return $"{model.employeeDetail.firstName} {model.employeeDetail.lastName}";
        }

        public void ExportXLS(object objSource, string fileName)
        {
            GridView gv = new GridView();
            gv.DataSource = objSource;
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            Response.ContentType = "application/ms-excel";
            Response.Charset = "";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            gv.RenderControl(htw);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }

        public string getFileNameFromPath(string path)
        {
            if (path != "")
            {
                var arrPath = path.Split('/');
                return arrPath[arrPath.Length - 1];
            }
            return "";
        }

        public bool IsTeam()
        {
            var isTeam = false;
            if (TempData.ContainsKey("isTeam"))
                isTeam = bool.Parse(TempData["isTeam"].ToString());
            TempData.Keep("isTeam");

            return isTeam;
        }


        public bool IsHR()
        {
            var manager = new JobDetailManager();
            var loggedInUserTeam = new JavaScriptSerializer().Deserialize<string>(manager.getLoggedInUserTeam(GetUserIdFromSession()));
            var isHR = false;
            if (loggedInUserTeam.ToString() == ApplicationConstants.HRServices)
            {
                isHR = true;
            }

            return isHR;
        }

        public DateTime GetDateTimeFromString(string dt)
        {
            string[] formats = { "dd/MM/yyyy" };
            var dateTime = DateTime.ParseExact(dt, formats, new CultureInfo("en-US"), DateTimeStyles.None);
            return dateTime;
        }

        #region "Redirect To Login Page"
        public void redirectToLoginPage()
        {
            HttpContext.Response.Redirect(GeneralHelper.LoginPath, true);
        }
        #endregion
    }
}