﻿using BHGHRModuleWeb.ViewModels;
using BusinessModule.NetworkManagers;
using BusinessModule.NetworkObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace BHGHRModuleWeb.Controllers.Common
{
    public class NavigationController : ABaseController
    {
        // GET: Navigation
        public ActionResult GetTopNavigationMenu()
        {
            var navigationManager = new NavigationManager();
            var menuDataRequest = new MenuDataRequest();
            menuDataRequest.userId = GetUserIdFromSession();
            if (Session["Module"] != null)
            {
                menuDataRequest.moduleName = Session["Module"].ToString();
            }
            var response = navigationManager.getTopNavigationMenu(menuDataRequest);
            NavigationViewModel model = new JavaScriptSerializer().Deserialize<NavigationViewModel>(response);
            model.CurrentModule = menuDataRequest.moduleName;
            if (Request.QueryString["isTeam"] != null && Convert.ToBoolean(Request.QueryString["isTeam"].ToString()))
            {
                model.menues = new List<MenuViewModel>();
            }
            return PartialView("~/Views/Shared/_Navigation.cshtml", model);
        }
    }
}