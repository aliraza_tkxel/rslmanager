﻿using AutoMapper;
using BHGHRModuleWeb.Helpers;
using BHGHRModuleWeb.ViewModels;
using BHGHRModuleWeb.ViewModels.Reports;
using BusinessModule.Constants;
using BusinessModule.NetworkManagers;
using BusinessModule.NetworkObjects;
using Rotativa;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace BHGHRModuleWeb.Controllers.Reports
{
    public class ReportsController : ABaseController
    {
        #region >>> helpers <<<

        private SelectList ReportLookup()
        {
            List<SelectViewModel> reportsSelect = new List<SelectViewModel>
            {
                new SelectViewModel(){ Name = "Establishment Report", ID="Establishment" },
                new SelectViewModel(){ Name = "Sickness Report", ID="Sickness" },
                new SelectViewModel(){ Name = "Leaver Report", ID="Leaver" },
                new SelectViewModel(){ Name = "Health Report", ID="Health" },
                new SelectViewModel(){ Name = "Annual Leave Report", ID="AnnualLeave" },
                new SelectViewModel(){ Name = "New Starter Report", ID="NewStarter" },
                new SelectViewModel(){ Name = "Remuneration Report", ID="Remuneration" },
                new SelectViewModel(){ Name = "Pay Point Report", ID="PayPoint" },
                new SelectViewModel(){ Name = "Declaration of Interests Report", ID="DeclarationOfInterests" },
                new SelectViewModel(){ Name = "Training Approval List", ID="GroupTraining" },
				new SelectViewModel(){ Name = "Mandatory Training Report", ID="Training" },
                new SelectViewModel(){ Name = "Gift & Hospitality", ID="GiftHospitality" }
            };
            reportsSelect = reportsSelect.OrderBy(e => e.Name).ToList();
            return new SelectList(reportsSelect, "ID", "Name");
        }

        public SelectList GetDisabilityTypes()
        {
            List<SelectListItem> disabilityTypes = new List<SelectListItem>
            {
                new SelectListItem(){ Text = "All Types", Value="All" ,Selected=true },
                new SelectListItem(){ Text = "Disability or Health Problem", Value="1" },
                new SelectListItem(){ Text = "Learning Difficulties", Value="2" }
            };

            return new SelectList(disabilityTypes, "Value", "Text");
        }

        public SelectList GetEmployeeTypes()
        {
            List<SelectListItem> employeeTypes = new List<SelectListItem>
            {
                new SelectListItem(){ Text = "All Employees", Value="All" ,Selected=true },
                new SelectListItem(){ Text = "Current Employees", Value="Current" },
                new SelectListItem(){ Text = "Previous Employees", Value="Previous" }
            };

            return new SelectList(employeeTypes, "Value", "Text");
        }

        private EstablishmentGridViewModel getEstablishmentReportPopulate()
        {
            int status = 1;
            string searchText = string.Empty;
            string pageText = string.Empty;
            if (Request["txtSearchText"] != null)
            {
                searchText = Request["txtSearchText"].ToString();
                ViewBag.searchText = searchText;
                Session["searchText"] = searchText;
            }
            if (Request["rdBtnstatus"] != null)
            {
                status = Convert.ToInt32(Request["rdBtnstatus"].ToString());
            }
            var employeesManager = new ReportsManager();
            var employeesDataRequest = new EstablishmentDataRequest();
            employeesDataRequest.pagination = new BusinessModule.NetworkObjects.PaginationDataRequest();
            if (Request["fromDate"] != null && Request["fromDate"] != "")
            {
                employeesDataRequest.fromDate = Request["fromDate"].ToString();
            }
            if (Request["toDate"] != null && Request["toDate"] != "")
            {
                employeesDataRequest.toDate = Request["toDate"].ToString();
            }
            if (Request["directorate"] != null && Request["directorate"] != "" && Request["directorate"] != "0" && Request["directorate"] != "undefined")
            {
                employeesDataRequest.directorate = Convert.ToInt32(Request["directorate"]);
            }

            if (Request["team"] != null && Request["team"] != "" && Request["team"] != "0" && Request["team"] != "undefined")
            {
                employeesDataRequest.team = Convert.ToInt32(Request["team"]);
            }

            if (Request["employeeType"] != null && Request["employeeType"] != "")
            {
                employeesDataRequest.employeeType = Request["employeeType"].ToString();
            }
            employeesDataRequest.searchText = searchText;
            if (Request["fullTime"] != null && Request["fullTime"] != "" && Request["fullTime"] != "undefined")
            {
                employeesDataRequest.fullTime = int.Parse(Request["fullTime"].ToString());
            }

            employeesDataRequest.sortBy = "fullName";
            employeesDataRequest.sortOrder = "ASC";
            int pageNo = 1;
            if (HttpContext.Request.QueryString["page"] != null && HttpContext.Request.QueryString["page"] != "0")
            {
                pageNo = Convert.ToInt32(HttpContext.Request.QueryString["page"]);
                ViewBag.pageText = pageText;
                Session["pageText"] = pageText;
            }
            if (HttpContext.Request.QueryString["sort"] != null && HttpContext.Request.QueryString["sortdir"] != null)
            {
                employeesDataRequest.sortBy = HttpContext.Request.QueryString["sort"].ToString();
                employeesDataRequest.sortOrder = HttpContext.Request.QueryString["sortdir"].ToString();
            }
            employeesDataRequest.pagination.pageSize = _ReportPageSize;
            employeesDataRequest.pagination.pageNo = pageNo;
            var response = employeesManager.getEstablishmentReport(employeesDataRequest);
            EstablishmentGridViewModel model = new JavaScriptSerializer().Deserialize<EstablishmentGridViewModel>(response);
            if (model.establishmentList.pagination != null)
            {
                ViewBag.TotalRows = model.establishmentList.pagination.totalRows;
                ViewBag.PageSize = model.establishmentList.pagination.pageSize;
            }
            else
            {
                ViewBag.TotalRows = 0;
            }

            model.directorate = employeesDataRequest.directorate ?? 0;
            model.employeeType = employeesDataRequest.employeeType;
            model.fullTime = employeesDataRequest.fullTime;
            model.employeeTypes = GetEmployeeTypes();
            return model;
        }

        private RemunerationGridViewModel GetRemunerationReportPopulate()
        {
            int status = 1;
            string searchText = string.Empty;
            if (Request["txtSearchText"] != null)
            {
                searchText = Request["txtSearchText"].ToString();
                ViewBag.searchText = searchText;
            }
            if (Request["rdBtnstatus"] != null)
            {
                status = Convert.ToInt32(Request["rdBtnstatus"].ToString());
            }
            var manager = new ReportsManager();
            var dataRequest = new RemunerationDataRequest();
            dataRequest.pagination = new BusinessModule.NetworkObjects.PaginationDataRequest();
            if (Request["fromDate"] != null && Request["fromDate"] != "")
            {
                dataRequest.fromDate = Request["fromDate"].ToString();
            }
            if (Request["toDate"] != null && Request["toDate"] != "")
            {
                dataRequest.toDate = Request["toDate"].ToString();
            }
            if (Request["directorate"] != null && Request["directorate"] != "" && Request["directorate"] != "0" && Request["directorate"] != "undefined")
            {
                dataRequest.directorate = Convert.ToInt32(Request["directorate"]);
            }
            if (Request["team"] != null && Request["team"] != "" && Request["team"] != "0" && Request["team"] != "undefined")
            {
                dataRequest.team = Convert.ToInt32(Request["team"]);
            }
            if (Request["employeeType"] != null && Request["employeeType"] != "")
            {
                dataRequest.employeeType = Request["employeeType"].ToString();
            }
            dataRequest.searchText = searchText;
            dataRequest.sortBy = "fullName";
            dataRequest.sortOrder = "ASC";
            int pageNo = 1;
            if (HttpContext.Request.QueryString["page"] != null && HttpContext.Request.QueryString["page"] != "0")
            {
                pageNo = Convert.ToInt32(HttpContext.Request.QueryString["page"]);
            }
            if (HttpContext.Request.QueryString["sort"] != null && HttpContext.Request.QueryString["sortdir"] != null)
            {
                dataRequest.sortBy = HttpContext.Request.QueryString["sort"].ToString();
                dataRequest.sortOrder = HttpContext.Request.QueryString["sortdir"].ToString();
            }
            dataRequest.pagination.pageSize = _ReportPageSize;
            dataRequest.pagination.pageNo = pageNo;
            var response = manager.GetRemunerationReport(dataRequest);
            RemunerationGridViewModel model = new JavaScriptSerializer().Deserialize<RemunerationGridViewModel>(response);
            if (model.statementList.pagination != null)
            {
                ViewBag.TotalRows = model.statementList.pagination.totalRows;
                ViewBag.PageSize = model.statementList.pagination.pageSize;
            }
            else
            {
                ViewBag.TotalRows = 0;
            }
            model.employeeTypes = GetEmployeeTypes();
            return model;
        }

        private SicknessGridViewModel getSicknessReportPopulate()
        {
            int status = 1;
            bool isLongTerm = false;
            string searchText = string.Empty;
            ViewBag.isLongTerm = false;
            if (Request["txtSearchText"] != null)
            {
                searchText = Request["txtSearchText"].ToString();
                ViewBag.searchText = searchText;
                
            }
            if (Request["isLongTerm"] != null)
            {
                isLongTerm = Convert.ToBoolean(Request["isLongTerm"]);
                ViewBag.isLongTerm = isLongTerm;
            }
            if (Request["rdBtnstatus"] != null)
            {
                status = Convert.ToInt32(Request["rdBtnstatus"].ToString());
            }
            var employeesManager = new ReportsManager();
            var employeesDataRequest = new SicknessDataRequest();
            
            employeesDataRequest.pagination = new BusinessModule.NetworkObjects.PaginationDataRequest();
            if (Request["fromDate"] != null && Request["fromDate"] != "")
            {
                employeesDataRequest.fromDate = Request["fromDate"].ToString();
            }
            if (Request["toDate"] != null && Request["toDate"] != "")
            {
                employeesDataRequest.toDate = Request["toDate"].ToString();
            }
            if (Request["reasonId"] != null && Request["reasonId"] != "")
            {
                employeesDataRequest.reasonId = Convert.ToInt32(Request["reasonId"]);
            }
            if (Request["directorate"] != null && Request["directorate"] != "" && Request["directorate"] != "0" && Request["directorate"] != "undefined")
            {
                employeesDataRequest.directorate = Convert.ToInt32(Request["directorate"]);
            }
            if (Request["team"] != null && Request["team"] != "" && Request["team"] != "0" && Request["team"] != "undefined")
            {
                employeesDataRequest.team = Convert.ToInt32(Request["team"]);
            }
            if (Request["employeeType"] != null && Request["employeeType"] != "")
            {
                employeesDataRequest.employeeType = Request["employeeType"].ToString();
            }
            else
            {
                employeesDataRequest.employeeType = "All";
            }
            employeesDataRequest.searchText = searchText;
            employeesDataRequest.isLongTerm = isLongTerm;
            employeesDataRequest.sortBy = "fullName";
            employeesDataRequest.sortOrder = "ASC";
            int pageNo = 1;
            if (HttpContext.Request.QueryString["page"] != null && HttpContext.Request.QueryString["page"] != "0")
            {
                pageNo = Convert.ToInt32(HttpContext.Request.QueryString["page"]);
            }
            if (HttpContext.Request.QueryString["sort"] != null && HttpContext.Request.QueryString["sortdir"] != null)
            {
                employeesDataRequest.sortBy = HttpContext.Request.QueryString["sort"].ToString();
                employeesDataRequest.sortOrder = HttpContext.Request.QueryString["sortdir"].ToString();
            }
            employeesDataRequest.pagination.pageSize = ViewBag.PageSize = _ReportPageSize;
            employeesDataRequest.pagination.pageNo = pageNo;
            var response = employeesManager.getSicknessReport(employeesDataRequest);
            SicknessGridViewModel model = new JavaScriptSerializer().Deserialize<SicknessGridViewModel>(response);
            if (model.sicknessList.pagination != null)
            {
                ViewBag.TotalRows = model.sicknessList.pagination.totalRows;
                ViewBag.PageSize = model.sicknessList.pagination.pageSize;
            }
            else
            {
                ViewBag.TotalRows = 0;
            }
            model.directorate = employeesDataRequest.directorate ?? 0;
            model.team = employeesDataRequest.team ?? 0;
            model.employeeTypes = GetEmployeeTypes();
            return model;
        }

        private DeclarationOfInterestsReportViewModel getDoIReportPopulate()
        {
            string searchText = string.Empty;
            if (Request["txtSearchText"] != null)
            {
                searchText = Request["txtSearchText"].ToString();
                ViewBag.searchText = searchText;
                Session["searchText"] = searchText;
            }
            var manager = new ReportsManager();
            var dataRequest = new DeclarationOfInterestsDataRequest();
            dataRequest.pagination = new BusinessModule.NetworkObjects.PaginationDataRequest();
            if (Session["__Report__"] != null && Session["__Report__"].ToString() == "DeclarationOfInterests")
            {
                dataRequest.lineManagerId = GetUserIdFromSession();
            }

            if (Request["year"] != null && Request["year"] != "" && Request["year"] != "undefined")
            {
                dataRequest.fiscalYearId = Convert.ToInt32(Request["year"]);
            }
            if (Request["directorate"] != null && Request["directorate"] != "" && Request["directorate"] != "0" && Request["directorate"] != "undefined")
            {
                dataRequest.directorate = Convert.ToInt32(Request["directorate"]);
            }

            if (Request["team"] != null && Request["team"] != "" && Request["team"] != "0" && Request["team"] != "undefined")
            {
                dataRequest.teamId = Convert.ToInt32(Request["team"]);
            }
            if (Request["status"] != null && Request["status"] != "")
            {
                dataRequest.status = Convert.ToInt32(Request["status"]);
            }
            else
            {
                dataRequest.status = 1;
            }


            dataRequest.searchText = searchText;
            dataRequest.sortBy = "employeeName";
            dataRequest.sortOrder = "desc";
            int pageNo = 1;
            if (HttpContext.Request.QueryString["page"] != null && HttpContext.Request.QueryString["page"] != "0")
            {
                pageNo = Convert.ToInt32(HttpContext.Request.QueryString["page"]);
            }
            if (HttpContext.Request.QueryString["sort"] != null && HttpContext.Request.QueryString["sortdir"] != null)
            {
                dataRequest.sortBy = HttpContext.Request.QueryString["sort"].ToString();
                if (dataRequest.sortBy == "submitted")
                {
                    dataRequest.sortBy = "submittedDate";
                }
                dataRequest.sortOrder = HttpContext.Request.QueryString["sortdir"].ToString();
            }
            dataRequest.pagination.pageSize = ViewBag.PageSize = _ReportPageSize;
            dataRequest.pagination.pageNo = pageNo;
            var response = manager.GetDOIReport(dataRequest);
            DeclarationOfInterestsReportViewModel model = new JavaScriptSerializer().Deserialize<DeclarationOfInterestsReportViewModel>(response);
            if (model.declarationOfInterestsListing.pagination != null)
            {
                ViewBag.TotalRows = model.declarationOfInterestsListing.pagination.totalRows;
                ViewBag.PageSize = model.declarationOfInterestsListing.pagination.pageSize;
            }
            else
            {
                ViewBag.TotalRows = 0;
            }

            model.teamId = dataRequest.teamId ?? 0;
            model.userId = GetUserIdFromSession();
            model.status = dataRequest.status;
            return model;
        }

        private AnnualLeaveGridViewModel GetAnnualLeaveReportPopulate()
        {
            string searchText = string.Empty;
            if (Request["txtSearchText"] != null)
            {
                searchText = Request["txtSearchText"].ToString();
                ViewBag.searchText = searchText;
                Session["searchText"] = searchText;
            }
            var manager = new ReportsManager();
            var dataRequest = new AnnualLeaveDataRequest();
            dataRequest.pagination = new BusinessModule.NetworkObjects.PaginationDataRequest();

            if (Request["year"] != null && Request["year"] != "")
            {
                dataRequest.year = Request["year"].ToString();
            }
            else
            {
                dataRequest.year = DateTime.Now.Year.ToString();
            }
            if (Request["directorate"] != null && Request["directorate"] != "" && Request["directorate"] != "0" && Request["directorate"] != "undefined")
            {
                var d = Request["directorate"];
                dataRequest.directorate = Convert.ToInt32(Request["directorate"]);
            }
            if (Request["team"] != null && Request["team"] != "" && Request["team"] != "0" && Request["team"] != "undefined")
            {
                var d = Request["team"];
                dataRequest.team = Convert.ToInt32(Request["team"]);
            }
            dataRequest.searchText = searchText;
            dataRequest.sortBy = "fullName";
            dataRequest.sortOrder = "ASC";
            int pageNo = 1;
            if (HttpContext.Request.QueryString["page"] != null && HttpContext.Request.QueryString["page"] != "0")
            {
                pageNo = Convert.ToInt32(HttpContext.Request.QueryString["page"]);
            }
            if (HttpContext.Request.QueryString["sort"] != null && HttpContext.Request.QueryString["sortdir"] != null)
            {
                dataRequest.sortBy = HttpContext.Request.QueryString["sort"].ToString();
                dataRequest.sortOrder = HttpContext.Request.QueryString["sortdir"].ToString();
            }
            dataRequest.pagination.pageSize = ViewBag.PageSize = _ReportPageSize;
            dataRequest.pagination.pageNo = pageNo;
            if (dataRequest.year == null || dataRequest.year == "")
                dataRequest.year = DateTime.Now.Year.ToString();
            var response = manager.GetAnnualLeaveReport(dataRequest);
            AnnualLeaveGridViewModel model = new JavaScriptSerializer().Deserialize<AnnualLeaveGridViewModel>(response);
            if (model.annualLeaveListing.pagination != null)
            {
                ViewBag.TotalRows = model.annualLeaveListing.pagination.totalRows;
                ViewBag.PageSize = model.annualLeaveListing.pagination.pageSize;
            }
            else
            {
                ViewBag.TotalRows = 0;
            }
            model.directorate = dataRequest.directorate ?? 0;
            model.team = dataRequest.team ?? 0;
            model.Year = int.Parse(dataRequest.year);
            return model;
        }
        public JsonResult TeamSelectListJson(string Origin, string Target, string Value)
        {
            var manager = new MyTeamManager();
            var response = manager.GetTeamsByDirectorate(int.Parse(Value));
            AnnualLeaveGridViewModel model = new JavaScriptSerializer().Deserialize<AnnualLeaveGridViewModel>(response);
            return Json(model.teamLookUps);
        }
        public JsonResult EmployeeSelectListJson()
        {
            var manager = new MyTeamManager();
            int teamId = Convert.ToInt32(Request["team"]);
            var response = manager.GetEmployeesByTeam(teamId);
            GroupTrainingApprovalReportViewModel model = new JavaScriptSerializer().Deserialize<GroupTrainingApprovalReportViewModel>(response);
            return Json(model.employees, JsonRequestBehavior.AllowGet);
        }
        public JsonResult TeamSelectListJsonGroupTraining()
        {
            var manager = new MyTeamManager();
            int directorateId = Convert.ToInt32(Request["directorate"]);
            var response = manager.GetTeamsByDirectorate(directorateId);
            GroupTrainingApprovalReportViewModel model = new JavaScriptSerializer().Deserialize<GroupTrainingApprovalReportViewModel>(response);
            return Json(model.teams, JsonRequestBehavior.AllowGet);
        }
        private LeaverGridViewModel GetLeaverReportPopulate()
        {
            string searchText = string.Empty;
            if (Request["txtSearchText"] != null)
            {
                searchText = Request["txtSearchText"].ToString();
                ViewBag.searchText = searchText;
                Session["searchText"] = searchText;
            }
            var manager = new ReportsManager();
            var dataRequest = new LeaverDataRequest();
            dataRequest.pagination = new BusinessModule.NetworkObjects.PaginationDataRequest();
            if (Request["fromDate"] != null && Request["fromDate"] != "")
            {
                dataRequest.fromDate = Request["fromDate"].ToString();
            }
            if (Request["toDate"] != null && Request["toDate"] != "")
            {
                dataRequest.toDate = Request["toDate"].ToString();
            }
            if (Request["directorate"] != null && Request["directorate"] != "" && Request["directorate"] != "0" && Request["directorate"] != "undefined")
            {
                dataRequest.directorate = Convert.ToInt32(Request["directorate"]);
            }
            if (Request["team"] != null && Request["team"] != "" && Request["team"] != "0" && Request["team"] != "undefined")
            {
                dataRequest.team = Convert.ToInt32(Request["team"]);
            }
            dataRequest.searchText = searchText;
            dataRequest.sortBy = "fullName";
            dataRequest.sortOrder = "ASC";
            int pageNo = 1;
            if (HttpContext.Request.QueryString["page"] != null && HttpContext.Request.QueryString["page"] != "0")
            {
                pageNo = Convert.ToInt32(HttpContext.Request.QueryString["page"].ToString());
            }
            if (HttpContext.Request.QueryString["sort"] != null && HttpContext.Request.QueryString["sortdir"] != null)
            {
                dataRequest.sortBy = HttpContext.Request.QueryString["sort"].ToString();
                dataRequest.sortOrder = HttpContext.Request.QueryString["sortdir"].ToString();
            }
            dataRequest.pagination.pageSize = ViewBag.PageSize = _ReportPageSize;
            dataRequest.pagination.pageNo = pageNo;
            var response = manager.GetLeaverReport(dataRequest);
            LeaverGridViewModel model = new JavaScriptSerializer().Deserialize<LeaverGridViewModel>(response);
            if (model.leaverReportListing.pagination != null)
            {
                ViewBag.TotalRows = model.leaverReportListing.pagination.totalRows;
                ViewBag.PageSize = model.leaverReportListing.pagination.pageSize;
            }
            else
            {
                ViewBag.TotalRows = 0;
            }
            return model;
        }

        private NewStarterGridViewModel getNewStarterReportPopulate()
        {
            string searchText = string.Empty;
            if (Request["txtSearchText"] != null)
            {
                searchText = Request["txtSearchText"].ToString();
                ViewBag.searchText = searchText;
                Session["searchText"] = searchText;
            }
            var manager = new ReportsManager();
            var dataRequest = new NewStarterDataRequest();
            dataRequest.pagination = new BusinessModule.NetworkObjects.PaginationDataRequest();
            if (Request["fromDate"] != null && Request["fromDate"] != "")
            {
                dataRequest.fromDate = Request["fromDate"].ToString();
            }
            if (Request["toDate"] != null && Request["toDate"] != "")
            {
                dataRequest.toDate = Request["toDate"].ToString();
            }
            if (Request["directorate"] != null && Request["directorate"] != "" && Request["directorate"] != "0" && Request["directorate"] != "undefined")
            {
                dataRequest.directorate = Convert.ToInt32(Request["directorate"]);
            }
            if (Request["team"] != null && Request["team"] != "" && Request["team"] != "0" && Request["team"] != "undefined")
            {
                dataRequest.team = Convert.ToInt32(Request["team"]);
            }
            if (Request["employeeType"] != null && Request["employeeType"] != "")
            {
                dataRequest.employeeType = Request["employeeType"].ToString();
            }
            else
            {
                dataRequest.employeeType = "All";
            }
            dataRequest.searchText = searchText;
            if (Session["searchText"] != null)
            {
                dataRequest.searchText = Session["searchText"].ToString();
            }
            dataRequest.sortBy = "fullName";
            dataRequest.sortOrder = "ASC";
            int pageNo = 1;
            if (HttpContext.Request.QueryString["page"] != null && HttpContext.Request.QueryString["page"] != "0")
            {
                pageNo = Convert.ToInt32(HttpContext.Request.QueryString["page"]);
            }
            if (HttpContext.Request.QueryString["sort"] != null && HttpContext.Request.QueryString["sortdir"] != null)
            {
                dataRequest.sortBy = HttpContext.Request.QueryString["sort"].ToString();
                dataRequest.sortOrder = HttpContext.Request.QueryString["sortdir"].ToString();
            }
            dataRequest.pagination.pageSize = ViewBag.PageSize = _ReportPageSize;
            dataRequest.pagination.pageNo = pageNo;
            var response = manager.GetNewStarterReport(dataRequest);
            NewStarterGridViewModel model = new JavaScriptSerializer().Deserialize<NewStarterGridViewModel>(response);
            if (model.newStarterList.pagination != null)
            {
                ViewBag.TotalRows = model.newStarterList.pagination.totalRows;
                ViewBag.PageSize = model.newStarterList.pagination.pageSize;
            }
            else
            {
                ViewBag.TotalRows = 0;
            }
            model.employeeTypes = GetEmployeeTypes();
            return model;
        }

        private PayPointGridViewModel getPayPointReportPopulate()
        {
            string searchText = string.Empty;
            if (Request["txtSearchText"] != null)
            {
                searchText = Request["txtSearchText"].ToString();
                ViewBag.searchText = searchText;
                Session["searchText"] = searchText;
            }
            var manager = new ReportsManager();
            var dataRequest = new PayPointDataRequest();
            dataRequest.pagination = new BusinessModule.NetworkObjects.PaginationDataRequest();
            if (Request["year"] != null && Request["year"] != "" && Request["year"] != "undefined" && Request["year"] != "0")
            {
                dataRequest.fiscalYearId = 1;
                dataRequest.fiscalYearStartDate = Request["year"];
                var date = GetDateTimeFromString(Request["year"]);
                dataRequest.fiscalYearEndDate = new DateTime(date.Year + 1, 3, 31).ToShortDateString();
            }
            if (Request["directorate"] != null && Request["directorate"] != "" && Request["directorate"] != "0" && Request["directorate"] != "undefined")
            {
                dataRequest.directorate = Convert.ToInt32(Request["directorate"]);
            }
            if (Request["team"] != null && Request["team"] != "" && Request["team"] != "0" && Request["team"] != "undefined")
            {
                dataRequest.teamId = Convert.ToInt32(Request["team"]);
            }
            if (Request["status"] != null && Request["status"] != "")
            {
                dataRequest.status = Convert.ToInt32(Request["status"]);
            }
            else
            {
                dataRequest.status = 0;
            }

            if (Request["notSubmitted"] != null && Request["notSubmitted"] != "" && Request["notSubmitted"] != "0" && Request["notSubmitted"] != "undefined")
            {
                dataRequest.notSubmitted = Convert.ToBoolean(Request["notSubmitted"]);
            }
            dataRequest.searchText = searchText;
            dataRequest.sortBy = "reviewDate";
            dataRequest.sortOrder = "ASC";
            int pageNo = 1;
            if (HttpContext.Request.QueryString["page"] != null && HttpContext.Request.QueryString["page"] != "0" && HttpContext.Request.QueryString["page"] != "")
            {
                pageNo = Convert.ToInt32(HttpContext.Request.QueryString["page"]);
            }
            if (HttpContext.Request.QueryString["sort"] != null && HttpContext.Request.QueryString["sortdir"] != null)
            {
                dataRequest.sortBy = HttpContext.Request.QueryString["sort"].ToString();
                dataRequest.sortOrder = HttpContext.Request.QueryString["sortdir"].ToString();
            }
            dataRequest.pagination.pageSize = ViewBag.PageSize = _ReportPageSize;
            dataRequest.pagination.pageNo = pageNo;
            var response = manager.GetPayPointReport(dataRequest);
            PayPointGridViewModel model = new JavaScriptSerializer().Deserialize<PayPointGridViewModel>(response);
            if (model != null)
            {
                foreach (PayPointViewModel payPoint in model.PayPointList.payPoints)
                {
                    payPoint.isCeo = 3;
                    if ((model.ceoId == GetUserIdFromSession() || payPoint.execDirectorId == GetUserIdFromSession()) && 
                        (payPoint.status != ApplicationConstants.declined || payPoint.status != ApplicationConstants.authorized))
                    {
                        if (model.ceoId == GetUserIdFromSession() && payPoint.execDirectorId == GetUserIdFromSession())
                        {
                            if (payPoint.status == ApplicationConstants.supported)
                            {
                                payPoint.isCeo = 1;
                            }
                            else
                            {
                                payPoint.isCeo = 2;
                            }
                        }
                        else if (model.ceoId == GetUserIdFromSession())
                        {
                            payPoint.isCeo = 1;
                        }
                        else if (payPoint.execDirectorId == GetUserIdFromSession())
                        {
                            payPoint.isCeo = 2;
                        }
                    }
                }

                if (model.PayPointList != null)
                {
                    model.PayPointList.payPointStatus = dataRequest.status;
                    model.PayPointList.notSubmitted = dataRequest.notSubmitted;
                }
            }
            if (model.PayPointList.pagination != null)
            {
                ViewBag.TotalRows = model.PayPointList.pagination.totalRows;
                ViewBag.PageSize = model.PayPointList.pagination.pageSize;
            }
            else
            {
                ViewBag.TotalRows = 0;
            }

            model.teamId = dataRequest.teamId ?? 0;

            var list = new List<SelectListItem>();
            var startDate = _FiscalYearStartDate;
            var endDate = _FiscalYearEndDate;
            do
            {
                list.Add(new SelectListItem() { Text = startDate.Year + " - " + endDate.Year, Value = startDate.ToShortDateString() });

                startDate = startDate.AddYears(1);
                endDate = endDate.AddYears(1);

            } while (endDate.Year <= (DateTime.Now.Year + 10));

            list.Insert(0, new SelectListItem()
            {
                Text = "Year",
                Value = "0"
            });

            model.fiscalYearLookUp = list;

            return model;
        }

        public HealthReportGridViewModel GetHealthList()
        {
            string searchText = string.Empty;
            var manager = new ReportsManager();
            var dataRequest = new HealthReportDataRequest();
            var model = new HealthReportGridViewModel();
            dataRequest.employeeId = 0;
            if (Request["directorate"] != null && Request["directorate"] != "" && Request["directorate"] != "0" && Request["directorate"] != "undefined")
            {
                dataRequest.directorate = Convert.ToInt32(Request["directorate"]);
            }
            if (Request["team"] != null && Request["team"] != "" && Request["team"] != "0" && Request["team"] != "undefined")
            {
                dataRequest.teamId = Convert.ToInt32(Request["team"]);
            }
            dataRequest.sortBy = "disablityId";
            dataRequest.sortOrder = "ASC";
            int pageNo = 1;
            if (HttpContext.Request.QueryString["page"] != null && HttpContext.Request.QueryString["page"] != "0" && HttpContext.Request.QueryString["page"] != "undefined")
            {
                pageNo = Convert.ToInt32(HttpContext.Request.QueryString["page"]);
            }
            if (HttpContext.Request.QueryString["sort"] != null && HttpContext.Request.QueryString["sortdir"] != null)
            {
                dataRequest.sortBy = HttpContext.Request.QueryString["sort"].ToString();
                dataRequest.sortOrder = HttpContext.Request.QueryString["sortdir"].ToString();
            }
            if (Request["fromDate"] != null && Request["fromDate"] != "")
            {
                dataRequest.fromDate = Request["fromDate"].ToString();
            }
            if (Request["toDate"] != null && Request["toDate"] != "")
            {
                dataRequest.toDate = Request["toDate"].ToString();
            }
            if (Request["disabilityType"] != null && Request["disabilityType"] != "")
            {
                dataRequest.disabilityType = Request["disabilityType"].ToString();
            }
            if (Request["txtSearchText"] != null)
            {
                searchText = Request["txtSearchText"].ToString();
                ViewBag.searchText = searchText;
            }
            dataRequest.searchText = searchText;
            dataRequest.pagination.pageSize = _ReportPageSize;
            dataRequest.pagination.pageNo = pageNo;
            var response = manager.GetHealthList(dataRequest);
            model = new JavaScriptSerializer().Deserialize<HealthReportGridViewModel>(response);

            if (model == null)
            {
                model = new HealthReportGridViewModel();
                model.healthReportListing.pagination = new PaginationViewModel();
                model.healthReportListing.pagination.pageSize = _PageSize;
            }

            if (model.healthReportListing.pagination == null)
            {
                model.healthReportListing.pagination = new PaginationViewModel();
                model.healthReportListing.pagination.pageSize = _PageSize;
            }
            model.fromDate = model.healthReportListing.fromDate;
            model.toDate = model.healthReportListing.toDate;
            ViewBag.TotalRows = model.healthReportListing.pagination.totalRows;
            ViewBag.PageSize = model.healthReportListing.pagination.pageSize;
            model.disabilityTypes = GetDisabilityTypes();
            return model;
        }
        //private EstablishmentGridViewModel getEstablishmentReportPopulate()
        //{
        //    int status = 1;
        //    string searchText = string.Empty;
        //    string pageText = string.Empty;
        //    if (Request["txtSearchText"] != null)
        //    {
        //        searchText = Request["txtSearchText"].ToString();
        //        ViewBag.searchText = searchText;
        //        Session["searchText"] = searchText;
        //    }
        //    if (Request["rdBtnstatus"] != null)
        //    {
        //        status = Convert.ToInt32(Request["rdBtnstatus"].ToString());
        //    }

        //    var employeesManager = new ReportsManager();
        //    var employeesDataRequest = new EstablishmentDataRequest();
        //    employeesDataRequest.pagination = new BusinessModule.NetworkObjects.PaginationDataRequest();

        //    if (Request["fromDate"] != null && Request["fromDate"] != "")
        //    {
        //        employeesDataRequest.fromDate = Request["fromDate"].ToString();
        //    }
        //    if (Request["toDate"] != null && Request["toDate"] != "")
        //    {
        //        employeesDataRequest.toDate = Request["toDate"].ToString();
        //    }
        //    if (Request["directorate"] != null && Request["directorate"] != "" && Request["directorate"] != "0" && Request["directorate"] != "undefined")
        //    {
        //        employeesDataRequest.directorate = Convert.ToInt32(Request["directorate"]);
        //    }

        //    if (Request["team"] != null && Request["team"] != "" && Request["team"] != "0" && Request["team"] != "undefined")
        //    {
        //        employeesDataRequest.team = Convert.ToInt32(Request["team"]);
        //    }

        //    if (Request["employeeType"] != null && Request["employeeType"] != "")
        //    {
        //        employeesDataRequest.employeeType = Request["employeeType"].ToString();
        //    }
        //    employeesDataRequest.searchText = searchText;
        //    if (Request["fullTime"] != null && Request["fullTime"] != "" && Request["fullTime"] != "undefined")
        //    {
        //        employeesDataRequest.fullTime = int.Parse(Request["fullTime"].ToString());
        //    }

        //    employeesDataRequest.sortBy = "fullName";
        //    employeesDataRequest.sortOrder = "ASC";
        //    int pageNo = 1;
        //    if (HttpContext.Request.QueryString["page"] != null && HttpContext.Request.QueryString["page"] != "0")
        //    {
        //        pageNo = Convert.ToInt32(HttpContext.Request.QueryString["page"]);
        //        ViewBag.pageText = pageText;
        //        Session["pageText"] = pageText;
        //    }
        //    if (HttpContext.Request.QueryString["sort"] != null && HttpContext.Request.QueryString["sortdir"] != null)
        //    {
        //        employeesDataRequest.sortBy = HttpContext.Request.QueryString["sort"].ToString();
        //        employeesDataRequest.sortOrder = HttpContext.Request.QueryString["sortdir"].ToString();
        //    }
        //    employeesDataRequest.pagination.pageSize = _ReportPageSize;
        //    employeesDataRequest.pagination.pageNo = pageNo;
        //    var response = employeesManager.getEstablishmentReport(employeesDataRequest);
        //    EstablishmentGridViewModel model = new JavaScriptSerializer().Deserialize<EstablishmentGridViewModel>(response);
        //    if (model.establishmentList.pagination != null)
        //    {
        //        ViewBag.TotalRows = model.establishmentList.pagination.totalRows;
        //        ViewBag.PageSize = model.establishmentList.pagination.pageSize;
        //    }
        //    else
        //    {
        //        ViewBag.TotalRows = 0;
        //    }

        //    model.directorate = employeesDataRequest.directorate ?? 0;
        //    model.employeeType = employeesDataRequest.employeeType;
        //    model.fullTime = employeesDataRequest.fullTime;
        //    model.employeeTypes = GetEmployeeTypes();
        //    return model;
        //}
        public MandatoryTrainGridViewModel GetTrainings(int teamid = 0)
        {
            int status = 1;
            string searchText = string.Empty;
            string pageText = string.Empty;
            if (Request["txtSearchText"] != null)
            {
                searchText = Request["txtSearchText"].ToString();
                ViewBag.searchText = searchText;
                Session["searchText"] = searchText;
            }
            if (Request["rdBtnstatus"] != null)
            {
                status = Convert.ToInt32(Request["rdBtnstatus"].ToString());
            }
            ///////////////////////////////////////////////////////////
            int teamId = 0;
            var manager = new ReportsManager();
            var dataRequest = new TrainingListingDataRequest();
            var model = new MandatoryTrainGridViewModel();
            dataRequest.employeeId = 0;
            ///////////////////////////////////////////////////////////
            if (Request["fromDate"] != null && Request["fromDate"] != "")
            {
                dataRequest.fromDate = Request["fromDate"].ToString();
            }
            if (Request["toDate"] != null && Request["toDate"] != "")
            {
                dataRequest.toDate = Request["toDate"].ToString();
            }
            if (Request["directorate"] != null && Request["directorate"] != "" && Request["directorate"] != "0" && Request["directorate"] != "undefined")
            {
                dataRequest.directorate = Convert.ToInt32(Request["directorate"]);
            }

            if (Request["team"] != null && Request["team"] != "" && Request["team"] != "0" && Request["team"] != "undefined")
            {
                dataRequest.team = Convert.ToInt32(Request["team"]);
            }
            if (teamid != 0) // in case if it is already being set.
            {
                dataRequest.team = teamid;
            }
            if (Request["employeeType"] != null && Request["employeeType"] != "")
            {
                dataRequest.employeeType = Request["employeeType"].ToString();
            }
            dataRequest.searchText = searchText;
            //////////////////////////////////////////////////////////////////////
            if (Request["teamId"] != null)
            {
                teamId = Convert.ToInt32(Request["teamId"].ToString());
               
            }
            dataRequest.sortBy = "expiryDate";
            dataRequest.sortOrder = "ASC";

            int pageNo = 1;
            if (HttpContext.Request.QueryString["page"] != null && HttpContext.Request.QueryString["page"] != "0")
            {
                pageNo = Convert.ToInt32(HttpContext.Request.QueryString["page"]);
            }
            if (HttpContext.Request.QueryString["sort"] != null && HttpContext.Request.QueryString["sortdir"] != null)
            {
                dataRequest.sortBy = HttpContext.Request.QueryString["sort"].ToString();
                dataRequest.sortOrder = HttpContext.Request.QueryString["sortdir"].ToString();
            }
            //dataRequest.employeeType = "All";
            dataRequest.pagination.pageSize = _ReportPageSize;
            dataRequest.pagination.pageNo = pageNo;
            var response = manager.GetMandatoryTrainings(dataRequest);
            model = new JavaScriptSerializer().Deserialize<MandatoryTrainGridViewModel>(response);
            model.employeeType = dataRequest.employeeType;
            ViewBag.TotalRows = model.pagination.totalRows;
            ViewBag.PageSize = model.pagination.pageSize;
            model.teamId = teamId;
            model.employeeTypes = GetEmployeeTypes();
            Session["Trainings"] = model;
            return model;
        }

        public GroupTrainingApprovalReportViewModel GetGroupTrainings(bool isSearch = false)
        {
            int directorateId = 0;
            int teamId = 0;
            int statusId = 0;
            int employeeId = 0;
            bool isHR = false;
            bool isDirector = false;
            string searchText = string.Empty;
            if (Request["txtSearchText"] != null)
            {
                searchText = Request["txtSearchText"].ToString();
                ViewBag.searchText = searchText;
                Session["searchText"] = searchText;
            }
            var manager = new ReportsManager();
            var dataRequest = new GroupTrainingApprovalDataRequest();
            var model = new GroupTrainingApprovalReportViewModel();
            dataRequest.employeeId = 0;
            dataRequest.isSearch = isSearch;
            if (Request["directorateId"] != null && Request["directorateId"] != "" && Request["directorateId"] != "0" && Request["directorateId"] != "undefined")
            {
                directorateId = Convert.ToInt32(Request["directorateId"]);
            }
            if (Request["teamId"] != null && Request["teamId"] != "" && Request["teamId"] != "0" && Request["teamId"] != "undefined")
            {
                teamId = Convert.ToInt32(Request["teamId"]);
            }
            dataRequest.directorateId = directorateId;
            dataRequest.teamId = teamId;

            if (Request["statusId"] != null)
            {
                statusId = Convert.ToInt32(Request["statusId"].ToString());
            }
            dataRequest.statusId = statusId;

            if (Request["employeeId"] != null && Request["employeeId"] != "" && Request["employeeId"] != "0" && Request["employeeId"] != "undefined")
            {
                employeeId = Convert.ToInt32(Request["employeeId"].ToString());
            }

            if (Request["Director"] != null && Request["Director"] != "" && Request["Director"] != "0" && Request["Director"] != "undefined")
            {
                isDirector = Convert.ToBoolean(Request["Director"]);
            }

            if (Request["HR"] != null && Request["HR"] != "" && Request["HR"] != "0" && Request["HR"] != "undefined")
            {
                isHR = Convert.ToBoolean(Request["HR"]);
            }

            dataRequest.employeeId = employeeId;
            dataRequest.isHR = isHR;
            dataRequest.isDirector = isDirector;
            dataRequest.searchText = searchText;
            dataRequest.loginUserId = GetUserIdFromSession();
            dataRequest.sortBy = "name";
            dataRequest.sortOrder = "ASC";

            int pageNo = 1;
            if (HttpContext.Request.QueryString["page"] != null && HttpContext.Request.QueryString["page"] != "0")
            {
                pageNo = Convert.ToInt32(HttpContext.Request.QueryString["page"]);
            }
            if (HttpContext.Request.QueryString["sort"] != null && HttpContext.Request.QueryString["sortdir"] != null)
            {
                dataRequest.sortBy = HttpContext.Request.QueryString["sort"].ToString();
                dataRequest.sortOrder = HttpContext.Request.QueryString["sortdir"].ToString();
            }

            dataRequest.pagination.pageSize = _ExportPageSize;
            dataRequest.pagination.pageNo = pageNo;
            var response = manager.GetGroupTrainings(dataRequest);
            model = new JavaScriptSerializer().Deserialize<GroupTrainingApprovalReportViewModel>(response);
            //Session["Trainings"] = model;
            return model;
        }

        public GiftReportViewModel GetGifts()
        {
            int directorateId = 0;
            int statusId = 0;
            int employeeId = 0;
            DateTime? fromDate = null;
            DateTime? toDate = null;
            string searchText = string.Empty;
            if (Request["txtSearchText"] != null)
            {
                searchText = Request["txtSearchText"].ToString();
                ViewBag.searchText = searchText;
                Session["searchText"] = searchText;
            }
            var manager = new ReportsManager();
            var dataRequest = new GiftDataApprovalRequest();
            var model = new GiftReportViewModel();
            dataRequest.employeeId = 0;
            if (Request["directorateId"] != null)
            {
                directorateId = Convert.ToInt32(Request["directorateId"].ToString());
            }
            dataRequest.directorateId = directorateId;

            if (Request["statusId"] != null)
            {
                statusId = Convert.ToInt32(Request["statusId"].ToString());
            }
            dataRequest.statusId = statusId;

            if (Request["employeeId"] != null)
            {
                employeeId = Convert.ToInt32(Request["employeeId"].ToString());
            }
            dataRequest.employeeId = employeeId;
            dataRequest.searchText = searchText;
            if (Session["searchText"] != null)
            {
                dataRequest.searchText = Session["searchText"].ToString();
            }

            if (Request["fromDate"] != null)
            {
                fromDate = Convert.ToDateTime(Request["fromDate"].ToString());
            }
            dataRequest.fromDate = fromDate;

            if (Request["toDate"] != null)
            {
                toDate = Convert.ToDateTime(Request["toDate"].ToString());
            }
            dataRequest.toDate = toDate;
            dataRequest.sortBy = "employeeName";
            dataRequest.sortOrder = "ASC";

            int pageNo = 1;
            if (HttpContext.Request.QueryString["page"] != null && HttpContext.Request.QueryString["page"] != "0" && HttpContext.Request.QueryString["page"] != "")
            {
                pageNo = Convert.ToInt32(HttpContext.Request.QueryString["page"]);
            }
            if (HttpContext.Request.QueryString["sort"] != null && HttpContext.Request.QueryString["sortdir"] != null)
            {
                dataRequest.sortBy = HttpContext.Request.QueryString["sort"].ToString();
                dataRequest.sortOrder = HttpContext.Request.QueryString["sortdir"].ToString();
            }

            dataRequest.pagination.pageSize = ViewBag.PageSize = _ReportPageSize;
            dataRequest.pagination.pageNo = pageNo;
            var response = manager.GetGifts(dataRequest);
            model = new JavaScriptSerializer().Deserialize<GiftReportViewModel>(response);
            if (model.pagination != null)
            {
                ViewBag.TotalRows = model.pagination.totalRows;
                ViewBag.PageSize = model.pagination.pageSize;
            }
            else
            {
                ViewBag.TotalRows = 0;
            }
            return model;
        }

        private void PrintGiftsExcel(List<GiftGridViewModel> objSource, string fileName)
        {
            WebGrid grid = new WebGrid(source: objSource, canPage: false, canSort: false);
            string gridData = grid.GetHtml(
             tableStyle: "table table-responsive backcolor",
             headerStyle: "wedgrid-header",
             footerStyle: "webgrid-footer",
             alternatingRowStyle: "webgrid-alternating-row",
             rowStyle: "webgrid-row-style",
             columns: grid.Columns(
              grid.Column(columnName: "employeeName", header: "Name"),
              grid.Column(columnName: "directorateName", header: "Directorate"),
              grid.Column(columnName: "details", header: "Details"),
              grid.Column(columnName: "receivedGivenDate", header: "Received/Offered"),
              grid.Column(columnName: "value", header: "Value"),
              grid.Column(columnName: "statusName", header: "Status")
                            )).ToString();
            Response.ClearContent();
            //give name to excel sheet.
            Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            //specify content type
            Response.ContentType = "application/excel";
            //write excel data using this method 
            Response.Write(gridData);
            Response.End();
        }

        #endregion

        #region >>> Action Methods <<<

        // GET: Reports
        public ActionResult Index()
        {
            ReportsCriteriaViewModel rptCr = new ReportsCriteriaViewModel();
            ViewBag.searchText = "";
            rptCr.reports = ReportLookup();
            return View("~/Views/Reports/ReportsMaster.cshtml", rptCr);
        }

        #region >>> establishment report <<<

        public ActionResult EstablishmentReportPopulate()
        {
            EstablishmentGridViewModel model = getEstablishmentReportPopulate();
            return View(model);
        }

        public ActionResult EstablishmentReport()
        {
            //ReportsManager rptManager = new ReportsManager();
            //var dataRequest = new EstablishmentDataRequest();
            //dataRequest.pagination = new BusinessModule.NetworkObjects.PaginationDataRequest();
            //ViewBag.searchText = "";
            //ViewBag.pageText = "";

            //dataRequest.sortBy = "fullName";
            //dataRequest.sortOrder = "ASC";
            //dataRequest.directorate = null;
            //dataRequest.employeeType = "All";
            //dataRequest.searchText = "";
            //int pageNo = 1;
            //if (HttpContext.Request.QueryString["page"] != null && HttpContext.Request.QueryString["page"] != "0")
            //{
            //    pageNo = Convert.ToInt32(HttpContext.Request.QueryString["page"]);
            //}
            //if (HttpContext.Request.QueryString["sort"] != null && HttpContext.Request.QueryString["sortdir"] != null)
            //{
            //    dataRequest.sortBy = HttpContext.Request.QueryString["sort"].ToString();
            //    dataRequest.sortOrder = HttpContext.Request.QueryString["sortdir"].ToString();
            //}
            //dataRequest.pagination.pageSize = _ReportPageSize;
            //dataRequest.pagination.pageNo = pageNo;
            //var response = rptManager.getEstablishmentReport(dataRequest);
            EstablishmentGridViewModel model = getEstablishmentReportPopulate();
            //if (model.establishmentList != null)
            //{
            //    ViewBag.TotalRows = model.establishmentList.pagination.totalRows;
            //    ViewBag.PageSize = model.establishmentList.pagination.pageSize;
            //}
            //else
            //{
            //    ViewBag.TotalRows = 0;
            //    ViewBag.PageSize = 0;
            //}
            model.employeeTypes = GetEmployeeTypes();
            return PartialView("~/Views/Reports/_EstablishmentReport.cshtml", model);
        }

        public ActionResult SearchEstablishment()
        {
            EstablishmentGridViewModel model = getEstablishmentReportPopulate();
            return PartialView("~/Views/Reports/_EstablishmentGrid.cshtml", model);
        }

        public ActionResult EstablishmentReportCriteria(EstablishmentDataRequest dataRequest)
        {
            ReportsManager rptManager = new ReportsManager();
            dataRequest.pagination = new BusinessModule.NetworkObjects.PaginationDataRequest();
            dataRequest.sortBy = "fullName";
            dataRequest.sortOrder = "ASC";
            if(string.IsNullOrEmpty(dataRequest.searchText))
            {
                dataRequest.searchText = "";
            }
            dataRequest.pagination.pageSize = _ReportPageSize;
            var response = rptManager.getEstablishmentReport(dataRequest);
            EstablishmentGridViewModel model = new JavaScriptSerializer().Deserialize<EstablishmentGridViewModel>(response);
            ViewBag.TotalRows = model.establishmentList.pagination.totalRows;
            ViewBag.PageSize = model.establishmentList.pagination.pageSize;
            model.employeeTypes = GetEmployeeTypes();
            return PartialView("~/Views/Reports/_EstablishmentReport.cshtml", model);
        }
        public ActionResult ExportXLsForMandatoryTraining(TrainingListingDataRequest request)
        {
            var manager = new ReportsManager();
            var model = new MandatoryTrainGridViewModel();
            request.sortBy = "trainingId";
            request.sortOrder = "ASC";
            if (string.IsNullOrEmpty(request.searchText))
            {
                request.searchText = "";
            }
            if (request.directorate == 0)
            {
                request.directorate = null;
            }
            if (request.team == 0)
            {
                request.team = null;
            }
            request.pagination = new PaginationDataRequest();
            request.pagination.pageNo = 1;
            request.pagination.pageSize = _ExportPageSize;
            var response = manager.GetMandatoryTrainings(request);
            if (response != "[]")
            {
                model = new JavaScriptSerializer().Deserialize<MandatoryTrainGridViewModel>(response);
            }
            var fileName = "MandatoryTraining_{0}_{1}_{2}.xls";
            fileName = string.Format(fileName, DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            PrintExcelForMandatoryTrainings(model.trainingList, fileName);
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ExportData(EstablishmentDataRequest request)
        {
            var manager = new ReportsManager();
            var model = new EstablishmentGridViewModel();
            request.sortBy = "fullName";
            request.sortOrder = "ASC";
            if(string.IsNullOrEmpty(request.searchText))
            {
                request.searchText = "";
            }
            if(request.directorate==0)
            {
                request.directorate = null;
            }
            if (request.team == 0)
            {
                request.team = null;
            }
            request.pagination = new PaginationDataRequest();
            request.pagination.pageNo = 1;
            request.pagination.pageSize = _ExportPageSize;
            var response = manager.getEstablishmentReport(request);
            if (response != "[]")
            {
                model = new JavaScriptSerializer().Deserialize<EstablishmentGridViewModel>(response);
            }
            var fileName = "Establishment_{0}_{1}_{2}.xls";
            fileName = string.Format(fileName, DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            PrintExcel(model.establishmentList.establishmentList, fileName);
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        public void PrintExcelForMandatoryTrainings(List<MandatoryTrainingViewModel> objSource, string fileName)
        {
            WebGrid grid = new WebGrid(source: objSource, canPage: false, canSort: false);
            string gridData = grid.GetHtml(
             tableStyle: "table table-responsive backcolor",
             headerStyle: "wedgrid-header",
             footerStyle: "webgrid-footer",
             alternatingRowStyle: "webgrid-alternating-row",
             rowStyle: "webgrid-row-style",
             columns: grid.Columns(
              grid.Column(columnName: "employeeName", header: "Name"),
              grid.Column(columnName: "directorateName", header: "Directorate"),
              grid.Column(columnName: "teamName", header: "Team"),
              grid.Column(columnName: "jobTitle", header: "Job Title"),
              grid.Column(columnName: "startDate", header: "Start"),
              grid.Column(columnName: "course", header: "Training Course"),
              grid.Column(columnName: "providerName", header: "Provider"),
              grid.Column(columnName: "expiry", header: "Renew"),
              grid.Column(columnName: "byName", header: "By")
                    
              )).ToString();

            Response.ClearContent();
            //give name to excel sheet.
            Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            //specify content type
            Response.ContentType = "application/excel";
            //write excel data using this method 
            Response.Write(gridData);
            Response.End();
        }
        public void PrintExcel(List<EstablishmentViewModel> objSource, string fileName)
        {
            WebGrid grid = new WebGrid(source: objSource, canPage: false, canSort: false);
            string gridData = grid.GetHtml(
             tableStyle: "table table-responsive backcolor",
             headerStyle: "wedgrid-header",
             footerStyle: "webgrid-footer",
             alternatingRowStyle: "webgrid-alternating-row",
             rowStyle: "webgrid-row-style",
             columns: grid.Columns(
              grid.Column(columnName: "fullName", header: "Name"),
              grid.Column(columnName: "directorate", header: "Directorate"),
              grid.Column(columnName: "team", header: "Team"),
              grid.Column(columnName: "jobTitle", header: "Job Title"),
              grid.Column(columnName: "gender", header: "Gender"),
              grid.Column(columnName: "ethnicity", header: "Ethnicity"),
              grid.Column(columnName: "dob", header: "DoB"),
              grid.Column(columnName: "salary", header: "Salary"),
              grid.Column(columnName: "grade", header: "Grade"),
              grid.Column(columnName: "startDate", header: "Start"),
              grid.Column(columnName: "endDate", header: "End"),
              grid.Column(columnName: "address", header: "Home Address")
                            )).ToString();
            Response.ClearContent();
            //give name to excel sheet.
            Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            //specify content type
            Response.ContentType = "application/excel";
            //write excel data using this method 
            Response.Write(gridData);
            Response.End();
        }

        #endregion

        #region >>> remuneration report <<<

        public ActionResult RemunerationReportPopulate()
        {
            RemunerationGridViewModel model = GetRemunerationReportPopulate();
            return View(model);
        }

        public ActionResult RemunerationReport()
        {
            ReportsManager rptManager = new ReportsManager();
            var dataRequest = new RemunerationDataRequest();
            dataRequest.pagination = new BusinessModule.NetworkObjects.PaginationDataRequest();
            ViewBag.searchText = "";
            dataRequest.sortBy = "fullName";
            dataRequest.sortOrder = "ASC";
            dataRequest.directorate = null;
            dataRequest.team = null;
            dataRequest.employeeType = "All";
            if (string.IsNullOrEmpty(dataRequest.searchText))
            {
                dataRequest.searchText = "";
            }
            int pageNo = 1;
            if (HttpContext.Request.QueryString["page"] != null && HttpContext.Request.QueryString["page"] != "0")
            {
                pageNo = Convert.ToInt32(HttpContext.Request.QueryString["page"]);
            }
            if (HttpContext.Request.QueryString["sort"] != null && HttpContext.Request.QueryString["sortdir"] != null)
            {
                dataRequest.sortBy = HttpContext.Request.QueryString["sort"].ToString();
                dataRequest.sortOrder = HttpContext.Request.QueryString["sortdir"].ToString();
            }
            dataRequest.pagination.pageSize = _ReportPageSize;
            dataRequest.pagination.pageNo = pageNo;
            var response = rptManager.GetRemunerationReport(dataRequest);
            RemunerationGridViewModel model = new JavaScriptSerializer().Deserialize<RemunerationGridViewModel>(response);
            if (model.statementList.pagination != null)
            {
                ViewBag.TotalRows = model.statementList.pagination.totalRows;
                ViewBag.PageSize = model.statementList.pagination.pageSize;
            }
            else
            {
                ViewBag.TotalRows = 0;
            }
            model.employeeTypes = GetEmployeeTypes();
            return PartialView("~/Views/Reports/_RemunerationReport.cshtml", model);
        }

        public ActionResult SearchRemuneration()
        {
            RemunerationGridViewModel model = GetRemunerationReportPopulate();
            return PartialView("~/Views/Reports/_RemunerationGrid.cshtml", model);
        }

        public ActionResult RemunerationReportCriteria(RemunerationDataRequest dataRequest)
        {
            ReportsManager rptManager = new ReportsManager();
            dataRequest.pagination = new BusinessModule.NetworkObjects.PaginationDataRequest();
            dataRequest.sortBy = "fullName";
            dataRequest.sortOrder = "ASC";
            if (string.IsNullOrEmpty(dataRequest.searchText))
            {
                dataRequest.searchText = "";
            }
            dataRequest.pagination.pageSize = _ReportPageSize;
            var response = rptManager.GetRemunerationReport(dataRequest);
            RemunerationGridViewModel model = new JavaScriptSerializer().Deserialize<RemunerationGridViewModel>(response);
            ViewBag.TotalRows = model.statementList.pagination.totalRows;
            ViewBag.PageSize = model.statementList.pagination.pageSize;
            model.employeeTypes = GetEmployeeTypes();
            return PartialView("~/Views/Reports/_RemunerationReport.cshtml", model);
        }

        public ActionResult ExportRemunerationData(RemunerationDataRequest request)
        {
            var manager = new ReportsManager();
            var model = new RemunerationGridViewModel();
            request.sortBy = "fullName";
            request.sortOrder = "ASC";
            if (string.IsNullOrEmpty(request.searchText))
            {
                request.searchText = "";
            }
            if(request.team==0)
            {
                request.team = null;
            }
            if (request.directorate == 0)
            {
                request.directorate = null;
            }
            request.pagination = new PaginationDataRequest();
            request.pagination.pageNo = 1;
            request.pagination.pageSize = _ExportPageSize;
            var response = manager.GetRemunerationReport(request);
            if (response != "[]")
            {
                model = new JavaScriptSerializer().Deserialize<RemunerationGridViewModel>(response);
            }
            var fileName = "Remuneration_{0}_{1}_{2}.xls";
            fileName = string.Format(fileName, DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            PrintRemunerationExcel(model.statementList.employeeList, fileName);
            return Json(true, JsonRequestBehavior.AllowGet);
        }
        public void PrintRemunerationExcel(List<RemunerationViewModel> objSource, string fileName)
        {
            WebGrid grid = new WebGrid(source: objSource, canPage: false, canSort: false);
            string gridData = grid.GetHtml(
             tableStyle: "table table-responsive backcolor",
             headerStyle: "wedgrid-header",
             footerStyle: "webgrid-footer",
             alternatingRowStyle: "webgrid-alternating-row",
             rowStyle: "webgrid-row-style",
             columns: grid.Columns(
              grid.Column(columnName: "fullName", header: "Name"),
              grid.Column(columnName: "directorate", header: "Directorate"),
              grid.Column(columnName: "team", header: "Team:"),
              grid.Column(columnName: "jobTitle", header: "Job Title"),
              grid.Column(columnName: "gender", header: "Gender"),
              grid.Column(columnName: "ethnicity", header: "Ethnicity"),
              grid.Column(columnName: "dob", header: "DoB"),
              grid.Column(columnName: "salary", header: "Salary"),
              grid.Column(columnName: "grade", header: "Grade"),
              grid.Column(columnName: "startDate", header: "Start"),
              grid.Column(columnName: "endDate", header: "End")
                            )).ToString();
            Response.ClearContent();
            //give name to excel sheet.
            Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            //specify content type
            Response.ContentType = "application/excel";
            //write excel data using this method 
            Response.Write(gridData);
            Response.End();
        }
        public ActionResult GetRenuDoc()
        {
            ReportsManager rptManager = new ReportsManager();
            var response = rptManager.GetRemunerationStatementDocument(2434);
            RemunerationDocumentViewModel model = new JavaScriptSerializer().Deserialize<RemunerationDocumentViewModel>(response);
            ViewBag.Date = String.Format("{0:ddd, MMM d, yyyy}", DateTime.Now);
            return View("~/Views/Reports/RemunerationDocument.cshtml", model);
        }
        public ViewAsPdf GetRemunerationStatementDocument(int Id)
        {
            ViewAsPdf pdfOutput = null;
            ReportsManager rptManager = new ReportsManager();
            var response = rptManager.GetRemunerationStatementDocument(Id);
            RemunerationDocumentViewModel model = new JavaScriptSerializer().Deserialize<RemunerationDocumentViewModel>(response);
            ViewBag.Date = String.Format("{0:ddd,d  MMM, yyyy}", DateTime.Now);
            string fileName = FileHelper.getUniqueFileName("RemunerationStatement.pdf");
            try
            {
                pdfOutput = new ViewAsPdf("~/Views/Reports/RemunerationDocument.cshtml", model)
                {
                    FileName = fileName,
                    CustomSwitches = "--footer-spacing \"5\" --footer-font-size \"8\" --footer-center \"Page [page] of [toPage]\" --footer-right \"PDF Created " + DateTime.Now.ToString() + "\" ",
                    PageMargins = { Top = 15, Bottom = 20 },
                    PageSize = Rotativa.Options.Size.A4
                };
            }
            catch (Exception ex)
            {
            }
            return pdfOutput;
        }

        #endregion

        #region >>> sickness report <<<

        public ActionResult SicknessReport()
        {
            SicknessGridViewModel model = getSicknessReportPopulate();
            ViewBag.TotalRows = model.sicknessList.pagination.totalRows;
            ViewBag.PageSize = model.sicknessList.pagination.pageSize;
            model.employeeTypes = GetEmployeeTypes();
            return PartialView("~/Views/Reports/_SicknessReport.cshtml", model);
        }

        public ActionResult SearchSickness()
        {
            SicknessGridViewModel model = getSicknessReportPopulate();
            return PartialView("~/Views/Reports/_SicknessGrid.cshtml", model);
        }

        public ActionResult ExportSicknessData(SicknessDataRequest request)
        {
            var manager = new ReportsManager();
            var model = new SicknessGridViewModel();
            request.sortBy = "surName";
            request.sortOrder = "ASC";
            if (string.IsNullOrEmpty(request.searchText))
            {
                request.searchText = "";
            }
            if (request.directorate == 0)
            {
                request.directorate = null;
            }
            if (request.team == 0)
            {
                request.team = null;
            }
            request.pagination = new PaginationDataRequest();
            request.pagination.pageNo = 1;
            request.pagination.pageSize = _ExportPageSize;
            var response = manager.getSicknessExport(request);
            if (response != "[]")
            {
                model = new JavaScriptSerializer().Deserialize<SicknessGridViewModel>(response);
            }
            var fileName = "Sickness_{0}_{1}_{2}.xls";
            fileName = string.Format(fileName, DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            PrintExcelSickness(model.sicknessExportList.sicknessExportList, fileName);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public void PrintExcelSickness(List<SicknessExportViewModel> objSource, string fileName)
        {
            WebGrid grid = new WebGrid(source: objSource, canPage: false, canSort: false);
            string gridData = grid.GetHtml(
             tableStyle: "table table-responsive backcolor",
             headerStyle: "wedgrid-header",
             footerStyle: "webgrid-footer",
             alternatingRowStyle: "webgrid-alternating-row",
             rowStyle: "webgrid-row-style",
             columns:
                grid.Columns(
                    grid.Column(columnName: "surName", header: "Sur Name"),
                    grid.Column(columnName: "firstName", header: "First Name"),
                    grid.Column(columnName: "full_partTime", header: "Full/Part Time"),
                    grid.Column(columnName: "employeeStartDate", header: "Employment Start Date"),
                    grid.Column(columnName: "jobTitle", header: "Job Title"),
                    grid.Column(columnName: "dept", header: "Dept."),
                    grid.Column(columnName: "startDate", header: "Start Date"),
                    grid.Column(columnName: "lastDateOfAbsence", header: "Last Date Of Absence"),
                    grid.Column(columnName: "endDate", header: "Actual Return Date"),
                    grid.Column(columnName: "durationString", header: "Duration"),
                    grid.Column(columnName: "reason", header: "Reason")
            )).ToString();
            Response.ClearContent();
            //give name to excel sheet.
            Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            //specify content type
            Response.ContentType = "application/excel";
            //write excel data using this method 
            Response.Write(gridData);
            Response.End();
        }

        #endregion

        #region >>> Annual Leave report <<<

        public ActionResult AnnualLeaveReportPopulate()
        {
            AnnualLeaveGridViewModel model = GetAnnualLeaveReportPopulate();
            return View(model);
        }

        public ActionResult SearchAnnualLeave()
        {
            AnnualLeaveGridViewModel model = GetAnnualLeaveReportPopulate();
            return PartialView("~/Views/Reports/_AnnualLeaveGrid.cshtml", model);
        }

        public ActionResult AnnualLeaveReportCriteria(AnnualLeaveDataRequest dataRequest)
        {
            ReportsManager rptManager = new ReportsManager();
            dataRequest.pagination = new BusinessModule.NetworkObjects.PaginationDataRequest();
            dataRequest.sortBy = "fullName";
            dataRequest.sortOrder = "ASC";
            if (string.IsNullOrEmpty(dataRequest.searchText))
            {
                dataRequest.searchText = "";
            }
            dataRequest.pagination.pageSize = _ReportPageSize;
            if (dataRequest.year == null || dataRequest.year == "")
                dataRequest.year = DateTime.Now.Year.ToString();
            var response = rptManager.GetAnnualLeaveReport(dataRequest);
            AnnualLeaveGridViewModel model = new JavaScriptSerializer().Deserialize<AnnualLeaveGridViewModel>(response);
            ViewBag.TotalRows = model.annualLeaveListing.pagination.totalRows;
            ViewBag.PageSize = model.annualLeaveListing.pagination.pageSize;
            return PartialView("~/Views/Reports/_AnnualLeaveReport.cshtml", model);
        }

        public ActionResult ExportAnnualLeaveData(AnnualLeaveDataRequest request)
        {
            var manager = new ReportsManager();
            var model = new AnnualLeaveGridViewModel();
            request.sortBy = "fullName";
            request.sortOrder = "ASC";
            if (string.IsNullOrEmpty(request.searchText))
            {
                request.searchText = "";
            }
            if (request.directorate == 0)
            {
                request.directorate = null;
            }
            if (request.team == 0)
            {
                request.team = null;
            }
            request.pagination = new PaginationDataRequest();
            request.pagination.pageNo = 1;
            request.pagination.pageSize = _ExportPageSize;
            if (request.year == null || request.year == "")
                request.year = DateTime.Now.Year.ToString();
            var response = manager.GetAnnualLeaveReport(request);
            if (response != "[]")
            {
                model = new JavaScriptSerializer().Deserialize<AnnualLeaveGridViewModel>(response);
            }
            var fileName = "AnnualLeave_{0}_{1}_{2}.xls";
            fileName = string.Format(fileName, DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            PrintAnnualLeaveExcel(model.annualLeaveListing.reportListing, fileName);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public void PrintAnnualLeaveExcel(List<AnnualLeaveViewModel> objSource, string fileName)
        {
            WebGrid grid = new WebGrid(source: objSource, canPage: false, canSort: false);
            string gridData = grid.GetHtml(
             tableStyle: "table table-responsive backcolor",
             headerStyle: "wedgrid-header",
             footerStyle: "webgrid-footer",
             alternatingRowStyle: "webgrid-alternating-row",
             rowStyle: "webgrid-row-style",
             columns: grid.Columns(
              grid.Column(columnName: "EmploymentStartDate", header: "Employment Start Date"),
              grid.Column(columnName: "AlStartDate", header: "Leave Start Date"),
              grid.Column(columnName: "fullName", header: "Name"),
              grid.Column(columnName: "directorate", header: "Directorate:"),
              grid.Column(columnName: "team", header: "Team"),
              grid.Column(columnName: "contractedHours", header: "Contracted Hours Per Week"),
              grid.Column(columnName: "totalAllowance", header: "Total Allowance"),
              grid.Column(columnName: "carryForward", header: "Carry Forward"),
              grid.Column(columnName: "daysBooked", header: "Days Booked"),
              grid.Column(columnName: "daysRequested", header: "Days Requested"),
              grid.Column(columnName: "daysTaken", header: "Leaves Taken"),
              grid.Column(columnName: "balanceRemaining", header: "Balance Remaining"),
              grid.Column(columnName: "payRoleNum", header: "Pay Roll Number"),
              grid.Column(columnName: "lineManager", header: "Line Manager")
                            )).ToString();
            Response.ClearContent();
            //give name to excel sheet.
            Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            //specify content type
            Response.ContentType = "application/excel";
            //write excel data using this method 
            Response.Write(gridData);
            Response.End();
        }

        public ActionResult AnnualLeaveReport()
        {
            //ReportsManager rptManager = new ReportsManager();
            //var dataRequest = new AnnualLeaveDataRequest();
            //dataRequest.pagination = new BusinessModule.NetworkObjects.PaginationDataRequest();
            //ViewBag.searchText = "";
            ////List<SelectListItem> employeeTypes = GetEmployeeTypes();
            //dataRequest.sortBy = "fullName";
            //dataRequest.sortOrder = "ASC";
            //dataRequest.directorate = null;
            //dataRequest.searchText = "";
            //int pageNo = 1;
            //if (HttpContext.Request.QueryString["page"] != null && HttpContext.Request.QueryString["page"] != "0")
            //{
            //    pageNo = Convert.ToInt32(HttpContext.Request.QueryString["page"]);
            //}
            //if (HttpContext.Request.QueryString["sort"] != null && HttpContext.Request.QueryString["sortdir"] != null)
            //{
            //    dataRequest.sortBy = HttpContext.Request.QueryString["sort"].ToString();
            //    dataRequest.sortOrder = HttpContext.Request.QueryString["sortdir"].ToString();
            //}

            //dataRequest.pagination.pageSize = _ReportPageSize;
            //dataRequest.pagination.pageNo = pageNo;
            //dataRequest.year = DateTime.Now.Year.ToString();
            //var response = rptManager.GetAnnualLeaveReport(dataRequest);
            //AnnualLeaveGridViewModel model = new JavaScriptSerializer().Deserialize<AnnualLeaveGridViewModel>(response);
            //ViewBag.TotalRows = model.annualLeaveListing.pagination.totalRows;
            //ViewBag.PageSize = model.annualLeaveListing.pagination.pageSize;
            AnnualLeaveGridViewModel model = GetAnnualLeaveReportPopulate();
            return PartialView("~/Views/Reports/_AnnualLeaveReport.cshtml", model);
        }

        #endregion

        #region >>> Leaver report <<<

        public ActionResult LeaverReportPopulate()
        {
            LeaverGridViewModel model = GetLeaverReportPopulate();
            return View(model);
        }

        public ActionResult SearchLeaver()
        {
            LeaverGridViewModel model = GetLeaverReportPopulate();
            return PartialView("~/Views/Reports/_LeaverReportList.cshtml", model);
        }

        public ActionResult LeaverReportCriteria(LeaverDataRequest dataRequest)
        {
            ReportsManager rptManager = new ReportsManager();
            dataRequest.pagination = new BusinessModule.NetworkObjects.PaginationDataRequest();
            dataRequest.sortBy = "fullName";
            dataRequest.sortOrder = "ASC";
            if (string.IsNullOrEmpty(dataRequest.searchText))
            {
                dataRequest.searchText = "";
            }
            dataRequest.pagination.pageSize = _ReportPageSize;
            var response = rptManager.GetLeaverReport(dataRequest);
            LeaverGridViewModel model = new JavaScriptSerializer().Deserialize<LeaverGridViewModel>(response);
            ViewBag.TotalRows = model.leaverReportListing.pagination.totalRows;
            ViewBag.PageSize = model.leaverReportListing.pagination.pageSize;
            return PartialView("~/Views/Reports/_LeaverReport.cshtml", model);
        }

        public ActionResult ExportLeaverData(LeaverDataRequest request)
        {
            var manager = new ReportsManager();
            var model = new LeaverGridViewModel();
            request.sortBy = "fullName";
            request.sortOrder = "ASC";
            if (string.IsNullOrEmpty(request.searchText))
            {
                request.searchText = "";
            }
            if (request.directorate == 0)
            {
                request.directorate = null;
            }
            if (request.team == 0)
            {
                request.team = null;
            }
            request.pagination = new PaginationDataRequest();
            request.pagination.pageNo = 1;
            request.pagination.pageSize = _ExportPageSize;
            var response = manager.GetLeaverReport(request);
            if (response != "[]")
            {
                model = new JavaScriptSerializer().Deserialize<LeaverGridViewModel>(response);
            }
            var fileName = "Leaver_{0}_{1}_{2}.xls";
            fileName = string.Format(fileName, DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            PrintLeaverExcel(model.leaverReportListing.reportListing, fileName);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public void PrintLeaverExcel(List<LeaverViewModel> objSource, string fileName)
        {
            WebGrid grid = new WebGrid(source: objSource, canPage: false, canSort: false);
            string gridData = grid.GetHtml(
             tableStyle: "table table-responsive backcolor",
             headerStyle: "wedgrid-header",
             footerStyle: "webgrid-footer",
             alternatingRowStyle: "webgrid-alternating-row",
             rowStyle: "webgrid-row-style",
             columns: grid.Columns(
              grid.Column(columnName: "fullName", header: "Name"),
              grid.Column(columnName: "directorate", header: "Directorate"),
              grid.Column(columnName: "team", header: "Team"),
              grid.Column(columnName: "startDate", header: "Start"),
              grid.Column(columnName: "leaveDate", header: "Leave"),
              grid.Column(columnName: "reason", header: "Reason")
                            )).ToString();
            Response.ClearContent();
            //give name to excel sheet.
            Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            //specify content type
            Response.ContentType = "application/excel";
            //write excel data using this method 
            Response.Write(gridData);
            Response.End();
        }

        public ActionResult LeaverReport()
        {
            ReportsManager rptManager = new ReportsManager();
            var dataRequest = new LeaverDataRequest();
            dataRequest.pagination = new BusinessModule.NetworkObjects.PaginationDataRequest();
            ViewBag.searchText = "";
            dataRequest.sortBy = "fullName";
            dataRequest.sortOrder = "ASC";
            dataRequest.directorate = null;
            if (string.IsNullOrEmpty(dataRequest.searchText))
            {
                dataRequest.searchText = "";
            }
            int pageNo = 1;
            if (HttpContext.Request.QueryString["page"] != null && HttpContext.Request.QueryString["page"] != "0")
            {
                pageNo = Convert.ToInt32(HttpContext.Request.QueryString["page"]);
            }
            if (HttpContext.Request.QueryString["sort"] != null && HttpContext.Request.QueryString["sortdir"] != null)
            {
                dataRequest.sortBy = HttpContext.Request.QueryString["sort"].ToString();
                dataRequest.sortOrder = HttpContext.Request.QueryString["sortdir"].ToString();
            }

            dataRequest.pagination.pageSize = _ReportPageSize;
            dataRequest.pagination.pageNo = pageNo;
            var response = rptManager.GetLeaverReport(dataRequest);
            LeaverGridViewModel model = new JavaScriptSerializer().Deserialize<LeaverGridViewModel>(response);
            ViewBag.TotalRows = model.leaverReportListing.pagination.totalRows;
            ViewBag.PageSize = model.leaverReportListing.pagination.pageSize;
            return PartialView("~/Views/Reports/_LeaverReport.cshtml", model);
        }

        public ActionResult LeaverReportPaging(int page)
        {
            ReportsManager rptManager = new ReportsManager();
            var dataRequest = new LeaverDataRequest();
            dataRequest.pagination = new BusinessModule.NetworkObjects.PaginationDataRequest();
            ViewBag.searchText = "";
            dataRequest.sortBy = "fullName";
            dataRequest.sortOrder = "ASC";
            dataRequest.directorate = null;
            if (string.IsNullOrEmpty(dataRequest.searchText))
            {
                dataRequest.searchText = "";
            }
            int pageNo = 1;
            if (HttpContext.Request.QueryString["page"] != null && HttpContext.Request.QueryString["page"] != "0")
            {
                pageNo = Convert.ToInt32(HttpContext.Request.QueryString["page"]);
            }
            if (HttpContext.Request.QueryString["sort"] != null && HttpContext.Request.QueryString["sortdir"] != null)
            {
                dataRequest.sortBy = HttpContext.Request.QueryString["sort"].ToString();
                dataRequest.sortOrder = HttpContext.Request.QueryString["sortdir"].ToString();
            }
            dataRequest.pagination.pageSize = _ReportPageSize;
            dataRequest.pagination.pageNo = pageNo;
            var response = rptManager.GetLeaverReport(dataRequest);
            LeaverGridViewModel model = new JavaScriptSerializer().Deserialize<LeaverGridViewModel>(response);
            ViewBag.TotalRows = model.leaverReportListing.pagination.totalRows;
            ViewBag.PageSize = model.leaverReportListing.pagination.pageSize;
            return PartialView(model);
        }

        #endregion

        #region >>> new starter report <<<

        public ActionResult NewStarterReportPopulate()
        {
            NewStarterGridViewModel model = getNewStarterReportPopulate();
            return View(model);
        }

        public ActionResult NewStarterReport()
        {
            NewStarterGridViewModel model = getNewStarterReportPopulate();
            model.employeeTypes = GetEmployeeTypes();
            return PartialView("~/Views/Reports/_NewStarterReport.cshtml", model);
        }

        public ActionResult SearchNewStarter()
        {
            NewStarterGridViewModel model = getNewStarterReportPopulate();
            return PartialView("~/Views/Reports/_NewStarterGrid.cshtml", model);
        }

        public ActionResult NewStarterReportCriteria(NewStarterDataRequest dataRequest)
        {
            ReportsManager rptManager = new ReportsManager();
            dataRequest.pagination = new BusinessModule.NetworkObjects.PaginationDataRequest();
            dataRequest.sortBy = "fullName";
            dataRequest.sortOrder = "ASC";
            if (string.IsNullOrEmpty(dataRequest.searchText))
            {
                dataRequest.searchText = "";
            }
            dataRequest.pagination.pageSize = _ReportPageSize;
            var response = rptManager.GetNewStarterReport(dataRequest);
            NewStarterGridViewModel model = new JavaScriptSerializer().Deserialize<NewStarterGridViewModel>(response);
            ViewBag.TotalRows = model.newStarterList.pagination.totalRows;
            ViewBag.PageSize = model.newStarterList.pagination.pageSize;
            model.employeeTypes = GetEmployeeTypes();
            return PartialView("~/Views/Reports/_NewStarterReport.cshtml", model);
        }

        public ActionResult ExportNewStarterData(NewStarterDataRequest request)
        {
            var manager = new ReportsManager();
            var model = new NewStarterGridViewModel();
            request.sortBy = "fullName";
            request.sortOrder = "ASC";
            if (string.IsNullOrEmpty(request.searchText))
            {
                request.searchText = "";
            }
            if (request.directorate == 0)
            {
                request.directorate = null;
            }
            if (request.team == 0)
            {
                request.team = null;
            }
            request.pagination = new PaginationDataRequest();
            request.pagination.pageNo = 1;
            request.pagination.pageSize = _ExportPageSize;
            var response = manager.GetNewStarterReport(request);
            if (response != "[]")
            {
                model = new JavaScriptSerializer().Deserialize<NewStarterGridViewModel>(response);
            }
            var fileName = "NewStarter_{0}_{1}_{2}.xls";
            fileName = string.Format(fileName, DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            if(model.newStarterList.reportListing==null)
            {
                model.newStarterList.reportListing = new List<ViewModels.NewStarterViewModel>();
            }
            PrintNewStarterExcel(model.newStarterList.reportListing, fileName);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public void PrintNewStarterExcel(List<NewStarterViewModel> objSource, string fileName)
        {
            WebGrid grid = new WebGrid(source: objSource, canPage: false, canSort: false);
            string gridData = grid.GetHtml(
             tableStyle: "table table-responsive backcolor",
             headerStyle: "wedgrid-header",
             footerStyle: "webgrid-footer",
             alternatingRowStyle: "webgrid-alternating-row",
             rowStyle: "webgrid-row-style",
             columns: grid.Columns(
              grid.Column(columnName: "fullName", header: "Name"),
              grid.Column(columnName: "directorate", header: "Directorate"),
              grid.Column(columnName: "team", header: "Team"),
              grid.Column(columnName: "jobTitle", header: "Job Title"),
              grid.Column(columnName: "gender", header: "Gender"),
              grid.Column(columnName: "ethnicity", header: "Ethnicity"),
              grid.Column(columnName: "dob", header: "DoB"),
              grid.Column(columnName: "salary", header: "Salary"),
              grid.Column(columnName: "grade", header: "Grade"),
              grid.Column(columnName: "startDate", header: "Start"),
              grid.Column(columnName: "reviewDate", header: "Review")
                            )).ToString();
            Response.ClearContent();
            //give name to excel sheet.
            Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            //specify content type
            Response.ContentType = "application/excel";
            //write excel data using this method 
            Response.Write(gridData);
            Response.End();
        }

        #endregion

        #region >>> Pay Point Report <<<

        public ActionResult PayPointReportPopulate()
        {
            PayPointGridViewModel model = getPayPointReportPopulate();
            return View(model);
        }

        public ActionResult PayPointReport()
        {
            PayPointGridViewModel model = getPayPointReportPopulate();
            return PartialView("~/Views/Reports/_PayPointReport.cshtml", model);
        }

        public ActionResult SearchPayPoint()
        {
            PayPointGridViewModel model = getPayPointReportPopulate();
            return PartialView("~/Views/Reports/_PayPointGrid.cshtml", model);
        }

        public ActionResult PayPointReportCriteria(PayPointDataRequest dataRequest)
        {
            ReportsManager rptManager = new ReportsManager();
            dataRequest.pagination = new BusinessModule.NetworkObjects.PaginationDataRequest();
            dataRequest.sortBy = "name";
            dataRequest.sortOrder = "ASC";
            if (string.IsNullOrEmpty(dataRequest.searchText))
            {
                dataRequest.searchText = "";
            }
            dataRequest.pagination.pageSize = _ReportPageSize;
            var response = rptManager.GetPayPointReport(dataRequest);
            PayPointGridViewModel model = new JavaScriptSerializer().Deserialize<PayPointGridViewModel>(response);
            ViewBag.TotalRows = model.PayPointList.pagination.totalRows;
            ViewBag.PageSize = model.PayPointList.pagination.pageSize;
            return PartialView("~/Views/Reports/_PayPointReport.cshtml", model);
        }

        public ActionResult ExportPayPointData(PayPointDataRequest request)
        {
            var manager = new ReportsManager();
            var model = new PayPointGridViewModel();
            request.sortBy = "name";
            request.sortOrder = "ASC";
            if (string.IsNullOrEmpty(request.searchText))
            {
                request.searchText = "";
            }
            if (request.directorate == 0)
            {
                request.directorate = null;
            }
            if (request.teamId == 0)
            {
                request.teamId = null;
            }
            request.pagination = new PaginationDataRequest();
            request.pagination.pageNo = 1;
            request.pagination.pageSize = _ExportPageSize;
            var response = manager.GetPayPointReport(request);
            if (response != "[]")
            {
                model = new JavaScriptSerializer().Deserialize<PayPointGridViewModel>(response);
            }
            var fileName = "PayPoint_{0}_{1}_{2}.xls";
            fileName = string.Format(fileName, DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            PrintPayPointExcel(model.PayPointList.payPoints, fileName);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public void PrintPayPointExcel(List<PayPointViewModel> objSource, string fileName)
        {
            WebGrid grid = new WebGrid(source: objSource, canPage: false, canSort: false);
            string gridData = grid.GetHtml(
             tableStyle: "table table-responsive backcolor",
             headerStyle: "wedgrid-header",
             footerStyle: "webgrid-footer",
             alternatingRowStyle: "webgrid-alternating-row",
             rowStyle: "webgrid-row-style",
             columns: grid.Columns(
              grid.Column(columnName: "name", header: "Name"),
              grid.Column(columnName: "directorate", header: "Directorate"),
              grid.Column(columnName: "team", header: "Team"),
              grid.Column(columnName: "gradeDescription", header: "Grade"),
              grid.Column(columnName: "gradePointDescription", header: "Pay Point"),
              grid.Column(columnName: "reviewDate", header: "Review"),
              grid.Column(columnName: "submittedByName", header: "By:"),
              grid.Column(columnName: "statusDisplay", header: "Status"),
              grid.Column(columnName: "reason", header: "Reason")
                            )).ToString();
            Response.ClearContent();
            //give name to excel sheet.
            Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            //specify content type
            Response.ContentType = "application/excel";
            //write excel data using this method 
            Response.Write(gridData);
            Response.End();
        }

        public ActionResult GetPayPointRequest(int Id)
        {
            ReportsManager rptManager = new ReportsManager();
            var response = rptManager.GetEmployeePaypointSubmission(Id);
            PayPointPopUpViewModel model = new JavaScriptSerializer().Deserialize<PayPointPopUpViewModel>(response);
            if (model.ceoId == GetUserIdFromSession() && model.payPoint.paypointStatusDescription == ApplicationConstants.supported)
            {
                model.isCeo = 1;
            }
            else
            {
                model.isCeo = 0;
            }
            return PartialView("~/Views/Reports/_PayPointDirectorPopUp.cshtml", model);
        }

        public ActionResult GetPayPointRequestReadonly(int Id)
        {
            ReportsManager rptManager = new ReportsManager();
            var response = rptManager.GetPaypointById(Id);
            PayPointPopUpViewModel model = new JavaScriptSerializer().Deserialize<PayPointPopUpViewModel>(response);
            if (model.ceoId == GetUserIdFromSession() && model.payPoint.paypointStatusDescription == ApplicationConstants.supported)
            {
                model.isCeo = 1;
            }
            else
            {
                model.isCeo = 0;
            }
            return PartialView("~/Views/Reports/_PayPointReadOnlyPopUp.cshtml", model);
        }

        public ActionResult UpdatePaypointStatus(PayPointStatus data)
        {
            ReportsManager rptManager = new ReportsManager();
            PayPointStatusDataRequest dataRequest = new PayPointStatusDataRequest();
            dataRequest = Mapper.Map<PayPointStatus, PayPointStatusDataRequest>(data);
            var response = rptManager.UpdatePaypoint(dataRequest);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region >>> Health List Report <<<

        public ActionResult GetHealthReport(int id = 0)
        {
            HealthReportGridViewModel model = GetHealthList();
            return PartialView("~/Views/Reports/_HealthReport.cshtml", model);
        }

        

        public ActionResult SearchHealthList()
        {
            HealthReportGridViewModel model = GetHealthList();
            return PartialView("~/Views/Reports/_HealthReportGrid.cshtml", model);
        }

        public ActionResult ExportHealthData(HealthReportDataRequest request)
        {
            var manager = new ReportsManager();
            var model = new HealthReportGridViewModel();
            request.sortBy = "fullName";
            request.sortOrder = "ASC";
            if (string.IsNullOrEmpty(request.searchText))
            {
                request.searchText = "";
            }
            if (request.directorate == 0)
            {
                request.directorate = null;
            }
            if (request.teamId == 0)
            {
                request.teamId = null;
            }
            request.pagination = new PaginationDataRequest();
            request.pagination.pageNo = 1;
            request.pagination.pageSize = _ExportPageSize;
            var response = manager.GetHealthList(request);
            if (response != "[]")
            {
                model = new JavaScriptSerializer().Deserialize<HealthReportGridViewModel>(response);
            }
            var fileName = "Health_{0}_{1}_{2}.xls";
            fileName = string.Format(fileName, DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            PrintHealthExcel(model.healthReportListing.employeeList, fileName);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public void PrintHealthExcel(List<HealthViewModel> objSource, string fileName)
        {
            WebGrid grid = new WebGrid(source: objSource, canPage: false, canSort: false);
            string gridData = grid.GetHtml(
             tableStyle: "table table-responsive backcolor",
             headerStyle: "wedgrid-header",
             footerStyle: "webgrid-footer",
             alternatingRowStyle: "webgrid-alternating-row",
             rowStyle: "webgrid-row-style",
             columns: grid.Columns(
             grid.Column(columnName: "loggedByString", header: "Logged By"),
             grid.Column(columnName: "fullName", header: "Employee"),
             grid.Column(columnName: "directorate", header: "Directorate:"),
             grid.Column(columnName: "team", header: "Team:"),
             grid.Column(columnName: "disabilityType", header: "Type"),
             grid.Column(columnName: "description", header: "Description"),
             grid.Column(columnName: "notifiedDate", header: "Notified Date")
                            )).ToString();
            Response.ClearContent();
            //give name to excel sheet.
            Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            //specify content type
            Response.ContentType = "application/excel";
            //write excel data using this method 
            Response.Write(gridData);
            Response.End();
        }
        #endregion

        #region >>> Mandatory Trainings Report <<<

        public ActionResult GetMandatoryTrainings(int id = 0)
        {
            int teamid = Convert.ToInt32(Request.QueryString["teamId"]);
            ViewBag.selectedTeamId = teamid;
            if (teamid == 0)
            {
                MandatoryTrainGridViewModel model = GetTrainings();
                model.team = teamid;
                return PartialView("~/Views/Reports/_MandatoryTraining.cshtml", model);
            }
            else
            {
                MandatoryTrainGridViewModel model = GetTrainings(teamid);
                model.team = teamid;
                return PartialView("~/Views/Reports/_MandatoryTraining.cshtml", model);
            }

        }

        

        public ActionResult SearchTrainings(int teamid = 0)
        {
            MandatoryTrainGridViewModel model = GetTrainings(teamid);
            return PartialView("~/Views/Reports/_MandatoryTrainingGrid.cshtml", model);
        }

        #endregion


        #region >>> Trainings Report <<<

        public ActionResult GetGroupTrainingsReport()
        {
            GroupTrainingApprovalReportViewModel model = GetGroupTrainings(false);
            return PartialView("~/Views/Reports/GroupTraining/_GroupTrainingReport.cshtml", model);
        }



        public ActionResult SearchGroupTrainings()
        {
            GroupTrainingApprovalReportViewModel model = GetGroupTrainings(true);
            return PartialView("~/Views/Reports/GroupTraining/_GroupTrainingGrid.cshtml", model);
        }

        public ActionResult ExportGroupTrainingsData(GroupTrainingApprovalDataRequest request)
        {
            var manager = new ReportsManager();
            var model = new GroupTrainingApprovalReportViewModel();
            request.sortBy = "employeeName";
            request.sortOrder = "ASC";
            if (string.IsNullOrEmpty(request.searchText))
            {
                request.searchText = "";
            }
            if (request.directorateId == 0)
            {
                request.directorateId = null;
            }
            if (request.teamId == 0)
            {
                request.teamId = null;
            }
            request.isSearch = true;
            request.pagination = new PaginationDataRequest();
            request.pagination.pageNo = 1;
            request.pagination.pageSize = _ExportPageSize;
            var response = manager.GetGroupTrainings(request);
            if (response != "[]")
            {
                model = new JavaScriptSerializer().Deserialize<GroupTrainingApprovalReportViewModel>(response);
            }
            var fileName = "GroupTraining_{0}_{1}_{2}.xls";
            fileName = string.Format(fileName, DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            PrintGroupTrainingsExcel(model.trainingApprovalList, fileName);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public void PrintGroupTrainingsExcel(List<GroupTrainingApprovalViewModel> objSource, string fileName)
        {
            WebGrid grid = new WebGrid(source: objSource, canPage: false, canSort: false);
            string gridData = grid.GetHtml(
             tableStyle: "table table-responsive backcolor",
             headerStyle: "wedgrid-header",
             footerStyle: "webgrid-footer",
             alternatingRowStyle: "webgrid-alternating-row",
             rowStyle: "webgrid-row-style",
             columns: grid.Columns(
                grid.Column(columnName: "employeeName", header: "Name:"),
                grid.Column(columnName: "directorateName", header: "Directorate:"),
                grid.Column(columnName: "teamName", header: "Team:"),
                grid.Column(columnName: "trainingCourseName", header: "Training Course:"),
                grid.Column(columnName: "cost", header: "Cost(NET):"),
                grid.Column(format: (item) => item.professionalQualification == true ? "Yes" : "No", header: "Professional Qual:"),
                grid.Column(format: (item) => item.isMandatory == true ? "Yes" : "No", header: "Mandatory:"),
                grid.Column(columnName: "status", header: "Status:"),
                grid.Column(columnName: "createdBy", header: "By:")
                            )).ToString();
            Response.ClearContent();
            //give name to excel sheet.
            Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            //specify content type
            Response.ContentType = "application/excel";
            //write excel data using this method 
            Response.Write(gridData);
            Response.End();
        }

        public ActionResult ApproveDeclineGroupTrainings(GroupTrainingApproveDeclineDataRequest request)
        {
            var manager = new ReportsManager();
            request.updatedBy = GetUserIdFromSession();
            request.trainingId = StringToIntList(request.trainingIds);
            var response = manager.ApproveDeclineGroupTrainings(request);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region >>> Declaration Of Interests Report <<<

        public ActionResult DeclarationOfInterestsReport()
        {
            DeclarationOfInterestsReportViewModel model = getDoIReportPopulate();
            return PartialView("~/Views/Reports/_DeclarationOfInterestsReport.cshtml", model);
        }

        public ActionResult GetDeclarationOfInterestsRequest(int Id)
        {
            var manager = new ReportsManager();
            var response = manager.GetDeclarationOfInterestById(Id);

            DeclarationViewModel model = new JavaScriptSerializer().Deserialize<DeclarationViewModel>(response);

            if (model.declarationOfInterest.page1.isSecondaryEmployment.HasValue)
            {
                model.declarationOfInterest.page1.isEmployment = model.declarationOfInterest.page1.isSecondaryEmployment.Value;
            }

            if (model.declarationOfInterest.page2.isConApprovedByLeadership.HasValue)
            {
                model.declarationOfInterest.page2.isLeadership = model.declarationOfInterest.page2.isConApprovedByLeadership.Value;
            }

            if (model.declarationOfInterest.page2.isDirectorP8.HasValue)
            {
                model.declarationOfInterest.page2.isTempDirectorP8 = model.declarationOfInterest.page2.isDirectorP8.Value;
            }

            if (model.declarationOfInterest.page2.isDirectorP9.HasValue)
            {
                model.declarationOfInterest.page2.isTempDirectorP9 = model.declarationOfInterest.page2.isDirectorP9.Value;
            }
            model.declarationOfInterest.page3 = model.declarationOfInterest.page3;

            return PartialView("~/Views/Reports/_DeclarationOfInterestsPopup.cshtml", model);
        }

        public ActionResult SearchDeclarationOfInterest()
        {
            DeclarationOfInterestsReportViewModel model = getDoIReportPopulate();
            return PartialView("~/Views/Reports/_DeclarationOfInterestsGrid.cshtml", model);
        }

        public ActionResult ExportDeclarationOfInterestsData(DeclarationOfInterestsReportViewModel model)
        {
            var manager = new ReportsManager();
            var dataRequest = new DeclarationOfInterestsDataRequest();
            dataRequest.pagination = new BusinessModule.NetworkObjects.PaginationDataRequest();
            if (Session["__Report__"] != null && Session["__Report__"].ToString() == "DeclarationOfInterests")
            {
                dataRequest.lineManagerId = GetUserIdFromSession();
            }
           
            dataRequest.fiscalYearId = model.fiscalYearId;
            dataRequest.teamId = model.teamId;
            dataRequest.status = model.status;
            dataRequest.sortBy = "employeeName";
            dataRequest.sortOrder = "desc";
            if (string.IsNullOrEmpty(dataRequest.searchText))
            {
                dataRequest.searchText = "";
            }
            if (dataRequest.directorate == 0)
            {
                dataRequest.directorate = null;
            }
            if (dataRequest.teamId == 0)
            {
                dataRequest.teamId = null;
            }
            dataRequest.pagination.pageSize = _ExportPageSize;
            dataRequest.pagination.pageNo = 1;
            var response = manager.GetDOIReport(dataRequest);
            if (response != "[]")
            {
                model = new JavaScriptSerializer().Deserialize<DeclarationOfInterestsReportViewModel>(response);
            }

            var fileName = "DOI_{0}_{1}_{2}.xls";
            fileName = string.Format(fileName, DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            PrintDeclarationOfInterestsExcel(model.declarationOfInterestsListing.DOIs, fileName);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public void PrintDeclarationOfInterestsExcel(List<DeclarationOfInterestVM> objSource, string fileName)
        {
            WebGrid grid = new WebGrid(source: objSource, canPage: false, canSort: false);
            string gridData = grid.GetHtml(
             tableStyle: "table table-responsive backcolor",
             headerStyle: "wedgrid-header",
             footerStyle: "webgrid-footer",
             alternatingRowStyle: "webgrid-alternating-row",
             rowStyle: "webgrid-row-style",
             columns: grid.Columns(
              grid.Column(columnName: "submitted", header: "Submitted"),
              grid.Column(columnName: "employeeName", header: "Name"),
              grid.Column(columnName: "jobRoleName", header: "Job Role"),
              grid.Column(columnName: "lineManagerName", header: "Line Manager"),
              grid.Column(columnName: "directorateName", header: "Directorate:"),
              grid.Column(columnName: "teamName", header: "Team"),
              grid.Column(columnName: "directorName", header: "Director"),
              grid.Column(columnName: "statusDisplay", header: "Status")
                            )).ToString();
            Response.ClearContent();
            //give name to excel sheet.
            Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            //specify content type
            Response.ContentType = "application/excel";
            //write excel data using this method 
            Response.Write(gridData);
            Response.End();
        }

        public ActionResult UpdateDeclarationOfInterestStatus(DeclarationOfInterestStatusViewModel data)
        {
            ReportsManager rptManager = new ReportsManager();
            DeclarationOfInterestStatusDataRequest dataRequest = new DeclarationOfInterestStatusDataRequest();
            dataRequest = Mapper.Map<DeclarationOfInterestStatusViewModel, DeclarationOfInterestStatusDataRequest>(data);
            dataRequest.lastActionUser = GetUserIdFromSession();
            var response = rptManager.UpdateDeclarationOfInterestStatus(dataRequest);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region >>> Gifts Report <<<

        public ActionResult GetEmployeesGifts(int id, int giftId = 0)
        {
            var manager = new PersonalDetailManager();
            var request = new GetPersonalDetailDataRequest();
            var model = new ViewModels.GiftAndHospitalityViewModel();
            var gifts = new List<GiftViewModel>();
            request.employeeId = id;

            var response = manager.getEmployeeGifts(request);
            if (response != "[]")
            {
                model.gifts = new JavaScriptSerializer().Deserialize<List<GiftViewModel>>(response);
            }

            model.employeeId = id;
            if (giftId > 0)
            {
                model.received = model.lstReceived.Where(e => e.giftId == giftId).FirstOrDefault();
                if (model.received != null)
                {
                    model.received.offeredBy = model.received.offerToBy;
                }
                model.given = model.lstGiven.Where(e => e.giftId == giftId).FirstOrDefault();
                if (model.given != null)
                {
                    model.given.offeredTo = model.given.offerToBy;
                }
            }


            return PartialView("~/Views/Reports/Gifts/_GiftApprovalPopup.cshtml", model);
        }

        public JsonResult EmployeeSelectListByDircetorateJson(string Origin, string Target, string Value)
        {
            var manager = new ReportsManager();
            var response = manager.GetEmployeeByDirectorate(int.Parse(Value));
            AnnualLeaveGridViewModel model = new JavaScriptSerializer().Deserialize<AnnualLeaveGridViewModel>(response);
            return Json(model.employeeLookUps);
        }

        public ActionResult GetGiftsReport(int id = 0)
        {
            GiftReportViewModel model = GetGifts();

            return PartialView("~/Views/Reports/Gifts/_GiftReport.cshtml", model);
        }

        public ActionResult SearchGifts()
        {
            GiftReportViewModel model = GetGifts();
            return PartialView("~/Views/Reports/Gifts/_GiftGrid.cshtml", model);
        }

        public ActionResult ExportGiftData(GiftReportViewModel model)
        {
            var manager = new ReportsManager();
            var dataRequest = new GiftDataApprovalRequest();
            dataRequest.pagination = new BusinessModule.NetworkObjects.PaginationDataRequest();
            
            dataRequest.fromDate = Convert.ToDateTime(model.fromDate);
            dataRequest.toDate = Convert.ToDateTime(model.toDate);
            dataRequest.employeeId = model.employeeId;
            dataRequest.directorateId = model.directorateId;
            dataRequest.statusId = model.statusId;

            dataRequest.sortBy = "employeeName";
            dataRequest.sortOrder = "desc";
            dataRequest.searchText = "";
            dataRequest.pagination.pageSize = _ExportPageSize;
            dataRequest.pagination.pageNo = 1;
            var response = manager.GetGifts(dataRequest);
            if (response != "[]")
            {
                model = new JavaScriptSerializer().Deserialize<GiftReportViewModel>(response);
            }

            var fileName = "Gifts_{0}_{1}_{2}.xls";
            fileName = string.Format(fileName, DateTime.Now.Day, DateTime.Now.Month, DateTime.Now.Year);
            PrintGiftsExcel(model.giftsAppovalList, fileName);
            return Json(true, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #endregion
    }
}