﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace BHGHRModuleWeb
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Bridge", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "EmployeeRoute",
                url: "{area}/{controller}/{action}/{id}",
                defaults: new { area="Employees", controller = "Dashboard", action = "Index", id = UrlParameter.Optional }
            );
            routes.MapRoute(
                name: "RemitanceSlipRoute",
                url: "{controller}/{action}",
                defaults: new { controller = "Remitance", action = "SendEmail" }
            );
        }
    }
}
