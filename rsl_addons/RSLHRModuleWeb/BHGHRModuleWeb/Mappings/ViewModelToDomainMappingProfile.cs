﻿using AutoMapper;
using BHGHRModuleWeb.ViewModels;
using BusinessModule.NetworkObjects;
using BusinessModule.NetworkManagers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TVM = BHGHRModuleWeb.ViewModels.Admin.Training;

namespace BHGHRModuleWeb.Mappings
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<BenefitsViewModel, BenefitsDataRequest>();

            Mapper.CreateMap<SkillsDetailViewModel, SkillsDataRequest>();
            Mapper.CreateMap<HealthDetailViewModel,HealthDataRequest>();
            Mapper.CreateMap<EmployeeDetailViewModel, PersonalDetailDataRequest>();
            Mapper.CreateMap<ContactDetailViewModel, ContactDetailDataRequest>();
            Mapper.CreateMap<JobDetailViewModel, JobDetailDataRequest>();
            Mapper.CreateMap<WorkingHoursHistoryViewModel, WorkingHoursDataRequest>();
            Mapper.CreateMap<DOIPage1ViewModel, DOIPage1>();
            Mapper.CreateMap<DOIPage2ViewModel, DOIPage2>();
            Mapper.CreateMap<DOIPage3ViewModel, DOIPage3>();
            Mapper.CreateMap<DeclarationOfInterestViewModel, DeclarationOfInterestDataRequest>();
			Mapper.CreateMap<DocumentUploadViewModel, DocumentDataRequest>();
            Mapper.CreateMap<TrainingDetailViewModel, TrainingDataRequest>();
            Mapper.CreateMap<TVM.ApprovedTrainingViewModel, ApprovedTrainingDataRequest>();
            Mapper.CreateMap<TrainingUpdateViewModel, TrainingUpdateDataRequest>();
            Mapper.CreateMap<GiftAndHospitalityViewModel, GiftDataRequest>();
            Mapper.CreateMap<GiftViewModel, Gift>();
            Mapper.CreateMap<ProposedSalaryViewModel, SalaryInformationDataRequest>();
            Mapper.CreateMap<PayPointStatus, PayPointStatusDataRequest>();
            Mapper.CreateMap<BenefitVehicleViewModel, BenefitVehicleDataRequest>();
            Mapper.CreateMap<BenefitGeneralViewModel, BenefitGeneralDataRequest>();
            Mapper.CreateMap<GeneralRequestViewModel, GeneralDataRequest>();
            Mapper.CreateMap<BenefitSubscriptionViewModel, BenefitSubscriptionDataRequest>();
            Mapper.CreateMap<BenefitHealthViewModel, BenefitHealthDataRequest>();
            Mapper.CreateMap<BenefitPensionViewModel, BenefitPensionDataRequest>();
            Mapper.CreateMap<BenefitPRPViewModel, BenefitPRPDataRequest>();
            Mapper.CreateMap<LanguageSkillsViewModel, LanguageSkillsDataRequest>();
            Mapper.CreateMap<AccessControlViewModel, AccessControlSaveRequest>();
            Mapper.CreateMap<TeamViewModel, TeamSaveDataRequest>();
            Mapper.CreateMap<GradePointSalaryViewModel, GradePointSalaryDataRequest>();
            Mapper.CreateMap<DeclarationOfInterestStatusViewModel, DeclarationOfInterestStatusDataRequest>();
            Mapper.CreateMap<WorkingHoursViewModel, EmployeeWorkingHoursDataRequest>();
            Mapper.CreateMap<TVM.GroupTrainingDetailViewModel, GroupTrainingDataRequest>();
			Mapper.CreateMap<DeclarationOfInterestReviewStatusViewModel, DeclarationOfInterestStatusDataRequest>();
            Mapper.CreateMap<JobDetailNoteViewModel, JobDetailNoteDataRequest>();
        }
    }
}