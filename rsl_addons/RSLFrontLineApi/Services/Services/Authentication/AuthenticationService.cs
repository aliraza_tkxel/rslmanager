﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessModels;
using DataModel.DatabaseEntities;
using Utilities;

namespace Services
{
   public class AuthenticationService: BaseService
    {
        public LoginResponseModel AuthenticateUser(LoginRequestModel request)
        {
            LoginResponseModel response = new LoginResponseModel();
            response = RepositryUnit.authenticationRepository.AuthenticateUser(request);           
            return response;
        }
           
        public bool EmailForgotPassword(string emailId)
        {
            EmailTemplateResponseModel userInfo = new EmailTemplateResponseModel();            
            userInfo = RepositryUnit.authenticationRepository.getUserInfoByEmail(emailId);
            if (userInfo != null)
            {
                String body = "User Name: " + userInfo.login + "<br>Password: " + userInfo.password;
                EmailHelper.sendHtmlFormattedEmail(userInfo.workEmail, userInfo.workEmail, UserMessageConstants.LoginEmailSubjectMsg, body);
                              
            }
            else
            {              
                throw new ArgumentException(UserMessageConstants.EmailIdNotMatchMsg);
            }
            return true;
        }


    }
}
