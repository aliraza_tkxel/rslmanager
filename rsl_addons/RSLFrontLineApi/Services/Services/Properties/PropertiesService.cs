﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessModels;
namespace Services
{
   public class PropertiesService:BaseService
    {

        #region Search property
        public PropertiesSearchResponseModel SearchProperties(bool isLookUpRequired)
        {
            PropertiesSearchResponseModel response = new PropertiesSearchResponseModel();
            response = RepositryUnit.propertiesRepository.SearchProperties();
            if (isLookUpRequired)
                response.lookups = loadPropertyLookUps();
            return response;
        }
        #endregion

        #region Get property details
        public PropertyInfoResponseModel GetPropertyDetails(string propertyId)
        {
            PropertyInfoResponseModel response = new PropertyInfoResponseModel();
            response = RepositryUnit.propertiesRepository.GetPropertiesInfo(propertyId);
            return response;
        }
        #endregion

        #region Get property viewings
        public List<PropertyViewingResponseModel> GetPropertyViewing(PropertyViewingRequestModel request)
        {
            return RepositryUnit.propertiesRepository.GetPropertyViewing(request);
        }

        #endregion

        #region Fetch look ups
        public PropertyLookUpsDataModel loadPropertyLookUps()
        {            
            PropertyLookUpsDataModel propertyLookUpsModel = new PropertyLookUpsDataModel();

            //schemes lookup
            var schemesList = this.RepositryUnit.schemesRepository.GetAll().ToList();
            propertyLookUpsModel.schemeModel = schemesList.Select(x => new LookUpResponseModel { id = x.SCHEMEID, description = x.SCHEMENAME }).ToList();           
            return propertyLookUpsModel;
        }
        #endregion

        #region  Get property financial info on start tenancy screen 
        public PropertyRentInfoDataModel GetPropertyFinancialInfo(string propertyId)
        {
            return RepositryUnit.propertyRentInfoRepository.GetPropertyFinancialInfo(propertyId);
        }
        #endregion

        }
}
