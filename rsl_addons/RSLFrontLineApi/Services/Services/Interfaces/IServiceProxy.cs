﻿using Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.BusinessLogic.Interfaces

{
   public interface IServiceProxy
    {
        CustomerService CustomerService { get; }
        AuthenticationService AuthenticationService { get; }
        PropertiesService PropertiesService { get; }
        ViewingsService ViewingsService { get; }
        SharedService SharedService { get; }
        AlertsService AlertsService { get; }
    }
}
