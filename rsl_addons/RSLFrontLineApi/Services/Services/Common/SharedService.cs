﻿using BusinessModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities;

namespace Services
{
   public class SharedService : BaseService
    {   
        #region load shared lookups       
        public SharedLookupResponseModel LoadSharedLookUps()
        {
            SharedLookupResponseModel sharedLookUpsModel = new SharedLookupResponseModel();

            var propertyTypeList = RepositryUnit.propertyTypeRepository.Find(x=>x.DESCRIPTION != ApplicationConstants.CarSpaceType && x.DESCRIPTION != ApplicationConstants.GarageType).ToList();
            sharedLookUpsModel.propertyType = propertyTypeList.Select(x => new LookUpResponseModel { id = x.PROPERTYTYPEID, description = x.DESCRIPTION }).ToList();

            var customerTypeList = RepositryUnit.customerTypeRepository.GetAll().ToList();
            sharedLookUpsModel.customerType = customerTypeList.Select(x => new LookUpResponseModel { id = x.CUSTOMERTYPEID, description = x.DESCRIPTION }).ToList();

            var supportAgenciesList = this.RepositryUnit.supportAgenciesRepository.GetAll().ToList();
            sharedLookUpsModel.supportAgencies = supportAgenciesList.Select(x => new LookUpResponseModel { id = x.AGID, description = x.DESCRIPTION }).ToList();

            var adaptionsList = this.RepositryUnit.adaptionsRepository.GetAll().ToList();
            sharedLookUpsModel.adaptions = adaptionsList.Select(x => new LookUpResponseModel { id = x.AdaptationId, description = x.Description }).ToList();

            var bankingFacilityList = this.RepositryUnit.bankingFacilityRepository.GetAll().ToList();
            sharedLookUpsModel.bankingFacility = bankingFacilityList.Select(x => new LookUpResponseModel { id = x.BankId, description = x.DESCRIPTION }).ToList();

            var employmentStatusList = this.RepositryUnit.employmentStatusRepository.GetAll().ToList();
            sharedLookUpsModel.employmentStatus = employmentStatusList.Select(x => new LookUpResponseModel { id = x.EMPLOYMENTSTATUSID, description = x.DESCRIPTION }).ToList();

            var disabilityList = this.RepositryUnit.disabilityRepository.GetAll().ToList();
            sharedLookUpsModel.disability = disabilityList.Select(x => new LookUpResponseModel { id = x.DISABILITYID, description = x.DESCRIPTION }).ToList();


            var referralsHelpCategoryList = this.RepositryUnit.referralHelpCategoryRepository.GetAll().ToList();
            sharedLookUpsModel.referralsHelpCategories = referralsHelpCategoryList.Select(x => new LookUpResponseModel { id = x.CATEGORYID, description = x.DESCRIPTION }).ToList();

            var referralsHelpSubCategoryList = this.RepositryUnit.referralHelpSubCategoryRepository.GetReferralHelpSubCategory().ToList();
            sharedLookUpsModel.referralsHelpSubCategories = referralsHelpSubCategoryList.Select(x => new DependentLookupResponseModel { id = x.id, description = x.description, subId = x.subId }).ToList();

            var referralsStagesList = this.RepositryUnit.referralStagesRepository.GetAll().ToList();
            sharedLookUpsModel.referralsStages = referralsStagesList.Select(x => new LookUpResponseModel { id = x.STAGEID, description = x.DESCRIPTION }).ToList();

            var TerminationReasonList = this.RepositryUnit.terminationReasonRepository.Find(x=>x.ACTIVE==true).OrderBy(x=>x.DESCRIPTION).ToList();
            sharedLookUpsModel.terminationResaon = TerminationReasonList.Select(x => new LookUpResponseModel { id = x.REASONID, description = x.DESCRIPTION }).ToList();

            var terminationCatList = RepositryUnit.terminationRepository.GetTerminationReasonCategory();
            sharedLookUpsModel.terminationResaonCategory = terminationCatList.Select(x => new DependentLookupResponseModel { id = x.id, description = x.description, subId = x.subId }).ToList();

            var customerTitlesList = this.RepositryUnit.customerTitleRepository.GetAll().ToList();
            sharedLookUpsModel.customerTitles = customerTitlesList.Select(x => new LookUpResponseModel { id = x.TITLEID, description = x.DESCRIPTION }).ToList();

            var benefitsList = this.RepositryUnit.benefitsRepository.GetAll().ToList();
            sharedLookUpsModel.benefits = benefitsList.Select(x => new LookUpResponseModel { id = x.BenefitId, description = x.DESCRIPTION }).ToList();

            var yesNoList = this.RepositryUnit.yesNoRepository.GetAll().ToList();
            sharedLookUpsModel.yesNo = yesNoList.Select(x => new LookUpResponseModel { id = x.YESNOID, description = x.DESCRIPTION }).ToList();

            var yesNoUnKnownList = this.RepositryUnit.yesNoUnKnownRepository.GetAll().ToList();
            sharedLookUpsModel.yesNoUnkown = yesNoUnKnownList.Select(x => new LookUpResponseModel { id = x.YESNOID, description = x.DESCRIPTION }).ToList();

            var AddressTypeList = this.RepositryUnit.addressTypeRepository.GetAll().ToList();
            sharedLookUpsModel.addressType = AddressTypeList.Select(x => new LookUpResponseModel { id = x.ADDRESSTYPEID, description = x.DESCRIPTION }).ToList();

            var relationshipList = this.RepositryUnit.relationShipRepository.GetAll().ToList();
            sharedLookUpsModel.relationshipStatus = relationshipList.Select(x => new LookUpResponseModel { id = x.RELATIONSHIPID, description = x.DESCRIPTION }).ToList();

            var riskVulnerabilityStatusList = this.RepositryUnit.riskVulnerabilityStatusRespository.GetAll().ToList();
            sharedLookUpsModel.riskVulnerabilityStatus = riskVulnerabilityStatusList.Select(x => new LookUpResponseModel { id = x.ITEMSTATUSID, description = x.DESCRIPTION }).ToList();

            var teamsList = this.RepositryUnit.teamsRepository.GetTeams().ToList();
            sharedLookUpsModel.teams = teamsList.Select(x => new LookUpResponseModel { id = x.TEAMID, description = x.TEAMNAME }).ToList();

            var employeesList = this.RepositryUnit.employeesRepository.GetEmployees().ToList();
            sharedLookUpsModel.employees = employeesList.Select(x => new DependentLookupResponseModel { id = x.id, description = x.description,subId =x.subId }).ToList();

            var riskCategoryList = this.RepositryUnit.riskCategoryRepository.GetAll().ToList();
            sharedLookUpsModel.riskCategory = riskCategoryList.Select(x => new LookUpResponseModel { id = x.CategoryId, description = x.Description}).ToList();

            var riskSubCategoryList = this.RepositryUnit.riskSubCategoryRepository.GetAll().ToList();
            sharedLookUpsModel.riskSubCategory = riskSubCategoryList.Select(x => new DependentLookupResponseModel { id = x.SubCategoryId, description = x.Description, subId = x.CategoryId }).ToList();

            var vulnerabilityCategoryList = this.RepositryUnit.vulnerabilityCategoryRepository.GetAll().ToList();
            sharedLookUpsModel.vulnerabilityCategory = vulnerabilityCategoryList.Select(x => new LookUpResponseModel { id = x.CategoryId, description = x.Description}).ToList();

            var vulnerabilitySubCategoryList = this.RepositryUnit.vulnerabilitySubCategoryRepository.GetAll().ToList();
            sharedLookUpsModel.vulnerabilitySubCategory = vulnerabilitySubCategoryList.Select(x => new DependentLookupResponseModel { id = x.SubCategoryId, description = x.Description, subId = x.CategoryId }).ToList();

            var letterActionList = this.RepositryUnit.letterActionRepository.GetAll().ToList();
            sharedLookUpsModel.letterAction = letterActionList.Select(x => new LookUpResponseModel { id = x.ACTIONID, description = x.DESCRIPTION }).ToList();

            var viewingCommentsTypes = this.RepositryUnit.viewingCommentsRepository.GetViewingCommentsTypes();
            sharedLookUpsModel.viewingCommentTypes = viewingCommentsTypes;
            return sharedLookUpsModel;
        }
        #endregion
    }
}
