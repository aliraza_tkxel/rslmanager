﻿using Services.BusinessLogic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
    public class ServicesProxy : IServiceProxy
    {
        private CustomerService _customerService;
        private AuthenticationService _authenticationService;
        private PropertiesService _propertiesService;
        private ViewingsService _viewingsService;
        private SharedService _sharedService;
        private AlertsService _alertsService;      
        public AuthenticationService AuthenticationService
        {
            get
            {
                if(_authenticationService == null)
                {
                    _authenticationService = new AuthenticationService();
                }
                return _authenticationService;
            }
        }

        public CustomerService CustomerService
        {
            get
            {
                if (_customerService == null)
                {
                    _customerService = new CustomerService();
                }
                return _customerService;
            }
        }

        public PropertiesService PropertiesService
        {
            get
            {
               if(_propertiesService == null)
                {
                    _propertiesService = new PropertiesService();
                }
                return _propertiesService;
            }
        }

        public ViewingsService ViewingsService
        {
            get
            {
               if(_viewingsService == null)
                {
                    _viewingsService = new ViewingsService();
                }
                return _viewingsService;
            }
        }

        public SharedService SharedService
        {
            get
            {
                if(_sharedService == null)
                {
                    _sharedService = new SharedService();
                }
                return _sharedService;
            }
        }
       
       public AlertsService AlertsService
        {
            get
            {
                if (_alertsService == null)
                {
                    _alertsService = new AlertsService();
                }
                return _alertsService;
            }
        }
    }
}
