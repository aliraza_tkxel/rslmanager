﻿using DataModel.UnitOfWork;

namespace Services
{
    public class BaseService
    {
        private IUnitOfWork _repositryUnit;
        public BaseService()
        {

        }

        public BaseService(IUnitOfWork repositryUnit)
        {
            this.RepositryUnit = repositryUnit;
        }

        public IUnitOfWork RepositryUnit
        {
            get
            {
                if (_repositryUnit == null) _repositryUnit = new UnitOfWork();
                return _repositryUnit;
            }
            private set { _repositryUnit = value; }
        }

        public void Dispose()
        {
            this.RepositryUnit = null;
        }
    }
}
