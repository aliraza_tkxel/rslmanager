﻿using AutoMapper;
using BusinessModels;
using DataModel.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
   public class AutoMapperBootStarper
    {
        public static void BootStrap()
        {

            Mapper.Initialize(cfg =>
            {

                //Mapping entity to DTO
                cfg.CreateMap<FLS_CustomerCurrentHome, CustomerCurrentHomeDataModel>().ReverseMap()
               .ForMember(dest => dest.CurrentHomeId, opt => opt.Ignore())
               ;

                cfg.CreateMap<FLS_GetAvailableOperatives_Result, HousingOfficerResponseModel>()
                .ForMember(res => res.employeeId, opt => opt.MapFrom(x => x.EmployeeId))
                .ForMember(res => res.email, opt => opt.MapFrom(x => x.Email))
                .ForMember(res => res.firstName, opt => opt.MapFrom(x => x.FirstName))
                .ForMember(res => res.gender, opt => opt.MapFrom(x => x.Gender))
                .ForMember(res => res.imageName, opt => opt.MapFrom(x => x.ImageName))
                .ForMember(res => res.imagePath, opt => opt.MapFrom(x => x.ImageName))
                .ForMember(res => res.lastName, opt => opt.MapFrom(x => x.LastName));

                cfg.CreateMap<FLS_Viewings, ArrangeViewingRequestModel>().ReverseMap()
                .ForMember(dest => dest.ViewingId , opt => opt.Ignore())
                ;

                cfg.CreateMap<FLS_ViewingComments, ViewingCommentRequestModel>().ReverseMap();

                cfg.CreateMap<FLS_GetAvailableOperativesForVoidInspection_Result, Supervisor>().ReverseMap();

                // cfg.CreateMap<C_JOURNAL, JournalDataModel>().ReverseMap()
                // .ForMember(dest => dest.JOURNALID, opt => opt.Ignore())
                // ;

                // cfg.CreateMap<C_REFERRAL, ReferralDataModel>().ReverseMap()
                //.ForMember(dest => dest.REFERRALHISTORYID, opt => opt.Ignore())
                //;

                // cfg.CreateMap<C_REFERRAL_CUSTOMER_HELPSUBCATEGORY , ReferralCustomerHelpSubCategoryDataModel>().ReverseMap()
                //.ForMember(dest => dest.SID, opt => opt.Ignore())
                //;

                cfg.CreateMap<C_ADDRESS, EmergencyContactDataModel>().ReverseMap()
              .ForMember(dest => dest.CUSTOMERID, opt => opt.Ignore())
              ;



                // End of mapping


            });


        }

        //internal static class MapperExtesions

        //{

        //    public static IMappingExpression<TSource, TDestination> IgnoreAllNonExisting<TSource, TDestination>(this AutoMapper.IMappingExpression<TSource, TDestination> expression)
        //    {
        //        var sourceType = typeof(TSource);
        //        var destinationType = typeof(TDestination);
        //        var existingMaps = Mapper.GetAllTypeMaps().First(x => x.SourceType.Equals(sourceType) && x.DestinationType.Equals(destinationType));
        //        foreach (var property in existingMaps.GetUnmappedPropertyNames())
        //        {
        //            expression.ForMember(property, opt => opt.Ignore());
        //        }
        //        return expression;
        //    }

        //}
    }
}
