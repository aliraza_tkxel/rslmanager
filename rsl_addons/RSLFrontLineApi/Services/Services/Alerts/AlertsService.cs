﻿using BusinessModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModel.DatabaseEntities;
namespace Services
{
    public class AlertsService: BaseService
    {
        #region Get Customer Risks
        public List<RisksListingDataModel> GetRisksList(int? statusId,int?customerId)
        {
            return RepositryUnit.risksRepository.GetRisksList(statusId,customerId);
        }
        #endregion

        #region Add/Edit Risk details
        public RisksListingDataModel AddEditRisks(RisksListingDataModel request)
        {
            C_RISK risk = new C_RISK();
            C_Customer_Risk custRisk = new C_Customer_Risk();
            //inserting data in C_RISK
            risk = RepositryUnit.risksRepository.AddEditRisks(request);
            RepositryUnit.Commit();
            request.riskHistoryId = risk.RISKHISTORYID;
            //inserting data in C_Customer_Risk
            string riskSubCat = request.customerRiskSubcategories;
            foreach (var data in riskSubCat.Split(','))
            {
                int subCat;
                if (int.TryParse(data, out subCat) == true)
                {
                    request.subCategoryId = subCat;
                    RepositryUnit.customerRisksRepository.AddEditCustomerRisks(request);
                    RepositryUnit.Commit();
                }

            }
            //inserting data in C_JOURNAL
            RepositryUnit.journalRepository.AddEditCustomerJournal(request.journalId, request.statusId);
            RepositryUnit.Commit();
            var lstRisks = RepositryUnit.risksRepository.GetRisksList(null, request.customerId);
            return lstRisks.Where(x => x.riskHistoryId == risk.RISKHISTORYID).FirstOrDefault();
        }
        #endregion

        #region Get Customer Vulnerability
        public List<VulnerabilitiesListingDataModel> GetVulnerabilitiesList(int? statusId, int? customerId)
        {
            return RepositryUnit.vulnerabilitiesRepository.GetVulnerabilitiesList(statusId, customerId);
        }
        #endregion

        #region Add/Edit Vulnerabilities details
        public VulnerabilitiesListingDataModel AddEditVulnerabilities(VulnerabilitiesListingDataModel request)
        {
            C_VULNERABILITY vulnerability = new C_VULNERABILITY();
            C_Customer_Vulnerability custVul = new C_Customer_Vulnerability();
            //inserting data in C_VULNERABILITY
            vulnerability = RepositryUnit.vulnerabilitiesRepository.AddEditVulnerabilities(request);
            RepositryUnit.Commit();
            request.vulnerabilityHistoryId = vulnerability.VULNERABILITYHISTORYID;
            //inserting data in C_Customer_Vulnerability
            string vulSubCat = request.customerVulnerabilitySubcategories;
            foreach (var data in vulSubCat.Split(','))
            {
                int subCat;
                if (int.TryParse(data, out subCat) == true)
                {
                    request.subCategoryId = subCat;
                    RepositryUnit.customerVulnerabilitiesRepository.AddEditCustomerVulnerabilities(request);
                    RepositryUnit.Commit();
                }
                                   
            }
            //inserting data in C_JOURNAL
            RepositryUnit.journalRepository.AddEditCustomerJournal(request.journalId, request.statusId);
            RepositryUnit.Commit();     
            var lstVulnerabilities =RepositryUnit.vulnerabilitiesRepository.GetVulnerabilitiesList(null, request.customerId);
            return lstVulnerabilities.Where(x => x.vulnerabilityHistoryId == vulnerability.VULNERABILITYHISTORYID).FirstOrDefault();          
        }
        #endregion
    }
}
