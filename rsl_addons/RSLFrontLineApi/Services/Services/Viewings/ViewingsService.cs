﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessModels;
using DataModel.DatabaseEntities;
using AutoMapper;
using Utilities;
using System.IO;
using System.Web;
using Utilities.Helpers;

namespace Services
{
    public class ViewingsService : BaseService
    {
        public List<HousingOfficerResponseModel> GetHousingOfficer(HousingOfficerFilter filter)
        {
            return Mapper.Map<List<FLS_GetAvailableOperatives_Result>, List<HousingOfficerResponseModel>>(RepositryUnit.housingOfficerRepository.GetHosusingOfficer(filter));
        }
        #region  Arrange new viewing
        public ArrangeViewingResponseModel ArrangeViewing(ArrangeViewingRequestModel request)
        {                   
            var model = Mapper.Map<ArrangeViewingRequestModel, FLS_Viewings>(request);
            if (request.viewingId > 0)
            {                            
                model.ViewingId = request.viewingId;
                model.LastModifiedOnServer = DateTime.Now;
                model.CreatedOnServer = request.createdOnServer;
                model.AssignedById = request.assignedById;                   
                RepositryUnit.viewingRepository.Update(model, request.viewingId);                
            }
            else
            {
                if(!RepositryUnit.viewingRepository.checkFromCurrentViewings(model))
                {
                    model.CreatedOnServer = DateTime.Now;
                    RepositryUnit.viewingRepository.Add(model);
                }              
                else
                {
                    throw new Exception(ApplicationConstants.viewingExist);
                }  
            }
            RepositryUnit.Commit();
            var result = RepositryUnit.viewingRepository.ArrangeViewing(model.ViewingId);
            //Below code for email and SMS
            //var viewing = RepositryUnit.viewingRepository.GetViewingInfo(result.viewingId);
            //GeneralHelper.SendEmail("Viewing Appointment",GetEmailBody(viewing),viewing.customerEmail, true);
            //GeneralHelper.SendSMS(GetSMSBody(viewing), viewing.customerMobile);
            return result;
          //  return RepositryUnit.viewingRepository.GetViewingDetails(model.ViewingId);           
        }
        #endregion

        #region "Add Comment To Viewing"
        public AddCommentToViewingResponseModel AddComment(ViewingCommentRequestModel request)
        {
            var model = Mapper.Map<ViewingCommentRequestModel, FLS_ViewingComments>(request);
            RepositryUnit.viewingCommentsRepository.Add(model);
            RepositryUnit.Commit();
            var result = RepositryUnit.viewingCommentsRepository.GetAllViewingCommentsForId((int)request.ViewingId);
            

            AddCommentToViewingResponseModel response = new AddCommentToViewingResponseModel();
            response.allComments = result;
            response.isSuccess = true;
            response.message = UserMessageConstants.ViewingFeedbackRecordingSuccess;
            return response;
        }
        #endregion

        #region Get details of a viewing
        public ViewingDetailsResponseModel GetViewingDetails(int viewingId)
        {
            return RepositryUnit.viewingRepository.GetViewingDetails(viewingId);
        }
        #endregion

        #region Get viewing list
        public ViewingListResponseModel GetViewingList(ViewingListRequestModel request)
        {
            ViewingListResponseModel response = new ViewingListResponseModel();
            response.viewingListDataModel = new List<ViewingListDataModel>();
            response.viewingListDataModel = RepositryUnit.viewingRepository.GetViewingList(request);
            if (request.isLookUpRequired)
            {
                response.lookups = loadViewingLookUps();
            }
            return response;
            
               
        }
        #endregion



        #region Fetch look ups
        public ViewingLookupsDataModel loadViewingLookUps()
        {
            ViewingLookupsDataModel viewingLookupsDataModel = new ViewingLookupsDataModel();
            //viewingStatus lookup
            var viewingStatusList = RepositryUnit.viewingStatusRepository.GetAll().ToList();
            viewingLookupsDataModel.viewingStatus = viewingStatusList.Select(x => new LookUpResponseModel { id = x.StatusId, description = x.Title }).ToList();
            return viewingLookupsDataModel;
        }
        #endregion


        #region  sms and email
        public string GetEmailBody(ViewingInfoDataModel response)
        {
            string body = string.Empty;
            var path = HttpContext.Current.Server.MapPath("~/Email/AppointmentEmail.html");
            using (StreamReader reader = new StreamReader(path))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{currentDate}", DateTime.Now.Date.ToString("dd/MM/yyyy"));
            body = body.Replace("{propertyAddress}", response.propertyAddress);
            body = body.Replace("{propertyTownCity}", response.propertyTownCity);
            body = body.Replace("{propertyPostCode}", response.propertyPostCode);
            body = body.Replace("{customerName}", response.customerName);
            body = body.Replace("{customerlastName}", response.customerlastName);
            body = body.Replace("{lastName}", response.customerlastName);
            body = body.Replace("{viewingAddress}", response.viewingAddress);
            body = body.Replace("{viewingDate}", ParseDateFormat(response.viewingDate));
            body = body.Replace("{viewingTiming}", ParseTimeFormat(response.viewingTime));
            body = body.Replace("{housingOfficerName}", response.housingOfficerName);
            body = body.Replace("{weekRent}", "£" + Convert.ToString(Math.Round(response.weekRent, 2)));
            body = body.Replace("{monthRent}", "£" + Convert.ToString(Math.Round(response.monthRent, 2)));
            return body;
        }
        public string  GetSMSBody(ViewingInfoDataModel response)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("We are pleased to confirm your appointment on ");
            builder.Append(ParseDateFormat(response.viewingDate));
            builder.Append(" at ");
            builder.Append(ParseTimeFormat(response.viewingTime));
            builder.Append(" for ");
            builder.Append(response.viewingAddress);
            builder.Append(".Should you wish to discuss this please contact XXXXX’ on 123455");
            return builder.ToString();
        }
        public  string ParseDateFormat(DateTime date)
        {
            return String.Format("{0:dddd, MMMM d, yyyy}", date);
        }
        public  string ParseTimeFormat(string time)
        {

            // Convert hour part of string to integer.
            String[] substrings = time.Split(':');
            string hour = substrings[0];
            int hourInt = int.Parse(hour);
            if (hourInt >= 24)
            {
                throw new ArgumentOutOfRangeException("Invalid hour");
            }
            //
            // Convert minute part of string to integer.
            //
            string minute = substrings[1];
            int minuteInt = int.Parse(minute);
            if (minuteInt >= 60)
            {
                throw new ArgumentOutOfRangeException("Invalid minute");
            }
            //
            // Return the DateTime.
            //
            return new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, hourInt, minuteInt, 0).ToString("hh:mm:ss tt");
        }

        #endregion
    }
}
