﻿using BusinessModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services
{
   public class CustomerRiskService : BaseService
    {
        #region Get Customer Risks
        public List<CustomerRiskResponseModel> GetCustomerRisk(int? statusId)
        {
            return RepositryUnit.customerRiskRepository.GetCustomerRisk(statusId);
        }
        #endregion
    }
}
