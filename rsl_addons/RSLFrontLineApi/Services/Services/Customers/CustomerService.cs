﻿using BusinessModels;
using DataModel.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Utilities;
using BusinessModels.BusinessModels.Customers.RequestModels;
using BusinessModels.BusinessModels.Customers.ResponseModels;

namespace Services
{
    public class CustomerService : BaseService
    {
        #region Search Customer
        public CustomerSearchResponseModel SearchCustomer(CustomerSearchRequestModel CustomerSearchRequestModel)
        {
            CustomerSearchResponseModel response = new CustomerSearchResponseModel();
            PaginationDataModel paginationObj = CustomerSearchRequestModel.pagination;
            response.customerList = this.RepositryUnit.customerRepository.SearchCustomer(CustomerSearchRequestModel.searchText, ref paginationObj);
            //response.pagination = paginationObj;
            response.commonData = new CustomerCommonDataModel();
            if (CustomerSearchRequestModel.isLookUpRequired)
                response.commonData.lookups = loadCustomerLookUps();
            response.commonData.profileThumb = FileHelper.getLogicalCustomerProfileThumbPath();
            response.commonData.cusProfile = FileHelper.getLogicalCustomerProfileImagePath();
            return response;
        }
        #endregion

        #region Load Customer lookups 

        public CustomerLookUpsDataModel loadCustomerLookUps()
        {

            CustomerLookUpsDataModel customerLookUpsModel = new CustomerLookUpsDataModel();
            #region start add/edit customer lookups            

            var communicationList = this.RepositryUnit.communicationRepository.GetAll().ToList();
            customerLookUpsModel.communication = communicationList.Select(x => new LookUpResponseModel { id = x.COMMUNICATIONID, description = x.DESCRIPTION }).ToList();

            var internetAccessList = this.RepositryUnit.internetAccessRepository.GetAll().ToList();
            customerLookUpsModel.internetAccess = internetAccessList.Select(x => new LookUpResponseModel { id = x.INTERNETACCESSID, description = x.DESCRIPTION }).ToList();

            var preferedContactList = this.RepositryUnit.preferedContactRepository.GetAll().ToList();
            customerLookUpsModel.preferedContact = preferedContactList.Select(x => new LookUpResponseModel { id = x.PREFEREDCONTACTID, description = x.DESCRIPTION }).ToList();

            var sexualOrientationList = this.RepositryUnit.sexualOrientationRepository.GetAll().ToList();
            customerLookUpsModel.sexualOrientation = sexualOrientationList.Select(x => new LookUpResponseModel { id = x.SEXUALORIENTATIONID, description = x.DESCRIPTION }).ToList();

            var ethnicityList = this.RepositryUnit.ethnicityRepository.GetAll().ToList();
            customerLookUpsModel.ethnicity = ethnicityList.Select(x => new LookUpResponseModel { id = x.ETHID, description = x.DESCRIPTION }).ToList();

            var languagesList = this.RepositryUnit.languagesRepository.GetAll().ToList();
            customerLookUpsModel.language = languagesList.Select(x => new LookUpResponseModel { id = x.LANGUAGEID, description = x.LANGUAGE }).ToList();

            var maritalStatusList = this.RepositryUnit.maritalStatusRepository.GetAll().ToList();
            customerLookUpsModel.maritalStatus = maritalStatusList.Select(x => new LookUpResponseModel { id = x.MARITALSTATUSID, description = x.DESCRIPTION }).ToList();

            var religionList = this.RepositryUnit.religionRepository.GetAll().ToList();
            customerLookUpsModel.religion = religionList.Select(x => new LookUpResponseModel { id = x.RELIGIONID, description = x.DESCRIPTION }).ToList();

            var nationalityList = this.RepositryUnit.nationalityRepository.GetAll().ToList();
            customerLookUpsModel.nationality = nationalityList.Select(x => new LookUpResponseModel { id = x.NATID, description = x.DESCRIPTION }).ToList();

            var landlordList = this.RepositryUnit.landLordRepoeitory.GetAll().ToList();
            customerLookUpsModel.landlord = landlordList.Select(x => new LookUpResponseModel { id = x.LandLordTypeId, description = x.Title }).ToList();

            var HouseTypeList = this.RepositryUnit.houseTypeRepository.GetAll().ToList();
            customerLookUpsModel.houseType = HouseTypeList.Select(x => new LookUpResponseModel { id = x.HouseTypeId, description = x.Title }).ToList();

            var FloorTypeList = this.RepositryUnit.floorTypeRepository.GetAll().ToList();
            customerLookUpsModel.floorType = FloorTypeList.Select(x => new LookUpResponseModel { id = x.FloorTypeId, description = x.Description }).ToList();

            #endregion

            return customerLookUpsModel;

        }

        #endregion

        #region upload ProfilePic
        public bool UploadProfilePic(int customerId, string fileName)
        {
            return this.RepositryUnit.customerRepository.UploadProfilePic(customerId, fileName);
        }
        #endregion

        #region Add Edit Customer
        public CustomerDetailDataModel AddEditCustomer(CustomerDetailDataModel customerModel)
        {
            //Add update Customer basic info
            C__CUSTOMER cust = new C__CUSTOMER();
            C_ADDRESS adrs = new C_ADDRESS();
            cust = RepositryUnit.customerRepository.AddEditCustomer(customerModel);
            RepositryUnit.Commit();
            customerModel.customerId = cust.CUSTOMERID;
            if (customerModel.customerAddress != null)
            {
                //Add update Customer address info
                foreach (var customerAddress in customerModel.customerAddress)
                {
                    adrs = RepositryUnit.customerAddressRepository.AddEditAddress(customerAddress, customerModel.customerId);
                    adrs.AppVersion = customerModel.appVersion;
                    adrs.CreatedOnApp = customerModel.createdOnApp;
                    adrs.CreatedOnServer = customerModel.createdOnServer;
                    adrs.LastModifiedOnApp = customerModel.lastModifiedOnApp;
                    adrs.LastModifiedOnServer = customerModel.lastModifiedOnServer;
                    RepositryUnit.Commit();
                }
            }
            if (customerModel.currentHome != null)
            {
                //Add update Customer current home info        
                RepositryUnit.customerCurrentHomeRepository.AddEditCurrentHome(customerModel);
            }
            int result = RepositryUnit.Commit();
            customerModel.customerId = cust.CUSTOMERID;
            return RepositryUnit.customerRepository.GetCustomerDetail(customerModel.customerId);


        }
        #endregion

        #region Get customer detail by Customer Id
        public CustomerDetailDataModel GetCustomerDetail(int customerId)
        {
            var customer = RepositryUnit.customerRepository.GetCustomerDetail(customerId);
            return customer;
        }
        #endregion

        #region Get customer viewings
        public List<CustomerViewingResponseModel> GetCustomerViewing(CustomerViewingRequestModel request)
        {
            return RepositryUnit.customerRepository.GetCustomerViewing(request);
        }
        #endregion

        #region Get a customer's referral list
        //public List<CustomerReferralResponseModel> GetReferralList(int customerId)
        //{
        //    return RepositryUnit.customerRepository.GetReferralList(customerId);
        //}
        #endregion

        #region Get a referral details
        public CustomerReferralsDataModel GetReferralDetails(int? customerId, int referralHistoryId = 0)
        {
            return RepositryUnit.customerRepository.GetReferralDetails(customerId);
        }
        #endregion

        #region Add/Edit referral
        public CustomerReferralsDataModel AddEditReferral(CustomerReferralsDataModel request)
        {
            C_JOURNAL jrnl = new C_JOURNAL();
            C_REFERRAL refrl = new C_REFERRAL();
            jrnl= RepositryUnit.journalRepository.AddEditReferralData(request);        
            request.journalId = jrnl.JOURNALID;
            refrl =  RepositryUnit.referralsRepository.AddEditReferral(request);

            List<byte> lstHelpSubCategoryId = new List<byte>();
            if (request.customerHelpSubCategories != null)
            {
                foreach (var itm in request.customerHelpSubCategories.Split(','))
                {
                    if (itm != string.Empty && itm != null)
                    {
                        lstHelpSubCategoryId.Add(byte.Parse(itm));
                    }

                }
            }       

            
            RepositryUnit.customerHelpSubCategoryRepository.AddEditHelpSubCategory(lstHelpSubCategoryId, request.journalId);
            RepositryUnit.Commit();
            //request.journalId = jrnl.JOURNALID;
            //request.referralHistoryId = refrl.REFERRALHISTORYID;
            return GetReferralDetails(request.customerId);

        }
        #endregion

        #region Get tenancy info against customerId
        public TenancyInfoResponseModel GetTenancyInfo(int customerId)
        {
            return RepositryUnit.customerRepository.GetTenancyInfo(customerId);
        }
        #endregion

        #region Terminate customer tenancy
        public TenancyTerminationDataModel TerminateTenancy(TenancyTerminationDataModel request)
        {
            if (request.forwardAddress != null)
            {
                C_ADDRESS address = new C_ADDRESS();
                address = RepositryUnit.customerAddressRepository.AddForwardingddress(request.forwardAddress, (int)request.customerId);
                RepositryUnit.Commit();
            }
            bool isTenancyExist = false;
            isTenancyExist = RepositryUnit.customerTenancyRepository.IsTerminationExist(request.customerId);
            if (isTenancyExist == false)
            {
                C_JOURNAL journal = new C_JOURNAL();                
                journal = RepositryUnit.journalRepository.AddTerminationData(request);
                RepositryUnit.Commit();
                request.journalId = journal.JOURNALID;
                RepositryUnit.terminationRepository.AddTerminationData(request);
                RepositryUnit.Commit(); //need commit here because function below is calling SP's.and these SP's need latest journalId,which is not possible without commit
                return RepositryUnit.customerRepository.TerminateTenancy(request, journal);
            }
            else
            {
                throw new ArgumentException(UserMessageConstants.TerminationExist);
            }
        }
        #endregion

        #region start tenancy
        public TenancySetupDataModel CreateTenancy(TenancySetupDataModel request)
        {
            TenancySetupDataModel response = new TenancySetupDataModel();
            C_TENANCY tenancy = new C_TENANCY();
            FLS_TenantDisclosure disclosure = new FLS_TenantDisclosure();
            TO_LOGIN login = new TO_LOGIN();
            CustomerReferralsDataModel referral = new CustomerReferralsDataModel();
            bool isTenancyExist = false;
            isTenancyExist = RepositryUnit.customerTenancyRepository.IsTenancyExist(request.customerId);
            if (isTenancyExist == false)
            {
                //updating tenant info
                if (request.customerGeneralInfo != null)
                {
                    RepositryUnit.customerRepository.UpdateCustomerInfoInTenancyCreation(request.customerGeneralInfo, request.employmentInfo, request.customerId);
                }
                else
                {
                    RepositryUnit.customerRepository.UpdateCustomerTenancyType(request.customerId);
                }

                if (request.emergencyContact != null)
                {
                    RepositryUnit.customerAddressRepository.AddEditEmergencyContactInfo(request.emergencyContact, request.customerId);
                }

                //updating joint tenant info 
                if (request.jointTenant != null)
                {
                    RepositryUnit.customerRepository.UpdateCustomerInfoInTenancyCreation(request.jointTenant.customerGeneralInfo, request.jointTenant.employmentInfo, request.jointTenant.customerId);
                    if (request.jointTenant.emergencyContact != null)
                    {
                        RepositryUnit.customerAddressRepository.AddEditEmergencyContactInfo(request.jointTenant.emergencyContact, request.jointTenant.customerId);
                    }
                }

                //Change LandLord Name to "BHG"
                RepositryUnit.customerCurrentHomeRepository.EditCurrentHome(request);

                //creating tenancy
                tenancy = RepositryUnit.tenancyRepository.SaveTenancyInfo(request);
                RepositryUnit.propertiesRepository.UpdatePropertyStatus(request.propertyId);
                RepositryUnit.Commit();
                if (request.propertyRentInfo != null)
                {
                    RepositryUnit.propertyRentInfoRepository.SaveRentInfo(request.propertyRentInfo, request.propertyId);
                }



                if (request.tenancyDisclosure != null)
                {
                    request.tenancyDisclosure.tenancyId = tenancy.TENANCYID;
                    request.tenancyDisclosure.customerId = request.customerId;
                    disclosure = RepositryUnit.tenantDisclosureInfoRepository.SaveDisclosureInfo(request.tenancyDisclosure);
                }

                if (request.tenantOnlineInfo != null)
                {
                    request.tenantOnlineInfo.customerId = request.customerId;
                    login = RepositryUnit.tenantOnlineInfoRepository.SaveTenantOnlineInfo(request.tenantOnlineInfo);
                }

                if (request.jointTenant != null)
                {
                    request.jointTenant.tenancyId = tenancy.TENANCYID;
                }


                request.tenancyId = tenancy.TENANCYID;
                //inserting data in rent journal
                RepositryUnit.rentJournalRepository.SaveRentJournalInfo(request);

                //inserting data of tenant and joint tenant in C_CUSTOMERTENANCY table
                RepositryUnit.customerTenancyRepository.SaveCustomerTenancyRelationInfo(request);

                //inserting occupant data     
                if (request.occupants != null)
                {
                    foreach (var item in request.occupants)
                    {
                        //assigning tenancyId and newly created customerId to each occupant 
                        C__CUSTOMER customer = new C__CUSTOMER();
                        customer = RepositryUnit.customerRepository.AddCustomerInfoForOccupants(item);
                        item.tenancyId = tenancy.TENANCYID;
                        RepositryUnit.Commit();
                        item.customerId = customer.CUSTOMERID;
                        RepositryUnit.occupantsRepository.SaveOccupantsList(item);

                    }
                }

                //inserting referral data
                if (request.referral != null)
                {
                    referral = AddEditReferral(request.referral);
                }

                RepositryUnit.Commit();
                response = RepositryUnit.customerRepository.GetTenancyInfo(request);
                return response;
            }
            else
            {
                throw new ArgumentException(UserMessageConstants.TenancyAlreadyExist);
            }



        }
        #endregion

        #region Get customer Risks
        public List<RisksListingDataModel> GetCustomerRisksList(int customerId)
        {
            return RepositryUnit.risksRepository.GetRisksList(null, customerId);
        }
        #endregion

        #region Get customer Vulnerabilities
        public List<VulnerabilitiesListingDataModel> GetCustomerVulnerabilitiesList(int customerId)
        {
            return RepositryUnit.vulnerabilitiesRepository.GetVulnerabilitiesList(null, customerId);
        }
        #endregion

        #region Intelligent Schedulling
        public GetOperativesForVoidInspectionResponseModel GetAvailablePublishers(OperativesForVoidInspectionRequestModel model)
        {
            var response = Mapper.Map<List<FLS_GetAvailableOperativesForVoidInspection_Result>, List<Supervisor>>(RepositryUnit.schedulerRepository.GetAvailablePublishers(model));
            return new GetOperativesForVoidInspectionResponseModel
            {
                Supervisors = response
            };
        }
        #endregion

        #region Save Void Inspection
        public VoidInspectionResponseModel SaveVoidInspection(VoidInspectionRequestModel model)
        {
            return RepositryUnit.schedulerRepository.SaveVoidInspection(model);
        }
        #endregion

    }
}