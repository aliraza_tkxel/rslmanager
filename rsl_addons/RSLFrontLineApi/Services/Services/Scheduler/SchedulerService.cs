﻿using System;
using System.Data;
using System.Data.Entity;
using BusinessModels;

namespace Services.Services.Scheduler
{
    public class SchedulerService : BaseService
    {
        #region Fetch Publishers

        public void GetPublisherList(ref DataSet resultDataSet, string tradeIds, string msattype, DateTime startDate)
        {
            var list = DbContext.PDR_GetAvailableOperatives(tradeIds, msattype, startDate);
        }

        #endregion
    }
}
