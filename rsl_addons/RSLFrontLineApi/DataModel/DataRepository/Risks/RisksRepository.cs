﻿using BusinessModels;
using DataModel.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities;

namespace DataModel.DataRepository
{
    public class RisksRepository : BaseRepository<C_RISK>
    {
        public RisksRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {
        }

        #region Get Customer Risks
        public List<RisksListingDataModel> GetRisksList(int? statusId,int?customerId)
        {
            if(statusId <= 0)
            {
                statusId = null;
            }
            var riskData = (from r in DbContext.C_RISK
                            join cr in DbContext.C_Customer_Risk on r.RISKHISTORYID equals cr.RiskHistoryId
                            join c in DbContext.C__CUSTOMER on cr.CustomerId equals c.CUSTOMERID
                            join j in DbContext.C_JOURNAL on cr.JournalId equals j.JOURNALID

                            join t in DbContext.C_CUSTOMERTENANCY.Where(x => x.ENDDATE == null) on c.CUSTOMERID equals t.CUSTOMERID
                            into group1
                            from ten in group1.DefaultIfEmpty()

                            join ct in DbContext.C_TENANCY.Where(x => x.ENDDATE == null) on ten.TENANCYID equals ct.TENANCYID
                            into group2
                            from cTen in group2.DefaultIfEmpty()
                            orderby r.RISKHISTORYID
                            where 1 == 1                          
                            &&
                            (
                                c.CUSTOMERID == customerId
                                || customerId == null
                            )
                            &&
                            (
                                j.CURRENTITEMSTATUSID == statusId
                                || statusId == null
                            )
                            select new RisksListingDataModel
                            {
                                customerId = c.CUSTOMERID,
                                customerName = string.Concat(c.FIRSTNAME, " ", c.LASTNAME),
                                reviewDate = r.REVIEWDATE,
                                riskHistoryId = r.RISKHISTORYID,
                                startDate = r.STARTDATE,
                                journalId = j.JOURNALID,
                                actionId = r.ITEMACTIONID,
                                assignedTo = r.ASSIGNTO,
                                isCustomerAware = r.CUSTOMERAWARE,
                                isTenantOnline = r.TENANTSONLINE,
                                lastActionDate = r.LASTACTIONDATE,
                                lastActionUserId = r.LASTACTIONUSER,
                                notes = r.NOTES,
                                statusId = r.ITEMSTATUSID,
                                teamId = r.TEAM,
                                tenancyId = ten.TENANCYID,
                                title = j.TITLE
                            }
                            ).Distinct().OrderByDescending(x => x.riskHistoryId).ToList();
            //Get Risk Subcategories
            foreach (var data in riskData)
            {
                data.customerRiskSubcategories = GetRisksSubcategories(data.journalId, data.riskHistoryId);
            }                                  
            return riskData;
        }

        #region Get Risk Subcategories
        public string GetRisksSubcategories(int journalId ,int riskHistoryId)
        {
            string subCat ="";           
            var data = (from risk in DbContext.C_Customer_Risk
                        join sub in DbContext.C_RISK_SUBCATEGORY on risk.SubCategoryId equals sub.SubCategoryId
                        join cat in DbContext.C_RISK_CATEGORY on sub.CategoryId equals cat.CategoryId
                        where risk.JournalId == journalId
                        && risk.RiskHistoryId == riskHistoryId
                        select sub.SubCategoryId
                        );
            foreach (var sub in data.ToList())
            {
                subCat = subCat +sub + ",";
            }
            return subCat;
        }
        #endregion


        #endregion

        #region Add/Edit Risks details
        public C_RISK AddEditRisks(RisksListingDataModel request)
        {
            C_RISK risk = new C_RISK();
            int statusId = int.Parse(DbContext.C_STATUS.Where(x => string.Compare(x.DESCRIPTION, ApplicationConstants.ItemStatusClosed, true) == 0).Select(x => x.ITEMSTATUSID).FirstOrDefault().ToString());
            if (request.riskHistoryId > 0)
            {
                var query = DbContext.C_RISK.Where(x => x.RISKHISTORYID == request.riskHistoryId);
                if (query.Count() > 0)
                {
                    risk = query.First();
                }
            }
            if (request.statusId == statusId)
            {
                risk.DTIMESTAMP = DateTime.Now;
            }
            risk.ASSIGNTO = request.assignedTo;
            risk.CUSTOMERAWARE = request.isCustomerAware;
            risk.ITEMACTIONID = request.actionId;
            risk.ITEMSTATUSID = request.statusId;
            risk.JOURNALID = request.journalId;
            risk.LASTACTIONDATE = request.lastActionDate;
            risk.LASTACTIONUSER = request.lastActionUserId;
            risk.NOTES = request.notes;
            risk.REVIEWDATE = request.reviewDate;
            risk.STARTDATE = request.startDate;
            risk.TEAM = request.teamId;
            risk.TENANTSONLINE = request.isTenantOnline;
            if (request.riskHistoryId > 0)
            {
                base.Update(risk, risk.RISKHISTORYID);
            }
            else
            {
                base.Add(risk);
            }
            return risk;
        }



        #endregion
    }
}
