﻿using DataModel.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DataRepository
{
    public class RiskSubCategoryRepository : BaseRepository<C_RISK_SUBCATEGORY>
    {
        public RiskSubCategoryRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {
        }
    }
}
