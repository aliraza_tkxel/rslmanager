﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModel.DatabaseEntities;
using Utilities;
using BusinessModels;

namespace DataModel.DataRepository
{
    public class TeamsRepository : BaseRepository<E_TEAM>
    {
        public TeamsRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {
        }
        #region Get Teams
        public List<E_TEAM> GetTeams()
        {
            var teams = (from t in DbContext.E_TEAM
                         join tc in DbContext.G_TEAMCODES on t.TEAMID equals tc.TEAMID
                         where t.ACTIVE == 1
                         && t.TEAMID != 1
                         && (
                            string.Compare(t.TEAMNAME, ApplicationConstants.TeamHousingTeam, true) == 0
                         || string.Compare(t.TEAMNAME, ApplicationConstants.TeamHousingServices, true) == 0
                         || string.Compare(t.TEAMNAME,ApplicationConstants.TrainingAndEmployment,true) ==0
                         )
                         select t
                         ).ToList();                         
            return teams;
            
        }
        #endregion
    }
}
