﻿using DataModel.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessModels;
namespace DataModel.DataRepository
{
    public class EmployeesRepository : BaseRepository<E__EMPLOYEE>
    {
        public EmployeesRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {
        }
        #region Get Employees
        public List<DependentLookupResponseModel> GetEmployees()
        {
            var employees = (from emp in DbContext.E__EMPLOYEE
                             join login in DbContext.AC_LOGINS on emp.EMPLOYEEID equals login.EMPLOYEEID
                             join j in DbContext.E_JOBDETAILS on emp.EMPLOYEEID equals j.EMPLOYEEID
                             join t in DbContext.E_TEAM on j.TEAM equals t.TEAMID
                             where login.ACTIVE ==1
                             && j.ACTIVE ==1
                             select new DependentLookupResponseModel
                             {
                                 id = emp.EMPLOYEEID,
                                 description = string.Concat(emp.FIRSTNAME," ",emp.LASTNAME),
                                 subId = t.TEAMID
                             }
                             ).ToList();
            return employees;

        }
        #endregion
    }
}
