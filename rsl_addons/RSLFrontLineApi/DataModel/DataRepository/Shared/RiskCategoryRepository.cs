﻿using DataModel.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DataRepository
{
    public class RiskCategoryRepository : BaseRepository<C_RISK_CATEGORY>
    {
        public RiskCategoryRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {
        }
    }
}
