﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModel.DatabaseEntities;
namespace DataModel.DataRepository
{
    public class LetterActionRepository : BaseRepository<C_LETTERACTION>
    {
        public LetterActionRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {
        }
    }
}
