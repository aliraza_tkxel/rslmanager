﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataModel.DatabaseEntities;

namespace DataModel.DataRepository.Helpers
{
    public static class OperativesHelper
    {
        public static List<FLS_GetAvailableOperativesAppointedForVoidInspection_Result> IsAppointedExist(List<FLS_GetAvailableOperativesAppointedForVoidInspection_Result> appointedDt, int operativeId, System.DateTime runningDate, System.DateTime aptStartTime, System.DateTime aptEndTime, System.DateTime aptStartDate = default(DateTime))
        {
            if (aptStartDate == default(DateTime))
            {
                aptStartTime = Convert.ToDateTime(runningDate.ToLongDateString() + " " + aptStartTime.ToShortTimeString());
            }
            else
            {
                aptStartTime = Convert.ToDateTime(aptStartDate.ToLongDateString() + " " + aptStartTime.ToShortTimeString());
            }

            aptEndTime = Convert.ToDateTime(runningDate.ToLongDateString() + " " + aptEndTime.ToShortTimeString());

            double startTimeInSec = 0;
            double endTimeInSec = 0;

            startTimeInSec = ConvertDateInSec(aptStartTime);
            endTimeInSec = ConvertDateInSec(aptEndTime);

            var appointments = appointedDt.AsEnumerable();
            var appResult = (from app in appointments
                             where app.OperativeId == operativeId &&
                             ((startTimeInSec == Convert.ToDouble(app.StartTimeInSec) & endTimeInSec < Convert.ToDouble(app.EndTimeInSec) |
                             (startTimeInSec < Convert.ToDouble(app.EndTimeInSec) & endTimeInSec == Convert.ToDouble(app.EndTimeInSec)) |
                             (startTimeInSec > Convert.ToDouble(app.StartTimeInSec) & endTimeInSec < Convert.ToDouble(app.EndTimeInSec)) |
                             (startTimeInSec < Convert.ToDouble(app.StartTimeInSec) & endTimeInSec > Convert.ToDouble(app.EndTimeInSec)) |
                             (startTimeInSec < Convert.ToDouble(app.StartTimeInSec) & endTimeInSec > Convert.ToDouble(app.StartTimeInSec)) |
                             (startTimeInSec < Convert.ToDouble(app.EndTimeInSec) & endTimeInSec > Convert.ToDouble(app.EndTimeInSec))))
                             orderby app.EndTimeInSec ascending
                             select app);

            return appResult.ToList();
        }

        public static bool IsLeavesExist(List<FLS_GetAvailableOperativesLeavesForVoidInspection_Result> leavesDt, int operativeId, System.DateTime appointmentStartDateTime, System.DateTime appoitmentEndDateTime)
        {
            //'çonvert time into seconds for comparison
            int lStartTimeInMin = 0;
            int lEndTimeInMin = 0;

            lStartTimeInMin = Convert.ToInt32(ConvertDateInMin(appointmentStartDateTime));
            lEndTimeInMin = Convert.ToInt32(ConvertDateInMin(appoitmentEndDateTime));

            var leaves = leavesDt.AsEnumerable();
            var leaveResult = (from app in leavesDt.AsEnumerable()
                               where app.OperativeId == operativeId
                               && ((lStartTimeInMin <= Convert.ToInt32(app.StartTimeInMin) & lEndTimeInMin > Convert.ToInt32(app.StartTimeInMin) & lEndTimeInMin <= Convert.ToInt32(app.EndTimeInMin))
                               | (lStartTimeInMin >= Convert.ToInt32(app.StartTimeInMin) & lEndTimeInMin <= Convert.ToInt32(app.EndTimeInMin))
                               | (lStartTimeInMin >= Convert.ToInt32(app.StartTimeInMin) & lStartTimeInMin < Convert.ToInt32(app.EndTimeInMin) & lEndTimeInMin >= Convert.ToInt32(app.EndTimeInMin))
                               | (lStartTimeInMin < Convert.ToInt32(app.StartTimeInMin) & lEndTimeInMin > Convert.ToInt32(app.EndTimeInMin)))
                               orderby app.EndTimeInMin ascending
                               select app);

            return leaveResult.ToList().Count > 0 ? true : false;
        }

        //Converts the given Date to minutes
        private static double ConvertDateInMin(DateTime timeToConvert)
        {
            DateTime calendarStartDate = new DateTime(1970, 1, 1);
            TimeSpan ts = timeToConvert - calendarStartDate;
            return ts.TotalMinutes;
        }
        
        //Converts the given Date to seconds
        public static double ConvertDateInSec(DateTime timeToConvert)
        {
            DateTime calendarStartDate = new DateTime(1970, 1, 1);
            TimeSpan ts = timeToConvert - calendarStartDate;
            return ts.TotalSeconds;
        }
    }
}
