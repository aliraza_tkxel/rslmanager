﻿using DataModel.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DataRepository
{
    public class SchemesRepository : BaseRepository<P_SCHEME>
    {
        public SchemesRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {
        }
    }
}
