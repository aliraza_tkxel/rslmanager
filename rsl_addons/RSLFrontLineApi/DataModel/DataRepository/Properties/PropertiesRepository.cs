﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModel.DatabaseEntities;
using BusinessModels;
using Utilities;
using System.Web;

namespace DataModel.DataRepository
{
   public class PropertiesRepository:BaseRepository<P__PROPERTY>
    {
        public PropertiesRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {
        }

        

        #region Search properties
        public PropertiesSearchResponseModel SearchProperties()
        {
            //start fetching general property info
            var data = (from prop in DbContext.P__PROPERTY
                        join fin in DbContext.P_FINANCIAL on prop.PROPERTYID equals fin.PROPERTYID
                        into group4
                        from pfin in group4.DefaultIfEmpty()

                        join ten in DbContext.C_TENANCY on prop.PROPERTYID equals ten.PROPERTYID
                        into group0
                        let tenancy = group0.OrderBy(x=>x.ENDDATE).FirstOrDefault()


                        join status in DbContext.P_STATUS on prop.STATUS equals status.STATUSID

                        join subSts in DbContext.P_SUBSTATUS on prop.SUBSTATUS equals subSts.SUBSTATUSID
                        into group1
                        from subStatus in group1.DefaultIfEmpty()

                        join img in DbContext.PA_PROPERTY_ITEM_IMAGES on prop.PropertyPicId equals img.SID
                        into group2
                        from image in group2.DefaultIfEmpty()

                        join schm in DbContext.P_SCHEME on prop.SCHEMEID equals schm.SCHEMEID
                        into group3
                        from scheme in group3.DefaultIfEmpty()

                        join type in DbContext.P_PROPERTYTYPE on prop.PROPERTYTYPE equals type.PROPERTYTYPEID

                        where

                        ((string.Compare(status.DESCRIPTION, ApplicationConstants.AvailableToRentPropertyStatus, true) == 0 
                            && (string.Compare(subStatus.DESCRIPTION, ApplicationConstants.propertySubStatusNotAvailable, true) == 0
                                || string.Compare(subStatus.DESCRIPTION, ApplicationConstants.propertySubStatusPendingTenancy, true) == 0
                                || prop.SUBSTATUS == null))

                            || (string.Compare(status.DESCRIPTION, ApplicationConstants.propertyStatusLet, true) == 0 
                                && string.Compare(subStatus.DESCRIPTION, ApplicationConstants.propertySubStatusPendingTermination , true) == 0))


                        && string.Compare(type.DESCRIPTION, ApplicationConstants.GarageType,true) != 0 && string.Compare(type.DESCRIPTION, ApplicationConstants.CarSpaceType, true) != 0 && string.Compare(type.DESCRIPTION, ApplicationConstants.CarPortType, true) != 0
                        orderby prop.PROPERTYID descending
                        select new PropertiesDataModel
                        {
                            address = string.Concat(prop.HOUSENUMBER, " ", prop.ADDRESS1," ", prop.ADDRESS2, " ",prop.ADDRESS3," ",prop.TOWNCITY," ",prop.POSTCODE),                         
                            imageName = image.ImageName,
                            rent = pfin.TOTALRENT == null ? 0 : pfin.TOTALRENT,
                            propertyId = prop.PROPERTYID,
                            status = status.DESCRIPTION,
                            subStatus = subStatus.DESCRIPTION == null ? "N/A" : subStatus.DESCRIPTION,
                            postalCode = prop.POSTCODE,
                            schemeId = scheme.SCHEMEID,
                            propertyTypeId = type.PROPERTYTYPEID,
                            tenancyEndDate = tenancy.ENDDATE
                        }
                        ).Distinct();
            //end fetching general property info
            
            //start fetching beds info
            var propertyData = data.ToList();
            foreach (var propId in propertyData)
            {
                propId.bedRooms = GetBeddingInfo(propId.propertyId);
                if(propId.imageName != null)
                {
                    propId.imagePath = getImagePath(propId.imageName,propId.propertyId);
                }
                    
            }
            //end fetching beds info
                 
            //start creating response
            PropertiesSearchResponseModel objPropertiesSearchResponse = new PropertiesSearchResponseModel();
            objPropertiesSearchResponse.propertiesData = new List<PropertiesDataModel>();
            objPropertiesSearchResponse.propertiesData = propertyData.ToList();        
            //end creating response

            return objPropertiesSearchResponse;

        }
        #endregion

        #region Get bedding info
        public string GetBeddingInfo(string propertyId)
        {
            string bedRooms;             
            var attributeData = (from acdm in DbContext.P_GetAccommodationSummary (propertyId)
                                 select acdm ).ToList();
            bedRooms = attributeData.Select(x => x.BEDROOMS).FirstOrDefault();
            return bedRooms == null ? "?" : bedRooms;
        }
        #endregion

        #region Get Properties info
        public PropertyInfoResponseModel GetPropertiesInfo(string propertyId)
        {
            //start filling basic info 
            var propertyBasicInfo = (from prop in DbContext.P__PROPERTY

                                join schm in DbContext.P_SCHEME on prop.SCHEMEID equals schm.SCHEMEID
                                into group1
                                from scheme in group1.DefaultIfEmpty()

                                join propType in DbContext.P_PROPERTYTYPE on prop.PROPERTYTYPE equals propType.PROPERTYTYPEID

                                join dev in DbContext.PDR_DEVELOPMENT on prop.DEVELOPMENTID equals dev.DEVELOPMENTID
                                into group2
                                from development in group2.DefaultIfEmpty()

                                join devType in DbContext.P_DEVELOPMENTTYPE on development.DEVELOPMENTTYPE equals devType.DEVELOPMENTTYPEID
                                into group6
                                from developmenttype in group6.DefaultIfEmpty()

                                join ph in DbContext.P_PHASE on prop.PhaseId equals ph.PHASEID
                                into group3
                                from phase in group3.DefaultIfEmpty()

                                join blk in DbContext.P_BLOCK on prop.BLOCKID equals blk.BLOCKID
                                into group4 
                                from block in group4.DefaultIfEmpty()

                                join img in DbContext.PA_PROPERTY_ITEM_IMAGES on prop.PropertyPicId equals img.SID
                                into group5
                                from image in group5.DefaultIfEmpty()

                                join fin in DbContext.P_FINANCIAL on prop.PROPERTYID equals fin.PROPERTYID
                                into group7
                                from pfin in group7.DefaultIfEmpty()
                                join status in DbContext.P_STATUS on prop.STATUS equals status.STATUSID
                                join asset in DbContext.P_ASSETTYPE on prop.ASSETTYPE equals asset.ASSETTYPEID
                                where prop.PROPERTYID == propertyId
                                select new PropertyInfoResponseModel
                                {
                                    address =string.Concat(prop.ADDRESS1," ",prop.ADDRESS2," ",prop.ADDRESS3),
                                    assetType = asset.DESCRIPTION,
                                    block = block.BLOCKNAME,
                                    county = prop.COUNTY == null ? "N/A" : prop.COUNTY,
                                    developmentType = developmenttype.DESCRIPTION == null ? "N/A" : developmenttype.DESCRIPTION,
                                    houseNo = prop.HOUSENUMBER,
                                    phase =  phase.PhaseName == null ? "N/A": phase.PhaseName,
                                    postalCode = prop.POSTCODE,
                                    rent = pfin.TOTALRENT == null ? 0 : pfin.TOTALRENT,
                                    propertyId = prop.PROPERTYID,
                                    propertyTypeId = propType.PROPERTYTYPEID,
                                    rightToAcquire = prop.RIGHTTOBUY ==1 ? "Yes" : "No",
                                    schemeId = scheme.SCHEMEID,
                                    status = status.DESCRIPTION,
                                    townCity = prop.TOWNCITY,
                                    imageName = image.ImageName                                                 
                                }                                                              
                                );
         var propertyInfo = propertyBasicInfo.FirstOrDefault();
            //end filling basic info 


            //start filling accommodation info 
            var accomodationInfo = (from parm in DbContext.PA_PARAMETER
                                    join itmParm in DbContext.PA_ITEM_PARAMETER on parm.ParameterID equals itmParm.ParameterId
                                    join itm in DbContext.PA_ITEM on itmParm.ItemId equals itm.ItemID

                                    join atr in DbContext.PA_PROPERTY_ATTRIBUTES on itmParm.ItemParamID equals atr.ITEMPARAMID
                                    into group1
                                    from propAtr in group1.DefaultIfEmpty()

                                    where propAtr.PROPERTYID == propertyId
                                    && parm.ShowInAccomodation == true
                                    && parm.IsActive ==true
                                    &&propAtr.PARAMETERVALUE != "0"
                                    orderby parm.ParameterSorder ascending                                   
                                    select new
                                    {
                                        parm.ParameterName,
                                        propAtr.PARAMETERVALUE
                                    }                                  
                                    ).ToList();
                
                foreach (var data in accomodationInfo)
                {
                    switch(data.ParameterName)
                    {
                    case ApplicationConstants.Bathroom:
                        propertyInfo.numberOfBathRooms =  data.PARAMETERVALUE;
                        break;
                    case ApplicationConstants.BedroomsQuantity:
                        propertyInfo.numberOfBedRooms = data.PARAMETERVALUE;
                        break;
                    case ApplicationConstants.HeatingType:
                        propertyInfo.boilerType = data.PARAMETERVALUE;
                        break;
                    case ApplicationConstants.LivingRoom:
                        propertyInfo.numberOfLivingRooms = data.PARAMETERVALUE;
                        break;
                    case ApplicationConstants.Garage:
                        propertyInfo.garage = data.PARAMETERVALUE;
                        break;
                    case ApplicationConstants.HeatingFuel:
                        propertyInfo.heatingFuel = data.PARAMETERVALUE;
                        break;
                    case ApplicationConstants.FloorArea:
                        propertyInfo.floorArea = data.PARAMETERVALUE;
                        break;
                    case ApplicationConstants.Parking:
                        propertyInfo.parking = data.PARAMETERVALUE;
                        break;
                    case ApplicationConstants.Garden:
                        propertyInfo.garden = data.PARAMETERVALUE;
                        break;
                    case ApplicationConstants.Kitchen:
                        propertyInfo.numberOfkitchen = data.PARAMETERVALUE;
                        break;
                    case ApplicationConstants.MaxPeople:
                        propertyInfo.maxPeople = data.PARAMETERVALUE;
                        break;
                    case ApplicationConstants.WheelChairAccess:
                        propertyInfo.wheelChairAccess = data.PARAMETERVALUE;
                         break;
                }                   
                    
               }
            
            //end filling accommodation info

            return propertyInfo;
       

        }





        #endregion

        #region Get image path
        public string getImagePath(string imageName, string propertyId)
        {
            var requestURL = HttpContext.Current.Request.Url;
            string imagePath = requestURL.GetLeftPart(UriPartial.Authority) + "/PropertyImages/" + propertyId + "/Images/" + imageName;
            return imagePath;
        }
        #endregion

        #region Get property viewings
        public List<PropertyViewingResponseModel> GetPropertyViewing(PropertyViewingRequestModel request)
        {
            var data = (from viewing in DbContext.FLS_Viewings
                        join prop in DbContext.P__PROPERTY on viewing.PropertyId equals prop.PROPERTYID
                        join cust in DbContext.C__CUSTOMER on viewing.CustomerId equals cust.CUSTOMERID
                        join emp in DbContext.E__EMPLOYEE on viewing.HousingOfficerId equals emp.EMPLOYEEID
                        where string.Compare(prop.PROPERTYID, request.propertyId, true) == 0
                        && viewing.ViewingDate >= request.startDate && viewing.ViewingDate <= request.endDate
                        select new PropertyViewingResponseModel
                        {
                            viewingId = viewing.ViewingId,
                            customerId = cust.CUSTOMERID,
                            customerName = string.Concat(cust.FIRSTNAME," ",cust.LASTNAME),
                            housingOfficerId = emp.EMPLOYEEID,
                            housingOfficerName = string.Concat(emp.FIRSTNAME," ",emp.LASTNAME),
                            propertyId = prop.PROPERTYID,
                            viewingDate = viewing.ViewingDate,
                            viewingStatusId = viewing.viewingStatusId,
                            viewingTime = viewing.ViewingTime
                        }
                        );
            return data.ToList();
        }
        #endregion

        #region Update property status
        public void UpdatePropertyStatus(string propertyId)
        {
            //var StatusId = DbContext.P_STATUS.Where(x => string.Compare(x.DESCRIPTION, ApplicationConstants.propertyStatusLet, true) == 0).Select(y => y.STATUSID).FirstOrDefault();
            P__PROPERTY property = new P__PROPERTY();

            if (propertyId != null)
            {
                var query = DbContext.P__PROPERTY.Where(x => x.PROPERTYID == propertyId);
                if (query.Count() > 0)
                {
                    property = query.First();

                }
            }




            property.SUBSTATUS = 21;
            base.UpdateStringTypeId(property, propertyId);

        }
        #endregion
    }
}