﻿using BusinessModels;
using DataModel.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DataRepository
{
    public class PropertyRentInfoRepository : BaseRepository<P_FINANCIAL>
    {
        public PropertyRentInfoRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {
        }

        #region Get property financial info on start tenancy screen 
        public PropertyRentInfoDataModel GetPropertyFinancialInfo(string propertyId)
        {
            var financialData = (from fin in DbContext.P_FINANCIAL
                                 where fin.PROPERTYID == propertyId
                                 select new PropertyRentInfoDataModel
                                 {
                                     councilTax = fin.COUNCILTAX,
                                     garage = fin.GARAGE,
                                     ineligServ = fin.INELIGSERV,
                                     rent = fin.RENT,
                                     rentPayable = fin.TOTALRENT,
                                     services = fin.SERVICES,
                                     supportedServices = fin.SUPPORTEDSERVICES,
                                     waterRates = fin.WATERRATES
                                 }
                        ).FirstOrDefault();
            return financialData;
        }
        #endregion

        #region Save property financial info on start tenancy screen
        public void SaveRentInfo(PropertyRentInfoDataModel request,string propertyId)
        {
            P_FINANCIAL fin = new P_FINANCIAL();
            fin.DATERENTSET = null;
            fin.RENTEFFECTIVE = null;
            fin.RENTTYPE = null;
            fin.RENTFREQUENCY = null;
            fin.RENT = request.rent;
            fin.SERVICES = request.services;
            fin.COUNCILTAX = request.councilTax;
            fin.WATERRATES = request.waterRates;
            fin.INELIGSERV = request.ineligServ;
            fin.SUPPORTEDSERVICES = request.supportedServices;
            fin.GARAGE = request.garage;
            fin.TOTALRENT = request.rentPayable;
            fin.PROPERTYID = propertyId;
            fin.PFUSERID = null;
            DbContext.AS_SavePropertyCurrentRentInformation(fin.DATERENTSET, fin.RENTEFFECTIVE, fin.RENTTYPE, fin.RENTFREQUENCY, fin.RENT, fin.SERVICES, fin.COUNCILTAX, fin.WATERRATES, fin.INELIGSERV, fin.SUPPORTEDSERVICES, fin.GARAGE, fin.TOTALRENT, fin.PROPERTYID, fin.PFUSERID);          
        }
        #endregion
    }
}
