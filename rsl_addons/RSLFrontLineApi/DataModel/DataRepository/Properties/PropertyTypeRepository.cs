﻿using DataModel.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DataRepository
{
    public class PropertyTypeRepository : BaseRepository<P_PROPERTYTYPE>
    {
        public PropertyTypeRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {
        }
    }
}
