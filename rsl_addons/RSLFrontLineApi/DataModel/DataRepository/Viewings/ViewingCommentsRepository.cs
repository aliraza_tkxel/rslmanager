﻿using BusinessModels;
using DataModel.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
namespace DataModel.DataRepository
{
    public class ViewingCommentsRepository : BaseRepository<FLS_ViewingComments>
    {
        public ViewingCommentsRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {

        }
        #region "Get List of Viewing Comments"
        public List<LookUpResponseModel> GetViewingCommentsTypes()
        {
            var data = (from cust in DbContext.FLS_ViewingCommentTypes
                        select new LookUpResponseModel
                        {
                            id = cust.CommentTypeId,
                            description = cust.CommentTypeTitle
                        }).ToList();
            return data;

        }

        public List<ViewingCommentResponseModel> GetAllViewingCommentsForId(int ViewingId)
        {
            var data = (from cust in DbContext.FLS_ViewingComments
                        join emp in DbContext.E__EMPLOYEE on cust.CommentedBy equals emp.EMPLOYEEID
                        where cust.ViewingId == ViewingId
                        select new ViewingCommentResponseModel
                        {
                            CommentId = cust.CommentId,
                            CommentText = cust.CommentText,
                            CommentedBy = cust.CommentedBy,
                            CommentOwnerName = string.Concat(emp.FIRSTNAME, " ", emp.LASTNAME),
                            ViewingId = cust.ViewingId,
                            CommentedDate = cust.CommentedDate,
                            CommentTypeId = cust.CommentTypeId
                        }).ToList();
            return data;
        }

        #endregion
    }
}
