﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities;
using DataModel.DatabaseEntities;
using BusinessModels;
using System.Web;
using System.Data;
using System.Linq.Expressions;

namespace DataModel.DataRepository
{        
  public class HousingOfficerRepository
    {
        private RSLBHALiveEntities DbContext { get; set; }
        public HousingOfficerRepository(RSLBHALiveEntities dbContext)
        {
            this.DbContext = dbContext;
        }

        #region Fetching housing officer info
        public List<FLS_GetAvailableOperatives_Result> GetHosusingOfficer(HousingOfficerFilter filter)
        {
            DateTime sDate = Convert.ToDateTime(filter.viewingDate.ToLongDateString() + " " + filter.viewingTime.Substring(0,5));
            var day = sDate.ToString("ddd");
            var availableOperatives = DbContext.FLS_GetAvailableOperatives(sDate.Date, filter.searchText).ToList();
            var availableOperativesLeaves = DbContext.FLS_GetAvailableOperativesLeaves(sDate.Date, filter.searchText).OrderBy(x=>x.OperativeId).ToList();

            var tempOperatives = new List<FLS_GetAvailableOperatives_Result>(availableOperatives);
            foreach (var operative in tempOperatives)
            {
                var isLeaveExist = isLeavesExist(availableOperativesLeaves, operative.EmployeeId, sDate, sDate);
                var isWrokingOnGivenDay = checkWorkingPattern(operative.EmployeeId, sDate, sDate);
                if(isLeaveExist || !isWrokingOnGivenDay)
                {
                    var indesx = availableOperatives.IndexOf(operative);
                    availableOperatives.RemoveAt(availableOperatives.IndexOf(operative));
                }
            }
           
            var lst = availableOperatives.Distinct().ToList();
            foreach(var i in lst)
            {            
                i.ImageName = getImagePath(i.ImageName);
            }

            return lst;
        }
        #endregion

        #region Get image path
        public string getImagePath(string imageName)
        {
            var requestURL = HttpContext.Current.Request.Url;
            string imagePath = requestURL.GetLeftPart(UriPartial.Authority) + "/EmployeeImages/" + imageName;
            return imagePath;
        }
        #endregion

        #region"is Leaves Exist "
        private bool isLeavesExist(List<FLS_GetAvailableOperativesLeaves_Result> leavesDt, int operativeId, System.DateTime appointmentStartDateTime, System.DateTime appoitmentEndDateTime)
        {

            //'çonvert time into seconds for comparison

            int lStartTimeInMin = 0;
            int lEndTimeInMin = 0;

            lStartTimeInMin = Convert.ToInt32(convertDateInMin(appointmentStartDateTime));
            lEndTimeInMin = Convert.ToInt32(convertDateInMin(appoitmentEndDateTime));

            var leaves = leavesDt.AsEnumerable();
            var leaveResult = (from app in leavesDt.AsEnumerable()
                               where app.OperativeId == operativeId
                               && ((lStartTimeInMin <= Convert.ToInt32(app.StartTimeInMin) & lEndTimeInMin > Convert.ToInt32(app.StartTimeInMin) & lEndTimeInMin <= Convert.ToInt32(app.EndTimeInMin))
                               | (lStartTimeInMin >= Convert.ToInt32(app.StartTimeInMin) & lEndTimeInMin <= Convert.ToInt32(app.EndTimeInMin))
                               | (lStartTimeInMin >= Convert.ToInt32(app.StartTimeInMin) & lStartTimeInMin < Convert.ToInt32(app.EndTimeInMin) & lEndTimeInMin >= Convert.ToInt32(app.EndTimeInMin))
                               | (lStartTimeInMin < Convert.ToInt32(app.StartTimeInMin) & lEndTimeInMin > Convert.ToInt32(app.EndTimeInMin)))
                               orderby app.EndTimeInMin ascending
                               select app);
       
            return leaveResult.ToList().Count > 0 ? true : false;
        }

        #endregion

        //Check if employee is working on the given Date.
        #region"Check Working Pattern "
        private bool checkWorkingPattern(int operativeId, System.DateTime appointmentStartDateTime, System.DateTime appoitmentEndDateTime)
        {
            bool isWorking = false;
            var dayOfWeek = appointmentStartDateTime.ToString("ddd");
            var weekNo = getWeekOfMonth(appointmentStartDateTime);
            var operativeWorkingPattern = DbContext.E_WorkingHours.Where(op => op.EmployeeId == operativeId).ToList();

            if (operativeWorkingPattern != null && operativeWorkingPattern.Count > 0)
            {
                var totalWeeks = operativeWorkingPattern.Count();

                //var operativeWorkingPattern = DbContext.E_WorkingHours.Where(op => op.EmployeeId == operativeId).Select(CreateNewStatement(dayOfWeek)).FirstOrDefault();

                if (weekNo > totalWeeks)
                {
                    while (weekNo > totalWeeks)
                    {
                        weekNo -= totalWeeks;
                    }
                }

                var val = operativeWorkingPattern[weekNo-1].GetType().GetProperty(dayOfWeek).GetValue(operativeWorkingPattern[weekNo - 1], null);
                if (val != null && double.Parse(val.ToString().Trim()) > 0)
                {
                    isWorking = true;
                }
            }
            
            return isWorking;
        }

        public int getWeekOfMonth(DateTime date)
        {
            var day = date.Day;
            var data = getWeeksStartAndEndInMonth(date.Month, date.Year, 1);
            var i = 0;
            for (i = 0; i < data.Count; i++)
            {
                if (data[i].start <= day && data[i].end >= day)
                    break;
            }
            return i + 1;
        }

        public int endFirstWeek(DateTime firstDate, int firstDay)
        {
            if (firstDay < 7)
            {
                return 7 - firstDate.Day;
            }
            if (firstDate.Day < firstDay)
            {
                return firstDay - firstDate.Day;
            }
            else
            {
                return 7 - firstDate.Day + firstDay;
            }
        }

        public List<KeyValuePairMonth> getWeeksStartAndEndInMonth(int month, int year, int start)
        {
            var list = new List<KeyValuePairMonth>();

            var firstDate = new DateTime(year, month, 1);
            var lastDate =  new DateTime(year, month, DateTime.DaysInMonth(year, month));
            var numDays = lastDate.Day;
            start = 1;
            var end = endFirstWeek(firstDate, firstDate.Day);
            while (start <= numDays)
            {
                list.Add(new KeyValuePairMonth() { start = start, end = end });
                start = end + 1;
                end = end + 7;
                end = start == 1 && end == 8 ? 1 : end;
                if (end > numDays)
                {
                    end = numDays;
                }
            }
            return list;
        }

        #endregion


        //Converts the given Date to minutes
        #region "Convert Date In Minute"
        private double convertDateInMin(System.DateTime timeToConvert)
        {
            DateTime calendarStartDate = new DateTime(1970, 1, 1);
            TimeSpan ts = timeToConvert - calendarStartDate;
            return ts.TotalMinutes;
        }
        #endregion

        #region "Create dynamic Select Statement on basis of given fields"
        private Func<E_WorkingHours, E_WorkingHours> CreateNewStatement(string fields)
        {
            // input parameter "o"
            var xParameter = Expression.Parameter(typeof(E_WorkingHours), "o");

            // new statement "new Data()"
            var xNew = Expression.New(typeof(E_WorkingHours));

            // create initializers
            var bindings = fields.Split(',').Select(o => o.Trim())
                .Select(o => {

            // property "Field1"
            var mi = typeof(E_WorkingHours).GetProperty(o);

            // original value "o.Field1"
            var xOriginal = Expression.Property(xParameter, mi);

            // set value "Field1 = o.Field1"
            return Expression.Bind(mi, xOriginal);
                }
            );

            // initialization "new Data { Field1 = o.Field1, Field2 = o.Field2 }"
            var xInit = Expression.MemberInit(xNew, bindings);

            // expression "o => new Data { Field1 = o.Field1, Field2 = o.Field2 }"
            var lambda = Expression.Lambda<Func<E_WorkingHours, E_WorkingHours>>(xInit, xParameter);

            // compile to Func<Data, Data>
            return lambda.Compile();
        }
        #endregion

    }
}
