﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModel.DatabaseEntities;
namespace DataModel.DataRepository
{
    public class ViewingStatusRepository : BaseRepository<FLS_ViewingStatus>
    {
        public ViewingStatusRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {
        }
    }
}
