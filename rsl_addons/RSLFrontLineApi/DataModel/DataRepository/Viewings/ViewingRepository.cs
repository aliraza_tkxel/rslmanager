﻿using BusinessModels;
using DataModel.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace DataModel.DataRepository
{
   public class ViewingRepository : BaseRepository<FLS_Viewings>
    {
        public ViewingRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {

        }
        #region Arrange new viewing
        public ArrangeViewingResponseModel ArrangeViewing(int viewingId)
        {
            //start creating response

            var data = (from cust in DbContext.C__CUSTOMER
                        join viewing in DbContext.FLS_Viewings on cust.CUSTOMERID equals viewing.CustomerId
                        join prop in DbContext.P__PROPERTY on viewing.PropertyId equals prop.PROPERTYID
                        join e in DbContext.E__EMPLOYEE on viewing.HousingOfficerId equals e.EMPLOYEEID
                        into group1
                        from emp in group1.DefaultIfEmpty()
                        where viewing.ViewingId == viewingId
                        select new ArrangeViewingResponseModel
                        {
                            viewingId = viewing.ViewingId,
                            customerId = viewing.CustomerId,
                            customerName = string.Concat(cust.FIRSTNAME, " ", cust.LASTNAME),
                            customerGender = cust.GENDER,
                            propertyAddress = string.Concat(prop.HOUSENUMBER, " ", prop.ADDRESS1, " ", prop.ADDRESS2),
                            viewingDate = viewing.ViewingDate,
                            viewingStatusId = viewing.viewingStatusId,
                            createdOnApp = viewing.CreatedOnApp,
                            housingOfficerId = viewing.HousingOfficerId,
                            housingOfficerName = string.Concat(emp.FIRSTNAME, " ", emp.LASTNAME),
                            housingOfficerGender = emp.GENDER,
                            viewingTime = viewing.ViewingTime
                        }
                        ).FirstOrDefault();
            //end creating response        
            return data;
        }
        #endregion
        
        #region Get details of a viewing
        public ViewingDetailsResponseModel GetViewingDetails(int viewingId)
        {
            PropertiesRepository objPropertiesRepository = new PropertiesRepository(DbContext);
            ViewingDetailsResponseModel response = new ViewingDetailsResponseModel();

            ViewingCommentsRepository objViewingCommentsRepository = new ViewingCommentsRepository(DbContext);
            var commentsList = objViewingCommentsRepository.GetAllViewingCommentsForId(viewingId);

            var data = (from cust in DbContext.C__CUSTOMER
                        join ad in DbContext.C_ADDRESS on cust.CUSTOMERID equals ad.CUSTOMERID
                        into group0
                        from adrs in group0.DefaultIfEmpty()


                        join viewing in DbContext.FLS_Viewings on cust.CUSTOMERID equals viewing.CustomerId
                        join prop in DbContext.P__PROPERTY on viewing.PropertyId equals prop.PROPERTYID
                        join fin in DbContext.P_FINANCIAL on prop.PROPERTYID equals fin.PROPERTYID

                        join emp in DbContext.E__EMPLOYEE on viewing.CreatedById equals emp.EMPLOYEEID

                        join assignedemp in DbContext.E__EMPLOYEE on viewing.AssignedById equals assignedemp.EMPLOYEEID

                        join pT in DbContext.P_PROPERTYTYPE on prop.PROPERTYTYPE equals pT.PROPERTYTYPEID
                        into group1
                        from propType in group1.DefaultIfEmpty()

                        join pPic in DbContext.PA_PROPERTY_ITEM_IMAGES on prop.PropertyPicId equals pPic.SID
                        into group3
                        from propertyPic in group3.DefaultIfEmpty()


                        join cT in DbContext.C_CUSTOMERTYPE on cust.CUSTOMERTYPE equals cT.CUSTOMERTYPEID
                        into group2
                        from custType in group2.DefaultIfEmpty()

                        where viewing.ViewingId == viewingId
                        select new ViewingDetailsResponseModel
                        {
                            viewingId = viewing.ViewingId,
                            customerName = string.Concat(cust.FIRSTNAME, cust.LASTNAME),
                            customerType = custType.DESCRIPTION,
                            dob = cust.DOB,
                            gender = cust.GENDER == null ? "N/A" : cust.GENDER,
                            mobileNo = adrs.TEL == null ? "N/A" : adrs.TEL,
                            propertyAddress = string.Concat(prop.HOUSENUMBER, " ", prop.ADDRESS1, " ", prop.ADDRESS2, " ", prop.ADDRESS3, " ", prop.TOWNCITY, " ", prop.POSTCODE),
                            propertyId = prop.PROPERTYID,
                            propertyType = propType.DESCRIPTION,
                            propertyImage = propertyPic.ImageName,
                            rent = fin.TOTALRENT,
                            createdOnApp = viewing.CreatedOnApp,
                            viewingDateTime = viewing.ViewingDate,
                            viewingStatusId = viewing.viewingStatusId,
                            viewingCreatedBy = string.Concat(emp.FIRSTNAME, " ", emp.LASTNAME),
                            viewingNotes = viewing.Notes == null ? "" : viewing.Notes,
                            outlookIdentifier = viewing.outlookIdentifier,
                            assignedById = viewing.AssignedById,
                            assignedByName = string.Concat(assignedemp.FIRSTNAME, " ", assignedemp.LASTNAME),
                          
                        }
                        );
            if (data.Count() > 0)
            {
                response = data.FirstOrDefault();

                response.noOfBedRooms = objPropertiesRepository.GetBeddingInfo(response.propertyId);
                response.commentsList = commentsList;
                
                
            }
            return response;
        }


        #endregion

        #region Get viewing list
        public List<ViewingListDataModel> GetViewingList(ViewingListRequestModel request)
        {
            var data = (from viewing in DbContext.FLS_Viewings
                        join viewingStatus in DbContext.FLS_ViewingStatus on viewing.viewingStatusId equals viewingStatus.StatusId
                        join cust in DbContext.C__CUSTOMER on viewing.CustomerId equals cust.CUSTOMERID
                        join prop in DbContext.P__PROPERTY on viewing.PropertyId equals prop.PROPERTYID
                        join emp in DbContext.E__EMPLOYEE on viewing.HousingOfficerId equals emp.EMPLOYEEID
                        join ec in DbContext.E_CONTACT on emp.EMPLOYEEID equals ec.EMPLOYEEID
                        into group1
                        from econtact in group1.DefaultIfEmpty()
                        where (viewing.HousingOfficerId == request.housingOfficer
                        || viewing.CreatedById == request.housingOfficer)
                        && viewing.ViewingDate >= request.startDate && viewing.ViewingDate <= request.endDate
                        select new ViewingListDataModel
                        {
                            viewingId = viewing.ViewingId,
                            customerId = viewing.CustomerId,
                            customerName = string.Concat(cust.FIRSTNAME, " ", cust.LASTNAME),
                            customerGender = cust.GENDER,
                            viewingDate = viewing.ViewingDate,
                            viewingStatusId = viewingStatus.StatusId,
                            viewingTime = viewing.ViewingTime,
                            housingOfficerId = viewing.HousingOfficerId,
                            createdById = viewing.CreatedById,
                            housingOfficerName = string.Concat(emp.FIRSTNAME, " ", emp.LASTNAME),
                            housingOfficerGender = emp.GENDER,
                            propertyAddress = string.Concat(prop.HOUSENUMBER," ", prop.ADDRESS1," ", prop.ADDRESS2, " ", prop.ADDRESS3, " ", prop.TOWNCITY, " ", prop.POSTCODE),
                            createdOnApp = viewing.CreatedOnApp,
                            housingOfficerEmail = econtact.WORKEMAIL
                            
                        }                        
                        ).ToList();
            return data;

        }
        #endregion

        #region Check if viewing exist for same housing officer on same day and time
        public bool checkFromCurrentViewings(FLS_Viewings model)
        {
            var viewingExist = false;
            var time = model.ViewingTime.Substring(0, 5);
            var date = model.ViewingDate;
            var housingOfficer = model.HousingOfficerId;
            var viewings = DbContext.FLS_Viewings.Where(x => x.HousingOfficerId == housingOfficer && x.ViewingTime.Substring(0, 5) == time && x.ViewingDate.Day==date.Day && x.ViewingDate.Month == date.Month && x.ViewingDate.Year == date.Year);
            if (viewings.ToList().Count() > 0)
            {
                foreach(FLS_Viewings viewing in viewings.ToList())
                {
                    if(viewing.PropertyId==model.PropertyId)
                    {
                        return false;
                    } 
                    else
                    {
                        viewingExist = true;
                    }
                }
            }
            return viewingExist;
        }
        #endregion

        public ViewingInfoDataModel GetViewingInfo(int viewingId)
        {
            var data = (from viewing in DbContext.FLS_Viewings
                        join cust in DbContext.C__CUSTOMER on viewing.CustomerId equals cust.CUSTOMERID
                        join emp in DbContext.E__EMPLOYEE on viewing.HousingOfficerId equals emp.EMPLOYEEID
                        join viewProp in DbContext.P__PROPERTY on viewing.PropertyId equals viewProp.PROPERTYID
                        join add in DbContext.C_ADDRESS on cust.CUSTOMERID equals add.CUSTOMERID
                        join fin in DbContext.P_FINANCIAL on viewProp.PROPERTYID equals fin.PROPERTYID
                        join rent in DbContext.P_RENTFREQUENCY on fin.RENTFREQUENCY equals rent.FID 
                        join ec in DbContext.E_CONTACT on emp.EMPLOYEEID equals ec.EMPLOYEEID
                        into group1
                        from econtact in group1.DefaultIfEmpty()
                        where viewing.ViewingId == viewingId && add.ISDEFAULT == 1
                        select new ViewingInfoDataModel
                        {
                            viewingId = viewing.ViewingId,
                            customerId = viewing.CustomerId,
                            customerName = cust.GENDER=="Male" ?  string.Concat("Mr ",cust.FIRSTNAME, " ", cust.LASTNAME): string.Concat("Miss ", cust.FIRSTNAME, " ", cust.LASTNAME),
                            customerlastName = cust.GENDER == "Male" ? string.Concat("Mr"," ", cust.LASTNAME) : string.Concat("Miss"," ", cust.LASTNAME),
                            customerGender = cust.GENDER,
                            customerEmail = add.EMAIL,
                            customerMobile=add.MOBILE,
                            viewingDate = viewing.ViewingDate,
                            viewingTime = viewing.ViewingTime,
                            housingOfficerId = viewing.HousingOfficerId,
                            createdById = viewing.CreatedById,
                            housingOfficerName = string.Concat(emp.FIRSTNAME, " ", emp.LASTNAME),
                            housingOfficerGender = emp.GENDER,
                            propertyAddress = string.Concat(add.HOUSENUMBER, " ", add.ADDRESS1),
                            propertyTownCity= add.TOWNCITY,
                            propertyPostCode = add.POSTCODE,
                            viewingAddress = string.Concat(viewProp.HOUSENUMBER, " ", viewProp.ADDRESS1, " ", viewProp.ADDRESS2, " ", viewProp.ADDRESS3, " ", viewProp.TOWNCITY, " ", viewProp.POSTCODE),
                            createdOnApp = viewing.CreatedOnApp,
                            housingOfficerEmail = econtact.WORKEMAIL,
                            weekRent = rent.DESCRIPTION=="Weekly" ? (decimal)fin.TOTALRENT : (decimal) (fin.TOTALRENT /30) * 7,
                            monthRent = rent.DESCRIPTION == "Monthly"  ? (decimal)fin.TOTALRENT : (decimal)(fin.TOTALRENT / 7) * 30


                        }
                        ).FirstOrDefault();


            return data;

        }



    }
}
