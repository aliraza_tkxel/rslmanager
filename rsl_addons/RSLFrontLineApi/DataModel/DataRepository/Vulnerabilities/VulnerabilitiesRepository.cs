﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModel.DatabaseEntities;
using BusinessModels;
using Utilities;

namespace DataModel.DataRepository
{
   public class VulnerabilitiesRepository : BaseRepository<C_VULNERABILITY>
    {
        public VulnerabilitiesRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {
        }

        #region Get Vulnerabilities List
        public List<VulnerabilitiesListingDataModel> GetVulnerabilitiesList(int? statusId,int?customerId)
        {
            if (statusId <= 0)
            {
                statusId = null;
            }           
            var vulnerabilityData = (from v in DbContext.C_VULNERABILITY
                            join cv in DbContext.C_Customer_Vulnerability on v.VULNERABILITYHISTORYID equals cv.VulnerabilityHistoryId
                            join c in DbContext.C__CUSTOMER on cv.CustomerId equals c.CUSTOMERID
                            join j in DbContext.C_JOURNAL on cv.JournalId equals j.JOURNALID

                            join t in DbContext.C_CUSTOMERTENANCY.Where(x => x.ENDDATE == null) on c.CUSTOMERID equals t.CUSTOMERID
                            into group1
                            from ten in group1.DefaultIfEmpty()

                            join ct in DbContext.C_TENANCY.Where(x => x.ENDDATE == null) on ten.TENANCYID equals ct.TENANCYID
                            into group2
                            from cTen in group2.DefaultIfEmpty()
                            orderby cv.VulnerabilityHistoryId
                            where 1 == 1
                            &&
                            (
                                c.CUSTOMERID == customerId
                                || customerId == null
                            )
                            &&
                            (
                                j.CURRENTITEMSTATUSID == statusId
                                || statusId == null
                            )
                            select new VulnerabilitiesListingDataModel
                            {
                                customerId = c.CUSTOMERID,
                                customerName = string.Concat(c.FIRSTNAME, " ", c.LASTNAME),
                                reviewDate = v.REVIEWDATE,
                                vulnerabilityHistoryId = v.VULNERABILITYHISTORYID,
                                startDate = v.STARTDATE,
                                journalId = j.JOURNALID,
                                actionId = v.ITEMACTIONID,
                                assignedTo = v.ASSIGNTO,
                                isCustomerAware = v.CUSTOMERAWARE,
                                isTenantOnline = v.TENANTSONLINE,
                                lastActionDate = v.LASTACTIONDATE,
                                lastActionUserId = v.LASTACTIONUSER,
                                notes = v.NOTES,
                                statusId = v.ITEMSTATUSID,
                                teamId = v.TEAM,
                                tenancyId = ten.TENANCYID,
                                title = j.TITLE
                            }
                           ).Distinct().OrderByDescending(x => x.vulnerabilityHistoryId).ToList();

            //Get Vulnerabilities Subcategories
            foreach (var data in vulnerabilityData)
            {
                data.customerVulnerabilitySubcategories = GetVulnerabilitiesSubcategories(data.journalId, data.vulnerabilityHistoryId);                                             
            }
            return vulnerabilityData;
        }

        #region Get Vulnerabilities Subcategories
        public string GetVulnerabilitiesSubcategories(int journalId, int vulnerabilityHistoryId)
        {
            string subCat = "";
            var data = (from vul in DbContext.C_Customer_Vulnerability
                        join sub in DbContext.C_VULNERABILITY_SUBCATEGORY on vul.SubCategoryId equals sub.SubCategoryId
                        join cat in DbContext.C_VULNERABILITY_CATEGORY on sub.CategoryId equals cat.CategoryId
                        where vul.JournalId == journalId
                        && vul.VulnerabilityHistoryId == vulnerabilityHistoryId
                        select sub.SubCategoryId
                        );
            foreach (var sub in data.ToList())
            {
                subCat = subCat + sub + ",";
            }
            return subCat;
        }
        #endregion



        #endregion

        #region Add/Edit Vulnerabilities details
        public C_VULNERABILITY AddEditVulnerabilities(VulnerabilitiesListingDataModel request)
        {
            C_VULNERABILITY vulnerability = new C_VULNERABILITY();
            int statusId = int.Parse(DbContext.C_STATUS.Where(x => string.Compare(x.DESCRIPTION, ApplicationConstants.ItemStatusClosed, true) == 0).Select(x => x.ITEMSTATUSID).FirstOrDefault().ToString());
            if (request.vulnerabilityHistoryId > 0)
            {
                var query = DbContext.C_VULNERABILITY.Where(x => x.VULNERABILITYHISTORYID == request.vulnerabilityHistoryId);
                if (query.Count() > 0)
                {
                    vulnerability = query.First();
                }
            }           
            if (request.statusId == statusId)
            {
                vulnerability.DTIMESTAMP = DateTime.Now;
            }
            vulnerability.ASSIGNTO = request.assignedTo;
            vulnerability.CUSTOMERAWARE = request.isCustomerAware;
            vulnerability.ITEMACTIONID = request.actionId;
            vulnerability.ITEMSTATUSID = request.statusId;
            vulnerability.JOURNALID = request.journalId;
            vulnerability.LASTACTIONDATE = request.lastActionDate;
            vulnerability.LASTACTIONUSER = request.lastActionUserId;
            vulnerability.NOTES = request.notes;
            vulnerability.REVIEWDATE = request.reviewDate;
            vulnerability.STARTDATE = request.startDate;
            vulnerability.TEAM = request.teamId;
            vulnerability.TENANTSONLINE = request.isTenantOnline;                
            if (request.vulnerabilityHistoryId > 0)
            {               
                base.Update(vulnerability, vulnerability.VULNERABILITYHISTORYID);
            }
            else
            {
                base.Add(vulnerability);
            }
            return vulnerability;
        }

        

        #endregion
    }
}
