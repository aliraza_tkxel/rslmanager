﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModel.DatabaseEntities;
using BusinessModels;
using Utilities;

namespace DataModel.DataRepository
{
    public class AuthenticationRepository
    {
        private RSLBHALiveEntities DbContext { get; set; }
        public AuthenticationRepository(RSLBHALiveEntities dbContext)
        {
            this.DbContext = dbContext;
        }
        #region Authenticate a user
        public LoginResponseModel AuthenticateUser(LoginRequestModel request)
        {
            LoginResponseModel response = new LoginResponseModel();
            int userId = 0;
           if(CheckUserTeamAndRole(request.userName) == true)
            {
                var data = (from lg in DbContext.SP_NET_CHECKLOGIN(request.userName, request.password)
                            select lg).ToList();

                if (data.Count() > 0)
                {
                    userId = data.Select(x => x.LOGINID).FirstOrDefault();
                    response.employeeFullName = string.Concat(data.Select(x => x.FIRSTNAME).FirstOrDefault(), data.Select(x => x.LASTNAME).FirstOrDefault());
                    response.isActive = data.Select(x => x.ACTIVE).FirstOrDefault();
                    response.userId = data.Select(x => x.EMPLOYEEID).FirstOrDefault();
                    response.userName = data.Select(x => x.LOGIN).FirstOrDefault();
                    response.teamId = data.Select(x => x.TEAMID).FirstOrDefault();
                    //get gender of logged in user
                    response.gender = DbContext.E__EMPLOYEE.Where(x => x.EMPLOYEEID == response.userId).Select(x => x.GENDER).FirstOrDefault();
                }
            }
            
            if (userId == 0)
            {
                SP_NET_SETLOGIN_THRESHOLD_Result loginThreshold = setLoginThreshold(request.userName);//set threshold limits

                if (loginThreshold != null)
                {

                    if (loginThreshold.Locked == true)//access lockdown msg
                    {

                        throw new ArgumentException(UserMessageConstants.AccessLockdownMsg);
                    }
                    else if (loginThreshold.Threshold == 4)//access lockdown warning msg
                    {
                        throw new ArgumentException(UserMessageConstants.AccessLockdownWarningMsg);
                    }
                    else//wrong UN and password msg
                    {
                        throw new ArgumentException(UserMessageConstants.LoginFailureMsg);
                    }

                }
                else //sussess msg
                {
                    throw new ArgumentException(UserMessageConstants.LoginScccessMsg);
                }
            }
            else
            {
                resetLoginThreshold(request.userName);//reset threshold limist in case of correct UN & password          
            }
            return response;
        }

        #endregion

        #region Reset login threshold           
        public void resetLoginThreshold(String username)
        {
            var userRecord = DbContext.AC_LOGINS.Where(usr => usr.LOGIN == username);

            if (userRecord.Count() > 0)
            {
                AC_LOGINS user = userRecord.First();
                user.THRESHOLD = 0;
                user.ISLOCKED = false;
                DbContext.SaveChanges();
            }
        }
        #endregion 

        #region Set Login Threshold      
        public SP_NET_SETLOGIN_THRESHOLD_Result setLoginThreshold(string userName)
        {

            var data = (from lst in DbContext.SP_NET_SETLOGIN_THRESHOLD(userName)
                        select lst).ToList();


            SP_NET_SETLOGIN_THRESHOLD_Result loginThreshold = null;
            if (data.Count() > 0)
            {
                loginThreshold = data.FirstOrDefault();
            }

            return loginThreshold;
        }

        #endregion

        #region Email Forgotten Password
        public EmailTemplateResponseModel getUserInfoByEmail(string emailAddress)
        {           
            var data = (from info in DbContext.SP_GetUserInfoByEmail(emailAddress)
                                select new EmailTemplateResponseModel
                                {
                                    login = info.LOGIN,
                                    password = info.PASSWORD,
                                    workEmail = info.WORKEMAIL
                                }
                               ).ToList();
            var result = data.FirstOrDefault();
            result.password = Encoding.ASCII.GetString(Convert.FromBase64String(result.password));
            return data.FirstOrDefault();
        }
        #endregion

        #region Check whether the user have frontline team and Neighbourhood Officer role
        public bool CheckUserTeamAndRole(string username)
        {
            var data = (from emp in DbContext.E__EMPLOYEE
                        join login in DbContext.AC_LOGINS on emp.EMPLOYEEID equals login.EMPLOYEEID
                        join jd in DbContext.E_JOBDETAILS on emp.EMPLOYEEID equals jd.EMPLOYEEID
                        join ejrt in DbContext.E_JOBROLETEAM on emp.JobRoleTeamId equals ejrt.JobRoleTeamId
                        join et in DbContext.E_TEAM on ejrt.TeamId equals et.TEAMID
                        //join ejrt in DbContext.E_JOBROLETEAM on et.TEAMID equals ejrt.TeamId
                        join ejr in DbContext.E_JOBROLE on ejrt.JobRoleId equals ejr.JobRoleId
                        where (jd.ACTIVE == 1)
                        && (et.TEAMNAME.Trim() == ApplicationConstants.TeamTypeHousingTeam.Trim() || et.TEAMNAME.Trim() == ApplicationConstants.TeamTypeDigitalEngagementTeam.Trim())
                        && (et.ACTIVE == 1)
                        && (ejr.JobeRoleDescription.Trim() == ApplicationConstants.JobRoleHousingTeamNeighbourhoodofficer.Trim()
                            || ejr.JobeRoleDescription.Trim() == ApplicationConstants.JobRoleHousingTeamLeader.Trim()
                            || ejr.JobeRoleDescription.Trim() == ApplicationConstants.JobRoleHousingTeamLocalAreaAdvisor.Trim()
                            || ejr.JobeRoleDescription.Trim() == ApplicationConstants.JobRoleHousingTeamLocalAreaAdvisorApprentice.Trim()
                            || ejr.JobeRoleDescription.Trim() == ApplicationConstants.JobRoleHousingTeamSchemeManager.Trim()
                            || ejr.JobeRoleDescription.Trim() == ApplicationConstants.JobRoleHousingTeamSeniorHousingManager.Trim()
                            || ejr.JobeRoleDescription.Trim() == ApplicationConstants.JobRoleDigitalEngagementTeamReleaseManager.Trim()
                            || ejr.JobeRoleDescription.Trim() == ApplicationConstants.JobRoleDigitalEngagementTeamSkillsTrainer.Trim()
                            || ejr.JobeRoleDescription.Trim() == ApplicationConstants.JobRoleDigitalEngagementTeamSupportAssistant.Trim())
                        && login.LOGIN == username
                        orderby emp.FIRSTNAME, emp.LASTNAME
                        select new HousingOfficerResponseModel
                        {
                            employeeId = emp.EMPLOYEEID,
                            firstName = emp.FIRSTNAME,
                            lastName = emp.LASTNAME,
                            gender = emp.GENDER,
                            imageName = emp.IMAGEPATH,
                            email = null
                        }
                        ).ToList();
            return data.Count > 0 ? true : false; 

        }
   
        #endregion

    }
}
