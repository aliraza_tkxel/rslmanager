﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModel.DatabaseEntities;
using Utilities;
using BusinessModels;

namespace DataModel.DataRepository
{
    public class ReferralsRepository : BaseRepository<C_REFERRAL>
    {
        public ReferralsRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {
        }
        #region Add/edit referrals
        public C_REFERRAL AddEditReferral(CustomerReferralsDataModel referralRequestModel)
        {
            var statusId = DbContext.C_STATUS.Where(x => string.Compare(x.DESCRIPTION, ApplicationConstants.ReferralstatusPending, true) == 0).OrderByDescending(y => y.ITEMSTATUSID).Select(x => x.ITEMSTATUSID).FirstOrDefault();
            var actionId = DbContext.C_ACTION.Where(x => string.Compare(x.DESCRIPTION, ApplicationConstants.ItemActionReferred, true) == 0).Select(x => x.ITEMACTIONID).FirstOrDefault();
            //start inserting data of referral
            C_REFERRAL referral = new C_REFERRAL();
            if (referralRequestModel.referralHistoryId > 0)
            {
                var query = DbContext.C_REFERRAL.Where(x => x.REFERRALHISTORYID == referralRequestModel.referralHistoryId);
                if (query.Count() > 0)
                {
                    referral = query.First();

                }
            }
            referral.JOURNALID = referralRequestModel.journalId;
            referral.ITEMSTATUSID = statusId;
            referral.ITEMACTIONID = actionId;
            referral.isTypeArrears = referralRequestModel.isTypeArrears;
            referral.isTypeASB = referralRequestModel.isTypeAsb;
            referral.isTypeDamage = referralRequestModel.isTypeDamage;
            referral.STAGEID = referralRequestModel.stageId;
            referral.SAFETYISSUES = referralRequestModel.safetyIssueId;
            referral.CONVICTION = referralRequestModel.isConviction;
            referral.SUBSTANCEMISUSE = referralRequestModel.isSubstanceMisuse;
            referral.SELFHARM = referralRequestModel.isSelfHarm;
            referral.ISTENANTAWARE = referralRequestModel.isTenantAware;
            referral.NOTES = referralRequestModel.yesNotes;
            referral.NEGLECTNOTES = referralRequestModel.neglectNotes;
            referral.OTHERNOTES = referralRequestModel.otherNotes;
            referral.MISCNOTES = referralRequestModel.miscNotes;
            referral.LASTACTIONUSER = referralRequestModel.lastActionUserId;
            referral.AppVersion = referralRequestModel.appVersion;
            referral.LASTACTIONDATE = DateTime.Now;
            referral.LastModifiedOnApp = referralRequestModel.lastModifiedOnApp;
            referral.CreatedOnApp = referralRequestModel.createdOnApp;           
            //end inserting data of referral


            if (referralRequestModel.referralHistoryId > 0)
            {               
                referral.LastModifiedOnServer = DateTime.Now;
                base.Update(referral, referral.REFERRALHISTORYID);
            }
            else
            {
                referral.CreatedOnServer = DateTime.Now;
                base.Add(referral);

            }

            return referral;

        }
        #endregion

    }
}
