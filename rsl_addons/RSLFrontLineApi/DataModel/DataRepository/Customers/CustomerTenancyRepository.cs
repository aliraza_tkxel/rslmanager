﻿using BusinessModels;
using DataModel.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities;

namespace DataModel.DataRepository
{
    public class CustomerTenancyRepository : BaseRepository<C_CUSTOMERTENANCY>
    {
        public CustomerTenancyRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {
        }

        #region Get existing tenancy info
        public bool IsTenancyExist(int? customerId)
        {
            var data = DbContext.C_CUSTOMERTENANCY.Where(x => x.CUSTOMERID == customerId).OrderByDescending(y=>y.TENANCYID).FirstOrDefault();
            if(data !=null)
            {
                return data.ENDDATE == null ? true : false;
            }
            else
            {
                return false;
            }
                
        }
        #endregion

        #region Save customer tenancy info
        public void SaveCustomerTenancyRelationInfo(TenancySetupDataModel request)
        {            
            C_CUSTOMERTENANCY tenant = new C_CUSTOMERTENANCY();
            C_CUSTOMERTENANCY jointTenant = new C_CUSTOMERTENANCY();


            //data insertion of tenant and join tenant in customerTenancy            
            tenant.CUSTOMERID = request.customerId;
            tenant.ENDDATE = null;           
            tenant.STARTDATE = request.tenancyStartDate;
            tenant.TENANCYID = request.tenancyId;

            if(request.jointTenant != null)
            {
                jointTenant.CUSTOMERID = request.jointTenant.customerId;
                jointTenant.ENDDATE = null;
                jointTenant.STARTDATE = request.jointTenant.startDate;
                jointTenant.TENANCYID = request.jointTenant.tenancyId;

                DbContext.C_MAKE_DEFAULT_CONTACT(request.jointTenant.isCorrespondingAddress, request.propertyId, request.customerId);
                base.Add(jointTenant);
            }
            else
            {
                DbContext.C_MAKE_DEFAULT_CONTACT(1, request.propertyId, request.customerId);
            }
            
            base.Add(tenant);
                     
        }
        #endregion

        #region Get existing termination info
        public bool IsTerminationExist(int? customerId)
        {
            var tenancyId = DbContext.C_CUSTOMERTENANCY.Where(x => x.CUSTOMERID == customerId).OrderByDescending(y => y.TENANCYID).Select(z=>z.TENANCYID).FirstOrDefault();
            var terminationData = (from cj in DbContext.C_JOURNAL
                                   join ct in DbContext.C_TERMINATION on cj.JOURNALID equals ct.JOURNALID
                                   where cj.CUSTOMERID == customerId 
                                   && cj.ITEMID == ApplicationConstants.ItemId
                                   && cj.ITEMNATUREID == ApplicationConstants.ItemNatureId
                                   && cj.TENANCYID == tenancyId
                                   select cj
                                   ).FirstOrDefault();


            return terminationData == null ? false : true;            
        }
        #endregion


    }
}
