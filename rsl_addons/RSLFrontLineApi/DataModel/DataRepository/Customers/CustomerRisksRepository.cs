﻿using BusinessModels;
using DataModel.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DataRepository
{
    public class CustomerRisksRepository : BaseRepository<C_Customer_Risk>
    {
        public CustomerRisksRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {
        }
        #region Add/Edit customer-risks details
        public void AddEditCustomerRisks(RisksListingDataModel request)
        {
            C_Customer_Risk custRisk = new C_Customer_Risk();
            custRisk.RiskHistoryId = request.riskHistoryId;
            custRisk.JournalId = request.journalId;
            custRisk.CustomerId = request.customerId;
            var cat = DbContext.C_VULNERABILITY_SUBCATEGORY.Where(x => x.SubCategoryId == request.subCategoryId).Select(x => x.CategoryId).FirstOrDefault();
           // custRisk.CategoryId = cat;
            custRisk.SubCategoryId = request.subCategoryId;
            base.Add(custRisk);



        }
        #endregion
    }
}
