﻿using BusinessModels;
using DataModel.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DataRepository
{
    public class TenantDisclosureInfoRepository : BaseRepository<FLS_TenantDisclosure>
    {
        public TenantDisclosureInfoRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {
        }

        #region Save disclosure info
       public FLS_TenantDisclosure SaveDisclosureInfo(TenancyDisclosureDataModel request)
        {
            FLS_TenantDisclosure disclosure = new FLS_TenantDisclosure();          
            if (request.disclosureId > 0)
            {
                var query = DbContext.FLS_TenantDisclosure.Where(x => x.DisclosureId == request.disclosureId);
                if (query.Count() > 0)
                {
                    disclosure = query.First();

                }
            }

            disclosure.CriminalOffence = request.hasCriminalOffense;
            disclosure.DebtReliefOrder = request.isSubjectToDebt;
            disclosure.EvictedFromTenancy = request.isEvicted;
            disclosure.Reason = request.evictedReason;
            disclosure.TenancyId = request.tenancyId;
            disclosure.UnsatisfiedCCJ = request.isUnsatisfiedCcj;
            disclosure.CustomerId = request.customerId;
            if (request.disclosureId > 0)
            {
                base.Update(disclosure, request.disclosureId);
            }
            else
            {
                base.Add(disclosure);
            }                            
            return disclosure;
        }
        #endregion
    }
}
