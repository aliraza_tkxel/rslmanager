﻿using DataModel.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace DataModel.DataRepository
{
    public class ReferralHelpCategoryRepository : BaseRepository<C_REFERRAL_HELPCATEGORY>
    {
        public ReferralHelpCategoryRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {
        }
    }
}
