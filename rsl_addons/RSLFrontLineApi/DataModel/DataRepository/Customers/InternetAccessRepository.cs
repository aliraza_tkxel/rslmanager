﻿using DataModel.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DataRepository
{
    public class InternetAccessRepository : BaseRepository<G_INTERNETACCESS >
    {
        public InternetAccessRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {

        }
    }
}
