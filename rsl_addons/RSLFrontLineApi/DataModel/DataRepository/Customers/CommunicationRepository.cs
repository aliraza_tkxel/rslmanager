﻿using DataModel.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DataRepository
{
  public   class CommunicationRepository : BaseRepository<G_COMMUNICATION >
    {
        public CommunicationRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {

        }
    }
}
