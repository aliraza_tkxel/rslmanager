﻿using DataModel.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DataRepository
{
    public class PreferedContactRepository : BaseRepository<G_PREFEREDCONTACT >
    {
        public PreferedContactRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {

        }

    }
}
