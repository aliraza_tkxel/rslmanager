﻿using BusinessModels;
using DataModel.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities;

namespace DataModel.DataRepository
{
    public class CustomerAddressRepository : BaseRepository<C_ADDRESS>
    {
        public CustomerAddressRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {

        }
        #region insert address
        public C_ADDRESS AddEditAddress(CustomerAddressDataModel customerAddress, int customerId)//CustomerDetailDataModel customerModel)
        {            
            C_ADDRESS address = new C_ADDRESS();
                if (customerAddress.addressId > 0)
                {
                    var query = DbContext.C_ADDRESS.Where(x => x.ADDRESSID== customerAddress.addressId);
                    if (query.Count() > 0)
                    {
                        address = query.First();

                    }
                }
                address.CUSTOMERID = customerId;
                address.HOUSENUMBER = customerAddress.houseNumber;
                address.ADDRESS1 = customerAddress.address1;
                address.ADDRESS2 = customerAddress.address2;
                address.ADDRESS3 = customerAddress.address3;
                address.TOWNCITY = customerAddress.city;
                address.POSTCODE = customerAddress.postCode;
                address.COUNTY = customerAddress.county;
                address.TEL = customerAddress.telephone;
                address.MOBILE = customerAddress.mobile;
                address.EMAIL = customerAddress.email;
                address.ISDEFAULT = customerAddress.isDefault;
                address.ADDRESSTYPE = customerAddress.addressTypeId;
               

                if (customerAddress.addressId > 0)
                {
                    base.Update(address, address.ADDRESSID);                 
                }
                else
                {
                    base.Add(address);
                }

            return address;
        }
        #endregion

        #region Insert ICE info
        public C_ADDRESS AddEditEmergencyContactInfo(EmergencyContactDataModel request, int customerId)
        {
            C_ADDRESS address = new C_ADDRESS();
            // var emergencyContactId = DbContext.C_ADDRESSTYPE.Where(x => string.Compare(x.DESCRIPTION, ApplicationConstants.AddressTypeICE, true) == 0).Select(y=>y.ADDRESSTYPEID).FirstOrDefault();
            if (request.addressId > 0)
            {
                var query = DbContext.C_ADDRESS.Where(x => x.ADDRESSID == request.addressId);
                if (query.Count() > 0)
                {
                    address = query.First();

                }
            }
            address.CUSTOMERID = customerId;
            address.HOUSENUMBER = request.houseNumber;
            address.ADDRESS1 = request.address1;
            address.ADDRESS2 = request.address2;
            address.ADDRESS3 = request.address3;
            address.TOWNCITY = request.city;
            address.POSTCODE = request.postCode;
            address.COUNTY = request.county;
            address.TEL = request.telephone;
            address.MOBILE = request.mobile;
            address.EMAIL = request.email;
            address.ISDEFAULT = request.isDefault;
            address.ADDRESSTYPE = request.addressTypeId;
            address.CONTACTFIRSTNAME = request.contactName;

            if (request.addressId > 0)
            {
                base.Update(address, address.ADDRESSID);
            }
            else
            {
                base.Add(address);
            }
            return address;
        }
        #endregion


        #region insert forwarding address
        public C_ADDRESS AddForwardingddress(ForwardingAddress customerAddress, int customerId)//CustomerDetailDataModel customerModel)
        {
            C_ADDRESS address = new C_ADDRESS();
            C_ADDRESSTYPE addresstype = new C_ADDRESSTYPE();
            var query = DbContext.C_ADDRESSTYPE.Where(x => x.DESCRIPTION.Equals("Forwarding Address"));
            addresstype = query.First();
            address.CUSTOMERID = customerId;
            address.HOUSENUMBER = customerAddress.Number;
            address.ADDRESS1 = customerAddress.Address1;
            address.ADDRESS2 = customerAddress.Address2;
            address.ADDRESS3 = customerAddress.Address3;
            address.TOWNCITY = customerAddress.TownCity;
            address.POSTCODE = customerAddress.PostCode;
            address.COUNTY = customerAddress.County;
            address.TEL = customerAddress.TelePhoneH;
            address.TELRELATIONSHIP = customerAddress.AlternateContact;
            address.TELRELATIVE = customerAddress.TelePhoneC;
            address.MOBILE = customerAddress.Mobile1;
            address.TELWORK = customerAddress.TelePhoneW;
            address.MOBILE2 = customerAddress.Mobile2;
            address.EMAIL = customerAddress.Email;
            address.ISDEFAULT = null;
            address.ADDRESSTYPE = addresstype.ADDRESSTYPEID;
                       
            base.Add(address);
            
            return address;
        }
        #endregion
    }
}
