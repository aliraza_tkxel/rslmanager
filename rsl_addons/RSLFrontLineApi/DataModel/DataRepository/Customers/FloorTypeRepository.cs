﻿using DataModel.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DataRepository
{
    public class FloorTypeRepository : BaseRepository<FLS_FloorType>
    {
        public FloorTypeRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {
        }
    }
}
