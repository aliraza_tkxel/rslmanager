﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using DataModel.DatabaseEntities;
using BusinessModels;
using Utilities;

namespace DataModel.DataRepository
{
    public class CustomerRepository : BaseRepository<C__CUSTOMER>
    {
        public CustomerRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {

        }

        #region Search a Customer
        public List<CustomerDataModel> SearchCustomer(string searchText, ref PaginationDataModel pagination)
        {
            List<CustomerDataModel> customerList = new List<CustomerDataModel>();

            var query = (from cus in DbContext.C__CUSTOMER
                         join gtitle in DbContext.G_TITLE on cus.TITLE equals gtitle.TITLEID into tempgti
                         from gti in tempgti.DefaultIfEmpty()
                         join cad in DbContext.C_ADDRESS on cus.CUSTOMERID equals cad.CUSTOMERID into tempAdd
                         from cusAdd in tempAdd.Where(ad => ad.ISDEFAULT == 1 && ad.ADDRESSTYPE == 3).DefaultIfEmpty()
                         join cut in DbContext.C_CUSTOMERTENANCY on cus.CUSTOMERID equals cut.CUSTOMERID into tempCut
                         from cusTen in tempCut.Where(tenant => tenant.ENDDATE == null).DefaultIfEmpty()
                         join tenan in DbContext.C_TENANCY on cusTen.TENANCYID equals tenan.TENANCYID into tempTenan
                         from ten in tempTenan.DefaultIfEmpty()
                         join pro in DbContext.P__PROPERTY on ten.PROPERTYID equals pro.PROPERTYID into tempPro
                         from prop in tempPro.DefaultIfEmpty()
                         join cusType in DbContext.C_CUSTOMERTYPE on cus.CUSTOMERTYPE equals cusType.CUSTOMERTYPEID
                         where 1 == 1 && (cusType.CUSTOMERTYPEID == 1 || cusType.CUSTOMERTYPEID == 2 || cusType.CUSTOMERTYPEID == 3)
                         && (searchText == string.Empty
                         || cus.CUSTOMERID.ToString().Contains(searchText)
                         || cus.FIRSTNAME.Contains(searchText)
                         || cus.MIDDLENAME.Contains(searchText)
                         || cus.LASTNAME.Contains(searchText)
                         || string.Concat(cus.FIRSTNAME, " ", cus.LASTNAME).Contains(searchText)
                         )
                         orderby cus.FIRSTNAME, cusAdd.ADDRESS1
                         select new CustomerDataModel
                         {
                             customerId = cus.CUSTOMERID,
                             customerTypeId = cus.CUSTOMERTYPE,
                             dob = cus.DOB,
                             firstName = cus.FIRSTNAME,
                             lastName = cus.LASTNAME,
                             gender = cus.GENDER,
                             profilePicture = cus.ProfilePicture,
                             tenancyId = cusTen.TENANCYID
                             //customerImage = new CustomerImageDataModel
                             //{
                             //    imageName = cus.ProfilePicture
                             //}                                                      
                         }



                         );
            if (query.Count() > 0)
            {


                //start of pagination                
                int totalPages = query.Count();
                pagination.totalRows = totalPages;
                pagination.totalPages = (int)Math.Ceiling((double)totalPages / pagination.pageSize);
                query = query.Skip((pagination.pageNumber - 1) * pagination.pageSize).Take(pagination.pageSize);
                //End of pagination
                customerList = query.ToList();
                foreach (var item in customerList)
                {
                    item.customerAddress = GetAddressList(item.customerId);
                    if (item.profilePicture != null)
                    {
                        item.customerImage = new CustomerImageDataModel();
                        item.customerImage.imageName = item.profilePicture;
                    }

                    if (item.tenancyId > 0)
                    {
                        item.tenancyCount = DbContext.C_CUSTOMERTENANCY.Where(x => x.TENANCYID == item.tenancyId && x.ENDDATE == null).Count();
                    }
                    else
                    {
                        item.tenancyCount = 0;
                    }
                }
            }


            return customerList;
        }

        #endregion

        #region Upload Profile Pic
        public bool UploadProfilePic(int customerId, string fileName)
        {
            bool result = false;
            var query = DbContext.C__CUSTOMER.Where(x => x.CUSTOMERID == customerId);
            if (query.Count() > 0)
            {
                C__CUSTOMER customer = query.First();
                customer.ProfilePicture = fileName;
                DbContext.SaveChanges();

            }
            return result;
        }

        #endregion

        #region Add/Edit a Customer
        public C__CUSTOMER AddEditCustomer(CustomerDetailDataModel customerModel)
        {
            C__CUSTOMER customer = new C__CUSTOMER();
            if (customerModel.customerId > 0)
            {
                var query = DbContext.C__CUSTOMER.Where(x => x.CUSTOMERID == customerModel.customerId);
                if (query.Count() > 0)
                {
                    customer = query.First();

                }
            }
            customer.TITLE = customerModel.titleId;
            if (customerModel.titleId == 6)
                customer.TITLEOTHER = customerModel.titleOther;
            customer.FIRSTNAME = customerModel.firstName;
            customer.MIDDLENAME = customerModel.middleName;
            customer.LASTNAME = customerModel.lastName;
            customer.CUSTOMERTYPE = customerModel.customerTypeId;
            customer.GENDER = customerModel.gender;
            customer.DOB = customerModel.dob.HasValue ? customerModel.dob.Value.Date : customerModel.dob;
            customer.MARITALSTATUS = customerModel.maritalStatusId;
            customer.ETHNICORIGIN = customerModel.ethnicityId;
            customer.NINUMBER = customerModel.niNumber;
            customer.RELIGION = customerModel.religionId;
            customer.SEXUALORIENTATION = customerModel.sexualOrientationId;
            customer.SEXUALORIENTATIONOTHER = customerModel.sexualOrientationOther;
            customer.FIRSTLANGUAGE = customerModel.firstLanguage;
            customer.COMMUNICATION = customerModel.communication;
            customer.COMMUNICATIONOTHER = customerModel.communicationOther;
            customer.PREFEREDCONTACT = customerModel.preferedContactId != null ? customerModel.preferedContactId.ToString() : null;
            customer.INTERNETACCESS = customerModel.internetAccess;
            customer.NATIONALITY = customerModel.nationalityId;
            customer.PermanentUKResidency = customerModel.isPermanentUKResident;
            customer.SubjectToImmigration = customerModel.isSubjectToImmigration;
            if (customerModel.customerGeneralInfo != null)
            {
                customer.BENEFIT = customerModel.customerGeneralInfo.benefits;
                customer.BENEFITOTHER = customerModel.customerGeneralInfo.benefitOther;
                customer.BANKFACILITY = customerModel.customerGeneralInfo.bankingFacilities;
                customer.BANKFACILITYOTHER = customerModel.customerGeneralInfo.bankingFacilityOthers;
                customer.DISABILITY = customerModel.customerGeneralInfo.healthProblems;
                customer.DISABILITYOTHER = customerModel.customerGeneralInfo.healthProblemOthers;
                customer.CARER = customerModel.customerGeneralInfo.liveInCarer;
                customer.WHEELCHAIR = customerModel.customerGeneralInfo.wheelChair;
                customer.SUPPORTAGENCIES = customerModel.customerGeneralInfo.supportFromAgencies;
                customer.SUPPORTAGENCIESOTHER = customerModel.customerGeneralInfo.supportFromAgenciesOthers;
                customer.AidAdaptations = customerModel.customerGeneralInfo.aidsAndAdaptation;
            }
            if (customerModel.employmentInfo != null)
            {
                customer.EMPLOYMENTSTATUS = customerModel.employmentInfo.employmentStatusId;
                customer.OCCUPATION = customerModel.employmentInfo.occupation;
                customer.EMPLOYERNAME = customerModel.employmentInfo.employerName;
                customer.EMPADDRESS1 = customerModel.employmentInfo.employerAddress;
            }
            if (customerModel.customerImage != null)
            {
                customer.ProfilePicture = customerModel.customerImage.imageName;
            }
            customer.LASTACTIONUSER = customerModel.createdBy;

            customer.AppVersion = customerModel.appVersion;
            customer.CreatedOnApp = customerModel.createdOnApp;
            customer.LastModifiedOnApp = customerModel.lastModifiedOnApp;


            if (customerModel.customerId > 0)
            {
                customer.LastModifiedOnServer = DateTime.Now;
                base.Update(customer, customer.CUSTOMERID);
            }
            else
            {
                customer.CreatedOnServer = DateTime.Now;
                base.Add(customer);

            }
            return customer;
        }

        #endregion

        #region Get a Customer Detail
        public CustomerDetailDataModel GetCustomerDetail(int customerId)
        {
            CustomerDetailDataModel customer = new CustomerDetailDataModel();

            var query = (from cus in DbContext.C__CUSTOMER
                         join gtitle in DbContext.G_TITLE on cus.TITLE equals gtitle.TITLEID into tempgti
                         from gti in tempgti.DefaultIfEmpty()

                         join cad in DbContext.C_ADDRESS on cus.CUSTOMERID equals cad.CUSTOMERID
                         into tempAdd
                         from cusAdd in tempAdd.DefaultIfEmpty()

                         join custTen in DbContext.C_CUSTOMERTENANCY.Where(x => x.CUSTOMERID == customerId).OrderByDescending(y => y.CUSTOMERTENANCYID).Take(1) on cus.CUSTOMERID equals custTen.CUSTOMERID
                         into group1
                         from custTenancy in group1.DefaultIfEmpty()

                         join ten in DbContext.C_TENANCY on custTenancy.TENANCYID equals ten.TENANCYID
                         into group2
                         from tenancy in group2.DefaultIfEmpty()

                         join tenantAddress in DbContext.P__PROPERTY on tenancy.PROPERTYID equals tenantAddress.PROPERTYID
                         into group3
                         from tenadd in group3.DefaultIfEmpty()

                         where cus.CUSTOMERID == customerId
                         select new CustomerDetailDataModel
                         {
                             customerId = cus.CUSTOMERID,
                             titleId = cus.TITLE,
                             titleOther = cus.TITLEOTHER,
                             firstName = cus.FIRSTNAME,
                             middleName = cus.MIDDLENAME,
                             lastName = cus.LASTNAME,
                             customerTypeId = cus.CUSTOMERTYPE,
                             gender = cus.GENDER,
                             dob = cus.DOB,
                             maritalStatusId = cus.MARITALSTATUS,
                             ethnicityId = cus.ETHNICORIGIN,
                             niNumber = cus.NINUMBER,
                             religionId = cus.RELIGION,
                             sexualOrientationId = cus.SEXUALORIENTATION,
                             sexualOrientationOther = cus.SEXUALORIENTATIONOTHER,
                             firstLanguage = cus.FIRSTLANGUAGE,
                             communication = cus.COMMUNICATION,
                             communicationOther = cus.COMMUNICATIONOTHER,
                             nationalityId = cus.NATIONALITY,
                             internetAccess = cus.INTERNETACCESS,
                             appVersion = cus.AppVersion,
                             createdOnServer = cus.CreatedOnServer,
                             lastModifiedOnServer = cus.LastModifiedOnServer,
                             prefContact = cus.PREFEREDCONTACT.ToString(),
                             profilePicture = cus.ProfilePicture,
                             adults = cus.OCCUPANTSADULTS,
                             children = cus.OCCUPANTSCHILDREN,
                             customerGeneralInfo = new CustomerGeneralInfoDataModel
                             {
                                 benefits = cus.BENEFIT,
                                 benefitOther = cus.BENEFITOTHER,
                                 aidsAndAdaptation = cus.AidAdaptations,
                                 bankingFacilities = cus.BANKFACILITY,
                                 bankingFacilityOthers = cus.BANKFACILITYOTHER,
                                 healthProblemOthers = cus.DISABILITYOTHER,
                                 healthProblems = cus.DISABILITY,
                                 liveInCarer = cus.CARER,
                                 supportFromAgencies = cus.SUPPORTAGENCIES,
                                 supportFromAgenciesOthers = cus.SUPPORTAGENCIESOTHER,
                                 wheelChair = cus.WHEELCHAIR
                             },
                             isSubjectToImmigration = cus.SubjectToImmigration,
                             isPermanentUKResident = cus.PermanentUKResidency,
                             createdBy = cus.LASTACTIONUSER,
                             lastModifiedOnApp = cus.LastModifiedOnApp,
                             createdOnApp = cus.CreatedOnApp,
                             tenancy = new TenancyDataModel
                             {
                                 tenancyStartDate = custTenancy.STARTDATE,
                                 tenancyEndDate = custTenancy.ENDDATE,
                                 customerId = custTenancy.CUSTOMERID,
                                 tenancyId = custTenancy.TENANCYID,
                                 propertyId = tenancy.PROPERTYID,
                                 tenancyAddress = new TenancyAddress { houseNumber = tenadd.HOUSENUMBER, address1 = tenadd.ADDRESS1, address2 = tenadd.ADDRESS2, address3 = tenadd.ADDRESS3, city = tenadd.TOWNCITY, county = tenadd.COUNTY, postCode = tenadd.POSTCODE },
                             },
                             employmentInfo = new EmploymentInfoDataModel
                             {
                                 employerAddress = string.Concat(cus.EMPADDRESS1, " ", cus.EMPADDRESS2),
                                 employerName = cus.EMPLOYERNAME,
                                 employmentStatusId = cus.EMPLOYMENTSTATUS,
                                 occupation = cus.OCCUPATION,
                                 takeHomePay = cus.TAKEHOMEPAY,
                             }


                         });

            if (query.Count() > 0)
            {
                customer = query.FirstOrDefault();
                if (customer.prefContact != null && customer.prefContact != "")
                {
                    customer.preferedContactId = int.Parse(customer.prefContact);
                }
                else
                {
                    customer.preferedContactId = null;
                }
                if (customer.profilePicture != null)
                {
                    customer.customerImage = new CustomerImageDataModel();
                    customer.customerImage.imageName = customer.profilePicture;

                }
            }
            if (customer.tenancy.tenancyId != null)
            {
                var occupants = GetOccupantList((int)customer.tenancy.tenancyId);
                customer.adults = 0;
                customer.children = 0;
                var currentDate = DateTime.Today;
                foreach (var occupant in occupants)
                {
                    var occCustomer = DbContext.C__CUSTOMER.Where(c => c.CUSTOMERID == occupant.customerId).FirstOrDefault();
                    if (occCustomer.DOB != null)
                    {
                        int age = currentDate.Year - occCustomer.DOB.Value.Year;
                        if (occCustomer.DOB > currentDate.AddYears(-age))
                            age--;
                        if (age >= 18)
                        {
                            customer.adults++;
                        }
                        else
                        {
                            customer.children++;
                        }
                    }
                }
            }
            //var occupant=GetOccupantList()
            customer.currentHome = GetCurrentHome(customerId);
            customer.customerAddress = GetAddressList(customerId);
            customer.tenancyCount = DbContext.C_CUSTOMERTENANCY.Where(x => x.CUSTOMERID == customerId && x.ENDDATE == null).Count();
            customer.customerNumber = "CN" + customer.customerId.ToString().PadLeft(8, '0');
            return customer;
        }
        #endregion

        #region Get Current home info
        public CustomerCurrentHomeDataModel GetCurrentHome(int customerId)
        {
            var curHomeData = (from curHome in DbContext.FLS_CustomerCurrentHome
                               where curHome.CustomerId == customerId
                               orderby curHome.CurrentHomeId descending
                               select new CustomerCurrentHomeDataModel
                               {
                                   currentHomeId = curHome.CurrentHomeId,
                                   floor = curHome.floor,
                                   customerId = curHome.CustomerId,
                                   houseTypeId = curHome.HouseTypeId,
                                   landlordName = curHome.LandlordName,
                                   landlordTypeId = curHome.LandlordTypeId,
                                   livingWithFamilyId = curHome.LivingWithFamilyId,
                                   noOfBedrooms = curHome.NoOfBedrooms,
                                   previousLandLordName = curHome.PreviousLandLordName,
                                   tenancyEndDate = curHome.TenancyEndDate,
                                   tenancyStartDate = curHome.TenancyStartDate
                               }
                       ).FirstOrDefault();
            return curHomeData;
        }
        #endregion

        #region Get customer address list
        public List<CustomerAddressDataModel> GetAddressList(int customerId)
        {
            var data = (from cusAdd in DbContext.C_ADDRESS
                        where cusAdd.CUSTOMERID == customerId
                        select new CustomerAddressDataModel
                        {
                            houseNumber = cusAdd.HOUSENUMBER,
                            addressId = cusAdd.ADDRESSID,
                            address1 = cusAdd.ADDRESS1,
                            address2 = cusAdd.ADDRESS2,
                            address3 = cusAdd.ADDRESS3,
                            addressTypeId = cusAdd.ADDRESSTYPE,
                            city = cusAdd.TOWNCITY,
                            county = cusAdd.COUNTY,
                            postCode = cusAdd.POSTCODE,
                            telephone = cusAdd.TEL,
                            mobile = cusAdd.MOBILE,
                            email = cusAdd.EMAIL,
                            isDefault = cusAdd.ISDEFAULT,
                            contactName = string.Concat(cusAdd.CONTACTFIRSTNAME, " ", cusAdd.CONTACTSURNAME)
                        }
                            );
            return data.ToList();
        }
        #endregion

        #region Get customer viewings
        public List<CustomerViewingResponseModel> GetCustomerViewing(CustomerViewingRequestModel request)
        {
            var data = (from viewing in DbContext.FLS_Viewings
                        join prop in DbContext.P__PROPERTY on viewing.PropertyId equals prop.PROPERTYID
                        join cust in DbContext.C__CUSTOMER on viewing.CustomerId equals cust.CUSTOMERID
                        join emp in DbContext.E__EMPLOYEE on viewing.HousingOfficerId equals emp.EMPLOYEEID
                        where cust.CUSTOMERID == request.customerId
                        && viewing.ViewingDate >= request.startDate && viewing.ViewingDate <= request.endDate
                        select new CustomerViewingResponseModel
                        {
                            viewingId = viewing.ViewingId,
                            housingOfficerId = emp.EMPLOYEEID,
                            housingOfficerName = string.Concat(emp.FIRSTNAME, " ", emp.LASTNAME),
                            propertyId = prop.PROPERTYID,
                            viewingDateTime = viewing.ViewingDate,
                            propertyAddress = string.Concat(prop.HOUSENUMBER, " ", prop.ADDRESS1, " ", prop.ADDRESS2, " ", prop.ADDRESS3, " ", prop.TOWNCITY, " ", prop.POSTCODE),
                            viewingStatusId = viewing.viewingStatusId,
                            viewingTime = viewing.ViewingTime
                        });
            return data.ToList();
        }

        #endregion

        #region Get a customer's referral list
        //public List<CustomerReferralResponseModel> GetReferralList(int customerId)
        //{
        //    var referralDataList = (from jrnl in DbContext.C_JOURNAL 
        //                        join i in DbContext.C_ITEM on jrnl.ITEMID equals i.ITEMID
        //                        into group1
        //                        from itm in group1.DefaultIfEmpty()

        //                        join st in DbContext.C_STATUS on jrnl.CURRENTITEMSTATUSID equals st.ITEMSTATUSID
        //                        into group2
        //                        from sts in group2.DefaultIfEmpty()

        //                        join nt in DbContext.C_NATURE on jrnl.ITEMNATUREID equals nt.ITEMNATUREID
        //                        into group3
        //                        from nature in group3.DefaultIfEmpty()
        //                        where jrnl.CUSTOMERID == customerId
        //                        && string.Compare(itm.DESCRIPTION,ApplicationConstants.ItemTypeTenant,true)==0 
        //                        && string.Compare(nature.DESCRIPTION,ApplicationConstants.NatureTypeTenantSupportReferral,true)==0
        //                        select new CustomerReferralResponseModel
        //                        {
        //                            creationDate= jrnl.CREATIONDATE,
        //                            item = itm.DESCRIPTION,
        //                            nature = nature.DESCRIPTION,
        //                            journal = jrnl.TITLE,
        //                            status = sts.DESCRIPTION
        //                        }                               
        //                        );
        //    return referralDataList.ToList();
        //}

        #endregion

        #region Get a referral details 
        public CustomerReferralsDataModel GetReferralDetails(int? customerId, int referralHistoryId = 0)
        {
            var referralDatails = (from referral in DbContext.C_REFERRAL
                                   join jrnl in DbContext.C_JOURNAL on referral.JOURNALID equals jrnl.JOURNALID
                                   join nature in DbContext.C_NATURE on jrnl.ITEMNATUREID equals nature.ITEMNATUREID
                                   join sts in DbContext.C_STATUS on referral.ITEMSTATUSID equals sts.ITEMSTATUSID
                                   join emp in DbContext.E__EMPLOYEE on referral.LASTACTIONUSER equals emp.EMPLOYEEID
                                   into group2
                                   from employee in group2.DefaultIfEmpty()
                                   join cust in DbContext.C__CUSTOMER on jrnl.CUSTOMERID equals cust.CUSTOMERID
                                   where cust.CUSTOMERID == customerId
                                   orderby referral.REFERRALHISTORYID descending
                                   select new CustomerReferralsDataModel
                                   {
                                       isConviction = referral.CONVICTION,
                                       isSelfHarm = referral.SELFHARM,
                                       isSubstanceMisuse = referral.SUBSTANCEMISUSE,
                                       isTypeArrears = referral.isTypeArrears,
                                       isTypeAsb = referral.isTypeASB,
                                       isTypeDamage = referral.isTypeDamage,
                                       journalId = jrnl.JOURNALID,
                                       title = jrnl.TITLE,
                                       miscNotes = referral.MISCNOTES,
                                       neglectNotes = referral.NEGLECTNOTES,
                                       otherNotes = referral.OTHERNOTES,
                                       yesNotes = referral.NOTES,
                                       referrerName = string.Concat(employee.FIRSTNAME == null ? "" : employee.FIRSTNAME, " ", employee.LASTNAME == null ? "" : employee.LASTNAME),
                                       safetyIssueId = referral.SAFETYISSUES,
                                       stageId = referral.STAGEID,
                                       referralHistoryId = referral.REFERRALHISTORYID,
                                       customerId = cust.CUSTOMERID,
                                       isTenantAware = referral.ISTENANTAWARE,
                                       appVersion = referral.AppVersion,
                                       createdOnApp = referral.CreatedOnApp,
                                       createdOnServer = referral.CreatedOnServer,
                                       lastModifiedOnApp = referral.LastModifiedOnApp,
                                       lastModifiedOnServer = referral.LastModifiedOnServer,
                                       lastActionUserId = referral.LASTACTIONUSER

                                   }
                                   ).FirstOrDefault();

            List<byte> lstCustomerHelpSubCategories = new List<byte>();
            string customerHelpSubCategories = "";
            if (referralDatails != null)
            {
                lstCustomerHelpSubCategories = GetHelpSubCategory(referralDatails.journalId);
                foreach (var helpCat in lstCustomerHelpSubCategories)
                {
                    customerHelpSubCategories = string.Concat(customerHelpSubCategories, helpCat.ToString(), ",");
                }
                referralDatails.customerHelpSubCategories = customerHelpSubCategories;
            }

            return referralDatails;
        }
        #endregion

        #region Get help Sub Category list
        public List<byte> GetHelpSubCategory(int journalId)
        {
            var data = (from custRef in DbContext.C_REFERRAL_CUSTOMER_HELPSUBCATEGORY
                        where custRef.JOURNALID == journalId
                        select custRef.SUBCATEGORYID);
            return data.ToList();
        }
        #endregion

        #region Get tenancy setup info
        public TenancyInfoResponseModel GetTenancyInfo(int customerId)//remove it.
        {
            //start fetching address typeId of contact  type ICE 
            var adrsTypeId = (from at in DbContext.C_ADDRESSTYPE
                              where string.Compare(at.DESCRIPTION, ApplicationConstants.AddressTypeICE, true) == 0
                              select at.ADDRESSTYPEID).FirstOrDefault();
            //end fetching address typeId of contact  type ICE 

            var tenancyInfoData = (from cust in DbContext.C__CUSTOMER
                                   join ad in DbContext.C_ADDRESS on cust.CUSTOMERID equals ad.CUSTOMERID
                                   into group1
                                   from adrs in group1.Where(x => x.ADDRESSTYPE == (Int32)adrsTypeId).DefaultIfEmpty()

                                   join at in DbContext.C_ADDRESSTYPE on adrs.ADDRESSTYPE equals at.ADDRESSTYPEID
                                   into group2
                                   from adrsType in group2.DefaultIfEmpty()
                                   where cust.CUSTOMERID == customerId
                                   select new TenancyInfoResponseModel
                                   {
                                       employmentStatusId = cust.EMPLOYMENTSTATUS,
                                       occupation = cust.OCCUPATION,
                                       employerName = cust.EMPLOYERNAME,
                                       employerAddress = cust.EMPADDRESS1,
                                       benefitId = cust.BENEFIT,
                                       bankingId = cust.BANKFACILITY,
                                       bankingOthers = cust.BANKFACILITYOTHER,
                                       healthProblemId = cust.DISABILITY,
                                       healthProblemOthers = cust.DISABILITYOTHER,
                                       supportAgencyId = cust.SUPPORTAGENCIES,
                                       supportAgencyOthers = cust.SUPPORTAGENCIESOTHER,
                                       aidAndAdaptationId = cust.AidAdaptations,
                                       customerId = cust.CUSTOMERID,
                                       wheelChair = cust.WHEELCHAIR,
                                       addressId = adrs.ADDRESSID,
                                       addressTypeId = adrs.ADDRESSTYPE,
                                       address = adrs.ADDRESS1,
                                       contactName = adrs.CONTACTFIRSTNAME,
                                       email = adrs.EMAIL,
                                       mobile = adrs.MOBILE,
                                       telephone = adrs.TEL,
                                       carer = cust.CARER,
                                   }
                        );
            return tenancyInfoData.FirstOrDefault();
        }
        #endregion

        #region Terminate customer tenancy
        public TenancyTerminationDataModel TerminateTenancy(TenancyTerminationDataModel request, C_JOURNAL journal)
        {
            //start insertion in related store procedures
            int mutual = 0;
            var mutualExchange = DbContext.C_TERMINATION_REASON.Where(x => 
                string.Compare(x.DESCRIPTION, ApplicationConstants.MutualExchangeBHA, true) == 0 || 
                string.Compare(x.DESCRIPTION, ApplicationConstants.MutualExchangeLA, true) == 0 || 
                string.Compare(x.DESCRIPTION, ApplicationConstants.MutualExchangeRP, true) == 0).Select(x => x.REASONID);

            foreach (var me in mutualExchange)
            {
                if (request.reasonId == me)
                {
                    mutual = 1;
                    break;
                }
            }
            DbContext.F_TENANCY_END_PROCESS(journal.TENANCYID, request.terminatedBy, request.terminationDate.ToString(), "", journal.PROPERTYID, journal.JOURNALID, mutual);
            ObjectParameter pdrJournalId = new ObjectParameter("JournalID", typeof(int));
            
            // SEE IF THIS TERMINATION IS AS A RESULT OF MUTUAL EXCHAMGE
            if (mutual == 0)
            {
                // CALL STORED PROCEDURE TO store value in PDR_MSAT AND PDR_Journal
                DbContext.FLS_SaveTenancyTermination(journal.PROPERTYID, request.customerId, journal.TENANCYID, request.terminationDate, request.terminatedBy, 1, pdrJournalId);
            }
            else
            {
                pdrJournalId.Value = DBNull.Value;
            }
            //end insertion in related store procedures
            return GetTerminationInfo(journal.JOURNALID, pdrJournalId.Value == DBNull.Value ? 0 : (int)pdrJournalId.Value);
        }
        #endregion

        #region Create Termination Response
        public TenancyTerminationDataModel GetTerminationInfo(int journalId, int pdrJournalId)
        {
            var tenancyData = (from termination in DbContext.C_TERMINATION
                               join jrnl in DbContext.C_JOURNAL on termination.JOURNALID equals jrnl.JOURNALID
                               where termination.JOURNALID == journalId
                               select new TenancyTerminationDataModel
                               {
                                   journalId = pdrJournalId,
                                   notes = termination.NOTES,
                                   reasonCategoryId = termination.REASONCATEGORYID,
                                   reasonId = termination.REASON,
                                   terminatedBy = termination.LASTACTIONUSER,
                                   terminationDate = termination.TERMINATIONDATE,
                                   terminationHistoryId = termination.TERMINATIONHISTORYID,
                                   AppVersion = termination.AppVersion,
                                   customerId = jrnl.CUSTOMERID
                               }).FirstOrDefault();
            return tenancyData;
        }
        #endregion

        #region Update customer info while creating tenancy
        public C__CUSTOMER UpdateCustomerInfoInTenancyCreation(CustomerGeneralInfoDataModel customerGeneralInfo, EmploymentInfoDataModel employmentInfo, int customerId)
        {
            C__CUSTOMER customer = new C__CUSTOMER();
            var customerTypeTenant = DbContext.C_CUSTOMERTYPE.Where(x => string.Compare(x.DESCRIPTION, ApplicationConstants.CustomerTypeTenant, true) == 0).Select(y => y.CUSTOMERTYPEID).FirstOrDefault();

            if (customerId > 0)
            {
                var query = DbContext.C__CUSTOMER.Where(x => x.CUSTOMERID == customerId);
                if (query.Count() > 0)
                {
                    customer = query.First();

                }
            }
            //start updating employment info
            customer.EMPLOYMENTSTATUS = employmentInfo.employmentStatusId;
            customer.OCCUPATION = employmentInfo.occupation;
            customer.EMPLOYERNAME = employmentInfo.employerName;
            customer.EMPADDRESS1 = employmentInfo.employerAddress;
            //end updating employment info

            //start updating customer general info
            customer.BENEFIT = customerGeneralInfo.benefits;
            customer.BANKFACILITY = customerGeneralInfo.bankingFacilities;
            customer.BANKFACILITYOTHER = customerGeneralInfo.bankingFacilityOthers;
            customer.DISABILITY = customerGeneralInfo.healthProblems;
            customer.DISABILITYOTHER = customerGeneralInfo.healthProblemOthers;
            customer.CARER = customerGeneralInfo.liveInCarer;
            customer.WHEELCHAIR = customerGeneralInfo.wheelChair;
            customer.SUPPORTAGENCIES = customerGeneralInfo.supportFromAgencies;
            customer.SUPPORTAGENCIESOTHER = customerGeneralInfo.supportFromAgenciesOthers;
            customer.AidAdaptations = customerGeneralInfo.aidsAndAdaptation;
            customer.CUSTOMERTYPE = customerTypeTenant;
            //end updating customer general info

            base.Update(customer, customerId);

            return customer;
        }
        #endregion

        #region Update customer info while creating tenancy
        public C__CUSTOMER UpdateCustomerTenancyType(int customerId)
        {
            C__CUSTOMER customer = new C__CUSTOMER();
            var customerTypeTenant = DbContext.C_CUSTOMERTYPE.Where(x => string.Compare(x.DESCRIPTION, ApplicationConstants.CustomerTypeTenant, true) == 0).Select(y => y.CUSTOMERTYPEID).FirstOrDefault();

            if (customerId > 0)
            {
                var query = DbContext.C__CUSTOMER.Where(x => x.CUSTOMERID == customerId);
                if (query.Count() > 0)
                {
                    customer = query.First();

                }
            }
            customer.CUSTOMERTYPE = customerTypeTenant;
            //end updating customer general info

            base.Update(customer, customerId);

            return customer;
        }
        #endregion

        #region Get Tenancy Info against customerId
        public TenancySetupDataModel GetTenancyInfo(TenancySetupDataModel request)
        {
            TenancySetupDataModel response = new TenancySetupDataModel();
            var IceTypeId = DbContext.C_ADDRESSTYPE.Where(x => string.Compare(x.DESCRIPTION, ApplicationConstants.AddressTypeICE, true) == 0).Select(y => y.ADDRESSTYPEID).FirstOrDefault();
            var customerData = (from cust in DbContext.C__CUSTOMER

                                join adrs in DbContext.C_ADDRESS.Where(x => x.ADDRESSTYPE == IceTypeId) on cust.CUSTOMERID equals adrs.CUSTOMERID
                                into group1
                                from address in group1.DefaultIfEmpty()

                                join custTen in DbContext.C_CUSTOMERTENANCY.Where(x => x.ENDDATE == null) on cust.CUSTOMERID equals custTen.CUSTOMERID
                                into group2
                                from custTenancy in group2.DefaultIfEmpty()

                                join ten in DbContext.C_TENANCY.Where(x => x.ENDDATE == null) on custTenancy.TENANCYID equals ten.TENANCYID
                                into group3
                                from tenancy in group3.DefaultIfEmpty()

                                join fin in DbContext.P_FINANCIAL on tenancy.PROPERTYID equals fin.PROPERTYID
                                into group4
                                from finInfo in group4.DefaultIfEmpty()

                                where cust.CUSTOMERID == request.customerId

                                select new TenancySetupDataModel
                                {
                                    customerId = cust.CUSTOMERID,
                                    tenancyStartDate = custTenancy.STARTDATE,
                                    tenancyId = custTenancy.TENANCYID,
                                    propertyId = tenancy.PROPERTYID,

                                    employmentInfo = new EmploymentInfoDataModel
                                    {
                                        employerAddress = cust.EMPADDRESS1,
                                        employerName = cust.EMPLOYERNAME,
                                        employmentStatusId = cust.EMPLOYMENTSTATUS,
                                        occupation = cust.OCCUPATION
                                    },
                                    customerGeneralInfo = new CustomerGeneralInfoDataModel
                                    {
                                        aidsAndAdaptation = cust.AidAdaptations,
                                        bankingFacilities = cust.BANKFACILITY,
                                        bankingFacilityOthers = cust.BANKFACILITYOTHER,
                                        benefits = cust.BENEFIT,
                                        benefitOther = cust.BENEFITOTHER,
                                        healthProblems = cust.DISABILITY,
                                        healthProblemOthers = cust.DISABILITYOTHER,
                                        liveInCarer = cust.CARER,
                                        supportFromAgencies = cust.SUPPORTAGENCIES,
                                        supportFromAgenciesOthers = cust.SUPPORTAGENCIESOTHER,
                                        wheelChair = cust.WHEELCHAIR,
                                    },
                                    emergencyContact = new EmergencyContactDataModel
                                    {
                                        address1 = address.ADDRESS1,
                                        address2 = address.ADDRESS2,
                                        address3 = address.ADDRESS3,
                                        addressId = address.ADDRESSID,
                                        addressTypeId = address.ADDRESSTYPE,
                                        city = address.TOWNCITY,
                                        contactName = address.CONTACTFIRSTNAME,
                                        county = address.COUNTY,
                                        email = address.EMAIL,
                                        houseNumber = address.HOUSENUMBER,
                                        isDefault = address.ISDEFAULT,
                                        mobile = address.MOBILE,
                                        postCode = address.POSTCODE,
                                        telephone = address.TEL
                                    },
                                    propertyRentInfo = new PropertyRentInfoDataModel
                                    {
                                        councilTax = finInfo.COUNCILTAX,
                                        garage = finInfo.GARAGE,
                                        ineligServ = finInfo.INELIGSERV,
                                        rent = finInfo.RENT,
                                        rentPayable = finInfo.TOTALRENT,
                                        services = finInfo.SERVICES,
                                        supportedServices = finInfo.SUPPORTEDSERVICES,
                                        waterRates = finInfo.WATERRATES
                                    }
                                }
                        ).FirstOrDefault();
            customerData.occupants = GetOccupantList(customerData.tenancyId);//get occupants
            if (request.referral != null)//get referral
            {
                customerData.referral = GetReferralDetails(request.referral.customerId);
            }
            if (request.jointTenant != null)//get joint tenants
            {
                customerData.jointTenant = GetJointTenantInfo(customerData.tenancyId);
            }
            if (request.tenancyDisclosure != null)
            {
                customerData.tenancyDisclosure = GetDisclosureInfo(request.customerId);
            }
            if (request.tenantOnlineInfo != null)
            {
                customerData.tenantOnlineInfo = GetTenantOnlineInfo(request.customerId);
            }

            return customerData;
        }
        #endregion

        #region Save customer info while adding occupants
        public C__CUSTOMER AddCustomerInfoForOccupants(OccupantsDataModel requset)
        {
            C__CUSTOMER customer = new C__CUSTOMER();
            if (requset.customerId > 0)
            {
                var query = DbContext.C__CUSTOMER.Where(x => x.CUSTOMERID == requset.customerId);
                if (query.Count() > 0)
                {
                    customer = query.First();

                }
            }
            customer.TITLE = requset.titleId;
            customer.FIRSTNAME = requset.firstName;
            customer.LASTNAME = requset.lastName;
            customer.CUSTOMERTYPE = 6;//hard coded it because on web customer type is 6 and it is not a customerType 
            customer.DOB = requset.dateOfBirth;
            if (requset.customerId > 0)
            {
                base.Update(customer, requset.customerId);
            }
            else
            {
                base.Add(customer);
            }




            return customer;
        }
        #endregion

        #region Get joint tenants info against a tenancyId
        public JointTenantInfoDataModel GetJointTenantInfo(int tenancyId)
        {
            var IceTypeId = DbContext.C_ADDRESSTYPE.Where(x => string.Compare(x.DESCRIPTION, ApplicationConstants.AddressTypeICE, true) == 0).Select(y => y.ADDRESSTYPEID).FirstOrDefault();
            var jointTenantsData = (from cust in DbContext.C__CUSTOMER

                                    join adrs in DbContext.C_ADDRESS.Where(x => x.ADDRESSTYPE == IceTypeId) on cust.CUSTOMERID equals adrs.CUSTOMERID
                                    into group1
                                    from address in group1.DefaultIfEmpty()

                                    join custTen in DbContext.C_CUSTOMERTENANCY.Where(x => x.ENDDATE == null) on cust.CUSTOMERID equals custTen.CUSTOMERID
                                    into group2
                                    from custTenancy in group2.DefaultIfEmpty()
                                    where custTenancy.TENANCYID == tenancyId
                                    orderby custTenancy.CUSTOMERTENANCYID descending
                                    select new JointTenantInfoDataModel
                                    {
                                        customerId = cust.CUSTOMERID,
                                        isCorrespondingAddress = address.ISDEFAULT,
                                        startDate = custTenancy.STARTDATE,
                                        tenancyId = custTenancy.TENANCYID,
                                        employmentInfo = new EmploymentInfoDataModel
                                        {
                                            employerAddress = cust.EMPADDRESS1,
                                            employerName = cust.EMPLOYERNAME,
                                            employmentStatusId = cust.EMPLOYMENTSTATUS,
                                            occupation = cust.OCCUPATION
                                        },
                                        customerGeneralInfo = new CustomerGeneralInfoDataModel
                                        {
                                            aidsAndAdaptation = cust.AidAdaptations,
                                            bankingFacilities = cust.BANKFACILITY,
                                            bankingFacilityOthers = cust.BANKFACILITYOTHER,
                                            benefits = cust.BENEFIT,
                                            benefitOther = cust.BENEFITOTHER,
                                            healthProblems = cust.DISABILITY,
                                            healthProblemOthers = cust.DISABILITYOTHER,
                                            liveInCarer = cust.CARER,
                                            supportFromAgencies = cust.SUPPORTAGENCIES,
                                            supportFromAgenciesOthers = cust.SUPPORTAGENCIESOTHER,
                                            wheelChair = cust.WHEELCHAIR,
                                        },
                                        emergencyContact = new EmergencyContactDataModel
                                        {
                                            address1 = address.ADDRESS1,
                                            address2 = address.ADDRESS2,
                                            address3 = address.ADDRESS3,
                                            addressId = address.ADDRESSID,
                                            addressTypeId = address.ADDRESSTYPE,
                                            city = address.TOWNCITY,
                                            contactName = address.CONTACTFIRSTNAME,
                                            county = address.COUNTY,
                                            email = address.EMAIL,
                                            houseNumber = address.HOUSENUMBER,
                                            isDefault = address.ISDEFAULT,
                                            mobile = address.MOBILE,
                                            postCode = address.POSTCODE,
                                            telephone = address.TEL
                                        },

                                    }

                                    ).FirstOrDefault();
            return jointTenantsData;
        }

        #endregion

        #region Get occupants list against a tenancyId
        public List<OccupantsDataModel> GetOccupantList(int tenancyId)
        {
            var occupantsLst = (from cust in DbContext.C__CUSTOMER
                                join occupant in DbContext.C_OCCUPANT on cust.CUSTOMERID equals occupant.CUSTOMERID
                                where occupant.TENANCYID == tenancyId
                                select new OccupantsDataModel
                                {
                                    customerId = cust.CUSTOMERID,
                                    dateOfBirth = cust.DOB,
                                    disabilities = occupant.DISABILITY,
                                    disabilitiesOthers = occupant.DisabilityOther,
                                    employmentStatusId = occupant.ECONOMICSTATUS,
                                    firstName = cust.FIRSTNAME,
                                    lastName = cust.LASTNAME,
                                    occupantId = occupant.OCCUPANTID,
                                    relationshipStatusId = occupant.RELATIONSHIP,
                                    tenancyId = occupant.TENANCYID,
                                    titleId = cust.TITLE
                                }
                             ).ToList();
            return occupantsLst;
        }
        #endregion

        #region Get disclosure info
        public TenancyDisclosureDataModel GetDisclosureInfo(int customerId)
        {
            var disclosureData = (from disclosure in DbContext.FLS_TenantDisclosure
                                  where disclosure.CustomerId == customerId
                                  select new TenancyDisclosureDataModel
                                  {
                                      disclosureId = disclosure.DisclosureId,
                                      evictedReason = disclosure.Reason,
                                      hasCriminalOffense = disclosure.CriminalOffence,
                                      isEvicted = disclosure.EvictedFromTenancy,
                                      isSubjectToDebt = disclosure.DebtReliefOrder,
                                      isUnsatisfiedCcj = disclosure.UnsatisfiedCCJ,
                                      tenancyId = disclosure.TenancyId
                                  }
                                  ).FirstOrDefault();
            return disclosureData;
        }
        #endregion

        #region Get tenant online info
        public TenantOnlineInfoDataModel GetTenantOnlineInfo(int customerId)
        {
            var tenantOnlineInfo = (from login in DbContext.TO_LOGIN
                                    where login.CustomerID == customerId
                                    select new TenantOnlineInfoDataModel
                                    {
                                        customerId = login.CustomerID,
                                        password = login.Password
                                    }
                                    ).FirstOrDefault();
            return tenantOnlineInfo;
        }

        #endregion       
    }
}
