﻿using BusinessModels;
using DataModel.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DataRepository
{
    public class TenantOnlineInfoRepository : BaseRepository<TO_LOGIN>
    {
        public TenantOnlineInfoRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {
        }

        #region Save tenant online info        
        public TO_LOGIN SaveTenantOnlineInfo(TenantOnlineInfoDataModel request)
        {
            var encodedPassword = Encoding.UTF8.GetBytes(request.password);
            request.password = Convert.ToBase64String(encodedPassword);
            //this will help us to check that customer info already exist or not
            var loginId = DbContext.TO_LOGIN.Where(x => x.CustomerID == request.customerId).Select(y => y.LoginID).FirstOrDefault();
            TO_LOGIN login = new TO_LOGIN();
            if (loginId > 0)
            {
                var query = DbContext.TO_LOGIN.Where(x => x.LoginID == loginId);
                if (query.Count() > 0)
                {
                    login = query.First();

                }
                
            }
            //else
            //{
            //    login.LoginID = DbContext.TO_LOGIN.Max(x => x.LoginID);//Select(y => y.LoginID).Max.FirstOrDefault();
            //}
            
            login.CustomerID = request.customerId;
            login.Password = request.password;
            login.RegisterDate = DateTime.Now;
            login.Active = 1;
            if (loginId > 0)
            {
                base.Update(login,loginId);
            }
            else
            {
                base.Add(login);
            }
            return login;
        }
        #endregion
    }
}
