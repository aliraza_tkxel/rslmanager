﻿using DataModel.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DataRepository
{
    public class SexualOrientationRepository : BaseRepository<G_SEXUALORIENTATION>
    {
        public SexualOrientationRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {

        }
    }
}
