﻿using DataModel.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DataRepository
{
    public class AddressTypeRepository : BaseRepository<C_ADDRESSTYPE>
    {
        public AddressTypeRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {
        }
    }
}
