﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModel.DatabaseEntities;
namespace DataModel.DataRepository
{
    public class SupportAgenciesRepository : BaseRepository<G_SUPPORTAGENCIES>
    {
        public SupportAgenciesRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {
        }
    }
}
