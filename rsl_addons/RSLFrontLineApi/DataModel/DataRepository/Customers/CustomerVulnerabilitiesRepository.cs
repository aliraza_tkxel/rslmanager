﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModel.DatabaseEntities;
using BusinessModels;

namespace DataModel.DataRepository
{
    public class CustomerVulnerabilitiesRepository : BaseRepository<C_Customer_Vulnerability>
    {
        public CustomerVulnerabilitiesRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {
        }

        #region Add/Edit customer-vulnerabilities details
        public void AddEditCustomerVulnerabilities(VulnerabilitiesListingDataModel request)
        {
            C_Customer_Vulnerability custVul = new C_Customer_Vulnerability();
            custVul.VulnerabilityHistoryId = request.vulnerabilityHistoryId;
            custVul.JournalId = request.journalId;
            custVul.CustomerId = request.customerId;
            var cat = DbContext.C_VULNERABILITY_SUBCATEGORY.Where(x => x.SubCategoryId == request.subCategoryId).Select(x => x.CategoryId).FirstOrDefault();
            custVul.CategoryId = cat;
            custVul.SubCategoryId = request.subCategoryId;
            base.Add(custVul);



        }
        #endregion
    }
}
