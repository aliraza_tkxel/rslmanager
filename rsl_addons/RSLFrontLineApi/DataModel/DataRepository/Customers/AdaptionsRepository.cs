﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModel.DatabaseEntities;
namespace DataModel.DataRepository
{
  public  class AdaptionsRepository : BaseRepository<FLS_Adaptations>
    {
        public AdaptionsRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {
        }
    }
}
