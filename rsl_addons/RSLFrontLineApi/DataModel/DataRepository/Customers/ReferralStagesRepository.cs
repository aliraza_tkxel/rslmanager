﻿using DataModel.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DataRepository
{
    public class ReferralStagesRepository : BaseRepository<C_REFERRAL_STAGES>
    {
        public ReferralStagesRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {
        }
    }
}
