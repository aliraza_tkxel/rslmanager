﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModel.DatabaseEntities;
namespace DataModel.DataRepository
{
    public class CustomerHelpSubCategoryRepository : BaseRepository<C_REFERRAL_CUSTOMER_HELPSUBCATEGORY>
    {
        public CustomerHelpSubCategoryRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {
        }
        #region Add/edit help Sub category
        public void AddEditHelpSubCategory(List<byte> subCategoryId, int journalId)
        {
            if (journalId > 0)
            {
                DeleteHelpSubCategory(journalId);
                AddHelpSubCategory(subCategoryId, journalId);
            }
            else
            {
                AddHelpSubCategory(subCategoryId, journalId);
            }
        }
        #endregion

        #region  Add help Sub category
        private void AddHelpSubCategory(List<byte> subCategoryId, int journalId)
        {
            //start inserting data of Help SubCategory                  
            foreach (var item in subCategoryId)
            {
                C_REFERRAL_CUSTOMER_HELPSUBCATEGORY helpSubCategory = new C_REFERRAL_CUSTOMER_HELPSUBCATEGORY();
                helpSubCategory.JOURNALID = journalId;
                helpSubCategory.SUBCATEGORYID = item;
                base.Add(helpSubCategory);
            }
            
            //end inserting data of Help SubCategory           
        }
        #endregion

        #region  delete help Sub category
        private void DeleteHelpSubCategory(int journalId)
        {
            var data = DbContext.C_REFERRAL_CUSTOMER_HELPSUBCATEGORY.Where(x => x.JOURNALID == journalId).ToList();
            foreach (var itm in data)
            {
                base.Delete(itm);
            }
        }
        #endregion

    }
}
