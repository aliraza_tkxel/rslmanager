﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModel.DatabaseEntities;
namespace DataModel.DataRepository
{
    public class NatureRepository : BaseRepository<C_NATURE>
    {
        public NatureRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {
        }
    }
}
