﻿using DataModel.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DataRepository
{
    public class CustomerTitleRepository : BaseRepository<G_TITLE >
    {
        public CustomerTitleRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {

        }
    }
}
