﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModel.DatabaseEntities;
using Utilities;
using BusinessModels;

namespace DataModel.DataRepository
{
    public class JournalRepository : BaseRepository<C_JOURNAL>
    {
        public JournalRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {

        }
        #region Add referrals data in journal        
        public C_JOURNAL AddEditReferralData(CustomerReferralsDataModel request)
        {       
            var tenancyId = (from ten in DbContext.C_TENANCY
                         join custTen in DbContext.C_CUSTOMERTENANCY on ten.TENANCYID equals custTen.TENANCYID
                         where custTen.CUSTOMERID == request.customerId
                         && custTen.ENDDATE == null
                         select ten.TENANCYID
                         ).FirstOrDefault();
            var itemId = DbContext.C_ITEM.Where(x => string.Compare(x.DESCRIPTION, ApplicationConstants.ItemTypeTenant, true) == 0).Select(x => x.ITEMID).FirstOrDefault();
            var itemNatureId = DbContext.C_NATURE.Where(x => string.Compare(x.DESCRIPTION, ApplicationConstants.NatureTypeTenantSupportReferral, true) == 0).Select(x => x.ITEMNATUREID).FirstOrDefault();
            var statusId = DbContext.C_STATUS.Where(x => string.Compare(x.DESCRIPTION, ApplicationConstants.ReferralstatusPending, true) == 0).OrderByDescending(y=>y.ITEMSTATUSID).Select(x => x.ITEMSTATUSID).FirstOrDefault();
            //start inserting data in customer journal
            C_JOURNAL journal = new C_JOURNAL();
            if (request.journalId > 0)
            {
                var query = DbContext.C_JOURNAL.Where(x => x.JOURNALID == request.journalId);
                if (query.Count() > 0)
                {
                    journal = query.First();

                }
            }
            journal.CUSTOMERID = request.customerId;
            journal.TENANCYID = tenancyId;
            journal.ITEMID = itemId;
            journal.ITEMNATUREID = itemNatureId;
            journal.CURRENTITEMSTATUSID = statusId;
            journal.CREATIONDATE = DateTime.Now;
            journal.TITLE = request.title;
            
            if(request.journalId > 0)
            {
                base.Update(journal,journal.JOURNALID);
            }
            else
            {
                base.Add(journal);
            }
                       
            return journal;
            //end inserting data in customer journal
        }
        #endregion

        #region Add termination data in journal
        public C_JOURNAL AddTerminationData(TenancyTerminationDataModel request)
        {
            var tenancyId = DbContext.C_CUSTOMERTENANCY.Where(x => x.CUSTOMERID == request.customerId && x.ENDDATE == null).Select(x => x.TENANCYID).FirstOrDefault();
            var propertyId = DbContext.C_TENANCY.Where(x => x.TENANCYID == tenancyId).Select(x => x.PROPERTYID).FirstOrDefault();
            var itemId = DbContext.C_ITEM.Where(x => string.Compare(x.DESCRIPTION, ApplicationConstants.ItemTypeTenant, true) == 0).Select(x => x.ITEMID).FirstOrDefault();
            var itemNatureId = DbContext.C_NATURE.Where(x => string.Compare(x.DESCRIPTION, ApplicationConstants.NatureTypeTermination, true) == 0).Select(x => x.ITEMNATUREID).FirstOrDefault();
            var statusId = DbContext.C_STATUS.Where(x => string.Compare(x.DESCRIPTION, ApplicationConstants.ReferralstatusPending, true) == 0).Select(x => x.ITEMSTATUSID).FirstOrDefault();

            //start insertion in journal
            C_JOURNAL journal = new C_JOURNAL();
            journal.CUSTOMERID = request.customerId;
            journal.TENANCYID = tenancyId;
            journal.PROPERTYID = propertyId;
            journal.ITEMID = itemId;
            journal.ITEMNATUREID = itemNatureId;
            journal.CURRENTITEMSTATUSID = statusId;
            journal.CREATIONDATE = DateTime.Now;
            journal.TITLE = string.Concat("(", tenancyId, ")");
            base.Add(journal);
            //end insertion in journal
            return journal;
        }
        #endregion

        #region Update risk/vulnerability current status
        public void AddEditCustomerJournal(int journalId,int? currentStatusId)
        {
            C_JOURNAL jrnl = new C_JOURNAL();
            if (journalId > 0)
            {
                var query = DbContext.C_JOURNAL.Where(x => x.JOURNALID == journalId);
                if (query.Count() > 0)
                {
                    jrnl = query.First();
                }
            }
            jrnl.CURRENTITEMSTATUSID = currentStatusId;
            if(journalId > 0)
            {
                base.Update(jrnl,journalId);
            }
            else
            {
                base.Add(jrnl);
            }
            
            
        } 
        #endregion


    }
}
