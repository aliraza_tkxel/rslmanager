﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModel.DatabaseEntities;
namespace DataModel.DataRepository
{
   public class YesNoUnKnownRepository : BaseRepository<G_YESNO_UNKNOWN>
    {
        public YesNoUnKnownRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {

        }
    }
}
