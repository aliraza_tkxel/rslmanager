﻿using BusinessModels;
using DataModel.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities;

namespace DataModel.DataRepository
{
    public class RentJournalRepository : BaseRepository<F_RENTJOURNAL>
    {
        public RentJournalRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {
        }
        #region Save rent journal info
        public F_RENTJOURNAL SaveRentJournalInfo(TenancySetupDataModel request)
        {
            F_RENTJOURNAL rent = new F_RENTJOURNAL();
            var itemType = DbContext.F_ITEMTYPE.Where(x => string.Compare(x.DESCRIPTION, ApplicationConstants.itemTypeInitialRent, true) == 0).Select(y => y.ITEMTYPEID).FirstOrDefault();
            var paymentEndDate = DbContext.Get_weekEnds(request.tenancyStartDate).FirstOrDefault();
           
            var rentFrequency = DbContext.P_FINANCIAL.Where(x => string.Compare(x.PROPERTYID, request.propertyId, true) == 0).Select(y => y.RENTFREQUENCY).FirstOrDefault();
            rent.TENANCYID = request.tenancyId;
            rent.TRANSACTIONDATE = DateTime.Now;
            rent.ITEMTYPE = itemType;
            rent.PAYMENTSTARTDATE = request.tenancyStartDate;
            rent.PAYMENTENDDATE = paymentEndDate;
            if(rentFrequency == 2)
            {
                ObjectParameter forecast =new ObjectParameter("forecast", typeof(double));
                DbContext.F_TENANCY_START_FORECAST_WEEKLY(request.tenancyStartDate, request.propertyId, forecast);
                rent.AMOUNT = decimal.Parse(forecast.Value.ToString());
            }
            else
            {
                ObjectParameter forecast = new ObjectParameter("forecast", typeof(double));
                DbContext.F_TENANCY_START_FORECAST(request.tenancyStartDate, request.propertyId, forecast);                
                rent.AMOUNT = decimal.Parse(forecast.Value.ToString());
                      
                
            }
            rent.STATUSID = 1; // hard coded it, because on web it is also hardcoded.
            //rest of field are not in use on web. so i did same
            base.Add(rent);
            DbContext.SaveChanges();
            DbContext.C_TENANCY_MONITORING();//calling it as on web this SP is called after insertion in F_RENTJOURNAL
            return rent;
        }
        #endregion
    }
}
