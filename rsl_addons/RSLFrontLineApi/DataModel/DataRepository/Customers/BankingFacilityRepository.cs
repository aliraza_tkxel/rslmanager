﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModel.DatabaseEntities;
namespace DataModel.DataRepository
{
   public class BankingFacilityRepository : BaseRepository<g_BankFacility>
    {
        public BankingFacilityRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {
        }
    }
}
