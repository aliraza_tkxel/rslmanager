﻿using DataModel.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DataRepository
{
   public  class EthnicityRepository : BaseRepository<G_ETHNICITY>
    {
        public EthnicityRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {

        }
    }
}
