﻿using DataModel.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DataRepository
{
    public class HouseTypeRepository : BaseRepository<FLS_HouseType>
    {
        public HouseTypeRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {
        }
    }
}
