﻿using DataModel.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DataRepository
{
    public class YesNoRepository : BaseRepository<G_YESNO>
    {
        public YesNoRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {

        }
    }
}
