﻿using BusinessModels;
using DataModel.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DataRepository
{
    public class ReferralHelpSubCategoryRepository : BaseRepository<C_REFERRAL_HELPSUBCATEGORY>
    {
        public ReferralHelpSubCategoryRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {
        }

        #region Get Referral Help SubCategory Repository
        public List<DependentLookupResponseModel> GetReferralHelpSubCategory()
        {

            var helpSubCategory = (from helpCat in DbContext.C_REFERRAL_HELPSUBCATEGORY
                                   select new DependentLookupResponseModel
                                   {
                                       id = helpCat.SUBCATEGORYID,
                                       description = helpCat.description,
                                       subId = helpCat.CATEGORYID
                                   }
                                  ).ToList();
                            
            return helpSubCategory;
        }
        #endregion

    }
}
