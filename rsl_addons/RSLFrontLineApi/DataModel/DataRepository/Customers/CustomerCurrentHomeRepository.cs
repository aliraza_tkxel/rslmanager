﻿using BusinessModels;
using DataModel.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities;

namespace DataModel.DataRepository
{
    public class CustomerCurrentHomeRepository : BaseRepository<FLS_CustomerCurrentHome>
    {
        public CustomerCurrentHomeRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {

        }
        #region Save current home info
        
        public void AddEditCurrentHome(CustomerDetailDataModel customerModel)
        {

            FLS_CustomerCurrentHome currHome = new FLS_CustomerCurrentHome();
            if (customerModel.currentHome.currentHomeId > 0)
            {
                var query = DbContext.FLS_CustomerCurrentHome.Where(x => x.CurrentHomeId == customerModel.currentHome.currentHomeId);
                if (query.Count() > 0)
                {
                    currHome = query.First();

                }
            }

            currHome.CustomerId = customerModel.customerId;
            currHome.floor = customerModel.currentHome.floor;
            currHome.HouseTypeId = customerModel.currentHome.houseTypeId;
            currHome.LandlordName = customerModel.currentHome.landlordName;
            currHome.LandlordTypeId = customerModel.currentHome.landlordTypeId;
            currHome.LivingWithFamilyId = customerModel.currentHome.livingWithFamilyId;
            currHome.NoOfBedrooms = customerModel.currentHome.noOfBedrooms;
            currHome.PreviousLandLordName = customerModel.currentHome.previousLandLordName;
            currHome.TenancyEndDate = customerModel.currentHome.tenancyEndDate;
            currHome.TenancyStartDate = customerModel.currentHome.tenancyStartDate;
            if (customerModel.currentHome.currentHomeId > 0)
            {
                base.Update(currHome, customerModel.currentHome.currentHomeId);
            }
            else
            {
                base.Add(currHome);
            }
        }
        #endregion

        #region Edit current hme on bases of customer ID while adding tenancy

        public void EditCurrentHome(TenancySetupDataModel request)
        {
            FLS_CustomerCurrentHome currHome = new FLS_CustomerCurrentHome();
            FLS_LandLordType landlordType = new FLS_LandLordType();
            landlordType = DbContext.FLS_LandLordType.Where(c => c.Title == ApplicationConstants.socialLandLord).FirstOrDefault();
            if (request.customerId > 0)
            {
                var query = DbContext.FLS_CustomerCurrentHome.Where(x => x.CustomerId == request.customerId).OrderByDescending(x=>x.CurrentHomeId);
                if (query.Count() > 0)
                {
                    currHome = query.First();

                }
            }

            currHome.CustomerId = request.customerId;
            currHome.LandlordName = ApplicationConstants.bhgLandLord;
            currHome.LandlordTypeId = landlordType.LandLordTypeId;
            if (currHome.CurrentHomeId > 0)
            {
                base.Update(currHome, currHome.CurrentHomeId);
            }
            else
            {
                base.Add(currHome);
            }
        }
        #endregion


    }

}

