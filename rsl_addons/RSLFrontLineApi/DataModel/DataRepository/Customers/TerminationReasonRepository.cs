﻿using DataModel.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DataRepository
{
    public class TerminationReasonRepository : BaseRepository<C_TERMINATION_REASON>
    {
        public TerminationReasonRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {
        }
    }
}
