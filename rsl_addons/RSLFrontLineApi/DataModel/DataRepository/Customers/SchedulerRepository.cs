﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using BusinessModels;
using BusinessModels.BusinessModels.Customers.RequestModels;
using BusinessModels.BusinessModels.Customers.ResponseModels;
using DataModel.DatabaseEntities;
using DataModel.DataRepository.Helpers;

namespace DataModel.DataRepository.Customers
{
    public class SchedulerRepository : BaseRepository<C__CUSTOMER>
    {
        public SchedulerRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {
        }

        #region Fetch Publisher

        public List<FLS_GetAvailableOperativesForVoidInspection_Result> GetAvailablePublishers(OperativesForVoidInspectionRequestModel model)
        {
            var availableOperatives = DbContext.FLS_GetAvailableOperativesForVoidInspection(model.msattype).ToList();
            var availableOperativesLeaves = DbContext.FLS_GetAvailableOperativesLeavesForVoidInspection(model.msattype, model.startDate).OrderBy(x => x.OperativeId).ToList();
            var availableOperativesAppointed = DbContext.FLS_GetAvailableOperativesAppointedForVoidInspection(model.msattype, model.startDate).OrderBy(x => x.OperativeId).ToList();


            var tempOperatives = new List<FLS_GetAvailableOperativesForVoidInspection_Result>(availableOperatives);
            foreach (var operative in tempOperatives)
            {
                var isLeaveExist = OperativesHelper.IsLeavesExist(availableOperativesLeaves, operative.EmployeeId, model.aptStartDateTime, model.aptEndDateTime);
                if (isLeaveExist)
                {
                    availableOperatives.RemoveAt(availableOperatives.IndexOf(operative));
                }

                var appointedExistList = OperativesHelper.IsAppointedExist(availableOperativesAppointed, operative.EmployeeId, model.startDate, model.aptStartDateTime, model.aptEndDateTime, model.startDate);
                if (appointedExistList != null && appointedExistList.Count > 0)
                {
                    availableOperatives.RemoveAt(availableOperatives.IndexOf(operative));
                }
            }

            var lstOfOperatives = availableOperatives.Distinct().ToList();

            return lstOfOperatives;
        }

        #endregion

        public VoidInspectionResponseModel SaveVoidInspection(VoidInspectionRequestModel model)
        {
            ObjectParameter isSaved = new ObjectParameter("isSaved", typeof(int));
            ObjectParameter appointmentIdOut = new ObjectParameter("appointmentIdOut", typeof(int));

            if (model.IsRearrange)
            {
                DbContext.V_ReScheduleVoidInspection(model.UserId, model.AppointmentNotes, model.AppointmentStartDate,
                model.AppointmentEndDate, model.StartTime, model.EndTime, model.OperativeId, model.JournalId, isSaved);

                return new VoidInspectionResponseModel
                {
                    Response = new Response
                    {
                        IsSaved = (int)isSaved.Value != 0,
                        StatusMessage = (int)isSaved.Value != 0 ? "Void Inspection has been scheduled successfully " : "Void Inspection could not be scheduled. Please try again."
                    }
                };                
            }
            DbContext.V_ScheduleVoidInspection(model.UserId, model.AppointmentNotes, model.AppointmentStartDate,
                    model.AppointmentEndDate, model.StartTime, model.EndTime, model.OperativeId, model.JournalId,
                    model.Duration, isSaved, appointmentIdOut);

            return new VoidInspectionResponseModel
            {
                Response = new Response
                {
                    AppointmentIdOut = (int)appointmentIdOut.Value,
                    IsSaved = (int)isSaved.Value != 0,
                    StatusMessage = (int)isSaved.Value != 0 ? "Void Inspection has been scheduled successfully " : "Void Inspection could not be scheduled. Please try again."
                }
            };
        }
    }
}