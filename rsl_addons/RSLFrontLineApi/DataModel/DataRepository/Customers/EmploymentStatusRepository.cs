﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModel.DatabaseEntities;
namespace DataModel.DataRepository
{
   public class EmploymentStatusRepository : BaseRepository<C_EMPLOYMENTSTATUS>
    {
        public EmploymentStatusRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {
        }
    }
}
