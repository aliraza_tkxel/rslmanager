﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModel.DatabaseEntities;
namespace DataModel.DataRepository
{
    public class DisabilityRepository : BaseRepository<G_DISABILITY>
    {
        public DisabilityRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {
        }
    }
}
