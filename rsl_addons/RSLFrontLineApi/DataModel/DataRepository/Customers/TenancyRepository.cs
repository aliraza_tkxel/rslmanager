﻿using BusinessModels;
using DataModel.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DataRepository
{
    public class TenancyRepository : BaseRepository<C_TENANCY>
    {
        public TenancyRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {
        }
        #region Save tenancy info
        public C_TENANCY SaveTenancyInfo(TenancySetupDataModel request)
        {
            var tenancyType = DbContext.P_FINANCIAL.Where(x => x.PROPERTYID == request.propertyId).Select(y=>y.RENTTYPE).FirstOrDefault();
            C_TENANCY tenancy = new C_TENANCY();
            tenancy.CREATIONDATE = DateTime.Now;
            //tenancy.CREATEDBY = request.referral.lastActionUserId;
            tenancy.TENANCYTYPE = tenancyType;
            tenancy.STARTDATE = request.tenancyStartDate;
            tenancy.ENDDATE = null;
            tenancy.PROPERTYID = request.propertyId;

//hardcoding these values as they are optional on web and UI/requirements related to them are not available in frontline
            tenancy.PROVISIONAL = 1;
            tenancy.MUTUALEXCHANGESTART = 0;
            tenancy.MUTUALEXCHANGEEND = 0;
            tenancy.CBL = false;
            // tenancy.LASTMODIFIEDBY = request.referral.lastActionUserId;
            base.Add(tenancy);
            return tenancy;
        }
        #endregion
    }
}
