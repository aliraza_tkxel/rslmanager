﻿using BusinessModels;
using DataModel.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities;

namespace DataModel.DataRepository
{
    public class TerminationRepository : BaseRepository<C_TERMINATION>
    {
        public TerminationRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {
        }
        #region Add termination info
        public void AddTerminationData(TenancyTerminationDataModel request)
        {
            var actionId = DbContext.C_ACTION.Where(x => string.Compare(x.DESCRIPTION, ApplicationConstants.ItemActionLog, true) == 0).Select(x => x.ITEMACTIONID).FirstOrDefault();
            var statusId = DbContext.C_STATUS.Where(x => string.Compare(x.DESCRIPTION, ApplicationConstants.ReferralstatusPending, true) == 0).OrderByDescending(y => y.ITEMSTATUSID).Select(x => x.ITEMSTATUSID).FirstOrDefault();
            //start insertion in termination
            C_TERMINATION termination = new C_TERMINATION();
            termination.JOURNALID = request.journalId;
            termination.ITEMSTATUSID = statusId;
            termination.ITEMACTIONID = actionId;
            termination.LASTACTIONDATE = DateTime.Now;
            termination.LASTACTIONUSER = request.terminatedBy;
            termination.TERMINATIONDATE = request.terminationDate.Value.Date;
            termination.REASON = request.reasonId;
            termination.REASONCATEGORYID = request.reasonCategoryId;
            termination.NOTES = request.notes;
            termination.TTIMESTAMP = DateTime.Now;
            termination.AppVersion = request.AppVersion;
            base.Add(termination);
            //end insertion in termination
        }
        #endregion


        #region Get termination Reason Category
        public List<DependentLookupResponseModel> GetTerminationReasonCategory()
        {

            var reasonCat = (from tr in DbContext.C_TERMINATION_REASON
                             join trta in DbContext.C_TERMINATION_REASON_TOACTIVITY on tr.REASONID equals trta.REASONID
                             join tra in DbContext.C_TERMINATION_REASON_ACTIVITY on trta.TERMINATION_REASON_ACTIVITYID equals tra.TERMINATION_REASON_ACTIVITYID
                             orderby tr.REASONID
                             select new DependentLookupResponseModel
                             {
                                 id = tra.TERMINATION_REASON_ACTIVITYID,
                                 subId = tr.REASONID,
                                 description = tra.DESCRIPTION                                 
                             }
                             ).ToList();
            return reasonCat;
        }
        #endregion
    }
}
