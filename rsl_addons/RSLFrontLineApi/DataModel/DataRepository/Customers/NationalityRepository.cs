﻿using DataModel.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DataRepository
{
    public class NationalityRepository : BaseRepository<G_CUSTOMERNATIONALITY>
    {
        public NationalityRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {
        }
    }
}
