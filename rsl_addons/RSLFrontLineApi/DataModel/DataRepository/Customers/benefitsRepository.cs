﻿using DataModel.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DataRepository
{
  public class BenefitsRepository : BaseRepository<g_benefit>
    {
        public BenefitsRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {

        }
    }
}
