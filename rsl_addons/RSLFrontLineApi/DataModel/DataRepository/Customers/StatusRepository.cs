﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModel.DatabaseEntities;
namespace DataModel.DataRepository
{
    public class StatusRepository : BaseRepository<C_STATUS>
    {
        public StatusRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {
        }
    }
}
