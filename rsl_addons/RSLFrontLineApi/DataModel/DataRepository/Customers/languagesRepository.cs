﻿using DataModel.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DataRepository
{
   public  class LanguagesRepository : BaseRepository<G_LANGUAGES>
    {
        public LanguagesRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {

        }

    }
}
