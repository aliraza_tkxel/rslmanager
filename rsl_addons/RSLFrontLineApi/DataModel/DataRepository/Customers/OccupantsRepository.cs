﻿using BusinessModels;
using DataModel.DatabaseEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataModel.DataRepository
{
    public class OccupantsRepository : BaseRepository<C_OCCUPANT>
    {
        public OccupantsRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {
        }
        public void SaveOccupantsList(OccupantsDataModel requset)
        {
            C_OCCUPANT occupant = new C_OCCUPANT();
            if (requset.occupantId > 0)
            {
                var query = DbContext.C_OCCUPANT.Where(x => x.OCCUPANTID == requset.occupantId);
                if (query.Count() > 0)
                {
                    occupant = query.First();

                }
            }
            occupant.TENANCYID = requset.tenancyId;
            occupant.CUSTOMERID = requset.customerId;
            occupant.RELATIONSHIP = requset.relationshipStatusId;
            occupant.ECONOMICSTATUS = requset.employmentStatusId;//on web occupants are saving in this way
            occupant.DISABILITY = requset.disabilities;
            occupant.DisabilityOther = requset.disabilitiesOthers;
            occupant.DATESTAMP = DateTime.Now;

            if(requset.occupantId > 0)
            {
                base.Update(occupant,requset.occupantId);
            }
            else
            {
                base.Add(occupant);
            }          
        }
    }
}
