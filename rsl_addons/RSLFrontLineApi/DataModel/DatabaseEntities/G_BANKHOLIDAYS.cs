//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataModel.DatabaseEntities
{
    using System;
    using System.Collections.Generic;
    
    public partial class G_BANKHOLIDAYS
    {
        public System.DateTime BHDATE { get; set; }
        public Nullable<short> BHA { get; set; }
        public Nullable<short> MeridianEast { get; set; }
    }
}
