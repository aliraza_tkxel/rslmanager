//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataModel.DatabaseEntities
{
    using System;
    using System.Collections.Generic;
    
    public partial class PA_PROPERTY_ITEM_IMAGES
    {
        public int SID { get; set; }
        public string PROPERTYID { get; set; }
        public Nullable<int> ItemId { get; set; }
        public string ImagePath { get; set; }
        public string ImageName { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public string Title { get; set; }
        public string ImageIdentifier { get; set; }
        public Nullable<int> SchemeId { get; set; }
        public Nullable<int> BlockId { get; set; }
        public Nullable<bool> Isdefault { get; set; }
    
        public virtual PA_ITEM PA_ITEM { get; set; }
    }
}
