//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace DataModel.DatabaseEntities
{
    using System;
    using System.Collections.Generic;
    
    public partial class P_PHASE
    {
        public int PHASEID { get; set; }
        public Nullable<System.DateTime> ConstructionStart { get; set; }
        public Nullable<System.DateTime> AnticipatedCompletion { get; set; }
        public Nullable<System.DateTime> ActualCompletion { get; set; }
        public Nullable<System.DateTime> HandoverintoManagement { get; set; }
        public Nullable<int> DEVELOPMENTID { get; set; }
        public string PhaseName { get; set; }
    }
}
