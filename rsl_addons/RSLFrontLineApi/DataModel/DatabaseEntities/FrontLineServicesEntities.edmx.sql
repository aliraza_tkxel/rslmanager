
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 01/17/2017 12:41:27
-- Generated from EDMX file: D:\TkxelGit(master branch)\TkxelGit\bhgfrontline\Frontline.API\DataModel\DatabaseEntities\FrontLineServicesEntities.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [RSLBHALive];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_AS_USER_AS_USERTYPE]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AS_USER] DROP CONSTRAINT [FK_AS_USER_AS_USERTYPE];
GO
IF OBJECT_ID(N'[dbo].[FK_AS_USER_INSPECTIONTYPE_P_INSPECTIONTYPE]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AS_USER_INSPECTIONTYPE] DROP CONSTRAINT [FK_AS_USER_INSPECTIONTYPE_P_INSPECTIONTYPE];
GO
IF OBJECT_ID(N'[dbo].[FK_C_RISK_CATEGORY_C_RISK_SUBCATEGORY]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[C_RISK_SUBCATEGORY] DROP CONSTRAINT [FK_C_RISK_CATEGORY_C_RISK_SUBCATEGORY];
GO
IF OBJECT_ID(N'[dbo].[FK_C_VULNERABILITY_CATEGORY_C_VULNERABILITY_SUBCATEGORY]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[C_VULNERABILITY_SUBCATEGORY] DROP CONSTRAINT [FK_C_VULNERABILITY_CATEGORY_C_VULNERABILITY_SUBCATEGORY];
GO
IF OBJECT_ID(N'[dbo].[FK_P__PROPERTY_PDR_DEVELOPMENT]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[P__PROPERTY] DROP CONSTRAINT [FK_P__PROPERTY_PDR_DEVELOPMENT];
GO
IF OBJECT_ID(N'[RSLBHALiveModelStoreContainer].[FK_P_FINANCIAL_P__PROPERTY]', 'F') IS NOT NULL
    ALTER TABLE [RSLBHALiveModelStoreContainer].[P_FINANCIAL] DROP CONSTRAINT [FK_P_FINANCIAL_P__PROPERTY];
GO
IF OBJECT_ID(N'[dbo].[FK_PA_ITEM_PARAMETER_PA_ITEM]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PA_ITEM_PARAMETER] DROP CONSTRAINT [FK_PA_ITEM_PARAMETER_PA_ITEM];
GO
IF OBJECT_ID(N'[dbo].[FK_PA_ITEM_PARAMETER_PA_PARAMETER]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PA_ITEM_PARAMETER] DROP CONSTRAINT [FK_PA_ITEM_PARAMETER_PA_PARAMETER];
GO
IF OBJECT_ID(N'[dbo].[FK_PA_PROPERTY_ATTRIBUTES_PA_ITEM_PARAMETER]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PA_PROPERTY_ATTRIBUTES] DROP CONSTRAINT [FK_PA_PROPERTY_ATTRIBUTES_PA_ITEM_PARAMETER];
GO
IF OBJECT_ID(N'[dbo].[FK_PA_PROPERTY_ITEM_IMAGES_PA_ITEM]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PA_PROPERTY_ITEM_IMAGES] DROP CONSTRAINT [FK_PA_PROPERTY_ITEM_IMAGES_PA_ITEM];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[AC_LOGIN_HISTORY]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AC_LOGIN_HISTORY];
GO
IF OBJECT_ID(N'[dbo].[AC_LOGINS]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AC_LOGINS];
GO
IF OBJECT_ID(N'[dbo].[AS_USER]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AS_USER];
GO
IF OBJECT_ID(N'[dbo].[AS_USER_INSPECTIONTYPE]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AS_USER_INSPECTIONTYPE];
GO
IF OBJECT_ID(N'[dbo].[AS_USERTYPE]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AS_USERTYPE];
GO
IF OBJECT_ID(N'[dbo].[C_ADDRESS]', 'U') IS NOT NULL
    DROP TABLE [dbo].[C_ADDRESS];
GO
IF OBJECT_ID(N'[dbo].[C_ADDRESSTYPE]', 'U') IS NOT NULL
    DROP TABLE [dbo].[C_ADDRESSTYPE];
GO
IF OBJECT_ID(N'[dbo].[C_CUSTOMERTENANCY]', 'U') IS NOT NULL
    DROP TABLE [dbo].[C_CUSTOMERTENANCY];
GO
IF OBJECT_ID(N'[dbo].[C_CUSTOMERTYPE]', 'U') IS NOT NULL
    DROP TABLE [dbo].[C_CUSTOMERTYPE];
GO
IF OBJECT_ID(N'[dbo].[C_LETTERACTION]', 'U') IS NOT NULL
    DROP TABLE [dbo].[C_LETTERACTION];
GO
IF OBJECT_ID(N'[dbo].[C_REFERRAL]', 'U') IS NOT NULL
    DROP TABLE [dbo].[C_REFERRAL];
GO
IF OBJECT_ID(N'[dbo].[C_RISK_CATEGORY]', 'U') IS NOT NULL
    DROP TABLE [dbo].[C_RISK_CATEGORY];
GO
IF OBJECT_ID(N'[dbo].[C_RISK_SUBCATEGORY]', 'U') IS NOT NULL
    DROP TABLE [dbo].[C_RISK_SUBCATEGORY];
GO
IF OBJECT_ID(N'[dbo].[C_STANDARDLETTERS]', 'U') IS NOT NULL
    DROP TABLE [dbo].[C_STANDARDLETTERS];
GO
IF OBJECT_ID(N'[dbo].[C_STATUS]', 'U') IS NOT NULL
    DROP TABLE [dbo].[C_STATUS];
GO
IF OBJECT_ID(N'[dbo].[C_TENANCY]', 'U') IS NOT NULL
    DROP TABLE [dbo].[C_TENANCY];
GO
IF OBJECT_ID(N'[dbo].[C_TENANCYTYPE]', 'U') IS NOT NULL
    DROP TABLE [dbo].[C_TENANCYTYPE];
GO
IF OBJECT_ID(N'[dbo].[C_TERMINATION]', 'U') IS NOT NULL
    DROP TABLE [dbo].[C_TERMINATION];
GO
IF OBJECT_ID(N'[dbo].[C_TERMINATION_REASON]', 'U') IS NOT NULL
    DROP TABLE [dbo].[C_TERMINATION_REASON];
GO
IF OBJECT_ID(N'[dbo].[C_VULNERABILITY]', 'U') IS NOT NULL
    DROP TABLE [dbo].[C_VULNERABILITY];
GO
IF OBJECT_ID(N'[dbo].[C_VULNERABILITY_CATEGORY]', 'U') IS NOT NULL
    DROP TABLE [dbo].[C_VULNERABILITY_CATEGORY];
GO
IF OBJECT_ID(N'[dbo].[C_VULNERABILITY_SUBCATEGORY]', 'U') IS NOT NULL
    DROP TABLE [dbo].[C_VULNERABILITY_SUBCATEGORY];
GO
IF OBJECT_ID(N'[dbo].[E__EMPLOYEE]', 'U') IS NOT NULL
    DROP TABLE [dbo].[E__EMPLOYEE];
GO
IF OBJECT_ID(N'[dbo].[E_JOBDETAILS]', 'U') IS NOT NULL
    DROP TABLE [dbo].[E_JOBDETAILS];
GO
IF OBJECT_ID(N'[dbo].[E_TEAM]', 'U') IS NOT NULL
    DROP TABLE [dbo].[E_TEAM];
GO
IF OBJECT_ID(N'[dbo].[FLS_CustomerCurrentHome]', 'U') IS NOT NULL
    DROP TABLE [dbo].[FLS_CustomerCurrentHome];
GO
IF OBJECT_ID(N'[dbo].[FLS_HouseType]', 'U') IS NOT NULL
    DROP TABLE [dbo].[FLS_HouseType];
GO
IF OBJECT_ID(N'[dbo].[FLS_LandLordType]', 'U') IS NOT NULL
    DROP TABLE [dbo].[FLS_LandLordType];
GO
IF OBJECT_ID(N'[dbo].[FLS_Viewings]', 'U') IS NOT NULL
    DROP TABLE [dbo].[FLS_Viewings];
GO
IF OBJECT_ID(N'[dbo].[FLS_ViewingsHistory]', 'U') IS NOT NULL
    DROP TABLE [dbo].[FLS_ViewingsHistory];
GO
IF OBJECT_ID(N'[dbo].[FLS_ViewingStatus]', 'U') IS NOT NULL
    DROP TABLE [dbo].[FLS_ViewingStatus];
GO
IF OBJECT_ID(N'[dbo].[G_BANKHOLIDAYS]', 'U') IS NOT NULL
    DROP TABLE [dbo].[G_BANKHOLIDAYS];
GO
IF OBJECT_ID(N'[dbo].[G_INTERNETACCESS]', 'U') IS NOT NULL
    DROP TABLE [dbo].[G_INTERNETACCESS];
GO
IF OBJECT_ID(N'[dbo].[G_LOCALAUTHORITY]', 'U') IS NOT NULL
    DROP TABLE [dbo].[G_LOCALAUTHORITY];
GO
IF OBJECT_ID(N'[dbo].[G_MARITALSTATUS]', 'U') IS NOT NULL
    DROP TABLE [dbo].[G_MARITALSTATUS];
GO
IF OBJECT_ID(N'[dbo].[G_TITLE]', 'U') IS NOT NULL
    DROP TABLE [dbo].[G_TITLE];
GO
IF OBJECT_ID(N'[dbo].[G_TRADE]', 'U') IS NOT NULL
    DROP TABLE [dbo].[G_TRADE];
GO
IF OBJECT_ID(N'[dbo].[G_YEAR]', 'U') IS NOT NULL
    DROP TABLE [dbo].[G_YEAR];
GO
IF OBJECT_ID(N'[dbo].[G_YESNO_UNKNOWN]', 'U') IS NOT NULL
    DROP TABLE [dbo].[G_YESNO_UNKNOWN];
GO
IF OBJECT_ID(N'[dbo].[P__PROPERTY]', 'U') IS NOT NULL
    DROP TABLE [dbo].[P__PROPERTY];
GO
IF OBJECT_ID(N'[dbo].[P_BLOCK]', 'U') IS NOT NULL
    DROP TABLE [dbo].[P_BLOCK];
GO
IF OBJECT_ID(N'[dbo].[P_DEVELOPMENTTYPE]', 'U') IS NOT NULL
    DROP TABLE [dbo].[P_DEVELOPMENTTYPE];
GO
IF OBJECT_ID(N'[dbo].[P_INSPECTIONTYPE]', 'U') IS NOT NULL
    DROP TABLE [dbo].[P_INSPECTIONTYPE];
GO
IF OBJECT_ID(N'[dbo].[P_PHASE]', 'U') IS NOT NULL
    DROP TABLE [dbo].[P_PHASE];
GO
IF OBJECT_ID(N'[dbo].[P_PROPERTYTYPE]', 'U') IS NOT NULL
    DROP TABLE [dbo].[P_PROPERTYTYPE];
GO
IF OBJECT_ID(N'[dbo].[P_SCHEME]', 'U') IS NOT NULL
    DROP TABLE [dbo].[P_SCHEME];
GO
IF OBJECT_ID(N'[dbo].[P_STATUS]', 'U') IS NOT NULL
    DROP TABLE [dbo].[P_STATUS];
GO
IF OBJECT_ID(N'[dbo].[P_SUBSTATUS]', 'U') IS NOT NULL
    DROP TABLE [dbo].[P_SUBSTATUS];
GO
IF OBJECT_ID(N'[dbo].[PA_ITEM]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PA_ITEM];
GO
IF OBJECT_ID(N'[dbo].[PA_ITEM_PARAMETER]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PA_ITEM_PARAMETER];
GO
IF OBJECT_ID(N'[dbo].[PA_PARAMETER]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PA_PARAMETER];
GO
IF OBJECT_ID(N'[dbo].[PA_PROPERTY_ATTRIBUTES]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PA_PROPERTY_ATTRIBUTES];
GO
IF OBJECT_ID(N'[dbo].[PA_PROPERTY_ITEM_IMAGES]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PA_PROPERTY_ITEM_IMAGES];
GO
IF OBJECT_ID(N'[dbo].[PDR_DEVELOPMENT]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PDR_DEVELOPMENT];
GO
IF OBJECT_ID(N'[RSLBHALiveModelStoreContainer].[C_ACTION]', 'U') IS NOT NULL
    DROP TABLE [RSLBHALiveModelStoreContainer].[C_ACTION];
GO
IF OBJECT_ID(N'[RSLBHALiveModelStoreContainer].[C_ECONOMICSTATUS]', 'U') IS NOT NULL
    DROP TABLE [RSLBHALiveModelStoreContainer].[C_ECONOMICSTATUS];
GO
IF OBJECT_ID(N'[RSLBHALiveModelStoreContainer].[C_EMPLOYMENTSTATUS]', 'U') IS NOT NULL
    DROP TABLE [RSLBHALiveModelStoreContainer].[C_EMPLOYMENTSTATUS];
GO
IF OBJECT_ID(N'[RSLBHALiveModelStoreContainer].[C_RELATIONSHIP]', 'U') IS NOT NULL
    DROP TABLE [RSLBHALiveModelStoreContainer].[C_RELATIONSHIP];
GO
IF OBJECT_ID(N'[RSLBHALiveModelStoreContainer].[g_BankFacility]', 'U') IS NOT NULL
    DROP TABLE [RSLBHALiveModelStoreContainer].[g_BankFacility];
GO
IF OBJECT_ID(N'[RSLBHALiveModelStoreContainer].[g_benefit]', 'U') IS NOT NULL
    DROP TABLE [RSLBHALiveModelStoreContainer].[g_benefit];
GO
IF OBJECT_ID(N'[RSLBHALiveModelStoreContainer].[G_COMMUNICATION]', 'U') IS NOT NULL
    DROP TABLE [RSLBHALiveModelStoreContainer].[G_COMMUNICATION];
GO
IF OBJECT_ID(N'[RSLBHALiveModelStoreContainer].[G_CUSTOMERNATIONALITY]', 'U') IS NOT NULL
    DROP TABLE [RSLBHALiveModelStoreContainer].[G_CUSTOMERNATIONALITY];
GO
IF OBJECT_ID(N'[RSLBHALiveModelStoreContainer].[G_DISABILITY]', 'U') IS NOT NULL
    DROP TABLE [RSLBHALiveModelStoreContainer].[G_DISABILITY];
GO
IF OBJECT_ID(N'[RSLBHALiveModelStoreContainer].[G_ETHNICITY]', 'U') IS NOT NULL
    DROP TABLE [RSLBHALiveModelStoreContainer].[G_ETHNICITY];
GO
IF OBJECT_ID(N'[RSLBHALiveModelStoreContainer].[g_HouseIncome]', 'U') IS NOT NULL
    DROP TABLE [RSLBHALiveModelStoreContainer].[g_HouseIncome];
GO
IF OBJECT_ID(N'[RSLBHALiveModelStoreContainer].[G_LANGUAGES]', 'U') IS NOT NULL
    DROP TABLE [RSLBHALiveModelStoreContainer].[G_LANGUAGES];
GO
IF OBJECT_ID(N'[RSLBHALiveModelStoreContainer].[G_OTHERCOMMUNICATION]', 'U') IS NOT NULL
    DROP TABLE [RSLBHALiveModelStoreContainer].[G_OTHERCOMMUNICATION];
GO
IF OBJECT_ID(N'[RSLBHALiveModelStoreContainer].[G_PREFEREDCONTACT]', 'U') IS NOT NULL
    DROP TABLE [RSLBHALiveModelStoreContainer].[G_PREFEREDCONTACT];
GO
IF OBJECT_ID(N'[RSLBHALiveModelStoreContainer].[G_RELIGION]', 'U') IS NOT NULL
    DROP TABLE [RSLBHALiveModelStoreContainer].[G_RELIGION];
GO
IF OBJECT_ID(N'[RSLBHALiveModelStoreContainer].[G_SEXUALORIENTATION]', 'U') IS NOT NULL
    DROP TABLE [RSLBHALiveModelStoreContainer].[G_SEXUALORIENTATION];
GO
IF OBJECT_ID(N'[RSLBHALiveModelStoreContainer].[G_SUPPORTAGENCIES]', 'U') IS NOT NULL
    DROP TABLE [RSLBHALiveModelStoreContainer].[G_SUPPORTAGENCIES];
GO
IF OBJECT_ID(N'[RSLBHALiveModelStoreContainer].[G_TEAMCODES]', 'U') IS NOT NULL
    DROP TABLE [RSLBHALiveModelStoreContainer].[G_TEAMCODES];
GO
IF OBJECT_ID(N'[RSLBHALiveModelStoreContainer].[G_YESNO]', 'U') IS NOT NULL
    DROP TABLE [RSLBHALiveModelStoreContainer].[G_YESNO];
GO
IF OBJECT_ID(N'[RSLBHALiveModelStoreContainer].[P_ASSETTYPE]', 'U') IS NOT NULL
    DROP TABLE [RSLBHALiveModelStoreContainer].[P_ASSETTYPE];
GO
IF OBJECT_ID(N'[RSLBHALiveModelStoreContainer].[P_FINANCIAL]', 'U') IS NOT NULL
    DROP TABLE [RSLBHALiveModelStoreContainer].[P_FINANCIAL];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'AC_LOGIN_HISTORY'
CREATE TABLE [dbo].[AC_LOGIN_HISTORY] (
    [LoginID] int IDENTITY(1,1) NOT NULL,
    [FirstName] varchar(50)  NULL,
    [LastName] varchar(50)  NULL,
    [UserName] varchar(50)  NULL,
    [LoginDate] datetime  NULL,
    [ISFAILED] bit  NULL,
    [ISLOCKED] bit  NULL
);
GO

-- Creating table 'AC_LOGINS'
CREATE TABLE [dbo].[AC_LOGINS] (
    [LOGINID] int IDENTITY(1,1) NOT NULL,
    [EMPLOYEEID] int  NULL,
    [LOGIN] nvarchar(50)  NULL,
    [PASSWORD] nvarchar(50)  NULL,
    [ACTIVE] int  NULL,
    [DATECREATED] datetime  NULL,
    [DATEMODIFIED] datetime  NULL,
    [CREATEDBY] int  NULL,
    [MODIFIEDBY] int  NULL,
    [EXPIRES] datetime  NULL,
    [DEVICETOKEN] nvarchar(70)  NULL,
    [ISLOCKED] bit  NULL,
    [THRESHOLD] tinyint  NULL,
    [DATEUPDATED] datetime  NULL,
    [LASTLOGGEDIN] datetime  NULL
);
GO

-- Creating table 'E__EMPLOYEE'
CREATE TABLE [dbo].[E__EMPLOYEE] (
    [EMPLOYEEID] int IDENTITY(1,1) NOT NULL,
    [CREATIONDATE] datetime  NULL,
    [TITLE] int  NULL,
    [FIRSTNAME] nvarchar(50)  NULL,
    [MIDDLENAME] nvarchar(50)  NULL,
    [LASTNAME] nvarchar(100)  NULL,
    [DOB] datetime  NULL,
    [GENDER] nvarchar(20)  NULL,
    [MARITALSTATUS] int  NULL,
    [ETHNICITY] int  NULL,
    [ETHNICITYOTHER] nvarchar(80)  NULL,
    [RELIGION] int  NULL,
    [RELIGIONOTHER] nvarchar(80)  NULL,
    [SEXUALORIENTATION] int  NULL,
    [SEXUALORIENTATIONOTHER] nvarchar(80)  NULL,
    [DISABILITY] nvarchar(80)  NULL,
    [DISABILITYOTHER] nvarchar(80)  NULL,
    [BANK] nvarchar(100)  NULL,
    [SORTCODE] nvarchar(30)  NULL,
    [ACCOUNTNUMBER] nvarchar(30)  NULL,
    [ACCOUNTNAME] nvarchar(100)  NULL,
    [ROLLNUMBER] nvarchar(30)  NULL,
    [BANK2] nvarchar(100)  NULL,
    [SORTCODE2ND] nvarchar(50)  NULL,
    [ACCOUNTNUMBER2] nvarchar(50)  NULL,
    [ACCOUNTNAME2] nvarchar(100)  NULL,
    [ROLLNUMBER2] nvarchar(50)  NULL,
    [ORGID] int  NULL,
    [IMAGEPATH] nvarchar(250)  NULL,
    [ROLEPATH] nvarchar(250)  NULL,
    [PROFILE] varchar(max)  NULL,
    [LASTLOGGEDIN] datetime  NULL,
    [SIGNATUREPATH] nvarchar(250)  NULL,
    [JobRoleTeamId] int  NULL,
    [LASTACTIONTIME] datetime  NULL,
    [LASTACTIONUSER] int  NULL,
    [EmploymentContracts] nvarchar(500)  NULL
);
GO

-- Creating table 'E_JOBDETAILS'
CREATE TABLE [dbo].[E_JOBDETAILS] (
    [JOBDETAILSID] int IDENTITY(1,1) NOT NULL,
    [EMPLOYEEID] int  NOT NULL,
    [STARTDATE] datetime  NULL,
    [ENDDATE] datetime  NULL,
    [JOBTITLE] nvarchar(100)  NULL,
    [REFERENCENUMBER] nvarchar(20)  NULL,
    [DETAILSOFROLE] nvarchar(400)  NULL,
    [TEAM] int  NULL,
    [PATCH] int  NULL,
    [LINEMANAGER] int  NULL,
    [GRADE] int  NULL,
    [SALARY] decimal(19,4)  NULL,
    [PREVIOUSSALARY] decimal(19,4)  NULL,
    [TAXOFFICE] nvarchar(50)  NULL,
    [TAXCODE] nvarchar(50)  NULL,
    [PAYROLLNUMBER] nvarchar(30)  NULL,
    [DRIVINGLICENSENUMBER] nvarchar(30)  NULL,
    [NINUMBER] nvarchar(10)  NULL,
    [HOLIDAYENTITLEMENTDAYS] float  NULL,
    [HOLIDAYENTITLEMENTHOURS] float  NULL,
    [REVIEWDATE] datetime  NULL,
    [APPRAISALDATE] datetime  NULL,
    [PROBATIONPERIOD] int  NULL,
    [PARTFULLTIME] int  NULL,
    [HOURS] float  NULL,
    [OTRATE] float  NULL,
    [EMPLOYEELIMIT] float  NULL,
    [NOTICEPERIOD] int  NULL,
    [DATEOFNOTICE] datetime  NULL,
    [FOREIGNNATIONALNUMBER] nvarchar(30)  NULL,
    [BUDDY] int  NULL,
    [ACTIVE] int  NULL,
    [ISDIRECTOR] int  NULL,
    [ISMANAGER] int  NULL,
    [GRADEPOINT] int  NULL,
    [OFFICELOCATION] int  NULL,
    [HOLIDAYRULE] int  NULL,
    [FTE] float  NULL,
    [HolidayIndicator] int  NULL,
    [CARRYFORWARD] float  NULL,
    [BANKHOLIDAY] float  NULL,
    [TOTALLEAVE] float  NULL,
    [ALSTARTDATE] datetime  NULL,
    [GSRENo] varchar(20)  NULL,
    [IsGasSafe] smallint  NULL,
    [IsOftec] smallint  NULL,
    [JobRoleId] int  NULL,
    [LASTACTIONUSER] int  NULL,
    [LASTACTIONTIME] datetime  NULL
);
GO

-- Creating table 'E_TEAM'
CREATE TABLE [dbo].[E_TEAM] (
    [TEAMID] int IDENTITY(1,1) NOT NULL,
    [CREATIONDATE] datetime  NOT NULL,
    [TEAMNAME] nvarchar(100)  NULL,
    [MANAGER] int  NULL,
    [DIRECTOR] int  NULL,
    [MAINFUNCTION] nvarchar(200)  NULL,
    [DESCRIPTION] nvarchar(500)  NULL,
    [ACTIVE] int  NULL
);
GO

-- Creating table 'G_TEAMCODES'
CREATE TABLE [dbo].[G_TEAMCODES] (
    [TEAMCODE] nvarchar(10)  NOT NULL,
    [TEAMNAME] nvarchar(100)  NULL,
    [DESCRIPTION] nvarchar(200)  NULL,
    [TEAMID] int  NULL,
    [NEWSALLOWED] int  NULL
);
GO

-- Creating table 'C_RISK_CATEGORY'
CREATE TABLE [dbo].[C_RISK_CATEGORY] (
    [CategoryId] int IDENTITY(1,1) NOT NULL,
    [Description] nvarchar(50)  NULL
);
GO

-- Creating table 'C_RISK_SUBCATEGORY'
CREATE TABLE [dbo].[C_RISK_SUBCATEGORY] (
    [SubCategoryId] int IDENTITY(1,1) NOT NULL,
    [CategoryId] int  NOT NULL,
    [Description] nvarchar(70)  NULL
);
GO

-- Creating table 'C_CUSTOMERTENANCY'
CREATE TABLE [dbo].[C_CUSTOMERTENANCY] (
    [CUSTOMERTENANCYID] int IDENTITY(1,1) NOT NULL,
    [CUSTOMERID] int  NOT NULL,
    [TENANCYID] int  NOT NULL,
    [CREATEDBY] int  NULL,
    [STARTDATE] datetime  NULL,
    [ENDDATE] datetime  NULL,
    [LASTMODIFIEDBY] int  NULL
);
GO

-- Creating table 'C_CUSTOMERTYPE'
CREATE TABLE [dbo].[C_CUSTOMERTYPE] (
    [CUSTOMERTYPEID] int IDENTITY(1,1) NOT NULL,
    [DESCRIPTION] nvarchar(50)  NULL
);
GO

-- Creating table 'C_LETTERACTION'
CREATE TABLE [dbo].[C_LETTERACTION] (
    [ACTIONID] int  NOT NULL,
    [NATURE] int  NULL,
    [NEXTACTION] int  NULL,
    [DESCRIPTION] nvarchar(50)  NULL
);
GO

-- Creating table 'C_REFERRAL'
CREATE TABLE [dbo].[C_REFERRAL] (
    [REFERRALHISTORYID] int IDENTITY(1,1) NOT NULL,
    [JOURNALID] int  NOT NULL,
    [ITEMSTATUSID] int  NULL,
    [ITEMACTIONID] int  NULL,
    [isTypeArrears] bit  NULL,
    [isTypeDamage] bit  NULL,
    [isTypeASB] bit  NULL,
    [STAGEID] tinyint  NULL,
    [SAFETYISSUES] tinyint  NULL,
    [CONVICTION] tinyint  NULL,
    [SUBSTANCEMISUSE] tinyint  NULL,
    [SELFHARM] tinyint  NULL,
    [NOTES] nvarchar(1000)  NULL,
    [ISTENANTAWARE] tinyint  NULL,
    [NEGLECTNOTES] nvarchar(1000)  NULL,
    [OTHERNOTES] nvarchar(1000)  NULL,
    [MISCNOTES] nvarchar(1000)  NULL,
    [REASONNOTES] nvarchar(1000)  NULL,
    [LASTACTIONDATE] datetime  NOT NULL,
    [ASSIGNTO] int  NULL,
    [LASTACTIONUSER] int  NULL
);
GO

-- Creating table 'C_STANDARDLETTERS'
CREATE TABLE [dbo].[C_STANDARDLETTERS] (
    [LETTERID] int IDENTITY(1,1) NOT NULL,
    [NATUREID] int  NULL,
    [SUBITEMID] int  NULL,
    [LETTERCODE] nvarchar(30)  NULL,
    [LETTERDESC] nvarchar(100)  NULL,
    [LETTERTEXT] nvarchar(max)  NULL,
    [FROMID] int  NULL,
    [TOID] int  NULL,
    [TEMPLETTER] nvarchar(max)  NULL,
    [TEMPSIG] nvarchar(4000)  NULL,
    [LETTERSIG] nvarchar(4000)  NULL
);
GO

-- Creating table 'C_STATUS'
CREATE TABLE [dbo].[C_STATUS] (
    [ITEMSTATUSID] int IDENTITY(1,1) NOT NULL,
    [ITEMID] int  NULL,
    [DESCRIPTION] nvarchar(35)  NULL
);
GO

-- Creating table 'C_TENANCY'
CREATE TABLE [dbo].[C_TENANCY] (
    [TENANCYID] int IDENTITY(1,1) NOT NULL,
    [CREATIONDATE] datetime  NULL,
    [CREATEDBY] int  NULL,
    [LASTMODIFIEDBY] int  NULL,
    [TENANCYREFOLDBYZ] nvarchar(50)  NULL,
    [TENANCYTYPE] int  NULL,
    [STARTDATE] datetime  NULL,
    [ENDDATE] datetime  NULL,
    [PROPERTYID] nvarchar(20)  NULL,
    [PROVISIONAL] int  NOT NULL,
    [MUTUALEXCHANGESTART] int  NOT NULL,
    [MUTUALEXCHANGEEND] int  NOT NULL,
    [CBL] bit  NULL
);
GO

-- Creating table 'C_TENANCYTYPE'
CREATE TABLE [dbo].[C_TENANCYTYPE] (
    [TENANCYTYPEID] int IDENTITY(1,1) NOT NULL,
    [DESCRIPTION] nvarchar(100)  NULL
);
GO

-- Creating table 'C_TERMINATION'
CREATE TABLE [dbo].[C_TERMINATION] (
    [TERMINATIONHISTORYID] int IDENTITY(1,1) NOT NULL,
    [JOURNALID] int  NULL,
    [ITEMSTATUSID] int  NULL,
    [ITEMACTIONID] int  NULL,
    [LASTACTIONDATE] datetime  NULL,
    [LASTACTIONUSER] int  NULL,
    [TERMINATIONDATE] datetime  NULL,
    [REASON] int  NULL,
    [NOTES] nvarchar(2000)  NULL,
    [TTIMESTAMP] datetime  NOT NULL,
    [REASONCATEGORYID] int  NULL
);
GO

-- Creating table 'C_TERMINATION_REASON'
CREATE TABLE [dbo].[C_TERMINATION_REASON] (
    [REASONID] int IDENTITY(1,1) NOT NULL,
    [DESCRIPTION] nvarchar(100)  NULL,
    [NROSH_DESCRIPTION] nvarchar(50)  NULL,
    [ACTIVE] bit  NULL
);
GO

-- Creating table 'C_VULNERABILITY'
CREATE TABLE [dbo].[C_VULNERABILITY] (
    [VULNERABILITYHISTORYID] int IDENTITY(1,1) NOT NULL,
    [JOURNALID] int  NULL,
    [ITEMSTATUSID] int  NULL,
    [ITEMACTIONID] int  NULL,
    [STARTDATE] datetime  NULL,
    [REVIEWDATE] datetime  NULL,
    [LASTACTIONDATE] datetime  NULL,
    [LASTACTIONUSER] int  NULL,
    [NOTES] nvarchar(1000)  NULL,
    [TEAM] int  NULL,
    [ASSIGNTO] int  NULL,
    [CUSTOMERAWARE] bit  NULL,
    [TENANTSONLINE] bit  NULL,
    [DTIMESTAMP] datetime  NULL
);
GO

-- Creating table 'C_VULNERABILITY_CATEGORY'
CREATE TABLE [dbo].[C_VULNERABILITY_CATEGORY] (
    [CategoryId] int IDENTITY(1,1) NOT NULL,
    [Description] nvarchar(50)  NULL
);
GO

-- Creating table 'C_VULNERABILITY_SUBCATEGORY'
CREATE TABLE [dbo].[C_VULNERABILITY_SUBCATEGORY] (
    [SubCategoryId] int IDENTITY(1,1) NOT NULL,
    [CategoryId] int  NOT NULL,
    [Description] nvarchar(70)  NULL
);
GO

-- Creating table 'C_ECONOMICSTATUS'
CREATE TABLE [dbo].[C_ECONOMICSTATUS] (
    [ECONOMICSTATUSID] int IDENTITY(1,1) NOT NULL,
    [DESCRIPTION] nvarchar(255)  NULL
);
GO

-- Creating table 'C_EMPLOYMENTSTATUS'
CREATE TABLE [dbo].[C_EMPLOYMENTSTATUS] (
    [EMPLOYMENTSTATUSID] int IDENTITY(1,1) NOT NULL,
    [DESCRIPTION] nvarchar(100)  NULL
);
GO

-- Creating table 'C_RELATIONSHIP'
CREATE TABLE [dbo].[C_RELATIONSHIP] (
    [RELATIONSHIPID] int IDENTITY(1,1) NOT NULL,
    [DESCRIPTION] nvarchar(255)  NULL
);
GO

-- Creating table 'G_BANKHOLIDAYS'
CREATE TABLE [dbo].[G_BANKHOLIDAYS] (
    [BHDATE] datetime  NOT NULL,
    [BHA] smallint  NULL,
    [MeridianEast] smallint  NULL
);
GO

-- Creating table 'G_INTERNETACCESS'
CREATE TABLE [dbo].[G_INTERNETACCESS] (
    [INTERNETACCESSID] int IDENTITY(1,1) NOT NULL,
    [DESCRIPTION] nvarchar(60)  NULL
);
GO

-- Creating table 'G_LOCALAUTHORITY'
CREATE TABLE [dbo].[G_LOCALAUTHORITY] (
    [LOCALAUTHORITYID] int IDENTITY(1,1) NOT NULL,
    [DESCRIPTION] nvarchar(100)  NULL,
    [LINKTOSUPPLIER] int  NULL,
    [Nrosh_Code] nvarchar(50)  NULL
);
GO

-- Creating table 'G_MARITALSTATUS'
CREATE TABLE [dbo].[G_MARITALSTATUS] (
    [MARITALSTATUSID] int IDENTITY(1,1) NOT NULL,
    [DESCRIPTION] nvarchar(60)  NULL
);
GO

-- Creating table 'G_TITLE'
CREATE TABLE [dbo].[G_TITLE] (
    [TITLEID] int IDENTITY(1,1) NOT NULL,
    [DESCRIPTION] nvarchar(10)  NULL
);
GO

-- Creating table 'G_TRADE'
CREATE TABLE [dbo].[G_TRADE] (
    [TradeId] int IDENTITY(1,1) NOT NULL,
    [Description] nvarchar(50)  NULL
);
GO

-- Creating table 'g_BankFacility'
CREATE TABLE [dbo].[g_BankFacility] (
    [BankId] int IDENTITY(1,1) NOT NULL,
    [DESCRIPTION] nvarchar(30)  NULL
);
GO

-- Creating table 'g_benefit'
CREATE TABLE [dbo].[g_benefit] (
    [BenefitId] int IDENTITY(1,1) NOT NULL,
    [DESCRIPTION] nvarchar(65)  NULL
);
GO

-- Creating table 'g_HouseIncome'
CREATE TABLE [dbo].[g_HouseIncome] (
    [IncomeId] int IDENTITY(1,1) NOT NULL,
    [DESCRIPTION] nvarchar(30)  NULL
);
GO

-- Creating table 'G_LANGUAGES'
CREATE TABLE [dbo].[G_LANGUAGES] (
    [LANGUAGEID] int  NOT NULL,
    [LANGUAGE] varchar(50)  NOT NULL
);
GO

-- Creating table 'G_OTHERCOMMUNICATION'
CREATE TABLE [dbo].[G_OTHERCOMMUNICATION] (
    [OTHERCOMMUNICATIONID] int IDENTITY(1,1) NOT NULL,
    [DESCRIPTION] nvarchar(60)  NULL
);
GO

-- Creating table 'G_PREFEREDCONTACT'
CREATE TABLE [dbo].[G_PREFEREDCONTACT] (
    [PREFEREDCONTACTID] int IDENTITY(1,1) NOT NULL,
    [DESCRIPTION] nvarchar(70)  NULL
);
GO

-- Creating table 'G_SEXUALORIENTATION'
CREATE TABLE [dbo].[G_SEXUALORIENTATION] (
    [SEXUALORIENTATIONID] int IDENTITY(1,1) NOT NULL,
    [DESCRIPTION] nvarchar(60)  NULL
);
GO

-- Creating table 'G_COMMUNICATION'
CREATE TABLE [dbo].[G_COMMUNICATION] (
    [COMMUNICATIONID] int IDENTITY(1,1) NOT NULL,
    [DESCRIPTION] nvarchar(60)  NULL
);
GO

-- Creating table 'P__PROPERTY'
CREATE TABLE [dbo].[P__PROPERTY] (
    [PROPERTYID] nvarchar(20)  NOT NULL,
    [PATCH] int  NULL,
    [DEVELOPMENTID] int  NULL,
    [BLOCKID] int  NULL,
    [HOUSENUMBER] nvarchar(50)  NULL,
    [FLATNUMBER] nvarchar(50)  NULL,
    [ADDRESS1] nvarchar(100)  NULL,
    [ADDRESS2] nvarchar(100)  NULL,
    [ADDRESS3] nvarchar(100)  NULL,
    [TOWNCITY] nvarchar(80)  NULL,
    [COUNTY] nvarchar(50)  NULL,
    [POSTCODE] nvarchar(10)  NULL,
    [STATUS] int  NULL,
    [SUBSTATUS] int  NULL,
    [AVDATE] datetime  NULL,
    [PROPERTYTYPE] int  NULL,
    [ASSETTYPE] int  NULL,
    [STOCKTYPE] int  NULL,
    [PROPERTYLEVEL] int  NULL,
    [DATEBUILT] datetime  NULL,
    [VIEWINGDATE] datetime  NULL,
    [LETTINGDATE] datetime  NULL,
    [RIGHTTOBUY] int  NULL,
    [UNITDESC] nvarchar(200)  NULL,
    [PRACTICALCOMPLETIONDATE] datetime  NULL,
    [HOUSINGOFFICER] int  NULL,
    [DEFECTSPERIODEND] datetime  NULL,
    [DATETIMESTAMP] datetime  NOT NULL,
    [SAP] int  NULL,
    [OWNERSHIP] int  NULL,
    [MINPURCHASE] float  NULL,
    [PURCHASELEVEL] float  NULL,
    [DWELLINGTYPE] int  NULL,
    [NROSHASSETTYPEMAIN] int  NULL,
    [NROSHASSETTYPESUB] int  NULL,
    [FUELTYPE] smallint  NULL,
    [PropertyPicId] int  NULL,
    [SCHEMEID] int  NULL,
    [PropertyValue] float  NULL,
    [IsTemplate] bit  NULL,
    [TemplateName] nvarchar(200)  NULL,
    [PhaseId] int  NULL,
    [LeaseStart] datetime  NULL,
    [LeaseEnd] datetime  NULL,
    [FloodingRisk] bit  NULL,
    [DateAcquired] datetime  NULL,
    [viewingcommencement] datetime  NULL,
    [Valuationdate] datetime  NULL,
    [GroundRent] nvarchar(200)  NULL,
    [LandLord] nvarchar(200)  NULL,
    [RentReview] datetime  NULL,
    [DeedLocation] varchar(200)  NULL,
    [TitleNumber] nvarchar(200)  NULL,
    [TENURETYPE] int  NULL
);
GO

-- Creating table 'PA_PROPERTY_ATTRIBUTES'
CREATE TABLE [dbo].[PA_PROPERTY_ATTRIBUTES] (
    [ATTRIBUTEID] int IDENTITY(1,1) NOT NULL,
    [PROPERTYID] varchar(20)  NULL,
    [ITEMPARAMID] int  NULL,
    [PARAMETERVALUE] varchar(1000)  NULL,
    [VALUEID] bigint  NULL,
    [UPDATEDON] datetime  NULL,
    [UPDATEDBY] int  NULL,
    [IsCheckBoxSelected] bit  NULL,
    [IsUpdated] bit  NOT NULL,
    [SchemeId] int  NULL,
    [BlockId] int  NULL
);
GO

-- Creating table 'P_FINANCIAL'
CREATE TABLE [dbo].[P_FINANCIAL] (
    [PROPERTYID] nvarchar(20)  NULL,
    [NOMINATINGBODY] nvarchar(100)  NULL,
    [FUNDINGAUTHORITY] int  NULL,
    [RENT] decimal(19,4)  NULL,
    [SERVICES] decimal(19,4)  NULL,
    [COUNCILTAX] decimal(19,4)  NULL,
    [WATERRATES] decimal(19,4)  NULL,
    [INELIGSERV] decimal(19,4)  NULL,
    [SUPPORTEDSERVICES] decimal(19,4)  NULL,
    [GARAGE] decimal(19,4)  NULL,
    [TOTALRENT] decimal(19,4)  NULL,
    [RENTTYPE] int  NULL,
    [OLDTOTAL] decimal(19,4)  NULL,
    [RENTTYPEOLD] nvarchar(50)  NULL,
    [DATERENTSET] datetime  NULL,
    [RENTEFFECTIVE] datetime  NULL,
    [TARGETRENT] decimal(19,4)  NULL,
    [YIELD] decimal(19,4)  NULL,
    [CAPITALVALUE] decimal(19,4)  NULL,
    [INSURANCEVALUE] decimal(19,4)  NULL,
    [OMVST] decimal(19,4)  NULL,
    [EUV] decimal(19,4)  NULL,
    [OMV] decimal(19,4)  NULL,
    [CHARGE] int  NULL,
    [CHARGEVALUE] decimal(19,4)  NULL,
    [NORENTCHANGE] int  NULL,
    [TARGETRENTSET] int  NULL,
    [PFUSERID] int  NULL,
    [FRApplicationDate] datetime  NULL,
    [FRRegistrationDate] datetime  NULL,
    [FREffectDate] datetime  NULL,
    [FRStartDate] datetime  NULL,
    [FRRegNum] nvarchar(50)  NULL,
    [FRRegNextDue] datetime  NULL,
    [PFTIMESTAMP] datetime  NOT NULL,
    [RENTFREQUENCY] int  NULL,
    [MarketRent] decimal(19,4)  NULL,
    [AffordableRent] decimal(19,4)  NULL,
    [SocialRent] decimal(19,4)  NULL,
    [MarketRentDate] datetime  NULL,
    [AffordableRentDate] datetime  NULL,
    [SocialRentDate] datetime  NULL,
    [ChargeValueDate] datetime  NULL,
    [OMVSTDate] datetime  NULL,
    [EUVDate] datetime  NULL,
    [OMVDate] datetime  NULL,
    [InsuranceValuedate] datetime  NULL,
    [YieldDate] datetime  NULL
);
GO

-- Creating table 'P_SUBSTATUS'
CREATE TABLE [dbo].[P_SUBSTATUS] (
    [SUBSTATUSID] int IDENTITY(1,1) NOT NULL,
    [DESCRIPTION] nvarchar(50)  NULL,
    [STATUSID] int  NULL,
    [NROSH_CODE_LOSS] nvarchar(50)  NULL,
    [NROSH_CODE_VACANCYSTATUS] nvarchar(50)  NULL
);
GO

-- Creating table 'PA_PROPERTY_ITEM_IMAGES'
CREATE TABLE [dbo].[PA_PROPERTY_ITEM_IMAGES] (
    [SID] int IDENTITY(1,1) NOT NULL,
    [PROPERTYID] nvarchar(20)  NULL,
    [ItemId] int  NULL,
    [ImagePath] nvarchar(400)  NULL,
    [ImageName] nvarchar(50)  NULL,
    [CreatedOn] datetime  NULL,
    [CreatedBy] int  NULL,
    [Title] varchar(500)  NULL,
    [ImageIdentifier] nvarchar(64)  NULL,
    [SchemeId] int  NULL,
    [BlockId] int  NULL,
    [Isdefault] bit  NULL
);
GO

-- Creating table 'PA_ITEM_PARAMETER'
CREATE TABLE [dbo].[PA_ITEM_PARAMETER] (
    [ItemParamID] int IDENTITY(1,1) NOT NULL,
    [ItemId] int  NULL,
    [ParameterId] int  NULL,
    [IsActive] bit  NULL,
    [SchemeId] int  NULL,
    [BlockId] int  NULL
);
GO

-- Creating table 'PA_PARAMETER'
CREATE TABLE [dbo].[PA_PARAMETER] (
    [ParameterID] int IDENTITY(1,1) NOT NULL,
    [ParameterName] varchar(50)  NOT NULL,
    [DataType] varchar(50)  NULL,
    [ControlType] varchar(50)  NULL,
    [IsDate] bit  NOT NULL,
    [ParameterSorder] int  NULL,
    [IsActive] bit  NULL,
    [ShowInApp] bit  NULL,
    [ShowInAccomodation] bit  NULL
);
GO

-- Creating table 'P_SCHEME'
CREATE TABLE [dbo].[P_SCHEME] (
    [SCHEMEID] int IDENTITY(1,1) NOT NULL,
    [SCHEMENAME] nvarchar(500)  NOT NULL,
    [PHASEID] int  NULL,
    [DEVELOPMENTID] int  NULL,
    [SCHEMECODE] nvarchar(100)  NULL
);
GO

-- Creating table 'P_PROPERTYTYPE'
CREATE TABLE [dbo].[P_PROPERTYTYPE] (
    [PROPERTYTYPEID] int IDENTITY(1,1) NOT NULL,
    [DESCRIPTION] nvarchar(50)  NULL
);
GO

-- Creating table 'PA_ITEM'
CREATE TABLE [dbo].[PA_ITEM] (
    [ItemID] int IDENTITY(1,1) NOT NULL,
    [AreaID] int  NOT NULL,
    [ItemName] varchar(50)  NOT NULL,
    [ItemSorder] int  NULL,
    [IsActive] bit  NULL,
    [ParentItemId] int  NULL,
    [ShowInApp] bit  NULL,
    [ShowListView] bit  NULL,
    [IsForSchemeBlock] bit  NULL
);
GO

-- Creating table 'C_ADDRESSTYPE'
CREATE TABLE [dbo].[C_ADDRESSTYPE] (
    [ADDRESSTYPEID] int IDENTITY(1,1) NOT NULL,
    [DESCRIPTION] nvarchar(100)  NULL,
    [CAREOF] bit  NOT NULL
);
GO

-- Creating table 'G_YEAR'
CREATE TABLE [dbo].[G_YEAR] (
    [Sid] int IDENTITY(1,1) NOT NULL,
    [Year] int  NULL
);
GO

-- Creating table 'G_YESNO_UNKNOWN'
CREATE TABLE [dbo].[G_YESNO_UNKNOWN] (
    [YESNOID] tinyint IDENTITY(1,1) NOT NULL,
    [DESCRIPTION] nvarchar(20)  NULL
);
GO

-- Creating table 'C_ACTION'
CREATE TABLE [dbo].[C_ACTION] (
    [ITEMACTIONID] int IDENTITY(1,1) NOT NULL,
    [ITEMID] int  NULL,
    [DESCRIPTION] nvarchar(35)  NULL,
    [STATUS] int  NULL,
    [EMPLOYEEONLY] nvarchar(50)  NULL,
    [CONTRACTORONLY] nvarchar(50)  NULL,
    [SORTORDER] int  NULL,
    [POACTION] nvarchar(10)  NULL
);
GO

-- Creating table 'G_CUSTOMERNATIONALITY'
CREATE TABLE [dbo].[G_CUSTOMERNATIONALITY] (
    [NATID] int IDENTITY(1,1) NOT NULL,
    [DESCRIPTION] nvarchar(200)  NULL
);
GO

-- Creating table 'G_DISABILITY'
CREATE TABLE [dbo].[G_DISABILITY] (
    [DISABILITYID] int IDENTITY(1,1) NOT NULL,
    [DESCRIPTION] nvarchar(60)  NULL
);
GO

-- Creating table 'G_ETHNICITY'
CREATE TABLE [dbo].[G_ETHNICITY] (
    [ETHID] int  NOT NULL,
    [DESCRIPTION] nvarchar(200)  NULL
);
GO

-- Creating table 'G_RELIGION'
CREATE TABLE [dbo].[G_RELIGION] (
    [RELIGIONID] int IDENTITY(1,1) NOT NULL,
    [DESCRIPTION] nvarchar(60)  NULL
);
GO

-- Creating table 'G_SUPPORTAGENCIES'
CREATE TABLE [dbo].[G_SUPPORTAGENCIES] (
    [AGID] int IDENTITY(1,1) NOT NULL,
    [DESCRIPTION] nvarchar(100)  NULL
);
GO

-- Creating table 'G_YESNO'
CREATE TABLE [dbo].[G_YESNO] (
    [YESNOID] int IDENTITY(1,1) NOT NULL,
    [DESCRIPTION] varchar(10)  NULL
);
GO

-- Creating table 'P_BLOCK'
CREATE TABLE [dbo].[P_BLOCK] (
    [BLOCKID] int IDENTITY(1,1) NOT NULL,
    [BLOCKNAME] nvarchar(100)  NULL,
    [DEVELOPMENTID] int  NULL,
    [ADDRESS1] nvarchar(100)  NULL,
    [ADDRESS2] nvarchar(100)  NULL,
    [ADDRESS3] nvarchar(100)  NULL,
    [TOWNCITY] nvarchar(100)  NULL,
    [COUNTY] nvarchar(100)  NULL,
    [POSTCODE] nvarchar(10)  NULL,
    [COMPLETIONDATE] datetime  NULL,
    [SchemeId] int  NULL,
    [PhaseId] int  NULL,
    [NoOfProperties] int  NULL,
    [Ownership] int  NULL,
    [BlockReference] varchar(200)  NULL,
    [IsTemplate] bit  NULL
);
GO

-- Creating table 'P_DEVELOPMENTTYPE'
CREATE TABLE [dbo].[P_DEVELOPMENTTYPE] (
    [DEVELOPMENTTYPEID] int IDENTITY(1,1) NOT NULL,
    [DESCRIPTION] nvarchar(100)  NULL,
    [NROSH_CODE] nvarchar(120)  NULL
);
GO

-- Creating table 'P_PHASE'
CREATE TABLE [dbo].[P_PHASE] (
    [PHASEID] int IDENTITY(1,1) NOT NULL,
    [ConstructionStart] datetime  NULL,
    [AnticipatedCompletion] datetime  NULL,
    [ActualCompletion] datetime  NULL,
    [HandoverintoManagement] datetime  NULL,
    [DEVELOPMENTID] int  NULL,
    [PhaseName] varchar(50)  NOT NULL
);
GO

-- Creating table 'P_STATUS'
CREATE TABLE [dbo].[P_STATUS] (
    [STATUSID] int IDENTITY(1,1) NOT NULL,
    [DESCRIPTION] nvarchar(50)  NULL
);
GO

-- Creating table 'PDR_DEVELOPMENT'
CREATE TABLE [dbo].[PDR_DEVELOPMENT] (
    [DEVELOPMENTID] int IDENTITY(1,1) NOT NULL,
    [PATCHID] int  NULL,
    [DEVELOPMENTNAME] nvarchar(100)  NULL,
    [DEVELOPMENTTYPE] int  NULL,
    [STARTDATE] datetime  NULL,
    [TARGETDATE] datetime  NULL,
    [ACTUALDATE] datetime  NULL,
    [DEFECTSPERIOD] datetime  NULL,
    [NUMBEROFUNITS] float  NULL,
    [USETYPE] int  NULL,
    [SUITABLEFOR] int  NULL,
    [LOCALAUTHORITY] int  NULL,
    [LEADDEVOFFICER] int  NULL,
    [SUPPORTDEVOFFICER] int  NULL,
    [PROJECTMANAGER] nvarchar(100)  NULL,
    [CONTRACTOR] int  NULL,
    [INSURANCEVALUE] decimal(19,4)  NULL,
    [AQUISITIONCOST] decimal(19,4)  NULL,
    [CONTRACTORCOST] decimal(19,4)  NULL,
    [OTHERCOST] decimal(19,4)  NULL,
    [SCHEMECOST] decimal(19,4)  NULL,
    [DEVELOPMENTADMIN] decimal(19,4)  NULL,
    [INTERESTCOST] decimal(19,4)  NULL,
    [BALANCE] decimal(19,4)  NULL,
    [HAG] decimal(19,4)  NULL,
    [MORTGAGE] decimal(19,4)  NULL,
    [BORROWINGRATE] decimal(19,4)  NULL,
    [NETTCOST] decimal(19,4)  NULL,
    [SURVEYOR] int  NULL,
    [SUPPORTEDCLIENTS] int  NULL,
    [ADDRESS1] varchar(500)  NULL,
    [ADDRESS2] varchar(500)  NULL,
    [TOWN] varchar(50)  NULL,
    [COUNTY] varchar(50)  NULL,
    [Architect] varchar(50)  NULL,
    [DefectsPeriodEnd] datetime  NULL,
    [LandValue] float  NULL,
    [FundingSource] int  NULL,
    [GrantAmount] float  NULL,
    [BorrowedAmount] float  NULL,
    [OutlinePlanningApplication] datetime  NULL,
    [DetailedPlanningApplication] datetime  NULL,
    [OutlinePlanningApproval] datetime  NULL,
    [DetailedPlanningApproval] datetime  NULL,
    [PostCode] varchar(50)  NULL,
    [PurchaseDate] datetime  NULL,
    [DEVELOPMENTSTATUS] int  NULL,
    [LandPurchasePrice] float  NULL,
    [ValueDate] datetime  NULL
);
GO

-- Creating table 'P_ASSETTYPE'
CREATE TABLE [dbo].[P_ASSETTYPE] (
    [ASSETTYPEID] int IDENTITY(1,1) NOT NULL,
    [DESCRIPTION] nvarchar(50)  NULL
);
GO

-- Creating table 'C_ADDRESS'
CREATE TABLE [dbo].[C_ADDRESS] (
    [ADDRESSID] int IDENTITY(1,1) NOT NULL,
    [CUSTOMERID] int  NULL,
    [HOUSENUMBER] nvarchar(50)  NULL,
    [ADDRESS1] nvarchar(100)  NULL,
    [ADDRESS2] nvarchar(100)  NULL,
    [ADDRESS3] nvarchar(100)  NULL,
    [TOWNCITY] nvarchar(100)  NULL,
    [POSTCODE] nvarchar(20)  NULL,
    [COUNTY] nvarchar(100)  NULL,
    [TEL] nvarchar(20)  NULL,
    [MOBILE] nvarchar(20)  NULL,
    [FAX] nvarchar(20)  NULL,
    [EMAIL] nvarchar(150)  NULL,
    [ISDEFAULT] int  NULL,
    [ADDRESSTYPE] int  NULL,
    [ISPOWEROFATTORNEY] int  NULL,
    [UPDATEDBYZ] int  NULL,
    [CONTACTFIRSTNAME] nvarchar(200)  NULL,
    [CONTACTSURNAME] nvarchar(200)  NULL,
    [RELATIONSHIP] nvarchar(1000)  NULL,
    [CONTACTDOB] datetime  NULL,
    [TELWORK] nvarchar(20)  NULL,
    [TELRELATIVE] nvarchar(20)  NULL,
    [TELRELATIONSHIP] nvarchar(1000)  NULL,
    [MOBILE2] nvarchar(20)  NULL,
    [AppVersion] nvarchar(100)  NULL,
    [CreatedOnApp] datetime  NULL,
    [CreatedOnServer] datetime  NULL,
    [LastModifiedOnApp] datetime  NULL,
    [LastModifiedOnServer] datetime  NULL
);
GO

-- Creating table 'AS_USER'
CREATE TABLE [dbo].[AS_USER] (
    [UserId] int IDENTITY(1,1) NOT NULL,
    [UserTypeID] smallint  NOT NULL,
    [EmployeeId] int  NOT NULL,
    [IsActive] bit  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [CreatedBy] int  NOT NULL,
    [ModifiedDate] datetime  NULL,
    [ModifiedBy] int  NULL
);
GO

-- Creating table 'AS_USER_INSPECTIONTYPE'
CREATE TABLE [dbo].[AS_USER_INSPECTIONTYPE] (
    [UserInspectionTypeId] int IDENTITY(1,1) NOT NULL,
    [EmployeeId] int  NOT NULL,
    [InspectionTypeID] smallint  NULL
);
GO

-- Creating table 'AS_USERTYPE'
CREATE TABLE [dbo].[AS_USERTYPE] (
    [UserTypeID] smallint IDENTITY(1,1) NOT NULL,
    [Description] nvarchar(50)  NULL
);
GO

-- Creating table 'FLS_CustomerCurrentHome'
CREATE TABLE [dbo].[FLS_CustomerCurrentHome] (
    [CurrentHomeId] int IDENTITY(1,1) NOT NULL,
    [CustomerId] int  NOT NULL,
    [LandlordTypeId] int  NULL,
    [LandlordName] nvarchar(500)  NULL,
    [HouseTypeId] int  NULL,
    [floor] nvarchar(50)  NULL,
    [LivingWithFamily] int  NULL,
    [NoOfBedrooms] int  NULL,
    [TenancyStartDate] datetime  NULL,
    [TenancyEndDate] datetime  NULL,
    [PreviousLandLoardName] nvarchar(500)  NULL
);
GO

-- Creating table 'FLS_HouseType'
CREATE TABLE [dbo].[FLS_HouseType] (
    [HouseTypeId] int IDENTITY(1,1) NOT NULL,
    [Title] nvarchar(500)  NOT NULL
);
GO

-- Creating table 'FLS_LandLordType'
CREATE TABLE [dbo].[FLS_LandLordType] (
    [LandLordTypeId] int IDENTITY(1,1) NOT NULL,
    [Title] nvarchar(500)  NOT NULL,
    [IsActive] bit  NOT NULL
);
GO

-- Creating table 'FLS_Viewings'
CREATE TABLE [dbo].[FLS_Viewings] (
    [ViewingId] int IDENTITY(1,1) NOT NULL,
    [CustomerId] int  NOT NULL,
    [PropertyId] nvarchar(20)  NOT NULL,
    [HousingOfficer] int  NOT NULL,
    [StatusId] int  NOT NULL,
    [ViewingDate] datetime  NOT NULL,
    [StartTime] nvarchar(10)  NOT NULL,
    [EndTime] nvarchar(10)  NULL,
    [Duration] float  NULL,
    [ActualEndTime] nvarchar(10)  NULL,
    [CreatedBy] int  NOT NULL,
    [IsActive] bit  NOT NULL,
    [RecordingSource] nvarchar(50)  NULL,
    [AppVersion] nvarchar(100)  NULL,
    [CreatedOnApp] datetime  NULL,
    [CreatedOnServer] datetime  NULL,
    [LastModifiedOnApp] datetime  NULL,
    [LastModifiedOnServer] datetime  NULL
);
GO

-- Creating table 'FLS_ViewingsHistory'
CREATE TABLE [dbo].[FLS_ViewingsHistory] (
    [ViewingHistoryId] int IDENTITY(1,1) NOT NULL,
    [ViewingId] int  NOT NULL,
    [CustomerId] int  NOT NULL,
    [PropertyId] nvarchar(20)  NOT NULL,
    [HousingOfficer] int  NOT NULL,
    [StatusId] int  NOT NULL,
    [ViewingDate] datetime  NOT NULL,
    [StartTime] nvarchar(10)  NOT NULL,
    [EndTime] nvarchar(10)  NULL,
    [Duration] float  NULL,
    [ActualEndTime] nvarchar(10)  NULL,
    [CreatedBy] int  NOT NULL,
    [IsActive] bit  NOT NULL,
    [RecordingSource] nvarchar(50)  NULL,
    [AppVersion] nvarchar(100)  NULL,
    [CreatedOnApp] datetime  NULL,
    [CreatedOnServer] datetime  NULL,
    [LastModifiedOnApp] datetime  NULL,
    [LastModifiedOnServer] datetime  NULL
);
GO

-- Creating table 'FLS_ViewingStatus'
CREATE TABLE [dbo].[FLS_ViewingStatus] (
    [StatusId] int IDENTITY(1,1) NOT NULL,
    [Title] nvarchar(100)  NOT NULL,
    [IsActive] bit  NOT NULL
);
GO

-- Creating table 'P_INSPECTIONTYPE'
CREATE TABLE [dbo].[P_INSPECTIONTYPE] (
    [InspectionTypeID] smallint IDENTITY(1,1) NOT NULL,
    [Description] nvarchar(50)  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [LoginID] in table 'AC_LOGIN_HISTORY'
ALTER TABLE [dbo].[AC_LOGIN_HISTORY]
ADD CONSTRAINT [PK_AC_LOGIN_HISTORY]
    PRIMARY KEY CLUSTERED ([LoginID] ASC);
GO

-- Creating primary key on [LOGINID] in table 'AC_LOGINS'
ALTER TABLE [dbo].[AC_LOGINS]
ADD CONSTRAINT [PK_AC_LOGINS]
    PRIMARY KEY CLUSTERED ([LOGINID] ASC);
GO

-- Creating primary key on [EMPLOYEEID] in table 'E__EMPLOYEE'
ALTER TABLE [dbo].[E__EMPLOYEE]
ADD CONSTRAINT [PK_E__EMPLOYEE]
    PRIMARY KEY CLUSTERED ([EMPLOYEEID] ASC);
GO

-- Creating primary key on [JOBDETAILSID] in table 'E_JOBDETAILS'
ALTER TABLE [dbo].[E_JOBDETAILS]
ADD CONSTRAINT [PK_E_JOBDETAILS]
    PRIMARY KEY CLUSTERED ([JOBDETAILSID] ASC);
GO

-- Creating primary key on [TEAMID] in table 'E_TEAM'
ALTER TABLE [dbo].[E_TEAM]
ADD CONSTRAINT [PK_E_TEAM]
    PRIMARY KEY CLUSTERED ([TEAMID] ASC);
GO

-- Creating primary key on [TEAMCODE] in table 'G_TEAMCODES'
ALTER TABLE [dbo].[G_TEAMCODES]
ADD CONSTRAINT [PK_G_TEAMCODES]
    PRIMARY KEY CLUSTERED ([TEAMCODE] ASC);
GO

-- Creating primary key on [CategoryId] in table 'C_RISK_CATEGORY'
ALTER TABLE [dbo].[C_RISK_CATEGORY]
ADD CONSTRAINT [PK_C_RISK_CATEGORY]
    PRIMARY KEY CLUSTERED ([CategoryId] ASC);
GO

-- Creating primary key on [SubCategoryId] in table 'C_RISK_SUBCATEGORY'
ALTER TABLE [dbo].[C_RISK_SUBCATEGORY]
ADD CONSTRAINT [PK_C_RISK_SUBCATEGORY]
    PRIMARY KEY CLUSTERED ([SubCategoryId] ASC);
GO

-- Creating primary key on [CUSTOMERTENANCYID] in table 'C_CUSTOMERTENANCY'
ALTER TABLE [dbo].[C_CUSTOMERTENANCY]
ADD CONSTRAINT [PK_C_CUSTOMERTENANCY]
    PRIMARY KEY CLUSTERED ([CUSTOMERTENANCYID] ASC);
GO

-- Creating primary key on [CUSTOMERTYPEID] in table 'C_CUSTOMERTYPE'
ALTER TABLE [dbo].[C_CUSTOMERTYPE]
ADD CONSTRAINT [PK_C_CUSTOMERTYPE]
    PRIMARY KEY CLUSTERED ([CUSTOMERTYPEID] ASC);
GO

-- Creating primary key on [ACTIONID] in table 'C_LETTERACTION'
ALTER TABLE [dbo].[C_LETTERACTION]
ADD CONSTRAINT [PK_C_LETTERACTION]
    PRIMARY KEY CLUSTERED ([ACTIONID] ASC);
GO

-- Creating primary key on [REFERRALHISTORYID] in table 'C_REFERRAL'
ALTER TABLE [dbo].[C_REFERRAL]
ADD CONSTRAINT [PK_C_REFERRAL]
    PRIMARY KEY CLUSTERED ([REFERRALHISTORYID] ASC);
GO

-- Creating primary key on [LETTERID] in table 'C_STANDARDLETTERS'
ALTER TABLE [dbo].[C_STANDARDLETTERS]
ADD CONSTRAINT [PK_C_STANDARDLETTERS]
    PRIMARY KEY CLUSTERED ([LETTERID] ASC);
GO

-- Creating primary key on [ITEMSTATUSID] in table 'C_STATUS'
ALTER TABLE [dbo].[C_STATUS]
ADD CONSTRAINT [PK_C_STATUS]
    PRIMARY KEY CLUSTERED ([ITEMSTATUSID] ASC);
GO

-- Creating primary key on [TENANCYID] in table 'C_TENANCY'
ALTER TABLE [dbo].[C_TENANCY]
ADD CONSTRAINT [PK_C_TENANCY]
    PRIMARY KEY CLUSTERED ([TENANCYID] ASC);
GO

-- Creating primary key on [TENANCYTYPEID] in table 'C_TENANCYTYPE'
ALTER TABLE [dbo].[C_TENANCYTYPE]
ADD CONSTRAINT [PK_C_TENANCYTYPE]
    PRIMARY KEY CLUSTERED ([TENANCYTYPEID] ASC);
GO

-- Creating primary key on [TERMINATIONHISTORYID] in table 'C_TERMINATION'
ALTER TABLE [dbo].[C_TERMINATION]
ADD CONSTRAINT [PK_C_TERMINATION]
    PRIMARY KEY CLUSTERED ([TERMINATIONHISTORYID] ASC);
GO

-- Creating primary key on [REASONID] in table 'C_TERMINATION_REASON'
ALTER TABLE [dbo].[C_TERMINATION_REASON]
ADD CONSTRAINT [PK_C_TERMINATION_REASON]
    PRIMARY KEY CLUSTERED ([REASONID] ASC);
GO

-- Creating primary key on [VULNERABILITYHISTORYID] in table 'C_VULNERABILITY'
ALTER TABLE [dbo].[C_VULNERABILITY]
ADD CONSTRAINT [PK_C_VULNERABILITY]
    PRIMARY KEY CLUSTERED ([VULNERABILITYHISTORYID] ASC);
GO

-- Creating primary key on [CategoryId] in table 'C_VULNERABILITY_CATEGORY'
ALTER TABLE [dbo].[C_VULNERABILITY_CATEGORY]
ADD CONSTRAINT [PK_C_VULNERABILITY_CATEGORY]
    PRIMARY KEY CLUSTERED ([CategoryId] ASC);
GO

-- Creating primary key on [SubCategoryId] in table 'C_VULNERABILITY_SUBCATEGORY'
ALTER TABLE [dbo].[C_VULNERABILITY_SUBCATEGORY]
ADD CONSTRAINT [PK_C_VULNERABILITY_SUBCATEGORY]
    PRIMARY KEY CLUSTERED ([SubCategoryId] ASC);
GO

-- Creating primary key on [ECONOMICSTATUSID] in table 'C_ECONOMICSTATUS'
ALTER TABLE [dbo].[C_ECONOMICSTATUS]
ADD CONSTRAINT [PK_C_ECONOMICSTATUS]
    PRIMARY KEY CLUSTERED ([ECONOMICSTATUSID] ASC);
GO

-- Creating primary key on [EMPLOYMENTSTATUSID] in table 'C_EMPLOYMENTSTATUS'
ALTER TABLE [dbo].[C_EMPLOYMENTSTATUS]
ADD CONSTRAINT [PK_C_EMPLOYMENTSTATUS]
    PRIMARY KEY CLUSTERED ([EMPLOYMENTSTATUSID] ASC);
GO

-- Creating primary key on [RELATIONSHIPID] in table 'C_RELATIONSHIP'
ALTER TABLE [dbo].[C_RELATIONSHIP]
ADD CONSTRAINT [PK_C_RELATIONSHIP]
    PRIMARY KEY CLUSTERED ([RELATIONSHIPID] ASC);
GO

-- Creating primary key on [BHDATE] in table 'G_BANKHOLIDAYS'
ALTER TABLE [dbo].[G_BANKHOLIDAYS]
ADD CONSTRAINT [PK_G_BANKHOLIDAYS]
    PRIMARY KEY CLUSTERED ([BHDATE] ASC);
GO

-- Creating primary key on [INTERNETACCESSID] in table 'G_INTERNETACCESS'
ALTER TABLE [dbo].[G_INTERNETACCESS]
ADD CONSTRAINT [PK_G_INTERNETACCESS]
    PRIMARY KEY CLUSTERED ([INTERNETACCESSID] ASC);
GO

-- Creating primary key on [LOCALAUTHORITYID] in table 'G_LOCALAUTHORITY'
ALTER TABLE [dbo].[G_LOCALAUTHORITY]
ADD CONSTRAINT [PK_G_LOCALAUTHORITY]
    PRIMARY KEY CLUSTERED ([LOCALAUTHORITYID] ASC);
GO

-- Creating primary key on [MARITALSTATUSID] in table 'G_MARITALSTATUS'
ALTER TABLE [dbo].[G_MARITALSTATUS]
ADD CONSTRAINT [PK_G_MARITALSTATUS]
    PRIMARY KEY CLUSTERED ([MARITALSTATUSID] ASC);
GO

-- Creating primary key on [TITLEID] in table 'G_TITLE'
ALTER TABLE [dbo].[G_TITLE]
ADD CONSTRAINT [PK_G_TITLE]
    PRIMARY KEY CLUSTERED ([TITLEID] ASC);
GO

-- Creating primary key on [TradeId] in table 'G_TRADE'
ALTER TABLE [dbo].[G_TRADE]
ADD CONSTRAINT [PK_G_TRADE]
    PRIMARY KEY CLUSTERED ([TradeId] ASC);
GO

-- Creating primary key on [BankId] in table 'g_BankFacility'
ALTER TABLE [dbo].[g_BankFacility]
ADD CONSTRAINT [PK_g_BankFacility]
    PRIMARY KEY CLUSTERED ([BankId] ASC);
GO

-- Creating primary key on [BenefitId] in table 'g_benefit'
ALTER TABLE [dbo].[g_benefit]
ADD CONSTRAINT [PK_g_benefit]
    PRIMARY KEY CLUSTERED ([BenefitId] ASC);
GO

-- Creating primary key on [IncomeId] in table 'g_HouseIncome'
ALTER TABLE [dbo].[g_HouseIncome]
ADD CONSTRAINT [PK_g_HouseIncome]
    PRIMARY KEY CLUSTERED ([IncomeId] ASC);
GO

-- Creating primary key on [LANGUAGEID], [LANGUAGE] in table 'G_LANGUAGES'
ALTER TABLE [dbo].[G_LANGUAGES]
ADD CONSTRAINT [PK_G_LANGUAGES]
    PRIMARY KEY CLUSTERED ([LANGUAGEID], [LANGUAGE] ASC);
GO

-- Creating primary key on [OTHERCOMMUNICATIONID] in table 'G_OTHERCOMMUNICATION'
ALTER TABLE [dbo].[G_OTHERCOMMUNICATION]
ADD CONSTRAINT [PK_G_OTHERCOMMUNICATION]
    PRIMARY KEY CLUSTERED ([OTHERCOMMUNICATIONID] ASC);
GO

-- Creating primary key on [PREFEREDCONTACTID] in table 'G_PREFEREDCONTACT'
ALTER TABLE [dbo].[G_PREFEREDCONTACT]
ADD CONSTRAINT [PK_G_PREFEREDCONTACT]
    PRIMARY KEY CLUSTERED ([PREFEREDCONTACTID] ASC);
GO

-- Creating primary key on [SEXUALORIENTATIONID] in table 'G_SEXUALORIENTATION'
ALTER TABLE [dbo].[G_SEXUALORIENTATION]
ADD CONSTRAINT [PK_G_SEXUALORIENTATION]
    PRIMARY KEY CLUSTERED ([SEXUALORIENTATIONID] ASC);
GO

-- Creating primary key on [COMMUNICATIONID] in table 'G_COMMUNICATION'
ALTER TABLE [dbo].[G_COMMUNICATION]
ADD CONSTRAINT [PK_G_COMMUNICATION]
    PRIMARY KEY CLUSTERED ([COMMUNICATIONID] ASC);
GO

-- Creating primary key on [PROPERTYID] in table 'P__PROPERTY'
ALTER TABLE [dbo].[P__PROPERTY]
ADD CONSTRAINT [PK_P__PROPERTY]
    PRIMARY KEY CLUSTERED ([PROPERTYID] ASC);
GO

-- Creating primary key on [ATTRIBUTEID] in table 'PA_PROPERTY_ATTRIBUTES'
ALTER TABLE [dbo].[PA_PROPERTY_ATTRIBUTES]
ADD CONSTRAINT [PK_PA_PROPERTY_ATTRIBUTES]
    PRIMARY KEY CLUSTERED ([ATTRIBUTEID] ASC);
GO

-- Creating primary key on [PFTIMESTAMP] in table 'P_FINANCIAL'
ALTER TABLE [dbo].[P_FINANCIAL]
ADD CONSTRAINT [PK_P_FINANCIAL]
    PRIMARY KEY CLUSTERED ([PFTIMESTAMP] ASC);
GO

-- Creating primary key on [SUBSTATUSID] in table 'P_SUBSTATUS'
ALTER TABLE [dbo].[P_SUBSTATUS]
ADD CONSTRAINT [PK_P_SUBSTATUS]
    PRIMARY KEY CLUSTERED ([SUBSTATUSID] ASC);
GO

-- Creating primary key on [SID] in table 'PA_PROPERTY_ITEM_IMAGES'
ALTER TABLE [dbo].[PA_PROPERTY_ITEM_IMAGES]
ADD CONSTRAINT [PK_PA_PROPERTY_ITEM_IMAGES]
    PRIMARY KEY CLUSTERED ([SID] ASC);
GO

-- Creating primary key on [ItemParamID] in table 'PA_ITEM_PARAMETER'
ALTER TABLE [dbo].[PA_ITEM_PARAMETER]
ADD CONSTRAINT [PK_PA_ITEM_PARAMETER]
    PRIMARY KEY CLUSTERED ([ItemParamID] ASC);
GO

-- Creating primary key on [ParameterID] in table 'PA_PARAMETER'
ALTER TABLE [dbo].[PA_PARAMETER]
ADD CONSTRAINT [PK_PA_PARAMETER]
    PRIMARY KEY CLUSTERED ([ParameterID] ASC);
GO

-- Creating primary key on [SCHEMEID] in table 'P_SCHEME'
ALTER TABLE [dbo].[P_SCHEME]
ADD CONSTRAINT [PK_P_SCHEME]
    PRIMARY KEY CLUSTERED ([SCHEMEID] ASC);
GO

-- Creating primary key on [PROPERTYTYPEID] in table 'P_PROPERTYTYPE'
ALTER TABLE [dbo].[P_PROPERTYTYPE]
ADD CONSTRAINT [PK_P_PROPERTYTYPE]
    PRIMARY KEY CLUSTERED ([PROPERTYTYPEID] ASC);
GO

-- Creating primary key on [ItemID] in table 'PA_ITEM'
ALTER TABLE [dbo].[PA_ITEM]
ADD CONSTRAINT [PK_PA_ITEM]
    PRIMARY KEY CLUSTERED ([ItemID] ASC);
GO

-- Creating primary key on [ADDRESSTYPEID] in table 'C_ADDRESSTYPE'
ALTER TABLE [dbo].[C_ADDRESSTYPE]
ADD CONSTRAINT [PK_C_ADDRESSTYPE]
    PRIMARY KEY CLUSTERED ([ADDRESSTYPEID] ASC);
GO

-- Creating primary key on [Sid] in table 'G_YEAR'
ALTER TABLE [dbo].[G_YEAR]
ADD CONSTRAINT [PK_G_YEAR]
    PRIMARY KEY CLUSTERED ([Sid] ASC);
GO

-- Creating primary key on [YESNOID] in table 'G_YESNO_UNKNOWN'
ALTER TABLE [dbo].[G_YESNO_UNKNOWN]
ADD CONSTRAINT [PK_G_YESNO_UNKNOWN]
    PRIMARY KEY CLUSTERED ([YESNOID] ASC);
GO

-- Creating primary key on [ITEMACTIONID] in table 'C_ACTION'
ALTER TABLE [dbo].[C_ACTION]
ADD CONSTRAINT [PK_C_ACTION]
    PRIMARY KEY CLUSTERED ([ITEMACTIONID] ASC);
GO

-- Creating primary key on [NATID] in table 'G_CUSTOMERNATIONALITY'
ALTER TABLE [dbo].[G_CUSTOMERNATIONALITY]
ADD CONSTRAINT [PK_G_CUSTOMERNATIONALITY]
    PRIMARY KEY CLUSTERED ([NATID] ASC);
GO

-- Creating primary key on [DISABILITYID] in table 'G_DISABILITY'
ALTER TABLE [dbo].[G_DISABILITY]
ADD CONSTRAINT [PK_G_DISABILITY]
    PRIMARY KEY CLUSTERED ([DISABILITYID] ASC);
GO

-- Creating primary key on [ETHID] in table 'G_ETHNICITY'
ALTER TABLE [dbo].[G_ETHNICITY]
ADD CONSTRAINT [PK_G_ETHNICITY]
    PRIMARY KEY CLUSTERED ([ETHID] ASC);
GO

-- Creating primary key on [RELIGIONID] in table 'G_RELIGION'
ALTER TABLE [dbo].[G_RELIGION]
ADD CONSTRAINT [PK_G_RELIGION]
    PRIMARY KEY CLUSTERED ([RELIGIONID] ASC);
GO

-- Creating primary key on [AGID] in table 'G_SUPPORTAGENCIES'
ALTER TABLE [dbo].[G_SUPPORTAGENCIES]
ADD CONSTRAINT [PK_G_SUPPORTAGENCIES]
    PRIMARY KEY CLUSTERED ([AGID] ASC);
GO

-- Creating primary key on [YESNOID] in table 'G_YESNO'
ALTER TABLE [dbo].[G_YESNO]
ADD CONSTRAINT [PK_G_YESNO]
    PRIMARY KEY CLUSTERED ([YESNOID] ASC);
GO

-- Creating primary key on [BLOCKID] in table 'P_BLOCK'
ALTER TABLE [dbo].[P_BLOCK]
ADD CONSTRAINT [PK_P_BLOCK]
    PRIMARY KEY CLUSTERED ([BLOCKID] ASC);
GO

-- Creating primary key on [DEVELOPMENTTYPEID] in table 'P_DEVELOPMENTTYPE'
ALTER TABLE [dbo].[P_DEVELOPMENTTYPE]
ADD CONSTRAINT [PK_P_DEVELOPMENTTYPE]
    PRIMARY KEY CLUSTERED ([DEVELOPMENTTYPEID] ASC);
GO

-- Creating primary key on [PHASEID] in table 'P_PHASE'
ALTER TABLE [dbo].[P_PHASE]
ADD CONSTRAINT [PK_P_PHASE]
    PRIMARY KEY CLUSTERED ([PHASEID] ASC);
GO

-- Creating primary key on [STATUSID] in table 'P_STATUS'
ALTER TABLE [dbo].[P_STATUS]
ADD CONSTRAINT [PK_P_STATUS]
    PRIMARY KEY CLUSTERED ([STATUSID] ASC);
GO

-- Creating primary key on [DEVELOPMENTID] in table 'PDR_DEVELOPMENT'
ALTER TABLE [dbo].[PDR_DEVELOPMENT]
ADD CONSTRAINT [PK_PDR_DEVELOPMENT]
    PRIMARY KEY CLUSTERED ([DEVELOPMENTID] ASC);
GO

-- Creating primary key on [ASSETTYPEID] in table 'P_ASSETTYPE'
ALTER TABLE [dbo].[P_ASSETTYPE]
ADD CONSTRAINT [PK_P_ASSETTYPE]
    PRIMARY KEY CLUSTERED ([ASSETTYPEID] ASC);
GO

-- Creating primary key on [ADDRESSID] in table 'C_ADDRESS'
ALTER TABLE [dbo].[C_ADDRESS]
ADD CONSTRAINT [PK_C_ADDRESS]
    PRIMARY KEY CLUSTERED ([ADDRESSID] ASC);
GO

-- Creating primary key on [UserId] in table 'AS_USER'
ALTER TABLE [dbo].[AS_USER]
ADD CONSTRAINT [PK_AS_USER]
    PRIMARY KEY CLUSTERED ([UserId] ASC);
GO

-- Creating primary key on [UserInspectionTypeId] in table 'AS_USER_INSPECTIONTYPE'
ALTER TABLE [dbo].[AS_USER_INSPECTIONTYPE]
ADD CONSTRAINT [PK_AS_USER_INSPECTIONTYPE]
    PRIMARY KEY CLUSTERED ([UserInspectionTypeId] ASC);
GO

-- Creating primary key on [UserTypeID] in table 'AS_USERTYPE'
ALTER TABLE [dbo].[AS_USERTYPE]
ADD CONSTRAINT [PK_AS_USERTYPE]
    PRIMARY KEY CLUSTERED ([UserTypeID] ASC);
GO

-- Creating primary key on [CurrentHomeId] in table 'FLS_CustomerCurrentHome'
ALTER TABLE [dbo].[FLS_CustomerCurrentHome]
ADD CONSTRAINT [PK_FLS_CustomerCurrentHome]
    PRIMARY KEY CLUSTERED ([CurrentHomeId] ASC);
GO

-- Creating primary key on [HouseTypeId] in table 'FLS_HouseType'
ALTER TABLE [dbo].[FLS_HouseType]
ADD CONSTRAINT [PK_FLS_HouseType]
    PRIMARY KEY CLUSTERED ([HouseTypeId] ASC);
GO

-- Creating primary key on [LandLordTypeId] in table 'FLS_LandLordType'
ALTER TABLE [dbo].[FLS_LandLordType]
ADD CONSTRAINT [PK_FLS_LandLordType]
    PRIMARY KEY CLUSTERED ([LandLordTypeId] ASC);
GO

-- Creating primary key on [ViewingId] in table 'FLS_Viewings'
ALTER TABLE [dbo].[FLS_Viewings]
ADD CONSTRAINT [PK_FLS_Viewings]
    PRIMARY KEY CLUSTERED ([ViewingId] ASC);
GO

-- Creating primary key on [ViewingHistoryId] in table 'FLS_ViewingsHistory'
ALTER TABLE [dbo].[FLS_ViewingsHistory]
ADD CONSTRAINT [PK_FLS_ViewingsHistory]
    PRIMARY KEY CLUSTERED ([ViewingHistoryId] ASC);
GO

-- Creating primary key on [StatusId] in table 'FLS_ViewingStatus'
ALTER TABLE [dbo].[FLS_ViewingStatus]
ADD CONSTRAINT [PK_FLS_ViewingStatus]
    PRIMARY KEY CLUSTERED ([StatusId] ASC);
GO

-- Creating primary key on [InspectionTypeID] in table 'P_INSPECTIONTYPE'
ALTER TABLE [dbo].[P_INSPECTIONTYPE]
ADD CONSTRAINT [PK_P_INSPECTIONTYPE]
    PRIMARY KEY CLUSTERED ([InspectionTypeID] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [CategoryId] in table 'C_RISK_SUBCATEGORY'
ALTER TABLE [dbo].[C_RISK_SUBCATEGORY]
ADD CONSTRAINT [FK_C_RISK_CATEGORY_C_RISK_SUBCATEGORY]
    FOREIGN KEY ([CategoryId])
    REFERENCES [dbo].[C_RISK_CATEGORY]
        ([CategoryId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_C_RISK_CATEGORY_C_RISK_SUBCATEGORY'
CREATE INDEX [IX_FK_C_RISK_CATEGORY_C_RISK_SUBCATEGORY]
ON [dbo].[C_RISK_SUBCATEGORY]
    ([CategoryId]);
GO

-- Creating foreign key on [CategoryId] in table 'C_VULNERABILITY_SUBCATEGORY'
ALTER TABLE [dbo].[C_VULNERABILITY_SUBCATEGORY]
ADD CONSTRAINT [FK_C_VULNERABILITY_CATEGORY_C_VULNERABILITY_SUBCATEGORY]
    FOREIGN KEY ([CategoryId])
    REFERENCES [dbo].[C_VULNERABILITY_CATEGORY]
        ([CategoryId])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_C_VULNERABILITY_CATEGORY_C_VULNERABILITY_SUBCATEGORY'
CREATE INDEX [IX_FK_C_VULNERABILITY_CATEGORY_C_VULNERABILITY_SUBCATEGORY]
ON [dbo].[C_VULNERABILITY_SUBCATEGORY]
    ([CategoryId]);
GO

-- Creating foreign key on [PROPERTYID] in table 'P_FINANCIAL'
ALTER TABLE [dbo].[P_FINANCIAL]
ADD CONSTRAINT [FK_P_FINANCIAL_P__PROPERTY]
    FOREIGN KEY ([PROPERTYID])
    REFERENCES [dbo].[P__PROPERTY]
        ([PROPERTYID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_P_FINANCIAL_P__PROPERTY'
CREATE INDEX [IX_FK_P_FINANCIAL_P__PROPERTY]
ON [dbo].[P_FINANCIAL]
    ([PROPERTYID]);
GO

-- Creating foreign key on [ITEMPARAMID] in table 'PA_PROPERTY_ATTRIBUTES'
ALTER TABLE [dbo].[PA_PROPERTY_ATTRIBUTES]
ADD CONSTRAINT [FK_PA_PROPERTY_ATTRIBUTES_PA_ITEM_PARAMETER]
    FOREIGN KEY ([ITEMPARAMID])
    REFERENCES [dbo].[PA_ITEM_PARAMETER]
        ([ItemParamID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PA_PROPERTY_ATTRIBUTES_PA_ITEM_PARAMETER'
CREATE INDEX [IX_FK_PA_PROPERTY_ATTRIBUTES_PA_ITEM_PARAMETER]
ON [dbo].[PA_PROPERTY_ATTRIBUTES]
    ([ITEMPARAMID]);
GO

-- Creating foreign key on [ParameterId] in table 'PA_ITEM_PARAMETER'
ALTER TABLE [dbo].[PA_ITEM_PARAMETER]
ADD CONSTRAINT [FK_PA_ITEM_PARAMETER_PA_PARAMETER]
    FOREIGN KEY ([ParameterId])
    REFERENCES [dbo].[PA_PARAMETER]
        ([ParameterID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PA_ITEM_PARAMETER_PA_PARAMETER'
CREATE INDEX [IX_FK_PA_ITEM_PARAMETER_PA_PARAMETER]
ON [dbo].[PA_ITEM_PARAMETER]
    ([ParameterId]);
GO

-- Creating foreign key on [ItemId] in table 'PA_ITEM_PARAMETER'
ALTER TABLE [dbo].[PA_ITEM_PARAMETER]
ADD CONSTRAINT [FK_PA_ITEM_PARAMETER_PA_ITEM]
    FOREIGN KEY ([ItemId])
    REFERENCES [dbo].[PA_ITEM]
        ([ItemID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PA_ITEM_PARAMETER_PA_ITEM'
CREATE INDEX [IX_FK_PA_ITEM_PARAMETER_PA_ITEM]
ON [dbo].[PA_ITEM_PARAMETER]
    ([ItemId]);
GO

-- Creating foreign key on [ItemId] in table 'PA_PROPERTY_ITEM_IMAGES'
ALTER TABLE [dbo].[PA_PROPERTY_ITEM_IMAGES]
ADD CONSTRAINT [FK_PA_PROPERTY_ITEM_IMAGES_PA_ITEM]
    FOREIGN KEY ([ItemId])
    REFERENCES [dbo].[PA_ITEM]
        ([ItemID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PA_PROPERTY_ITEM_IMAGES_PA_ITEM'
CREATE INDEX [IX_FK_PA_PROPERTY_ITEM_IMAGES_PA_ITEM]
ON [dbo].[PA_PROPERTY_ITEM_IMAGES]
    ([ItemId]);
GO

-- Creating foreign key on [DEVELOPMENTID] in table 'P__PROPERTY'
ALTER TABLE [dbo].[P__PROPERTY]
ADD CONSTRAINT [FK_P__PROPERTY_PDR_DEVELOPMENT]
    FOREIGN KEY ([DEVELOPMENTID])
    REFERENCES [dbo].[PDR_DEVELOPMENT]
        ([DEVELOPMENTID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_P__PROPERTY_PDR_DEVELOPMENT'
CREATE INDEX [IX_FK_P__PROPERTY_PDR_DEVELOPMENT]
ON [dbo].[P__PROPERTY]
    ([DEVELOPMENTID]);
GO

-- Creating foreign key on [UserTypeID] in table 'AS_USER'
ALTER TABLE [dbo].[AS_USER]
ADD CONSTRAINT [FK_AS_USER_AS_USERTYPE]
    FOREIGN KEY ([UserTypeID])
    REFERENCES [dbo].[AS_USERTYPE]
        ([UserTypeID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AS_USER_AS_USERTYPE'
CREATE INDEX [IX_FK_AS_USER_AS_USERTYPE]
ON [dbo].[AS_USER]
    ([UserTypeID]);
GO

-- Creating foreign key on [InspectionTypeID] in table 'AS_USER_INSPECTIONTYPE'
ALTER TABLE [dbo].[AS_USER_INSPECTIONTYPE]
ADD CONSTRAINT [FK_AS_USER_INSPECTIONTYPE_P_INSPECTIONTYPE]
    FOREIGN KEY ([InspectionTypeID])
    REFERENCES [dbo].[P_INSPECTIONTYPE]
        ([InspectionTypeID])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AS_USER_INSPECTIONTYPE_P_INSPECTIONTYPE'
CREATE INDEX [IX_FK_AS_USER_INSPECTIONTYPE_P_INSPECTIONTYPE]
ON [dbo].[AS_USER_INSPECTIONTYPE]
    ([InspectionTypeID]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------