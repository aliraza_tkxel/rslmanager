﻿using DataModel.DataRepository;
using DataModel.DataRepository.Customers;

namespace DataModel.UnitOfWork
{
    public interface IUnitOfWork
    {
        #region start authenticatio repo       
        AuthenticationRepository authenticationRepository { get; }
        #endregion

        #region start customer repo           
        CustomerRepository customerRepository { get; }
        CustomerAddressRepository customerAddressRepository { get; }
        CommunicationRepository communicationRepository { get; }
        CustomerTitleRepository customerTitleRepository { get; }
        InternetAccessRepository internetAccessRepository { get; }
        PreferedContactRepository preferedContactRepository { get; }
        SexualOrientationRepository sexualOrientationRepository { get; }
        BenefitsRepository benefitsRepository { get; }
        EthnicityRepository ethnicityRepository { get; }
        LanguagesRepository languagesRepository { get; }
        MaritalStatusRepository maritalStatusRepository { get; }
        ReligionRepository religionRepository { get; }
        YesNoRepository yesNoRepository { get; }
        CustomerCurrentHomeRepository customerCurrentHomeRepository { get; }
        CustomerTypeRepository customerTypeRepository { get; }
        NationalityRepository nationalityRepository { get; }
        LandLordRepoeitory landLordRepoeitory { get; }
        YesNoUnKnownRepository yesNoUnKnownRepository { get; }
        ReferralStagesRepository referralStagesRepository { get; }
        ReferralHelpCategoryRepository referralHelpCategoryRepository { get; }
        ReferralHelpSubCategoryRepository referralHelpSubCategoryRepository { get; }
        ReferralsRepository referralsRepository { get; }
        JournalRepository journalRepository { get; }
        CustomerHelpSubCategoryRepository customerHelpSubCategoryRepository { get; }
        NatureRepository natureRepository { get; }
        ActionRepository actionRepository { get; }
        StatusRepository statusRepository { get; }
        ItemRepository itemRepository { get; }
        AddressTypeRepository addressTypeRepository { get; }
        HouseTypeRepository houseTypeRepository { get; }
        TerminationReasonRepository terminationReasonRepository { get; }

        FloorTypeRepository floorTypeRepository { get; }
        #endregion

        #region start property repo          
        SchemesRepository schemesRepository { get; }
        PropertiesRepository propertiesRepository { get; }
        PropertyTypeRepository propertyTypeRepository { get; }
        PropertyRentInfoRepository propertyRentInfoRepository { get; }
        #endregion

        #region start viewing repo      
        HousingOfficerRepository housingOfficerRepository { get; }
        ViewingRepository viewingRepository { get; }
        ViewingStatusRepository viewingStatusRepository { get; }
        ViewingCommentsRepository viewingCommentsRepository { get; }
        #endregion

        #region start tenancy info repo
        EmploymentStatusRepository employmentStatusRepository { get; }
        BankingFacilityRepository bankingFacilityRepository { get; }
        DisabilityRepository disabilityRepository { get; }
        SupportAgenciesRepository supportAgenciesRepository { get; }
        AdaptionsRepository adaptionsRepository { get; }
        SchedulerRepository schedulerRepository { get; }
        #endregion

        #region termination and tenancy creation repo
        TerminationRepository terminationRepository { get; }
        CustomerTenancyRepository customerTenancyRepository { get; }
        TenancyRepository tenancyRepository { get; }
        RentJournalRepository rentJournalRepository { get; }
        TenantDisclosureInfoRepository tenantDisclosureInfoRepository { get; }
        TenantOnlineInfoRepository tenantOnlineInfoRepository { get; }
        OccupantsRepository occupantsRepository { get; }
        RelationShipRepository relationShipRepository { get; }
        #endregion

        #region Customer Risk/Vulnerability repo
        RisksRepository risksRepository { get; }
        VulnerabilitiesRepository vulnerabilitiesRepository { get; }
        RiskVulnerabilityStatusRespository riskVulnerabilityStatusRespository { get; }
        TeamsRepository teamsRepository { get; }
        EmployeesRepository employeesRepository { get; }
        RiskCategoryRepository riskCategoryRepository { get; }
        RiskSubCategoryRepository riskSubCategoryRepository { get; }
        VulnerabilityCategoryRepository vulnerabilityCategoryRepository { get; }
        VulnerabilitySubCategoryRepository vulnerabilitySubCategoryRepository { get; }
        LetterActionRepository letterActionRepository { get; }
        CustomerVulnerabilitiesRepository customerVulnerabilitiesRepository { get; }
        CustomerRisksRepository customerRisksRepository { get; }
        #endregion
        int Commit();

    }
}
