﻿using DataModel.DatabaseEntities;
using DataModel.DataRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataModel.DataRepository.Customers;

namespace DataModel.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {

        private RSLBHALiveEntities dataContext = null;
        //start authentication repo
        private AuthenticationRepository _authenticationRepository;
        //end authentication repo

        //Start customer repo
        private CustomerRepository _customerRepository;

        private CustomerAddressRepository _customerAddressRepository;        
        private CommunicationRepository _communicationRepository;
        private CustomerTitleRepository _customerTitleRepository;
        private InternetAccessRepository _internetAccessRepository;
        private PreferedContactRepository _preferedContactRepository;       
        private SexualOrientationRepository _sexualOrientationRepository;      
        private CustomerCurrentHomeRepository _customerCurrentHomeRepository;
        private CustomerTypeRepository _customerTypeRepository;
        private BenefitsRepository _benefitsRepository;
        private EthnicityRepository _ethnicityRepository;
        private LanguagesRepository _languagesRepository;
        private MaritalStatusRepository _maritalStatusRepository;
        private ReligionRepository _religionRepository;
        private YesNoRepository _yesNoRepository;
        private NationalityRepository _nationalityRepository;
        private LandLordRepoeitory _landLordRepoeitory;
        private YesNoUnKnownRepository _yesNoUnKnownRepository;
        private ReferralStagesRepository _referralStagesRepository;
        private ReferralHelpCategoryRepository _referralHelpCategoryRepository;
        private ReferralHelpSubCategoryRepository _referralHelpSubCategoryRepository;
        private ReferralsRepository _referralsRepository;
        private JournalRepository _journalRepository;
        private CustomerHelpSubCategoryRepository _customerHelpSubCategoryRepository;
        private NatureRepository _natureRepository;
        private ItemRepository _itemRepository;
        private StatusRepository _statusRepository;
        private ActionRepository _actionRepository;
        private AddressTypeRepository _addressTypeRepository;
        private HouseTypeRepository _houseTypeRepository;
        private TerminationReasonRepository _terminationReasonRepository;
        private FloorTypeRepository _floorTypeRepository;
        private SchedulerRepository _schedulerRepository;
        //End customer repo

        //start property repo
        private PropertiesRepository _propertiesRepository;
        private SchemesRepository _schemesRepository;
        private PropertyTypeRepository _propertyTypeRepository;
        private PropertyRentInfoRepository _propertyRentInfoRepository;
        //end property repo

        //start housing officer repo
        private HousingOfficerRepository _housingOfficerRepository;
        private ViewingRepository _viewingRepository;
        private ViewingStatusRepository _viewingStatusRepository;
        private ViewingCommentsRepository _viewingCommentsRepository;
        //end housing office repo

        //start termination info repo
        private EmploymentStatusRepository _employmentStatusRepository;
        private BankingFacilityRepository _bankingFacilityRepository;
        private DisabilityRepository _disabilityRepository;
        private SupportAgenciesRepository _supportAgenciesRepository;
        private AdaptionsRepository _adaptionsRepository;
        //end termination info repo

        //start tenancy info repo
        private TerminationRepository _terminationRepository;
        private CustomerTenancyRepository _customerTenancyRepository;
        private TenancyRepository _tenancyRepository;
        private RentJournalRepository _rentJournalRepository;
        private TenantDisclosureInfoRepository _tenantDisclosureInfoRepository;
        private TenantOnlineInfoRepository _tenantOnlineInfoRepository;
        private OccupantsRepository _occupantsRepository;
        private RelationShipRepository _relationShipRepository;
        //end tenancy info repo

        //start customer risk/Vulnerability info repo
        private RisksRepository _customerRiskRepository;
        private VulnerabilitiesRepository _customerVulnerabilityRepository;
        private RiskVulnerabilityStatusRespository _riskVulnerabilityStatusRespository;
        private TeamsRepository _teamsRepository;
        private EmployeesRepository _employeesRepository;
        private RiskCategoryRepository _riskCategoryRepository;
        private RiskSubCategoryRepository _riskSubCategoryRepository;
        private VulnerabilityCategoryRepository _vulnerabilityCategoryRepository;
        private VulnerabilitySubCategoryRepository _vulnerabilitySubCategoryRepository;
        private LetterActionRepository _letterActionRepository;
        private CustomerVulnerabilitiesRepository _customerVulnerabilitiesRepository;
        private CustomerRisksRepository _customerRisksRepository;
        //end customer risk info repo


        public UnitOfWork()
        {
            if (dataContext == null)
            {
                dataContext = new RSLBHALiveEntities();
                dataContext.Configuration.ProxyCreationEnabled = false;
            }
        }

        public ViewingCommentsRepository viewingCommentsRepository
        {
            get { return (_viewingCommentsRepository == null) ? _viewingCommentsRepository = new ViewingCommentsRepository(dataContext) : _viewingCommentsRepository; }
        }
        public CustomerRepository customerRepository
        {
            get { return (_customerRepository == null) ? _customerRepository = new CustomerRepository(dataContext) : _customerRepository; }
        }

        public AuthenticationRepository authenticationRepository
        {
            get { return (_authenticationRepository == null) ? _authenticationRepository = new AuthenticationRepository(dataContext) : _authenticationRepository; }
        }



        public CommunicationRepository communicationRepository
        {
            get { return (_communicationRepository == null) ? _communicationRepository = new CommunicationRepository(dataContext) : _communicationRepository; }
        }

        public CustomerTitleRepository customerTitleRepository
        {
            get { return (_customerTitleRepository == null) ? _customerTitleRepository = new CustomerTitleRepository(dataContext) : _customerTitleRepository; }
        }

        public InternetAccessRepository internetAccessRepository
        {
            get { return (_internetAccessRepository == null) ? _internetAccessRepository = new InternetAccessRepository(dataContext) : _internetAccessRepository; }
        }

        public PreferedContactRepository preferedContactRepository
        {
            get { return (_preferedContactRepository == null) ? _preferedContactRepository = new PreferedContactRepository(dataContext) : _preferedContactRepository; }
        }
              
        public SexualOrientationRepository sexualOrientationRepository
        {
            get { return (_sexualOrientationRepository == null) ? _sexualOrientationRepository = new SexualOrientationRepository(dataContext) : _sexualOrientationRepository; }
        }
                
        public BenefitsRepository benefitsRepository
        {
            get { return (_benefitsRepository == null) ? _benefitsRepository = new BenefitsRepository(dataContext) : _benefitsRepository; }
        }

        public EthnicityRepository ethnicityRepository
        {
            get { return (_ethnicityRepository == null) ? _ethnicityRepository = new EthnicityRepository(dataContext) : _ethnicityRepository; }
        }

        public LanguagesRepository languagesRepository
        {
            get { return (_languagesRepository == null) ? _languagesRepository = new LanguagesRepository(dataContext) : _languagesRepository; }
        }

        public MaritalStatusRepository maritalStatusRepository
        {
            get { return (_maritalStatusRepository == null) ? _maritalStatusRepository = new MaritalStatusRepository(dataContext) : _maritalStatusRepository; }
        }

        public ReligionRepository religionRepository
        {
            get { return (_religionRepository == null) ? _religionRepository = new ReligionRepository(dataContext) : _religionRepository; }
        }

        public SchedulerRepository schedulerRepository
        {
            get { return (_schedulerRepository == null) ? _schedulerRepository = new SchedulerRepository(dataContext) : _schedulerRepository; }
        }

        public YesNoRepository yesNoRepository
        {
            get { return (_yesNoRepository == null) ? _yesNoRepository = new YesNoRepository(dataContext) : _yesNoRepository; }
        }
        public PropertiesRepository propertiesRepository
        {
            get { return (_propertiesRepository == null) ? _propertiesRepository = new PropertiesRepository(dataContext) : _propertiesRepository; }
        }
        public SchemesRepository schemesRepository
        {
            get { return (_schemesRepository == null) ? _schemesRepository = new SchemesRepository(dataContext) : _schemesRepository; }
        }

        public PropertyTypeRepository propertyTypeRepository
        {
            get { return (_propertyTypeRepository == null) ? _propertyTypeRepository = new PropertyTypeRepository(dataContext) : _propertyTypeRepository; }
        }

        public CustomerAddressRepository customerAddressRepository
        {
            get { return (_customerAddressRepository == null) ? _customerAddressRepository = new CustomerAddressRepository(dataContext) : _customerAddressRepository; }
        }

        public CustomerCurrentHomeRepository customerCurrentHomeRepository
        {
            get { return (_customerCurrentHomeRepository == null) ? _customerCurrentHomeRepository = new CustomerCurrentHomeRepository(dataContext) : _customerCurrentHomeRepository; }
        }

        public HousingOfficerRepository housingOfficerRepository
        {
            get { return (_housingOfficerRepository == null) ? _housingOfficerRepository = new HousingOfficerRepository(dataContext) : _housingOfficerRepository; }
        }

        public ViewingRepository viewingRepository
        {
            get { return (_viewingRepository == null) ? _viewingRepository = new ViewingRepository(dataContext) : _viewingRepository; }
        }

        public ViewingStatusRepository viewingStatusRepository
        {
            get { return (_viewingStatusRepository == null) ? _viewingStatusRepository = new ViewingStatusRepository(dataContext) : _viewingStatusRepository; }
        }

        public CustomerTypeRepository customerTypeRepository
        {
            get { return (_customerTypeRepository == null) ? _customerTypeRepository = new CustomerTypeRepository(dataContext) : _customerTypeRepository; }
        }

        public NationalityRepository nationalityRepository
        {
            get { return (_nationalityRepository == null) ? _nationalityRepository = new NationalityRepository(dataContext) : _nationalityRepository; }
        }

        public LandLordRepoeitory landLordRepoeitory
        {
            get { return (_landLordRepoeitory == null) ? _landLordRepoeitory = new LandLordRepoeitory(dataContext) : _landLordRepoeitory; }
        }

        public YesNoUnKnownRepository yesNoUnKnownRepository
        {
            get { return (_yesNoUnKnownRepository == null) ? _yesNoUnKnownRepository = new YesNoUnKnownRepository(dataContext) : _yesNoUnKnownRepository; }
        }

        public ReferralStagesRepository referralStagesRepository
        {
            get { return (_referralStagesRepository == null) ? _referralStagesRepository = new ReferralStagesRepository(dataContext) : _referralStagesRepository; }
        }

        public ReferralHelpCategoryRepository referralHelpCategoryRepository
        {
            get { return (_referralHelpCategoryRepository == null) ? _referralHelpCategoryRepository = new ReferralHelpCategoryRepository(dataContext) : _referralHelpCategoryRepository; }
        }

        public ReferralHelpSubCategoryRepository referralHelpSubCategoryRepository
        {
            get { return (_referralHelpSubCategoryRepository == null) ? _referralHelpSubCategoryRepository = new ReferralHelpSubCategoryRepository(dataContext) : _referralHelpSubCategoryRepository; }
        }

        public ReferralsRepository referralsRepository
        {
            get { return (_referralsRepository == null) ? _referralsRepository = new ReferralsRepository(dataContext) : _referralsRepository; }
        }

        public JournalRepository journalRepository
        {
            get { return (_journalRepository == null) ? _journalRepository = new JournalRepository(dataContext) : _journalRepository; }
        }

        public CustomerHelpSubCategoryRepository customerHelpSubCategoryRepository
        {
            get { return (_customerHelpSubCategoryRepository == null) ? _customerHelpSubCategoryRepository = new CustomerHelpSubCategoryRepository(dataContext) : _customerHelpSubCategoryRepository; }
        }

        public NatureRepository natureRepository
        {
            get { return (_natureRepository == null) ? _natureRepository = new NatureRepository(dataContext) : _natureRepository; }
        }

        public ActionRepository actionRepository
        {
            get { return (_actionRepository == null) ? _actionRepository = new ActionRepository(dataContext) : _actionRepository; }
        }

        public StatusRepository statusRepository
        {
            get { return (_statusRepository == null) ? _statusRepository = new StatusRepository(dataContext) : _statusRepository; }
        }

        public ItemRepository itemRepository
        {
            get { return (_itemRepository == null) ? _itemRepository = new ItemRepository(dataContext) : _itemRepository; }
        }

        public EmploymentStatusRepository employmentStatusRepository
        {
            get { return (_employmentStatusRepository == null) ? _employmentStatusRepository = new EmploymentStatusRepository(dataContext) : _employmentStatusRepository; }
        }

        public BankingFacilityRepository bankingFacilityRepository
        {
            get { return (_bankingFacilityRepository == null) ? _bankingFacilityRepository = new BankingFacilityRepository(dataContext) : _bankingFacilityRepository; }
        }

        public DisabilityRepository disabilityRepository
        {
            get { return (_disabilityRepository == null) ? _disabilityRepository = new DisabilityRepository(dataContext) : _disabilityRepository; }
        }

        public SupportAgenciesRepository supportAgenciesRepository
        {
            get { return (_supportAgenciesRepository == null) ? _supportAgenciesRepository = new SupportAgenciesRepository(dataContext) : _supportAgenciesRepository; }
        }

        public AdaptionsRepository adaptionsRepository
        {
            get { return (_adaptionsRepository == null) ? _adaptionsRepository = new AdaptionsRepository(dataContext) : _adaptionsRepository; }
        }

        public TerminationRepository terminationRepository
        {
            get { return (_terminationRepository == null) ? _terminationRepository = new TerminationRepository(dataContext) : _terminationRepository; }
        }

        public CustomerTenancyRepository customerTenancyRepository
        {
            get { return (_customerTenancyRepository == null) ? _customerTenancyRepository = new CustomerTenancyRepository(dataContext) : _customerTenancyRepository; }
        }

        public TenancyRepository tenancyRepository
        {
            get { return (_tenancyRepository == null) ? _tenancyRepository = new TenancyRepository(dataContext) : _tenancyRepository; }
        }
        public RentJournalRepository rentJournalRepository
        {
            get { return (_rentJournalRepository == null) ? _rentJournalRepository = new RentJournalRepository(dataContext) : _rentJournalRepository; }
        }

        public AddressTypeRepository addressTypeRepository
        {
            get { return (_addressTypeRepository == null) ? _addressTypeRepository = new AddressTypeRepository(dataContext) : _addressTypeRepository; }
        }
        public HouseTypeRepository houseTypeRepository
        {
            get { return (_houseTypeRepository == null) ? _houseTypeRepository = new HouseTypeRepository(dataContext) : _houseTypeRepository; }
        }
        public TerminationReasonRepository terminationReasonRepository
        {
            get { return (_terminationReasonRepository == null) ? _terminationReasonRepository = new TerminationReasonRepository(dataContext) : _terminationReasonRepository; }
        }
        public FloorTypeRepository floorTypeRepository
        {
            get { return (_floorTypeRepository == null) ? _floorTypeRepository = new FloorTypeRepository(dataContext) : _floorTypeRepository; }
        }

        public PropertyRentInfoRepository propertyRentInfoRepository
        {
            get { return (_propertyRentInfoRepository == null) ? _propertyRentInfoRepository = new PropertyRentInfoRepository(dataContext) : _propertyRentInfoRepository; }
        }

        public TenantDisclosureInfoRepository tenantDisclosureInfoRepository
        {
            get { return (_tenantDisclosureInfoRepository == null) ? _tenantDisclosureInfoRepository = new TenantDisclosureInfoRepository(dataContext) : _tenantDisclosureInfoRepository; }
        }

        public TenantOnlineInfoRepository tenantOnlineInfoRepository
        {
            get { return (_tenantOnlineInfoRepository == null) ? _tenantOnlineInfoRepository = new TenantOnlineInfoRepository(dataContext) : _tenantOnlineInfoRepository; }
        }

        public OccupantsRepository occupantsRepository
        {
            get { return (_occupantsRepository == null) ? _occupantsRepository = new OccupantsRepository(dataContext) : _occupantsRepository; }
        }

        public RelationShipRepository relationShipRepository
        {
            get { return (_relationShipRepository == null) ? _relationShipRepository = new RelationShipRepository(dataContext) : _relationShipRepository; }
        }

        public RisksRepository risksRepository
        {
            get { return (_customerRiskRepository == null) ? _customerRiskRepository = new RisksRepository(dataContext) : _customerRiskRepository; }
        }

        public VulnerabilitiesRepository vulnerabilitiesRepository
        {
            get { return (_customerVulnerabilityRepository == null) ? _customerVulnerabilityRepository = new VulnerabilitiesRepository(dataContext) : _customerVulnerabilityRepository; }
        }

        public RiskVulnerabilityStatusRespository riskVulnerabilityStatusRespository
        {
            get { return (_riskVulnerabilityStatusRespository == null) ? _riskVulnerabilityStatusRespository = new RiskVulnerabilityStatusRespository(dataContext) : _riskVulnerabilityStatusRespository; }
        }

        public TeamsRepository teamsRepository
        {
            get { return (_teamsRepository == null) ? _teamsRepository = new TeamsRepository(dataContext) : _teamsRepository; }
        }
        public EmployeesRepository employeesRepository
        {
            get { return (_employeesRepository == null) ? _employeesRepository = new EmployeesRepository(dataContext) : _employeesRepository; }
        }

        public RiskCategoryRepository riskCategoryRepository
        {
            get { return (_riskCategoryRepository == null) ? _riskCategoryRepository = new RiskCategoryRepository(dataContext) : _riskCategoryRepository; }
        }

        public RiskSubCategoryRepository riskSubCategoryRepository
        {
            get { return (_riskSubCategoryRepository == null) ? _riskSubCategoryRepository = new RiskSubCategoryRepository(dataContext) : _riskSubCategoryRepository; }
        }

        public VulnerabilityCategoryRepository vulnerabilityCategoryRepository
        {
            get { return (_vulnerabilityCategoryRepository == null) ? _vulnerabilityCategoryRepository = new VulnerabilityCategoryRepository(dataContext) : _vulnerabilityCategoryRepository; }
        }

        public VulnerabilitySubCategoryRepository vulnerabilitySubCategoryRepository
        {
            get { return (_vulnerabilitySubCategoryRepository == null) ? _vulnerabilitySubCategoryRepository = new VulnerabilitySubCategoryRepository(dataContext) : _vulnerabilitySubCategoryRepository; }
        }

        public LetterActionRepository letterActionRepository
        {
            get { return (_letterActionRepository == null) ? _letterActionRepository = new LetterActionRepository(dataContext) : _letterActionRepository; }
        }

        public CustomerVulnerabilitiesRepository customerVulnerabilitiesRepository
        {
            get { return (_customerVulnerabilitiesRepository == null) ? _customerVulnerabilitiesRepository = new CustomerVulnerabilitiesRepository(dataContext) : _customerVulnerabilitiesRepository; }
        }

        public CustomerRisksRepository customerRisksRepository
        {
            get { return (_customerRisksRepository == null) ? _customerRisksRepository = new CustomerRisksRepository(dataContext) : _customerRisksRepository; }
        }
        int IUnitOfWork.Commit()
        {
            return dataContext.SaveChanges();
        }


    }
}
