#@del /F /Q *.user
@PUSHD BusinessEntities
  @del /F /Q *.user
  @del /F /Q *.log
  @rmdir /S /Q bin
  @rmdir /S /Q obj
@POPD

@PUSHD  DataModel
  @rmdir /S /Q bin
  @rmdir /S /Q obj
  @del /F /Q *.user
  @del /F /Q *.log
@POPD

@PUSHD FrontLineAPI
  @rmdir /S /Q bin
  @rmdir /S /Q obj
  @del /F /Q *.user
  @del /F /Q *.log
@POPD

@PUSHD Services
  @rmdir /S /Q bin
  @rmdir /S /Q obj
  @del /F /Q *.user
  @del /F /Q *.log
@POPD

@PUSHD Utilities
  @rmdir /S /Q bin
  @rmdir /S /Q obj
  @del /F /Q *.user
  @del /F /Q *.log
@POPD


@PAUSE