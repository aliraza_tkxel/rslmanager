﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModels
{
  public class RisksListingDataModel
    {
        public int riskHistoryId { get; set; }
        public int? customerId { get; set; }
        public string customerName { get; set; }
        public int? tenancyId { get; set; }
        public int journalId { get; set; }
        public string title { get; set; }
        public int? teamId { get; set; }
        public int? assignedTo { get; set; }
        public DateTime? startDate { get; set; }
       // public string customerRiskcategories { get; set; }
        public string customerRiskSubcategories { get; set; }
        public string notes { get; set; }
        public DateTime? reviewDate { get; set; }
        public bool? isCustomerAware { get; set; }
        public bool? isTenantOnline { get; set; }
        public int? actionId { get; set; }
        //public int letterId { get; set; } removed it as discussed with omer
        public int? lastActionUserId { get; set; }
        public DateTime? lastActionDate { get; set; }
        public int? statusId { get; set; }
        [JsonIgnore]
        public int categoryId { get; set; }
        [JsonIgnore]
        public int subCategoryId { get; set; }
    }
}
