﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModels
{
   public class PaginationDataModel
    {
        public int pageSize { get; set; }   // total no.of record per page    
        public int pageNumber { get; set; }// current page no.
        public int totalRows { get; set; }// total no.of records
        public int totalPages { get; set; }// total no.of pages in record set
    }
}
