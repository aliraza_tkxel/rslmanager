﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModels
{ 
   public class SharedLookupResponseModel
    {
        public List<LookUpResponseModel> propertyType { get; set; }
        public List<LookUpResponseModel> customerType { get; set; }
        public List<LookUpResponseModel> supportAgencies { get; set; }
        public List<LookUpResponseModel> adaptions { get; set; }
        public List<LookUpResponseModel> bankingFacility { get; set; }
        public List<LookUpResponseModel> employmentStatus { get; set; }
        public List<LookUpResponseModel> disability { get; set; }
        public List<LookUpResponseModel> referralsHelpCategories { get; set; }
        public List<DependentLookupResponseModel> referralsHelpSubCategories { get; set; }
        public List<LookUpResponseModel> referralsStages { get; set; }
        public List<LookUpResponseModel> terminationResaon { get; set; }
        public List<DependentLookupResponseModel> terminationResaonCategory { get; set; }
        public List<LookUpResponseModel> customerTitles { get; set; }
        public List<LookUpResponseModel> benefits { get; set; }
        public List<LookUpResponseModel> yesNo { get; set; }
        public List<LookUpResponseModel> yesNoUnkown { get; set; }
        public List<LookUpResponseModel> addressType { get; set; }
        public List<LookUpResponseModel> relationshipStatus { get; set; }
        public List<LookUpResponseModel> riskVulnerabilityStatus { get; set; }
        public List<LookUpResponseModel> teams { get; set; }
        public List<DependentLookupResponseModel> employees { get; set; }
        public List<LookUpResponseModel> riskCategory { get; set; }
        public List<DependentLookupResponseModel> riskSubCategory { get; set; }
        public List<LookUpResponseModel> vulnerabilityCategory { get; set; }
        public List<DependentLookupResponseModel> vulnerabilitySubCategory { get; set; }
        public List<LookUpResponseModel> letterAction { get; set; }

        public List<LookUpResponseModel> viewingCommentTypes { get; set; }
    }
}
