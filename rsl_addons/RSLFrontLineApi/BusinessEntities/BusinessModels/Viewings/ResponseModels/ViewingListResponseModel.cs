﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModels
{
  public class ViewingListResponseModel
    {
       public List<ViewingListDataModel> viewingListDataModel { get; set; }
        public ViewingLookupsDataModel lookups { get; set; }
    }
    public class KeyValuePairMonth
    {
        public int start { get; set; }
        public int end { get; set; }
    }

}
