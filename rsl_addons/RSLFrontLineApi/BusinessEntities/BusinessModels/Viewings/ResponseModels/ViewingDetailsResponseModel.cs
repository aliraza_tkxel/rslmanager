﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModels
{
  public  class ViewingDetailsResponseModel
    {
        public int viewingId { get; set; }
        public DateTime viewingDateTime { get; set; }
        public string propertyId { get; set; }
        public decimal? rent { get; set; }
        public string propertyAddress { get; set; }
        public string noOfBedRooms { get; set; }
        public string propertyType { get; set; }
        public string customerName { get; set; }
        public string gender { get; set; }
        public string customerType { get; set; }
        public string mobileNo { get; set; }
        public DateTime? dob { get; set; }
        public DateTime? createdOnApp { get; set; }
        public int viewingStatusId { get; set; }
        public string viewingCreatedBy { get; set; }
        public string viewingNotes { get; set; }
        public string propertyImage { get; set; }
        public string outlookIdentifier { get; set; }
        public int? assignedById { get; set; }
        public string assignedByName { get; set; }
        public List<ViewingCommentResponseModel> commentsList { get; set; } = new List<ViewingCommentResponseModel>();
    }
}
