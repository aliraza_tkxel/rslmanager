﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModels
{
    public class ViewingListDataModel
    {
        public int viewingId { get; set; }
        public DateTime viewingDate { get; set; }
        public string viewingTime { get; set; }
        public int? customerId { get; set; }
        public string customerName { get; set; }
        public string customerGender { get; set; }
        public int viewingStatusId { get; set; }
        public int? housingOfficerId { get; set; }
        public string housingOfficerName { get; set; }
        public string housingOfficerGender { get; set; }
        public int? createdById { get; set; }
        public string propertyAddress { get; set; }
        public DateTime? createdOnApp { get; set; }
        public string housingOfficerEmail { get; set; }
    }
}
