﻿using Newtonsoft.Json;
using System;

namespace BusinessModels
{
    public class ArrangeViewingRequestModel
    {       
        public int viewingId { get; set; }
        public int customerId { get; set; }
        public string propertyId { get; set; }
        public int housingOfficerId { get; set; }
        public int viewingStatusId { get; set; }
        public DateTime viewingDate { get; set; }
        public string viewingTime { get; set; }
        public string endTime { get; set; }
        public double? duration { get; set; }
        public string actualEndTime { get; set; }
        public int createdById { get; set; }
        public bool isActive { get; set; }
        public string recordingSource { get; set; }
        public string appVersion { get; set; }
        public string outlookIdentifier { get; set; }
        public DateTime? createdOnApp { get; set; }
        public DateTime? createdOnServer { get; set; }
        public DateTime? lastModifiedOnApp { get; set; }
        public DateTime? lastModifiedOnServer { get; set; }    
        public string notes { get; set; }   
        public int? assignedById { get; set; }
    }
}
