﻿using System;

namespace BusinessModels
{
    public class ViewingListRequestModel
    {
        public int housingOfficer { get; set; }
        public bool isLookUpRequired { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }       
        
    }
}
