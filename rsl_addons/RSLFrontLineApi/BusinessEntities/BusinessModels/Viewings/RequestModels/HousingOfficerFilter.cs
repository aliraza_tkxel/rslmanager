﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModels
{
    public class HousingOfficerFilter
    {
        public DateTime viewingDate { get; set; }
        public string viewingTime { get; set; }
        public string searchText { get; set; }
    }
}
