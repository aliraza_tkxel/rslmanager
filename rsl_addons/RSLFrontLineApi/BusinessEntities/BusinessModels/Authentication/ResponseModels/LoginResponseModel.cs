﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModels
{
    /// <summary>
    /// Login Response for Client
    /// </summary>
   public  class LoginResponseModel
    {
        public string employeeFullName { get; set; }
        public int? isActive { get; set; }
        public int? userId { get; set; }
        public string userName { get; set; }
        public int? teamId { get; set; }
        public string gender { get; set; }

    }
}
