﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModels
{
   public class EmailTemplateResponseModel
    {
      public string login { get; set; }
      public string password { get; set; }
      public string workEmail { get; set; }
    }
}
