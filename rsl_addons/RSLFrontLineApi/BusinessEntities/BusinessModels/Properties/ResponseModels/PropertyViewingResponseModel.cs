﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModels
{
   public class PropertyViewingResponseModel
    {
        public string propertyId { get; set; }
        public int viewingId { get; set; }
        public DateTime viewingDate { get; set; }       
        public int customerId { get; set; }
        public string viewingTime { get; set; }
        public string customerName { get; set; }
        public int housingOfficerId { get; set; }
        public int viewingStatusId { get; set; }
        public string housingOfficerName { get; set; }
    }
}
