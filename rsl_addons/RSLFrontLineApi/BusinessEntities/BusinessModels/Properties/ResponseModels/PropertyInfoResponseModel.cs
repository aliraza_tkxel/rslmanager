﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModels
{
   public class PropertyInfoResponseModel
    {
        //basic info
        public string propertyId { get; set; }
        public int propertyTypeId { get; set; }
        public string developmentType { get; set; }
        public int? schemeId { get; set; }
        public string phase { get; set; }
        public string block { get; set; }
        public string houseNo { get; set; }
        public string address { get; set; }
        public string townCity { get; set; }
        public string county { get; set; }
        public string postalCode { get; set; }
        public decimal? rent { get; set; }       
        public string status { get; set; }
        public string rightToAcquire { get; set; }
        public string assetType { get; set; }
        public string imageName { get; set; }


        //accommodation related info
        private string _floorArea;
        public string floorArea
        {
            set
            {
                _floorArea = value;
            }
            get
            {
                return _floorArea == null ? "N/A" : _floorArea;
            }
        }
        private string _maxPeople;
        public string maxPeople
        {
            set
            {
                _maxPeople = value;
            }
            get
            {
                return _maxPeople == null ? "N/A" : _maxPeople;
            }
        }
        private string _numberOfLivingRooms;
        public string numberOfLivingRooms
        {
            set
            {
                _numberOfLivingRooms = value;

            }
            get
            {
                return _numberOfLivingRooms == null ? "N/A" : _numberOfLivingRooms;
            }
        }
        private string _numberOfBedRooms;
        public string numberOfBedRooms
        {
            set
            {
                _numberOfBedRooms = value;
            }
            get
            {
                return _numberOfBedRooms == null ? "N/A" : _numberOfBedRooms;
            }
        }
        private string _numberOfBathRooms;
        public string numberOfBathRooms
        {
            set
            {
                _numberOfBathRooms = value;
            }
            get
            {
                return _numberOfBathRooms == null ? "N/A" : _numberOfBathRooms;
            }
        }
        private string __numberOfkitchen;
        public string numberOfkitchen
        {
            set
            {
                __numberOfkitchen = value;
            }

            get
            {
                return __numberOfkitchen == null ? "N/A" : __numberOfkitchen;
            }
        }
        private string _parking;
        public string parking
        {
            set
            {
                _parking = value;
            }

            get
            {
                return _parking == null ? "N/A" : _parking;
            }


        }
        private string _garden;
        public string garden
        {
            set
            {
                _garden = value;
            }

            get
            {
                return _garden == null ? "N/A" : _garden;
            }

        }
        private string _garage;
        public string garage
        {
            set
            {
                _garage = value;
            }

            get
            {
                return _garage == null ? "N/A" : _garage;
            }
        }
        private string _heatingFuel;
        public string heatingFuel
        {
            set
            {
                _heatingFuel = value;
            }
            get
            {
                return _heatingFuel == null ? "N/A" : _heatingFuel;
            }
        }
        private string _boilerType;
        public string boilerType
        {
            set
            {
                _boilerType = value;
            }
            get
            {
                return _boilerType == null ? "N/A" : _boilerType;
            }
        }
        private string _wheelChairAccess;
        public string wheelChairAccess
        {

            set
            {
                _wheelChairAccess = value;
            }
            get
            {
                return _wheelChairAccess == null ? "N/A" : _wheelChairAccess;
            }

        }







    }
}
