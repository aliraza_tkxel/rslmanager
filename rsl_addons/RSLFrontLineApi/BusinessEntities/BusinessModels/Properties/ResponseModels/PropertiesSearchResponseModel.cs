﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModels
{
   public class PropertiesSearchResponseModel
    {
       public List<PropertiesDataModel> propertiesData { get; set; }             
       public PropertyLookUpsDataModel lookups { get; set; }
    }
}
