﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModels
{
    public class PropertiesDataModel
    {
        public string propertyId { get; set; }     
        public DateTime? tenancyEndDate { get; set; }
        public decimal? rent { get; set; }      
        public string bedRooms { get; set; }
        public string address { get; set; }
        public string status { get; set; }
        public string subStatus { get; set; }
        public string imagePath { get; set; }
        public string imageName { get; set; }
        public string postalCode { get; set; }
        public int? schemeId { get; set; }
        public int propertyTypeId { get; set; }
        
     
    }
}
