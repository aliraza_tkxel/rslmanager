﻿using System;

namespace BusinessModels
{
    public class PropertyViewingRequestModel
    {
        public string propertyId { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }       
    }
}
