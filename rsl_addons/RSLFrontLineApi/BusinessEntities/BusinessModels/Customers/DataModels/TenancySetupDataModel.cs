﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModels
{
   public class TenancySetupDataModel
    {
        public int tenancyId { get; set; }
        public string propertyId { get; set; }
        public int customerId { get; set; }
        public DateTime? tenancyStartDate { get; set; }
        public EmploymentInfoDataModel employmentInfo { get; set; }
        public CustomerGeneralInfoDataModel customerGeneralInfo { get; set; }
        public EmergencyContactDataModel emergencyContact { get; set; }
        public JointTenantInfoDataModel jointTenant { get; set; }
        public List<OccupantsDataModel> occupants { get; set; }
        public CustomerReferralsDataModel referral { get; set; }
        public PropertyRentInfoDataModel propertyRentInfo { get; set; }
        public TenancyDisclosureDataModel tenancyDisclosure { get; set; }
        public TenantOnlineInfoDataModel tenantOnlineInfo { get; set; }       
    }
}
