﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModels
{
   public class TenantOnlineInfoDataModel
    {
       // public string username { get; set; }
        public string password { get; set; }
        [JsonIgnore]
        public int? customerId { get; set; }
    }
}
