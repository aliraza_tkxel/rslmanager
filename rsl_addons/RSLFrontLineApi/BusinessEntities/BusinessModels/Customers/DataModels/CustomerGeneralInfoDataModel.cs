﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModels
{
   public class CustomerGeneralInfoDataModel
    {
        public string benefits { get; set; }
        [JsonIgnore]
        public string benefitOther { get; set; }
        public string bankingFacilities { get; set; }
        public string bankingFacilityOthers { get; set; }
        public string healthProblems { get; set; }
        public string healthProblemOthers { get; set; }
        public short? liveInCarer { get; set; }
        public short? wheelChair { get; set; }
        public string supportFromAgencies { get; set; }
        public string supportFromAgenciesOthers { get; set; }
        public string aidsAndAdaptation { get; set; }
    }
}
