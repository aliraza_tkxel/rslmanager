﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModels
{
   public class OccupantsDataModel
    {
        public int occupantId { get; set; }
        public int customerId { get; set; }
        public int? titleId { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public DateTime? dateOfBirth { get; set; }
        public int? relationshipStatusId { get; set; }
        public string disabilities { get; set; }
        public string disabilitiesOthers { get; set; } 
        public int? employmentStatusId { get; set; }
        [JsonIgnore]
        public int? tenancyId { get; set; }
        //public int? consent { get; set; }
    }
}
