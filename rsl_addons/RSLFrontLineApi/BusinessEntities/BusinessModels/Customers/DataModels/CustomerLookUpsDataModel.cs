﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModels
{
    public class CustomerLookUpsDataModel
    {
        //start Add/edit customer lookups
        
        public List<LookUpResponseModel> communication { get; set; }
        public List<LookUpResponseModel> internetAccess { get; set; }
        public List<LookUpResponseModel> preferedContact { get; set; }        
        public List<LookUpResponseModel> sexualOrientation { get; set; }                      
        public List<LookUpResponseModel> ethnicity { get; set; }
        public List<LookUpResponseModel> language { get; set; }
        public List<LookUpResponseModel> maritalStatus { get; set; }
        public List<LookUpResponseModel> religion { get; set; }       
        public List<LookUpResponseModel> nationality { get; set; }
        public List<LookUpResponseModel> landlord { get; set; }        
        public List<LookUpResponseModel> houseType { get; set; }        
        public List<LookUpResponseModel> floorType { get; set; }       
        //end Add/edit customer lookups              



    }
}
