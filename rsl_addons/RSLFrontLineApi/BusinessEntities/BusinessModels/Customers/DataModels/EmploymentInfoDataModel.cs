﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModels
{
   public class EmploymentInfoDataModel
    {
        public int? employmentStatusId { get; set; }
        public string occupation { get; set; }
        public string employerName { get; set; }
        public string employerAddress { get; set; }
        public int? takeHomePay { get; set; }
    }
}
