﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModels
{
    public class CustomerDetailDataModel
    {
        public int customerId { get; set; }
        public string customerNumber { get; set; }
        public int? customerTypeId { get; set; }
        public int tenancyCount { get; set; }
        public int? titleId { get; set; }
        public string titleOther { get; set; }//remove

        [JsonIgnore]
        public string prefContact { get; set; }
        public int? preferedContactId { get; set; }   
        
        [JsonIgnore]
        public string profilePicture { get; set; }  
        public string firstName { get; set; }
        public string middleName { get; set; }
        public string lastName { get; set; }             
        public string gender { get; set; }
        public DateTime? dob { get; set; }
        public int? maritalStatusId { get; set; }
        public Nullable<int> ethnicityId { get; set; }
        
        public string niNumber { get; set; }
        
        public Nullable<int> religionId { get; set; }     
        public Nullable<int> sexualOrientationId { get; set; }
        [JsonIgnore]
        public string sexualOrientationOther { get; set; }
        public string firstLanguage { get; set; }
        public string communication { get; set; }
        [JsonIgnore]
        public string communicationOther { get; set; }          
        public string internetAccess { get; set; }

        public int? nationalityId { get; set; }

       
        public int? isSubjectToImmigration { get; set; }
        public int? isPermanentUKResident { get; set; }
        
        public CustomerCurrentHomeDataModel currentHome { get; set; }     
        public CustomerImageDataModel customerImage { get; set; }
        public List<CustomerAddressDataModel> customerAddress { get; set; }
        public TenancyDataModel tenancy { get; set; }
        public EmploymentInfoDataModel employmentInfo { get; set; }
        public CustomerGeneralInfoDataModel customerGeneralInfo { get; set; }
        public int? createdBy { get; set; }
        public string appVersion { get; set; }
        public DateTime? createdOnApp { get; set; }
        public DateTime? createdOnServer { get; set; }
        public DateTime? lastModifiedOnApp { get; set; }
        public DateTime? lastModifiedOnServer { get; set; }
        public int? adults { get; set; }
        public int? children { get; set; }
    }
}
