﻿using System;

namespace BusinessModels
{
    public class TenancyTerminationDataModel
    {
        public int terminationHistoryId { get; set; }
        public int? journalId { get; set; }
        public int? customerId { get; set; }
        public int? reasonId { get; set; }
        public DateTime? terminationDate { get; set; }
        public int? reasonCategoryId { get; set; }
        public int? terminatedBy { get; set; }
        public string notes { get; set; }
        public string AppVersion { get; set; }
        public ForwardingAddress forwardAddress { get; set; }
    }
}
