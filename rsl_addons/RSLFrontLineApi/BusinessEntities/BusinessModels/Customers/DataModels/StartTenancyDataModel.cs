﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModels
{
   public class StartTenancyDataModel
    {
        public decimal? rentPayable { get; set; }
        public decimal? rent { get; set; }
        public decimal? services { get; set; }
        public decimal? councilTax { get; set; }
        public decimal? waterRates { get; set; }
        public decimal? ineligServ { get; set; }
        public decimal? supportedServices { get; set; }
        public decimal? garage { get; set; }
        public bool isEvictedFromTenancy { get; set; }
        public string reason { get; set; }
        public bool? debtReliefOrder { get; set; }
        public bool? unsatisfiedCCJ { get; set; }
        public bool? criminalOffence { get; set; }
        public int? tenancyId { get; set; }
        public int? customerId { get; set; }
    }
}
