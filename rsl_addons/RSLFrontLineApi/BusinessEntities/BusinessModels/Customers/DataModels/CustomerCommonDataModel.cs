﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModels
{
   public  class CustomerCommonDataModel
    {
        public CustomerLookUpsDataModel lookups { get; set; }
        public string profileThumb { get; set; }
        public string cusProfile { get; set; }
    }
}
