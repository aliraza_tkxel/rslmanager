﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModels
{
    public class TenancyDataModel
    {
        public int? tenancyId { get; set; }
        public string propertyId { get; set; }
        public int? customerId { get; set; }
        public DateTime? tenancyStartDate { get; set; }
        public DateTime? tenancyEndDate { get; set; }
        public TenancyAddress tenancyAddress { get; set; }
    }
}
