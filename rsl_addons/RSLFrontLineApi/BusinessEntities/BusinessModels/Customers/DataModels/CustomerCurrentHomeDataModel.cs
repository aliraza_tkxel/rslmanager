﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModels
{
   public  class CustomerCurrentHomeDataModel
    {
        public int currentHomeId { get; set; }
        [JsonIgnore]
        public int? customerId { get; set; }
        public Nullable<int> landlordTypeId { get; set; }
        public string landlordName { get; set; }
        public Nullable<int> houseTypeId { get; set; }
        public int? floor { get; set; }
        public Nullable<int> livingWithFamilyId { get; set; }
        public Nullable<int> noOfBedrooms { get; set; }
        public Nullable<System.DateTime> tenancyStartDate { get; set; }
        public Nullable<System.DateTime> tenancyEndDate { get; set; }
        public string previousLandLordName { get; set; }
    }
}
