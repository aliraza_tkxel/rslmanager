﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModels
{
   public class CustomerReferralsDataModel
    {          
        public int customerId { get; set; }
        public string referrerName { get; set; }
        public bool? isTypeArrears { get; set; }
        public bool? isTypeDamage { get; set; }
        public bool? isTypeAsb { get; set; }
        public byte? stageId { get; set; }
        public byte? safetyIssueId { get; set; }
        public byte? isConviction { get; set; }
        public byte? isSubstanceMisuse { get; set; }
        public byte? isSelfHarm { get; set; }
        public string yesNotes { get; set; }
        public string neglectNotes { get; set; }
        public string otherNotes { get; set; }
        public string miscNotes { get; set; }
        public int journalId { get; set; }
        public int referralHistoryId { get; set; }
        public byte? isTenantAware { get; set; }
        public string title { get; set; }
        public string appVersion { get; set; }
        public DateTime? createdOnApp { get; set; }
        public DateTime? createdOnServer { get; set; }
        public DateTime? lastModifiedOnApp { get; set; }
        public DateTime? lastModifiedOnServer { get; set; }
        public string customerHelpSubCategories { get; set; }
        public int? lastActionUserId { get; set; }
    }
}
