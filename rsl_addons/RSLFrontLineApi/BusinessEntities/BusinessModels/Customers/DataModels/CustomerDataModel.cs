﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BusinessModels
{
    public class CustomerDataModel
    {
        public int customerId { get; set; }     
        public string firstName { get; set; }
        public string lastName{ get; set; }
        public DateTime? dob { get; set; }
        public string gender { get; set; }
        public int? customerTypeId { get; set; }
        public int tenancyCount { get; set; }
        public CustomerImageDataModel customerImage { get; set; }
        public List<CustomerAddressDataModel> customerAddress { get; set; }      
         [JsonIgnore]
         public int? tenancyId { get; set; }      
         [JsonIgnore]
         public string profilePicture{ get; set; }
    }
}
