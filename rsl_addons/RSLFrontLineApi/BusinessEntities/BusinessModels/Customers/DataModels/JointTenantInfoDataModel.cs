﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModels
{
  public class JointTenantInfoDataModel
    {      
        public int tenancyId { get; set; }
        public int customerId { get; set; }
        public DateTime? startDate { get; set; }
        public int? isCorrespondingAddress { get; set; }
        public EmploymentInfoDataModel employmentInfo { get; set; }
        public CustomerGeneralInfoDataModel customerGeneralInfo { get; set; }
        public EmergencyContactDataModel emergencyContact { get; set; }
    }
}
