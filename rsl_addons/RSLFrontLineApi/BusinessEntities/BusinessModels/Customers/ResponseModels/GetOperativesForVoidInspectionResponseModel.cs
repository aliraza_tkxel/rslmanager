﻿using System.Collections.Generic;

namespace BusinessModels
{
    public class GetOperativesForVoidInspectionResponseModel
    {
        public List<Supervisor> Supervisors { get; set; }
    }

    public class Supervisor
    {
        public int EmployeeId { get; set; }
        public string FullName { get; set; }
    }
}