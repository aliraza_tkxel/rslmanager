﻿namespace BusinessModels.BusinessModels.Customers.ResponseModels
{

    public class VoidInspectionResponseModel
    {
        public Response Response { get; set; }
    }

    public class Response
    {
        public bool IsSaved { get; set; }
        public int AppointmentIdOut { get; set; }
        public string StatusMessage { get; set; }
    }
}
