﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModels
{
    public class CustomerSearchResponseModel
    {
        public List<CustomerDataModel> customerList { get; set; }
        //public PaginationDataModel pagination { get; set; }       
        public CustomerCommonDataModel commonData { get; set; }
    }
}
