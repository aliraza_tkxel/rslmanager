﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModels
{
    public class UploadProfilePicResponseModel
    {
        public int customerId { get; set; }
        public string ProfilePicPath { get; set; }
        public string ProfilePicThumb { get; set; }

    }
}
