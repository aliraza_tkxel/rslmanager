﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModels
{
   public class TenancyInfoResponseModel
    {       
        public int customerId { get; set; }
        public int? employmentStatusId { get; set; }
        public string occupation { get; set; }
        public string employerName { get; set; }
        public string employerAddress { get; set; }
        public string benefitId { get; set; }
        public string bankingId { get; set; }
        public string bankingOthers { get; set; }
        public string healthProblemId { get; set; }
        public string healthProblemOthers { get; set; }
        public short? carer { get; set; }
        public short? wheelChair { get; set; }
        public string supportAgencyId { get; set; }
        public string supportAgencyOthers { get; set; }
        public string aidAndAdaptationId { get; set; }
        public int addressId { get; set; }
        public int? addressTypeId { get; set; }
        public string contactName { get; set; }
        public string address { get; set; }        
        public string telephone { get; set; }
        public string mobile { get; set; }
        public string email { get; set; }
                  
    }
}
