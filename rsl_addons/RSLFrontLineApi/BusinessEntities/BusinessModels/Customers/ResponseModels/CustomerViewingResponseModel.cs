﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessModels
{
  public class CustomerViewingResponseModel
    {
        public int viewingId { get; set; }
        public DateTime viewingDateTime { get; set; }
        public int housingOfficerId { get; set; }
        public string housingOfficerName { get; set; }
        public string propertyAddress { get; set; }
        public string propertyId { get; set; }
        public int viewingStatusId { get; set; }
        public string viewingTime { get; set; }



    }
}
