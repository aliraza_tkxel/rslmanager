﻿using System;

namespace BusinessModels
{
    public class CustomerViewingRequestModel
    {
        public int customerId { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }        
    }
}
