﻿namespace BusinessModels
{
    public   class CustomerSearchRequestModel
    {
        public PaginationDataModel pagination { get; set; }
        public string searchText { get; set; }
        public bool isLookUpRequired { get; set; }
    }
}
