﻿using System;

namespace BusinessModels.BusinessModels.Customers.RequestModels
{
    public class OperativesForVoidInspectionRequestModel
    {
        public string tradeIds { get; set; }
        public string msattype { get; set; }
        public DateTime startDate { get; set; }
        public DateTime aptStartDateTime { get; set; }
        public DateTime aptEndDateTime { get; set; }
    }
}