﻿using System;

namespace BusinessModels.BusinessModels.Customers.RequestModels
{
    public class VoidInspectionRequestModel
    {
        public int UserId { get; set; }
        public string AppointmentNotes { get; set; }
        public DateTime AppointmentStartDate { get; set; }
        public DateTime AppointmentEndDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public int OperativeId { get; set; }
        public int JournalId { get; set; }
        public double Duration { get; set; }
        public bool IsRearrange { get; set; }
    }
}