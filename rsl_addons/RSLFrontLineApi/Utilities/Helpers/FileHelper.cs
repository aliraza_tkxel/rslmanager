﻿using System;
using System.Configuration;
using System.Drawing;
using System.IO;
using System.Web;

namespace Utilities
{
    public class FileHelper
    {
        public static void WriteFileFromStream(Stream stream, string filePath)
        {
            using (FileStream fileToSave = new FileStream(filePath, FileMode.Create))
            {
                stream.CopyTo(fileToSave);
            }
        }

        public static string GetProfilePicUploadPath()
        {
            string imageUploadPath = ConfigurationManager.AppSettings["ProfilePicUploadPath"].ToString();
            //return HttpContext.Current.Server.MapPath(imageUploadPath);
            return imageUploadPath;
        }

        #region "getUniqueFileName"

        /// <summary>
        /// This function returns the unique filename
        /// </summary>
        /// <param name="pfileName"></param>
        /// <returns></returns>
        public static string getUniqueFileName(string pfileName)
        {
            string fileName = Path.GetFileNameWithoutExtension(pfileName);
            string ext = Path.GetExtension(pfileName);
            string uniqueString = DateTime.Now.ToString().GetHashCode().ToString("x");

            if (fileName.Length > 35)
            {
                fileName = fileName.Substring(0, 35);
            }

            fileName = fileName + uniqueString;

            return fileName + ext;
        }

        #region saving customer profile image on disk

        public static void saveImageOnDisk(string srcImagePath, string dstImagePath, long fileSize)
        {
            FileStream inStream = new FileStream(srcImagePath, FileMode.Open);
            FileStream outStream = File.Open(dstImagePath, FileMode.Create, FileAccess.Write);
            //read from the input stream in 4K chunks and save to output stream

            int bufferLen = (int)fileSize;
            byte[] buffer = new byte[bufferLen];
            int count = 0;
            while ((count = inStream.Read(buffer, 0, bufferLen)) > 0)
            {
                outStream.Write(buffer, 0, count);
            }
            outStream.Close();
            inStream.Close();
        }
        #endregion

        #endregion

        #region "Generate Thumbnail"

        /// <summary>
        /// This function generates thumbnails
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="thumbnailFolderPath"></param>
        public static void generateThumbnail(string fileName, string thumbnailFolderPath, string fullImagePath)
        {
            //Create a new Bitmap Image loading from location of origional file            
            Bitmap bm = (Bitmap)Image.FromFile(fullImagePath);

            string strFileName = fileName;

            //Declare Thumbnails Height and Width
            int newWidth = 100;
            int newHeight = 100;
            //Create the new image as a blank bitmap
            Bitmap resized = new Bitmap(newWidth, newHeight);
            //Create a new graphics object with the contents of the origional image
            Graphics g = Graphics.FromImage(resized);
            //Resize graphics object to fit onto the resized image
            g.DrawImage(bm, new Rectangle(0, 0, resized.Width, resized.Height), 0, 0, bm.Width, bm.Height, GraphicsUnit.Pixel);
            //Get rid of the evidence
            g.Dispose();

            if (Directory.Exists(thumbnailFolderPath) == false)
            {
                Directory.CreateDirectory(thumbnailFolderPath);
            }
            //Create new path and filename for the resized image
            string newStrFileName = thumbnailFolderPath + strFileName;
            //Save the new image to the same folder as the origional
            resized.Save(newStrFileName);
            resized.Dispose();
            bm.Dispose();
        }

        #endregion


        public static void createDirectory(string path)
        {
            try
            {
                if (!Directory.Exists(@path))
                {
                    Directory.CreateDirectory(@path);
                }

            }
            catch (IOException ex)
            {
                throw ex;
            }
        }

        public static string getLogicalCustomerProfileImagePath()
        {
            var requestURL = HttpContext.Current.Request.Url;
            string imagePath = requestURL.GetLeftPart(UriPartial.Authority) + "/CustomerProfilePic/Images/";

            return imagePath;
        }
        public static string getLogicalCustomerProfileThumbPath()
        {
            var requestURL = HttpContext.Current.Request.Url;
            string imagePath = requestURL.GetLeftPart(UriPartial.Authority) + "/CustomerProfilePic/Thumbs/";
            return imagePath;
        }
        #region Delete file        
        public static void deleteImage(string filePath)
        {
            if (File.Exists(filePath) == true)
            {
                File.Delete(filePath);
            }
        }
        #endregion
    }
}
