﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.IO;
namespace Utilities
{
   public class EmailHelper
    {
        public static void sendHtmlFormattedEmail(String recepientName, String recepientEmail, String subject, String body)
        {
            try
            {

                MailMessage mailMessage = new MailMessage();
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = true;
                mailMessage.To.Add(new MailAddress(recepientEmail, recepientName));

                //The SmtpClient gets configuration from Web.Config
                SmtpClient smtp = new SmtpClient();
                smtp.Send(mailMessage);

            }
            catch (IOException)
            {             
                throw new ArgumentException(UserMessageConstants.ErrorSendingEmailMsg);
            }
        }
    }
}
