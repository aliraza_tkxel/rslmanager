﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Configuration;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Utilities.Helpers
{
  public static  class GeneralHelper
    {
        public static void SendEmail(string subject,string body,string recipient,bool IsBodyHtml)
        {

            try
            {


                if (!String.IsNullOrEmpty(recipient))
                {
                    MailMessage mailMessage = new MailMessage();
                    var smtpSection = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");
                    mailMessage.Subject = subject;
                    mailMessage.Body = body;
                    mailMessage.IsBodyHtml = IsBodyHtml;
                    mailMessage.To.Add(new MailAddress(recipient));
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = smtpSection.Network.Host;
                    smtp.EnableSsl = smtpSection.Network.EnableSsl;
                    System.Net.NetworkCredential NetworkCred = new System.Net.NetworkCredential();
                    NetworkCred.UserName = smtpSection.Network.UserName;
                    NetworkCred.Password = smtpSection.Network.Password;
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = smtpSection.Network.Port;
                    smtp.Send(mailMessage);

                }

            }
            catch (Exception ex)
            {
               
               throw ex;
            }

        }
        public static void SendSMS(string body,string recepientMobile)
        {
            String result;
            string apiKey = "nVv8yYd3mYo-1UXMeTpnYJ4pXqspvYPrgC7HTsYH3s";
            string sender = "Jims Autos";
            String url = "https://api.txtlocal.com/send/?apikey=" + apiKey + "&numbers=" + recepientMobile + "&message=" + body + "&sender=" + sender;
            //refer to parameters to complete correct url string
            StreamWriter myWriter = null;
            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(url);
            objRequest.Method = "POST";
            objRequest.ContentLength = Encoding.UTF8.GetByteCount(url);
            objRequest.ContentType = "application/x-www-form-urlencoded";
            try
            {
                myWriter = new StreamWriter(objRequest.GetRequestStream());
                myWriter.Write(url);
            }
            catch (Exception e)
            {
                //return e.Message;
            }
            finally
            {
                myWriter.Close();
            }

            HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
            using (StreamReader sr = new StreamReader(objResponse.GetResponseStream()))
            {
                result = sr.ReadToEnd();
                // Close and clean up the StreamReader
                sr.Close();
            }
            //return result;

        }
    }
}
