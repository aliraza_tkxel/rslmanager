﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities
{
   public class UserMessageConstants
    {
        //Login Messages
        public const string LoginEmailSubjectMsg = "RSL Manager Login Information";
        public const string LoginScccessMsg = "Authenticated";
        public const string LoginFailureMsg = "Invalid username or password!";
        public const string AccessLockdownMsg = "Your account is now locked, please contact the Systems Team for assistance.";
        public const string AccessLockdownWarningMsg = "You have one more login attempt before your access is locked down!";
        public const string IncorrectUsernamePasswordMsg = "Your username or password do not appear to be correct.";
        public const string ErrorSendingEmailMsg = "An error occurred while sending email, please try again.";
        public const string EmailIdNotMatchMsg = "Your email address does not match any of our records, please try again.";       
        public const string TerminationExist = "This tenancy is already terminated, and cannot be terminated again";
        public const string ProfilePicUploadedSuccessfully = "Image has been uploaded successfully.";
        public const string TenancyAlreadyExist = "Tenancy already exist";
        public const string ViewingFeedbackRecordingSuccess = "Your feedback has been recorded successfully.";

    }
}
