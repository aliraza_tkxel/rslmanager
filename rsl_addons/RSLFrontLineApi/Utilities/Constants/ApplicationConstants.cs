﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities
{
   public class ApplicationConstants
    {
        #region property search constants       
        public const string ParameterName = "Quantity";
        public const string ItemName = "Bedrooms";
        public const string AvailableToRentPropertyStatus = "Available to rent";
        public const string propertySubStatusPendingTenancy = "Pending Tenancy";
        public const string propertySubStatusNotAvailable = "N/A";
        public const string propertySubStatusPendingTermination = "Pending Termination";
        public const string propertyStatusLet = "Let";
        public const string viewingExist = "Viewing already exists for Housing Officer";
        #endregion

        #region accommodation info constants
        public const string Bathroom = "Bathroom";
        public const string BedroomsQuantity = "Quantity";
        public const string HeatingType = "Heating Type";
        public const string LivingRoom = "Living Room";
        public const string Garage = "Garage";
        public const string HeatingFuel = "Heating Fuel";
        public const string FloorArea = "Floor Area(sq m)";
        public const string Parking = "Parking";
        public const string Garden = "Garden";
        public const string Kitchen = "Kitchen";
        public const string MaxPeople = "Max People";
        public const string WheelChairAccess = "W/Chair Access";
        #endregion

        #region Property Types Not Allow
        public const string CarSpaceType = "Car Space";
        public const string GarageType = "Garage";
        public const string CarPortType = "Car Port";
        #endregion

        #region viewing constants

        //Housing Team
        public const string TeamTypeHousingTeam = "Housing Team";
        public const string JobRoleHousingTeamNeighbourhoodofficer = "Neighbourhood Officer";
        public const string JobRoleHousingTeamLeader = "Housing Team Leader";
        public const string JobRoleHousingTeamLocalAreaAdvisor = "Local Area Advisor";
        public const string JobRoleHousingTeamLocalAreaAdvisorApprentice = "Local Area Advisor Apprentice";
        public const string JobRoleHousingTeamSchemeManager = "Scheme Manager";
        public const string JobRoleHousingTeamSeniorHousingManager = "Senior Housing Manager";

        //Digital Engagement Team
        public const string TeamTypeDigitalEngagementTeam = "Digital Engagement";
        public const string JobRoleDigitalEngagementTeamReleaseManager = "Release and Deployment Manager";
        public const string JobRoleDigitalEngagementTeamSkillsTrainer = "Digital Skills Trainer";
        public const string JobRoleDigitalEngagementTeamSupportAssistant = "Digital Support Assistant";

        #endregion

        #region Referral constants
        public const string ItemTypeTenant = "Tenant";
        public const string NatureTypeTenantSupportReferral = "Tenant Support Referral";
        public const string ReferralstatusPending = "Pending";
        public const string ItemActionReferred = "Referred";
        #endregion

        #region Tenancy constants
        public const string NatureTypeTermination = "Termination";
        public const string ItemActionLog = "Log";

        public const string MutualExchangeBHA = "Mutual Exchange – BHA";
        public const string MutualExchangeLA = "Mutual Exchange – LA";
        public const string MutualExchangeRP = "Mutual Exchange – RP";

        public const string CustomerTypeTenant = "Tenant";
        public const int ItemId = 2;
        public const int ItemNatureId = 27;

        #endregion

        #region start tenancy constants
        public const string AddressTypeICE = "ICE";
        public const string itemTypeInitialRent = "Initial Rent";
        #endregion

        #region Risk/Vulnerabilty constants
        public const string TeamHousingServices = "Housing Services";
        public const string TrainingAndEmployment = "Training and Employment Services (Dibden Road)";
        public const string TeamHousingTeam = "Housing Team";
        public const string ItemStatusClosed = "Closed";
        #endregion

        #region LandLordName/LandLorType
        public const string bhgLandLord = "Broadland Housing";
        public const string privateLandLord = "Private";
        public const string socialLandLord = "Social";
        #endregion
    }
}
