﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BusinessModels;
using Utilities;
using Services.BusinessLogic.Interfaces;
using log4net;
using System.IO;
using System.Threading.Tasks;
using BusinessModels.BusinessModels.Customers.RequestModels;
using BusinessModels.BusinessModels.Customers.ResponseModels;

namespace FrontLineAPI.Controllers
{   
    public class CustomerController : ABaseController
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(CustomerController));
        public CustomerController(IServiceProxy service)
            : base(service)
        {

        }

        #region Search Customer List with lookup tables

        [HttpPost]
        public HttpResponseMessage SearchCustomer(CustomerSearchRequestModel CustomerSearchRequestModel)
        {

            try
            {
                CustomerSearchResponseModel response = new CustomerSearchResponseModel();
                response = Services.CustomerService.SearchCustomer(CustomerSearchRequestModel);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Search Customer List with lookup tables--------------------------");
                log.Error(ex.Message + ex.InnerException);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region Upload Profile Pic

        [HttpPost]
        public async Task<HttpResponseMessage> UploadProfilePic()
        {
             
            try
            {
                if (!Request.Content.IsMimeMultipartContent())
                {
                    throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
                }

                var streamProvider = new MultipartFormDataStreamProvider(FileHelper.GetProfilePicUploadPath());
                var result = await Request.Content.ReadAsMultipartAsync(streamProvider);

                string fileNames = "";
                foreach (MultipartFileData file in streamProvider.FileData)
                {
                    string tempFileName = file.Headers.ContentDisposition.FileName;
                    fileNames = Path.GetFileName(tempFileName.Replace("\"", ""));


                    long dataLength = 0;
                    string imageName = string.Empty;
                    string imageUploadPath = string.Empty;
                    string thumbsUploadPath = string.Empty;
                    imageUploadPath = FileHelper.GetProfilePicUploadPath() + "\\Images\\";
                    thumbsUploadPath = FileHelper.GetProfilePicUploadPath() + "\\Thumbs\\";
                    FileHelper.createDirectory(imageUploadPath);

                    dataLength = long.Parse(file.LocalFileName.Length.ToString());
                    
                    //Save Image
                    FileHelper.saveImageOnDisk(file.LocalFileName, imageUploadPath + fileNames, dataLength);
                    FileHelper.generateThumbnail(fileNames, thumbsUploadPath, imageUploadPath + fileNames);
                    FileHelper.deleteImage(file.LocalFileName);                             
            }
                    return Request.CreateResponse(HttpStatusCode.OK, UserMessageConstants.ProfilePicUploadedSuccessfully);
            }
            catch (Exception ex)
            {
                log.Error("---------------Upload profile picture  --------------------------");
                log.Error(ex.Message + ex.InnerException);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region Add Edit customer

        [HttpPost]
        public HttpResponseMessage AddEditCustomer(CustomerDetailDataModel customerModel)
        {

            try
            {

                var response = Services.CustomerService.AddEditCustomer(customerModel);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Add Edit customer--------------------------");
                log.Error(ex.Message + ex.InnerException);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region Get customer detail by Customer Id

        [HttpGet]
        public HttpResponseMessage GetCustomerDetail(int customerId)
        {

            try
            {

                var response = Services.CustomerService.GetCustomerDetail(customerId);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Add Edit customer--------------------------");
                log.Error(ex.Message + ex.InnerException);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region Get customer viewings
        [HttpPost]
        public HttpResponseMessage GetCustomerViewings(CustomerViewingRequestModel request)
        {
            log.Debug("Enter:GetCustomerViewings");
            try
            {
                List<CustomerViewingResponseModel> response = new List<CustomerViewingResponseModel>();
                 response = Services.CustomerService.GetCustomerViewing(request);
                 return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Customer Viewings--------------------------");
                log.Error(ex.Message + ex.InnerException);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region Get a customer's referral list
        //[HttpGet]
        //public HttpResponseMessage GetCustomerReferrals(int customerId)
        //{
        //    log.Debug("Enter:GetCustomerReferrals");
        //    try
        //    {
        //        List<CustomerReferralResponseModel> response = new List<CustomerReferralResponseModel>();
        //        response = Services.CustomerService.GetReferralList(customerId);
        //        return Request.CreateResponse(HttpStatusCode.OK, response);
        //    }
        //    catch (Exception ex)
        //    {
        //        log.Error("---------------Get Customer Referrals--------------------------");
        //        log.Error(ex.Message + ex.InnerException);
        //        return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
        //    }
        //}
        #endregion

        #region Get a referral details
        [HttpGet]
        public HttpResponseMessage GetReferralDetails(int customerId, int referralHistoryId= 0)
        {
            log.Debug("Enter:GetReferralDetails");
            try
            {
                CustomerReferralsDataModel response = new CustomerReferralsDataModel();
                response = Services.CustomerService.GetReferralDetails(customerId);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Referral Details--------------------------");
                log.Error(ex.Message + ex.InnerException);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region Add a referral
        [HttpPost]
        public HttpResponseMessage AddEditReferral(CustomerReferralsDataModel request)
        {
            log.Debug("Enter:AddReferral");
            try
            {
                CustomerReferralsDataModel response = new CustomerReferralsDataModel();
                response = Services.CustomerService.AddEditReferral(request);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Add a Referral--------------------------");
                log.Error(ex.Message + ex.InnerException);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion
      
        #region Terminate tenancy
        [HttpPost]
        public HttpResponseMessage TerminateTenancy(TenancyTerminationDataModel request)
        {
            log.Debug("Enter:TerminateTenancy");
            try
            {
                TenancyTerminationDataModel response = new TenancyTerminationDataModel();
                response = Services.CustomerService.TerminateTenancy(request);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Terminate Tenancy--------------------------");
                log.Error(ex.Message + ex.InnerException);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region start tenancy
        [HttpPost]
        public HttpResponseMessage CreateTenancy(TenancySetupDataModel request)
        {
            log.Debug("Enter:CreateTenancy");
            try
            {
                
                var response =  Services.CustomerService.CreateTenancy(request);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Create Tenancy--------------------------");
                log.Error(ex.Message + ex.InnerException);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region Get customer Risks
        [HttpGet]
        public HttpResponseMessage GetCustomerRisksList(int customerId)
        {
            log.Debug("Enter:GetCustomerRisksList");
            try
            {

                var response = Services.CustomerService.GetCustomerRisksList(customerId);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Customer Risks List--------------------------");
                log.Error(ex.Message + ex.InnerException);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region Get customer Vulnerabilities
        public HttpResponseMessage GetCustomerVulnerabilitiesList(int customerId)
        {
            log.Debug("Enter:GetCustomerVulnerabilitiesList");
            try
            {

                var response = Services.CustomerService.GetCustomerVulnerabilitiesList(customerId);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Customer Vulnerabilities List--------------------------");
                log.Error(ex.Message + ex.InnerException);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region Fetch Available Supervisors
        [HttpPost]
        public HttpResponseMessage GetSupervisorsForVoidInspectionList(OperativesForVoidInspectionRequestModel model)
        {
            log.Debug("Enter:GetSupervisorForVoidInspectionList");
            try
            {
                var response = Services.CustomerService.GetAvailablePublishers(model);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Supervisors For Void Inspection List--------------------------");
                log.Error(ex.Message + ex.InnerException);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion

        #region Save Void Inspection

        [HttpPost]
        public HttpResponseMessage SaveVoidInspection(VoidInspectionRequestModel model)
        {
            log.Debug("Enter:SaveVoidInspection");
            try
            {
                VoidInspectionResponseModel response = Services.CustomerService.SaveVoidInspection(model);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Save Void Inspection--------------------------");
                log.Error(ex.Message + ex.InnerException);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }

        #endregion
    }
}
