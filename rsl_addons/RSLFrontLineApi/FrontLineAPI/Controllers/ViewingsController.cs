﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Services.BusinessLogic.Interfaces;
using BusinessModels;
namespace FrontLineAPI.Controllers
{
    public class ViewingsController : ABaseController
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(ViewingsController));

        public ViewingsController(IServiceProxy services) : base(services)
        {

        }

        #region Get housing officer

        [HttpPost]
        public HttpResponseMessage GetHousingOfficer(HousingOfficerFilter filter)
        {
            log.Debug("Enter:GetHousingOfficer");
            try
            {
                List<HousingOfficerResponseModel> response = new List<HousingOfficerResponseModel>();
                response = Services.ViewingsService.GetHousingOfficer(filter);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Housing Officer--------------------------");
                log.Error(ex.Message + ex.InnerException);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region arrange new viewing
        [HttpPost]
        public HttpResponseMessage ArrangeViewing([FromBody] ArrangeViewingRequestModel request)
        {
            log.Debug("Enter:ArrangeViewing");
            try
            {
                ArrangeViewingResponseModel response = new ArrangeViewingResponseModel();
                response = Services.ViewingsService.ArrangeViewing(request);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Arrange new Viewing--------------------------");
                log.Error(ex.Message + ex.InnerException);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region Get details of a viewing
        [HttpGet]
        public HttpResponseMessage GetViewingDetails(int viewingId)
        {
            log.Debug("Enter:GetViewingDetails");
            try
            {
                ViewingDetailsResponseModel response = new ViewingDetailsResponseModel();
                response = Services.ViewingsService.GetViewingDetails(viewingId);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get a Viewing Details--------------------------");
                log.Error(ex.Message + ex.InnerException);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region Get viewing list
        [HttpPost]
        public HttpResponseMessage GetViewingList(ViewingListRequestModel request)
        {
            log.Debug("Enter:GetViewingList");
            try
            {
                ViewingListResponseModel response = new ViewingListResponseModel();
                response = Services.ViewingsService.GetViewingList(request);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Viewing List--------------------------");
                log.Error(ex.Message + ex.InnerException);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region "Add Feedback to viewing"
        [HttpPost]
        public HttpResponseMessage AddCommentToViewing(ViewingCommentRequestModel request)
        {
            log.Debug("Enter:AddCommentToViewing");
            try
            {
                AddCommentToViewingResponseModel response = new AddCommentToViewingResponseModel();
                response = Services.ViewingsService.AddComment(request);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Viewing List--------------------------");
                log.Error(ex.Message + ex.InnerException);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion


    }
}
