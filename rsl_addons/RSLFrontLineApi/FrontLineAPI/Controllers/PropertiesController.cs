﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Services.BusinessLogic.Interfaces;
using BusinessModels;
using log4net;
namespace FrontLineAPI.Controllers
{
    public class PropertiesController : ABaseController
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(PropertiesController));
        public PropertiesController(IServiceProxy services) : base(services)
        {

        }

        #region Search Properties
        [HttpGet]
        public HttpResponseMessage SearchProperties(bool isLookUpRequired)
        {
            log.Debug("Enter:SearchProperties");
            try
            {
                PropertiesSearchResponseModel response = new PropertiesSearchResponseModel();
                response = Services.PropertiesService.SearchProperties(isLookUpRequired);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Search Properties--------------------------");
                log.Error(ex.Message + ex.InnerException);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region Get Property details
        [HttpGet]
        public HttpResponseMessage GetPropertyDetails(string propertyId)
        {
            log.Debug("Enter:GetPropertyDetails");
            try
            {
                PropertyInfoResponseModel response = new PropertyInfoResponseModel();
                response = Services.PropertiesService.GetPropertyDetails(propertyId);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Property Details--------------------------");
                log.Error(ex.Message + ex.InnerException);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region Get property viewings
        [HttpPost]
        public HttpResponseMessage GetPropertyViewings(PropertyViewingRequestModel request)
        {
            log.Debug("Enter:GetPropertyViewings");
            try
            {
                List<PropertyViewingResponseModel> response = new List<PropertyViewingResponseModel>();
                response = Services.PropertiesService.GetPropertyViewing(request);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Property viewings--------------------------");
                log.Error(ex.Message + ex.InnerException);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region Get property financial info on start tenancy screen 
        [HttpGet]
        public HttpResponseMessage GetPropertyFinancialInfo(string propertyId)
        {
            log.Debug("Enter:GetPropertyFinancialInfo");
            try
            {
                PropertyRentInfoDataModel response = new PropertyRentInfoDataModel();
                response = Services.PropertiesService.GetPropertyFinancialInfo(propertyId);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch(Exception ex)
            {
                log.Error("---------------Get Property Financial Info--------------------------");
                log.Error(ex.Message + ex.InnerException);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }

        }
        #endregion
    }
}
