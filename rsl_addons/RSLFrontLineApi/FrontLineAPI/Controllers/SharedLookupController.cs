﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Services.BusinessLogic.Interfaces;
using BusinessModels;

namespace FrontLineAPI.Controllers
{
    public class SharedLookupController : ABaseController
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(SharedLookupController));
        public SharedLookupController(IServiceProxy services) : base(services)
        {

        }
        #region Get Shared lookups
        [HttpGet]        
        public HttpResponseMessage GetSharedLookups()
        {
            log.Debug("Enter:GetSharedLookups");
            try
            {
                SharedLookupResponseModel response = new SharedLookupResponseModel();
                response = Services.SharedService.LoadSharedLookUps();                
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Shared Lookups--------------------------");
                log.Error(ex.Message + ex.InnerException);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion
    }
}
