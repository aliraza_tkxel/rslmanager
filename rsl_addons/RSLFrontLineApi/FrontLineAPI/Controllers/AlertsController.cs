﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BusinessModels;
using log4net;
using Services.BusinessLogic.Interfaces;

namespace FrontLineAPI.Controllers
{
    public class AlertsController : ABaseController
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(AlertsController));

        public AlertsController(IServiceProxy services) : base(services)
        {
        }

        #region Get Risks listing
        [HttpGet]
        public HttpResponseMessage GetRisksList(int? statusId)
        {
            log.Debug("Enter:GetRisksList");
            try
            {

                var response = Services.AlertsService.GetRisksList(statusId,null);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Risks List--------------------------");
                log.Error(ex.Message + ex.InnerException);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region Get Customer Vulnerability
        [HttpGet]
        public HttpResponseMessage GetVulnerabilitiesList(int? statusId)
        {
            log.Debug("Enter:GetVulnerabilitiesList");
            try
            {

                var response = Services.AlertsService.GetVulnerabilitiesList(statusId,null);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Get Vulnerabilities List--------------------------");
                log.Error(ex.Message + ex.InnerException);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region Add/Edit Vulnerability details
        [HttpPost]
        public HttpResponseMessage AddEditVulnerabilities(VulnerabilitiesListingDataModel request)
        {
            log.Debug("Enter:AddEditVulnerabilities");
            try
            {

                var response = Services.AlertsService.AddEditVulnerabilities(request);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Add Edit Vulnerabilities--------------------------");
                log.Error(ex.Message + ex.InnerException);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region Add/Edit Risk details
        [HttpPost]
        public HttpResponseMessage AddEditRisks(RisksListingDataModel request)
        {
            log.Debug("Enter:AddEditRisks");
            try
            {

                var response = Services.AlertsService.AddEditRisks(request);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Add Edit Risks--------------------------");
                log.Error(ex.Message + ex.InnerException);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

    }
}
