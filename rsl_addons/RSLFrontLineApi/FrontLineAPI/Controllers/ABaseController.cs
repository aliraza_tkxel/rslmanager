﻿using Services.BusinessLogic.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace FrontLineAPI.Controllers
{
    public abstract class ABaseController : ApiController
    {
        public IServiceProxy Services { get; set; }
        public  ABaseController(IServiceProxy services)
        {
            this.Services = services;
        }

        protected HttpResponseMessage ModelFormatError(ModelStateDictionary modelStates)
        {
            List<string> errors = new List<string>();
            List<ModelError> modelErrors = new List<ModelError>();

            for (int i = 0; i < ModelState.Values.Count; i++)
            {
                modelErrors.AddRange(ModelState.Values.ElementAt(i).Errors);
            }

            foreach (var error in modelErrors)
            {
                errors.Add(error.ErrorMessage);
            }

            FrontLineFailureWrapperViewModel failure;

            try
            {
                failure = new FrontLineFailureWrapperViewModel()
                {
                    Response = new FrontLineStatusViewModel
                    {
                        StatusMessage = errors.First(),
                        Success = false
                    }
                };
            }
            catch (Exception)
            {
                failure = new FrontLineFailureWrapperViewModel()
                {
                    Response = new FrontLineStatusViewModel
                    {
                        StatusMessage = "Failure",
                        Success = false
                    }
                };
            }

            return Request.CreateResponse(HttpStatusCode.BadRequest, failure);
        }

        public class FrontLineFailureWrapperViewModel
        {
            public FrontLineStatusViewModel Response { get; set; }
        }

        public class FrontLineStatusViewModel
        {
            public bool Success { get; set; }
            public string StatusMessage { get; set; }
        }
    }
}
