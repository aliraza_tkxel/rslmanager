﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Services.BusinessLogic.Interfaces;
using BusinessModels;
using log4net;

namespace FrontLineAPI.Controllers
{
    public class AuthenticationController : ABaseController
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(AuthenticationController));
        public  AuthenticationController(IServiceProxy service): base(service)
        {           

        }
        #region Authenticate a user
        [HttpPost]
        public HttpResponseMessage AuthenticateUser([FromBody] LoginRequestModel request)
        {
            log.Debug("Enter:");
            try
            {
                LoginResponseModel response = new LoginResponseModel();
                response = Services.AuthenticationService.AuthenticateUser(request);
                return Request.CreateResponse(HttpStatusCode.OK, response);
            }
            catch (Exception ex)
            {
                log.Error("---------------Authenticate User--------------------------");
                log.Error(ex.Message + ex.InnerException);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

        #region  Email Forgotten Password
        [HttpGet]
        public HttpResponseMessage EmailForgotPassword(string emailId)
        {
            log.Debug("Enter:");
            try
            {              
                Services.AuthenticationService.EmailForgotPassword(emailId);
                return Request.CreateResponse(HttpStatusCode.OK, true);
            }
            catch (Exception ex)
            {
                log.Error("---------------EmailForgotPassword--------------------------");
                log.Error(ex.Message + ex.InnerException);
                return Request.CreateResponse(HttpStatusCode.ExpectationFailed, ex.Message);
            }
        }
        #endregion

       

    }
}
