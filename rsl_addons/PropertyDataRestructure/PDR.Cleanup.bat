#@del /F /Q *.user
@PUSHD PropertyDataRestructure
  @del /F /Q *.user
  @del /F /Q *.log
  @rmdir /S /Q bin
  @rmdir /S /Q obj
@POPD

@PUSHD  PDR_BusinessLogic
  @rmdir /S /Q bin
  @rmdir /S /Q obj
  @del /F /Q *.user
  @del /F /Q *.log
@POPD

@PUSHD PDR_BusinessObject
  @rmdir /S /Q bin
  @rmdir /S /Q obj
  @del /F /Q *.user
  @del /F /Q *.log
@POPD

@PUSHD PDR_DataAccess
  @rmdir /S /Q bin
  @rmdir /S /Q obj
  @del /F /Q *.user
  @del /F /Q *.log
@POPD

@PUSHD PDR_Utilities
  @rmdir /S /Q bin
  @rmdir /S /Q obj
  @del /F /Q *.user
  @del /F /Q *.log
@POPD


@PAUSE