﻿using System;
using System.Collections.Generic;
using System.Text;
using PDR_BusinessLogic.Base;
using System.Data;
using PDR_DataAccess;
using PDR_BusinessObject;
using PDR_BusinessObject.PageSort;
using PDR_Utilities.Helpers;
using PDR_Utilities.Constants;
using PDR_DataAccess.Maintenance;
using System.Linq;

namespace PDR_BusinessLogic.Maintenance
{
   public class MaintenanceBL : Base.BaseBL
    {

        IMaintenanceRepo objMaintenance;
        public MaintenanceBL(IMaintenanceRepo objMain)
        {
            objMaintenance = objMain;
        }

        #region Get Maintenance Types
        /// <summary>
        /// Get Maintenance Types
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <returns></returns>
        public void getMaintenanceTypes(ref DataSet resultDataSet)
        {
            objMaintenance.getMaintenanceTypes(ref resultDataSet);
        }
        #endregion

        #region Get All Schemes
        /// <summary>
        /// Get All Schemes
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <returns></returns>
        public void getAllSchemes(ref DataSet resultDataSet)
        {
            objMaintenance.getAllSchemes(ref resultDataSet);
        }
        #endregion

        #region Get Blocks by scheme
        /// <summary>
        /// Get Blocks by scheme
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <returns></returns>
        public void getBlocksBySchemeId(int schemeId, ref DataSet resultDataSet)
        {
            objMaintenance.getBlocksBySchemeId(schemeId,ref resultDataSet);
        }
        public void getPropertiesBySchemeId(int schemeId, ref DataSet resultDataSet)
        {
            objMaintenance.getPropertiesBySchemeId(schemeId, ref resultDataSet);
        }
        #endregion

        #region Get Properties by block
        /// <summary>
        /// Get Blocks by scheme
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <returns></returns>
        public void getPropertiesByBlockId(int blockId, ref DataSet resultDataSet)
        {
            objMaintenance.getPropertiesByBlockId(blockId, ref resultDataSet);
        }
        
        #endregion


        #region Get Maintenance Statuses
        /// <summary>
        /// Get Maintenance Statuses
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <returns></returns>
        public void getMaintenanceStatuses(ref DataSet resultDataSet)
        {
            objMaintenance.getMaintenanceStatuses(ref resultDataSet);
        }
        #endregion

        #region Get No Entry Data
        /// <summary>
        /// Get No Entry Data
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="schemeId"></param>
        /// <param name="blockId"></param>
        /// <param name="maintenanceType"></param>
        /// <returns></returns>
        public int getNoEntryData(ref DataSet resultDataSet,PageSortBO objPageSort, int schemeId, int blockId, int maintenanceType, bool getOnlyCount = true)
        {
            return objMaintenance.getNoEntryData(ref resultDataSet, objPageSort, schemeId, blockId, maintenanceType, getOnlyCount);
        }
        #endregion

        #region Get Overdue Data
        /// <summary>
        /// Get Overdue Data
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="schemeId"></param>
        /// <param name="blockId"></param>
        /// <param name="maintenanceType"></param>
        /// <returns></returns>
        public int getOverdueData(ref DataSet resultDataSet, PageSortBO objPageSort, int schemeId, int blockId, int maintenanceType, bool getOnlyCount = true)
        {
            return objMaintenance.getOverdueData(ref resultDataSet, objPageSort, schemeId, blockId, maintenanceType, getOnlyCount);
        }
        #endregion

        #region Get Appointment Arranged Data
        /// <summary>
        /// Get Appointment Arranged Data
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="schemeId"></param>
        /// <param name="blockId"></param>
        /// <param name="maintenanceType"></param>
        /// <returns></returns>
        public int getAppointmentsArrangedData(ref DataSet resultDataSet, PageSortBO objPageSort, int schemeId, int blockId, int maintenanceType, bool getOnlyCount = true)
        {
            return objMaintenance.getAppointmentsArrangedData(ref resultDataSet, objPageSort, schemeId, blockId, maintenanceType, getOnlyCount);
        }
        #endregion

        #region Get Appointment To Be Arranged Data
        /// <summary>
        /// Get Appointment To Be Arranged Data
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="schemeId"></param>
        /// <param name="blockId"></param>
        /// <param name="maintenanceType"></param>
        /// <returns></returns>
        public int getAppointmentsToBeArrangedData(ref DataSet resultDataSet, PageSortBO objPageSort, int schemeId, int blockId, int maintenanceType, bool getOnlyCount = true)
        {
            return objMaintenance.getAppointmentsToBeArrangedData(ref resultDataSet, objPageSort, schemeId, blockId, maintenanceType, getOnlyCount);
        }
        #endregion

    }
}
