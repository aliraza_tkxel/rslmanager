﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PDR_BusinessLogic.Base;
using System.Data;
using PDR_BusinessObject.Reports;
using PDR_DataAccess.PurchaseOrder;
using PDR_BusinessObject;
using PDR_BusinessObject.PageSort;
using PDR_DataAccess.AssignToContractor;
using PDR_BusinessLogic;
using PDR_BusinessObject.CompletePurchaseOrder;


namespace PDR_BusinessLogic.PurchaseOrder
{
    public class PurchaseOrderBL : BaseBL
    {
        IPurchaseOrderRepo PoRepo;
        public PurchaseOrderBL(IPurchaseOrderRepo PRepo) 
        {
            PoRepo = PRepo;
        }

        public DataSet GetPurchaseOrder(int PoId, PageSortBO objPageSortBo, ref int totalCount)
        {
            return PoRepo.getPurchaseOrderById(PoId,objPageSortBo,ref totalCount);
        }
        public DataSet UpdatePurchaseOrder(int PoId, PageSortBO objPageSortBo, ref int totalCount)
        {
            return PoRepo.UpdatePurchaseOrderById(PoId, objPageSortBo, ref totalCount);
        }
        public void getRepairSearchResult(ref DataSet resultDataSet, ref string searchText)
        {
            PoRepo.getRepairSearchResult(ref resultDataSet,ref searchText);
        }
        public int SaveWorkCompletionData(ref CompletePurchaseOrderBO objCompletePurchaseOrderBO)
        {
            return PoRepo.SaveWorkCompletionData(ref objCompletePurchaseOrderBO);
        }
        public void ChangeWorkStatus(int purchaseOrderId, bool isAccepted)
        {
            PoRepo.ChangeWorkStatus(purchaseOrderId, isAccepted);
        }
        public void SaveNoEntryData(int faultLogId, DateTime recordedDateTime)
        {
            PoRepo.SaveNoEntryData(faultLogId, recordedDateTime);
        }
        public void SaveCancelledData(int faultLogId,int orderId)
        {
            PoRepo.SaveCancelledData(faultLogId, orderId);
        }

        public string GetSchedulerEmailId(int orderId)
        {
            return PoRepo.GetSchedulerEmailId(orderId);
        }

        public string GetPoStatusForSessionCreation(int orderId)
        {
            return PoRepo.GetPoStatusForSessionCreation(orderId);
        }
        

    }
}
