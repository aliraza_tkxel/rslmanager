﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PDR_DataAccess.Reports;
using System.Data;
using PDR_Utilities.Helpers;
using PDR_BusinessObject.Reports;
using System.Reflection;

namespace PDR_BusinessLogic.Reports.ReportsRdlcBL
{
    public class ReportsRdlcBL
    {
        //IReportsRepo objReportsRepo;
        //public ReportsRdlcBL(IReportsRepo rRepo)
        //{
        //    objReportsRepo = rRepo;
        //}
        //public ReportsRdlcBL()
        //{

        //}
        public List<C__RentLetter_MultiPrint_BO> getRentLetterMultiPrint(string tenancyIds, int printAction)
        {
            ReportsRepo objReportsRepo = new ReportsRepo();
            DataSet resultDataSet = new DataSet();
            objReportsRepo.getRentLetterMultiPrint(ref resultDataSet, tenancyIds, printAction);
            List<C__RentLetter_MultiPrint_BO> result = new List<C__RentLetter_MultiPrint_BO>();
            foreach (DataRow row in resultDataSet.Tables[0].Rows)
            {
                C__RentLetter_MultiPrint_BO itemList = new C__RentLetter_MultiPrint_BO();
                itemList.ADDITIONAL_GARAGE = row.Field<decimal?>("ADDITIONAL_GARAGE").GetValueOrDefault();

                itemList.ADDRESS1 = row["ADDRESS1"].ToString();
                if (row["TOWNCITY"] != null && row["ADDRESS1"] != null && row["TOWNCITY"].ToString() !="")
                {
                    itemList.ADDRESS1 = itemList.ADDRESS1 + "<br/>" + row["TOWNCITY"].ToString();
                }

                if (row["COUNTY"] != null && row["ADDRESS1"] != null && row["COUNTY"].ToString() != "")
                {
                    itemList.ADDRESS1 = itemList.ADDRESS1 + "<br/>" + row["COUNTY"].ToString();
                }
                if (row["POSTCODE"] != null && row["ADDRESS1"] != null && row["POSTCODE"].ToString() != "")
                {
                    itemList.ADDRESS1 = itemList.ADDRESS1 + "<br/>" + row["POSTCODE"].ToString();
                }
                //itemList.COUNTY = row["COUNTY"].ToString();
                //itemList.TOWNCITY = row["TOWNCITY"].ToString();
                //itemList.POSTCODE = row["POSTCODE"].ToString();

                itemList.CAPITALVALUE = row.Field<decimal?>("CAPITALVALUE").GetValueOrDefault();
                itemList.COUNCILTAX = row.Field<decimal?>("COUNCILTAX").GetValueOrDefault();

                itemList.CUSTOMERID = row.Field<int?>("CUSTOMERID").GetValueOrDefault();
                itemList.DATERENTSET = row.Field<DateTime?>("DATERENTSET").GetValueOrDefault();
                itemList.FIRSTNAME = row["FIRSTNAME"].ToString();
                itemList.GARAGE = row.Field<decimal?>("GARAGE").GetValueOrDefault();
                itemList.GARAGE_COUNT = row.Field<int?>("GARAGE_COUNT").GetValueOrDefault();
                itemList.HOUSENUMBER = row["HOUSENUMBER"].ToString();
                itemList.incomeOfficer = row["incomeOfficer"].ToString();
                itemList.INELIGSERV = row.Field<decimal?>("INELIGSERV").GetValueOrDefault();
                itemList.INSURANCEVALUE = row.Field<decimal?>("INSURANCEVALUE").GetValueOrDefault();
                itemList.LASTNAME = row["LASTNAME"].ToString();
                itemList.NEWGARAGECHARGE = row.Field<decimal?>("NEWGARAGECHARGE").GetValueOrDefault();
                itemList.OLDGARAGECHARGE = row.Field<decimal?>("OLDGARAGECHARGE").GetValueOrDefault();
                itemList.OLDTOTAL = row.Field<decimal?>("OLDTOTAL").GetValueOrDefault();
                itemList.P_FULLADDRESS = row["P_FULLADDRESS"].ToString();

                itemList.PROPERTYID = row["PROPERTYID"].ToString();
                itemList.REASSESMENTDATE = row.Field<DateTime?>("REASSESMENTDATE").GetValueOrDefault();
                itemList.RENT = row.Field<decimal?>("RENT").GetValueOrDefault();
                itemList.RENTEFFECTIVE = row.Field<DateTime?>("RENTEFFECTIVE").GetValueOrDefault();
                itemList.RENTTYPE = row.Field<int?>("RENTTYPE").GetValueOrDefault();
                itemList.SERVICES = row.Field<decimal?>("SERVICES").GetValueOrDefault();
                itemList.SETRENTTO = row.Field<int?>("SETRENTTO").GetValueOrDefault();
                itemList.SUPPORTEDSERVICES = row.Field<decimal?>("SUPPORTEDSERVICES").GetValueOrDefault();
                itemList.T_COUNCILTAX = row.Field<decimal?>("T_COUNCILTAX").GetValueOrDefault();
                itemList.T_INELIGSERV = row.Field<decimal?>("T_INELIGSERV").GetValueOrDefault();
                itemList.T_RENT = row.Field<decimal?>("T_RENT").GetValueOrDefault();
                itemList.T_SERVICES = row.Field<decimal?>("T_SERVICES").GetValueOrDefault();
                itemList.T_SUPPORTEDSERVICES = row.Field<decimal?>("T_SUPPORTEDSERVICES").GetValueOrDefault();
                itemList.T_WATERRATES = row.Field<decimal?>("T_WATERRATES").GetValueOrDefault();
                itemList.TARGETRENT = row.Field<decimal?>("TARGETRENT").GetValueOrDefault();
                itemList.TARGETRENTID = row.Field<int?>("TARGETRENTID").GetValueOrDefault();
                //TARGETRENTSET = row.Field<int?>("TARGETRENTSET").GetValueOrDefault();
                itemList.TENANCYID = row.Field<int?>("TENANCYID").GetValueOrDefault();
                itemList.TITLE = row["TITLE"].ToString();
                itemList.TOTALRENT = row.Field<decimal?>("TOTALRENT").GetValueOrDefault();

                itemList.WATERRATES = row.Field<decimal?>("WATERRATES").GetValueOrDefault();
                itemList.WORKEMAIL = row["WORKEMAIL"].ToString();
                itemList.WORKMOBILE = row["WORKMOBILE"].ToString();
                itemList.YEARNUMBER = row.Field<int?>("YEARNUMBER").GetValueOrDefault();
                itemList.YIELD = row.Field<decimal?>("YIELD").GetValueOrDefault();
                result.Add(itemList);
            }


            string tenant = "";
            string DearTenantName = "";
            foreach (C__RentLetter_MultiPrint_BO item in result)
            {
                tenant = item.TITLE + " " + item.FIRSTNAME + " " + item.LASTNAME;
                DearTenantName = item.TITLE + " " + item.LASTNAME;
                item.TenantName = tenant;
                item.DearTenantName = DearTenantName;
                decimal? GarageSpace = item.OLDGARAGECHARGE;
                decimal? T_GarageSpace = item.ADDITIONAL_GARAGE;
                item.TOTALRENT = item.ADDITIONAL_GARAGE + item.TOTALRENT;
                if (GarageSpace == 0)
                    GarageSpace = GarageSpace + item.ADDITIONAL_GARAGE;


                item.t_totalrent = item.T_RENT + item.T_SUPPORTEDSERVICES + item.T_INELIGSERV + item.T_SERVICES + item.T_COUNCILTAX + item.T_WATERRATES + item.NEWGARAGECHARGE;
            }
            if (result.Count() > 1)
            {
                result[0].TenantName = result[0].TenantName + " & " + tenant;
                result[0].DearTenantName = result[0].DearTenantName + " & " + DearTenantName;
                result.RemoveAt(1);
            }
            return result;
        }
        public List<TenancyIds> populateRentLetterReport(string tenancyIds)
        {

            List<TenancyIds> TenIds = tenancyIds.Split(',').Select(x => new TenancyIds { tenancyId = x }).ToList();
            return TenIds;
        }
        //public DataSet getRentLetterMultiPrintDs(string tenancyIds)
        //{
        //    ReportsRepo objReportsRepo = new ReportsRepo();
        //    DataSet resultDataSet = new DataSet();
        //    objReportsRepo.getRentLetterMultiPrint(ref resultDataSet, tenancyIds);
        //    return resultDataSet;
        //}
    }
    public class TenancyIds
    {
        public string tenancyId { get; set; }
    }



}
