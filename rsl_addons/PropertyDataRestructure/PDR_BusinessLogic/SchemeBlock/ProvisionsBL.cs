﻿using System.Data;
using PDR_BusinessObject.PageSort;
using PDR_BusinessObject.SchemeBlock;
using PDR_DataAccess.SchemeBlock;

namespace PDR_BusinessLogic.SchemeBlock
{
    public class ProvisionsBL : Base.BaseBL
    {
        IProvisionsRepo objProvisionsRepo;

        public int GetProvisioningReport(ref DataSet resultDataSet, PageSortBO objPageSortBO, string filterText, int categoryId)
        {
            return objProvisionsRepo.GetProvisioningReport(ref resultDataSet, objPageSortBO, filterText, categoryId);
        }

        public ProvisionsBL(ProvisionsRepo provisionsRepo)
        {
            objProvisionsRepo = provisionsRepo;
        }
        public DataSet GetProvisionsData(int? schemeId, int? blockId)
        {
            return objProvisionsRepo.GetProvisionsData(schemeId, blockId);
        }
        public DataSet GetProvisionsDataById(int provisionId)
        {
            return objProvisionsRepo.GetProvisionsDataById(provisionId);
        }
        public DataSet GetProvisionItemHistory(int provisionId, int? schemeId, int? blockId)
        {
            return objProvisionsRepo.GetProvisionItemHistory(provisionId, schemeId, blockId);
        }
        public DataSet LoadCycleTypeDDL()
        {
            return objProvisionsRepo.LoadCycleTypeDDL();
        }
        public string SaveProvsionItemDetail(ProvisionsBO objProvisionItemBO, DataTable MSATDetailDt)
        {
            return objProvisionsRepo.SaveProvsionItemDetail(objProvisionItemBO, MSATDetailDt);
        }
        public string UpdateProvsionItemDetail(ProvisionsBO objProvisionItemBO, DataTable MSATDetailDt)
        {
            return objProvisionsRepo.UpdateProvsionItemDetail(objProvisionItemBO, MSATDetailDt);
        }
        public DataSet getProvisionNotes(int? schemeId, int? blockId, int provisionId)
        {
            return objProvisionsRepo.GetProvisionNotes(schemeId, blockId, provisionId);
        }
        public void SaveProvisionNotes(ProvisionNotesBO objProvisionNotesBO)
        {
            objProvisionsRepo.SaveProvisionNotes(objProvisionNotesBO);
        }
        public DataSet GetProvisionImages(int provisionId, int? schemeId, int? blockId)
        {
            return objProvisionsRepo.GetProvisionImages(provisionId, schemeId, blockId);
        }
        public void SaveProvisionImages(ProvisionPhotographsBO objProvisionPhotoBO)
        {
            objProvisionsRepo.SaveProvisionImages(objProvisionPhotoBO);
        }
        public DataSet GetProvisionCategories()
        {
            return objProvisionsRepo.GetProvisionCategories();
        }
        
        public void SaveProvisionChargeProperties(ProvisionChargeProperties serProperties, DataTable excludedIncludedProperties, int itemId)
        {
            objProvisionsRepo.SaveProvisionChargeProperties(serProperties, excludedIncludedProperties, itemId);
        }

        public DataSet getSavedProvisionChargeProperties(int schemeId, int blockId, int itemId)
        {
            return objProvisionsRepo.getSavedProvisionChargeProperties(schemeId, blockId, itemId);
        }

        #region "get excluded Properties va scheme and block"
        public DataSet getExPropertiesBySchemeBlock(int scheme, int block, int itemId)
        {
            return objProvisionsRepo.getExPropertiesBySchemeBlock(scheme, block, itemId);
        }
        #endregion

        #region "get provisions by category for scheme and block"
        public DataSet GetProvisionsByCategories(int scheme, int block, int itemId)
        {
            return objProvisionsRepo.GetProvisionsByCategories(scheme, block, itemId);
        }
        #endregion

        public DataSet GetProvisionByProvisionId(int provisionId)
        {
            return objProvisionsRepo.GetProvisionByProvisionId(provisionId);
        }
    }
}
