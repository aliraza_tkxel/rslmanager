﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PDR_BusinessLogic.Base;
using System.Data;
using PDR_DataAccess;
using PDR_BusinessObject;
using PDR_DataAccess.Scheme;
using PDR_BusinessObject.PageSort;
using PDR_DataAccess.SchemeBlock;
using PDR_BusinessObject.SchemeBlock;

namespace PDR_BusinessLogic.SchemeBlock
{
    public class DocumentsBL : Base.BaseBL
    {
        IDocumentsRpo objDocumentsRpo;
        public DocumentsBL(DocumentsRepo dRepo)
        {
            objDocumentsRpo = dRepo;
        }
        #region "get Document Types"
        

        public DataSet getDocumentTypesList(string reportFor = "", int categoryid = 0)
        {

            return objDocumentsRpo.getDocumentTypes(true, reportFor, categoryid);


        }

        #endregion



        #region get EPC Category types


        public DataSet getEpcCategoryTypesList(ref DataSet resultDataSet)
        {
            return objDocumentsRpo.getEpcCategoryTypesList(ref resultDataSet);
        }

        #endregion

      

        #region get Document Subtypes


        public DataSet getDocumentSubtypesList(int DocumentTypeId, string reportFor="")
        {
            return objDocumentsRpo.getDocumentSubtypes(DocumentTypeId, true, reportFor);
        }

        #endregion

        #region "Save Uploaded Document"


        public void saveDocumentUpload(DocumentsBO objDocumentBO)
        {
            objDocumentsRpo.saveDocumentUpload(objDocumentBO);
        }

        #endregion

        #region "Get Appliance Locations"
        public int getPropertyDocuments(ref DataSet resultDataSet, int? schemeId, int? blockId, ref PageSortBO objPageSortBo)
        {


            return objDocumentsRpo.getPropertyDocuments(ref resultDataSet, schemeId, blockId, ref objPageSortBo);


        }
        #endregion

        public string deleteDocumentByIDandGetPath(int docId)
        {
            return objDocumentsRpo.deleteDocumentByIDandGetPath(docId);
        }

        #region "get Document Subtypes"


        public DataSet getDocumentInformationById(int documentId)
        {

            return objDocumentsRpo.getDocumentInformationById(documentId);


        }

        #endregion

        public void getDocumentToDownload(ref int documentId, ref string type, ref string documentName, ref string documentPath, ref string documentExt, ref string typeId)
        {
            objDocumentsRpo.getDocumentToDownload(ref documentId, ref type, ref documentName, ref documentPath, ref documentExt, ref typeId);
        }


        #region "Get CP12Document By LGSRID"

        public void getCP12DocumentByLGSRID(ref DataSet dataSet, ref int LGSRID)
        {
            objDocumentsRpo.getCP12DocumentByLGSRID(ref dataSet, ref LGSRID);
        }

        #endregion
    }
}
