﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PDR_DataAccess.SchemeBlock;
using System.Data;
using PDR_BusinessObject.SchemeBlock;
using PDR_BusinessObject.PageSort;

namespace PDR_BusinessLogic.SchemeBlock
{
    public class AttributesBL : Base.BaseBL
    {
        #region Constructor
        IAttributesRepo objAttrRpo;
        public AttributesBL(AttributesRepo AttrRepo)
        {
            objAttrRpo = AttrRepo;
        }
        #endregion
        #region get locations
        public DataSet getLocations()
        {

            return objAttrRpo.getLocations();
        }
        #endregion
        #region "get Areas by Location Id"
        public DataSet getAreasByLocationId(int locationId)
        {
            return objAttrRpo.getAreasByLocationId(locationId);
        }
        #endregion

        #region "get Items by Area Id"
        public DataSet getItemsByAreaId(int areaId)
        {
            return objAttrRpo.getItemsByAreaId(areaId);
        }
        #endregion
        #region Get Boilers List
        public DataSet getBoilerslist(int? schemeId, int? blockId)
        {
            return objAttrRpo.getBoilerslist(schemeId, blockId);
        }
        #endregion

        #region Get Water Meter List
        public DataSet getWaterMeterlist(int? schemeId, int? blockId, int?itemId)
        {
            return objAttrRpo.getMultiAttributelistByItemId(schemeId, blockId, itemId);
        }
        #endregion

        #region Get Passenger Lift List
        public DataSet getPassengerLiftlist(int? schemeId, int? blockId, int? itemId)
        {
            return objAttrRpo.getMultiAttributelistByItemId(schemeId, blockId, itemId);
        }
        #endregion

        #region Get Multi Attribute List By ItemId
        public DataSet getMultiAttributelistByItemId(int? schemeId, int? blockId, int? itemId)
        {
            return objAttrRpo.getMultiAttributelistByItemId(schemeId, blockId, itemId);
        }
        #endregion
        /////////////////////////////////////////////

        #region "get Items by Area Id"
        public DataSet getSubItemsByItemId(int itemId)
        {
            return objAttrRpo.getSubItemsByItemId(itemId);
        }
        #endregion

        #region "get Items by Item Id"
        public DataSet getItemsByItemId(int itemId)
        {
            return objAttrRpo.getItemsByItemId(itemId);
        }
        #endregion

        #region "Get Item Notes"
        public DataSet getItemNotes(int? schemeId, int? blockId, int itemId,int? heatingMappingId)
        {
            return objAttrRpo.getItemNotes(schemeId, blockId, itemId, heatingMappingId);
        }
        #endregion

        #region "save Item Detail"
        public void SaveItemNotes(AttributeNotesBO objItemNotesBo)
        {
            objAttrRpo.saveItemNotes(objItemNotesBo);
        }
        #endregion

        #region "Update Item Detail"
        public void UpdateItemNotes(AttributeNotesBO objItemNotesBo)
        {
            objAttrRpo.UpdateItemNotes(objItemNotesBo);
        }
        #endregion

        #region "Delete Item Detail"
        public void DeleteItemNotes(int itemNotesId)
        {
            objAttrRpo.DeleteItemNotes(itemNotesId);
        }
        #endregion

        #region "Get Item Notes Trail"
        public void GetItemNotesTrail(ref DataSet resultDataSet, int itemNotesId)
        {
            objAttrRpo.GetItemNotesTrail(ref resultDataSet, itemNotesId);
        }
        #endregion

        #region "Save Uploaded Document"


        public void saveDocumentUpload(AttributePhotographsBO objAttrPhotoBO)
        {


            objAttrRpo.saveDocumentUpload(objAttrPhotoBO);

        }

        #endregion

        #region get Property Images
        public DataSet getPropertyImages(int itemId, int? schemeId, int? blockId, int? heatingMappingId)
        {
            return objAttrRpo.getPropertyImages(itemId, schemeId, blockId,heatingMappingId );
        }
        #endregion
        #region "get Item Detail"
        public DataSet getItemDetail(int itemId, int? schemeId, int? blockId, int? childAttributeMappingId = null)
        {
            return objAttrRpo.getItemDetail(itemId, schemeId, blockId,childAttributeMappingId);
        }
        #endregion
        #region " load Cycle DDL"
        public DataSet loadCycleDdl()
        {

            return objAttrRpo.loadCycleDdl();
        }
        #endregion

        #region amend Attribute Detail
        public int amendAttributeDetail(ItemAttributeBO objItemAttributeBo, DataTable itemDetailDt, DataTable itemDatesDt, DataTable conditionRatingDt, DataTable MSATDetailDt, CyclicalServiceBO cyclicalService)
        {
            return objAttrRpo.amendAttributeDetail(objItemAttributeBo, itemDetailDt, itemDatesDt, conditionRatingDt, MSATDetailDt, cyclicalService);
        }
        #endregion

        #region amend Attribute Detail
        public void saveServiceChargeProperties(ServiceChargeProperties serProperties, DataTable excludedIncludedProperties, int itemId, int attributeChildId)
        {
            objAttrRpo.saveServiceChargeProperties(serProperties, excludedIncludedProperties, itemId, attributeChildId);
        }
        #endregion

        #region get Appliances List for grid
        public int getAppliancesList(ref DataSet resultDataSet, ref PageSortBO objPageSortBo, int? schemeId, int itemId, int? blockId)
        {
            return objAttrRpo.getAppliancesList(ref resultDataSet, ref objPageSortBo, schemeId, itemId, blockId);
        }
        #endregion

        #region "get Appliance Locations"
        public DataSet getApplianceLocations(string prefix)
        {
            return objAttrRpo.getApplianceLocations(prefix);
        }
        #endregion

        #region "get excluded Properties va scheme and block"
        public DataSet getExPropertiesBySchemeBlock(int scheme, int block, int itemId, int childItemId)
        {
            return objAttrRpo.getExPropertiesBySchemeBlock(scheme, block, itemId, childItemId);
        }
        #endregion

        #region "get General Journal excluded Properties "
        public DataSet getGeneralJournalExcludedProperties(int txnId)
        {
            return objAttrRpo.getGeneralJournalExcludedProperties(txnId);
        }
        #endregion
      

        #region "get Applliance Type"
        public DataSet getApplianceType(string prefix)
        {
            return objAttrRpo.getApplianceType(prefix);
        }
        #endregion

        #region "get Make"
        public DataSet getMake(string prefix)
        {
            return objAttrRpo.getMake(prefix);
        }
        #endregion

        #region "Get Models List"

        public DataSet getModelsList(string prefix)
        {
            return objAttrRpo.getModelsList(prefix);
        }

        #endregion

        #region "Save  Appliance "

        public int saveAppliance(ApplianceBO objApplianceBo)
        {
            return objAttrRpo.saveAppliance(objApplianceBo);
        }
        #endregion

        #region ::ST - Service Charge Report
        public int GetServiceChargeReport(ref DataSet resultDataSet, PageSortBO objPageSortBO, int schemeId, int blockId,int itemId, int fiscalYear, bool fetchAllRecords = false)
        {
            return objAttrRpo.GetServiceChargeReport(ref resultDataSet, objPageSortBO, schemeId, blockId,itemId, fiscalYear, fetchAllRecords);
        }

        public int GetServiceChargeReportPO(ref DataSet resultDataSet, PageSortBO objPageSortBO, int schemeId, int blockId, int fiscalYear)
        {
            return objAttrRpo.GetServiceChargeReportPO(ref resultDataSet, objPageSortBO, schemeId, blockId, fiscalYear);
        }

        public int GetServiceChargeReportDC(ref DataSet resultDataSet, PageSortBO objPageSortBO, string filterText, int fiscalYear)
        {
            return objAttrRpo.GetServiceChargeReportDC(ref resultDataSet, objPageSortBO, filterText, fiscalYear);
        }

        #endregion

        public DataSet  getDetectorByPropertyId(int  schemeId,int blockId, string detectorType)
        {

            return objAttrRpo.getDetectorByPropertyId(schemeId,blockId, detectorType);

        }
        public  void getSchemeBlockAppliances(string Id, ref DataSet resultDataSet)
        {
            objAttrRpo.getSchemeBlockAppliances(Id, ref resultDataSet);
        }

        public void getDefectManagementLookupValues(string Id, string requestType, ref DataSet resultDataSet)
        {
            objAttrRpo.getDefectManagementLookupValues(Id, requestType, ref resultDataSet);
        }

        #region save Defect
        public void saveDefect(ApplianceDefectBO objDefectBO)
        {
            objAttrRpo.saveDefect(objDefectBO);
        }
        #endregion

        public bool saveDetectors(DetectorsBO objDetectorBo, string smokeDetectorType, string noOfSmokeDetectors, int InstalledBy)
        {
            return objAttrRpo.saveDetectors(objDetectorBo, smokeDetectorType, noOfSmokeDetectors, InstalledBy);
        }

        public DataSet getCyclingServicesContractor(int schemeId, int blockId, string msatType)
        {

            return objAttrRpo.getCyclingServicesContractor(schemeId, blockId, msatType);

        }

        public DataSet getBlocksByScheme(int schemeId)
        {

            return objAttrRpo.getBlocksByScheme(schemeId);

        }
        public DataSet getPropertiesBySchemeBlock(int schemeId, int blockId)
        {

            return objAttrRpo.getPropertiesBySchemeBlock(schemeId, blockId);

        }

        public DataSet getSelectedProperties(int schemeId, int blockId, int itemId)
        {

            return objAttrRpo.getSelectedProperties(schemeId, blockId, itemId);

        }
        public DataSet getPurchaseItemExcludedProperties(int itemId)
        {
            return objAttrRpo.getPurchaseItemExcludedProperties(itemId);
        }

        public DataSet getFiscalYears()
        {

            return objAttrRpo.getFiscalYears();


        }

        public void getSchemeBlockDefectDetails(int propertyDefectId, ref ApplianceDefectBO objApplianceDefectBO)
        {
            objAttrRpo.getSchemeBlockDefectDetails(propertyDefectId, ref objApplianceDefectBO);
        }

        #region load Scheme/Block Appliances DDL
        public void loadSchemeBlockAppliancesDDL(string Id, string requestType, ref DataSet resultDataSet)
        {
            objAttrRpo.loadSchemeBlockAppliancesDDL(Id, requestType, ref resultDataSet);
        }
        #endregion

        #region  load Category DDL
        public void loadCategoryDDL(ref DataSet resultDataSet)
        {
            objAttrRpo.loadCategoryDDL(ref resultDataSet);
        }
        #endregion

        #region Get Scheme/Block Images
        public void getSchemeBlockDefectImages(string Id, string requestType, ref DataSet photosDataSet)
        {
            objAttrRpo.getSchemeBlockDefectImages(Id, requestType, ref photosDataSet);
        }
        #endregion

        #region Get Scheme/Block Images
        /// <summary>
        /// Get Scheme/Block Images
        /// </summary>
        /// <remarks></remarks>

        public void getSchemeBlockImages(ref DataSet resultDataSet, string Id, string requestType, int itemId)
        {
            objAttrRpo.getSchemeBlockImages(ref resultDataSet, Id, requestType, itemId);
        }

        #endregion

        #region Save Photographs
        public void savePhotograph(PhotographBO objPhotographBO)
        {
            objAttrRpo.savePhotograph(objPhotographBO);
        }
        #endregion
    
    }
}
