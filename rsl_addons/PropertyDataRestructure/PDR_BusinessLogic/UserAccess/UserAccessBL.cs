﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using PDR_DataAccess.UserAccess;
using PDR_Utilities.Managers;
using PDR_Utilities.Constants;
using System.Collections.Specialized;
using System.Web;

namespace PDR_BusinessLogic.UserAccessBL
{
    public class UserAccessBL : Base.BaseBL
    {
        IUserAccessRepo objUserAccessRepo;
        public UserAccessBL(IUserAccessRepo usrRepo)
        {
            objUserAccessRepo = usrRepo;
        }

        #region "Get Property Page List"
        /// <summary>
        /// Get Property Page List
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="employeeId"></param>
        /// <param name="selectedMenu"></param>
        /// <remarks></remarks>
        public void getPropertyPageList(ref DataSet resultDataSet, int employeeId, string selectedMenu)
        {
            objUserAccessRepo.getPropertyPageList(ref resultDataSet, employeeId, selectedMenu);
        }
        #endregion

        #region "Check Page Access"
        /// <summary>
        /// Check Page Access
        /// </summary>
        /// <remarks></remarks>
        public bool checkPageAccess(string menu)
        {

            int userId = session.EmployeeId;
            bool isAccessGranted = false;
            int pageId = 0;

            DataSet resultDataset = new DataSet();
            getPropertyPageList(ref resultDataset, userId, menu);

            DataTable accessGrantedModulesDt = resultDataset.Tables[ApplicationConstants.AccessGrantedModulesDt];
            DataTable accessGrantedMenusDt = resultDataset.Tables[ApplicationConstants.AccessGrantedMenusDt];
            DataTable accessGrantedPagesDt = resultDataset.Tables[ApplicationConstants.AccessGrantedPagesDt];
            DataTable randomPageDt = resultDataset.Tables[ApplicationConstants.RandomPageDt];

            if ((checkPageExist(accessGrantedPagesDt, ref pageId)))
            {
                isAccessGranted = checkPageHierarchyAccessRights(accessGrantedModulesDt, accessGrantedMenusDt, accessGrantedPagesDt, pageId);
            }
            else if ((checkPageExist(randomPageDt, ref pageId)))
            {
                randomPageDt.Merge(accessGrantedPagesDt);
                isAccessGranted = checkPageHierarchyAccessRights(accessGrantedModulesDt, accessGrantedMenusDt, randomPageDt, pageId);
            }

            return isAccessGranted;

        }

        #endregion

        #region "Check Page Exist"
        /// <summary>
        /// Check Page Exist
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public bool checkPageExist(DataTable pagesDt, ref int pageId)
        {

            bool pageFound = false;
            dynamic pageFileName = HttpContext.Current.Request.Url.AbsolutePath.Substring(HttpContext.Current.Request.Url.AbsolutePath.LastIndexOf("/") + 1);
            NameValueCollection pageQueryString = HttpUtility.ParseQueryString(HttpContext.Current.Request.QueryString.ToString());

            var query = (from dataRow in pagesDt.AsEnumerable()
                         where
                             dataRow.Field<string>(ApplicationConstants.GrantPageFileNameCol).ToLower() == pageFileName.ToLower()
                         orderby dataRow.Field<int>(ApplicationConstants.GrantPageAccessLevelCol) descending
                         select dataRow);
            DataTable resultPageDt = pagesDt.Clone();


            if (query.Count() > 0)
            {
                resultPageDt = query.CopyToDataTable();

                foreach (DataRow row in resultPageDt.Rows)
                {
                    string fileName = row[ApplicationConstants.GrantPageFileNameCol].ToString();
                    string url = row[ApplicationConstants.GrantPageQueryStringCol].ToString();
                    NameValueCollection queryString = HttpUtility.ParseQueryString(url);

                    if (queryString.Count == 0 & pageQueryString.Count == 0)
                    {
                        pageFound = true;
                        pageId = Convert.ToInt32(row[ApplicationConstants.GrantPageIdCol]);
                        break; // TODO: might not be correct. Was : Exit For
                    }
                    else
                    {
                        int matchCount = 0;
                        foreach (string key in queryString.AllKeys)
                        {
                            if (queryString[key].Equals(pageQueryString[key]))
                            {
                                matchCount = matchCount + 1;
                            }
                        }

                        if (queryString.Count == matchCount)
                        {
                            pageId = Convert.ToInt32(row[ApplicationConstants.GrantPageIdCol]);
                            pageFound = true;
                            break; // TODO: might not be correct. Was : Exit For
                        }

                    }

                }

            }

            return pageFound;

        }

        #endregion

        #region "Check Page Hierarchy Access Rights"
        /// <summary>
        /// Check Page Hierarchy Access Rights
        /// First check in pages levels e.g (level 3 , level 2 , level 1)
        /// Second check in menu
        /// Third check in modules
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public bool checkPageHierarchyAccessRights(DataTable modulesDt, DataTable menusDt, DataTable pagesDt, int pageId)
        {

            bool isAccessGranted = false;


            if ((checkInPageLevelsAccessRights(pagesDt, pageId)))
            {
                string pageExpression = ApplicationConstants.GrantPageIdCol + " = " + Convert.ToString(pageId);
                DataRow[] pageRows = pagesDt.Select(pageExpression);
                int linkedMenuId = Convert.ToInt32(pageRows[0][ApplicationConstants.GrantPageMenuIdCol]);
                int linkedModuleId = Convert.ToInt32(pageRows[0][ApplicationConstants.GrantPageModuleIdCol]);

                string menuExpression = ApplicationConstants.GrantMenuMenuIdCol + " = " + Convert.ToString(linkedMenuId);
                DataRow[] menuRows = menusDt.Select(menuExpression);


                if ((menuRows.Count() > 0))
                {
                    string moduleExpression = ApplicationConstants.GrantModulesModuleIdCol + " = " + Convert.ToString(linkedModuleId);
                    DataRow[] moduleRows = modulesDt.Select(moduleExpression);

                    if ((moduleRows.Count() > 0))
                    {
                        isAccessGranted = true;
                    }

                }

            }

            return isAccessGranted;
        }

        #endregion

        #region "Check In Page Levels Access Rights"
        /// <summary>
        /// Check In Page Levels Access Rights from bottom to top
        /// </summary>
        /// <param name="pagesDt"></param>
        /// <param name="pageId"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public bool checkInPageLevelsAccessRights(DataTable pagesDt, int pageId)
        {

            int parentPageId = 0;
            string expression = ApplicationConstants.GrantPageIdCol + " = " + Convert.ToString(pageId);
            DataRow[] rows = pagesDt.Select(expression);

            if (rows[0][ApplicationConstants.GrantPageParentPageCol] == System.DBNull.Value | (rows[0][ApplicationConstants.GrantPageParentPageCol] == null))
            {
                return true;
            }
            else
            {
                parentPageId = Convert.ToInt32(rows[0][ApplicationConstants.GrantPageParentPageCol]);
                string parentExpression = ApplicationConstants.GrantPageIdCol + " = " + Convert.ToString(parentPageId);
                DataRow[] parentRow = pagesDt.Select(parentExpression);

                if ((parentRow.Count() > 0))
                {
                    return checkInPageLevelsAccessRights(pagesDt, parentPageId);
                }
                else
                {
                    return false;
                }

            }

        }

        #endregion

        #region "Get Employee By Id"
        /// <summary>
        /// Get Employee detail By Id
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public DataSet getEmployeeById(Int32 employeeId)
        {
            return objUserAccessRepo.getEmployeeById(employeeId);
        }
        #endregion

    }
}
