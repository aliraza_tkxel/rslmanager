﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PDR_BusinessLogic.Base;
using System.Data;
using PDR_DataAccess;
using PDR_BusinessObject;
using PDR_DataAccess.Block;
using PDR_BusinessObject.PageSort;
using PDR_BusinessObject.Block;
using PDR_BusinessObject.Warranties;
using PDR_BusinessObject.Asbestos;
using PDR_BusinessObject.Restriction;

namespace PDR_BusinessLogic.Block
{
    public class BlockBL : BaseBL
    {
        IBlockRepo objBlockRepo;
        public BlockBL(IBlockRepo bRepo)
        {
            objBlockRepo = bRepo;
        }

        public BlockBL()
        {
            // TODO: Complete member initialization
        }

        public Int32 getBlockList(ref DataSet resultDataSet, PageSortBO objPageSortBO, String searchText)
        {
            
            return objBlockRepo.getBlockList(ref resultDataSet, objPageSortBO, searchText);

        }

        public void getBlockTemplate(ref DataSet TemplateDataSet)
        {
            
            objBlockRepo.getBlockTemplate(ref TemplateDataSet);
        }

        public void getOwnershipData(ref DataSet ownershipDataSet)
        {
            
            objBlockRepo.getOwnershipData(ref ownershipDataSet);
        }


        public void getBlockTemplateData(ref DataSet BlockTemplateDataSet, int templateId)
        {
            
            objBlockRepo.getBlockTemplateData(ref BlockTemplateDataSet, templateId);
        }


        public void getDevelopments(ref DataSet DevelopmentDataSet)
        {
            
            objBlockRepo.getDevelopments(ref DevelopmentDataSet);
        }

        public string SaveBlock(BlockBO objSaveBlockBO)
        {
            
            return objBlockRepo.SaveBlock(objSaveBlockBO);
        }

        public bool SaveRestriction(ref RestrictionBO objRestrictionBO)
        {

            return objBlockRepo.SaveRestriction(ref objRestrictionBO);
        }


        public void getBlockWarranties(ref DataSet warrantyDataSet, int Id, PageSortBO objPageSortBO, string requestType)
        {
            
            objBlockRepo.getBlockWarranties(ref warrantyDataSet, Id, objPageSortBO, requestType);
        }

        public void getNHBCWarranties(ref DataSet warrantyDataSet, int id, string requestType)
        {
            objBlockRepo.getNHBCWarranties(ref warrantyDataSet, id, requestType);
        }

        public void getContractors(ref DataSet contractorDataSet)
        {
            
            objBlockRepo.getContractors(ref contractorDataSet);
        }

        public void getforDropdownValue(ref DataSet forDataSet)
        {
            
            objBlockRepo.getforDropdownValue(ref forDataSet);
        }

       
        public void getcategoryDropdownValue(ref DataSet categoryDataSet)
        {
            objBlockRepo.getCategoryDropdownValue(ref categoryDataSet);
        }

        public void getAreaDropdownValue(ref DataSet areaDataSet, int CategoryId)
        {
            objBlockRepo.getAreaDropdownValue(ref areaDataSet, CategoryId);
        }

        public void getItemDropdownValue(ref DataSet itemDataSet, int ItemId)
        {
            objBlockRepo.getItemDropdownValue(ref itemDataSet, ItemId);
        }

        public void getWarrantyTypes(ref DataSet warrantyTypeDataSet)
        {
            
            objBlockRepo.getWarrantyTypes(ref warrantyTypeDataSet);
        }

        public void saveWarranty(WarrantyBO objWarrantyBO)
        {
            
            objBlockRepo.saveWarranty(objWarrantyBO);
        }

        public void getWarrantyData(ref DataSet warrantyDataSet, int warrantyId)
        {
            
            objBlockRepo.getWarrantyData(ref warrantyDataSet, warrantyId);
        }

        public void deleteWarranty(int warrantyId)
        {
            
            objBlockRepo.deleteWarranty(warrantyId);
        }

        public int getProperties(ref DataSet propertiesDataSet, int Id, PageSortBO objPageSortBO, string requestType)
        {
            
           return  objBlockRepo.getProperties(ref propertiesDataSet, Id, objPageSortBO, requestType);
        }


        public void getPropertiesByDevPhaseBlock(ref DataSet propertiesDataSet, int developmentId, int phaseId, int blockid)
        {
            objBlockRepo.getPropertiesByDevPhaseBlock(ref propertiesDataSet, developmentId, phaseId, blockid);
        }


        public void removeProperties(string PropertyRef, string requestType)
        {
            
            objBlockRepo.removeProperties(PropertyRef, requestType);
        }

        public void getAsbestosAndRisk(ref DataSet resultDataSet)
        {
            objBlockRepo.getAsbestosAndRisk(ref resultDataSet);
        }

        public DataSet getBlockListBySchemeId(int schemeId)
        {

          return   objBlockRepo.getBlockListBySchemeId(schemeId);
        }

        public int getHealthAndSafetyTabInformation(ref DataSet resultDataSet, int Id, PageSortBO objPageSortBo, string requestType)
        {
          return  objBlockRepo.getHealthAndSafetyTabInformation(ref resultDataSet, Id, objPageSortBo, requestType);
        }

        public void getBlockData(ref DataSet blockDataSet, int blockId)
        {
            objBlockRepo.getBlockData(ref blockDataSet, blockId);
        }

        public void getBlockRestrictions(ref DataSet blockDataSet, int blockId)
        {
            objBlockRepo.getBlockRestrictions(ref blockDataSet, blockId);
        }

        public void getLandRegistrationOptions(ref DataSet dataSet)
        {
            objBlockRepo.getLandRegistrationOptions (ref dataSet);
        }

        public void getSchemeBlockAsbestosAndRisk(ref DataSet ddlDataSet)
        {
           
            objBlockRepo.getSchemeBlockAsbestosAndRisk(ref ddlDataSet);
        }

        public void getSchemeBlockAsbestosRiskAndOtherInfo(ref DataSet resultDataSet, int asbestosId)
        {
            objBlockRepo.getSchemeBlockAsbestosRiskAndOtherInfo(ref resultDataSet, asbestosId);
        }

        public void getRefurbishmentInformation(ref DataSet resultDataSet, int Id, string requestType)
        {
            objBlockRepo.getRefurbishmentInformation(ref resultDataSet, Id, requestType);
        }

        public string saveRefurbishment(PDR_BusinessObject.SchemeBlock.RefurbishmentBO objRefurbishmentBO)
        {
            return objBlockRepo.saveRefurbishment(objRefurbishmentBO);
        }

        public void saveAsbestos(AsbestosBO objAsbestosBO)
        {
            objBlockRepo.saveAsbestos(objAsbestosBO);
        }

        public void amendAsbestos(AsbestosBO objAsbestosBO)
        {
            objBlockRepo.amendAsbestos(objAsbestosBO);
        }

        public void GetEmployeeEmailForAsbestosEmailNotifications(string propertyId, string requestType, ref DataSet employeeEmailDataSet)
        {
            objBlockRepo.GetEmployeeEmailForAsbestosEmailNotifications(propertyId, requestType, ref employeeEmailDataSet);
        }
    }
}
