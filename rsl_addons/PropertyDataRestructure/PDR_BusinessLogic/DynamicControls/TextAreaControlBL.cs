﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using PDR_Utilities.Constants;
using System.Data;
using PDR_Utilities.Managers;
using System.Web.UI;

namespace PDR_BusinessLogic.DynamicControls
{
    public class TextAreaControlBL
    {
        SessionManager sessionMgr = new SessionManager();
        #region "Functions"
        #region "Create controls"
        /// <summary>
        /// Create dynamic text box 
        /// </summary>
        /// <param name="parameterId"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public TableRow createControl(int parameterId)
        {
            //Dim loadParameterDDL As DataTable = New DataTable()
            TextBox txtArea = new TextBox();
            txtArea.ID = ApplicationConstants.txtArea + parameterId.ToString();
            txtArea.CssClass = ApplicationConstants.textBoxStyle;
            txtArea.TextMode = TextBoxMode.MultiLine;
            TableRow tr = new TableRow();
            DataSet parameterDataSet = sessionMgr.AttributeResultDataSet;
            var attributeResult = (from ps in parameterDataSet.Tables[ApplicationConstants.Parameters].AsEnumerable() where ps["ParameterID"].ToString() == parameterId.ToString() select ps).FirstOrDefault();
            if (attributeResult != null)
            {
                int ItemParamID = Convert.ToInt32(attributeResult["ItemParamID"]);
                Control ctrl = (TextBox)txtArea;
                if (sessionMgr.TreeItemName.Contains("Boiler"))
                {
                    populateBoilerControl(ItemParamID, ref ctrl, sessionMgr.HeatingMappingId);
                }
                else
                {
                    populateControl(ItemParamID, ref ctrl);
                }
                // populateControl(attributeResult["ItemParamID"], ref txtArea);
                Label lblTextBox = new Label();

                tr.Attributes.Add("ID", sessionMgr.WorksRequiredRowId);
                // SessionManager.setWorksRequiredRowId(tr.ID.ToString())
                // tr.Attributes.Add("runat", "server")
                TableCell td1 = new TableCell();
                td1.HorizontalAlign = HorizontalAlign.Right;
                td1.ID = ApplicationConstants.Td1 + parameterId.ToString();
                TableCell td2 = new TableCell();
                td2.ID = ApplicationConstants.Td2 + parameterId.ToString();
                lblTextBox.ID = ApplicationConstants.lblTextBox + parameterId.ToString();
                lblTextBox.Text = attributeResult["ParameterName"].ToString() + ": ";
                td1.Controls.Add(lblTextBox);
                td2.Controls.Add(txtArea);
                tr.Cells.Add(td1);
                tr.Cells.Add(td2);
                tr.Height = 30;
                if (sessionMgr.IsWorkSatisfactory)
                {
                    tr.Style.Add("display", "none");
                }
            }
            return tr;
        }
        #endregion
        
        #region " Populate Controls"
        public void populateControl(int itemParamId, ref Control control)
        {
            DataSet parameterDataSet = sessionMgr.AttributeResultDataSet;
            var preInsertedResult = (from ps in parameterDataSet.Tables[ApplicationConstants.PreInsertedValues].AsEnumerable() where ps["ItemParamID"].ToString() == itemParamId.ToString() select ps).FirstOrDefault();

            if (preInsertedResult != null)
            {
                TextBox txtParameter = (TextBox)control;
                txtParameter.Text = preInsertedResult["ParameterValue"].ToString();
            }

        }
        #endregion

        #region " Populate Boiler Controls"
        public void populateBoilerControl(int itemParamId, ref Control control, int heatingMappingId)
        {
            DataSet parameterDataSet = sessionMgr.AttributeResultDataSet;
            var preInsertedResult = (from ps in parameterDataSet.Tables[ApplicationConstants.PreInsertedValues].AsEnumerable() where ps["ItemParamID"].ToString() == itemParamId.ToString() && ps["HeatingMappingId"].ToString() == heatingMappingId.ToString() select ps).FirstOrDefault();

            if (preInsertedResult != null)
            {
                TextBox txtParameter = (TextBox)control;
                txtParameter.Text = preInsertedResult["ParameterValue"].ToString();
            }

        }
        #endregion

        #region " create Read Only Control"
        public TableRow createReadOnlyControl(int parameterId)
        {
            DataTable loadParameterDdl = new DataTable();
            //create drop down list dynamically  
            Label parameterLiteral = new Label();
            //set style of control
            parameterLiteral.ID = ApplicationConstants.ltrlReadOnly + parameterId.ToString();
            parameterLiteral.CssClass = ApplicationConstants.ddlStyle;
            //Load Parameter values datatable
            DataSet parameterDataSet = sessionMgr.AttributeResultDataSet;
            loadParameterDdl = parameterDataSet.Tables[ApplicationConstants.ParameterValues];
            TableRow tr = new TableRow();
            //  tr.ID = ApplicationConstants.Tr + valueId.ToString()
            var attributeResult = (from ps in parameterDataSet.Tables[ApplicationConstants.Parameters].AsEnumerable() where ps["ParameterID"].ToString() == parameterId.ToString() select ps).FirstOrDefault();

            if (attributeResult != null)
            {
                // parameterLiteral.Text = attributeResult["ParameterName"].ToString();
                int ItemParamID = Convert.ToInt32(attributeResult["ItemParamID"]);
                //populate pre inserted value for dropdown
                this.populateControl(ItemParamID, ref parameterLiteral);
                Label lblParameterItem = new Label();

                TableCell td1 = new TableCell();
                // td1.ID = ApplicationConstants.Td1 + valueId.ToString()
                td1.HorizontalAlign = HorizontalAlign.Left;

                lblParameterItem.ID = ApplicationConstants.ddlParameter + parameterId.ToString();
                TableCell td2 = new TableCell();
                //td2.ID = ApplicationConstants.Td2 + valueId.ToString()
                lblParameterItem.Text = attributeResult["ParameterName"].ToString() + ":";
                td2.Style.Add("padding-left", "20px");
                td1.Width = 150;
                td1.Controls.Add(lblParameterItem);
                td2.Controls.Add(parameterLiteral);
                tr.Cells.Add(td1);
                tr.Cells.Add(td2);
                tr.Height = 30;
            }


            return tr;
        }
        #endregion


        public void populateControl(int itemParamId, ref Label control)
        {
            DataSet parameterDataSet = sessionMgr.AttributeResultDataSet;

            var preInsertedResult = (from ps in parameterDataSet.Tables[ApplicationConstants.PreInsertedValues].AsEnumerable() where ps["ItemParamID"].ToString() == itemParamId.ToString() select ps).FirstOrDefault();
            if (preInsertedResult != null && preInsertedResult["PARAMETERVALUE"].ToString() != "Please Select")
            {
                control.Text = preInsertedResult["PARAMETERVALUE"].ToString();

            }
            else
            {
                control.Text = "-";


            }
        }
        #endregion

    }
}
