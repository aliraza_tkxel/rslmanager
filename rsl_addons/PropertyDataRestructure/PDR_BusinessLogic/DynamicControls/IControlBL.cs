﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Web.UI;

namespace PDR_BusinessLogic.DynamicControls
{
    public interface IControlBL
    {
        TableRow createControl(int parameterId);

        void populateControl(int itemParamId, ref Control   control);
        //void populateControl(int itemParamId, ref TextBox  control);
        //void populateControl(int itemParamId, ref RadioButtonList control);
        //void populateControl(int itemParamId, ref CheckBoxList  control);
        TableRow createReadOnlyControl(int parameterId);
        void populateControl(int itemParamId, ref Label control);
    }
}
