﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using PDR_Utilities.Constants;
using PDR_Utilities.Managers;

namespace PDR_BusinessLogic.DynamicControls
{
    public class DropDownControlBL : IControlBL
    {
        SessionManager sessionMgr = new SessionManager();
        #region "Create controls"
        public TableRow createControl(int parameterId)
        {
            DataTable loadParameterDdl = new DataTable();
            //create drop down list dynamically  
            DropDownList parameter = new DropDownList();
            //set style of control
            parameter.ID = ApplicationConstants.ddlParameter + parameterId.ToString();
            parameter.CssClass = ApplicationConstants.ddlStyle;
            parameter.Attributes.Add("ParameterID", parameterId.ToString());
            // Dim parameterId As Integer = dr.Item("ParameterID")
            //Load Parameter values data table
            DataSet parameterDataSet = sessionMgr.AttributeResultDataSet;
            loadParameterDdl = parameterDataSet.Tables[ApplicationConstants.ParameterValues];

            //convert Parameter values data table into data view            
            DataView loadParameterDv = new DataView();
            loadParameterDv = loadParameterDdl.AsDataView();
            var attributeResult = (from ps in parameterDataSet.Tables[ApplicationConstants.Parameters].AsEnumerable() where ps["ParameterID"].ToString() == parameterId.ToString() select ps).FirstOrDefault();

            // Dim lifeCycleResult = (From ps In parameterDataSet.Tables(ApplicationConstants.plannedComponent) Where ps.Item("PARAMETERID").ToString() = parameterId And ps.Item("VALUEID").ToString() IsNot Nothing Select ps)
            //if (SessionManager.getIsDdlChangeEvent == true) {
            //    parameter.AutoPostBack = true;
            //    parameter.SelectedIndexChanged += parameter_SelectedIndexChanged;
            //    SessionManager.setValueLifeCycle(true);

            //}
            //Filter the Parameter values which have the same ParameterID.
            loadParameterDv.RowFilter = "ParameterID =  " + parameterId.ToString();
            loadParameterDdl = loadParameterDv.ToTable();
            parameter.DataSource = loadParameterDdl;
            parameter.DataValueField = "ValueID";
            parameter.DataTextField = "ValueDetail";
            //parameter.SelectedIndex = 1
            parameter.DataBind();
            //parameter.Items.Insert(0, New ListItem("Please Select", "-1"))
            //if (SessionManager.getValueLifeCycle() == true) {
            //    SessionManager.setValueId(parameter.SelectedValue());
            //}
            int ItemParamID = Convert.ToInt32(attributeResult["ItemParamID"]);
            Control ctrl = (DropDownList)parameter;
            if (sessionMgr.TreeItemName.Contains("Boiler"))
            {
                populateBoilerControl(ItemParamID, ref ctrl, sessionMgr.HeatingMappingId);
            }
            else
            {
                populateControl(ItemParamID, ref ctrl);
            }
            Label lblDdl = new Label();
            TableRow tr = new TableRow();
            //  tr.ID = ApplicationConstants.Tr + parameterId.ToString()
            TableCell td1 = new TableCell();
            // td1.ID = ApplicationConstants.Td1 + parameterId.ToString()
            td1.HorizontalAlign = HorizontalAlign.Right;
            lblDdl.ID = ApplicationConstants.lblDdl + parameterId.ToString();
            TableCell td2 = new TableCell();
            //td2.ID = ApplicationConstants.Td2 + parameterId.ToString()
            lblDdl.Text = attributeResult["ParameterName"].ToString() + ": ";
            td1.Controls.Add(lblDdl);
            td1.Attributes.Add("Width", "150");

            td2.Controls.Add(parameter);
            tr.Cells.Add(td1);
            tr.Cells.Add(td2);
            tr.Height = 30;
            return tr;
        }
        #endregion




        public void populateControl(int itemParamId, ref Control control)
        {

            DataSet parameterDataSet = sessionMgr.AttributeResultDataSet;
            DropDownList ddllist = (DropDownList)control;
            var preInsertedResult = (from ps in parameterDataSet.Tables[ApplicationConstants.PreInsertedValues].AsEnumerable() where ps["ItemParamID"].ToString() == itemParamId.ToString() select ps).FirstOrDefault();
            if (preInsertedResult != null)
            {
                ddllist.SelectedValue = preInsertedResult["VALUEID"].ToString();

            }
            else
            {
                ddllist.SelectedValue = "-1";
            }
        }

        public void populateBoilerControl(int itemParamId, ref Control control,int heatingMappingId)
        {

            DataSet parameterDataSet = sessionMgr.AttributeResultDataSet;
            DropDownList ddllist = (DropDownList)control;
            var preInsertedResult = (from ps in parameterDataSet.Tables[ApplicationConstants.PreInsertedValues].AsEnumerable() where ps["ItemParamID"].ToString() == itemParamId.ToString() && ps["HeatingMappingId"].ToString() == heatingMappingId.ToString() select ps).FirstOrDefault();
            if (preInsertedResult != null)
            {
                ddllist.SelectedValue = preInsertedResult["VALUEID"].ToString();

            }
            else
            {
                ddllist.SelectedValue = "-1";
            }
        }

        #region "create Read Only Control"
        public TableRow createReadOnlyControl(int parameterId)
        {
            DataTable loadParameterDdl = new DataTable();
            //create drop down list dynamically  
            Label parameterLiteral = new Label();
            //set style of control
            parameterLiteral.ID = ApplicationConstants.ltrlReadOnly + parameterId.ToString();
            parameterLiteral.CssClass = ApplicationConstants.ddlStyle;
            //Load Parameter values datatable
            DataSet parameterDataSet = sessionMgr.AttributeResultDataSet;
            loadParameterDdl = parameterDataSet.Tables[ApplicationConstants.ParameterValues];
            TableRow tr = new TableRow();
            //  tr.ID = ApplicationConstants.Tr + valueId.ToString()
            var attributeResult = (from ps in parameterDataSet.Tables[ApplicationConstants.Parameters].AsEnumerable() where ps["ParameterID"].ToString() == parameterId.ToString() select ps).FirstOrDefault();

            if (attributeResult != null)
            {
                // parameterLiteral.Text = attributeResult["ParameterName"].ToString();
                int ItemParamID = Convert.ToInt32(attributeResult["ItemParamID"]);
                //populate pre inserted value for dropdown
                this.populateControl(ItemParamID, ref parameterLiteral);
                Label lblParameterItem = new Label();

                TableCell td1 = new TableCell();
                // td1.ID = ApplicationConstants.Td1 + valueId.ToString()
                td1.HorizontalAlign = HorizontalAlign.Left;
                  
                lblParameterItem.ID = ApplicationConstants.ddlParameter + parameterId.ToString();
                TableCell td2 = new TableCell();
                //td2.ID = ApplicationConstants.Td2 + valueId.ToString()
                lblParameterItem.Text = attributeResult["ParameterName"].ToString() + ":";
                td2.Style.Add("padding-left", "20px");
                td1.Width = 150;
                td1.Controls.Add(lblParameterItem);
                td2.Controls.Add(parameterLiteral);
                tr.Cells.Add(td1);
                tr.Cells.Add(td2);
                tr.Height = 30;
            }


            return tr;
        }
        #endregion


        public void populateControl(int itemParamId, ref Label control)
        {
            DataSet parameterDataSet = sessionMgr.AttributeResultDataSet;

            var preInsertedResult = (from ps in parameterDataSet.Tables[ApplicationConstants.PreInsertedValues].AsEnumerable() where ps["ItemParamID"].ToString() == itemParamId.ToString() select ps).FirstOrDefault();
            if (preInsertedResult != null && preInsertedResult["PARAMETERVALUE"].ToString() != "Please Select")
            {
                control.Text = preInsertedResult["PARAMETERVALUE"].ToString();

            }
            else
            {
                control.Text = "-";


            }
        }
    }
}
