﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Data;
using PDR_Utilities.Constants;
using System.Web.UI;
using PDR_Utilities.Managers;

namespace PDR_BusinessLogic.DynamicControls
{
    public class CheckBoxControlBL:IControlBL
    {
        SessionManager sessionMgr = new SessionManager();
        #region "Functions"
        #region "Create controls"

        public TableRow createControl(int parameterId)
        {
            DataTable loadParameterDdl = new DataTable();
            CheckBoxList chkList = new CheckBoxList();
            //set control styles
            chkList.ID = ApplicationConstants.chkList + parameterId.ToString();
            chkList.CssClass = ApplicationConstants.dynamicRadioButton;
            //Load Parameter values datatable
            DataSet parameterDataSet = sessionMgr.AttributeResultDataSet;
            loadParameterDdl = parameterDataSet.Tables[ApplicationConstants.ParameterValues];
            //convert Parametervalues data table into dataview            
            DataView loadParameterDv = new DataView();
            loadParameterDv = loadParameterDdl.AsDataView();
            //Filter the Parameters datatable w.r.t parameterId
            var attributeResult = (from ps in parameterDataSet.Tables[ApplicationConstants.Parameters].AsEnumerable() where ps["ParameterID"].ToString() == parameterId.ToString() select ps).FirstOrDefault();

            //Filter the Parametervalues which have the same ParameterID.
            loadParameterDv.RowFilter = "ParameterID =  " + parameterId.ToString();
            loadParameterDdl = loadParameterDv.ToTable();
            //Bind the datasource
            chkList.DataSource = loadParameterDdl;
            chkList.DataValueField = "ValueID";
            chkList.DataTextField = "ValueDetail";
            chkList.DataBind();
            //Populate the check box list with existing inserted values
            int ItemParamID = Convert.ToInt32(attributeResult["ItemParamID"]);
            Control ctrl = (CheckBoxList)chkList;
            if (sessionMgr.TreeItemName.Contains("Boiler"))
            {
                populateBoilerControl(ItemParamID, ref ctrl, sessionMgr.HeatingMappingId);
            }
            else
            {
                populateControl(ItemParamID, ref ctrl);
            }
            Label lblChkBox = new Label();
            TableRow tr = new TableRow();
            tr.ID = ApplicationConstants.Tr + parameterId.ToString();
            TableCell td1 = new TableCell();
            td1.HorizontalAlign = HorizontalAlign.Right;
            td1.ID = ApplicationConstants.Td1 + parameterId.ToString();
            lblChkBox.ID = ApplicationConstants.lblChkBox + parameterId.ToString();
            TableCell td2 = new TableCell();
            td2.ID = ApplicationConstants.Td2 + parameterId.ToString();
            lblChkBox.Text = attributeResult["ParameterName"].ToString() + ": ";
            td1.Controls.Add(lblChkBox);
            td2.Controls.Add(chkList);
            tr.Cells.Add(td1);
            tr.Cells.Add(td2);
            tr.Height = 30;
            return tr;
        }

        #endregion
        
        #region " Populate Controls"
        public void populateControl(int itemParamId, ref Control control)
        {
            // Dim sa As textBo
            DataSet parameterDataSet = sessionMgr.AttributeResultDataSet;
            CheckBoxList chklist = (CheckBoxList)control;
            var preInsertedResult = (from ps in parameterDataSet.Tables[ApplicationConstants.PreInsertedValues].AsEnumerable() where ps["ItemParamID"].ToString() == itemParamId.ToString() select ps).ToList();

            foreach (DataRow lst in preInsertedResult)
            {
                if (Convert.ToBoolean(lst["IsCheckBoxSelected"]) == true)
                {
                    chklist.Items.FindByValue(lst["VALUEID"].ToString ()).Selected = true;
                    //tem.Selected = lst.Item("VALUEID")
                }
                
            }


        }
        #endregion

        #region " Populate BoilerControls"
        public void populateBoilerControl(int itemParamId, ref Control control, int heatingMappingId)
        {
            // Dim sa As textBo
            DataSet parameterDataSet = sessionMgr.AttributeResultDataSet;
            CheckBoxList chklist = (CheckBoxList)control;
            var preInsertedResult = (from ps in parameterDataSet.Tables[ApplicationConstants.PreInsertedValues].AsEnumerable() where ps["ItemParamID"].ToString() == itemParamId.ToString() && ps["HeatingMappingId"].ToString() == heatingMappingId.ToString() select ps).ToList();

            foreach (DataRow lst in preInsertedResult)
            {
                if (Convert.ToBoolean(lst["IsCheckBoxSelected"]) == true)
                {
                    chklist.Items.FindByValue(lst["VALUEID"].ToString()).Selected = true;
                    //tem.Selected = lst.Item("VALUEID")
                }

            }


        }
        #endregion
        #endregion


        public TableRow createReadOnlyControl(int parameterId)
        {
            throw new NotImplementedException();
        }

        public void populateControl(int itemParamId, ref Label control)
        {
            throw new NotImplementedException();
        }
    }
}
