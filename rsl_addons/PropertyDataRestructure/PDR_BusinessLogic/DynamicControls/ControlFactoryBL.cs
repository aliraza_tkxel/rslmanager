﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Data;
using PDR_Utilities.Constants;
using PDR_Utilities.Managers;

namespace PDR_BusinessLogic.DynamicControls
{
    public class ControlFactoryBL
    {
        SessionManager sessionMgr = new SessionManager();
        #region "create dynamic control"
        /// <summary>
        /// create dynamic control for edit view and readonly
        /// </summary>
        /// <param name="parameterDataSet"></param>
        /// <param name="isReadOnly"></param>
        /// <returns></returns>
        public static Table createDynamicControls(DataSet parameterDataSet, bool isReadOnly)
        {
            Table tblOtherDetails = new Table();
            //showInAppIndex,notShowInApp and showInApp will used to populate controls on right hand side of panel.
            int showInAppIndex = -1;
            int notShowInApp = 0;
            //Check Datatable is not null while iterating for parameters
            if (parameterDataSet.Tables[ApplicationConstants.Parameters].Rows.Count > 0)
            {
                foreach (DataRow dr in parameterDataSet.Tables[ApplicationConstants.Parameters].Rows)
                {
                    string controlType = dr["ControlType"].ToString();
                    int parameterId = Convert.ToInt32(dr["ParameterID"]);
                    bool showInApp = Convert.ToBoolean(dr["ShowInApp"]);
                    if (showInApp == false)
                    {
                        showInAppIndex = showInAppIndex + 1;
                    }
                    else
                    {
                        notShowInApp = notShowInApp + 1;
                    }
                    if (controlType.ToUpper() == "Dropdown".ToUpper())
                    {
                        DataTable dt = new DataTable();

                        TableRow tr = new TableRow();
                        DropDownControlBL objDropDownControlBl = new DropDownControlBL();
                        if (showInApp == false && notShowInApp > 0)
                        {
                            TableRow trShowInApp = tblOtherDetails.Rows[showInAppIndex];
                            TableCell td3 = new TableCell();
                            Table tblShowInApp = new Table();
                            TableRow trInnerShowInApp = new TableRow();
                            if (isReadOnly)
                                trInnerShowInApp = objDropDownControlBl.createReadOnlyControl(parameterId);
                            else
                                trInnerShowInApp = objDropDownControlBl.createControl(parameterId);
                            trInnerShowInApp.Style.Add("display", "table-row");
                            tblShowInApp.Rows.Add(trInnerShowInApp);
                            td3.Controls.Add(tblShowInApp);
                            trShowInApp.Cells.Add(td3);
                            trShowInApp.Style.Add("display", "table-row");

                        }
                        else
                        {
                            if (isReadOnly)
                                tr = objDropDownControlBl.createReadOnlyControl(parameterId);
                            else
                                tr = objDropDownControlBl.createControl(parameterId);
                            tblOtherDetails.Rows.Add(tr);
                        }



                    }
                    else if (controlType.ToUpper() == "Date".ToUpper())
                    {
                        TableRow tr = new TableRow();
                        DatesControlBL objDatesBl = new DatesControlBL();
                        tr = objDatesBl.createControl(parameterId);
                        tblOtherDetails.Rows.Add(tr);


                    }
                    else if (controlType.ToUpper() == "Textbox".ToUpper())
                    {

                        DataTable dt = new DataTable();

                        TableRow tr = new TableRow();
                        TextBoxControlBL objTextBoxControlBl = new TextBoxControlBL();
                        if (showInApp == false && notShowInApp > 0)
                        {
                            TableRow trShowInApp = tblOtherDetails.Rows[showInAppIndex];
                            TableCell td3 = new TableCell();
                            Table tblShowInApp = new Table();
                            TableRow trInnerShowInApp = new TableRow();
                            if (isReadOnly)
                                trInnerShowInApp = objTextBoxControlBl.createReadOnlyControl(parameterId);
                            else
                                trInnerShowInApp = objTextBoxControlBl.createControl(parameterId);
                            trInnerShowInApp.Style.Add("display", "table-row");
                            tblShowInApp.Rows.Add(trInnerShowInApp);
                            td3.Controls.Add(tblShowInApp);
                            trShowInApp.Cells.Add(td3);
                            trShowInApp.Style.Add("display", "table-row");

                        }
                        else
                        {
                            if (isReadOnly)
                                tr = objTextBoxControlBl.createReadOnlyControl(parameterId);
                            else
                                tr = objTextBoxControlBl.createControl(parameterId);
                            tblOtherDetails.Rows.Add(tr);
                        }

                    }
                    else if (controlType.ToUpper() == "TextArea".ToUpper())
                    {
                        TableRow tr = new TableRow();
                        TextAreaControlBL textBoxBlObj = new TextAreaControlBL();

                        if (isReadOnly)
                            tr = textBoxBlObj.createReadOnlyControl(parameterId);
                        else
                            tr = textBoxBlObj.createControl(parameterId);
                        tblOtherDetails.Rows.Add(tr);

                    }
                    else if (controlType.ToUpper() == "Checkboxes".ToUpper())
                    {
                        TableRow tr = new TableRow();
                        RadioButtonControlBL objRadioButtonBl = new RadioButtonControlBL();
                        CheckBoxControlBL objCheckBoxControlBl = new CheckBoxControlBL();
                        if (isReadOnly)
                            tr = objRadioButtonBl.createReadOnlyControl(parameterId);
                        else
                            tr = objCheckBoxControlBl.createControl(parameterId);
                        tblOtherDetails.Rows.Add(tr);

                    }
                    else if (controlType.ToUpper() == "Radiobutton".ToUpper())
                    {
                        TableRow tr = new TableRow();
                        RadioButtonControlBL objRadioButtonBl = new RadioButtonControlBL();

                        if (isReadOnly)
                            tr = objRadioButtonBl.createReadOnlyControl(parameterId);
                        else
                            tr = objRadioButtonBl.createControl(parameterId);
                        tblOtherDetails.Rows.Add(tr);

                    }




                }

            }

            //TableRow inspectedTblDataRow = new TableRow();
            //inspectedTblDataRow = CreateLastInspected();
            //tblOtherDetails.Rows.Add(inspectedTblDataRow);


            return tblOtherDetails;

        }
        #endregion






    }
}
