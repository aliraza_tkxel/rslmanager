﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PDR_Utilities.Managers;
using System.Web.UI.WebControls;
using PDR_Utilities.Constants;
using System.Data;
using System.Web.UI;
using AjaxControlToolkit;

namespace PDR_BusinessLogic.DynamicControls
{
    public class DatesControlBL
    {
        SessionManager sessionMgr = new SessionManager();
        #region "Create controls"
        public TableRow createControl(int parameterId)
        {
            //Dim loadParameterDDL As DataTable = New DataTable()
            TextBox txtDate = new TextBox();
            //Set control styles
            txtDate.ID = ApplicationConstants.txtDate + parameterId.ToString();
            txtDate.CssClass = ApplicationConstants.textBoxStyle;
            //get dataset from session
            DataSet parameterDataSet = sessionMgr.AttributeResultDataSet;
            // filter the Parameters datatable w.r.t parameterID
            dynamic attributeResult = (from ps in parameterDataSet.Tables[ApplicationConstants.Parameters].AsEnumerable() where ps["ParameterID"].ToString() == parameterId.ToString() select ps).FirstOrDefault();
            //Populate control with existing inserted values
            int ItemParamID = Convert.ToInt32(attributeResult["ItemParamID"]);
            Control ctrl = (TextBox)txtDate;
            if (sessionMgr.TreeItemName.Contains("Boiler"))
            {
                populateBoilerControl(parameterId, ref ctrl, sessionMgr.HeatingMappingId);
            }
            else
            {
                populateControl(parameterId, ref ctrl);
            }

            Label lblDate = new Label();
            TableRow tr = new TableRow();
            tr.ID = ApplicationConstants.Tr + parameterId.ToString();
            TableCell td1 = new TableCell();
            td1.HorizontalAlign = HorizontalAlign.Right;
            td1.ID = ApplicationConstants.Td1 + parameterId.ToString();
            TableCell td2 = new TableCell();
            td2.ID = ApplicationConstants.Td2 + parameterId.ToString();
            lblDate.ID = ApplicationConstants.lblDate + parameterId.ToString();
            lblDate.Text = attributeResult["ParameterName"].ToString() + ": ";
            //'Add Calendar Extender to date control

            CalendarExtender calendarExtender = new CalendarExtender();
            calendarExtender.TargetControlID = txtDate.ID;
            calendarExtender.ID = ApplicationConstants.extender + parameterId.ToString();
            calendarExtender.Format = "dd/MM/yyyy";
            //txtDate.ReadOnly = True
            td1.Controls.Add(lblDate);
            td2.Controls.Add(txtDate);
            td2.Controls.Add(calendarExtender);
            tr.Cells.Add(td1);
            tr.Cells.Add(td2);
            tr.Height = 30;
            return tr;
        }
        #endregion

        #region " Populate Controls"
        public void populateControl(int parameterId, ref Control control)
        {
            //these different type of condition will help to populate component mapping with life cycle 
            //commented area will used for component mapping with item,parameter,parameter value , subparameter and subparameter value
            DataSet parameterDataSet = sessionMgr.AttributeResultDataSet;
            var attributeResult = (from ps in parameterDataSet.Tables[ApplicationConstants.Parameters].AsEnumerable() where ps["ParameterID"].ToString() == parameterId.ToString() select ps).FirstOrDefault();
            int itemParamId = Convert.ToInt32(attributeResult["ItemParamID"]);
            bool isEventAdded = false;
            if (attributeResult != null)
            {
                TextBox txtParameter = (TextBox)control;
                //var replacementResult = (from ps in parameterDataSet.Tables[ApplicationConstants.PlannedComponent].AsEnumerable() where ps["ITEMID"].ToString() == sessionMgr.TreeItemId.ToString() select ps).FirstOrDefault();
                //if (replacementResult != null)
                //{
                //    isEventAdded = true;
                //}


                if (attributeResult["ParameterName"].ToString().Contains("Due"))
                {
                    // if parameter name is Rewired due the, 
                    if (attributeResult["ParameterName"].ToString().Contains("Rewire Due"))
                    {
                        //if (replacementResult != null)
                        //{
                        //    txtParameter.Enabled = false;
                        //}
                        txtParameter.ID = ApplicationConstants.txtDate + ApplicationConstants.rewiredDue.ToString();
                        var ItemDatesResult = (from ps in parameterDataSet.Tables[ApplicationConstants.LastReplaced].AsEnumerable()
                                               where ps["ItemID"].ToString().Equals(sessionMgr.TreeItemId.ToString()) && ps["ParameterName"].Equals("Last Rewired")
                                               select ps).FirstOrDefault();
                        if (ItemDatesResult != null)
                        {
                            if (ItemDatesResult["DueDate"] != DBNull.Value)
                            {
                                txtParameter.Text = ItemDatesResult["DueDate"].ToString();
                            }

                        }
                    }
                    else if (attributeResult["ParameterName"].ToString().Contains("Upgrade Due"))
                    {
                        //if (replacementResult != null)
                        //{
                        //    txtParameter.Enabled = false;
                        //}
                        txtParameter.ID = ApplicationConstants.txtDate + ApplicationConstants.upgradeDue.ToString();
                        var ItemDatesResult = (from ps in parameterDataSet.Tables[ApplicationConstants.LastReplaced].AsEnumerable()
                                               where ps["ItemID"].ToString().Equals(sessionMgr.TreeItemId.ToString()) && ps["ParameterName"].Equals("Electrical Upgrade Last Done")
                                               select ps).FirstOrDefault();
                        if (ItemDatesResult != null)
                        {
                            if (ItemDatesResult["DueDate"] != DBNull.Value)
                            {
                                txtParameter.Text = ItemDatesResult["DueDate"].ToString();
                            }

                        }

                    }
                    else if (attributeResult["ParameterName"].ToString().ToUpper() == "Replacement Due".ToUpper())
                    {
                        //if (replacementResult != null)
                        //{
                        //    txtParameter.Enabled = false;
                        //}
                        txtParameter.ID = ApplicationConstants.txtDate + ApplicationConstants.ReplacementDue.ToString();
                        var ItemDatesResult = (from ps in parameterDataSet.Tables[ApplicationConstants.LastReplaced].AsEnumerable()
                                               where ps["ItemID"].ToString().Equals(sessionMgr.TreeItemId.ToString()) && object.ReferenceEquals(ps["ParameterID"], DBNull.Value)
                                               select ps).FirstOrDefault();
                        if (ItemDatesResult != null)
                        {
                            if (ItemDatesResult["DueDate"] != DBNull.Value)
                            {
                                txtParameter.Text = ItemDatesResult["DueDate"].ToString();
                            }

                        }

                    }
                    else
                    {
                        var ReplacementDueResult = (from ps in parameterDataSet.Tables[ApplicationConstants.LastReplaced].AsEnumerable()
                                                    where ps["ItemID"].ToString().Equals(sessionMgr.TreeItemId.ToString()) && ps["ParameterID"].ToString().Equals(parameterId.ToString())
                                                    select ps).FirstOrDefault();
                        if (ReplacementDueResult != null)
                        {
                            if (ReplacementDueResult["DueDate"] != DBNull.Value)
                            {
                                txtParameter.Text = ReplacementDueResult["DueDate"].ToString();
                            }

                        }
                    }
                }
                else
                {
                    if (attributeResult["ParameterName"].ToString().Contains("Last Rewired"))
                    {
                        txtParameter.ID = ApplicationConstants.txtDate + ApplicationConstants.lastRewired.ToString();
                        //txtParameter.TextChanged += parameter_TextChanged;
                        //txtParameter.AutoPostBack = true;
                        var ItemDatesResult = (from ps in parameterDataSet.Tables[ApplicationConstants.LastReplaced].AsEnumerable()
                                               where ps["ItemID"].ToString().Equals(sessionMgr.TreeItemId.ToString()) && ps["ParameterName"].Equals("Last Rewired")
                                               select ps).FirstOrDefault();
                        if (ItemDatesResult != null)
                        {
                            if (ItemDatesResult["LastDone"] != DBNull.Value)
                            {
                                txtParameter.Text = ItemDatesResult["LastDone"].ToString();
                            }
                        }
                    }
                    else if (attributeResult["ParameterName"].ToString().Contains("Upgrade Last Done"))
                    {
                        txtParameter.ID = ApplicationConstants.txtDate + ApplicationConstants.lastUpdrade.ToString();
                        //txtParameter.TextChanged += parameter_TextChanged;
                        //txtParameter.AutoPostBack = true;
                        var ItemDatesResult = (from ps in parameterDataSet.Tables[ApplicationConstants.LastReplaced].AsEnumerable()
                                               where ps["ItemID"].ToString().Equals(sessionMgr.TreeItemId.ToString()) && ps["ParameterName"].Equals("Electrical Upgrade Last Done")
                                               select ps).FirstOrDefault();
                        if (ItemDatesResult != null)
                        {
                            if (ItemDatesResult["LastDone"] != DBNull.Value)
                            {
                                txtParameter.Text = ItemDatesResult["LastDone"].ToString();
                            }
                        }

                    }
                    else if (attributeResult["ParameterName"].ToString().ToUpper() == "Last Replaced".ToUpper())
                    {
                        txtParameter.ID = ApplicationConstants.txtDate + ApplicationConstants.LastReplaced.ToString();
                        if (isEventAdded)
                        {
                            //txtParameter.TextChanged += parameter_TextChanged;
                            //txtParameter.AutoPostBack = true;
                        }
                        var ItemDatesResult = (from ps in parameterDataSet.Tables[ApplicationConstants.LastReplaced].AsEnumerable()
                                               where ps["ItemID"].ToString().Equals(sessionMgr.TreeItemId.ToString()) && object.ReferenceEquals(ps["ParameterID"], DBNull.Value)
                                               select ps).FirstOrDefault();
                        if (ItemDatesResult != null)
                        {
                            if (ItemDatesResult["LastDone"] != DBNull.Value)
                            {
                                txtParameter.Text = ItemDatesResult["LastDone"].ToString();
                            }
                        }
                    }
                    else
                    {
                        var ReplacementDueResult = (from ps in parameterDataSet.Tables[ApplicationConstants.LastReplaced].AsEnumerable()
                                                    where ps["ItemID"].ToString().Equals(sessionMgr.TreeItemId.ToString()) && ps["ParameterID"].ToString().Equals(parameterId.ToString())
                                                    select ps).FirstOrDefault();
                        if (ReplacementDueResult != null)
                        {
                            if (ReplacementDueResult["DueDate"] != DBNull.Value)
                            {
                                txtParameter.Text = ReplacementDueResult["DueDate"].ToString();
                            }

                        }
                    }
                }


            }
        }
        #endregion

        #region " Populate Controls"
        public void populateBoilerControl(int parameterId, ref Control control, int heatingMappingId)
        {
            //these different type of condition will help to populate component mapping with life cycle 
            //commented area will used for component mapping with item,parameter,parameter value , subparameter and subparameter value
            DataSet parameterDataSet = sessionMgr.AttributeResultDataSet;
            var attributeResult = (from ps in parameterDataSet.Tables[ApplicationConstants.Parameters].AsEnumerable() where ps["ParameterID"].ToString() == parameterId.ToString() select ps).FirstOrDefault();
            int itemParamId = Convert.ToInt32(attributeResult["ItemParamID"]);
            bool isEventAdded = false;
            if (attributeResult != null)
            {
                TextBox txtParameter = (TextBox)control;
                //var replacementResult = (from ps in parameterDataSet.Tables[ApplicationConstants.PlannedComponent].AsEnumerable() where ps["ITEMID"].ToString() == sessionMgr.TreeItemId.ToString() select ps).FirstOrDefault();
                //if (replacementResult != null)
                //{
                //    isEventAdded = true;
                //}


                if (attributeResult["ParameterName"].ToString().Contains("Due"))
                {
                    // if parameter name is Rewired due the, 
                    if (attributeResult["ParameterName"].ToString().Contains("Rewire Due"))
                    {
                        //if (replacementResult != null)
                        //{
                        //    txtParameter.Enabled = false;
                        //}
                        txtParameter.ID = ApplicationConstants.txtDate + ApplicationConstants.rewiredDue.ToString();
                        var ItemDatesResult = (from ps in parameterDataSet.Tables[ApplicationConstants.LastReplaced].AsEnumerable()
                                               where ps["ItemID"].ToString().Equals(sessionMgr.TreeItemId.ToString()) && ps["ParameterName"].Equals("Last Rewired")
                                               select ps).FirstOrDefault();
                        if (ItemDatesResult != null)
                        {
                            if (ItemDatesResult["DueDate"] != DBNull.Value)
                            {
                                txtParameter.Text = ItemDatesResult["DueDate"].ToString();
                            }

                        }
                    }
                    else if (attributeResult["ParameterName"].ToString().Contains("Upgrade Due"))
                    {
                        //if (replacementResult != null)
                        //{
                        //    txtParameter.Enabled = false;
                        //}
                        txtParameter.ID = ApplicationConstants.txtDate + ApplicationConstants.upgradeDue.ToString();
                        var ItemDatesResult = (from ps in parameterDataSet.Tables[ApplicationConstants.LastReplaced].AsEnumerable()
                                               where ps["ItemID"].ToString().Equals(sessionMgr.TreeItemId.ToString()) && ps["ParameterName"].Equals("Electrical Upgrade Last Done")
                                               select ps).FirstOrDefault();
                        if (ItemDatesResult != null)
                        {
                            if (ItemDatesResult["DueDate"] != DBNull.Value)
                            {
                                txtParameter.Text = ItemDatesResult["DueDate"].ToString();
                            }

                        }

                    }
                    else if (attributeResult["ParameterName"].ToString().ToUpper() == "Replacement Due".ToUpper())
                    {
                        //if (replacementResult != null)
                        //{
                        //    txtParameter.Enabled = false;
                        //}
                        txtParameter.ID = ApplicationConstants.txtDate + ApplicationConstants.ReplacementDue.ToString();
                        var ItemDatesResult = (from ps in parameterDataSet.Tables[ApplicationConstants.LastReplaced].AsEnumerable()
                                               where ps["ItemID"].ToString().Equals(sessionMgr.TreeItemId.ToString()) && object.ReferenceEquals(ps["ParameterID"], DBNull.Value)
                                                && ps["HeatingMappingId"].ToString() == heatingMappingId.ToString()
                                               select ps).FirstOrDefault();
                        if (ItemDatesResult != null)
                        {
                            if (ItemDatesResult["DueDate"] != DBNull.Value)
                            {
                                txtParameter.Text = ItemDatesResult["DueDate"].ToString();
                            }

                        }

                    }
                    else
                    {
                        var ReplacementDueResult = (from ps in parameterDataSet.Tables[ApplicationConstants.LastReplaced].AsEnumerable()
                                                    where ps["ItemID"].ToString().Equals(sessionMgr.TreeItemId.ToString()) && ps["ParameterID"].ToString().Equals(parameterId.ToString())
                                                    && ps["HeatingMappingId"].ToString() == heatingMappingId.ToString()
                                                    select ps).FirstOrDefault();
                        if (ReplacementDueResult != null)
                        {
                            if (ReplacementDueResult["DueDate"] != DBNull.Value)
                            {
                                txtParameter.Text = ReplacementDueResult["DueDate"].ToString();
                            }

                        }
                    }
                }
                else
                {
                    if (attributeResult["ParameterName"].ToString().Contains("Last Rewired"))
                    {
                        txtParameter.ID = ApplicationConstants.txtDate + ApplicationConstants.lastRewired.ToString();
                        //txtParameter.TextChanged += parameter_TextChanged;
                        //txtParameter.AutoPostBack = true;
                        var ItemDatesResult = (from ps in parameterDataSet.Tables[ApplicationConstants.LastReplaced].AsEnumerable()
                                               where ps["ItemID"].ToString().Equals(sessionMgr.TreeItemId.ToString()) && ps["ParameterName"].Equals("Last Rewired")
                                               select ps).FirstOrDefault();
                        if (ItemDatesResult != null)
                        {
                            if (ItemDatesResult["LastDone"] != DBNull.Value)
                            {
                                txtParameter.Text = ItemDatesResult["LastDone"].ToString();
                            }
                        }
                    }
                    else if (attributeResult["ParameterName"].ToString().Contains("Upgrade Last Done"))
                    {
                        txtParameter.ID = ApplicationConstants.txtDate + ApplicationConstants.lastUpdrade.ToString();
                        //txtParameter.TextChanged += parameter_TextChanged;
                        //txtParameter.AutoPostBack = true;
                        var ItemDatesResult = (from ps in parameterDataSet.Tables[ApplicationConstants.LastReplaced].AsEnumerable()
                                               where ps["ItemID"].ToString().Equals(sessionMgr.TreeItemId.ToString()) && ps["ParameterName"].Equals("Electrical Upgrade Last Done")
                                               select ps).FirstOrDefault();
                        if (ItemDatesResult != null)
                        {
                            if (ItemDatesResult["LastDone"] != DBNull.Value)
                            {
                                txtParameter.Text = ItemDatesResult["LastDone"].ToString();
                            }
                        }

                    }
                    else if (attributeResult["ParameterName"].ToString().ToUpper() == "Last Replaced".ToUpper())
                    {
                        txtParameter.ID = ApplicationConstants.txtDate + ApplicationConstants.LastReplaced.ToString();
                        if (isEventAdded)
                        {
                            //txtParameter.TextChanged += parameter_TextChanged;
                            //txtParameter.AutoPostBack = true;
                        }
                        var ItemDatesResult = (from ps in parameterDataSet.Tables[ApplicationConstants.LastReplaced].AsEnumerable()
                                               where ps["ItemID"].ToString().Equals(sessionMgr.TreeItemId.ToString()) && object.ReferenceEquals(ps["ParameterID"], DBNull.Value)
                                               && ps["HeatingMappingId"].ToString() == heatingMappingId.ToString()
                                               select ps).FirstOrDefault();
                        if (ItemDatesResult != null)
                        {
                            if (ItemDatesResult["LastDone"] != DBNull.Value)
                            {
                                txtParameter.Text = ItemDatesResult["LastDone"].ToString();
                            }
                        }
                    }
                    else
                    {
                        var ReplacementDueResult = (from ps in parameterDataSet.Tables[ApplicationConstants.LastReplaced].AsEnumerable()
                                                    where ps["ItemID"].ToString().Equals(sessionMgr.TreeItemId.ToString()) && ps["ParameterID"].ToString().Equals(parameterId.ToString())
                                                    && ps["HeatingMappingId"].ToString() == heatingMappingId.ToString()
                                                    select ps).FirstOrDefault();
                        if (ReplacementDueResult != null)
                        {
                            if (ReplacementDueResult["DueDate"] != DBNull.Value)
                            {
                                txtParameter.Text = ReplacementDueResult["DueDate"].ToString();
                            }

                        }
                    }
                }


            }
        }
        #endregion


    }
}
