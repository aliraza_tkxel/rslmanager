﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Data;
using PDR_Utilities.Constants;
using PDR_Utilities.Managers;
using System.Web.UI;

namespace PDR_BusinessLogic.DynamicControls
{
    public class RadioButtonControlBL
    {
        SessionManager sessionMgr = new SessionManager();
        #region "Functions"
        #region "Create controls"
        public TableRow createControl(int parameterId)
        {
            DataTable loadParameterRdbtn = new DataTable();
            RadioButtonList rdBtn = new RadioButtonList();
            rdBtn.CssClass = ApplicationConstants.dynamicRadioButton;
            rdBtn.RepeatDirection = RepeatDirection.Horizontal;
            //set control styles
            rdBtn.ID = ApplicationConstants.rdBtn + parameterId.ToString();
            //Load Parameter values data table
            DataSet parameterDataSet = sessionMgr.AttributeResultDataSet;
            loadParameterRdbtn = parameterDataSet.Tables[ApplicationConstants.ParameterValues];
            //convert Parameter values data table into data view            
            DataView loadParameterDv = new DataView();
            loadParameterDv = loadParameterRdbtn.AsDataView();
            //Filter the Parameters data table w.r.t parameterId
            dynamic attributeResult = (from ps in parameterDataSet.Tables[ApplicationConstants.Parameters].AsEnumerable() where ps["ParameterID"].ToString() == parameterId.ToString() select ps).FirstOrDefault();

            //rdBtn.AutoPostBack = True
            //AddHandler rdBtn.SelectedIndexChanged, AddressOf parameter_SelectedIndexChanged


            //Filter the Parameter values which have the same ParameterID.
            loadParameterDv.RowFilter = "ParameterID =  " + parameterId.ToString();
            loadParameterRdbtn = loadParameterDv.ToTable();
            //Bind the data source
            rdBtn.DataSource = loadParameterRdbtn;
            rdBtn.DataValueField = "ValueID";
            rdBtn.DataTextField = "ValueDetail";
            rdBtn.DataBind();
            //Populate the check box list with existing inserted values
            int ItemParamID = Convert.ToInt32(attributeResult["ItemParamID"]);
            Control ctrl = (RadioButtonList)rdBtn;
            if (sessionMgr.TreeItemName.Contains("Boiler"))
            {
                populateBoilerControl(ItemParamID, ref ctrl, sessionMgr.HeatingMappingId);
            }
            else
            {
                populateControl(ItemParamID, ref ctrl);
            }
            string worksRequiredId = ApplicationConstants.Tr + "_WorksRequired_" + rdBtn.ClientID;
            sessionMgr.WorksRequiredRowId=worksRequiredId;
            foreach (ListItem item in rdBtn.Items)
            {
                if (item.Text == ApplicationConstants.conditionSatisfactory)
                {
                    item.Attributes.Add("onclick", "HideWorksRequired(this,'" + worksRequiredId + "');");

                }
                else
                {
                    item.Attributes.Add("onclick", "ShowWorksRequired(this,'" + worksRequiredId + "');");
                }
            }
            Label lblRadioBtn = new Label();
            TableRow tr = new TableRow();
            tr.ID = ApplicationConstants.Tr + parameterId.ToString();
            TableCell td1 = new TableCell();
            td1.HorizontalAlign = HorizontalAlign.Right;
            td1.ID = ApplicationConstants.Td1 + parameterId.ToString();
            lblRadioBtn.ID = ApplicationConstants.lblRadioBtn + parameterId.ToString();
            TableCell td2 = new TableCell();
            td2.ID = ApplicationConstants.Td2 + parameterId.ToString();
            lblRadioBtn.Text = attributeResult["ParameterName"].ToString() + ": ";
            td1.Controls.Add(lblRadioBtn);
            td2.Controls.Add(rdBtn);
            td2.Attributes.Add("colspan", "2");
            tr.Cells.Add(td1);
            tr.Cells.Add(td2);
            tr.Height = 30;
            return tr;
        }
        #endregion
        
        #region " Populate Controls"
        public void populateControl(int itemParamId, ref Control control)
        {
            // Dim sa As textBo
            DataSet parameterDataSet = sessionMgr.AttributeResultDataSet;
            RadioButtonList chklist = (RadioButtonList)control;

            var preInsertedResult = (from ps in parameterDataSet.Tables[ApplicationConstants.PreInsertedValues].AsEnumerable() where ps["ItemParamID"].ToString() == itemParamId.ToString() select ps);
            if (preInsertedResult != null && preInsertedResult.Count() > 0)
            {
                foreach (DataRow lst in preInsertedResult)
                {
                    foreach (ListItem item in chklist.Items)
                    {
                        if (item.Value == lst["VALUEID"].ToString ())
                        {
                            item.Selected = true;


                            if (item.Text == ApplicationConstants.conditionSatisfactory)
                            {
                                sessionMgr.IsWorkSatisfactory = true;
                            }
                            else
                            {
                                sessionMgr.IsWorkSatisfactory = false;
                            }
                        }
                    }
                }
            }
            else
            {
                sessionMgr.IsWorkSatisfactory = true;
            }
        }
        #endregion
        #region " Populate Boiler Controls"
        public void populateBoilerControl(int itemParamId, ref Control control, int heatingMappingId)
        {
            // Dim sa As textBo
            DataSet parameterDataSet = sessionMgr.AttributeResultDataSet;
            RadioButtonList chklist = (RadioButtonList)control;

            var preInsertedResult = (from ps in parameterDataSet.Tables[ApplicationConstants.PreInsertedValues].AsEnumerable() where ps["ItemParamID"].ToString() == itemParamId.ToString() && ps["HeatingMappingId"].ToString() == heatingMappingId.ToString()  select ps);
            if (preInsertedResult != null && preInsertedResult.Count() > 0)
            {
                foreach (DataRow lst in preInsertedResult)
                {
                    foreach (ListItem item in chklist.Items)
                    {
                        if (item.Value == lst["VALUEID"].ToString())
                        {
                            item.Selected = true;


                            if (item.Text == ApplicationConstants.conditionSatisfactory)
                            {
                                sessionMgr.IsWorkSatisfactory = true;
                            }
                            else
                            {
                                sessionMgr.IsWorkSatisfactory = false;
                            }
                        }
                    }
                }
            }
            else
            {
                sessionMgr.IsWorkSatisfactory = true;
            }
        }
        #endregion

        #region  create Read Only Control
        public TableRow createReadOnlyControl(int parameterId)
        {
            DataTable loadParameterDdl = new DataTable();
            //create drop down list dynamically  
            Label parameterLiteral = new Label();
            //set style of control
            parameterLiteral.ID = ApplicationConstants.ltrlReadOnly + parameterId.ToString();
            parameterLiteral.CssClass = ApplicationConstants.ddlStyle;
            //Load Parameter values datatable
            DataSet parameterDataSet = sessionMgr.AttributeResultDataSet;
            loadParameterDdl = parameterDataSet.Tables[ApplicationConstants.ParameterValues];
            TableRow tr = new TableRow();
            //  tr.ID = ApplicationConstants.Tr + valueId.ToString()
            var attributeResult = (from ps in parameterDataSet.Tables[ApplicationConstants.Parameters].AsEnumerable() where ps["ParameterID"].ToString() == parameterId.ToString() select ps).FirstOrDefault();

            if (attributeResult != null)
            {
                // parameterLiteral.Text = attributeResult["ParameterName"].ToString();
                int ItemParamID = Convert.ToInt32(attributeResult["ItemParamID"]);
                //populate pre inserted value for dropdown
                this.populateControl(ItemParamID, ref parameterLiteral);
                Label lblParameterItem = new Label();

                TableCell td1 = new TableCell();
                // td1.ID = ApplicationConstants.Td1 + valueId.ToString()
                td1.HorizontalAlign = HorizontalAlign.Left;

                lblParameterItem.ID = ApplicationConstants.ddlParameter + parameterId.ToString();
                TableCell td2 = new TableCell();
                //td2.ID = ApplicationConstants.Td2 + valueId.ToString()
                lblParameterItem.Text = attributeResult["ParameterName"].ToString() + ":";
                td2.Style.Add("padding-left", "20px");
                td1.Width = 150;
                td1.Controls.Add(lblParameterItem);
                td2.Controls.Add(parameterLiteral);
                tr.Cells.Add(td1);
                tr.Cells.Add(td2);
                tr.Height = 30;
            }


            return tr;
        }
        #endregion


        public void populateControl(int itemParamId, ref Label control)
        {
            DataSet parameterDataSet = sessionMgr.AttributeResultDataSet;

            var preInsertedResult = (from ps in parameterDataSet.Tables[ApplicationConstants.PreInsertedValues].AsEnumerable() where ps["ItemParamID"].ToString() == itemParamId.ToString() select ps).FirstOrDefault();
            if (preInsertedResult != null && preInsertedResult["PARAMETERVALUE"].ToString() != "Please Select")
            {
                control.Text = preInsertedResult["PARAMETERVALUE"].ToString();

            }
            else
            {
                control.Text = "-";


            }
        }

        #endregion
    }
}
