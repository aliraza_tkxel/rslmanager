﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PDR_BusinessLogic.Base;
using PDR_DataAccess.VoidInspections;
using System.Data;
using PDR_BusinessObject.PageSort;
using PDR_BusinessObject.RequiredWorks;
using PDR_BusinessObject.CommonSearch;

namespace PDR_BusinessLogic.VoidInspections
{
    public class VoidInspectionsBL : BaseBL
    {
        IVoidInspectionsRepo objVoidInspection;
        public VoidInspectionsBL(IVoidInspectionsRepo rRepo)
        {
            objVoidInspection = rRepo;
        }
        #region get Inspection To Be Arranged List
        /// <summary>
        /// get Inspection To Be Arranged List
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        public int getInspectionToBeArrangedList(ref DataSet resultDataSet, PageSortBO objPageSortBO, string searchText,int PatchNum, bool getOnlyCount = false)
        {
            return objVoidInspection.getInspectionToBeArrangedList(ref resultDataSet, objPageSortBO, searchText, PatchNum, getOnlyCount);
        }
        #endregion

        #region get Inspection Arranged List
        /// <summary>
        /// get Inspection Arranged List
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        public int getInspectionArrangedList(ref DataSet resultDataSet, PageSortBO objPageSortBO, string searchText,int patchNum, bool getOnlyCount = false)
        {
            return objVoidInspection.getInspectionArrangedList(ref resultDataSet, objPageSortBO, searchText,patchNum, getOnlyCount);
        }
        #endregion

        #region get Inspection Arranged By Journal Id
        /// <summary>
        /// get Inspection Arranged By Journal Id
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        public void getInspectionArrangedByJournalId(ref DataSet resultDataSet,int journalId)
        {
            objVoidInspection.getInspectionArrangedByJournalId(ref resultDataSet, journalId);
        }
        #endregion

        #region get Void Works To Be Arranged
        /// <summary>
        /// get Void Works To Be Arranged
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <param name="getOnlyCount"></param>
        /// <returns></returns>
        public int getVoidWorksToBeArranged(ref DataSet resultDataSet, PageSortBO objPageSortBO, string searchText,int patchNum, bool getOnlyCount = false)
        {
            return objVoidInspection.getVoidWorksToBeArranged(ref resultDataSet, objPageSortBO, searchText,patchNum, getOnlyCount);
        }

        #endregion

        #region get Void Works Required
        /// <summary>
        /// Get Void Works Required
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        public int getVoidWorksRequired(ref DataSet resultDataSet, PageSortBO objPageSortBO, int journalId,bool isRearrange=false)
        {
            return objVoidInspection.getVoidWorksRequired(ref resultDataSet, objPageSortBO, journalId, isRearrange);
        }
        #endregion

        #region get Arranged Void Works Required
        /// <summary>
        /// Get Arranged Void Works Required
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        public int getArrangedVoidWorksRequired(ref DataSet resultDataSet, PageSortBO objPageSortBO, int journalId,bool isRearrange=false)
        {
            return objVoidInspection.getArrangedVoidWorksRequired(ref resultDataSet, objPageSortBO, journalId, isRearrange);
        }
        #endregion

        #region get Void Works by inspection journalId
        /// <summary>
        /// get Void Works by inspection journalId
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        public void getVoidWorksbyinspectionjournalId(ref DataSet resultDataSet, int journalId)
        {
            objVoidInspection.getVoidWorksbyinspectionjournalId(ref resultDataSet, journalId);
        }
        #endregion

        #region get Operative Data For Alert
        /// <summary>
        /// get Operative Data For Alert
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="journalId"></param>
        /// <returns></returns>
        public void getOperativeDataForAlert(ref DataSet resultDataSet, int journalId)
        {
            objVoidInspection.getOperativeDataForAlert(ref resultDataSet, journalId);
        }
        #endregion

        #region get Supplier Data For Alert
        /// <summary>
        /// get Supplier Data For Alert
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="poId"></param>
        /// <returns></returns>
        public void getSupplierDataForAlert(ref DataSet resultDataSet, int poId)
        {
            objVoidInspection.getSupplierDataForAlert(ref resultDataSet, poId);
        }
        #endregion

        #region update Tenant To Complete
        /// <summary>
        /// update Tenant To Complete
        /// </summary>
        /// <param name="worksRequiredId"></param>
        /// <param name="checkedValue"></param>
        public string updateTenantToComplete(int worksRequiredId, bool checkedValue)
        {

            return objVoidInspection.updateTenantToComplete(worksRequiredId, checkedValue);
        }
        #endregion

        #region add Tenant Recharges
        /// <summary>
        /// add Tenant Recharges
        /// </summary>
        /// <param name="worksRequiredId"></param>
        /// <param name="checkedValue"></param>
        public string addTenantRecharges(int worksRequiredId, bool checkedValue, Decimal tenantNeglect)
        {

            return objVoidInspection.addTenantRecharges(worksRequiredId, checkedValue, tenantNeglect);
        }
        #endregion

        #region get Void Area List
        /// <summary>
        /// get Void Area List
        /// </summary>
        /// <param name="resultDataSet"></param>
        public void getVoidAreaList(ref DataSet resultDataSet)
        {
            objVoidInspection.getVoidAreaList(ref resultDataSet);
        }
        #endregion

        #region update Required Works
        /// <summary>
        /// update Required Works
        /// </summary>
        /// <param name="requiredWorksBO"></param>
        public string updateRequiredWorks(RequiredWorksBO requiredWorksBO)
        {

            return objVoidInspection.updateRequiredWorks(requiredWorksBO);
        }
        #endregion

        #region "Get Tenant(s) Info by TenancyId - Joint Tenancy"

        //To implement joint tenants information.

        public void GetJointTenantsInfoByTenancyID(ref DataSet dstenantsInfo, ref int tenancyID)
        {
            objVoidInspection.GetJointTenantsInfoByTenancyID(ref  dstenantsInfo, ref  tenancyID);

        }

        #endregion

        #region get Checks To Be Arranged
        /// <summary>
        /// get Checks To Be Arranged
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        public Int32 getGasElectricChecksToBeArranged(ref DataSet resultDataSet, PageSortBO objPageSortBO, CommonSearchBO objCommonSearchBo,int patchNum ,bool getOnlyCount = false)
        {
            return objVoidInspection.getGasElectricChecksToBeArranged(ref resultDataSet, objPageSortBO, objCommonSearchBo, patchNum, getOnlyCount);
        }
        #endregion

        #region get Checks  Arranged
        /// <summary>
        /// get Checks Arranged
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        public Int32 getGasElectricChecksArranged(ref DataSet resultDataSet, PageSortBO objPageSortBO, CommonSearchBO objCommonSearchBo,int patchNum, bool getOnlyCount = false)
        {
            return objVoidInspection.getGasElectricChecksArranged(ref resultDataSet, objPageSortBO, objCommonSearchBo,patchNum, getOnlyCount);
        }
        #endregion

        #region get Post Void Inspection To Be Arranged List
        /// <summary>
        /// get Post Void Inspection To Be Arranged List
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        public int getPostVoidInspectionToBeArrangedList(ref DataSet resultDataSet, PageSortBO objPageSortBO, string searchText,int patch, bool getOnlyCount = false)
        {
            return objVoidInspection.getPostVoidInspectionToBeArrangedList(ref resultDataSet, objPageSortBO, searchText,patch , getOnlyCount);
               
        }
        #endregion

        #region get Post Void Inspection Arranged List
        /// <summary>
        /// get Post Void Inspection Arranged List
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        public int getPostVoidInspectionArrangedList(ref DataSet resultDataSet, PageSortBO objPageSortBO, string searchText,int patch, bool getOnlyCount = false)
        {
            return objVoidInspection.getPostVoidInspectionArrangedList(ref resultDataSet, objPageSortBO, searchText,patch, getOnlyCount);
        }
        #endregion

        #region get Pending Termination
        /// <summary>
        /// get Pending Termination
        /// </summary>
        /// <param name="schemeId"></param>
        /// <returns></returns>
        public int getPendingTermination(ref DataSet resultDataSet, PageSortBO objPageSortBO, string searchText, bool getOnlyCount = false)
        {
            return objVoidInspection.getPendingTermination(ref resultDataSet, objPageSortBO, searchText, getOnlyCount);
        }
        #endregion

        #region get Relet Alert Count
        /// <summary>
        /// get Relet Alert Count
        /// </summary>
        /// <param name="schemeId"></param>
        /// <returns></returns>
        public int getReletAlertCount(ref DataSet resultDataSet, PageSortBO objPageSortBO, string searchText, bool getOnlyCount = false)
        {
            return objVoidInspection.getReletAlertCount(ref resultDataSet,objPageSortBO,searchText,getOnlyCount);
        }

        #endregion

        #region get No entry Count and List
        /// <summary>
        /// get Count no entry
        /// </summary>
        /// <param name="schemeId"></param>
        /// <returns></returns>
        public int getNoEntryCountAndList(ref DataSet resultDataSet, PageSortBO objPageSortBO, string searchText, bool getOnlyCount = false)
        {
            return objVoidInspection.getNoEntryCountAndList(ref resultDataSet, objPageSortBO, searchText, getOnlyCount);
        }
        #endregion

        #region get Available Properties
        /// <summary>
        /// get Available Properties
        /// </summary>
        /// <param name="schemeId"></param>
        /// <returns></returns>
        public int getAvailableProperties(ref DataSet resultDataSet, PageSortBO objPageSortBO, string searchText, bool getOnlyCount = false)
        {
            return objVoidInspection.getAvailableProperties(ref resultDataSet, objPageSortBO, searchText, getOnlyCount);
        }
        #endregion

        public DataSet getPropertyInfoByJournalId(int journalId)
        {
            return objVoidInspection.getPropertyInfoByJournalId(journalId);
        }

        #region get Void Works Arranged
        /// <summary>
        /// get Void Works To Be Arranged
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <param name="getOnlyCount"></param>
        /// <returns></returns>
        public int getVoidWorksArranged(ref DataSet resultDataSet, PageSortBO objPageSortBO, string searchText,int patchNum, bool getOnlyCount = false)
        {
            return objVoidInspection.getVoidWorksArranged(ref resultDataSet, objPageSortBO, searchText,patchNum, getOnlyCount);
        }

        #endregion

        #region get patch data
        #endregion

        public void getPatchData(ref DataSet resultDataSet)
        {
            objVoidInspection.getPatchData(ref resultDataSet);
        }
    }
}
