﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PDR_DataAccess.CyclicalServices;
using System.Data;
using PDR_BusinessObject.CyclicalServices;
using PDR_BusinessObject.Expenditure;
using PDR_BusinessLogic.AssignToContractor;
using PDR_DataAccess.AssignToContractor;
using PDR_BusinessObject.Vat;
using PDR_Utilities.Helpers;
using PDR_Utilities.Constants;
using System.IO;
using System.Web;
using System.Net.Mail;
using System.Net.Mime;

namespace PDR_BusinessLogic.CyclicalServices
{
    public class CyclicalServicesBL : Base.BaseBL
    {
        ICyclicalServicesRepo objService;
        public CyclicalServicesBL(ICyclicalServicesRepo obj)
        {
            objService = obj;
        }

        public int getServicesToBeAllocated(ref DataSet resultDataSet, string searchText, int offset, int limit, string sortColumn, string sortOrder, int schemeId, int attributeId)
        {
            return objService.getServicesToBeAllocated(ref resultDataSet, searchText, offset, limit, sortColumn, sortOrder, schemeId, attributeId);
        }


        public int getServicesAllocated(ref DataSet resultDataSet, string searchText, int offset, int limit, string sortColumn, string sortOrder)
        {
            return objService.getServicesAllocated(ref resultDataSet, searchText, offset, limit, sortColumn, sortOrder);
        }
        public void GetCostCentreDropDownVales(ref DataSet resultDataSet)
        {

            objService.getCostCentreDropDownVales(ref  resultDataSet);
        }

        public void GetBudgetHeadDropDownValuesByCostCentreId(ref DataSet resultDataSet, int CostCentreId)
        {

            objService.getBudgetHeadDropDownValuesByCostCentreId(ref resultDataSet, CostCentreId);
        }
        public void GetContactDropDownValuesbyContractorId(ref DataSet resultDataSet, int contractorId)
        {

            objService.getContactDropDownValuesbyContractorId(ref resultDataSet, contractorId);
        }

        public void getServiceItemDetails(ref DataSet resultDataSet, string serviceItemIds)
        {
            objService.getServiceItemDetails(ref resultDataSet, serviceItemIds);
        }

        public void UpdateCycleValue(double cycleValue, int serviceItemId)
        {
            objService.updateCycleValue(cycleValue, serviceItemId);
        }

        public void GeneratePo(GeneratePoRequestBO request)
        {
            List<ContractorWorkBO> contractorWorkList = new List<ContractorWorkBO>();
            List<ContractorWorkDetailBO> contractorWorkDetailList = new List<ContractorWorkDetailBO>();
            foreach (RequiredWorkDetail item in request.requiredWorks)
            {
                ContractorWorkBO contractorWorkObj = new ContractorWorkBO();
                AttributeDetail attributeObj = new AttributeDetail();
                attributeObj = request.attribute.Find(x => x.ServiceItemId == item.ServiceItemId);
                //get purchase order and Purchase Item status
                int poStatus = getPoStatus(Convert.ToInt32(request.expenditureId), Convert.ToDecimal(item.Gross), Convert.ToInt32(request.headId), Convert.ToInt32(request.assignedBy));
                contractorWorkObj.AssignedBy = Convert.ToInt32(request.assignedBy);
                contractorWorkObj.AssignedDate = DateTime.Now;
                contractorWorkObj.Commence = Convert.ToDateTime(attributeObj.Commence);
                if (request.contactId != null || request.contactId != "")
                contractorWorkObj.ContactId = Convert.ToInt32(request.contactId);
                
                contractorWorkObj.ContractGrossValue = Convert.ToDecimal(item.Gross);
                contractorWorkObj.ContractNetValue = Convert.ToDecimal(item.Net);
                contractorWorkObj.ContractorId = Convert.ToInt32(request.contractorId);
                contractorWorkObj.ExpenditureId = Convert.ToInt32(request.expenditureId);
                contractorWorkObj.POStatus = poStatus;
                contractorWorkObj.RequiredWorks = request.moreDetail;
                contractorWorkObj.SendApprovalLink = Convert.ToBoolean(request.sendApprovalLink);
                contractorWorkObj.ServiceItemId = Convert.ToInt32(item.ServiceItemId);
                contractorWorkObj.Vat = Convert.ToDecimal(item.Vat);
                contractorWorkObj.VatId = Convert.ToInt32(request.vatId);
                contractorWorkList.Add(contractorWorkObj);
                contractorWorkDetailList.AddRange(populateContractorWorkDetailBo(attributeObj, item, Convert.ToInt32(request.vatId)));

            }

            bool isSaved = objService.generatePo(GeneralHelper.convertToDataTable(contractorWorkList), GeneralHelper.convertToDataTable(contractorWorkDetailList));
            if (isSaved && request.sendApprovalLink == "true")
            {

                int index = 1;
                int totalPo = request.requiredWorks.Count;
                foreach (RequiredWorkDetail item in request.requiredWorks)
                {
                    int poStatus = (int)contractorWorkList.Find(x => x.ServiceItemId == Convert.ToInt32(item.ServiceItemId)).POStatus;
                    // int poStatus = poStatusData.
                    //getPoStatus(Convert.ToInt32(request.expenditureId), Convert.ToDecimal(item.Gross), Convert.ToInt32(request.headId), Convert.ToInt32(request.assignedBy));
                    if (poStatus > 0)
                    {
                        sendEmailtoContractor(Convert.ToInt32(request.contactId), Convert.ToInt32(item.ServiceItemId), index, totalPo);
                    }
                    index++;
                }
            }
        }

        #region "Get Pi Status (Pending Status)"

        private int getPoStatus(int expenditureId, decimal gross, int budgetHeadId, int employeeId)
        {
            int pIStatus = 3;
            // direct = true;
            // 3 = "Work Ordered" in table F_POSTATUS
            List<ExpenditureBO> expenditureBoList = new List<ExpenditureBO>();
            AssignToContractorBL objAssignToContractorBL = new AssignToContractorBL(new AssignToContractorRepo());
            objAssignToContractorBL.GetExpenditureDropDownValuesByBudgetHeadId(ref expenditureBoList, ref budgetHeadId, ref employeeId);

            // List<ExpenditureBO> expenditureBoList = objSession.ExpenditureBOList;

            //Check if an item is costing 0 (zero) or less then it should not go to queued list.
            if (gross > 0)
            {
                ExpenditureBO ExpenditureBo = expenditureBoList.Find(i => i.Id == expenditureId);

                if ((ExpenditureBo != null))
                {
                    if ((gross > ExpenditureBo.Limit || gross > ExpenditureBo.Remaining))
                    {
                        pIStatus = 0;
                        //  direct = false;
                        // 0 = "Queued" in table F_POSTATUS
                    }
                }
            }

            return pIStatus;
        }

        #endregion

        private List<ContractorWorkDetailBO> populateContractorWorkDetailBo(AttributeDetail attribute, RequiredWorkDetail item, int vatId)
        {
            List<ContractorWorkDetailBO> contractorWorkDetailList = new List<ContractorWorkDetailBO>();

            DateTime commenceDate = Convert.ToDateTime(attribute.Commence);
            int cycles = Convert.ToInt32(attribute.Cycles);
            int cycleDays = Convert.ToInt32(attribute.CycleDays);
            decimal CycleValue = Convert.ToDecimal(attribute.CycleValue);
            string cycleType = attribute.CycleType;
            int cyclePeriod = attribute.cyclePeriod;

            for (int i = 1; i <= cycles; i++)
            {
                ContractorWorkDetailBO contractorWorkDetailBo = new ContractorWorkDetailBO();
                // TODO: apply checks for weeeks, months and years, to add in commnce date
                
                if (cycleType.Equals("Month(s)"))
                {
                    if (i == 1)
                    {
                        contractorWorkDetailBo.CycleDate = commenceDate;
                    }
                    else
                    {
                        contractorWorkDetailBo.CycleDate = Convert.ToDateTime(commenceDate.AddMonths(cyclePeriod));
                        commenceDate = Convert.ToDateTime(commenceDate.AddMonths(cyclePeriod));
                    }
                    
                }
                else 
                {
                    if (i == 1)
                    {
                        contractorWorkDetailBo.CycleDate = commenceDate;
                    }
                    else
                    {
                        contractorWorkDetailBo.CycleDate = Convert.ToDateTime(commenceDate.AddDays(cycleDays));
                        commenceDate = Convert.ToDateTime(commenceDate.AddDays(cycleDays));
                    }
                }
                //contractorWorkDetailBo.CycleDate = Convert.ToDateTime(commenceDate.AddDays(cycleDays));
                //commenceDate = Convert.ToDateTime(commenceDate.AddDays(cycleDays));
                contractorWorkDetailBo.NetCost = CycleValue;
                decimal vat = 0.0M, gross = 0.0M;
                AddVAT(vatId, CycleValue, ref vat, ref gross);
                contractorWorkDetailBo.VatId = vatId;
                contractorWorkDetailBo.VAT = vat;
                contractorWorkDetailBo.GrossCost = gross;
                contractorWorkDetailBo.ServiceItemId = Convert.ToInt32(attribute.ServiceItemId);
                contractorWorkDetailBo.WorksRequired = item.worksRequired;
                contractorWorkDetailList.Add(contractorWorkDetailBo);
            }

            return contractorWorkDetailList;
        }


        private void AddVAT(int vatId, decimal netCost, ref decimal vat, ref decimal gross)
        {
            List<VatBo> vatBoList = new List<VatBo>();
            AssignToContractorBL objAssignToContractorBL = new AssignToContractorBL(new AssignToContractorRepo());

            objAssignToContractorBL.getVatDropDownValues(ref vatBoList);

            decimal VatRate = 0.0M;

            if (vatId >= 0)
            {
                VatRate = getVatRateByVatId(vatId, vatBoList);
                vat = netCost * VatRate / 100;
                gross = netCost + vat;
            }
            else
            {
                vat = 0.0M;
                gross = netCost;
            }

        }
        #region "Get Vat Rate by VatId"

        private decimal getVatRateByVatId(int vatRateId, List<VatBo> vatBoList)
        {
            decimal vatRate = 0.0M;
            VatBo vatBo = vatBoList.Find(i => i.Id == vatRateId);

            if ((vatBo != null))
            {
                vatRate = vatBo.VatRate;
            }

            return vatRate;
        }

        #endregion


        #region "Populate Body and Send Email to Contractor"

        public void sendEmailtoContractor(int empolyeeId, int serviceItemId, int index, int totalPo)
        {
            DataSet detailsForEmailDS = new DataSet();
            detailsForEmailDS = objService.getdetailsForEmail(empolyeeId, serviceItemId);

            if (detailsForEmailDS != null)
            {

                if ((detailsForEmailDS.Tables[ApplicationConstants.ContractorDetailsDt] != null) && detailsForEmailDS.Tables[ApplicationConstants.ContractorDetailsDt].Rows.Count > 0)
                {
                    string email = Convert.ToString(detailsForEmailDS.Tables[ApplicationConstants.ContractorDetailsDt].Rows[0]["Email"]);
                    if (string.IsNullOrEmpty(email) || !GeneralHelper.isEmail(email))
                    {
                        throw new Exception("Unable to send email, invalid email address.");
                    }
                    else
                    {
                        StringBuilder body = new StringBuilder();
                        StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Email/CyclicalWorkToContractor.html"));
                        body.Append(reader.ReadToEnd());

                        // Set contractor detail(s) '
                        //==========================================='
                        //Populate Contractor Contact Name
                        body.Replace("{ContractorContactName}", Convert.ToString(detailsForEmailDS.Tables[ApplicationConstants.ContractorDetailsDt].Rows[0]["ContractorContactName"]));
                        //==========================================='
                        body.Replace("{Orderby}", Convert.ToString(detailsForEmailDS.Tables[ApplicationConstants.PurchaseOrderDetailsDt].Rows[0]["OrderedBy"]));
                        body.Replace("{PONumber}", Convert.ToString(detailsForEmailDS.Tables[ApplicationConstants.PurchaseOrderDetailsDt].Rows[0]["PurchaseOrderId"]));
                        body.Replace("{DDDial}", Convert.ToString(detailsForEmailDS.Tables[ApplicationConstants.PurchaseOrderDetailsDt].Rows[0]["DD"]));
                        body.Replace("{Email}", Convert.ToString(detailsForEmailDS.Tables[ApplicationConstants.PurchaseOrderDetailsDt].Rows[0]["Email"]));
                        body.Replace("{Scheme}", Convert.ToString(detailsForEmailDS.Tables[ApplicationConstants.PurchaseOrderDetailsDt].Rows[0]["SchemeName"]));
                        body.Replace("{Block}", Convert.ToString(detailsForEmailDS.Tables[ApplicationConstants.PurchaseOrderDetailsDt].Rows[0]["BlockName"]));
                        body.Replace("{ServiceRequired}", Convert.ToString(detailsForEmailDS.Tables[ApplicationConstants.PurchaseOrderDetailsDt].Rows[0]["ServiceRequired"]));
                        body.Replace("{Cycle}", Convert.ToString(detailsForEmailDS.Tables[ApplicationConstants.PurchaseOrderDetailsDt].Rows[0]["Cycle"]));
                        body.Replace("{Commencement}", Convert.ToString(detailsForEmailDS.Tables[ApplicationConstants.PurchaseOrderDetailsDt].Rows[0]["Commencement"]));
                        body.Replace("{CycleValue}", Convert.ToString(detailsForEmailDS.Tables[ApplicationConstants.PurchaseOrderDetailsDt].Rows[0]["CycleValue"]));
                        body.Replace("{NetCost}", Convert.ToString(detailsForEmailDS.Tables[ApplicationConstants.PurchaseOrderDetailsDt].Rows[0]["NetCost"]));
                        body.Replace("{VAT}", Convert.ToString(detailsForEmailDS.Tables[ApplicationConstants.PurchaseOrderDetailsDt].Rows[0]["vat"]));
                        body.Replace("{TOTAL}", Convert.ToString(detailsForEmailDS.Tables[ApplicationConstants.PurchaseOrderDetailsDt].Rows[0]["GrossCost"]));
                        StringBuilder sb = new StringBuilder();
                        sb.Append("<table>");
                        int rows = 0;
                        int firstColumnDataCounter = 0;
                        int secondColumnDataCounter = 1;
                        int totalData = detailsForEmailDS.Tables[ApplicationConstants.PurchaseItemDetailsDt].Rows.Count;
                        DataTable dt = detailsForEmailDS.Tables[ApplicationConstants.PurchaseItemDetailsDt];
                        if (totalData % 2 == 0)
                            rows = totalData / 2;

                        else rows = (totalData / 2) + 1;
                        for (int i = 0; i < rows; i++)
                        {
                            if (totalData % 2 == 0)
                            {
                                int dataSecondColumn = (totalData / 2) + firstColumnDataCounter;
                                sb.Append("<tr><td>" + dt.Rows[i]["row"].ToString().PadLeft(dt.Rows[i]["row"].ToString().Length + 2, '0') + "</td><td style='padding-left:7px;'>" + dt.Rows[i]["CycleDate"] + "</td><td style='padding-left:7px;'>&nbsp;</td><td style='padding-left:7px;'> " + dt.Rows[dataSecondColumn]["row"].ToString().PadLeft(dt.Rows[dataSecondColumn]["row"].ToString().Length + 2, '0') + "</td><td style='padding-left:7px;'>" + dt.Rows[dataSecondColumn]["CycleDate"] + "</td></tr>");
                                firstColumnDataCounter++;
                            }
                            else
                            {
                                int dataSecondColumn = ((totalData - 1) / 2) + secondColumnDataCounter;
                                if (i == rows - 1)
                                {
                                    sb.Append("<tr><td>" + dt.Rows[i]["row"].ToString().PadLeft(dt.Rows[i]["row"].ToString().Length + 2, '0') + "</td><td style='padding-left:7px;'>" + dt.Rows[i]["CycleDate"] + "</td><td style='padding-left:7px;'>&nbsp;</td></tr>");
                                }
                                else
                                {
                                    sb.Append("<tr><td>" + dt.Rows[i]["row"].ToString().PadLeft(dt.Rows[i]["row"].ToString().Length + 2, '0') + "</td><td style='padding-left:7px;'>" + dt.Rows[i]["CycleDate"] + "</td><td style='padding-left:7px;'>&nbsp;</td><td style='padding-left:7px;'> " + dt.Rows[dataSecondColumn]["row"].ToString().PadLeft(dt.Rows[dataSecondColumn]["row"].ToString().Length + 2, '0') + "</td><td style='padding-left:7px;'>" + dt.Rows[dataSecondColumn]["CycleDate"] + "</td></tr>");

                                }

                                secondColumnDataCounter++;
                            }
                        }


                        sb.Append("</table>");
                        body.Replace("{CycleDates}", sb.ToString());

                        string acceptUrl;
                        acceptUrl = "https://" + HttpContext.Current.Request.ServerVariables["server_name"] + "/PropertyDataRestructure/Views/CyclicalServices/AcceptCyclicalPurchaseOrder.aspx?oid=" + Convert.ToString(detailsForEmailDS.Tables[ApplicationConstants.PurchaseOrderDetailsDt].Rows[0]["PurchaseOrderId"]) + "&uid=" + empolyeeId.ToString();
                        body.Replace("{#}", acceptUrl);
                        body.Replace("{accept}", "Please follow this link to accept the PO:");

                        //===========================================
                        // Attach logo images with email
                        //===========================================
                        LinkedResource logo50Years = new LinkedResource(HttpContext.Current.Server.MapPath("~/Images/50_Years.gif"));
                        logo50Years.ContentId = "logo50Years_Id";

                        body = body.Replace("{Logo_50_years}", string.Format("cid:{0}", logo50Years.ContentId));

                        LinkedResource logoBroadLandRepairs = new LinkedResource(HttpContext.Current.Server.MapPath("~/Images/Broadland-Housing-Association.gif"));
                        logoBroadLandRepairs.ContentId = "logoBroadLandRepairs_Id";

                        body = body.Replace("{Logo_Broadland-Housing-Association}", string.Format("cid:{0}", logoBroadLandRepairs.ContentId));

                        ContentType mimeType = new ContentType("text/html");

                        AlternateView alternatevw = AlternateView.CreateAlternateViewFromString(body.ToString(), mimeType);
                        alternatevw.LinkedResources.Add(logo50Years);
                        alternatevw.LinkedResources.Add(logoBroadLandRepairs);
                        //==========================================='

                        MailMessage mailMessage = new MailMessage();

                        mailMessage.Subject = ApplicationConstants.CyclicalEmailSubject + index.ToString() + " of " + totalPo.ToString();
                        mailMessage.To.Add(new MailAddress(detailsForEmailDS.Tables[ApplicationConstants.ContractorDetailsDt].Rows[0]["Email"].ToString(), detailsForEmailDS.Tables[ApplicationConstants.ContractorDetailsDt].Rows[0]["ContractorContactName"].ToString()));
                        // For a graphical view with logos, alternative view will be visible, body is attached for text view.
                        mailMessage.Body = body.ToString();
                        mailMessage.AlternateViews.Add(alternatevw);
                        mailMessage.IsBodyHtml = true;

                        EmailHelper.sendEmail(ref mailMessage);

                    }
                }
                else
                {
                    throw new Exception("Unable to send email, contractor details not available");
                }
            }
        }

        #endregion

        public string getPoStatusForSessionCreation(int orderId)
        {
            return objService.getPoStatusForSessionCreation(orderId);
        }

        public DataSet getPurchaseOrderContractorDetails(int orderId)
        {
            return objService.getPurchaseOrderContractorDetails(orderId);
        }

        public bool acceptCyclicalPurchaseOrder(int orderId, string status, ref string contactName, ref string email)
        {
            return objService.acceptCyclicalPurchaseOrder(orderId, status, ref contactName, ref email);
        }

        public bool updateWorksCompleted(UpdateWorksCompletedBO request, int userId)
        {
            return objService.updateWorksCompleted(request, userId);
        }
        public bool updateInvoiceUpload(UpdateWorksCompletedBO request, int userId)
        {
            return objService.updateInvoiceUpload(request, userId);
        }

        public int getCyclicalTab(ref DataSet resultDataSet, string searchText, int offset, int limit, string sortColumn, string sortOrder, int schemeId, int blockId)
        {
            return objService.getCyclicalTab(ref resultDataSet, searchText, offset, limit, sortColumn, sortOrder, schemeId, blockId);
        }
        public int getUnassignedServices(ref DataSet resultDataSet, string searchText, int offset, int limit, string sortColumn, string sortOrder, int getOnlyCount, int schemeId)
        {
            return objService.getUnassignedServices(ref resultDataSet, searchText, offset, limit, sortColumn, sortOrder, getOnlyCount, schemeId);
        }
        public bool cancelCyclicalWork(int orderId, string reason, string notes, int cancelBy, string transactionIdentity)
        {
            return objService.cancelCyclicalWork(orderId, reason, notes, cancelBy, transactionIdentity);
        }
        public int getCancelledOrder(ref DataSet resultDataSet, string searchText, int offset, int limit, string sortColumn, string sortOrder)
        {
            return objService.getCancelledOrder(ref resultDataSet, searchText, offset, limit, sortColumn, sortOrder);
        }
    }
}
