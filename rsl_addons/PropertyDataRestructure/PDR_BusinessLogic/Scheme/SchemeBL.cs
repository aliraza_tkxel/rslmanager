﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PDR_BusinessLogic.Base;
using System.Data;
using PDR_DataAccess;
using PDR_BusinessObject;
using PDR_DataAccess.Scheme;
using PDR_BusinessObject.PageSort;
using PDR_BusinessObject.Scheme;
using PDR_BusinessObject.Restriction;

namespace PDR_BusinessLogic.Scheme
{
    public class SchemeBL
    {
        ISchemeRepo objSchemeRepo;
        public SchemeBL(ISchemeRepo sRepo)
        {
            objSchemeRepo = sRepo;
        }

        #region get Scheme List
        /// <summary>
        /// get Scheme List
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        public Int32 getSchemeList(ref DataSet resultDataSet, PageSortBO objPageSortBO, String searchText)
        {
            return objSchemeRepo.getSchemeList(ref resultDataSet, objPageSortBO, searchText);
        }
        #endregion

        #region populate Scheme DropDown
        /// <summary>
        /// populate Scheme DropDown
        /// </summary>
        /// <returns></returns>
        public DataSet populateSchemeDropDown()
        {

            return objSchemeRepo.populateSchemeDropDown();
        }
        #endregion

        #region populate Block DropDown by Scheme Id
        /// <summary>
        /// populate Scheme DropDown
        /// </summary>
        /// <returns></returns>
        public DataSet populateBlockDropDown(int schemeId)
        {

            return objSchemeRepo.populateBlockDropDown(schemeId);
        }
        #endregion


        #region get scheme reactive repairs
        /// <summary>
        /// get scheme reactive repairs
        /// </summary>
        /// <param name="schemeId"></param>
        /// <returns></returns>
        public DataSet getSchemeFaultsAndDefects(int schemeId)
        {            
            return objSchemeRepo.getSchemeFaultsAndDefects(schemeId);
        }
        #endregion

        #region get scheme Planned Appointment
        /// <summary>
        /// get scheme Planned Appointment
        /// </summary>
        /// <param name="schemeId"></param>
        /// <returns></returns>
        public DataSet getSchemePlannedAppointment(int schemeId)
        {
            return objSchemeRepo.getSchemePlannedAppointment(schemeId);
        }
        #endregion

        #region Get Reported Faults/Defects For Scheme/Block
        /// <summary>
        /// Get Reported Faults/Defects For Scheme/Block
        /// </summary>
        /// <param name="schemeId"></param>
        /// <param name="resultDataSet"></param>
        /// <returns></returns>
        public void GetReportedFaultsDefectsForSchemeBlock(ref DataSet resultDataSet, int schemeId)
        {
             objSchemeRepo.GetReportedFaultsDefectsForSchemeBlock(ref resultDataSet, schemeId);
        }
        #endregion

        #region get blocks by schemeId
        /// <summary>
        /// get blocks by schemeId
        /// </summary>
        /// <param name="schemeId"></param>
        /// <param name="totalCount"></param>
        /// <returns></returns>
        public DataSet getBlocksBySchemeID(int schemeId, ref int totalCount)
        {
            return objSchemeRepo.getBlocksBySchemeID(schemeId, ref totalCount);
        }
        #endregion

        #region get Scheme ASB Count
        /// <summary>
        /// get Scheme ASB Count
        /// </summary>
        /// <param name="schemeId"></param>
        /// <param name="totalCount"></param>
        /// <returns></returns>
        public DataSet getSchemeASBCount(int schemeId)
        {
            return objSchemeRepo.getSchemeASBCount(schemeId);
        }
        #endregion

        #region get Scheme Repairs Count
        /// <summary>
        /// get Scheme Repairs Count
        /// </summary>
        /// <param name="schemeId"></param>
        /// <param name="totalCount"></param>
        /// <returns></returns>
        public DataSet getSchemeRepairsCount(int schemeId)
        {
            return objSchemeRepo.getSchemeRepairsCount(schemeId);
        }
        #endregion

        #region get Scheme Repairs Count
        /// <summary>
        /// get Scheme Repairs Count
        /// </summary>
        /// <param name="schemeId"></param>
        /// <param name="totalCount"></param>
        /// <returns></returns>
        public DataSet getSchemeDetailBySchemeId(int schemeId)
        {
            return objSchemeRepo.getSchemeDetailBySchemeId(schemeId);
        }
        #endregion

        #region "get Blocks & Properties by PhaseID"
        /// <summary>
        /// Get Blocks and Properties by PhaseId
        /// </summary>
        /// <param name="blockPropertiesDataSet"></param>
        /// <param name="phaseId"></param>
        public void getBlocksPropertiesbyPhaseID(ref DataSet blockPropertiesDataSet, int phaseId, int developmentId)
        {
            objSchemeRepo.getBlocksPropertiesbyPhaseID(ref blockPropertiesDataSet, phaseId, developmentId);
        }
        #endregion

        public string saveScheme(ref SaveSchemeBO objSchemeBO)
        {
          return  objSchemeRepo.saveScheme(ref objSchemeBO);
        }

        public void getSchemeData(ref DataSet schemeDataSet, int schemeId)
        {
            objSchemeRepo.getSchemeData(ref schemeDataSet, schemeId);
        }

        public bool SaveRestriction(ref RestrictionBO objRestrictionBO)
        {
            return objSchemeRepo.SaveRestriction(ref objRestrictionBO);
        }

        public void getRestrictions(ref DataSet dataSet, int schemeId)
        {
            objSchemeRepo.getRestrictions(ref dataSet, schemeId);
        }

        public void getLandRegistrationOptions(ref DataSet dataSet)
        {
            objSchemeRepo.getLandRegistrationOptions(ref dataSet);
        }

    }
}

