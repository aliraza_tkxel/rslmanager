﻿using PDR_BusinessLogic.Base;
using System.Data;
using PDR_BusinessObject.Reports;
using PDR_DataAccess.Reports;
using PDR_BusinessObject.PageSort;

namespace PDR_BusinessLogic.Reports
{
    public class ReportsBL : BaseBL
    {
        IReportsRepo objReportsRepo;
        public ReportsBL(IReportsRepo rRepo)
        {
            objReportsRepo = rRepo;
        }
      
        #region get Professional  Analysis Report
        /// <summary>
        /// get Professional  Analysis Report
        /// </summary>
        /// <param name="objReportsBO"></param>
        /// <returns></returns>
        public DataSet getProfessionalAnalysisReport(ReportsBO objReportsBO)
        {
            return objReportsRepo.getProfessionalAnalysisReport(objReportsBO);
        }

        #endregion

        #region get Professional Analysis Report Single Asset Type
        /// <summary>
        /// get Professional Analysis Report Single Asset Type
        /// </summary>
        /// <param name="objReportsBO"></param>
        /// <returns></returns>
        public DataSet getProfessionalAnalysisReportSingleAssetType(ReportsBO objReportsBO)
        {
            return objReportsRepo.getProfessionalAnalysisReportSingleAssetType(objReportsBO);
        }

        #endregion

        #region get Professional Analysis Report Single Asset Type With Occupancy
        /// <summary>
        /// get Professional Analysis Report Single Asset Type With Occupancy
        /// </summary>
        /// <param name="objReportsBO"></param>
        /// <returns></returns>
        public DataSet AnalysisReportWithOccupancy(ReportsBO objReportsBO)
        {
            return objReportsRepo.getProfessionalAnalysisReportWithOccupancy(objReportsBO);
        }

        #endregion

        #region get Professional Analysis Report Single Asset Type With out Occupancy
        /// <summary>
        /// get Professional Analysis Report Single Asset Type With Occupancy
        /// </summary>
        /// <param name="objReportsBO"></param>
        /// <returns></returns>
        public DataSet AnalysisReportWithOutOccupancy(ReportsBO objReportsBO)
        {
            return objReportsRepo.getProfessionalAnalysisReportWithOutOccupancy(objReportsBO);
        }

        #endregion

        #region get Professional  Analysis Report Detail
        /// <summary>
        /// get Professional  Analysis Report Detail
        /// </summary>
        /// <param name="objReportsBO"></param>
        /// <returns></returns>
        public DataSet getProfessionalAnalysisReportDetail(ReportsBO objReportsBO, PageSortBO objPageSortBo, ref int totalCount)
        {
            return objReportsRepo.getProfessionalAnalysisReportDetail(objReportsBO, objPageSortBo, ref totalCount);
        }

        #endregion

        #region get Patch
        public DataSet getPatch()
        {
            return objReportsRepo.getPatch();
        }
        #endregion

        #region get Local Authority
        public DataSet getLocalAuthority()
        {
            return objReportsRepo.getLocalAuthority();
        }
        #endregion

        #region getDevelopmentByPatchIdAndLAId

        public DataSet getDevelopmentByPatchIdAndLAId(int? patchId, int? LAId)
        {
            return objReportsRepo.getDevelopmentByPatchIdAndLAId(patchId, LAId);
        }

        #endregion

        #region get Professional Analysis Report Detail for print
        /// <summary>
        /// get Professional Analysis Report Detail for print
        /// </summary>
        /// <param name="objReportsBOrtsBo"></param>
        /// <returns>resultDataSet</returns>
        public DataSet getAnalysisReportDetailForPrint(ReportsBO objReportsBO)
        {
            return objReportsRepo.getAnalysisReportDetailForPrint(objReportsBO);
        }

        #endregion

        #region get Void Analysis Report
        /// <summary>
        /// get Professional Analysis Report Single Asset Type
        /// </summary>
        /// <param name="objReportsBO"></param>
        /// <returns></returns>
        public DataSet getVoidAnalysisReport(ReportsBO objReportsBO)
        {
            return objReportsRepo.getVoidAnalysisReport(objReportsBO);
        }

        #endregion

        #region get CP12 Status Report
        /// <summary>
        /// get CP12 Status Report 
        /// </summary>
        /// <param name="objReportsBO"></param>
        /// <returns></returns>
        public int getCP12StatusReportReport(ref DataSet resultDataSet, PageSortBO objPageSortBO, string propertyStatus, string searchText)
        {
            return objReportsRepo.getCP12StatusReport(ref resultDataSet, objPageSortBO, propertyStatus, searchText);
        }

        #endregion
        public void getCategories(ref DataSet resultSet, string reportforType = "")
        {
            objReportsRepo.getCategories(ref resultSet, reportforType);
        }
        public void GetIncomeRecoveryOfficer(ref DataSet resultDataSet, string tenancyId)
        {
            objReportsRepo.GetIncomeRecoveryOfficer(ref resultDataSet, tenancyId);
        }

        public void GetCustomerInfo(ref DataSet resultDataSet, int tenancyId)
        {
            objReportsRepo.GetCustomerInfo(ref resultDataSet, tenancyId);
        }

        public void GetCustomerLastPayment(ref DataSet resultDataSet, string customerId, string tenancyId)
        {
            objReportsRepo.GetCustomerLastPayment(ref resultDataSet, customerId, tenancyId);
        }

        public void GetCustomerAccountData(ref DataSet resultDataSet, string tenancyId)
        {
            objReportsRepo.GetCustomerAccountData(ref resultDataSet, tenancyId);
        }
        
        public void GetCustomerAccountRecharge(ref DataSet resultDataSet, string tenancyId)
        {
            objReportsRepo.GetCustomerAccountRecharge(ref resultDataSet, tenancyId);
        }

        #region get Void  Analysis Report Detail
        /// <summary>
        /// get Void  Analysis Report Detail
        /// </summary>
        /// <param name="objReportsBO"></param>
        /// <returns></returns>
        public DataSet getVoidAnalysisReportDetail(ReportsBO objReportsBO, PageSortBO objPageSortBo, ref int totalCount)
        {
            return objReportsRepo.getVoidAnalysisReportDetail(objReportsBO, objPageSortBo, ref totalCount);
        }

        #endregion

        #region get Void  Analysis Report Detail for print
        /// <summary>
        /// get Void  Analysis Report Detail for print
        /// </summary>
        /// <param name="objReportsBO"></param>
        /// <returns></returns>
        public DataSet getVoidAnalysisReportDetailForPrint(ReportsBO objReportsBO)
        {
            return objReportsRepo.getVoidAnalysisReportDetailForPrint(objReportsBO);
        }

        #endregion
        #region get Terrior Report Data
        /// <summary>
        /// get Terrior Report Data
        /// </summary>
        /// <param name="objReportsBO"></param>
        /// <returns></returns>
        public DataSet getTerriorReportData(ReportsBO objReportsBO, PageSortBO objPageSortBo, ref int totalCount)
        {
            return objReportsRepo.getTerriorReportData(objReportsBO, objPageSortBo, ref totalCount);
        }

        public DataSet getTerriorReportDataForExcel(ReportsBO objReportsBO, PageSortBO objPageSortBo, ref int totalCount)
        {
            return objReportsRepo.getTerriorReportDataForExcel(objReportsBO, objPageSortBo, ref totalCount);
        }


        #endregion

        #region Save Excluded Properties
        /// <summary>
        /// Save Excluded Properties
        /// </summary>
        /// <param name="objReportsBO"></param>
        /// <returns></returns>
        public bool saveExcludedProperties(DataTable excludedPropDt, int schemeId, int orderItemId)
        {
            return objReportsRepo.saveExcludedProperties(excludedPropDt, schemeId, orderItemId);
        }

        #endregion

        #region Get Service Charge Scheme Blocks
        /// <summary>
        /// Get Service Charge Scheme Blocks
        /// </summary>
        /// <param name="objReportsBO"></param>
        /// <returns></returns>
        public DataSet getServiceChargeSchemeBlocks(int schemeId, int orderItemId)
        {
            return objReportsRepo.getServiceChargeSchemeBlocks(schemeId, orderItemId);
        }

        #endregion

        #region "Get Associated Schemes and Blocks"
        public DataSet getServiceChargeAssociatedSchemes(int itemId)
        {
            return objReportsRepo.getServiceChargeAssociatedSchemes(itemId);
        }
        #endregion

        #region "Get Service Charge Inc Exc Properties By Scheme"
        public DataSet getServiceChargeIncExcPropertiesByScheme(int schemeId, int orderItemId)
        {
            return objReportsRepo.getServiceChargeIncExcPropertiesByScheme(schemeId, orderItemId);
        }
        #endregion

        #region "Compliance Documents"

        /*Get All Employees who uploaded At Least one Document*/

        public DataSet getDocumentsEmployees()
        {
            ReportsRepo reportsRepo = new ReportsRepo();
            return reportsRepo.getDocumentsEmployees();

        }
        #endregion

        public int getDocuments(ref DataSet resultDataSet, PageSortBO objPageSortBO, string reportFor, string category, int type, int intTitle, string strTitle, string dateType, string fromDate, string toDate, string uploadedOrNot, string searchedText, bool getOnlyCount = false)
        {
            switch (reportFor)
            {
                case "Development":
                    return objReportsRepo.getDevDocuments(ref resultDataSet, objPageSortBO, category, type, strTitle, dateType, fromDate, toDate, uploadedOrNot, searchedText, getOnlyCount = false);

                case "Scheme":
                    return objReportsRepo.getSchemeDocuments(ref resultDataSet, objPageSortBO, category, type, intTitle, dateType, fromDate, toDate, uploadedOrNot, searchedText, getOnlyCount = false);

                case "Property":
                    return objReportsRepo.getPropDocuments(ref resultDataSet, objPageSortBO, category, type, intTitle, dateType, fromDate, toDate, uploadedOrNot, searchedText, getOnlyCount = false);

                default:
                    return 0;

            }

        }


        
    }
}
