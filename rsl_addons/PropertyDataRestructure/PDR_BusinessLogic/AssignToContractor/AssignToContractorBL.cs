﻿// -----------------------------------------------------------------------
// <copyright file="AssignToContractor.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace PDR_BusinessLogic.AssignToContractor
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using PDR_BusinessObject.DropDown;
    using PDR_BusinessObject.Expenditure;
    using PDR_BusinessObject.AssignToContractor;
    using PDR_DataAccess.AssignToContractor;
    using PDR_BusinessObject.Vat;
    using System.Data;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class AssignToContractorBL
    {

        IAssignToContractor objAssignToContractor;
        public AssignToContractorBL(IAssignToContractor objAssign)
        {
            objAssignToContractor = objAssign;
        }

        #region "Functions"


        #region "Get contact email details"
        /// <summary>
        /// Get contact email details
        /// </summary>
        /// <param name="contactId"></param>
        /// <param name="detailsForEmailDS"></param>
        /// <remarks></remarks>

        public void getContactEmailDetail(ref int contactId, ref DataSet detailsForEmailDS)
        {
            objAssignToContractor.getContactEmailDetail(ref contactId,ref detailsForEmailDS);
        }

        #endregion

        //=======================================================
        //Service provided by Telerik (www.telerik.com)
        //Conversion powered by NRefactory.
        //Twitter: @telerik
        //Facebook: facebook.com/telerik
        //=======================================================


        #region "Get Cost Centre Drop Down Values."
        public void GetCostCentreDropDownVales(ref List<DropDownBO> dropDownList)
        {
            objAssignToContractor.GetCostCentreDropDownVales(ref dropDownList);
        }
        #endregion

        #region "Get Budget Head Drop Down Values by Cost Centre Id"

        public void GetBudgetHeadDropDownValuesByCostCentreId(ref List<DropDownBO> dropDownList, int CostCentreId)
        {
            objAssignToContractor.GetBudgetHeadDropDownValuesByCostCentreId(ref dropDownList, CostCentreId);
        }

        #endregion

        #region "Get Expenditure Drop Down Values by Budget Head Id"

        public void GetExpenditureDropDownValuesByBudgetHeadId(ref List<ExpenditureBO> expenditureBOList, ref int BudgetHeadId, ref int EmployeeId)
        {
            objAssignToContractor.GetExpenditureDropDownValuesByBudgetHeadId(ref expenditureBOList, ref BudgetHeadId, ref EmployeeId);
        }

        #endregion

        #region "Get Contractor Having ME Servicing Contract"

        public void GetMeServicingContractors(ref List<DropDownBO> dropDownList, AssignToContractorBo objAssignToContractorBo)
        {
            objAssignToContractor.GetMeContractors(ref dropDownList, objAssignToContractorBo);
        }

        #endregion

        #region "Get Contact DropDown Values By ContractorId"

        public void GetContactDropDownValuesbyContractorId(ref List<DropDownBO> dropDownList, ref DataSet resultDataset, AssignToContractorBo objAssignToContractorBo)
        {
            objAssignToContractor.GetContactDropDownValuesbyContractorId(ref dropDownList, ref resultDataset, objAssignToContractorBo);
        }

        public void GetContactDropDownValuesbyContractorId(ref List<DropDownBO> dropDownBoList, int contractorId)
        {
            objAssignToContractor.GetContactDropDownValuesbyContractorId(ref dropDownBoList, contractorId);
        }

        #endregion

        #region "Get Vat Drop down Values - Vat Rate as Value Field and Vat Name as text field."

        public void getVatDropDownValues(ref List<VatBo> vatBoList)
        {
            objAssignToContractor.GetVatDropDownValues(ref vatBoList);
        }

        public void getVatDropDownValues(ref List<DropDownBO> dropDownList)
        {
            objAssignToContractor.GetVatDropDownValues(ref dropDownList);
        }

        #endregion

        #region "Assign Work to Contractor"

        public bool assignToContractor(AssignToContractorBo assignToContractorBo)
        {
            return objAssignToContractor.assignToContractor(assignToContractorBo);
        }

        public bool assignToContractorForSchemePO(AssignToContractorBo assignToContractorBo)
        {
            return objAssignToContractor.assignToContractorForSchemePO(assignToContractorBo);
        }
        #endregion

        #region Get Areas for Assign To Contractor

        public void GetAreaDropDownValues(ref List<DropDownBO> dropDownList)
        {
            objAssignToContractor.GetAreaDropDownValues(ref dropDownList);
        }
        #endregion

        #region "Get Details for email"

        /// <summary>
        /// This function is to get details for email, these details include contractor name, email and other details.
        /// Contact details of customer/tenant.
        /// Risk/Vulnerability details of the tenant
        /// Property Address
        /// </summary>
        /// <param name="detailsForEmailDS">Dataset to get details returned from Database.</param>
        /// <param name="assignToContractorBo">Assign to Contractor Bo containing values used to get details</param>
        /// <remarks></remarks>
        public void getdetailsForEmail(ref AssignToContractorBo assignToContractorBo, ref DataSet detailsForEmailDS)
        {
            objAssignToContractor.getdetailsForEmail(ref assignToContractorBo, ref detailsForEmailDS);
        }

        #endregion


        #region "Get Details for void email-Scheme/Block Case"

        /// <summary>
        /// This function is to get details for email, these details include contractor name, email and other details.
        /// Contact details of customer/tenant.
        /// </summary>
        /// <param name="detailsForEmailDS">Dataset to get details returned from Database.</param>
        /// <param name="assignToContractorBo">Assign to Contractor Bo containing values used to get details</param>
        /// <remarks></remarks>
        public void getSB_DetailsForEmail(ref AssignToContractorBo assignToContractorBo, ref DataSet detailsForEmailDS)
        {
            objAssignToContractor.getSB_DetailsForEmail(ref assignToContractorBo, ref detailsForEmailDS);
        }

        #endregion

        #region "Get Details for Provision email-Scheme/Block Case"

        /// <summary>
        /// This function is to get details for email, these details include contractor name, email and other details.
        /// Contact details of customer/tenant.
        /// </summary>
        /// <param name="detailsForEmailDS">Dataset to get details returned from Database.</param>
        /// <param name="assignToContractorBo">Assign to Contractor Bo containing values used to get details</param>
        /// <remarks></remarks>
        public void getSB_DetailsForProvisionEmail(ref AssignToContractorBo assignToContractorBo, ref DataSet detailsForEmailDS)
        {
            objAssignToContractor.getSB_DetailsForProvisionEmail(ref assignToContractorBo, ref detailsForEmailDS);
        }

        #endregion

        #region "Get Assign To Contractor Detail By JournalId"
        /// <summary>
        /// This function 'll get Assign To Contractor Detail By JournalId
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="tradeIds"></param>
        /// <param name="propertyId"></param>
        /// <param name="startDate"></param>
        /// <remarks></remarks>
        public void getAssignToContractorDetailByJournalId(ref DataSet resultDataSet, int journalId)
        {
            objAssignToContractor.getAssignToContractorDetailByJournalId(ref resultDataSet, journalId);
        }
        #endregion

        #region "Get Contractor Having Reactive Repair Contract"

        public void getReactiveRepairContractors(ref List<DropDownBO> dropDownList)
        {
            objAssignToContractor.getReactiveRepairContractors(ref dropDownList);
        }

        public void getAllContractors(ref List<DropDownBO> dropDownList)
        {
            objAssignToContractor.getAllContractors(ref dropDownList);
        }
        #endregion

        #region "Assign Work to Contractor"

        public bool voidWorkAssignToContractor(ref AssignToContractorBo assignToContractorBo)
        {
            return objAssignToContractor.voidWorkAssignToContractor(ref assignToContractorBo);
        }

        #endregion

        #region "Get Contact DropDown Values By ContractorId for Scheme PO"

        public void GetContactDropDownValuesAndDetailsbyContractorId(ref List<DropDownBO> dropDownList, ref DataSet resultDataset, AssignToContractorBo objAssignToContractorBo)
        {
            objAssignToContractor.GetContactDropDownValuesAndDetailsbyContractorId(ref dropDownList, ref resultDataset, objAssignToContractorBo);
        }

        #endregion

        #region "void Paint Pack Assign Work to Contractor"

        public bool voidPaintPackAssignToContractor(ref AssignToContractorBo assignToContractorBo)
        {
            return objAssignToContractor.voidPaintPackAssignToContractor(ref assignToContractorBo);
        }

        #endregion

        public void getBudgetHolderByOrderId(ref DataSet resultDs, int orderId, int expenditureId)
        {
            AssignToContractorRepo objAssignToContractorDAL = new AssignToContractorRepo();
            objAssignToContractorDAL.getBudgetHolderByOrderId(ref resultDs, orderId, expenditureId);
        }

        #region Get SubItems By AreaId

        public void GetAttributesSubItemsDropdownValuesByAreaId(ref List<DropDownBO> dropDownList, int areaId)
        {
            objAssignToContractor.GetAttributesSubItemsDropdownValuesByAreaId(ref dropDownList, areaId);
        }

        #endregion

        #region "get Sub Item Detail"
        public DataSet GetItemMSATDetailByItemId(int itemId, int? schemeId, int? blockId)
        {
            return objAssignToContractor.GetItemMSATDetailByItemId(itemId, schemeId, blockId);
        }
        #endregion

        #endregion

        public DataSet getBlockDetailByBlockId(int blockId)
        {
            return objAssignToContractor.getBlockDetailByBlockId(blockId);
        }

        public DataSet GetBlockNameBySchemeId(int schemeId, int itemId)
        {
            return objAssignToContractor.GetBlockNameBySchemeId(schemeId, itemId);
        }

        public DataSet GetSchemeByBlockId(int blockId)
        {
            return objAssignToContractor.GetSchemeByBlockId(blockId);
        }
    }

}
