﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PDR_BusinessLogic.Base;
using PDR_DataAccess.PaintPacks;
using System.Data;
using PDR_BusinessObject.PageSort;
using PDR_BusinessObject.PaintPacks;

namespace PDR_BusinessLogic.PaintPacks
{
    public class PaintPacksBL : BaseBL
    {
        IPaintPacksRepo objPaintPacksRepo;
        public PaintPacksBL(IPaintPacksRepo rRepo)
        {
            objPaintPacksRepo = rRepo;
        }


        #region get Paint Packs List
        /// <summary>
        /// get Paint Packs List
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <returns></returns>
        public int getPaintPacksList(ref DataSet resultDataSet, PageSortBO objPageSortBO)
        {
            return objPaintPacksRepo.getPaintPacksList(ref resultDataSet, objPageSortBO);
        }

        #endregion

        #region get Paint Packs Detail
        /// <summary>
        /// get Paint Packs Detail
        /// </summary>
        /// <param name="paintPackId"></param>
        /// <returns></returns>
        public DataSet getPaintPacksDetail(int paintPackId)
        {
            return objPaintPacksRepo.getPaintPacksDetail(paintPackId);
        }
        #endregion

        #region amend Paint Packs
        /// <summary>
        /// amend Paint Packs
        /// </summary>
        /// <param name="objPaintPacksBo"></param>
        public void amendPaintPacks(PaintPacksBO objPaintPacksBo) 
        {
            objPaintPacksRepo.amendPaintPacks(objPaintPacksBo);
        }
        #endregion

        #region update Paint Packs
        /// <summary>
        /// update Paint Packs
        /// </summary>
        /// <param name="objPaintPacksBo"></param>
        public void updatePaintPacks(PaintPacksBO objPaintPacksBo)
        {
            objPaintPacksRepo.updatePaintPacks(objPaintPacksBo);
        }
        #endregion
    }
}
