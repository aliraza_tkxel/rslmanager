﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PDR_BusinessLogic.Base;
using PDR_DataAccess.Scheduling;
using System.Data;
using PDR_BusinessObject.VoidAppointment;

namespace PDR_BusinessLogic.Scheduling
{
    public class VoidSchedulingBL : BaseBL
    {
        ISchedulingRepo objScheduling;
        public VoidSchedulingBL(ISchedulingRepo objSched)
        {
            objScheduling = objSched;
        }

        #region get All Void Operatives
        /// <summary>
        /// get All Void Operatives
        /// </summary>
        /// <returns></returns>
        public DataSet getAllVoidOperatives()
        {
            return objScheduling.getAllVoidOperatives();
        }
        #endregion

        #region get Operatives Leaves and Appointments
        /// <summary>
        /// get Operatives Leaves and  Appointments
        /// </summary>
        /// <returns></returns>
        public void getOperativesLeavesAppointments(ref DataSet resultDataSet, int OperativeId, DateTime startDate)
        {
            objScheduling.getOperativesLeavesAppointments(ref resultDataSet, OperativeId, startDate);
        }
        #endregion

        #region Schedule Void Inspection
        /// <summary>
        /// Schedule Void Inspection
        /// </summary>
        /// <param name="objVoidAppointmentBO"></param>
        /// <returns></returns>
        public string scheduleVoidInspection(VoidAppointmentBO objVoidAppointmentBO, ref int appointmentId)
        {

            return objScheduling.scheduleVoidInspection(objVoidAppointmentBO, ref appointmentId);

        }

        #endregion

        #region schedule Void Required Works
        /// <summary>
        /// Schedule Void Inspection
        /// </summary>
        /// <param name="objVoidAppointmentBO"></param>
        /// <returns></returns>
        public string scheduleVoidRequiredWorks(VoidAppointmentBO objVoidAppointmentBO, ref int appointmentId, ref int JournalId, DataTable voidRequiredWorkDuration)
        {

            return objScheduling.scheduleVoidRequiredWorks(objVoidAppointmentBO, ref appointmentId, ref JournalId, voidRequiredWorkDuration);

        }

        #endregion


        #region Schedule Void Required Works
        /// <summary>
        /// Schedule Void Required Works
        /// </summary>
        /// <param name="objVoidAppointmentBO"></param>
        /// <returns></returns>
        public string reScheduleWorksRequired(VoidAppointmentBO objVoidAppointmentBO)
        {
            return objScheduling.reScheduleWorksRequired(objVoidAppointmentBO);
        }
        #endregion

        #region "Cancel Void appointment"
        /// <summary>
        /// Cancel Void appointment
        /// </summary>
        /// <param name="pmo"></param>
        /// <param name="reason"></param>
        /// <returns></returns>
        /// <remarks></remarks>

        public int cancelVoidAppointment(int journalId)
        {

            return objScheduling.cancelVoidAppointment(journalId);
        }

        #endregion

        #region Re-Schedule Void Inspection
        /// <summary>
        /// Re-Schedule Void Inspection
        /// </summary>
        /// <param name="objVoidAppointmentBO"></param>
        /// <returns></returns>
        public string reScheduleVoidInspection(VoidAppointmentBO objVoidAppointmentBO)
        {

            return objScheduling.reScheduleVoidInspection(objVoidAppointmentBO);

        }

        #endregion

        #region schedule Gas Electric Checks
        /// <summary>
        /// schedule Gas Electric Checks
        /// </summary>
        /// <param name="objVoidAppointmentBO"></param>
        /// <param name="appointmentId"></param>
        /// <returns></returns>
        public string scheduleGasElectricChecks(VoidAppointmentBO objVoidAppointmentBO, ref int appointmentId)
        {

            return objScheduling.scheduleGasElectricChecks(objVoidAppointmentBO, ref appointmentId);

        }

        #endregion

        #region "Cancel Gas/Electric Appointment "
        /// <summary>
        /// Cancel Gas/Electric Appointment 
        /// </summary>
        /// <param name="pmo"></param>
        /// <param name="reason"></param>
        /// <returns></returns>
        /// <remarks></remarks>

        public int cancelGasElectricAppointment (int journalId,int createdBy)
        {

            return objScheduling.cancelGasElectricAppointment(journalId, createdBy);
        }

        #endregion

        #region Get Appointment Detail By JournalId
        /// <summary>
        ///  Get Appointment Detail By JournalId
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="operativeId"></param>
        /// <remarks></remarks>

        public void getAppointmentDetailByJournalId(ref DataSet resultDataSet, int journalId)
        {
            objScheduling.getAppointmentDetailByJournalId(ref resultDataSet, journalId);
        }
        #endregion
    }
}
