﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using PDR_BusinessLogic.Base;
using System.Data;
using PDR_DataAccess;
using PDR_BusinessObject;
using PDR_BusinessObject.PageSort;
using PDR_Utilities.Helpers;
using PDR_Utilities.Constants;
using PDR_DataAccess.Scheduling;
using PDR_BusinessObject.TradeAppointment;
using PDR_BusinessObject.MeAppointment;
using PDR_BusinessObject.MeSearch;
using System.Linq;
using PDR_BusinessObject.LookUp;

namespace PDR_BusinessLogic.Scheduling
{
    public class SchedulingBL : Base.BaseBL
    {

        ISchedulingRepo objScheduling;
        public SchedulingBL(ISchedulingRepo objSched)
        {
            objScheduling = objSched;
        }

        #region Get Appointment To Be Arranged List
        /// <summary>
        /// Get Appointment To Be Arranged List
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <param name="check56Days"></param>
        /// <param name="msatType"></param>
        /// <returns></returns>
        public Int32 getAppointmentToBeArrangedList(ref DataSet resultDataSet, PageSortBO objPageSortBO, MeSearchBO objMeSearchBO)
        {
            return objScheduling.getAppointmentToBeArrangedList(ref resultDataSet, objPageSortBO, objMeSearchBO);
        }
        #endregion

        #region Get Appointment Arranged List
        /// <summary>
        /// Get Appointment Arranged List
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <param name="check56Days"></param>
        /// <param name="msatType"></param>
        /// <returns></returns>
        public Int32 getAppointmentArrangedList(ref DataSet resultDataSet, PageSortBO objPageSortBO, MeSearchBO objMeSearchBO)
        {
            return objScheduling.getAppointmentArrangedList(ref resultDataSet, objPageSortBO, objMeSearchBO);
        }
        #endregion

        #region "Get All trades"
        /// <summary>
        /// Get All trades
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <remarks></remarks>
        public void getAllTrades(ref DataSet resultDataSet)
        {
            objScheduling.getAllTrades(ref resultDataSet);
        }
        #endregion

        #region "Get Available Operatives"
        /// <summary>
        /// This function 'll get the list of operatives that are available
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="tradeIds"></param>
        /// <param name="propertyId"></param>
        /// <param name="startDate"></param>
        /// <remarks></remarks>
        public void getAvailableOperatives(ref DataSet resultDataSet, string tradeIds, string msattype, DateTime startDate)
        {
            objScheduling.getAvailableOperatives(ref resultDataSet, tradeIds, msattype, startDate);
        }

        #endregion

        #region "Region For ME Appointments - Intelligent Scheduling"

        #region "get Available ME Appointments"
        /// <summary>
        /// This function 'll return the 
        /// </summary>
        /// <param name="operativesDt"></param>
        /// <param name="leavesDt"></param>
        /// <param name="appointmentsDt"></param>
        /// <param name="startDate"></param>
        /// <param name="tradeDurationDt"></param>        
        /// <remarks></remarks>
        public List<TempMeAppointmentBO> getAvailableMeAppointments(DataTable operativesDt, DataTable leavesDt, DataTable appointmentsDt, ref TempAppointmentDtBO tempAppointmentDtBo, System.DateTime startDate, DataTable tradeDurationDt)
        {

            TempMeAppointmentBO tempMeAptBo = new TempMeAppointmentBO();
            List<TempMeAppointmentBO> tempMeAptBoList = new List<TempMeAppointmentBO>();
            //Loop through trade           

            foreach (DataRow meTradrow in tradeDurationDt.Rows)
            {
                tempMeAptBo.Duration = Convert.ToString(meTradrow["Duration"]);
                tempMeAptBo.DurationString = Convert.ToString(meTradrow["DurationString"]);
                tempMeAptBo.TradeId = Convert.ToString(meTradrow["TradeId"]);
                tempMeAptBo.Trade = Convert.ToString(meTradrow["TradeName"]);

                int tradeId = Convert.ToInt32(meTradrow["TradeId"]);
                DataTable filteredOperativesDt = operativesDt;
                //This function 'll apply the filter on operatives , leaves and appointments on the basis of trade id. 
                // This'll further help us to minimize the time to create appointment slots
                filterOperativesLeavesAppointmentsOnTrade(ref filteredOperativesDt, ref leavesDt, ref appointmentsDt, tradeId);
                //'this function 'll create the appointment slot based on leaves, appointments & start date
                this.createTempAppointmentSlots(filteredOperativesDt, leavesDt, appointmentsDt, ref tempMeAptBo.tempAppointmentDtBo
                    , startDate, Convert.ToDouble(meTradrow["Duration"]), GeneralHelper.getAppointmentCreationLimitForSingleRequest());

                //'once appointment is added in list then create a new item 
                //'for new list of trade and appointment against them
                tempMeAptBoList.Add(tempMeAptBo);
                tempMeAptBo = new TempMeAppointmentBO();
            }

            return tempMeAptBoList;

        }
        #endregion

        #region "get Available Condition Appointments"
        /// <summary>
        /// This function 'll return the 
        /// </summary>
        /// <param name="operativesDt"></param>
        /// <param name="leavesDt"></param>
        /// <param name="appointmentsDt"></param>
        /// <param name="startDate"></param>
        /// <param name="durationHours"></param>        
        /// <remarks></remarks>
        public void getAvailableConditionAppointments(DataTable operativesDt, DataTable leavesDt, DataTable appointmentsDt, ref TempAppointmentDtBO tempAppointmentDtBo, System.DateTime startDate, double durationHours)
        {
            this.createTempAppointmentSlots(operativesDt, leavesDt, appointmentsDt, ref tempAppointmentDtBo, startDate
                , durationHours, GeneralHelper.getMeAppointmentCreationLimitForSingleRequest());
        }

        #endregion

        #region "add More Temp ME Appointments"
        /// <summary>
        /// This function 'll do multiple tasks. It filters operatives and creates appointment slots based on leaves, appointments and start date
        /// </summary>
        /// <param name="operativesDt"></param>
        /// <param name="leavesDt"></param>
        /// <param name="appointmentsDt"></param>
        /// <param name="tempTradeAptBo"></param>
        /// <param name="startDate"></param>
        /// <param name="startAgain">This 'll help to identify that user wants to fetch the appointments onward from the selected date</param>
        /// <remarks></remarks>
        public void addMoreTempMeAppointments(DataTable operativesDt, DataTable leavesDt, DataTable appointmentsDt, ref TempMeAppointmentBO tempTradeAptBo, System.DateTime startDate, bool startAgain = false)
        {
            // this id 'll retain the operative id, against which the slots were created
            int previousOperativeId = 0;
            // retrieve the trade id 
            int tradeId = Convert.ToInt32(tempTradeAptBo.TradeId);
            DataTable filteredOperativesDt = operativesDt;
            //This function 'll apply the filter on operatives , leaves and appointments on the basis of trade id. 
            // This function 'll further help us to minimize the time to create appointment slots
            filterOperativesLeavesAppointmentsOnTrade(ref filteredOperativesDt, ref leavesDt, ref appointmentsDt, tradeId);
            if (startAgain == false)
            {
                //fetch the previous operative id so the operatives name should appear in order                
                var appointments = tempTradeAptBo.tempAppointmentDtBo.dt.AsEnumerable();
                var appResult = (from app in appointments
                                 where Convert.ToBoolean(app[TempAppointmentDtBO.lastAddedColName]) == true
                                 select app);
                if (appResult.Count() > 0)
                {
                    previousOperativeId = Convert.ToInt32(appResult.LastOrDefault()[TempAppointmentDtBO.operativeIdColName]);
                    //This function 'll create the appointment slots based on operatives, appointments, leaves and start date
                    //as we have previous operative id , so it 'll continue creating the appointment slots after existing slots
                    this.createTempAppointmentSlots(filteredOperativesDt, leavesDt, appointmentsDt, ref tempTradeAptBo.tempAppointmentDtBo, startDate, Convert.ToDouble(tempTradeAptBo.Duration), GeneralHelper.getAppointmentCreationLimitForSingleRequest(), previousOperativeId);
                }
            }
            else
            {
                //This function 'll create the appointment slots based on operatives, appointments, leaves and start date. 
                // This 'll restart appointments slot from the start date
                tempTradeAptBo.tempAppointmentDtBo.dt.Clear();
                this.createTempAppointmentSlots(filteredOperativesDt, leavesDt, appointmentsDt, ref tempTradeAptBo.tempAppointmentDtBo, startDate, Convert.ToDouble(tempTradeAptBo.Duration), GeneralHelper.getAppointmentCreationLimitForSingleRequest());
            }
        }
        #endregion

        #endregion

        #region "Region For Temporary Trade And Appointments - Intelligent Scheduling"

        #region "add More Temp Trade Appointments"
        /// <summary>
        /// This function 'll do multiple tasks. It filters operatives and creates appointment slots based on leaves, appointments and start date
        /// </summary>
        /// <param name="operativesDt"></param>
        /// <param name="leavesDt"></param>
        /// <param name="appointmentsDt"></param>
        /// <param name="tempTradeAptBo"></param>
        /// <param name="startDate"></param>
        /// <param name="startAgain">This 'll help to identify that user wants to fetch the appointments onward from the selected date</param>
        /// <remarks></remarks>
        public void addMoreTempTradeAppointments(DataTable operativesDt, DataTable leavesDt, DataTable appointmentsDt, ref TempTradeAppointmentBO tempTradeAptBo, System.DateTime startDate, bool startAgain = false)
        {
            // this id 'll retain the operative id, against which the slots were created
            int previousOperativeId = 0;
            int tradeId = tempTradeAptBo.tempTradeDtBo.tradeId;
            DataTable filteredOperativesDt = operativesDt;
            //This function 'll apply the filter on operatives , leaves and appointments on the basis of trade id. 
            // This function 'll further help us to minimize the time to create appointment slots
            filterOperativesLeavesAppointmentsOnTrade(ref filteredOperativesDt, ref leavesDt, ref appointmentsDt, tradeId);
            if (startAgain == false)
            {
                //fetch the previous operative id so the operatives name should appear in order                
                var appointments = tempTradeAptBo.tempAppointmentDtBo.dt.AsEnumerable();
                var appResult = (from app in appointments
                                 where Convert.ToBoolean(app[TempAppointmentDtBO.lastAddedColName]) == true
                                 select app);
                if (appResult.Count() > 0)
                {
                    previousOperativeId = Convert.ToInt32(appResult.LastOrDefault()[TempAppointmentDtBO.operativeIdColName]);
                    //This function 'll create the appointment slots based on operatives, appointments, leaves and start date
                    //as we have previous operative id , so it 'll continue creating the appointment slots after existing slots
                    this.createTempAppointmentSlots(filteredOperativesDt, leavesDt, appointmentsDt, ref tempTradeAptBo.tempAppointmentDtBo, startDate, tempTradeAptBo.tempTradeDtBo.tradeDurationHrs, GeneralHelper.getAppointmentCreationLimitForSingleRequest(), previousOperativeId);
                }
            }
            else
            {
                //This function 'll create the appointment slots based on operatives, appointments, leaves and start date. 
                // This 'll restart appointments slot from the start date
                tempTradeAptBo.tempAppointmentDtBo.dt.Clear();
                this.createTempAppointmentSlots(filteredOperativesDt, leavesDt, appointmentsDt, ref tempTradeAptBo.tempAppointmentDtBo, startDate, tempTradeAptBo.tempTradeDtBo.tradeDurationHrs, GeneralHelper.getAppointmentCreationLimitForSingleRequest());
            }
        }
        #endregion

        #region "get Temp Trade Appointments"
        /// <summary>
        /// This function 'll use to get the available appointments against the operatives, after checking leaves and appointments datatable using start date
        /// </summary>
        /// <param name="compTradeDt"></param>
        /// <param name="operativesDt"></param>
        /// <param name="leavesDt"></param>
        /// <param name="appointmentsDt"></param>        
        /// <returns></returns>
        /// <remarks></remarks>
        public object getTempTradeAppointments(DataTable compTradeDt, DataTable operativesDt, DataTable leavesDt, DataTable appointmentsDt, System.DateTime startDate)
        {
            TempTradeAppointmentBO tempTradeAptBo = new TempTradeAppointmentBO();
            List<TempTradeAppointmentBO> tempTradeAptBoList = new List<TempTradeAppointmentBO>();

            //Loop through trade           
            foreach (DataRow compTradrow in compTradeDt.Rows)
            {
                //save the start date             
                tempTradeAptBo.StartSelectedDate = startDate;

                if (compTradrow.Table.Columns.Contains("JSN"))
                {
                    //in case of appointments with status "arranged" or others
                    tempTradeAptBo.tempTradeDtBo.jsn = Convert.ToString(compTradrow["JSN"]);
                }
                else
                {
                    //in case of appointments with status "appointment to be arranged"
                    tempTradeAptBo.tempTradeDtBo.jsn = "N/A";
                }

                tempTradeAptBo.tempTradeDtBo.componentName = Convert.ToString(compTradrow["Component"]);
                tempTradeAptBo.tempTradeDtBo.durationString = GeneralHelper.appendHourLabel(Convert.ToDouble(compTradrow["Duration"]));
                tempTradeAptBo.tempTradeDtBo.tradeName = Convert.ToString(compTradrow["Trade"]);
                if (compTradrow["Status"] == ApplicationConstants.StatusArranged)
                {
                    //if status is already arranged 
                    tempTradeAptBo.tempTradeDtBo.status = Convert.ToString(compTradrow["InterimStatus"]);
                }
                else
                {
                    tempTradeAptBo.tempTradeDtBo.status = Convert.ToString(compTradrow["Status"]);
                }
                tempTradeAptBo.tempTradeDtBo.tradeId = Convert.ToInt32(compTradrow["TradeId"]);
                tempTradeAptBo.tempTradeDtBo.tradeDurationHrs = Convert.ToDouble(compTradrow["Duration"]);
                // * GeneralHelper.getAppointmentDurationHours() ' This is commented as duration in now in hours.
                tempTradeAptBo.tempTradeDtBo.tradeDurationDays = Convert.ToDouble(compTradrow["Duration"]) / ConfigHelper.getAppointmentDurationHours();
                tempTradeAptBo.tempTradeDtBo.componentTradeId = Convert.ToInt32(compTradrow["ComponentTradeId"]);
                tempTradeAptBo.tempTradeDtBo.componentId = Convert.ToInt32(compTradrow["ComponentId"]);
                tempTradeAptBo.tempTradeDtBo.addNewDataRow();

                int tradeId = Convert.ToInt32(compTradrow["TradeId"]);
                //This function 'll apply the filter on operatives , leaves and appointments on the basis of trade id. 
                // This() 'll further help us to minimize the time to create appointment slots
                DataTable filteredOperativesDt = operativesDt;
                filterOperativesLeavesAppointmentsOnTrade(ref filteredOperativesDt, ref leavesDt, ref appointmentsDt, tradeId);
                //'this function 'll create the appointment slot based on leaves, appointments & due date
                this.createTempAppointmentSlots(filteredOperativesDt, leavesDt, appointmentsDt, ref tempTradeAptBo.tempAppointmentDtBo, startDate, tempTradeAptBo.tempTradeDtBo.tradeDurationHrs, GeneralHelper.getAppointmentCreationLimitForSingleRequest());

                //'this function 'll sort the appointments by appointment date and operatives' name between operative's last appointment and current calcualted appointment 
                orderAppointmentsByTimeAndOperative(ref tempTradeAptBo);

                //'once appointment is added in list then create a new item 
                //'for new list of trade and appointment against them
                tempTradeAptBoList.Add(tempTradeAptBo);
                tempTradeAptBo = new TempTradeAppointmentBO();
            }

            return tempTradeAptBoList;
        }
        #endregion

        #region "create Temp Appointment Slots"
        /// <summary>
        /// 'This function 'll create the appointment slots based on operatives, appointments, leaves and due date 
        /// </summary>
        /// <param name="operativesDt"></param>
        /// <param name="leavesDt"></param>
        /// <param name="appointmentsDt"></param>
        /// <param name="tempAppointmentDtBo"></param>
        /// <param name="selectedStartDate"></param>
        /// <param name="previousOperativeId"></param>
        /// <remarks></remarks>
        public void createTempAppointmentSlots(DataTable operativesDt, DataTable leavesDt, DataTable appointmentsDt, ref TempAppointmentDtBO tempAppointmentDtBo, System.DateTime selectedStartDate, double tradeDurationHrs, int requiredResultCount, int previousOperativeId = 0, DateTime? endDateTime = null)
        {
            int operativeId = 0;
            string operativeName = string.Empty;
            double officialDayStartHour = this.getTodaysStartingHour(ref selectedStartDate);
            double officialDayEndHour = ConfigHelper.getOfficialDayEndHour();
            int displayedTimeSlotCount = 1;
            DateTime aptStartDate;
            DateTime aptEndDate;
            DateTime aptStartTime = default(DateTime);
            DateTime aptEndTime = default(DateTime);
            double aptStartHour = 0;
            double aptEndHour = 0;
            DateTime appointmentDueDate;
            string ptachName = string.Empty;
            bool exitFlag = false;
            while ((displayedTimeSlotCount <= requiredResultCount))
            {
                //First Condition: If no operative found then display error and exit the loop
                if (operativesDt.Rows.Count == 0 || exitFlag == true)
                {
                    break; // TODO: might not be correct. Was : Exit While
                }

                foreach (DataRow operativeDr in operativesDt.Rows)
                {
                    operativeId = Convert.ToInt32(operativeDr["EmployeeId"]);
                    operativeName = Convert.ToString(operativeDr["FullName"]);
                    //this if block 'll be used when page 'll post back, 
                    //this will help to start creating appointment from the operative which was in the last row of previously displayed slots 

                    if ((previousOperativeId != 0))
                    {
                        if ((operativeId == previousOperativeId))
                        {
                            previousOperativeId = 0;
                            continue;
                        }

                        if ((operativeId != previousOperativeId))
                        {
                            continue;
                        }

                    }

                    //set the appointment starting hour e.g 9 (represents 9 o'clock in the morning)  
                    //now we i'll add up the number(hours) in date to get date and time
                    aptStartDate = GeneralHelper.getDateTimeFromDateAndNumber(selectedStartDate, officialDayStartHour);
                    appointmentDueDate = Convert.ToDateTime(DateTime.Today.AddDays(ConfigHelper.getMaximunLookAhead()).ToLongDateString() + " " + "23:59");


                    if (aptStartDate >= appointmentDueDate)
                    {
                        break; // TODO: might not be correct. Was : Exit While
                    }
                    //skip the weekend if it exists
                    this.skipWeekend(ref aptStartDate, ref officialDayStartHour);
                    //initially appointment end date time 'll be equal to appointment start date time. After that we 'll use trade duration to calculate the appointment end date time
                    aptEndDate = aptStartDate;
                    aptEndHour = officialDayStartHour;
                    aptStartHour = officialDayStartHour;

                    //get the old time slots that have been displayed / added lately
                    var slots = tempAppointmentDtBo.dt.AsEnumerable();
                    TempAppointmentDtBO tempAppointmentDtBoCol = tempAppointmentDtBo;
                    var slotsResult = (from app in slots
                                       orderby Convert.ToDateTime(app[TempAppointmentDtBO.endDateStringColName]) ascending
                                       where Convert.ToInt32(app[TempAppointmentDtBO.operativeIdColName]) == operativeId
                                       select app);

                    //if the there's some old time slot that has been added lately get the new time slot
                    if (slotsResult.Count() > 0)
                    {
                        //some old time slot that has been added lately get the new time slot based on old slot
                        aptEndDate = Convert.ToDateTime(slotsResult.Last()[TempAppointmentDtBO.endDateStringColName]);
                        aptEndDate = Convert.ToDateTime(aptEndDate.ToLongDateString() + " " + slotsResult.Last()[TempAppointmentDtBO.endTimeColName].ToString());
                    }
                    //set appointment start date , end time
                    this.setAppointmentStartEndTime(ref aptStartDate, ref aptEndDate, ref aptStartTime, ref aptEndTime, ref aptStartHour, ref aptEndHour, tradeDurationHrs);
                    int slotCheckCounter = 1;
                    //this loop 'll create the appointment slots but it 'll not end until and unless it 'll be sure
                    //that no appointment or leave exist in the new created appointment time slot
                    while ((slotCheckCounter > 0))
                    {
                        //this slotCheckCounter = 0 , means that we are assuming time slot is valid i.e no appointment or leave exist during this assumed time slot
                        //if leave or appointment 'll exist then this slotCheckCounter 'll be incremented by 1 and one more while loop cycle 'll be executed.
                        slotCheckCounter = 0;
                        //check the leaves if exist or not
                        EnumerableRowCollection<DataRow> leaveResult = this.isLeaveExist(leavesDt, operativeId, aptEndDate, aptStartTime, aptEndTime);
                        //if leave exist get the new date time but still its possibility that leave ''ll exist in the next date time so 'll also be checked again.
                        if (leaveResult.Count() > 0)
                        {
                            aptEndDate = Convert.ToDateTime(leaveResult.Last()["EndDate"]);
                            aptEndDate = GeneralHelper.combineDateAndTime(aptEndDate, leaveResult.Last()["EndTime"].ToString());
                            //set appointment start date , end time
                            this.setAppointmentStartEndTime(ref aptStartDate, ref aptEndDate, ref aptStartTime, ref aptEndTime, ref aptStartHour, ref aptEndHour, tradeDurationHrs);
                            //this addition of 1 in slotCheckCounter means we have created the new time slot and we need another loop cycle to verify this slot, 
                            //that no appointment or leave exist during this time slot
                            slotCheckCounter += 1;
                        }
                        //if appointment exist in this time slot get the new date time but still its possibility that appointment ''ll exist in the next time slot, so this while loop 'll run until
                        //this 'll be established that no new appointment actually exist in this time slot
                        EnumerableRowCollection<DataRow> appResult = this.isAppointmentExist(appointmentsDt, operativeId, aptEndDate, aptStartTime, aptEndTime, aptStartDate);
                        if ((appResult != null) & appResult.Count() > 0)
                        {
                            aptEndDate = Convert.ToDateTime(appResult.Last()["AppointmentEndDate"]);
                            aptEndDate = GeneralHelper.unixTimeStampToDateTime(Convert.ToDouble(appResult.Last()["EndTimeInSec"]));
                            //set appointment start date , end time
                            this.setAppointmentStartEndTime(ref aptStartDate, ref aptEndDate, ref aptStartTime, ref aptEndTime, ref aptStartHour, ref aptEndHour, tradeDurationHrs);
                            //this addition of 1 in slotCheckCounter means we have created the new time slot and we need another loop cycle to verify this slot,
                            //that no appointment or leave exist during this time slot
                            slotCheckCounter += 1;
                        }

                    }
                    if (endDateTime != null)
                    {
                        if (aptEndDate.Date != endDateTime)
                        {
                            exitFlag = true;
                            break; // TODO: might not be correct. Was : Exit While
                        }
                    }
                    // while loop end
                    //add this time slot in appointment list
                    tempAppointmentDtBo.operativeId = operativeId;
                    tempAppointmentDtBo.operative = operativeName;
                    tempAppointmentDtBo.startTime = GeneralHelper.getHrsAndMinString(aptStartTime);
                    tempAppointmentDtBo.endTime = GeneralHelper.getHrsAndMinString(aptEndTime);
                    tempAppointmentDtBo.startDateString = GeneralHelper.convertDateToCustomString(aptStartDate);
                    tempAppointmentDtBo.endDateString = GeneralHelper.convertDateToCustomString(aptEndDate);
                    tempAppointmentDtBo.lastAdded = true;
                    updateLastAdded(ref tempAppointmentDtBo);
                    tempAppointmentDtBo.addNewDataRow();

                    //increase the displayed time slot count
                    displayedTimeSlotCount = displayedTimeSlotCount + 1;
                    if (displayedTimeSlotCount > requiredResultCount)
                    {
                        break; // TODO: might not be correct. Was : Exit While
                    }
                }
            }
            //outer while loop end
        }

        #endregion

        #region "set appointment start end time"
        /// <summary>
        /// This function calculates the appointment start time and appointment end time. We are assuming that end date 'll be used to calculate the new start date and end date
        /// </summary>
        /// <param name="aptEndDate"></param>
        /// <param name="aptStartTime"></param>
        /// <param name="aptEndTime"></param>
        /// <param name="aptEndHour"></param>
        /// <param name="tDuration"></param>
        /// <remarks></remarks>

        private void setAppointmentStartEndTime(ref DateTime aptStartDate, ref DateTime aptEndDate, ref DateTime aptStartTime, ref DateTime aptEndTime, ref double aptStartHour, ref double aptEndHour, double tDuration)
        {
            double officialDayEndHour = ConfigHelper.getOfficialDayEndHour();
            double officialDayStartHour = ConfigHelper.getOfficialDayStartHour();
            double aptEndLapse = 0;

            //calculate the appointment end time and end hour. This 'll be used to find out the next date , time, hour            
            aptEndTime = GeneralHelper.get12HourTimeFormatFromDate(aptEndDate);
            aptEndHour = GeneralHelper.getHoursFromTime(aptEndTime);

            //if coming date and time is  change the day if time ends
            this.changeDayIfTimeEnd(ref aptEndDate, ref aptEndHour, officialDayEndHour);

            //now use appointment end date, end time & end hour to calculate the appointment start date, start time & start hour 
            aptStartDate = aptEndDate;
            aptStartTime = GeneralHelper.get12HourTimeFormatFromDate(aptEndDate);
            aptEndHour = GeneralHelper.getHoursFromTime(aptStartTime);
            aptStartHour = aptEndHour;

            //This will actually work as day counter in the loop, every time entering the it will be increased by 1 mean set to 1 first time.
            int dayCounter = 0;
            //run the loop until duration become zero
            while ((tDuration > 0))
            {
                dayCounter += 1;
                double lapse = 0;
                //calculate the remaining time/lapse of the day
                lapse = officialDayEndHour - aptEndHour;
                //subtract the total lapse/remaining time of the day from the trade duration
                tDuration = tDuration - lapse;
                //if duration is less than zero than it means now we have to calculate the end hour of appointment otherwise we have to go to one day ahead to calculate the end time
                if ((tDuration < 0))
                {
                    if (dayCounter > 1)
                    {
                        aptEndHour = officialDayStartHour + tDuration + lapse;
                        aptEndLapse = aptEndHour - officialDayStartHour;
                    }
                    else
                    {
                        aptEndHour = aptStartHour + tDuration + lapse;
                        aptEndLapse = aptEndHour - aptStartHour;
                    }

                }
                else if ((tDuration == 0))
                {
                    aptEndLapse = lapse;
                }
                else if (tDuration > 0)
                {
                    //we have to go to one day ahead to calculate the end time, so setting running hour = dayEndhour
                    aptEndHour = officialDayEndHour;
                }
                this.changeDayIfTimeEnd(ref aptEndDate, ref aptEndHour, officialDayEndHour);
            }
            aptEndDate = aptEndDate.AddHours(aptEndLapse);
            aptEndTime = GeneralHelper.get12HourTimeFormatFromDate(aptEndDate);
            aptEndHour = GeneralHelper.getHoursFromTime(aptEndTime);
        }


        private void setVoidAppointmentStartEndTime(ref DateTime appointmentStartDate, ref DateTime appointmentEndDate, double workDuration, ref bool skipOperative,
                        DataTable leavesDt, DataTable appointmentsDt, int operativeId, TempAppointmentDtBO tempFaultAptBo, DateTime appointmentDueDate, ref Hashtable dueDateCheckCount, ref int uniqueCounter)
        {
            bool isAppointmentCreated = false;

            //Get Operative core and extended working hours from session
            DataSet operativeWorkingHourDs = session.OperativeWorkingHoursDataSet;
            DataTable operativeWorkingHourDt = operativeWorkingHourDs.Tables[0];

            System.DateTime currentRunningDate = appointmentStartDate;
            int loggingTime = 0;
          
            //Do Until: appointment is created or the operative is skipped
            do
            {
                //Check Operative's Appointment Start Date for Due date and also for operatives availability (today and future) 

                if (isOpertiveAvailable(operativeWorkingHourDt, appointmentStartDate, ref dueDateCheckCount, operativeId, uniqueCounter, appointmentDueDate))
                {
                    skipOperative = true;

                }
                else
                {
                    //Get operatives running day available time in ascending order of StartTime column
                    //DataTable operativeWorkingHourForCurrentDayDt = getOperativeAvailableSlotsByDateTime(operativeWorkingHourDt, appointmentStartDate);

                    isAppointmentCreated = getOperativeAvailableSlotsForMultipleDays(operativeWorkingHourDt, ref appointmentStartDate, ref appointmentEndDate, workDuration, appointmentDueDate, ref loggingTime);

                }

                if (isAppointmentCreated && !skipOperative)
                {
                    //check the leaves if exist or not
                    EnumerableRowCollection<DataRow> leaveResult = this.isLeavesExist(leavesDt, operativeId, appointmentStartDate, appointmentEndDate);
                    //if leave exist get the new date time but still its possibility that leave will exist in the next date time so will also be checked again.
                    if (leaveResult.Count() > 0)
                    {
                        appointmentStartDate = Convert.ToDateTime(leaveResult.Last().Field<DateTime>("EndDate"));
                        appointmentStartDate = Convert.ToDateTime(appointmentStartDate.ToLongDateString() + " " + leaveResult.Last().Field<string>("EndTime"));
                        isAppointmentCreated = false;
                    }
                    else
                    {
                        //if appointment exist in this time slot get the new date time but still its possibility that appointment ''ll exist in the next time slot, so this while loop 'll run until
                        //this will be established that no new appointment actually exist in this time slot
                        EnumerableRowCollection<DataRow> appResult = this.isAppointmentsExist(appointmentsDt, operativeId, appointmentStartDate, appointmentEndDate);
                        if ((appResult != null) & appResult.Count() > 0)
                        {
                            //appointmentStartDate = Convert.ToDateTime(appResult.Last().Field(Of DateTime)("AppointmentEndDate"))
                            appointmentStartDate = GeneralHelper.unixTimeStampToDateTime(Convert.ToDouble(appResult.Last().Field<int>("EndTimeInSec")));
                            isAppointmentCreated = false;
                        }

                    }
                }

            } while (!((isAppointmentCreated || skipOperative)));
        }

        #endregion

        #region "Skip Weekend"
        /// <summary>
        /// This function 'll skip the weekend (saturday or sunday)
        /// </summary>
        /// <param name="aptStartDate"></param>
        /// <param name="aptStartHour"></param>
        /// <remarks></remarks>
        private void skipWeekend(ref System.DateTime aptStartDate, ref double aptStartHour)
        {
            if (aptStartDate.DayOfWeek == DayOfWeek.Saturday)
            {
                aptStartDate = aptStartDate.AddDays(2);
                aptStartHour = ConfigHelper.getOfficialDayStartHour();
            }
            else if (aptStartDate.DayOfWeek == DayOfWeek.Sunday)
            {
                aptStartDate = aptStartDate.AddDays(1);
                aptStartHour = ConfigHelper.getOfficialDayStartHour();
            }
            aptStartDate = GeneralHelper.getDateTimeFromDateAndNumber(aptStartDate, aptStartHour);
        }
        #endregion

        #region "change Day If Time End"
        private bool changeDayIfTimeEnd(ref DateTime aptEndDate, ref double aptEndHour, double OfficialDayEndHour)
        {
            if ((aptEndHour >= OfficialDayEndHour))
            {
                // get core day starthour of operative.
                //DataSet operativeWorkingHours = session.OperativeWorkingHoursDataSet;
                //DataTable operativeWorkingHourDt = operativeWorkingHours.Tables[0];
                //if (operativeWorkingHourDt.Rows.Count > 0)
                //{
                //    string nextBlockStartTime = operativeWorkingHourDt.Rows[0][ApplicationConstants.StartTimeColumn].ToString();
                //    string[] substrings = nextBlockStartTime.Split(':');
                //    aptEndHour = Convert.ToDouble(substrings[0]);

                //}
                //else
                //{
                aptEndHour = ConfigHelper.getOfficialDayStartHour();
                //}
                //go to next day because current day slots have finished
                //Also set the running hour to zero for the nex day 
                aptEndDate = aptEndDate.AddDays(1);
                aptEndDate = GeneralHelper.getDateTimeFromDateAndNumber(aptEndDate, aptEndHour);
                //this.skipWeekend(ref aptEndDate, ref aptEndHour);
                return true;
            }
            else
            {
                return false;
            }
        }


        #endregion

        #region "is Leave Exist"
        /// <summary>
        /// This function checks either operative has leave in the given start time and end time
        /// if leave exists then go to next time slot that 'll start from end time of leave
        /// </summary>
        /// <param name="operativeId"></param>
        /// <param name="aptStartTime"></param>
        /// <param name="aptEndTime"></param>
        /// <returns>true or false</returns>
        /// <remarks></remarks>
        private EnumerableRowCollection<DataRow> isLeaveExist(DataTable leavesDt, int operativeId, System.DateTime runningDate, System.DateTime aptStartTime, System.DateTime aptEndTime)
        {

            aptStartTime = Convert.ToDateTime(runningDate.ToLongDateString() + " " + aptStartTime.ToShortTimeString());
            aptEndTime = Convert.ToDateTime(runningDate.ToLongDateString() + " " + aptEndTime.ToShortTimeString());

            //Convert time into seconds for comparison
            double lStartTimeInMin = 0;
            double lEndTimeInMin = 0;

            lStartTimeInMin = GeneralHelper.convertDateInMin(aptStartTime);
            lEndTimeInMin = GeneralHelper.convertDateInMin(aptEndTime);

            var leaves = leavesDt.AsEnumerable();
            var leaveResult = (from app in leaves
                               orderby Convert.ToDouble(app["EndTimeInMin"]) ascending
                               where Convert.ToDouble(app["OperativeId"]) == operativeId &&
                               ((lStartTimeInMin <= Convert.ToDouble(app["StartTimeInMin"]) & lEndTimeInMin > Convert.ToDouble(app["StartTimeInMin"]) & lEndTimeInMin <= Convert.ToDouble(app["EndTimeInMin"])) |
                               (lStartTimeInMin >= Convert.ToDouble(app["StartTimeInMin"]) & lEndTimeInMin <= Convert.ToDouble(app["EndTimeInMin"])) |
                               (lStartTimeInMin >= Convert.ToDouble(app["StartTimeInMin"]) & lStartTimeInMin < Convert.ToDouble(app["EndTimeInMin"]) & lEndTimeInMin >= Convert.ToDouble(app["EndTimeInMin"])) |
                               (lStartTimeInMin < Convert.ToDouble(app["StartTimeInMin"]) & lEndTimeInMin > Convert.ToDouble(app["EndTimeInMin"])))
                               select app);

            return leaveResult;
        }
        #endregion

        #region "is Appointment Exist"
        /// <summary>
        /// This function checks either operative has appointment in the given start time and end time
        /// </summary>
        /// <param name="operativeId"></param>
        /// <param name="aptStartTime "></param>
        /// <param name="aptEndTime "></param>
        /// <returns>true or false</returns>
        /// <remarks></remarks>
        private EnumerableRowCollection<DataRow> isAppointmentExist(DataTable appointmentsDt, int operativeId, System.DateTime runningDate, System.DateTime aptStartTime, System.DateTime aptEndTime, System.DateTime aptStartDate = default(DateTime))
        {

            if (aptStartDate == default(DateTime))
            {
                aptStartTime = Convert.ToDateTime(runningDate.ToLongDateString() + " " + aptStartTime.ToShortTimeString());
            }
            else
            {
                aptStartTime = Convert.ToDateTime(aptStartDate.ToLongDateString() + " " + aptStartTime.ToShortTimeString());
            }

            aptEndTime = Convert.ToDateTime(runningDate.ToLongDateString() + " " + aptEndTime.ToShortTimeString());

            //convert time into seconds for comparison
            double startTimeInSec = 0;
            double endTimeInSec = 0;

            startTimeInSec = GeneralHelper.convertDateInSec(aptStartTime);
            endTimeInSec = GeneralHelper.convertDateInSec(aptEndTime);

            var appointments = appointmentsDt.AsEnumerable();
            var appResult = (from app in appointments
                             orderby Convert.ToDouble(app["EndTimeInSec"]) ascending
                             where Convert.ToDouble(app["OperativeId"]) == operativeId &&
                             ((startTimeInSec == Convert.ToDouble(app["StartTimeInSec"]) & endTimeInSec < Convert.ToDouble(app["EndTimeInSec"]) |
                             (startTimeInSec < Convert.ToDouble(app["EndTimeInSec"]) & endTimeInSec == Convert.ToDouble(app["EndTimeInSec"])) |
                             (startTimeInSec > Convert.ToDouble(app["StartTimeInSec"]) & endTimeInSec < Convert.ToDouble(app["EndTimeInSec"])) |
                             (startTimeInSec < Convert.ToDouble(app["StartTimeInSec"]) & endTimeInSec > Convert.ToDouble(app["EndTimeInSec"])) |
                             (startTimeInSec < Convert.ToDouble(app["StartTimeInSec"]) & endTimeInSec > Convert.ToDouble(app["StartTimeInSec"])) |
                             (startTimeInSec < Convert.ToDouble(app["EndTimeInSec"]) & endTimeInSec > Convert.ToDouble(app["EndTimeInSec"]))))
                             select app);

            return appResult;
        }
        #endregion

        #region "Filter Operatives Leaves Appointments On Trade"
        /// <summary>
        /// This function 'll apply the filter on operatives , leaves and appointments on the basis of trade id. This 'll further help us to minimize the time to create appointment slots
        /// </summary>
        /// <param name="operativesDt"></param>
        /// <param name="leavesDt"></param>
        /// <param name="appointmentsDt"></param>
        /// <param name="tradeId"></param>
        /// <remarks></remarks>

        private void filterOperativesLeavesAppointmentsOnTrade(ref DataTable operativesDt, ref DataTable leavesDt, ref DataTable appointmentsDt, int tradeId)
        {
            //filter the operatives on the basis of trade             
            var operativesRs = (from op in operativesDt.AsEnumerable()
                                where Convert.ToInt32(op["TradeId"]) == tradeId
                                select op);

            if (operativesRs.Count() > 0)
            {
                //save filtered operatives in separate data table
                operativesDt = operativesRs.CopyToDataTable();
                //make comma separated string of operative ids. This 'll be used to fetch the                
                string operativeIds = System.Convert.ToString(operativesRs.Select(x => x["EmployeeId"]).Aggregate((x, y) => x.ToString() + "," + y.ToString()));
                //query the leaves data set to fetch only the appointments of selected operatives
                var leavesRs = (from ap in leavesDt.AsEnumerable()
                                where string.Join(",", operativeIds).Contains(Convert.ToString(ap["OperativeId"]))
                                select ap);
                if (leavesRs.Count() > 0)
                {
                    //save filtered appointments in separate data table
                    leavesDt = leavesRs.CopyToDataTable();
                }

                //query the appointment data set to fetch only the appointments of selected operatives
                var appointmentRs = (from ap in appointmentsDt.AsEnumerable()
                                     where string.Join(",", operativeIds).Contains(Convert.ToString(ap["OperativeId"]))
                                     select ap);
                if (appointmentRs.Count() > 0)
                {
                    //save filtered appointments in separate data table
                    appointmentsDt = appointmentRs.CopyToDataTable();
                }
            }
        }
        #endregion

        #region "Add Hours in Start Date"
        /// <summary>
        /// Requirement is to start the appointments from today then appointment slots should start from 2 hours ahead of current time
        /// </summary>
        /// <param name="startDate"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        private Double getTodaysStartingHour(ref System.DateTime startDate)
        {
            //if both date are same then add two hours in it
            if (DateTime.Compare(startDate.Date, System.DateTime.Today) == 0)
            {
                startDate = DateTime.Now.AddHours(2);
            }
            else
            {
                startDate = GeneralHelper.getDateTimeFromDateAndNumber(startDate, ConfigHelper.getOfficialDayStartHour());
            }
            return startDate.Hour;
        }
        #endregion

        #region "Order Temp Appointments By Distance"
        /// <summary>
        /// 'this function 'll sort the appointments by appointment date and operatives' name between operative's last appointment and current calcualted appointment 
        /// </summary>
        /// <param name="tempTradeAptBo"></param>
        /// <remarks></remarks>
        public void orderAppointmentsByTimeAndOperative(ref TempTradeAppointmentBO tempTradeAptBo)
        {
            var appointments = tempTradeAptBo.tempAppointmentDtBo.dt.AsEnumerable();
            //Order The appointment, First By Distance then By Date and Then By Start Time.
            var appResult = (from app in appointments
                             orderby Convert.ToDateTime(app[TempAppointmentDtBO.startDateStringColName]) ascending, app[TempAppointmentDtBO.operativeColName] ascending
                             select app);

            if (appResult.Count() > 0)
            {
                tempTradeAptBo.tempAppointmentDtBo.dt = appResult.CopyToDataTable();
            }

        }

        #endregion

        #region "update Last added column in tempTradeAptBo data table"
        /// <summary>
        /// 'update Last added column in tempTradeAptBo datatable 
        /// </summary>
        /// <param name="tempTradeAptBo"></param>
        /// <remarks></remarks>
        public void updateLastAdded(ref TempAppointmentDtBO tempTradeAptBo)
        {
            var appointments = tempTradeAptBo.dt.AsEnumerable();
            var appResult = (from app in appointments
                             where Convert.ToBoolean(app[TempAppointmentDtBO.lastAddedColName]) == true
                             select app);
            if (appResult.Count() > 0)
            {
                appResult.First()[TempAppointmentDtBO.lastAddedColName] = false;
            }

        }

        #endregion

        public DataTable applyPatchFilter(DataTable operativesDt, ref TempAppointmentDtBO appointmentSlotsDtBo, int patchId)
        {
            DataTable filteredOperativesDt = new DataTable();

            //convert operatives data table into dv            
            DataView operativesDv = new DataView();
            operativesDv = operativesDt.AsDataView();

            //if patch is selected then filter the operatives which have the same patch as selected property's patch
            if (appointmentSlotsDtBo.isPatchSelected == true)
            {
                //(PatchId = -1 ) => Filter for operatives with all patches.
                operativesDv.RowFilter = string.Format("PatchId = {0}", patchId.ToString());
                filteredOperativesDt = operativesDv.ToTable();

                //get the ids of filtered operatives
                string employeeIds = string.Empty;
                foreach (DataRow dr in filteredOperativesDt.Rows)
                {
                    employeeIds = employeeIds + dr["EmployeeId"].ToString() + ",";
                }
                if (employeeIds.Length > 1)
                {
                    employeeIds = employeeIds.Substring(0, employeeIds.Length - 1);

                    //select the appointments based on operatives which are filtered above
                    DataView tempAptDv = new DataView();
                    tempAptDv = appointmentSlotsDtBo.dt.AsDataView();
                    tempAptDv.RowFilter = appointmentSlotsDtBo.operativeId + " IN (" + employeeIds + ")";
                    appointmentSlotsDtBo.dt = tempAptDv.ToTable();
                }
                else
                {
                    appointmentSlotsDtBo.dt.Clear();
                }
            }
            else
            {
                filteredOperativesDt = operativesDv.ToTable();
            }

            return filteredOperativesDt;
        }

        private DataTable getOperativeAvailableSlotsByDateTime(DataTable operativeWorkingHourDt, System.DateTime dateToFilter)
        {
            //Check one or more working hours slots from the operative data table.
            var dayFilterResult = (from optHour in operativeWorkingHourDt.AsEnumerable()
                                   where optHour[ApplicationConstants.StartDateColumn].ToString() == dateToFilter.ToString("dd/MM/yyyy")
                                   | ((optHour[ApplicationConstants.WeekDayCol].ToString() == dateToFilter.DayOfWeek.ToString())
                                       //& optHour[ApplicationConstants.StartDateColumn] == "NA"
                                       //& optHour[ApplicationConstants.EndDateColumn] == "NA"
                                   & !string.IsNullOrEmpty(optHour[ApplicationConstants.StartTimeColumn].ToString()))
                                   orderby (Convert.ToDateTime(optHour[ApplicationConstants.StartTimeColumn])) ascending
                                   select optHour).Where(optHour => string.IsNullOrEmpty(optHour[ApplicationConstants.EndTimeColumn].ToString()) ?
                                           false : TimeSpan.Parse(optHour[ApplicationConstants.EndTimeColumn].ToString()) > dateToFilter.TimeOfDay);
            // Check if there is a slot then return the slots in data table otherwise return an empty data table with same schema 
            DataTable operativeWorkingHourForCurrentDayDt = dayFilterResult.Any() ? dayFilterResult.CopyToDataTable() : operativeWorkingHourDt.Clone();
            return operativeWorkingHourForCurrentDayDt;
        }

        #endregion

        #region "Schedule Me Work appointment"
        /// <summary>
        /// Schedule Me Work appointment
        /// </summary>
        /// <param name="objMeAppointmentBO"></param>
        /// <param name="journalId"></param>
        /// <returns></returns>
        public string scheduleMeWorkAppointment(MeAppointmentBO objMeAppointmentBO, int journalId, ref int appointmentId)
        {

            return objScheduling.scheduleMeWorkAppointment(objMeAppointmentBO, journalId, ref appointmentId);

        }

        #endregion

        #region "Get Property Customer Detail"
        /// <summary>
        /// Get Property Customer Detail
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="propertyId"></param>
        /// <remarks></remarks>

        public void getPropertyDetailByJournalId(ref DataSet resultDataSet, int journalId, bool isVoid)
        {
            objScheduling.getPropertyDetailByJournalId(ref resultDataSet, journalId, isVoid);
        }
        #endregion

        #region "Get Arranged Appointments Detail"
        /// <summary>
        /// Get Arranged Appointments Detail
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="pmo"></param>
        /// <remarks></remarks>

        public void getArrangedAppointmentsDetail(ref DataSet resultDataSet, int journalId)
        {
            objScheduling.getArrangedAppointmentsDetail(ref resultDataSet, journalId);
        }

        #endregion

        #region "Update appointment notes"

        /// <summary>
        /// Update appointment notes
        /// </summary>
        /// <param name="customerNotes"></param>
        /// <param name="jobsheetNotes"></param>
        /// <param name="appointmentId"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public int updateMeAppointmentsNotes(string customerNotes, string jobsheetNotes, int appointmentId)
        {

            return objScheduling.updateMeAppointmentsNotes(customerNotes, jobsheetNotes, appointmentId);

        }

        #endregion

        #region "Cancel appointment"
        /// <summary>
        /// Cancel appointment
        /// </summary>
        /// <param name="pmo"></param>
        /// <param name="reason"></param>
        /// <returns></returns>
        /// <remarks></remarks>

        public int cancelAppointment(int journalId, string reason)
        {
            return objScheduling.cancelAppointment(journalId, reason);
        }

        #endregion

        #region "Get Operative Info"
        /// <summary>
        /// Get Operative Info
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="operativeId"></param>
        /// <remarks></remarks>

        public void getOperativeInformation(ref DataSet resultDataSet, int operativeId)
        {
            objScheduling.getOperativeInformation(ref resultDataSet, operativeId);
        }
        #endregion

        #region Void Intelligent Scheduling
        public List<LookUpBO> getVoidAvailableOperative(DataSet OperativesDataSet, DateTime startDate, DateTime aptStartTime, DateTime aptEndTime)
        {
            int operativeId = 0;
            string operativeName = string.Empty;
            List<int> outOperatives = new List<int>();
            List<LookUpBO> lookupBo = new List<LookUpBO>();

            foreach (DataRow operativeDr in OperativesDataSet.Tables[0].Rows)
            {
                operativeId = Convert.ToInt32(operativeDr["EmployeeId"]);
                operativeName = Convert.ToString(operativeDr["FullName"]);


                //check the leaves if exist or not
                EnumerableRowCollection<DataRow> leaveResult = this.isLeaveExist(OperativesDataSet.Tables[1], operativeId, startDate, aptStartTime, aptEndTime);
                //if leave exist for current operative, break the loop  and add operativeId in outOperatives list
                if (leaveResult.Count() > 0)
                {
                    outOperatives.Add(operativeId);
                    continue;
                }

                //if Appointment already exist for current operative, break the loop  and add operativeId in outOperatives list
                EnumerableRowCollection<DataRow> appResult = this.isAppointmentExist(OperativesDataSet.Tables[2], operativeId, startDate, aptStartTime, aptEndTime, startDate);
                if ((appResult != null) & appResult.Count() > 0)
                {
                    outOperatives.Add(operativeId);
                    continue;
                }
            }


            //convert list of outOperative to comma seperated seperated string to filter operatives datatable
            string result = string.Join(",", outOperatives.Select(x => x.ToString()).ToArray());
            //filter the operatives on the basis of Operative Id             
            var operativesRs = (from op in OperativesDataSet.Tables[0].AsEnumerable()
                                where !result.Contains(op["EmployeeId"].ToString())
                                select op);

            
            if (operativesRs.Count() > 0)
            {
                DataTable operativesDt = operativesRs.CopyToDataTable();
                operativesDt.Columns.Remove("TradeId");
                var results = from p in operativesDt.AsEnumerable()
                              group p by new
                              {
                                  EmployeeId = p.Field<int>("EmployeeId"),
                                  FullName = p.Field<string>("FullName")
                              } into g
                              select new
                              {

                                  EmployeeId = g.Key.EmployeeId,
                                  FullName = g.Key.FullName
                              };


                //save filtered operatives in separate data table
                lookupBo = results.Select(x => new LookUpBO() { EmployeeId = x.EmployeeId, FullName = x.FullName }).ToList();

            }

            return lookupBo;
        }

        #endregion

        #region"Salman's Work"

        #region "checkOperativeAvailability"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="runningDate"></param>
        /// <param name="operativeHoursDt"></param>
        /// <returns></returns>
        private Boolean checkOperativeAvailability(DateTime runningDate, DataTable operativeHoursDt)
        {

            bool isAvailable = true;
            var workingHours = operativeHoursDt.AsEnumerable();

            var coreWorkingHoursResult = (from app in workingHours
                                          where
                                              app[ApplicationConstants.StartDateColumn].ToString() == "NA"
                                              & app[ApplicationConstants.EndDateColumn].ToString() == "NA"
                                              & !string.IsNullOrEmpty(app[ApplicationConstants.StartTimeColumn].ToString())
                                          select app);

            var extendedWorkingHoursResult = (from app in workingHours
                                              where app[ApplicationConstants.StartDateColumn].ToString() != "NA"
                                              select app);

            if (extendedWorkingHoursResult.Count() > 0)
            {
                extendedWorkingHoursResult = (from app in extendedWorkingHoursResult.CopyToDataTable().AsEnumerable()
                                              where Convert.ToDateTime(app[ApplicationConstants.StartDateColumn]) >= runningDate
                                              select app);
            }

            if (coreWorkingHoursResult.Count() == 0 & extendedWorkingHoursResult.Count() == 0)
            {
                isAvailable = false;
            }

            return isAvailable;

        }
        #endregion

        #region"change day"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="runningDate"></param>
        /// <param name="nextDayStartTime"></param>
        public void changeday(ref DateTime runningDate, string nextDayStartTime)
        {
            runningDate = runningDate.AddDays(1);
            runningDate = Convert.ToDateTime(runningDate.ToLongDateString() + " " + nextDayStartTime);
        }

        #endregion

        #region"convert Date In Sec"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="timeToConvert"></param>
        /// <returns></returns>
        public static int convertDateInSec(DateTime timeToConvert)
        {
            DateTime calendarStartDate = new DateTime(1970, 1, 1);
            int timeInSec = Convert.ToInt32((timeToConvert - calendarStartDate).TotalSeconds);
            return timeInSec;
        }
        #endregion


        #region" is Appointments Exist"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="appointmentsDt"></param>
        /// <param name="operativeId"></param>
        /// <param name="appointmentStartDateTime"></param>
        /// <param name="appointmentEndDateTime"></param>
        /// <returns></returns>
        public EnumerableRowCollection<DataRow> isAppointmentsExist(DataTable appointmentsDt, int operativeId, System.DateTime appointmentStartDateTime, System.DateTime appointmentEndDateTime)
        {

            //'çonvert time into seconds for comparison

            int startTimeInSec = 0;
            int endTimeInSec = 0;

            startTimeInSec = convertDateInSec(appointmentStartDateTime);
            endTimeInSec = convertDateInSec(appointmentEndDateTime);


            //Dim appResult = (From app In appointments Where app("AppointmentDate") = runningDateAlias AndAlso app("OperativeId") = operativeId AndAlso (app("StartTimeInSec") >= startTimeInSec Or endTimeInSec <= app("EndTimeInSec")) Select app)
            var appResult = (from app in appointmentsDt.AsEnumerable()
                             where Convert.ToInt32(app["OperativeId"]) == operativeId
                                && (1 == 0
                                || (startTimeInSec == Convert.ToInt32(app["StartTimeInSec"]) && endTimeInSec < Convert.ToInt32(app["EndTimeInSec"]))
                                || (startTimeInSec < Convert.ToInt32(app["EndTimeInSec"]) && endTimeInSec == Convert.ToInt32(app["EndTimeInSec"]))
                                || (startTimeInSec > Convert.ToInt32(app["StartTimeInSec"]) && endTimeInSec < Convert.ToInt32(app["EndTimeInSec"]))
                                || (startTimeInSec < Convert.ToInt32(app["StartTimeInSec"]) && endTimeInSec > Convert.ToInt32(app["EndTimeInSec"]))
                                || (startTimeInSec < Convert.ToInt32(app["StartTimeInSec"]) && endTimeInSec > Convert.ToInt32(app["StartTimeInSec"]))
                                || (startTimeInSec < Convert.ToInt32(app["EndTimeInSec"]) && endTimeInSec > Convert.ToInt32(app["EndTimeInSec"])))
                             orderby app["EndTimeInSec"] ascending
                             select app
                                    );

            return appResult;
        }
        #endregion

        #region"is Leaves Exist "
        public EnumerableRowCollection<DataRow> isLeavesExist(DataTable leavesDt, int operativeId, System.DateTime appointmentStartDateTime, System.DateTime appoitmentEndDateTime)
        {

            //'çonvert time into seconds for comparison

            int lStartTimeInMin = 0;
            int lEndTimeInMin = 0;

            lStartTimeInMin = Convert.ToInt32(GeneralHelper.convertDateInMin(appointmentStartDateTime));
            lEndTimeInMin = Convert.ToInt32(GeneralHelper.convertDateInMin(appoitmentEndDateTime));

            var leaves = leavesDt.AsEnumerable();
            var leaveResult = (from app in leavesDt.AsEnumerable()
                               where Convert.ToInt32(app["OperativeId"]) == operativeId
                               && ((lStartTimeInMin <= Convert.ToInt32(app["StartTimeInMin"]) & lEndTimeInMin > Convert.ToInt32(app["StartTimeInMin"]) & lEndTimeInMin <= Convert.ToInt32(app["EndTimeInMin"]))
                               | (lStartTimeInMin >= Convert.ToInt32(app["StartTimeInMin"]) & lEndTimeInMin <= Convert.ToInt32(app["EndTimeInMin"]))
                               | (lStartTimeInMin >= Convert.ToInt32(app["StartTimeInMin"]) & lStartTimeInMin < Convert.ToInt32(app["EndTimeInMin"]) & lEndTimeInMin >= Convert.ToInt32(app["EndTimeInMin"]))
                               | (lStartTimeInMin < Convert.ToInt32(app["StartTimeInMin"]) & lEndTimeInMin > Convert.ToInt32(app["EndTimeInMin"])))
                               orderby app["EndTimeInMin"] ascending
                               select app);

            return leaveResult;
        }

        #endregion

        #region"create Voids TempAppointment Slots"
        public void createVoidsTempAppointmentSlots(DataTable operativesDt, DataTable leavesDt, DataTable appointmentsDt, ref TempAppointmentDtBO tempFaultAptBo, System.DateTime selectedStartDate, double tradeDurationHrs, int requiredResultCount, int previousOperativeId = 0, DateTime? endDateTime = null)
        {
            //System.DateTime startDate = getStartingDate();
            //if (!(fromDate == null) && tempFaultAptBo.tempAppointmentDtBo.dt.Rows.Count() == 0) {
            //    startDate = fromDate;
            //}
            int operativeId = 0;
            string operativeName = string.Empty;
            string patchName = string.Empty;
            Hashtable dueDateCheckCount = new Hashtable();
            double dayStartHour = this.getTodaysStartingHour(ref selectedStartDate);
            double dayEndHour = ConfigHelper.getOfficialDayEndHour(); ;
            int displayedTimeSlotCount = 1;
            DateTime sDate = Convert.ToDateTime(selectedStartDate.ToLongDateString() + " " + dayStartHour.ToString() + ":00");
            System.DateTime appointmentStartDateTime = default(System.DateTime);
            System.DateTime appointmentEndDate = default(System.DateTime);
            //System.DateTime aptEndTime = new System.DateTime();
            //int isGasOftecOperativeCount = 0;
            //int timeSlotsToDisplay = GeneralHelper.getTimeSlotToDisplay;
            //bool isMultipleDaysAppointmentAllowed = GeneralHelper.isMultipleDaysAppointmentAllowed;

            Dictionary<int, bool> operativeEligibility = new Dictionary<int, bool>();
            string ofcCoreStartTime = DateTime.Parse(ConfigHelper.getOfficialDayStartHour().ToString() + ":00").ToString("HH:mm");
            string ofcCoreEndTime = DateTime.Parse(Convert.ToInt32(dayEndHour - dayEndHour % 1).ToString() + ":" + (dayEndHour % 1 == 0 ? "00" : Convert.ToInt32((dayEndHour % 1) * 60).ToString())).ToString("HH:mm");
            bool skipOperative = false;

            DateTime appointmentDueDate = Convert.ToDateTime(DateTime.Today.AddDays(ConfigHelper.getMaximunLookAhead()).ToLongDateString() + " " + "23:59");

            //Just to initlize start date and end date.
            appointmentStartDateTime = selectedStartDate;
            appointmentEndDate = appointmentStartDateTime;

            //Calculate Time slots given in count
            // Case: when fault duration is greater than MaximumDayWorkingHours (i.e 23.5 hours) then do not run following process
            while (displayedTimeSlotCount <= requiredResultCount)
            {
                int uniqueCounter = 0;
                int operativeCounter = 0;
                //First Condition: If no operative found then display error and exit the loop

                if (operativesDt.Rows.Count == 0)
                {
                    break; // TODO: might not be correct. Was : Exit While
                }

                foreach (DataRow operativeDr in operativesDt.Rows)
                {
                    skipOperative = false;
                    uniqueCounter += 1;
                    operativeCounter += 1;
                    operativeId = Convert.ToInt32(operativeDr["EmployeeId"]);
                    operativeName = Convert.ToString(operativeDr["FullName"]);
                    patchName = Convert.ToString(operativeDr["PatchName"]);
                    DataSet operativeWorkingHours = new DataSet();
                    objScheduling.getCoreAndOutOfHoursInfo(ref operativeWorkingHours, operativeId, ofcCoreStartTime, ofcCoreEndTime);
                    session.OperativeWorkingHoursDataSet = operativeWorkingHours;

                    //this if block 'll be used when page 'll post back, 
                    //this will help to start creating appointment from the operative which was in the last row of previously displayed slots 

                    if ((previousOperativeId != 0))
                    {
                        if ((operativeId == previousOperativeId))
                        {
                            previousOperativeId = 0;
                            continue;
                        }

                        if ((operativeId != previousOperativeId))
                        {
                            continue;
                        }

                    }

                    //if syetem does not get any slot for an operative then system will check in maximum days which Defined in web.cong file.
                    // Other wise system will goes to infinit loop. 
                    appointmentDueDate = Convert.ToDateTime(System.DateTime.Today.AddDays(ConfigHelper.getMaximunLookAhead()).ToLongDateString() + " " + "23:59");


                    //get the old time slots that have been displayed / added lately
                    var slots = tempFaultAptBo.dt.AsEnumerable();
                    var slotsResult = (from app in slots
                                       orderby Convert.ToDateTime(app[TempAppointmentDtBO.endDateStringColName]) ascending
                                       where Convert.ToInt32(app[TempAppointmentDtBO.operativeIdColName]) == operativeId
                                       select app);

                    //if the there's some old time slot that has been added lately get the new time slot
                    if (slotsResult.Count() > 0)
                    {
                        //some old time slot that has been added lately get the new time slot based on old slot
                        appointmentStartDateTime = Convert.ToDateTime(slotsResult.Last().Field<string>(TempAppointmentDtBO.endDateStringColName));
                    }
                    else
                    {
                        //set the appointment starting hour e.g 8 (represents 8 o'clock in the morning)
                        appointmentStartDateTime = Convert.ToDateTime(selectedStartDate.ToLongDateString() + " " + dayStartHour.ToString() + ":00");
                        //appointmentStartDateTime = sDate;
                    }

                    this.setVoidAppointmentStartEndTime(ref appointmentStartDateTime, ref appointmentEndDate, tradeDurationHrs, ref skipOperative, leavesDt, appointmentsDt, operativeId, tempFaultAptBo, appointmentDueDate, ref dueDateCheckCount, ref uniqueCounter);

                    if ((skipOperative == true))
                    {
                        //Requirement is to check, running date should not be greater than fault's due date (if due date check box is selected)
                        //If its true then we are adding entry in hash table and this (current) operative is not being processed any more for appointments
                        //this condition 'll check either due date check has been executed against all the operatives
                        if ((dueDateCheckCount.Count == operativesDt.Rows.Count))
                        {
                            //break; // TODO: might not be correct. Was : 
                            return;
                        }
                        else
                        {
                            continue;
                        }
                    }


                    //if operative has any appointment prior to the running appointment slot then 
                    //find the distance between customer property's address and property address of operative's last arranged appointment on the same day
                    //double distanceFrom = getPropertyDistance(appointmentsDt, operativeId, appointmentStartDateTime, customerBo);
                    //double distanceTo = getPropertyDistance(appointmentsDt, operativeId, appointmentEndDate, customerBo, isToNext: true);

                    //update the lastAdded column in tempfaultBO.dt
                    //updateLastAdded(ref tempFaultAptBo);
                    //add this time slot in appointment list
                    tempFaultAptBo.operativeId = operativeId;
                    tempFaultAptBo.operative = operativeName;
                    tempFaultAptBo.startTime = GeneralHelper.getHrsAndMinString(appointmentStartDateTime);
                    tempFaultAptBo.endTime = GeneralHelper.getHrsAndMinString(appointmentEndDate);
                    tempFaultAptBo.startDateString = GeneralHelper.convertDateToCustomString(appointmentStartDateTime);
                    tempFaultAptBo.endDateString = GeneralHelper.convertDateToCustomString(appointmentEndDate);
                    tempFaultAptBo.lastAdded = true;
                    updateLastAdded(ref tempFaultAptBo);
                    tempFaultAptBo.addNewDataRow();
                    //tempFaultAptBo.addRowInAppointmentList(operativeId, operativeName, appointmentStartDateTime, appointmentEndDate, patchName, distanceFrom, distanceTo);

                    //increase the displayed time slot count
                    displayedTimeSlotCount = displayedTimeSlotCount + 1;
                    if (displayedTimeSlotCount > requiredResultCount)
                    {
                        break; // TODO: might not be correct. Was : Exit While
                    }
                }
            }
            //Calculate Time slots loop ends here
        }

        #endregion


        #region check Date Is Greater Than DueDate And Add To Skip List
        private bool checkDateIsGreaterThanDueDateAndAddToSkipList(ref int uniqueCounter, ref Hashtable dueDateCheckCount, System.DateTime dateToCheck, int operativeId, DateTime appointmentDueDate)
        {
            bool operativeSkipped = false;

            //Check whether the due date is less than appointment date(s)(start/end), if yes then this operative will be skipped.
            if (Convert.ToDateTime(dateToCheck.ToShortDateString()) > appointmentDueDate)
            {
                operativeSkipped = true;
                //this variable 'll check either due date check has been executed against all the operatives
                if (!(dueDateCheckCount.ContainsValue(operativeId)))
                {
                    if (dueDateCheckCount.ContainsKey(this.getUniqueKey(uniqueCounter)) == false)
                    {
                        dueDateCheckCount.Add(this.getUniqueKey(uniqueCounter), operativeId);
                    }
                    else
                    {
                        dueDateCheckCount.Add(this.getUniqueKey(uniqueCounter + 1), operativeId);
                    }
                }
            }
            return operativeSkipped;
        }

        #endregion

        #region is Opertive Available
        private bool isOpertiveAvailable(DataTable operativeWorkingHourDt, DateTime dateToCheck, ref Hashtable dueDateCheckCount, int operativeId, int uniqueCounter, DateTime appointmentDueDate)
        {

            //1- Check whether the due date less than appointment date(s)(start/end), if yes then this operative will be skipped.
            //2- Check whether operative is completely not available in coming days. if yes then this operative will be skipped.
            bool operativeAvailable = checkDateIsGreaterThanDueDateAndAddToSkipList(ref uniqueCounter, ref  dueDateCheckCount, dateToCheck, operativeId, appointmentDueDate)
                || !checkOperativeAvailability(dateToCheck, operativeWorkingHourDt);

            return operativeAvailable;
        }

        #endregion

        #region get Unique Key
        public string getUniqueKey(int uniqueCounter)
        {
            //this 'll be used to create the unique index of dictionary
            string uniqueKey = uniqueCounter.ToString() + session.EmployeeId.ToString() + DateTime.Now.ToString("ddMMyyyyhhmmssffff");
            return uniqueKey;
        }

        #endregion

        #region Day calculation algorithm.

        private bool getOperativeAvailableSlotsForMultipleDays(DataTable operativeWorkingHourDt, ref DateTime dateToFilter, ref DateTime appointmentEndDate, double workDuration, DateTime appointmentDueDate, ref int loggingTime)
        {
            double totalWorkingHour = 0;
            double hourDiff = 0;
            bool isAppointmentCreated = true;
            DateTime appointmentStartDate = dateToFilter;
            bool isStartDateSelected = false;
            bool isExtendedIncluded = false;//extended ot Oncall hours included foe specific day ot not.
            double remainingDuration = workDuration;
            do
            {
                //Check one or more working hours slots from the operative data table.
                var dayFilterResult = (from optHour in operativeWorkingHourDt.AsEnumerable()
                                       where ((optHour[ApplicationConstants.WeekDayCol].ToString() == appointmentStartDate.DayOfWeek.ToString())
                                       & !string.IsNullOrEmpty(optHour[ApplicationConstants.StartTimeColumn].ToString()))
                                       orderby (Convert.ToDateTime(optHour[ApplicationConstants.StartTimeColumn])) ascending
                                       select optHour).Where(optHour => string.IsNullOrEmpty(optHour[ApplicationConstants.EndTimeColumn].ToString()) ?
                                           false : TimeSpan.Parse(optHour[ApplicationConstants.EndTimeColumn].ToString()) > appointmentStartDate.TimeOfDay);

                // Check if there is a slot then return the slots in data table otherwise return an empty data table with same schema 
                DataTable operativeWorkingHourForCurrentDayDt = dayFilterResult.Any() ? dayFilterResult.CopyToDataTable() : operativeWorkingHourDt.Clone();

                if ((operativeWorkingHourForCurrentDayDt.Rows.Count > 0))
                {
                    //Iterate result set including core, Extended and on call hours.
                    foreach (DataRow operativeWorkingHoursBlock in operativeWorkingHourForCurrentDayDt.Rows)
                    {
                        //get operative day start hour for specific day
                        string nextBlockStartTime = operativeWorkingHoursBlock[ApplicationConstants.StartTimeColumn].ToString();
                        DateTime nextBlockStartDateTime = Convert.ToDateTime(appointmentStartDate.ToLongDateString() + " " + nextBlockStartTime);
                        // get operative day end hour for specific day
                        string nextBlockEndTime = operativeWorkingHoursBlock[ApplicationConstants.EndTimeColumn].ToString();
                        DateTime nextBlockEndDateTime = Convert.ToDateTime(appointmentStartDate.ToLongDateString() + " " + nextBlockEndTime);
                        loggingTime = Convert.ToInt32(operativeWorkingHoursBlock[ApplicationConstants.CreationDateCol]);
                        if (operativeWorkingHoursBlock[ApplicationConstants.EndDateColumn].ToString() != "NA")
                        {
                            string extendedBlockStartTime = operativeWorkingHoursBlock[ApplicationConstants.EndDateColumn].ToString();
                            DateTime ExtendedBlockStartDateTime = Convert.ToDateTime(Convert.ToDateTime(operativeWorkingHoursBlock[ApplicationConstants.StartDateColumn]).ToLongDateString() + " " + nextBlockStartTime);
                            string extendedBlockEndTime = operativeWorkingHoursBlock[ApplicationConstants.EndDateColumn].ToString();
                            DateTime ExtendedBlockEndDateTime = Convert.ToDateTime(Convert.ToDateTime(operativeWorkingHoursBlock[ApplicationConstants.EndDateColumn]).ToLongDateString() + " " + nextBlockEndTime);
                            if (nextBlockStartDateTime.ToString("dd/MM/yyyy") == ExtendedBlockStartDateTime.ToString("dd/MM/yyyy"))
                            {
                                isExtendedIncluded = true;
                            }

                        }
                        if (nextBlockEndDateTime > dateToFilter && (operativeWorkingHoursBlock[ApplicationConstants.EndDateColumn].ToString() == "NA" || isExtendedIncluded))
                        {

                            if (!isStartDateSelected && nextBlockStartDateTime > appointmentStartDate)
                            {
                                isStartDateSelected = true;
                                dateToFilter = nextBlockStartDateTime;
                                appointmentStartDate = nextBlockStartDateTime;
                            }
                            else if (!isStartDateSelected && dateToFilter > nextBlockStartDateTime)
                            {
                                isStartDateSelected = true;
                            }
                            else if (nextBlockStartDateTime > appointmentStartDate)
                            {
                                appointmentStartDate = nextBlockStartDateTime;
                            }
                            double operativeWorkingDayHour = Convert.ToDouble(nextBlockEndDateTime.Subtract(appointmentStartDate).TotalHours);
                            appointmentEndDate = nextBlockEndDateTime;
                            if (totalWorkingHour + operativeWorkingDayHour > workDuration)
                            {
                                hourDiff = (totalWorkingHour + operativeWorkingDayHour) - workDuration;
                                appointmentEndDate = nextBlockEndDateTime.AddHours(-hourDiff);
                                appointmentEndDate = appointmentStartDate.AddHours(remainingDuration);
                            }

                            if (hourDiff > 0)
                            {
                                totalWorkingHour = totalWorkingHour + operativeWorkingDayHour - hourDiff;
                            }
                            else if (hourDiff <= 0 && isStartDateSelected)
                            {
                                totalWorkingHour = totalWorkingHour + operativeWorkingDayHour;
                            }
                            if (totalWorkingHour == workDuration)
                                break;
                        }
                    }

                }
                changeday(ref appointmentStartDate, "00:00");
                //Check whether the due date is less than appointment date(s)(start/end), if yes then this operative will be skipped.
                if (Convert.ToDateTime(dateToFilter.ToShortDateString()) > appointmentDueDate)
                {
                    isAppointmentCreated = false;
                    break;
                }

                remainingDuration = workDuration - totalWorkingHour;
            } while (!(totalWorkingHour == workDuration));


            return isAppointmentCreated;
            // return operativeWorkingHourForCurrentDayDt;
        }
        #endregion







        #endregion

        #region "Get All Attributes"
        /// <summary>
        /// Get All Attributes
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <remarks></remarks>
        public void getAllAttributes(ref DataSet resultDataSet)
        {
            objScheduling.getAllAttributes(ref resultDataSet);
        }
        #endregion
    }
}

