﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PDR_BusinessLogic.Base;
using System.Data;
using PDR_DataAccess;
using PDR_BusinessObject;
using PDR_BusinessObject.Phase;
using PDR_BusinessObject.Development;
using PDR_BusinessObject.PageSort;
using PDR_DataAccess.Development;
namespace PDR_BusinessLogic.Development
{
    public class DevelopmentBL : BaseBL
    {
        IDevelopmentRepo objDevelopmentRepo;
        public DevelopmentBL(IDevelopmentRepo devRepo)
        {
            objDevelopmentRepo = devRepo;
        }
        public Int32 getDevelopmentList(ref DataSet resultDataSet, PageSortBO objPageSortBO, String searchText, string searchCompany)
        {
            
            return objDevelopmentRepo.getDevelopmentList(ref resultDataSet, objPageSortBO, searchText, searchCompany);

        }

        public void getDevelopmentPhase(ref DataSet phaseDataSet, int developmentId)
        {
            
            objDevelopmentRepo.getDevelopmentPhase(ref phaseDataSet, developmentId);
        }



        public void getScheme(ref DataSet SchemeDataSet, int developmentId)
        {
            
            objDevelopmentRepo.getScheme(ref SchemeDataSet, -1);
        }

        public void getPatch(ref DataSet PatchDataSet)
        {
            
            objDevelopmentRepo.getPatch(ref PatchDataSet);
        }

        public void getCompany(ref DataSet CompanyDataSet)
        {

            objDevelopmentRepo.getCompany(ref CompanyDataSet);
        }

        public void getDevelopmentType(ref DataSet DevelopmentTypeDataSet)
        {
            
            objDevelopmentRepo.getDevelopmentType(ref DevelopmentTypeDataSet);
        }

        public void getDevelopmentStatus(ref DataSet DevelopmentStatusDataSet)
        {
            
            objDevelopmentRepo.getDevelopmentStatus(ref DevelopmentStatusDataSet);
        }

        public void SavePhase(PhaseBO objPhase, ref int phaseId)
        {
            
            objDevelopmentRepo.SavePhase(objPhase, ref phaseId);
        }

        public void getPhaseInfo(ref DataSet resultData, int phaseId)
        {
            
            objDevelopmentRepo.getPhaseInfo(ref resultData, phaseId);
        }

        public int SaveDocument(DevelopmentDocumentsBO objDevelopmentDocs)
        {
            
            return objDevelopmentRepo.SaveDocument(objDevelopmentDocs);
        }

        public void getDocumentsData(ref DataSet documentDataSet)
        {
            
            objDevelopmentRepo.getDocumentsData(ref documentDataSet);
        }

        public void getDevelopmentsDocumentsData(ref DataSet documentDataSet, int developmentId)
        {

            objDevelopmentRepo.getDevelopmentsDocumentsData(ref documentDataSet, developmentId);
        }

        public void getBlocksData(ref DataSet BlockDataSet, int developmentId)
        {
            
            objDevelopmentRepo.getBlocksData(ref BlockDataSet, developmentId);
        }
        public string SaveDevelpoment(ref DevelopmentBO objSaveDevelopmentBO, List<int> DocIdList, List<int> PhaseIdList)
        {
            
          return  objDevelopmentRepo.saveDevelpoment(ref objSaveDevelopmentBO, DocIdList, PhaseIdList);
        }




        public void getFundingSource(ref DataSet FuncdingSourceDataSet)
        {
            objDevelopmentRepo.getFundingSource(ref FuncdingSourceDataSet);
        }

        public void fetchExpireDocuments(ref DataSet fetchDocuments)
        {
            objDevelopmentRepo.fetchExpireDocuments(ref fetchDocuments);
        }

        public bool checkNotificationSent(int documentId)
        {
            return objDevelopmentRepo.checkNotificationSent(documentId);
        }

        public void logSentNotification(int documentId, string documentTitle, int userId, string status, string message)
        {
            objDevelopmentRepo.logSentNotification(documentId, documentTitle, userId, status, message);
        }

        public void updateDocument(DevelopmentDocumentsBO objDevelopmentDocs)
        {
            objDevelopmentRepo.updateDocument(objDevelopmentDocs);
        }

        public void GetDevelopmentDetail(ref DataSet resultDataset, int developmentId)
        {
            objDevelopmentRepo.GetDevelopmentDetail(ref resultDataset, developmentId);
        }

        public DataSet getDocumentInformationById(int documentId)
        {

            return objDevelopmentRepo.getDocumentInformationById(documentId);


        }

        public string[] deleteDocumentByIDandGetPath(int docId)
        {
            return objDevelopmentRepo.deleteDocumentByIDandGetPath(docId);
        }


    }
}
