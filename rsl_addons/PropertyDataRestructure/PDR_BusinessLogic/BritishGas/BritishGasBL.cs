﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PDR_BusinessLogic.Base;
using PDR_DataAccess.BritishGas;
using PDR_BusinessObject.MeSearch;
using PDR_BusinessObject.BritishGas;
using System.Data;

namespace PDR_BusinessLogic.BritishGas
{
    public class BritishGasBL : BaseBL
    {
        IBritishGasRepo objBritishGasRepo;
        public BritishGasBL(IBritishGasRepo rRepo)
        {
            objBritishGasRepo = rRepo;
        }
        
        #region get British Gas Notification List
        /// <summary>
        /// get the british gas notification list
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="objSearchBo"></param>
        /// <returns></returns>
        public int getBritishGasNotificationList(ref System.Data.DataSet resultDataSet, PDR_BusinessObject.PageSort.PageSortBO objPageSortBO, BritishGasSearchBo objSearchBo)
        {
            return objBritishGasRepo.getBritishGasNotificationList(ref resultDataSet, objPageSortBO, objSearchBo);
        }

        #endregion

        #region get British Gas Stage1 Data
        /// <summary>
        /// Get the british gas stage 1 data from database
        /// </summary>
        /// <param name="britishGasBo"></param>
        public void getBritishGasStage1Data(ref BritishGasNotifcationBO britishGasBo)
        {
            objBritishGasRepo.getBritishGasStage1Data(ref britishGasBo);
        }
        #endregion

        #region get British Gas Stage 2 Data
        /// <summary>
        /// Get the british gas stage 2 data from database
        /// </summary>
        /// <param name="britishGasBo"></param>
        public void getBritishGasStage2AndStage3Data(ref BritishGasNotifcationBO britishGasBo)
        {
            objBritishGasRepo.getBritishGasStage2AndStage3Data(ref britishGasBo);
        }
        #endregion

        #region get Meter Types
        /// <summary>
        /// This function would populate the dataset of gas meter types
        /// </summary>
        /// <param name="resultDataSet"></param>
        public void getMeterTypes(ref DataSet resultDataSet)
        {
            objBritishGasRepo.getMeterTypes(ref resultDataSet);
        }
        #endregion

        #region save British Gas Data
        /// <summary>
        /// Save british gas data
        /// </summary>
        /// <param name="britishGasBo"></param>
        public void saveBritishGasData(ref BritishGasNotifcationBO britishGasBo)
        {
            objBritishGasRepo.saveBritishGasData(ref britishGasBo);
        }
        #endregion

        #region update British Gas Data
        /// <summary>
        /// update british gas data
        /// </summary>
        /// <param name="britishGasBo"></param>
        public bool updateBritishGasStage1Data(ref BritishGasNotifcationBO britishGasBo)
        {
             return objBritishGasRepo.updateBritishGasStage1Data(ref britishGasBo);
        }
        #endregion

        #region update British Gas Stage 2 Data
        /// <summary>
        /// update british gas data
        /// </summary>
        /// <param name="britishGasBo"></param>
        public bool updateBritishGasStage2Data(ref BritishGasNotifcationBO britishGasBo)
        {
            return objBritishGasRepo.updateBritishGasStage2Data(ref britishGasBo);
        }
        #endregion

        #region update British Gas Stage 3 Data
        /// <summary>
        /// update british gas data
        /// </summary>
        /// <param name="britishGasBo"></param>
        public bool updateBritishGasStage3Data(ref BritishGasNotifcationBO britishGasBo)
        {
            return objBritishGasRepo.updateBritishGasStage3Data(ref britishGasBo);
        }
        #endregion

        #region Get Void PDF Document Info
        /// <summary>
        /// This function would be used to Get  british Gas Void Notification  PDF Document Info
        /// </summary>
        /// <param name="britishGasVoidNotificationId"></param>
        /// <returns></returns>
        public DataSet getVoidPDFDocumentInfo(int britishGasVoidNotificationId)
        {
            return objBritishGasRepo.getVoidPDFDocumentInfo(britishGasVoidNotificationId);
        }
        #endregion
    }
}
