﻿using PDR_BusinessLogic.Base;
using System.Data;
using PDR_DataAccess;
using PDR_BusinessObject;
using PDR_BusinessObject.Customer;

// -----------------------------------------------------------------------
// <copyright file="CustomerBL.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace PDR_BusinessLogic.CustomerBL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using PDR_DataAccess.Customer;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class CustomerBL : BaseBL
    {

        ICustomerRepo objCustomerRepo;
        public CustomerBL(ICustomerRepo custRepo)
        {
            objCustomerRepo = custRepo;
        }

        #region "Update customer address"
        public void updateAddress(CustomerBO objCustomerBO)
        {
            objCustomerRepo.updateAddress(objCustomerBO);
        }

        #endregion

    }
}
