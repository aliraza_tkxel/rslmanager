﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_BusinessObject.SchemeBlock
{
   public  class MSATDetailBO
    {

        public bool IsRequired { get; set; }
        public Nullable<DateTime> LastDate { get; set; }
        public Nullable<int> Cycle { get; set; }
        public Nullable<int> CycleTypeId { get; set; }
        public Nullable<decimal> CycleCost { get; set; }
        public Nullable<DateTime> NextDate { get; set; }
        public Nullable<decimal> AnnualApportionment { get; set; }
        public int MSATTypeId { get; set; }
    }
}
