﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_BusinessObject.SchemeBlock
{
    public class DocumentsBO
    {
        #region "Attributes"

        private string _propertyId;
        private int _schemeId;

        public int SchemeId
        {
            get { return _schemeId; }
            set { _schemeId = value; }
        }
        private int _blockId;

        public int BlockId
        {
            get { return _blockId; }
            set { _blockId = value; }
        }
        private string _documentPath;

        private string _documentName;
        private string _documentType;
        private string _keyword;
        private string _documentSize;
        #endregion
        private string _documentFormat;

        private DateTime _expiryDate;
        private DateTime _documentDate;
        private int _uploadedBy;
        private int _EpcRating;
        private String _CP12Number ;
        private Byte[] _CP12Dcoument;
        private string _category;            
        #region "Properties"

        public string PropertyId
        {
            get { return _propertyId; }
            set { _propertyId = value; }
        }
        public int DocumentSubTypeId { get; set; }

        public int DocumentTypeId { get; set; }

        public string DocumentName
        {
            get { return _documentName; }
            set { _documentName = value; }
        }
        public string Category
        {
            get { return _category; }
            set { _category = value; }
        }

        public string DocumentPath
        {
            get { return _documentPath; }
            set { _documentPath = value; }
        }
        public string DocumentType
        {
            get { return _documentType; }
            set { _documentType = value; }
        }
        public string Keyword
        {
            get { return _keyword; }
            set { _keyword = value; }
        }
        public string DocumentSize
        {
            get { return _documentSize; }
            set { _documentSize = value; }
        }
        public string DocumentFormat
        {
            get { return _documentFormat; }
            set { _documentFormat = value; }
        }

        private int _documentId;
        public int DocumentId
        {
            get { return _documentId; }
            set { _documentId = value; }
        }


        public DateTime DocumentDate
        {
            get { return _documentDate; }
            set { _documentDate = value; }
        }

        public int UploadedBy
        {
            get { return _uploadedBy; }
            set { _uploadedBy = value; }
        }
        public int EpcRating
        {
            get { return _EpcRating; }
            set { _EpcRating = value; }
        }

        public DateTime  ExpiryDate
        {
            get { return _expiryDate; }
            set { _expiryDate = value; }
        }

        public String CP12Number 
        {
        get{ return _CP12Number;}
        set{_CP12Number = value;}
   
        }

        public Byte[] CP12Dcoument
        {
            get{ return _CP12Dcoument;}
            set { _CP12Dcoument = value; }
        }

        #endregion
    }
}
