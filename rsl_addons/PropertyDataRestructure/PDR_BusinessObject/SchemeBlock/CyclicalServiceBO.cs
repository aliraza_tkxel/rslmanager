﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_BusinessObject.SchemeBlock
{
    public class CyclicalServiceBO
    {
        public int? ContractorId { get; set; }
        public DateTime? Commencment { get; set; }
        public int ContractPeriod { get; set; }
        public int ContractPeriodType { get; set; }
        public int Cycle { get; set; }
        public int CycleType { get; set; }
        public decimal CycleValue { get; set; }
        public bool BFSCarryingOutWork { get; set; }
        public string VAT { get; set; }
        public int TotalValue { get; set; }
        public int CustomCycleFrequency { get; set; }
        public string CustomCycleOccurance { get; set; }
        public string CustomCycleType { get; set; }

    }
}
