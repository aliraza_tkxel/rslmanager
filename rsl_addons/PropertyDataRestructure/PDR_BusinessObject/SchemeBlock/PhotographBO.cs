﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_BusinessObject.SchemeBlock
{
    public class PhotographBO
    {

        #region Attributes
        private string _Id;
        private string _requestType;
        private string _title;
        private DateTime _uploadDate;
        private string _imageName;
        private string _filePath;
        private int _createdBy;
        private int _itemId;
        private bool _isDefaultImage;
        #endregion

        #region Construtor

        public PhotographBO()
        {
            _Id = string.Empty;
            _requestType = string.Empty;
            _title = string.Empty;
            _uploadDate = DateTime.Now;
            _imageName = string.Empty;
            _filePath = string.Empty;
            _createdBy = 0;
            _itemId = 0;
            _isDefaultImage = false;
        }

        #endregion

        #region Properties
        // Get / Set property for _propertyId
        public string Id
        {

            get
            {
                return _Id;
            }

            set
            {
                _Id = value;
            }

        }

        public string RequestType
        {

            get
            {
                return _requestType;
            }

            set
            {
                _requestType = value;
            }

        }
        // Get / Set property for _title
        public string Title
        {

            get
            {
                return _title;
            }

            set
            {
                _title = value;
            }

        }
        // Get / Set property for _uploadDate
        public DateTime UploadDate
        {

            get
            {
                return _uploadDate;
            }

            set
            {
                _uploadDate = value;
            }

        }
        // Get / Set property for _imageName
        public string ImageName
        {

            get
            {
                return _imageName;
            }

            set
            {
                _imageName = value;
            }

        }
        // Get / Set property for _filePath
        public string FilePath
        {

            get
            {
                return _filePath;
            }

            set
            {
                _filePath = value;
            }

        }
        // Get / Set property for _createdBy
        public int CreatedBy
        {

            get
            {
                return _createdBy;
            }

            set
            {
                _createdBy = value;
            }

        }
        // Get / Set property for _itemId
        public int ItemId
        {

            get
            {
                return _itemId;
            }

            set
            {
                _itemId = value;
            }

        }

        // Get / Set property for _isDefaultImage
        public bool IsDefaultImage
        {

            get
            {
                return _isDefaultImage;
            }

            set
            {
                _isDefaultImage = value;
            }

        }

        #endregion

    }
}