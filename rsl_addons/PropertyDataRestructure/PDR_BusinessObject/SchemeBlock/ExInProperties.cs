﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_BusinessObject.SchemeBlock
{
    public class ExInProperties
    {
        public string PropertyId { get; set; }
        public int IsIncluded { get; set; }
    }
}

