﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_BusinessObject.SchemeBlock
{
    public class RefurbishmentBO
    {
        DateTime _refurbishmentDate;
        string _notes;
        int _Id;
        int _userId;
        string _requestType;

        // Property Asbestos Added Date
        public DateTime RefurbishmentDate
        {
            get
            {
                return _refurbishmentDate;
            }
            set
            {
                _refurbishmentDate = value;
            }
        }

        public string Notes
        {
            get
            {
                return _notes;
            }
            set
            {
                _notes = value;
            }
        }

        public int Id
        {
            get
            {
                return _Id;
            }
            set
            {
                _Id = value;
            }
        }

        public int UserId
        {
            get
            {
                return _userId;
            }
            set
            {
                _userId = value;
            }
        }

        public string RequestType
        {
            get
            {
                return _requestType;
            }
            set
            {
                _requestType = value;
            }
        }
    }
}
