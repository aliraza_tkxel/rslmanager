﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_BusinessObject.SchemeBlock
{
    public class ApplianceBO
    {
        public int? SchemeId { get; set; }
        public int? BlockId{ get; set; }
        private string _propertyId;
        public string PropertyId
        {
            get { return _propertyId; }
            set { _propertyId = value; }
        }
        private int _existingApplianceId;
        public int ExistingApplianceId
        {
            get { return _existingApplianceId; }
            set { _existingApplianceId = value; }
        }
        private string _location;
        public string Location
        {
            get { return _location; }
            set { _location = value; }
        }
        private int _locationId;
        public int LocationId
        {
            get { return _locationId; }
            set { _locationId = value; }
        }


        private string _type;
        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }

        private int _typeId;
        public int TypeId
        {
            get { return _typeId; }
            set { _typeId = value; }
        }

        private string _make;
        public string Make
        {
            get { return _make; }
            set { _make = value; }
        }

        private int _makeId;
        public int MakeId
        {
            get { return _makeId; }
            set { _makeId = value; }
        }



        private string _model;
        public string Model
        {
            get { return _model; }
            set { _model = value; }
        }

        private int _modelId;
        public int ModelId
        {
            get { return _modelId; }
            set { _modelId = value; }
        }

        private int _flueType;
        public int FlueType
        {
            get { return _flueType; }
            set { _flueType = value; }
        }

        private int _itemId;
        public int ItemId
        {
            get { return _itemId; }
            set { _itemId = value; }
        }

        private string _item;
        public string Item
        {
            get { return _item; }
            set { _item = value; }
        }
        private string _quantity;
        public string Quantity
        {
            get { return _quantity; }
            set { _quantity = value; }
        }
        private string _dimensions;
        public string Dimensions
        {
            get { return _dimensions; }
            set { _dimensions = value; }
        }

        private int _lifeSpan;
        public int LifeSpan
        {
            get { return _lifeSpan; }
            set { _lifeSpan = value; }
        }

        private decimal? _purchaseCost;
        public decimal? PurchaseCost
        {
            get { return _purchaseCost; }
            set { _purchaseCost = value; }
        }


        private System.DateTime _datepurchased;
        public System.DateTime Datepurchased
        {
            get { return _datepurchased; }
            set { _datepurchased = value; }
        }
        private System.DateTime _installed;
        public System.DateTime Installed
        {
            get { return _installed; }
            set { _installed = value; }
        }

        private bool isLandLordAppliance;
        public bool IsLandLoardAppliance
        {
            get { return isLandLordAppliance; }
            set { isLandLordAppliance = value; }
        }

        //Start - Changes by Aamir Wahed on 07 May 2013
        //To Implement Additional Field in Add Appliance, Serial Number, Gas Council Number, Replacement Due Field(s)

        private string _serialNumber;
        public string SerialNumber
        {
            get { return _serialNumber; }
            set { _serialNumber = value; }
        }

        private string _notes;
        public string Notes
        {
            get { return _notes; }
            set { _notes = value; }
        }

        private string _gasCouncilNumber;
        public string GasCouncilNumber
        {
            get { return _gasCouncilNumber; }
            set { _gasCouncilNumber = value; }
        }

        private System.DateTime _replacementDue;
        public System.DateTime ReplacementDue
        {
            get { return _replacementDue; }
            set { _replacementDue = value; }
        }

        private System.DateTime _lastReplaced;
        public System.DateTime LastReplaced
        {
            get { return _lastReplaced; }
            set { _lastReplaced = value; }
        }
        private System.DateTime _dateRemoved;

        public System.DateTime DateRemoved
        {
            get { return _dateRemoved; }
            set { _dateRemoved = value; }
        }
        public ApplianceBO()
        {
            this.Location = string.Empty;
            this.Model = string.Empty;
            this.Make = string.Empty;
            this.Type = string.Empty;
            this.PurchaseCost = null;


        }

        //End - Changes by Aamir Wahed on 07 May 2013



    }
    public class LookUpBO
    {
        #region "Attributes"

        private int _value;

        private string _name;
        #endregion

        #region "Constructors"


        public LookUpBO(int val, string name)
        {
            this.LookUpValue = val;
            this.LookUpName = name;

        }

        #endregion

        #region "Properties"

        public int LookUpValue
        {
            get { return _value; }
            set { _value = value; }
        }

        public string LookUpName
        {
            get { return _name; }
            set { _name = value; }
        }

        #endregion

    }
}