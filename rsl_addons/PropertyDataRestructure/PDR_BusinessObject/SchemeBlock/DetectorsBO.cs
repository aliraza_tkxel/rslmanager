﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_BusinessObject.SchemeBlock
{
    public class DetectorsBO
    {
        private string _loaction;
        public string Location
        {
            get { return _loaction; }
            set { _loaction = value; }
        }

        private string _Manufacturer;
        public string Manufacturer
        {
            get { return _Manufacturer; }
            set { _Manufacturer = value; }
        }

        private string _SerialNumber;
        public string SerialNumber
        {
            get { return _SerialNumber; }
            set { _SerialNumber = value; }
        }
        private int _PowerSource;
        public int PowerSource
        {
            get { return _PowerSource; }
            set { _PowerSource = value; }
        }
        public Nullable<System.DateTime> InstalledDate { get; set; }

        private bool _IsLandlordsDetector;
        public bool IsLandlordsDetector
        {
            get { return _IsLandlordsDetector; }
            set { _IsLandlordsDetector = value; }
        }
        public Nullable<System.DateTime> LastTestedDate { get; set; }
        public Nullable<System.DateTime> BatteryReplaced { get; set; }
        public string Notes { get; set; }
        private bool _IsPassed;
        public bool IsPassed
        {
            get { return _IsPassed; }
            set { _IsPassed = value; }
        }
        public int SchemeId { get; set; }
        public int BlockId { get; set; }

        private string _DetectorType;
        public string DetectorType
        {
            get { return _DetectorType; }
            set { _DetectorType = value; }
        }
    }
}
