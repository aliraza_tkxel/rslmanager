﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_BusinessObject.SchemeBlock
{
    public class ProvisionNotesBO
    {
        public int ProvisionId { get; set; }
        public int? SchemeId { get; set; }
        public int? BlockId { get; set; }
        public string Notes { get; set; }
        public int CreatedBy { get; set; }

    }
}
