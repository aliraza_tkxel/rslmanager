﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_BusinessObject.SchemeBlock
{
    [Serializable()]
    public class ApplianceDefectBO
    {

        #region Construtor(s)

        #endregion

        #region Properties

        // Get / Set property for _propertyDefectId
        public int PropertyDefectId { get; set; }

        // Get / Set property for Id
        public string Id { get; set; }

        // Get / Set property for Request Type
        public string RequestType { get; set; }

        // Get / Set property for _categoryId
        public int CategoryId { get; set; }

        // Get / Set property for _categoryDescription
        public string CategoryDescription { get; set; }

        // Get / Set property for _defectDate
        public DateTime DefectDate { get; set; }

        // Get / Set property for _defectIdentified
        public bool IsDefectIdentified { get; set; }

        // Get / Set property for _defectIdentifiedNotes
        public string DefectIdentifiedNotes { get; set; }

        // Get / Set property for _remedialAction
        public bool IsRemedialActionTaken { get; set; }

        // Get / Set property for _remedialActionNotes
        public string RemedialActionNotes { get; set; }

        // Get / Set property for _isWarningIssued
        public bool IsWarningNoteIssued { get; set; }

        // Get / Set property for _applianceId
        public int ApplianceId { get; set; }

        // Get / Set property for _serialNumber
        public string SerialNumber { get; set; }

        // Get / Set property for _serialNumber
        public string GcNumber { get; set; }

        // Get / Set property for _filePath
        public bool IsWarningTagFixed { get; set; }

        // Get / Set property for _isApplianceDisconnected
        public Nullable<bool> IsAppliancedDisconnected = null;

        // Get / Set property for _isPartsRequired
        public Nullable<bool> IsPartsRequired = null;

        // Get / Set property for _isPartsOrdered
        public Nullable<bool> IsPartsOrdered = null;

        // Get / Set property for _partsOrderedBy
        public Nullable<int> PartsOrderedBy { get; set; }

        // Get / Set property for _partsOrderedBy
        public Nullable<DateTime> PartsDueDate { get; set; }

        // Get / Set property for _partsDescription
        public string PartsDescription { get; set; }

        // Get / Set property for _partsLocation
        public string PartsLocation { get; set; }

        // Get / Set property for _isTwoPersonsJob
        public bool IsTwoPersonsJob { get; set; }

        // Get / Set property for _reasonForSecondPerson
        public string ReasonForSecondPerson { get; set; }

        // Get / Set property for _estimatedDuration
        public Nullable<decimal> EstimatedDuration { get; set; }

        // Get / Set property for _priorityId
        public Nullable<int> PriorityId { get; set; }

        // Get / Set property for _estimatedDuration
        public Nullable<int> TradeId { get; set; }

        // Get / Set property for _filePath
        public string FilePath { get; set; }

        // Get / Set property for _photoName
        public string PhotoName { get; set; }

        // Get / Set property for _photoNotes
        public string PhotosNotes { get; set; }

        // Get / Set property for _journalId
        public int JournalId { get; set; }

        // Get / Set property for _partsOrderedByName
        public string PartsOrderedByName { get; set; }

        // Get / Set property for _journalId
        public string Trade { get; set; }

        // Get / Set property for _priorityName
        public string PriorityName { get; set; }

        // Get / Set property for _appliance
        public string Appliance { get; set; }

        // Get / Set property for _userId
        public int UserId { get; set; }

        // Get / Set property for _userId
        public int HeatingMappingId { get; set; }

        // Get / Set property for _boiler
        public string Boiler { get; set; }

        #endregion

    }
}