﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_BusinessObject.SchemeBlock
{
    public class ServiceChargeProperties
    {
        public int? SchemeId { get; set; }
        
        public int? BlockId { get; set; }
        
        public List<ExInProperties> excludedIncludedProperties { get; set; }
    }
}
