﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_BusinessObject.SchemeBlock
{
   public  class ItemAttributeBO
    {
        public int? SchemeId { get; set; }
        public int? BlockId { get; set; }
        public int ItemId { get; set; }
        public int UpdatedBy { get; set; }
        public int? HeatingMappingId { get; set; }
        public string attributeName { get; set; }
        public int? ChildAttributeMappingId { get; set; }
    }
}
