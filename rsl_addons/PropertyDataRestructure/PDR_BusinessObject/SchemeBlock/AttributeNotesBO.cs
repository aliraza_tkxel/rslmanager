﻿namespace PDR_BusinessObject.SchemeBlock
{
    public class AttributeNotesBO
    {
        public string PropertyId { get; set; }
        public int? SchemeId { get; set; }
        public int? BlockId { get; set; }
        public int ItemId { get; set; }
        public string Notes { get; set; }
        public int NotesId { get; set; }
        public int CreatedBy { get; set; }
        public bool ShowInScheduling { get; set; }
        public bool ShowOnApp { get; set; }
        public int? heatingMappingId { get; set; }
    }
}
