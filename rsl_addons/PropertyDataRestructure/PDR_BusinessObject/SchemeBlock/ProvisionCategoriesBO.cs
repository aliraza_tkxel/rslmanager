﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_BusinessObject.SchemeBlock
{
    class ProvisionCategoriesBO
    {
        public int ProvisionCategoriesId { get; set; }
        public string Name { get; set; }
    }
}
