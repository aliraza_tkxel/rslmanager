﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_BusinessObject.SchemeBlock
{
    public class ItemDetailBO
    {

        #region "Private"
        private int _itemParamId;
        private string _parameterValue;
        private int _valueId;
        private bool _isCheckBoxSelected;
        #endregion

        #region "Public Properties"
        //Item Param ID
        public int ItemParamId
        {
            get { return _itemParamId; }
            set { _itemParamId = value; }
        }
        //Parameter Value
        public string ParameterValue
        {
            get { return _parameterValue; }
            set { _parameterValue = value; }
        }

        //Value ID
        public int ValueId
        {
            get { return _valueId; }
            set { _valueId = value; }
        }

        //Is Check Box Selected
        public bool IsCheckBoxSelected
        {
            get { return _isCheckBoxSelected; }
            set { _isCheckBoxSelected = value; }
        }

        #endregion
    }
}
