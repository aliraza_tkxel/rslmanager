﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_BusinessObject.SchemeBlock
{
   public  class ConditionRatingBO
    {
        #region "Private"
        private string _worksRequired;
        private int? _ComponentId;
        #endregion
        private int? _ValueId;

        #region "Public Properties"
        //ComponentId
        public int? ComponentId
        {
            get { return _ComponentId; }
            set { _ComponentId = value; }
        }
        //WorksRequired
        public string WorksRequired
        {
            get { return _worksRequired; }
            set { _worksRequired = value; }
        }

        //Value ID
        public int? ValueId
        {
            get { return _ValueId; }
            set { _ValueId = value; }
        }


        #endregion

    }
}
