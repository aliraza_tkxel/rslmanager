﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_BusinessObject.SchemeBlock
{
    public class ProvisionsBO
    {
        public int ProvisionId { get; set; }
        public int ProvisionCategoryId { get; set; }
        public string Description { get; set; }
        public int Type { get; set; }
        public int Quantity { get; set; }
        public string Location { get; set; }
        public int SupplierId { get; set; }
        public string Model { get; set; }
        public string SerialNumber { get; set; }
        public string BHG { get; set; }
        public string GasAppliance { get; set; }
        public bool Sluice { get; set; }
        public DateTime PurchasedDate { get; set; }
        public Decimal PurchasedCost { get; set; }
        public bool Warranty { get; set; }
        public bool Owned { get; set; }
        public DateTime? LeaseExpiryDate { get; set; }
        public Decimal LeaseCost { get; set; }
        public int LifeSpan { get; set; }
        public DateTime? LastReplaced { get; set; }
        public DateTime? ReplacementDue { get; set; }
        public int Condition { get; set; }
        public bool DuctServicing { get; set; }


        public int ServicingProviderId { get; set; }
        public bool ServicingRequired { get; set; }
        //public bool DuctServicing { get; set; }
        public DateTime? LastServicingDate { get; set; }
        public string ServiceCycle { get; set; }
        public DateTime? NextServiceDate { get; set; }


        public bool ProvisionCharge { get; set; }
        public bool PATTesting { get; set; }      
        public Decimal? AnnualAppointmentPC { get; set; }
        public int BlockPC { get; set; }
        public List<ExInProperties> excludedIncludedPropertiesPC { get; set; }
        public int? ProvisionParentId { get; set; }
        public int? ExistingProvisionId { get; set; }
        public int? SchemeId { get; set; }
        public int? BlockId { get; set; }
        public bool IsActive { get; set; }

        public int UpdatedBy { get; set; }
        
    }
}
