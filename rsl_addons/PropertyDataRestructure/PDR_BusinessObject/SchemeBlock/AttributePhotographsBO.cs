﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_BusinessObject.SchemeBlock
{
    public class AttributePhotographsBO
    {
        public string PropertyId { get; set; }
        public int? SchemeId { get; set; }
        public int? BlockId { get; set; }
        public string Title { get; set; }
        public DateTime UploadDate { get; set; }
        public string ImageName { get; set; }
        public string FilePath { get; set; }
        public int CreatedBy { get; set; }
        public int ItemId { get; set; }
        public int? heatingMappingId { get; set; }


    }
}
