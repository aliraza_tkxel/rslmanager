﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_BusinessObject.SchemeBlock
{
    public class ItemDatesControlBO
    {
        #region "Private"
        private int _paramId;
        private string _lastDone;
        private string _dueDate;
        private string _parameterName;
        private int? _componentId;
        #endregion
        #region "Public Properties"
        // Param ID
        public int ParameterId
        {
            get { return _paramId; }
            set { _paramId = value; }
        }
        //Last Done
        public string LastDone
        {
            get { return _lastDone; }
            set { _lastDone = value; }
        }

        //Due Date
        public string DueDate
        {
            get { return _dueDate; }
            set { _dueDate = value; }
        }

        //Parameter Name
        public string ParameterName
        {
            get { return _parameterName; }
            set { _parameterName = value; }
        }
        // component ID
        public int? ComponentId
        {
            get { return _componentId; }
            set { _componentId = value; }
        }

        #endregion


    }
}
