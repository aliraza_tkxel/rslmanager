﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_BusinessObject.CyclicalServices
{
   public class ContractorWorkBO
    {
        

        public int? ServiceItemId { get; set; }

        public int? ContractorId { get; set; }

        public int? ContactId { get; set; }

        public int? ExpenditureId { get; set; }

        public decimal? ContractNetValue { get; set; }

        public int? VatId { get; set; }

        public decimal? Vat { get; set; }

        public decimal? ContractGrossValue { get; set; }

        public bool? SendApprovalLink { get; set; }

        public DateTime? Commence { get; set; }

        public int? AssignedBy { get; set; }

        public DateTime? AssignedDate { get; set; }

        public int? POStatus { get; set; }
        public string RequiredWorks { get; set; }

       
    }
}
