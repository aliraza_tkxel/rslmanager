﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_BusinessObject.CyclicalServices
{
   public class ServicesAllocatedResponseBO
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<AllocatedServicesDetailBO> data { get; set; }
    }
    public class AllocatedServicesDetailBO
    {
        public int ServiceItemId { get; set; }        
        public string SchemeName { get; set; }
        public string BlockName { get; set; }
        public string PostCode { get; set; }
        public string PORef { get; set; }
        public string ServiceRequired { get; set; }
        public string Contractor { get; set; }
        public int ContractorId { get; set; }
        public string Status { get; set; }
        public int StatusId { get; set; }
        public string Commencement { get; set; }
        public string ContractStart { get; set; }
		
    }
}
