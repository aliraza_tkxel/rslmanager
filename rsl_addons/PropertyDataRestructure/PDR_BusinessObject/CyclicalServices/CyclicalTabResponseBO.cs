﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_BusinessObject.CyclicalServices
{
   public class CyclicalTabResponseBO
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<CyclicalTabDetailBO> data { get; set; }
    }
   public class CyclicalTabDetailBO
   {
       public int ServiceItemId { get; set; }
       public string SchemeName { get; set; }
       public string BlockName { get; set; }
       public string Cycle { get; set; }
       public string PORef { get; set; }
       public string ServiceRequired { get; set; }
       public string Contractor { get; set; }
       public string RecentCompletion { get; set; }
       public string Status { get; set; }
       public int RemainingCycle { get; set; }
       public string Commencement { get; set; }
       public string NextCycle { get; set; }

   }

}
