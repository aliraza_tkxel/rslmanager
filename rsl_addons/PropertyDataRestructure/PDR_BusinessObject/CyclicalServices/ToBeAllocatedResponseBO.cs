﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_BusinessObject.CyclicalServices
{
    public class ToBeAllocatedResponseBO
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<ServicesItemDetailBO> data { get; set; }
    }
    public class ServicesItemDetailBO
    {
        public int ServiceItemId { get; set; }
        public int? SchemeId { get; set; }
        public int? BlockId { get; set; }
        public string SchemeName { get; set; }
        public string BlockName { get; set; }
        public string PostCode { get; set; }
        public string CyclePeriod { get; set; }
        public string Attribute { get; set; }
        public string Contractor { get; set; }
        public int ContractorId { get; set; }
        public string Status { get; set; }
        public int StatusId { get; set; }
        public string Commencement { get; set; }
        public string VAT { get; set; }
        public int? totalValue { get; set; }
        public string ContractStart { get; set; }

    }
}
