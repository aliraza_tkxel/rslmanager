﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_BusinessObject.CyclicalServices
{
   public class CyclicalServicesDetailBO
    {
        public int serviceItemId { get; set; }
        public string scheme { get; set; }
        public string commencement { get; set; }
        public string cycle { get; set; }
        public int cycles { get; set; }
        public double cycleValue { get; set; }
        public double value { get; set; }
        public int cycleDays { get; set; }
        public string endDate { get; set; }
        public string cycleType { get; set; }
        public int cyclePeriod { get; set; }
    }
}
