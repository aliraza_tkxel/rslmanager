﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_BusinessObject.CyclicalServices
{
   public class GeneratePoRequestBO
    {
        public string contractorId { get; set; }
        public string contactId { get; set; }
        public string expenditureId { get; set; }
        public string headId { get; set; }
        public string moreDetail { get; set; }
        public string sendApprovalLink { get; set; }
        public string vatId { get; set; }
        public string assignedBy { get; set; }
        public List<AttributeDetail> attribute { get; set; }
        public List<RequiredWorkDetail> requiredWorks { get; set; }
    }
   public class AttributeDetail
   {
       public string ServiceItemId { get; set; }
       public string Commence { get; set; }
       public string CycleValue { get; set; }
       public string Cycles { get; set; }
       public string CycleDays { get; set; }
       public string EndDate { get; set; }
       public string CycleType { get; set; }
       public int cyclePeriod { get; set; }
   }

   public class RequiredWorkDetail
   {
       public string ServiceItemId { get; set; }
       public string worksRequired { get; set; }
       public string Net { get; set; }
       public string Vat { get; set; }
       public string Gross { get; set; }
   }
}
