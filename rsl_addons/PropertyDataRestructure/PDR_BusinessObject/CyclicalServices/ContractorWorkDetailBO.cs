﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_BusinessObject.CyclicalServices
{
   public class ContractorWorkDetailBO
    {
       public int ServiceItemId { get; set; }

        public DateTime? CycleDate { get; set; }

        public decimal? NetCost { get; set; }

        public int? VatId { get; set; }

        public decimal? VAT { get; set; }

        public decimal? GrossCost { get; set; }
        public string WorksRequired { get; set; }
    }
}
