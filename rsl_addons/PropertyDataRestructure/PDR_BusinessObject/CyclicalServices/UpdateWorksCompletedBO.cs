﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_BusinessObject.CyclicalServices
{
   public class UpdateWorksCompletedBO
    {
        public int workDetailId { get; set; }
        public int cmContractorId { get; set; }
        public DateTime? cycleCompleted { get; set; }
        public string status { get; set; }
        public int purchaseOrderItemId { get; set; }
        public int purchaseOrderId { get; set; }
        public string file { get; set; }
        public string transactionIdentity { get; set; }

    }
}
