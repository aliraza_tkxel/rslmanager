﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_BusinessObject.CyclicalServices
{
   public  class CancelledOrderBO
    {
        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public List<CancelledOrderDetailBO> data { get; set; }
    }
   public class CancelledOrderDetailBO
   {
       public string OrderId { get; set; }
       public string CancelDate { get; set; }
       public string PONumber { get; set; }
       public string CancelledBy { get; set; }
       public string Reason { get; set; }
       public string Scheme { get; set; }
       public string Block { get; set; }
       public string Attribute { get; set; }
       public string Contractor { get; set; }
      
   }
}
