﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_BusinessObject.PaintPacks
{
   public class PaintPacksBO
    {
        public int PaintPackDetailId { get; set; }
        public int PaintPackId { get; set; }
        public string PaintName { get; set; }
        public string PaintColor { get; set; }
        public string PaintRef { get; set; }
        public int Status { get; set; }
        public int Supplier { get; set; }
        public DateTime DeliveryDue { get; set; }
    }
}
