﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_BusinessObject.Asbestos
{
    public class AsbestosBO
    {
        int _asbestosLevelId;
        int _asbestosElementId;
        string _riskId;
        DateTime _addedDate;
        string _removedDate;
        string _notes;
        int _id;
        int _userId;
        string _propasbLevelId;
        string _requestType;
        // Property Asbestos Level Id
        public int AsbestosLevelId
        {
            get
            {
                return _asbestosLevelId;
            }
            set
            {
                _asbestosLevelId = value;
            }
        }

        public int AsbestosElementId
        {
            get
            {
                return _asbestosElementId;
            }
            set
            {
                _asbestosElementId = value;
            }
        }

        public string RiskId
        {
            get
            {
                return _riskId;
            }
            set
            {
                _riskId = value;
            }
        }

        public DateTime AddedDate
        {
            get
            {
                return _addedDate;
            }
            set
            {
                _addedDate = value;
            }
        }

        public string RemovedDate
        {
            get
            {
                return _removedDate;
            }
            set
            {
                _removedDate = value;
            }
        }

        public string Notes
        {
            get
            {
                return _notes;
            }
            set
            {
                _notes = value;
            }
        }

        public int Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }

        public int UserId
        {
            get
            {
                return _userId;
            }
            set
            {
                _userId = value;
            }
        }

        public string PropasbLevelID
        {
            get
            {
                return _propasbLevelId;
            }
            set
            {
                _propasbLevelId = value;
            }
        }

        public string RequestType
        {
            get
            {
                return _requestType;
            }
            set
            {
                _requestType = value;
            }
        }
        public int RiskLevel { get; set; }
        public bool UrgentActionRequired { get; set; }
    }
}
