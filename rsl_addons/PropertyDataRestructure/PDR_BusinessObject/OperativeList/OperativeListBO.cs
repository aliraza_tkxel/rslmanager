﻿using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;

// -----------------------------------------------------------------------
// <copyright file="OperativeListBO.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace PDR_BusinessObject.OperativeList
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class OperativeListBO
    {

        #region "Private"
        private int _tradeId;
        private decimal _duration;
        private int _sorder;
        #endregion


        #region "Public"
        [RangeValidator(0, RangeBoundaryType.Inclusive, 100, RangeBoundaryType.Inclusive, MessageTemplate = "Please select trade. <br/>")]
        public int TradeId
        {
            get { return _tradeId; }
            set { _tradeId = value; }
        }
        [RangeValidator(typeof(decimal), "0.00", RangeBoundaryType.Inclusive, "0.00", RangeBoundaryType.Ignore, MessageTemplate = "Please select duration. <br/>")]
        public decimal Duration
        {
            get { return _duration; }
            set { _duration = value; }
        }

        public int SOrder
        {
            get { return _sorder; }
            set { _sorder = value; }
        }

        #endregion
    }

}
