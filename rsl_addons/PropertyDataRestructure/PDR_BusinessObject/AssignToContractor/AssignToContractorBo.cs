﻿ // -----------------------------------------------------------------------
// <copyright file="AssignToContractorBO.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace PDR_BusinessObject.AssignToContractor
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Data;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class AssignToContractorBo
    {

        #region "Constructor"

        public AssignToContractorBo()
        {

            PdrContractorId = -1;
            JournalId = -1;
            CostCenterId = -1;
            BudgetHeadId = -1;
            ExpenditureId = -1;
            ContractorId = -1;
            ContractorName = string.Empty;
            ContactId = -1;
            ServiceRequired = string.Empty;
            LifeCycle = string.Empty;
            ServiceDue = string.Empty;
            Estimate = 0.0M;
            EstimateRef = string.Empty;
            ContractStartDate = string.Empty;
            ContractEndDate = string.Empty;
            EmpolyeeId = -1;
            POStatus = -1;
            SchemeId = -1;
            BlockId = -1;
            UserId = -1;
            MsatType = string.Empty;
        }
        #endregion

        #region "Properties"

        public int PdrContractorId { get; set; }
        public int? AttributeTypeId { get; set; }
        public int? ProvisionId { get; set; }
        public string ProvisionCategory { get; set; }
        public string ProvisionDescription { get; set; }

        public int? ItemId { get; set; }
        public int JournalId { get; set; }
        public int CostCenterId { get; set; }
        public int BudgetHeadId { get; set; }
        public int ExpenditureId { get; set; }
        public int ContractorId { get; set; } 
        public string ContractorName { get; set; }
        public int ContactId { get; set; }
        public string POName { get; set; }
        public string ServiceRequired { get; set; }
        public string LifeCycle { get; set; }
        public string ServiceDue { get; set; }
        public decimal Estimate { get; set; }
        public string EstimateRef { get; set; }
        public string ContractStartDate { get; set; }
        public string ContractEndDate { get; set; }
        public int EmpolyeeId { get; set; }
        public int POStatus { get; set; }
        public int? SchemeId { get; set; }
        public int? BlockId { get; set; }
        public string PropertyId { get; set; }
        public DataTable ServiceRequiredDt { get; set; }
        public int UserId { get; set; }
        public string MsatType { get; set; }
        public int? MSATTypeId { get; set; }
        //this property will used in Required works assign to contractor
        public string RequiredWorkIds { get; set; }
        public int PaintPackId { get; set; }
        public int InspectionJournalId { get; set; }
        public int OrderId { get; set; }
        public bool? IncInSChge { get; set; }        
        public decimal? PropertyApportionment { get; set; }
        #endregion

    }

}
