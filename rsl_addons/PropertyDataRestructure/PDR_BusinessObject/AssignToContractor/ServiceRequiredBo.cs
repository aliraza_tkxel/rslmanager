﻿// -----------------------------------------------------------------------
// <copyright file="WorkRequiredBo.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;

namespace PDR_BusinessObject.AssignToContractor
{


    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class ServiceRequiredBo
    {

        #region "Properties"

        #region "Work Detail Id"        
        public int WorkDetailId { get; set; }
        #endregion

        #region "Service Required"

        public string ServiceRequired { get; set; }

        #endregion

        #region "Net Cost"
        //<TypeConversionValidator(GetType(Decimal), MessageTemplate:="Net cost must be a number.")> _
        [RangeValidator(typeof(decimal), "0.00", RangeBoundaryType.Inclusive, "0.00", RangeBoundaryType.Ignore, MessageTemplate = "Net Cost must be a positive number.")]
        public decimal NetCost { get; set; }
        #endregion

        #region "Vat Drop Down Value"

        [NotNullValidator(MessageTemplate = "Please Select Vat.")]
        [TypeConversionValidator(typeof(int), MessageTemplate = "Please Select Vat.")]
        [RangeValidator(typeof(int), "0", RangeBoundaryType.Inclusive, "0", RangeBoundaryType.Ignore, MessageTemplate = "Please Select Vat.")]
        public int VatIdDDLValue { get; set; }

        #endregion

        #region "Vat Amount"

        [TypeConversionValidator(typeof(decimal), MessageTemplate = "VAT must be a number.")]
        [RangeValidator(typeof(decimal), "0.00", RangeBoundaryType.Inclusive, "0.00", RangeBoundaryType.Ignore, MessageTemplate = "VAT must be a positive number.")]
        public decimal Vat { get; set; }

        #endregion

        #region "Total"

        [TypeConversionValidator(typeof(decimal), MessageTemplate = "Total must be a number.")]
        [RangeValidator(typeof(decimal), "0.00", RangeBoundaryType.Inclusive, "0.00", RangeBoundaryType.Ignore, MessageTemplate = "Total must be a positive number.")]
        public decimal Total { get; set; }

        #endregion

        #region "Expenditure Id"

        [TypeConversionValidator(typeof(int), MessageTemplate = "You must select a valid expenditure from list.")]
        [RangeValidator(typeof(int), "0", RangeBoundaryType.Exclusive, "0", RangeBoundaryType.Ignore, MessageTemplate = "You must select a valid expenditure from list.")]
        public int ExpenditureId { get; set; }

        #endregion

        #region "Budget Head Id"

        [TypeConversionValidator(typeof(int), MessageTemplate = "You must select a valid Budget Head from list.")]
        [RangeValidator(typeof(int), "0", RangeBoundaryType.Exclusive, "0", RangeBoundaryType.Ignore, MessageTemplate = "You must select a valid Budget Head from list.")]
        public int BudgetHeadId { get; set; }

        #endregion

        #region "Cost Center Id"

        [TypeConversionValidator(typeof(int), MessageTemplate = "You must select a valid Cost Center from list.")]
        [RangeValidator(typeof(int), "0", RangeBoundaryType.Exclusive, "0", RangeBoundaryType.Ignore, MessageTemplate = "You must select a valid Cost Center from list.")]
        public int CostCenterId { get; set; }

        #endregion

        #region "Is Pending"

        public bool IsPending { get; set; }

        #endregion

        #region "Vat Rate"

        public decimal VatRate { get; set; }

        #endregion

        #endregion

    }

}
