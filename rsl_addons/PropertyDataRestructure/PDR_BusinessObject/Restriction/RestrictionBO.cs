﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_BusinessObject.Restriction
{
    public class RestrictionBO
    {
        public int RestrictionId { get; set; }
        public string PermittedPlanning { get; set; }
        public string RelevantPlanning { get; set; }
        public string RelevantTitle { get; set; }
        public string RestrictionComments { get; set; }
        public string AccessIssues { get; set; }
        public string MediaIssues { get; set; }
        public string ThirdPartyAgreement { get; set; }
        public string SpFundingArrangements { get; set; }
        public int? IsRegistered { get; set; }
        public string ManagementDetail { get; set; }
        public string NonBhaInsuranceDetail { get; set; }
        public int? SchemeId { get; set; }
        public int? BlockId { get; set; }
        public string PropertyId { get; set; }
        public int UpdatedBy { get; set; }

    }
}
