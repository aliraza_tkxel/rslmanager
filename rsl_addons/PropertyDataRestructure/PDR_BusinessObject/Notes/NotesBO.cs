﻿// -----------------------------------------------------------------------
// <copyright file="NotesBO.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace PDR_BusinessObject.Notes
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Microsoft.Practices.EnterpriseLibrary.Validation;
    using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class NotesBO
    {

        #region "Attributes"

        private string _customerNotes;
        private string _appointmentNotes;

        #endregion

        #region "Constructor"

        public NotesBO()
        {
            _customerNotes = string.Empty;
            _appointmentNotes = string.Empty;

        }
        #endregion

        #region "Properties"

        [StringLengthValidator(0, 1000)]
        public string AppointmentNotes
        {
            get { return _appointmentNotes; }
            set { _appointmentNotes = value; }
        }

        [StringLengthValidator(0, 1000)]
        public string CustomerNotes
        {
            get { return _customerNotes; }
            set { _customerNotes = value; }
        }

        #endregion

    }

}
