﻿// -----------------------------------------------------------------------
// <copyright file="VatBo.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace PDR_BusinessObject.Vat
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using PDR_BusinessObject.Vat;
    using PDR_BusinessObject.DropDown;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class VatBo : DropDownBO
    {

        #region "Constructor(s)"

        public VatBo(int id, string name, decimal vatRate)
            : base(id, name)
        {           
            _vatRate = vatRate;
        }

        #endregion

        #region "Properties"

        private decimal _vatRate;
        public decimal VatRate
        {
            get { return _vatRate; }
        }

        #endregion

    }

}
