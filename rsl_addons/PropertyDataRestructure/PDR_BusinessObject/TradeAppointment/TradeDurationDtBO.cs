﻿// -----------------------------------------------------------------------
// <copyright file="TradeDurationDtBO.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Data;

namespace PDR_BusinessObject.TradeAppointment
{

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class TradeDurationDtBO : IDtBO
    {

        public DataTable dt;

        public DataRow dr;
        public string tradeId = string.Empty;
        public string duration = string.Empty;
        public string durationString = string.Empty;
        public string tradeName = string.Empty;

        public string sOrder = string.Empty;
        private const string tradeIdColName = "TradeId";
        private const string durationColName = "Duration";
        private const string durationStringColName = "DurationString";
        private const string tradeColName = "TradeName";

        public const string sOrderColName = "SortOrder";
        public TradeDurationDtBO()
        {
            createDataTable();
        }

        public void addNewDataRow()
        {
            dr = dt.NewRow();
            dr[tradeIdColName] = tradeId;
            dr[durationStringColName] = durationString;
            dr[durationColName] = duration;
            dr[tradeColName] = tradeName;
            dr[sOrderColName] = sOrder;
            dt.Rows.Add(dr);
        }

        


       public  void createDataTable()
        {
            dt = new DataTable();
            dt.Columns.Add(tradeIdColName);
            dt.Columns.Add(durationStringColName);
            dt.Columns.Add(durationColName);
            dt.Columns.Add(tradeColName);
            dt.Columns.Add(sOrderColName);
        }
    }
}
