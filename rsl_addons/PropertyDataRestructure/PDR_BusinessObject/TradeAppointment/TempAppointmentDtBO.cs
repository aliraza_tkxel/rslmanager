﻿// -----------------------------------------------------------------------
// <copyright file="TempAppointmentDtBO.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace PDR_BusinessObject.TradeAppointment
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Data;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class TempAppointmentDtBO : IDtBO
    {

        public string operative = string.Empty;
        public string startDateString = string.Empty;
        public string endDateString = string.Empty;
        public string lastColumn = "--";
        public int operativeId = 0;
        public string startTime = string.Empty;
        public string endTime = string.Empty;
        public int tradeId = 0;
        public bool isPatchSelected { get; set; }
        public bool lastAdded = true;
        public DataTable dt;

        private DataRow dr;
        
        public const string operativeColName = "Operative:";
        public const string startDateStringColName = "Start date:";
        public const string endDateStringColName = "End date:";
        public const string lastColName = " ";
        public const string operativeIdColName = "OperativeId";
        public const string startTimeColName = "StartTime";
        public const string endTimeColName = "EndTime";
        public const string tradeIdColName = "TradeId";
        public const string lastAddedColName = "LastAdded:";

        public TempAppointmentDtBO()
        {
            this.createDataTable();
        }

        public void createDataTable()
        {
            dt = new DataTable();
            dt.Columns.Add(operativeColName);
            dt.Columns.Add(startDateStringColName);
            dt.Columns.Add(endDateStringColName);
            dt.Columns.Add(lastColName);
            dt.Columns.Add(operativeIdColName);
            dt.Columns.Add(startTimeColName);
            dt.Columns.Add(endTimeColName);
            dt.Columns.Add(tradeIdColName);
            dt.Columns.Add(lastAddedColName);
        }

        public void addNewDataRow()
        {
            dr = dt.NewRow();
            dr[operativeColName] = operative;
            dr[startDateStringColName] = startDateString;
            dr[endDateStringColName] = endDateString;
            dr[lastColName] = lastColumn;
            dr[operativeIdColName] = operativeId;
            dr[startTimeColName] = startTime;
            dr[endTimeColName] = endTime;
            dr[tradeIdColName] = tradeId;
            dr[lastAddedColName] = lastAdded;
            dt.Rows.Add(dr);
        }
    
    }
}
