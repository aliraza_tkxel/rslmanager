﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_BusinessObject.Warranties
{
    public class WarrantyBO
    {
        private int _warrantyType;

        public int WarrantyType
        {
            get
            {
                return _warrantyType;
            }
            set
            {
                _warrantyType = value;
            }
        }
        private int _category;

        public int Category
        {
            get
            {
                return _category;
            }
            set
            {
                _category = value;
            }
        }
        private int? _area;

        public int? Area
        {
            get
            {
                return _area;
            }
            set
            {
                _area = value;
            }
        }
        private int? _item;

        public int? Item
        {
            get
            {
                return _item;
            }
            set
            {
                _item = value;
            }
        }
        private int _contractor;

        public int Contractor
        {
            get
            {
                return _contractor;
            }
            set
            {
                _contractor = value;
            }
        }

        private DateTime _expiry;

        public DateTime Expiry
        {
            get
            {
                return _expiry;
            }
            set
            {
                _expiry = value;
            }
        }
        private string _notes;

        public string Notes
        {
            get
            {
                return _notes;
            }
            set
            {
                _notes = value;
            }
        }
        private int _id;

        public int Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }
        private string _requestType;

        public string RequestType
        {
            get
            {
                return _requestType;
            }
            set
            {
                _requestType = value;
            }
        }

        private int _warrantyId;
        public int WarrantyId
        {
            get
            {
                return _warrantyId;
            }
            set
            {
                _warrantyId = value;
            }
        }
    }
}
