﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_BusinessObject.Block
{
    public class BlockBO
    {
        private string _blockName;

        public string BlockName
        {
            get
            {
                return _blockName;
            }
            set
            {
                _blockName = value;
            }
        }
        private string _blockReference;

        public string BlockReference
        {
            get
            {
                return _blockReference;
            }
            set
            {
                _blockReference = value;
            }
        }

        private int _developmentId;

        public int DevelopmentId
        {
            get
            {
                return _developmentId;
            }
            set
            {
                _developmentId = value;
            }
        }

        private int _phaseId;

        public int PhaseId
        {
            get
            {
                return _phaseId;
            }
            set
            {
                _phaseId = value;
            }
        }

        private string _address1;

        public string Address1
        {
            get
            {
                return _address1;
            }
            set
            {
                _address1 = value;
            }
        }

        private string _address2;

        public string Address2
        {
            get
            {
                return _address2;
            }
            set
            {
                _address2 = value;
            }
        }
        private string _address3;

        public string Address3
        {
            get
            {
                return _address3;
            }
            set
            {
                _address3 = value;
            }
        }

        private string _county;

        public string County
        {
            get
            {
                return _county;
            }
            set
            {
                _county = value;
            }
        }
        private string _postCode;

        public string PostCode
        {
            get
            {
                return _postCode;
            }
            set
            {
                _postCode = value;
            }
        }

        private string _townCity;

        public string TownCity
        {
            get
            {
                return _townCity;
            }
            set
            {
                _townCity = value;
            }
        }
        private int _ownerShip;

        public int OwnerShip
        {
            get
            {
                return _ownerShip;
            }
            set
            {
                _ownerShip = value;
            }
        }
        private Nullable<DateTime> _buildDate;

        public Nullable<DateTime> BuildDate
        {
            get
            {
                return _buildDate;
            }
            set
            {
                _buildDate = value;
            }
        }
        private int _noOfProperties;

        public int NoOfProperties
        {
            get
            {
                return _noOfProperties;
            }
            set
            {
                _noOfProperties = value;
            }
        }
        private string _blockId;

        public string BlockId
        {
            get
            {
                return _blockId;
            }
            set
            {
                _blockId = value;
            }
        }
        public int ExistingBlockId { get; set; }

    }
}
