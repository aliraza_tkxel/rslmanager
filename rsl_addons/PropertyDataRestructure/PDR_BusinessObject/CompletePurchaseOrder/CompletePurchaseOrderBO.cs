﻿using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using Microsoft.Practices.EnterpriseLibrary.Validation;

namespace PDR_BusinessObject.CompletePurchaseOrder
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System;
    using System.Data;
    public class CompletePurchaseOrderBO
    {
        #region "Attributes"
        private int _faultLogId;
        private string _faultRepairIdList;
        private string _time;
        private DateTime _inspectionDate;
        private int _userId;
        private bool _followOnStatus;
        private string _FollowOnNotes;

        //starts Email details
        private string _ContactorName;
        private string _ContactName;
        private string _ContactorEmailAddress;
        private string _SupplierEmailAddress;
        private string _PoRef;
        private string _JsRef;
        private string _PoStatus;
        private string _OrderedBy;
        private string _DirectDial;
        private string _TanentName;
        private string _TanentAddress;
        private string _TownCity;
        private string _Country;
        private string _PostCode;
        private string _DueDate;
        private string _NetCost;
        private string _VAT;
        private string _Total;
        private string _EstimatedCompletionDate;
        private string _ReportedFault;
        private string _WorksRequired;
        private string _VulnerabilityDetails;
        private string _AsbestosDetails;
        private string _RiskDetails;
        private int _EmpId;
        private int _OrderId;
        private string _Telephone;
        private string _TotalNetCost;
        private string _TotalVAT;
        private string _TotalGROSS;
        //End email details

        #endregion

        #region "Properties"
        // Get / Set property for _faultLogId
        public int FaultLogId
        {

            get { return _faultLogId; }

            set { _faultLogId = value; }
        }
        // Get / Set property for _faultRepairIdList
        public string FaultRepairIdList
        {

            get { return _faultRepairIdList; }

            set { _faultRepairIdList = value; }
        }
        // Get / Set property for _time
        public string Time
        {

            get { return _time; }

            set { _time = value; }
        }
        // Get / Set property for _inspectionDate
        public DateTime InspectionDate
        {

            get { return _inspectionDate; }

            set { _inspectionDate = value; }
        }

        // Get / Set property for _userId
        public int UserId
        {

            get { return _userId; }

            set { _userId = value; }
        }
        public bool followOnStatus
        {
            get { return _followOnStatus; }
            set { _followOnStatus = value; }
        }
        public string FollowOnNotes
        {
            get { return _FollowOnNotes; }
            set { _FollowOnNotes = value; }
        }

        //Get / Set property for Email details 

        public string ContactorName
        {
            get { return _ContactorName; }
            set { _ContactorName = value; }
        }

        public string ContactName
        {
            get { return _ContactName; }
            set { _ContactName = value; }
        }
        public string ContactorEmailAddress
        {
            get { return _ContactorEmailAddress; }
            set { _ContactorEmailAddress = value; }
        }
        public string SupplierEmailAddress
        {
            get { return _SupplierEmailAddress; }
            set { _SupplierEmailAddress = value; }
        }
        public string PoRef
        {
            get { return _PoRef; }
            set { _PoRef = value; }
        }
        public string JsRef
        {
            get { return _JsRef; }
            set { _JsRef = value; }
        }
        public string PoStatus
        {
            get { return _PoStatus; }
            set { _PoStatus = value; }
        }
        public string OrderedBy
        {
            get { return _OrderedBy; }
            set { _OrderedBy = value; }
        }
        public string DirectDial
        {
            get { return _DirectDial; }
            set { _DirectDial = value; }
        }
        public string TanentName
        {
            get { return _TanentName; }
            set { _TanentName = value; }
        }
        public string TanentAddress
        {
            get { return _TanentAddress; }
            set { _TanentAddress = value; }
        }

        public string TownCity
        {
            get { return _TownCity; }
            set { _TownCity = value; }
        }

        public string Country
        {
            get { return _Country; }
            set { _Country = value; }
        }
        public string PostCode
        {
            get { return _PostCode; }
            set { _PostCode = value; }
        }
        public string DueDate
        {
            get { return _DueDate; }
            set { _DueDate = value; }
        }
        public string NetCost
        {
            get { return _NetCost; }
            set { _NetCost = value; }
        }
        public string VAT
        {
            get { return _VAT; }
            set { _VAT = value; }
        }
        public string Total
        {
            get { return _Total; }
            set { _Total = value; }
        }
        public string EstimatedCompletionDate
        {
            get { return _EstimatedCompletionDate; }
            set { _EstimatedCompletionDate = value; }
        }
        public string ReportedFault
        {
            get { return _ReportedFault; }
            set { _ReportedFault = value; }
        }
        public string WorksRequired
        {
            get { return _WorksRequired; }
            set { _WorksRequired = value; }
        }
        public string AsbestosDetails
        {
            get { return _AsbestosDetails; }
            set { _AsbestosDetails = value; }
        }
        public string RiskDetails
        {
            get { return _RiskDetails; }
            set { _RiskDetails = value; }
        }
        public string VulnerabilityDetails
        {
            get { return _VulnerabilityDetails; }
            set { _VulnerabilityDetails = value; }
        }
        public int EmpId
        {
            get { return _EmpId; }
            set { _EmpId = value; }
        }
        public int OrderId
        {
            get { return _OrderId; }
            set { _OrderId = value; }
        }

        public string Telephone
        {
            get { return _Telephone; }
            set { _Telephone = value; }
        }



        public string TotalNetCost
        {
            get { return _TotalNetCost; }
            set { _TotalNetCost = value; }
        }
        public string TotalVAT
        {
            get { return _TotalVAT; }
            set { _TotalVAT = value; }
        }
        public string TotalGROSS
        {
            get { return _TotalGROSS; }
            set { _TotalGROSS = value; }
        }
        //End Get / Set property for Email details 

        #endregion

        #region "Constructor"
        public CompletePurchaseOrderBO()
        {
            _faultLogId = 0;
            _faultRepairIdList = string.Empty;
            _time = string.Empty;
            _inspectionDate = default(DateTime);
            _userId = 0;
            _followOnStatus = false;
            _FollowOnNotes = string.Empty;

            //starts Email details initialization
            _ContactorName = string.Empty;
            _ContactName = string.Empty;
            _ContactorEmailAddress = string.Empty;
            _SupplierEmailAddress = string.Empty;
            _PoRef = string.Empty;
            _JsRef = string.Empty;
            _PoStatus = string.Empty;
            _OrderedBy = string.Empty;
            _DirectDial = string.Empty;
            _TanentName = string.Empty;
            _TanentAddress = string.Empty;
            _TownCity = string.Empty;
            _Country = string.Empty;
            _PostCode = string.Empty;
            _DueDate = string.Empty;
            _NetCost = string.Empty;
            _VAT = string.Empty;
            _Total = string.Empty;
            _EstimatedCompletionDate = string.Empty;
            _ReportedFault = string.Empty;
            _WorksRequired = string.Empty;
            _AsbestosDetails = string.Empty;
            _VulnerabilityDetails = string.Empty;
            _RiskDetails = string.Empty;
            _EmpId = 0;
            _OrderId = 0;
            _Telephone = string.Empty;

            _TotalNetCost = string.Empty;
            _TotalVAT = string.Empty;
            _TotalGROSS = string.Empty;
            //End email details initialization
        }
        #endregion

        public string RepairNotes { get; set; }

    }
}
