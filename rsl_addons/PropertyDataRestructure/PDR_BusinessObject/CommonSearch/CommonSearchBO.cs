﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_BusinessObject.CommonSearch
{
    public class CommonSearchBO
    {
        public int ChecksRequiredType { get; set; }
        public string SearchText { get; set; }
        public CommonSearchBO()
        {
            ChecksRequiredType = 1;
            SearchText = string.Empty;
        }
    }
}
