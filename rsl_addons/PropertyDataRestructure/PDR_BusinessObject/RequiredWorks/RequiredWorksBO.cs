﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PDR_BusinessObject.TradeAppointment;

namespace PDR_BusinessObject.RequiredWorks
{
    public class RequiredWorksBO
    {
        public int RequiredWorksId { get; set; }
        public int InspectionJournalId { get; set; }
        public int JournalId { get; set; }
        public string Ref { get; set; }
        public string Location { get; set; }
        public string WorkDescription { get; set; }
        public string Appointment { get; set; }
        public decimal Neglect { get; set; }
        public bool IsTenantWorks { get; set; }
        public string Contractor { get; set; }
        public int LocationId { get; set; }
        public string Operative { get; set; }
        public int UpdatedBy { get; set; }
        public bool IsCanceled { get; set; }
        public decimal Duration { get; set; }
        public DateTime  TerminationDate{ get; set; }
        public DateTime  ReletDate { get; set; }
        public DateTime StartDate { get; set; }
        public string Address { get; set; }
        public int AppointmentId { get; set; }
        public string  AppointmentNotes { get; set; }
        public string JobSheetNotes { get; set; }
        public string WorkType { get; set; }
        public RequiredWorksBO()
        {

            Duration = 1;
            StartDate = DateTime.Now.Date;  
        }
        public TempAppointmentDtBO tempAppointmentDtBo = new TempAppointmentDtBO();
    }
}
