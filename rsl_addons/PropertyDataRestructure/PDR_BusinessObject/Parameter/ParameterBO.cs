﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace PDR_BusinessObject.Parameter
{
    public class ParameterBO
    {
        #region "Attributes"
        private string _name;
        private object _value;
        private DbType _type;
        private SqlDbType _sqlType;


        #endregion

        #region "Constructor"
        public ParameterBO(ref string name, ref object value, ref DbType type)
        {
            _name = name;
            _value = value;
            _type = type;

        }
        public ParameterBO(string name, object value, DbType type)
        {
            _name = name;
            _value = value;
            _type = type;

        }
        public ParameterBO( string name,  object value,  SqlDbType type)
        {
            _name = name;
            _value = value;
            _sqlType = type;

        }
        #endregion

        #region "Properties"
        public DbType Type
        {
            get { return _type; }
            set { _type = value; }
        }
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public object Value
        {
            get { return _value; }
            set { _value = value; }
        }
        public SqlDbType SqlType
        {
            get { return _sqlType; }
            set { _sqlType = value; }
        }
        #endregion
    }
}
