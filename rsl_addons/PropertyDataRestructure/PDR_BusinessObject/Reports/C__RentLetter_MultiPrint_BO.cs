﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
namespace PDR_BusinessObject.Reports
{
    public class C__RentLetter_MultiPrint_BO
    {
        public Nullable<int> YEARNUMBER { get; set; }
        public string PROPERTYID { get; set; }
        public decimal? RENT { get; set; }
        public decimal? SERVICES { get; set; }
        public decimal? COUNCILTAX { get; set; }
        public decimal? WATERRATES { get; set; }
        public decimal? INELIGSERV { get; set; }
        public decimal? SUPPORTEDSERVICES { get; set; }
        public decimal? GARAGE { get; set; }
        public decimal? TOTALRENT { get; set; }
        public decimal? t_totalrent { get; set; }
        public decimal? t_garageSpace { get; set; }
        public int? RENTTYPE { get; set; }
        public decimal? OLDTOTAL { get; set; }
        public string RENTTYPEOLD { get; set; }
        public Nullable<System.DateTime> DATERENTSET { get; set; }
        public Nullable<System.DateTime> RENTEFFECTIVE { get; set; }
        public decimal? TARGETRENT { get; set; }
        public decimal? YIELD { get; set; }
        public decimal? CAPITALVALUE { get; set; }
        public decimal? INSURANCEVALUE { get; set; }
        public int? CUSTOMERID { get; set; }
        public int? TENANCYID { get; set; }
        public string TITLE { get; set; }
        public string TenantName { get; set; }
        public string DearTenantName { get; set; }
        public string PAYMENTDATE { get; set; }
        public string FIRSTNAME { get; set; }
        public string LASTNAME { get; set; }
        public string HOUSENUMBER { get; set; }
        public string ADDRESS1 { get; set; }
        public string TOWNCITY { get; set; }
        public string POSTCODE { get; set; }
        public string COUNTY { get; set; }
        public int? SETRENTTO { get; set; }
        public int? TARGETRENTID { get; set; }
        public Nullable<System.DateTime> REASSESMENTDATE { get; set; }
        public int? GARAGE_COUNT { get; set; }
        public decimal? OLDGARAGECHARGE { get; set; }
        public decimal? NEWGARAGECHARGE { get; set; }
        public decimal? T_RENT { get; set; }
        public decimal? T_SERVICES { get; set; }
        public decimal? T_COUNCILTAX { get; set; }
        public decimal? T_WATERRATES { get; set; }
        public decimal? T_INELIGSERV { get; set; }
        public decimal? T_SUPPORTEDSERVICES { get; set; }
       
        public decimal? ADDITIONAL_GARAGE { get; set; }
   
        public string P_FULLADDRESS { get; set; }
        public string incomeOfficer { get; set; }
        public string WORKEMAIL { get; set; }
        public string WORKMOBILE { get; set; }
    }
}
