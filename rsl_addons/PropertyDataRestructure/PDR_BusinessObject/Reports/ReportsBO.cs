﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_BusinessObject.Reports
{
   public class ReportsBO
    {
        private DateTime _reportDate;

        public DateTime ReportDate
        {
            get { return _reportDate; }
            set { _reportDate = value; }
        }
        private int? _localAuthority;

        public int? LocalAuthority
        {
            get { return _localAuthority; }
            set { _localAuthority = value; }
        }
       private int? _scheme;

       public int? Scheme
       {
           get { return _scheme; }
           set { _scheme = value; }
       }
       private string _postCode;

       public string PostCode
       {
           get { return _postCode; }
           set { _postCode = value; }
       }
       private int? _patch;

       public int? Patch
       {
           get { return _patch; }
           set { _patch = value; }
       }
       private int? assetType;

       public int? AssetType
       {
           get { return assetType; }
           set { assetType = value; }
       }
       private int? _propertyType;

       public int? PropertyType
       {
           get { return _propertyType; }
           set { _propertyType = value; }
       }
       private string _beds;

       public string Beds
       {
           get { return _beds; }
           set { _beds = value; }
       }
       private string _occupancy;

       public string Occupancy
       {
           get { return _occupancy; }
           set { _occupancy = value; }
       }
       private int? _status;

       public int? Status
       {
           get { return _status; }
           set { _status = value; }
       }
       private int? _subStatus;

       public int? SubStatus
       {
           get { return _subStatus; }
           set { _subStatus = value; }
       }
       private int? _period;

       public int? Period
       {
           get { return _period; }
           set { _period = value; }
       }
       private string _searchText;

       public string SearchText
       {
           get { return _searchText; }
           set { _searchText = value; }
       }
       public string  Asset { get; set; }
       public string  Header { get; set; }
       public bool IsSoldChecked { get; set; }

    }
}
