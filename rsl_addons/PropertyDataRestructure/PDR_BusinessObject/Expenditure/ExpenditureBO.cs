﻿// -----------------------------------------------------------------------
// <copyright file="ExpenditureBO.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace PDR_BusinessObject.Expenditure
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using PDR_BusinessObject.DropDown;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class ExpenditureBO : DropDownBO
    {


        #region "Attributes"

        private decimal _limit;

        private decimal _remaining;
        #endregion

        #region "Constructor(s)"

        public ExpenditureBO(int id, string name, decimal limit, decimal remaining)
            : base(id, name)
        {
            _limit = limit;
            _remaining = remaining;
        }

        #endregion

        #region "Properties"

        public decimal Limit
        {
            get { return _limit; }
        }

        public decimal Remaining
        {
            get { return _remaining; }
        }

        #endregion

    }

}
