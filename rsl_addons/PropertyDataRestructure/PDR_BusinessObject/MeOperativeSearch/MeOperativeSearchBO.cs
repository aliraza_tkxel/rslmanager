﻿// -----------------------------------------------------------------------
// <copyright file="MeOperativeSearchBO.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using Microsoft.Practices.EnterpriseLibrary.Validation;


namespace PDR_BusinessObject.MeOperativeSearch
{

    /// <summary>
    /// TODO: Update summary.
    /// </summary>

    public class MeOperativeSearchBO
    {

        #region "Attributes"

        private string _worksRequired;
        private int _operativeTradeId;
        private double _duration;
        private DateTime _startDate;
        private int _type;

        #endregion

        #region "Properties"

        [RangeValidator(0, RangeBoundaryType.Inclusive, 100, RangeBoundaryType.Inclusive, MessageTemplate = "Please select Type. <br/>")]
        public int Type
        {
            get { return _type; }
            set { _type = value; }
        }

       
        public string Worksrequired
        {
            get { return _worksRequired; }
            set { _worksRequired = value; }
        }


        public int OperativeTradeId
        {
            get { return _operativeTradeId; }
            set { _operativeTradeId = value; }
        }


        public double Duration
        {
            get { return _duration; }
            set { _duration = value; }
        }

        public System.DateTime StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }

        #endregion

    }
}
