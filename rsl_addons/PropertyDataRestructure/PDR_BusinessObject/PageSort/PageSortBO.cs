﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_BusinessObject.PageSort
{
    [Serializable]
    public class PageSortBO
    {

        public PageSortBO(string sSDirection, string sExpression, int pNumber, int pSize)
        {
            SortExpression = sExpression;
            PageNumber = pNumber;
            PageSize = pSize;
            if (((sSDirection == "ASC")
                        || (sSDirection == "Ascending")))
            {
                SortDirection = "Ascending";
                SmallSortDirection = "ASC";
            }
            else
            {
                SortDirection = "Descending";
                SmallSortDirection = "DESC";
            }
        }

        private string _sortExpression;

        public string SortExpression
        {
            get
            {
                return _sortExpression;
            }
            set
            {
                _sortExpression = value;
            }
        }

        private string _sortDirection;

        public string SortDirection
        {
            get
            {
                return _sortDirection;
            }
            set
            {
                _sortDirection = value;
            }
        }

        private string _smallSortDirection;

        public string SmallSortDirection
        {
            get
            {
                return _smallSortDirection;
            }
            set
            {
                _smallSortDirection = value;
            }
        }

        private int _pageNumber;

        public int PageNumber
        {
            get
            {
                return _pageNumber;
            }
            set
            {
                _pageNumber = value;
            }
        }

        private int _pageSize;

        public int PageSize
        {
            get
            {
                return _pageSize;
            }
            set
            {
                _pageSize = value;
            }
        }

        private int _totalPages;

        public int TotalPages
        {
            get
            {
                return _totalPages;
            }
            set
            {
                _totalPages = value;
            }
        }

        private int _totalRecords;

        public int TotalRecords
        {
            get
            {
                return _totalRecords;
            }
            set
            {
                _totalRecords = value;
            }
        }

        public void setSortDirection()
        {
            if ((this.SortDirection == "Ascending"))
            {
                this.SortDirection = "Descending";
                this.SmallSortDirection = "DESC";
            }
            else
            {
                this.SortDirection = "Ascending";
                this.SmallSortDirection = "ASC";
            }
        }

       
    }
}
