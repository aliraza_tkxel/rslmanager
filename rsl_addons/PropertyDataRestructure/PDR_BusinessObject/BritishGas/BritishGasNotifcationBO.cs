﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using System.ComponentModel.DataAnnotations;

namespace PDR_BusinessObject.BritishGas
{
    public class BritishGasNotifcationBO
    {
        public int BritishGasNotificationId { get; set; }
        public int CustomerId { get; set; }
        public string PropertyId { get; set; }        
        public int TenancyId { get; set; }        
        public bool? IsGasElectricCheck { get; set; }        
        public int? GasCheckJournalId { get; set; }
        public int? ElectricCheckJournalId { get; set; }
        public string PropertyAddress1 { get; set; }
        public string PropertyAddress2 { get; set; }
        public string PropertyCity { get; set; }
        public string PropertyCounty { get; set; }
        public string PropertyPostCode { get; set; }
        public string CurrentTenantName { get; set; }
        public string TenantFwAddress1 { get; set; }
        public string TenantFwAddress2 { get; set; }
        public string TenantFwCity { get; set; }
        public string TenantFwPostCode { get; set; }
        public DateTime? OccupancyCeaseDate { get; set; }
        public string BritishGasEmail { get; set; }
        public int? GasMeterTypeId { get; set; }
        public Int64? GasMeterReading { get; set; }
        public DateTime? GasMeterReadingDate { get; set; }
        public int? ElectricMeterTypeId { get; set; }
        public Int64? ElectricMeterReading { get; set; }
        public DateTime? ElectricMeterReadingDate { get; set; }
        public decimal? GasDebtAmount { get; set; }
        public DateTime? GasDebtAmountDate { get; set; }
        public decimal? ElectricDebtAmount { get; set; }
        public DateTime? ElectricDebtAmountDate { get; set; }
        public string NewTenantName { get; set; }
        public DateTime? NewTenantDateOfBirth { get; set; }
        public string NewTenantTel{ get; set; }
        public string NewTenantMobile { get; set; }
        public DateTime? NewTenantOccupancyDate { get; set; }
        public string NewTenantPreviousAddress1{ get; set; }
        public string NewTenantPreviousAddress2 { get; set; }
        public string NewTenantPreviousTownCity { get; set; }
        public string NewTenantPreviousPostCode { get; set; }
        public Int64? NewGasMeterReading { get; set; }
        public DateTime? NewGasMeterReadingDate { get; set; }
        public Int64? NewElectricMeterReading { get; set; }
        public DateTime?  NewElectricMeterReadingDate{ get; set; }
        public int? StatusId { get; set; }
        public int? StageId { get; set; }
        public string StageName { get; set; }
        public string DocumentName { get; set; }
        public int UserId { get; set; }
        public bool IsNotificationSent { get; set; }

        public bool SendNotificationLater { get; set; }
    }
}
