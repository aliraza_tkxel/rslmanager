﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_BusinessObject.BritishGas
{
    public class BritishGasSearchBo
    {
        public string searchText { get; set; }
        public string notificationStatus { get; set; }
    }
}
