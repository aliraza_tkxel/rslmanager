﻿// -----------------------------------------------------------------------
// <copyright file="MeSearchBO.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace PDR_BusinessObject.MeSearch
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class MeSearchBO
    {

        #region "Attributes"
        private string _searchText;
        private string _msatType;
        private bool _check56Days;
        private int _schemeId;
        private int _blockId;
        private int _attributeTypeId;
        private string _notificationStatus;
        private string _ItemName;
        private int _ItemId;
        #endregion

        #region "Constructor"
        public MeSearchBO()
        {
            _searchText = string.Empty;
            _msatType = string.Empty;
            _check56Days = true;
            _schemeId = -1;
            _blockId = -1;
            _attributeTypeId = -1;
            _notificationStatus = string.Empty;
            _ItemName = string.Empty;
            _ItemId = -1;
        }
        #endregion

        #region "Properties"
        
        public string SearchText
        {
            get { return _searchText; }
            set { _searchText = value; }
        }

        public string MsatType
        {
            get { return _msatType; }
            set { _msatType = value; }
        }

        public bool Check56Days
        {
            get { return _check56Days; }
            set { _check56Days = value; }
        }

        public int SchemeId
        {
            get { return _schemeId; }
            set { _schemeId = value; }
        }

        public int BlockId
        {
            get { return _blockId; }
            set { _blockId = value; }
        }

        public int AttributeTypeId
        {
            get { return _attributeTypeId; }
            set { _attributeTypeId = value; }
        }

        /// <summary>
        /// The parameter for british gas notification
        /// </summary>
        public string NotificationStatus
        {
            get { return _notificationStatus; }
            set { _notificationStatus = value; }
        }

        public string ItemName
        {
            get { return _ItemName; }
            set { _ItemName = value; }
        } 

        public int ItemId
        {
            get { return _ItemId; }
            set { _ItemId = value; }
        }
        #endregion

    }
}
