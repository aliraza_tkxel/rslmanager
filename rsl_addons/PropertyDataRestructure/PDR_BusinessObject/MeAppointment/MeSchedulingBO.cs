﻿using System.Data;
// -----------------------------------------------------------------------
// <copyright file="MeSchedulingBO.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace PDR_BusinessObject.MeAppointment
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class MeSchedulingBO
    {

        /// <summary>
        /// This contains the record of property info in data table , which user selects for arranging the appointment
        /// </summary>
        /// <remarks></remarks>
        private DataTable _appointmentInfoDt;
        /// <summary>
        /// This contains the all component and trade information in data table, against the selected property.
        /// </summary>
        /// <remarks></remarks>
        private DataTable _allTradesDt;
        /// <summary>
        /// This contains the component and trade information (with complete status) in data table, against the selected property.
        /// </summary>
        /// <remarks></remarks>
        private DataTable _completeTradesDt;
        /// <summary>
        /// This contains the temporary component and trade information in data table, against the selected property.
        /// </summary>
        /// <remarks></remarks>
        private DataTable _tempTradesDt;
        /// <summary>
        /// This contains the confirmed component and trade information in data table, against the selected property.
        /// </summary>
        /// <remarks></remarks>
        private DataTable _confirmedTradesDt;
        /// <summary>
        /// This contains the removed component trade information in data table, against the selected property.
        /// </summary>
        /// <remarks></remarks>
        private DataTable _removedTradesDt;
        /// <summary>
        /// This contains the appointment record that is in pending state
        /// </summary>
        /// <remarks></remarks>
        private DataTable _confirmAppointmentsDt;
        /// <summary>
        /// This contains the the data table of all arranged appointments and their information
        /// </summary>
        /// <remarks></remarks>
        private DataTable _arrangedTradesAppointmentsDt;
        /// <summary>
        /// This contains the property info in data table
        /// </summary>
        /// <remarks></remarks>
        private DataTable _propertyInfoDt;


        #region "Constructor"
        public MeSchedulingBO()
        {
            _appointmentInfoDt = new DataTable();
            _allTradesDt = new DataTable();
            _tempTradesDt = new DataTable();
            _confirmedTradesDt = new DataTable();
            _removedTradesDt = new DataTable();
            _confirmAppointmentsDt = new DataTable();
            _arrangedTradesAppointmentsDt = new DataTable();
            _propertyInfoDt = new DataTable();
        }
        #endregion

        #region "Properties"
        /// <summary>
        /// This contains the record of property info in data table , which user selects for arranging the appointment
        /// </summary>
        /// <remarks></remarks>
        public DataTable AppointmentInfoDt
        {
            get { return _appointmentInfoDt; }
            set { _appointmentInfoDt = value; }
        }

        /// <summary>
        /// This contains the all component and trade information in data table, against the selected property.
        /// </summary>
        /// <remarks></remarks>
        public DataTable AllTradesDt
        {
            get { return _allTradesDt; }
            set { _allTradesDt = value; }
        }


        /// <summary>
        /// This contains the component and trade information (with complete appointment status) in data table, against the selected property.
        /// </summary>
        /// <remarks></remarks>
        public DataTable CompleteTradesDt
        {
            get { return _allTradesDt; }
            set { _allTradesDt = value; }
        }

        /// <summary>
        /// This contains the temp component and trade information in data table, against the selected property.
        /// </summary>
        /// <remarks></remarks>
        public DataTable TempTradesDt
        {
            get { return _tempTradesDt; }
            set { _tempTradesDt = value; }
        }

        /// <summary>
        /// This contains the confirmed component and trade information in data table, against the selected property.
        /// </summary>
        /// <remarks></remarks>
        public DataTable ConfirmedTradesDt
        {
            get { return _confirmedTradesDt; }
            set { _confirmedTradesDt = value; }
        }

        /// <summary>
        /// This contains the removed component trade information in data table, against the selected property.
        /// </summary>
        /// <remarks></remarks>
        public DataTable RemovedTradesDt
        {
            get { return _removedTradesDt; }
            set { _removedTradesDt = value; }
        }

        /// <summary>
        /// This contains the appointment record that is in pending state
        /// </summary>
        /// <remarks></remarks>
        public DataTable ConfirmAppointmentsDt
        {
            get { return _confirmAppointmentsDt; }
            set { _confirmAppointmentsDt = value; }
        }

        /// <summary>
        /// This contains the all the appointments that has been arrange against component
        /// </summary>
        /// <remarks></remarks>
        public DataTable ArrangedTradesAppointmentsDt
        {
            get { return _arrangedTradesAppointmentsDt; }
            set { _arrangedTradesAppointmentsDt = value; }
        }

        /// <summary>
        /// This contains the property info in data table
        /// </summary>
        /// <remarks></remarks>
        public DataTable PropertyInfoDt
        {
            get { return _propertyInfoDt; }
            set { _propertyInfoDt = value; }
        }

        #endregion
    }
}
