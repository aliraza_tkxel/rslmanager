﻿// -----------------------------------------------------------------------
// <copyright file="MeAppointmentBO.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace PDR_BusinessObject.MeAppointment
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    using Microsoft.VisualBasic;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Diagnostics;

    public class MeAppointmentBO
    {

        #region "Attributes"

        private string _attributeName;
        private string _tradeName;
        private string _msatType;
        private int _msatId;
        private string _tradeId;
        private double _duration;

        private string _startTime;
        private string _endTime;
        private DateTime _startDate;
        private DateTime _endDate;

        private string _operativeName;
        private int _operativeId;
        private int _journalId;
        private int _appointmentId;

        private DateTime _startDateDefaultFormat;
        private int _userId;
        private int _tenancyId;
        private string _journalNotes;
        private string _customerNotes;
        private string _appointmentNotes;
        private string _locationName;
        private int _locationId;

        private string _worksRequired;
        private string _appointmentType;
        private int _appointmentTypeId;
        private int _index;
        private string _userSelectedStartDate;

        #endregion

        #region "Constructor"
        public MeAppointmentBO()
        {
            _attributeName = string.Empty;
            _msatId = -1;
            _msatType = string.Empty;
            _tradeName = string.Empty;
            _worksRequired = string.Empty;
            _tradeId = string.Empty;
            _duration = 0;
            _startTime = string.Empty;
            _endTime = string.Empty;
            _operativeName = string.Empty;
            _operativeId = -1;
            _userId = 0;
            _customerNotes = string.Empty;
            _appointmentNotes = string.Empty;
            _locationName = string.Empty;
            _locationId = -1;
            _appointmentTypeId = -1;
            _appointmentType = string.Empty;
            _journalId = -1;
            _appointmentId = -1;
            _journalNotes = string.Empty;
            
        }
        #endregion

        #region "Properties"

        public int MsatId
        {
            get { return _msatId; }
            set { _msatId = value; }
        }

        public string AttributeName
        {
            get { return _attributeName; }
            set { _attributeName = value; }
        }

        public string MsatType
        {
            get { return _msatType; }
            set { _msatType = value; }
        }

        public string JournalNotes
        {
            get { return _journalNotes; }
            set { _journalNotes = value; }
        }

        public string AppointmentNotes
        {
            get { return _appointmentNotes; }
            set { _appointmentNotes = value; }
        }

        public string CustomerNotes
        {
            get { return _customerNotes; }
            set { _customerNotes = value; }
        }

        public int TenancyId
        {
            get { return _tenancyId; }
            set { _tenancyId = value; }
        }

        public int UserId
        {
            get { return _userId; }
            set { _userId = value; }
        }

        public string TradeId
        {
            get { return _tradeId; }
            set { _tradeId = value; }
        }

        public string TradeName
        {
            get { return _tradeName; }
            set { _tradeName = value; }
        }

        public double Duration
        {
            get { return _duration; }
            set { _duration = value; }
        }

        public string StartTime
        {
            get { return _startTime; }
            set { _startTime = value; }
        }

        public string EndTime
        {
            get { return _endTime; }
            set { _endTime = value; }
        }

        public System.DateTime StartDate
        {
            get { return _startDate; }
            set { _startDate = value; }
        }

        public System.DateTime EndDate
        {
            get { return _endDate; }
            set { _endDate = value; }
        }

        public string OperativeName
        {
            get { return _operativeName; }
            set { _operativeName = value; }
        }

        public int OperativeId
        {
            get { return _operativeId; }
            set { _operativeId = value; }
        }

        public int JournalId
        {
            get { return _journalId; }
            set { _journalId = value; }
        }

        public int AppointmentId
        {
            get { return _appointmentId; }
            set { _appointmentId = value; }
        }

        public DateTime StartDateDefault
        {
            get { return _startDateDefaultFormat; }
            set { _startDateDefaultFormat = value; }
        }

        public string LocationName
        {
            get { return _locationName; }
            set { _locationName = value; }
        }

        public int LocationId
        {
            get { return _locationId; }
            set { _locationId = value; }
        }

        public string WorksRequired
        {
            get { return _worksRequired; }
            set { _worksRequired = value; }
        }

        public int AppointmentTypeId
        {
            get { return _appointmentTypeId; }
            set { _appointmentTypeId = value; }
        }
        public string AppointmentType
        {
            get { return _appointmentType; }
            set { _appointmentType = value; }
        }
        public int Index
        {
            get { return _index; }
            set { _index = value; }
        }

        public string UserSelectedStartDate
        {
            get { return _userSelectedStartDate; }
            set { _userSelectedStartDate = value; }
        }

        #endregion

    }

}
