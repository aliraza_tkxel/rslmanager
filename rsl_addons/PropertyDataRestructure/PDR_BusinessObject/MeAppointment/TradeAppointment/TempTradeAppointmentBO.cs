﻿// -----------------------------------------------------------------------
// <copyright file="TempTradeAppointmentBO.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace PDR_BusinessObject.TradeAppointment
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class TempTradeAppointmentBO
    {

        private System.DateTime _startSelectedDate;
        public System.DateTime StartSelectedDate
        {
            get { return _startSelectedDate; }
            set { _startSelectedDate = value; }
        }

        private int _displayCount;
        public int DisplayCount
        {
            get { return _displayCount; }
            set { _displayCount = value; }
        }

        public TempTradeDtBO tempTradeDtBo = new TempTradeDtBO();
        public TempAppointmentDtBO tempAppointmentDtBo = new TempAppointmentDtBO();

    }
}
