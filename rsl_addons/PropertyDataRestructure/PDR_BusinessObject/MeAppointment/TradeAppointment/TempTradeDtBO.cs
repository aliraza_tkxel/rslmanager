﻿// -----------------------------------------------------------------------
// <copyright file="TempTradeDtBO.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace PDR_BusinessObject.TradeAppointment
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Data;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class TempTradeDtBO : IDtBO
    {

        public DataTable dt;

        public DataRow dr;
        public string jsn = string.Empty;
        public string componentName = string.Empty;
        public string durationString = string.Empty;
        public string tradeName = string.Empty;
        public string status = string.Empty;
        public int tradeId = 0;
        public double tradeDurationHrs = 0;
        public double tradeDurationDays = 0;
        public int componentTradeId = 0;

        public int componentId = 0;
        private const string jsnColName = "JSN:";
        private const string componentColName = "Component:";
        private const string durationStringColName = "Duration:";
        private const string tradeColName = "Trade:";
        private const string statusColName = "Status:";
        public const string tradeIdColName = "TradeId:";
        public const string tradeDurationHrsColName = "TradeDurationHrs:";
        public const string tradeDurationDaysColName = "TradeDurationDays:";
        public const string componentTradeIdColName = "ComponentTradeId:";

        public const string componentIdColName = "ComponentId:";
        public TempTradeDtBO()
        {
            this.createDataTable();
        }

        public void addNewDataRow()
        {
            dr = dt.NewRow();
            dr[jsnColName] = jsn;
            dr[componentColName] = componentName;
            dr[durationStringColName] = durationString;
            dr[tradeColName] = tradeName;
            dr[statusColName] = status;
            dr[tradeIdColName] = tradeId;
            dr[tradeDurationHrsColName] = tradeDurationHrs;
            dr[tradeDurationDaysColName] = tradeDurationDays;
            dr[componentTradeIdColName] = componentTradeId;
            dr[componentIdColName] = componentId;
            dt.Rows.Add(dr);
        }

        public  void createDataTable()
        {
            dt = new DataTable();
            dt.Columns.Add(jsnColName);
            dt.Columns.Add(componentColName);
            dt.Columns.Add(durationStringColName);
            dt.Columns.Add(tradeColName);
            dt.Columns.Add(statusColName);
            dt.Columns.Add(tradeIdColName);
            dt.Columns.Add(tradeDurationHrsColName);
            dt.Columns.Add(tradeDurationDaysColName);
            dt.Columns.Add(componentTradeIdColName);
            dt.Columns.Add(componentIdColName);
        }



      
    }
}
