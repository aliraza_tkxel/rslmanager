﻿// -----------------------------------------------------------------------
// <copyright file="SelectedAttributeBO.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace PDR_BusinessObject.TradeAppointment
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class SelectedAttributeBO
    {

        private int _journalId;
        public int JournalId
        {
            get { return _journalId; }
            set { _journalId = value; }
        }

        private int _appointmentId;
        public int AppointmentId
        {
            get { return _appointmentId; }
            set { _appointmentId = value; }
        }

        private string _attributeName;
        public string AttributeName
        {
            get { return _attributeName; }
            set { _attributeName = value; }
        }

        private string _schemeName;
        public string SchemeName
        {
            get { return _schemeName; }
            set { _schemeName = value; }
        }

        private string _blockName;
        public string BlockName
        {
            get { return _blockName; }
            set { _blockName = value; }
        }

        private string _msatType;
        public string MsatType
        {
            get { return _msatType; }
            set { _msatType = value; }
        }

        private string _msatTypeId;
        public string MsatTypeId
        {
            get { return _msatTypeId; }
            set { _msatTypeId = value; }
        }

        private string _msatQueryStringPath;
        public string MsatQueryStringPath
        {
            get { return _msatQueryStringPath; }
            set { _msatQueryStringPath = value; }
        }

    }
}
