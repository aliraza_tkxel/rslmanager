﻿// -----------------------------------------------------------------------
// <copyright file="TempMeAppointmentBO.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace PDR_BusinessObject.TradeAppointment
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class TempMeAppointmentBO
    {

        private string _duration;
        public string Duration
        {
            get { return _duration; }
            set { _duration = value; }
        }

        private string _durationString;
        public string DurationString
        {
            get { return _durationString; }
            set { _durationString = value; }
        }

        private string _trade;
        public string Trade
        {
            get { return _trade; }
            set { _trade = value; }
        }

        private string _tradeId;
        public string TradeId
        {
            get { return _tradeId; }
            set { _tradeId = value; }
        }


        public TempAppointmentDtBO tempAppointmentDtBo = new TempAppointmentDtBO();
    }
}
