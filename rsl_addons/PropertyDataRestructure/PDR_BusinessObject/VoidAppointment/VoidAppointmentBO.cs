﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_BusinessObject.VoidAppointment
{
    public class VoidAppointmentBO
    {
        public int UserId { get; set; }
        public string AppointmentNotes { get; set; }
        public DateTime AppointmentStartDate { get; set; }
        public DateTime AppointmentEndDate { get; set; }
        public string  StartTime { get; set; }
        public string EndTime { get; set; }
        public int OperativeId { get; set; }
        public string OperativeName { get; set; }
        public int JournalId { get; set; }
        public int InspectionJournalId { get; set; }
        public double  Duration { get; set; }
        public int InspectionType{ get; set; }
        public string RequiredWorkIds { get; set; }
        public string JobSheetNotes { get; set; }
        public int AppointmentId { get; set; }
        public string ChecksType { get; set; }
        public bool Legionella { get; set; }

    }
}
