﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_BusinessObject.ICalFile
{
    public class ICalFileBO
    {
        public string OperativeName { get; set; }
        public string OperativeEmail { get; set; }
        public DateTime AppointmentStartDate { get; set; }
        public DateTime AppointmentEndDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string JS { get; set; }
        public string  Summary { get; set; }


    }
}
