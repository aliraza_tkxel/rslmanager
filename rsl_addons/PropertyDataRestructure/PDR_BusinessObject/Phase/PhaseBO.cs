﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_BusinessObject.Phase
{
    public class PhaseBO
    {

        public PhaseBO()
        {
            _phaseName = string.Empty;
            _constructionStart = null;
            _anticipatedCompletion = null;
            _actualCompletion = null;
            _management = null;
            _schemeId = 0;
            _phaseId = 0;
            _developmentId = 0;
        }

        private string _phaseName;

        public string PhaseName
        {
            get
            {
                return _phaseName;
            }
            set
            {
                _phaseName = value;
            }
        }

        private Nullable<DateTime> _constructionStart;

        public Nullable<DateTime> ConstructionStart
        {
            get
            {
                return _constructionStart;
            }
            set
            {
                _constructionStart = value;
            }
        }


        private Nullable<DateTime> _anticipatedCompletion;

        public Nullable<DateTime> AnticipatedCompletion
        {
            get
            {
                return _anticipatedCompletion;
            }
            set
            {
                _anticipatedCompletion = value;
            }
        }


        private Nullable<DateTime> _actualCompletion;

        public Nullable<DateTime> ActualCompletion
        {
            get
            {
                return _actualCompletion;
            }
            set
            {
                _actualCompletion = value;
            }
        }


        private Nullable<DateTime> _management;

        public Nullable<DateTime> Management
        {
            get
            {
                return _management;
            }
            set
            {
                _management = value;
            }
        }


        private int _schemeId;

        public int SchemeId
        {
            get
            {
                return _schemeId;
            }
            set
            {
                _schemeId = value;
            }
        }


        private int _phaseId;

        public int PhaseId
        {
            get
            {
                return _phaseId;
            }
            set
            {
                _phaseId = value;
            }
        }

        private int _developmentId;
        public int DevelopmentId
        {
            get
            {
                return _developmentId;
            }
            set
            {
                _developmentId = value;
            }
        }
    }
}
