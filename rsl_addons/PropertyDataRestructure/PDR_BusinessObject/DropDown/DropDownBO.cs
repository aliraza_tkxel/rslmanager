﻿// -----------------------------------------------------------------------
// <copyright file="DropDownBO.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace PDR_BusinessObject.DropDown
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class DropDownBO
    {

        #region "Attributes"
        private int _Id;
        private string _description;
        #endregion

        #region "Constructor"

        public DropDownBO(int id, string name)
        {
            _Id = id;
            _description = name;
        }

        #endregion

        #region "Properties"

        public int Id
        {
            get { return _Id; }
            set { _Id = value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        #endregion

    }
}
