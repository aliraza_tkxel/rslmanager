﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_BusinessObject.Development
{
    public class DevelopmentDocumentsBO
    {
        private string _category;
        public string Category
        {
            get
            {
                return _category;
            }
            set
            {
                _category = value;
            }
        }

        private string _title;

        public string Title
        {
            get
            {
                return _title;
            }
            set
            {
                _title = value;
            }
        }
        private Nullable<DateTime> _expiry;

        public Nullable<DateTime> Expiry
        {
            get
            {
                return _expiry;
            }
            set
            {
                _expiry = value;
            }
        }

        private string _fileType;

        public string FileType
        {
            get
            {
                return _fileType;
            }
            set
            {
                _fileType = value;
            }
        }
        private string _docPath;

        public string DocPath
        {
            get
            {
                return _docPath;
            }
            set
            {
                _docPath = value;
            }
        }

        private int _userId;

        public int UserId
        {
            get
            {
                return _userId;
            }
            set
            {
                _userId = value;
            }
        }

        private int _docId;

        public int DocId
        {
            get
            {
                return _docId;
            }
            set
            {
                _docId = value;
            }
        }
        private int _developmentId;
        public int DevelopmentId
        {
            get
            {
                return _developmentId;
            }
            set
            {
                _developmentId = value;
            }
        }
        public string FileName { get; set; }

        private int _documentTypeId;
        public int DocumentTypeId
        {
            get
            {
                return _documentTypeId;
            }
            set
            {
                _documentTypeId = value;
            }
        }

        private int _documentSubTypeId;
        public int DocumentSubTypeId
        {
            get
            {
                return _documentSubTypeId;
            }
            set
            {
                _documentSubTypeId = value;
            }
        }

        


        private Nullable<DateTime> _UploadedDate;

        public Nullable<DateTime> UploadedDate
        {
            get
            {
                return _UploadedDate;
            }
            set
            {
                _UploadedDate = value;
            }
        }




        private Nullable<DateTime> _DocumentDate;

        public Nullable<DateTime> DocumentDate
        {
            get
            {
                return _DocumentDate;
            }
            set
            {
                _DocumentDate = value;
            }
        }


    }
}
