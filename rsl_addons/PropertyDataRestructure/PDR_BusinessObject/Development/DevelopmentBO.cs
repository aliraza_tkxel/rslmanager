﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace PDR_BusinessObject.Development
{
    public class DevelopmentBO
    {
        public DevelopmentBO()
        {
            _developmentName = string.Empty;
            _patch = 0;
            _address1 = string.Empty;
            _address2 = string.Empty; ;
            _townCity = string.Empty; ;
            _county = string.Empty; ;
            _postCode = string.Empty; ;
            _architect = string.Empty; ;
            _projectManager = string.Empty; ;
            _developmentType = 0;
            _developmentStatus = 0;
            _landValue = 0.0;
            _valueDate = null;
            _purchaseDate = null;
            _grantAmount = 0.0;
            _borrowedAmount = 0.0;
            _outlinePlanningApplication = null;
            _detailedPlanningApplication = null;
            _outlinePlanningApproval = null;
            _detailedPlanningApproval = null;
            _noOfUnits = 0;
            _fundingSourceDt = null;
            _developmentId = 0;
            _localAuthorityId = -1;
            _landPurchasePrice = 0.0;
            _companyId = 0;
        }

        private string _developmentName;
        public string DevelopmentName
        {
            get
            {
                return _developmentName;
            }
            set
            {
                _developmentName = value;
            }
        }

        private int _patch;
        public int Patch
        {
            get
            {
                return _patch;
            }
            set
            {
                _patch = value;
            }
        }

        private string _address1;
        public string Address1
        {
            get
            {
                return _address1;
            }
            set
            {
                _address1 = value;
            }
        }

        private string _address2;
        public string Address2
        {
            get
            {
                return _address2;
            }
            set
            {
                _address2 = value;
            }
        }

        private string _townCity;
        public string TownCity
        {
            get
            {
                return _townCity;
            }
            set
            {
                _townCity = value;
            }
        }

        private string _county;
        public string County
        {
            get
            {
                return _county;
            }
            set
            {
                _county = value;
            }
        }

        private string _postCode;
        public string PostCode
        {
            get
            {
                return _postCode;
            }
            set
            {
                _postCode = value;
            }
        }

        private string _architect;
        public string Architect
        {
            get
            {
                return _architect;
            }
            set
            {
                _architect = value;
            }
        }

        private string _projectManager;
        public string ProjectManager
        {
            get
            {
                return _projectManager;
            }
            set
            {
                _projectManager = value;
            }
        }

        private int _companyId;
        public int CompanyId
        {
            get
            {
                return _companyId;
            }
            set
            {
                _companyId = value;
            }
        }

        private int _developmentType;
        public int DevelopmentType
        {
            get
            {
                return _developmentType;
            }
            set
            {
                _developmentType = value;
            }
        }

        private int _developmentStatus;
        public int DevelopmentStatus
        {
            get
            {
                return _developmentStatus;
            }
            set
            {
                _developmentStatus = value;
            }
        }

        private double _landValue;
        public double LandValue
        {
            get
            {
                return _landValue;
            }
            set
            {
                _landValue = value;
            }
        }
        private Nullable<DateTime> _valueDate;
        public Nullable<DateTime> ValueDate
        {
            get
            {
                return _valueDate;
            }
            set
            {
                _valueDate = value;
            }
        }

        private double _landPurchasePrice;
        public double LandPurchasePrice
        {
            get
            {
                return _landPurchasePrice;
            }
            set
            {
                _landPurchasePrice = value;
            }
        }
        private Nullable<DateTime> _purchaseDate;
        public Nullable<DateTime> PurchaseDate
        {
            get
            {
                return _purchaseDate;
            }
            set
            {
                _purchaseDate = value;
            }
        }

        private double _grantAmount;
        public double GrantAmount
        {
            get
            {
                return _grantAmount;
            }
            set
            {
                _grantAmount = value;
            }
        }

        private double _borrowedAmount;
        public double BorrowedAmount
        {
            get
            {
                return _borrowedAmount;
            }
            set
            {
                _borrowedAmount = value;
            }
        }

        private Nullable<DateTime> _outlinePlanningApplication;
        public Nullable<DateTime> OutlinePlanningApplication
        {
            get
            {
                return _outlinePlanningApplication;
            }
            set
            {
                _outlinePlanningApplication = value;
            }
        }

        private Nullable<DateTime> _detailedPlanningApplication;
        public Nullable<DateTime> DetailedPlanningApplication
        {
            get
            {
                return _detailedPlanningApplication;
            }
            set
            {
                _detailedPlanningApplication = value;
            }
        }

        private Nullable<DateTime> _outlinePlanningApproval;
        public Nullable<DateTime> OutlinePlanningApproval
        {
            get
            {
                return _outlinePlanningApproval;
            }
            set
            {
                _outlinePlanningApproval = value;
            }
        }

        private Nullable<DateTime> _detailedPlanningApproval;
        public Nullable<DateTime> DetailedPlanningApproval
        {
            get
            {
                return _detailedPlanningApproval;
            }
            set
            {
                _detailedPlanningApproval = value;
            }
        }

        private int _noOfUnits;
        public int NoofUnits
        {
            get
            {
                return _noOfUnits;
            }
            set
            {
                _noOfUnits = value;
            }
        }

        private DataTable _fundingSourceDt;
        public DataTable FundingSourceDt
        {
            get
            {
                return _fundingSourceDt;
            }
            set
            {
                _fundingSourceDt = value;
            }
        }

        private int _developmentId;

        public int DevelopmentId
        {
            get
            {
                return _developmentId;
            }
            set
            {
                _developmentId = value;
            }
        }

        private int _localAuthorityId;
        public int LocalAuthorityId
        {
            get
            {
                return _localAuthorityId;
            }
            set
            {
                _localAuthorityId = value;
            }
        }
        private int userId;

        public int UserId
        {
            get { return userId; }
            set { userId = value; }
        }

    }
}
