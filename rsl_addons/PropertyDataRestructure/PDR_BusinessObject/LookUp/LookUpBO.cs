﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_BusinessObject.LookUp
{
  public   class LookUpBO
    {
       #region "Attributes"
        private int _EmployeeId;
        private string _FullName;
        #endregion

        #region "Constructor"
        public LookUpBO()
        {

        }
        public LookUpBO(int id, string name)
        {
            _EmployeeId = id;
            _FullName = name;
        }

        #endregion

        #region "Properties"

        public int EmployeeId
        {
            get { return _EmployeeId; }
            set { _EmployeeId = value; }
        }

        public string FullName
        {
            get { return _FullName; }
            set { _FullName = value; }
        }

        #endregion
    }
}
