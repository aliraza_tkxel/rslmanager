﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_BusinessObject.Scheme
{
    public class SaveSchemeBO
    {
        private string _schemeName;

        public string SchemeName
        {
            get
            {
                return _schemeName;
            }
            set
            {
                _schemeName = value;
            }
        }
        private string _schemeCode;

        public string SchemeCode
        {
            get
            {
                return _schemeCode;
            }
            set
            {
                _schemeCode = value;
            }
        }


        private int _developmentId;

        public int DevelopmentId
        {
            get
            {
                return _developmentId;
            }
            set
            {
                _developmentId = value;
            }
        }

        private int _phaseId;

        public int PhaseId
        {
            get
            {
                return _phaseId;
            }
            set
            {
                _phaseId = value;
            }
        }

        private string _schemeId;

        public string SchemeId
        {
            get
            {
                return _schemeId;
            }
            set
            {
                _schemeId = value;
            }
        }

        public int ExistingSchemeId { get; set; }

        public List<string> Properties = new List<string>();
        

        public List<int> Blocks = new List<int>();
        
    }
}
