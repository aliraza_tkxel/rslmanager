﻿namespace PDR_BusinessObject.Scheme
{
    public class SchemeActivitiesBO
    {
        private int _JournalHistoryId;
        public int JournalHistoryId
        {
            get
            {
                return _JournalHistoryId;
            }
            set
            {
                _JournalHistoryId = value;
            }
        }

        private int _PropertyDefectId;
        public int PropertyDefectId
        {
            get
            {
                return _PropertyDefectId;
            }
            set
            {
                _PropertyDefectId = value;
            }
        }

        private int _IsDefectImageExist;
        public int IsDefectImageExist
        {
            get
            {
                return _IsDefectImageExist;
            }
            set
            {
                _IsDefectImageExist = value;
            }
        }

        private int _JournalId;
        public int JournalId
        {
            get
            {
                return _JournalId;
            }
            set
            {
                _JournalId = value;
            }
        }


        private string _Status;
        public string Status
        {
            get
            {
                return _Status;
            }
            set
            {
                _Status = value;
            }
        }

        private string _Action;
        public string Action
        {
            get
            {
                return _Action;
            }
            set
            {
                _Action = value;
            }
        }
        private string _InspectionType;
        public string InspectionType
        {
            get
            {
                return _InspectionType;
            }
            set
            {
                _InspectionType = value;
            }
        }

        private string _CreateDate;
        public string CreateDate
        {
            get
            {
                return _CreateDate;
            }
            set
            {
                _CreateDate = value;
            }
        }
        private string _Name;
        public string Name
        {
            get
            {
                return _Name;
            }
            set
            {
                _Name = value;
            }
        }
        private bool _IsLetterAttached;
        public bool IsLetterAttached
        {
            get
            {
                return _IsLetterAttached;
            }
            set
            {
                _IsLetterAttached = value;
            }
        }
        private bool _IsDocumentAttached;
        public bool IsDocumentAttached
        {
            get
            {
                return _IsDocumentAttached;
            }
            set
            {
                _IsDocumentAttached = value;
            }
        }
        private string _Document;
        public string Document
        {
            get
            {
                return _Document;
            }
            set
            {
                _Document = value;
            }
        }
        private int _CP12DocumentID;
        public int CP12DocumentID
        {
            get
            {
                return _CP12DocumentID;
            }
            set
            {
                _CP12DocumentID = value;
            }
        }
        private string _CREATIONDate;
        public string CREATIONDate
        {
            get
            {
                return _CREATIONDate;
            }
            set
            {
                _CREATIONDate = value;
            }
        }
        private string _REF;
        public string REF
        {
            get
            {
                return _REF;
            }
            set
            {
                _REF = value;
            }
        }
        private string _AppointmentDate;
        public string AppointmentDate
        {
            get
            {
                return _AppointmentDate;
            }
            set
            {
                _AppointmentDate = value;
            }
        }
        private string _OPERATIVENAME;
        public string OPERATIVENAME
        {
            get
            {
                return _OPERATIVENAME;
            }
            set
            {
                _OPERATIVENAME = value;
            }
        }
        private string _OPERATIVETRADE;
        public string OPERATIVETRADE
        {
            get
            {
                return _OPERATIVETRADE;
            }
            set
            {
                _OPERATIVETRADE = value;
            }
        }


        private int _AppointmentId;
        public int AppointmentId
        {
            get
            {
                return _AppointmentId;
            }
            set
            {
                _AppointmentId = value;
            }
        }

        private string _AppointmentNotes;
        public string AppointmentNotes
        {
            get
            {
                return _AppointmentNotes;
            }
            set
            {
                _AppointmentNotes = value;
            }
        }

        private string _InspectionTypeDescription;
        public string InspectionTypeDescription
        {
            get
            {
                return _InspectionTypeDescription;
            }
            set
            {
                _InspectionTypeDescription = value;
            }
        }

        // my change
        private int _LGSRHISTORYID;
        public int LGSRHISTORYID
        {
            get
            {
                return _LGSRHISTORYID;
            }
            set
            {
                _LGSRHISTORYID = value;
            }
        }
    }
}
