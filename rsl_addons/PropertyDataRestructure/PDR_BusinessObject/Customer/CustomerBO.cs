﻿using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using Microsoft.Practices.EnterpriseLibrary.Validation;

// -----------------------------------------------------------------------
// <copyright file="CustomerBO.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace PDR_BusinessObject.Customer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class CustomerBO
    {

        #region "Attributes"
        private int _customerid;
        private string _name;
        private string _street;
        private string _address;
        private string _area;
        private string _telephone;
        private string _mobile;
        private string _email;
        private string _city;
        private string _postCode;
        private int _patchId;
        private string _patchName;
        #endregion

        #region "Constructor"
        public CustomerBO()
        {
            _customerid = 0;
            _name = string.Empty;
            _street = string.Empty;
            _address = string.Empty;
            _area = string.Empty;
            _telephone = string.Empty;
            _mobile = string.Empty;
            _email = string.Empty;
            _city = string.Empty;
            _postCode = string.Empty;
            _patchId = 0;
            _patchName = string.Empty;
        }
        #endregion

        #region "Properties"
        // Get / Set property for _customerid
        public int CustomerId
        {

            get { return _customerid; }

            set { _customerid = value; }
        }

        // Get / Set property for _name
        public string Name
        {

            get { return _name; }

            set { _name = value; }
        }

        // Get / Set property for _street
        public string Street
        {

            get { return _street; }

            set { _street = value; }
        }

        // Get / Set property for _address
        public string Address
        {

            get { return _address; }

            set { _address = value; }
        }

        // Get / Set property for _area
        public string Area
        {

            get { return _area; }

            set { _area = value; }
        }

        // Get / Set property for _telephone

       // [RegexValidator("^\\+?\\d+(-\\d+)*$", MessageTemplate = "Invalid Telephone number.")]
        [StringLengthValidator(0, 18, MessageTemplate = "Invalid Telephone number.")]
        public string Telephone
        {

            get { return _telephone; }

            set { _telephone = value; }
        }

        // Get / Set property for _mobile
       // [RegexValidator("^\\+?\\d+(-\\d+)*$", MessageTemplate = "Invalid Mobile number.")]
        [StringLengthValidator(0, 18, MessageTemplate = "Invalid Mobile number.")]
        public string Mobile
        {

            get { return _mobile; }

            set { _mobile = value; }
        }


        // Get / Set email for _email
        [RegexValidator("^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})$", MessageTemplate = "Invalid Email address.")]
        public string Email
        {

            get { return _email; }

            set { _email = value; }
        }


        // Get / Set cit for _city
        public string City
        {

            get { return _city; }

            set { _city = value; }
        }


        // Get / Set post code for _email
        public string PostCode
        {

            get { return _postCode; }

            set { _postCode = value; }
        }


        // Get / Set patch id for _patchid
        public int PatchId
        {

            get { return _patchId; }

            set { _patchId = value; }
        }


        // Get / Set patch name for _patchName
        public string PatchName
        {

            get { return _patchName; }

            set { _patchName = value; }
        }


        #endregion

    }
}
