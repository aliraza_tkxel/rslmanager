﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using PDR_BusinessObject.PageSort;
using PDR_BusinessObject.PaintPacks;

namespace PDR_DataAccess.PaintPacks
{
    public interface IPaintPacksRepo
    {
        #region get Paint Packs List
        /// <summary>
        /// get Paint Packs List
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <returns></returns>
        Int32 getPaintPacksList(ref DataSet resultDataSet, PageSortBO objPageSortBO);
        #endregion

        #region get Paint Packs Detail
        /// <summary>
        /// get Paint Packs Detail
        /// </summary>
        /// <param name="paintPackId"></param>
        /// <returns></returns>
        DataSet getPaintPacksDetail(int paintPackId);
        #endregion

        #region amend Paint Packs
        /// <summary>
        /// amend Paint Packs
        /// </summary>
        /// <param name="objPaintPacksBo"></param>
        void amendPaintPacks(PaintPacksBO objPaintPacksBo);
        #endregion

        #region update Paint Packs
        /// <summary>
        /// update Paint Packs
        /// </summary>
        /// <param name="objPaintPacksBo"></param>
        void updatePaintPacks(PaintPacksBO objPaintPacksBo);
        #endregion
    }
}
