﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using PDR_BusinessObject.PageSort;
using PDR_BusinessObject.Parameter;
using PDR_DataAccess.Base;
using PDR_Utilities.Constants;
using PDR_BusinessObject.PaintPacks;

namespace PDR_DataAccess.PaintPacks
{
    public class PaintPacksRepo : BaseDAL, IPaintPacksRepo
    {

        #region get Paint Packs List
        /// <summary>
        /// get Paint Packs List
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <returns></returns>
        public int getPaintPacksList(ref DataSet resultDataSet, PageSortBO objPageSortBO)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO pageNumberParam = new ParameterBO("PageNumber", objPageSortBO.PageNumber, DbType.Int32);
            paramList.Add(pageNumberParam);

            ParameterBO pageSizeParam = new ParameterBO("PageSize", objPageSortBO.PageSize, DbType.Int32);
            paramList.Add(pageSizeParam);

            ParameterBO SortDirectionParam = new ParameterBO("sortOrder", objPageSortBO.SmallSortDirection, DbType.String);
            paramList.Add(SortDirectionParam);

            ParameterBO SortExpressionParam = new ParameterBO("sortColumn", objPageSortBO.SortExpression, DbType.String);
            paramList.Add(SortExpressionParam);

            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);

            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetPaintPacksList);

            ParameterBO obj = outParametersList.First();
            Int32 outvalue = Convert.ToInt32(obj.Value.ToString());

            return outvalue;
        }

        #endregion

        #region get Paint Packs Detail
        /// <summary>
        /// get Paint Packs Detail
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="paintPackId"></param>
        public DataSet getPaintPacksDetail( int paintPackId)
        {
            ParameterList parametersList = new ParameterList();
            ParameterList outParametersList = new ParameterList();
            DataSet resultDataSet = new DataSet();
            DataTable paintPackHeaderDt = new DataTable();
            paintPackHeaderDt.TableName = ApplicationConstants.PaintPackHeader;
            DataTable paintPackDetailDt = new DataTable();
            paintPackDetailDt.TableName = ApplicationConstants.PaintPackDetail;
            DataTable paintStatusDt = new DataTable();
            paintStatusDt.TableName = ApplicationConstants.PaintStatus;

            DataTable paintPackSupplierDt = new DataTable();
            paintPackSupplierDt.TableName = ApplicationConstants.PaintPackSupplier;
            resultDataSet.Tables.Add(paintPackHeaderDt);
            resultDataSet.Tables.Add(paintPackDetailDt);
            resultDataSet.Tables.Add(paintStatusDt);
            resultDataSet.Tables.Add(paintPackSupplierDt);

            ParameterBO paintPackIdParam = new ParameterBO("paintPackId", paintPackId, DbType.Int32);
            parametersList.Add(paintPackIdParam);

            IDataReader iResultDataReader = base.SelectRecord(parametersList, SpNameConstants.GetPaintPacksDetail);
            resultDataSet.Load(iResultDataReader, LoadOption.OverwriteChanges, paintPackHeaderDt, paintPackDetailDt, paintStatusDt, paintPackSupplierDt);
            iResultDataReader.Close();
            return resultDataSet;
        }
        #endregion

        #region amend Paint Packs
        /// <summary>
        /// amend Paint Packs
        /// </summary>
        /// <param name="objPaintPacksBo"></param>
        public void amendPaintPacks(PaintPacksBO objPaintPacksBo)
        {
            ParameterList parameterList = new ParameterList();
            ParameterList outparameterList = new ParameterList();


            ParameterBO paintPackDetailIdParam = new ParameterBO("paintPackDetailId", objPaintPacksBo.PaintPackDetailId, DbType.Int32);
            parameterList.Add(paintPackDetailIdParam);

            ParameterBO paintRefParam = new ParameterBO("paintRef", objPaintPacksBo.PaintRef, DbType.String);
            parameterList.Add(paintRefParam);


            ParameterBO paintColorParam = new ParameterBO("paintColor", objPaintPacksBo.PaintColor, DbType.String);
            parameterList.Add(paintColorParam);

            ParameterBO paintNameParam = new ParameterBO("paintName", objPaintPacksBo.PaintName, DbType.String);
            parameterList.Add(paintNameParam);

            SaveRecord(ref parameterList, ref outparameterList, SpNameConstants.AmendPaintPacks);
        }
        #endregion

        #region update Paint Packs
        /// <summary>
        /// update Paint Packs
        /// </summary>
        /// <param name="objPaintPacksBo"></param>
        public void updatePaintPacks(PaintPacksBO objPaintPacksBo)
        {
            ParameterList parameterList = new ParameterList();
            ParameterList outparameterList = new ParameterList();


            ParameterBO paintPackDetailIdParam = new ParameterBO("paintPackId", objPaintPacksBo.PaintPackId, DbType.Int32);
            parameterList.Add(paintPackDetailIdParam);

            ParameterBO statusIdParam = new ParameterBO("statusId", objPaintPacksBo.Status, DbType.Int32);
            parameterList.Add(statusIdParam);


            ParameterBO supplierIdParam = new ParameterBO("supplierId", objPaintPacksBo.Supplier, DbType.Int32);
            parameterList.Add(supplierIdParam);

            ParameterBO deliveryDueDateParam = new ParameterBO("deliveryDueDate", objPaintPacksBo.DeliveryDue, DbType.DateTime);
            parameterList.Add(deliveryDueDateParam);

            SaveRecord(ref parameterList, ref outparameterList, SpNameConstants.UpdatePaintPacks);
        }
        #endregion
    }
}
