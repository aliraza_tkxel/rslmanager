﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PDR_BusinessObject.DropDown;
using PDR_BusinessObject.Expenditure;
using PDR_BusinessObject.AssignToContractor;
using PDR_DataAccess.AssignToContractor;
using PDR_BusinessObject.Vat;
using System.Data;
using PDR_BusinessObject.PageSort;
using PDR_BusinessObject.CompletePurchaseOrder;
namespace PDR_DataAccess.PurchaseOrder
{
    public interface IPurchaseOrderRepo
    {
        #region "Get Contractor details For purchase order by orderId"
        DataSet getPurchaseOrderById(int purchaseOrderId, PageSortBO objPageSortBo, ref int totalCount);
        DataSet UpdatePurchaseOrderById(int purchaseOrderId, PageSortBO objPageSortBo, ref int totalCount);
        void getRepairSearchResult(ref DataSet resultDataSet, ref string searchText);
        int SaveWorkCompletionData(ref CompletePurchaseOrderBO objCompletePurchaseOrderBO);
        void ChangeWorkStatus(int purchaseOrderId, bool isAccepted);
        void SaveNoEntryData(int faultLogId, DateTime recordedDateTime);
        void SaveCancelledData(int faultLogId,int orderId);
        string GetSchedulerEmailId(int orderId);
        string GetPoStatusForSessionCreation(int orderId);
        #endregion
    }
}
