﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace PDR_DataAccess.PurchaseOrder
{
   using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using PDR_DataAccess.Base;
    using System.Data;
    using PDR_BusinessObject;
    using PDR_Utilities.Constants;
    using PDR_BusinessObject.PageSort;
    using PDR_BusinessObject.DropDown;
    using PDR_BusinessObject.Parameter;
    using PDR_BusinessObject.AssignToContractor;
    using PDR_BusinessObject.Vat;
    using PDR_BusinessObject.Expenditure;
    using PDR_BusinessObject;
    using PDR_BusinessObject.CompletePurchaseOrder;

    public class PurchaseOrderRepo : BaseDAL, IPurchaseOrderRepo
    {
        #region get contractor details of purchase Order BY Id
        public DataSet getPurchaseOrderById(int purchaseOrderId,PageSortBO objPageSortBo, ref int totalCount)
        {
            ParameterList parametersList = new ParameterList();
            DataSet resultDataSet = new DataSet();

            ParameterBO purchaseOrderIdParam = new ParameterBO("orderId", purchaseOrderId, DbType.Int32);
            parametersList.Add(purchaseOrderIdParam);
            DataTable contractorDetailDt = new DataTable();
            contractorDetailDt.TableName = ApplicationConstants.ContractorDetailsForContractorsAcceptPOPage;
            DataTable contarctDetailDt = new DataTable();
            contarctDetailDt.TableName = ApplicationConstants.PropertyRiskDataTableForContractorsAcceptPOPage;
            DataTable vulnerabalityDetailDt = new DataTable();
            vulnerabalityDetailDt.TableName = ApplicationConstants.VulnerabalityDetailsForContractorsAcceptPOPage;
            DataTable reportedFaultVatCost = new DataTable();
            reportedFaultVatCost.TableName = ApplicationConstants.ReportedFaultVatCostForContractorsAcceptPOPage;
            DataTable asbestosDetailDt = new DataTable();
            asbestosDetailDt.TableName = ApplicationConstants.AsbestosDetailsForContractorsAcceptPOPage;
            resultDataSet.Tables.Add(contractorDetailDt);
            resultDataSet.Tables.Add(contarctDetailDt);
            resultDataSet.Tables.Add(vulnerabalityDetailDt);
            resultDataSet.Tables.Add(reportedFaultVatCost);
            resultDataSet.Tables.Add(asbestosDetailDt);
            IDataReader iResultDataReader = base.SelectRecord(parametersList, SpNameConstants.PurchaseOrderById);
            resultDataSet.Load(iResultDataReader, LoadOption.OverwriteChanges, contractorDetailDt, contarctDetailDt, vulnerabalityDetailDt, reportedFaultVatCost, asbestosDetailDt);
            totalCount = resultDataSet.Tables[ApplicationConstants.ReportedFaultVatCostForContractorsAcceptPOPage].Rows.Count;
            iResultDataReader.Close();
            return resultDataSet;
        }
        #endregion


        #region Update purchase Order BY Id
        public DataSet UpdatePurchaseOrderById(int purchaseOrderId, PageSortBO objPageSortBo, ref int totalCount)
        {
            ParameterList parametersList = new ParameterList();
            DataSet resultDataSet = new DataSet();

            ParameterBO purchaseOrderIdParam = new ParameterBO("orderId", purchaseOrderId, DbType.Int32);
            parametersList.Add(purchaseOrderIdParam);
            DataTable contractorDetailDt = new DataTable();
            contractorDetailDt.TableName = ApplicationConstants.ContractorDetailsForContractorsAcceptPOPage;
            DataTable contarctDetailDt = new DataTable();
            contarctDetailDt.TableName = ApplicationConstants.PropertyRiskDataTableForContractorsAcceptPOPage;
            DataTable vulnerabalityDetailDt = new DataTable();
            vulnerabalityDetailDt.TableName = ApplicationConstants.VulnerabalityDetailsForContractorsAcceptPOPage;
            DataTable reportedFaultVatCost = new DataTable();
            reportedFaultVatCost.TableName = ApplicationConstants.ReportedFaultVatCostForContractorsAcceptPOPage;
            DataTable PoStatus = new DataTable();
            PoStatus.TableName = ApplicationConstants.PoStatusForUpdatePoPage;
            DataTable asbestosDetailDt = new DataTable();
            asbestosDetailDt.TableName = ApplicationConstants.AsbestosDetailsForContractorsAcceptPOPage;
            resultDataSet.Tables.Add(contractorDetailDt);
            resultDataSet.Tables.Add(contarctDetailDt);
            resultDataSet.Tables.Add(vulnerabalityDetailDt);
            resultDataSet.Tables.Add(reportedFaultVatCost);
            resultDataSet.Tables.Add(PoStatus);
            resultDataSet.Tables.Add(asbestosDetailDt);
            IDataReader iResultDataReader = base.SelectRecord(parametersList, SpNameConstants.UpdatePurchaseOrderById);
            resultDataSet.Load(iResultDataReader, LoadOption.OverwriteChanges, contractorDetailDt, contarctDetailDt, vulnerabalityDetailDt, reportedFaultVatCost, PoStatus, asbestosDetailDt);
            totalCount = resultDataSet.Tables[ApplicationConstants.ReportedFaultVatCostForContractorsAcceptPOPage].Rows.Count;
            iResultDataReader.Close();
            return resultDataSet;
        }
        #endregion

        #region Get Property Search Result
        
        public void getRepairSearchResult(ref DataSet resultDataSet, ref string searchText)
        {
            ParameterList  parametersList = new ParameterList();
            DataTable dtPropertyResult = new DataTable();
            dtPropertyResult.TableName = ApplicationConstants.RepairSearchResult;
            resultDataSet.Tables.Add(dtPropertyResult);
            ParameterBO  searchTextParam = new ParameterBO("searchText", searchText, DbType.String);
            parametersList.Add(searchTextParam);
            IDataReader iResultDataReader = base.SelectRecord(parametersList, SpNameConstants.GetFaultRepairList);
            resultDataSet.Load(iResultDataReader, LoadOption.OverwriteChanges, dtPropertyResult);
        }
        #endregion

        #region Save Work completion data

        public int SaveWorkCompletionData(ref CompletePurchaseOrderBO objCompletePurchaseOrderBO)
        {
           ParameterList outParametersList = new ParameterList();
           ParameterList parameterList = new ParameterList();
          
            ParameterBO faultLogId = new ParameterBO("FaultLogID", objCompletePurchaseOrderBO.FaultLogId, DbType.Int32);
            parameterList.Add(faultLogId);

            ParameterBO faultRepairIdList = new ParameterBO("FaultRepairIDList", objCompletePurchaseOrderBO.FaultRepairIdList, DbType.String);
            parameterList.Add(faultRepairIdList);

            ParameterBO time = new ParameterBO("Time", objCompletePurchaseOrderBO.Time, DbType.String);
            parameterList.Add(time);

            ParameterBO inspectionDate = new ParameterBO("Date", objCompletePurchaseOrderBO.InspectionDate, DbType.DateTime);
            parameterList.Add(inspectionDate);

            ParameterBO userId = new ParameterBO("UserId", objCompletePurchaseOrderBO.UserId, DbType.Int32);
            parameterList.Add(userId);

            ParameterBO followOnStatus = new ParameterBO("FollowOnStatus", objCompletePurchaseOrderBO.followOnStatus, DbType.Boolean);
            parameterList.Add(followOnStatus);

            ParameterBO followOnNotes = new ParameterBO("FollowOnNotes", objCompletePurchaseOrderBO.FollowOnNotes, DbType.String);
            parameterList.Add(followOnNotes);

            ParameterBO RepairNotes = new ParameterBO("RepairNotes", objCompletePurchaseOrderBO.RepairNotes, DbType.String);
            parameterList.Add(RepairNotes);

            ParameterBO totalCountParam = new ParameterBO("Result", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);
          
            base.SaveRecord(ref parameterList,ref outParametersList,SpNameConstants.SaveCompletedWork);
            return int.Parse(outParametersList[0].Value.ToString());
        }
        #endregion

        #region Change work status by contractor
        public void ChangeWorkStatus(int purchaseOrderId, bool isAccepted)
        {
            ParameterList parametersList = new ParameterList();
            ParameterList outparameterList = new ParameterList();
            ParameterBO purchaseOrderIdParam = new ParameterBO("orderId",purchaseOrderId,DbType.Int32);
            parametersList.Add(purchaseOrderIdParam);
            ParameterBO statusParam = new ParameterBO("isAccept",isAccepted,DbType.Boolean);
            parametersList.Add(statusParam);
            base.SaveRecord(ref parametersList,ref outparameterList, SpNameConstants.ChangeWorkStatusByContractor);
        }
        #endregion

        #region Saves no entry data
   
        public void SaveNoEntryData(int faultLogId, DateTime recordedDateTime)
        {
            ParameterList parametersList = new ParameterList();
            ParameterList outparameterList = new ParameterList();
            ParameterBO faultLogIdParam = new ParameterBO("FaultLogId", faultLogId, DbType.Int32);
            parametersList.Add(faultLogIdParam);
            ParameterBO recordedDateTimeParam = new ParameterBO("recordedDateTime", recordedDateTime, DbType.DateTime);
            parametersList.Add(recordedDateTimeParam);
            base.SaveRecord(ref parametersList,ref outparameterList, SpNameConstants.SaveNoEntryData);
        }

        #endregion

        #region Saves Cancelled Data

        public void SaveCancelledData(int faultLogId,int OrderId)
        {
            ParameterList parametersList = new ParameterList();
            ParameterList outparameterList = new ParameterList();
            ParameterBO faultLogIdParam = new ParameterBO("FaultLogId", faultLogId, DbType.Int32);
            parametersList.Add(faultLogIdParam);
            ParameterBO orderIdParam = new ParameterBO("orderId", OrderId, DbType.Int32);
            parametersList.Add(orderIdParam);
            base.SaveRecord(ref parametersList, ref outparameterList, SpNameConstants.SaveCancelledData);
        }

        #endregion


        #region Get scheduler email address in case of rejected faults by contractor

        public string GetSchedulerEmailId(int orderId)
        {
            
            ParameterList parametersList = new ParameterList();
            ParameterList outparameterList = new ParameterList();
            ParameterBO orderIdParam = new ParameterBO("orderId", orderId, DbType.Int32);
            parametersList.Add(orderIdParam);
            DataSet resultDataSet = new DataSet();
            LoadDataSet(ref resultDataSet, ref parametersList, ref outparameterList, SpNameConstants.GetSchedulerEmailId);
            return resultDataSet.Tables[0].Rows[0]["workemail"].ToString();
        }

        #endregion

        #region Get PO Status For Session Creation

        public string GetPoStatusForSessionCreation(int orderId)
        {
            ParameterList parametersList = new ParameterList();
            ParameterList outparameterList = new ParameterList();
            ParameterBO orderIdParam = new ParameterBO("orderId", orderId, DbType.Int32);
            parametersList.Add(orderIdParam);
            DataSet resultDataSet = new DataSet();
            LoadDataSet(ref resultDataSet, ref parametersList, ref outparameterList, SpNameConstants.GetPoStatusForSessionCreation);
            return resultDataSet.Tables[0].Rows[0]["PoStatus"].ToString();
        }
        #endregion




    }
}
