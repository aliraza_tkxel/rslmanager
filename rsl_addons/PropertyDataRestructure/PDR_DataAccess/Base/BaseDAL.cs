﻿using System.Data.SqlClient;
using System.Diagnostics;
using Microsoft.Practices.EnterpriseLibrary.Common;
using Microsoft.Practices.EnterpriseLibrary.Data;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using System.Data;
using PDR_BusinessObject;
using System;
using System.Data.Common;
using System.Collections.Generic;
using PDR_BusinessObject.Parameter;
using PDR_BusinessObject.PageSort;

namespace PDR_DataAccess.Base
{
    public class BaseDAL : IDisposable
    {
        #region "Atrributes"
        // Establish connection with Database by reading configuration from web.config file
        private Database database;
        // Execute commands e-g StoredProcedures
        private DbCommand dbCommand;
        #endregion

        #region "Properties"
        // No Properties are defined as Attributes are supposed to be accessed within this class only
        #endregion

        #region "Constructor"
        public BaseDAL()
        {
            database = null;
            dbCommand = null;
        }

        #endregion

        #region "Functions"

        #region "SaveRecord"
        /// <summary>
        /// This generic Function will be used to save(new record), delete and update records
        /// </summary>
        /// <param name="inParamList"></param>
        /// <param name="outParamList"></param>
        /// <param name="sprocName"></param>
        /// <returns></returns>
        protected int SaveRecord(ref ParameterList inParamList, ref ParameterList outParamList, string sprocName)
        {

            // Used to return No of rows affected
            int rowCount = -1;
            database = DatabaseFactory.CreateDatabase();
            dbCommand = database.GetStoredProcCommand(sprocName);
            // For Each Loop to iterate over inParamList
            // IN-Parameters will be extracted and added to StoredProcedure
            foreach (ParameterBO paramBO in inParamList)
            {
                // Adding parameter Name and Type
                //' SqlDbType.Structured is used to send table valued parameter

                
                    if (paramBO.SqlType == SqlDbType.Structured)
                    {
                        
                        dbCommand.Parameters.Add(new SqlParameter(paramBO.Name, paramBO.Value));
                        // {SqlDbType = SqlDbType.Structured})
                        //  varDatabase.AddInParameter(varDbCommand, paramBO.Name, paramBO.Type, paramBO.Value)
                    }
                    else
                    {
                        database.AddInParameter(dbCommand, paramBO.Name, paramBO.Type);

                        // Setting value of parameter
                        database.SetParameterValue(dbCommand, paramBO.Name, paramBO.Value);
                    }
                

                // Adding parameter Name and Type
                //database.AddInParameter(dbCommand, paramBO.Name, paramBO.Type);
                // Setting value of parameter
                //database.SetParameterValue(dbCommand, paramBO.Name, paramBO.Value);
            }
            // For Each Loop to iterate over outParamList
            // OUT-Parameters will be extracted and added to StoredProcedure
            foreach (ParameterBO outParamBO in outParamList)
            {
                // Adding output parameters Name and Type
                database.AddOutParameter(dbCommand, outParamBO.Name, outParamBO.Type, 100);
            }
            // Executing Non query and recieving results (No. of rows affected)
            rowCount = database.ExecuteNonQuery(dbCommand);
            // Getting out parameter values after executing query
            foreach (ParameterBO paramBO in outParamList)
            {
                paramBO.Value = database.GetParameterValue(dbCommand, paramBO.Name);

            }
            return rowCount;
        }

        #endregion

        #region "Select"
        /// <summary>
        /// // This Function will be used to Select Single/Multiple records
        /// </summary>
        /// <param name="paramList"></param>
        /// <param name="sprocName"></param>
        /// <returns></returns>
        protected IDataReader SelectRecord(ParameterList paramList, string sprocName)
        {
            // Holds the results/records found after executing stored procedure
            IDataReader myReader = null;
            database = DatabaseFactory.CreateDatabase();
            dbCommand = database.GetStoredProcCommand(sprocName);
            // For Each Loop to iterate over paramList
            // Parameter's will be extracted and added to StoredProcedure
            if ((paramList != null))
            {
                foreach (ParameterBO paramBO in paramList)
                {
                    // Adding parameter Name and Type
                    database.AddInParameter(dbCommand, paramBO.Name, paramBO.Type);
                    // Setting value of parameter
                    database.SetParameterValue(dbCommand, paramBO.Name, paramBO.Value);
                }
            }
            // Executing ExecuteReader() and returning records found
            myReader = database.ExecuteReader(dbCommand);
            return myReader;
        }

        /// <summary>
        /// // This Function will be used to Select Single/Multiple records with return output parameter
        /// </summary>
        /// <param name="paramList"></param>
        /// <param name="sprocName"></param>
        /// <returns></returns>
        protected void SelectRecord(ref DataSet myDataSet, ParameterList paramList, ref ParameterList outParamList, string sprocName, params DataTable[] tables)
        {
            // Holds the results/records found after executing stored procedure
            IDataReader myReader = null;
            database = DatabaseFactory.CreateDatabase();
            dbCommand = database.GetStoredProcCommand(sprocName);
            // For Each Loop to iterate over paramList
            // Parameter's will be extracted and added to StoredProcedure
            if ((paramList != null))
            {
                foreach (ParameterBO paramBO in paramList)
                {
                    // Adding parameter Name and Type
                    database.AddInParameter(dbCommand, paramBO.Name, paramBO.Type);
                    // Setting value of parameter
                    database.SetParameterValue(dbCommand, paramBO.Name, paramBO.Value);
                }
            }
            foreach (ParameterBO outParamBO in outParamList)
            {
                // Adding parameter Name and Type
                database.AddOutParameter(dbCommand, outParamBO.Name, outParamBO.Type, 8000);
            }
            // Executing ExecuteReader() and returning records found
            myReader = database.ExecuteReader(dbCommand);
            myDataSet.Load(myReader, LoadOption.OverwriteChanges, tables);
            dbCommand.Connection.Close();
            foreach (ParameterBO paramBO in outParamList)
            {
                paramBO.Value = database.GetParameterValue(dbCommand, paramBO.Name);
            }
           
        }

        // This Function will be used to Select Single/Multiple records

        protected ParameterList SelectRecord(ref ParameterList inParamList, ref ParameterList outParamList, string sprocName)
        {
            database = DatabaseFactory.CreateDatabase();
            dbCommand = database.GetStoredProcCommand(sprocName);

            // For Each Loop to iterate over paramList
            // Parameter's will be extracted and added to StoredProcedure


            foreach (ParameterBO paramBO in inParamList)
            {

                if (paramBO.SqlType == SqlDbType.Structured)
                {
                    dbCommand.Parameters.Add(new SqlParameter(paramBO.Name, paramBO.Value));
                }
                else
                {

                    // Adding parameter Name and Type
                    database.AddInParameter(dbCommand, paramBO.Name, paramBO.Type);

                    // Setting value of parameter
                    database.SetParameterValue(dbCommand, paramBO.Name, paramBO.Value);
                }
            }

            foreach (ParameterBO outParamBO in outParamList)
            {
                // Adding parameter Name and Type
                database.AddOutParameter(dbCommand, outParamBO.Name, outParamBO.Type, 8000);
            }

            database.ExecuteScalar(dbCommand);

            foreach (ParameterBO paramBO in outParamList)
            {
                paramBO.Value = database.GetParameterValue(dbCommand, paramBO.Name);

            }


            return outParamList;

        }

        #endregion        

        //public IEnumerable<T> SelectSingle<T>(string sprocName, int id, ParameterList paramList) where T : new()  // <---- here's the constraint
        //{
        //    var procedure = database.CreateSprocAccessor<T>(sprocName);            
        //    object[] paramValues = new object[paramList.Count];            
        //    return procedure.Execute(paramValues);            
        //}
        //protected void Select(<T>objectMapper, string sprocName)
        //{
        //    var procedure = database.CreateSprocAccessor<objectMapper>(sprocName);
        //    //roomsStatus = procedure.Execute(requestSentTime);
        //}

        //public void GenericAnonHandler<T>(IEnumerable<T> param, string sprocName)
        //{
        //    var procedure = database.CreateSprocAccessor<param>(sprocName);
        //    //roomsStatus = procedure.Execute(requestSentTime);
        //}

        //protected void Select()
        //{
        //    var procedure = database.CreateSprocAccessor<PageSortBO>("");
        //    //roomsStatus = procedure.Execute(requestSentTime);
        //}

        #region "Load DataSet"
        /// <summary>
        /// read data and from reader and prepare the data set
        /// </summary>
        /// <param name="myDataSet"></param>
        /// <param name="paramList"></param>
        /// <param name="sprocName"></param>
        protected void LoadDataSet(ref DataSet myDataSet, ref ParameterList paramList, string sprocName)
        {
            database = DatabaseFactory.CreateDatabase();
            dbCommand = database.GetStoredProcCommand(sprocName);
            //' For Each Loop to iterate over paramList
            //' Parameter's will be extracted and added to StoredProcedure
            foreach (ParameterBO paramBO in paramList)
            {
                //' Adding parameter Name and Type
                database.AddInParameter(dbCommand, paramBO.Name, paramBO.Type);
                //' Setting value of parameter
                database.SetParameterValue(dbCommand, paramBO.Name, paramBO.Value);
            }
            IDataReader reader = database.ExecuteReader(dbCommand);
            this.ConvertDataReaderToDataSet(ref myDataSet, reader);
            dbCommand.Connection.Close();
        }
        #endregion

        #region "Load DataSet"
        /// <summary>
        /// read data and from reader and prepare the data set
        /// </summary>
        /// <param name="myDataSet"></param>
        /// <param name="paramList"></param>
        /// <param name="outParamList"></param>
        /// <param name="sprocName"></param>
        protected void LoadDataSet(ref DataSet myDataSet, ref ParameterList paramList, ref ParameterList outParamList, string sprocName)
        {
            database = DatabaseFactory.CreateDatabase();
            dbCommand = database.GetStoredProcCommand(sprocName);
            //' For Each Loop to iterate over paramList
            //' Parameter's will be extracted and added to StoredProcedure
            foreach (ParameterBO paramBO in paramList)
            {
                //' Adding parameter Name and Type
                database.AddInParameter(dbCommand, paramBO.Name, paramBO.Type);
                //' Setting value of parameter
                database.SetParameterValue(dbCommand, paramBO.Name, paramBO.Value);
            }
            foreach (ParameterBO outParamBO in outParamList)
            {
                // Adding parameter Name and Type
                database.AddOutParameter(dbCommand, outParamBO.Name, outParamBO.Type, 8000);
            }
            IDataReader reader = database.ExecuteReader(dbCommand);
            this.ConvertDataReaderToDataSet(ref myDataSet, reader);
            dbCommand.Connection.Close();
            foreach (ParameterBO paramBO in outParamList)
            {
                paramBO.Value = database.GetParameterValue(dbCommand, paramBO.Name);
            }
        }
        #endregion

        #region "Conversion from dataReader to dataSet"
        /// <summary>
        /// Convert reader into dataset
        /// </summary>
        /// <param name="dataSet"></param>
        /// <param name="reader"></param>
        private void ConvertDataReaderToDataSet(ref DataSet dataSet, IDataReader reader)
        {
            DataTable schemaTable = reader.GetSchemaTable();
            DataTable dataTable = new DataTable();
            int intCounter = 0;
            if (schemaTable != null)
            {
                
                for (intCounter = 0; intCounter <= schemaTable.Rows.Count - 1; intCounter++)
                {
                    DataRow dataRow = schemaTable.Rows[intCounter];
                    string columnName = Convert.ToString(dataRow["ColumnName"]);
                    DataColumn column = new DataColumn(columnName, (Type)dataRow["DataType"]);
                    dataTable.Columns.Add(column);
                }
                dataSet.Tables.Add(dataTable);
            }
            while (reader.Read())
            {
                DataRow dataRow = dataTable.NewRow();
                for (intCounter = 0; intCounter <= reader.FieldCount - 1; intCounter++)
                {
                    dataRow[intCounter] = reader.GetValue(intCounter);
                }
                dataTable.Rows.Add(dataRow);
            }
        }

        #endregion

        #endregion

        public void Dispose()
        {
            //TODO: implement this method.
            throw new NotImplementedException();
        }
    }
}
