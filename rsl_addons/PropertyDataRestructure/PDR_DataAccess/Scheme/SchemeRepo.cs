using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PDR_DataAccess.Base;
using System.Data;
using PDR_BusinessObject;
using PDR_Utilities.Constants;
using PDR_BusinessObject.PageSort;
using PDR_BusinessObject.Parameter;
using PDR_BusinessObject.Scheme;
using PDR_BusinessObject.Restriction;

namespace PDR_DataAccess.Scheme
{
    public class SchemeRepo : BaseDAL, ISchemeRepo
    {
        #region get Scheme List
        /// <summary>
        /// get Scheme List
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        public Int32 getSchemeList(ref DataSet resultDataSet, PageSortBO objPageSortBO, String searchText)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO searchTextParam = new ParameterBO("searchText", searchText, DbType.String);
            paramList.Add(searchTextParam);

            ParameterBO pageNumberParam = new ParameterBO("PageNumber", objPageSortBO.PageNumber, DbType.Int32);
            paramList.Add(pageNumberParam);

            ParameterBO pageSizeParam = new ParameterBO("PageSize", objPageSortBO.PageSize, DbType.Int32);
            paramList.Add(pageSizeParam);

            ParameterBO SortDirectionParam = new ParameterBO("sortOrder", objPageSortBO.SmallSortDirection, DbType.String);
            paramList.Add(SortDirectionParam);

            ParameterBO SortExpressionParam = new ParameterBO("sortColumn", objPageSortBO.SortExpression, DbType.String);
            paramList.Add(SortExpressionParam);

            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);

            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetSchemeList);

            ParameterBO obj = outParametersList.First();
            Int32 outvalue = Convert.ToInt32(obj.Value.ToString());

            return outvalue;

        }
        #endregion

        #region populate Scheme DropDown
        /// <summary>
        /// populate Scheme DropDown
        /// </summary>
        /// <returns></returns>
        public DataSet populateSchemeDropDown()
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();
            ParameterBO schemeIdParam = new ParameterBO("developmentId", -1, DbType.Int32);
            paramList.Add(schemeIdParam);
            DataSet resultDataSet = new DataSet();
            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.PopulateSchemeDropDown);
            return resultDataSet;
        }
        #endregion


        #region populate Block DropDown
        /// <summary>
        /// populate Scheme DropDown
        /// </summary>
        /// <returns></returns>
        public DataSet populateBlockDropDown(int schemeId)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();
            ParameterBO schemeIdParam = new ParameterBO("SchemeId", schemeId, DbType.Int32);
            paramList.Add(schemeIdParam);
            DataSet resultDataSet = new DataSet();
            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetBlockDropDownBySchemeId);
            return resultDataSet;
        }
        #endregion


        #region get scheme reactive repairs
        /// <summary>
        /// get scheme reactive repairs
        /// </summary>
        /// <param name="schemeId"></param>
        /// <returns></returns>
        public DataSet getSchemeFaultsAndDefects(int schemeId)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO schemeIdParam = new ParameterBO("schemeId", schemeId, DbType.Int32);
            paramList.Add(schemeIdParam);
            DataSet resultDataSet = new DataSet();
            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetSchemeFaultsAndDefects);

            return resultDataSet;

        }
        #endregion

        #region get scheme Planned Appointment
        /// <summary>
        /// get scheme Planned Appointment
        /// </summary>
        /// <param name="schemeId"></param>
        /// <returns></returns>
        public DataSet getSchemePlannedAppointment(int schemeId)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO schemeIdParam = new ParameterBO("SchemeID", schemeId, DbType.Int32);
            paramList.Add(schemeIdParam);
            DataSet resultDataSet = new DataSet();
            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetSchemePlannedAppointment);

            return resultDataSet;

        }
        #endregion

        #region get scheme Planned Appointment
        /// <summary>
        /// get scheme Planned Appointment
        /// </summary>
        /// <param name="schemeId"></param>
        /// <param name="resultDataSet"></param>
        /// <returns></returns>
        public void GetReportedFaultsDefectsForSchemeBlock(ref DataSet resultDataSet, int schemeId)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO schemeIdParam = new ParameterBO("SchemeID", schemeId, DbType.Int32);
            paramList.Add(schemeIdParam);
            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetFaultsAndDefectsForSchemeBlock);
        }
        #endregion

        #region get blocks by schemeId
        /// <summary>
        /// get blocks by schemeId
        /// </summary>
        /// <param name="schemeId"></param>
        /// <param name="totalCount"></param>
        /// <returns></returns>
        public DataSet getBlocksBySchemeID(int schemeId, ref int totalCount)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO schemeIdParam = new ParameterBO("SchemeId", schemeId, DbType.Int32);
            paramList.Add(schemeIdParam);

            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);

            DataSet resultDataSet = new DataSet();
            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetBlocksBySchemeID);
            ParameterBO obj = outParametersList.First();
            totalCount = Convert.ToInt32(obj.Value.ToString());
            return resultDataSet;

        }
        #endregion

        #region get Scheme ASB Count
        /// <summary>
        /// get Scheme ASB Count
        /// </summary>
        /// <param name="schemeId"></param>
        /// <returns></returns>
        public DataSet getSchemeASBCount(int schemeId)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO schemeIdParam = new ParameterBO("SchemeID", schemeId, DbType.Int32);
            paramList.Add(schemeIdParam);
            DataSet resultDataSet = new DataSet();
            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetSchemeASBCount);

            return resultDataSet;

        }
        #endregion

        #region get Scheme Repairs Count
        /// <summary>
        /// get Scheme Repairs Count
        /// </summary>
        /// <param name="schemeId"></param>
        /// <returns></returns>
        public DataSet getSchemeRepairsCount(int schemeId)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO schemeIdParam = new ParameterBO("SchemeID", schemeId, DbType.Int32);
            paramList.Add(schemeIdParam);
            DataSet resultDataSet = new DataSet();
            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetSchemeRepairsCount);

            return resultDataSet;

        }
        #endregion

        #region get Scheme Repairs Count
        /// <summary>
        /// get Scheme Repairs Count
        /// </summary>
        /// <param name="schemeId"></param>
        /// <returns></returns>
        public DataSet getSchemeDetailBySchemeId(int schemeId)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO schemeIdParam = new ParameterBO("schemeId", schemeId, DbType.Int32);
            paramList.Add(schemeIdParam);
            DataSet resultDataSet = new DataSet();
            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetSchemeDetailBySchemeId);

            return resultDataSet;

        }
        #endregion

        #region"get Blocks & Properties by PhaseID"
        /// <summary>
        ///  Get Blocks and Properties by PhaseId
        /// </summary>
        /// <param name="blockPropertiesDataSet"></param>
        /// <param name="phaseId"></param>
        public void getBlocksPropertiesbyPhaseID(ref DataSet blockPropertiesDataSet, int phaseId, int developmentId)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO phaseIdParam = new ParameterBO("phaseId", phaseId, DbType.Int32);
            paramList.Add(phaseIdParam);
            ParameterBO developmentIdParam = new ParameterBO("developmentId", developmentId, DbType.Int32);
            paramList.Add(developmentIdParam);
            DataTable blockTable = new DataTable("blocks");
            DataTable propertiesTable = new DataTable("properties");
             blockPropertiesDataSet.Tables.Add(blockTable);
            blockPropertiesDataSet.Tables.Add(propertiesTable);
           
            IDataReader iResultDataReader = base.SelectRecord(paramList, SpNameConstants.GetBlocksPropertiesByPhaseId);
            blockPropertiesDataSet.Load(iResultDataReader, LoadOption.OverwriteChanges, blockTable);
            blockPropertiesDataSet.Load(iResultDataReader, LoadOption.OverwriteChanges, propertiesTable);
            iResultDataReader.Close();
        }
        #endregion

        #region"Save Recrod"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="objSchemeBO"></param>
         public string saveScheme(ref SaveSchemeBO objSchemeBO)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();
            string result = string.Empty;

            ParameterBO paramSchemeName = new ParameterBO("schemeName",objSchemeBO.SchemeName,DbType.String);
            paramList.Add(paramSchemeName);

            ParameterBO paramSchemeCode = new ParameterBO("SchemeCode", objSchemeBO.SchemeCode, DbType.String);
            paramList.Add(paramSchemeCode);

            ParameterBO paramDevelopment = new ParameterBO("developmentId", objSchemeBO.DevelopmentId, DbType.Int32);
            paramList.Add(paramDevelopment);

            ParameterBO paramPhaseId = new ParameterBO("PhaseId", objSchemeBO.PhaseId, DbType.Int32);
            paramList.Add(paramPhaseId);

            ParameterBO existingSchemeId = new ParameterBO("existingSchemeId", objSchemeBO.ExistingSchemeId, DbType.Int32);
            paramList.Add(existingSchemeId);
            string blockId = string.Empty;
            if (objSchemeBO.Blocks.Count > 0)
            {
                StringBuilder sbBlock = new StringBuilder();
                foreach (int b in objSchemeBO.Blocks)
                {
                    sbBlock.Append(b);
                    sbBlock.Append(",");
                }

                 blockId = sbBlock.ToString();
                blockId = blockId.Remove(blockId.Length - 1);
            }
            ParameterBO blockIds = new ParameterBO("blockIds", blockId, DbType.String);
            paramList.Add(blockIds);
            string propertyId = string.Empty; 
            StringBuilder sbProperties = new StringBuilder();
            if (objSchemeBO.Properties.Count > 0)
            {
                foreach (string p in objSchemeBO.Properties)
                {
                   // sbProperties.Append("'");
                    sbProperties.Append(p);
                    //sbProperties.Append("'");
                    sbProperties.Append(",");
                }
                propertyId = sbProperties.ToString();
                propertyId = propertyId.Remove(propertyId.Length - 1);
            }
            
            ParameterBO propertyIds = new ParameterBO("propertyIds", propertyId, DbType.String);
            paramList.Add(propertyIds);

            ParameterBO ParamSCHEMEID = new ParameterBO("SCHEMEID", 0, DbType.String);
            outParametersList.Add(ParamSCHEMEID);

            SaveRecord(ref paramList, ref outParametersList,SpNameConstants.SaveScheme);
            ParameterBO obj = outParametersList.First();
            objSchemeBO.SchemeId = obj.Value.ToString();

            if (objSchemeBO.SchemeId == "Exist")
            {
                result = "Exist";
            }
            else if (objSchemeBO.SchemeId == "Failed")
            {
                result = "failed";
            }
            else
            {
                result = "success";
                //Update Blocks with new SchemeId
                 
                //foreach (int b in objSchemeBO.Blocks)
                //{
                    
                //    UpdateSchemeBlocks(Convert.ToInt32(objSchemeBO.SchemeId), b);
                //}
                //// Update Properties with new SchemeId
                //foreach (string p in objSchemeBO.Properties)
                //{
                //    UpdateSchemeProperties(Convert.ToInt32(objSchemeBO.SchemeId), p);
                //}
            }

            return result;
        }


        private void UpdateSchemeBlocks(int SchemeId, int BlockId)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO paramSchemeId = new ParameterBO("SchemeId", SchemeId, DbType.Int32);
            paramList.Add(paramSchemeId);

            ParameterBO paramBlockId = new ParameterBO("BlockId", BlockId, DbType.Int32);
            paramList.Add(paramBlockId);

            SaveRecord(ref paramList, ref outParametersList, SpNameConstants.UpdateSchemeBlocks);
        }

        private void UpdateSchemeProperties(int SchemeId, string PropertyId)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO paramSchemeId = new ParameterBO("SchemeId", SchemeId, DbType.Int32);
            paramList.Add(paramSchemeId);

            ParameterBO paramPropertyId = new ParameterBO("PropertyId", PropertyId, DbType.String);
            paramList.Add(paramPropertyId);

            SaveRecord(ref paramList, ref outParametersList, SpNameConstants.UpdateSchemeProperties);
        }
        #endregion

        #region "get Scheme Data"
        public void getSchemeData(ref DataSet schemeDataSet, int schemeId)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO schemeIdParam = new ParameterBO("schemeId", schemeId, DbType.Int32);
            paramList.Add(schemeIdParam);

            DataTable schemeTable = new DataTable("scheme");
            DataTable blockTable = new DataTable("blocks");
            DataTable propertiesTable = new DataTable("properties");
            schemeDataSet.Tables.Add(schemeTable);
            schemeDataSet.Tables.Add(blockTable);
            schemeDataSet.Tables.Add(propertiesTable);

            IDataReader iResultDataReader = base.SelectRecord(paramList, SpNameConstants.GetSchemeData);
            schemeDataSet.Load(iResultDataReader, LoadOption.OverwriteChanges, schemeTable);
            schemeDataSet.Load(iResultDataReader, LoadOption.OverwriteChanges, blockTable);
            schemeDataSet.Load(iResultDataReader, LoadOption.OverwriteChanges, propertiesTable);
            iResultDataReader.Close();
        }
        #endregion

        #region Save Restriction
        /// <summary>
        /// Save Restriction
        /// </summary>
        /// <param name="objRestrictionBO"></param>
        /// <returns></returns>
        public bool SaveRestriction(ref RestrictionBO objRestrictionBO)
        {
            ParameterList inParamList = new ParameterList();
            ParameterList outParamList = new ParameterList();

            ParameterBO paramRestrictionId = new ParameterBO("RestrictionId", objRestrictionBO.RestrictionId, DbType.Int32);
            inParamList.Add(paramRestrictionId);

            ParameterBO paramPermittedPlanning = new ParameterBO("PermittedPlanning", objRestrictionBO.PermittedPlanning, DbType.String);
            inParamList.Add(paramPermittedPlanning);

            ParameterBO paramRelevantPlanning = new ParameterBO("RelevantPlanning", objRestrictionBO.RelevantPlanning, DbType.String);
            inParamList.Add(paramRelevantPlanning);

            ParameterBO paramRelevantTitle = new ParameterBO("RelevantTitle", objRestrictionBO.RelevantTitle, DbType.String);
            inParamList.Add(paramRelevantTitle);

            ParameterBO paramRestrictionComments = new ParameterBO("RestrictionComments", objRestrictionBO.RestrictionComments, DbType.String);
            inParamList.Add(paramRestrictionComments);

            ParameterBO paramAccessIssues = new ParameterBO("AccessIssues", objRestrictionBO.AccessIssues, DbType.String);
            inParamList.Add(paramAccessIssues);

            ParameterBO paramMediaIssues = new ParameterBO("MediaIssues", objRestrictionBO.MediaIssues, DbType.String);
            inParamList.Add(paramMediaIssues);

            ParameterBO paramThirdPartyAgreement = new ParameterBO("ThirdPartyAgreement", objRestrictionBO.ThirdPartyAgreement, DbType.String);
            inParamList.Add(paramThirdPartyAgreement);

            ParameterBO paramSpFundingArrangements = new ParameterBO("SpFundingArrangements", objRestrictionBO.SpFundingArrangements, DbType.String);
            inParamList.Add(paramSpFundingArrangements);

            ParameterBO paramIsRegistered = new ParameterBO("IsRegistered", objRestrictionBO.IsRegistered, DbType.Int32);
            inParamList.Add(paramIsRegistered);

            ParameterBO paramManagementDetail = new ParameterBO("ManagementDetail", objRestrictionBO.ManagementDetail, DbType.String);
            inParamList.Add(paramManagementDetail);

            ParameterBO paramNonBhaInsuranceDetail = new ParameterBO("NonBhaInsuranceDetail", objRestrictionBO.NonBhaInsuranceDetail, DbType.String);
            inParamList.Add(paramNonBhaInsuranceDetail);

            ParameterBO paramSchemeId = new ParameterBO("SchemeId", objRestrictionBO.SchemeId, DbType.Int32);
            inParamList.Add(paramSchemeId);

            ParameterBO paramUpdatedBy = new ParameterBO("UpdatedBy", objRestrictionBO.UpdatedBy, DbType.Int32);
            inParamList.Add(paramUpdatedBy);

            ParameterBO isSavedParam = new ParameterBO("IsSaved", 0, DbType.Boolean);
            outParamList.Add(isSavedParam);

            ParameterBO restrictionIdOutParam = new ParameterBO("RestrictionIdOut", -1, DbType.Int32);
            outParamList.Add(restrictionIdOutParam);

            outParamList = base.SelectRecord(ref inParamList, ref outParamList, SpNameConstants.SaveRestrictions);
            objRestrictionBO.RestrictionId = Convert.ToInt32(outParamList[1].Value);

            bool isSaved = false;
            isSaved = Convert.ToBoolean(outParamList[0].Value);
            return isSaved;

        }
        #endregion

        #region "Get Land Registration Options"
        /// <summary>
        /// Get Land Registration Options
        /// </summary>
        /// <param name="regDataSet"></param>        
        public void getLandRegistrationOptions(ref DataSet regDataSet)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            LoadDataSet(ref regDataSet, ref paramList, ref outParametersList, SpNameConstants.GetLandRegistrationOptions);

        }
        #endregion

        #region "get Restrictions"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dataSet"></param>
        /// <param name="schemeId"></param>
        public void getRestrictions(ref DataSet dataSet, int schemeId)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO schemeIdParam = new ParameterBO("schemeId", schemeId, DbType.Int32);
            paramList.Add(schemeIdParam);

            LoadDataSet(ref dataSet, ref paramList, ref outParametersList, SpNameConstants.GetSchemeRestrictions);

        }
        #endregion
    }
}

