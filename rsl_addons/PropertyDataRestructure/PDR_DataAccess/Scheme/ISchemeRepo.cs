﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using PDR_BusinessObject.PageSort;
using PDR_BusinessObject.Scheme;
using PDR_BusinessObject.Restriction;

namespace PDR_DataAccess.Scheme
{
    public interface ISchemeRepo
    {
        #region get Scheme List
        /// <summary>
        /// get Scheme List
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        Int32 getSchemeList(ref DataSet resultDataSet, PageSortBO objPageSortBO, String searchText);
        #endregion

        #region populate Scheme DropDown
        /// <summary>
        /// populate Scheme DropDown
        /// </summary>
        /// <returns></returns>
        DataSet populateSchemeDropDown();
        #endregion 

        #region populate Block DropDown
        /// <summary>
        /// populate Scheme DropDown
        /// </summary>
        /// <returns></returns>
        DataSet populateBlockDropDown(int schemeId);
        #endregion 


        #region get Scheme Faults And Defects
        /// <summary>
        /// get scheme reactive repairs
        /// </summary>
        /// <param name="schemeId"></param>
        /// <returns></returns>
        DataSet getSchemeFaultsAndDefects(int schemeId);
        #endregion

        #region get Scheme Planned Appointment
        /// <summary>
        /// get Scheme Planned Appointment
        /// </summary>
        /// <param name="schemeId"></param>
        /// <returns></returns>
        DataSet getSchemePlannedAppointment(int schemeId);
        #endregion

        #region Get Reported Faults/Defects For Scheme/Block
        /// <summary>
        /// Get Reported Faults/Defects For Scheme/Block
        /// </summary>
        /// <param name="schemeId"></param>
        /// <param name="resultDataSet"></param>
        /// <returns></returns>
        void GetReportedFaultsDefectsForSchemeBlock(ref DataSet resultDataSet, int schemeId);
        #endregion

        #region get Blocks By Scheme ID
        /// <summary>
        /// get Blocks By Scheme ID
        /// </summary>
        /// <param name="schemeId"></param>
        /// <param name="totalCount"></param>
        /// <returns></returns>
        DataSet getBlocksBySchemeID(int schemeId, ref int totalCount);
        #endregion 

        #region get Scheme ASB Count
        /// <summary>
        /// get Scheme ASB Count
        /// </summary>
        /// <param name="schemeId"></param>
        /// <returns></returns>
        DataSet getSchemeASBCount(int schemeId);
        #endregion

        #region get Scheme Repairs Count
        /// <summary>
        /// get Scheme Repairs Count
        /// </summary>
        /// <param name="schemeId"></param>
        /// <returns></returns>
        DataSet getSchemeRepairsCount(int schemeId);
        #endregion

        #region get Scheme Detail By Scheme Id
        /// <summary>
        /// get Scheme Detail By Scheme Id
        /// </summary>
        /// <param name="schemeId"></param>
        /// <returns></returns>
        DataSet getSchemeDetailBySchemeId(int schemeId);
        #endregion 
        
        #region"get Blocks & Properties by PhaseID"
        /// <summary>
        ///  Get Blocks and Properties by PhaseId
        /// </summary>
        /// <param name="blockPropertiesDataSet"></param>
        /// <param name="phaseId"></param>
        void getBlocksPropertiesbyPhaseID(ref DataSet blockPropertiesDataSet, int phaseId, int developmentID);
        #endregion

        #region "Save Scheme"
        /// <summary>
        /// Save Scheme Record
        /// </summary>
        /// <param name="objSchemeBO"></param>
        string saveScheme(ref SaveSchemeBO objSchemeBO);
        #endregion

        #region"get Scheme Data"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="schemeDataSet"></param>
        /// <param name="schemeId"></param>
        void getSchemeData(ref DataSet schemeDataSet, int schemeId);
        #endregion
        
        #region "Get Scheme Restrictions"
        /// <summary>
        /// Get Scheme Restrictions
        /// </summary>
        /// <param name="dataSet"></param>
        /// <param name="schemeId"></param>
        void getRestrictions(ref DataSet dataSet, int schemeId);
        #endregion   
                
        #region Save Restriction
        /// <summary>
        /// Save Block
        /// </summary>
        /// <param name="objRestrictionBO"></param>
        /// <returns></returns>
        bool SaveRestriction(ref RestrictionBO objRestrictionBO);
        #endregion 
 
        #region "Get Land Registration Options"
        /// <summary>
        /// Get Land Registration Options
        /// </summary>
        /// <param name="dataSet"></param>        
        void getLandRegistrationOptions(ref DataSet dataSet);
        #endregion 

    }
}
