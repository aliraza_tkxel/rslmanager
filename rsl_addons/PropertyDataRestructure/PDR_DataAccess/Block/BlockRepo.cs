﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PDR_DataAccess.Base;
using System.Data;
using PDR_BusinessObject;
using PDR_Utilities.Constants;
using PDR_BusinessObject.PageSort;
using PDR_BusinessObject.Parameter;
using PDR_BusinessObject.Block;
using PDR_BusinessObject.Warranties;
using PDR_BusinessObject.SchemeBlock;
using PDR_BusinessObject.Asbestos;
using PDR_BusinessObject.Restriction;

namespace PDR_DataAccess.Block
{
    public class BlockRepo: BaseDAL, IBlockRepo
    {
        #region get Block List
        /// <summary>
        /// get Block List
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        public Int32 getBlockList(ref DataSet resultDataSet, PageSortBO objPageSortBO, String searchText)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO searchTextParam = new ParameterBO("searchText", searchText, DbType.String);
            paramList.Add(searchTextParam);

            ParameterBO pageNumberParam = new ParameterBO("PageNumber", objPageSortBO.PageNumber, DbType.Int32);
            paramList.Add(pageNumberParam);

            ParameterBO pageSizeParam = new ParameterBO("PageSize", objPageSortBO.PageSize, DbType.Int32);
            paramList.Add(pageSizeParam);

            ParameterBO SortDirectionParam = new ParameterBO("sortOrder", objPageSortBO.SmallSortDirection, DbType.String);
            paramList.Add(SortDirectionParam);

            ParameterBO SortExpressionParam = new ParameterBO("sortColumn", objPageSortBO.SortExpression, DbType.String);
            paramList.Add(SortExpressionParam);

            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);

            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetBlockList);

            ParameterBO obj = outParametersList.First();
            Int32 outvalue = Convert.ToInt32(obj.Value.ToString());

            return outvalue;

        }
        #endregion 

        #region getBlockTemplate
        /// <summary>
        /// get Block Template
        /// </summary>
        /// <param name="TemplateDataSet"></param>
        public void getBlockTemplate(ref DataSet TemplateDataSet)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            LoadDataSet(ref TemplateDataSet, ref paramList, ref outParametersList, SpNameConstants.GetBlockTempaltes);
        }
        #endregion

        #region getDevelopments
        /// <summary>
        /// get Developments
        /// </summary>
        /// <param name="DevelopmentDataSet"></param>
        public void getDevelopments(ref DataSet DevelopmentDataSet)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            LoadDataSet(ref DevelopmentDataSet, ref paramList, ref outParametersList, SpNameConstants.GetDevelopment);
        }
        #endregion

        #region get Ownership Data
        /// <summary>
        /// get Ownership Data
        /// </summary>
        /// <param name="ownershipDataSet"></param>
        public void getOwnershipData(ref DataSet ownershipDataSet)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            LoadDataSet(ref ownershipDataSet, ref paramList, ref outParametersList, SpNameConstants.GetPropertyOwnerShip);
        }
        #endregion

        #region get Block Template Data
        /// <summary>
        /// get Block Template Data
        /// </summary>
        /// <param name="BlockTemplateDataSet"></param>
        /// <param name="templateId"></param>
        public void getBlockTemplateData(ref DataSet BlockTemplateDataSet, int templateId)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO paramBlockId = new ParameterBO("blockId",templateId, DbType.Int32);
            paramList.Add(paramBlockId);

            LoadDataSet(ref BlockTemplateDataSet, ref paramList, ref outParametersList, SpNameConstants.GetBlockTemplateData);
        }
        #endregion

        #region Save Restriction
        /// <summary>
        /// Save Restriction
        /// </summary>
        /// <param name="objRestrictionBO"></param>
        /// <returns></returns>
        public bool SaveRestriction(ref RestrictionBO objRestrictionBO)
        {            
            ParameterList inParamList = new ParameterList();
            ParameterList outParamList = new ParameterList();

            ParameterBO paramRestrictionId = new ParameterBO("RestrictionId", objRestrictionBO.RestrictionId, DbType.Int32);
            inParamList.Add(paramRestrictionId);

            ParameterBO paramPermittedPlanning = new ParameterBO("PermittedPlanning", objRestrictionBO.PermittedPlanning, DbType.String);
            inParamList.Add(paramPermittedPlanning);

            ParameterBO paramRelevantPlanning = new ParameterBO("RelevantPlanning", objRestrictionBO.RelevantPlanning, DbType.String);
            inParamList.Add(paramRelevantPlanning);

            ParameterBO paramRelevantTitle = new ParameterBO("RelevantTitle", objRestrictionBO.RelevantTitle, DbType.String);
            inParamList.Add(paramRelevantTitle);

            ParameterBO paramRestrictionComments = new ParameterBO("RestrictionComments", objRestrictionBO.RestrictionComments, DbType.String);
            inParamList.Add(paramRestrictionComments);

            ParameterBO paramAccessIssues = new ParameterBO("AccessIssues", objRestrictionBO.AccessIssues, DbType.String);
            inParamList.Add(paramAccessIssues);

            ParameterBO paramMediaIssues = new ParameterBO("MediaIssues", objRestrictionBO.MediaIssues, DbType.String);
            inParamList.Add(paramMediaIssues);

            ParameterBO paramThirdPartyAgreement = new ParameterBO("ThirdPartyAgreement", objRestrictionBO.ThirdPartyAgreement, DbType.String);
            inParamList.Add(paramThirdPartyAgreement);

            ParameterBO paramSpFundingArrangements = new ParameterBO("SpFundingArrangements", objRestrictionBO.SpFundingArrangements, DbType.String);
            inParamList.Add(paramSpFundingArrangements);

            ParameterBO paramIsRegistered = new ParameterBO("IsRegistered", objRestrictionBO.IsRegistered, DbType.Int32);
            inParamList.Add(paramIsRegistered);

            ParameterBO paramManagementDetail = new ParameterBO("ManagementDetail", objRestrictionBO.ManagementDetail, DbType.String);
            inParamList.Add(paramManagementDetail);

            ParameterBO paramNonBhaInsuranceDetail = new ParameterBO("NonBhaInsuranceDetail", objRestrictionBO.NonBhaInsuranceDetail, DbType.String);
            inParamList.Add(paramNonBhaInsuranceDetail);

            ParameterBO paramBlockId = new ParameterBO("BlockId", objRestrictionBO.BlockId, DbType.Int32);
            inParamList.Add(paramBlockId);

            ParameterBO paramUpdatedBy = new ParameterBO("UpdatedBy", objRestrictionBO.UpdatedBy, DbType.Int32);
            inParamList.Add(paramUpdatedBy);

            ParameterBO isSavedParam = new ParameterBO("IsSaved", 0, DbType.Boolean);
            outParamList.Add(isSavedParam);

            ParameterBO restrictionIdOutParam = new ParameterBO("RestrictionIdOut", -1, DbType.Int32);
            outParamList.Add(restrictionIdOutParam);
            
            outParamList = base.SelectRecord(ref inParamList, ref outParamList, SpNameConstants.SaveRestrictions);
            objRestrictionBO.RestrictionId = Convert.ToInt32(outParamList[1].Value);
            
            bool isSaved = false;
            isSaved = Convert.ToBoolean(outParamList[0].Value);
            return isSaved;
            
        }
        #endregion

        #region Save Block
        /// <summary>
        /// Save Block
        /// </summary>
        /// <param name="objSaveBlockBO"></param>
        /// <returns></returns>
        public string SaveBlock(BlockBO objSaveBlockBO)
        {
            string result = string.Empty;
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO paramBlockName = new ParameterBO("BlockName", objSaveBlockBO.BlockName, DbType.String);
            paramList.Add(paramBlockName);

            ParameterBO paramBlockReference = new ParameterBO("BlockReference", objSaveBlockBO.BlockReference, DbType.String);
            paramList.Add(paramBlockReference);

            ParameterBO paramDevelopmentId = new ParameterBO("DevelopmentId", objSaveBlockBO.DevelopmentId, DbType.Int32);
            paramList.Add(paramDevelopmentId);

            ParameterBO paramPhaseId = new ParameterBO("PhaseId", objSaveBlockBO.PhaseId, DbType.Int32);
            paramList.Add(paramPhaseId);

            ParameterBO paramAddress1 = new ParameterBO("Address1", objSaveBlockBO.Address1, DbType.String);
            paramList.Add(paramAddress1);

            ParameterBO paramAddress2 = new ParameterBO("Address2", objSaveBlockBO.Address2, DbType.String);
            paramList.Add(paramAddress2);

            ParameterBO paramAddress3 = new ParameterBO("Address3", objSaveBlockBO.Address3, DbType.String);
            paramList.Add(paramAddress3);

            ParameterBO paramCounty = new ParameterBO("County", objSaveBlockBO.County, DbType.String);
            paramList.Add(paramCounty);

            ParameterBO paramTownCity = new ParameterBO("Town", objSaveBlockBO.TownCity, DbType.String);
            paramList.Add(paramTownCity);

            ParameterBO paramPostCode = new ParameterBO("PostCode", objSaveBlockBO.PostCode, DbType.String);
            paramList.Add(paramPostCode);

            ParameterBO paramOwnerShip = new ParameterBO("OwnerShip", objSaveBlockBO.OwnerShip, DbType.Int32);
            paramList.Add(paramOwnerShip);

            ParameterBO paramBuildDate = new ParameterBO("BuildDate", objSaveBlockBO.BuildDate, DbType.Date);
            paramList.Add(paramBuildDate);

            ParameterBO paramNoOfProperties = new ParameterBO("NoOfProperties", objSaveBlockBO.NoOfProperties, DbType.Int32);
            paramList.Add(paramNoOfProperties);

            ParameterBO existingBlockId = new ParameterBO("existingBlockId", objSaveBlockBO.ExistingBlockId, DbType.Int32);
            paramList.Add(existingBlockId);

            ParameterBO paramBlockId = new ParameterBO("BlockId", 0, DbType.String);
            outParametersList.Add(paramBlockId);

            SaveRecord(ref paramList, ref outParametersList,SpNameConstants.SaveBlock);

            ParameterBO obj = outParametersList.First();
            objSaveBlockBO.BlockId = obj.Value.ToString();

            if (objSaveBlockBO.BlockId == "Exist")
            {
                result = "failed, already exists ";
            }
            else if (objSaveBlockBO.BlockId == "Failed")
            {
                result = "failed";
            }
            else
            {
                result = "success";
            }

            return result;
        }
        #endregion

        #region get Block Warranties
        /// <summary>
        /// get Block Warranties
        /// </summary>
        /// <param name="warrantyDataSet"></param>
        /// <param name="Id"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="requestType"></param>
        public void getBlockWarranties(ref DataSet warrantyDataSet, int Id, PageSortBO objPageSortBO, string requestType)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO paramBlockId = new ParameterBO("Id", Id.ToString(), DbType.String);
            paramList.Add(paramBlockId);

            ParameterBO pageNumberParam = new ParameterBO("PageNumber", objPageSortBO.PageNumber, DbType.Int32);
            paramList.Add(pageNumberParam);

            ParameterBO pageSizeParam = new ParameterBO("PageSize", objPageSortBO.PageSize, DbType.Int32);
            paramList.Add(pageSizeParam);

            ParameterBO SortDirectionParam = new ParameterBO("sortOrder", objPageSortBO.SmallSortDirection, DbType.String);
            paramList.Add(SortDirectionParam);

            ParameterBO SortExpressionParam = new ParameterBO("sortColumn", objPageSortBO.SortExpression, DbType.String);
            paramList.Add(SortExpressionParam);

            ParameterBO requestTypeParam = new ParameterBO("requestType", requestType, DbType.String);
            paramList.Add(requestTypeParam);

            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);

            LoadDataSet(ref warrantyDataSet, ref paramList, ref outParametersList, SpNameConstants.GetWarrantyList);

            
        }
        #endregion 

        #region get NHBC Warranties
        /// <summary>
        /// get NHBC Warranties
        /// </summary>
        /// <param name="warrantyDataSet"></param>
        /// <param name="Id"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="requestType"></param>
        public void getNHBCWarranties(ref DataSet warrantyDataSet, int Id, string requestType)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO paramBlockId = new ParameterBO("Id", Id.ToString(), DbType.String);
            paramList.Add(paramBlockId);

            ParameterBO requestTypeParam = new ParameterBO("requestType", requestType, DbType.String);
            paramList.Add(requestTypeParam);


            LoadDataSet(ref warrantyDataSet, ref paramList, ref outParametersList, SpNameConstants.GetNHBCWarrantyList);

        }
        #endregion 

        #region get Contractors
        /// <summary>
        /// get contractors
        /// </summary>
        /// <param name="contractorDataSet"></param>
        public void getContractors(ref DataSet contractorDataSet)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            LoadDataSet(ref contractorDataSet, ref paramList, ref outParametersList, SpNameConstants.GetContractors);
        }
        #endregion

        #region get for Dropdown Value
        /// <summary>
        /// get for Dropdown Value
        /// </summary>
        /// <param name="forDataSet"></param>
        public void getforDropdownValue(ref DataSet forDataSet)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            LoadDataSet(ref forDataSet, ref paramList, ref outParametersList, SpNameConstants.GetForDropDownValues);
        }
        #endregion

        #region get Category Dropdown Value
        /// <summary>
        /// get Category Dropdown Value
        /// </summary>
        /// <param name="forDataSet"></param>
        public void getCategoryDropdownValue(ref DataSet categoryDataSet)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            LoadDataSet(ref categoryDataSet, ref paramList, ref outParametersList, SpNameConstants.GetCategoryDropDownValues);
        }
        #endregion

        #region get Area Dropdown Value
        /// <summary>
        /// get Area Dropdown Value
        /// </summary>
        /// <param name="forDataSet"></param>
        public void getAreaDropdownValue(ref DataSet areaDataSet, int CategoryId)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();
            ParameterBO paramWarrantyType = new ParameterBO("locationId", CategoryId, DbType.Int32);
            paramList.Add(paramWarrantyType);

            LoadDataSet(ref areaDataSet, ref paramList, ref outParametersList, SpNameConstants.GetAreaDropDownValuesByCategory);
        }
        #endregion

        #region get Item Dropdown Value
        /// <summary>
        /// get Item Dropdown Value
        /// </summary>
        /// <param name="forDataSet"></param>
        public void getItemDropdownValue(ref DataSet itemDataSet, int AreaId)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();
            ParameterBO paramWarrantyType = new ParameterBO("areaId", AreaId, DbType.Int32);
            paramList.Add(paramWarrantyType);

            LoadDataSet(ref itemDataSet, ref paramList, ref outParametersList, SpNameConstants.GetItemDropDownValuesByArea);
        }
        #endregion

        #region get Warranty Types
        /// <summary>
        /// get Warranty Types
        /// </summary>
        /// <param name="warrantyTypeDataSet"></param>
        public void getWarrantyTypes(ref DataSet warrantyTypeDataSet)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            LoadDataSet(ref warrantyTypeDataSet, ref paramList, ref outParametersList, SpNameConstants.GetWarrantyTypes);
        }
        #endregion

        #region save Warranty
        /// <summary>
        /// save Warranty
        /// </summary>
        /// <param name="objWarrantyBO"></param>
        public void saveWarranty(WarrantyBO objWarrantyBO)
        {
             ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO paramWarrantyType = new ParameterBO("warrantyType", objWarrantyBO.WarrantyType, DbType.Int32);
            paramList.Add(paramWarrantyType);


            ParameterBO paramCategory = new ParameterBO("Category", objWarrantyBO.Category, DbType.Int32);
            paramList.Add(paramCategory);

            ParameterBO paramArea = new ParameterBO("Area", objWarrantyBO.Area, DbType.Int32);
            paramList.Add(paramArea);

            ParameterBO paramItem = new ParameterBO("Item", objWarrantyBO.Item, DbType.Int32);
            paramList.Add(paramItem);


            ParameterBO paramContractor = new ParameterBO("Contractor", objWarrantyBO.Contractor, DbType.Int32);
            paramList.Add(paramContractor);


            ParameterBO paramExpiry = new ParameterBO("EXPIRYDATE", objWarrantyBO.Expiry, DbType.DateTime);
            paramList.Add(paramExpiry);


            ParameterBO paramNotes = new ParameterBO("Notes", objWarrantyBO.Notes, DbType.String);
            paramList.Add(paramNotes);


            ParameterBO paramId = new ParameterBO("Id", objWarrantyBO.Id, DbType.Int32);
            paramList.Add(paramId);

            ParameterBO paramWarrantyId = new ParameterBO("WarrantyId", objWarrantyBO.WarrantyId, DbType.Int32);
            paramList.Add(paramWarrantyId);

            ParameterBO paramRequestType = new ParameterBO("RequestType", objWarrantyBO.RequestType, DbType.String);
            paramList.Add(paramRequestType);

            SaveRecord(ref paramList, ref outParametersList, SpNameConstants.SaveWarranty);
        }
        #endregion

        #region get Warranty Data
        /// <summary>
        /// get Warranty Data
        /// </summary>
        /// <param name="warrantyDataSet"></param>
        /// <param name="warrantyId"></param>
        public void getWarrantyData(ref DataSet warrantyDataSet, int warrantyId)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO paramWarrantyID = new ParameterBO("warrantyId", warrantyId, DbType.Int32);
            paramList.Add(paramWarrantyID);

            LoadDataSet(ref warrantyDataSet, ref paramList, ref outParametersList,SpNameConstants.GetWarrantyData);
        }
        #endregion

        #region"delete Warranty
        /// <summary>
        /// delete Warranty
        /// </summary>
        /// <param name="warrantyId"></param>
        public void deleteWarranty(int warrantyId)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO paramWarrantyID = new ParameterBO("warrantyId", warrantyId, DbType.Int32);
            paramList.Add(paramWarrantyID);

            SaveRecord(ref paramList, ref outParametersList, SpNameConstants.DeleteWarranty);
        }
        #endregion

        #region get Properties
        /// <summary>
        /// get Properties
        /// </summary>
        /// <param name="propertiesDataSet"></param>
        /// <param name="Id"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="requestType"></param>
        public int getProperties(ref DataSet propertiesDataSet, int Id, PageSortBO objPageSortBO, string requestType)
        {
            int totalCount = 0;
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO paramBlockId = new ParameterBO("Id", Id, DbType.Int32);
            paramList.Add(paramBlockId);

            ParameterBO pageNumberParam = new ParameterBO("PageNumber", objPageSortBO.PageNumber, DbType.Int32);
            paramList.Add(pageNumberParam);

            ParameterBO pageSizeParam = new ParameterBO("PageSize", objPageSortBO.PageSize, DbType.Int32);
            paramList.Add(pageSizeParam);

            ParameterBO SortDirectionParam = new ParameterBO("sortOrder", objPageSortBO.SmallSortDirection, DbType.String);
            paramList.Add(SortDirectionParam);

            ParameterBO SortExpressionParam = new ParameterBO("sortColumn", objPageSortBO.SortExpression, DbType.String);
            paramList.Add(SortExpressionParam);

            ParameterBO requestTypeParam = new ParameterBO("requestType", requestType, DbType.String);
            paramList.Add(requestTypeParam);

            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);

            LoadDataSet(ref propertiesDataSet, ref paramList, ref outParametersList, SpNameConstants.GetPropertiesList);
            ParameterBO obj = outParametersList.First();
            totalCount = Convert.ToInt32(obj.Value.ToString());
            return totalCount;
        }
        #endregion

        #region get Properties By Dev Phase Block
        /// <summary>
        /// get Properties
        /// </summary>
        /// <param name="propertiesDataSet"></param>
        /// <param name="developmentId"></param>
        /// <param name="phaseId"></param>
        /// <param name="blockid"></param>
        /// 
        public void getPropertiesByDevPhaseBlock(ref DataSet propertiesDataSet, int developmentId, int phaseId, int blockid )
        {
            ParameterList paramList = new ParameterList();

            ParameterBO paramDevelopmentId = new ParameterBO("developmentId", developmentId, DbType.Int32);
            paramList.Add(paramDevelopmentId);

            ParameterBO paramPhaseId = new ParameterBO("phaseId", phaseId, DbType.Int32);
            paramList.Add(paramPhaseId);

            ParameterBO paramBlockid = new ParameterBO("blockid", blockid, DbType.Int32);
            paramList.Add(paramBlockid);

            DataTable propertiesTable = new DataTable("properties");
            propertiesDataSet.Tables.Add(propertiesTable);

            //LoadDataSet(ref propertiesDataSet, ref paramList,  SpNameConstants.GetPropertiesByDevPhaseBlock);

            IDataReader iResultDataReader = base.SelectRecord(paramList, SpNameConstants.GetPropertiesByDevPhaseBlock);
            propertiesDataSet.Load(iResultDataReader, LoadOption.OverwriteChanges, propertiesTable);
            iResultDataReader.Close();
        
        }
        #endregion

        #region remove Properties
        /// <summary>
        /// remove Properties
        /// </summary>
        /// <param name="PropertyRef"></param>
        /// <param name="requestType"></param>
        public void removeProperties(string PropertyRef,string requestType)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO paramPropertyRef = new ParameterBO("PropertyRef", PropertyRef, DbType.String);
            paramList.Add(paramPropertyRef);

            ParameterBO paramRequestType = new ParameterBO("requestType", requestType, DbType.String);
            paramList.Add(paramRequestType);

            SaveRecord(ref paramList, ref outParametersList, SpNameConstants.RemoveProperties);
        }
        #endregion

        #region "get Asbestos And Risk"
        /// <summary>
        /// get Asbestos And Risk dropdown values 
        /// </summary>
        /// <param name="resultDataSet"></param>
        public void getAsbestosAndRisk(ref DataSet resultDataSet)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            DataTable dtAsbestosLevel = new DataTable("AsbestosLevel");
            resultDataSet.Tables.Add(dtAsbestosLevel);
            DataTable dtAsbestosElements = new DataTable("AsbestosElements");
            resultDataSet.Tables.Add(dtAsbestosElements);
            DataTable dtRisk = new DataTable("Risk");
            DataTable dtLevel = new DataTable("Level");
            resultDataSet.Tables.Add(dtRisk);
            resultDataSet.Tables.Add(dtLevel);
            LoadDataSet(ref resultDataSet, ref paramList, SpNameConstants.GetAsbestosAndRiskInformation);

            IDataReader iResultDataReader = base.SelectRecord(paramList, SpNameConstants.GetAsbestosAndRiskInformation);
            resultDataSet.Load(iResultDataReader, LoadOption.OverwriteChanges, dtAsbestosLevel, dtAsbestosElements, dtRisk, dtLevel);
            iResultDataReader.Close();
        }
        #endregion
   
        #region get Block List for dropdown List
        /// <summary>
        /// get Block List for dropdown List
        /// </summary>
        /// <param name="schemeId"></param>
        /// <returns></returns>
        public DataSet getBlockListBySchemeId(int schemeId)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO schemeIdParam = new ParameterBO("schemeId", schemeId, DbType.Int32);
            paramList.Add(schemeIdParam);

            
            DataSet resultDataSet = new DataSet();
            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetBlockListBySchemeID);

            return resultDataSet;

        }
        #endregion 

        #region "get Block Data"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="blockDataSet"></param>
        /// <param name="blockId"></param>
        public void getBlockData(ref DataSet blockDataSet, int blockId)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO blockIdParam = new ParameterBO("blockId", blockId, DbType.Int32);
            paramList.Add(blockIdParam);

            LoadDataSet(ref blockDataSet, ref paramList, ref outParametersList, SpNameConstants.GetBlockTemplateData);

        }
        #endregion

        #region "get Block Restrictions"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="blockDataSet"></param>
        /// <param name="blockId"></param>
        public void getBlockRestrictions(ref DataSet blockDataSet, int blockId)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO blockIdParam = new ParameterBO("blockId", blockId, DbType.Int32);
            paramList.Add(blockIdParam);

            LoadDataSet(ref blockDataSet, ref paramList, ref outParametersList, SpNameConstants.GetBlockRestrictions);

        }
        #endregion


        #region "Get Land Registration Options"
        /// <summary>
        /// Get Land Registration Options
        /// </summary>
        /// <param name="regDataSet"></param>        
        public void getLandRegistrationOptions(ref DataSet regDataSet)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            LoadDataSet(ref regDataSet, ref paramList, ref outParametersList, SpNameConstants.GetLandRegistrationOptions);

        }
        #endregion

        #region "get SchemeBlock Asbestos And Risk"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ddlDataSet"></param>
        public void getSchemeBlockAsbestosAndRisk(ref DataSet ddlDataSet)
        {

        }
        #endregion

        #region " get SchemeBlock Asbestos Risk And OtherInfo"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="asbestosId"></param>
        public void getSchemeBlockAsbestosRiskAndOtherInfo(ref DataSet resultDataSet, int asbestosId)
        {
            ParameterList parameterList = new ParameterList();
            ParameterBO asbestosIDParam = new ParameterBO("asbestosID", asbestosId, DbType.Int32);
            parameterList.Add(asbestosIDParam);
            LoadDataSet(ref resultDataSet,ref  parameterList,SpNameConstants.getAsbestosRiskAndOtherInfo);
        }
        #endregion

        #region Refurbishment Tab
        
        public string saveRefurbishment(RefurbishmentBO objRefurbishmentBO)
        {
            ParameterList parameterList = new ParameterList();
            ParameterList outparameterList = new ParameterList();
            ParameterBO RefurbishmentDateParam = new ParameterBO("REFURBISHMENT_DATE", objRefurbishmentBO.RefurbishmentDate, DbType.Date);
            parameterList.Add(RefurbishmentDateParam);
            ParameterBO NotesParam = new ParameterBO("NOTES", objRefurbishmentBO.Notes, DbType.String);
            parameterList.Add(NotesParam);
            ParameterBO PropertyIdParam = new ParameterBO("Id", objRefurbishmentBO.Id, DbType.Int32);
            parameterList.Add(PropertyIdParam);
            ParameterBO userIdParam = new ParameterBO("USER_ID", objRefurbishmentBO.UserId, DbType.Int32);
            parameterList.Add(userIdParam);
            ParameterBO requestTypeParam = new ParameterBO("requestType",objRefurbishmentBO.RequestType, DbType.String);
            parameterList.Add(requestTypeParam);

            ParameterBO errorMessgaeParam = new ParameterBO("ErrorMessage", 0, DbType.String);
            outparameterList.Add(errorMessgaeParam);
            base.SaveRecord(ref parameterList,ref outparameterList,SpNameConstants.saveRefurbishmentInformaiton);

            ParameterBO obj = outparameterList.First();

            return obj.Value.ToString();
        }

        public void getRefurbishmentInformation(ref DataSet resultDataSet, int Id, string requestType)
        {
            ParameterList parametersList = new ParameterList();
            ParameterBO IdParam = new ParameterBO("Id", Id, DbType.Int32);
            parametersList.Add(IdParam);

            ParameterBO requestTypeParam = new ParameterBO("requestType", requestType, DbType.String);
            parametersList.Add(requestTypeParam);

            LoadDataSet(ref resultDataSet,ref parametersList,SpNameConstants.getRefurbishmentInformaiton);
        }
        #endregion

        #region getHealthAndSafetyTabInformation

        public int getHealthAndSafetyTabInformation(ref DataSet resultDataSet, int Id, PageSortBO objPageSortBo, string requestType)
        {
            ParameterList parametersList = new ParameterList();
            ParameterList outParametersList = new ParameterList();
            ParameterBO propertyIdParam = new ParameterBO("Id", Id, DbType.Int32);
            parametersList.Add(propertyIdParam);
            ParameterBO requestTypeParam = new ParameterBO("requestType", requestType, DbType.String);
            parametersList.Add(requestTypeParam);
            ParameterBO pageSize = new ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32);
            parametersList.Add(pageSize);
            ParameterBO pageNumber = new ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32);
            parametersList.Add(pageNumber);
            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);
            base.LoadDataSet(ref resultDataSet,ref parametersList,ref outParametersList,SpNameConstants.getHealthAndSafetyTabInformation);
            ParameterBO obj = outParametersList.First();

            return Convert.ToInt32( obj.Value.ToString());
        }
        #endregion

        #region saveAsbestos
        /// <summary>
        /// 
        /// </summary>
        /// <param name="objAsbestosBO"></param>
        public void saveAsbestos(AsbestosBO objAsbestosBO)
        {
            ParameterList parameterList = new ParameterList();
            ParameterList outparameterList = new ParameterList();
            ParameterBO asbestosLevelId = new ParameterBO("asbestosLevelId", objAsbestosBO.AsbestosLevelId, DbType.Int32);
            parameterList.Add(asbestosLevelId);
            ParameterBO asbestosElementId = new ParameterBO("asbestosElementId", objAsbestosBO.AsbestosElementId, DbType.Int32);
            parameterList.Add(asbestosElementId);
            ParameterBO RiskIdParam = new ParameterBO("riskId", objAsbestosBO.RiskId, DbType.String);
            parameterList.Add(RiskIdParam);
            ParameterBO AddedDateParam = new ParameterBO("addedDate", objAsbestosBO.AddedDate, DbType.Date);
            parameterList.Add(AddedDateParam);
            ParameterBO RemovedParam = new ParameterBO("removedDate", objAsbestosBO.RemovedDate, DbType.String);
            parameterList.Add(RemovedParam);
            ParameterBO riskLevelParam = new ParameterBO("riskLevel", objAsbestosBO.RiskLevel, DbType.Int32);
            parameterList.Add(riskLevelParam);

            ParameterBO urgentActionRequiredParam = new ParameterBO("UrgentActionRequired", objAsbestosBO.UrgentActionRequired, DbType.Boolean);
            parameterList.Add(urgentActionRequiredParam);

            ParameterBO NotesParam = new ParameterBO("notes", objAsbestosBO.Notes, DbType.String);
            parameterList.Add(NotesParam);
            ParameterBO IdParam = new ParameterBO("Id", objAsbestosBO.Id, DbType.Int32);
            parameterList.Add(IdParam);
            ParameterBO RequestTypeParam = new ParameterBO("RequestType", objAsbestosBO.RequestType, DbType.String);
            parameterList.Add(RequestTypeParam);
            ParameterBO userIdParam = new ParameterBO("userId", objAsbestosBO.UserId, DbType.Int32);
            parameterList.Add(userIdParam);
            base.SaveRecord(ref parameterList,ref outparameterList,SpNameConstants.saveAsbestosInformation);
        }
        #endregion

        #region amendAsbestos
        public void amendAsbestos(AsbestosBO objAsbestosBO)
        {
            ParameterList parameterList = new ParameterList();
            ParameterList outparameterList = new ParameterList();
            ParameterBO asbestosLevelId = new ParameterBO("asbestosLevelId", objAsbestosBO.AsbestosLevelId, DbType.Int32);
            parameterList.Add(asbestosLevelId);
            ParameterBO asbestosElementId = new ParameterBO("asbestosElementId", objAsbestosBO.AsbestosElementId, DbType.Int32);
            parameterList.Add(asbestosElementId);
            ParameterBO RiskIdParam = new ParameterBO("riskId", objAsbestosBO.RiskId, DbType.String);
            parameterList.Add(RiskIdParam);
            ParameterBO AddedDateParam = new ParameterBO("addedDate", objAsbestosBO.AddedDate, DbType.Date);
            parameterList.Add(AddedDateParam);
            ParameterBO RemovedParam = new ParameterBO("removedDate", objAsbestosBO.RemovedDate, DbType.String);
            parameterList.Add(RemovedParam);
            ParameterBO riskLevelParam = new ParameterBO("riskLevel", objAsbestosBO.RiskLevel, DbType.Int32);
            parameterList.Add(riskLevelParam);

            ParameterBO urgentActionRequiredParam = new ParameterBO("UrgentActionRequired", objAsbestosBO.UrgentActionRequired, DbType.Boolean);
            parameterList.Add(urgentActionRequiredParam);
            ParameterBO NotesParam = new ParameterBO("notes", objAsbestosBO.Notes, DbType.String);
            parameterList.Add(NotesParam);
            ParameterBO PropasbLevelId = new ParameterBO("propasbLevelId", objAsbestosBO.PropasbLevelID, DbType.String);
            parameterList.Add(PropasbLevelId);
            ParameterBO userIdParam = new ParameterBO("userId", objAsbestosBO.UserId, DbType.Int32);
            parameterList.Add(userIdParam);
            base.SaveRecord(ref parameterList, ref outparameterList, SpNameConstants.amendAsbestosInformation);
        }

        #region "Get Details for email"
		 


        /// <summary>
        /// This function is to get details for email, these details include contractor name, email and other details.
        /// Contact details of customer/tenant.
        /// Risk/Vulnerability details of the tenant
        /// Property Address
        /// </summary>
        /// <param name="detailsForEmailDS">Dataset to get details returned from Database.</param>
        /// <param name="propertyId">Assign to Contractor Bo containing values used to get details</param>
        /// <remarks></remarks>
        public void GetEmployeeEmailForAsbestosEmailNotifications(string propertyId, string requestType, ref DataSet detailsForEmailDS)
        {
            var inParamList = new ParameterList();

            var propertyIdParam = new ParameterBO("propertyId", propertyId, DbType.String);
            inParamList.Add(propertyIdParam);

            var requestTypeParam = new ParameterBO("requestType", requestType, DbType.String);
            inParamList.Add(requestTypeParam);

            DataTable AsbestosEmailNotificationTable = new DataTable("AsbestosEmailNotification");
            detailsForEmailDS.Tables.Add(AsbestosEmailNotificationTable);

            IDataReader iResultDataReader = base.SelectRecord(inParamList, SpNameConstants.GetEmployeeEmailForAsbestosEmailNotifications);
            detailsForEmailDS.Load(iResultDataReader, LoadOption.OverwriteChanges, AsbestosEmailNotificationTable);
            iResultDataReader.Close();
        }

        #endregion
        #endregion
    }
}
