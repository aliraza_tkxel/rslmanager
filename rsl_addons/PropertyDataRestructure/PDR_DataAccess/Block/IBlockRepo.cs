﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using PDR_BusinessObject.PageSort;
using PDR_BusinessObject.Block;
using PDR_BusinessObject.Warranties;
using PDR_BusinessObject.SchemeBlock;
using PDR_BusinessObject.Asbestos;
using PDR_BusinessObject.Restriction;

namespace PDR_DataAccess.Block
{
    public interface IBlockRepo
    {
        #region get Block List
        /// <summary>
        /// get Block List
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        Int32 getBlockList(ref DataSet resultDataSet, PageSortBO objPageSortBO, String searchText);        
        #endregion 

        #region getBlockTemplate
        /// <summary>
        /// get Block Template
        /// </summary>
        /// <param name="TemplateDataSet"></param>
        void getBlockTemplate(ref DataSet TemplateDataSet);        
        #endregion

        #region getDevelopments
        /// <summary>
        /// get Developments
        /// </summary>
        /// <param name="DevelopmentDataSet"></param>
        void getDevelopments(ref DataSet DevelopmentDataSet);
        #endregion

        #region get Ownership Data
        /// <summary>
        /// get Ownership Data
        /// </summary>
        /// <param name="ownershipDataSet"></param>
        void getOwnershipData(ref DataSet ownershipDataSet);
        #endregion

        #region get Block Template Data
        /// <summary>
        /// get Block Template Data
        /// </summary>
        /// <param name="BlockTemplateDataSet"></param>
        /// <param name="templateId"></param>
        void getBlockTemplateData(ref DataSet BlockTemplateDataSet, int templateId);
        #endregion

        #region Save Block
        /// <summary>
        /// Save Block
        /// </summary>
        /// <param name="objSaveBlockBO"></param>
        /// <returns></returns>
        string SaveBlock(BlockBO objSaveBlockBO);
        #endregion

        #region Save Restriction
        /// <summary>
        /// Save Block
        /// </summary>
        /// <param name="objRestrictionBO"></param>
        /// <returns></returns>
        bool SaveRestriction(ref RestrictionBO objRestrictionBO);
        #endregion        

        #region get Block Warranties
        /// <summary>
        /// get Block Warranties
        /// </summary>
        /// <param name="warrantyDataSet"></param>
        /// <param name="Id"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="requestType"></param>
        void getBlockWarranties(ref DataSet warrantyDataSet, int Id, PageSortBO objPageSortBO, string requestType);        
        #endregion

        #region get NHBC Warranties
        /// <summary>
        /// get NHBC Warranties
        /// </summary>
        /// <param name="warrantyDataSet"></param>
        /// <param name="Id"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="requestType"></param>
        void getNHBCWarranties(ref DataSet warrantyDataSet, int id, string requestType);
        #endregion     

        #region get Contractors
        /// <summary>
        /// get contractors
        /// </summary>
        /// <param name="contractorDataSet"></param>
        void getContractors(ref DataSet contractorDataSet);
        #endregion

        #region get for Dropdown Value
        /// <summary>
        /// get for Dropdown Value
        /// </summary>
        /// <param name="forDataSet"></param>
        void getforDropdownValue(ref DataSet forDataSet);
        #endregion
        #region get Category Dropdown Value
        /// <summary>
        /// get category Dropdown Value
        /// </summary>
        /// <param name="forDataSet"></param>
        void getCategoryDropdownValue(ref DataSet categoryDataSet);
        #endregion
        #region get Area Dropdown Value
        /// <summary>
        /// get area Dropdown Value
        /// </summary>
        /// <param name="forDataSet"></param>
        void getAreaDropdownValue(ref DataSet areaDataSet, int CategoryId);
        #endregion
        #region get Item Dropdown Value
        /// <summary>
        /// get item Dropdown Value
        /// </summary>
        /// <param name="forDataSet"></param>
        void getItemDropdownValue(ref DataSet itemDataSet, int ItemId);
        #endregion

        #region get Warranty Types
        /// <summary>
        /// get Warranty Types
        /// </summary>
        /// <param name="warrantyTypeDataSet"></param>
        void getWarrantyTypes(ref DataSet warrantyTypeDataSet);
        #endregion

        #region save Warranty
        /// <summary>
        /// save Warranty
        /// </summary>
        /// <param name="objWarrantyBO"></param>
        void saveWarranty(WarrantyBO objWarrantyBO);
        #endregion

        #region get Warranty Data
        /// <summary>
        /// get Warranty Data
        /// </summary>
        /// <param name="warrantyDataSet"></param>
        /// <param name="warrantyId"></param>
        void getWarrantyData(ref DataSet warrantyDataSet, int warrantyId);
        #endregion

        #region"delete Warranty
        /// <summary>
        /// delete Warranty
        /// </summary>
        /// <param name="warrantyId"></param>
        void deleteWarranty(int warrantyId);
        #endregion

        #region get Properties
        /// <summary>
        /// get Properties
        /// </summary>
        /// <param name="propertiesDataSet"></param>
        /// <param name="Id"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="requestType"></param>
        int getProperties(ref DataSet propertiesDataSet, int Id, PageSortBO objPageSortBO, string requestType);
        #endregion


        #region get Properties By Dev Phase Block
        /// <summary>
        /// get Properties
        /// </summary>
        /// <param name="propertiesDataSet"></param>
        /// <param name="developmentId"></param>
        /// <param name="phaseId"></param>
        /// <param name="blockid"></param>
        /// 
        void getPropertiesByDevPhaseBlock(ref DataSet propertiesDataSet, int developmentId, int phaseId, int blockid);
        #endregion


        #region remove Properties
        /// <summary>
        /// remove Properties
        /// </summary>
        /// <param name="PropertyRef"></param>
        /// <param name="requestType"></param>
        void removeProperties(string PropertyRef, string requestType);
        #endregion

        #region "get Asbestos And Risk"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="resultDataSet"></param>
        void getAsbestosAndRisk(ref DataSet resultDataSet);
        #endregion

        #region get Block List for dropdown List
        /// <summary>
        /// get Block List for dropdown List
        /// </summary>
        /// <param name="schemeId"></param>
        /// <returns></returns>
        DataSet getBlockListBySchemeId(int schemeId);
       
        #endregion 
        

        #region "get Block Data"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="blockDataSet"></param>
        /// <param name="blockId"></param>
        void getBlockData(ref DataSet blockDataSet, int blockId);
        #endregion

        #region "Get Block Restrictions"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="blockDataSet"></param>
        /// <param name="blockId"></param>
        void getBlockRestrictions(ref DataSet blockDataSet, int blockId);
        #endregion
        
        #region "Get Land Registration Options"
        /// <summary>
        /// Get Land Registration Options
        /// </summary>
        /// <param name="dataSet"></param>        
        void getLandRegistrationOptions(ref DataSet dataSet);
        #endregion          

        #region"get SchemeBlock Asbestos And Risk"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ddlDataSet"></param>
        void getSchemeBlockAsbestosAndRisk(ref DataSet ddlDataSet);
        #endregion


        #region"get SchemeBlock AsbestosRisk And OtherInfo"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="asbestosId"></param>
        void getSchemeBlockAsbestosRiskAndOtherInfo(ref DataSet resultDataSet, int asbestosId);
        #endregion

        #region Save Refurbishment
        /// <summary>
        /// 
        /// </summary>
        /// <param name="objRefurbishmentBO"></param>
        /// <returns></returns>
        string saveRefurbishment(RefurbishmentBO objRefurbishmentBO);
        #endregion

        #region SchemeBlock Refurbishment Info
        /// <summary>
        /// 
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="Id"></param>
        void getRefurbishmentInformation(ref DataSet resultDataSet, int Id, string requestType);
        #endregion

        #region getHealthAndSafetyTabInformation
        /// <summary>
        /// 
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="Id"></param>
        /// <param name="objPageSortBo"></param>
        /// <param name="requestType"></param>
        int getHealthAndSafetyTabInformation(ref DataSet resultDataSet, int Id, PageSortBO objPageSortBo, string requestType);
        #endregion

        #region saveAsbestos
        void saveAsbestos(AsbestosBO objAsbestosBO);
        #endregion

        #region amendAsbestos
        void amendAsbestos(AsbestosBO objAsbestosBO);
        void GetEmployeeEmailForAsbestosEmailNotifications(string propertyId, string requestType, ref DataSet employeeEmailDataSet);
        #endregion
    }
}
