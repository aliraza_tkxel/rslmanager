﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using PDR_BusinessObject.CyclicalServices;

namespace PDR_DataAccess.CyclicalServices
{
    public interface ICyclicalServicesRepo
    {
        int getServicesToBeAllocated(ref DataSet resultDataSet, string searchText, int offset, int limit, string sortColumn, string sortOrder, int schemeId, int attributeId);
        int getServicesAllocated(ref DataSet resultDataSet, string searchText, int offset, int limit, string sortColumn, string sortOrder);
        void getCostCentreDropDownVales(ref DataSet resultDataSet);
        void getBudgetHeadDropDownValuesByCostCentreId(ref DataSet resultDataSet, int CostCentreId);
        void getContactDropDownValuesbyContractorId(ref DataSet resultDataSet, int contractorId);
        void getServiceItemDetails(ref DataSet resultDataSet, string serviceItemIds);
        void updateCycleValue(double cycleValue, int serviceItemId);
        bool generatePo(DataTable contractorWorks, DataTable contractorWorkDetail);
        DataSet getdetailsForEmail(int empolyeeId, int serviceItemId);
        string getPoStatusForSessionCreation(int orderId);
        DataSet getPurchaseOrderContractorDetails(int orderId);
        bool acceptCyclicalPurchaseOrder(int orderId, string status, ref string contactName, ref string email);
        bool updateWorksCompleted(UpdateWorksCompletedBO request, int userId);
        bool updateInvoiceUpload(UpdateWorksCompletedBO request, int userId);
        int getCyclicalTab(ref DataSet resultDataSet, string searchText, int offset, int limit, string sortColumn, string sortOrder, int schemeId, int blockId);
        int getUnassignedServices(ref DataSet resultDataSet, string searchText, int offset, int limit, string sortColumn, string sortOrder, int getOnlyCount, int schemeId);
        bool cancelCyclicalWork(int orderId, string reason, string notes, int cancelBy, string transactionIdentity);
        int getCancelledOrder(ref DataSet resultDataSet, string searchText, int offset, int limit, string sortColumn, string sortOrder);
    }
}
