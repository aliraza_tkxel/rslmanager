﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PDR_DataAccess.Base;
using PDR_BusinessObject.Parameter;
using System.Data;
using PDR_BusinessObject.PageSort;
using PDR_Utilities.Constants;
using PDR_BusinessObject.CyclicalServices;

namespace PDR_DataAccess.CyclicalServices
{
    public class CyclicalServicesRepo : BaseDAL, ICyclicalServicesRepo
    {
        public int getServicesToBeAllocated(ref DataSet resultDataSet, string searchText, int offset, int limit, string sortColumn, string sortOrder, int schemeId, int attributeId)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO searchTextParam = new ParameterBO("searchText", searchText, DbType.String);
            paramList.Add(searchTextParam);

            ParameterBO offsetParam = new ParameterBO("offset", offset, DbType.Int32);
            paramList.Add(offsetParam);

            ParameterBO limitParam = new ParameterBO("limit", limit, DbType.Int32);
            paramList.Add(limitParam);

            ParameterBO schemeIdParam = new ParameterBO("schemeId", schemeId, DbType.Int32);
            paramList.Add(schemeIdParam);

            ParameterBO attributeIdParam = new ParameterBO("attribute", attributeId, DbType.Int32);
            paramList.Add(attributeIdParam);

            ParameterBO SortDirectionParam = new ParameterBO("sortColumn", sortColumn, DbType.String);
            paramList.Add(SortDirectionParam);

            ParameterBO SortExpressionParam = new ParameterBO("sortOrder", sortOrder, DbType.String);
            paramList.Add(SortExpressionParam);



            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);


            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetServicesToBeAllocated);

            ParameterBO obj = outParametersList.First();
            Int32 outvalue = Convert.ToInt32(obj.Value.ToString());

            return outvalue;
        }

        public int getServicesAllocated(ref DataSet resultDataSet, string searchText, int offset, int limit, string sortColumn, string sortOrder)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO searchTextParam = new ParameterBO("searchText", searchText, DbType.String);
            paramList.Add(searchTextParam);

            ParameterBO offsetParam = new ParameterBO("offset", offset, DbType.Int32);
            paramList.Add(offsetParam);

            ParameterBO limitParam = new ParameterBO("limit", limit, DbType.Int32);
            paramList.Add(limitParam);

            ParameterBO SortDirectionParam = new ParameterBO("sortColumn", sortColumn, DbType.String);
            paramList.Add(SortDirectionParam);

            ParameterBO SortExpressionParam = new ParameterBO("sortOrder", sortOrder, DbType.String);
            paramList.Add(SortExpressionParam);



            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);


            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetServicesAllocated);

            ParameterBO obj = outParametersList.First();
            Int32 outvalue = Convert.ToInt32(obj.Value.ToString());

            return outvalue;
        }


        public void getCostCentreDropDownVales(ref DataSet resultDataSet)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();
            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetCostCentreDropDownValues);
        }

        public void getBudgetHeadDropDownValuesByCostCentreId(ref DataSet resultDataSet, int CostCentreId)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO costCentreIdparam = new ParameterBO("costCentreId", CostCentreId, DbType.Int32);
            paramList.Add(costCentreIdparam);

            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetBudgetHeadDropDownValuesByCostCentreId);
        }


        public void getContactDropDownValuesbyContractorId(ref DataSet resultDataSet, int contractorId)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO costCentreIdparam = new ParameterBO("contractorId", contractorId, DbType.Int32);
            paramList.Add(costCentreIdparam);

            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetContactDropDownValuesbyContractorId);
        }

        public void getServiceItemDetails(ref DataSet resultDataSet, string serviceItemIds)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO serviceItemIdsparam = new ParameterBO("serviceItemIds", serviceItemIds, DbType.String);
            paramList.Add(serviceItemIdsparam);

            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetServiceItemDetails);

        }

        public void updateCycleValue(double cycleValue, int serviceItemId)
        {
            ParameterList parameterList = new ParameterList();
            ParameterList outparameterList = new ParameterList();

            ParameterBO itemIdParam = new ParameterBO("cycleValue", cycleValue, DbType.Double);
            parameterList.Add(itemIdParam);

            ParameterBO schemeIdParam = new ParameterBO("serviceItemId", serviceItemId, DbType.Int32);
            parameterList.Add(schemeIdParam);

            SaveRecord(ref parameterList, ref outparameterList, SpNameConstants.UdateCycleValue);

        }


        public bool generatePo(DataTable contractorWorks, DataTable contractorWorkDetail)
        {
            ParameterList inParamList = new ParameterList();
            ParameterList outParamList = new ParameterList();
            ParameterBO contractorWorksParam = new ParameterBO("ContractorWorks", contractorWorks, SqlDbType.Structured);
            inParamList.Add(contractorWorksParam);

            ParameterBO contractorWorkDetailParam = new ParameterBO("ContractorWorkDetail", contractorWorkDetail, SqlDbType.Structured);
            inParamList.Add(contractorWorkDetailParam);

            ParameterBO isSavedParam = new ParameterBO("isSaved", 0, DbType.Boolean);
            outParamList.Add(isSavedParam);

            outParamList = base.SelectRecord(ref inParamList, ref outParamList, SpNameConstants.AssignCyclicalServicesToContractor);


            bool isSaved = false;
            isSaved = Convert.ToBoolean(outParamList[0].Value);
            return isSaved;

        }

        #region "Get Details for email"

        /// <summary>
        /// This function is to get details for email, these details include contractor name, email and other details.
        /// Contact details of customer/tenant.
        /// Risk/Vulnerability details of the tenant
        /// Property Address
        /// </summary>
        /// <param name="detailsForEmailDS">Dataset to get details returned from Database.</param>
        /// <param name="assignToContractorBo">Assign to Contractor Bo containing values used to get details</param>
        /// <remarks></remarks>
        public DataSet getdetailsForEmail(int empolyeeId, int serviceItemId)
        {
            ParameterList inParamList = new ParameterList();
            DataSet detailsForEmailDS = new DataSet();
            ParameterBO serviceItemIdParam = new ParameterBO("serviceItemId", serviceItemId, DbType.Int32);
            inParamList.Add(serviceItemIdParam);

            ParameterBO empolyeeIdParam = new ParameterBO("empolyeeId", empolyeeId, DbType.Int32);
            inParamList.Add(empolyeeIdParam);

            dynamic datareader = base.SelectRecord(inParamList, SpNameConstants.CMGetDetailforEmailToContractor);

            detailsForEmailDS.Load(datareader, LoadOption.OverwriteChanges,
                    ApplicationConstants.ContractorDetailsDt,
                    ApplicationConstants.PurchaseOrderDetailsDt,
                    ApplicationConstants.PurchaseItemDetailsDt
                    );
            datareader.Close();
            return detailsForEmailDS;
        }

        #endregion

        #region Get PO Status For Session Creation

        public string getPoStatusForSessionCreation(int orderId)
        {
            ParameterList parametersList = new ParameterList();
            ParameterList outparameterList = new ParameterList();
            ParameterBO orderIdParam = new ParameterBO("orderId", orderId, DbType.Int32);
            parametersList.Add(orderIdParam);
            DataSet resultDataSet = new DataSet();
            LoadDataSet(ref resultDataSet, ref parametersList, ref outparameterList, SpNameConstants.CMGetPoStatusForSessionCreating);
            if (resultDataSet.Tables[0].Rows.Count > 0)
            {
                return resultDataSet.Tables[0].Rows[0]["PoStatus"].ToString();
            }
            else
            {
                return string.Empty;
            }
        }
        #endregion


        public DataSet getPurchaseOrderContractorDetails(int orderId)
        {
            ParameterList inParamList = new ParameterList();
            DataSet detailsForEmailDS = new DataSet();
            ParameterBO orderIdParam = new ParameterBO("orderId", orderId, DbType.Int32);
            inParamList.Add(orderIdParam);

            dynamic datareader = base.SelectRecord(inParamList, SpNameConstants.CMPurchaseOrderContractorDetails);

            detailsForEmailDS.Load(datareader, LoadOption.OverwriteChanges,

                    ApplicationConstants.PurchaseOrderDetailsDt,
                    ApplicationConstants.AsbestosDetailsForContractorsAcceptPOPage
                    );
            datareader.Close();
            return detailsForEmailDS;
        }



        public bool acceptCyclicalPurchaseOrder(int orderId, string status, ref string contactName, ref string email)
        {
            ParameterList inParamList = new ParameterList();
            ParameterList outParamList = new ParameterList();

            ParameterBO orderIdParam = new ParameterBO("orderId", orderId, DbType.Int32);
            inParamList.Add(orderIdParam);

            ParameterBO statusParam = new ParameterBO("status", status, DbType.String);
            inParamList.Add(statusParam);

            ParameterBO isSavedParam = new ParameterBO("isSaved", 0, DbType.Boolean);
            outParamList.Add(isSavedParam);

            ParameterBO contactNameParam = new ParameterBO("contactName", 0, DbType.String);
            outParamList.Add(contactNameParam);

            ParameterBO emailParam = new ParameterBO("email", 0, DbType.String);
            outParamList.Add(emailParam);

            outParamList = base.SelectRecord(ref inParamList, ref outParamList, SpNameConstants.AcceptCyclicalPurchaseOrder);
            bool isSaved = false;
            isSaved = Convert.ToBoolean(outParamList[0].Value);
            contactName = outParamList[1].Value.ToString();
            email = outParamList[2].Value.ToString();
            return isSaved;
        }

        public bool updateWorksCompleted(UpdateWorksCompletedBO request, int userId)
        {
            ParameterList inParamList = new ParameterList();
            ParameterList outParamList = new ParameterList();

            ParameterBO cmContractorIdParam = new ParameterBO("cmContractorId", request.cmContractorId, DbType.Int32);
            inParamList.Add(cmContractorIdParam);

            ParameterBO cycleCompletedParam = new ParameterBO("cycleCompleted", request.cycleCompleted, DbType.DateTime);
            inParamList.Add(cycleCompletedParam);
            ParameterBO purchaseOrderIdParam = new ParameterBO("purchaseOrderId", request.purchaseOrderId, DbType.Int32);
            inParamList.Add(purchaseOrderIdParam);
            ParameterBO purchaseOrderItemIdParam = new ParameterBO("purchaseOrderItemId", request.purchaseOrderItemId, DbType.Int32);
            inParamList.Add(purchaseOrderItemIdParam);
            ParameterBO statusParam = new ParameterBO("status", request.status, DbType.String);
            inParamList.Add(statusParam);
            ParameterBO workDetailIdParam = new ParameterBO("workDetailId", request.workDetailId, DbType.Int32);
            inParamList.Add(workDetailIdParam);
            ParameterBO transactionIdentityParam = new ParameterBO("transactionIdentity", request.transactionIdentity, DbType.String);
            inParamList.Add(transactionIdentityParam);
            ParameterBO userIdParam = new ParameterBO("userId", userId, DbType.Int32);
            inParamList.Add(userIdParam);
            ParameterBO isSavedParam = new ParameterBO("isSaved", 0, DbType.Boolean);
            outParamList.Add(isSavedParam);

            outParamList = base.SelectRecord(ref inParamList, ref outParamList, SpNameConstants.UpdateWorksCompleted);
            bool isSaved = false;
            isSaved = Convert.ToBoolean(outParamList[0].Value);
            return isSaved;
        }

        public bool updateInvoiceUpload(UpdateWorksCompletedBO request, int userId)
        {
            ParameterList inParamList = new ParameterList();
            ParameterList outParamList = new ParameterList();

            ParameterBO cmContractorIdParam = new ParameterBO("cmContractorId", request.cmContractorId, DbType.Int32);
            inParamList.Add(cmContractorIdParam);
            ParameterBO workDetailIdParam = new ParameterBO("workDetailId", request.workDetailId, DbType.Int32);
            inParamList.Add(workDetailIdParam);

            ParameterBO transactionIdentityParam = new ParameterBO("transactionIdentity", request.transactionIdentity, DbType.String);
            inParamList.Add(transactionIdentityParam);

            ParameterBO purchaseOrderIdParam = new ParameterBO("purchaseOrderId", request.purchaseOrderId, DbType.Int32);
            inParamList.Add(purchaseOrderIdParam);

            ParameterBO purchaseOrderItemIdParam = new ParameterBO("purchaseOrderItemId", request.purchaseOrderItemId, DbType.Int32);
            inParamList.Add(purchaseOrderItemIdParam);

            ParameterBO statusParam = new ParameterBO("status", request.status, DbType.String);
            inParamList.Add(statusParam);

            ParameterBO fileParam = new ParameterBO("file", request.file, DbType.String);
            inParamList.Add(fileParam);

            ParameterBO userIdParam = new ParameterBO("userId", userId, DbType.Int32);
            inParamList.Add(userIdParam);


            ParameterBO isSavedParam = new ParameterBO("isSaved", 0, DbType.Boolean);
            outParamList.Add(isSavedParam);

            outParamList = base.SelectRecord(ref inParamList, ref outParamList, SpNameConstants.UpdateInvoiceUpload);
            bool isSaved = false;
            isSaved = Convert.ToBoolean(outParamList[0].Value);
            return isSaved;
        }


        public int getCyclicalTab(ref DataSet resultDataSet, string searchText, int offset, int limit, string sortColumn, string sortOrder, int schemeId, int blockId)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO searchTextParam = new ParameterBO("searchText", searchText, DbType.String);
            paramList.Add(searchTextParam);

            ParameterBO offsetParam = new ParameterBO("offset", offset, DbType.Int32);
            paramList.Add(offsetParam);

            ParameterBO limitParam = new ParameterBO("limit", limit, DbType.Int32);
            paramList.Add(limitParam);

            ParameterBO schemeIdParam = new ParameterBO("schemeId", schemeId, DbType.Int32);
            paramList.Add(schemeIdParam);

            ParameterBO blockIdParam = new ParameterBO("blockId", blockId, DbType.Int32);
            paramList.Add(blockIdParam);

            ParameterBO SortDirectionParam = new ParameterBO("sortColumn", sortColumn, DbType.String);
            paramList.Add(SortDirectionParam);

            ParameterBO SortExpressionParam = new ParameterBO("sortOrder", sortOrder, DbType.String);
            paramList.Add(SortExpressionParam);

            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);

            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetCyclicalTab);

            ParameterBO obj = outParametersList.First();
            Int32 outvalue = Convert.ToInt32(obj.Value.ToString());
            return outvalue;
        }




        public int getUnassignedServices(ref DataSet resultDataSet, string searchText, int offset, int limit, string sortColumn, string sortOrder, int getOnlyCount, int schemeId)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO searchTextParam = new ParameterBO("searchText", searchText, DbType.String);
            paramList.Add(searchTextParam);

            ParameterBO offsetParam = new ParameterBO("offset", offset, DbType.Int32);
            paramList.Add(offsetParam);

            ParameterBO limitParam = new ParameterBO("limit", limit, DbType.Int32);
            paramList.Add(limitParam);

            ParameterBO schemeIdParam = new ParameterBO("schemeId", schemeId, DbType.Int32);
            paramList.Add(schemeIdParam);
            ParameterBO getOnlyCountParam = new ParameterBO("getOnlyCount", getOnlyCount, DbType.Int32);
            paramList.Add(getOnlyCountParam);

            ParameterBO SortDirectionParam = new ParameterBO("sortColumn", sortColumn, DbType.String);
            paramList.Add(SortDirectionParam);

            ParameterBO SortExpressionParam = new ParameterBO("sortOrder", sortOrder, DbType.String);
            paramList.Add(SortExpressionParam);

            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);

            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetUnassignedCyclicalServices);

            ParameterBO obj = outParametersList.First();
            Int32 outvalue = Convert.ToInt32(obj.Value.ToString());
            return outvalue;
        }


        public bool cancelCyclicalWork(int orderId, string reason, string notes, int cancelBy, string transactionIdentity)
        {
            ParameterList inParamList = new ParameterList();
            ParameterList outParamList = new ParameterList();

            ParameterBO cmContractorIdParam = new ParameterBO("orderId", orderId, DbType.Int32);
            inParamList.Add(cmContractorIdParam);


            ParameterBO reasonParam = new ParameterBO("reason", reason, DbType.String);
            inParamList.Add(reasonParam);

            ParameterBO notesParam = new ParameterBO("notes", notes, DbType.String);
            inParamList.Add(notesParam);

            ParameterBO CancelByParam = new ParameterBO("cancelBy", cancelBy, DbType.Int32);
            inParamList.Add(CancelByParam);


            ParameterBO transactionIdentityParam = new ParameterBO("transactionIdentity", transactionIdentity, DbType.String);
            inParamList.Add(transactionIdentityParam);

            ParameterBO isSavedParam = new ParameterBO("isSaved", 0, DbType.Boolean);
            outParamList.Add(isSavedParam);

            outParamList = base.SelectRecord(ref inParamList, ref outParamList, SpNameConstants.CancelCyclicalWork);
            bool isSaved = false;
            isSaved = Convert.ToBoolean(outParamList[0].Value);
            return isSaved;
        }
        public int getCancelledOrder(ref DataSet resultDataSet, string searchText, int offset, int limit, string sortColumn, string sortOrder)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO searchTextParam = new ParameterBO("searchText", searchText, DbType.String);
            paramList.Add(searchTextParam);

            ParameterBO offsetParam = new ParameterBO("offset", offset, DbType.Int32);
            paramList.Add(offsetParam);

            ParameterBO limitParam = new ParameterBO("limit", limit, DbType.Int32);
            paramList.Add(limitParam);

            ParameterBO SortDirectionParam = new ParameterBO("sortColumn", sortColumn, DbType.String);
            paramList.Add(SortDirectionParam);

            ParameterBO SortExpressionParam = new ParameterBO("sortOrder", sortOrder, DbType.String);
            paramList.Add(SortExpressionParam);

            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);

            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetCancelledOrder);

            ParameterBO obj = outParametersList.First();
            Int32 outvalue = Convert.ToInt32(obj.Value.ToString());
            return outvalue;
        }


    }
}
