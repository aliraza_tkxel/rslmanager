﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PDR_DataAccess.Base;
using System.Data;
using PDR_BusinessObject;
using PDR_Utilities.Constants;
using PDR_BusinessObject.Parameter;
using PDR_BusinessObject.PageSort;

namespace PDR_DataAccess.Maintenance
{
   public class MaintenanceRepo : BaseDAL, IMaintenanceRepo
    {

        #region Get Maintenance Types
        /// <summary>
        /// Get Maintenance Types
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <returns></returns>
        public void getMaintenanceTypes(ref DataSet resultDataSet)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();
            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetAttributeTypeForAssignToContractor);
        }
        #endregion

        #region Get All Schemes
        /// <summary>
        /// Get All Schemes
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <returns></returns>
       public void getAllSchemes(ref DataSet resultDataSet) 
        {

            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();
            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetAllSchemes);

        }
        #endregion

        #region Get Blocks by scheme
        /// <summary>
        /// Get Blocks by scheme
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <returns></returns>
        public void getBlocksBySchemeId(int schemeId, ref DataSet resultDataSet)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO schemeIdParam = new ParameterBO("SchemeId", schemeId, DbType.Int32);
            paramList.Add(schemeIdParam);

            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetBlockListByScheme);
        }
        #endregion

        #region Get Properties by scheme
        /// <summary>
        /// Get Blocks by scheme
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <returns></returns>
        public void getPropertiesBySchemeId(int schemeId, ref DataSet resultDataSet)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();
            ParameterBO schemeIdParam = new ParameterBO("SchemeId", schemeId, DbType.Int32);
            ParameterBO blockIdParam = new ParameterBO("BlockId", 0, DbType.Int32);
            paramList.Add(schemeIdParam);
            paramList.Add(blockIdParam);
            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetPropertiesByScheme);
        }
        #endregion
        #region Get Properties by block
        /// <summary>
        /// Get Properties by block
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <returns></returns>
        public void getPropertiesByBlockId(int blockId, ref DataSet resultDataSet)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();
            ParameterBO schemeIdParam = new ParameterBO("SchemeId", -1, DbType.Int32);
            ParameterBO blockIdParam = new ParameterBO("BlockId", blockId, DbType.Int32);
            paramList.Add(schemeIdParam);
            paramList.Add(blockIdParam);
            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetPropertiesByScheme);
        }
        #endregion

        #region Get Maintenance Statuses
        /// <summary>
        /// Get Maintenance Statuses
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <returns></returns>
        public void getMaintenanceStatuses(ref DataSet resultDataSet)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();
            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetMaintenanceStatuses);
        }
        #endregion

        #region Get No Entry Data
        /// <summary>
        /// Get No Entry Data
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="schemeId"></param>
        /// <param name="blockId"></param>
        /// <param name="maintenanceType"></param>
        /// <returns></returns>
        public int getNoEntryData(ref DataSet resultDataSet, PageSortBO objPageSortBO, int schemeId, int blockId, int maintenanceType, bool getOnlyCount)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO searchTextParam = new ParameterBO("schemeId", schemeId, DbType.Int32);
            paramList.Add(searchTextParam);

            ParameterBO blockIdParam = new ParameterBO("blockId", blockId, DbType.Int32);
            paramList.Add(blockIdParam);

            ParameterBO maintenanceTypeParam = new ParameterBO("maintenanceType", maintenanceType, DbType.Int32);
            paramList.Add(maintenanceTypeParam);

            ParameterBO pageNumberParam = new ParameterBO("PageNumber", objPageSortBO.PageNumber, DbType.Int32);
            paramList.Add(pageNumberParam);

            ParameterBO pageSizeParam = new ParameterBO("PageSize", objPageSortBO.PageSize, DbType.Int32);
            paramList.Add(pageSizeParam);

            ParameterBO SortDirectionParam = new ParameterBO("sortOrder", objPageSortBO.SmallSortDirection, DbType.String);
            paramList.Add(SortDirectionParam);

            ParameterBO SortExpressionParam = new ParameterBO("sortColumn", objPageSortBO.SortExpression, DbType.String);
            paramList.Add(SortExpressionParam);

            ParameterBO getCountParam = new ParameterBO("getOnlyCount", getOnlyCount, DbType.Boolean);
            paramList.Add(getCountParam);

            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);


            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetMaintenanceNoEntryCount);

            ParameterBO obj = outParametersList.First();
            Int32 outvalue = Convert.ToInt32(obj.Value.ToString());

            return outvalue;
        }
        #endregion

        #region Get Overdue Data
        /// <summary>
        /// Get Overdue Data
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="schemeId"></param>
        /// <param name="blockId"></param>
        /// <param name="maintenanceType"></param>
        /// <returns></returns>
        public int getOverdueData(ref DataSet resultDataSet, PageSortBO objPageSortBO, int schemeId, int blockId, int maintenanceType, bool getOnlyCount)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO searchTextParam = new ParameterBO("schemeId", schemeId, DbType.Int32);
            paramList.Add(searchTextParam);

            ParameterBO blockIdParam = new ParameterBO("blockId", blockId, DbType.Int32);
            paramList.Add(blockIdParam);

            ParameterBO maintenanceTypeParam = new ParameterBO("maintenanceType", maintenanceType, DbType.Int32);
            paramList.Add(maintenanceTypeParam);

            ParameterBO pageNumberParam = new ParameterBO("PageNumber", objPageSortBO.PageNumber, DbType.Int32);
            paramList.Add(pageNumberParam);

            ParameterBO pageSizeParam = new ParameterBO("PageSize", objPageSortBO.PageSize, DbType.Int32);
            paramList.Add(pageSizeParam);

            ParameterBO SortDirectionParam = new ParameterBO("sortOrder", objPageSortBO.SmallSortDirection, DbType.String);
            paramList.Add(SortDirectionParam);

            ParameterBO SortExpressionParam = new ParameterBO("sortColumn", objPageSortBO.SortExpression, DbType.String);
            paramList.Add(SortExpressionParam);

            ParameterBO getCountParam = new ParameterBO("getOnlyCount", getOnlyCount, DbType.Boolean);
            paramList.Add(getCountParam);

            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);


            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetMaintenanceOverdueCount);

            ParameterBO obj = outParametersList.First();
            Int32 outvalue = Convert.ToInt32(obj.Value.ToString());

            return outvalue;
        }
        #endregion

        #region Get Appointment Arranged Data
        /// <summary>
        /// Get Appointment Arranged Data
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="schemeId"></param>
        /// <param name="blockId"></param>
        /// <param name="maintenanceType"></param>
        /// <returns></returns>
        public int getAppointmentsArrangedData(ref DataSet resultDataSet, PageSortBO objPageSortBO, int schemeId, int blockId, int maintenanceType, bool getOnlyCount)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO searchTextParam = new ParameterBO("schemeId", schemeId, DbType.Int32);
            paramList.Add(searchTextParam);

            ParameterBO blockIdParam = new ParameterBO("blockId", blockId, DbType.Int32);
            paramList.Add(blockIdParam);

            ParameterBO maintenanceTypeParam = new ParameterBO("maintenanceType", maintenanceType, DbType.Int32);
            paramList.Add(maintenanceTypeParam);

            ParameterBO pageNumberParam = new ParameterBO("PageNumber", objPageSortBO.PageNumber, DbType.Int32);
            paramList.Add(pageNumberParam);

            ParameterBO pageSizeParam = new ParameterBO("PageSize", objPageSortBO.PageSize, DbType.Int32);
            paramList.Add(pageSizeParam);

            ParameterBO SortDirectionParam = new ParameterBO("sortOrder", objPageSortBO.SmallSortDirection, DbType.String);
            paramList.Add(SortDirectionParam);

            ParameterBO SortExpressionParam = new ParameterBO("sortColumn", objPageSortBO.SortExpression, DbType.String);
            paramList.Add(SortExpressionParam);

            ParameterBO getCountParam = new ParameterBO("getOnlyCount", getOnlyCount, DbType.Boolean);
            paramList.Add(getCountParam);

            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);


            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetMaintenanceArrangedAppointmentsCount);

            ParameterBO obj = outParametersList.First();
            Int32 outvalue = Convert.ToInt32(obj.Value.ToString());

            return outvalue;
        }
        #endregion

        #region Get Appointment To Be Arranged Data
        /// <summary>
        /// Get Appointment To Be Arranged Count
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="schemeId"></param>
        /// <param name="blockId"></param>
        /// <param name="maintenanceType"></param>
        /// <returns></returns>
        public int getAppointmentsToBeArrangedData(ref DataSet resultDataSet, PageSortBO objPageSortBO, int schemeId, int blockId, int maintenanceType, bool getOnlyCount)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO searchTextParam = new ParameterBO("schemeId", schemeId, DbType.Int32);
            paramList.Add(searchTextParam);

            ParameterBO blockIdParam = new ParameterBO("blockId", blockId, DbType.Int32);
            paramList.Add(blockIdParam);

            ParameterBO maintenanceTypeParam = new ParameterBO("maintenanceType", maintenanceType, DbType.Int32);
            paramList.Add(maintenanceTypeParam);

            ParameterBO pageNumberParam = new ParameterBO("PageNumber", objPageSortBO.PageNumber, DbType.Int32);
            paramList.Add(pageNumberParam);

            ParameterBO pageSizeParam = new ParameterBO("PageSize", objPageSortBO.PageSize, DbType.Int32);
            paramList.Add(pageSizeParam);

            ParameterBO SortDirectionParam = new ParameterBO("sortOrder", objPageSortBO.SmallSortDirection, DbType.String);
            paramList.Add(SortDirectionParam);

            ParameterBO SortExpressionParam = new ParameterBO("sortColumn", objPageSortBO.SortExpression, DbType.String);
            paramList.Add(SortExpressionParam);

            ParameterBO getCountParam = new ParameterBO("getOnlyCount", getOnlyCount, DbType.Boolean);
            paramList.Add(getCountParam);

            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);


            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetMaintenanceAppointmentToBeArrangedCount);

            ParameterBO obj = outParametersList.First();
            Int32 outvalue = Convert.ToInt32(obj.Value.ToString());

            return outvalue;
        }
        #endregion

    }
}
