﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using PDR_BusinessObject.PageSort;

namespace PDR_DataAccess.Maintenance
{
   public interface IMaintenanceRepo
    {

        #region Get Maintenance Types
        /// <summary>
        /// Get Maintenance Types
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <returns></returns>
        void getMaintenanceTypes(ref DataSet resultDataSet);
        #endregion

        #region Get All Schemes
        /// <summary>
        /// Get All Schemes
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <returns></returns>
        void getAllSchemes(ref DataSet resultDataSet);
        #endregion

        #region Get Blocks by scheme
        /// <summary>
        /// Get Blocks by scheme
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <returns></returns>
        void getBlocksBySchemeId(int schemeId,ref DataSet resultDataSet);
        #endregion
        #region Get Properties by scheme
        /// <summary>
        /// Get Blocks by scheme
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <returns></returns>
        void getPropertiesBySchemeId(int schemeId, ref DataSet resultDataSet);
        #endregion
        #region Get Properties by block
        /// <summary>
        /// Get Blocks by scheme
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <returns></returns>
        void getPropertiesByBlockId(int blockId, ref DataSet resultDataSet);
        #endregion
        #region Get Maintenance Statuses
        /// <summary>
        /// Get Maintenance Statuses
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <returns></returns>
        void getMaintenanceStatuses(ref DataSet resultDataSet);
        #endregion

        #region Get No Entry Data
        /// <summary>
        /// Get No Entry Data
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="schemeId"></param>
        /// <param name="blockId"></param>
        /// <param name="maintenanceType"></param>
        /// <returns></returns>
        int getNoEntryData(ref DataSet resultDataSet, PageSortBO objPageSortBO, int schemeId, int blockId, int maintenanceType, bool getOnlyCount);
        #endregion

        #region Get Overdue Data
        /// <summary>
        /// Get Overdue Data
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="schemeId"></param>
        /// <param name="blockId"></param>
        /// <param name="maintenanceType"></param>
        /// <returns></returns>
        int getOverdueData(ref DataSet resultDataSet, PageSortBO objPageSortBO, int schemeId, int blockId, int maintenanceType, bool getOnlyCount);
        #endregion

        #region Get Appointment Arranged Data
        /// <summary>
        /// Get Appointment Arranged Data
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="schemeId"></param>
        /// <param name="blockId"></param>
        /// <param name="maintenanceType"></param>
        /// <returns></returns>
        int getAppointmentsArrangedData(ref DataSet resultDataSet, PageSortBO objPageSortBO, int schemeId, int blockId, int maintenanceType, bool getOnlyCount);
        #endregion

        #region Get Appointment To Be Arranged Data
        /// <summary>
        /// Get Appointment To Be Arranged Data
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="schemeId"></param>
        /// <param name="blockId"></param>
        /// <param name="maintenanceType"></param>
        /// <returns></returns>
        int getAppointmentsToBeArrangedData(ref DataSet resultDataSet, PageSortBO objPageSortBO, int schemeId, int blockId, int maintenanceType, bool getOnlyCount);
        #endregion

    }
}
