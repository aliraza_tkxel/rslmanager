﻿// -----------------------------------------------------------------------
// <copyright file="IAssignToContractor.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace PDR_DataAccess.AssignToContractor
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using PDR_BusinessObject.DropDown;
    using PDR_BusinessObject.Expenditure;
    using PDR_BusinessObject.AssignToContractor;
    using PDR_DataAccess.AssignToContractor;
    using PDR_BusinessObject.Vat;
    using System.Data;
    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public interface IAssignToContractor
    {

        #region "Get Cost Centre Drop Down Values."

        void GetCostCentreDropDownVales(ref List<DropDownBO> dropDownList);
        #endregion

        #region "Get Budget Head Drop Down Values by Cost Centre Id"

        void GetBudgetHeadDropDownValuesByCostCentreId(ref List<DropDownBO> dropDownList, int CostCentreId);
        #endregion

        #region "Get Expenditure Drop Down Values by Budget Head Id and EmployeeId"

        void GetExpenditureDropDownValuesByBudgetHeadId(ref List<ExpenditureBO> expenditureBOList, ref int BudgetHeadId, ref int EmployeeId);
        #endregion

        #region "Get Contractor Having ME Servicing Contract"

        void GetMeContractors(ref List<DropDownBO> dropDownList, AssignToContractorBo objAssignToContractorBo);
        #endregion

        #region "Get Contact DropDown Values By ContractorId"
        void GetContactDropDownValuesbyContractorId(ref List<DropDownBO> dropDownBoList, ref DataSet resultDataset, AssignToContractorBo objAssignToContractorBo);
        void GetContactDropDownValuesbyContractorId(ref List<DropDownBO> dropDownBoList, int contractorId);
        #endregion

        #region "Get contact email details"
        /// <summary>
        /// Get contact email details
        /// </summary>
        /// <param name="contactId"></param>
        /// <param name="detailsForEmailDS"></param>
        /// <remarks></remarks>

        void getContactEmailDetail(ref int contactId, ref DataSet detailsForEmailDS);

        #endregion

        //=======================================================
        //Service provided by Telerik (www.telerik.com)
        //Conversion powered by NRefactory.
        //Twitter: @telerik
        //Facebook: facebook.com/telerik
        //=======================================================

        void GetAreaDropDownValues(ref List<DropDownBO> dropDownList);
        #region Get SubItems By AreaId
        void GetAttributesSubItemsDropdownValuesByAreaId(ref List<DropDownBO> dropDownList, int areaId);
        #endregion

        #region "Get Sub Item Detail"
        DataSet GetItemMSATDetailByItemId(int itemId, int? schemeId, int? blockId);
        #endregion

        #region "Get Vat Drop down Values - Vat Rate as Value Field and Vat Name as text field."

        void GetVatDropDownValues(ref List<VatBo> vatBoList);

        void GetVatDropDownValues(ref List<DropDownBO> dropDownList);
        #endregion

        #region "Assign Work to Contractor - Save Process"
        bool assignToContractor(AssignToContractorBo assignToContractorBo);
        bool assignToContractorForSchemePO(AssignToContractorBo assignToContractorBo);
        #endregion

        #region "Get Details for email"

        /// <summary>
        /// This function is to get details for email, these details include contractor name, email and other details.
        /// Contact details of customer/tenant.
        /// Risk/Vulnerability details of the tenant
        /// Property Address
        /// </summary>
        /// <param name="detailsForEmailDS">Dataset to get details returned from Database.</param>
        /// <param name="assignToContractorBo">Assign to Contractor Bo containing values used to get details</param>
        /// <remarks></remarks>
        void getdetailsForEmail(ref AssignToContractorBo assignToContractorBo, ref DataSet detailsForEmailDS);
        #endregion

         #region "Get Details for void email-Scheme/Block Case"

        /// <summary>
        /// This function is to get details for email, these details include contractor name, email and other details.
        /// Contact details of customer/tenant.
        /// </summary>
        /// <param name="detailsForEmailDS">Dataset to get details returned from Database.</param>
        /// <param name="assignToContractorBo">Assign to Contractor Bo containing values used to get details</param>
        /// <remarks></remarks>
        void getSB_DetailsForEmail(ref AssignToContractorBo assignToContractorBo, ref DataSet detailsForEmailDS);
        #endregion

        #region "Get Details for Provision email-Scheme/Block Case"

        /// <summary>
        /// This function is to get details for email, these details include contractor name, email and other details.
        /// Contact details of customer/tenant.
        /// </summary>
        /// <param name="detailsForEmailDS">Dataset to get details returned from Database.</param>
        /// <param name="assignToContractorBo">Assign to Contractor Bo containing values used to get details</param>
        /// <remarks></remarks>
        void getSB_DetailsForProvisionEmail(ref AssignToContractorBo assignToContractorBo, ref DataSet detailsForEmailDS);
        #endregion

        

        #region "Get Assign To Contractor Detail By JournalId"
        /// <summary>
        /// This function 'll get Assign To Contractor Detail By JournalId
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="tradeIds"></param>
        /// <param name="propertyId"></param>
        /// <param name="startDate"></param>
        /// <remarks></remarks>
        void getAssignToContractorDetailByJournalId(ref DataSet resultDataSet, int journalId);
        #endregion


        #region "Get Contractor Having Reactive Repair Contract"

        void getAllContractors(ref List<DropDownBO> dropDownList);
        void getReactiveRepairContractors(ref List<DropDownBO> dropDownList);

        #endregion

        #region "Required Works Assign Work to Contractor - Save Process"

        bool voidWorkAssignToContractor(ref AssignToContractorBo assignToContractorBo);
        #endregion
        void GetContactDropDownValuesAndDetailsbyContractorId(ref List<DropDownBO> dropDownBoList, ref DataSet resultDataset, AssignToContractorBo objAssignToContractorBo);
        
        #region Paint Packs Assign Work to Contractor - Save Process

        bool voidPaintPackAssignToContractor(ref AssignToContractorBo assignToContractorBo);
        #endregion

        DataSet getBlockDetailByBlockId(int blockId);

        DataSet GetBlockNameBySchemeId(int schemeId, int itemId);

        DataSet GetSchemeByBlockId(int blockId);
        
    }
}
