﻿ // -----------------------------------------------------------------------
// <copyright file="AssignToContractorRepo.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace PDR_DataAccess.AssignToContractor
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using PDR_DataAccess.Base;
    using System.Data;
    using PDR_BusinessObject;
    using PDR_Utilities.Constants;
    using PDR_BusinessObject.PageSort;
    using PDR_BusinessObject.DropDown;
    using PDR_BusinessObject.Parameter;
    using PDR_BusinessObject.AssignToContractor;
    using PDR_BusinessObject.Vat;
    using PDR_BusinessObject.Expenditure;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class AssignToContractorRepo : BaseDAL, IAssignToContractor
    {

        #region "Functions"

        #region "Get Cost Centre Drop Down Values."

        public void GetCostCentreDropDownVales(ref List<DropDownBO> dropDownList)
        {
            getDropDownValuesBySpName(ref dropDownList, SpNameConstants.GetCostCentreDropDownValues);
        }

        #endregion

        #region "Get Budget Head Drop Down Values by Cost Centre Id"

        public void GetBudgetHeadDropDownValuesByCostCentreId(ref List<DropDownBO> dropDownList, int CostCentreId)
        {
            ParameterList inParamList = new ParameterList();

            ParameterBO costCentreIdparam = new ParameterBO("costCentreId", CostCentreId, DbType.Int32);
            inParamList.Add(costCentreIdparam);

            getDropDownValuesBySpName(ref dropDownList, SpNameConstants.GetBudgetHeadDropDownValuesByCostCentreId, inParamList);
        }

        #endregion

        #region "Get Expenditure Drop Down Values by Budget Head Id and EmployeeId"

        public void GetExpenditureDropDownValuesByBudgetHeadId(ref List<ExpenditureBO> expenditureBOList, ref int BudgetHeadId, ref int EmployeeId)
        {
            ParameterList inParamList = new ParameterList();

            ParameterBO budgetHeadIdparam = new ParameterBO("HeadId", BudgetHeadId, DbType.Int32);
            inParamList.Add(budgetHeadIdparam);

            ParameterBO employeeIdparam = new ParameterBO("userId", EmployeeId, DbType.Int32);
            inParamList.Add(employeeIdparam);

            string spName = SpNameConstants.GetExpenditureDropDownValuesByBudgetHeadId;

            IDataReader myDataReader = base.SelectRecord(inParamList, spName);

            //'Iterate the dataReader

            while ((myDataReader.Read()))
            {
                int id = 0;
                string description = string.Empty;
                decimal limit = 0.0M;
                decimal remaining = 0.0M;

                if (!myDataReader.IsDBNull(myDataReader.GetOrdinal("id")))
                {
                    id = myDataReader.GetInt32(myDataReader.GetOrdinal("id"));
                }

                if (!myDataReader.IsDBNull(myDataReader.GetOrdinal("description")))
                {
                    description = myDataReader.GetString(myDataReader.GetOrdinal("description"));
                }

                if (!myDataReader.IsDBNull(myDataReader.GetOrdinal("LIMIT")))
                {
                    limit = myDataReader.GetDecimal(myDataReader.GetOrdinal("LIMIT"));
                }

                if (!myDataReader.IsDBNull(myDataReader.GetOrdinal("REMAINING")))
                {
                    remaining = myDataReader.GetDecimal(myDataReader.GetOrdinal("REMAINING"));
                }

                ExpenditureBO objExpenditureBo = new ExpenditureBO(id, description, limit, remaining);
                expenditureBOList.Add(objExpenditureBo);
            }
            //getDropDownValuesBySpName(dropDownList, SpNameConstants.GetExpenditureDropDownValuesByBudgetHeadId, inParamList)
            myDataReader.Close();
        }

        #endregion

        #region "Get Contractor Having ME Servicing Contract"

        public void GetMeContractors(ref List<DropDownBO> dropDownList, AssignToContractorBo objAssignToContractorBo)
        {

            ParameterList inParamList = new ParameterList();

            ParameterBO schemeIdParam = new ParameterBO("schemeId", objAssignToContractorBo.SchemeId, DbType.Int32);
            inParamList.Add(schemeIdParam);

            ParameterBO blockIdParam = new ParameterBO("blockId", objAssignToContractorBo.BlockId, DbType.Int32);
            inParamList.Add(blockIdParam);

            ParameterBO msatTypeParam = new ParameterBO("msat", objAssignToContractorBo.MsatType, DbType.String);
            inParamList.Add(msatTypeParam);

            getDropDownValuesBySpName(ref dropDownList, SpNameConstants.GetMeContractorDropDownValues, inParamList);
        }

        #endregion

        #region "Get Contact DropDown Values By ContractorId"

        public void GetContactDropDownValuesbyContractorId(ref List<DropDownBO> dropDownBoList, ref DataSet resultDataset, AssignToContractorBo objAssignToContractorBo)
        {
            ParameterList inParamList = new ParameterList();

            ParameterBO propertyIdparam = new ParameterBO("contractorId", objAssignToContractorBo.ContractorId, DbType.Int32);
            inParamList.Add(propertyIdparam);

            DataTable contractorDetailDt = new DataTable();
            contractorDetailDt.TableName = ApplicationConstants.ContractorDetailDt;
            resultDataset.Tables.Add(contractorDetailDt);

            getDropDownValuesBySpName(ref dropDownBoList, SpNameConstants.GetContactDropDownValuesbyContractorId, inParamList);

            ParameterBO areaofworkparam = new ParameterBO("areaofwork", objAssignToContractorBo.MsatType, DbType.String);
            inParamList.Add(areaofworkparam);

            ParameterBO schemeIdparam = new ParameterBO("schemeId", objAssignToContractorBo.SchemeId, DbType.Int32);
            inParamList.Add(schemeIdparam);

            ParameterBO blockIdparam = new ParameterBO("blockId", objAssignToContractorBo.BlockId, DbType.Int32);
            inParamList.Add(blockIdparam);

            IDataReader iResultDataReader = base.SelectRecord(inParamList, SpNameConstants.GetContractDetailByContractorId);
            resultDataset.Load(iResultDataReader, LoadOption.OverwriteChanges, contractorDetailDt);

            iResultDataReader.Close();
        }

        #endregion


        #region "Get contact email details"
        /// <summary>
        /// Get contact email details
        /// </summary>
        /// <param name="contactId"></param>
        /// <param name="detailsForEmailDS"></param>
        /// <remarks></remarks>

        public void getContactEmailDetail(ref int contactId, ref DataSet detailsForEmailDS)
        {
            ParameterList inParamList = new ParameterList();

            ParameterBO contactIdParam = new ParameterBO("employeeId", contactId, DbType.Int32);
            inParamList.Add(contactIdParam);

            dynamic datareader = base.SelectRecord(inParamList, SpNameConstants.GetContactEmailDetail);

            detailsForEmailDS.Load(datareader, LoadOption.OverwriteChanges, ApplicationConstants.ContactEmailDetailsDt);
        }

        #endregion

        //=======================================================
        //Service provided by Telerik (www.telerik.com)
        //Conversion powered by NRefactory.
        //Twitter: @telerik
        //Facebook: facebook.com/telerik
        //=======================================================


        #region "Get Vat Drop down Values - Vat Rate as Value Field and Vat Name as text field."


        public void GetVatDropDownValues(ref List<VatBo> vatBoList)
        {
            IDataReader myDataReader = base.SelectRecord(null, SpNameConstants.GetVatDropDownValues);

            if (vatBoList == null)
            {
                vatBoList = new List<VatBo>();
            }

            //'Iterate the dataReader

            while (myDataReader.Read())
            {
                int id = 0;
                string description = string.Empty;
                decimal vatRate = default(decimal);

                if (!myDataReader.IsDBNull(myDataReader.GetOrdinal("id")))
                {
                    id = myDataReader.GetInt32(myDataReader.GetOrdinal("id"));
                }

                if (!myDataReader.IsDBNull(myDataReader.GetOrdinal("description")))
                {
                    description = myDataReader.GetString(myDataReader.GetOrdinal("description"));
                }

                if (!myDataReader.IsDBNull(myDataReader.GetOrdinal("vatRate")))
                {
                    vatRate = myDataReader.GetDecimal(myDataReader.GetOrdinal("vatRate"));
                }

                VatBo objVatBo = new VatBo(id, description, vatRate);
                vatBoList.Add(objVatBo);
            }
            myDataReader.Close();
        }

        public void GetVatDropDownValues(ref List<DropDownBO> dropDownList)
        {
            getDropDownValuesBySpName(ref dropDownList, SpNameConstants.GetVatDropDownValues);
        }

        #endregion

        #region Get Areas for Assign To Contractor
        public void GetAreaDropDownValues(ref List<DropDownBO> dropDownList)
        {
            getDropDownValuesBySpName(ref dropDownList, SpNameConstants.GetAreaDropDownValues);
        }

        #endregion

        #region Get SubItems By AreaId

        public void GetAttributesSubItemsDropdownValuesByAreaId(ref List<DropDownBO> dropDownList, int areaId)
        {
            ParameterList inParamList = new ParameterList();
            ParameterBO areaIdparam = new ParameterBO("areaId", areaId, DbType.Int32);
            inParamList.Add(areaIdparam);
            getDropDownValuesBySpName(ref dropDownList, SpNameConstants.GetAttributesSubItemsDropDownValuesByAreaId, inParamList);
        }

        #endregion


        #region "get Sub Item Detail"
        public DataSet GetItemMSATDetailByItemId(int itemId, int? schemeId, int? blockId)
        {
            ParameterList parameterList = new ParameterList();
            DataSet resultDataSet = new DataSet();
            ParameterBO itemIdParam = new ParameterBO("itemId", itemId, DbType.Int32);
            parameterList.Add(itemIdParam);

            ParameterBO schemeIdParam = new ParameterBO("SchemeId", schemeId, DbType.Int32);
            parameterList.Add(schemeIdParam);

            ParameterBO blockIdParam = new ParameterBO("blockId", blockId, DbType.Int32);
            parameterList.Add(blockIdParam);

            DataTable dtMST = new DataTable();
            dtMST.TableName = ApplicationConstants.MST;
            resultDataSet.Tables.Add(dtMST);

            IDataReader iResultDataReader = base.SelectRecord(parameterList, SpNameConstants.GetItemMSATDetailByItemId);
            resultDataSet.Load(iResultDataReader, LoadOption.OverwriteChanges, dtMST);
            iResultDataReader.Close();
            return resultDataSet;
        }
        #endregion

        #region "Assign Work to Contractor - Save Process"

        public bool assignToContractor(AssignToContractorBo assignToContractorBo)
        {

            ParameterList inParamList = new ParameterList();
            ParameterList outParamList = new ParameterList();

            ParameterBO pdrContractorIdParam = new ParameterBO("PdrContractorId", assignToContractorBo.PdrContractorId, DbType.Int32);
            inParamList.Add(pdrContractorIdParam);

            ParameterBO journalIdParam = new ParameterBO("JournalId", assignToContractorBo.JournalId, DbType.Int32);
            inParamList.Add(journalIdParam);

            ParameterBO contractroIdParam = new ParameterBO("ContractorId", assignToContractorBo.ContractorId, DbType.Int32);
            inParamList.Add(contractroIdParam);

            ParameterBO contactIdParam = new ParameterBO("ContactId", assignToContractorBo.ContactId, DbType.Int32);
            inParamList.Add(contactIdParam);

            ParameterBO userIdParam = new ParameterBO("userId", assignToContractorBo.UserId, DbType.Int32);
            inParamList.Add(userIdParam);

            ParameterBO estimateParam = new ParameterBO("Estimate", assignToContractorBo.Estimate, DbType.Decimal);
            inParamList.Add(estimateParam);

            ParameterBO estimateRefParam = new ParameterBO("EstimateRef", assignToContractorBo.EstimateRef, DbType.String);
            inParamList.Add(estimateRefParam);

            ParameterBO contractStartDateParam = new ParameterBO("ContractStartDate", assignToContractorBo.ContractStartDate, DbType.String);
            inParamList.Add(contractStartDateParam);

            ParameterBO contractEndDateParam = new ParameterBO("ContractEndDate", assignToContractorBo.ContractEndDate, DbType.String);
            inParamList.Add(contractEndDateParam);

            ParameterBO msatTypeParam = new ParameterBO("MsatType", assignToContractorBo.MsatType, DbType.String);
            inParamList.Add(msatTypeParam);

            ParameterBO poStatusParam = new ParameterBO("POStatus", assignToContractorBo.POStatus, DbType.Int32);
            inParamList.Add(poStatusParam);

            ParameterBO contractorWorkDetalParam = new ParameterBO("ContractorWorksDetail", assignToContractorBo.ServiceRequiredDt, SqlDbType.Structured);
            inParamList.Add(contractorWorkDetalParam);

            ParameterBO isSavedParam = new ParameterBO("isSaved", 0, DbType.Boolean);
            outParamList.Add(isSavedParam);

            ParameterBO journalIdOutParam = new ParameterBO("journalIdOut", -1, DbType.Int32);
            outParamList.Add(journalIdOutParam);

            outParamList = base.SelectRecord(ref inParamList, ref outParamList, SpNameConstants.AssignWorkToContractor);
            assignToContractorBo.JournalId = Convert.ToInt32(outParamList[1].Value);

            bool isSaved = false;
            isSaved = Convert.ToBoolean(outParamList[0].Value);
            return isSaved;
        }

        #endregion

        #region Assign Work to Contractor - Save Process for Scheme Purchase Order
        public bool assignToContractorForSchemePO(AssignToContractorBo assignToContractorBo)
        {

            ParameterList inParamList = new ParameterList();
            ParameterList outParamList = new ParameterList();

            ParameterBO pdrSchemeIdParam = new ParameterBO("schemeId", assignToContractorBo.SchemeId, DbType.Int32);
            inParamList.Add(pdrSchemeIdParam);

            ParameterBO pdrBlockIdParam = new ParameterBO("blockId", assignToContractorBo.BlockId, DbType.Int32);
            inParamList.Add(pdrBlockIdParam);

            ParameterBO pdrIncServiceChargeParam = new ParameterBO("incServiceCharge", assignToContractorBo.IncInSChge, DbType.Boolean);
            inParamList.Add(pdrIncServiceChargeParam);

            ParameterBO pdrPropApporParam = new ParameterBO("propertyApportionment", assignToContractorBo.PropertyApportionment, DbType.Decimal);
            inParamList.Add(pdrPropApporParam);

            ParameterBO pdrPropertyIdParam = new ParameterBO("schemeProperty", assignToContractorBo.PropertyId, DbType.String);
            inParamList.Add(pdrPropertyIdParam);

            ParameterBO pdrAttributeTypeIdParam = new ParameterBO("attributeTypeId", assignToContractorBo.AttributeTypeId, DbType.Int32);
            inParamList.Add(pdrAttributeTypeIdParam);

            ParameterBO pdrProvisionIdParam = new ParameterBO("provisionId", assignToContractorBo.ProvisionId, DbType.Int32);
            inParamList.Add(pdrProvisionIdParam);

            ParameterBO provisionCategoryParam = new ParameterBO("provisionCategory", assignToContractorBo.ProvisionCategory, DbType.String);
            inParamList.Add(provisionCategoryParam);
            ParameterBO pdrprovisionDescriptionParam = new ParameterBO("provisionDescription", assignToContractorBo.ProvisionDescription, DbType.String);
            inParamList.Add(pdrprovisionDescriptionParam);

            ParameterBO pdrItemIdParam = new ParameterBO("itemId", assignToContractorBo.ItemId, DbType.Int32);
            inParamList.Add(pdrItemIdParam);

            ParameterBO pdrContractorIdParam = new ParameterBO("pdrContractorId", assignToContractorBo.PdrContractorId, DbType.Int32);
            inParamList.Add(pdrContractorIdParam);

            ParameterBO contractroIdParam = new ParameterBO("ContractorId", assignToContractorBo.ContractorId, DbType.Int32);
            inParamList.Add(contractroIdParam);

            ParameterBO poNameParam = new ParameterBO("POName", assignToContractorBo.POName, DbType.String);
            inParamList.Add(poNameParam);

            ParameterBO contactIdParam = new ParameterBO("ContactId", assignToContractorBo.ContactId, DbType.Int32);
            inParamList.Add(contactIdParam);            

            ParameterBO userIdParam = new ParameterBO("userId", assignToContractorBo.UserId, DbType.Int32);
            inParamList.Add(userIdParam);

            ParameterBO estimateParam = new ParameterBO("Estimate", assignToContractorBo.Estimate, DbType.Decimal);
            inParamList.Add(estimateParam);

            ParameterBO estimateRefParam = new ParameterBO("EstimateRef", assignToContractorBo.EstimateRef, DbType.String);
            inParamList.Add(estimateRefParam);

            ParameterBO poStatusParam = new ParameterBO("POStatus", assignToContractorBo.POStatus, DbType.Int32);
            inParamList.Add(poStatusParam);

            ParameterBO contractStartDateParam = new ParameterBO("ContractStartDate", assignToContractorBo.ContractStartDate, DbType.String);
            inParamList.Add(contractStartDateParam);

            ParameterBO contractEndDateParam = new ParameterBO("ContractEndDate", assignToContractorBo.ContractEndDate, DbType.String);
            inParamList.Add(contractEndDateParam);

            ParameterBO contractorWorkDetalParam = new ParameterBO("ContractorWorksDetail", assignToContractorBo.ServiceRequiredDt, SqlDbType.Structured);
            inParamList.Add(contractorWorkDetalParam);

            ParameterBO msatTypeIdParam = new ParameterBO("MSATTypeId", assignToContractorBo.MSATTypeId, DbType.Int32);
            inParamList.Add(msatTypeIdParam);

            ParameterBO isSavedParam = new ParameterBO("isSaved", 0, DbType.Boolean);
            outParamList.Add(isSavedParam);

            ParameterBO journalIdOutParam = new ParameterBO("journalIdOut", -1, DbType.Int32);
            outParamList.Add(journalIdOutParam);

            //Changes for Purchas Order ID as output param
            ParameterBO orderIdOutParam = new ParameterBO("orderIdOut", -1, DbType.Int32);
            outParamList.Add(orderIdOutParam);

            ParameterBO expenditureOutParam = new ParameterBO("expenditureOut", -1, DbType.Int32);
            outParamList.Add(expenditureOutParam);

            outParamList = base.SelectRecord(ref inParamList, ref outParamList, SpNameConstants.AssignWorkToContractorForSchemePO);
            assignToContractorBo.JournalId = Convert.ToInt32(outParamList[1].Value);
            assignToContractorBo.OrderId = Convert.ToInt32(outParamList[2].Value);
            if (outParamList[3].Value != System.DBNull.Value)
                      assignToContractorBo.ExpenditureId = Convert.ToInt32(outParamList[3].Value);
            bool isSaved = false;
            isSaved = Convert.ToBoolean(outParamList[0].Value);
            return isSaved;
        }
        #endregion

        #region "Get Details for email"

        /// <summary>
        /// This function is to get details for email, these details include contractor name, email and other details.
        /// Contact details of customer/tenant.
        /// Risk/Vulnerability details of the tenant
        /// Property Address
        /// </summary>
        /// <param name="detailsForEmailDS">Dataset to get details returned from Database.</param>
        /// <param name="assignToContractorBo">Assign to Contractor Bo containing values used to get details</param>
        /// <remarks></remarks>
        public void getdetailsForEmail(ref AssignToContractorBo assignToContractorBo, ref DataSet detailsForEmailDS)
        {
            ParameterList inParamList = new ParameterList();

            ParameterBO contractorIdParam = new ParameterBO("journalId", assignToContractorBo.JournalId, DbType.Int32);
            inParamList.Add(contractorIdParam);

            ParameterBO inspectionJournalId = new ParameterBO("inspectionJournalId", assignToContractorBo.InspectionJournalId, DbType.Int32);
            inParamList.Add(inspectionJournalId);

            ParameterBO empolyeeIdParam = new ParameterBO("empolyeeId", assignToContractorBo.EmpolyeeId, DbType.Int32);
            inParamList.Add(empolyeeIdParam);

            dynamic datareader = base.SelectRecord(inParamList, SpNameConstants.GetDetailforEmailToContractor);

            detailsForEmailDS.Load(datareader, LoadOption.OverwriteChanges,
                    ApplicationConstants.ContractorDetailsDt,
                    ApplicationConstants.PropertyDetailsDt,
                    ApplicationConstants.TenantDetailsDt,
                    ApplicationConstants.TenantRiskDetailsDt,
                    ApplicationConstants.TenantVulnerabilityDetailsDt,
                    ApplicationConstants.PurchaseOrderDetailsDt,
                    ApplicationConstants.AppointmentSupplierDetailsDt
                    );
            datareader.Close();
        }

        #endregion

        
        #region "get Budget Holder By OrderId"

        public void getBudgetHolderByOrderId(ref DataSet resultDs, int orderId ,int expenditureId)
        {
            ParameterList parametersList = new ParameterList();

            ParameterBO orderIdParam = new ParameterBO("orderId", orderId, DbType.Int32);
            parametersList.Add(orderIdParam);

            ParameterBO expenditureIdParam = new ParameterBO("expenditureId", expenditureId, DbType.Int32);
            parametersList.Add(expenditureIdParam);

            dynamic datareader = base.SelectRecord(parametersList, SpNameConstants.GetBudgetHolderByOrderId);
            resultDs.Load(datareader, LoadOption.OverwriteChanges,
                    ApplicationConstants.ContractorDetailsDt);

        }

        #endregion

        #region "Get Details for void email-Scheme/Block Case"

        /// <summary>
        /// This function is to get details for email, these details include contractor name, email and other details.
        /// Contact details of customer/tenant.
        /// </summary>
        /// <param name="detailsForEmailDS">Dataset to get details returned from Database.</param>
        /// <param name="assignToContractorBo">Assign to Contractor Bo containing values used to get details</param>
        /// <remarks></remarks>
        public void getSB_DetailsForEmail(ref AssignToContractorBo assignToContractorBo, ref DataSet detailsForEmailDS)
        {
            ParameterList inParamList = new ParameterList();

            ParameterBO contractorIdParam = new ParameterBO("journalId", assignToContractorBo.JournalId, DbType.Int32);
            inParamList.Add(contractorIdParam);

            ParameterBO inspectionJournalId = new ParameterBO("inspectionJournalId", assignToContractorBo.InspectionJournalId, DbType.Int32);
            inParamList.Add(inspectionJournalId);

            ParameterBO empolyeeIdParam = new ParameterBO("empolyeeId", assignToContractorBo.EmpolyeeId, DbType.Int32);
            inParamList.Add(empolyeeIdParam);

            dynamic datareader = base.SelectRecord(inParamList, SpNameConstants.GetSBDetailforEmailToContractor);

            detailsForEmailDS.Load(datareader, LoadOption.OverwriteChanges,
                    ApplicationConstants.ContractorDetailsDt,
                    ApplicationConstants.PropertyDetailsDt,
                    ApplicationConstants.PurchaseOrderDetailsDt,
                    ApplicationConstants.AppointmentSupplierDetailsDt
                    );
            datareader.Close();
        }

        #endregion

        #region "Get Details for Provision email-Scheme/Block Case"

        /// <summary>
        /// This function is to get details for email, these details include contractor name, email and other details.
        /// Contact details of customer/tenant.
        /// </summary>
        /// <param name="detailsForEmailDS">Dataset to get details returned from Database.</param>
        /// <param name="assignToContractorBo">Assign to Contractor Bo containing values used to get details</param>
        /// <remarks></remarks>
        public void getSB_DetailsForProvisionEmail(ref AssignToContractorBo assignToContractorBo, ref DataSet detailsForEmailDS)
        {
            ParameterList inParamList = new ParameterList();

            ParameterBO contractorIdParam = new ParameterBO("journalId", assignToContractorBo.JournalId, DbType.Int32);
            inParamList.Add(contractorIdParam);

            ParameterBO inspectionJournalId = new ParameterBO("inspectionJournalId", assignToContractorBo.InspectionJournalId, DbType.Int32);
            inParamList.Add(inspectionJournalId);

            ParameterBO empolyeeIdParam = new ParameterBO("empolyeeId", assignToContractorBo.EmpolyeeId, DbType.Int32);
            inParamList.Add(empolyeeIdParam);

            dynamic datareader = base.SelectRecord(inParamList, SpNameConstants.GetSBDetailforProvisionEmailToContractor);

            detailsForEmailDS.Load(datareader, LoadOption.OverwriteChanges,
                    ApplicationConstants.ContractorDetailsDt,
                    ApplicationConstants.PropertyDetailsDt,
                    ApplicationConstants.PurchaseOrderDetailsDt,
                    ApplicationConstants.AppointmentSupplierDetailsDt
                    );
            datareader.Close();
        }

        #endregion

        #region "Supporting Functions"

        #region "Get Drop Down Values By Stored Procedure name and optional input Parameters(filters)"

        /// <summary>
        /// Requirements to Use This function: the stored procedure must return two columns
        /// 1- id of type integer
        /// 2- description of type string
        /// To use this function provide a stored procedure name that fulfill the above conditions and provide
        /// optional input parameters(filters).
        /// </summary>
        /// <param name="dropDownList">A list of DropDownBo(s) List</param>
        /// <param name="spName">Stored Procedure name as string</param>
        /// <param name="paramList">List of input parameter(s) as ParameterList business object.</param>
        /// <remarks></remarks>
        private void getDropDownValuesBySpName(ref List<DropDownBO> dropDownList, string spName, ParameterList paramList = null)
        {
            IDataReader myDataReader = base.SelectRecord(paramList, spName);

            if (dropDownList == null)
            {
                dropDownList = new List<DropDownBO>();
            }

            //'Iterate the dataReader

            while ((myDataReader.Read()))
            {
                int id = 0;
                string description = string.Empty;

                if (!myDataReader.IsDBNull(myDataReader.GetOrdinal("id")))
                {
                    id = myDataReader.GetInt32(myDataReader.GetOrdinal("id"));
                }

                if (!myDataReader.IsDBNull(myDataReader.GetOrdinal("description")))
                {
                    description = myDataReader.GetString(myDataReader.GetOrdinal("description"));
                }
                DropDownBO objDropDownBo = new DropDownBO(id, description);
                dropDownList.Add(objDropDownBo);
            }

            myDataReader.Close();
        }

        #endregion

        #endregion

        #region "Get Assign To Contractor Detail By JournalId"
        /// <summary>
        /// This function 'll get Assign To Contractor Detail By JournalId
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="tradeIds"></param>
        /// <param name="propertyId"></param>
        /// <param name="startDate"></param>
        /// <remarks></remarks>
        public void getAssignToContractorDetailByJournalId(ref DataSet resultDataSet, int journalId)
        {
            ParameterList parametersList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            DataTable contractorWorkDt = new DataTable();
            contractorWorkDt.TableName = ApplicationConstants.ContractorWorkDt;
            DataTable contractorWorkDetailDt = new DataTable();
            contractorWorkDetailDt.TableName = ApplicationConstants.ContractorWorkDetailDt;

            resultDataSet.Tables.Add(contractorWorkDt);
            resultDataSet.Tables.Add(contractorWorkDetailDt);

            ParameterBO journalIdParam = new ParameterBO("JournalId", journalId, DbType.Int32);
            parametersList.Add(journalIdParam);

            IDataReader iResultDataReader = base.SelectRecord(parametersList, SpNameConstants.GetAssignToContractorDetailByJournalId);
            resultDataSet.Load(iResultDataReader, LoadOption.OverwriteChanges, contractorWorkDt, contractorWorkDetailDt);
            iResultDataReader.Close();
        }
        #endregion

        #region "Get Contractor Having ME Servicing Contract"

        public void getReactiveRepairContractors(ref List<DropDownBO> dropDownList)
        {
            ParameterList inParamList = new ParameterList();
            getDropDownValuesBySpName(ref dropDownList, SpNameConstants.GetReactiveRepairContractors, inParamList);
        }

        public void getAllContractors(ref List<DropDownBO> dropDownList)
        {
            ParameterList inParamList = new ParameterList();
            getDropDownValuesBySpName(ref dropDownList, SpNameConstants.GetAllContractorsDropDownValues, inParamList);
        }

        #endregion

        #region Required Works Assign Work to Contractor - Save Process

        public bool voidWorkAssignToContractor(ref AssignToContractorBo assignToContractorBo)
        {

            ParameterList inParamList = new ParameterList();
            ParameterList outParamList = new ParameterList();

            ParameterBO pdrContractorIdParam = new ParameterBO("PdrContractorId", assignToContractorBo.PdrContractorId, DbType.Int32);
            inParamList.Add(pdrContractorIdParam);

            ParameterBO journalIdParam = new ParameterBO("inspectionJournalId", assignToContractorBo.JournalId, DbType.Int32);
            inParamList.Add(journalIdParam);

            ParameterBO contractroIdParam = new ParameterBO("ContractorId", assignToContractorBo.ContractorId, DbType.Int32);
            inParamList.Add(contractroIdParam);

            ParameterBO contactIdParam = new ParameterBO("ContactId", assignToContractorBo.ContactId, DbType.Int32);
            inParamList.Add(contactIdParam);

            ParameterBO userIdParam = new ParameterBO("userId", assignToContractorBo.UserId, DbType.Int32);
            inParamList.Add(userIdParam);

            ParameterBO estimateParam = new ParameterBO("Estimate", assignToContractorBo.Estimate, DbType.Decimal);
            inParamList.Add(estimateParam);

            ParameterBO estimateRefParam = new ParameterBO("EstimateRef", assignToContractorBo.EstimateRef, DbType.String);
            inParamList.Add(estimateRefParam);

            ParameterBO poStatusParam = new ParameterBO("POStatus", assignToContractorBo.POStatus, DbType.Int32);
            inParamList.Add(poStatusParam);

            ParameterBO requiredWorkIdsParam = new ParameterBO("requiredWorkIds", assignToContractorBo.RequiredWorkIds, DbType.String);
            inParamList.Add(requiredWorkIdsParam);

            ParameterBO contractorWorkDetalParam = new ParameterBO("ContractorWorksDetail", assignToContractorBo.ServiceRequiredDt, SqlDbType.Structured);
            inParamList.Add(contractorWorkDetalParam);

            ParameterBO isSavedParam = new ParameterBO("isSaved", 0, DbType.Boolean);
            outParamList.Add(isSavedParam);

            ParameterBO journalIdOutParam = new ParameterBO("journalIdOut", -1, DbType.Int32);
            outParamList.Add(journalIdOutParam);

            outParamList = base.SelectRecord(ref inParamList, ref outParamList, SpNameConstants.RequiredWorksAssignWorkToContractor);
            assignToContractorBo.JournalId = Convert.ToInt32(outParamList[1].Value);

            bool isSaved = false;
            isSaved = Convert.ToBoolean(outParamList[0].Value);
            return isSaved;
        }

        #endregion

        #region "Get Void Contact DropDown Values By ContractorId"

        public void GetContactDropDownValuesAndDetailsbyContractorId(ref List<DropDownBO> dropDownBoList, ref DataSet resultDataset, AssignToContractorBo objAssignToContractorBo)
        {
            ParameterList inParamList = new ParameterList();

            ParameterBO propertyIdparam = new ParameterBO("contractorId", objAssignToContractorBo.ContractorId, DbType.Int32);
            inParamList.Add(propertyIdparam);

            DataTable contractorDetailDt = new DataTable();
            contractorDetailDt.TableName = ApplicationConstants.ContractorDetailDt;
            resultDataset.Tables.Add(contractorDetailDt);

            getDropDownValuesBySpName(ref dropDownBoList, SpNameConstants.GetContactDropDownValuesbyContractorId, inParamList);

            ParameterBO schemeIdparam = new ParameterBO("schemeId", objAssignToContractorBo.SchemeId, DbType.Int32);
            inParamList.Add(schemeIdparam);

            ParameterBO blockIdparam = new ParameterBO("blockId", objAssignToContractorBo.BlockId, DbType.Int32);
            inParamList.Add(blockIdparam);

            IDataReader iResultDataReader = base.SelectRecord(inParamList, SpNameConstants.GetContractDetailByContractorIdForAssignToContractor);
            resultDataset.Load(iResultDataReader, LoadOption.OverwriteChanges, contractorDetailDt);

            iResultDataReader.Close();
        }

        public void GetContactDropDownValuesbyContractorId(ref List<DropDownBO> dropDownBoList, int contractorId)
        {
            ParameterList inParamList = new ParameterList();

            ParameterBO propertyIdparam = new ParameterBO("contractorId", contractorId, DbType.Int32);
            inParamList.Add(propertyIdparam);
            getDropDownValuesBySpName(ref dropDownBoList, SpNameConstants.GetContactDropDownValuesbyContractorId, inParamList);
        }

        #endregion

        #region Paint Packs Assign Work to Contractor - Save Process

        public bool voidPaintPackAssignToContractor(ref AssignToContractorBo assignToContractorBo)
        {

            ParameterList inParamList = new ParameterList();
            ParameterList outParamList = new ParameterList();

            ParameterBO pdrContractorIdParam = new ParameterBO("PdrContractorId", assignToContractorBo.PdrContractorId, DbType.Int32);
            inParamList.Add(pdrContractorIdParam);

            ParameterBO paintPackIdParam = new ParameterBO("paintPackId", assignToContractorBo.PaintPackId, DbType.Int32);
            inParamList.Add(paintPackIdParam);

            ParameterBO contractroIdParam = new ParameterBO("ContractorId", assignToContractorBo.ContractorId, DbType.Int32);
            inParamList.Add(contractroIdParam);

            ParameterBO contactIdParam = new ParameterBO("ContactId", assignToContractorBo.ContactId, DbType.Int32);
            inParamList.Add(contactIdParam);

            ParameterBO userIdParam = new ParameterBO("userId", assignToContractorBo.UserId, DbType.Int32);
            inParamList.Add(userIdParam);

            ParameterBO estimateParam = new ParameterBO("Estimate", assignToContractorBo.Estimate, DbType.Decimal);
            inParamList.Add(estimateParam);

            ParameterBO estimateRefParam = new ParameterBO("EstimateRef", assignToContractorBo.EstimateRef, DbType.String);
            inParamList.Add(estimateRefParam);

            ParameterBO poStatusParam = new ParameterBO("POStatus", assignToContractorBo.POStatus, DbType.Int32);
            inParamList.Add(poStatusParam);


            ParameterBO contractorWorkDetalParam = new ParameterBO("ContractorWorksDetail", assignToContractorBo.ServiceRequiredDt, SqlDbType.Structured);
            inParamList.Add(contractorWorkDetalParam);

            ParameterBO isSavedParam = new ParameterBO("isSaved", 0, DbType.Boolean);
            outParamList.Add(isSavedParam);

            ParameterBO journalIdOutParam = new ParameterBO("journalIdOut", -1, DbType.Int32);
            outParamList.Add(journalIdOutParam);

            outParamList = base.SelectRecord(ref inParamList, ref outParamList, SpNameConstants.PaintPackAssignWorkToContractor);
            assignToContractorBo.JournalId = Convert.ToInt32(outParamList[1].Value);

            bool isSaved = false;
            isSaved = Convert.ToBoolean(outParamList[0].Value);
            return isSaved;
        }

        #endregion

        #endregion

        public DataSet getBlockDetailByBlockId(int blockId)
        {
            ParameterList parameterList = new ParameterList();


            ParameterBO blockIdParam = new ParameterBO("blockId", blockId, DbType.Int32);
            parameterList.Add(blockIdParam);
            DataSet resultDataSet = new DataSet();
            LoadDataSet(ref resultDataSet, ref parameterList, SpNameConstants.GetBlocksByBlockID);
            return resultDataSet;
        }

        public DataSet GetBlockNameBySchemeId(int schemeId, int itemId)
        {
            ParameterList parameterList = new ParameterList();

            ParameterBO schemeIdParam = new ParameterBO("schemeId", schemeId, DbType.Int32);
            parameterList.Add(schemeIdParam);
            ParameterBO itemIdParam = new ParameterBO("itemId", itemId, DbType.Int32);
            parameterList.Add(itemIdParam);
            DataSet resultDataSet = new DataSet();
            LoadDataSet(ref resultDataSet, ref parameterList, SpNameConstants.GetBlockNameBySchemeId);
            return resultDataSet;
        }

        public DataSet GetSchemeByBlockId(int blockId)
        {
            ParameterList parameterList = new ParameterList();

            ParameterBO blockIdParam = new ParameterBO("blockId", blockId, DbType.Int32);
            parameterList.Add(blockIdParam);
            DataSet resultDataSet = new DataSet();
            LoadDataSet(ref resultDataSet, ref parameterList, SpNameConstants.GetSchemeByBlockId);
            return resultDataSet;
        }
    }
}
