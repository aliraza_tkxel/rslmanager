using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PDR_DataAccess.Base;
using System.Data;
using PDR_BusinessObject;
using PDR_Utilities.Constants;
using PDR_BusinessObject.PageSort;
using PDR_BusinessObject.MeAppointment;
using PDR_BusinessObject.Parameter;
using PDR_BusinessObject.MeSearch;
using PDR_BusinessObject.VoidAppointment;

namespace PDR_DataAccess.Scheduling
{
    public class SchedulingRepo : BaseDAL, ISchedulingRepo
    {

        #region Get Appointment To Be Arranged List
        /// <summary>
        /// Get Appointment To Be Arranged List
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <param name="check56Days"></param>
        /// <param name="msatType"></param>
        /// <returns></returns>
        public Int32 getAppointmentToBeArrangedList(ref DataSet resultDataSet, PageSortBO objPageSortBO, MeSearchBO objMeSearchBO)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO searchTextParam = new ParameterBO("searchedText", objMeSearchBO.SearchText, DbType.String);
            paramList.Add(searchTextParam);

            ParameterBO check56DaysParam = new ParameterBO("check56Days", objMeSearchBO.Check56Days, DbType.Boolean);
            paramList.Add(check56DaysParam);

            ParameterBO msatTypeParam = new ParameterBO("msatType", objMeSearchBO.MsatType, DbType.String);
            paramList.Add(msatTypeParam);

            ParameterBO schemeIdParam = new ParameterBO("schemeId", objMeSearchBO.SchemeId, DbType.Int32);
            paramList.Add(schemeIdParam);

            ParameterBO blockIdParam = new ParameterBO("blockId", objMeSearchBO.BlockId, DbType.Int32);
            paramList.Add(blockIdParam);

            ParameterBO pageNumberParam = new ParameterBO("PageNumber", objPageSortBO.PageNumber, DbType.Int32);
            paramList.Add(pageNumberParam);

            ParameterBO pageSizeParam = new ParameterBO("PageSize", objPageSortBO.PageSize, DbType.Int32);
            paramList.Add(pageSizeParam);

            ParameterBO attributeTypeIdParam = new ParameterBO("attributeTypeId", objMeSearchBO.AttributeTypeId, DbType.Int32);
            paramList.Add(attributeTypeIdParam);

            ParameterBO itemIdParam = new ParameterBO("itemName", objMeSearchBO.ItemName, DbType.String);
            paramList.Add(itemIdParam);
            
            ParameterBO SortDirectionParam = new ParameterBO("sortOrder", objPageSortBO.SmallSortDirection, DbType.String);
            paramList.Add(SortDirectionParam);

            ParameterBO SortExpressionParam = new ParameterBO("sortColumn", objPageSortBO.SortExpression, DbType.String);
            paramList.Add(SortExpressionParam);

            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);

            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetAppointmentsToBeArranged);

            ParameterBO obj = outParametersList.First();
            Int32 outvalue = Convert.ToInt32(obj.Value.ToString());

            return outvalue;

        }
        #endregion

        #region Get Appointment Arranged List
        /// <summary>
        /// Get Appointment Arranged List
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <param name="check56Days"></param>
        /// <param name="msatType"></param>
        /// <returns></returns>
        public Int32 getAppointmentArrangedList(ref DataSet resultDataSet, PageSortBO objPageSortBO, MeSearchBO objMeSearchBO)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO searchTextParam = new ParameterBO("searchedText", objMeSearchBO.SearchText, DbType.String);
            paramList.Add(searchTextParam);

            ParameterBO check56DaysParam = new ParameterBO("check56Days", objMeSearchBO.Check56Days, DbType.Boolean);
            paramList.Add(check56DaysParam);

            ParameterBO msatTypeParam = new ParameterBO("msatType", objMeSearchBO.MsatType, DbType.String);
            paramList.Add(msatTypeParam);

            ParameterBO schemeIdParam = new ParameterBO("schemeId", objMeSearchBO.SchemeId, DbType.Int32);
            paramList.Add(schemeIdParam);

            ParameterBO blockIdParam = new ParameterBO("blockId", objMeSearchBO.BlockId, DbType.Int32);
            paramList.Add(blockIdParam);

            ParameterBO pageNumberParam = new ParameterBO("PageNumber", objPageSortBO.PageNumber, DbType.Int32);
            paramList.Add(pageNumberParam);

            ParameterBO pageSizeParam = new ParameterBO("PageSize", objPageSortBO.PageSize, DbType.Int32);
            paramList.Add(pageSizeParam);

            ParameterBO attributeTypeIdParam = new ParameterBO("attributeTypeId", objMeSearchBO.AttributeTypeId, DbType.Int32);
            paramList.Add(attributeTypeIdParam);

            ParameterBO itemIdParam = new ParameterBO("itemName", objMeSearchBO.ItemName, DbType.String);
            paramList.Add(itemIdParam);
            
            ParameterBO SortDirectionParam = new ParameterBO("sortOrder", objPageSortBO.SmallSortDirection, DbType.String);
            paramList.Add(SortDirectionParam);

            ParameterBO SortExpressionParam = new ParameterBO("sortColumn", objPageSortBO.SortExpression, DbType.String);
            paramList.Add(SortExpressionParam);

            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);

            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetAppointmentsArranged);

            ParameterBO obj = outParametersList.First();
            Int32 outvalue = Convert.ToInt32(obj.Value.ToString());

            return outvalue;

        }
        #endregion

        #region "Get All trades"
        /// <summary>
        /// Get All trades
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <remarks></remarks>
        public void getAllTrades(ref DataSet resultDataSet)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();
            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetAllTrades);
        }
        #endregion

        #region "Get Available Operatives"
        /// <summary>
        /// This function 'll get the list of operatives that are available
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="tradeIds"></param>
        /// <param name="propertyId"></param>
        /// <param name="startDate"></param>
        /// <remarks></remarks>
        public void getAvailableOperatives(ref DataSet resultDataSet, string tradeIds, string msattype, System.DateTime startDate)
        {
            ParameterList parametersList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            DataTable employeeDt = new DataTable();
            employeeDt.TableName = ApplicationConstants.AvailableOperatives;
            DataTable leavesDt = new DataTable();
            leavesDt.TableName = ApplicationConstants.OperativesLeaves;
            DataTable appointmentsDt = new DataTable();
            appointmentsDt.TableName = ApplicationConstants.OperativesAppointments;

            resultDataSet.Tables.Add(employeeDt);
            resultDataSet.Tables.Add(leavesDt);
            resultDataSet.Tables.Add(appointmentsDt);

            ParameterBO tradeIdsParam = new ParameterBO("tradeIds", tradeIds, DbType.String);
            parametersList.Add(tradeIdsParam);

            ParameterBO msattypeParam = new ParameterBO("msattype", msattype, DbType.String);
            parametersList.Add(msattypeParam);

            ParameterBO startDateParam = new ParameterBO("startDate", startDate.Date, DbType.Date);
            parametersList.Add(startDateParam);

            IDataReader iResultDataReader = base.SelectRecord(parametersList, SpNameConstants.GetAvailableOperatives);
            resultDataSet.Load(iResultDataReader, LoadOption.OverwriteChanges, employeeDt, leavesDt, appointmentsDt);
            iResultDataReader.Close();
        }
        #endregion

        #region "Schedule Me Work appointment"
        /// <summary>
        /// Schedule Me Work appointment
        /// </summary>
        /// <param name="objMeAppointmentBO"></param>
        /// <param name="journalId"></param>
        /// <returns></returns>
        public string scheduleMeWorkAppointment(MeAppointmentBO objMeAppointmentBO, int journalId, ref int appointmentId)
        {

            ParameterList parametersList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO userIdParam = new ParameterBO("userId", objMeAppointmentBO.UserId, DbType.Int32);
            parametersList.Add(userIdParam);

            ParameterBO tradeIdParam = new ParameterBO("tradeId", objMeAppointmentBO.TradeId, DbType.Int32);
            parametersList.Add(tradeIdParam);

            ParameterBO journalNotesParam = new ParameterBO("journalNotes", objMeAppointmentBO.JournalNotes, DbType.String);
            parametersList.Add(journalNotesParam);

            ParameterBO customerNotesParam = new ParameterBO("customerNotes", objMeAppointmentBO.CustomerNotes, DbType.String);
            parametersList.Add(customerNotesParam);

            ParameterBO appointmentNotesParam = new ParameterBO("appointmentNotes", objMeAppointmentBO.AppointmentNotes, DbType.String);
            parametersList.Add(appointmentNotesParam);

            ParameterBO worksRequiredParam = new ParameterBO("worksRequired", objMeAppointmentBO.WorksRequired, DbType.String);
            parametersList.Add(worksRequiredParam);

            ParameterBO tenancyIdParam = new ParameterBO("tenancyId", objMeAppointmentBO.TenancyId, DbType.Int32);
            parametersList.Add(tenancyIdParam);

            ParameterBO appointmentStartDateParam = new ParameterBO("appointmentStartDate", objMeAppointmentBO.StartDate, DbType.Date);
            parametersList.Add(appointmentStartDateParam);

            ParameterBO appointmentEndDateParam = new ParameterBO("appointmentEndDate", objMeAppointmentBO.EndDate, DbType.Date);
            parametersList.Add(appointmentEndDateParam);

            ParameterBO startTimeParam = new ParameterBO("startTime", objMeAppointmentBO.StartTime, DbType.String);
            parametersList.Add(startTimeParam);

            ParameterBO endTimeParam = new ParameterBO("endTime", objMeAppointmentBO.EndTime, DbType.String);
            parametersList.Add(endTimeParam);

            ParameterBO operativeIdParam = new ParameterBO("operativeId", objMeAppointmentBO.OperativeId, DbType.Int32);
            parametersList.Add(operativeIdParam);

            ParameterBO journalIdParam = new ParameterBO("journalId", objMeAppointmentBO.JournalId, DbType.Int32);
            parametersList.Add(journalIdParam);

            ParameterBO durationParam = new ParameterBO("duration", objMeAppointmentBO.Duration, DbType.Int32);
            parametersList.Add(durationParam);

            ParameterBO isSavedParam = new ParameterBO("isSaved", 0, DbType.Int32);
            outParametersList.Add(isSavedParam);

            ParameterBO appointmentIdOutParam = new ParameterBO("appointmentIdOut", 0, DbType.Int32);
            outParametersList.Add(appointmentIdOutParam);

            outParametersList = base.SelectRecord(ref parametersList, ref outParametersList, SpNameConstants.ScheduleMeWorkAppointment);
            appointmentId = Convert.ToInt32(outParametersList[1].Value);

            return outParametersList[0].Value.ToString();

        }

        #endregion

        #region "Get Property Customer Detail"
        /// <summary>
        /// Get Property Customer Detail
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="propertyId"></param>
        /// <remarks></remarks>

        public void getPropertyDetailByJournalId(ref DataSet resultDataSet, int journalId,bool isVoid)
        {
            ParameterList parametersList = new ParameterList();
            DataTable dtPropertyResult = new DataTable();
            dtPropertyResult.TableName = ApplicationConstants.PropertyInfoByJournalId;
            resultDataSet.Tables.Add(dtPropertyResult);

            ParameterBO journalIdParam = new ParameterBO("journalId", journalId, DbType.Int32);
            parametersList.Add(journalIdParam);
            string spName =  SpNameConstants.GetPropertyInfoByJournalId;
            if (isVoid)
            {
                spName = SpNameConstants.VoidGetPropertyInfoByJournalId;
            }

            IDataReader iResultDataReader = base.SelectRecord(parametersList, spName);
            resultDataSet.Load(iResultDataReader, LoadOption.OverwriteChanges, dtPropertyResult);
            iResultDataReader.Close();
        }
        #endregion

        #region "Get Arranged Appointments Detail"
        /// <summary>
        /// Get Arranged Appointments Detail
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="pmo"></param>
        /// <remarks></remarks>

        public void getArrangedAppointmentsDetail(ref DataSet resultDataSet, int journalId)
        {
            ParameterList parametersList = new ParameterList();

            DataTable dtMeAppointmentDetail = new DataTable();
            dtMeAppointmentDetail.TableName = ApplicationConstants.MeAppointmentsDetail;
            resultDataSet.Tables.Add(dtMeAppointmentDetail);

            DataTable dtPropertyDetail = new DataTable();
            dtPropertyDetail.TableName = ApplicationConstants.PropertyDetail;
            resultDataSet.Tables.Add(dtPropertyDetail);

            ParameterBO journalIdParam = new ParameterBO("journalId", journalId, DbType.Int32);
            parametersList.Add(journalIdParam);

            IDataReader iResultDataReader = base.SelectRecord(parametersList, SpNameConstants.GetArrangedAppointmentsDetailByJournalId);
            resultDataSet.Load(iResultDataReader, LoadOption.OverwriteChanges, dtMeAppointmentDetail, dtPropertyDetail);
            iResultDataReader.Close();
        }

        #endregion

        #region "Update appointment notes"

        /// <summary>
        /// Update appointment notes
        /// </summary>
        /// <param name="customerNotes"></param>
        /// <param name="jobsheetNotes"></param>
        /// <param name="appointmentId"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public int updateMeAppointmentsNotes(string customerNotes, string jobsheetNotes, int appointmentId)
        {

            ParameterList parametersList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO customerNotesParam = new ParameterBO("customerNotes", customerNotes, DbType.String);
            parametersList.Add(customerNotesParam);

            ParameterBO jobsheetNotesParam = new ParameterBO("jobsheetNotes", jobsheetNotes, DbType.String);
            parametersList.Add(jobsheetNotesParam);

            ParameterBO appointmentIdParam = new ParameterBO("appointmentId", appointmentId, DbType.Int32);
            parametersList.Add(appointmentIdParam);

            ParameterBO isSavedParam = new ParameterBO("isSaved", 0, DbType.Int32);
            outParametersList.Add(isSavedParam);

            outParametersList = base.SelectRecord(ref parametersList, ref outParametersList, SpNameConstants.UpdateMeAppointmentNotes);
            return Convert.ToInt32(outParametersList[0].Value);

        }

        #endregion

        #region "Cancel appointment"
        /// <summary>
        /// Cancel appointment
        /// </summary>
        /// <param name="pmo"></param>
        /// <param name="reason"></param>
        /// <returns></returns>
        /// <remarks></remarks>

        public int cancelAppointment(int journalId, string reason)
        {

            ParameterList parametersList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO pmoParam = new ParameterBO("journalId", journalId, DbType.Int32);
            parametersList.Add(pmoParam);

            ParameterBO jobsheetNotesParam = new ParameterBO("reason", reason, DbType.String);
            parametersList.Add(jobsheetNotesParam);

            ParameterBO isCancelledParam = new ParameterBO("isCancelled", 0, DbType.Int32);
            outParametersList.Add(isCancelledParam);

            outParametersList = base.SelectRecord(ref parametersList, ref outParametersList, SpNameConstants.CancelArrangedAppointment);
            return Convert.ToInt32(outParametersList[0].Value);

        }

        #endregion

        #region "Get Operative Info"
        /// <summary>
        /// Get Operative Info
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="operativeId"></param>
        /// <remarks></remarks>

        public void getOperativeInformation(ref DataSet resultDataSet, int operativeId)
        {
            ParameterList parameterList = new ParameterList();

            ParameterBO operativeIdParam = new ParameterBO("operativeId", operativeId, DbType.Int32);
            parameterList.Add(operativeIdParam);

            DataTable dtOperativeDetail = new DataTable();
            dtOperativeDetail.TableName = ApplicationConstants.PropertyDetail;
            resultDataSet.Tables.Add(dtOperativeDetail);

            IDataReader iResultDataReader = base.SelectRecord(parameterList, SpNameConstants.GetOperativeInformation);
            resultDataSet.Load(iResultDataReader, LoadOption.OverwriteChanges, dtOperativeDetail);
            iResultDataReader.Close();
        }
        #endregion

        #region get All Void Operatives
        /// <summary>
        /// get All Void Operatives
        /// </summary>
        /// <returns></returns>
        public DataSet getAllVoidOperatives()
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();
            DataSet resultDataSet = new DataSet();
            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetAllVoidOperatives);
            return resultDataSet;

        }
        #endregion

        #region get Operatives Leaves Appointments
        public void getOperativesLeavesAppointments(ref DataSet resultDataSet, int OperativeId, DateTime startDate)
        {
            ParameterList parametersList = new ParameterList();
            ParameterList outParametersList = new ParameterList();
            DataTable employeeDt = new DataTable();
            employeeDt.TableName = ApplicationConstants.AvailableOperatives;
            DataTable leavesDt = new DataTable();
            leavesDt.TableName = ApplicationConstants.OperativesLeaves;
            DataTable appointmentsDt = new DataTable();
            appointmentsDt.TableName = ApplicationConstants.OperativesAppointments;
            resultDataSet.Tables.Add(employeeDt);
            resultDataSet.Tables.Add(leavesDt);
            resultDataSet.Tables.Add(appointmentsDt);

            ParameterBO tradeIdsParam = new ParameterBO("employeeId", OperativeId, DbType.Int32);
            parametersList.Add(tradeIdsParam);

            ParameterBO startDateParam = new ParameterBO("startDate", startDate.Date, DbType.Date);
            parametersList.Add(startDateParam);

            IDataReader iResultDataReader = base.SelectRecord(parametersList, SpNameConstants.GetOperativesLeavesAndAppointment);
            resultDataSet.Load(iResultDataReader, LoadOption.OverwriteChanges, employeeDt, leavesDt, appointmentsDt);
            iResultDataReader.Close();
        }
        #endregion

        #region Schedule Void Inspection
        /// <summary>
        /// Schedule Void Inspection
        /// </summary>
        /// <param name="objVoidAppointmentBO"></param>
        /// <returns></returns>
        public string scheduleVoidInspection(VoidAppointmentBO objVoidAppointmentBO, ref int appointmentId)
        {

            ParameterList parametersList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO userIdParam = new ParameterBO("userId", objVoidAppointmentBO.UserId, DbType.Int32);
            parametersList.Add(userIdParam);

            ParameterBO legionellaParam = new ParameterBO("Legionella", objVoidAppointmentBO.Legionella, DbType.Boolean);
            parametersList.Add(legionellaParam);

            ParameterBO appointmentNotesParam = new ParameterBO("appointmentNotes", objVoidAppointmentBO.AppointmentNotes, DbType.String);
            parametersList.Add(appointmentNotesParam);


            ParameterBO appointmentStartDateParam = new ParameterBO("appointmentStartDate", objVoidAppointmentBO.AppointmentStartDate, DbType.Date);
            parametersList.Add(appointmentStartDateParam);

            ParameterBO appointmentEndDateParam = new ParameterBO("appointmentEndDate", objVoidAppointmentBO.AppointmentEndDate, DbType.Date);
            parametersList.Add(appointmentEndDateParam);

            ParameterBO startTimeParam = new ParameterBO("startTime", objVoidAppointmentBO.StartTime, DbType.String);
            parametersList.Add(startTimeParam);

            ParameterBO endTimeParam = new ParameterBO("endTime", objVoidAppointmentBO.EndTime, DbType.String);
            parametersList.Add(endTimeParam);

            ParameterBO operativeIdParam = new ParameterBO("operativeId", objVoidAppointmentBO.OperativeId, DbType.Int32);
            parametersList.Add(operativeIdParam);

            ParameterBO journalIdParam = new ParameterBO("journalId", objVoidAppointmentBO.JournalId, DbType.Int32);
            parametersList.Add(journalIdParam);

            ParameterBO durationParam = new ParameterBO("duration", objVoidAppointmentBO.Duration, DbType.Int32);
            parametersList.Add(durationParam);

            ParameterBO isSavedParam = new ParameterBO("isSaved", 0, DbType.Int32);
            outParametersList.Add(isSavedParam);

            ParameterBO appointmentIdOutParam = new ParameterBO("appointmentIdOut", 0, DbType.Int32);
            outParametersList.Add(appointmentIdOutParam);

            outParametersList = base.SelectRecord(ref parametersList, ref outParametersList, SpNameConstants.ScheduleVoidInspection);
            appointmentId = Convert.ToInt32(outParametersList[1].Value);

            return outParametersList[0].Value.ToString();

        }

        #endregion


        #region Schedule Void Required Works
        /// <summary>
        /// Schedule Void Required Works
        /// </summary>
        /// <param name="objVoidAppointmentBO"></param>
        /// <returns></returns>
        public string scheduleVoidRequiredWorks(VoidAppointmentBO objVoidAppointmentBO, ref int appointmentId, ref int JournalId, DataTable voidRequiredWorkDuration)
        {

            ParameterList parametersList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO userIdParam = new ParameterBO("userId", objVoidAppointmentBO.UserId, DbType.Int32);
            parametersList.Add(userIdParam);



            ParameterBO appointmentNotesParam = new ParameterBO("appointmentNotes", objVoidAppointmentBO.AppointmentNotes, DbType.String);
            parametersList.Add(appointmentNotesParam);

            ParameterBO jobSheetNotesParam = new ParameterBO("jobSheetNotes", objVoidAppointmentBO.JobSheetNotes, DbType.String);
            parametersList.Add(jobSheetNotesParam);

            ParameterBO appointmentStartDateParam = new ParameterBO("appointmentStartDate", objVoidAppointmentBO.AppointmentStartDate, DbType.Date);
            parametersList.Add(appointmentStartDateParam);

            ParameterBO appointmentEndDateParam = new ParameterBO("appointmentEndDate", objVoidAppointmentBO.AppointmentEndDate, DbType.Date);
            parametersList.Add(appointmentEndDateParam);

            ParameterBO startTimeParam = new ParameterBO("startTime", objVoidAppointmentBO.StartTime, DbType.String);
            parametersList.Add(startTimeParam);

            ParameterBO endTimeParam = new ParameterBO("endTime", objVoidAppointmentBO.EndTime, DbType.String);
            parametersList.Add(endTimeParam);

            ParameterBO operativeIdParam = new ParameterBO("operativeId", objVoidAppointmentBO.OperativeId, DbType.Int32);
            parametersList.Add(operativeIdParam);

            ParameterBO journalIdParam = new ParameterBO("inspectionJournalId ", objVoidAppointmentBO.InspectionJournalId, DbType.Int32);
            parametersList.Add(journalIdParam);

            ParameterBO durationParam = new ParameterBO("duration", objVoidAppointmentBO.Duration, DbType.Decimal);
            parametersList.Add(durationParam);

            //ParameterBO requiredWorkIdsParam = new ParameterBO("requiredWorkIds", objVoidAppointmentBO.RequiredWorkIds, DbType.String);
            //parametersList.Add(requiredWorkIdsParam);

            ParameterBO trades = new ParameterBO("@voidRequiredWorkDuration", voidRequiredWorkDuration, SqlDbType.Structured);
            parametersList.Add(trades);

            ParameterBO isSavedParam = new ParameterBO("isSaved", 0, DbType.Int32);
            outParametersList.Add(isSavedParam);

            ParameterBO appointmentIdOutParam = new ParameterBO("appointmentIdOut", 0, DbType.Int32);
            outParametersList.Add(appointmentIdOutParam);

            ParameterBO journalIdOutParam = new ParameterBO("JournalIdOut", 0, DbType.Int32);
            outParametersList.Add(journalIdOutParam);

            outParametersList = base.SelectRecord(ref parametersList, ref outParametersList, SpNameConstants.ScheduleVoidRequiredWorks);
            appointmentId = Convert.ToInt32(outParametersList[1].Value);
            JournalId = Convert.ToInt32(outParametersList[2].Value);
            return outParametersList[0].Value.ToString();

        }

        #endregion

        #region Schedule Void Required Works
        /// <summary>
        /// Schedule Void Required Works
        /// </summary>
        /// <param name="objVoidAppointmentBO"></param>
        /// <returns></returns>
        public string reScheduleWorksRequired(VoidAppointmentBO objVoidAppointmentBO)
        {

            ParameterList parametersList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO userIdParam = new ParameterBO("userId", objVoidAppointmentBO.UserId, DbType.Int32);
            parametersList.Add(userIdParam);



            ParameterBO appointmentNotesParam = new ParameterBO("appointmentNotes", objVoidAppointmentBO.AppointmentNotes, DbType.String);
            parametersList.Add(appointmentNotesParam);

            ParameterBO jobSheetNotesParam = new ParameterBO("jobSheetNotes", objVoidAppointmentBO.JobSheetNotes, DbType.String);
            parametersList.Add(jobSheetNotesParam);

            ParameterBO appointmentStartDateParam = new ParameterBO("appointmentStartDate", objVoidAppointmentBO.AppointmentStartDate, DbType.Date);
            parametersList.Add(appointmentStartDateParam);

            ParameterBO appointmentEndDateParam = new ParameterBO("appointmentEndDate", objVoidAppointmentBO.AppointmentEndDate, DbType.Date);
            parametersList.Add(appointmentEndDateParam);

            ParameterBO startTimeParam = new ParameterBO("startTime", objVoidAppointmentBO.StartTime, DbType.String);
            parametersList.Add(startTimeParam);

            ParameterBO endTimeParam = new ParameterBO("endTime", objVoidAppointmentBO.EndTime, DbType.String);
            parametersList.Add(endTimeParam);

            ParameterBO operativeIdParam = new ParameterBO("operativeId", objVoidAppointmentBO.OperativeId, DbType.Int32);
            parametersList.Add(operativeIdParam);

            ParameterBO journalIdParam = new ParameterBO("JournalId ", objVoidAppointmentBO.JournalId, DbType.Int32);
            parametersList.Add(journalIdParam);

            ParameterBO durationParam = new ParameterBO("duration", objVoidAppointmentBO.Duration, DbType.Int32);
            parametersList.Add(durationParam);

            ParameterBO appointmentIdOutParam = new ParameterBO("appointmentId", objVoidAppointmentBO.AppointmentId, DbType.Int32);
            parametersList.Add(appointmentIdOutParam);


            ParameterBO isSavedParam = new ParameterBO("isSaved", 0, DbType.Int32);
            outParametersList.Add(isSavedParam);


            outParametersList = base.SelectRecord(ref parametersList, ref outParametersList, SpNameConstants.ReScheduleWorksRequired);


            return outParametersList[0].Value.ToString();

        }

        #endregion


        #region "Cancel Void appointment"
        /// <summary>
        /// Cancel Void appointment
        /// </summary>
        /// <param name="pmo"></param>
        /// <param name="reason"></param>
        /// <returns></returns>
        /// <remarks></remarks>

        public int cancelVoidAppointment(int journalId)
        {

            ParameterList parametersList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO pmoParam = new ParameterBO("journalId", journalId, DbType.Int32);
            parametersList.Add(pmoParam);

            ParameterBO isCancelledParam = new ParameterBO("isCancelled", 0, DbType.Int32);
            outParametersList.Add(isCancelledParam);

            outParametersList = base.SelectRecord(ref parametersList, ref outParametersList, SpNameConstants.CancelVoidAppointment);
            return Convert.ToInt32(outParametersList[0].Value);

        }

        #endregion


        #region Re-Schedule Void Inspection
        /// <summary>
        /// Re-Schedule Void Inspection
        /// </summary>
        /// <param name="objVoidAppointmentBO"></param>
        /// <returns></returns>
        public string reScheduleVoidInspection(VoidAppointmentBO objVoidAppointmentBO)
        {

            ParameterList parametersList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO userIdParam = new ParameterBO("userId", objVoidAppointmentBO.UserId, DbType.Int32);
            parametersList.Add(userIdParam);

            ParameterBO legionellaParam = new ParameterBO("Legionella", objVoidAppointmentBO.Legionella, DbType.Boolean);
            parametersList.Add(legionellaParam);



            ParameterBO appointmentNotesParam = new ParameterBO("appointmentNotes", objVoidAppointmentBO.AppointmentNotes, DbType.String);
            parametersList.Add(appointmentNotesParam);

            ParameterBO appointmentStartDateParam = new ParameterBO("appointmentStartDate", objVoidAppointmentBO.AppointmentStartDate, DbType.Date);
            parametersList.Add(appointmentStartDateParam);

            ParameterBO appointmentEndDateParam = new ParameterBO("appointmentEndDate", objVoidAppointmentBO.AppointmentEndDate, DbType.Date);
            parametersList.Add(appointmentEndDateParam);

            ParameterBO startTimeParam = new ParameterBO("startTime", objVoidAppointmentBO.StartTime, DbType.String);
            parametersList.Add(startTimeParam);

            ParameterBO endTimeParam = new ParameterBO("endTime", objVoidAppointmentBO.EndTime, DbType.String);
            parametersList.Add(endTimeParam);

            ParameterBO operativeIdParam = new ParameterBO("operativeId", objVoidAppointmentBO.OperativeId, DbType.Int32);
            parametersList.Add(operativeIdParam);

            ParameterBO journalIdParam = new ParameterBO("journalId", objVoidAppointmentBO.JournalId, DbType.Int32);
            parametersList.Add(journalIdParam);

         

            ParameterBO isSavedParam = new ParameterBO("isSaved", 0, DbType.Int32);
            outParametersList.Add(isSavedParam);



            outParametersList = base.SelectRecord(ref parametersList, ref outParametersList, SpNameConstants.ReScheduleVoidInspection);
           

            return outParametersList[0].Value.ToString();

        }

        #endregion

        #region Schedule Gas Electric Checks 
        /// <summary>
        /// Schedule Gas Electric Checks 
        /// </summary>
        /// <param name="objVoidAppointmentBo"></param>
        /// <returns></returns>
        public string scheduleGasElectricChecks(VoidAppointmentBO objVoidAppointmentBo, ref int appointmentId)
        {

            ParameterList parametersList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO userIdParam = new ParameterBO("userId", objVoidAppointmentBo.UserId, DbType.Int32);
            parametersList.Add(userIdParam);

            ParameterBO appointmentNotesParam = new ParameterBO("appointmentNotes", objVoidAppointmentBo.AppointmentNotes, DbType.String);
            parametersList.Add(appointmentNotesParam);

            ParameterBO appointmentStartDateParam = new ParameterBO("appointmentStartDate", objVoidAppointmentBo.AppointmentStartDate, DbType.Date);
            parametersList.Add(appointmentStartDateParam);

            ParameterBO appointmentEndDateParam = new ParameterBO("appointmentEndDate", objVoidAppointmentBo.AppointmentEndDate, DbType.Date);
            parametersList.Add(appointmentEndDateParam);

            ParameterBO startTimeParam = new ParameterBO("startTime", objVoidAppointmentBo.StartTime, DbType.String);
            parametersList.Add(startTimeParam);

            ParameterBO endTimeParam = new ParameterBO("endTime", objVoidAppointmentBo.EndTime, DbType.String);
            parametersList.Add(endTimeParam);

            ParameterBO operativeIdParam = new ParameterBO("operativeId", objVoidAppointmentBo.OperativeId, DbType.Int32);
            parametersList.Add(operativeIdParam);

            ParameterBO journalIdParam = new ParameterBO("journalId", objVoidAppointmentBo.JournalId, DbType.Int32);
            parametersList.Add(journalIdParam);

           //ParameterBO checksTypeParam = new ParameterBO("checksType", objVoidAppointmentBo.ChecksType, DbType.String);
           // parametersList.Add(checksTypeParam);
            ParameterBO durationParam = new ParameterBO("duration", objVoidAppointmentBo.Duration, DbType.Decimal);
            parametersList.Add(durationParam);


            ParameterBO appointmentIdOutParam = new ParameterBO("appointmentIdOut", 0, DbType.Int32);
            outParametersList.Add(appointmentIdOutParam);

            ParameterBO isSavedParam = new ParameterBO("isSaved", 0, DbType.Int32);
            outParametersList.Add(isSavedParam);



            outParametersList = base.SelectRecord(ref parametersList, ref outParametersList, SpNameConstants.ScheduleGasElectricChecks);
            appointmentId = Convert.ToInt32(outParametersList[0].Value);

            return outParametersList[1].Value.ToString();

        }

        #endregion

        #region Cancel Gas Electric appointment
        public int cancelGasElectricAppointment(int journalId, int createdBy)
        {
            ParameterList parametersList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO journalIdParam = new ParameterBO("journalId", journalId, DbType.Int32);
            parametersList.Add(journalIdParam);

            ParameterBO createdByParam = new ParameterBO("createdBy", createdBy, DbType.Int32);
            parametersList.Add(createdByParam);


            ParameterBO isCancelledParam = new ParameterBO("isCancelled", 0, DbType.Int32);
            outParametersList.Add(isCancelledParam);

            outParametersList = base.SelectRecord(ref parametersList, ref outParametersList, SpNameConstants.CancelGasElectricAppointment);
            return Convert.ToInt32(outParametersList[0].Value);
        }
        #endregion

        #region Get Appointment Detail By JournalId
        /// <summary>
        ///  Get Appointment Detail By JournalId
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="operativeId"></param>
        /// <remarks></remarks>

        public void getAppointmentDetailByJournalId(ref DataSet resultDataSet, int journalId)
        {
            ParameterList parameterList = new ParameterList();
            ParameterList outParametersList = new ParameterList();
            ParameterBO journalIdParam = new ParameterBO("journalId", journalId, DbType.Int32);
            parameterList.Add(journalIdParam);

            LoadDataSet(ref resultDataSet, ref parameterList, ref outParametersList, SpNameConstants.GetAppointmentDetailByJournalId);
            
        }
        #endregion

        #region "Get Core And Out Of Hours Info"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="resultdataSet"></param>
        /// <param name="employeeId"></param>
        public void getCoreAndOutOfHoursInfo(ref DataSet resultdataSet, int employeeId, string ofcCoreStartTime, string ofcCoreEndTime)
        {
            ParameterList paramList = new ParameterList();

            DataTable coreHoursdt = new DataTable();
            coreHoursdt.TableName = ApplicationConstants.CoreHoursDataTable;
            resultdataSet.Tables.Add(coreHoursdt);

            ParameterBO paramEmployeeId = new ParameterBO("operativeId", employeeId, DbType.Int32);
            paramList.Add(paramEmployeeId);

            ParameterBO ofcCoreStartTimeParam = new ParameterBO("defaultStartTime", ofcCoreStartTime, DbType.String);
            paramList.Add(ofcCoreStartTimeParam);

            ParameterBO ofcCoreEndTimeParam = new ParameterBO("defaultEndTime", ofcCoreEndTime, DbType.String);
            paramList.Add(ofcCoreEndTimeParam);


            IDataReader iDataReader = base.SelectRecord(paramList, SpNameConstants.GetOperativeWorkingHours);
            resultdataSet.Load(iDataReader, LoadOption.OverwriteChanges,coreHoursdt);
            iDataReader.Close();
        }
        #endregion

        #region "Get All Attributes"
        /// <summary>
        /// Get All Attributes
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <remarks></remarks>
        public void getAllAttributes(ref DataSet resultDataSet)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();
            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetAttributeList);
        }
        #endregion
    }
}

