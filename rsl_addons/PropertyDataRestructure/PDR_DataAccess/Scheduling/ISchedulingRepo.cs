﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using PDR_BusinessObject.PageSort;
using PDR_BusinessObject.MeAppointment;
using PDR_BusinessObject.MeSearch;
using PDR_BusinessObject.VoidAppointment;

namespace PDR_DataAccess.Scheduling
{
    public interface ISchedulingRepo
    {
        #region Get Appointment To Be Arranged List
        /// <summary>
        /// Get Appointment To Be Arranged List
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <param name="check56Days"></param>
        /// <param name="msatType"></param>
        /// <returns></returns>
        Int32 getAppointmentToBeArrangedList(ref DataSet resultDataSet, PageSortBO objPageSortBO, MeSearchBO objMeSearchBO);
        #endregion

        #region Get Appointment Arranged List
        /// <summary>
        /// Get Appointment Arranged List
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <param name="check56Days"></param>
        /// <param name="msatType"></param>
        /// <returns></returns>
        Int32 getAppointmentArrangedList(ref DataSet resultDataSet, PageSortBO objPageSortBO, MeSearchBO objMeSearchBO);
        #endregion

        #region Get All Trades
        /// <summary>
        /// Get All Trades
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <param name="check56Days"></param>
        /// <param name="msatType"></param>
        /// <returns></returns>
        void getAllTrades(ref DataSet resultDataSet);
        #endregion

        #region "Get Available Operatives"
        /// <summary>
        /// Get Available Operatives
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="tradeIds"></param>
        /// <param name="startDate"></param>
        void getAvailableOperatives(ref DataSet resultDataSet, string tradeIds, string msattype, DateTime startDate);
        #endregion

        #region "Schedule Me Work appointment"
        /// <summary>
        /// Schedule Me Work appointment
        /// </summary>
        /// <param name="objMeAppointmentBO"></param>
        /// <param name="journalId"></param>
        /// <returns></returns>
        string scheduleMeWorkAppointment(MeAppointmentBO objMeAppointmentBO, int journalId, ref int appointmentId);
        #endregion

        #region "Get Property Detail By JournalId"
        /// <summary>
        /// Get Property Detail By JournalId
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="propertyId"></param>
        /// <remarks></remarks>

        void getPropertyDetailByJournalId(ref DataSet resultDataSet, int journalId, bool isVoid);
        #endregion

        #region "Get Arranged Appointments Detail"
        /// <summary>
        /// Get Arranged Appointments Detail
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="pmo"></param>
        /// <remarks></remarks>

        void getArrangedAppointmentsDetail(ref DataSet resultDataSet, int journalId);
        #endregion

        #region "Update appointment notes"

        /// <summary>
        /// Update appointment notes
        /// </summary>
        /// <param name="customerNotes"></param>
        /// <param name="jobsheetNotes"></param>
        /// <param name="appointmentId"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        int updateMeAppointmentsNotes(string customerNotes, string jobsheetNotes, int appointmentId);
        #endregion

        #region "Cancel appointment"
        /// <summary>
        /// Cancel appointment
        /// </summary>
        /// <param name="pmo"></param>
        /// <param name="reason"></param>
        /// <returns></returns>
        /// <remarks></remarks>

        int cancelAppointment(int journalId, string reason);
        #endregion

        #region "Get Operative Info"
        /// <summary>
        /// Get Operative Info
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="operativeId"></param>
        /// <remarks></remarks>

        void getOperativeInformation(ref DataSet resultDataSet, int operativeId);

        #endregion


        #region Get All Void Operatives
        /// <summary>
        /// Get All Operatives
        /// </summary>
        /// <param name="resultDataSet"></param>       
        /// <returns></returns>
        DataSet getAllVoidOperatives();
        #endregion

        #region "Get Selected Operatives Leaves and Appointments"
        /// <summary>
        /// Get Available Operatives
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="tradeIds"></param>
        /// <param name="startDate"></param>
        void getOperativesLeavesAppointments(ref DataSet resultDataSet, int OperativeId, DateTime startDate);
        #endregion

        #region schedule Void Inspection
        string scheduleVoidInspection(VoidAppointmentBO objVoidAppointmentBO, ref int appointmentId);
        #endregion

        #region schedule Void Required Works
        string scheduleVoidRequiredWorks(VoidAppointmentBO objVoidAppointmentBO, ref int appointmentId, ref int JournalId, DataTable voidRequiredWorkDuration);
        #endregion


        #region Schedule Void Required Works
        /// <summary>
        /// Schedule Void Required Works
        /// </summary>
        /// <param name="objVoidAppointmentBO"></param>
        /// <returns></returns>
        string reScheduleWorksRequired(VoidAppointmentBO objVoidAppointmentBO);

        #endregion

        #region "Cancel Void appointment"
        /// <summary>
        /// Cancel Void appointment
        /// </summary>
        /// <param name="pmo"></param>
        /// <param name="reason"></param>
        /// <returns></returns>
        /// <remarks></remarks>

        int cancelVoidAppointment(int journalId);


        #endregion

        #region Re-Schedule Void Inspection
        string reScheduleVoidInspection(VoidAppointmentBO objVoidAppointmentBO);
        #endregion

        #region Schedule Gas Electric Checks
        /// <summary>
        /// Schedule Gas Electric Checks 
        /// </summary>
        /// <param name="objVoidAppointmentBO"></param>
        /// <returns></returns>
        string scheduleGasElectricChecks(VoidAppointmentBO objVoidAppointmentBo, ref int appointmentId);
        #endregion

        #region cancel Gas/Electric Appointment
        /// <summary>
        /// cancel Gas/Electric Appointment
        /// </summary>
        /// <param name="pmo"></param>
        /// <param name="reason"></param>
        /// <returns></returns>
        /// <remarks></remarks>

        int cancelGasElectricAppointment(int journalId, int createdBy);

        #endregion

        #region Get Appointment Detail By JournalId
        /// <summary>
        ///  Get Appointment Detail By JournalId
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="operativeId"></param>
        /// <remarks></remarks>

        void getAppointmentDetailByJournalId(ref DataSet resultDataSet, int journalId);
        #endregion

        #region "Get Core And Out Of Hours Info"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="employeeId"></param>

        void getCoreAndOutOfHoursInfo(ref DataSet resultDataSet, int employeeId,string ofcCoreStartTime,string ofcCoreEndTime);
        #endregion
        
        #region "Get All Attributes"
        /// <summary>
        /// Get All Attributes
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <remarks></remarks>
        void getAllAttributes(ref DataSet resultDataSet);
        #endregion
    }
}
