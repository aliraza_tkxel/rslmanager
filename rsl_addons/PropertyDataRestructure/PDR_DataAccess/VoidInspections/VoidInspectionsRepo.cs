﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PDR_DataAccess.Base;
using PDR_BusinessObject.Parameter;
using PDR_Utilities.Constants;
using System.Data;
using PDR_BusinessObject.PageSort;
using PDR_BusinessObject.RequiredWorks;
using PDR_BusinessObject.CommonSearch;

namespace PDR_DataAccess.VoidInspections
{
    public class VoidInspectionsRepo : BaseDAL, IVoidInspectionsRepo
    {
        #region get Inspection To Be Arranged List
        /// <summary>
        /// get Inspection To Be Arranged List
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        public int getInspectionToBeArrangedList(ref DataSet resultDataSet, PageSortBO objPageSortBO, string searchText, int patchNum,bool getOnlyCount = false)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO searchTextParam = new ParameterBO("searchText", searchText, DbType.String);
            paramList.Add(searchTextParam);

            ParameterBO patchParam = new ParameterBO("patch", patchNum, DbType.Int32);
            paramList.Add(patchParam);

            ParameterBO pageNumberParam = new ParameterBO("PageNumber", objPageSortBO.PageNumber, DbType.Int32);
            paramList.Add(pageNumberParam);

            ParameterBO pageSizeParam = new ParameterBO("PageSize", objPageSortBO.PageSize, DbType.Int32);
            paramList.Add(pageSizeParam);

            ParameterBO SortDirectionParam = new ParameterBO("sortOrder", objPageSortBO.SmallSortDirection, DbType.String);
            paramList.Add(SortDirectionParam);

            ParameterBO SortExpressionParam = new ParameterBO("sortColumn", objPageSortBO.SortExpression, DbType.String);
            paramList.Add(SortExpressionParam);

            ParameterBO getCountParam = new ParameterBO("getOnlyCount", getOnlyCount, DbType.Boolean);
            paramList.Add(getCountParam);

            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);

            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetVoidInspectionsToBeArranged);

            ParameterBO obj = outParametersList.First();
            Int32 outvalue = Convert.ToInt32(obj.Value.ToString());

            return outvalue;

        }
        #endregion

        #region get Inspection Arranged List
        /// <summary>
        /// get Inspection Arranged List
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        public int getInspectionArrangedList(ref DataSet resultDataSet, PageSortBO objPageSortBO, string searchText,int patchNum, bool getOnlyCount = false)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO searchTextParam = new ParameterBO("searchText", searchText, DbType.String);
            paramList.Add(searchTextParam);

            ParameterBO patchParam = new ParameterBO("patch", patchNum, DbType.Int32);
            paramList.Add(patchParam);

            ParameterBO pageNumberParam = new ParameterBO("PageNumber", objPageSortBO.PageNumber, DbType.Int32);
            paramList.Add(pageNumberParam);

            ParameterBO pageSizeParam = new ParameterBO("PageSize", objPageSortBO.PageSize, DbType.Int32);
            paramList.Add(pageSizeParam);

            ParameterBO SortDirectionParam = new ParameterBO("sortOrder", objPageSortBO.SmallSortDirection, DbType.String);
            paramList.Add(SortDirectionParam);

            ParameterBO SortExpressionParam = new ParameterBO("sortColumn", objPageSortBO.SortExpression, DbType.String);
            paramList.Add(SortExpressionParam);

            ParameterBO getCountParam = new ParameterBO("getOnlyCount", getOnlyCount, DbType.Boolean);
            paramList.Add(getCountParam);

            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);

            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetVoidInspectionArranged);

            ParameterBO obj = outParametersList.First();
            Int32 outvalue = Convert.ToInt32(obj.Value.ToString());

            return outvalue;

        }
        #endregion

        #region get Inspection Arranged By Journal Id
        /// <summary>
        /// get Inspection Arranged By Journal Id
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        public void getInspectionArrangedByJournalId(ref DataSet resultDataSet, int journalId)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO searchTextParam = new ParameterBO("JournalId", journalId, DbType.Int32);
            paramList.Add(searchTextParam);

            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetVoidInspectionByJournalId);

        }
        #endregion

        #region get Void Works To Be Arranged
        /// <summary>
        /// get Void Works To Be Arranged
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        public int getVoidWorksToBeArranged(ref DataSet resultDataSet, PageSortBO objPageSortBO, string searchText,int patch, bool getOnlyCount = false)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO searchTextParam = new ParameterBO("searchText", searchText, DbType.String);
            paramList.Add(searchTextParam);

            ParameterBO patchParam = new ParameterBO("patch", patch, DbType.Int32);
            paramList.Add(patchParam);

            ParameterBO pageNumberParam = new ParameterBO("PageNumber", objPageSortBO.PageNumber, DbType.Int32);
            paramList.Add(pageNumberParam);

            ParameterBO pageSizeParam = new ParameterBO("PageSize", objPageSortBO.PageSize, DbType.Int32);
            paramList.Add(pageSizeParam);

            ParameterBO SortDirectionParam = new ParameterBO("sortOrder", objPageSortBO.SmallSortDirection, DbType.String);
            paramList.Add(SortDirectionParam);

            ParameterBO SortExpressionParam = new ParameterBO("sortColumn", objPageSortBO.SortExpression, DbType.String);
            paramList.Add(SortExpressionParam);

            ParameterBO getCountParam = new ParameterBO("getOnlyCount", getOnlyCount, DbType.Boolean);
            paramList.Add(getCountParam);

            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);

            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetVoidWorksToBeArranged);

            ParameterBO obj = outParametersList.First();
            Int32 outvalue = Convert.ToInt32(obj.Value.ToString());

            return outvalue;

        }
        #endregion

        #region get Void Works Required
        /// <summary>
        /// Get Void Works Required
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        public int getVoidWorksRequired(ref DataSet resultDataSet, PageSortBO objPageSortBO, int journalId, bool isRearrange = false)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParamList = new ParameterList();

            ParameterBO searchTextParam = new ParameterBO("JournalId", journalId, DbType.Int32);
            paramList.Add(searchTextParam);

            ParameterBO isRearrangeParam = new ParameterBO("isRearrange", isRearrange, DbType.Boolean);
            paramList.Add(isRearrangeParam);

            ParameterBO pageNumberParam = new ParameterBO("PageNumber", objPageSortBO.PageNumber, DbType.Int32);
            paramList.Add(pageNumberParam);

            ParameterBO pageSizeParam = new ParameterBO("PageSize", objPageSortBO.PageSize, DbType.Int32);
            paramList.Add(pageSizeParam);

            ParameterBO SortDirectionParam = new ParameterBO("sortOrder", objPageSortBO.SmallSortDirection, DbType.String);
            paramList.Add(SortDirectionParam);

            ParameterBO SortExpressionParam = new ParameterBO("sortColumn", objPageSortBO.SortExpression, DbType.String);
            paramList.Add(SortExpressionParam);

            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParamList.Add(totalCountParam);


            DataTable RecordedWorkDt = new DataTable();
            RecordedWorkDt.TableName = ApplicationConstants.RecordedWorkDt;
            DataTable RequiredWorkDt = new DataTable();
            RequiredWorkDt.TableName = ApplicationConstants.RequiredWorkDt;


            resultDataSet.Tables.Add(RecordedWorkDt);
            resultDataSet.Tables.Add(RequiredWorkDt);


            base.SelectRecord(ref resultDataSet, paramList, ref outParamList, SpNameConstants.GetVoidWorksRequired, RecordedWorkDt, RequiredWorkDt);

            ParameterBO obj = outParamList.First();
            Int32 outvalue = Convert.ToInt32(obj.Value.ToString());

            return outvalue;

        }
        #endregion

        #region get Arranged Void Works Required
        /// <summary>
        /// Get Arranged Void Works Required
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        public int getArrangedVoidWorksRequired(ref DataSet resultDataSet, PageSortBO objPageSortBO, int journalId, bool isRearrange = false)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParamList = new ParameterList();

            ParameterBO searchTextParam = new ParameterBO("JournalId", journalId, DbType.Int32);
            paramList.Add(searchTextParam);

            ParameterBO isRearrangeParam = new ParameterBO("isRearrange", isRearrange, DbType.Boolean);
            paramList.Add(isRearrangeParam);

            ParameterBO pageNumberParam = new ParameterBO("PageNumber", objPageSortBO.PageNumber, DbType.Int32);
            paramList.Add(pageNumberParam);

            ParameterBO pageSizeParam = new ParameterBO("PageSize", objPageSortBO.PageSize, DbType.Int32);
            paramList.Add(pageSizeParam);

            ParameterBO SortDirectionParam = new ParameterBO("sortOrder", objPageSortBO.SmallSortDirection, DbType.String);
            paramList.Add(SortDirectionParam);

            ParameterBO SortExpressionParam = new ParameterBO("sortColumn", objPageSortBO.SortExpression, DbType.String);
            paramList.Add(SortExpressionParam);

            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParamList.Add(totalCountParam);


            DataTable RecordedWorkDt = new DataTable();
            RecordedWorkDt.TableName = ApplicationConstants.RecordedWorkDt;
            DataTable RequiredWorkDt = new DataTable();
            RequiredWorkDt.TableName = ApplicationConstants.RequiredWorkDt;


            resultDataSet.Tables.Add(RecordedWorkDt);
            resultDataSet.Tables.Add(RequiredWorkDt);


            base.SelectRecord(ref resultDataSet, paramList, ref outParamList, SpNameConstants.GetArrangedVoidWorks, RecordedWorkDt, RequiredWorkDt);

            ParameterBO obj = outParamList.First();
            Int32 outvalue = Convert.ToInt32(obj.Value.ToString());

            return outvalue;

        }
        #endregion

        #region get Void Works by inspection journalId
        /// <summary>
        /// get Void Works by inspection journalId
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="journalId"></param>
        /// <returns></returns>
        public void getVoidWorksbyinspectionjournalId(ref DataSet resultDataSet, int journalId)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParamList = new ParameterList();

            ParameterBO inspectionJournalId = new ParameterBO("InspectionJournalID", journalId, DbType.Int32);
            paramList.Add(inspectionJournalId);

            DataTable RecordedWorkDt = new DataTable();
            RecordedWorkDt.TableName = ApplicationConstants.RecordedWorkDt;

            resultDataSet.Tables.Add(RecordedWorkDt);

            base.SelectRecord(ref resultDataSet, paramList, ref outParamList, SpNameConstants.GetVoidWorksByInspectionJournalId, RecordedWorkDt);
        }
        #endregion

        #region get Operative Data For Alert
        /// <summary>
        /// get Operative Data For Alert
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="JournalId"></param>
        /// <returns></returns>
        public void getOperativeDataForAlert(ref DataSet resultDataSet, int journalId)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParamList = new ParameterList();

            ParameterBO inspectionJournalId = new ParameterBO("JournalId", journalId, DbType.Int32);
            paramList.Add(inspectionJournalId);

            DataTable RecordedWorkDt = new DataTable();
            RecordedWorkDt.TableName = ApplicationConstants.RecordedWorkDt;

            resultDataSet.Tables.Add(RecordedWorkDt);

            base.SelectRecord(ref resultDataSet, paramList, ref outParamList, SpNameConstants.GetOperativeDataForAlert, RecordedWorkDt);
        }

        #endregion

        #region get Supplier Data For Alert
        /// <summary>
        /// get Supplier Data For Alert
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="poId"></param>
        /// <returns></returns>
        public void getSupplierDataForAlert(ref DataSet resultDataSet, int poId)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParamList = new ParameterList();

            ParameterBO inspectionJournalId = new ParameterBO("poID", poId, DbType.Int32);
            paramList.Add(inspectionJournalId);

            DataTable RecordedWorkDt = new DataTable();
            RecordedWorkDt.TableName = ApplicationConstants.RecordedWorkDt;

            resultDataSet.Tables.Add(RecordedWorkDt);

            base.SelectRecord(ref resultDataSet, paramList, ref outParamList, SpNameConstants.GetSupplierDataForAlert, RecordedWorkDt);
        }

        #endregion

        #region update Tenant To Complete
        /// <summary>
        /// update Tenant To Complete
        /// </summary>
        /// <param name="worksRequiredId"></param>
        /// <param name="checkedValue"></param>
        public string updateTenantToComplete(int worksRequiredId, bool checkedValue)
        {
            ParameterList parameterList = new ParameterList();
            ParameterList outparameterList = new ParameterList();


            ParameterBO worksRequiredIdParam = new ParameterBO("worksRequiredId", worksRequiredId, DbType.Int32);
            parameterList.Add(worksRequiredIdParam);

            ParameterBO checkedValueParam = new ParameterBO("checkedValue", checkedValue, DbType.Boolean);
            parameterList.Add(checkedValueParam);

            ParameterBO isSavedParam = new ParameterBO("isSaved", 0, DbType.Int32);
            outparameterList.Add(isSavedParam);

            SaveRecord(ref parameterList, ref outparameterList, SpNameConstants.UpdateTenantToComplete);
            outparameterList = base.SelectRecord(ref parameterList, ref outparameterList, SpNameConstants.UpdateTenantToComplete);
           

            return outparameterList[0].Value.ToString();
        }
        #endregion


        #region add Tenant Recharges
        /// <summary>
        /// add Tenant Recharges
        /// </summary>
        /// <param name="worksRequiredId"></param>
        /// <param name="checkedValue"></param>
        /// /// <param name="tenantNeglect"></param>
        public string addTenantRecharges(int worksRequiredId, bool checkedValue, Decimal tenantNeglect)
        {
            ParameterList parameterList = new ParameterList();
            ParameterList outparameterList = new ParameterList();


            ParameterBO worksRequiredIdParam = new ParameterBO("worksRequiredId", worksRequiredId, DbType.Int32);
            parameterList.Add(worksRequiredIdParam);

            ParameterBO checkedValueParam = new ParameterBO("checkedValue", checkedValue, DbType.Boolean);
            parameterList.Add(checkedValueParam);

            ParameterBO tenantNeglectParam = new ParameterBO("tenantNeglect", tenantNeglect, DbType.Decimal);
            parameterList.Add(tenantNeglectParam);

            ParameterBO isSavedParam = new ParameterBO("isSaved", 0, DbType.Int32);
            outparameterList.Add(isSavedParam);

            outparameterList = base.SelectRecord(ref parameterList, ref outparameterList, SpNameConstants.AddTenantRecharges);

            return outparameterList[0].Value.ToString();
        }
        #endregion

        #region get Void Area List
        /// <summary>
        /// get Void Area List
        /// </summary>
        /// <param name="resultDataSet"></param>
        public void getVoidAreaList(ref DataSet resultDataSet)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();
            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetVoidAreaList);
        }
        #endregion

        #region update Required Works
        /// <summary>
        /// update Required Works
        /// </summary>
        /// <param name="requiredWorksBO"></param>
        public string updateRequiredWorks(RequiredWorksBO requiredWorksBO)
        {
            ParameterList parameterList = new ParameterList();
            ParameterList outparameterList = new ParameterList();


            ParameterBO requiredWorksIdParam = new ParameterBO("requiredWorksId", requiredWorksBO.RequiredWorksId, DbType.Int32);
            parameterList.Add(requiredWorksIdParam);

            ParameterBO locationIdParam = new ParameterBO("locationId", requiredWorksBO.LocationId, DbType.Int32);
            parameterList.Add(locationIdParam);

            ParameterBO workDescriptionParam = new ParameterBO("workDescription", requiredWorksBO.WorkDescription, DbType.String);
            parameterList.Add(workDescriptionParam);

            ParameterBO isTenantWorksParam = new ParameterBO("isTenantWorks", requiredWorksBO.IsTenantWorks, DbType.Boolean);
            parameterList.Add(isTenantWorksParam);

            ParameterBO isCanceledParam = new ParameterBO("isCanceled", requiredWorksBO.IsCanceled, DbType.Boolean);
            parameterList.Add(isCanceledParam);

            ParameterBO updatedByParam = new ParameterBO("updatedBy", requiredWorksBO.UpdatedBy, DbType.Boolean);
            parameterList.Add(updatedByParam);

            ParameterBO isSavedParam = new ParameterBO("isSaved", 0, DbType.Int32);
            outparameterList.Add(isSavedParam);

            
            outparameterList = base.SelectRecord(ref parameterList, ref outparameterList, SpNameConstants.UpdateRequiredWorks);


            return outparameterList[0].Value.ToString();
           // SaveRecord(ref parameterList, ref outparameterList, SpNameConstants.UpdateRequiredWorks);
        }
        #endregion

        #region "Get Tenant(s) Info by TenancyId - Joint Tenancy"

        //To implement joint tenants information.

        public void GetJointTenantsInfoByTenancyID(ref DataSet dstenantsInfo, ref int tenancyID)
        {
            ParameterList parametersList = new ParameterList();

            ParameterBO tenancyIDParam = new ParameterBO("tenancyID", tenancyID, DbType.Int32);
            parametersList.Add(tenancyIDParam);

            LoadDataSet(ref dstenantsInfo, ref  parametersList, SpNameConstants.GetJointTenantsInfoByTenancyID);

        }

        #endregion

        #region get Checks To Be Arranged
        /// <summary>
        /// get Checks To Be Arranged
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        public Int32 getGasElectricChecksToBeArranged(ref DataSet resultDataSet, PageSortBO objPageSortBO, CommonSearchBO objCommonSearchBo,int patch, bool getOnlyCount = false)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO searchTextParam = new ParameterBO("searchText", objCommonSearchBo.SearchText, DbType.String);
            paramList.Add(searchTextParam);

            ParameterBO patchParam = new ParameterBO("patch", patch, DbType.Int32);
            paramList.Add(patchParam);
           
            ParameterBO checkTypeParam = new ParameterBO("checksRequired", objCommonSearchBo.ChecksRequiredType, DbType.Int32);
            paramList.Add(checkTypeParam);


            ParameterBO pageNumberParam = new ParameterBO("PageNumber", objPageSortBO.PageNumber, DbType.Int32);
            paramList.Add(pageNumberParam);

            ParameterBO pageSizeParam = new ParameterBO("PageSize", objPageSortBO.PageSize, DbType.Int32);
            paramList.Add(pageSizeParam);

            ParameterBO SortDirectionParam = new ParameterBO("sortOrder", objPageSortBO.SmallSortDirection, DbType.String);
            paramList.Add(SortDirectionParam);

            ParameterBO SortExpressionParam = new ParameterBO("sortColumn", objPageSortBO.SortExpression, DbType.String);
            paramList.Add(SortExpressionParam);

            ParameterBO getCountParam = new ParameterBO("getOnlyCount", getOnlyCount, DbType.Boolean);
            paramList.Add(getCountParam);

            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);

            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetChecksToBeArranged);

            ParameterBO obj = outParametersList.First();
            Int32 outvalue = Convert.ToInt32(obj.Value.ToString());

            return outvalue;

        }
        #endregion

        #region void checks Arranged
        /// <summary>
        /// void checks Arranged
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="objCommonSearchBo"></param>
        /// <returns></returns>
        public int getGasElectricChecksArranged(ref DataSet resultDataSet, PageSortBO objPageSortBO, CommonSearchBO objCommonSearchBo,int patch, bool getOnlyCount = false)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO searchTextParam = new ParameterBO("searchText", objCommonSearchBo.SearchText, DbType.String);
            paramList.Add(searchTextParam);

            ParameterBO patchParam = new ParameterBO("patch", patch, DbType.Int32);
            paramList.Add(patchParam);

            ParameterBO checkTypeParam = new ParameterBO("checksRequired", objCommonSearchBo.ChecksRequiredType, DbType.Int32);
            paramList.Add(checkTypeParam);


            ParameterBO pageNumberParam = new ParameterBO("PageNumber", objPageSortBO.PageNumber, DbType.Int32);
            paramList.Add(pageNumberParam);

            ParameterBO pageSizeParam = new ParameterBO("PageSize", objPageSortBO.PageSize, DbType.Int32);
            paramList.Add(pageSizeParam);

            ParameterBO SortDirectionParam = new ParameterBO("sortOrder", objPageSortBO.SmallSortDirection, DbType.String);
            paramList.Add(SortDirectionParam);

            ParameterBO SortExpressionParam = new ParameterBO("sortColumn", objPageSortBO.SortExpression, DbType.String);
            paramList.Add(SortExpressionParam);

            ParameterBO getCountParam = new ParameterBO("getOnlyCount", getOnlyCount, DbType.Boolean);
            paramList.Add(getCountParam);

            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);

            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetChecksArranged);

            ParameterBO obj = outParametersList.First();
            Int32 outvalue = Convert.ToInt32(obj.Value.ToString());

            return outvalue;
        }

        #endregion

        #region get Post Void Inspection To Be Arranged List
        /// <summary>
        /// get Inspection To Be Arranged List
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        public int getPostVoidInspectionToBeArrangedList(ref DataSet resultDataSet, PageSortBO objPageSortBO, string searchText,int patch, bool getOnlyCount = false)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO searchTextParam = new ParameterBO("searchText", searchText, DbType.String);
            paramList.Add(searchTextParam);


            ParameterBO patchParam = new ParameterBO("patch", patch, DbType.Int32);
            paramList.Add(patchParam);

            ParameterBO pageNumberParam = new ParameterBO("PageNumber", objPageSortBO.PageNumber, DbType.Int32);
            paramList.Add(pageNumberParam);

            ParameterBO pageSizeParam = new ParameterBO("PageSize", objPageSortBO.PageSize, DbType.Int32);
            paramList.Add(pageSizeParam);

            ParameterBO SortDirectionParam = new ParameterBO("sortOrder", objPageSortBO.SmallSortDirection, DbType.String);
            paramList.Add(SortDirectionParam);

            ParameterBO SortExpressionParam = new ParameterBO("sortColumn", objPageSortBO.SortExpression, DbType.String);
            paramList.Add(SortExpressionParam);

            ParameterBO getCountParam = new ParameterBO("getOnlyCount", getOnlyCount, DbType.Boolean);
            paramList.Add(getCountParam);

            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);

            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetPostVoidInspectionsToBeArranged);

            ParameterBO obj = outParametersList.First();
            Int32 outvalue = Convert.ToInt32(obj.Value.ToString());

            return outvalue;

        }
        #endregion

        #region get Post Void Inspection Arranged List
        /// <summary>
        /// get Inspection Arranged List
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        public int getPostVoidInspectionArrangedList(ref DataSet resultDataSet, PageSortBO objPageSortBO, string searchText,int patch, bool getOnlyCount = false)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO searchTextParam = new ParameterBO("searchText", searchText, DbType.String);
            paramList.Add(searchTextParam);

            ParameterBO patchParam = new ParameterBO("patch", patch, DbType.String);
            paramList.Add(patchParam);

            ParameterBO pageNumberParam = new ParameterBO("PageNumber", objPageSortBO.PageNumber, DbType.Int32);
            paramList.Add(pageNumberParam);

            ParameterBO pageSizeParam = new ParameterBO("PageSize", objPageSortBO.PageSize, DbType.Int32);
            paramList.Add(pageSizeParam);

            ParameterBO SortDirectionParam = new ParameterBO("sortOrder", objPageSortBO.SmallSortDirection, DbType.String);
            paramList.Add(SortDirectionParam);

            ParameterBO SortExpressionParam = new ParameterBO("sortColumn", objPageSortBO.SortExpression, DbType.String);
            paramList.Add(SortExpressionParam);

            ParameterBO getCountParam = new ParameterBO("getOnlyCount", getOnlyCount, DbType.Boolean);
            paramList.Add(getCountParam);

            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);

            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetPostVoidInspectionArranged);

            ParameterBO obj = outParametersList.First();
            Int32 outvalue = Convert.ToInt32(obj.Value.ToString());

            return outvalue;

        }
        #endregion

        #region get Pending Termination
        /// <summary>
        /// Get Available Properties
        /// </summary>
        /// <param name="schemeId"></param>
        /// <returns></returns>
        public int getPendingTermination(ref DataSet resultDataSet, PageSortBO objPageSortBO, string searchText, bool getOnlyCount = false)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO searchTextParam = new ParameterBO("searchText", searchText, DbType.String);
            paramList.Add(searchTextParam);

            ParameterBO pageNumberParam = new ParameterBO("PageNumber", objPageSortBO.PageNumber, DbType.Int32);
            paramList.Add(pageNumberParam);

            ParameterBO pageSizeParam = new ParameterBO("PageSize", objPageSortBO.PageSize, DbType.Int32);
            paramList.Add(pageSizeParam);

            ParameterBO SortDirectionParam = new ParameterBO("sortOrder", objPageSortBO.SmallSortDirection, DbType.String);
            paramList.Add(SortDirectionParam);

            ParameterBO SortExpressionParam = new ParameterBO("sortColumn", objPageSortBO.SortExpression, DbType.String);
            paramList.Add(SortExpressionParam);

            ParameterBO getCountParam = new ParameterBO("getOnlyCount", getOnlyCount, DbType.Boolean);
            paramList.Add(getCountParam);

            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);


            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetPendingTermination);

            ParameterBO obj = outParametersList.First();
            Int32 outvalue = Convert.ToInt32(obj.Value.ToString());

            return outvalue;

        }
        #endregion

        #region get Relet Alert Count
        /// <summary>
        /// get Relet Alert Count
        /// </summary>
        /// <param name="schemeId"></param>
        /// <returns></returns>
        public int getReletAlertCount(ref DataSet resultDataSet, PageSortBO objPageSortBO, string searchText, bool getOnlyCount = false)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO searchTextParam = new ParameterBO("searchText", searchText, DbType.String);
            paramList.Add(searchTextParam);

            ParameterBO pageNumberParam = new ParameterBO("PageNumber", objPageSortBO.PageNumber, DbType.Int32);
            paramList.Add(pageNumberParam);

            ParameterBO pageSizeParam = new ParameterBO("PageSize", objPageSortBO.PageSize, DbType.Int32);
            paramList.Add(pageSizeParam);

            ParameterBO SortDirectionParam = new ParameterBO("sortOrder", objPageSortBO.SmallSortDirection, DbType.String);
            paramList.Add(SortDirectionParam);

            ParameterBO SortExpressionParam = new ParameterBO("sortColumn", objPageSortBO.SortExpression, DbType.String);
            paramList.Add(SortExpressionParam);

            ParameterBO getCountParam = new ParameterBO("getOnlyCount", getOnlyCount, DbType.Boolean);
            paramList.Add(getCountParam);

            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);


            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetReletAlertCount);

            ParameterBO obj = outParametersList.First();
            Int32 outvalue = Convert.ToInt32(obj.Value.ToString());

            return outvalue;

          

        }
        #endregion

        #region get  No entry Count and List
        /// <summary>
        /// get Count no entry
        /// </summary>
        /// <param name="schemeId"></param>
        /// <returns></returns>
        public int getNoEntryCountAndList(ref DataSet resultDataSet, PageSortBO objPageSortBO, string searchText, bool getOnlyCount = false)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO searchTextParam = new ParameterBO("searchText", searchText, DbType.String);
            paramList.Add(searchTextParam);

            ParameterBO pageNumberParam = new ParameterBO("PageNumber", objPageSortBO.PageNumber, DbType.Int32);
            paramList.Add(pageNumberParam);

            ParameterBO pageSizeParam = new ParameterBO("PageSize", objPageSortBO.PageSize, DbType.Int32);
            paramList.Add(pageSizeParam);

            ParameterBO SortDirectionParam = new ParameterBO("sortOrder", objPageSortBO.SmallSortDirection, DbType.String);
            paramList.Add(SortDirectionParam);

            ParameterBO SortExpressionParam = new ParameterBO("sortColumn", objPageSortBO.SortExpression, DbType.String);
            paramList.Add(SortExpressionParam);

            ParameterBO getCountParam = new ParameterBO("getOnlyCount", getOnlyCount, DbType.Boolean);
            paramList.Add(getCountParam);

            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);

            
            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetNoEntryCountAndList);

            ParameterBO obj = outParametersList.First();
            Int32 outvalue = Convert.ToInt32(obj.Value.ToString());

            return outvalue;

        }
        #endregion

        #region Get Available Properties
        /// <summary>
        /// Get Available Properties
        /// </summary>
        /// <param name="schemeId"></param>
        /// <returns></returns>
        public int getAvailableProperties(ref DataSet resultDataSet, PageSortBO objPageSortBO, string searchText, bool getOnlyCount = false)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO searchTextParam = new ParameterBO("searchText", searchText, DbType.String);
            paramList.Add(searchTextParam);

            ParameterBO pageNumberParam = new ParameterBO("PageNumber", objPageSortBO.PageNumber, DbType.Int32);
            paramList.Add(pageNumberParam);

            ParameterBO pageSizeParam = new ParameterBO("PageSize", objPageSortBO.PageSize, DbType.Int32);
            paramList.Add(pageSizeParam);

            ParameterBO SortDirectionParam = new ParameterBO("sortOrder", objPageSortBO.SmallSortDirection, DbType.String);
            paramList.Add(SortDirectionParam);

            ParameterBO SortExpressionParam = new ParameterBO("sortColumn", objPageSortBO.SortExpression, DbType.String);
            paramList.Add(SortExpressionParam);

            ParameterBO getCountParam = new ParameterBO("getOnlyCount", getOnlyCount, DbType.Boolean);
            paramList.Add(getCountParam);

            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);


            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetAvailableProperties);

            ParameterBO obj = outParametersList.First();
            Int32 outvalue = Convert.ToInt32(obj.Value.ToString());

            return outvalue;

        }
        #endregion


        #region get Property Info By JournalId

        //To implement joint tenants information.

        public DataSet getPropertyInfoByJournalId(int journalId)
        {
            ParameterList parametersList = new ParameterList();
            DataSet resultDs = new DataSet();

            ParameterBO journalIdParam = new ParameterBO("journalId", journalId, DbType.Int32);
            parametersList.Add(journalIdParam);

            LoadDataSet(ref resultDs, ref  parametersList, SpNameConstants.GetPropertyAndTenantInfoByJournalId);
            return resultDs;
        }

        #endregion

        #region get Void Works Arranged
        /// <summary>
        /// get Void Works Arranged
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        public int getVoidWorksArranged(ref DataSet resultDataSet, PageSortBO objPageSortBO, string searchText,int patch, bool getOnlyCount = false)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO searchTextParam = new ParameterBO("searchText", searchText, DbType.String);
            paramList.Add(searchTextParam);

            ParameterBO patchParam = new ParameterBO("patch", patch, DbType.Int32);
            paramList.Add(patchParam);

            ParameterBO pageNumberParam = new ParameterBO("PageNumber", objPageSortBO.PageNumber, DbType.Int32);
            paramList.Add(pageNumberParam);

            ParameterBO pageSizeParam = new ParameterBO("PageSize", objPageSortBO.PageSize, DbType.Int32);
            paramList.Add(pageSizeParam);

            ParameterBO SortDirectionParam = new ParameterBO("sortOrder", objPageSortBO.SmallSortDirection, DbType.String);
            paramList.Add(SortDirectionParam);

            ParameterBO SortExpressionParam = new ParameterBO("sortColumn", objPageSortBO.SortExpression, DbType.String);
            paramList.Add(SortExpressionParam);

            ParameterBO getCountParam = new ParameterBO("getOnlyCount", getOnlyCount, DbType.Boolean);
            paramList.Add(getCountParam);

            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);

            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetVoidWorksArranged);

            ParameterBO obj = outParametersList.First();
            Int32 outvalue = Convert.ToInt32(obj.Value.ToString());

            return outvalue;

        }
        #endregion

        #region Get patch data
        public void getPatchData(ref DataSet resultDataset)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            LoadDataSet(ref resultDataset, ref paramList, ref outParametersList, SpNameConstants.GetPatch);
        }
        #endregion
    }
}
