﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using PDR_BusinessObject.PageSort;
using PDR_BusinessObject.RequiredWorks;
using PDR_BusinessObject.CommonSearch;

namespace PDR_DataAccess.VoidInspections
{
    public interface IVoidInspectionsRepo
    {
        #region get Inspection To Be Arranged List
        /// <summary>
        /// get Inspection To Be Arranged List
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        Int32 getInspectionToBeArrangedList(ref DataSet resultDataSet, PageSortBO objPageSortBO, String searchText, int patchNum,bool getOnlyCount = false);
        #endregion

        #region get Inspection Arranged List
        /// <summary>
        /// get Inspection Arranged List
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        Int32 getInspectionArrangedList(ref DataSet resultDataSet, PageSortBO objPageSortBO, String searchText, int patchNum,bool getOnlyCount = false);
        #endregion

        #region get Inspection Arranged By Journal Id
        /// <summary>
        /// get Inspection Arranged By Journal Id
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        void getInspectionArrangedByJournalId(ref DataSet resultDataSet, int journalId);
        #endregion

        #region get Void Works To Be Arranged
        /// <summary>
        /// get Void Works To Be Arranged
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        int getVoidWorksToBeArranged(ref DataSet resultDataSet, PageSortBO objPageSortBO, string searchText,int patchNum, bool getOnlyCount = false);
        #endregion

        #region get Void Works Required
        /// <summary>
        /// Get Void Works Required
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        int getVoidWorksRequired(ref DataSet resultDataSet, PageSortBO objPageSortBO, int journalId, bool isRearrange = false);
        #endregion

        #region get Arranged Void Works Required
        /// <summary>
        /// Get Arranged Void Works Required
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        int getArrangedVoidWorksRequired(ref DataSet resultDataSet, PageSortBO objPageSortBO, int journalId, bool isRearrange = false);
        #endregion

        #region get Void Works by inspection journalId
        /// <summary>
        /// get Void Works by inspection journalId
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="journalId"></param>
        /// <returns></returns>
        void getVoidWorksbyinspectionjournalId(ref DataSet resultDataSet, int journalId);
        
        #endregion

        #region get Operative Data For Alert
        /// <summary>
        /// get Operative Data For Alert
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="journalId"></param>
        /// <returns></returns>
        void getOperativeDataForAlert(ref DataSet resultDataSet, int journalId);

        #endregion

        #region get Supplier Data For Alert
        /// <summary>
        /// get Supplier Data For Alert
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="poId"></param>
        /// <returns></returns>
        void getSupplierDataForAlert(ref DataSet resultDataSet, int poId);

        #endregion

        #region update Tenant To Complete
        /// <summary>
        /// update Tenant To Complete
        /// </summary>
        /// <param name="worksRequiredId"></param>
        /// <param name="checkedValue"></param>
        string updateTenantToComplete(int worksRequiredId, bool checkedValue);
        #endregion

        #region add Tenant Recharges
        /// <summary>
        /// add Tenant Recharges
        /// </summary>
        /// <param name="worksRequiredId"></param>
        /// <param name="checkedValue"></param>
        string addTenantRecharges(int worksRequiredId, bool checkedValue, Decimal tenantNeglect);
        #endregion

        #region get Void Area List
        /// <summary>
        /// get Void Area List
        /// </summary>
        /// <param name="resultDataSet"></param>
        void getVoidAreaList(ref DataSet resultDataSet);

        #endregion

        #region Update Required Works
        /// <summary>
        /// Update Required Works
        /// </summary>
        /// <param name="worksRequiredId"></param>
        /// <param name="checkedValue"></param>
        string updateRequiredWorks(RequiredWorksBO requiredWorksBO);
        #endregion

        #region "Get Tenant(s) Info by TenancyId - Joint Tenancy"

        //To implement joint tenants information.

        void GetJointTenantsInfoByTenancyID(ref DataSet dstenantsInfo, ref int tenancyID);


        #endregion

        #region get Gas Electric Checks To Be Arranged List
        /// <summary>
        /// get Gas Electric Checks To Be Arranged List
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        Int32 getGasElectricChecksToBeArranged(ref DataSet resultDataSet, PageSortBO objPageSortBO, CommonSearchBO objCommonSearchBo,int patch, bool getOnlyCount = false);
        #endregion

        #region get Gas Electric Checks  Arranged List
        /// <summary>
        /// get Inspection Arranged List
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        Int32 getGasElectricChecksArranged(ref DataSet resultDataSet, PageSortBO objPageSortBO, CommonSearchBO objCommonSearchBo, int patch,bool getOnlyCount = false);
        #endregion

        #region get Inspection To Be Arranged List
        /// <summary>
        /// get Post Void Inspection To Be Arranged List
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        Int32 getPostVoidInspectionToBeArrangedList(ref DataSet resultDataSet, PageSortBO objPageSortBO, String searchText,int patch, bool getOnlyCount = false);
        #endregion

        #region get Inspection Arranged List
        /// <summary>
        /// get Post Void Inspection Arranged List
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        Int32 getPostVoidInspectionArrangedList(ref DataSet resultDataSet, PageSortBO objPageSortBO, String searchText,int patch, bool getOnlyCount = false);
        #endregion

        #region get Pending Termination
        /// <summary>
        /// get Pending Termination
        /// </summary>
        /// <param name="schemeId"></param>
        /// <returns></returns>
        int getPendingTermination(ref DataSet resultDataSet, PageSortBO objPageSortBO, string searchText, bool getOnlyCount = false);
        #endregion

        #region get Relet Alert Count
        /// <summary>
        /// get Relet Alert Count
        /// </summary>
        /// <param name="schemeId"></param>
        /// <returns></returns>
       
        int getReletAlertCount(ref DataSet resultDataSet, PageSortBO objPageSortBO, string searchText, bool getOnlyCount = false);
        #endregion

        #region get  Count No entry
        /// <summary>
        /// get Count no entry
        /// </summary>
        /// <param name="schemeId"></param>
        /// <returns></returns>
        int getNoEntryCountAndList(ref DataSet resultDataSet, PageSortBO objPageSortBO, string searchText, bool getOnlyCount = false);
        #endregion

        #region get Available Properties
        /// <summary>
        /// get Available Properties
        /// </summary>
        /// <param name="schemeId"></param>
        /// <returns></returns>
        int getAvailableProperties(ref DataSet resultDataSet, PageSortBO objPageSortBO, string searchText, bool getOnlyCount = false);
        #endregion

        DataSet getPropertyInfoByJournalId(int journalId);
        #region get Void Works Arranged
        /// <summary>
        /// get Void Works  Arranged
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        int getVoidWorksArranged(ref DataSet resultDataSet, PageSortBO objPageSortBO, string searchText, int patch,bool getOnlyCount = false);
        #endregion

        #region get patch filter
        void getPatchData(ref DataSet resultDataset);
        #endregion
    }
}
