﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using PDR_BusinessObject;
using PDR_DataAccess.Base;
using PDR_Utilities.Constants;
using PDR_BusinessObject.Reports;
using PDR_BusinessObject.PageSort;
using PDR_BusinessObject.Parameter;


namespace PDR_DataAccess.Reports
{
    public class ReportsRepo : BaseDAL, IReportsRepo
    {

        #region get Professional Analysis Report
        /// <summary>
        /// get Professional Analysis Report
        /// </summary>
        /// <param name="objReportsBO"></param>
        /// <returns>resultDataSet</returns>
        public DataSet getProfessionalAnalysisReport(ReportsBO objReportsBO)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO reportDateParam = new ParameterBO("REPORTDATE", objReportsBO.ReportDate, DbType.Date);
            paramList.Add(reportDateParam);

            ParameterBO localAuthorityParam = new ParameterBO("LOCALAUTHORITY", objReportsBO.LocalAuthority, DbType.Int32);
            paramList.Add(localAuthorityParam);

            ParameterBO schemeParam = new ParameterBO("SCHEME", objReportsBO.Scheme, DbType.Int32);
            paramList.Add(schemeParam);

            ParameterBO postCodeParam = new ParameterBO("POSTCODE", objReportsBO.PostCode, DbType.String);
            paramList.Add(postCodeParam);

            ParameterBO patchParam = new ParameterBO("PATCH", objReportsBO.Patch, DbType.String);
            paramList.Add(patchParam);

            DataSet resultDataSet = new DataSet();
            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetProfessionalAnalysisReport);
            return resultDataSet;


        }
        #endregion

        #region get Professional Analysis Report Single Asset Type
        /// <summary>
        /// get Professional Analysis Report Single Asset Type
        /// </summary>
        /// <param name="objReportsBO"></param>
        /// <returns>resultDataSet</returns>
        public DataSet getProfessionalAnalysisReportSingleAssetType(ReportsBO objReportsBO)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO reportDateParam = new ParameterBO("REPORTDATE", objReportsBO.ReportDate, DbType.Date);
            paramList.Add(reportDateParam);

            ParameterBO localAuthorityParam = new ParameterBO("LOCALAUTHORITY", objReportsBO.LocalAuthority, DbType.Int32);
            paramList.Add(localAuthorityParam);

            ParameterBO schemeParam = new ParameterBO("SCHEME", objReportsBO.Scheme, DbType.Int32);
            paramList.Add(schemeParam);

            ParameterBO postCodeParam = new ParameterBO("POSTCODE", objReportsBO.PostCode, DbType.String);
            paramList.Add(postCodeParam);

            ParameterBO patchParam = new ParameterBO("PATCH", objReportsBO.Patch, DbType.String);
            paramList.Add(patchParam);

            ParameterBO assetTypeParam = new ParameterBO("ASSETTYPE", objReportsBO.AssetType, DbType.Int32);
            paramList.Add(assetTypeParam);

            ParameterBO statusParam = new ParameterBO("STATUS", objReportsBO.Status, DbType.Int32);
            paramList.Add(statusParam);

            DataSet resultDataSet = new DataSet();
            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetProfessionalAnalysisReportSingleAssetType);
            return resultDataSet;


        }
        #endregion

        #region get Professional Analysis Report Single Asset Type With Occupancy
        /// <summary>
        /// get Professional Analysis Report Single Asset Type With Occupancy
        /// </summary>
        /// <param name="objReportsBO"></param>
        /// <returns>resultDataSet</returns>
        public DataSet getProfessionalAnalysisReportWithOccupancy(ReportsBO objReportsBO)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO reportDateParam = new ParameterBO("REPORTDATE", objReportsBO.ReportDate, DbType.Date);
            paramList.Add(reportDateParam);

            ParameterBO localAuthorityParam = new ParameterBO("LOCALAUTHORITY", objReportsBO.LocalAuthority, DbType.Int32);
            paramList.Add(localAuthorityParam);

            ParameterBO schemeParam = new ParameterBO("SCHEME", objReportsBO.Scheme, DbType.Int32);
            paramList.Add(schemeParam);

            ParameterBO postCodeParam = new ParameterBO("POSTCODE", objReportsBO.PostCode, DbType.String);
            paramList.Add(postCodeParam);

            ParameterBO patchParam = new ParameterBO("PATCH", objReportsBO.Patch, DbType.String);
            paramList.Add(patchParam);

            ParameterBO assetTypeParam = new ParameterBO("ASSETTYPE", objReportsBO.AssetType, DbType.Int32);
            paramList.Add(assetTypeParam);

            ParameterBO propertyTypeParam = new ParameterBO("PROPERTYTYPE", objReportsBO.PropertyType, DbType.Int32);
            paramList.Add(propertyTypeParam);

            ParameterBO statusParam = new ParameterBO("STATUS", objReportsBO.Status, DbType.Int32);
            paramList.Add(statusParam);

            DataSet resultDataSet = new DataSet();
            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetProfessionalAnalysisReportWithOccupancy);
            return resultDataSet;


        }
        #endregion

        #region get Professional Analysis Report Single Asset Type With Out Occupancy
        /// <summary>
        /// get Professional Analysis Report Single Asset Type With Occupancy
        /// </summary>
        /// <param name="objReportsBO"></param>
        /// <returns>resultDataSet</returns>
        public DataSet getProfessionalAnalysisReportWithOutOccupancy(ReportsBO objReportsBO)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO reportDateParam = new ParameterBO("REPORTDATE", objReportsBO.ReportDate, DbType.Date);
            paramList.Add(reportDateParam);

            ParameterBO localAuthorityParam = new ParameterBO("LOCALAUTHORITY", objReportsBO.LocalAuthority, DbType.Int32);
            paramList.Add(localAuthorityParam);

            ParameterBO schemeParam = new ParameterBO("SCHEME", objReportsBO.Scheme, DbType.Int32);
            paramList.Add(schemeParam);

            ParameterBO postCodeParam = new ParameterBO("POSTCODE", objReportsBO.PostCode, DbType.String);
            paramList.Add(postCodeParam);

            ParameterBO patchParam = new ParameterBO("PATCH", objReportsBO.Patch, DbType.String);
            paramList.Add(patchParam);

            ParameterBO assetTypeParam = new ParameterBO("ASSETTYPE", objReportsBO.AssetType, DbType.Int32);
            paramList.Add(assetTypeParam);

            ParameterBO propertyTypeParam = new ParameterBO("PROPERTYTYPE", objReportsBO.PropertyType, DbType.Int32);
            paramList.Add(propertyTypeParam);

            ParameterBO statusParam = new ParameterBO("STATUS", objReportsBO.Status, DbType.Int32);
            paramList.Add(statusParam);

            DataSet resultDataSet = new DataSet();
            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetProfessionalAnalysisReportWithOutOccupancy);
            return resultDataSet;


        }
        #endregion
        #region get Professional Analysis Report Detail
        /// <summary>
        /// get Professional Analysis Report Detail
        /// </summary>
        /// <param name="objReportsBO"></param>
        /// <returns>resultDataSet</returns>
        public DataSet getProfessionalAnalysisReportDetail(ReportsBO objReportsBO, PageSortBO objPageSortBo, ref int totalCount)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO reportDateParam = new ParameterBO("REPORTDATE", objReportsBO.ReportDate, DbType.Date);
            paramList.Add(reportDateParam);

            ParameterBO localAuthorityParam = new ParameterBO("LOCALAUTHORITY", objReportsBO.LocalAuthority, DbType.Int32);
            paramList.Add(localAuthorityParam);

            ParameterBO schemeParam = new ParameterBO("SCHEME", objReportsBO.Scheme, DbType.Int32);
            paramList.Add(schemeParam);

            ParameterBO postCodeParam = new ParameterBO("POSTCODE", objReportsBO.PostCode, DbType.String);
            paramList.Add(postCodeParam);

            ParameterBO patchParam = new ParameterBO("PATCH", objReportsBO.Patch, DbType.String);
            paramList.Add(patchParam);

            ParameterBO assetTypeParam = new ParameterBO("ASSETTYPE", objReportsBO.AssetType, DbType.Int32);
            paramList.Add(assetTypeParam);

            ParameterBO propertyTypeParam = new ParameterBO("PROPERTYTYPE", objReportsBO.PropertyType, DbType.Int32);
            paramList.Add(propertyTypeParam);

            ParameterBO bedsParam = new ParameterBO("BEDS", objReportsBO.Beds, DbType.String);
            paramList.Add(bedsParam);

            ParameterBO occupancyParam = new ParameterBO("OCCUPANCY", objReportsBO.Occupancy, DbType.String);
            paramList.Add(occupancyParam);

            ParameterBO statusParam = new ParameterBO("STATUS", objReportsBO.Status, DbType.Int32);
            paramList.Add(statusParam);

            ParameterBO subStatusParam = new ParameterBO("SUBSTATUS", objReportsBO.SubStatus, DbType.Int32);
            paramList.Add(subStatusParam);

            ParameterBO pageNumberParam = new ParameterBO("PageNumber", objPageSortBo.PageNumber, DbType.Int32);
            paramList.Add(pageNumberParam);

            ParameterBO pageSizeParam = new ParameterBO("PageSize", objPageSortBo.PageSize, DbType.Int32);
            paramList.Add(pageSizeParam);

            ParameterBO sortDirectionParam = new ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String);
            paramList.Add(sortDirectionParam);

            ParameterBO sortExpressionParam = new ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String);
            paramList.Add(sortExpressionParam);

            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);

            DataSet resultDataSet = new DataSet();
            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetProfessionalAnalysisReportDetail);
            ParameterBO obj = outParametersList.First();
            totalCount = Convert.ToInt32(obj.Value.ToString());
            return resultDataSet;


        }
        #endregion

        #region get Patch
        /// <summary>
        /// get patch
        /// </summary>
        /// <returns></returns>
        public DataSet getPatch()
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();
            DataSet resultDataSet = new DataSet();
            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetPatch);
            return resultDataSet;
        }
        #endregion

        #region get Local Authority
        /// <summary>
        /// get Local Authority
        /// </summary>
        /// <returns></returns>
        public DataSet getLocalAuthority()
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();
            DataSet resultDataSet = new DataSet();
            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetLocalAuthority);
            return resultDataSet;
        }
        #endregion

        #region get Development By Patch Id And LAId
        /// <summary>
        /// get Development By Patch Id And LAId
        /// </summary>
        /// <param name="patchId"></param>
        /// <param name="LAId"></param>
        /// <returns></returns>
        public DataSet getDevelopmentByPatchIdAndLAId(int? patchId, int? LAId)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();
            ParameterBO patchIdParam = new ParameterBO("patchId", patchId, DbType.Int32);
            paramList.Add(patchIdParam);

            ParameterBO localAuthorityParam = new ParameterBO("LAId", LAId, DbType.Int32);
            paramList.Add(localAuthorityParam);

            DataSet resultDataSet = new DataSet();
            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetDevelopmentByPatchIdAndLAId);
            return resultDataSet;
        }
        #endregion

        #region get Professional Analysis Report Detail for print
        /// <summary>
        /// get Professional Analysis Report Detail for print
        /// </summary>
        /// <param name="objReportsBO"></param>
        /// <returns>resultDataSet</returns>
        public DataSet getAnalysisReportDetailForPrint(ReportsBO objReportsBO)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO reportDateParam = new ParameterBO("REPORTDATE", objReportsBO.ReportDate, DbType.Date);
            paramList.Add(reportDateParam);

            ParameterBO localAuthorityParam = new ParameterBO("LOCALAUTHORITY", objReportsBO.LocalAuthority, DbType.Int32);
            paramList.Add(localAuthorityParam);

            ParameterBO schemeParam = new ParameterBO("SCHEME", objReportsBO.Scheme, DbType.Int32);
            paramList.Add(schemeParam);

            ParameterBO postCodeParam = new ParameterBO("POSTCODE", objReportsBO.PostCode, DbType.String);
            paramList.Add(postCodeParam);

            ParameterBO patchParam = new ParameterBO("PATCH", objReportsBO.Patch, DbType.String);
            paramList.Add(patchParam);

            ParameterBO assetTypeParam = new ParameterBO("ASSETTYPE", objReportsBO.AssetType, DbType.Int32);
            paramList.Add(assetTypeParam);

            ParameterBO propertyTypeParam = new ParameterBO("PROPERTYTYPE", objReportsBO.PropertyType, DbType.Int32);
            paramList.Add(propertyTypeParam);

            ParameterBO bedsParam = new ParameterBO("BEDS", objReportsBO.Beds, DbType.String);
            paramList.Add(bedsParam);

            ParameterBO occupancyParam = new ParameterBO("OCCUPANCY", objReportsBO.Occupancy, DbType.String);
            paramList.Add(occupancyParam);

            ParameterBO statusParam = new ParameterBO("STATUS", objReportsBO.Status, DbType.Int32);
            paramList.Add(statusParam);

            ParameterBO subStatusParam = new ParameterBO("SUBSTATUS", objReportsBO.SubStatus, DbType.Int32);
            paramList.Add(subStatusParam);



            DataSet resultDataSet = new DataSet();
            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetAnalysisReportDetailPrint);

            return resultDataSet;


        }
        #endregion

        #region get Void Analysis Report
        /// <summary>
        /// get Void Analysis Report 
        /// </summary>
        /// <param name="objReportsBO"></param>
        /// <returns>resultDataSet</returns>
        public DataSet getVoidAnalysisReport(ReportsBO objReportsBO)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO reportDateParam = new ParameterBO("REPORTDATE", objReportsBO.ReportDate, DbType.Date);
            paramList.Add(reportDateParam);

            ParameterBO localAuthorityParam = new ParameterBO("LOCALAUTHORITY", objReportsBO.LocalAuthority, DbType.Int32);
            paramList.Add(localAuthorityParam);

            ParameterBO schemeParam = new ParameterBO("SCHEME", objReportsBO.Scheme, DbType.Int32);
            paramList.Add(schemeParam);

            ParameterBO postCodeParam = new ParameterBO("POSTCODE", objReportsBO.PostCode, DbType.String);
            paramList.Add(postCodeParam);

            ParameterBO patchParam = new ParameterBO("PATCH", objReportsBO.Patch, DbType.String);
            paramList.Add(patchParam);

            ParameterBO assetTypeParam = new ParameterBO("ASSETTYPE", objReportsBO.AssetType, DbType.Int32);
            paramList.Add(assetTypeParam);

            ParameterBO statusParam = new ParameterBO("STATUS", objReportsBO.Status, DbType.Int32);
            paramList.Add(statusParam);

            DataSet resultDataSet = new DataSet();
            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetVoidAnalysisReport);
            return resultDataSet;


        }
        #endregion

        #region get Void Analysis Report Detail
        /// <summary>
        /// get Void Analysis Report Detail
        /// </summary>
        /// <param name="objReportsBO"></param>
        /// <returns>resultDataSet</returns>
        public DataSet getVoidAnalysisReportDetail(ReportsBO objReportsBO, PageSortBO objPageSortBo, ref int totalCount)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO reportDateParam = new ParameterBO("REPORTDATE", objReportsBO.ReportDate, DbType.Date);
            paramList.Add(reportDateParam);

            ParameterBO localAuthorityParam = new ParameterBO("LOCALAUTHORITY", objReportsBO.LocalAuthority, DbType.Int32);
            paramList.Add(localAuthorityParam);

            ParameterBO schemeParam = new ParameterBO("SCHEME", objReportsBO.Scheme, DbType.Int32);
            paramList.Add(schemeParam);

            ParameterBO postCodeParam = new ParameterBO("POSTCODE", objReportsBO.PostCode, DbType.String);
            paramList.Add(postCodeParam);

            ParameterBO patchParam = new ParameterBO("PATCH", objReportsBO.Patch, DbType.String);
            paramList.Add(patchParam);

            ParameterBO statusParam = new ParameterBO("STATUS", objReportsBO.Status, DbType.Int32);
            paramList.Add(statusParam);

            ParameterBO assetTypeParam = new ParameterBO("ASSETTYPE", objReportsBO.AssetType, DbType.Int32);
            paramList.Add(assetTypeParam);

            ParameterBO periodParam = new ParameterBO("PERIOD", objReportsBO.Period, DbType.Int32);
            paramList.Add(periodParam);

            ParameterBO pageNumberParam = new ParameterBO("PageNumber", objPageSortBo.PageNumber, DbType.Int32);
            paramList.Add(pageNumberParam);

            ParameterBO pageSizeParam = new ParameterBO("PageSize", objPageSortBo.PageSize, DbType.Int32);
            paramList.Add(pageSizeParam);

            ParameterBO sortDirectionParam = new ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String);
            paramList.Add(sortDirectionParam);

            ParameterBO sortExpressionParam = new ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String);
            paramList.Add(sortExpressionParam);

            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);

            DataSet resultDataSet = new DataSet();
            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetVoidAnalysisReportDetail);
            ParameterBO obj = outParametersList.First();
            totalCount = Convert.ToInt32(obj.Value.ToString());
            return resultDataSet;


        }
        #endregion

        #region get Void Analysis Report Detail for print
        /// <summary>
        /// get Void Analysis Report Detail for print
        /// </summary>
        /// <param name="objReportsBO"></param>
        /// <returns>resultDataSet</returns>
        public DataSet getVoidAnalysisReportDetailForPrint(ReportsBO objReportsBO)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO reportDateParam = new ParameterBO("REPORTDATE", objReportsBO.ReportDate, DbType.Date);
            paramList.Add(reportDateParam);

            ParameterBO localAuthorityParam = new ParameterBO("LOCALAUTHORITY", objReportsBO.LocalAuthority, DbType.Int32);
            paramList.Add(localAuthorityParam);

            ParameterBO schemeParam = new ParameterBO("SCHEME", objReportsBO.Scheme, DbType.Int32);
            paramList.Add(schemeParam);

            ParameterBO postCodeParam = new ParameterBO("POSTCODE", objReportsBO.PostCode, DbType.String);
            paramList.Add(postCodeParam);

            ParameterBO patchParam = new ParameterBO("PATCH", objReportsBO.Patch, DbType.String);
            paramList.Add(patchParam);

            ParameterBO statusParam = new ParameterBO("STATUS", objReportsBO.Status, DbType.Int32);
            paramList.Add(statusParam);

            ParameterBO assetTypeParam = new ParameterBO("ASSETTYPE", objReportsBO.AssetType, DbType.Int32);
            paramList.Add(assetTypeParam);

            ParameterBO periodParam = new ParameterBO("PERIOD", objReportsBO.Period, DbType.Int32);
            paramList.Add(periodParam);

            DataSet resultDataSet = new DataSet();
            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetVoidAnalysisReportDetailForPrint);

            return resultDataSet;


        }
        #endregion

        #region get Terrior Report Data
        /// <summary>
        /// get Terrior Report Data
        /// </summary>
        /// <param name="objReportsBO"></param>
        /// <returns>resultDataSet</returns>
        public DataSet getTerriorReportData(ReportsBO objReportsBO, PageSortBO objPageSortBo, ref int totalCount)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO isSoldCheckedParam = new ParameterBO("isSold", objReportsBO.IsSoldChecked, DbType.Boolean);
            paramList.Add(isSoldCheckedParam);

            ParameterBO schemeParam = new ParameterBO("SCHEMEID", objReportsBO.Scheme, DbType.Int32);
            paramList.Add(schemeParam);

            ParameterBO postCodeParam = new ParameterBO("SEARCHTEXT", objReportsBO.SearchText, DbType.String);
            paramList.Add(postCodeParam);


            ParameterBO pageNumberParam = new ParameterBO("PageNumber", objPageSortBo.PageNumber, DbType.Int32);
            paramList.Add(pageNumberParam);

            ParameterBO pageSizeParam = new ParameterBO("PageSize", objPageSortBo.PageSize, DbType.Int32);
            paramList.Add(pageSizeParam);

            ParameterBO sortDirectionParam = new ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String);
            paramList.Add(sortDirectionParam);

            ParameterBO sortExpressionParam = new ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String);
            paramList.Add(sortExpressionParam);

            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);

            DataSet resultDataSet = new DataSet();
            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetTerriorReportData);
            ParameterBO obj = outParametersList.First();
            totalCount = Convert.ToInt32(obj.Value.ToString());
            return resultDataSet;


        }
        public DataSet getTerriorReportDataForExcel(ReportsBO objReportsBO, PageSortBO objPageSortBo, ref int totalCount)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO isSoldCheckedParam = new ParameterBO("isSold", objReportsBO.IsSoldChecked, DbType.Boolean);
            paramList.Add(isSoldCheckedParam);

            ParameterBO schemeParam = new ParameterBO("SCHEMEID", objReportsBO.Scheme, DbType.Int32);
            paramList.Add(schemeParam);

            ParameterBO postCodeParam = new ParameterBO("SEARCHTEXT", objReportsBO.SearchText, DbType.String);
            paramList.Add(postCodeParam);


            ParameterBO pageNumberParam = new ParameterBO("PageNumber", objPageSortBo.PageNumber, DbType.Int32);
            paramList.Add(pageNumberParam);

            ParameterBO pageSizeParam = new ParameterBO("PageSize", objPageSortBo.PageSize, DbType.Int32);
            paramList.Add(pageSizeParam);

            ParameterBO sortDirectionParam = new ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String);
            paramList.Add(sortDirectionParam);

            ParameterBO sortExpressionParam = new ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String);
            paramList.Add(sortExpressionParam);

            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);

            DataSet resultDataSet = new DataSet();
            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetTerriorReportDataForExcel);
            ParameterBO obj = outParametersList.First();
            totalCount = Convert.ToInt32(obj.Value.ToString());
            return resultDataSet;


        }
        #endregion


        #region get CP12 Status Report
        /// <summary>
        /// get CP12 Status Report
        /// </summary>
        /// <param name="objReportsBO"></param>
        /// <returns>resultDataSet</returns>
        public int getCP12StatusReport(ref DataSet resultDataSet, PageSortBO objPageSortBO, string propertyStatus, string searchText)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO searchTextParam = new ParameterBO("searchText", searchText, DbType.String);
            paramList.Add(searchTextParam);

            ParameterBO propertyStatusParam = new ParameterBO("propertyStatus", propertyStatus, DbType.String);
            paramList.Add(propertyStatusParam);


            ParameterBO pageNumberParam = new ParameterBO("PageNumber", objPageSortBO.PageNumber, DbType.Int32);
            paramList.Add(pageNumberParam);

            ParameterBO pageSizeParam = new ParameterBO("PageSize", objPageSortBO.PageSize, DbType.Int32);
            paramList.Add(pageSizeParam);

            ParameterBO sortDirectionParam = new ParameterBO("sortOrder", objPageSortBO.SmallSortDirection, DbType.String);
            paramList.Add(sortDirectionParam);

            ParameterBO sortExpressionParam = new ParameterBO("sortColumn", objPageSortBO.SortExpression, DbType.String);
            paramList.Add(sortExpressionParam);

            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);

            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetCP12StatusReport);
            ParameterBO obj = outParametersList.First();
            int totalCount = Convert.ToInt32(obj.Value.ToString());
            return totalCount;

        }
        #endregion

        public void getCategories(ref DataSet resultset, string reportForType = "")
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO reportForParam = new ParameterBO("reportFor", reportForType, DbType.String);
            paramList.Add(reportForParam);

            LoadDataSet(ref resultset, ref paramList, ref outParametersList, SpNameConstants.PdrGetCategoriesList);
         
        }
        public int getDevDocuments(ref DataSet resultDataSet, PageSortBO objPageSortBO, string category, int type, string title, string dateType, string fromDate, string toDate, string uploadedOrNot, string searchedText, bool getOnlyCount = false)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO categoryParam = new ParameterBO("category", category, DbType.String);
            paramList.Add(categoryParam);

            ParameterBO typeParam = new ParameterBO("type", type, DbType.Int32);
            paramList.Add(typeParam);

            ParameterBO titleParam = new ParameterBO("title", title, DbType.String);
            paramList.Add(titleParam);

            ParameterBO dateTypeParam = new ParameterBO("dateType", dateType, DbType.String);
            paramList.Add(dateTypeParam);

            ParameterBO fromParam = new ParameterBO("from", fromDate, DbType.String);
            paramList.Add(fromParam);

            ParameterBO toParam = new ParameterBO("to", toDate, DbType.String);
            paramList.Add(toParam);

            ParameterBO uploadedOrNotParam = new ParameterBO("uploadedOrNot", uploadedOrNot, DbType.String);
            paramList.Add(uploadedOrNotParam);

            ParameterBO searchedTextParam = new ParameterBO("searchedText", searchedText, DbType.String);
            paramList.Add(searchedTextParam);

            ParameterBO pageNumberParam = new ParameterBO("PageNumber", objPageSortBO.PageNumber, DbType.Int32);
            paramList.Add(pageNumberParam);

            ParameterBO pageSizeParam = new ParameterBO("PageSize", objPageSortBO.PageSize, DbType.Int32);
            paramList.Add(pageSizeParam);

            ParameterBO SortDirectionParam = new ParameterBO("sortOrder", objPageSortBO.SmallSortDirection, DbType.String);
            paramList.Add(SortDirectionParam);

            ParameterBO SortExpressionParam = new ParameterBO("sortColumn", objPageSortBO.SortExpression, DbType.String);
            paramList.Add(SortExpressionParam);

            ParameterBO getCountParam = new ParameterBO("getOnlyCount", getOnlyCount, DbType.Boolean);
            paramList.Add(getCountParam);

            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);

            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetAllDevelopmentDocuments);

            ParameterBO obj = outParametersList.First();
            Int32 outvalue = Convert.ToInt32(obj.Value.ToString());

            return outvalue;


        }

        #region "Save Excluded Properties"

        public bool saveExcludedProperties(DataTable excludedPropDt, int schemeId, int orderItemId)
        {

            ParameterList inParamList = new ParameterList();
            ParameterList outParamList = new ParameterList();

            ParameterBO schemeIdParam = new ParameterBO("schemeId", schemeId, DbType.Int32);
            inParamList.Add(schemeIdParam);

            ParameterBO orderItemIdParam = new ParameterBO("orderItemId", orderItemId, DbType.Int32);
            inParamList.Add(orderItemIdParam);

            ParameterBO excludedPropertiesParam = new ParameterBO("excludedProperties", excludedPropDt, SqlDbType.Structured);
            inParamList.Add(excludedPropertiesParam);

            ParameterBO isSavedParam = new ParameterBO("isSaved", 0, DbType.Boolean);
            outParamList.Add(isSavedParam);

            outParamList = base.SelectRecord(ref inParamList, ref outParamList, SpNameConstants.SaveExcludedProperties);            

            bool isSaved = false;
            isSaved = Convert.ToBoolean(outParamList[0].Value);
            return isSaved;
        }

        #endregion

        public DataSet getServiceChargeSchemeBlocks(int schemeId, int orderItemId)
        {
            ParameterList parametersList = new ParameterList();

            ParameterBO schemeIdParam = new ParameterBO("filterSchemeId", schemeId, DbType.Int32);
            parametersList.Add(schemeIdParam);

            ParameterBO orderItemIdParam = new ParameterBO("orderItemId", orderItemId, DbType.Int32);
            parametersList.Add(orderItemIdParam);

            DataSet resultDataSet = new DataSet();

            DataTable dtBlocks = new DataTable();
            dtBlocks.TableName = "Blocks";
            resultDataSet.Tables.Add(dtBlocks);

            IDataReader iResultDataReader = base.SelectRecord(parametersList, SpNameConstants.GetServiceChargeSchemeAssociatedBlocks);
            resultDataSet.Load(iResultDataReader, LoadOption.OverwriteChanges, dtBlocks);
            iResultDataReader.Close();
            return resultDataSet;
        }

        #region "Get Associated Schemes and Blocks"
        public DataSet getServiceChargeAssociatedSchemes(int orderItemId)
        {
            ParameterList parameterList = new ParameterList();

            ParameterBO orderItemIdParam;
            orderItemIdParam = new ParameterBO("orderItemId", orderItemId, DbType.Int32);
            parameterList.Add(orderItemIdParam);

            DataSet resultDataSet = new DataSet();
            base.LoadDataSet(ref resultDataSet, ref parameterList, SpNameConstants.GetServiceChargeAssociatedSchemesBlocks);
            return resultDataSet;
        }
        #endregion

        #region "Get Service Charge Inc Exc Properties By Scheme"
        public DataSet getServiceChargeIncExcPropertiesByScheme(int schemeId, int orderItemId)
        {
            ParameterList parametersList = new ParameterList();

            ParameterBO schemeIdParam = new ParameterBO("filterSchemeId", schemeId, DbType.Int32);
            parametersList.Add(schemeIdParam);

            ParameterBO orderItemIdParam = new ParameterBO("orderItemId", orderItemId, DbType.Int32);
            parametersList.Add(orderItemIdParam);

            DataSet resultDataSet = new DataSet();

            DataTable dtProperty = new DataTable();
            dtProperty.TableName = "Properties";
            resultDataSet.Tables.Add(dtProperty);

            IDataReader iResultDataReader = base.SelectRecord(parametersList, SpNameConstants.GetIncExcPropertiesOfServiceCharge);
            resultDataSet.Load(iResultDataReader, LoadOption.OverwriteChanges, dtProperty);
            iResultDataReader.Close();
            return resultDataSet;
        }
        #endregion

        public int getSchemeDocuments(ref DataSet resultDataSet, PageSortBO objPageSortBO, string category, int type, int title, string dateType, string fromDate, string toDate, string uploadedOrNot, string searchedText, bool getOnlyCount = false)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO categoryParam = new ParameterBO("category", category, DbType.String);
            paramList.Add(categoryParam);

            ParameterBO typeParam = new ParameterBO("type", type, DbType.Int32);
            paramList.Add(typeParam);

            ParameterBO titleParam = new ParameterBO("title", title, DbType.String);
            paramList.Add(titleParam);

            ParameterBO dateTypeParam = new ParameterBO("dateType", dateType, DbType.String);
            paramList.Add(dateTypeParam);

            ParameterBO fromParam = new ParameterBO("from", fromDate, DbType.String);
            paramList.Add(fromParam);

            ParameterBO toParam = new ParameterBO("to", toDate, DbType.String);
            paramList.Add(toParam);

            ParameterBO uploadedOrNotParam = new ParameterBO("uploadedOrNot", uploadedOrNot, DbType.String);
            paramList.Add(uploadedOrNotParam);

            ParameterBO searchedTextParam = new ParameterBO("searchedText", searchedText, DbType.String);
            paramList.Add(searchedTextParam);

            ParameterBO pageNumberParam = new ParameterBO("PageNumber", objPageSortBO.PageNumber, DbType.Int32);
            paramList.Add(pageNumberParam);

            ParameterBO pageSizeParam = new ParameterBO("PageSize", objPageSortBO.PageSize, DbType.Int32);
            paramList.Add(pageSizeParam);

            ParameterBO SortDirectionParam = new ParameterBO("sortOrder", objPageSortBO.SmallSortDirection, DbType.String);
            paramList.Add(SortDirectionParam);

            ParameterBO SortExpressionParam = new ParameterBO("sortColumn", objPageSortBO.SortExpression, DbType.String);
            paramList.Add(SortExpressionParam);

            ParameterBO getCountParam = new ParameterBO("getOnlyCount", getOnlyCount, DbType.Boolean);
            paramList.Add(getCountParam);

            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);

            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetAllSchemeDocuments);

            ParameterBO obj = outParametersList.First();
            Int32 outvalue = Convert.ToInt32(obj.Value.ToString());

            return outvalue;
        }

        public int getPropDocuments(ref DataSet resultDataSet, PageSortBO objPageSortBO, string category, int type, int title, string dateType, string fromDate, string toDate, string uploadedOrNot, string searchedText, bool getOnlyCount = false)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO categoryParam = new ParameterBO("category", category, DbType.String);
            paramList.Add(categoryParam);

            ParameterBO typeParam = new ParameterBO("type", type, DbType.Int32);
            paramList.Add(typeParam);

            ParameterBO titleParam = new ParameterBO("title", title, DbType.String);
            paramList.Add(titleParam);

            ParameterBO dateTypeParam = new ParameterBO("dateType", dateType, DbType.String);
            paramList.Add(dateTypeParam);

            ParameterBO fromParam = new ParameterBO("from", fromDate, DbType.String);
            paramList.Add(fromParam);

            ParameterBO toParam = new ParameterBO("to", toDate, DbType.String);
            paramList.Add(toParam);

            ParameterBO uploadedOrNotParam = new ParameterBO("uploadedOrNot", uploadedOrNot, DbType.String);
            paramList.Add(uploadedOrNotParam);

            ParameterBO searchedTextParam = new ParameterBO("searchedText", searchedText, DbType.String);
            paramList.Add(searchedTextParam);

            ParameterBO pageNumberParam = new ParameterBO("PageNumber", objPageSortBO.PageNumber, DbType.Int32);
            paramList.Add(pageNumberParam);

            ParameterBO pageSizeParam = new ParameterBO("PageSize", objPageSortBO.PageSize, DbType.Int32);
            paramList.Add(pageSizeParam);

            ParameterBO SortDirectionParam = new ParameterBO("sortOrder", objPageSortBO.SmallSortDirection, DbType.String);
            paramList.Add(SortDirectionParam);

            ParameterBO SortExpressionParam = new ParameterBO("sortColumn", objPageSortBO.SortExpression, DbType.String);
            paramList.Add(SortExpressionParam);

            ParameterBO getCountParam = new ParameterBO("getOnlyCount", getOnlyCount, DbType.Boolean);
            paramList.Add(getCountParam);

            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);

            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetAllPropertyDocuments);

            ParameterBO obj = outParametersList.First();
            Int32 outvalue = Convert.ToInt32(obj.Value.ToString());

            return outvalue;
        }

        public DataSet getDocumentsEmployees()
        {

            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();
            DataSet resultDataSet = new DataSet();
            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetDocumentsEmployees);
            return resultDataSet;
        }

        #region Get Income Recovery Officer Data
        public void GetIncomeRecoveryOfficer(ref DataSet resultDataSet, string tenancyId)
        {
            ParameterList paramList = new ParameterList();

            ParameterBO tenancyIdParam = new ParameterBO("TENANCYID", tenancyId, DbType.String);
            paramList.Add(tenancyIdParam);

            LoadDataSet(ref resultDataSet, ref paramList, SpNameConstants.GetIncomeRecoveryOfficer);
        }
        #endregion

        #region Get Customer Account Data
        public void GetCustomerAccountData(ref DataSet resultDataSet, string tenancyId)
        {
            ParameterList paramList = new ParameterList();

            ParameterBO tenancyIdParam = new ParameterBO("TENANCYID", tenancyId, DbType.String);
            paramList.Add(tenancyIdParam);

            LoadDataSet(ref resultDataSet, ref paramList, SpNameConstants.GetCustomerAccountDesc);
        }
        #endregion

        #region Get Customer Info
        public void GetCustomerInfo(ref DataSet resultDataSet, int tenancyId)
        {
            ParameterList paramList = new ParameterList();

            ParameterBO tenancyIdParam = new ParameterBO("tenantId", tenancyId, DbType.Int32);
            paramList.Add(tenancyIdParam);

            DataTable customerInfoTable = new DataTable("CustomerInfo");
            DataTable customerNameTable = new DataTable("CustomerName");
            resultDataSet.Tables.Add(customerInfoTable);
            resultDataSet.Tables.Add(customerNameTable);

            IDataReader iResultDataReader = base.SelectRecord(paramList, SpNameConstants.GetCustomerInformation);
            resultDataSet.Load(iResultDataReader, LoadOption.OverwriteChanges, customerInfoTable);
            resultDataSet.Load(iResultDataReader, LoadOption.OverwriteChanges, customerNameTable);
            iResultDataReader.Close();

            //LoadDataSet(ref resultDataSet, ref paramList, SpNameConstants.GetCustomerInformation);
        }
        #endregion

        #region Get Customer Last Payment
        public void GetCustomerLastPayment(ref DataSet resultDataSet, string customerId, string tenancyId)
        {
            ParameterList paramList = new ParameterList();

            ParameterBO tenancyIdParam = new ParameterBO("tenantId", tenancyId, DbType.String);
            paramList.Add(tenancyIdParam);

            ParameterBO customerIdParam = new ParameterBO("customerId", customerId, DbType.String);
            paramList.Add(customerIdParam);

            LoadDataSet(ref resultDataSet, ref paramList, SpNameConstants.GetCustomerLastPayment);
        }
        #endregion

        #region Get Customer Account Recharge
        public void GetCustomerAccountRecharge(ref DataSet resultDataSet, string tenancyId)
        {
            ParameterList paramList = new ParameterList();

            ParameterBO tenancyIdParam = new ParameterBO("TENANCYID", tenancyId, DbType.String);
            paramList.Add(tenancyIdParam);

            LoadDataSet(ref resultDataSet, ref paramList, SpNameConstants.GetCustomerAccountRecharges);
        }
        #endregion
        #region Get Customer Account Recharge
        public void getRentLetterMultiPrint(ref DataSet resultDataSet, string tenancyIds, int printAction)
        {
            ParameterList paramList = new ParameterList();

            ParameterBO tenancyIdParam = new ParameterBO("tenancyIds", tenancyIds, DbType.String);
            paramList.Add(tenancyIdParam);

            ParameterBO printActionParam = new ParameterBO("PrintAction", printAction, DbType.String);
            paramList.Add(printActionParam);

            LoadDataSet(ref resultDataSet, ref paramList, SpNameConstants.GetRentLetterMultiPrint);
        }
        #endregion
    }
}
