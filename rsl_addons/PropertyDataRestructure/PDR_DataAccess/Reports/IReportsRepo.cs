﻿using System.Data;
using PDR_BusinessObject.Reports;
using PDR_BusinessObject.PageSort;

namespace PDR_DataAccess.Reports
{
    public interface IReportsRepo
    {
        #region get Professional Analysis Report
        /// <summary>
        /// get Professional Analysis Report
        /// </summary>
        /// <param name="objReportsBO"></param>
        /// <returns>resultDataSet</returns>
        DataSet getProfessionalAnalysisReport(ReportsBO objReportsBO);
        #endregion

        #region get Professional Analysis Report Single Asset Type
        /// <summary>
        /// get Professional Analysis Report Single Asset Type
        /// </summary>
        /// <param name="objReportsBO"></param>
        /// <returns>resultDataSet</returns>
        DataSet getProfessionalAnalysisReportSingleAssetType(ReportsBO objReportsBO);
        #endregion

        #region get Professional Analysis Report Single Asset Type With Occupancy
        /// <summary>
        /// get Professional Analysis Report Single Asset Type With Occupancy
        /// </summary>
        /// <param name="objReportsBO"></param>
        /// <returns>resultDataSet</returns>
        DataSet getProfessionalAnalysisReportWithOccupancy(ReportsBO objReportsBO);
        #endregion
        #region get Professional Analysis Report Single Asset Type With Occupancy
        /// <summary>
        /// get Professional Analysis Report Single Asset Type With Occupancy
        /// </summary>
        /// <param name="objReportsBO"></param>
        /// <returns>resultDataSet</returns>
        DataSet getProfessionalAnalysisReportWithOutOccupancy(ReportsBO objReportsBO);
        #endregion

        #region get Professional Analysis Report Detail
        /// <summary>
        /// get Professional Analysis Report Detail
        /// </summary>
        /// <param name="objReportsBO"></param>
        /// <returns>resultDataSet</returns>
        DataSet getProfessionalAnalysisReportDetail(ReportsBO objReportsBO, PageSortBO objPageSortBo, ref int totalCount);
        #endregion

        #region get Patch
        /// <summary>
        /// Get the patch from database
        /// </summary>
        /// <returns></returns>
        DataSet getPatch();
        #endregion

        #region get Local Authority
        /// <summary>
        /// Get local authority
        /// </summary>
        /// <returns></returns>
        DataSet getLocalAuthority();
        #endregion

        #region get Development By Patch Id And LAId
        /// <summary>
        /// get Development By Patch Id And LAId
        /// </summary>
        /// <param name="patchId"></param>
        /// <param name="LAId"></param>
        /// <returns></returns>
        DataSet getDevelopmentByPatchIdAndLAId(int? patchId, int? LAId);
        #endregion

        #region get Professional Analysis Report Detail for print
        /// <summary>
        /// get Professional Analysis Report Detail for print
        /// </summary>
        /// <param name="objReportsBO"></param>
        /// <returns>resultDataSet</returns>
        DataSet getAnalysisReportDetailForPrint(ReportsBO objReportsBO);
        #endregion

        #region get Void Analysis Report
        /// <summary>
        /// get Void Analysis Report 
        /// </summary>
        /// <param name="objReportsBO"></param>
        /// <returns>resultDataSet</returns>
        DataSet getVoidAnalysisReport(ReportsBO objReportsBO);
        #endregion


        #region get CP12 Status Report
        /// <summary>
        /// get CP12 Status Report
        /// </summary>
        /// <param name="objReportsBO"></param>
        /// <returns>resultDataSet</returns>
        int getCP12StatusReport(ref DataSet resultDataSet, PageSortBO objPageSortBO, string propertyStatus, string searchedText);
        #endregion

        #region get Void Analysis Report Detail
        /// <summary>
        /// get Void Analysis Report Detail
        /// </summary>
        /// <param name="objReportsBO"></param>
        /// <returns>resultDataSet</returns>
        DataSet getVoidAnalysisReportDetail(ReportsBO objReportsBO, PageSortBO objPageSortBo, ref int totalCount);
        #endregion

        #region get Void Analysis Report Detail for print
        /// <summary>
        /// get Void Analysis Report Detail for print
        /// </summary>
        /// <param name="objReportsBO"></param>
        /// <returns>resultDataSet</returns>
        DataSet getVoidAnalysisReportDetailForPrint(ReportsBO objReportsBO);
        #endregion

        #region get Terrior Report Data
        /// <summary>
        /// get Terrior Report Data
        /// </summary>
        /// <param name="objReportsBO"></param>
        /// <returns>resultDataSet</returns>
        DataSet getTerriorReportData(ReportsBO objReportsBO, PageSortBO objPageSortBo, ref int totalCount);
        DataSet getTerriorReportDataForExcel(ReportsBO objReportsBO, PageSortBO objPageSortBo, ref int totalCount);
        #endregion

        int getDevDocuments(ref DataSet resultDataSet, PageSortBO objPageSortBO, string category, int type, string title, string dateType, string fromDate, string toDate, string uploadedOrNot, string searchedText, bool getOnlyCount = false);
        int getSchemeDocuments(ref DataSet resultDataSet, PageSortBO objPageSortBO, string category, int type, int title, string dateType, string fromDate, string toDate, string uploadedOrNot, string searchedText, bool getOnlyCount = false);
        int getPropDocuments(ref DataSet resultDataSet, PageSortBO objPageSortBO, string category, int type, int title, string dateType, string fromDate, string toDate, string uploadedOrNot, string searchedText, bool getOnlyCount = false);

        #region Rent Account Summary

        
        void GetIncomeRecoveryOfficer(ref DataSet resultDataSet, string tenancyId);
        void GetCustomerInfo(ref DataSet resultDataSet, int tenancyId);
        void GetCustomerLastPayment(ref DataSet resultDataSet, string customerId, string tenancyId);
        void GetCustomerAccountData(ref DataSet resultDataSet, string tenancyId);
        void GetCustomerAccountRecharge(ref DataSet resultDataSet, string tenancyId);
        void getCategories(ref DataSet resultDataSet, string reportFor);

        void getRentLetterMultiPrint(ref DataSet resultDataSet, string tenancyIds, int printAction);
        #endregion

        #region "Reports"
        bool saveExcludedProperties(DataTable excludedPropDt, int schemeId, int orderItemId);
        DataSet getServiceChargeSchemeBlocks(int schemeId, int orderItemId);
        DataSet getServiceChargeAssociatedSchemes(int orderItemId);
        DataSet getServiceChargeIncExcPropertiesByScheme(int schemeId, int orderItemId); 
        #endregion


    }
}
