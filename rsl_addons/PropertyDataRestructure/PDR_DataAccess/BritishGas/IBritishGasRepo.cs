﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using PDR_BusinessObject.PageSort;
using PDR_BusinessObject.BritishGas;

namespace PDR_DataAccess.BritishGas
{
    public interface IBritishGasRepo
    {
        #region get British Gas Notification List
        /// <summary>
        /// get British Gas In Progress List
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        Int32 getBritishGasNotificationList(ref DataSet resultDataSet, PageSortBO objPageSortBO, BritishGasSearchBo objSearchBo);
        #endregion

        /// <summary>
        /// This function would be used to return the stage1 notification data.
        /// </summary>
        /// <param name="britishGasBo"></param>
        void getBritishGasStage1Data(ref BritishGasNotifcationBO britishGasBo);

        /// <summary>
        /// This function would get the british gas notification data of stage 2
        /// </summary>
        /// <param name="britishGasBo"></param>
        void getBritishGasStage2AndStage3Data(ref BritishGasNotifcationBO britishGasBo);

        /// <summary>
        /// This fuction would get the meter types
        /// </summary>
        /// <param name="resultDs"></param>
        void getMeterTypes(ref DataSet resultDataSet);

        /// <summary>
        /// This function would be used to save british gas notification data.
        /// </summary>
        /// <param name="britishGasBo"></param>
        void saveBritishGasData(ref BritishGasNotifcationBO britishGasBo);

        /// <summary>
        /// This function would be used to update british gas stage 1 notification data.
        /// </summary>
        /// <param name="britishGasBo"></param>
        bool updateBritishGasStage1Data(ref BritishGasNotifcationBO britishGasBo);

        /// <summary>
        /// This function would be used to update british gas notification data for stage2
        /// </summary>
        /// <param name="britishGasBo"></param>
        /// <returns></returns>
        bool updateBritishGasStage2Data(ref BritishGasNotifcationBO britishGasBo);

        /// <summary>
        /// This function would be used to update british gas notification data for stage3
        /// </summary>
        /// <param name="britishGasBo"></param>
        /// <returns></returns>
        bool updateBritishGasStage3Data(ref BritishGasNotifcationBO britishGasBo);

        /// <summary>
        /// This function would be used to Get  british Gas Void Notification  PDF Document Info
        /// </summary>
        /// <param name="britishGasVoidNotificationId"></param>
        /// <returns></returns>
        DataSet getVoidPDFDocumentInfo(int britishGasVoidNotificationId);
    }
}
