﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PDR_DataAccess.Base;
using PDR_BusinessObject.Parameter;
using System.Data;
using PDR_Utilities.Constants;
using PDR_BusinessObject.PageSort;
using PDR_BusinessObject.BritishGas;

namespace PDR_DataAccess.BritishGas
{
    public class BritishGasRepo : BaseDAL, IBritishGasRepo
    {
        #region get British Gas Notification List
        /// <summary>
        /// get British Gas In Progress List
        /// Currently for inProgress and Complete list is using the same code and procedure   
        /// because the screen data is same except the status. If there is some change in tables or join or some big change then 
        /// please consider creating the separate SP and function for Inporgress and complete list of british gas notification.												   												   
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        public int getBritishGasNotificationList(ref DataSet resultDataSet, PageSortBO objPageSortBO, BritishGasSearchBo objSearchBo)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO searchTextParam = new ParameterBO("searchText", objSearchBo.searchText, DbType.String);
            paramList.Add(searchTextParam);

            ParameterBO notificationStatus = new ParameterBO("notificationStatus", objSearchBo.notificationStatus, DbType.String);
            paramList.Add(notificationStatus);

            ParameterBO pageNumberParam = new ParameterBO("pageNumber", objPageSortBO.PageNumber, DbType.Int32);
            paramList.Add(pageNumberParam);

            ParameterBO pageSizeParam = new ParameterBO("pageSize", objPageSortBO.PageSize, DbType.Int32);
            paramList.Add(pageSizeParam);

            ParameterBO SortDirectionParam = new ParameterBO("sortOrder", objPageSortBO.SmallSortDirection, DbType.String);
            paramList.Add(SortDirectionParam);

            ParameterBO SortExpressionParam = new ParameterBO("sortColumn", objPageSortBO.SortExpression, DbType.String);
            paramList.Add(SortExpressionParam);

            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);

            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetBritishGasNotificationList);

            ParameterBO obj = outParametersList.First();
            Int32 outvalue = Convert.ToInt32(obj.Value.ToString());

            return outvalue;

        }
        #endregion

        #region Get British Gas Stage 1 Data
        /// <summary>
        /// get the british gas notification data from database
        /// </summary>
        /// <param name="britishGasBo"></param>
        public void getBritishGasStage1Data(ref BritishGasNotifcationBO britishGasBo)
        {
            ParameterList inParamList = new ParameterList();

            ParameterBO propertyIdparam = new ParameterBO("propertyId", britishGasBo.PropertyId, DbType.String);
            inParamList.Add(propertyIdparam);

            ParameterBO customerIdparam = new ParameterBO("customerId", britishGasBo.CustomerId, DbType.Int32);
            inParamList.Add(customerIdparam);

            ParameterBO tenancyIdparam = new ParameterBO("tenancyId", britishGasBo.TenancyId, DbType.Int32);
            inParamList.Add(tenancyIdparam);

            IDataReader myDataReader = base.SelectRecord(inParamList, SpNameConstants.GetBritishGasStage1Data);

            //'Iterate the dataReader

            while ((myDataReader.Read()))
            {
                if (!myDataReader.IsDBNull(myDataReader.GetOrdinal("IsGasElectricCheck")))
                {
                    britishGasBo.IsGasElectricCheck = Convert.ToBoolean(myDataReader.GetInt32 (myDataReader.GetOrdinal("IsGasElectricCheck")));
                }

                if (!myDataReader.IsDBNull(myDataReader.GetOrdinal("Address1")))
                {
                    britishGasBo.PropertyAddress1 = myDataReader.GetString(myDataReader.GetOrdinal("Address1"));
                }

                if (!myDataReader.IsDBNull(myDataReader.GetOrdinal("Address2")))
                {
                    britishGasBo.PropertyAddress2 = myDataReader.GetString(myDataReader.GetOrdinal("Address2"));
                }

                if (!myDataReader.IsDBNull(myDataReader.GetOrdinal("City")))
                {
                    britishGasBo.PropertyCity = myDataReader.GetString(myDataReader.GetOrdinal("City"));
                }

                if (!myDataReader.IsDBNull(myDataReader.GetOrdinal("County")))
                {
                    britishGasBo.PropertyCounty = myDataReader.GetString(myDataReader.GetOrdinal("County"));
                }

                if (!myDataReader.IsDBNull(myDataReader.GetOrdinal("PostCode")))
                {
                    britishGasBo.PropertyPostCode = myDataReader.GetString(myDataReader.GetOrdinal("PostCode"));
                }

                if (!myDataReader.IsDBNull(myDataReader.GetOrdinal("TenantName")))
                {
                    britishGasBo.CurrentTenantName = myDataReader.GetString(myDataReader.GetOrdinal("TenantName"));
                }

                if (!myDataReader.IsDBNull(myDataReader.GetOrdinal("TenantFwAddress1")))
                {
                    britishGasBo.TenantFwAddress1 = myDataReader.GetString(myDataReader.GetOrdinal("TenantFwAddress1"));
                }

                if (!myDataReader.IsDBNull(myDataReader.GetOrdinal("TenantFwAddress2")))
                {
                    britishGasBo.TenantFwAddress2 = myDataReader.GetString(myDataReader.GetOrdinal("TenantFwAddress2"));
                }

                if (!myDataReader.IsDBNull(myDataReader.GetOrdinal("TenantFwCity")))
                {
                    britishGasBo.TenantFwCity = myDataReader.GetString(myDataReader.GetOrdinal("TenantFwCity"));
                }

                if (!myDataReader.IsDBNull(myDataReader.GetOrdinal("TenantFwPostCode")))
                {
                    britishGasBo.TenantFwPostCode = myDataReader.GetString(myDataReader.GetOrdinal("TenantFwPostCode"));
                }

                if (!myDataReader.IsDBNull(myDataReader.GetOrdinal("DateOccupancyCease")))
                {
                    britishGasBo.OccupancyCeaseDate = myDataReader.GetDateTime(myDataReader.GetOrdinal("DateOccupancyCease"));
                }
            }
            myDataReader.Close();
        }
        #endregion

        #region Get British Gas Stage 2 And Stage 3 Data
        /// <summary>
        /// get the british gas notification stage 2 data from database
        /// </summary>
        /// <param name="britishGasBo"></param>
        public void getBritishGasStage2AndStage3Data(ref BritishGasNotifcationBO britishGasBo)
        {
            ParameterList inParamList = new ParameterList();

            ParameterBO propertyIdparam = new ParameterBO("britishGasVoidNotificationId", britishGasBo.BritishGasNotificationId, DbType.Int32);
            inParamList.Add(propertyIdparam);

            IDataReader myDataReader = base.SelectRecord(inParamList, SpNameConstants.GetBritishGasStage2AndStage3Data);

            //'Iterate the dataReader

            while ((myDataReader.Read()))
            {
                if (!myDataReader.IsDBNull(myDataReader.GetOrdinal("BritishGasVoidNotificationId")))
                {
                    britishGasBo.BritishGasNotificationId = myDataReader.GetInt32(myDataReader.GetOrdinal("BritishGasVoidNotificationId"));
                }

                if (!myDataReader.IsDBNull(myDataReader.GetOrdinal("GasMeterTypeId")))
                {
                    britishGasBo.GasMeterTypeId = myDataReader.GetInt32(myDataReader.GetOrdinal("GasMeterTypeId"));
                }

                if (!myDataReader.IsDBNull(myDataReader.GetOrdinal("GasMeterReading")))
                {
                    britishGasBo.GasMeterReading = myDataReader.GetInt64(myDataReader.GetOrdinal("GasMeterReading"));
                }

                if (!myDataReader.IsDBNull(myDataReader.GetOrdinal("GasMeterReadingDate")))
                {
                    britishGasBo.GasMeterReadingDate = myDataReader.GetDateTime(myDataReader.GetOrdinal("GasMeterReadingDate"));
                }

                if (!myDataReader.IsDBNull(myDataReader.GetOrdinal("ElectricMeterTypeId")))
                {
                    britishGasBo.ElectricMeterTypeId = myDataReader.GetInt32(myDataReader.GetOrdinal("ElectricMeterTypeId"));
                }

                if (!myDataReader.IsDBNull(myDataReader.GetOrdinal("ElectricMeterReading")))
                {
                    britishGasBo.ElectricMeterReading = myDataReader.GetInt64(myDataReader.GetOrdinal("ElectricMeterReading"));
                }

                if (!myDataReader.IsDBNull(myDataReader.GetOrdinal("ElectricMeterReadingDate")))
                {
                    britishGasBo.ElectricMeterReadingDate = myDataReader.GetDateTime(myDataReader.GetOrdinal("ElectricMeterReadingDate"));
                }

                if (!myDataReader.IsDBNull(myDataReader.GetOrdinal("GasDebtAmount")))
                {
                    britishGasBo.GasDebtAmount = myDataReader.GetDecimal(myDataReader.GetOrdinal("GasDebtAmount"));
                }

                if (!myDataReader.IsDBNull(myDataReader.GetOrdinal("GasDebtAmountDate")))
                {
                    britishGasBo.GasDebtAmountDate = myDataReader.GetDateTime(myDataReader.GetOrdinal("GasDebtAmountDate"));
                }

                if (!myDataReader.IsDBNull(myDataReader.GetOrdinal("ElectricDebtAmount")))
                {
                    britishGasBo.ElectricDebtAmount = myDataReader.GetDecimal(myDataReader.GetOrdinal("ElectricDebtAmount"));
                }

                if (!myDataReader.IsDBNull(myDataReader.GetOrdinal("ElectricDebtAmountDate")))
                {
                    britishGasBo.ElectricDebtAmountDate = myDataReader.GetDateTime(myDataReader.GetOrdinal("ElectricDebtAmountDate"));
                }

                if (!myDataReader.IsDBNull(myDataReader.GetOrdinal("NewTenantName")))
                {
                    britishGasBo.NewTenantName = myDataReader.GetString(myDataReader.GetOrdinal("NewTenantName"));
                }

                if (!myDataReader.IsDBNull(myDataReader.GetOrdinal("NewTenantDateOfBirth")))
                {
                    britishGasBo.NewTenantDateOfBirth = myDataReader.GetDateTime(myDataReader.GetOrdinal("NewTenantDateOfBirth"));
                }

                if (!myDataReader.IsDBNull(myDataReader.GetOrdinal("NewTenantTel")))
                {
                    britishGasBo.NewTenantTel = myDataReader.GetString(myDataReader.GetOrdinal("NewTenantTel"));
                }

                if (!myDataReader.IsDBNull(myDataReader.GetOrdinal("NewTenantMobile")))
                {
                    britishGasBo.NewTenantMobile = myDataReader.GetString(myDataReader.GetOrdinal("NewTenantMobile"));
                }

                if (!myDataReader.IsDBNull(myDataReader.GetOrdinal("NewTenantOccupancyDate")))
                {
                    britishGasBo.NewTenantOccupancyDate = myDataReader.GetDateTime(myDataReader.GetOrdinal("NewTenantOccupancyDate"));
                }

                if (!myDataReader.IsDBNull(myDataReader.GetOrdinal("NewTenantPreviousAddress1")))
                {
                    britishGasBo.NewTenantPreviousAddress1 = myDataReader.GetString(myDataReader.GetOrdinal("NewTenantPreviousAddress1"));
                }

                if (!myDataReader.IsDBNull(myDataReader.GetOrdinal("NewTenantPreviousAddress2")))
                {
                    britishGasBo.NewTenantPreviousAddress2 = myDataReader.GetString(myDataReader.GetOrdinal("NewTenantPreviousAddress2"));
                }

                if (!myDataReader.IsDBNull(myDataReader.GetOrdinal("NewTenantPreviousPostCode")))
                {
                    britishGasBo.NewTenantPreviousPostCode = myDataReader.GetString(myDataReader.GetOrdinal("NewTenantPreviousPostCode"));
                }

                if (!myDataReader.IsDBNull(myDataReader.GetOrdinal("NewTenantPreviousTownCity")))
                {
                    britishGasBo.NewTenantPreviousTownCity = myDataReader.GetString(myDataReader.GetOrdinal("NewTenantPreviousTownCity"));
                }

                if (!myDataReader.IsDBNull(myDataReader.GetOrdinal("NewGasMeterReading")))
                {
                    britishGasBo.NewGasMeterReading = myDataReader.GetInt64(myDataReader.GetOrdinal("NewGasMeterReading"));
                }

                if (!myDataReader.IsDBNull(myDataReader.GetOrdinal("NewGasMeterReadingDate")))
                {
                    britishGasBo.NewGasMeterReadingDate = myDataReader.GetDateTime(myDataReader.GetOrdinal("NewGasMeterReadingDate"));
                }

                if (!myDataReader.IsDBNull(myDataReader.GetOrdinal("NewElectricMeterReading")))
                {
                    britishGasBo.NewElectricMeterReading = myDataReader.GetInt64(myDataReader.GetOrdinal("NewElectricMeterReading"));
                }

                if (!myDataReader.IsDBNull(myDataReader.GetOrdinal("NewElectricMeterReadingDate")))
                {
                    britishGasBo.NewElectricMeterReadingDate = myDataReader.GetDateTime(myDataReader.GetOrdinal("NewElectricMeterReadingDate"));
                }
            }
            myDataReader.Close();
        }
        #endregion

        #region get Meter Types
        /// <summary>
        /// This function would populate the dataset of gas meter types
        /// </summary>
        /// <param name="resultDataSet"></param>
        public void getMeterTypes(ref DataSet resultDataSet)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            DataTable gasMeterTypesTable = new DataTable("GasMeterTypes");
            DataTable electricMeterTypesTable = new DataTable("ElectricMeterTypes");
            resultDataSet.Tables.Add(gasMeterTypesTable);
            resultDataSet.Tables.Add(electricMeterTypesTable);

            IDataReader iResultDataReader = base.SelectRecord(paramList, SpNameConstants.GetMeterTypes);
            resultDataSet.Load(iResultDataReader, LoadOption.OverwriteChanges, gasMeterTypesTable);
            resultDataSet.Load(iResultDataReader, LoadOption.OverwriteChanges, electricMeterTypesTable);
            iResultDataReader.Close();

            //LoadDataSet(ref resultDataSet, ref paramList, SpNameConstants.GetMeterTypes);
        }
        #endregion

        #region save British Gas Data
        /// <summary>
        /// save British Gas Data
        /// </summary>
        /// <param name="objPaintPacksBo"></param>
        public void saveBritishGasData(ref BritishGasNotifcationBO britishGasBo)
        {
            ParameterList parameterList = new ParameterList();
            ParameterList outparameterList = new ParameterList();


            ParameterBO customerIdParam = new ParameterBO("customerId", britishGasBo.CustomerId, DbType.Int32);
            parameterList.Add(customerIdParam);

            ParameterBO propertyIdParam = new ParameterBO("propertyId", britishGasBo.PropertyId, DbType.String);
            parameterList.Add(propertyIdParam);

            ParameterBO tenancyIdParam = new ParameterBO("tenancyId", britishGasBo.TenancyId, DbType.Int32);
            parameterList.Add(tenancyIdParam);

            ParameterBO isNotificationSendLaterParam = new ParameterBO("isNotificationSendLater ", britishGasBo.SendNotificationLater, DbType.Boolean);
            parameterList.Add(isNotificationSendLaterParam);

            ParameterBO isSavedParam = new ParameterBO("isSaved", 0, DbType.Boolean);
            outparameterList.Add(isSavedParam);

            ParameterBO britishGasIdParam = new ParameterBO("britishGasId", 0, DbType.Int32);
            outparameterList.Add(britishGasIdParam);

            ParameterBO britishGasStageIdParam = new ParameterBO("britishGasStageId", 0, DbType.Int32);
            outparameterList.Add(britishGasStageIdParam);

            ParameterBO britishGasStageParam = new ParameterBO("britishGasStage", string.Empty, DbType.String);
            outparameterList.Add(britishGasStageParam);

            SaveRecord(ref parameterList, ref outparameterList, SpNameConstants.SaveBritishGasData);

            ParameterBO obj = outparameterList.First();

            if (Convert.ToBoolean(obj.Value) == true)
            {
                foreach (ParameterBO pbo in outparameterList)
                {
                    if (pbo.Name == "britishGasId")
                    {
                        britishGasBo.BritishGasNotificationId = Convert.ToInt32(pbo.Value.ToString());
                    }
                    else if (pbo.Name == "britishGasStageId")
                    {
                        britishGasBo.StageId = Convert.ToInt32(pbo.Value.ToString());
                    }
                    else if (pbo.Name == "britishGasStage")
                    {
                        britishGasBo.StageName = pbo.Value.ToString();
                    }
                }
            }

        }
        #endregion

        #region update British Gas Stage1 Data
        /// <summary>
        /// 
        /// </summary>
        /// <param name="britishGasBo"></param>
        /// <returns></returns>
        public bool updateBritishGasStage1Data(ref BritishGasNotifcationBO britishGasBo)
        {
            ParameterList parameterList = new ParameterList();
            ParameterList outparameterList = new ParameterList();

            ParameterBO britishGasIdParam = new ParameterBO("britishGasId", britishGasBo.BritishGasNotificationId, DbType.Int32);
            parameterList.Add(britishGasIdParam);

            ParameterBO customerIdParam = new ParameterBO("customerId", britishGasBo.CustomerId, DbType.Int32);
            parameterList.Add(customerIdParam);

            ParameterBO propertyIdParam = new ParameterBO("propertyId", britishGasBo.PropertyId, DbType.String);
            parameterList.Add(propertyIdParam);

            ParameterBO tenancyIdParam = new ParameterBO("tenancyId", britishGasBo.TenancyId, DbType.Int32);
            parameterList.Add(tenancyIdParam);

            //ParameterBO currentTenantNameParam = new ParameterBO("currentTenantName", britishGasBo.CurrentTenantName, DbType.String);
            //parameterList.Add(currentTenantNameParam);                        

            ParameterBO isGasElectricCheckParam = new ParameterBO("isGasElectricCheck", britishGasBo.IsGasElectricCheck, DbType.Boolean);
            parameterList.Add(isGasElectricCheckParam);

            ParameterBO occupancyCeaseDateParam = new ParameterBO("occupancyCeaseDate", britishGasBo.OccupancyCeaseDate, DbType.Date);
            parameterList.Add(occupancyCeaseDateParam);

            ParameterBO tenantFwAddress1Param = new ParameterBO("tenantFwAddress1", britishGasBo.TenantFwAddress1, DbType.String);
            parameterList.Add(tenantFwAddress1Param);

            ParameterBO tenantFwAddress2Param = new ParameterBO("tenantFwAddress2", britishGasBo.TenantFwAddress2, DbType.String);
            parameterList.Add(tenantFwAddress2Param);

            ParameterBO tenantFwCityParam = new ParameterBO("tenantFwCity", britishGasBo.TenantFwCity, DbType.String);
            parameterList.Add(tenantFwCityParam);

            ParameterBO tenantFwPostCodeParam = new ParameterBO("tenantFwPostCode", britishGasBo.TenantFwPostCode, DbType.String);
            parameterList.Add(tenantFwPostCodeParam);

            ParameterBO britishGasEmailParam = new ParameterBO("britishGasEmail", britishGasBo.BritishGasEmail, DbType.String);
            parameterList.Add(britishGasEmailParam);



            ParameterBO isNotificationSentParam = new ParameterBO("isNotificationSent", britishGasBo.IsNotificationSent, DbType.Boolean);
            parameterList.Add(isNotificationSentParam);

            ParameterBO userIdParam = new ParameterBO("userId", britishGasBo.UserId, DbType.Int32);
            parameterList.Add(userIdParam);

            ParameterBO isSavedParam = new ParameterBO("isSaved", 0, DbType.Boolean);
            outparameterList.Add(isSavedParam);

            SaveRecord(ref parameterList, ref outparameterList, SpNameConstants.UpdateBritishGasData);

            ParameterBO obj = outparameterList.First();
            bool isSaved = Convert.ToBoolean(obj.Value);
            return isSaved;
        }
        #endregion

        #region update British Gas Stage2 Data
        /// <summary>
        /// 
        /// </summary>
        /// <param name="britishGasBo"></param>
        /// <returns></returns>
        public bool updateBritishGasStage2Data(ref BritishGasNotifcationBO britishGasBo)
        {
            ParameterList parameterList = new ParameterList();
            ParameterList outparameterList = new ParameterList();

            ParameterBO britishGasIdParam = new ParameterBO("britishGasId", britishGasBo.BritishGasNotificationId, DbType.Int32);
            parameterList.Add(britishGasIdParam);

            ParameterBO gasMeterTypeIdParam = new ParameterBO("gasMeterTypeId", britishGasBo.GasMeterTypeId, DbType.Int32);
            parameterList.Add(gasMeterTypeIdParam);

            ParameterBO gasMeterReadingParam = new ParameterBO("gasMeterReading", britishGasBo.GasMeterReading, DbType.Int64);
            parameterList.Add(gasMeterReadingParam);

            ParameterBO gasMeterReadingDateParam = new ParameterBO("gasMeterReadingDate", britishGasBo.GasMeterReadingDate, DbType.Date);
            parameterList.Add(gasMeterReadingDateParam);

            ParameterBO electricMeterTypeIdParam = new ParameterBO("electricMeterTypeId", britishGasBo.ElectricMeterTypeId, DbType.Int32);
            parameterList.Add(electricMeterTypeIdParam);

            ParameterBO electricMeterReadingParam = new ParameterBO("electricMeterReading", britishGasBo.ElectricMeterReading, DbType.Int64);
            parameterList.Add(electricMeterReadingParam);

            ParameterBO electricMeterReadingDateParam = new ParameterBO("electricMeterReadingDate", britishGasBo.ElectricMeterReadingDate, DbType.Date);
            parameterList.Add(electricMeterReadingDateParam);

            ParameterBO gasDebtAmountParam = new ParameterBO("gasDebtAmount", britishGasBo.GasDebtAmount, DbType.Double);
            parameterList.Add(gasDebtAmountParam);

            ParameterBO gasDebtAmountDateParam = new ParameterBO("gasDebtAmountDate", britishGasBo.GasDebtAmountDate, DbType.Date);
            parameterList.Add(gasDebtAmountDateParam);

            ParameterBO electricDebtAmountParam = new ParameterBO("electricDebtAmount", britishGasBo.ElectricDebtAmount, DbType.Double);
            parameterList.Add(electricDebtAmountParam);

            ParameterBO electricDebtAmountDateParam = new ParameterBO("electricDebtAmountDate", britishGasBo.ElectricDebtAmountDate, DbType.Date);
            parameterList.Add(electricDebtAmountDateParam);

            ParameterBO britishGasStageIdParam = new ParameterBO("britishGasStageId", britishGasBo.StageId, DbType.Int32);
            parameterList.Add(britishGasStageIdParam);

            ParameterBO isNotificationSentParam = new ParameterBO("isNotificationSent", britishGasBo.IsNotificationSent, DbType.Boolean);
            parameterList.Add(isNotificationSentParam);

            ParameterBO userIdParam = new ParameterBO("userId", britishGasBo.UserId, DbType.Int32);
            parameterList.Add(userIdParam);

            ParameterBO isSavedParam = new ParameterBO("isSaved", 0, DbType.Boolean);
            outparameterList.Add(isSavedParam);

            SaveRecord(ref parameterList, ref outparameterList, SpNameConstants.UpdateBritishGasStage2Data);

            ParameterBO obj = outparameterList.First();
            bool isSaved = Convert.ToBoolean(obj.Value);
            return isSaved;
        }
        #endregion

        #region update British Gas Stage 3 Data
        /// <summary>
        /// 
        /// </summary>
        /// <param name="britishGasBo"></param>
        /// <returns></returns>
        public bool updateBritishGasStage3Data(ref BritishGasNotifcationBO britishGasBo)
        {
            ParameterList parameterList = new ParameterList();
            ParameterList outparameterList = new ParameterList();

            ParameterBO britishGasIdParam = new ParameterBO("britishGasId", britishGasBo.BritishGasNotificationId, DbType.Int32);
            parameterList.Add(britishGasIdParam);

            ParameterBO newTenantNameParam = new ParameterBO("newTenantName", britishGasBo.NewTenantName, DbType.String);
            parameterList.Add(newTenantNameParam);

            ParameterBO gasMeterReadingParam = new ParameterBO("gasMeterReading", britishGasBo.GasMeterReading, DbType.Int64);
            parameterList.Add(gasMeterReadingParam);

            ParameterBO gasMeterReadingDateParam = new ParameterBO("gasMeterReadingDate", britishGasBo.GasMeterReadingDate, DbType.Date);
            parameterList.Add(gasMeterReadingDateParam);

            ParameterBO newTenantTelParam = new ParameterBO("newTenantTel", britishGasBo.NewTenantTel, DbType.String);
            parameterList.Add(newTenantTelParam);

            ParameterBO electricMeterReadingParam = new ParameterBO("electricMeterReading", britishGasBo.ElectricMeterReading, DbType.Int64);
            parameterList.Add(electricMeterReadingParam);

            ParameterBO electricMeterReadingDateParam = new ParameterBO("electricMeterReadingDate", britishGasBo.ElectricMeterReadingDate, DbType.Date);
            parameterList.Add(electricMeterReadingDateParam);

            ParameterBO newTenantMobileParam = new ParameterBO("newTenantMobile", britishGasBo.NewTenantMobile, DbType.String);
            parameterList.Add(newTenantMobileParam);

            ParameterBO newTenantDateOfBirthParam = new ParameterBO("newTenantDateOfBirth", britishGasBo.NewTenantDateOfBirth, DbType.Date);
            parameterList.Add(newTenantDateOfBirthParam);

            ParameterBO newTenantOccupancyDateParam = new ParameterBO("newTenantOccupancyDate", britishGasBo.NewTenantOccupancyDate, DbType.Date);
            parameterList.Add(newTenantOccupancyDateParam);

            ParameterBO newTenantPreviousAddress1Param = new ParameterBO("newTenantPreviousAddress1", britishGasBo.NewTenantPreviousAddress1, DbType.String);
            parameterList.Add(newTenantPreviousAddress1Param);

            ParameterBO newTenantPreviousAddress2Param = new ParameterBO("newTenantPreviousAddress2", britishGasBo.NewTenantPreviousAddress2, DbType.String);
            parameterList.Add(newTenantPreviousAddress2Param);

            ParameterBO newTenantPreviousPostCodeParam = new ParameterBO("newTenantPreviousPostCode", britishGasBo.NewTenantPreviousPostCode, DbType.String);
            parameterList.Add(newTenantPreviousPostCodeParam);

            ParameterBO newTenantPreviousTownCityParam = new ParameterBO("newTenantPreviousTownCity", britishGasBo.NewTenantPreviousTownCity, DbType.String);
            parameterList.Add(newTenantPreviousTownCityParam);

            ParameterBO userIdParam = new ParameterBO("userId", britishGasBo.UserId, DbType.Int32);
            parameterList.Add(userIdParam);

            ParameterBO isSavedParam = new ParameterBO("isSaved", 0, DbType.Boolean);
            outparameterList.Add(isSavedParam);

            SaveRecord(ref parameterList, ref outparameterList, SpNameConstants.UpdateBritishGasStage3Data);

            ParameterBO obj = outparameterList.First();
            bool isSaved = Convert.ToBoolean(obj.Value);
            return isSaved;
        }
        #endregion

        #region GetVoidPDFDocumentInfo
        /// <summary>
        /// This function would be used to Get  british Gas Void Notification  PDF Document Info
        /// </summary>
        /// <param name="britishGasVoidNotificationId"></param>
        /// <returns></returns>
        public DataSet getVoidPDFDocumentInfo(int britishGasVoidNotificationId)
        {
            ParameterList parametersList = new ParameterList();

            ParameterBO britishGasIdParam = new ParameterBO("britishGasId", britishGasVoidNotificationId, DbType.Int32);
            parametersList.Add(britishGasIdParam);
            DataSet resultDs = new DataSet();
            LoadDataSet(ref resultDs, ref  parametersList, SpNameConstants.GetVoidPDFDocumentInfo);
            return resultDs;
        }
        #endregion
    }
}
