﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PDR_DataAccess.Base;
using System.Data;
using PDR_BusinessObject;
using PDR_Utilities.Constants;
using PDR_BusinessObject.Parameter;

namespace PDR_DataAccess.UserAccess
{
    public class UserAccessRepo : BaseDAL, IUserAccessRepo
    {
        #region "Get Property Menus List"
        /// <summary>
        /// Get Property Menus List
        /// </summary>
        /// <param name="resultDataset"></param>
        /// <param name="employeeId"></param>
        /// <remarks></remarks>

        public void getPropertyMenuList(ref DataSet resultDataset, int employeeId)
        {
            ParameterList parametersList = new ParameterList();

            DataTable menusDt = new DataTable();
            menusDt.TableName = ApplicationConstants.AccessGrantedMenusDt;
            resultDataset.Tables.Add(menusDt);

            ParameterBO employeeIdParam = new ParameterBO("employeeId", employeeId, DbType.Int32);
            parametersList.Add(employeeIdParam);

            IDataReader iResultDataReader = base.SelectRecord(parametersList, SpNameConstants.GetPropertyMenuList);
            resultDataset.Load(iResultDataReader, LoadOption.OverwriteChanges, menusDt);
            iResultDataReader.Close();
        }

        #endregion

        #region "Get Property Page List "
        /// <summary>
        /// Get Property Page List 
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <remarks></remarks>

        public void getPropertyPageList(ref DataSet resultDataSet, int employeeId, string selectedMenu)
        {
            ParameterList parametersList = new ParameterList();

            DataTable modulesDt = new DataTable();
            modulesDt.TableName = ApplicationConstants.AccessGrantedModulesDt;
            resultDataSet.Tables.Add(modulesDt);

            DataTable menusDt = new DataTable();
            menusDt.TableName = ApplicationConstants.AccessGrantedMenusDt;
            resultDataSet.Tables.Add(menusDt);

            DataTable pageDt = new DataTable();
            pageDt.TableName = ApplicationConstants.AccessGrantedPagesDt;
            resultDataSet.Tables.Add(pageDt);

            DataTable randomPageDt = new DataTable();
            randomPageDt.TableName = ApplicationConstants.RandomPageDt;
            resultDataSet.Tables.Add(randomPageDt);

            ParameterBO employeeIdParam = new ParameterBO("employeeId", employeeId, DbType.Int32);
            parametersList.Add(employeeIdParam);

            ParameterBO menuTextParam = new ParameterBO("menuText", selectedMenu, DbType.String);
            parametersList.Add(menuTextParam);

            IDataReader iResultDataReader = base.SelectRecord(parametersList, SpNameConstants.GetPropertyMenuPages);
            resultDataSet.Load(iResultDataReader, LoadOption.OverwriteChanges, modulesDt, menusDt, pageDt, randomPageDt);
            iResultDataReader.Close();
        }
        #endregion

        #region "get Employee By Id"
        /// <summary>
        /// Get Employee detail By Id
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public DataSet getEmployeeById(Int32 employeeId)
        {
            ParameterList parametersList = new ParameterList();
            DataSet resultDataSet = new DataSet();

            ParameterBO employeeIdParam = new ParameterBO("employeeId", employeeId, DbType.Int32);
            parametersList.Add(employeeIdParam);

            DataTable userDt = new DataTable();
            userDt.TableName = ApplicationConstants.UserDt;
            resultDataSet.Tables.Add(userDt);

            IDataReader iResultDataReader = base.SelectRecord(parametersList,  SpNameConstants.GetEmployeeById);
            resultDataSet.Load(iResultDataReader, LoadOption.OverwriteChanges, userDt);
            iResultDataReader.Close();
            return resultDataSet;
        }
        #endregion

    }
}
