﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace PDR_DataAccess.UserAccess
{
    public interface IUserAccessRepo
    {
        #region get Property Menu List
        /// <summary>
        /// get Property Menu List
        /// </summary>
        /// <param name="resultDataset"></param>
        /// <param name="employeeId"></param>
        void getPropertyMenuList(ref DataSet resultDataset, int employeeId);
        #endregion

        #region get Property Page List
        /// <summary>
        /// get Property Page List
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="employeeId"></param>
        /// <param name="selectedMenu"></param>
        void getPropertyPageList(ref DataSet resultDataSet, int employeeId, string selectedMenu);
        #endregion

        #region "get Employee By Id"
        /// <summary>
        /// Get Employee detail By Id
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        DataSet getEmployeeById(Int32 employeeId);
        #endregion

    }
}
