﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using PDR_BusinessObject.SchemeBlock;
using PDR_BusinessObject.PageSort;

namespace PDR_DataAccess.SchemeBlock
{
    public interface IDocumentsRpo
    {
        #region "Get Document Types"
        DataSet getDocumentTypes(bool ShowActiveOnly, string reportFor = "", int categoryid = 0);
        DataSet getEpcCategoryTypesList(ref DataSet resultDataSet);
        #endregion

        #region "Get Document SubTypes"


        DataSet getDocumentSubtypes(int DocumentTypeId, bool ShowActiveOnly, string reportFor = "");
        #endregion

        #region "Save Property Document Upload"


        void saveDocumentUpload(DocumentsBO objPropertyDocumentBO);

        #endregion

        int getPropertyDocuments(ref DataSet resultDataSet, int? schemeId,int? blockId, ref PageSortBO objPageSortBo);
        string deleteDocumentByIDandGetPath(int docId);
        DataSet getDocumentInformationById(int documentId);



        void getDocumentToDownload(ref int documentId, ref string type, ref string documentName, ref string documentPath, ref string documentExt, ref string typeId);


        void getCP12DocumentByLGSRID(ref DataSet dataSet, ref int LGSRID);
        
    }
}
