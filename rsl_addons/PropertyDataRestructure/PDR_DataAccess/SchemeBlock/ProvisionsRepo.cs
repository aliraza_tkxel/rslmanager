﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using PDR_BusinessObject.PageSort;
using PDR_BusinessObject.Parameter;
using PDR_BusinessObject.SchemeBlock;
using PDR_DataAccess.Base;
using PDR_Utilities.Constants;

namespace PDR_DataAccess.SchemeBlock
{
    public class ProvisionsRepo : BaseDAL, IProvisionsRepo
    {
        public DataSet GetProvisionsData(int? schemeId, int? blockId)
        {
            DataSet resultDataSet = new DataSet();
            ParameterList parameterList = new ParameterList();

            ParameterBO schemeIdParam = new ParameterBO("schemeId", schemeId, DbType.Int32);
            parameterList.Add(schemeIdParam);

            ParameterBO blockIdParam = new ParameterBO("blockId", blockId, DbType.Int32);
            parameterList.Add(blockIdParam);

            DataTable dtProvisions = new DataTable();
            dtProvisions.TableName = ApplicationConstants.Provisions;
            resultDataSet.Tables.Add(dtProvisions);

            DataTable dtMST = new DataTable();
            dtMST.TableName = ApplicationConstants.MST;
            resultDataSet.Tables.Add(dtMST);

            IDataReader iResultDataReader = base.SelectRecord(parameterList, SpNameConstants.GetProvisions);
            resultDataSet.Load(iResultDataReader, LoadOption.OverwriteChanges, dtProvisions, dtMST);
            iResultDataReader.Close();

            return resultDataSet;
        }
        public DataSet GetProvisionsDataById(int provisionId)
        {
            DataSet resultDataSet = new DataSet();
            ParameterList parameterList = new ParameterList();

            ParameterBO provisionIdParam = new ParameterBO("provisionId", provisionId, DbType.Int32);
            parameterList.Add(provisionIdParam);

            DataTable dtProvisions = new DataTable();
            dtProvisions.TableName = ApplicationConstants.Provisions;
            resultDataSet.Tables.Add(dtProvisions);

            DataTable dtMST = new DataTable();
            dtMST.TableName = ApplicationConstants.MST;
            resultDataSet.Tables.Add(dtMST);

            IDataReader iResultDataReader = base.SelectRecord(parameterList, SpNameConstants.GetProvisionItemDetail);
            resultDataSet.Load(iResultDataReader, LoadOption.OverwriteChanges, dtProvisions, dtMST);
            iResultDataReader.Close();

            return resultDataSet;
        }
        public DataSet GetProvisionItemHistory(int provisionId, int? schemeId, int? blockId)
        {
            DataSet resultDataSet = new DataSet();
            ParameterList parameterList = new ParameterList();

            ParameterBO provisionIdParam = new ParameterBO("provisionId", provisionId, DbType.Int32);
            parameterList.Add(provisionIdParam);

            ParameterBO schemeIdParam = new ParameterBO("schemeId", schemeId, DbType.Int32);
            parameterList.Add(schemeIdParam);

            ParameterBO blockIdParam = new ParameterBO("blockId", blockId, DbType.Int32);
            parameterList.Add(blockIdParam);


            DataSet provisionHistoryDS = new DataSet();
            LoadDataSet(ref provisionHistoryDS, ref  parameterList, SpNameConstants.GetProvisionItemHistory);
            return provisionHistoryDS;
        }

        public int GetProvisioningReport(ref DataSet resultDataSet, PageSortBO objPageSortBO, string filterText, int categoryId)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO filterTextParam = new ParameterBO("filterText", filterText, DbType.String);
            paramList.Add(filterTextParam);


            ParameterBO pageNumberParam = new ParameterBO("PageNumber", objPageSortBO.PageNumber, DbType.Int32);
            paramList.Add(pageNumberParam);

            ParameterBO pageSizeParam = new ParameterBO("PageSize", objPageSortBO.PageSize, DbType.Int32);
            paramList.Add(pageSizeParam);

            ParameterBO SortDirectionParam = new ParameterBO("sortOrder", objPageSortBO.SmallSortDirection, DbType.String);
            paramList.Add(SortDirectionParam);

            ParameterBO SortExpressionParam = new ParameterBO("sortColumn", objPageSortBO.SortExpression, DbType.String);
            paramList.Add(SortExpressionParam);

            ParameterBO categoryIdParam = new ParameterBO("categoryId", categoryId, DbType.Int32);
            paramList.Add(categoryIdParam);

            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);

            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetProvisionsListReport);

            ParameterBO obj = outParametersList.First();
            Int32 outvalue = Convert.ToInt32(obj.Value.ToString());

            return outvalue;

        }

        
        public DataSet LoadCycleTypeDDL()
        {
            ParameterList parameterList = new ParameterList();
            DataSet resultDataSet = new DataSet();
            base.LoadDataSet(ref resultDataSet, ref parameterList, SpNameConstants.GetCycleType);
            return resultDataSet;
        }

        public string SaveProvsionItemDetail(ProvisionsBO objProvisionItemBO, DataTable MSATDetailDt)
        {
            ParameterList outParametersList = new ParameterList();
            ParameterList parameterList = new ParameterList();
            string result = string.Empty;

            
            ParameterBO provisionNameParam = new ParameterBO("description", objProvisionItemBO.Description, DbType.String);
            parameterList.Add(provisionNameParam);
            ParameterBO manufacturerParam = new ParameterBO("type", objProvisionItemBO.Type, DbType.Int32);
            parameterList.Add(manufacturerParam);
            ParameterBO provisionCategoryParam = new ParameterBO("provisionCategoryId", objProvisionItemBO.ProvisionCategoryId, DbType.Int32);
            parameterList.Add(provisionCategoryParam);
            ParameterBO quantityParam = new ParameterBO("quantity", objProvisionItemBO.Quantity, DbType.Int32);
            parameterList.Add(quantityParam);
            ParameterBO locationParam = new ParameterBO("location", objProvisionItemBO.Location, DbType.String);
            parameterList.Add(locationParam);
            ParameterBO supplierIdParam = new ParameterBO("supplierId", objProvisionItemBO.SupplierId, DbType.Int32);
            parameterList.Add(supplierIdParam);
            ParameterBO modelParam = new ParameterBO("model", objProvisionItemBO.Model, DbType.String);
            parameterList.Add(modelParam);
            ParameterBO serialNumberParam = new ParameterBO("serialNumber", objProvisionItemBO.SerialNumber, DbType.String);
            parameterList.Add(serialNumberParam);
            ParameterBO BhgParam = new ParameterBO("bhg", objProvisionItemBO.BHG, DbType.String);
            parameterList.Add(BhgParam);

            ParameterBO gasApplianceParam = new ParameterBO("gasAppliance", objProvisionItemBO.GasAppliance, DbType.String);
            parameterList.Add(gasApplianceParam);
            ParameterBO sluiceParam = new ParameterBO("sluice", objProvisionItemBO.Sluice, DbType.String);
            parameterList.Add(sluiceParam);
            ParameterBO ductServicingParam = new ParameterBO("ductServicing", objProvisionItemBO.DuctServicing, DbType.String);
            parameterList.Add(ductServicingParam);

            ParameterBO installDtParam = new ParameterBO("purchaseDate", objProvisionItemBO.PurchasedDate, DbType.DateTime2);
            parameterList.Add(installDtParam);

            if (objProvisionItemBO.PurchasedCost == 0)
            {
                ParameterBO installCostParam = new ParameterBO("purchaseCost", null, DbType.Decimal);
                parameterList.Add(installCostParam);
            }
            else
            {
                ParameterBO installCostParam = new ParameterBO("purchaseCost", objProvisionItemBO.PurchasedCost,
                    DbType.Decimal);
                parameterList.Add(installCostParam);
            }

            ParameterBO warrantyParam = new ParameterBO("warranty", objProvisionItemBO.Warranty, DbType.String);
            parameterList.Add(warrantyParam);
            ParameterBO ownedParam = new ParameterBO("owned", objProvisionItemBO.Owned, DbType.String);
            parameterList.Add(ownedParam);
            ParameterBO leaseExpiryDateParam = new ParameterBO("leaseExpiryDate", objProvisionItemBO.LeaseExpiryDate, DbType.DateTime2);
            parameterList.Add(leaseExpiryDateParam);

            if (objProvisionItemBO.LeaseCost == 0)
            {
                ParameterBO leaseCostParam = new ParameterBO("leaseCost", null, DbType.Decimal);
                parameterList.Add(leaseCostParam);
            }
            else
            {
                ParameterBO leaseCostParam = new ParameterBO("leaseCost", objProvisionItemBO.LeaseCost, DbType.Decimal);
                parameterList.Add(leaseCostParam);
            }

            ParameterBO lifeSpanParam = new ParameterBO("lifeSpan", objProvisionItemBO.LifeSpan, DbType.Int32);
            parameterList.Add(lifeSpanParam);
            ParameterBO replacementDueDtParam = new ParameterBO("replacementDue", objProvisionItemBO.ReplacementDue, DbType.DateTime2);
            parameterList.Add(replacementDueDtParam);
            ParameterBO lastReplacedDtParam = new ParameterBO("lastReplaced", objProvisionItemBO.LastReplaced, DbType.DateTime2);
            parameterList.Add(lastReplacedDtParam);

            ParameterBO conditionParam = new ParameterBO("condition", objProvisionItemBO.Condition, DbType.Int32);
            parameterList.Add(conditionParam);

            ParameterBO ProvisionChargeParam = new ParameterBO("ProvisionCharge", objProvisionItemBO.ProvisionCharge, DbType.String);
            parameterList.Add(ProvisionChargeParam);
            ParameterBO annualApportionmentParam = new ParameterBO("annualApportionment", objProvisionItemBO.AnnualAppointmentPC, DbType.String);
            parameterList.Add(annualApportionmentParam);
            ParameterBO blockIdPCParam = new ParameterBO("blockIdPC", objProvisionItemBO.BlockPC, DbType.Int32);
            parameterList.Add(blockIdPCParam);
            // ParameterBO propertiesParam = new ParameterBO("properties", objProvisionItemBO.PropertiesPC, DbType.String);
            //parameterList.Add(propertiesParam);

            
            ParameterBO ServicingProviderIdParam = new ParameterBO("servicingProviderId", objProvisionItemBO.ServicingProviderId, DbType.Int32);
            parameterList.Add(ServicingProviderIdParam);
            //ParameterBO ductServicingParam = new ParameterBO("ductServicing", objProvisionItemBO.DuctServicing, DbType.String);
            //parameterList.Add(ductServicingParam); 
            ParameterBO servicingRequiredParam = new ParameterBO("servicingRequired", objProvisionItemBO.ServicingRequired, DbType.String);
            parameterList.Add(servicingRequiredParam);
            ParameterBO lastDuctServicingDateParam = new ParameterBO("lastDuctServicingDate", objProvisionItemBO.LastServicingDate, DbType.DateTime2);
            parameterList.Add(lastDuctServicingDateParam);
            ParameterBO ductServiceCycleParam = new ParameterBO("ductServiceCycle", objProvisionItemBO.ServiceCycle, DbType.String);
            parameterList.Add(ductServiceCycleParam);
            ParameterBO nextDuctServicingDateParam = new ParameterBO("nextDuctServicingDate", objProvisionItemBO.NextServiceDate, DbType.DateTime2);
            parameterList.Add(nextDuctServicingDateParam);
            
            ParameterBO PATTestingParam = new ParameterBO("patTesting", objProvisionItemBO.PATTesting, DbType.String);
            parameterList.Add(PATTestingParam);


            ParameterBO provisionParentIdParam = new ParameterBO("provisionParentId", objProvisionItemBO.ProvisionParentId, DbType.Int32);
            parameterList.Add(provisionParentIdParam);
            
            //ParameterBO cycleTypeIdParam = new ParameterBO("cycleTypeId", objProvisionItemBO.CycleTypeId, DbType.Int32);
            //parameterList.Add(cycleTypeIdParam);

            ParameterBO schemeIdParam = new ParameterBO("schemeId", objProvisionItemBO.SchemeId, DbType.Int32);
            parameterList.Add(schemeIdParam);
            ParameterBO blockIdParam = new ParameterBO("blockId", objProvisionItemBO.BlockId, DbType.Int32);
            parameterList.Add(blockIdParam);
            ParameterBO isAccount = new ParameterBO("updatedBy", objProvisionItemBO.UpdatedBy, DbType.Int32);
            parameterList.Add(isAccount);
            ParameterBO existingProvisionId = new ParameterBO("existingProvisionId", 0  , DbType.Int32);
            parameterList.Add(existingProvisionId);
            ParameterBO paramProvisionId = new ParameterBO("ProvisionId", 0, DbType.String);
            outParametersList.Add(paramProvisionId); 
           
            ParameterBO MSATDetail = new ParameterBO("@MSATDetail", MSATDetailDt, SqlDbType.Structured);
            parameterList.Add(MSATDetail);
                        
            base.SaveRecord(ref parameterList, ref outParametersList, SpNameConstants.AmendProvisionItemDetail);
            ParameterBO obj = outParametersList.First();

            int provisionId = Convert.ToInt32(obj.Value);
            if (provisionId == -1)
                result = "failed";
            else
            {
                objProvisionItemBO.ProvisionId = Convert.ToInt32(obj.Value);
                result = "success";
            }

            return result;
        }

        public string UpdateProvsionItemDetail(ProvisionsBO objProvisionItemBO, DataTable MSATDetailDt)
        {
            ParameterList outParametersList = new ParameterList();
            ParameterList parameterList = new ParameterList();
            string result = string.Empty;

            ParameterBO provisionNameParam = new ParameterBO("description", objProvisionItemBO.Description, DbType.String);
            parameterList.Add(provisionNameParam);
            ParameterBO manufacturerParam = new ParameterBO("type", objProvisionItemBO.Type, DbType.Int32);
            parameterList.Add(manufacturerParam);
            ParameterBO provisionCategoryParam = new ParameterBO("provisionCategoryId", objProvisionItemBO.ProvisionCategoryId, DbType.Int32);
            parameterList.Add(provisionCategoryParam);
            ParameterBO quantityParam = new ParameterBO("quantity", objProvisionItemBO.Quantity, DbType.Int32);
            parameterList.Add(quantityParam);
            ParameterBO locationParam = new ParameterBO("location", objProvisionItemBO.Location, DbType.String);
            parameterList.Add(locationParam);
            ParameterBO supplierIdParam = new ParameterBO("supplierId", objProvisionItemBO.SupplierId, DbType.Int32);
            parameterList.Add(supplierIdParam);
            ParameterBO modelParam = new ParameterBO("model", objProvisionItemBO.Model, DbType.String);
            parameterList.Add(modelParam);
            ParameterBO serialNumberParam = new ParameterBO("serialNumber", objProvisionItemBO.SerialNumber, DbType.String);
            parameterList.Add(serialNumberParam);
            ParameterBO BhgParam = new ParameterBO("bhg", objProvisionItemBO.BHG, DbType.String);
            parameterList.Add(BhgParam);

            ParameterBO gasApplianceParam = new ParameterBO("gasAppliance", objProvisionItemBO.GasAppliance, DbType.String);
            parameterList.Add(gasApplianceParam);
            ParameterBO sluiceParam = new ParameterBO("sluice", objProvisionItemBO.Sluice, DbType.String);
            parameterList.Add(sluiceParam);
            ParameterBO ductServicingParam = new ParameterBO("ductServicing", objProvisionItemBO.DuctServicing, DbType.String);
            parameterList.Add(ductServicingParam);
          
            ParameterBO installDtParam = new ParameterBO("purchaseDate", objProvisionItemBO.PurchasedDate, DbType.DateTime2);
            parameterList.Add(installDtParam);
            if (objProvisionItemBO.PurchasedCost == 0)
            {
                ParameterBO installCostParam = new ParameterBO("purchaseCost", null, DbType.Decimal);
                parameterList.Add(installCostParam);
            }
            else
            {
                ParameterBO installCostParam = new ParameterBO("purchaseCost", objProvisionItemBO.PurchasedCost,
                    DbType.Decimal);
                parameterList.Add(installCostParam);
            }
            ParameterBO warrantyParam = new ParameterBO("warranty", objProvisionItemBO.Warranty, DbType.String);
            parameterList.Add(warrantyParam);
            ParameterBO ownedParam = new ParameterBO("owned", objProvisionItemBO.Owned, DbType.String);
            parameterList.Add(ownedParam);
            ParameterBO leaseExpiryDateParam = new ParameterBO("leaseExpiryDate", objProvisionItemBO.LeaseExpiryDate, DbType.DateTime2);
            parameterList.Add(leaseExpiryDateParam);
            if (objProvisionItemBO.LeaseCost == 0)
            {
                ParameterBO leaseCostParam = new ParameterBO("leaseCost", null, DbType.Decimal);
                parameterList.Add(leaseCostParam);
            }
            else
            {
                ParameterBO leaseCostParam = new ParameterBO("leaseCost", objProvisionItemBO.LeaseCost, DbType.Decimal);
                parameterList.Add(leaseCostParam);
            }
        
            ParameterBO lifeSpanParam = new ParameterBO("lifeSpan", objProvisionItemBO.LifeSpan, DbType.Int32);
            parameterList.Add(lifeSpanParam);
            ParameterBO replacementDueDtParam = new ParameterBO("replacementDue", objProvisionItemBO.ReplacementDue, DbType.DateTime2);
            parameterList.Add(replacementDueDtParam);
            ParameterBO lastReplacedDtParam = new ParameterBO("lastReplaced", objProvisionItemBO.LastReplaced, DbType.DateTime2);
            parameterList.Add(lastReplacedDtParam);

            ParameterBO conditionParam = new ParameterBO("condition", objProvisionItemBO.Condition, DbType.Int32);
            parameterList.Add(conditionParam);
            ParameterBO PATTestingParam = new ParameterBO("patTesting", objProvisionItemBO.PATTesting, DbType.String);
            parameterList.Add(PATTestingParam);

            ParameterBO ProvisionChargeParam = new ParameterBO("ProvisionCharge", objProvisionItemBO.ProvisionCharge, DbType.String);
            parameterList.Add(ProvisionChargeParam);
            ParameterBO annualApportionmentParam = new ParameterBO("annualApportionment", objProvisionItemBO.AnnualAppointmentPC, DbType.String);
            parameterList.Add(annualApportionmentParam);
            ParameterBO blockIdPCParam = new ParameterBO("blockIdPC", objProvisionItemBO.BlockPC, DbType.Int32);
            parameterList.Add(blockIdPCParam);

            ParameterBO servicingRequiredParam = new ParameterBO("servicingRequired", objProvisionItemBO.ServicingRequired, DbType.String);
            parameterList.Add(servicingRequiredParam);
            ParameterBO ServicingProviderIdParam = new ParameterBO("servicingProviderId", objProvisionItemBO.ServicingProviderId, DbType.Int32);
            parameterList.Add(ServicingProviderIdParam);
            //ParameterBO ductServicingParam = new ParameterBO("ductServicing", objProvisionItemBO.DuctServicing, DbType.String);
            //parameterList.Add(ductServicingParam);
            ParameterBO lastDuctServicingDateParam = new ParameterBO("lastDuctServicingDate", objProvisionItemBO.LastServicingDate, DbType.DateTime2);
            parameterList.Add(lastDuctServicingDateParam);
            ParameterBO ductServiceCycleParam = new ParameterBO("ductServiceCycle", objProvisionItemBO.ServiceCycle, DbType.String);
            parameterList.Add(ductServiceCycleParam);
            ParameterBO nextDuctServicingDateParam = new ParameterBO("nextDuctServicingDate", objProvisionItemBO.NextServiceDate, DbType.DateTime2);
            parameterList.Add(nextDuctServicingDateParam);


            ParameterBO provisionParentIdParam = new ParameterBO("provisionParentId", objProvisionItemBO.ProvisionParentId, DbType.Int32);
            parameterList.Add(provisionParentIdParam);

            //ParameterBO cycleTypeIdParam = new ParameterBO("cycleTypeId", objProvisionItemBO.CycleTypeId, DbType.Int32);
            //parameterList.Add(cycleTypeIdParam);

            ParameterBO schemeIdParam = new ParameterBO("schemeId", objProvisionItemBO.SchemeId, DbType.Int32);
            parameterList.Add(schemeIdParam);
            ParameterBO blockIdParam = new ParameterBO("blockId", objProvisionItemBO.BlockId, DbType.Int32);
            parameterList.Add(blockIdParam);
            ParameterBO isAccount = new ParameterBO("updatedBy", objProvisionItemBO.UpdatedBy, DbType.Int32);
            parameterList.Add(isAccount);
            ParameterBO existingProvisionId = new ParameterBO("existingProvisionId", objProvisionItemBO.ExistingProvisionId, DbType.Int32);
            parameterList.Add(existingProvisionId);
            ParameterBO paramProvisionId = new ParameterBO("ProvisionId", 0, DbType.String);
            outParametersList.Add(paramProvisionId);

            ParameterBO MSATDetail = new ParameterBO("@MSATDetail", MSATDetailDt, SqlDbType.Structured);
            parameterList.Add(MSATDetail);

            base.SaveRecord(ref parameterList, ref outParametersList, SpNameConstants.AmendProvisionItemDetail);
            ParameterBO obj = outParametersList.First();

            int provisionId = Convert.ToInt32(obj.Value);
            if (provisionId == -1)
                result = "failed";
            else
            {
                objProvisionItemBO.ProvisionId = Convert.ToInt32(obj.Value);
                result = "success";
            }

            return result;
        }

        public DataSet GetProvisionNotes(int? schemeId, int? blockId, int provisionId)
        {
            ParameterList parameterList = new ParameterList();

            ParameterBO schemeIdParam = new ParameterBO("schemeId", schemeId, DbType.Int32);
            parameterList.Add(schemeIdParam);

            ParameterBO blockIdParam = new ParameterBO("blockId", blockId, DbType.Int32);
            parameterList.Add(blockIdParam);

            ParameterBO provisionIdParam = new ParameterBO("provisionId", provisionId, DbType.Int32);
            parameterList.Add(provisionIdParam);

            DataSet notesDataSet = new DataSet();
            LoadDataSet(ref notesDataSet, ref  parameterList, SpNameConstants.GetProvisionNotes);
            return notesDataSet;
        }

        public void SaveProvisionNotes(ProvisionNotesBO objProvisionNotesBO)
        {
            ParameterList parameterList = new ParameterList();
            ParameterList outparameterList = new ParameterList();

            ParameterBO itemIdParam = new ParameterBO("provisionId", objProvisionNotesBO.ProvisionId, DbType.Int32);
            parameterList.Add(itemIdParam);

            ParameterBO schemeIdParam = new ParameterBO("schemeId", objProvisionNotesBO.SchemeId, DbType.Int32);
            parameterList.Add(schemeIdParam);

            ParameterBO blockIdParam = new ParameterBO("blockId", objProvisionNotesBO.BlockId, DbType.Int32);
            parameterList.Add(blockIdParam);

            ParameterBO FilePathParam = new ParameterBO("note", objProvisionNotesBO.Notes, DbType.String);
            parameterList.Add(FilePathParam);

            ParameterBO ImageName = new ParameterBO("createdBy", objProvisionNotesBO.CreatedBy, DbType.Int32);
            parameterList.Add(ImageName);
            SaveRecord(ref parameterList, ref outparameterList, SpNameConstants.SaveProvisionNotes);
        }
    
        public DataSet GetProvisionImages(int provisionId, int? schemeId, int? blockId)
        {
            ParameterList parameterList = new ParameterList();
            
            ParameterBO schemeIdParam = new ParameterBO("schemeId", schemeId, DbType.Int32);
            parameterList.Add(schemeIdParam);

            ParameterBO blockIdParam = new ParameterBO("blockId", blockId, DbType.Int32);
            parameterList.Add(blockIdParam);

            ParameterBO provisionIdParam = new ParameterBO("provisionId", provisionId, DbType.Int32);
            parameterList.Add(provisionIdParam);

            DataSet photosDataSet = new DataSet();
            LoadDataSet(ref photosDataSet, ref parameterList, SpNameConstants.GetProvisionImages);
            return photosDataSet;
        }

        public void SaveProvisionImages(ProvisionPhotographsBO objProvisionPhotoBO)
        {
            ParameterList parametersList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO itemIdParam = new ParameterBO("provisionId", objProvisionPhotoBO.ProvisionId, DbType.Int32);
            parametersList.Add(itemIdParam);
            ParameterBO Title = new ParameterBO("title", objProvisionPhotoBO.Title, DbType.String);
            parametersList.Add(Title);
            ParameterBO UploadDate = new ParameterBO("uploadDate", objProvisionPhotoBO.UploadDate, DbType.DateTime);
            parametersList.Add(UploadDate);
            ParameterBO FilePathParam = new ParameterBO("imagePath", objProvisionPhotoBO.FilePath, DbType.String);
            parametersList.Add(FilePathParam);
            ParameterBO ImageName = new ParameterBO("imageName", objProvisionPhotoBO.ImageName, DbType.String);
            parametersList.Add(ImageName);           
            ParameterBO CreatedBy = new ParameterBO("createdBy", objProvisionPhotoBO.CreatedBy, DbType.Int32);
            parametersList.Add(CreatedBy);
            ParameterBO schemeIdParam = new ParameterBO("schemeId", objProvisionPhotoBO.SchemeId, DbType.Int32);
            parametersList.Add(schemeIdParam);
            ParameterBO blockIdParam = new ParameterBO("blockId", objProvisionPhotoBO.BlockId, DbType.Int32);
            parametersList.Add(blockIdParam);
            ParameterBO isDefaultImageParam = new ParameterBO("isDefaultImage", false, DbType.Boolean);
            parametersList.Add(isDefaultImageParam);
            base.SaveRecord(ref parametersList, ref outParametersList, SpNameConstants.SaveProvisionPhotograph);

        }

        public DataSet GetSupplierList()
        {
            DataSet supplierDataset = new DataSet();
            return supplierDataset;
        }
        public DataSet GetProvisionCategories()
        {
            DataSet categoriesDataset = new DataSet();
            ParameterList parameterList = new ParameterList();
            LoadDataSet(ref categoriesDataset, ref parameterList, SpNameConstants.GetProvisionsCategories);
            return categoriesDataset;
        }

        #region "Save Service Charge Properties"
        public void SaveProvisionChargeProperties(ProvisionChargeProperties serProperties, DataTable excludedIncludedProperties, int itemId)
        {
            ParameterList outParametersList = new ParameterList();
            ParameterList parameterList = new ParameterList();
            ParameterBO schemeIdParam = new ParameterBO("schemeId", serProperties.SchemeId, DbType.Int32);
            parameterList.Add(schemeIdParam);

            ParameterBO blockIdParam = new ParameterBO("blockId", serProperties.BlockId, DbType.Int32);
            parameterList.Add(blockIdParam);

            ParameterBO itemParam = new ParameterBO("itemId", itemId, DbType.Int32);
            parameterList.Add(itemParam);

            ParameterBO properties = new ParameterBO("@ItemDetail", excludedIncludedProperties, SqlDbType.Structured);
            parameterList.Add(properties);

            ParameterList outParamList = new ParameterList();
            SaveRecord(ref parameterList, ref outParamList, SpNameConstants.SaveProvisionChargeProperties);

        }
        #endregion

        public DataSet getSavedProvisionChargeProperties(int schemeId, int blockId, int itemId)
        {
            DataSet resultDataSet = new DataSet();
            ParameterList parametersList = new ParameterList();
            ParameterBO schemeParam;
            ParameterBO blockParam;
            if (schemeId == 0)
            {
                schemeParam = new ParameterBO("schemeId", null, DbType.Int32);
            }
            else
            {
                schemeParam = new ParameterBO("schemeId", schemeId, DbType.Int32);
            }
            parametersList.Add(schemeParam);
            if (blockId == 0)
            {
                blockParam = new ParameterBO("blockId", null, DbType.Int32);
            }
            else
            {
                blockParam = new ParameterBO("blockId", blockId, DbType.Int32);
            }
            parametersList.Add(blockParam);

            ParameterBO itemIdParam;

            itemIdParam = new ParameterBO("itemId", itemId, DbType.Int32);
            parametersList.Add(itemIdParam);

            

            DataTable dtProperty = new DataTable();
            dtProperty.TableName = "Properties";
            resultDataSet.Tables.Add(dtProperty);

            IDataReader iResultDataReader = base.SelectRecord(parametersList, SpNameConstants.GetProvisionChargeSelectedProperties);
            resultDataSet.Load(iResultDataReader, LoadOption.OverwriteChanges, dtProperty);
            iResultDataReader.Close();
            return resultDataSet;
        }

        #region "get Excluded properties by scheme and block"
        public DataSet getExPropertiesBySchemeBlock(int scheme, int block, int itemId)
        {
            ParameterList parameterList = new ParameterList();
            ParameterBO schemeParam;
            ParameterBO blockParam;
            if (scheme == 0)
            {
                schemeParam = new ParameterBO("schemeId", null, DbType.Int32);
            }
            else
            {
                schemeParam = new ParameterBO("schemeId", scheme, DbType.Int32);
            }
            parameterList.Add(schemeParam);
            if (block == 0)
            {
                blockParam = new ParameterBO("blockId", null, DbType.Int32);
            }
            else
            {
                blockParam = new ParameterBO("blockId", block, DbType.Int32);
            }
            parameterList.Add(blockParam);
            ParameterBO itemParam;
            itemParam = new ParameterBO("itemid", itemId, DbType.Int32);
            parameterList.Add(itemParam);

            DataSet resultDataSet = new DataSet();
            base.LoadDataSet(ref resultDataSet, ref parameterList, SpNameConstants.GetProvisionChargeExProperties);
            return resultDataSet;
        }
        #endregion

        #region "get provisions by category for scheme and block"
        public DataSet GetProvisionsByCategories(int categoryId, int? schemeId, int? blockId)
        {
            ParameterList parameterList = new ParameterList();

            ParameterBO schemeIdParam = new ParameterBO("schemeId", schemeId, DbType.Int32);
            parameterList.Add(schemeIdParam);

            ParameterBO blockIdParam = new ParameterBO("blockId", blockId, DbType.Int32);
            parameterList.Add(blockIdParam);

            ParameterBO provisionIdParam = new ParameterBO("categoryId", categoryId, DbType.Int32);
            parameterList.Add(provisionIdParam);

            DataSet resultDataSet = new DataSet();
            base.LoadDataSet(ref resultDataSet, ref parameterList, SpNameConstants.GetProvisionsByCategory);
            return resultDataSet;
        }
        #endregion

        public DataSet GetProvisionByProvisionId(int provisionId)
        {
            ParameterList parameterList = new ParameterList();

            ParameterBO schemeIdParam = new ParameterBO("provisionId", provisionId, DbType.Int32);
            parameterList.Add(schemeIdParam);

            DataSet resultDataSet = new DataSet();
            base.LoadDataSet(ref resultDataSet, ref parameterList, SpNameConstants.GetProvisionByProvisionId);
            return resultDataSet;
        }
    }
}
