﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PDR_DataAccess.Base;
using PDR_BusinessObject.Parameter;
using System.Data;
using System.Globalization;
using PDR_Utilities.Constants;
using PDR_BusinessObject.SchemeBlock;
using PDR_BusinessObject.PageSort;

namespace PDR_DataAccess.SchemeBlock
{
    public class AttributesRepo : BaseDAL, IAttributesRepo
    {



        #region "get Locations"
        public DataSet getLocations()
        {
            ParameterList parameterList = new ParameterList();
            DataSet resultDataSet = new DataSet();
            LoadDataSet(ref resultDataSet, ref parameterList, SpNameConstants.GetLocations);
            return resultDataSet;
        }
        #endregion

        #region "get Areas by Location Id"
        public DataSet getAreasByLocationId(int locationId)
        {
            ParameterList parameterList = new ParameterList();

            ParameterBO LocationIdParam = new ParameterBO("locationId", locationId, DbType.Int32);
            parameterList.Add(LocationIdParam);
            DataSet resultDataSet = new DataSet();
            LoadDataSet(ref resultDataSet, ref parameterList, SpNameConstants.GetAreasByLocationId);
            return resultDataSet;
        }
        #endregion

        #region "get Items by Area Id"
        public DataSet getItemsByAreaId(int areaId)
        {
            ParameterList parameterList = new ParameterList();


            ParameterBO AreaIdParam = new ParameterBO("areaId", areaId, DbType.Int32);
            parameterList.Add(AreaIdParam);
            DataSet resultDataSet = new DataSet();
            LoadDataSet(ref resultDataSet, ref parameterList, SpNameConstants.GetItemsByAreaId);
            return resultDataSet;
        }
        #endregion

        #region "get Items by Area Id"
        public DataSet getSubItemsByItemId(int itemId)
        {
            ParameterList parameterList = new ParameterList();


            ParameterBO ItemIdParam = new ParameterBO("itemId", itemId, DbType.Int32);
            parameterList.Add(ItemIdParam);
            DataSet resultDataSet = new DataSet();
            LoadDataSet(ref resultDataSet, ref parameterList, SpNameConstants.GetSubItemsByItemId);
            return resultDataSet;
        }
        #endregion

        #region "get Items by Item Id"
        public DataSet getItemsByItemId(int itemId)
        {
            ParameterList parameterList = new ParameterList();


            ParameterBO ItemIdParam = new ParameterBO("itemId", itemId, DbType.Int32);
            parameterList.Add(ItemIdParam);
            DataSet resultDataSet = new DataSet();
            LoadDataSet(ref resultDataSet, ref parameterList, SpNameConstants.GetItemByItemId);
            return resultDataSet;
        }
        #endregion
        #region get boilers list

        public DataSet getBoilerslist(int? schemeId, int? blockId)
        {
            ParameterList parameterList = new ParameterList();

            ParameterBO schemeIdParam = new ParameterBO("schemeId", schemeId, DbType.Int32);
            parameterList.Add(schemeIdParam);

            ParameterBO blockIdParam = new ParameterBO("blockId", blockId, DbType.Int32);
            parameterList.Add(blockIdParam);

            DataSet notesDataSet = new DataSet();
            LoadDataSet(ref notesDataSet, ref  parameterList, SpNameConstants.GetBoilersList);
            return notesDataSet;
        }
        #endregion
        //////////////////////////////////////////////////////////
        #region get water meter list

        public DataSet getWaterMeterlist(int? schemeId, int? blockId, int? itemId)
        {
            ParameterList parameterList = new ParameterList();

            ParameterBO schemeIdParam = new ParameterBO("schemeId", schemeId, DbType.Int32);
            parameterList.Add(schemeIdParam);

            ParameterBO blockIdParam = new ParameterBO("blockId", blockId, DbType.Int32);
            parameterList.Add(blockIdParam);

            ParameterBO itermIdParam = new ParameterBO("itemId", itemId, DbType.Int32);
            parameterList.Add(itermIdParam);

            DataSet notesDataSet = new DataSet();
            LoadDataSet(ref notesDataSet, ref  parameterList, SpNameConstants.GetMultiAttributeListByItemId);
            return notesDataSet;
        }
        #endregion

        #region get water meter list

        public DataSet getMultiAttributelistByItemId(int? schemeId, int? blockId, int? itemId)
        {
            ParameterList parameterList = new ParameterList();

            ParameterBO schemeIdParam = new ParameterBO("schemeId", schemeId, DbType.Int32);
            parameterList.Add(schemeIdParam);

            ParameterBO blockIdParam = new ParameterBO("blockId", blockId, DbType.Int32);
            parameterList.Add(blockIdParam);

            ParameterBO itermIdParam = new ParameterBO("itemId", itemId, DbType.Int32);
            parameterList.Add(itermIdParam);

            DataSet notesDataSet = new DataSet();
            LoadDataSet(ref notesDataSet, ref  parameterList, SpNameConstants.GetMultiAttributeListByItemId);
            return notesDataSet;
        }
        #endregion

        #region get Passenger Lift list

        public DataSet getPassengerLiftlist(int? schemeId, int? blockId, int? itemId)
        {
            ParameterList parameterList = new ParameterList();

            ParameterBO schemeIdParam = new ParameterBO("schemeId", schemeId, DbType.Int32);
            parameterList.Add(schemeIdParam);

            ParameterBO blockIdParam = new ParameterBO("blockId", blockId, DbType.Int32);
            parameterList.Add(blockIdParam);

            ParameterBO itermIdParam = new ParameterBO("itemId", itemId, DbType.Int32);
            parameterList.Add(itermIdParam);

            DataSet notesDataSet = new DataSet();
            LoadDataSet(ref notesDataSet, ref  parameterList, SpNameConstants.GetMultiAttributeListByItemId);
            return notesDataSet;
        }
        #endregion
        //////////////////////////////////////////////////////////


        #region "Get Item Notes"
        public DataSet getItemNotes(int? schemeId, int? blockId, int itemId, int? heatingMappingId)
        {
            ParameterList parameterList = new ParameterList();

            ParameterBO itemIdParam = new ParameterBO("itemId", itemId, DbType.Int32);
            parameterList.Add(itemIdParam);

            ParameterBO schemeIdParam = new ParameterBO("schemeId", schemeId, DbType.Int32);
            parameterList.Add(schemeIdParam);

            ParameterBO blockIdParam = new ParameterBO("blockId", blockId, DbType.Int32);
            parameterList.Add(blockIdParam);

            ParameterBO heatingMappingIdParam = new ParameterBO("heatingMappingId", heatingMappingId, DbType.Int32);
            parameterList.Add(heatingMappingIdParam);

            DataSet notesDataSet = new DataSet();
            LoadDataSet(ref notesDataSet, ref  parameterList, SpNameConstants.GetItemNotes);
            return notesDataSet;
        }
        #endregion

        #region "Save Item Notes"
        public void saveItemNotes(AttributeNotesBO objItemNotesBo)
        {
            ParameterList parameterList = new ParameterList();
            ParameterList outparameterList = new ParameterList();


            ParameterBO itemIdParam = new ParameterBO("itemId", objItemNotesBo.ItemId, DbType.Int32);
            parameterList.Add(itemIdParam);

            ParameterBO proerptyIdParam = new ParameterBO("propertyId", objItemNotesBo.PropertyId, DbType.String);
            parameterList.Add(proerptyIdParam);

            ParameterBO schemeIdParam = new ParameterBO("schemeId", objItemNotesBo.SchemeId, DbType.Int32);
            parameterList.Add(schemeIdParam);

            ParameterBO blockIdParam = new ParameterBO("blockId", objItemNotesBo.BlockId, DbType.Int32);
            parameterList.Add(blockIdParam);

            ParameterBO heatingMappingIdParam = new ParameterBO("heatingMappingId", objItemNotesBo.heatingMappingId, DbType.Int32);
            parameterList.Add(heatingMappingIdParam);

            ParameterBO FilePathParam = new ParameterBO("note", objItemNotesBo.Notes, DbType.String);
            parameterList.Add(FilePathParam);

            ParameterBO showInSchedulingParam = new ParameterBO("showInScheduling", objItemNotesBo.ShowInScheduling,
                DbType.Boolean);
            parameterList.Add(showInSchedulingParam);

            ParameterBO showOnAppParam = new ParameterBO("showOnApp", objItemNotesBo.ShowOnApp, DbType.Boolean);
            parameterList.Add(showOnAppParam);


            ParameterBO ImageName = new ParameterBO("createdBy", objItemNotesBo.CreatedBy, DbType.Int32);
            parameterList.Add(ImageName);
            SaveRecord(ref parameterList, ref outparameterList, SpNameConstants.SaveItemNotes);
        }
        #endregion

        #region "Update Item Notes"
        public void UpdateItemNotes(AttributeNotesBO objItemNotesBo)
        {
            ParameterList parameterList = new ParameterList();
            ParameterList outparameterList = new ParameterList();

            ParameterBO notesIdParam = new ParameterBO("notesId", objItemNotesBo.NotesId, DbType.Int32);
            parameterList.Add(notesIdParam);

            ParameterBO notesParam = new ParameterBO("note", objItemNotesBo.Notes, DbType.String);
            parameterList.Add(notesParam);

            ParameterBO showInSchedulingParam = new ParameterBO("showInScheduling", objItemNotesBo.ShowInScheduling,
                DbType.Boolean);
            parameterList.Add(showInSchedulingParam);

            ParameterBO showOnAppParam = new ParameterBO("showOnApp", objItemNotesBo.ShowOnApp, DbType.Boolean);
            parameterList.Add(showOnAppParam);

            SaveRecord(ref parameterList, ref outparameterList, SpNameConstants.UpdateItemNotes);
        }
        #endregion

        #region "Delete Item Notes"
        public void DeleteItemNotes(int itemNotesId)
        {
            ParameterList parameterList = new ParameterList();
            ParameterList outparameterList = new ParameterList();

            ParameterBO notesIdParam = new ParameterBO("notesId", itemNotesId, DbType.Int32);
            parameterList.Add(notesIdParam);

            SaveRecord(ref parameterList, ref outparameterList, SpNameConstants.DeleteItemNotes);
        }
        #endregion

        #region "Get Item Notes Trail"
        public void GetItemNotesTrail(ref DataSet resultDataSet, int itemNotesId)
        {
            ParameterList parameterList = new ParameterList();

            ParameterBO notesIdParam = new ParameterBO("notesId", itemNotesId, DbType.Int32);
            parameterList.Add(notesIdParam);

            LoadDataSet(ref resultDataSet, ref  parameterList, SpNameConstants.GetItemNotesTrail);
        }
        #endregion


        #region Save Document Upload
        public void saveDocumentUpload(AttributePhotographsBO objAttrPhotoBO)
        {
            ParameterList parametersList = new ParameterList();
            ParameterList outParametersList = new ParameterList();


            ParameterBO itemIdParam = new ParameterBO("itemId", objAttrPhotoBO.ItemId, DbType.Int32);
            parametersList.Add(itemIdParam);

            ParameterBO proerptyIdParam = new ParameterBO("propertyId", string.Empty, DbType.String);
            parametersList.Add(proerptyIdParam);

            ParameterBO FilePathParam = new ParameterBO("imagePath", objAttrPhotoBO.FilePath, DbType.String);
            parametersList.Add(FilePathParam);

            ParameterBO ImageName = new ParameterBO("imageName", objAttrPhotoBO.ImageName, DbType.String);
            parametersList.Add(ImageName);

            ParameterBO Title = new ParameterBO("title", objAttrPhotoBO.Title, DbType.String);
            parametersList.Add(Title);

            ParameterBO UploadDate = new ParameterBO("uploadDate", objAttrPhotoBO.UploadDate, DbType.DateTime);
            parametersList.Add(UploadDate);

            ParameterBO CreatedBy = new ParameterBO("createdBy", objAttrPhotoBO.CreatedBy, DbType.Int32);
            parametersList.Add(CreatedBy);

            ParameterBO schemeIdParam = new ParameterBO("schemeId", objAttrPhotoBO.SchemeId, DbType.Int32);
            parametersList.Add(schemeIdParam);

            ParameterBO blockIdParam = new ParameterBO("blockId", objAttrPhotoBO.BlockId, DbType.Int32);
            parametersList.Add(blockIdParam);

            ParameterBO heatingMappingIdParam = new ParameterBO("heatingMappingId", objAttrPhotoBO.heatingMappingId, DbType.Int32);
            parametersList.Add(heatingMappingIdParam);

            ParameterBO isDefaultImageParam = new ParameterBO("isDefaultImage", false, DbType.Boolean);
            parametersList.Add(isDefaultImageParam);
            SaveRecord(ref parametersList, ref outParametersList, SpNameConstants.SavePhotograph);

        }

        #endregion
        #region "Get Property Images"
        public DataSet getPropertyImages(int itemId, int? schemeId, int? blockId, int? heatingMappingId)
        {
            ParameterList parameterList = new ParameterList();

            ParameterBO itemIdParam = new ParameterBO("itemId", itemId, DbType.Int32);
            parameterList.Add(itemIdParam);

            ParameterBO proerptyIdParam = new ParameterBO("proerptyId", string.Empty, DbType.String);
            parameterList.Add(proerptyIdParam);

            ParameterBO schemeIdParam = new ParameterBO("schemeId", schemeId, DbType.Int32);
            parameterList.Add(schemeIdParam);

            ParameterBO blockIdParam = new ParameterBO("blockId", blockId, DbType.Int32);
            parameterList.Add(blockIdParam);

            ParameterBO heatingMappingIdParam = new ParameterBO("heatingMappingId", heatingMappingId, DbType.Int32);
            parameterList.Add(heatingMappingIdParam);

            DataSet photosDataSet = new DataSet();
            LoadDataSet(ref photosDataSet, ref parameterList, SpNameConstants.GetPropertyImages);
            return photosDataSet;
        }
        #endregion


        #region "get Item Detail"
        public DataSet getItemDetail(int itemId, int? schemeId, int? blockId, int? childAttributeMappingId = null)
        {
            ParameterList parameterList = new ParameterList();
            DataSet resultDataSet = new DataSet();
            ParameterBO itemIdParam = new ParameterBO("itemId", itemId, DbType.Int32);
            parameterList.Add(itemIdParam);

            ParameterBO schemeIdParam = new ParameterBO("SchemeId", schemeId, DbType.Int32);
            parameterList.Add(schemeIdParam);

            ParameterBO blockIdParam = new ParameterBO("blockId", blockId, DbType.Int32);
            parameterList.Add(blockIdParam);

            ParameterBO childAttributMappingIdParam = new ParameterBO("ChildAttributeMappingId", childAttributeMappingId, DbType.Int32);
            parameterList.Add(childAttributMappingIdParam);


            DataTable dtParameters = new DataTable();
            dtParameters.TableName = ApplicationConstants.Parameters;
            resultDataSet.Tables.Add(dtParameters);

            DataTable dtParametervalues = new DataTable();
            dtParametervalues.TableName = ApplicationConstants.ParameterValues;
            resultDataSet.Tables.Add(dtParametervalues);

            DataTable dtpreinsertedvalues = new DataTable();
            dtpreinsertedvalues.TableName = ApplicationConstants.PreInsertedValues;
            resultDataSet.Tables.Add(dtpreinsertedvalues);

            DataTable dtLastReplaced = new DataTable();
            dtLastReplaced.TableName = ApplicationConstants.LastReplaced;
            resultDataSet.Tables.Add(dtLastReplaced);

            DataTable dtMST = new DataTable();
            dtMST.TableName = ApplicationConstants.MST;
            resultDataSet.Tables.Add(dtMST);


            DataTable dtPlannedComponent = new DataTable();
            dtPlannedComponent.TableName = ApplicationConstants.PlannedComponent;
            resultDataSet.Tables.Add(dtPlannedComponent);

            DataTable dtCyclic = new DataTable();
            dtCyclic.TableName = ApplicationConstants.CyclicServices;
            resultDataSet.Tables.Add(dtCyclic);

            IDataReader iResultDataReader = base.SelectRecord(parameterList, SpNameConstants.GetItemDetail);
            resultDataSet.Load(iResultDataReader, LoadOption.OverwriteChanges, dtParameters, dtParametervalues, dtpreinsertedvalues, dtLastReplaced, dtMST, dtPlannedComponent, dtCyclic);
            iResultDataReader.Close();
            return resultDataSet;
        }
        #endregion



        #region " load Cycle DDL"
        public DataSet loadCycleDdl()
        {
            ParameterList parameterList = new ParameterList();
            DataSet resultDataSet = new DataSet();
            base.LoadDataSet(ref resultDataSet, ref parameterList, SpNameConstants.GetCycleType);
            return resultDataSet;
        }
        #endregion

        #region "Amend Item Detail"

        public int amendAttributeDetail(ItemAttributeBO objItemAttributeBo, DataTable itemDetailDt, DataTable itemDatesDt, DataTable conditionRatingDt, DataTable MSATDetailDt, CyclicalServiceBO cyclicalService)
        {
            ParameterList outParametersList = new ParameterList();
            ParameterList parameterList = new ParameterList();
            ParameterBO schemeIdParam = new ParameterBO("schemeId", objItemAttributeBo.SchemeId, DbType.Int32);
            parameterList.Add(schemeIdParam);

            ParameterBO blockIdParam = new ParameterBO("blockId", objItemAttributeBo.BlockId, DbType.Int32);
            parameterList.Add(blockIdParam);

            ParameterBO componentName = new ParameterBO("ItemId", objItemAttributeBo.ItemId, DbType.Int32);
            parameterList.Add(componentName);

            ParameterBO attributeName = new ParameterBO("AttributeName", objItemAttributeBo.attributeName, DbType.String);
            parameterList.Add(attributeName);

            ParameterBO heatingMappingId = new ParameterBO("HeatingMappingId", objItemAttributeBo.HeatingMappingId, DbType.Int32);
            parameterList.Add(heatingMappingId);

            ParameterBO updatedBy = new ParameterBO("UpdatedBy", objItemAttributeBo.UpdatedBy, DbType.Int32);
            parameterList.Add(updatedBy);
            ///Cyclical Service Item
            ParameterBO contractorId = new ParameterBO("ContractorId", cyclicalService.ContractorId, DbType.Int32);
            parameterList.Add(contractorId);

            ParameterBO commencment = new ParameterBO("Commencment", cyclicalService.Commencment, DbType.DateTime);
            parameterList.Add(commencment);

            ParameterBO contractPeriod = new ParameterBO("ContractPeriod", cyclicalService.ContractPeriod, DbType.Int32);
            parameterList.Add(contractPeriod);

            ParameterBO contractPeriodType = new ParameterBO("ContractPeriodType", cyclicalService.ContractPeriodType, DbType.Int32);
            parameterList.Add(contractPeriodType);

            ParameterBO cycle = new ParameterBO("Cycle", cyclicalService.Cycle, DbType.Int32);
            parameterList.Add(cycle);

            ParameterBO cycleType = new ParameterBO("CycleType", cyclicalService.CycleType, DbType.Int32);
            parameterList.Add(cycleType);
            ParameterBO cycleValue = new ParameterBO("CycleValue", cyclicalService.CycleValue, DbType.Decimal);
            parameterList.Add(cycleValue);

            ParameterBO BFSCarryingOutWork = new ParameterBO("BFSCarryingOutWork", cyclicalService.BFSCarryingOutWork, DbType.Boolean);
            parameterList.Add(BFSCarryingOutWork);

            ParameterBO VAT = new ParameterBO("VAT", cyclicalService.VAT, DbType.String);
            parameterList.Add(VAT);

            ParameterBO TotalValue = new ParameterBO("TotalValue", cyclicalService.TotalValue, DbType.Int32);
            parameterList.Add(TotalValue);

            ParameterBO CustomCycleFrequency = new ParameterBO("CustomCycleFrequency", cyclicalService.CustomCycleFrequency, DbType.Int32);
            parameterList.Add(CustomCycleFrequency);

            ParameterBO CustomCycleOccurance = new ParameterBO("CustomCycleOccurance", cyclicalService.CustomCycleOccurance, DbType.String);
            parameterList.Add(CustomCycleOccurance);

            ParameterBO CustomCycleType = new ParameterBO("CustomCycleType", cyclicalService.CustomCycleType, DbType.String);
            parameterList.Add(CustomCycleType);


            ///Cyclical Service Item

            ParameterBO ChildAttributeMappingId = new ParameterBO("ChildAttributeMappingId", objItemAttributeBo.ChildAttributeMappingId, DbType.Int32);
            parameterList.Add(ChildAttributeMappingId);


            ParameterBO trades = new ParameterBO("@ItemDetail", itemDetailDt, SqlDbType.Structured);
            parameterList.Add(trades);

            ParameterBO itemDates = new ParameterBO("@ItemDates", itemDatesDt, SqlDbType.Structured);
            parameterList.Add(itemDates);


            ParameterBO conditionRating = new ParameterBO("@ConditionWork", conditionRatingDt, SqlDbType.Structured);
            parameterList.Add(conditionRating);

            ParameterBO MSATDetail = new ParameterBO("@MSATDetail", MSATDetailDt, SqlDbType.Structured);
            parameterList.Add(MSATDetail);

            ParameterBO outputChildAttributeMappingIdParam = new ParameterBO("outputChildAttributeMappingId", 0, DbType.Int32);
            outParametersList.Add(outputChildAttributeMappingIdParam);

            SaveRecord(ref parameterList, ref outParametersList, SpNameConstants.AmendAttributeItemDetail);

            ParameterBO obj = outParametersList.First();
            int  outvalue = 0;
            Int32.TryParse(obj.Value.ToString(), out outvalue);
            return outvalue;
        }
        #endregion

        #region "Save Service Charge Properties"
        public void saveServiceChargeProperties(ServiceChargeProperties serProperties, DataTable excludedIncludedProperties, int itemId, int attributeChildId)
        {
            ParameterList outParametersList = new ParameterList();
            ParameterList parameterList = new ParameterList();
            ParameterBO schemeIdParam = new ParameterBO("schemeId", serProperties.SchemeId, DbType.Int32);
            parameterList.Add(schemeIdParam);

            ParameterBO blockIdParam = new ParameterBO("blockId", serProperties.BlockId, DbType.Int32);
            parameterList.Add(blockIdParam);

            ParameterBO itemParam = new ParameterBO("itemId", itemId, DbType.Int32);
            parameterList.Add(itemParam);

            ParameterBO properties = new ParameterBO("@ItemDetail", excludedIncludedProperties, SqlDbType.Structured);
            parameterList.Add(properties);

            if (attributeChildId == 0)
            {
                ParameterBO attributeChildIdParam = new ParameterBO("@attributeChildId", null, SqlDbType.Int);
                parameterList.Add(attributeChildIdParam);
            }
            else
            {
                ParameterBO attributeChildIdParam = new ParameterBO("@attributeChildId", attributeChildId, SqlDbType.Int);
                parameterList.Add(attributeChildIdParam);
            }            

            ParameterList outParamList = new ParameterList();
            SaveRecord(ref parameterList, ref outParamList, SpNameConstants.SaveServiceChargeProperties);

        }
        #endregion

        #region "get Appliances List"

        public int getAppliancesList(ref DataSet resultDataSet, ref PageSortBO objPageSortBo, int? schemeId, int itemId, int? blockId)
        {
            ParameterList parametersList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO schemeIdParam = new ParameterBO("schemeId", schemeId, DbType.Int32);
            parametersList.Add(schemeIdParam);

            ParameterBO blockIdParam = new ParameterBO("blockId", blockId, DbType.Int32);
            parametersList.Add(blockIdParam);
            ParameterBO itemIdParam = new ParameterBO("itemId", itemId, DbType.String);
            parametersList.Add(itemIdParam);
            ParameterBO pageSizeColumn = new ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32);
            parametersList.Add(pageSizeColumn);

            ParameterBO pageNumberColumn = new ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32);
            parametersList.Add(pageNumberColumn);

            ParameterBO sortColumn = new ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String);
            parametersList.Add(sortColumn);

            ParameterBO sortOrder = new ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String);
            parametersList.Add(sortOrder);

            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);

            base.LoadDataSet(ref resultDataSet, ref parametersList, ref outParametersList, SpNameConstants.PropertyAppliances);
            ParameterBO obj = outParametersList.First();
            return Convert.ToInt32(obj.Value.ToString());
        }

        #endregion

        #region "get Appliance Locations"
        public DataSet getApplianceLocations(string prefix)
        {
            ParameterList parameterList = new ParameterList();
            ParameterBO prefixParam = new ParameterBO("prefix", prefix, DbType.String);
            parameterList.Add(prefixParam);
            DataSet resultDataSet = new DataSet();
            base.LoadDataSet(ref resultDataSet, ref parameterList, SpNameConstants.GetApplianceLocations);
            return resultDataSet;
        }
        #endregion

        #region "get Excluded properties by scheme and block"
        public DataSet getExPropertiesBySchemeBlock(int scheme, int block, int itemId, int childItemId)
        {
            ParameterList parameterList = new ParameterList();
            ParameterBO schemeParam;
            ParameterBO blockParam;
            if (scheme == 0)
            {
                schemeParam = new ParameterBO("schemeId", null, DbType.Int32);
            }
            else
            {
                schemeParam = new ParameterBO("schemeId", scheme, DbType.Int32);
            }
            parameterList.Add(schemeParam);
            if (block == 0)
            {
                blockParam = new ParameterBO("blockId", null, DbType.Int32);
            }
            else
            {
                blockParam = new ParameterBO("blockId", block, DbType.Int32);
            }
            parameterList.Add(blockParam);

            ParameterBO childItemParam;
            if (childItemId == 0)
            {
                childItemParam = new ParameterBO("childItemId", null, DbType.Int32);
            }
            else
            {
                childItemParam = new ParameterBO("childItemId", childItemId, DbType.Int32);
            }
            parameterList.Add(childItemParam);
            
            ParameterBO itemParam;
            itemParam = new ParameterBO("itemid", itemId, DbType.Int32);
            parameterList.Add(itemParam);

            DataSet resultDataSet = new DataSet();
            base.LoadDataSet(ref resultDataSet, ref parameterList, SpNameConstants.GetServiceChargeExProperties);
            return resultDataSet;
        }
        #endregion

        #region "get Applliance Type"
        public DataSet getApplianceType(string prefix)
        {
            ParameterList parametersList = new ParameterList();
            ParameterBO prefixParam = new ParameterBO("prefix", prefix, DbType.String);
            parametersList.Add(prefixParam);
            DataSet resultDataSet = new DataSet();
            LoadDataSet(ref resultDataSet, ref parametersList, SpNameConstants.GetApplianceType);
            return resultDataSet;
        }
        #endregion

        #region "get Make"
        public DataSet getMake(string prefix)
        {
            ParameterList parametersList = new ParameterList();
            ParameterBO prefixParam = new ParameterBO("prefix", prefix, DbType.String);
            parametersList.Add(prefixParam);
            DataSet resultDataSet = new DataSet();
            base.LoadDataSet(ref resultDataSet, ref parametersList, SpNameConstants.GetMake);
            return resultDataSet;
        }
        #endregion

        #region "Get Models List"

        public DataSet getModelsList(string prefix)
        {
            ParameterList parametersList = new ParameterList();
            ParameterBO prefixParam = new ParameterBO("prefix", prefix, DbType.String);
            parametersList.Add(prefixParam);
            DataSet resultDataSet = new DataSet();
            base.LoadDataSet(ref resultDataSet, ref parametersList, SpNameConstants.GetModelsList);
            return resultDataSet;
        }

        #endregion

        #region "Save  Appliance "

        public int saveAppliance(ApplianceBO objApplianceBo)
        {

            ParameterList parametersList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO schemeIdParam = new ParameterBO("schemeId", objApplianceBo.SchemeId, DbType.Int32);
            parametersList.Add(schemeIdParam);

            ParameterBO blockIdParam = new ParameterBO("blockId", objApplianceBo.BlockId, DbType.Int32);
            parametersList.Add(blockIdParam);

            ParameterBO locationIdParam = new ParameterBO("locationId", objApplianceBo.LocationId, DbType.Int32);
            parametersList.Add(locationIdParam);
            ParameterBO locationParam = new ParameterBO("location", objApplianceBo.Location, DbType.String);
            parametersList.Add(locationParam);

            ParameterBO typeIdParam = new ParameterBO("typeId", objApplianceBo.TypeId, DbType.Int32);
            parametersList.Add(typeIdParam);
            ParameterBO typeParam = new ParameterBO("type", objApplianceBo.Type, DbType.String);
            parametersList.Add(typeParam);


            ParameterBO makeIdParam = new ParameterBO("makeId", objApplianceBo.MakeId, DbType.Int32);
            parametersList.Add(makeIdParam);
            ParameterBO makeParam = new ParameterBO("make", objApplianceBo.Make, DbType.String);
            parametersList.Add(makeParam);

            ParameterBO modelIdParam = new ParameterBO("modelId", objApplianceBo.ModelId, DbType.Int32);
            parametersList.Add(modelIdParam);
            ParameterBO modelParam = new ParameterBO("model", objApplianceBo.Model, DbType.String);
            parametersList.Add(modelParam);

            ParameterBO itemIdParam = new ParameterBO("itemId", objApplianceBo.ItemId, DbType.Int32);
            parametersList.Add(itemIdParam);
            ParameterBO itemParam = new ParameterBO("item", objApplianceBo.Item, DbType.String);
            parametersList.Add(itemParam);
            ParameterBO quantityParam = new ParameterBO("quantity", objApplianceBo.Quantity, DbType.String);
            parametersList.Add(quantityParam);

            ParameterBO dimensionsParam = new ParameterBO("dimensions", objApplianceBo.Dimensions, DbType.String);
            parametersList.Add(dimensionsParam);

            ParameterBO serialNumberParam = new ParameterBO("serialNumber", objApplianceBo.SerialNumber, DbType.String);
            parametersList.Add(serialNumberParam);
            ParameterBO purchaseCostParam = new ParameterBO("purchaseCost", objApplianceBo.PurchaseCost, DbType.Decimal);
            parametersList.Add(purchaseCostParam);
            ParameterBO lifeSpanParam = new ParameterBO("lifeSpan", objApplianceBo.LifeSpan, DbType.Int32);
            parametersList.Add(lifeSpanParam);

            ParameterBO dateRemovedParam = new ParameterBO("dateRemoved", null, DbType.Date);


            ParameterBO datePurchasedParam = new ParameterBO("datePurchased", null, DbType.Date);

            if (!(objApplianceBo.Datepurchased == System.DateTime.MinValue))
            {
                datePurchasedParam.Value = objApplianceBo.Datepurchased;
            }
            if (!(objApplianceBo.DateRemoved == System.DateTime.MinValue))
            {
                dateRemovedParam.Value = objApplianceBo.DateRemoved;
            }
            parametersList.Add(datePurchasedParam);
            parametersList.Add(dateRemovedParam);
            ParameterBO notesParam = new ParameterBO("notes", objApplianceBo.Notes, DbType.String);
            parametersList.Add(notesParam);

            ParameterBO existingApplianceIdParam = new ParameterBO("existingApplianceId", objApplianceBo.ExistingApplianceId, DbType.Int32);
            parametersList.Add(existingApplianceIdParam);


            ParameterBO applianceId = new ParameterBO("applianceId", 0, DbType.Int32);
            outParametersList.Add(applianceId);

            outParametersList = SelectRecord(ref parametersList, ref  outParametersList, SpNameConstants.SavePropertyAppliance);

            ParameterBO obj = outParametersList.First();
            return Convert.ToInt32(obj.Value.ToString());
        }
        #endregion

        #region Service Charge Report
        public int GetServiceChargeReport(ref DataSet resultDataSet, PageSortBO objPageSortBO, int schemeId, int blockId,int itemId ,int fiscalYear, bool fetchAllRecords)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO schemeIdParam = new ParameterBO("filterSchemeId", schemeId, DbType.Int32);
            paramList.Add(schemeIdParam);

            ParameterBO blockIdParam = new ParameterBO("filterBlockId", blockId, DbType.Int32);
            paramList.Add(blockIdParam);

            ParameterBO fiscalYearParam = new ParameterBO("fiscalYear", fiscalYear, DbType.Int32);
            paramList.Add(fiscalYearParam);

            ParameterBO itemIdParam = new ParameterBO("filterItemId", itemId, DbType.Int32);
            paramList.Add(itemIdParam);

            ParameterBO fetchAllRecordsParam = new ParameterBO("fetchAllRecords", fetchAllRecords, DbType.Boolean);
            paramList.Add(fetchAllRecordsParam);

            ParameterBO pageNumberParam = new ParameterBO("pageNumber", objPageSortBO.PageNumber, DbType.Int32);
            paramList.Add(pageNumberParam);

            ParameterBO pageSizeParam = new ParameterBO("pageSize", objPageSortBO.PageSize, DbType.Int32);
            paramList.Add(pageSizeParam);

            ParameterBO SortDirectionParam = new ParameterBO("sortOrder", objPageSortBO.SmallSortDirection, DbType.String);
            paramList.Add(SortDirectionParam);

            ParameterBO SortExpressionParam = new ParameterBO("sortColumn", objPageSortBO.SortExpression, DbType.String);
            paramList.Add(SortExpressionParam);

            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);

            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetServiceChargeReport);

            ParameterBO obj = outParametersList.First();
            Int32 outvalue = Convert.ToInt32(obj.Value.ToString());

            return outvalue;

        }

        public int GetServiceChargeReportPO(ref DataSet resultDataSet, PageSortBO objPageSortBO, int schemeId, int blockId, int fiscalYear)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO schemeIdParam = new ParameterBO("filterSchemeId", schemeId, DbType.Int32);
            paramList.Add(schemeIdParam);

            ParameterBO blockIdParam = new ParameterBO("filterBlockId", blockId, DbType.Int32);
            paramList.Add(blockIdParam);

            ParameterBO fiscalYearParam = new ParameterBO("fiscalYear", fiscalYear, DbType.Int32);
            paramList.Add(fiscalYearParam);

            ParameterBO pageNumberParam = new ParameterBO("PageNumber", objPageSortBO.PageNumber, DbType.Int32);
            paramList.Add(pageNumberParam);

            ParameterBO pageSizeParam = new ParameterBO("PageSize", objPageSortBO.PageSize, DbType.Int32);
            paramList.Add(pageSizeParam);

            ParameterBO SortDirectionParam = new ParameterBO("sortOrder", objPageSortBO.SmallSortDirection, DbType.String);
            paramList.Add(SortDirectionParam);

            ParameterBO SortExpressionParam = new ParameterBO("sortColumn", objPageSortBO.SortExpression, DbType.String);
            paramList.Add(SortExpressionParam);

            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);

            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetServiceChargeReportPO);

            ParameterBO obj = outParametersList.First();
            Int32 outvalue = Convert.ToInt32(obj.Value.ToString());

            return outvalue;

        }

        public int GetServiceChargeReportDC(ref DataSet resultDataSet, PageSortBO objPageSortBO, string filterText, int fiscalYear)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO filterTextParam = new ParameterBO("filterText", filterText, DbType.String);
            paramList.Add(filterTextParam);

            ParameterBO fiscalYearParam = new ParameterBO("fiscalYear", fiscalYear, DbType.Int32);
            paramList.Add(fiscalYearParam);

            ParameterBO pageNumberParam = new ParameterBO("PageNumber", objPageSortBO.PageNumber, DbType.Int32);
            paramList.Add(pageNumberParam);

            ParameterBO pageSizeParam = new ParameterBO("PageSize", objPageSortBO.PageSize, DbType.Int32);
            paramList.Add(pageSizeParam);

            ParameterBO SortDirectionParam = new ParameterBO("sortOrder", objPageSortBO.SmallSortDirection, DbType.String);
            paramList.Add(SortDirectionParam);

            ParameterBO SortExpressionParam = new ParameterBO("sortColumn", objPageSortBO.SortExpression, DbType.String);
            paramList.Add(SortExpressionParam);

            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);

            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetServiceChargeReportDC);

            ParameterBO obj = outParametersList.First();
            Int32 outvalue = Convert.ToInt32(obj.Value.ToString());

            return outvalue;

        }
        #endregion


        public DataSet getDetectorByPropertyId(int schemeId, int blockId, string detectorType)
        {
            ParameterList parametersList = new ParameterList();
            ParameterBO schemeIdParam = new ParameterBO("schemeId", schemeId, DbType.Int32);
            parametersList.Add(schemeIdParam);

            ParameterBO blockIdParam = new ParameterBO("blockId", blockId, DbType.Int32);
            parametersList.Add(blockIdParam);

            ParameterBO detectorTypeParam = new ParameterBO("detectorType", detectorType, DbType.String);
            parametersList.Add(detectorTypeParam);

            DataSet resultDataSet = new DataSet();

            DataTable dtDetectors = new DataTable();
            dtDetectors.TableName = "Detectors";
            resultDataSet.Tables.Add(dtDetectors);


            DataTable dtPowerSource = new DataTable();
            dtPowerSource.TableName = "PowerSource";
            resultDataSet.Tables.Add(dtPowerSource);

            IDataReader iResultDataReader = base.SelectRecord(parametersList, SpNameConstants.GetDetectorBySchemeBlockId);
            resultDataSet.Load(iResultDataReader, LoadOption.OverwriteChanges, dtDetectors, dtPowerSource);
            iResultDataReader.Close();
            return resultDataSet;
        }


        public bool saveDetectors(DetectorsBO objDetectorBo, string smokeDetectorType, string noOfSmokeDetectors, int InstalledBy)
        {

            ParameterList inParametersList = new ParameterList();
            inParametersList.Add(new ParameterBO("SchemeId", objDetectorBo.SchemeId, DbType.Int32));
            inParametersList.Add(new ParameterBO("BlockId", objDetectorBo.BlockId, DbType.Int32));
            inParametersList.Add(new ParameterBO("detectorType", objDetectorBo.DetectorType, DbType.String));
            inParametersList.Add(new ParameterBO("Location", objDetectorBo.Location, DbType.String));
            inParametersList.Add(new ParameterBO("Manufacturer", objDetectorBo.Manufacturer, DbType.String));
            inParametersList.Add(new ParameterBO("SerialNumber", objDetectorBo.SerialNumber, DbType.String));
            inParametersList.Add(new ParameterBO("PowerSource", objDetectorBo.PowerSource, DbType.Int32));
            inParametersList.Add(new ParameterBO("InstalledDate", objDetectorBo.InstalledDate, DbType.DateTime));
            inParametersList.Add(new ParameterBO("InstalledBy", InstalledBy, DbType.Int32));
            inParametersList.Add(new ParameterBO("IsLandlordsDetector", objDetectorBo.IsLandlordsDetector, DbType.Boolean));
            inParametersList.Add(new ParameterBO("TestedDate", objDetectorBo.LastTestedDate, DbType.DateTime));

            inParametersList.Add(new ParameterBO("BatteryReplaced", objDetectorBo.BatteryReplaced, DbType.DateTime));
            inParametersList.Add(new ParameterBO("Passed", objDetectorBo.IsPassed, DbType.Boolean));
            inParametersList.Add(new ParameterBO("Notes", objDetectorBo.Notes, DbType.String));
            inParametersList.Add(new ParameterBO("smokeDetectorType", smokeDetectorType, DbType.String));
            inParametersList.Add(new ParameterBO("noOfSmokeDetector", noOfSmokeDetectors, DbType.String));
            // inParametersList.Add(New ParameterBO("userId", SessionManager.getAppServicingUserId(), DbType.Int32))

            ParameterList outParametersList = new ParameterList();

            ParameterBO saveStatusParam = new ParameterBO("saveStatus", false, DbType.Boolean);
            outParametersList.Add(saveStatusParam);

            base.SaveRecord(ref inParametersList, ref outParametersList, SpNameConstants.SavedetectorsForSchemeBlock);

            return Convert.ToBoolean(saveStatusParam.Value);
        }

        public DataSet getCyclingServicesContractor(int schemeId, int blockId, string msatType)
        {
            ParameterList parametersList = new ParameterList();
            ParameterBO schemeIdParam = new ParameterBO("schemeId", schemeId, DbType.Int32);
            parametersList.Add(schemeIdParam);

            ParameterBO blockIdParam = new ParameterBO("blockId", blockId, DbType.Int32);
            parametersList.Add(blockIdParam);

            ParameterBO detectorTypeParam = new ParameterBO("msat", msatType, DbType.String);
            parametersList.Add(detectorTypeParam);

            DataSet resultDataSet = new DataSet();

            DataTable dtContractor = new DataTable();
            dtContractor.TableName = "Contractor";
            resultDataSet.Tables.Add(dtContractor);

            IDataReader iResultDataReader = base.SelectRecord(parametersList, SpNameConstants.GetContractorDropDownValues);
            resultDataSet.Load(iResultDataReader, LoadOption.OverwriteChanges, dtContractor);
            iResultDataReader.Close();
            return resultDataSet;
        }
        public DataSet getBlocksByScheme(int schemeId)
        {
            ParameterList parametersList = new ParameterList();
            ParameterBO schemeIdParam = new ParameterBO("schemeId", schemeId, DbType.Int32);
            parametersList.Add(schemeIdParam);

            DataSet resultDataSet = new DataSet();

            DataTable dtBlock = new DataTable();
            dtBlock.TableName = "Blocks";
            resultDataSet.Tables.Add(dtBlock);

            IDataReader iResultDataReader = base.SelectRecord(parametersList, SpNameConstants.GetBlocksByScheme);
            resultDataSet.Load(iResultDataReader, LoadOption.OverwriteChanges, dtBlock);
            iResultDataReader.Close();
            return resultDataSet;
        }

        public DataSet getPropertiesBySchemeBlock(int schemeId, int blockId)
        {
            ParameterList parametersList = new ParameterList();
            ParameterBO schemeIdParam = new ParameterBO("schemeId", schemeId, DbType.Int32);
            parametersList.Add(schemeIdParam);

            ParameterBO blockIdParam = new ParameterBO("blockId", blockId, DbType.Int32);
            parametersList.Add(blockIdParam);

            DataSet resultDataSet = new DataSet();

            DataTable dtProperty = new DataTable();
            dtProperty.TableName = "Properties";
            resultDataSet.Tables.Add(dtProperty);

            IDataReader iResultDataReader = base.SelectRecord(parametersList, SpNameConstants.GetPropertyByBlock);
            resultDataSet.Load(iResultDataReader, LoadOption.OverwriteChanges, dtProperty);
            iResultDataReader.Close();
            return resultDataSet;
        }


        public DataSet getGeneralJournalExcludedProperties(int txnId)
        {
            ParameterList parametersList = new ParameterList();

            ParameterBO txnIdParam = new ParameterBO("txnId", txnId, DbType.Int32);
            parametersList.Add(txnIdParam);

            DataSet resultDataSet = new DataSet();

            DataTable dtProperty = new DataTable();
            dtProperty.TableName = "Properties";
            resultDataSet.Tables.Add(dtProperty);

            IDataReader iResultDataReader = base.SelectRecord(parametersList, SpNameConstants.GetGeneralJournalExProperties);
            resultDataSet.Load(iResultDataReader, LoadOption.OverwriteChanges, dtProperty);
            iResultDataReader.Close();
            return resultDataSet;
        }

        public DataSet getPurchaseItemExcludedProperties(int orderItemId)
        {
            ParameterList parametersList = new ParameterList();

            ParameterBO orderItemIdParam = new ParameterBO("orderItemId", orderItemId, DbType.Int32);
            parametersList.Add(orderItemIdParam);

            DataSet resultDataSet = new DataSet();

            DataTable dtProperty = new DataTable();
            dtProperty.TableName = "Properties";
            resultDataSet.Tables.Add(dtProperty);

            IDataReader iResultDataReader = base.SelectRecord(parametersList, SpNameConstants.GetPurchaseItemExProperties);
            resultDataSet.Load(iResultDataReader, LoadOption.OverwriteChanges, dtProperty);
            iResultDataReader.Close();
            return resultDataSet;
        }

        public DataSet getSelectedProperties(int schemeId, int blockId, int itemId)
        {
            ParameterList parametersList = new ParameterList();
            ParameterBO schemeParam;
            ParameterBO blockParam;
            if (schemeId == 0)
            {
                schemeParam = new ParameterBO("schemeId", null, DbType.Int32);
            }
            else
            {
                schemeParam = new ParameterBO("schemeId", schemeId, DbType.Int32);
            }
            parametersList.Add(schemeParam);
            if (blockId == 0)
            {
                blockParam = new ParameterBO("blockId", null, DbType.Int32);
            }
            else
            {
                blockParam = new ParameterBO("blockId", blockId, DbType.Int32);
            }
            parametersList.Add(blockParam);

            ParameterBO itemIdParam;

            itemIdParam = new ParameterBO("itemId", itemId, DbType.Int32);
            parametersList.Add(itemIdParam);

            DataSet resultDataSet = new DataSet();

            DataTable dtProperty = new DataTable();
            dtProperty.TableName = "Properties";
            resultDataSet.Tables.Add(dtProperty);

            IDataReader iResultDataReader = base.SelectRecord(parametersList, SpNameConstants.GetServiceChargeSelectedProperties);
            resultDataSet.Load(iResultDataReader, LoadOption.OverwriteChanges, dtProperty);
            iResultDataReader.Close();
            return resultDataSet;
        }

        public DataSet getFiscalYears()
        {
            DataSet resultDataSet = new DataSet();
            DataTable dtBlock = new DataTable();
            dtBlock.TableName = "FiscalYears";
            resultDataSet.Tables.Add(dtBlock);
            IDataReader iResultDataReader = base.SelectRecord(null, SpNameConstants.GetFiscalYears);
            resultDataSet.Load(iResultDataReader, LoadOption.OverwriteChanges, dtBlock);
            iResultDataReader.Close();
            return resultDataSet;

        }

        public void getSchemeBlockAppliances(string Id, ref DataSet resultDataSet)
        {
            ParameterList parameterList = new ParameterList();
            ApplianceDefectBO objAppliaceDefectBO = new ApplianceDefectBO();

            int id = Convert.ToInt32(Id);
            ParameterBO IdParam = new ParameterBO("id", id, DbType.Int32);
            parameterList.Add(IdParam);

            DataTable dtProperty = new DataTable();
            dtProperty.TableName = "Appliances";
            resultDataSet.Tables.Add(dtProperty);

            IDataReader iResultDataReader = base.SelectRecord(parameterList, SpNameConstants.GetSchemeBlockDefectsById);
            resultDataSet.Load(iResultDataReader, LoadOption.OverwriteChanges, dtProperty);
            iResultDataReader.Close();

            if (resultDataSet.Tables[0].Rows.Count > 0)
            {
                objAppliaceDefectBO.DefectDate = DateTime.Parse(resultDataSet.Tables[0].Rows[0].ItemArray[0].ToString(), new CultureInfo("en-GB"));
                objAppliaceDefectBO.IsDefectIdentified = (bool)resultDataSet.Tables[0].Rows[0].ItemArray[2];
                objAppliaceDefectBO.IsRemedialActionTaken = (bool)resultDataSet.Tables[0].Rows[0].ItemArray[3];
                objAppliaceDefectBO.CategoryDescription = (string)resultDataSet.Tables[0].Rows[0].ItemArray[4];

            }
        }

        public void getDefectManagementLookupValues(string Id, string requestType, ref DataSet resultDataSet)
        {
            ParameterList parameterList = new ParameterList();

            int id = Convert.ToInt32(Id);
            ParameterBO IdParam = new ParameterBO("id", id, DbType.Int32);
            parameterList.Add(IdParam);

            ParameterBO requestTypeParam = new ParameterBO("requestType", requestType, DbType.String);
            parameterList.Add(requestTypeParam);

            DataTable Category = new DataTable();
            Category.TableName = "Category";
            DataTable Appliance = new DataTable();
            Appliance.TableName = "Appliance";
            DataTable EmployeeList = new DataTable();
            EmployeeList.TableName = "EmployeeList";
            DataTable DefectPriorities = new DataTable();
            DefectPriorities.TableName = "DefectPriorities";
            DataTable Trades = new DataTable();
            Trades.TableName = "Trades";
            DataTable Boilers = new DataTable();
            Boilers.TableName = "Boilers";

            resultDataSet.Tables.Add(Category);
            resultDataSet.Tables.Add(Appliance);
            resultDataSet.Tables.Add(EmployeeList);
            resultDataSet.Tables.Add(DefectPriorities);
            resultDataSet.Tables.Add(Trades);
            resultDataSet.Tables.Add(Boilers);

            IDataReader iResultDataReader = base.SelectRecord(parameterList, SpNameConstants.GetDefectManagementLoopkupsData);
            resultDataSet.Load(iResultDataReader, LoadOption.OverwriteChanges, Category, Appliance, EmployeeList, DefectPriorities, Trades, Boilers);
            iResultDataReader.Close();

            //base.LoadDataSet(ref resultDataSet, ref parameterList, SpNameConstants.GetDefectManagementLoopkupsData);
        }

        #region get Scheme/Block Defect Details
        public void getSchemeBlockDefectDetails(int propertyDefectId, ref ApplianceDefectBO objApplianceDefectBO)
        {
            ParameterList parameterList = new ParameterList();

            parameterList.Add(new ParameterBO("propertyDefectId", propertyDefectId, DbType.Int32));

            var lDataReader = SelectRecord(parameterList, SpNameConstants.getSchemeBlockDefectDetails);

            if (lDataReader.Read())
            {
                objApplianceDefectBO.PropertyDefectId = (int)lDataReader["PropertyDefectId"];
                objApplianceDefectBO.Id = (string)lDataReader["Id"];
                objApplianceDefectBO.RequestType = (string)lDataReader["RequestType"];
                objApplianceDefectBO.CategoryId = (int)lDataReader["CategoryId"];
                objApplianceDefectBO.CategoryDescription = (string)lDataReader["DefectCategory"];

                objApplianceDefectBO.DefectDate = DateTime.Now;
                if (!Convert.IsDBNull(lDataReader["DefectDate"]))
                {
                    objApplianceDefectBO.DefectDate = (DateTime)lDataReader["DefectDate"];
                }

                objApplianceDefectBO.JournalId = (int)lDataReader["JournalId"];
                objApplianceDefectBO.IsDefectIdentified = (bool)lDataReader["IsDefectIdentified"];
                objApplianceDefectBO.DefectIdentifiedNotes = (string)lDataReader["DefectNotes"];
                objApplianceDefectBO.IsRemedialActionTaken = (bool)lDataReader["IsRemedialActionTaken"];
                objApplianceDefectBO.RemedialActionNotes = (string)lDataReader["RemedialActionNotes"];
                objApplianceDefectBO.IsWarningNoteIssued = (bool)lDataReader["IsWarningIssued"];

                objApplianceDefectBO.ApplianceId = 0;
                if (!Convert.IsDBNull(lDataReader["ApplianceId"]))
                {
                    objApplianceDefectBO.ApplianceId = (int)lDataReader["ApplianceId"];
                }

                objApplianceDefectBO.Boiler = "0";
                if (!Convert.IsDBNull(lDataReader["BOILER"]))
                {
                    objApplianceDefectBO.Boiler = (string)lDataReader["BOILER"];
                }

                objApplianceDefectBO.SerialNumber = (string)lDataReader["SerialNumber"];
                objApplianceDefectBO.GcNumber = (string)lDataReader["GcNumber"];
                objApplianceDefectBO.IsWarningTagFixed = (bool)lDataReader["IsWarningFixed"];

                objApplianceDefectBO.IsAppliancedDisconnected = null;
                if (!Convert.IsDBNull(lDataReader["IsApplianceDisconnected"]))
                {
                    objApplianceDefectBO.IsAppliancedDisconnected = (bool)lDataReader["IsApplianceDisconnected"];
                }

                objApplianceDefectBO.IsPartsRequired = null;
                if (!Convert.IsDBNull(lDataReader["IsPartsRequired"]))
                {
                    objApplianceDefectBO.IsPartsRequired = (bool)lDataReader["IsPartsRequired"];
                }

                objApplianceDefectBO.IsPartsOrdered = null;
                if (!Convert.IsDBNull(lDataReader["IsPartsOrdered"]))
                {
                    objApplianceDefectBO.IsPartsOrdered = (bool)lDataReader["IsPartsOrdered"];
                }

                objApplianceDefectBO.PartsOrderedBy = null;
                if (!Convert.IsDBNull(lDataReader["PartsOrderedBy"]))
                {
                    objApplianceDefectBO.PartsOrderedBy = (int)lDataReader["PartsOrderedBy"];
                }

                objApplianceDefectBO.PartsOrderedByName = (string)lDataReader["PartsOrderedByName"];

                objApplianceDefectBO.PartsDueDate = null;
                if (!Convert.IsDBNull(lDataReader["PartsDueDate"]))
                {
                    objApplianceDefectBO.PartsDueDate = (DateTime)lDataReader["PartsDueDate"];
                }

                objApplianceDefectBO.PartsDescription = (string)lDataReader["PartsDescription"];
                objApplianceDefectBO.PartsLocation = (string)lDataReader["PartsLocation"];
                objApplianceDefectBO.IsTwoPersonsJob = (bool)lDataReader["IsTwoPersonsJob"];
                objApplianceDefectBO.ReasonForSecondPerson = (string)lDataReader["ReasonForSecondPerson"];

                objApplianceDefectBO.EstimatedDuration = null;
                if (!Convert.IsDBNull(lDataReader["EstimatedDuration"]))
                {
                    objApplianceDefectBO.EstimatedDuration = (decimal)lDataReader["EstimatedDuration"];
                }

                objApplianceDefectBO.PriorityId = null;
                if (!Convert.IsDBNull(lDataReader["PriorityId"]))
                {
                    objApplianceDefectBO.PriorityId = (int)lDataReader["PriorityId"];
                }

                objApplianceDefectBO.PriorityName = (string)lDataReader["PriorityName"];

                objApplianceDefectBO.TradeId = null;
                if (!Convert.IsDBNull(lDataReader["TradeId"]))
                {
                    objApplianceDefectBO.TradeId = (int)lDataReader["TradeId"];
                }

                objApplianceDefectBO.Trade = (string)lDataReader["Trade"];
                objApplianceDefectBO.PhotosNotes = (string)lDataReader["PhotoNotes"];
                objApplianceDefectBO.Appliance = (string)lDataReader["APPLIANCE"];
            }
        }
        #endregion

        #region load Scheme/Block Appliances DDL
        public void loadSchemeBlockAppliancesDDL(string Id, string requestType, ref DataSet resultDataSet)
        {
            ParameterList parameterList = new ParameterList();

            int id = Convert.ToInt32(Id);
            ParameterBO IdParam = new ParameterBO("id", id, DbType.Int32);
            parameterList.Add(IdParam);

            ParameterBO requestTypeParam = new ParameterBO("requestType", requestType, DbType.String);
            parameterList.Add(requestTypeParam);

            base.LoadDataSet(ref resultDataSet, ref parameterList, SpNameConstants.getSchemeBlockAppliances);
        }
        #endregion

        #region  load Category DDL
        public void loadCategoryDDL(ref DataSet resultDataSet)
        {
            ParameterList parameterList = new ParameterList();

            base.LoadDataSet(ref resultDataSet, ref parameterList, SpNameConstants.getPropertyCategory);
        }
        #endregion

        #region save Defect

        public void saveDefect(ApplianceDefectBO objDefectBO)
        {
            ParameterList parameterList = new ParameterList();
            ParameterList outParameterList = new ParameterList();

            int id = Convert.ToInt32(objDefectBO.Id);
            parameterList.Add(new ParameterBO("propertyDefectId", objDefectBO.PropertyDefectId, DbType.Int32));
            parameterList.Add(new ParameterBO("HeatingMappingId", objDefectBO.HeatingMappingId, DbType.Int32));
            parameterList.Add(new ParameterBO("id", id, DbType.Int32));
            parameterList.Add(new ParameterBO("requestType", objDefectBO.RequestType, DbType.String));
            parameterList.Add(new ParameterBO("CategoryId", objDefectBO.CategoryId, DbType.Int32));
            parameterList.Add(new ParameterBO("DefectDate", objDefectBO.DefectDate, DbType.DateTime));
            parameterList.Add(new ParameterBO("isDefectIdentified", objDefectBO.IsDefectIdentified, DbType.Boolean));
            parameterList.Add(new ParameterBO("DefectIdentifiedNotes", objDefectBO.DefectIdentifiedNotes, DbType.String));
            parameterList.Add(new ParameterBO("isRemedialActionTaken", objDefectBO.IsRemedialActionTaken, DbType.Boolean));
            parameterList.Add(new ParameterBO("remedialActionNotes", objDefectBO.RemedialActionNotes, DbType.String));
            parameterList.Add(new ParameterBO("isWarningIssued", objDefectBO.IsWarningNoteIssued, DbType.Boolean));
            parameterList.Add(new ParameterBO("serialNumber", objDefectBO.SerialNumber, DbType.String));
            parameterList.Add(new ParameterBO("GcNumber", objDefectBO.GcNumber, DbType.String));
            parameterList.Add(new ParameterBO("isWarningFixed", objDefectBO.IsWarningTagFixed, DbType.Boolean));
            parameterList.Add(new ParameterBO("isApplianceDisconnected", objDefectBO.IsAppliancedDisconnected, DbType.Boolean));
            parameterList.Add(new ParameterBO("isPartsRequired", objDefectBO.IsPartsOrdered, DbType.Boolean));
            parameterList.Add(new ParameterBO("isPartsOrdered", objDefectBO.IsPartsOrdered, DbType.Boolean));
            parameterList.Add(new ParameterBO("partsOrderedBy", objDefectBO.PartsOrderedBy, DbType.Int32));
            parameterList.Add(new ParameterBO("partsDueDate", objDefectBO.PartsDueDate, DbType.Date));
            parameterList.Add(new ParameterBO("partsDescription", objDefectBO.PartsDescription, DbType.String));
            parameterList.Add(new ParameterBO("partsLocation", objDefectBO.PartsLocation, DbType.String));
            parameterList.Add(new ParameterBO("isTwoPersonsJob", objDefectBO.IsTwoPersonsJob, DbType.Boolean));
            parameterList.Add(new ParameterBO("reasonForSecondPerson", objDefectBO.ReasonForSecondPerson, DbType.String));
            parameterList.Add(new ParameterBO("estimatedDuration", objDefectBO.EstimatedDuration, DbType.Decimal));
            parameterList.Add(new ParameterBO("priorityId", objDefectBO.PriorityId, DbType.Int32));
            parameterList.Add(new ParameterBO("tradeId", objDefectBO.TradeId, DbType.Int32));
            parameterList.Add(new ParameterBO("filePath", objDefectBO.FilePath, DbType.String));
            parameterList.Add(new ParameterBO("photoName", objDefectBO.PhotoName, DbType.String));
            parameterList.Add(new ParameterBO("PhotoNotes", objDefectBO.PhotosNotes, DbType.String));
            parameterList.Add(new ParameterBO("userID", objDefectBO.UserId, DbType.Int32));

            base.SaveRecord(ref parameterList, ref outParameterList, SpNameConstants.saveDefect);
        }

        #endregion

        #region Get Scheme/Block Images
        public void getSchemeBlockDefectImages(string Id, string requestType, ref DataSet photosDataSet)
        {
            ParameterList parameterList = new ParameterList();

            int id = Convert.ToInt32(Id);
            ParameterBO IdParam = new ParameterBO("id", id, DbType.Int32);
            parameterList.Add(IdParam);

            ParameterBO requestTypeParam = new ParameterBO("requestType", requestType, DbType.String);
            parameterList.Add(requestTypeParam);

            base.LoadDataSet(ref photosDataSet, ref parameterList, SpNameConstants.getSchemeBlockDefectImages);
        }
        #endregion

        #region Get Scheme/Block Images
        /// <summary>
        /// Get Scheme/Block Images
        /// </summary>
        /// <remarks></remarks>

        public void getSchemeBlockImages(ref DataSet resultDataSet, string Id, string requestType, int itemId)
        {

            ParameterList parametersList = new ParameterList();
            ParameterList outParamList = new ParameterList();
            int id = Convert.ToInt32(Id);
            ParameterBO ItemIdParam = new ParameterBO("itemId", itemId, DbType.Int32);
            parametersList.Add(ItemIdParam);

            ParameterBO IdParam;
            if (requestType == ApplicationConstants.Block)
            {
                IdParam = new ParameterBO("blockId", id, DbType.Int32);
            }
            else
            {
                IdParam = new ParameterBO("schemeId", id, DbType.Int32);
            }
            parametersList.Add(IdParam);
            base.LoadDataSet(ref resultDataSet, ref parametersList, SpNameConstants.getPropertyImages);
        }

        #endregion

        #region Save Photographs
        public void savePhotograph(PhotographBO objPhotographBO)
        {
            ParameterList parameterList = new ParameterList();
            ParameterList outparameterList = new ParameterList();


            ParameterBO itemIdParam = default(ParameterBO);
            if (objPhotographBO.ItemId == 0)
            {
                itemIdParam = new ParameterBO("itemId", DBNull.Value, DbType.Int32);
            }
            else
            {
                itemIdParam = new ParameterBO("itemId", objPhotographBO.ItemId, DbType.Int32);
            }
            parameterList.Add(itemIdParam);
            int id = Convert.ToInt32(objPhotographBO.Id);
            ParameterBO proerptyIdParam = new ParameterBO("Id", id, DbType.Int32);
            parameterList.Add(proerptyIdParam);

            ParameterBO FilePathParam = new ParameterBO("imagePath", objPhotographBO.FilePath, DbType.String);
            parameterList.Add(FilePathParam);

            ParameterBO ImageName = new ParameterBO("imageName", objPhotographBO.ImageName, DbType.String);
            parameterList.Add(ImageName);

            ParameterBO Title = new ParameterBO("title", objPhotographBO.Title, DbType.String);
            parameterList.Add(Title);

            ParameterBO UploadDate = new ParameterBO("uploadDate", objPhotographBO.UploadDate, DbType.DateTime);
            parameterList.Add(UploadDate);

            ParameterBO RequestTypeParam = new ParameterBO("requestType", objPhotographBO.RequestType, DbType.String);
            parameterList.Add(RequestTypeParam);

            ParameterBO CreatedBy = new ParameterBO("createdBy", objPhotographBO.CreatedBy, DbType.Int32);
            parameterList.Add(CreatedBy);
            ParameterBO IsDefaultImage = new ParameterBO("isDefaultImage", objPhotographBO.IsDefaultImage, DbType.Boolean);
            parameterList.Add(IsDefaultImage);

            base.SaveRecord(ref parameterList, ref outparameterList, SpNameConstants.savePhotograph);
        }
        #endregion

    }
}
