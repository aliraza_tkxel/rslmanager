﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using PDR_BusinessObject.PageSort;
using PDR_BusinessObject.SchemeBlock;

namespace PDR_DataAccess.SchemeBlock
{
    public interface IProvisionsRepo
    {
        DataSet GetProvisionsData(int? schemeId, int? blockId);
        DataSet LoadCycleTypeDDL();
        DataSet GetProvisionsDataById(int provisionId);
        DataSet GetProvisionItemHistory(int provisionId, int? schemeId, int? blockId);
        string SaveProvsionItemDetail(ProvisionsBO objProvisionItemBO, DataTable MSATDetailDt);
        string UpdateProvsionItemDetail(ProvisionsBO objProvisionItemBO, DataTable MSATDetailDt);
        DataSet GetProvisionNotes(int? schemeId, int? blockId, int provisionId);
        void SaveProvisionNotes(ProvisionNotesBO objProvisionNotesBO);
        DataSet GetProvisionImages(int provisionId, int? schemeId, int? blockId);
        void SaveProvisionImages(ProvisionPhotographsBO objProvisionPhotoBO);
        int GetProvisioningReport(ref DataSet resultDataSet, PageSortBO objPageSortBO, string filterText, int categoryId);
        DataSet GetSupplierList();
        DataSet GetProvisionCategories();
        void SaveProvisionChargeProperties(ProvisionChargeProperties serProperties, DataTable excludedIncludedProperties, int itemId);
        DataSet getSavedProvisionChargeProperties(int schemeId, int blockId, int itemId);
        DataSet getExPropertiesBySchemeBlock(int scheme, int block, int itemId);
        DataSet GetProvisionsByCategories(int categoryId, int? schemeId, int? blockId);
        DataSet GetProvisionByProvisionId(int provisionId);
    }
}
