﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PDR_BusinessObject.Parameter;
using PDR_DataAccess.Base;
using System.Data;
using PDR_Utilities.Constants;
using PDR_BusinessObject.SchemeBlock;
using PDR_BusinessObject.PageSort;

namespace PDR_DataAccess.SchemeBlock
{
    public class DocumentsRepo : BaseDAL, IDocumentsRpo
    {
        #region "Get Document Types"


        public DataSet getDocumentTypes(bool ShowActiveOnly, string reportFor = "", int categoryId=0)
        {
            ParameterList parametersList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO propertyIdParam = new ParameterBO("ShowActiveOnly", ShowActiveOnly, DbType.Boolean);
            parametersList.Add(propertyIdParam);

            ParameterBO reportForParam = new ParameterBO("reportFor", reportFor, DbType.String);
            parametersList.Add(reportForParam);

            ParameterBO categoryIdParam = new ParameterBO("categoryId", categoryId, DbType.Int32);
            parametersList.Add(categoryIdParam);

            DataSet resultDataSet = new DataSet();
            LoadDataSet(ref resultDataSet, ref parametersList, ref outParametersList, SpNameConstants.PdrGetDocumentTypes);


            return resultDataSet;
        }

        #endregion

        #region "Get Document SubTypes"


        public DataSet getDocumentSubtypes(int DocumentTypeId, bool ShowActiveOnly, string reportFor = "")
        {
            ParameterList parametersList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO ShowActiveOnlyParam = new ParameterBO("ShowActiveOnly", ShowActiveOnly, DbType.Boolean);
            ParameterBO DocumentTypeIdParam = new ParameterBO("DocumentTypeId", DocumentTypeId, DbType.Int32);
            parametersList.Add(ShowActiveOnlyParam);
            parametersList.Add(DocumentTypeIdParam);

            if (reportFor != "") 
            {
                ParameterBO reportForParam  = new ParameterBO("reportFor", reportFor, DbType.String);
                parametersList.Add(reportForParam);
            }
            DataSet resultDataSet = new DataSet();
            LoadDataSet(ref resultDataSet, ref parametersList, ref outParametersList, SpNameConstants.GetDocumentSubtypes);


            return resultDataSet;
        }

        #endregion




        #region "Save Property Document Upload"


        public void saveDocumentUpload(DocumentsBO objPropertyDocumentBO)
        {
            ParameterList parametersList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO paramCategory = new ParameterBO("documentCategory", objPropertyDocumentBO.Category, DbType.String);
            parametersList.Add(paramCategory);

            ParameterBO typeParam = new ParameterBO("DocumentTypeId", objPropertyDocumentBO.DocumentTypeId, DbType.Int32);
            parametersList.Add(typeParam);

            ParameterBO subtypeParam = new ParameterBO("DocumentSubTypeId", objPropertyDocumentBO.DocumentSubTypeId, DbType.Int32);
            parametersList.Add(subtypeParam);

            ParameterBO fileNameParam = new ParameterBO("documentName", objPropertyDocumentBO.DocumentName, DbType.String);
            parametersList.Add(fileNameParam);

            ParameterBO filePathParam = new ParameterBO("documentPath", objPropertyDocumentBO.DocumentPath, DbType.String);
            parametersList.Add(filePathParam);

            ParameterBO keywordParam = new ParameterBO("keyword", objPropertyDocumentBO.Keyword, DbType.String);
            parametersList.Add(keywordParam);

            ParameterBO sizeParam = new ParameterBO("documentSize", objPropertyDocumentBO.DocumentSize, DbType.String);
            parametersList.Add(sizeParam);

            ParameterBO formatParam = new ParameterBO("documentFormat", objPropertyDocumentBO.DocumentFormat, DbType.String);
            parametersList.Add(formatParam);
            ParameterBO schemeIdParam = new ParameterBO("schemeId", objPropertyDocumentBO.SchemeId, DbType.Int32);
            parametersList.Add(schemeIdParam);
            ParameterBO blockIdParam = new ParameterBO("blockId", objPropertyDocumentBO.BlockId, DbType.Int32);
            parametersList.Add(blockIdParam);


            ParameterBO uploadedByParam = new ParameterBO("uploadedBy", objPropertyDocumentBO.UploadedBy, DbType.Int32);
            parametersList.Add(uploadedByParam);
            ParameterBO expiryDateParam = new ParameterBO("expiryDate", objPropertyDocumentBO.ExpiryDate, DbType.DateTime );
            parametersList.Add(expiryDateParam);
            ParameterBO documentDateParam = new ParameterBO("documentDate", objPropertyDocumentBO.DocumentDate, DbType.DateTime);
            parametersList.Add(documentDateParam);

            //These values are not required while saving documents.because we are using same SP for saving documents in Appliance servicing and PDR
            ParameterBO propertyIdParam = new ParameterBO("propertyId", objPropertyDocumentBO.PropertyId, DbType.String);
            parametersList.Add(propertyIdParam);
            ParameterBO CP12DocumentParam = new ParameterBO("cp12Doc", objPropertyDocumentBO.CP12Dcoument, DbType.Binary);
            parametersList.Add(CP12DocumentParam);
            ParameterBO CP12NumberParam = new ParameterBO("CP12Number", objPropertyDocumentBO.CP12Number, DbType.Int32);
            parametersList.Add(CP12NumberParam);
            ParameterBO EpcCategoryParam = new ParameterBO("EpcRating", objPropertyDocumentBO.EpcRating, DbType.Int32);
            parametersList.Add(EpcCategoryParam);


            SaveRecord(ref parametersList, ref outParametersList, SpNameConstants.SavePropertyDocumentInformation);

        }

        #endregion

        #region "get Property Documents"
        public int getPropertyDocuments(ref DataSet resultDataSet, int? schemeId, int? blockId, ref PageSortBO objPageSortBo)
        {
            ParameterList parametersList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO propertyIdParam = new ParameterBO("propertyId", string.Empty, DbType.String);
            parametersList.Add(propertyIdParam);

            ParameterBO schemeIdParam = new ParameterBO("schemeId", schemeId, DbType.Int32);
            parametersList.Add(schemeIdParam);

            ParameterBO blockIdParam = new ParameterBO("blockId", blockId, DbType.Int32);
            parametersList.Add(blockIdParam);

            ParameterBO pageSize = new ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32);
            parametersList.Add(pageSize);

            ParameterBO pageNumber = new ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32);
            parametersList.Add(pageNumber);

            ParameterBO sortColumn = new ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String);
            parametersList.Add(sortColumn);

            ParameterBO sortOrder = new ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String);
            parametersList.Add(sortOrder);

            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);

            LoadDataSet(ref resultDataSet, ref parametersList, ref outParametersList, SpNameConstants.GetPropertyDocumentsInfo);
            ParameterBO obj = outParametersList.First();
            return Convert.ToInt32(obj.Value.ToString());

        }
        #endregion

        #region Get EPC Category types


        public DataSet getEpcCategoryTypesList(ref DataSet resultDataSet)
        {
            ParameterList parametersList = new ParameterList();
            ParameterList outParametersList = new ParameterList();
            base.LoadDataSet(ref resultDataSet, ref parametersList, ref outParametersList, SpNameConstants.GetEpcCategoryTypes);
            return resultDataSet;
        }

        #endregion

       public  string deleteDocumentByIDandGetPath(int docId)
        {
            ParameterList parametersList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO documentIdParam = new ParameterBO("documentId", docId, DbType.Int32);
            parametersList.Add(documentIdParam);

            ParameterBO documentPathParam = new ParameterBO("documentName", 0, DbType.String);
            outParametersList.Add(documentPathParam);
            SaveRecord(ref parametersList, ref outParametersList, SpNameConstants.DeletePropertyDocumentsByDocumentId);
            ParameterBO obj = outParametersList.First();
            return obj.Value.ToString();
        }

       #region Get Document SubTypes
       public DataSet getDocumentInformationById(int documentId)
       {
           ParameterList parametersList = new ParameterList();
           ParameterList outParametersList = new ParameterList();           
           ParameterBO docId = new ParameterBO("DocumentId", documentId, DbType.Int32);
           parametersList.Add(docId);
           
           DataSet resultDataSet = new DataSet();
           LoadDataSet(ref resultDataSet, ref parametersList, ref outParametersList, SpNameConstants.GetPropertyDocumentInformationById);
                      return resultDataSet;
       }

       #endregion

       public void getDocumentToDownload(ref int docId, ref string type, ref string documentName, ref string documentPath, ref string documentExt, ref string typeId)
       {
            ParameterList parametersList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO docIdParam = new ParameterBO("documentId", docId, DbType.Int32);
            parametersList.Add(docIdParam);

            ParameterBO typeParam = new ParameterBO("type", type, DbType.String);
            parametersList.Add(typeParam);

            ParameterBO docNameParam = new ParameterBO("documentName", documentName, DbType.String);
            outParametersList.Add(docNameParam);

            ParameterBO docPathParam = new ParameterBO("documentPath", documentPath, DbType.String);
            outParametersList.Add(docPathParam);

            ParameterBO docExtensionParam = new ParameterBO("documentExtension", documentExt, DbType.String);
            outParametersList.Add(docExtensionParam);

            ParameterBO typeIdParam = new ParameterBO("typeId", typeId, DbType.String);
            outParametersList.Add(typeIdParam);

            outParametersList = SelectRecord(ref parametersList,ref outParametersList, SpNameConstants.DownloadComplianceDocument);

            documentName = outParametersList[0].Value.ToString();
            documentPath = outParametersList[1].Value.ToString();
            documentExt = outParametersList[2].Value.ToString();
            typeId = outParametersList[3].Value.ToString();
           
           
           
           

       }

       #region "Get CP12Document By LGSRID"


       public void getCP12DocumentByLGSRID(ref DataSet dataSet, ref int LGSRID)
       {
           ParameterList parameterList = new ParameterList();

           ParameterBO LGSRIDParam = new ParameterBO("LGSRID", LGSRID, DbType.Int32);
           parameterList.Add(LGSRIDParam);

           LoadDataSet(ref dataSet, ref parameterList, SpNameConstants.GetCP12DocumentByLGSRID);
       }

       #endregion
    }

}
