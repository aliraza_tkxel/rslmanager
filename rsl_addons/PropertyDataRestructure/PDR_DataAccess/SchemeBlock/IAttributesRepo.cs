﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using PDR_BusinessObject.SchemeBlock;
using PDR_BusinessObject.PageSort;

namespace PDR_DataAccess.SchemeBlock
{
    public interface IAttributesRepo

    {
        DataSet getLocations();
        DataSet getAreasByLocationId(int locationId);
        DataSet getItemsByAreaId(int areaId);
        DataSet getBoilerslist(int? schemeId, int? blockId);
        DataSet getWaterMeterlist(int? schemeId, int? blockId,int? itemId);
        DataSet getPassengerLiftlist(int? schemeId, int? blockId, int? itemId);
        DataSet getMultiAttributelistByItemId(int? schemeId, int? blockId, int? itemId);
        DataSet getSubItemsByItemId(int itemId);
        DataSet getItemsByItemId(int itemId);
        DataSet getItemNotes(int? schemeId, int? blockId, int itemId, int? heatingMappingId);
        void saveItemNotes(AttributeNotesBO objItemNotesBo);
        void saveDocumentUpload(AttributePhotographsBO objAttrPhotoBO);
        DataSet getPropertyImages(int itemId, int? schemeId, int? blockId, int? heatingMappingId);
        DataSet getItemDetail(int itemId, int? schemeId, int? blockId,int? childAttributeMappingId=null );
        DataSet loadCycleDdl();
        int amendAttributeDetail(ItemAttributeBO objItemAttributeBo, DataTable itemDetailDt, DataTable itemDatesDt, DataTable conditionRatingDt, DataTable MSATDetailDt, CyclicalServiceBO cyclicalService);
        void saveServiceChargeProperties(ServiceChargeProperties serProperties, DataTable excludedIncludedProperties, int itemId, int attributeChildId);
        int getAppliancesList(ref DataSet resultDataSet, ref PageSortBO objPageSortBo, int? schemeId, int itemId, int? blockId);
        DataSet getApplianceLocations(string prefix);
        DataSet getExPropertiesBySchemeBlock(int scheme, int block, int itemId, int childItemId);
        DataSet getApplianceType(string prefix);
        DataSet getMake(string prefix);
        DataSet getModelsList(string prefix);
        int saveAppliance(ApplianceBO objApplianceBo);
        int GetServiceChargeReport(ref DataSet resultDataSet, PageSortBO objPageSortBO, int schemeId, int blockId, int itemId, int fiscalYear, bool fetchAllRecords);
        int GetServiceChargeReportPO(ref DataSet resultDataSet, PageSortBO objPageSortBO, int schemeId, int blockId, int fiscalYear);
        int GetServiceChargeReportDC(ref DataSet resultDataSet, PageSortBO objPageSortBO, string filterText, int fiscalYear);
        DataSet getDetectorByPropertyId(int schemeId, int blockId, string detectorType);
        bool saveDetectors(DetectorsBO objDetectorBo, string smokeDetectorType, string noOfSmokeDetectors, int InstalledBy);
        DataSet getCyclingServicesContractor(int schemeId, int blockId, string msatType);
        DataSet getBlocksByScheme(int schemeId);
        DataSet getPropertiesBySchemeBlock(int schemeId, int blockId);               
        DataSet getPurchaseItemExcludedProperties(int orderItemId);
        DataSet getGeneralJournalExcludedProperties(int txnId);
        DataSet getSelectedProperties(int schemeId, int blockId, int itemId);
        DataSet getFiscalYears();
        void getSchemeBlockAppliances(string Id, ref DataSet resultDataSet);
        void getDefectManagementLookupValues(string Id, string requestType, ref DataSet resultDataSet);
        void saveDefect(ApplianceDefectBO objDefectBO);
        void getSchemeBlockDefectDetails(int propertyDefectId, ref ApplianceDefectBO objApplianceDefectBO);
        void loadSchemeBlockAppliancesDDL(string Id, string requestType, ref DataSet resultDataSet);
        void loadCategoryDDL(ref DataSet resultDataSet);
        void getSchemeBlockDefectImages(string Id, string requestType, ref DataSet photosDataSet);
        void getSchemeBlockImages(ref DataSet resultDataSet, string Id, string requestType, int itemId);
        void savePhotograph(PhotographBO objPhotographBO);

        void UpdateItemNotes(AttributeNotesBO objItemNotesBo);
        void DeleteItemNotes(int itemNotesId);
        void GetItemNotesTrail(ref DataSet resultDataSet, int itemNotesId);

    }
}
