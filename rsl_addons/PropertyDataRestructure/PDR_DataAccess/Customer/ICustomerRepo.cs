﻿
// -----------------------------------------------------------------------
// <copyright file="ICustomer.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace PDR_DataAccess.Customer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using PDR_BusinessObject.Customer;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public interface ICustomerRepo
    {

        #region "Update customer address"
        /// <summary>
        /// Update customer address
        /// </summary>
        /// <param name="objCustomerBO"></param>
         void updateAddress(CustomerBO objCustomerBO);
        #endregion
    }
}
