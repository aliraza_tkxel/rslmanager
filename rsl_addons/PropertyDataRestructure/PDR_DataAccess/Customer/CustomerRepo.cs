﻿// -----------------------------------------------------------------------
// <copyright file="CustomerRepo.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace PDR_DataAccess.Customer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using PDR_DataAccess.Base;
    using System.Data;
    using PDR_Utilities.Constants;
    using PDR_BusinessObject.Customer;
    using PDR_BusinessObject;
    using PDR_BusinessObject.Parameter;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class CustomerRepo : BaseDAL, ICustomerRepo
    {

        #region "Update customer address"
        /// <summary>
        /// Update customer address
        /// </summary>
        /// <param name="objCustomerBO"></param>
        public void updateAddress(CustomerBO objCustomerBO)
        {

            ParameterList parametersList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO customerIdParam = new ParameterBO("customerId", objCustomerBO.CustomerId, DbType.Int32);
            parametersList.Add(customerIdParam);

            ParameterBO telephoneParam = new ParameterBO("telephone", objCustomerBO.Telephone, DbType.String);
            parametersList.Add(telephoneParam);

            ParameterBO mobileParam = new ParameterBO("mobile", objCustomerBO.Mobile, DbType.String);
            parametersList.Add(mobileParam);

            ParameterBO emailParam = new ParameterBO("email", objCustomerBO.Email, DbType.String);
            parametersList.Add(emailParam);

            ParameterBO resultParam = new ParameterBO("result", null, DbType.Int32);
            outParametersList.Add(resultParam);

            base.SaveRecord(ref parametersList, ref outParametersList,SpNameConstants.AmendCustomerAddress);

        }

        #endregion


    }
}
