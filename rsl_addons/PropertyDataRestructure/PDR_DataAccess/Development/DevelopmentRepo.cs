﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PDR_DataAccess.Base;
using System.Data;
using PDR_BusinessObject.PageSort;
using PDR_Utilities.Constants;
using PDR_BusinessObject.Development;
using PDR_BusinessObject;
using PDR_BusinessObject.Parameter;

namespace PDR_DataAccess.Development
{
    public class DevelopmentRepo : BaseDAL, IDevelopmentRepo
    {
        #region get Development List
        /// <summary>
        /// get Development List
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        public Int32 getDevelopmentList(ref DataSet resultDataSet, PageSortBO objPageSortBO, String searchText, String searchCompany)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO searchTextParam = new ParameterBO("searchText", searchText, DbType.String);
            paramList.Add(searchTextParam);

            ParameterBO searchCompanyParam = new ParameterBO("searchCompany", searchCompany, DbType.String);
            paramList.Add(searchCompanyParam);

            ParameterBO pageNumberParam = new ParameterBO("PageNumber", objPageSortBO.PageNumber, DbType.Int32);
            paramList.Add(pageNumberParam);

            ParameterBO pageSizeParam = new ParameterBO("PageSize", objPageSortBO.PageSize, DbType.Int32);
            paramList.Add(pageSizeParam);

            ParameterBO SortDirectionParam = new ParameterBO("sortOrder", objPageSortBO.SmallSortDirection, DbType.String);
            paramList.Add(SortDirectionParam);

            ParameterBO SortExpressionParam = new ParameterBO("sortColumn", objPageSortBO.SortExpression, DbType.String);
            paramList.Add(SortExpressionParam);

            ParameterBO totalCountParam = new ParameterBO("totalCount", 0, DbType.Int32);
            outParametersList.Add(totalCountParam);

            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.GetDevelopmentList);

            ParameterBO obj = outParametersList.First();
            Int32 outvalue = Convert.ToInt32(obj.Value.ToString());

            return outvalue;

        }
        #endregion

        #region get Development Phase
        /// <summary>
        /// get Development Phase
        /// </summary>
        /// <param name="phaseDataSet"></param>
        /// <param name="developmentId"></param>
        public void getDevelopmentPhase(ref DataSet phaseDataSet, int developmentId)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO pagedevelopmentId = new ParameterBO("developmentId", developmentId, DbType.Int32);
            paramList.Add(pagedevelopmentId);

            LoadDataSet(ref phaseDataSet, ref paramList, ref outParametersList, SpNameConstants.GetDevelpomentPhases);
        }
        #endregion
        
        #region get Scheme
        /// <summary>
        /// get Scheme
        /// </summary>
        /// <param name="SchemeDataSet"></param>
        /// <param name="developmentId"></param>
        public void getScheme(ref DataSet SchemeDataSet, int developmentId)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO pagedevelopmentId = new ParameterBO("developmentId", developmentId, DbType.Int32);
            paramList.Add(pagedevelopmentId);

            LoadDataSet(ref SchemeDataSet, ref paramList, ref outParametersList, SpNameConstants.GetDevelopmentScheme);
        }
        #endregion

        #region get Patch
        /// <summary>
        /// get Patch
        /// </summary>
        /// <param name="PatchDataSet"></param>
        public void getPatch(ref DataSet PatchDataSet)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            LoadDataSet(ref PatchDataSet, ref paramList, ref outParametersList, SpNameConstants.GetPatch);
        }
        #endregion

        #region get Company
        /// <summary>
        /// get Company
        /// </summary>
        /// <param name="CompanyDataSet"></param>
        public void getCompany(ref DataSet CompanyDataSet)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            LoadDataSet(ref CompanyDataSet, ref paramList, ref outParametersList, SpNameConstants.GetCompany);
        }
        #endregion

        #region get Development Type
        /// <summary>
        /// get Development Type
        /// </summary>
        /// <param name="DevelopmentTypeDataSet"></param>
        public void getDevelopmentType(ref DataSet DevelopmentTypeDataSet)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            LoadDataSet(ref DevelopmentTypeDataSet, ref paramList, ref outParametersList, SpNameConstants.GetDevelopmentType);
        }
        #endregion

        #region get Development Status
        /// <summary>
        /// get Development Status
        /// </summary>
        /// <param name="DevelopmentStatusDataSet"></param>
        public void getDevelopmentStatus(ref DataSet DevelopmentStatusDataSet)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            LoadDataSet(ref DevelopmentStatusDataSet, ref paramList, ref outParametersList, SpNameConstants.GetDevelopmentStatus);
        }
        #endregion

        #region Save Phase
        /// <summary>
        /// Save Phase
        /// </summary>
        /// <param name="objPhase"></param>
        /// <param name="phaseId"></param>
        public void SavePhase(PDR_BusinessObject.Phase.PhaseBO objPhase, ref int phaseId)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO paramPhaseName = new ParameterBO("PhaseName", objPhase.PhaseName, DbType.String);
            paramList.Add(paramPhaseName);

            ParameterBO paramConstruction = new ParameterBO("ConstructionStart", objPhase.ConstructionStart, DbType.Date);
            paramList.Add(paramConstruction);

            ParameterBO paramAnticipatedCompletion = new ParameterBO("AnticipatedCompletion", objPhase.AnticipatedCompletion, DbType.Date);
            paramList.Add(paramAnticipatedCompletion);

            ParameterBO paramActualCompletion = new ParameterBO("ActualCompletion", objPhase.ActualCompletion, DbType.Date);
            paramList.Add(paramActualCompletion);

            ParameterBO paramHandoverintoManagement = new ParameterBO("HandoverintoManagement", objPhase.Management, DbType.Date);
            paramList.Add(paramHandoverintoManagement);

            ParameterBO paramSchemeId = new ParameterBO("schemeId", objPhase.SchemeId, DbType.Int32);
            paramList.Add(paramSchemeId);

            ParameterBO paramUpdatePhaseId = new ParameterBO("PId", objPhase.PhaseId, DbType.Int32);
            paramList.Add(paramUpdatePhaseId);

            ParameterBO paramPhaseId = new ParameterBO("PhaseId", 0, DbType.Int32);
            outParametersList.Add(paramPhaseId);

            ParameterBO paramDevelopmentId = new ParameterBO("DevelopmentId", objPhase.DevelopmentId, DbType.Int32);
            paramList.Add(paramDevelopmentId);

            SaveRecord(ref paramList, ref outParametersList, SpNameConstants.SavePhase);

            ParameterBO obj = outParametersList.First();
            phaseId = Convert.ToInt32(obj.Value.ToString());

        }
        #endregion

        #region get Phase Info
        /// <summary>
        /// get Phase Info
        /// </summary>
        /// <param name="resultData"></param>
        /// <param name="phaseId"></param>
        public void getPhaseInfo(ref DataSet resultData, int phaseId)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO paramPhaseId = new ParameterBO("PhaseId", phaseId, DbType.Int32);
            paramList.Add(paramPhaseId);

            LoadDataSet(ref resultData, ref paramList, SpNameConstants.GetPhaseInfo);

        }
        #endregion

        #region Save Document
        /// <summary>
        /// Save Document
        /// </summary>
        /// <param name="objDevelopmentDocs"></param>
        /// <returns></returns>
        public int SaveDocument(DevelopmentDocumentsBO objDevelopmentDocs)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO paramCategory = new ParameterBO("category", objDevelopmentDocs.Category, DbType.String);
            paramList.Add(paramCategory);

            ParameterBO paramTitle = new ParameterBO("title", objDevelopmentDocs.Title, DbType.String);
            paramList.Add(paramTitle);

            ParameterBO paramDocumentPath = new ParameterBO("documentPath", objDevelopmentDocs.DocPath, DbType.String);
            paramList.Add(paramDocumentPath);

            ParameterBO paramExpires = new ParameterBO("expires", objDevelopmentDocs.Expiry, DbType.DateTime);
            paramList.Add(paramExpires);

            ParameterBO paramFileType = new ParameterBO("fileType", objDevelopmentDocs.FileType, DbType.String);
            paramList.Add(paramFileType);

            ParameterBO paramUserId = new ParameterBO("createdBy", objDevelopmentDocs.UserId, DbType.Int32);
            paramList.Add(paramUserId);

            ParameterBO paramTypeId = new ParameterBO("DocumentTypeId", objDevelopmentDocs.DocumentTypeId, DbType.Int32);
            paramList.Add(paramTypeId);

            ParameterBO paramDocument = new ParameterBO("DocumentId", 0, DbType.Int32);
            outParametersList.Add(paramDocument);


            ParameterBO paramDocumentDate = new ParameterBO("DocumentDate", objDevelopmentDocs.DocumentDate, DbType.DateTime);
            paramList.Add(paramDocumentDate);

            ParameterBO paramUploadedDate = new ParameterBO("UploadedDate", objDevelopmentDocs.UploadedDate, DbType.DateTime);
            paramList.Add(paramUploadedDate);

            ParameterBO paramFileName = new ParameterBO("documentName", objDevelopmentDocs.FileName, DbType.String);
            paramList.Add(paramFileName);

            SaveRecord(ref paramList, ref outParametersList, SpNameConstants.SaveDocument);

            ParameterBO obj = outParametersList.First();

            int newDocumentId = Convert.ToInt32(obj.Value.ToString());

            // Now add newly created document in P_DevelopmentDocument middle table.
            ParameterList paramListDocs = new ParameterList();
            ParameterList outparamListDocs = new ParameterList();

            ParameterBO ParamDevelopmentId = new ParameterBO("DevelopmentId", objDevelopmentDocs.DevelopmentId, DbType.Int32);
            paramListDocs.Add(ParamDevelopmentId);

            ParameterBO ParamDocId = new ParameterBO("DocId", newDocumentId, DbType.Int32);
            paramListDocs.Add(ParamDocId);

            SaveRecord(ref paramListDocs, ref outparamListDocs, SpNameConstants.SaveDevelopmentDocument);

            return (newDocumentId);
        }
        #endregion

        #region get Blocks Data
        /// <summary>
        /// get Blocks Data
        /// </summary>
        /// <param name="BlockDataSet"></param>
        /// <param name="developmentId"></param>
        public void getBlocksData(ref DataSet BlockDataSet, int developmentId)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO pagedevelopmentId = new ParameterBO("developmentId", developmentId, DbType.Int32);
            paramList.Add(pagedevelopmentId);

            LoadDataSet(ref BlockDataSet, ref paramList, SpNameConstants.GetDevelopmentBlocks);
        }
        #endregion

        #region get Documents Data
        /// <summary>
        /// get Documents Data
        /// </summary>
        /// <param name="documentDataSet"></param>
        public void getDocumentsData(ref DataSet documentDataSet)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            LoadDataSet(ref documentDataSet, ref paramList, SpNameConstants.GetAllDocuments);
        }
        #endregion 

        #region get Developments Documents Data
        /// <summary>
        /// get Developments Documents Data
        /// </summary>
        /// <param name="documentDataSet"></param>
        public void getDevelopmentsDocumentsData(ref DataSet documentDataSet, int developmentId)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO pagedevelopmentId = new ParameterBO("developmentId", developmentId, DbType.Int32);
            paramList.Add(pagedevelopmentId);

            LoadDataSet(ref documentDataSet, ref paramList, SpNameConstants.GetDevelopmentsDocuments);
        }
        #endregion 

        
        #region Delete Document by ID
        public string[] deleteDocumentByIDandGetPath(int docId)
        {
            ParameterList parametersList = new ParameterList();
            ParameterList outParametersList = new ParameterList();
            

            ParameterBO documentIdParam = new ParameterBO("documentId", docId, DbType.Int32);
            parametersList.Add(documentIdParam);

            ParameterBO documentNameParam = new ParameterBO("documentName", 0, DbType.String);
            ParameterBO documentPathParam = new ParameterBO("documentPath", 0, DbType.String);
            outParametersList.Add(documentNameParam);
            outParametersList.Add(documentPathParam);
            SaveRecord(ref parametersList, ref outParametersList, SpNameConstants.DeleteDevelopmentDocumentsByDocumentId);
            ParameterBO objDocName = outParametersList.ElementAt(0);
            ParameterBO objDocPath = outParametersList.ElementAt(1);
            string[] docInfo = { objDocName.Value.ToString() , objDocPath.Value.ToString() };

            return docInfo;
        }
        #endregion

        #region Get Document Information by ID
        public DataSet getDocumentInformationById(int documentId)
        {
            ParameterList parametersList = new ParameterList();
            ParameterList outParametersList = new ParameterList();
            ParameterBO docId = new ParameterBO("DocumentId", documentId, DbType.Int32);
            parametersList.Add(docId);

            DataSet resultDataSet = new DataSet();
            LoadDataSet(ref resultDataSet, ref parametersList, ref outParametersList, SpNameConstants.GetDevelopmentDocumentInformationById);
            return resultDataSet;
        }

        #endregion

        #region Save Develpoment
        /// <summary>
        /// Save Develpoment
        /// </summary>
        /// <param name="objSaveDevelopmentBO"></param>
        /// <param name="DocIdList"></param>
        /// <param name="PhaseIdList"></param>
        public string saveDevelpoment(ref DevelopmentBO objSaveDevelopmentBO, List<int> DocIdList, List<int> PhaseIdList)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO paramDevelopmentId = new ParameterBO("DevelopmentId", objSaveDevelopmentBO.DevelopmentId, DbType.Int32);
            paramList.Add(paramDevelopmentId);

            ParameterBO paramDevelopmentName = new ParameterBO("DevelopmentName", objSaveDevelopmentBO.DevelopmentName, DbType.String);
            paramList.Add(paramDevelopmentName);

            ParameterBO paramPatch = new ParameterBO("Patch", objSaveDevelopmentBO.Patch, DbType.Int32);
            paramList.Add(paramPatch);

            ParameterBO paramAddress1 = new ParameterBO("Address1", objSaveDevelopmentBO.Address1, DbType.String);
            paramList.Add(paramAddress1);

            ParameterBO paramAddress2 = new ParameterBO("Address2", objSaveDevelopmentBO.Address2, DbType.String);
            paramList.Add(paramAddress2);

            ParameterBO paramCounty = new ParameterBO("County", objSaveDevelopmentBO.County, DbType.String);
            paramList.Add(paramCounty);

            ParameterBO paramTownCity = new ParameterBO("TownCity", objSaveDevelopmentBO.TownCity, DbType.String);
            paramList.Add(paramTownCity);

            ParameterBO paramPostCode = new ParameterBO("PostCode", objSaveDevelopmentBO.PostCode, DbType.String);
            paramList.Add(paramPostCode);

            ParameterBO paramArchitect = new ParameterBO("Architect", objSaveDevelopmentBO.Architect, DbType.String);
            paramList.Add(paramArchitect);

            ParameterBO paramProjectManager = new ParameterBO("ProjectManager", objSaveDevelopmentBO.ProjectManager, DbType.String);
            paramList.Add(paramProjectManager);

            ParameterBO paramCompanyId = new ParameterBO("CompanyId", objSaveDevelopmentBO.CompanyId, DbType.Int32);
            paramList.Add(paramCompanyId);

            ParameterBO paramDevelopmentType = new ParameterBO("DevelopmentType", objSaveDevelopmentBO.DevelopmentType, DbType.Int32);
            paramList.Add(paramDevelopmentType);

            ParameterBO paramDevelopmentStatus = new ParameterBO("DevelopmentStatus", objSaveDevelopmentBO.DevelopmentStatus, DbType.Int32);
            paramList.Add(paramDevelopmentStatus);

            ParameterBO paramLandValue = new ParameterBO("LandValue", objSaveDevelopmentBO.LandValue, DbType.Double);
            paramList.Add(paramLandValue);
            
            ParameterBO paramLandValueDate = new ParameterBO("ValueDate", objSaveDevelopmentBO.ValueDate, DbType.DateTime);//new code
            paramList.Add(paramLandValueDate);


            ParameterBO paramLandPurchasePrice = new ParameterBO("LandPurchasePrice", objSaveDevelopmentBO.LandPurchasePrice, DbType.Double);
            paramList.Add(paramLandPurchasePrice);

            ParameterBO paramPurchaseDate = new ParameterBO("PurchaseDate", objSaveDevelopmentBO.PurchaseDate, DbType.DateTime);
            paramList.Add(paramPurchaseDate);

            ParameterBO paramGrantAmount = new ParameterBO("GrantAmount", objSaveDevelopmentBO.GrantAmount, DbType.Double);
            paramList.Add(paramGrantAmount);

            ParameterBO paramBorrowedAmount = new ParameterBO("BorrowedAmount", objSaveDevelopmentBO.BorrowedAmount, DbType.Double);
            paramList.Add(paramBorrowedAmount);

            ParameterBO paramOutPLanningApplication = new ParameterBO("OutlinePlanningApplication", objSaveDevelopmentBO.OutlinePlanningApplication, DbType.DateTime);
            paramList.Add(paramOutPLanningApplication);

            ParameterBO paramOutPLanningApproval = new ParameterBO("OutlinePlanningApproval", objSaveDevelopmentBO.OutlinePlanningApproval, DbType.DateTime);
            paramList.Add(paramOutPLanningApproval);

            ParameterBO paramDetailPLanningApplication = new ParameterBO("DetailedPlanningApplication", objSaveDevelopmentBO.DetailedPlanningApplication, DbType.DateTime);
            paramList.Add(paramDetailPLanningApplication);

            ParameterBO paramDetailPLanningApproval = new ParameterBO("DetailedPlanningApproval", objSaveDevelopmentBO.DetailedPlanningApproval, DbType.DateTime);
            paramList.Add(paramDetailPLanningApproval);

            ParameterBO paramNoOfUnits = new ParameterBO("NoofUnits", objSaveDevelopmentBO.NoofUnits, DbType.Int32);
            paramList.Add(paramNoOfUnits);

            ParameterBO paramLocalAuthorityId = new ParameterBO("localAuthorityId", objSaveDevelopmentBO.LocalAuthorityId, DbType.Int32);
            paramList.Add(paramLocalAuthorityId);

            ParameterBO paramUserId = new ParameterBO("UserId", objSaveDevelopmentBO.UserId, DbType.Int32);
            paramList.Add(paramUserId);
            
            ParameterBO fundingSourceDtParam = new ParameterBO("developmentFundingDetail", objSaveDevelopmentBO.FundingSourceDt, SqlDbType.Structured);
            paramList.Add(fundingSourceDtParam);

            ParameterBO paramUpdatedDevelopmentId = new ParameterBO("UpdatedDevelopmentId", 0, DbType.String);
            outParametersList.Add(paramUpdatedDevelopmentId);

            

            SaveRecord(ref paramList, ref outParametersList, SpNameConstants.SaveDevelopment);

            ParameterBO obj = outParametersList.First();
           string result = obj.Value.ToString();

           if (result == "Exist")
            {
                result = "Exist";
            }
           else if (result == "Failed")
           {
               result = "failed";
           }
           else
           {
               result = "success";

               int DevelopmentId = Convert.ToInt32(obj.Value.ToString());
               objSaveDevelopmentBO.DevelopmentId = DevelopmentId;

               if ( PhaseIdList != null && PhaseIdList.Count > 0)
               {

                   foreach (int phaseid in PhaseIdList)
                   {
                       ParameterList paramListPhase = new ParameterList();
                       ParameterList outparamListPhase = new ParameterList();

                       ParameterBO ParamPhaseId = new ParameterBO("phaseid", phaseid, DbType.Int32);
                       paramListPhase.Add(ParamPhaseId);

                       ParameterBO ParamDevelopmentId = new ParameterBO("DevelopmentId", DevelopmentId, DbType.Int32);
                       paramListPhase.Add(ParamDevelopmentId);

                       SaveRecord(ref paramListPhase, ref outparamListPhase, SpNameConstants.SaveDevelopmentPhase);
                   }
               }
               //if (DocIdList != null && DocIdList.Count > 0)
               //{
               //    foreach (int DocId in DocIdList)
               //    {
               //        ParameterList paramListDocs = new ParameterList();
               //        ParameterList outparamListDocs = new ParameterList();

               //        ParameterBO ParamDevelopmentId = new ParameterBO("DevelopmentId", DevelopmentId, DbType.Int32);
               //        paramListDocs.Add(ParamDevelopmentId);

               //        ParameterBO ParamDocId = new ParameterBO("DocId", DocId, DbType.Int32);
               //        paramListDocs.Add(ParamDocId);

               //        SaveRecord(ref paramListDocs, ref outparamListDocs, SpNameConstants.SaveDevelopmentDocument);
               //    }

               //}
           }

           return result;
        }
        #endregion

        #region "Get Funding Source list"
        /// <summary>
        /// Populate Funding Source dropdown
        /// </summary>
        /// <param name="FuncdingSourceDataSet"></param>
        public void getFundingSource(ref DataSet FuncdingSourceDataSet)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            LoadDataSet(ref FuncdingSourceDataSet, ref paramList, ref outParametersList, SpNameConstants.GetFundingSource);
        }
        #endregion

        #region "Fetch Expire Documents"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fetchDocuments"></param>
        public void fetchExpireDocuments(ref DataSet fetchDocuments)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            LoadDataSet(ref fetchDocuments, ref paramList, ref outParametersList, SpNameConstants.FetchExpireDocument);

        }
        #endregion

        #region "checkNotificationSent"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        public bool checkNotificationSent(int documentId)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();
            bool result = false;
            DataSet resultDataSet = new DataSet();

            ParameterBO documentIdParam = new ParameterBO("documentId", documentId, DbType.Int32);
            paramList.Add(documentIdParam);

            LoadDataSet(ref resultDataSet, ref paramList, ref outParametersList, SpNameConstants.CheckNotificationSent);

            if (resultDataSet.Tables[0].Rows.Count > 0)
            {
                if (resultDataSet.Tables[0].Rows[0]["Status"] == "sent")
                {
                    result = true;
                }
                else
                {
                     result = false; ;
                }
            }
            else
            {
                result = false;
            }
            return result;
        }
        #endregion

        #region "log Sent Notification"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentId"></param>
        /// <param name="documentTitle"></param>
        /// <param name="userId"></param>
        /// <param name="status"></param>
        /// <param name="message"></param>
        public void logSentNotification(int documentId, string documentTitle, int userId, string status, string message)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO documentIdParam = new ParameterBO("documentId", documentId, DbType.Int32);
            paramList.Add(documentIdParam);

            ParameterBO documentTitleParam = new ParameterBO("documentTitle", documentTitle, DbType.Int32);
            paramList.Add(documentTitleParam);

            ParameterBO userIdParam = new ParameterBO("userId", userId, DbType.Int32);
            paramList.Add(userIdParam);

            ParameterBO statusParam = new ParameterBO("status", status, DbType.Int32);
            paramList.Add(statusParam);

            ParameterBO messageParam = new ParameterBO("message", message, DbType.Int32);
            paramList.Add(messageParam);

            SaveRecord(ref paramList, ref outParametersList, SpNameConstants.LogPushNotifications);



        }
        #endregion

        #region "update Document"
        public void updateDocument(DevelopmentDocumentsBO objDevelopmentDocs)
        {
            ParameterList paramList = new ParameterList();
            ParameterList outParametersList = new ParameterList();

            ParameterBO paramTitle = new ParameterBO("Title", objDevelopmentDocs.Title, DbType.String);
            paramList.Add(paramTitle);

            ParameterBO paramDocumentPath = new ParameterBO("DocId", objDevelopmentDocs.DocId, DbType.Int32);
            paramList.Add(paramDocumentPath);

            ParameterBO paramExpires = new ParameterBO("expires", objDevelopmentDocs.Expiry, DbType.DateTime);
            paramList.Add(paramExpires);

            ParameterBO paramDevelopmentId = new ParameterBO("DevelopmentId", objDevelopmentDocs.DevelopmentId, DbType.Int32);
            paramList.Add(paramDevelopmentId);

            SaveRecord(ref paramList, ref outParametersList, SpNameConstants.UpdateDocument);

        }
        #endregion

        #region "Get Development Detail"

        public void GetDevelopmentDetail(ref DataSet resultDataset, int developmentId)
        {
            ParameterList inParamList = new ParameterList();

            ParameterBO developmentIdparam = new ParameterBO("developmentId", developmentId, DbType.Int32);
            inParamList.Add(developmentIdparam);

            DataTable developmentDetailDt = new DataTable();
            developmentDetailDt.TableName = ApplicationConstants.DevelopmentDetailDt;
            resultDataset.Tables.Add(developmentDetailDt);

            DataTable developmentFundingDt = new DataTable();
            developmentFundingDt.TableName = ApplicationConstants.DevelopmentFundingDt;
            resultDataset.Tables.Add(developmentFundingDt);

            IDataReader iResultDataReader = base.SelectRecord(inParamList, SpNameConstants.GetDevelopmentDetail);
            resultDataset.Load(iResultDataReader, LoadOption.OverwriteChanges, developmentDetailDt, developmentFundingDt);
            iResultDataReader.Close();
        }

        #endregion
       


    }
}
