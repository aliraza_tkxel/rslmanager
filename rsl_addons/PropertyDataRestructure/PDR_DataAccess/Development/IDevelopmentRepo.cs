﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using PDR_BusinessObject.PageSort;
using PDR_BusinessObject.Phase;
using PDR_BusinessObject.Development;

namespace PDR_DataAccess.Development
{
    public interface IDevelopmentRepo
    {
        #region get Development List
        /// <summary>
        /// get Development List
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <param name="objPageSortBO"></param>
        /// <param name="searchText"></param>
        /// <param name="searchCompany"></param>
        /// <returns></returns>
        Int32 getDevelopmentList(ref DataSet resultDataSet, PageSortBO objPageSortBO, String searchText, String searchCompany);
        #endregion

        #region get Development Phase
        /// <summary>
        /// get Development Phase
        /// </summary>
        /// <param name="phaseDataSet"></param>
        /// <param name="developmentId"></param>
        void getDevelopmentPhase(ref DataSet phaseDataSet, int developmentId);
        #endregion

        #region get Scheme
        /// <summary>
        /// get Scheme
        /// </summary>
        /// <param name="SchemeDataSet"></param>
        /// <param name="developmentId"></param>
        void getScheme(ref DataSet SchemeDataSet, int developmentId);
        #endregion

        #region get Patch
        /// <summary>
        /// get Patch
        /// </summary>
        /// <param name="PatchDataSet"></param>
        void getPatch(ref DataSet PatchDataSet);
        #endregion

        #region get Development Type
        /// <summary>
        /// get Development Type
        /// </summary>
        /// <param name="DevelopmentTypeDataSet"></param>
        void getDevelopmentType(ref DataSet DevelopmentTypeDataSet);
        #endregion

        #region get Company
        /// <summary>
        /// get Development Type
        /// </summary>
        /// <param name="CompanyDataSet"></param>
        void getCompany(ref DataSet CompanyDataSet);
        #endregion

        #region get Development Status
        /// <summary>
        /// get Development Status
        /// </summary>
        /// <param name="DevelopmentStatusDataSet"></param>
        void getDevelopmentStatus(ref DataSet DevelopmentStatusDataSet);
        #endregion

        #region Save Phase
        /// <summary>
        /// Save Phase
        /// </summary>
        /// <param name="objPhase"></param>
        /// <param name="phaseId"></param>
        void SavePhase(PhaseBO objPhase, ref int phaseId);
        #endregion

        #region get Phase Info
        /// <summary>
        /// get Phase Info
        /// </summary>
        /// <param name="resultData"></param>
        /// <param name="phaseId"></param>
        void getPhaseInfo(ref DataSet resultData, int phaseId);
        #endregion

        #region Save Document
        /// <summary>
        /// Save Document
        /// </summary>
        /// <param name="objDevelopmentDocs"></param>
        /// <returns></returns>
        int SaveDocument(DevelopmentDocumentsBO objDevelopmentDocs);
        #endregion

        #region get Blocks Data
        /// <summary>
        /// get Blocks Data
        /// </summary>
        /// <param name="BlockDataSet"></param>
        /// <param name="developmentId"></param>
        void getBlocksData(ref DataSet BlockDataSet, int developmentId);
        #endregion

        #region get Documents Data
        /// <summary>
        /// get Documents Data
        /// </summary>
        /// <param name="documentDataSet"></param>
        void getDocumentsData(ref DataSet documentDataSet);
        #endregion

        #region Delete Document
        /// <summary>
        /// get Document information
        /// </summary>
        /// <param name="documentId"></param>
        string[] deleteDocumentByIDandGetPath(int docId);
        #endregion


        #region get Document information
        /// <summary>
        /// get Document information
        /// </summary>
        /// <param name="documentId"></param>
        DataSet getDocumentInformationById(int documentId);
        #endregion

        #region get Developments Documents Data
        /// <summary>
        /// get Developments Documents Data
        /// </summary>
        /// <param name="documentDataSet"></param>
        void getDevelopmentsDocumentsData(ref DataSet documentDataSet, int developmentId);
        #endregion

        #region Save Develpoment
        /// <summary>
        /// Save Develpoment
        /// </summary>
        /// <param name="objSaveDevelopmentBO"></param>
        /// <param name="DocIdList"></param>
        /// <param name="PhaseIdList"></param>
        string saveDevelpoment(ref DevelopmentBO objSaveDevelopmentBO, List<int> DocIdList, List<int> PhaseIdList);
        #endregion

        #region "Get Funding Source"
        /// <summary>
        /// Populate Funding source dropdown
        /// </summary>
        /// <param name="FuncdingSourceDataSet"></param>
        void getFundingSource(ref DataSet FuncdingSourceDataSet);
        #endregion

        #region "Fetch Expire Documents"
        /// <summary>
        /// Fetch Documents info which are going to expire
        /// </summary>
        /// <param name="fetchDocuments"></param>
        void fetchExpireDocuments(ref DataSet fetchDocuments);
        #endregion

        #region"checkNotificationSent"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentId"></param>
        /// <returns></returns>
        bool checkNotificationSent(int documentId);
        #endregion

        #region "log Sent Notification"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentId"></param>
        /// <param name="documentTitle"></param>
        /// <param name="userId"></param>
        /// <param name="status"></param>
        /// <param name="message"></param>
        void logSentNotification(int documentId, string documentTitle, int userId, string status, string message);
        #endregion

        #region"update Document"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="objDevelopmentDocs"></param>
        void updateDocument(DevelopmentDocumentsBO objDevelopmentDocs);
        #endregion

        #region "Get Development Detail"
        void GetDevelopmentDetail(ref DataSet resultDataset, int developmentId);
        #endregion

    }
}
