﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Pdr.Master" AutoEventWireup="true" CodeBehind="TabTemplate.aspx.cs" Inherits="PropertyDataRestructure.Templates.TabTemplate" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="block" TagName="MainDetail" Src="~/UserControls/Pdr/SchemeBlock/Attributes.ascx" %>
<%--<%@ Register TagPrefix="block" TagName="Attributes" Src="~/UserControls/Pdr/SchemeBlock/Attributes.ascx" %>
<%@ Register TagPrefix="block" TagName="Provisions" Src="~/UserControls/Pdr/SchemeBlock/Provisions.ascx" %>
<%@ Register TagPrefix="block" TagName="Warranties" Src="~/UserControls/Pdr/SchemeBlock/Warranties.ascx" %>
<%@ Register TagPrefix="block" TagName="HealthSafety" Src="~/UserControls/Pdr/SchemeBlock/HealthSafety.ascx" %>
<%@ Register TagPrefix="block" TagName="Properties" Src="~/UserControls/Pdr/SchemeBlock/Properties.ascx" %>
<%@ Register TagPrefix="block" TagName="Documents" Src="~/UserControls/Pdr/SchemeBlock/Documents.ascx" %>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<table style="border: thin solid #000000; width: 100%;">
        <tr>
            <td style="border-bottom-style: solid; border-width: thin; border-color: #000000">
                <div>
                    <asp:Panel ID="pnlBlockAddress" runat="server" CssClass="pnlPropertyAddress">
                        &nbsp;&nbsp;&nbsp;
                        <asp:Label ID="lblBlock" runat="server" Font-Bold="True" Text="Block &gt;"></asp:Label>
                        <asp:Label ID="lblBlockAddres" runat="server" Font-Bold="True"></asp:Label>
                    </asp:Panel>
                    <div style="float: right;">
                        <asp:Button ID="btnBack" runat="server" OnClick="btnBack_click" Text="&nbsp;&lt; Back &nbsp;" /></div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                    <asp:Label ID="lblMessage" runat="server">
                    </asp:Label>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td style="padding-left: 0px; padding-right: 1px; padding-top: 20px;">
                <div style='margin-left: 8px;'>
                    <asp:LinkButton ID="lnkBtnMainDetailTab" OnClick="lnkBtnMainDetailTab_Click" CommandArgument="lnkBtnMainDetailTab_Click"
                        CssClass="TabInitial" runat="server" Visible="false" BorderStyle="Solid">Main Detail: </asp:LinkButton>
                    <asp:LinkButton ID="lnkBtnAttributesTab" OnClick="lnkBtnAttributesTab_Click" CssClass="TabInitial"
                        runat="server" Visible="false" BorderStyle="Solid">Attributes: </asp:LinkButton>
                    <asp:LinkButton ID="lnkBtnProvisionsTab" OnClick="lnkBtnProvisionsTab_Click" CssClass="TabInitial"
                        runat="server" Visible="false" BorderStyle="Solid">Provisions: </asp:LinkButton>
                    <asp:LinkButton ID="lnkBtnWarranties" OnClick="lnkBtnWarranties_Click" CssClass="TabInitial"
                        runat="server" Visible="false" BorderStyle="Solid">Warranties: </asp:LinkButton>
                    <asp:LinkButton ID="lnkBtnHealthSafetyTab" OnClick="lnkBtnHealthSafetyTab_Click" CssClass="TabInitial"
                        runat="server" Visible="false" BorderStyle="Solid">HealthSafety: </asp:LinkButton>
                    <asp:LinkButton ID="lnkBtnProperties" OnClick="lnkBtnProperties_Click" CssClass="TabInitial"
                        runat="server" Visible="false" BorderStyle="Solid">Properties: </asp:LinkButton>
                    <asp:LinkButton ID="lnkBtnDocuments" OnClick="lnkBtnDocuments_Click" CssClass="TabInitial"
                        runat="server" Visible="false" BorderStyle="Solid">Documents: </asp:LinkButton>
                    <div style="clear: both; margin-bottom: 5px;">
                    </div>
                </div>
                <br />
                <div style="border-bottom: 1px solid black; height: 0px\9; clear: both; margin-left: 2px;
                    width: 100%;">
                </div>
                <div style="clear: both; margin-bottom: 5px;">
                </div>
               <%-- <asp:MultiView ID="MainView" runat="server">
                    <asp:View ID="View1" runat="server">
                        <block:MainDetail id="MainDetail" runat="server" />
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <block:Attributes id="Attributes" runat="server" />
                    </asp:View>
                    <asp:View ID="View3" runat="server">
                        <block:Provisions id="Provisions" runat="server" />
                    </asp:View>
                    <asp:View ID="View7" runat="server">
                        <block:Warranties id="Warranties" runat="server" />
                    </asp:View>
                    <asp:View ID="View4" runat="server">
                        <block:HealthSafety id="HealthSafety" runat="server" />
                    </asp:View>
                    <asp:View ID="View5" runat="server">
                        <block:Properties id="Properties" runat="server" />
                    </asp:View>
                    <asp:View ID="View6" runat="server">
                        <block:Documents id="Documents" runat="server" />
                    </asp:View>
                </asp:MultiView>--%>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
</asp:Content>
