﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PDR_BusinessObject.PageSort;
using PDR_Utilities.Constants;
using PDR_Utilities.Managers;
using AjaxControlToolkit;
using AS_Utilities;

namespace PropertyDataRestructure.Base
{
    public class UserControlBase : System.Web.UI.UserControl
    {
        public PageBase objPageBase = new PageBase();
        protected SessionManager objSession = new SessionManager();
        public UIMessageHelper uiMessageHelper = new UIMessageHelper();
        public PageSortBO pageSortViewState
        {
            get
            {
                if (ViewState[ViewStateConstants.PageSortBo] == null)
                {
                    return null;
                }
                else
                {
                    return (PageSortBO)(ViewState[ViewStateConstants.PageSortBo]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (ViewState[ViewStateConstants.PageSortBo] != null)
                    {
                        ViewState.Remove(ViewStateConstants.PageSortBo);
                    }
                }
                else
                {
                    ViewState[ViewStateConstants.PageSortBo] = value;
                }
            }
        }

        #region show hide popup in parent
        /// <summary>
        /// 
        /// </summary>
        /// <param name="modalPoupName"></param>
        public void showHidePopupInParent(string modalPoupName, bool hide = false)
        {
            ModalPopupExtender modalPoup = (ModalPopupExtender)Parent.FindControl(modalPoupName);
            if (hide == true)
            {
                modalPoup.Hide();
            }
            else
            {
                modalPoup.Show();
            }

        }

        #endregion
    }
}