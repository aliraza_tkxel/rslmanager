﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using PDR_Utilities.Helpers;
using PDR_BusinessObject.PageSort;
using PDR_Utilities.Constants;
using PDR_Utilities.Managers;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using PDR_BusinessLogic.UserAccessBL;
using PDR_DataAccess.UserAccess;

namespace PropertyDataRestructure.Base
{
    public class PageBase : System.Web.UI.Page
    {

        protected SessionManager objSession = new SessionManager();
        protected ValidatorFactory valFactory = EnterpriseLibraryContainer.Current.GetInstance<ValidatorFactory>();
        
        #region "Application Root"
        public string applicationRoot
        {
            get
            {
                string appRootPath = string.Concat(base.Request.Url.GetLeftPart(UriPartial.Authority), HttpRuntime.AppDomainAppVirtualPath);
                if (appRootPath[appRootPath.Length - 1] != '/')
                {
                    appRootPath = string.Concat(appRootPath, "/");
                }
                return appRootPath;
            }
        }
        #endregion

        #region "Relative Application Path"
        protected string relativeApplicationPath
        {
            get
            {
                string appDomainAppVirtualPath = HttpRuntime.AppDomainAppVirtualPath;
                if (!appDomainAppVirtualPath.EndsWith("/"))
                {
                    appDomainAppVirtualPath = string.Concat(appDomainAppVirtualPath, "/");
                }
                return appDomainAppVirtualPath;
            }
        }
        #endregion

        #region "Website root path"
        public string websiteRootPath()
        {
            return base.Server.MapPath("~/");
        }
        #endregion

        #region "Page Sort View state"
        public PageSortBO pageSortViewState
        {
            get
            {
                if (ViewState[ViewStateConstants.PageSortBo] == null)
                {
                    return null;
                }
                else
                {
                    return (PageSortBO)(ViewState[ViewStateConstants.PageSortBo]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (ViewState[ViewStateConstants.PageSortBo] != null)
                    {
                        ViewState.Remove(ViewStateConstants.PageSortBo);
                    }
                }
                else
                {
                    ViewState[ViewStateConstants.PageSortBo] = value;
                }
            }
        }
        #endregion

        #region "Do not cache"
        public void doNotCache()
        {
            base.Response.Cache.SetNoStore();
            base.Response.Cache.SetExpires(DateTime.Now);
        }
        #endregion

        #region "Is Directory Exists"
        protected void isDirectoryExists(string directory)
        {
            if (!Directory.Exists(directory))
            {
                Directory.CreateDirectory(directory);
            }
        }
        #endregion

        #region "Is Url Exists"
        protected void isUrlExists(string url)
        {
            string str = base.Server.MapPath(url);
            if (!Directory.Exists(str))
            {
                Directory.CreateDirectory(str);
            }
        }
        #endregion

        #region "is Session Exist"
        public void isSessionExist()
        {
            //int userId = 760;
            int userId = 0;

            userId = objSession.EmployeeId;
            if (userId == 0)
            {
                Response.Redirect(PathConstants.BridgePath);
            }
        }
        #endregion

        #region "Check Page Access"
        /// <summary>
        /// Check Page Access
        /// </summary>
        /// <remarks></remarks>

        public void checkPageAccess()
        {
            UserAccessBL objUserAccessBL = new UserAccessBL(new UserAccessRepo());
            string pageFileName = HttpContext.Current.Request.Url.AbsolutePath.Substring(HttpContext.Current.Request.Url.AbsolutePath.LastIndexOf("/") + 1);
            string menu = getPageMenu();

            if (!objUserAccessBL.checkPageAccess(menu))
            {
                Response.Redirect(PathConstants.AccessDeniedPath + "?" + PathConstants.Menu + "=" + menu);
            }

        }

        #endregion

        #region "Page Init"
        /// <summary>
        /// This event handles the page_init event of the base page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void Page_Init(object sender, EventArgs e) 
        {
            this.isSessionExist();
            this.checkPageAccess();
        }
        #endregion

        #region "Get Page Menu"
        /// <summary>
        /// Get Page Menu
        /// </summary>
        /// <remarks></remarks>

        public string getPageMenu()
        {
            string menu = string.Empty;
            if (Request.QueryString[PathConstants.MsatType] != null)
            {
                if (Request.QueryString[PathConstants.MsatType] == PathConstants.CyclicMaintenance)
                {
                    menu = PathConstants.MaintenanceMenu;
                }
                else if (Request.QueryString[PathConstants.MsatType] == PathConstants.MEServicing)
                {
                    menu = PathConstants.ServicingMenu;
                }
            }
            else
            {
                string pageFileName = HttpContext.Current.Request.Url.AbsolutePath.Substring(HttpContext.Current.Request.Url.AbsolutePath.LastIndexOf("/") + 1);
                menu = GeneralHelper.getPageMenu(pageFileName);
            }

            return menu;

        }

        #endregion

    }
}