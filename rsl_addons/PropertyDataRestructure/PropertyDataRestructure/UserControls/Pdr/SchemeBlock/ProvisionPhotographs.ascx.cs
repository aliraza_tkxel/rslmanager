﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_BusinessLogic.SchemeBlock;
using PDR_BusinessObject.SchemeBlock;
using PDR_DataAccess.SchemeBlock;
using PDR_Utilities.Constants;
using PDR_Utilities.Helpers;
using PropertyDataRestructure.Base;
using PropertyDataRestructure.Interface;

namespace PropertyDataRestructure.UserControls.Pdr.SchemeBlock
{
    public partial class ProvisionPhotographs :  UserControlBase, IListingPage, IAddEditPage
    {
        int id = 0;
        string requestType = string.Empty;
        ProvisionsBL objProvisionBL = new ProvisionsBL(new ProvisionsRepo());
        Boolean isReadOnly = false;
        String pageName = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            pageName = System.IO.Path.GetFileName(Request.Url.ToString());
            if (pageName.Contains(PathConstants.SchemeRecordPage))
            {
                isReadOnly = true;
            }
            else
            {
                isReadOnly = false;
            }
            if (isReadOnly == true)
            {
                btnUploadPhoto.Visible = false;
                btnSave.Visible = false;
            }
            try
            {
                Page.Form.Attributes.Add("enctype", "multipart/form-data");
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        protected void btnUploadPhoto_Click(object sender, EventArgs e)
        {
            try
            {
                this.resetControls();
                mdlPopUpAddPhoto.Show();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            try
            {
                if (validateData())
                {
                    uiMessage.hideMessage();
                    uiMessagePopUp.hideMessage();
                    saveData();
                    loadData();
                }
                else
                {
                    mdlPopUpAddPhoto.Show();
                }

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        /// <summary>
        /// lstViewPhotos Item Command
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lstViewPhotos_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (e.CommandName == "ShowImage")
            {
                string fileName = e.CommandArgument.ToString();
                this.GetQueryStringParams();
                if (requestType == ApplicationConstants.Scheme)
                {
                    int startIndex = fileName.IndexOf("PDRDocuments");
                    fileName = fileName.Substring(startIndex);
                    var requestURL = HttpContext.Current.Request.Url;
                    string imagePath = requestURL.GetLeftPart(UriPartial.Authority) + "/" + fileName;
                    imgDynamicImage.ImageUrl = imagePath;
                    imgDynamicImage.DataBind();
                }
                else
                {
                    imgDynamicImage.ImageUrl = fileName;
                    imgDynamicImage.DataBind();
                }

                mdlPopUpViewImage.Show();
            }
        }

        #region IListingPage,IAddEditPage Implementation

        public void saveData()
        {
            ProvisionPhotographsBO objProvisionPhotoBO = new ProvisionPhotographsBO();
            this.GetQueryStringParams();
            string filename = flUploadDoc.FileName;
            DocumentsBO objDocumentBO = new DocumentsBO();
            var fileDirectoryPath = string.Empty;
            if (requestType == ApplicationConstants.Scheme)
            {
                objProvisionPhotoBO.SchemeId = id;
                fileDirectoryPath = Server.MapPath(ResolveClientUrl(ConfigHelper.GetSchemeDocUploadPath()) + id.ToString() + "/Images/");
            }
            else if (requestType == ApplicationConstants.Block)
            {
                objProvisionPhotoBO.BlockId = id;
                fileDirectoryPath = Server.MapPath(ResolveClientUrl(ConfigHelper.GetBlockDocUploadPath()) + id.ToString() + "/Images/");
            }

            if ((Directory.Exists(fileDirectoryPath) == false))
            {
                Directory.CreateDirectory(fileDirectoryPath);
            }

            filename = FileHelper.getUniqueFileName(filename);
            objDocumentBO.DocumentName = filename;
            string filePath = fileDirectoryPath + filename;

            objProvisionPhotoBO.ImageName = filename;
            objProvisionPhotoBO.FilePath = filePath;
            objProvisionPhotoBO.UploadDate = Convert.ToDateTime(txtBoxDueDate.Text);
            objProvisionPhotoBO.Title = txtBoxTitle.Text;
            objProvisionPhotoBO.ProvisionId = objSession.TreeItemId;
            objProvisionPhotoBO.CreatedBy = objSession.EmployeeId;
            flUploadDoc.SaveAs(@filePath);

            objProvisionBL.SaveProvisionImages(objProvisionPhotoBO);
        }

        public bool validateData()
        {
            if (ValidationHelper.emptyString(txtBoxTitle.Text))
            {
                uiMessagePopUp.showErrorMessage(UserMessageConstants.fillMandatory);
                return false;
            }
            if (!ValidationHelper.validateDateFormate(txtBoxDueDate.Text))
            {
                uiMessagePopUp.showErrorMessage(UserMessageConstants.notValidDateFormat);
                return false;
            }
            if (!flUploadDoc.HasFile)
            {
                uiMessagePopUp.showErrorMessage(UserMessageConstants.SelectAFile);
                return false;
            }
            if (!this.CheckFileExtension(flUploadDoc.FileName))
            {
                uiMessage.showErrorMessage(UserMessageConstants.InvalidImageType);
                return false;
            }

            return true;
        }

        public void resetControls()
        {
            this.txtBoxDueDate.Text = string.Empty;
            this.txtBoxTitle.Text = string.Empty;
        }

        public void loadData()
        {
            if (objSession.TreeItemId == -1 || objSession.TreeItemId == -2 || objSession.TreeItemId == 0)
            {
                this.btnUploadPhoto.Visible = false;
                return;
            }
            else
            {
                if (isReadOnly == true)
                {
                    this.btnUploadPhoto.Visible = false;
                }
                else
                {
                    this.btnUploadPhoto.Visible = true;
                }
                this.GetQueryStringParams();
                int? schemeId = null;
                int? blockId = null;

                if (requestType == ApplicationConstants.Scheme)
                    schemeId = id;
                else if (requestType == ApplicationConstants.Block)
                    blockId = id;

                DataSet provisionImagesDS = objProvisionBL.GetProvisionImages(objSession.TreeItemId, schemeId, blockId);
                lstViewPhotos.DataSource = provisionImagesDS;
                lstViewPhotos.DataBind();
            }

        }

        public void populateDropDown(DropDownList ddl)
        {
            throw new NotImplementedException();
        }

        public void populateDropDowns()
        {
            throw new NotImplementedException();
        }

        public void searchData()
        {
            throw new NotImplementedException();
        }

        public void populateData()
        {
            throw new NotImplementedException();
        }

        public void printData()
        {
            throw new NotImplementedException();
        }

        public void exportGridToExcel()
        {
            throw new NotImplementedException();
        }

        public void exportToPdf()
        {
            throw new NotImplementedException();
        }

        public void downloadData()
        {
            throw new NotImplementedException();
        }

        public void applyFilters()
        {
            throw new NotImplementedException();
        }
        #endregion
        public string getImageUri(string imageName)
        {
            this.GetQueryStringParams();

            var fileDirectoryPath = string.Empty;
            if (requestType == ApplicationConstants.Scheme)
            {

                fileDirectoryPath = ResolveClientUrl(ConfigHelper.GetSchemeDocUploadPath()) + id.ToString() + "/Images/";
            }
            else if (requestType == ApplicationConstants.Block)
            {

                fileDirectoryPath = ResolveClientUrl(ConfigHelper.GetBlockDocUploadPath()) + id.ToString() + "/Images/";
            }
            return fileDirectoryPath + imageName;
        }
        private void GetQueryStringParams()
        {
            if ((Request.QueryString[ApplicationConstants.Id] != null))
                id = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);

            if ((Request.QueryString[ApplicationConstants.RequestType] != null))
                requestType = Request.QueryString[ApplicationConstants.RequestType];

        }
        bool CheckFileExtension(string fileName)
        {
            string ext = Path.GetExtension(fileName);
            switch (ext.ToLower())
            {
                case ".gif":
                    return true;
                case ".jpg":
                    return true;
                case ".jpeg":
                    return true;
                case ".png":
                    return true;
                default:
                    return false;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                mdlPopUpAddPhoto.Hide();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
   
    }
}