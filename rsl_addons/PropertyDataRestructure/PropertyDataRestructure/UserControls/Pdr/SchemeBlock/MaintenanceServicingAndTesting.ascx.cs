﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PropertyDataRestructure.Interface;
using PropertyDataRestructure.Base;
using System.Data;
using PDR_Utilities.Managers;
using PDR_Utilities.Constants;
using PDR_BusinessLogic.SchemeBlock;
using PDR_DataAccess.SchemeBlock;
using PDR_BusinessObject.SchemeBlock;
using PDR_Utilities.Helpers;
using PDR_BusinessObject.AssignToContractor;
using System.Threading;
using PDR_BusinessLogic.AssignToContractor;
using PDR_DataAccess.AssignToContractor;
using PDR_BusinessObject.Vat;

namespace PropertyDataRestructure.UserControls.Pdr.SchemeBlock
{
    public partial class MaintenanceServicingAndTesting : UserControlBase, IListingPage, IAddEditPage
    {
        public bool _readOnly = false;
        private const double noOfDaysInYear=365;
        private const double noOfDaysInMonth = 30;
        private const double noOfDaysInWeek = 7;

        AttributesBL objAttributeBL = new AttributesBL(new AttributesRepo());
        enum MSTType
        {
            PATTesting = 1,
            MaintenanceItem = 2,
            ServiceCharge = 3,
            MEServicing = 4

        }
        #region Page Load event
        protected void Page_Load(object sender, EventArgs e)
        {
            string PropPageName = Request.Path.ToString();
            if (PropPageName.Contains("SchemeDashBoard"))
            {
                _readOnly = false;
            }
            else if (PropPageName.Contains("SchemeRecord"))
            {
                _readOnly = true;
            }
            if (_readOnly == true)
            {
                pnlEditable.Enabled = false;
            }
            try
            {
                if (objSession.IsProvisionClicked)
                {
                    tblMEServicing.Visible = false;
                }
                else
                    tblMEServicing.Visible = true;
                //PAT Testing Disabled
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pops", "$('#ddlProperties').select2({ placeholder: 'Select Properties', multiple: true, }); $('#txtPropertiesPC').select2({ placeholder: 'Select Properties', multiple: true, });", true);
                rdBtnPATTesting.Attributes.Add("onclick", "ShowHidePatTesting(this,'" + pnlPATTesting.ClientID + "');");
                if (!IsPostBack)
                {
                    
                    //ScriptManager.RegisterClientScriptBlock(this.Page, typeof(UpdatePanel), Guid.NewGuid().ToString(), "loadSelect2();", true);
                    loadCycleDdl();
                    populateDropDowns();
                    string requestType = Request.QueryString[ApplicationConstants.RequestType];
                    if (requestType == ApplicationConstants.Block)
                    {
                        blockSpan.Visible = false;
                        ddlBlock.Visible = false;
                    }
                }
                else
                {
                    //populateServiceChargeDropDowns();
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {

            }
        }
        #endregion
        #region IListingPage And IAddEditPage Implementation
        public void loadData()
        {
            throw new NotImplementedException();
        }

        public void searchData()
        {
            throw new NotImplementedException();
        }

        //::ST - Populate MST Data for provision
        public void PopulateProvisionMSTData()
        {
            this.pnlEditable.Visible = true;
            DataSet provisionDS = objSession.ProvisionDataSet;  //Get the saved data set from session
            if (provisionDS != null && provisionDS.Tables[ApplicationConstants.MST].Rows.Count > 0)
            {
                int provisionId = Convert.ToInt32(objSession.TreeItemId);//Get the Provision Item Id
                DataRow MSTItem = provisionDS.Tables[ApplicationConstants.MST].AsEnumerable().Where(p => ((int)p["ProvisionId"]) == provisionId).FirstOrDefault();
                if (MSTItem != null)
                {

                    var serviceChargeResult = (from ps in provisionDS.Tables[ApplicationConstants.MST].AsEnumerable() where Convert.ToInt32(ps["MSATTypeId"]) == (int)MSTType.ServiceCharge select ps).FirstOrDefault();
                    if (serviceChargeResult != null)
                    {
                        if (serviceChargeResult["IsRequired"] != DBNull.Value)
                        {
                            rdBtnServiceCharge.SelectedValue = (Convert.ToBoolean(serviceChargeResult["IsRequired"]) == true ? "1" : "0");
                        }
                        txtApportionment.Text = (serviceChargeResult["AnnualApportionment"] != DBNull.Value ? serviceChargeResult["AnnualApportionment"].ToString() : string.Empty);
                        // txtParameter.Text = mCycleResult.First.Item("DueDate")

                    }
                    var PATTestingResult = (from ps in provisionDS.Tables[ApplicationConstants.MST].AsEnumerable() where Convert.ToInt32(ps["MSATTypeId"]) == (int)MSTType.PATTesting select ps).FirstOrDefault();
                    if (PATTestingResult != null)
                    {
                        if (PATTestingResult["IsRequired"] != DBNull.Value)
                        {
                            //PAT Testing Disabled
                            rdBtnPATTesting.SelectedValue = (Convert.ToBoolean(PATTestingResult["IsRequired"]) == true ? "1" : "0");
                        }
                        txtTestDate.Text = (PATTestingResult["LastDate"] != DBNull.Value ? PATTestingResult["LastDate"].ToString() : string.Empty);
                        txtTestingCycle.Text = (PATTestingResult["Cycle"] != DBNull.Value ? PATTestingResult["Cycle"].ToString() : string.Empty);
                        txtNextTestDate.Text = (PATTestingResult["NextDate"] != DBNull.Value ? PATTestingResult["NextDate"].ToString() : string.Empty);
                        ddlTestingCycle.SelectedValue = (PATTestingResult["CycleTypeId"] != DBNull.Value ? PATTestingResult["CycleTypeId"].ToString() : "1");
                        //PAT Testing Disabled
                        if (rdBtnPATTesting.SelectedValue == "1")
                        {
                            //pnlPATTesting.Visible = true;
                            pnlPATTesting.Style.Add("display", "table-row");
                        }
                        else
                        {
                            pnlPATTesting.Style.Add("display", "none");
                        }
                    }

                    var maintenanceItemResult = (from ps in provisionDS.Tables[ApplicationConstants.MST].AsEnumerable() where Convert.ToInt32(ps["MSATTypeId"]) == (int)MSTType.MaintenanceItem select ps).FirstOrDefault();
                    if (maintenanceItemResult != null)
                    {
                        //if (maintenanceItemResult["IsRequired"] != DBNull.Value)
                        //{
                        //    rdBtnRegularMaintenance.SelectedValue = (Convert.ToBoolean(maintenanceItemResult["IsRequired"]) == true ? "1" : "0");
                        //}
                        //txtPreviousMaintenance.Text = (maintenanceItemResult["LastDate"] != DBNull.Value ? maintenanceItemResult["LastDate"].ToString() : string.Empty);
                        //txtMaintenanceCycle.Text = (maintenanceItemResult["Cycle"] != DBNull.Value ? maintenanceItemResult["Cycle"].ToString() : string.Empty);
                        //txtNextMaintenance.Text = (maintenanceItemResult["NextDate"] != DBNull.Value ? maintenanceItemResult["NextDate"].ToString() : string.Empty);
                        //ddlMaintenanceCycle.SelectedValue = (maintenanceItemResult["CycleTypeId"] != DBNull.Value ? maintenanceItemResult["CycleTypeId"].ToString() : "1");


                    }
                    var MEServicingResult = (from ps in provisionDS.Tables[ApplicationConstants.MST].AsEnumerable() where Convert.ToInt32(ps["MSATTypeId"]) == (int)MSTType.MEServicing select ps).FirstOrDefault();
                    if (MEServicingResult != null)
                    {
                        if (MEServicingResult["IsRequired"] != DBNull.Value)
                        {
                            rdBtnServicingRequired.SelectedValue = (Convert.ToBoolean(MEServicingResult["IsRequired"]) == true ? "1" : "0");
                        }
                        txtLastServiceDate.Text = (MEServicingResult["LastDate"] != DBNull.Value ? MEServicingResult["LastDate"].ToString() : string.Empty);
                        txtServiceCycle.Text = (MEServicingResult["Cycle"] != DBNull.Value ? MEServicingResult["Cycle"].ToString() : string.Empty);
                        txtNextServiceDate.Text = (MEServicingResult["NextDate"] != DBNull.Value ? MEServicingResult["NextDate"].ToString() : string.Empty);
                        ddlServiceCycle.SelectedValue = (MEServicingResult["CycleTypeId"] != DBNull.Value ? MEServicingResult["CycleTypeId"].ToString() : "1");


                    }

                }
            }
        }

        public void populateData()
        {
            DataSet parameterDataSet = objSession.AttributeResultDataSet;
            days.Attributes.CssStyle.Add("Display", "none");
            month.Attributes.CssStyle.Add("Display", "none");
            weeks.Attributes.CssStyle.Add("Display", "none");
            if (parameterDataSet.Tables[ApplicationConstants.MST].Rows.Count > 0)
            {
                var serviceChargeResult = (from ps in parameterDataSet.Tables[ApplicationConstants.MST].AsEnumerable() where Convert.ToInt32(ps["MSATTypeId"]) == (int)MSTType.ServiceCharge select ps).FirstOrDefault();
                if (serviceChargeResult != null)
                {
                    if (serviceChargeResult["IsRequired"] != DBNull.Value)
                    {
                        rdBtnServiceCharge.SelectedValue = (Convert.ToBoolean(serviceChargeResult["IsRequired"]) == true ? "1" : "0");
                    }
                    txtApportionment.Text = (serviceChargeResult["AnnualApportionment"] != DBNull.Value ? serviceChargeResult["AnnualApportionment"].ToString() : string.Empty);
                    // txtParameter.Text = mCycleResult.First.Item("DueDate")

                }
                var PATTestingResult = (from ps in parameterDataSet.Tables[ApplicationConstants.MST].AsEnumerable() where Convert.ToInt32(ps["MSATTypeId"]) == (int)MSTType.PATTesting select ps).FirstOrDefault();
                if (PATTestingResult != null)
                {
                    if (PATTestingResult["IsRequired"] != DBNull.Value)
                    {
                        //PAT Testing Disabled
                        rdBtnPATTesting.SelectedValue = (Convert.ToBoolean(PATTestingResult["IsRequired"]) == true ? "1" : "0");
                    }
                    txtTestDate.Text = (PATTestingResult["LastDate"] != DBNull.Value ? PATTestingResult["LastDate"].ToString() : string.Empty);
                    txtTestingCycle.Text = (PATTestingResult["Cycle"] != DBNull.Value ? PATTestingResult["Cycle"].ToString() : string.Empty);
                    txtNextTestDate.Text = (PATTestingResult["NextDate"] != DBNull.Value ? PATTestingResult["NextDate"].ToString() : string.Empty);
                    ddlTestingCycle.SelectedValue = (PATTestingResult["CycleTypeId"] != DBNull.Value ? PATTestingResult["CycleTypeId"].ToString() : "1");
                    //PAT Testing Disabled
                    if (rdBtnPATTesting.SelectedValue == "1")
                    {
                        //pnlPATTesting.Visible = true;
                        pnlPATTesting.Style.Add("display", "table-row");
                    }
                    else
                    {
                        pnlPATTesting.Style.Add("display", "none");
                    }
                }

                //var maintenanceItemResult = (from ps in parameterDataSet.Tables[ApplicationConstants.MST].AsEnumerable() where Convert.ToInt32(ps["MSATTypeId"]) == (int)MSTType.MaintenanceItem select ps).FirstOrDefault();

                var MEServicingResult = (from ps in parameterDataSet.Tables[ApplicationConstants.MST].AsEnumerable() where Convert.ToInt32(ps["MSATTypeId"]) == (int)MSTType.MEServicing select ps).FirstOrDefault();
                if (MEServicingResult != null)
                {
                    if (MEServicingResult["IsRequired"] != DBNull.Value)
                    {
                        rdBtnServicingRequired.SelectedValue = (Convert.ToBoolean(MEServicingResult["IsRequired"]) == true ? "1" : "0");
                    }
                    txtLastServiceDate.Text = (MEServicingResult["LastDate"] != DBNull.Value ? MEServicingResult["LastDate"].ToString() : string.Empty);
                    txtServiceCycle.Text = (MEServicingResult["Cycle"] != DBNull.Value ? MEServicingResult["Cycle"].ToString() : string.Empty);
                    txtNextServiceDate.Text = (MEServicingResult["NextDate"] != DBNull.Value ? MEServicingResult["NextDate"].ToString() : string.Empty);
                    ddlServiceCycle.SelectedValue = (MEServicingResult["CycleTypeId"] != DBNull.Value ? MEServicingResult["CycleTypeId"].ToString() : "1");


                }
            }
            if (parameterDataSet.Tables[ApplicationConstants.CyclicServices].Rows.Count > 0)
            {
                

                DataTable dtCyclicServices = parameterDataSet.Tables[ApplicationConstants.CyclicServices];
                rdBtnBFSCarryingOutWork.SelectedValue = dtCyclicServices.Rows[0]["BFSCarryingOutWork"].ToString() == "True" ? "1" : "0";
                updateContractor();
                if (dtCyclicServices.Rows[0]["ContractorId"] != DBNull.Value)
                {
                    ddlContractor.SelectedValue = dtCyclicServices.Rows[0]["ContractorId"].ToString();
                }
                txtContractPeriod.Text = (dtCyclicServices.Rows[0]["ContractPeriod"] != DBNull.Value ? dtCyclicServices.Rows[0]["ContractPeriod"].ToString() : string.Empty);
                txtContractCommencement.Text = (dtCyclicServices.Rows[0]["Commencement"] != DBNull.Value ? dtCyclicServices.Rows[0]["Commencement"].ToString() : string.Empty);
                txtCycle.Text = (dtCyclicServices.Rows[0]["Cycle"] != DBNull.Value ? dtCyclicServices.Rows[0]["Cycle"].ToString() : string.Empty);
                txtCycleCost.Text = (dtCyclicServices.Rows[0]["CycleValue"] != DBNull.Value ? Convert.ToDecimal(dtCyclicServices.Rows[0]["CycleValue"]).ToString("#.##") : string.Empty);
                ddlCycle.SelectedValue = (dtCyclicServices.Rows[0]["CycleType"] != DBNull.Value ? dtCyclicServices.Rows[0]["CycleType"].ToString() : "0");
                ddlContractPeriod.SelectedValue = (dtCyclicServices.Rows[0]["ContractPeriodType"] != DBNull.Value ? dtCyclicServices.Rows[0]["ContractPeriodType"].ToString() : "0");
                ltrlCurrentContract.Text = (dtCyclicServices.Rows[0]["CurrentContractor"] != DBNull.Value ? dtCyclicServices.Rows[0]["CurrentContractor"].ToString() : "None");
                ltrlPoRef.Text = (dtCyclicServices.Rows[0]["PORef"] != DBNull.Value ? dtCyclicServices.Rows[0]["PORef"].ToString() : "-");
                
                DropDownListVATList.SelectedValue = (dtCyclicServices.Rows[0]["VAT"] != DBNull.Value ? dtCyclicServices.Rows[0]["VAT"].ToString() : "Please Select");
                TextBoxTotalValue.Text = (dtCyclicServices.Rows[0]["TotalValue"] != DBNull.Value ? dtCyclicServices.Rows[0]["TotalValue"].ToString() : string.Empty);
                periodDropdownList.SelectedValue = (dtCyclicServices.Rows[0]["CustomCycleType"] != DBNull.Value ? dtCyclicServices.Rows[0]["CustomCycleType"].ToString() : "Daily");
                CustomFrequency.Text = (dtCyclicServices.Rows[0]["CustomCycleFrequency"] != DBNull.Value ? dtCyclicServices.Rows[0]["CustomCycleFrequency"].ToString() : string.Empty);

                string customCycleOccurance = dtCyclicServices.Rows[0]["customCycleOccurance"].ToString();

                if (CustomFrequency.Text != "" && periodDropdownList.SelectedValue == "Weekly")
                {
                    days.Attributes.CssStyle.Add("Display", "none");
                    month.Attributes.CssStyle.Add("Display", "none");
                    weeks.Attributes.CssStyle.Add("Display", "block");
                    everyStatment.InnerText = "Week(s)";
                    string[] splittedString = customCycleOccurance.Split(';');
                    List<string> strList = splittedString.ToList();
                    foreach (var item in strList)
                    {
                        foreach (ListItem chkItem in chkWeeklyList.Items)
                        {
                            if (chkItem.Value == item)
                            {
                                chkItem.Selected = true;
                            }

                        }
                    }
                }
                else if (CustomFrequency.Text != "" && periodDropdownList.SelectedValue == "Monthly")
                {
                    days.Attributes.CssStyle.Add("Display", "block");
                    month.Attributes.CssStyle.Add("Display", "none");
                    weeks.Attributes.CssStyle.Add("Display", "none");
                    everyStatment.InnerText = "Month(s)";
                    foreach (ListItem chkItem in chkDailyList.Items)
                    {
                        if (chkItem.Value == customCycleOccurance)
                        {
                            chkItem.Selected = true;
                        }

                    }
                }
                else if (CustomFrequency.Text != "" && periodDropdownList.SelectedValue == "Yearly")
                {
                    days.Attributes.CssStyle.Add("Display", "none");
                    month.Attributes.CssStyle.Add("Display", "block");
                    weeks.Attributes.CssStyle.Add("Display", "none");
                    everyStatment.InnerText = "Year(s)";
                    foreach (ListItem chkItem in chkMonthlyList.Items)
                    {
                        if (chkItem.Value == customCycleOccurance)
                        {
                            chkItem.Selected = true;
                        }

                    }
                }

                else if (CustomFrequency.Text != "" && periodDropdownList.SelectedValue == "Daily")
                {
                    days.Attributes.CssStyle.Add("Display", "none");
                    month.Attributes.CssStyle.Add("Display", "none");
                    weeks.Attributes.CssStyle.Add("Display", "none");
                }
            }
            pnlEditable.Visible = true;

        }

        public void printData()
        {
            throw new NotImplementedException();
        }

        public void exportGridToExcel()
        {
            throw new NotImplementedException();
        }

        public void exportToPdf()
        {
            throw new NotImplementedException();
        }

        public void downloadData()
        {
            throw new NotImplementedException();
        }

        public void applyFilters()
        {
            throw new NotImplementedException();
        }

        public void saveData()
        {
            throw new NotImplementedException();
        }

        public bool validateData()
        {
            throw new NotImplementedException();
        }

        public void resetControls()
        {
            //if (rdBtnPATTesting.SelectedIndex >= 0)
            //{
            //    rdBtnPATTesting.ClearSelection();
            //}

            if (rdBtnServiceCharge.SelectedIndex >= 0)
            {
                rdBtnServiceCharge.ClearSelection();
            }
            if (rdBtnServicingRequired.SelectedIndex >= 0)
            {
                rdBtnServicingRequired.ClearSelection();
            }

            // pnlPATTesting.Visible = false;
            rdBtnBFSCarryingOutWork.ClearSelection();
            DropDownListVATList.SelectedIndex = 0;
            TextBoxTotalValue.Text = string.Empty;
            pnlPATTesting.Style.Add("display", "none");
            txtTestDate.Text = string.Empty;
            txtTestingCycle.Text = string.Empty;
            ddlTestingCycle.SelectedIndex = 0;
            txtNextTestDate.Text = string.Empty;
            ddlContractor.SelectedIndex = 0;
            txtContractCommencement.Text = string.Empty;
            txtContractPeriod.Text = string.Empty;
            ddlContractPeriod.SelectedIndex = 0;
            txtCycle.Text = string.Empty;
            ddlCycle.SelectedIndex = 0;
            ltrlCurrentContract.Text = "NONE";
            ltrlPoRef.Text = "NONE";
            txtCycleCost.Text = string.Empty;
            txtApportionment.Text = string.Empty;
            txtLastServiceDate.Text = string.Empty;
            txtServiceCycle.Text = string.Empty;
            ddlServiceCycle.SelectedIndex = 0;
            txtNextServiceDate.Text = string.Empty;
            CustomFrequency.Text = string.Empty;
            periodDropdownList.SelectedValue = "Daily";
            chkMonthlyList.SelectedValue = null;
            chkWeeklyList.SelectedValue = null;
            chkDailyList.SelectedValue = null;
            populateServiceChargeDropDowns();

        }

        public void populateDropDown(DropDownList ddl)
        {
            throw new NotImplementedException();
        }

        public void populateDropDowns()
        {
            int schemeId = -1;
            int blockId = -1;

            if ((Request.QueryString[ApplicationConstants.RequestType] != null))
            {

                string requestType = Request.QueryString[ApplicationConstants.RequestType];

                if (requestType == ApplicationConstants.Scheme)
                {
                    schemeId = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);
                    populateBlockDropdown(schemeId);
                    populatePropertiesDropdown(schemeId, Convert.ToInt32(ddlBlock.SelectedValue));

                }
                else if (requestType == ApplicationConstants.Block)
                {
                    blockId = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);
                    populatePropertiesDropdown(0, blockId);
                }


            }
            DataSet resultDataSet = new DataSet();
            resultDataSet = objAttributeBL.getCyclingServicesContractor(schemeId, blockId, "Cyclical Services");
            objSession.ContractorDataSet = resultDataSet;
            ddlContractor.Items.Clear();
            ddlContractor.DataSource = resultDataSet;
            ddlContractor.DataValueField = "id";
            ddlContractor.DataTextField = "description";
            ddlContractor.DataBind();
            ddlContractor.Items.Insert(0, new ListItem("Please Select", "-1"));
            //ddlContractor.Items.Insert(1, new ListItem("N/A", "0"));
            ddlContractor.SelectedIndex = 0;
            List<VatBo> vatBoList = new List<VatBo>();
            AssignToContractorBL objAssignToContractorBL = new AssignToContractorBL(new AssignToContractorRepo());
            objAssignToContractorBL.getVatDropDownValues(ref vatBoList);
            DropDownListVATList.Items.Insert(0, "Please Select");
            int VATDropDownListIndex = 1;
            foreach (var vatBo in vatBoList)
            {
                DropDownListVATList.Items.Insert(VATDropDownListIndex, new ListItem(vatBo.Description, vatBo.Id.ToString()));
                VATDropDownListIndex++;
            }
            objSession.VatBOList = vatBoList;
            }
        #endregion 
        protected void updateContractor(object sender, EventArgs e) 
        {
            int schemeId = -1;
            int blockId = -1;

            if ((Request.QueryString[ApplicationConstants.RequestType] != null))
            {

                string requestType = Request.QueryString[ApplicationConstants.RequestType];

                if (requestType == ApplicationConstants.Scheme)
                {
                    schemeId = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);
                    populateBlockDropdown(schemeId);
                    populatePropertiesDropdown(schemeId, Convert.ToInt32(ddlBlock.SelectedValue));

                }
                else if (requestType == ApplicationConstants.Block)
                {
                    blockId = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);
                    populatePropertiesDropdown(0, blockId);
                }
            }
            if (rdBtnBFSCarryingOutWork.SelectedValue == "0")
            {
                DataSet resultDataSet = new DataSet();
                resultDataSet = objAttributeBL.getCyclingServicesContractor(schemeId, blockId, "Cyclical Services");
                objSession.ContractorDataSet = resultDataSet;
                ddlContractor.Items.Clear();
                ddlContractor.DataSource = resultDataSet;
                ddlContractor.DataValueField = "id";
                ddlContractor.DataTextField = "description";
                ddlContractor.DataBind();
                ddlContractor.Items.Insert(0, new ListItem("Please Select", "-1"));
                
                ddlContractor.SelectedIndex = 0;

            }
            else 
            {
                ddlContractor.Items.Clear();
                ddlContractor.Items.Insert(0, new ListItem("Please Select", "-1"));
                ddlContractor.Items.Insert(1, new ListItem("N/A", "0"));
                ddlContractor.SelectedIndex = 0;
            }

        }

        // ////////////////////////VAT calculation//////////////////////////////
        protected void DropDownListVATList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                calculateVatAndTotal();
                DropDownListVATList.Focus();
                //showModalPopup();
            }
            catch (Exception ex)
            {
                //uiMessage.isError = true;
                //uiMessage.messageText = ex.Message;

                //if ((uiMessage.isExceptionLogged == false))
                //{
                //    ExceptionPolicy.HandleException(ex, "Exception Policy");
                //}
            }
            finally
            {
                //if ((uiMessage.isError == true))
                //{
                //    uiMessage.showErrorMessage(uiMessage.messageText);
                //}
            }
        }
        private void calculateVatAndTotal()
        {
            int vatRateId = Convert.ToInt32(DropDownListVATList.SelectedValue);
            int ContractPeriodType = Convert.ToInt32(ddlContractPeriod.SelectedValue);
            int CyclePeriodType = Convert.ToInt32(ddlCycle.SelectedValue);
            int ContractPeriod=Convert.ToInt32(txtContractPeriod.Text);
            int CyclePeriod = Convert.ToInt32(txtCycle.Text);
            decimal totalDays = 0;
            decimal cycle = 1;
            DateTime commencementStartDate = Convert.ToDateTime(txtContractCommencement.Text);
            DateTime commencementEndDate = new DateTime(); ;

            switch(ContractPeriodType)
            {
            case 1:
                    commencementEndDate=commencementStartDate.AddDays(ContractPeriod);
                    break;
            case 2 :
                    commencementEndDate = commencementStartDate.AddDays (ContractPeriod*7); // addWeek() method is not avalable
                    break;
            case 3 :
                    commencementEndDate = commencementStartDate.AddMonths(ContractPeriod);
                    break;
            case 4:
                    commencementEndDate = commencementStartDate.AddYears(ContractPeriod);
                    break;
            }

            totalDays = (commencementEndDate - commencementStartDate).Days;
            switch (CyclePeriodType)
            {
                case 1:
                    cycle = Math.Round((totalDays / (CyclePeriod * 1))); 
                    break;
                case 2:
                    cycle = Math.Round((totalDays / (CyclePeriod * 7)));
                    break;
                case 3:
                    cycle = Math.Round((totalDays / (CyclePeriod * 30)));
                    break;
                case 4:
                    cycle = Math.Round((totalDays / (CyclePeriod * 365)));
                    break;
            }



            decimal VatRate = 0.0M;
            if (vatRateId >= 0)
            {
                VatRate = getVatRateByVatId(vatRateId);
            }
            decimal netCost = default(decimal);
            decimal defaultNetCost = 0.0M;
            if (decimal.TryParse(txtCycleCost.Text.ToString().Trim(), out defaultNetCost))
            {
                netCost = (string.IsNullOrEmpty(txtCycleCost.Text.Trim()) ? defaultNetCost : Convert.ToDecimal(txtCycleCost.Text.Trim()));
                netCost = netCost * cycle;
                decimal vat = netCost * VatRate / 100;
                decimal total = netCost + vat;

                //txtCycleCost.Text = string.Format("{0:0.00}", netCost);
                //txtVat.Text = string.Format("{0:0.00}", vat);
                TextBoxTotalValue.Text = string.Format("{0:0.00}", total);
                if (string.IsNullOrEmpty(txtApportionment.Text))
                {
                    txtApportionment.Text = string.Format("{0:0.00}", total);
                }
                
            }
            else
            {
                //txtVat.Text = string.Empty;
                TextBoxTotalValue.Text = string.Empty;
            }

        }
        private decimal getVatRateByVatId(int vatRateId)
        {
            decimal vatRate = 0.0M;

            List<VatBo> vatBoList = objSession.VatBOList;

            VatBo vatBo = vatBoList.Find(i => i.Id == vatRateId);

            if ((vatBo != null))
            {
                vatRate = vatBo.VatRate;
            }

            return vatRate;
        }

        // /////////////////////////////////////////////////////



        private void populateServiceChargeDropDowns()
        {
            if ((Request.QueryString[ApplicationConstants.RequestType] != null))
            {

                string requestType = Request.QueryString[ApplicationConstants.RequestType];

                if (requestType == ApplicationConstants.Scheme)
                {
                    int schemeId = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);
                    populateBlockDropdown(schemeId);
                    populatePropertiesDropdown(schemeId, Convert.ToInt32(ddlBlock.SelectedValue));

                }
                else if (requestType == ApplicationConstants.Block)
                {
                    int blockId = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);
                    populatePropertiesDropdown(0, blockId);
                }


            }
        }

        #region "Populate dropdown"
        private void populateDropdown(ref DropDownList ddlLookup, DataSet resultDataSet)
        {
            ddlLookup.Items.Clear();
            ddlLookup.DataSource = resultDataSet;
            ddlLookup.DataValueField = "CycleTypeId";
            ddlLookup.DataTextField = "CycleType";
            ddlLookup.DataBind();
            //ddlLookup.Items.Insert(0, New ListItem("Please Select", -1))
            //ddlLookup.SelectedIndex = 0

        }

        #endregion

        #region "Populate Block dropdown"
        private void populateBlockDropdown(int schemeId)
        {
            DataSet resultData = new DataSet();
            resultData = objAttributeBL.getBlocksByScheme(schemeId);

            ddlBlock.Items.Clear();
            ddlBlock.DataSource = resultData;
            ddlBlock.DataValueField = "BLOCKID";
            ddlBlock.DataTextField = "BLOCKNAME";
            ddlBlock.DataBind();
            //ddlLookup.Items.Insert(0, New ListItem("Please Select", -1))
            ddlBlock.SelectedIndex = 0;

        }

        #endregion

        #region "Populate Properties dropdown"
        private void populatePropertiesDropdown(int schemeId, int blockId)
        {
            DataSet resultData = new DataSet();
            resultData = objAttributeBL.getPropertiesBySchemeBlock(schemeId, blockId);

            ddlProperties.Items.Clear();
            ddlProperties.DataSource = resultData;
            ddlProperties.DataValueField = "PROPERTYID";
            ddlProperties.DataTextField = "PROPERTYNAME";

            ddlProperties.DataBind();

            populateSelectedProperties(schemeId, blockId);
            //ddlLookup.Items.Insert(0, New ListItem("Please Select", -1))

        }

        #endregion

        #region "Populate Properties dropdown"
        private void populateSelectedProperties(int schemeId, int blockId)
        {
            DataSet resultData = new DataSet();
            resultData = objAttributeBL.getSelectedProperties(schemeId, blockId, objSession.TreeItemId);
            int count = 0;
            for (count = 0; count < resultData.Tables[0].Rows.Count; count++)
            {
                ddlProperties.Items.FindByValue(resultData.Tables[0].Rows[count][0].ToString()).Selected = true;
            }
            if (count == 0)
            {
                foreach (var item in ddlProperties.Items)
                {
                    ((ListItem)item).Selected = true;
                }
            }
            //ddlLookup.Items.Insert(0, New ListItem("Please Select", -1))

        }

        #endregion

        #region "load Cycle Ddl"
        public void loadCycleDdl()
        {
            DataSet resultDataSet = new DataSet();
            resultDataSet = objAttributeBL.loadCycleDdl();
            populateDropdown(ref ddlTestingCycle, resultDataSet);
            populateDropdown(ref ddlCycle, resultDataSet);
            populateDropdown(ref ddlContractPeriod, resultDataSet);
            populateDropdown(ref ddlServiceCycle, resultDataSet);

            //if (objSession.TreeItemName == "Appliances")
            //{
            //    pnlPATTesting.Visible = true;
            //    pnlPrePAT.Visible = true;
            //}
            //else
            //{
            //    pnlPATTesting.Visible = false;
            //    pnlPrePAT.Visible = false;
            //}

        }
        #endregion

        #region "fill MSAT Object"

        public string fillMSATObject(ref List<MSATDetailBO> msatDetailList, ref CyclicalServiceBO cyclicalService)
        {
            string message = string.Empty;
            #region add PAT Testing object in msatDetailList
            //PAT Testing Disabled
            //if ((rdBtnPATTesting.SelectedItem != null))
            //{
            //    if (Convert.ToInt32(rdBtnPATTesting.SelectedValue) > 0)
            //    {
            //        bool valid = false;

            //        MSATDetailBO objmsatDetailBo = new MSATDetailBO();
            //        objmsatDetailBo.IsRequired = Convert.ToBoolean(Convert.ToInt32(rdBtnPATTesting.SelectedValue));
            //        valid = ValidationHelper.validateDateFormate(txtTestDate.Text);
            //        if (valid == true)
            //        {
            //            objmsatDetailBo.LastDate = Convert.ToDateTime(txtTestDate.Text);
            //        }
            //        else
            //        {
            //            message = UserMessageConstants.NotValidPATDateFormat;
            //            pnlPATTesting.Style.Add("display", "table-row");
            //            return message;
            //        }
            //        if (!string.IsNullOrEmpty(txtTestingCycle.Text))
            //        {
            //            objmsatDetailBo.Cycle = Convert.ToInt32(txtTestingCycle.Text);
            //        }
            //        else
            //        {
            //            message = UserMessageConstants.EnterTestingCycle;
            //            pnlPATTesting.Style.Add("display", "table-row");
            //            return message;
            //        }

            //        objmsatDetailBo.CycleTypeId = Convert.ToInt32(ddlTestingCycle.SelectedValue);
            //        valid = ValidationHelper.validateDateFormate(txtNextTestDate.Text);
            //        if (valid == true)
            //        {
            //            objmsatDetailBo.NextDate = Convert.ToDateTime(txtNextTestDate.Text);
            //        }
            //        else
            //        {

            //            message = UserMessageConstants.NotValidPATDateFormat;
            //            pnlPATTesting.Style.Add("display", "table-row");
            //            return message;
            //        }
            //        objmsatDetailBo.MSATTypeId = Convert.ToInt32(MSTType.PATTesting);
            //        msatDetailList.Add(objmsatDetailBo);
            //    }
            //    else
            //    {
            //        MSATDetailBO objmsatDetailBo = new MSATDetailBO();
            //        objmsatDetailBo.IsRequired = Convert.ToBoolean(Convert.ToInt32(rdBtnPATTesting.SelectedValue));
            //        objmsatDetailBo.LastDate = null;
            //        objmsatDetailBo.Cycle = null;
            //        objmsatDetailBo.CycleTypeId = null;
            //        objmsatDetailBo.NextDate = null;
            //        objmsatDetailBo.MSATTypeId = Convert.ToInt32(MSTType.PATTesting);
            //        msatDetailList.Add(objmsatDetailBo);
            //    }
            //}
            #endregion
            #region  add Maintenance item object in msatDetailList

            if ((ddlContractor.SelectedItem != null))
            {
                if (Convert.ToInt32(ddlContractor.SelectedValue) >= 0)
                {
                    bool valid = false;
                    MSATDetailBO objmsatDetailBo = new MSATDetailBO();
                    cyclicalService.ContractorId = Convert.ToInt32(ddlContractor.SelectedValue);
                    valid = ValidationHelper.validateDateFormate(txtContractCommencement.Text);
                    if (valid == true)
                    {
                        DataSet contractorDataSet = objSession.ContractorDataSet;
                        //to allow N/A as contractor{ 
                        DataRow contractor = null;
                        if (txtContractCommencement.Text != null)
                        {
                            cyclicalService.Commencment = Convert.ToDateTime(txtContractCommencement.Text);
                        }
                        //}
                        if (cyclicalService.ContractorId > 0)
                        {
                            contractor = contractorDataSet.Tables[0].AsEnumerable().Where(p => ((int)p["id"]) == Convert.ToInt32(ddlContractor.SelectedValue)).FirstOrDefault();
                            DateTime contractStart = Convert.ToDateTime(contractor["STARTDATE"]);
                            DateTime contractEnd = Convert.ToDateTime(contractor["RENEWALDATE"]);
                            if (Convert.ToDateTime(txtContractCommencement.Text) >= contractStart && Convert.ToDateTime(txtContractCommencement.Text) <= contractEnd)
                            {
                                cyclicalService.Commencment = Convert.ToDateTime(txtContractCommencement.Text);
                            }
                            else
                            {

                                return "Commencement date should be between " + contractStart.ToString("dd/MM/yyyy") + " and " + contractEnd.ToString("dd/MM/yyyy") + ".";
                            }
                        }

                    }
                    else
                    {

                        message = UserMessageConstants.NotValidMaintenanceDateFormat;
                        return message;
                    }
                    if (!string.IsNullOrEmpty(txtContractPeriod.Text) && ValidationHelper.isNumber(txtContractPeriod.Text))
                    {
                        cyclicalService.ContractPeriod = Convert.ToInt32(txtContractPeriod.Text);
                    }
                    else
                    {
                        message = UserMessageConstants.EnterMaintenanceCycle;

                        return message;
                    }
                    cyclicalService.ContractPeriodType = Convert.ToInt32(ddlContractPeriod.SelectedValue);

                    if (!string.IsNullOrEmpty(txtCycle.Text) && ValidationHelper.isNumber(txtCycle.Text))
                    {
                        cyclicalService.Cycle = Convert.ToInt32(txtCycle.Text);
                    }
                    else
                    {
                        message = UserMessageConstants.EnterCyclicalCycle;

                        return message;
                    }
                    cyclicalService.CycleType = Convert.ToInt32(ddlCycle.SelectedValue);
                    if (!string.IsNullOrEmpty(txtCycleCost.Text))
                    {
                        cyclicalService.CycleValue = Convert.ToDecimal(txtCycleCost.Text.Trim());
                    }

                    if (!string.IsNullOrEmpty(DropDownListVATList.SelectedItem.Value))
                    {
                        cyclicalService.VAT = DropDownListVATList.SelectedItem.Value;
                    }

                    if (!string.IsNullOrEmpty(TextBoxTotalValue.Text))
                    {
                        cyclicalService.TotalValue = Convert.ToInt32(Convert.ToDecimal(TextBoxTotalValue.Text));
                    }


                    if (!string.IsNullOrWhiteSpace(CustomFrequency.Text) && (!string.IsNullOrEmpty(periodDropdownList.SelectedValue)))
                    {
                        int frequency = 0;
                        int.TryParse(CustomFrequency.Text.ToString(), out frequency);

                        cyclicalService.CustomCycleFrequency = frequency;
                        if (!string.IsNullOrEmpty(chkWeeklyList.SelectedValue))
                        {
                            //cyclicalService.CustomCycleOccurance = chkWeeklyList.SelectedValue;
                            List<String> YrStrList = new List<string>();
                            // Loop through each item.
                            foreach (ListItem item in chkWeeklyList.Items)
                            {
                                if (item.Selected)
                                {
                                    // If the item is selected, add the value to the list.
                                    YrStrList.Add(item.Value);
                                }

                            }
                            // Join the string together using the ; delimiter.
                            String YrStr = String.Join(";", YrStrList.ToArray());
                            cyclicalService.CustomCycleOccurance = YrStr;
                        }
                        else if (!string.IsNullOrEmpty(chkMonthlyList.SelectedValue))
                        {
                            cyclicalService.CustomCycleOccurance = chkMonthlyList.SelectedValue;
                        }
                        else if (!string.IsNullOrEmpty(chkDailyList.SelectedValue))
                        {
                            cyclicalService.CustomCycleOccurance = chkDailyList.SelectedValue;
                        }
                        cyclicalService.CustomCycleType = periodDropdownList.SelectedValue;
                    }


                    if ((rdBtnBFSCarryingOutWork.SelectedItem != null))
                    {
                        cyclicalService.BFSCarryingOutWork = Convert.ToBoolean(rdBtnBFSCarryingOutWork.SelectedValue=="1");
                    }
                    int cyclePeriod = validateCyclical(Convert.ToInt32(txtCycle.Text), ddlCycle.SelectedItem.Text.ToString());
                    int contractPeriod = validateCyclical(Convert.ToInt32(txtContractPeriod.Text), ddlContractPeriod.SelectedItem.Text.ToString());
                    if (cyclePeriod > contractPeriod)
                    {
                        message = "Contract Period should be greater then Cycle Period";
                        return message;
                    }

                }
            }
            #endregion

            #region add Service charge item object in msatDetailList

            if ((rdBtnServiceCharge.SelectedItem != null))
            {
                //if (Convert.ToInt32(rdBtnServiceCharge.SelectedValue) > 0)
                //{
                    MSATDetailBO objmsatDetailBo = new MSATDetailBO();
                    objmsatDetailBo.IsRequired = Convert.ToBoolean(Convert.ToInt32(rdBtnServiceCharge.SelectedValue));
                    if (!string.IsNullOrEmpty(txtApportionment.Text))
                    {
                        objmsatDetailBo.AnnualApportionment = Convert.ToDecimal(txtApportionment.Text);
                    }
                    objmsatDetailBo.MSATTypeId = Convert.ToInt32(MSTType.ServiceCharge);
                    msatDetailList.Add(objmsatDetailBo);
                //}
                //else
                //{
                //    MSATDetailBO objmsatDetailBo = new MSATDetailBO();
                //    objmsatDetailBo.AnnualApportionment = null;
                //    objmsatDetailBo.IsRequired = Convert.ToBoolean(Convert.ToInt32(rdBtnServiceCharge.SelectedValue));
                //    objmsatDetailBo.MSATTypeId = Convert.ToInt32(MSTType.ServiceCharge);
                //    msatDetailList.Add(objmsatDetailBo);
                //}
            }
            #endregion

            #region add M&E Servicing item object in msatDetailList
            if ((rdBtnServicingRequired.SelectedItem != null))
            {
                if (Convert.ToInt32(rdBtnServicingRequired.SelectedValue) > 0)
                {
                    bool valid = false;
                    MSATDetailBO objmsatDetailBo = new MSATDetailBO();
                    objmsatDetailBo.IsRequired = Convert.ToBoolean(Convert.ToInt32(rdBtnServicingRequired.SelectedValue));
                    valid = ValidationHelper.validateDateFormate(txtLastServiceDate.Text);
                    if (valid == true)
                    {
                        objmsatDetailBo.LastDate = Convert.ToDateTime(txtLastServiceDate.Text);
                    }
                    else
                    {
                        message = UserMessageConstants.NotValidMEDateFormat;
                        return message;
                    }
                    if (!string.IsNullOrEmpty(txtServiceCycle.Text))
                    {
                        objmsatDetailBo.Cycle = Convert.ToInt32(txtServiceCycle.Text);
                    }
                    else
                    {
                        message = UserMessageConstants.EnterServiceCycle;
                        return message;
                    }

                    objmsatDetailBo.CycleTypeId = Convert.ToInt32(ddlServiceCycle.SelectedValue);
                    valid = ValidationHelper.validateDateFormate(txtNextServiceDate.Text);
                    if (valid == true)
                    {
                        objmsatDetailBo.NextDate = Convert.ToDateTime(txtNextServiceDate.Text);
                    }
                    else
                    {
                        message = UserMessageConstants.NotValidMEDateFormat;
                        return message;
                    }
                    objmsatDetailBo.MSATTypeId = Convert.ToInt32(MSTType.MEServicing);
                    msatDetailList.Add(objmsatDetailBo);
                }
                else
                {
                    MSATDetailBO objmsatDetailBo = new MSATDetailBO();
                    objmsatDetailBo.IsRequired = Convert.ToBoolean(Convert.ToInt32(rdBtnServicingRequired.SelectedValue));
                    objmsatDetailBo.LastDate = null;
                    objmsatDetailBo.Cycle = null;
                    objmsatDetailBo.CycleTypeId = null;
                    objmsatDetailBo.NextDate = null;
                    objmsatDetailBo.MSATTypeId = Convert.ToInt32(MSTType.MEServicing);
                    msatDetailList.Add(objmsatDetailBo);
                }
            }
            #endregion
            return message;
        }
        #endregion

        protected void updateContractor()
        {
            int schemeId = -1;
            int blockId = -1;

            if ((Request.QueryString[ApplicationConstants.RequestType] != null))
            {

                string requestType = Request.QueryString[ApplicationConstants.RequestType];

                if (requestType == ApplicationConstants.Scheme)
                {
                    schemeId = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);
                    //populateBlockDropdown(schemeId);
                    //populatePropertiesDropdown(schemeId, Convert.ToInt32(ddlBlock.SelectedValue));

                }
                else if (requestType == ApplicationConstants.Block)
                {
                    blockId = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);
                    //populatePropertiesDropdown(0, blockId);
                }
            }
            if (rdBtnBFSCarryingOutWork.SelectedValue == "0")
            {
                DataSet resultDataSet = new DataSet();
                resultDataSet = objAttributeBL.getCyclingServicesContractor(schemeId, blockId, "Cyclical Services");
                objSession.ContractorDataSet = resultDataSet;
                ddlContractor.Items.Clear();
                ddlContractor.DataSource = resultDataSet;
                ddlContractor.DataValueField = "id";
                ddlContractor.DataTextField = "description";
                ddlContractor.DataBind();
                ddlContractor.Items.Insert(0, new ListItem("Please Select", "-1"));

                ddlContractor.SelectedIndex = 0;

            }
            else
            {
                ddlContractor.Items.Clear();
                ddlContractor.Items.Insert(0, new ListItem("Please Select", "-1"));
                ddlContractor.Items.Insert(1, new ListItem("N/A", "0"));
                ddlContractor.SelectedIndex = 0;
            }
        }

        #region "fill MSAT Object"

        public string fillServiceChargeObject(ref ServiceChargeProperties serProperties)
        {
            string message = string.Empty;
            int? schemeId = -1;
            int? blockId = -1;
            DataSet resultSet = new DataSet();
            if ((Request.QueryString[ApplicationConstants.RequestType] != null))
            {

                string requestType = Request.QueryString[ApplicationConstants.RequestType];

                if (requestType == ApplicationConstants.Scheme)
                {
                    schemeId = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);
                    blockId = Convert.ToInt32(ddlBlock.SelectedValue);
                    resultSet = objAttributeBL.getPropertiesBySchemeBlock((int)schemeId, (int)blockId);
                    if (blockId == 0)
                    {
                        blockId = null;
                    }
                }
                else if (requestType == ApplicationConstants.Block)
                {
                    blockId = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);
                    schemeId = null;
                    resultSet = objAttributeBL.getPropertiesBySchemeBlock(0, (int)blockId);
                }
                else
                {
                    message = "error";
                    return message;
                }
            }
            else
            {
                message = "error";
                return message;
            }

            serProperties.SchemeId = schemeId;
            serProperties.BlockId = blockId;
            serProperties.excludedIncludedProperties = new List<ExInProperties>();
            foreach (var item in resultSet.Tables[0].Rows)
            {
                var exProperties = new ExInProperties();
                exProperties.PropertyId = (string)((DataRow)item)[0];
                exProperties.IsIncluded = 0;
                var indexes = ddlProperties.GetSelectedIndices();
                foreach (var index in ddlProperties.GetSelectedIndices())
                {
                    if (ddlProperties.Items[index].Value == exProperties.PropertyId)
                    {
                        exProperties.IsIncluded = 1;
                        break;
                    }
                }
                serProperties.excludedIncludedProperties.Add(exProperties);
            }
            if (serProperties.excludedIncludedProperties.Count <= 0)
            {
                message = "error";
            }
            //resultSet = objAttributeBL.getPropertiesBySchemeBlock(schemeId, blockId);


            return message;
        }
        #endregion


        private int validateCyclical(int contractPeriod, string contractPeriodType)
        {
            int result = 0;
            if (contractPeriodType == "Week(s)")
            {
                result = contractPeriod * 7;
            }
            else if (contractPeriodType == "Month(s)")
            {
                result = contractPeriod * 30;
            }
            else if (contractPeriodType == "Year(s)")
            {
                result = contractPeriod * 52 * 7;
            }
            else
            {
                result = contractPeriod;
            }

            return result;
        }

        protected void blockIndex_changed(object sender, EventArgs e)
        {

            var blockId = Convert.ToInt32(ddlBlock.SelectedValue);
            var schemeId = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);
            populatePropertiesDropdown(schemeId, blockId);
            //objBlockBL.getAreaDropdownValue(ref areaDataSet, Convert.ToInt32(ddlCategory.SelectedItem.Value));
            //if (areaDataSet.Tables[0].Rows.Count > 0)
            //{
            //    ddlArea.DataSource = areaDataSet;
            //    ddlArea.DataValueField = "AreaID";
            //    ddlArea.DataTextField = "AreaName";
            //    ddlArea.DataBind();
            //}
            //ddlArea.Items.Insert(0, new ListItem("Select Area", "-1"));
            //ddlItem.Items.Insert(0, new ListItem("Select Item", "-1"));
            //mdlAddWarrantyPopup.Show();

        }

        protected void ddlProperties_Load(object sender, EventArgs e)
        {
            //ScriptManager.RegisterClientScriptBlock(this.Page, typeof(UpdatePanel), Guid.NewGuid().ToString(), "loadSelect2();", true);
        }

        //protected void ddlContractor_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    DataSet resultDataSet = new DataSet();
        //    resultDataSet=objSession.ContractorDataSet;

        //}
    }
}