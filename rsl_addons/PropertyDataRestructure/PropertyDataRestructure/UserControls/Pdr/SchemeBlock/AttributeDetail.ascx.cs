﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PDR_Utilities.Constants;
using PDR_Utilities.Managers;
using PDR_BusinessObject.SchemeBlock;
using System.Data;
using System.Globalization;
using PDR_Utilities.Helpers;
using PDR_BusinessLogic.SchemeBlock;
using PDR_DataAccess.SchemeBlock;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PropertyDataRestructure.Base;
using PropertyDataRestructure.Interface;

namespace PropertyDataRestructure.UserControls.Pdr.SchemeBlock
{
    public partial class AttributeDetail : UserControlBase, IAddEditPage
    {
        #region Properties
        AttributesBL objAttributeBL = new AttributesBL(new AttributesRepo());
        public bool _readOnly = false;
        public Action delegateInvoke { get; set; }
        public Action delegateTreeView { get; set; }
        int id = 0;
        string requestType = string.Empty;
        #endregion
        #region Page Load event
        protected void Page_Load(object sender, EventArgs e)
        {
            string PropPageName = Request.Path.ToString();
            if (PropPageName.Contains("SchemeDashBoard"))
            {
                _readOnly = false;
            }
            else if (PropPageName.Contains("SchemeRecord"))
            {
                _readOnly = true;
            }
            if (_readOnly == true)
            {
                btnAmend.Visible = false;
            }
        }
        #endregion



        #region "lnk Btn Detail Tab Click"
        /// <summary>
        /// lnk Btn Detail Tab Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void lnkBtnDetailTab_Click(object sender, EventArgs e)
        {
            try
            {
                lnkBtnDetailTab.CssClass = ApplicationConstants.TabClickedCssClass;
                lnkBtnNotesTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnPhotographsTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnAppliancesTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnDetectorTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnDefectsTab.CssClass = ApplicationConstants.TabInitialCssClass;
                MainView.ActiveViewIndex = 0;
                if (_readOnly == true)
                {
                    btnAmend.Visible = false;
                }
                else
                {
                    btnAmend.Visible = true;
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }

        #endregion

        #region "lnk Btn Notes Tab Click"
        /// <summary>
        /// lnk Btn Notes Tab Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>

        protected void lnkBtnNotesTab_Click(object sender, EventArgs e)
        {
            try
            {
                lnkBtnDetailTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnNotesTab.CssClass = ApplicationConstants.TabClickedCssClass;
                lnkBtnPhotographsTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnAppliancesTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnDetectorTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnDefectsTab.CssClass = ApplicationConstants.TabInitialCssClass;
                ucAttributeNotes.loadData();
                btnAmend.Visible = false;
                MainView.ActiveViewIndex = 1;
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region "lnk Btn Photographs Tab Click"
        /// <summary>
        /// lnk Btn Photographs Tab Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void lnkBtnPhotographsTab_Click(object sender, EventArgs e)
        {
            try
            {

                lnkBtnDetailTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnNotesTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnPhotographsTab.CssClass = ApplicationConstants.TabClickedCssClass;
                lnkBtnAppliancesTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnDetectorTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnDefectsTab.CssClass = ApplicationConstants.TabInitialCssClass;
                btnAmend.Visible = false;
                ucAttributePhotographs.loadData();

                if (SessionManager.getIsPhotoGraphExist() != null)
                {
                    bool isPhotoGraphExist = Convert.ToBoolean(SessionManager.getIsPhotoGraphExist());
                    if (isPhotoGraphExist)
                    {
                        lnkBtnPhotographsTab.Text =
                        "Photographs: <img src='../../../../Images/CameraIcon.png'  width='12px' height='12px' /> ";
                    }
                    else
                    {
                        lnkBtnPhotographsTab.Text = "Photographs: ";
                    }
                }

                if (SessionManager.getIsNotesExist() != null)
                {
                    bool isNotesExist = Convert.ToBoolean(SessionManager.getIsNotesExist());
                    if (isNotesExist)
                    {
                        lnkBtnNotesTab.Text =
                        "Notes: <img src='../../../../Images/notes_icon.png'  width='16px' height='12px' /> ";
                    }
                    else
                    {
                        lnkBtnNotesTab.Text = "Notes: ";
                    }
                }

                MainView.ActiveViewIndex = 2;
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion
        #region "lnk Btn Appliance Tab Click"
        /// <summary>
        /// lnk Btn Appliance Tab Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void lnkBtnAppliancesTab_Click(object sender, EventArgs e)
        {
            try
            {
                lnkBtnDetailTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnNotesTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnPhotographsTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnAppliancesTab.CssClass = ApplicationConstants.TabClickedCssClass;
                lnkBtnDetectorTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnDefectsTab.CssClass = ApplicationConstants.TabInitialCssClass;
                btnAmend.Visible = false;
                ucAppliances.loadData();
                MainView.ActiveViewIndex = 3;
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion
        #region "lnk Btn Defect Tab Click"
        /// <summary>
        /// lnk Btn Defect Tab Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        public void lnkBtnDefectsTab_Click(object sender, EventArgs e)
        {
            try
            {
                lnkBtnDetailTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnNotesTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnPhotographsTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnAppliancesTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnDetectorTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnDefectsTab.CssClass = ApplicationConstants.TabClickedCssClass;
                btnAmend.Visible = false;
                defectTab.loadDefects();
                MainView.ActiveViewIndex = 5;
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        protected void lnkBtnDetectorTab_Click(object sender, EventArgs e)
        {
            //setSelectedMenuItemStyle(lnkBtnDetectorTab);
            lnkBtnDetailTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnNotesTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnPhotographsTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnAppliancesTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnDetectorTab.CssClass = ApplicationConstants.TabClickedCssClass;
            lnkBtnDefectsTab.CssClass = ApplicationConstants.TabInitialCssClass;
            btnAmend.Visible = false;
            MainView.ActiveViewIndex = 4;
            detectorTab.loadData();
        }


        #region Btn Amend click event
        /// <summary>
        /// btn Amend click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAmend_Click(object sender, EventArgs e)
        {
            #region commented code will used for readonly and editable view, don't delete it
            //if (!objSession.AmendAttribute)
            //{
            //    objSession.AmendAttribute = true;
            //    btnAmend.Text = "Save";
            //    if (this.delegateInvoke != null)
            //    {
            //        this.delegateInvoke();
            //    }
            //}
            //else
            //{
            //    btnAmend.Text = "Amend";
            //}
            #endregion
            try
            {
                Page.Validate("save");
                if (Page.IsValid)
                {
                    saveData();
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion


        #region "Amend Item Attribute Detail"
        /// <summary>
        /// Amend Item Attribute Detail
        /// </summary>
        /// <param name="itemDetailList"></param>
        /// <param name="itemDateList"></param>
        /// <remarks></remarks>
        private void amendAttribute(ref List<ItemDetailBO> itemDetailList, ref List<ItemDatesControlBO> itemDateList, ref List<ConditionRatingBO> conditionRatingList)
        {

            string itemName = objSession.TreeItemName;
            //'if item name is heating then validation will be applien on base of landlord appliance check box
            if (itemName.Contains("Boiler"))
            {
                bool result = validateData();
                if (result == false)
                {
                    return;
                }
            }
            DataSet parameterDataSet = objSession.AttributeResultDataSet;
            if (parameterDataSet.Tables[ApplicationConstants.Parameters].Rows.Count > 0)
            {
                int conditionValueId = 0;

                foreach (DataRow dr in parameterDataSet.Tables[ApplicationConstants.Parameters].Rows)
                {
                    ItemDetailBO objItemDetailBo = new ItemDetailBO();
                    ItemDatesControlBO objItemDatesControlBo = new ItemDatesControlBO();
                    string ControlType = dr["ControlType"].ToString();
                    int parameterId = Convert.ToInt32(dr["ParameterID"]);
                    objItemDetailBo.ItemParamId = Convert.ToInt32(dr["ItemParamID"]);
                    string parameterName = dr["ParameterName"].ToString();
                    #region commented portion will used to create planned component
                    //if (SessionManager.getAttributrTypeId() == 1) {
                    //    dynamic attributeResult = (from ps in parameterDataSet.Tables(ApplicationConstants.plannedComponent)where ps.Item("ITEMID").ToString() == SessionManager.getTreeItemId().ToString() & object.ReferenceEquals(ps.Item("PARAMETERID"), DBNull.Value) & object.ReferenceEquals(ps.Item("VALUEID"), DBNull.Value)ps);
                    //    if (attributeResult.Count > 0) {
                    //        int plannedComponentId = attributeResult.First.Item("COMPONENTID");
                    //        SessionManager.setPlannedComponentId(plannedComponentId);
                    //    }
                    //} else if (SessionManager.getAttributrTypeId() == 2) {
                    //    dynamic parameterLifeCycleResult = (from ps in parameterDataSet.Tables(ApplicationConstants.plannedComponent)where ps.Item("PARAMETERID").ToString() == parameterId.ToString() & !object.ReferenceEquals(ps.Item("PARAMETERID"), DBNull.Value) & object.ReferenceEquals(ps.Item("VALUEID"), DBNull.Value)ps);

                    //    if (parameterLifeCycleResult.Count > 0) {
                    //        int plannedComponentId = parameterLifeCycleResult.First.Item("COMPONENTID");
                    //        SessionManager.setPlannedComponentId(plannedComponentId);

                    //    } else {
                    //        SessionManager.setPlannedComponentId(0);
                    //    }
                    //}
                    #endregion
                    //fill object for drop down
                    if (ControlType.ToUpper() == "Dropdown".ToUpper())
                    {
                        DropDownList ddlParameter = (DropDownList)ucItemDetail.FindControl(ApplicationConstants.ddlParameter + parameterId.ToString());
                        objItemDetailBo.ParameterValue = ddlParameter.SelectedItem.Text;
                        objItemDetailBo.ValueId = Convert.ToInt32(ddlParameter.SelectedValue);
                        if (Convert.ToInt32(ddlParameter.SelectedValue) > 0 && ddlParameter.SelectedItem.Text != "Please Select")
                        {
                            itemDetailList.Add(objItemDetailBo);
                        }
                    }
                    // fill object for Date
                    else if (ControlType.ToUpper() == "Date".ToUpper())
                    {
                        // There can be different type of date control on single page, get exact control id via if conditions
                        TextBox txtDate = getDateControl(parameterName, parameterId);
                        objItemDatesControlBo.ParameterId = parameterId;
                        objItemDatesControlBo.ParameterName = parameterName;
                        System.DateTime dateTime = default(System.DateTime);
                        bool valid = System.DateTime.TryParseExact(txtDate.Text, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dateTime);

                        if (valid == true || !(txtDate.Enabled))
                        {
                            // first time else condition will be true for  parameters which has last key word in parameter name(i.e Last replaced, Last Rewired etc)
                            //last replaced and replacement due will be in single entry with parameterId null but last rewired, rewired due will be single line with
                            //parameterid of last date(i.e ;ast replaced,last rewired etc)
                            if (parameterName.Contains("Due"))
                            {
                                if (parameterName.Contains("Rewire Due") | parameterName.Contains("Upgrade Due") | parameterName.ToUpper() == "Replacement Due".ToUpper())
                                {
                                    itemDateList[itemDateList.Count - 1].DueDate = txtDate.Text;
                                    //itemDateList[itemDateList.Count - 1].ComponentId = SessionManager.getPlannedComponentId();
                                    itemDateList[itemDateList.Count - 1].ParameterName = parameterName;
                                    if (parameterName.ToUpper() == "Replacement Due".ToUpper())
                                    {
                                        itemDateList[itemDateList.Count - 1].ParameterId = 0;
                                    }
                                }
                                else
                                {
                                    objItemDatesControlBo.DueDate = txtDate.Text;
                                    //objItemDatesControlBo.ComponentId = SessionManager.getPlannedComponentId();
                                    itemDateList.Add(objItemDatesControlBo);
                                }
                            }
                            else if (parameterName.Contains("Last Rewired") | parameterName.Contains("Upgrade Last Done") | parameterName.ToUpper() == "Last Replaced".ToUpper())
                            {
                                objItemDatesControlBo.LastDone = txtDate.Text;
                                // objItemDatesControlBo.DueDate = String.Empty

                                //objItemDatesControlBo.ComponentId = SessionManager.getPlannedComponentId();
                                itemDateList.Add(objItemDatesControlBo);

                            }
                            else
                            {
                                objItemDatesControlBo.LastDone = string.Empty;
                                objItemDatesControlBo.DueDate = txtDate.Text;
                                //objItemDatesControlBo.ComponentId = SessionManager.getPlannedComponentId()
                                itemDateList.Add(objItemDatesControlBo);
                            }

                        }
                        else
                        {
                            uiMessage.isError = true;
                            uiMessage.messageText = UserMessageConstants.ValidDateFormat + parameterName;
                            return;
                        }

                        // Dim selectedvalue As String = ddlParameter.SelectedValue
                    }
                    //fill object for Textbox control
                    else if (ControlType.ToUpper() == "Textbox".ToUpper())
                    {
                        TextBox txtBox = (TextBox)ucItemDetail.FindControl(ApplicationConstants.txt + parameterId.ToString());
                        if ((txtBox.Text != ""))
                        {
                            objItemDetailBo.ParameterValue = txtBox.Text;
                            objSession.FireToolQuantity = txtBox.Text;
                            itemDetailList.Add(objItemDetailBo);
                        }

                    }
                    //fill object for text area control
                    else if (ControlType.ToUpper() == "TextArea".ToUpper())
                    {
                        TextBox txtBox = (TextBox)ucItemDetail.FindControl(ApplicationConstants.txtArea + parameterId.ToString());
                        if (conditionValueId > 0)
                        {
                            if ((txtBox.Text != ""))
                            {
                                objItemDetailBo.ParameterValue = txtBox.Text;
                                itemDetailList.Add(objItemDetailBo);
                            }
                            #region this portion will be used while implementing condition rating  please dont delete it
                            //int attrId = SessionManager.getAttributrTypeId();

                            //if (attrId > 0 && itemDateList.Count > 0)
                            //{

                            //    foreach (ItemDatesControlBO itemDate in itemDateList)
                            //    {
                            //        ConditionRatingBO objConditionWorkBO = new ConditionRatingBO();
                            //        if (itemDate.ComponentId > 0)
                            //        {
                            //            objConditionWorkBO.ComponentId = itemDate.ComponentId;
                            //        }
                            //        objConditionWorkBO.WorksRequired = txtBox.Text.Trim();
                            //        objConditionWorkBO.ValueId = conditionValueId;
                            //        conditionRatingList.Add(objConditionWorkBO);
                            //    }
                            //}
                            //else
                            //{
                            //    ConditionRatingBO objConditionWorkBO = new ConditionRatingBO();
                            //    // objConditionWorkBO.ComponentId = DBNull.Value
                            //    objConditionWorkBO.WorksRequired = txtBox.Text;
                            //    objConditionWorkBO.ValueId = conditionValueId;
                            //    conditionRatingList.Add(objConditionWorkBO);
                            //}

                            #endregion
                        }
                    }
                    // fill object for checboxes control
                    else if (ControlType.ToUpper() == "Checkboxes".ToUpper())
                    {
                        CheckBoxList chkList = (CheckBoxList)ucItemDetail.FindControl(ApplicationConstants.chkList + parameterId.ToString());
                        foreach (ListItem item in chkList.Items)
                        {
                            ItemDetailBO objSubItemDetailBO = new ItemDetailBO();
                            objSubItemDetailBO.ItemParamId = Convert.ToInt32(dr["ItemParamID"]);
                            objSubItemDetailBO.ParameterValue = item.Text;
                            objSubItemDetailBO.ValueId = Convert.ToInt32(item.Value);
                            if (item.Selected)
                            {
                                objSubItemDetailBO.IsCheckBoxSelected = true;
                            }
                            else
                            {
                                objSubItemDetailBO.IsCheckBoxSelected = false;
                            }
                            itemDetailList.Add(objSubItemDetailBO);
                        }

                    }
                    //fill object for radio button control
                    else if (ControlType.ToUpper() == "Radiobutton".ToUpper())
                    {
                        RadioButtonList rdBtn = (RadioButtonList)ucItemDetail.FindControl(ApplicationConstants.rdBtn + parameterId.ToString());
                        foreach (ListItem item in rdBtn.Items)
                        {
                            if (item.Selected)
                            {
                                objItemDetailBo.ParameterValue = item.Text;
                                objItemDetailBo.ValueId = Convert.ToInt32(item.Value);
                                itemDetailList.Add(objItemDetailBo);
                                if (item.Text.ToUpper() != ApplicationConstants.conditionSatisfactory.ToUpper())
                                {
                                    conditionValueId = Convert.ToInt32(item.Value);
                                }
                                else
                                {
                                    conditionValueId = 0;

                                }
                            }
                        }
                    }
                }
            }

        }

        #endregion

        #region "Amend Boiler Item Attribute Detail"
        /// <summary>
        /// Amend Boiler Attribute Detail
        /// </summary>
        /// <param name="itemDetailList"></param>
        /// <param name="itemDateList"></param>
        /// <remarks></remarks>
        private void amendBoilerAttribute(ref List<ItemDetailBO> itemDetailList, ref List<ItemDatesControlBO> itemDateList, ref List<ConditionRatingBO> conditionRatingList)
        {

            string itemName = objSession.TreeItemName;
            //'if item name is heating then validation will be applien on base of landlord appliance check box
            if (itemName.Contains("Boiler"))
            {
                bool result = validateData();
                if (result == false)
                {
                    return;
                }
            }
            DataSet parameterDataSet = objSession.AttributeResultDataSet;
            if (parameterDataSet.Tables[ApplicationConstants.Parameters].Rows.Count > 0)
            {
                int conditionValueId = 0;

                foreach (DataRow dr in parameterDataSet.Tables[ApplicationConstants.Parameters].Rows)
                {
                    ItemDetailBO objItemDetailBo = new ItemDetailBO();
                    ItemDatesControlBO objItemDatesControlBo = new ItemDatesControlBO();
                    string ControlType = dr["ControlType"].ToString();
                    int parameterId = Convert.ToInt32(dr["ParameterID"]);
                    objItemDetailBo.ItemParamId = Convert.ToInt32(dr["ItemParamID"]);
                    string parameterName = dr["ParameterName"].ToString();
                    #region commented portion will used to create planned component
                    //if (SessionManager.getAttributrTypeId() == 1) {
                    //    dynamic attributeResult = (from ps in parameterDataSet.Tables(ApplicationConstants.plannedComponent)where ps.Item("ITEMID").ToString() == SessionManager.getTreeItemId().ToString() & object.ReferenceEquals(ps.Item("PARAMETERID"), DBNull.Value) & object.ReferenceEquals(ps.Item("VALUEID"), DBNull.Value)ps);
                    //    if (attributeResult.Count > 0) {
                    //        int plannedComponentId = attributeResult.First.Item("COMPONENTID");
                    //        SessionManager.setPlannedComponentId(plannedComponentId);
                    //    }
                    //} else if (SessionManager.getAttributrTypeId() == 2) {
                    //    dynamic parameterLifeCycleResult = (from ps in parameterDataSet.Tables(ApplicationConstants.plannedComponent)where ps.Item("PARAMETERID").ToString() == parameterId.ToString() & !object.ReferenceEquals(ps.Item("PARAMETERID"), DBNull.Value) & object.ReferenceEquals(ps.Item("VALUEID"), DBNull.Value)ps);

                    //    if (parameterLifeCycleResult.Count > 0) {
                    //        int plannedComponentId = parameterLifeCycleResult.First.Item("COMPONENTID");
                    //        SessionManager.setPlannedComponentId(plannedComponentId);

                    //    } else {
                    //        SessionManager.setPlannedComponentId(0);
                    //    }
                    //}
                    #endregion
                    //fill object for drop down
                    if (ControlType.ToUpper() == "Dropdown".ToUpper())
                    {
                        DropDownList ddlParameter = (DropDownList)ucItemDetail.FindControl(ApplicationConstants.ddlParameter + parameterId.ToString());
                        objItemDetailBo.ParameterValue = ddlParameter.SelectedItem.Text;
                        objItemDetailBo.ValueId = Convert.ToInt32(ddlParameter.SelectedValue);
                        if (Convert.ToInt32(ddlParameter.SelectedValue) > 0 && ddlParameter.SelectedItem.Text != "Please Select")
                        {
                            itemDetailList.Add(objItemDetailBo);
                        }
                    }
                    // fill object for Date
                    else if (ControlType.ToUpper() == "Date".ToUpper())
                    {
                        // There can be different type of date control on single page, get exact control id via if conditions
                        TextBox txtDate = getDateControl(parameterName, parameterId);
                        objItemDatesControlBo.ParameterId = parameterId;
                        objItemDatesControlBo.ParameterName = parameterName;
                        System.DateTime dateTime = default(System.DateTime);
                        bool valid = System.DateTime.TryParseExact(txtDate.Text, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dateTime);

                        if (valid == true || !(txtDate.Enabled))
                        {
                            // first time else condition will be true for  parameters which has last key word in parameter name(i.e Last replaced, Last Rewired etc)
                            //last replaced and replacement due will be in single entry with parameterId null but last rewired, rewired due will be single line with
                            //parameterid of last date(i.e ;ast replaced,last rewired etc)
                            if (parameterName.Contains("Due"))
                            {
                                if (parameterName.Contains("Rewire Due") | parameterName.Contains("Upgrade Due") | parameterName.ToUpper() == "Replacement Due".ToUpper())
                                {
                                    itemDateList[itemDateList.Count - 1].DueDate = txtDate.Text;
                                    //itemDateList[itemDateList.Count - 1].ComponentId = SessionManager.getPlannedComponentId();
                                    itemDateList[itemDateList.Count - 1].ParameterName = parameterName;
                                    if (parameterName.ToUpper() == "Replacement Due".ToUpper())
                                    {
                                        itemDateList[itemDateList.Count - 1].ParameterId = 0;
                                    }
                                }
                                else
                                {
                                    objItemDatesControlBo.DueDate = txtDate.Text;
                                    //objItemDatesControlBo.ComponentId = SessionManager.getPlannedComponentId();
                                    itemDateList.Add(objItemDatesControlBo);
                                }
                            }
                            else if (parameterName.Contains("Last Rewired") | parameterName.Contains("Upgrade Last Done") | parameterName.ToUpper() == "Last Replaced".ToUpper())
                            {
                                objItemDatesControlBo.LastDone = txtDate.Text;
                                // objItemDatesControlBo.DueDate = String.Empty

                                //objItemDatesControlBo.ComponentId = SessionManager.getPlannedComponentId();
                                itemDateList.Add(objItemDatesControlBo);

                            }
                            else
                            {
                                objItemDatesControlBo.LastDone = string.Empty;
                                objItemDatesControlBo.DueDate = txtDate.Text;
                                //objItemDatesControlBo.ComponentId = SessionManager.getPlannedComponentId()
                                itemDateList.Add(objItemDatesControlBo);
                            }

                        }
                        else
                        {
                            uiMessage.isError = true;
                            uiMessage.messageText = UserMessageConstants.ValidDateFormat + parameterName;
                            return;
                        }

                        // Dim selectedvalue As String = ddlParameter.SelectedValue
                    }
                    //fill object for Textbox control
                    else if (ControlType.ToUpper() == "Textbox".ToUpper())
                    {
                        TextBox txtBox = (TextBox)ucItemDetail.FindControl(ApplicationConstants.txt + parameterId.ToString());
                        if ((txtBox.Text != ""))
                        {
                            objItemDetailBo.ParameterValue = txtBox.Text;
                            itemDetailList.Add(objItemDetailBo);
                        }

                    }
                    //fill object for text area control
                    else if (ControlType.ToUpper() == "TextArea".ToUpper())
                    {
                        TextBox txtBox = (TextBox)ucItemDetail.FindControl(ApplicationConstants.txtArea + parameterId.ToString());
                        if (conditionValueId > 0)
                        {
                            if ((txtBox.Text != ""))
                            {
                                objItemDetailBo.ParameterValue = txtBox.Text;
                                itemDetailList.Add(objItemDetailBo);
                            }
                            #region this portion will be used while implementing condition rating  please dont delete it
                            //int attrId = SessionManager.getAttributrTypeId();

                            //if (attrId > 0 && itemDateList.Count > 0)
                            //{

                            //    foreach (ItemDatesControlBO itemDate in itemDateList)
                            //    {
                            //        ConditionRatingBO objConditionWorkBO = new ConditionRatingBO();
                            //        if (itemDate.ComponentId > 0)
                            //        {
                            //            objConditionWorkBO.ComponentId = itemDate.ComponentId;
                            //        }
                            //        objConditionWorkBO.WorksRequired = txtBox.Text.Trim();
                            //        objConditionWorkBO.ValueId = conditionValueId;
                            //        conditionRatingList.Add(objConditionWorkBO);
                            //    }
                            //}
                            //else
                            //{
                            //    ConditionRatingBO objConditionWorkBO = new ConditionRatingBO();
                            //    // objConditionWorkBO.ComponentId = DBNull.Value
                            //    objConditionWorkBO.WorksRequired = txtBox.Text;
                            //    objConditionWorkBO.ValueId = conditionValueId;
                            //    conditionRatingList.Add(objConditionWorkBO);
                            //}

                            #endregion
                        }
                    }
                    // fill object for checboxes control
                    else if (ControlType.ToUpper() == "Checkboxes".ToUpper())
                    {
                        CheckBoxList chkList = (CheckBoxList)ucItemDetail.FindControl(ApplicationConstants.chkList + parameterId.ToString());
                        foreach (ListItem item in chkList.Items)
                        {
                            ItemDetailBO objSubItemDetailBO = new ItemDetailBO();
                            objSubItemDetailBO.ItemParamId = Convert.ToInt32(dr["ItemParamID"]);
                            objSubItemDetailBO.ParameterValue = item.Text;
                            objSubItemDetailBO.ValueId = Convert.ToInt32(item.Value);
                            if (item.Selected)
                            {
                                objSubItemDetailBO.IsCheckBoxSelected = true;
                            }
                            else
                            {
                                objSubItemDetailBO.IsCheckBoxSelected = false;
                            }
                            itemDetailList.Add(objSubItemDetailBO);
                        }

                    }
                    //fill object for radio button control
                    else if (ControlType.ToUpper() == "Radiobutton".ToUpper())
                    {
                        RadioButtonList rdBtn = (RadioButtonList)ucItemDetail.FindControl(ApplicationConstants.rdBtn + parameterId.ToString());
                        foreach (ListItem item in rdBtn.Items)
                        {
                            if (item.Selected)
                            {
                                objItemDetailBo.ParameterValue = item.Text;
                                objItemDetailBo.ValueId = Convert.ToInt32(item.Value);
                                itemDetailList.Add(objItemDetailBo);
                                if (item.Text.ToUpper() != ApplicationConstants.conditionSatisfactory.ToUpper())
                                {
                                    conditionValueId = Convert.ToInt32(item.Value);
                                }
                                else
                                {
                                    conditionValueId = 0;

                                }
                            }
                        }
                    }
                }
            }

        }

        #endregion

        #region get Date Control for validation and fill object
        private TextBox getDateControl(string parameterName, int parameterId)
        {
            TextBox txtDate = default(TextBox);
            if (parameterName.Contains("Rewire Due"))
            {
                txtDate = (TextBox)ucItemDetail.FindControl(ApplicationConstants.txtDate + ApplicationConstants.rewiredDue.ToString());
            }
            else if (parameterName.Contains("Upgrade Due"))
            {
                txtDate = (TextBox)ucItemDetail.FindControl(ApplicationConstants.txtDate + ApplicationConstants.upgradeDue.ToString());
            }
            else if (parameterName.Contains("Replacement Due"))
            {
                txtDate = (TextBox)ucItemDetail.FindControl(ApplicationConstants.txtDate + ApplicationConstants.ReplacementDue.ToString());
            }
            else if (parameterName.Contains("Last Rewired"))
            {
                txtDate = (TextBox)ucItemDetail.FindControl(ApplicationConstants.txtDate + ApplicationConstants.lastRewired.ToString());
            }
            else if (parameterName.Contains("Upgrade Last Done"))
            {
                txtDate = (TextBox)ucItemDetail.FindControl(ApplicationConstants.txtDate + ApplicationConstants.lastUpdrade.ToString());
            }
            else if (parameterName.Contains("Last Replaced"))
            {
                txtDate = (TextBox)ucItemDetail.FindControl(ApplicationConstants.txtDate + ApplicationConstants.LastReplaced.ToString());
            }
            else
            {
                txtDate = (TextBox)ucItemDetail.FindControl(ApplicationConstants.txtDate + parameterId.ToString());

            }
            return txtDate;
        }
        #endregion
        #region get Query String values
        private void getQueryString()
        {

            if ((Request.QueryString[ApplicationConstants.Id] != null))
            {
                id = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);

            }

            if ((Request.QueryString[ApplicationConstants.RequestType] != null))
            {

                requestType = Request.QueryString[ApplicationConstants.RequestType];
            }
        }
        #endregion


        #region "Set Selected Menu Item (Link Button) Style"

        /// <summary>
        /// Set the style of the selected link button(Horizontal Tab Menu):
        /// Set all item to the initial styles and selected item differently.    ''' 
        /// </summary>
        /// <param name="lnkBtnSelected">Selected menu item.</param>
        /// <remarks></remarks>
        public void setSelectedMenuItemStyle(ref LinkButton lnkBtnSelected)
        {
            lnkBtnDetailTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnAppliancesTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnNotesTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnPhotographsTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnDefectsTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnSelected.CssClass = ApplicationConstants.TabClickedCssClass;
        }

        #endregion

        #region IAddEditPage Implementation
        public void saveData()
        {
            int fireToolQuantity = 1;
            getQueryString();
            int? schemeId = null;
            int? blockId = null;

            if (requestType == ApplicationConstants.Scheme)
            {
                schemeId = id;
            }
            else if (requestType == ApplicationConstants.Block)
            {
                blockId = id;
            }
            List<ItemDetailBO> itemDetailList = new List<ItemDetailBO>();
            List<ItemDatesControlBO> itemDateList = new List<ItemDatesControlBO>();
            List<ConditionRatingBO> conditionRatingList = new List<ConditionRatingBO>();
            ItemAttributeBO objItemAttributeBo = new ItemAttributeBO();
            List<MSATDetailBO> msatDetailList = new List<MSATDetailBO>();
            UserControl detailsControl = (UserControl)MainView.FindControl("detailsTabID");
            UserControl ucMSAT = (UserControl)ucItemDetail.FindControl("ucMSAT");
            MaintenanceServicingAndTesting objMST = (MaintenanceServicingAndTesting)ucMSAT;
            this.amendAttribute(ref itemDetailList, ref itemDateList, ref conditionRatingList);
            CyclicalServiceBO cyclicalService = new CyclicalServiceBO();
            string result = objMST.fillMSATObject(ref msatDetailList, ref cyclicalService);
            ServiceChargeProperties serProperties = new ServiceChargeProperties();
            string message = objMST.fillServiceChargeObject(ref serProperties);
            if (uiMessage.isError == false && result == string.Empty)
            {
                objItemAttributeBo.SchemeId = schemeId;
                objItemAttributeBo.BlockId = blockId;
                objItemAttributeBo.ItemId = objSession.TreeItemId;
                objItemAttributeBo.UpdatedBy = objSession.EmployeeId;
                string itemName = objSession.TreeItemName;

                if (itemName.Contains("Water Meter")
                    || itemName.Contains("Passenger Lift")
                    || itemName.Contains("Stair Lift")
                    || itemName.Contains("Assisted Bath")
                    || itemName.Contains("Hoist")
                    || itemName.Contains("Tumble Dryer")
                    || itemName.Contains("Washing Machine")
                    || itemName.Contains("Gas")
                    || itemName.Contains("Electricity"))
                {
                    objItemAttributeBo.attributeName = objSession.TreeItemName;
                    objItemAttributeBo.ChildAttributeMappingId = objSession.ChildeAttributMappingId;
                }



                if (itemName.Contains("Boiler"))
                {
                    objItemAttributeBo.HeatingMappingId = objSession.HeatingMappingId;
                }


                int attributeChildId = objAttributeBL.amendAttributeDetail(objItemAttributeBo, GeneralHelper.convertToDataTable(itemDetailList), GeneralHelper.convertToDataTable(itemDateList), GeneralHelper.convertToDataTable(conditionRatingList), GeneralHelper.convertToDataTable(msatDetailList), cyclicalService);

                if (message == string.Empty)
                {
                    objAttributeBL.saveServiceChargeProperties(serProperties, GeneralHelper.convertToDataTable(serProperties.excludedIncludedProperties), objItemAttributeBo.ItemId, attributeChildId);
                }
                DataSet attributeResultDataSet = objAttributeBL.getItemDetail(objSession.TreeItemId, schemeId, blockId);
                objSession.AttributeResultDataSet = attributeResultDataSet;
                DataTable dtCyclicServices = attributeResultDataSet.Tables[ApplicationConstants.CyclicServices];
                if (dtCyclicServices.Rows.Count > 0)
                {
                    if (dtCyclicServices.Rows[0]["StatusId"] != DBNull.Value && (Convert.ToInt32(dtCyclicServices.Rows[0]["StatusId"]) != 2 && Convert.ToInt32(dtCyclicServices.Rows[0]["StatusId"]) != 3))
                    {
                        uiMessage.showInformationMessage(UserMessageConstants.itemAddedSuccessfuly);

                    }
                    else
                    {
                        uiMessage.showInformationMessage(UserMessageConstants.itemAddedSuccessfuly + " But Amendment of service item is not allowed because PO has already been generated for this service item.");
                    }

                }
                else
                { uiMessage.showInformationMessage(UserMessageConstants.itemAddedSuccessfuly); }

                if (this.delegateInvoke != null)
                {
                    this.delegateInvoke();
                }
                if (this.delegateTreeView != null)
                {
                    this.delegateTreeView();
                }
            }
            else
            {
                if (result != string.Empty)
                {
                    uiMessage.messageText = result;
                }
                uiMessage.showErrorMessage(uiMessage.messageText);
            }

        }

        public bool validateData()
        {
            DataSet parameterDataSet = objSession.AttributeResultDataSet;
            //get landloard appliance parameter to check checkbox is selected or not.
            var attributeResult = (from ps in parameterDataSet.Tables[ApplicationConstants.Parameters].AsEnumerable() where ps["ParameterName"].ToString() == "Landlord Appliance" select ps).FirstOrDefault();
            if (attributeResult != null)
            {
                int parameterId = Convert.ToInt32(attributeResult["ParameterID"]);
                CheckBoxList chkList = (CheckBoxList)ucItemDetail.FindControl(ApplicationConstants.chkList + parameterId.ToString());
                bool chkboxSelected = false;
                foreach (ListItem item in chkList.Items)
                {
                    if (item.Selected)
                    {
                        chkboxSelected = true;
                    }
                }
                if (chkboxSelected == true)
                {
                    foreach (DataRow dr in parameterDataSet.Tables[ApplicationConstants.Parameters].Rows)
                    {
                        ItemDetailBO objItemDetailBo = new ItemDetailBO();
                        ItemDatesControlBO objItemDatesControlBo = new ItemDatesControlBO();
                        string ControlType = dr["ControlType"].ToString();
                        parameterId = Convert.ToInt32(dr["ParameterID"]);
                        string parameterName = dr["ParameterName"].ToString();
                        if (ControlType.ToUpper() == "Dropdown".ToUpper())
                        {
                            DropDownList ddlParameter = (DropDownList)ucItemDetail.FindControl(ApplicationConstants.ddlParameter + parameterId.ToString());
                            if (Convert.ToInt32(ddlParameter.SelectedValue) <= 0 | ddlParameter.SelectedItem.Text == "Please Select")
                            {
                                ddlParameter.Focus();
                                uiMessage.isError = true;
                                uiMessage.messageText = UserMessageConstants.MandatoryField;
                                return false;

                            }

                        }
                        else if (ControlType.ToUpper() == "Date".ToUpper())
                        {
                            TextBox txtDate = getDateControl(parameterName, parameterId);

                            bool valid = ValidationHelper.validateDateFormate(txtDate.Text);

                            if (valid == false)
                            {
                                txtDate.Focus();
                                uiMessage.isError = true;
                                uiMessage.messageText = UserMessageConstants.notValidDateFormat;
                                return false;

                            }
                        }
                        else if (ControlType.ToUpper() == "Textbox".ToUpper())
                        {
                            TextBox txtBox = (TextBox)ucItemDetail.FindControl(ApplicationConstants.txt + parameterId.ToString());
                            if (txtBox.Text == null | string.IsNullOrEmpty(txtBox.Text))
                            {
                                txtBox.Focus();
                                uiMessage.isError = true;
                                uiMessage.messageText = UserMessageConstants.MandatoryField;
                                return false;

                            }
                        }
                        else if (ControlType.ToUpper() == "Radiobutton".ToUpper())
                        {
                            RadioButtonList rdBtn = (RadioButtonList)ucItemDetail.FindControl(ApplicationConstants.rdBtn + parameterId.ToString());
                            int isSelected = 0;
                            foreach (ListItem item in rdBtn.Items)
                            {
                                if (item.Selected)
                                {
                                    isSelected = isSelected + 1;
                                }
                            }
                            if (isSelected == 0)
                            {
                                rdBtn.Focus();
                                uiMessage.isError = true;
                                uiMessage.messageText = UserMessageConstants.MandatoryField;
                                return false;

                            }
                        }
                    }
                }
            }
            return true;

        }

        public void resetControls()
        {
            throw new NotImplementedException();
        }

        public void loadData()
        {
            throw new NotImplementedException();
        }

        public void populateDropDown(DropDownList ddl)
        {
            throw new NotImplementedException();
        }

        public void populateDropDowns()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}