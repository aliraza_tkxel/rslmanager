﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProvisionItem.ascx.cs"
    Inherits="PropertyDataRestructure.UserControls.Pdr.SchemeBlock.ProvisionItem" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Panel ID="pnlEditableProvisionItem" runat="server">
    <table>
        <tr>
            <td align="right" style="width: 150px;">
                Category:<span class="Required">*</span>
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:DropDownList ID="txtProvisionCategory" runat="server" Width="175px" OnSelectedIndexChanged="CategoryChanged"
                    AutoPostBack="true">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="reqProvisionCategory" CssClass="Required" InitialValue="-1"
                    ControlToValidate="txtProvisionCategory" ValidationGroup="save" ErrorMessage="Please select Provision Category"
                    runat="server"></asp:RequiredFieldValidator>
            </td>
        </tr>
    </table>
    <table id="FieldsTable" runat="server">
        <tr id="Description" runat="server">
            <td align="right" style="width: 150px;">
                <asp:Label runat="server" ID="lblDescription" Text="Description:"></asp:Label><span
                    class="Required">*</span>
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:TextBox ID="txtDescription" runat="server" Width="175px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rqfvDescription" CssClass="Required" ControlToValidate="txtDescription"
                    ValidationGroup="save" ErrorMessage="Please enter Description" runat="server"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr id="Type" runat="server">
            <td align="right" style="width: 150px;">
                Type:<span class="Required">*</span>
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:DropDownList ID="txtType" runat="server" Width="175px">
                    <asp:ListItem Value="-1" Text="Select Type"></asp:ListItem>
                    <asp:ListItem Value="1" Text="Tenant"></asp:ListItem>
                    <asp:ListItem Value="2" Text="Communal"></asp:ListItem>
                    <asp:ListItem Value="3" Text="Office"></asp:ListItem>
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rqfvType" CssClass="Required" ControlToValidate="txtType"
                    ValidationGroup="save" InitialValue="-1" ErrorMessage="Please select Type" runat="server"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr id="Quantity" runat="server">
            <td align="right" style="width: 150px;">
                Quantity:<span class="Required">*</span>
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:TextBox ID="txtQuantity" runat="server" onkeyup="NumberOnly(this);" Width="175px"></asp:TextBox>
                <asp:CompareValidator CssClass="Required" ID="CompareValidatorQuantity" runat="server"
                    Visible="true" ControlToValidate="txtQuantity" ErrorMessage="Must be &gt; 0"
                    Operator="GreaterThan" Type="Integer" ValidationGroup="save" ValueToCompare="0" />
                <asp:RequiredFieldValidator ID="rqfvQuantity" CssClass="Required" ControlToValidate="txtQuantity"
                    ValidationGroup="save" ErrorMessage="Please enter Quantity" runat="server"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr id="Location" runat="server">
            <td align="right" style="width: 150px;">
                Location:
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:TextBox ID="txtLocation" runat="server" Width="175px"></asp:TextBox>
            </td>
        </tr>
        <tr id="Supplier" runat="server">
            <td align="right" style="width: 150px;">
                Supplier:<span class="Required">*</span>
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:DropDownList ID="txtSupplier" runat="server" Width="175px">
                </asp:DropDownList>
                <asp:RequiredFieldValidator ID="rqfvSupplier" CssClass="Required" ControlToValidate="txtSupplier"
                    InitialValue="-1" ValidationGroup="save" ErrorMessage="Please select Supplier"
                    runat="server"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr id="Model" runat="server">
            <td align="right" style="width: 150px;">
                Model/Ref Name:
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:TextBox ID="txtModel" runat="server" Width="175px"></asp:TextBox>
            </td>
        </tr>
        <tr id="Serial" runat="server">
            <td align="right" style="width: 150px;">
                Serial Number:<span runat="server" id="rqIconSerialNumber" class="Required">*</span>
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:TextBox ID="txtSerialNumber" runat="server" Width="175px"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rqfvSerialNumber" CssClass="Required" ControlToValidate="txtSerialNumber"
                    ValidationGroup="save" runat="server" ErrorMessage="Please enter serial number."></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr id="Stock" runat="server">
            <td align="right" style="width: 150px;">
                BHG Stock Ref:
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:TextBox ID="txtStockRef" runat="server" Width="175px"></asp:TextBox>
            </td>
        </tr>
        <tr id="Gas" runat="server">
            <td align="right" style="width: 150px;">
                Gas Appliance:
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:RadioButtonList RepeatDirection="Horizontal" ID="txtGasAppliance" runat="server"
                    Style="display: inline">
                    <asp:ListItem Value="True" Text="Yes"></asp:ListItem>
                    <asp:ListItem Value="False" Text="No"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr id="Sluice" runat="server">
            <td align="right" style="width: 150px;">
                Sluice:
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:RadioButtonList RepeatDirection="Horizontal" ID="txtSluice" runat="server" Style="display: inline">
                    <asp:ListItem Value="True" Text="Yes"></asp:ListItem>
                    <asp:ListItem Value="False" Text="No"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr id="DuctServicingDiv" runat="server">
            <td align="right" style="width: 150px;">
                Duct Servicing:
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:RadioButtonList runat="server" ID="DuctServicing" RepeatDirection="Horizontal"
                    CellPadding="5">
                    <asp:ListItem Text="Yes" Value="True" />
                    <asp:ListItem Text="No" Value="False" />
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr id="PurchaseDate" runat="server">
            <td align="right" style="width: 150px;">
                Purchased Date:<span class="Required">*</span>
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:TextBox ID="txtInstalledDate" runat="server" Width="150px"></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtenderInstalledDate" runat="server" Format="dd/MM/yyyy"
                    PopupButtonID="imgInstalledDate" TargetControlID="txtInstalledDate" />
                <asp:Image ID="imgInstalledDate" runat="server" ImageUrl="~/Images/Calendar.png" />
                <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="txtInstalledDate"
                    CssClass="Required" Display="Dynamic" ErrorMessage="&lt;br/&gt; Enter a valid date"
                    Operator="DataTypeCheck" Type="Date" ValidationGroup="save" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtInstalledDate"
                    CssClass="Required" ErrorMessage="Please enter Purchase Date." ValidationGroup="save"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr id="PurchaseCost" runat="server">
            <td align="right" style="width: 150px;">
                Purchased Cost(£):<span runat="server" id="reqIconInstalledCost" class="Required">*</span>
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:TextBox MaxLength="4" ID="txtInstalledCost" runat="server" Width="50px" Style="padding-right: 20px;"
                    onkeyup="NumberOnly(this);" AutoPostBack="true" OnTextChanged="updateReplacementDue"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rqfvInstalledCost" CssClass="Required" ControlToValidate="txtInstalledCost"
                    ValidationGroup="save" runat="server" ErrorMessage="Please enter Purchased Cost"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr id="Warranty" runat="server">
            <td align="right" style="width: 150px;">
                Warranty:
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:RadioButtonList RepeatDirection="Horizontal" ID="txtWarranty" runat="server"
                    Style="display: inline">
                    <asp:ListItem Value="True" Text="Yes"></asp:ListItem>
                    <asp:ListItem Value="False" Text="No"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr id="Owned" runat="server">
            <td align="right" style="width: 150px;">
                Owned/Leased:
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:DropDownList ID="txtOwned" runat="server" Width="175px">
                    <asp:ListItem Text="Owned" Value="True" />
                    <asp:ListItem Text="Leased" Value="False" />
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="ExpiryDate" runat="server">
            <td align="right" style="width: 150px;">
                Lease Expiry Date:
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:TextBox ID="txtLeaseExpiryDate" runat="server" Width="150px"></asp:TextBox>
                <asp:CalendarExtender ID="CalendarLeaseExpiryDate" runat="server" Format="dd/MM/yyyy"
                    PopupButtonID="Image4" TargetControlID="txtLeaseExpiryDate" />
                <asp:Image ID="Image4" runat="server" ImageUrl="~/Images/Calendar.png" />
                <asp:CompareValidator ID="CompareValidator7" runat="server" ControlToValidate="txtLeaseExpiryDate"
                    ErrorMessage="<br/> Enter a valid date" Operator="DataTypeCheck" Type="Date"
                    Display="Dynamic" CssClass="Required" ValidationGroup="save" />
            </td>
        </tr>
        <tr id="LeaveCost" runat="server">
            <td align="right" style="width: 150px;">
                Annual Lease Cost (£):<span runat="server" id="reqIconAnnualLease" class="Required">*</span>
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:TextBox ID="txtAnnualLease" runat="server" Width="50px" Style="padding-right: 20px;"
                    onkeyup="NumberOnly(this);" AutoPostBack="true" OnTextChanged="updateReplacementDue"></asp:TextBox>
                <asp:RequiredFieldValidator ID="rqfvAnnualLease" CssClass="Required" ControlToValidate="txtAnnualLease"
                    runat="server" ValidationGroup="save" ErrorMessage="Please enter Annual Lease Cost."></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr id="LifeSpan" runat="server">
            <td align="right" style="width: 150px;">
                Life Span(Years):<span class="Required">*</span>
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:TextBox ID="txtLifeSpan" runat="server" Width="50px" Style="padding-right: 20px;"
                    onkeyup="NumberOnly(this);" AutoPostBack="true" OnTextChanged="updateReplacementDue"></asp:TextBox>
                <asp:CompareValidator ID="LifeSpanCompare" CssClass="Required" runat="server" ControlToValidate="txtLifeSpan"
                    ErrorMessage="Must be &gt; 0" Operator="GreaterThan" Type="Integer" ValidationGroup="save"
                    ValueToCompare="0" />
                <asp:CompareValidator CssClass="Required" runat="server" Operator="DataTypeCheck"
                    Type="Integer" ControlToValidate="txtLifeSpan" ValidationGroup="save" ErrorMessage="Value must be a whole number" />
                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" CssClass="Required" ControlToValidate="txtLifeSpan"
                    runat="server" ValidationGroup="save" ErrorMessage="Please enter value in Life Span."></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr id="LastReplaced" runat="server">
            <td align="right" style="width: 150px;">
                Last Replaced:
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:TextBox ID="txtLastReplaced" runat="server" Width="150px" AutoPostBack="true"
                    OnTextChanged="updateReplacementDue"></asp:TextBox>
                <asp:CalendarExtender ID="CalendarLastReplaced" runat="server" Format="dd/MM/yyyy"
                    PopupButtonID="imgLastReplaced" TargetControlID="txtLastReplaced" />
                <asp:Image ID="imgLastReplaced" runat="server" ImageUrl="~/Images/Calendar.png" />
                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtLastReplaced"
                    ErrorMessage="<br/> Enter a valid date" Operator="DataTypeCheck" Type="Date"
                    Display="Dynamic" CssClass="Required" ValidationGroup="save" />
            </td>
        </tr>
        <tr id="ReplacementDue" runat="server">
            <td align="right" style="width: 150px;">
                Replacement Due:
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:TextBox ID="txtReplacementDue" runat="server" Width="150px"></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtenderReplacementDue" runat="server" Format="dd/MM/yyyy"
                    PopupButtonID="imgReplacementDue" TargetControlID="txtReplacementDue" />
                <asp:Image ID="imgReplacementDue" runat="server" ImageUrl="~/Images/Calendar.png" />
                <asp:CompareValidator CssClass="Required" ValidationGroup="save" ID="cmpr" runat="server"
                    ControlToCompare="txtLastReplaced" ControlToValidate="txtReplacementDue" Operator="GreaterThan"
                    Type="Date" ErrorMessage="Replacement due date should be great than Last Replaced date"></asp:CompareValidator>
                <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="txtReplacementDue"
                    ErrorMessage="<br/> Enter a valid date" Operator="DataTypeCheck" Type="Date"
                    Display="Dynamic" CssClass="Required" ValidationGroup="save" />
            </td>
        </tr>
        <tr id="Condition" runat="server">
            <td align="right" style="width: 150px;">
                Condition:<span class="Required">*</span>
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:DropDownList ID="txtConditionRating" Width="200" runat="server">
                    <asp:ListItem Text="Please select" Value="-1" />
                    <asp:ListItem Text="Satisfactory" Value="2" />
                    <asp:ListItem Text="Unsatisfactory" Value="1" />
                    <asp:ListItem Text="Potentially Unsatisfactory" Value="0" />
                </asp:DropDownList>
                <asp:RequiredFieldValidator runat="server" ID="rqfvConditionRating" InitialValue="-1"
                    ControlToValidate="txtConditionRating" CssClass="Required" ErrorMessage="Condition Rating is required."
                    ValidationGroup="save"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr id="PAT" runat="server">
            <td align="right" style="width: 150px;">
                PAT Testing:
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:RadioButtonList runat="server" ID="txtPatTesting" RepeatDirection="Horizontal"
                    CellPadding="2">
                    <asp:ListItem Text="Yes" Value="True" />
                    <asp:ListItem Text="No" Value="False" />
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
    <%------------       Provision Charge      ----------------------%>
    <table id="ProvisionChargeTable" runat="server">
        <tr>
            <td colspan="2" align="left">
                <b>Provision Charge:</b>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="border-top-style: solid; border-width: thin; border-color: #c5c5c5">
                &nbsp;
            </td>
        </tr>
        <tr id="ProvisionCharge" runat="server">
            <td align="right" style="width: 150px;">
                Provision Charge Item:
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:RadioButtonList runat="server" ID="txtProvisionCharge" RepeatDirection="Horizontal"
                    CellPadding="2">
                    <asp:ListItem Text="Yes" Value="True" />
                    <asp:ListItem Text="No" Value="False" />
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr id="Tr1" runat="server">
            <td align="right" style="width: 150px;">
                Annual Apportionment (£):
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:TextBox runat="server" ID="txtAnnualAppointment" />
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="<br/>Please enter estimate, in following form XXXXXX.XXXX where X is a digit between 0 to 9."
                    Display="Dynamic" ValidationGroup="save" CssClass="Required" ControlToValidate="txtAnnualAppointment"
                    ValidationExpression="^[0-9]{1,6}(?:\.[0-9]{1,4})?$" />
                <asp:RangeValidator ID="RangeValidator1" ErrorMessage='<br/> Please enter a value between "0" and "214748.3647".'
                    ControlToValidate="txtAnnualAppointment" runat="server" MinimumValue="0" MaximumValue="214748.3647"
                    Display="Dynamic" CssClass="Required" ValidationGroup="save" Type="Double" />
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 150px;">
                <span runat="server" id="blockSpan">Property Allocation:</span>
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:DropDownList runat="server" ID="txtBlockPC" OnSelectedIndexChanged="block_changed"
                    AutoPostBack="true" Width="130px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 150px;">
                <span runat="server" id="Span1">Properties</span>:
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:ListBox runat="server" ID="txtPropertiesPC" ClientIDMode="Static" OnInit="ddlProperties_Load"
                    SelectionMode="Multiple" Width="400px"></asp:ListBox>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
    <%-----------    Servicing     -------------------%>
    <table id="Table1" runat="server">
        <tr id="ServicingPanel" runat="server">
            <td colspan="2" align="left">
                <b>Servicing:</b>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="border-top-style: solid; border-width: thin; border-color: #c5c5c5">
                &nbsp;
            </td>
        </tr>
        <tr id="ServicingRequired" runat="server">
            <td align="right" style="width: 150px;">
                Servicing Required:
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:RadioButtonList runat="server" ID="txtServicingRequired" RepeatDirection="Horizontal"
                    CellPadding="5">
                    <asp:ListItem Text="Yes" Value="True" />
                    <asp:ListItem Text="No" Value="False" />
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr id="ServicingProvider" runat="server">
            <td align="right" style="width: 150px;">
                Servicing Provider:
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:DropDownList ID="txtServicingProvider" runat="server" Width="175px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="LastServicingDate" runat="server">
            <td align="right" style="width: 150px;">
                Last Servicing Date:
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:TextBox ID="txtLastServicingDate" runat="server" Width="150px"></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender3" runat="server" Format="dd/MM/yyyy" PopupButtonID="Image2"
                    TargetControlID="txtLastServicingDate" />
                <asp:Image ID="Image2" runat="server" ImageUrl="~/Images/Calendar.png" />
                <asp:CompareValidator ID="CompareValidator5" runat="server" ControlToValidate="txtLastServicingDate"
                    ErrorMessage="<br/> Enter a valid date" Operator="DataTypeCheck" Type="Date"
                    Display="Dynamic" CssClass="Required" ValidationGroup="save" />
            </td>
        </tr>
        <tr id="ServicingCycle" runat="server">
            <td align="right" style="width: 150px;">
                Service Cycle:
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:DropDownList ID="txtServiceCycle" runat="server" Width="175px">
                    <asp:ListItem Value="-1" Text="Select Service Cycle"></asp:ListItem>
                    <asp:ListItem Value="Monthly" Text="Monthly"></asp:ListItem>
                    <asp:ListItem Value="Quarterly" Text="Quarterly"></asp:ListItem>
                    <asp:ListItem Value="Yearly" Text="Yearly"></asp:ListItem>
                    <asp:ListItem Value="Bi Annually" Text="Bi Annually"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr id="NextServicingDate" runat="server">
            <td align="right" style="width: 150px;">
                Next Service Date:
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:TextBox ID="txtNextServiceDate" runat="server" Width="150px"></asp:TextBox>
                <asp:CalendarExtender ID="CalendarExtender2" runat="server" Format="dd/MM/yyyy" PopupButtonID="Image3"
                    TargetControlID="txtNextServiceDate" />
                <asp:Image ID="Image3" runat="server" ImageUrl="~/Images/Calendar.png" />
                <asp:CompareValidator ID="CompareValidator6" runat="server" ControlToValidate="txtNextServiceDate"
                    ErrorMessage="<br/> Enter a valid date" Operator="DataTypeCheck" Type="Date"
                    Display="Dynamic" CssClass="Required" ValidationGroup="save" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
</asp:Panel>
