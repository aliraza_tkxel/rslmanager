﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Warranties.ascx.cs"
    Inherits="PropertyDataRestructure.UserControls.Pdr.SchemeBlock.Warranties" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<div style="border: 1px solid black; margin: 2px;">
<asp:UpdatePanel runat="server" ID="updPnlWarrantyListing">
    <ContentTemplate>
        <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="500px" />
        <cc1:PagingGridView runat="server" ID="grdWarranties" AutoGenerateColumns="False"
            BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px"
            CellPadding="4" ForeColor="Black" GridLines="None" Width="100%" AllowSorting="True"
            AllowPaging="false" PageSize="30" ShowHeaderWhenEmpty="true" EmptyDataText="No Record Found">
            <Columns>
                <asp:TemplateField HeaderText="Warranty Type:" SortExpression="WARRANTYTYPE" >
                    <ItemTemplate>
                        <asp:HiddenField runat="server" ID="hdnWarrantyId" Value='<%# Eval("WARRANTYID") %>' />
                        <asp:Label ID="lblWARRANTYTYPE" runat="server" Text='<%# Bind("WARRANTYTYPE") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle Width="15%"></ItemStyle>
                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Category:" SortExpression="LOCATIONNAME">
                    <ItemTemplate>
                        <asp:Label ID="lblCategory" runat="server" Text='<%# Bind("LOCATIONNAME") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle Width="15%"></ItemStyle>
                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Area:" SortExpression="AREANAME" >
                    <ItemTemplate>
                        <asp:Label ID="lblArea" runat="server" Text='<%# Bind("AREANAME") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle Width="15%"></ItemStyle>
                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Item:" SortExpression="ITEMNAME" >
                    <ItemTemplate>
                        <asp:Label ID="lblItem" runat="server" Text='<%# Bind("ITEMNAME") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle Width="15%"></ItemStyle>
                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Org/Contractor" SortExpression="CONTRACTOR">
                    <ItemTemplate>
                        <asp:Label ID="lblCONTRACTOR" runat="server" Text='<%# Bind("CONTRACTOR") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle Width="15%" />
                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Expiry:" SortExpression="Expiry">
                    <ItemTemplate>
                        <asp:Label ID="lblExpiry" runat="server" Text='<%# Bind("EXPIRYDATE") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle Width="10%" />
                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                </asp:TemplateField>
                <asp:TemplateField>
                    <EditItemTemplate>
                        <asp:LinkButton ID="LinkButton3" runat="server" CausesValidation="False" CommandName="Edit"
                            Text="Amend"></asp:LinkButton>
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:Button ID="btnAmend" runat="server" Text="Amend" BorderStyle="Solid" BorderWidth="1px"
                            CommandArgument='<%# Eval("WARRANTYID")%>' OnClick="btnAmend_Click" style="vertical-align:middle;"></asp:Button>
                        <asp:ImageButton ID="imgBtnDelete" runat="server" BorderStyle="None" BorderWidth="0px"
                            ImageUrl="~/Images/cross2.png" CommandArgument='<%# Eval("WARRANTYID")%>' OnClick="imgBtnDelete_Click" style="vertical-align:middle;" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <RowStyle BackColor="#EFF3FB"></RowStyle>
            <EditRowStyle BackColor="#2461BF"></EditRowStyle>
            <SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>
            <HeaderStyle BackColor="#ffffff" ForeColor="black" Font-Bold="True" HorizontalAlign="Left"
                CssClass="table-head"></HeaderStyle>
            <AlternatingRowStyle BackColor="White" Wrap="True"></AlternatingRowStyle>
            <PagerSettings Mode="NumericFirstLast"></PagerSettings>
        </cc1:PagingGridView>
        <br />
<asp:Button runat="server" ID="btnAddNewWarranty" Text="Add New Warranty" Style="margin-top: 10px; float:right" 
    OnClick="btnAddNewWarranty_Click" />

    </ContentTemplate>
</asp:UpdatePanel>

<asp:Panel ID="pnlAddWarrantyPopup" runat="server" CssClass="modalPopup" Style="width: 375px;
    border-color: #CCCCCC; border-width: 1px;">
    <asp:ImageButton ID="imgBtnCancelAddWarrantyPopup" runat="server" Style="position: absolute;
        top: -12px; right: -12px; width: 22px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png"
        BorderWidth="0" />
    <asp:UpdatePanel runat="server" ID="updPnlPopUp" UpdateMode="Always">
        <ContentTemplate>
            <uim:UIMessage ID="uiMessagePopup" runat="Server" Visible="false" width="500px" />

            <table style="margin-left:10px; margin-top:10px;" >
                <tr>
                    <td>
                        Warranty Type:
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlWarrantyType" Style="width: 160px;">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator runat="server" ID="rfvCostCentre" ControlToValidate="ddlWarrantyType"
                        InitialValue="-1" ErrorMessage="<br/>Please select warranty type." Display="Dynamic"
                        ValidationGroup="save" ForeColor="Red" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Category:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlCategory" runat="server" Style="width: 160px;"
                            onselectedindexchanged="categoryIndex_changed" AutoPostBack="true">
                        </asp:DropDownList>
                         <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="ddlCategory"
                        InitialValue="-1" ErrorMessage="<br/>Please select Category." Display="Dynamic"
                        ValidationGroup="save" ForeColor="Red" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Area:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlArea" runat="server" Style="width: 160px;"
                            onselectedindexchanged="areaIndex_changed" AutoPostBack="true">
                        </asp:DropDownList>
                        
                    </td>
                </tr>
                <tr>
                    <td>
                        Item:
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlItem" Style="width: 160px;">
                        </asp:DropDownList>
                        
                    </td>
                </tr>
                <tr>
                    <td>
                        Org/Contractor:
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlContractor" Style="width: 160px;">
                        </asp:DropDownList>
                         <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="ddlContractor"
                        InitialValue="-1" ErrorMessage="<br/>Please select contractor." Display="Dynamic"
                        ValidationGroup="save" ForeColor="Red" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Expiry:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtExpiry" Style="width: 116px;"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="clndrExpiry" runat="server" TargetControlID="txtExpiry"
                            PopupButtonID="imgCalDate" PopupPosition="Right" TodaysDateFormat="dd/MM/yyyy"
                            Format="dd/MM/yyyy">
                        </ajaxToolkit:CalendarExtender>
                        <asp:ImageButton ImageUrl="~/Images/calendar.png" runat="server" ID="imgCalDate"
                            Style="vertical-align: bottom; border: none;" />
                        <asp:CompareValidator ID="CompareValidator13" runat="server" 
                            ControlToValidate="txtExpiry" ErrorMessage="<b/>Enter valid date" Operator="DataTypeCheck"
                            Type="Date" Display="Dynamic" ForeColor="Red" ValidationGroup="save" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtExpiry"
                        ErrorMessage="Required" ValidationGroup="save" ForeColor="Red"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        Notes:
                    </td>
                    <td>
                        <asp:TextBox TextMode="MultiLine" runat="server" ID="txtNotes"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button runat="server" ID="btnRest" Text="RESET" OnClick="btnReset_Click" Style="float: right;" />
                    </td>
                    <td>
                        <asp:Button runat="server" ID="btnAdd" Text="ADD" OnClick="btnAdd_Click" ValidationGroup="save" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
<asp:Label ID="lblDisplaySuccessPopup" runat="server" Text=""></asp:Label>
<ajaxToolkit:ModalPopupExtender ID="mdlAddWarrantyPopup" runat="server" DynamicServicePath=""
    Enabled="True" TargetControlID="lblDisplaySuccessPopup" PopupControlID="pnlAddWarrantyPopup"
    DropShadow="true" CancelControlID="imgBtnCancelAddWarrantyPopup" BackgroundCssClass="modalBackground">
</ajaxToolkit:ModalPopupExtender>
</div>