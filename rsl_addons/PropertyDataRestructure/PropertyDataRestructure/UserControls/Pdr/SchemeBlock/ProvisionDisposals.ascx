﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProvisionDisposals.ascx.cs" Inherits="PropertyDataRestructure.UserControls.Pdr.SchemeBlock.ProvisionDisposals" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="500px" />
<asp:UpdatePanel ID="updPanelDisposals" runat="server">
    <ContentTemplate>
        <asp:GridView ID="grdProvisionDisposals" runat="server" AutoGenerateColumns="false" GridLines="None"
            ShowHeaderWhenEmpty="false" Width="100%" EmptyDataText="No Record Found">
            <Columns>
                <asp:BoundField DataField="Manufacturer" HeaderText="Manufacturer:">
                    <ItemStyle Width="15%" VerticalAlign="Top" />
                </asp:BoundField>
                <asp:BoundField DataField="Model" HeaderText="Model:">
                    <ItemStyle Width="15%" VerticalAlign="Top" />
                </asp:BoundField>
                <asp:BoundField DataField="SerialNumber" HeaderText="Serial No:">
                    <ItemStyle Width="15%" VerticalAlign="Top" />
                </asp:BoundField>
                <asp:BoundField DataField="ReplacementDue" HeaderText="Disposal Due:" DataFormatString="{0:d}">
                    <ItemStyle Width="15%" VerticalAlign="Top" />
                </asp:BoundField>

                <asp:BoundField DataField="LastReplaced" HeaderText="Actual Disposal:" DataFormatString="{0:d}">
                    <ItemStyle Width="30%" />
                </asp:BoundField>
            </Columns>
            <RowStyle BackColor="#EFF3FB"></RowStyle>
            <EditRowStyle BackColor="#2461BF"></EditRowStyle>
            <SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>
            <HeaderStyle BackColor="#ffffff" ForeColor="black" Font-Bold="True" HorizontalAlign="Left"
                CssClass="table-head"></HeaderStyle>
            <AlternatingRowStyle BackColor="White" Wrap="True"></AlternatingRowStyle>
        </asp:GridView>
    </ContentTemplate>
</asp:UpdatePanel>
