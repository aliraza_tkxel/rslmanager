﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Appliance.ascx.cs" Inherits="PropertyDataRestructure.UserControls.Pdr.SchemeBlock.Appliance" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<style type="text/css">
    .style3
    {
        width: 283px;
    }
    .style4
    {
        width: 138px;
    }
    .style5
    {
        width: 221px;
    }
</style>
<div style="margin-top: 10px">
    <%-- <asp:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </asp:ToolkitScriptManager>--%>
    <asp:UpdatePanel runat="server" ID="updPnlAppliances">
        <ContentTemplate>
            <div style="float: right; margin-top: -15px; margin-bottom: 8px;">
                <asp:Button ID="btnAddAppliance" runat="server" Text="Add Appliance" BackColor="White"
                    OnClick="btnAddAppliance_Click" />
            </div>
            <div style="display: none;">
                <asp:HiddenField ID="hdnApplianceId" runat="server" Value="-1" />
                <asp:HiddenField ID="hdnSelectedLocationId" runat="server" Value="-1" />
                <asp:HiddenField ID="hdnSelectedmakeId" runat="server" Value="-1" />
                <asp:HiddenField ID="hdnSelectedTypeId" runat="server" Value="-1" />
                <asp:HiddenField ID="hdnSelectedModelId" runat="server" Value="-1" />
            </div>
            <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="500px" />
            <div style="clear: both;">
            </div>
            <asp:Panel ID="pnlAppliancesGrid" runat="server" Height="450px" ScrollBars="Auto">
                <cc1:PagingGridView runat="server" ID="grdAppliances" AutoGenerateColumns="False"
                    BackColor="White" BorderColor="#CCCCCC" BorderWidth="1px" CellPadding="4" ForeColor="Black"
                    GridLines="Horizontal" Width="100%" AllowSorting="false" ShowHeaderWhenEmpty="True"
                    PageSize="30" AllowPaging="True" OrderBy="" EmptyDataText="No Record Found">
                    <columns>
                        <asp:TemplateField HeaderText="Item" SortExpression="Item">
                            <ItemTemplate>
                                <asp:Label ID="lblItem" runat="server" Text='<%# Bind("Item") %>'></asp:Label></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Quantity" SortExpression="Quantity">
                            <ItemTemplate>
                                <asp:Label ID="lblQuantity" runat="server" Text='<%# Bind("Quantity") %>'></asp:Label></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Dimensions" SortExpression="Dimensions">
                            <ItemTemplate>
                                <asp:Label ID="lblDimensions" runat="server" Text='<%# Bind("Dimensions") %>'></asp:Label></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Location" SortExpression="Location">
                            <ItemTemplate>
                                <asp:Label ID="lblLocation" runat="server" Text='<%# Bind("Location") %>'></asp:Label></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Make" SortExpression="Make">
                            <ItemTemplate>
                                <asp:Label ID="lblMake" runat="server" Text='<%# Bind("Make") %>'></asp:Label></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Serial No" SortExpression="SerialNumber">
                            <ItemTemplate>
                                <asp:Label ID="lblSerialNumber" runat="server" Text='<%# Bind("SerialNumber") %>'></asp:Label></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Purchase Cost" SortExpression="PurchaseCost">
                            <ItemTemplate>
                                <asp:Label ID="lblPurchaseCost" runat="server" Text='<%# Bind("PurchaseCost") %>'></asp:Label></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Date Purchased" SortExpression="DatePurchased">
                            <ItemTemplate>
                                <asp:Label ID="lblDatePurchased" runat="server" Text='<%# Bind("DatePurchased") %>'></asp:Label></ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ShowHeader="False">
                            <ItemTemplate>
                                <asp:Button ID="btnAmend" runat="server" CommandArgument='<%# Eval("ApplianceId") %>'
                                    Text="Amend" OnClick="btnAmend_Click" BackColor="White" />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Right" />
                        </asp:TemplateField>
                    </columns>
                    <rowstyle backcolor="#EFF3FB"></rowstyle>
                    <editrowstyle backcolor="#2461BF"></editrowstyle>
                    <selectedrowstyle backcolor="#D1DDF1" forecolor="#333333" font-bold="True"></selectedrowstyle>
                    <headerstyle backcolor="#ffffff" forecolor="black" font-bold="True" horizontalalign="Left"
                        cssclass="table-head"></headerstyle>
                    <alternatingrowstyle backcolor="White" wrap="True"></alternatingrowstyle>
                </cc1:PagingGridView>
                <asp:Label ID="Message" CssClass="Required" runat="server" Visible="False" />
            </asp:Panel>
            <asp:Panel runat="server" ID="pnlPagination" Visible="true" Style="border-top: 1px solid  #000;
                vertical-align: middle;">
                <table style="width: 100%;">
                    <tbody>
                        <tr>
                            <td>
                                <asp:LinkButton ID="lnkbtnPagerFirst" Text="" runat="server" CommandName="Page" CommandArgument="First"
                                    OnClick="lnkbtnPager_Click" Style="text-decoration: none;">&lt;&lt;First</asp:LinkButton>
                                &nbsp;
                            </td>
                            <td>
                                <asp:LinkButton ID="lnkbtnPagerPrev" Text="" runat="server" CommandName="Page" CommandArgument="Prev"
                                    OnClick="lnkbtnPager_Click" Style="text-decoration: none;">&lt;Prev</asp:LinkButton>
                                &nbsp;
                            </td>
                            <td>
                                Page:&nbsp;
                                <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="Required" />
                                &nbsp;of&nbsp;
                                <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />. Records:
                                <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                                &nbsp;to&nbsp;
                                <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                                &nbsp;of&nbsp;
                                <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                            </td>
                            <td>
                                <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page"
                                    CommandArgument="Next" OnClick="lnkbtnPager_Click" Style="text-decoration: none;" />
                                &nbsp;
                            </td>
                            <td>
                                <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page"
                                    CommandArgument="Last" OnClick="lnkbtnPager_Click" Style="text-decoration: none;" />
                                &nbsp;
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
<asp:Panel ID="pnlAddAppliancePopup" runat="server" BackColor="White" Width="430px"
    Height="600px">
    <table id="pnlAddActivityTable" style="margin: 2px; border-color: Black !important;
        width: 100%;">
        <tr>
            <td colspan="2" valign="top">
                <div style="float: left; font-weight: bold; padding-left: 10px; padding-top: 6px;">
                    <asp:Label Text="Add New Appliance" runat="server" ID="lblNewAppliance" />
                </div>
                <div style="clear: both">
                </div>
                <hr />
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" colspan="2">
                <asp:Panel ID="pnlAddApplianceMessage" runat="server" Visible="false">
                    <asp:Label ID="lblAddApplianceMessage" runat="server" Text=""></asp:Label>
                </asp:Panel>
            </td>
        </tr>
        <tr style="height: 35px;">
            <td align="left" valign="top" width="120px" style="padding-left: 10px;">
                Item:
            </td>
            <td align="left" valign="top">
                <asp:TextBox runat="server" ID="txtItem" Width="200" />
            </td>
        </tr>
        <tr style="height: 35px;">
            <td align="left" valign="top" width="120px" style="padding-left: 10px;">
                Quantity:
            </td>
            <td align="left" valign="top">
                <asp:TextBox runat="server" ID="txtQuantity" Width="200" />
            </td>
        </tr>
        <tr style="height: 35px;">
            <td align="left" valign="top" width="120px" style="padding-left: 10px;">
                Dimensions:
            </td>
            <td align="left" valign="top">
                <asp:TextBox runat="server" ID="txtDimensions" Width="200" />
            </td>
        </tr>
        <tr style="height: 35px;">
            <td align="left" valign="top" width="120px" style="padding-left: 10px;">
                Location:
            </td>
            <td align="left" valign="top">
                <asp:TextBox ID="txtLocation" runat="server" AutoPostBack="false" AutoCompleteType="Search"
                    Width="200"></asp:TextBox>
                <asp:AutoCompleteExtender ID="autoComplete1" runat="server" EnableCaching="true"
                    BehaviorID="AutoCompleteEx" TargetControlID="txtLocation" ServicePath="~/Views/Pdr/Scheme/Dashboard/SchemeDashBoard.aspx"
                    ServiceMethod="getApplianceLocations" CompletionInterval="1000" CompletionSetCount="1"
                    CompletionListCssClass="autocomplete_completionListElement" CompletionListElementID="locationList"
                    CompletionListItemCssClass="autocomplete_listItem" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                    DelimiterCharacters=";, :" ShowOnlyCurrentWordInCompletionListItem="true" OnClientItemSelected="LocationItemSelected"></asp:AutoCompleteExtender>
                <div id="locationList">
                </div>
            </td>
        </tr>
        <tr style="height: 35px;">
            <td align="left" valign="top" width="120px" style="padding-left: 10px;">
                Make:
            </td>
            <td align="left" valign="top">
                <asp:TextBox ID="txtMake" runat="server" AutoPostBack="false" AutoCompleteType="Search"
                    Width="200"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender1" runat="server" EnableCaching="true"
                    TargetControlID="txtMake" ServicePath="~/Views/Pdr/Scheme/Dashboard/SchemeDashBoard.aspx"
                    ServiceMethod="getApplianceManufacturer" CompletionInterval="1000" CompletionSetCount="1"
                    CompletionListCssClass="autocomplete_completionListElement" CompletionListElementID="makeList"
                    CompletionListItemCssClass="autocomplete_listItem" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                    DelimiterCharacters=";, :" ShowOnlyCurrentWordInCompletionListItem="true" OnClientItemSelected="makeItemSelected"></asp:AutoCompleteExtender>
                <div id="makeList">
                </div>
            </td>
        </tr>
        <tr style="height: 35px;">
            <td align="left" valign="top" width="120px" style="padding-left: 10px;">
                Type:
            </td>
            <td align="left" valign="top">
                <asp:TextBox ID="txtType" runat="server" AutoPostBack="false" AutoCompleteType="Search"
                    Width="200"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender2" runat="server" EnableCaching="true"
                    TargetControlID="txtType" ServicePath="~/Views/Pdr/Scheme/Dashboard/SchemeDashBoard.aspx"
                    ServiceMethod="getApplianceType" CompletionInterval="1000" CompletionSetCount="1"
                    CompletionListCssClass="autocomplete_completionListElement" CompletionListElementID="typeList"
                    CompletionListItemCssClass="autocomplete_listItem" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                    DelimiterCharacters=";, :" ShowOnlyCurrentWordInCompletionListItem="true" OnClientItemSelected="typeItemSelected"></asp:AutoCompleteExtender>
                <div id="typeList">
                </div>
            </td>
        </tr>
        <tr style="height: 35px;">
            <td align="left" valign="top" width="120px" style="padding-left: 10px;">
                Model:
            </td>
            <td align="left" valign="top">
                <asp:TextBox ID="txtModel" runat="server" MaxLength="50" Width="200"></asp:TextBox>
                <asp:AutoCompleteExtender ID="AutoCompleteExtender3" runat="server" EnableCaching="true"
                    TargetControlID="txtModel" ServicePath="~/Views/Pdr/Scheme/Dashboard/SchemeDashBoard.aspx"
                    ServiceMethod="getApplianceModel" CompletionInterval="1000" CompletionSetCount="1"
                    CompletionListCssClass="autocomplete_completionListElement" CompletionListElementID="modelList"
                    CompletionListItemCssClass="autocomplete_listItem" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                    DelimiterCharacters=";, :" ShowOnlyCurrentWordInCompletionListItem="true" OnClientItemSelected="modelItemSelected"></asp:AutoCompleteExtender>
                <div id="modelList">
                </div>
            </td>
        </tr>
        <tr style="height: 35px;">
            <td align="left" valign="top" width="120px" style='padding-left: 10px;'>
                Serial No:
            </td>
            <td align="left" valign="top">
                <asp:TextBox ID="txtSerialNumber" runat="server" MaxLength="50" Width="200"></asp:TextBox>
            </td>
        </tr>
         <tr style="height: 35px;">
            <td align="left" valign="top" width="140px" style="padding-left: 10px;">
                Life Span:
            </td>
            <td align="left" valign="top">
                <asp:DropDownList runat="server" ID="ddlLifeSpan" Width="200" Style='float: left;'>
                </asp:DropDownList>
                <p style='float: left; margin: 0px; padding-left: 3px;'>
                    Year(s)</p>
            </td>
        </tr>
        <tr style="height: 35px;">
            <td align="left" valign="top" width="120px" style="padding-left: 10px;">
                Purchase Cost(£):
            </td>
            <td align="left" valign="top">
                <asp:TextBox runat="server" ID="txtPurchaseCost" Width="200" onkeyup="NumberOnly(this)" />
            </td>
        </tr>
        <tr style="height: 35px;">
            <td align="left" valign="top" width="120px" style="padding-left: 10px;">
                Date Purchased:
            </td>
            <td align="left" valign="top">
                <asp:CalendarExtender ID="calInstalledDate" runat="server" DaysModeTitleFormat="dd/MM/yyyy"
                    TargetControlID="txtDatePurchased" PopupButtonID="imgCalInstalledDate" PopupPosition="Right"
                    TodaysDateFormat="dd/MM/yyyy" Format="dd/MM/yyyy"></asp:CalendarExtender>
                <asp:TextBox ID="txtDatePurchased" runat="server" Width="200"></asp:TextBox>&nbsp;<img
                    alt="Open Calendar" src="../../../Images/calendar.png" id="imgCalInstalledDate" />
                <asp:CompareValidator ID="CompareValidator7" runat="server" ControlToValidate="txtDatePurchased"
                    ErrorMessage="<br/>Enter a valid date" Operator="DataTypeCheck" Type="Date" Display="Dynamic"
                    CssClass="Required" ValidationGroup="save" />
            </td>
        </tr>
        <tr style="height: 35px;">
            <td align="left" valign="top" width="120px" style="padding-left: 10px;">
                Date Removed:
            </td>
            <td align="left" valign="top">
                <asp:CalendarExtender ID="calDateRemoved" runat="server" DaysModeTitleFormat="dd/MM/yyyy"
                    TargetControlID="txtDateRemoved" PopupButtonID="imgCalDateRemoved" PopupPosition="Right"
                    TodaysDateFormat="dd/MM/yyyy" Format="dd/MM/yyyy"></asp:CalendarExtender>
                <asp:TextBox ID="txtDateRemoved" runat="server" Width="200"></asp:TextBox>&nbsp;<img
                    alt="Open Calendar" src="../../../Images/calendar.png" id="imgCalDateRemoved" />
                <asp:CompareValidator ID="compValidDateRemoved" runat="server" ControlToValidate="txtDateRemoved"
                    ErrorMessage="<br/>Enter a valid date" Operator="DataTypeCheck" Type="Date" Display="Dynamic"
                    CssClass="Required" ValidationGroup="save" />
            </td>
        </tr>
    <tr style=" height:35px; ">
            <td align="left" valign="top" width="120px" style="padding-left: 10px;">
                Notes:
            </td>
            <td align="left" valign="top">
                <asp:TextBox runat="server" ID="txtNotes" TextMode="MultiLine" Width="200" Height="100" />
                <asp:RegularExpressionValidator ID="revTexbox3" runat="server" ErrorMessage="<br/>You must enter up to a maximum of 200 characters"
                    ValidationExpression="^([\S\s]{0,200})$" ControlToValidate="txtNotes" Display="Dynamic"
                    CssClass="Required" ValidationGroup="AddNotes"></asp:RegularExpressionValidator>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                &nbsp;
            </td>
            <td align="right" valign="top" style="text-align: right;">
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" BackColor="White" />
                &nbsp;
                <asp:Button ID="btnSave" runat="server" Text="Save" BackColor="White" OnClick="btnSave_Click"
                    ValidationGroup="save" />
                &nbsp;
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:ModalPopupExtender ID="mdlPopUpAddAppliance" runat="server" DynamicServicePath=""
    Enabled="True" TargetControlID="lblDispAppliance" PopupControlID="pnlAddAppliancePopup"
    DropShadow="true" CancelControlID="btnCancel" BackgroundCssClass="modalBackground">
</asp:ModalPopupExtender>
<asp:Label ID="lblDispAppliance" runat="server"></asp:Label>