﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PropertyDataRestructure.Base;
using PDR_BusinessObject.PageSort;
using System.Data;
using PDR_Utilities.Helpers;
using PDR_Utilities.Constants;
using PDR_BusinessObject.Asbestos;
using PDR_BusinessLogic.Block;
using PropertyDataRestructure.Interface;
using PDR_DataAccess.Block;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_BusinessLogic.SchemeBlock;
using PDR_DataAccess.SchemeBlock;
using PDR_BusinessObject.SchemeBlock;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Net.Mime;

namespace PropertyDataRestructure.UserControls.Pdr.SchemeBlock
{
    public partial class Asbestos : UserControlBase, IAddEditPage, IListingPage
    {
        int id = 0;
        string requestType = string.Empty;         
        String pageName = "";
        BlockBL objBlockBL = new BlockBL(new BlockRepo());
        protected void Page_Load(object sender, EventArgs e)
        {
            Page.Form.Attributes.Add("enctype", "multipart/form-data");
            pageName = System.IO.Path.GetFileName(Request.Url.ToString());
            if (pageName.Contains(PathConstants.SchemeRecordPage))
                btnSavePropertyDocuments.Enabled = false; 
            if (!IsPostBack)
            {
                populateDropDowns();
                populateDropDown(ddlSubtype);
                loadData();
                rdBtnUrgentActionRequired.SelectedValue = "False";
            }
        }


        #region getQueryString()
        private void getQueryString()
        {
            try
            {

                if ((Request.QueryString[ApplicationConstants.Id] != null))
                {
                    id = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);

                }

                if ((Request.QueryString[ApplicationConstants.RequestType] != null))
                {

                    requestType = Request.QueryString[ApplicationConstants.RequestType];
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region "Send Email notification on Urgent Action Required"
		 protected void sendEmailNotificationOnUrgentAction()
         {
            try 
	        {
                getQueryString();
		        // property address
                // specific job role persons email address
                    var employeeEmailForAsbestosDS = new DataSet();
                    objBlockBL.GetEmployeeEmailForAsbestosEmailNotifications(id.ToString(), requestType, ref employeeEmailForAsbestosDS);
                if (employeeEmailForAsbestosDS != null)
	            {
                    foreach (DataRow row in employeeEmailForAsbestosDS.Tables[0].Rows)
	                {
                        if (row["WORKEMAIL"].ToString() != "")
                        {
                            // send email
                            var mailMessage = new MailMessage();
                            mailMessage.Subject = ApplicationConstants.UrgentAsbestosEmailSubject;

                            // load email body

                            var body = new StringBuilder();
                            StreamReader reader = new StreamReader(Server.MapPath("~/Email/AsbestosUrgentAction.htm"));
                            body.Append(reader.ReadToEnd());

                            //==========================================='
                            // Employee Name
                            body.Replace("{FULLNAME}", row["FULLNAME"].ToString());
                            // Property Address
                            body.Replace("{PROPERTYADDRESS}", row["PROPERTYADDRESS"].ToString());
                            //==========================================='
                            // There is an asbestos present at the below property. Please contact Broad land Housing for details.
                            // Attach logo images with email
                            //==========================================='

                            var logoBroadLandRepairs = new LinkedResource(Server.MapPath("~/Images/broadland.png"));
                            logoBroadLandRepairs.ContentId = "logoBroadLandRepairs_Id";

                            body = body.Replace("{Logo_Broadland-Housing-Association}", String.Format("cid:{0}", logoBroadLandRepairs.ContentId));

                            var mimeType = new ContentType("text/html");
                            var alternatevw = AlternateView.CreateAlternateViewFromString(body.ToString(), mimeType);
                            alternatevw.LinkedResources.Add(logoBroadLandRepairs);

                            //'==========================================='

                            // For a graphical view with logos, alternative view will be visible, body is attached for text view.
                            mailMessage.To.Add(new MailAddress(row["WORKEMAIL"].ToString(), row["FULLNAME"].ToString()));
                            mailMessage.Body = body.ToString();
                            mailMessage.AlternateViews.Add(alternatevw);
                            mailMessage.IsBodyHtml = true;
                            EmailHelper.sendEmail(ref mailMessage);
                        }
	                }
	            }
	        }
	        catch (Exception ex)
	        {
		        uiMessage.isError = true;
                uiMessage.messageText = ex.Message;
                if (uiMessage.isExceptionLogged == false)
	            {
		             ExceptionPolicy.HandleException(ex, "Exception Policy");
	            }
	        }
            finally
            {
                if (uiMessage.isError == true)
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
         }
#endregion

        public void saveData()
        {
            try
            {
                AsbestosBO objAsbestosBO = new AsbestosBO();
                Nullable<DateTime> dat2;
                dat2 = null;
                objAsbestosBO.AsbestosLevelId = Convert.ToInt32(ddlAsbestosLevel.SelectedValue);
                objAsbestosBO.AsbestosElementId = Convert.ToInt32(ddlAsbestosElement.SelectedValue);
                objAsbestosBO.RiskId = ddlRisk.SelectedValue;
                objAsbestosBO.PropasbLevelID = hdnHiddenId.Value;
                //Parse(txtAdded.Text);
                DateTime dat1 = DateTime.Parse(txtAdded.Text);
                objAsbestosBO.AddedDate = dat1;
                if ((txtRemoved.Text != String.Empty))
                {
                    dat2 = DateTime.Parse(txtRemoved.Text);
                }
                objAsbestosBO.Notes = txtNotes.Text;
                objAsbestosBO.Id = Convert.ToInt32(Request.QueryString["id"]);
                objAsbestosBO.RequestType = requestType;
                objAsbestosBO.UserId = objSession.EmployeeId;
                if (Convert.ToInt32(ddlRiskLevel.SelectedValue) > 0)
                {
                    objAsbestosBO.RiskLevel = Convert.ToInt32(ddlRiskLevel.SelectedValue);
                }
                objAsbestosBO.UrgentActionRequired = Convert.ToBoolean(rdBtnUrgentActionRequired.SelectedValue);

                if (objAsbestosBO.UrgentActionRequired == true)
                {
                    sendEmailNotificationOnUrgentAction();
                }

                if (((dat2 > dat1) || (dat2 == null)))
                {

                    if (dat2 != null)
                    {
                        if (dat2 < DateTime.Now.Date)
                        {
                            uiMessage.showErrorMessage(UserMessageConstants.InvalidDateGreaterThanCurrent);
                            return;
                        }
                    }



                    if ((txtRemoved.Text != String.Empty))
                    {
                        objAsbestosBO.RemovedDate = txtRemoved.Text;
                    }
                    if (string.IsNullOrEmpty(objAsbestosBO.PropasbLevelID))
                    {
                        objBlockBL.saveAsbestos(objAsbestosBO);
                    }
                    else
                    {
                        objBlockBL.amendAsbestos(objAsbestosBO);
                    }
                    uiMessage.showInformationMessage(UserMessageConstants.SuccessMessage);
                    resetControls();

                }
                else
                {
                    uiMessage.showErrorMessage(UserMessageConstants.InvalidDate);
                }

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        public bool validateData()
        {
            if (Convert.ToInt32(this.ddlType.SelectedValue) == -1)
            {
                uiMessageDoc.showErrorMessage(UserMessageConstants.SelectTypeValidationError);

                return false;
            }

            if (Convert.ToInt32(this.ddlSubtype.SelectedValue) == -1)
            {
                uiMessageDoc.showErrorMessage(UserMessageConstants.SelectTitleValidationError);
                return false;
            }
            if (string.IsNullOrEmpty(txtDocumentDate.Text))
            {
                uiMessageDoc.showErrorMessage(UserMessageConstants.SelectDocumentDateValidationError);
                return false;
            }


            if (string.IsNullOrEmpty(txtExpires.Text))
            {
                uiMessageDoc.showErrorMessage(UserMessageConstants.SelectExpiryDateValidationError);
                return false;
            }

            if (DateTime.Parse(txtDocumentDate.Text) >= DateTime.Parse(txtExpires.Text))
            {
                uiMessageDoc.showErrorMessage(UserMessageConstants.DateValidationError);
                return false;
            }



            if (!flUploadDoc.HasFile)
            {
                uiMessageDoc.showErrorMessage(UserMessageConstants.SelectAFile);
                return false;
            }

            if ((flUploadDoc.HasFile & System.IO.Path.GetExtension(flUploadDoc.FileName) != ".pdf"))
            {
                uiMessageDoc.showErrorMessage(UserMessageConstants.SelectAPDF);
                return false;
            }
            if (flUploadDoc.PostedFile.ContentLength > 20728650)
            {
                uiMessageDoc.showErrorMessage(UserMessageConstants.FileSizeError);
                return false;
            }

            return true;
        }

        public void resetControls()
        {
            txtAdded.Text = string.Empty;
            txtNotes.Text = string.Empty;
            txtRemoved.Text = string.Empty;
            ddlRisk.SelectedIndex = 0;
            ddlAsbestosElement.SelectedIndex = 0;
            ddlAsbestosLevel.SelectedIndex = 0;
            btnAddAsbestos.Enabled = true;
            btnAmend.Enabled = false;
            ddlRiskLevel.SelectedIndex = 0;
            rdBtnUrgentActionRequired.SelectedValue = "False";

        }
        public void resetDocumentControls()
        {
            ddlSubtype.SelectedIndex = 0;
            ddlType.SelectedIndex = 0;
            txtKeyword.Text = String.Empty;            
            txtExpires.Text = String.Empty;
            txtDocumentDate.Text = String.Empty;
        }
        public void loadData()
        {
            populateData();
        }

        public void populateDropDown(DropDownList ddl)
        {
            DocumentsBL objDocumentTypeBL = new DocumentsBL(new DocumentsRepo());
            DataSet resultDataSet = new DataSet();

            resultDataSet = objDocumentTypeBL.getDocumentSubtypesList(3);
            ddlSubtype.DataSource = resultDataSet.Tables[0];
            ddlSubtype.DataValueField = "DocumentSubtypeId";
            ddlSubtype.DataTextField = "Title";
            ddlSubtype.DataBind();
        }

        public void populateDropDowns()
        {
            DataSet resultDataSet = new DataSet();
            objBlockBL.getAsbestosAndRisk(ref resultDataSet);
            //Populating Asbestos Level Drop down List
            ddlAsbestosLevel.DataSource = resultDataSet.Tables["AsbestosLevel"];
            ddlAsbestosLevel.DataTextField = "ASBRISKLEVELDESCRIPTION";
            ddlAsbestosLevel.DataValueField = "ASBRISKLEVELID";
            ddlAsbestosLevel.DataBind();
            //Populating Asbestos Element Drop down List   
            ddlAsbestosElement.DataSource = resultDataSet.Tables["AsbestosElements"];
            ddlAsbestosElement.DataTextField = "RISKDESCRIPTION";
            ddlAsbestosElement.DataValueField = "ASBESTOSID";
            ddlAsbestosElement.DataBind();
            //Populating Risk Drop down List
            ddlRisk.DataSource = resultDataSet.Tables["Risk"];
            ddlRisk.DataTextField = "ASBRISKID";
            ddlRisk.DataValueField = "ASBRISKID";
            ddlRisk.DataBind();
            ddlRiskLevel.DataSource = resultDataSet.Tables["Level"];
            ddlRiskLevel.DataTextField = "Description";
            ddlRiskLevel.DataValueField = "AsbestosLevelId";
            ddlRiskLevel.DataBind();

            ddlAsbestosLevel.Items.Insert(0, new ListItem("Please Select", "-1"));
            ddlAsbestosElement.Items.Insert(0, new ListItem("Please Select", "-1"));
            ddlRisk.Items.Insert(0, new ListItem("Please Select", "-1"));
            ddlRiskLevel.Items.Insert(0, new ListItem("Please Select", "-1"));
        }


        public void searchData()
        {
            uiMessage.hideMessage();
            uiMessageDoc.hideMessage();
        }

        public void populateData()
        {
            //uiMessage.hideMessage();
            int totalCount = Convert.ToInt32(ViewState[ViewStateConstants.TotalCount]);
            getQueryString();
            DataSet resultDataSet = new DataSet();
            BlockBL objBlockBL = new BlockBL(new BlockRepo());
            PageSortBO objPageSortBo = new PageSortBO("DESC", "AsbestosId", 1, 10);
            if (pageSortViewState != null)
            {
                objPageSortBo = pageSortViewState;
            }
            totalCount = objBlockBL.getHealthAndSafetyTabInformation(ref resultDataSet, id, objPageSortBo, requestType);
            //if (totalCount >= 0)
            //{
            objPageSortBo.TotalRecords = totalCount;
            objPageSortBo.TotalPages = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(totalCount) / objPageSortBo.PageSize));
            grdDocumentInfo.DataSource = resultDataSet;
            grdDocumentInfo.DataBind();
            pageSortViewState = objPageSortBo;
            GeneralHelper.setGridViewPager(ref pnlPagination, objPageSortBo);
            divAsbestosGrid.Visible = true;
            //}
            //else
            //{
            //    //divAsbestosGrid.Visible = false;
            //    uiMessage.showErrorMessage(UserMessageConstants.NoRecordFound);
            //}
        }

        public void printData()
        {
            throw new NotImplementedException();
        }

        public void exportGridToExcel()
        {
            throw new NotImplementedException();
        }

        public void exportToPdf()
        {
            throw new NotImplementedException();
        }

        public void downloadData()
        {
            throw new NotImplementedException();
        }

        public void applyFilters()
        {
            throw new NotImplementedException();
        }
        public void saveDocumentData()
        {
            getQueryString();
            string filename = flUploadDoc.FileName;
            DocumentsBO objDocumentBO = new DocumentsBO();
            if (requestType == ApplicationConstants.Scheme)
            {
                objDocumentBO.SchemeId = id;
            }
            else if (requestType == ApplicationConstants.Block)
            {
                objDocumentBO.BlockId = id;
            }
            objDocumentBO.Category = ddlDocumentCategory.SelectedValue;
            objDocumentBO.DocumentTypeId = Convert.ToInt32(ddlType.SelectedItem.Value);
            objDocumentBO.DocumentSubTypeId = Convert.ToInt32(ddlSubtype.SelectedItem.Value);
            objDocumentBO.Keyword = txtKeyword.Text;
            objDocumentBO.DocumentFormat = System.IO.Path.GetExtension(filename).Replace(".", "");
            objDocumentBO.DocumentSize = Math.Ceiling(Convert.ToDecimal(flUploadDoc.PostedFile.ContentLength / 1024)).ToString() + "KB";
            objDocumentBO.UploadedBy = objSession.EmployeeId;
            objDocumentBO.ExpiryDate = DateTime.Parse(txtExpires.Text);
            objDocumentBO.DocumentDate = DateTime.Parse(txtDocumentDate.Text);

            filename = FileHelper.getUniqueFileName(filename);
            dynamic fileDirectoryPath = Server.MapPath(getFileDirectoryPath());

            if ((Directory.Exists(fileDirectoryPath) == false))
            {
                Directory.CreateDirectory(fileDirectoryPath);
            }


            objDocumentBO.DocumentName = filename;
            string filePath = fileDirectoryPath + filename;

            flUploadDoc.SaveAs(@filePath);
            objDocumentBO.DocumentPath = fileDirectoryPath;
            DocumentsBL objDocumentsBL = new DocumentsBL(new DocumentsRepo());
            objDocumentsBL.saveDocumentUpload(objDocumentBO);
            uiMessageDoc.showInformationMessage("Document uploaded successfully!");
        }

        protected void btnAddAsbestos_Click(object sender, EventArgs e)
        {
            try
            {
                saveData();
                populateData();

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        protected void btnAmend_Click(object sender, EventArgs e)
        {
            try
            {
                saveData();
                populateData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #region  btn save event

        protected void Save(object sender, EventArgs e)
        {
            try
            {
                if (validateData())
                {
                    saveDocumentData();
                    resetDocumentControls();
                }
            }
            catch (Exception ex)
            {
                uiMessageDoc.isError = true;
                uiMessageDoc.messageText = ex.Message;

                if ((uiMessageDoc.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessageDoc.isError == true))
                {
                    uiMessageDoc.showErrorMessage(uiMessageDoc.messageText);
                }
            }
        }
        #endregion

        protected void btnReset_Click(object sender, EventArgs e)
        {
            try
            {
                resetControls();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        protected void btnView_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet ddlDataSet = new DataSet();
                Button btnView = (Button)sender;
                int asbestosId = Convert.ToInt32(btnView.CommandArgument);
                DataSet resultDataSet = new DataSet();
                BlockBL objBlockBL = new BlockBL(new BlockRepo());
                objBlockBL.getSchemeBlockAsbestosRiskAndOtherInfo(ref resultDataSet, asbestosId);
                objBlockBL.getAsbestosAndRisk(ref ddlDataSet);


                if (!string.IsNullOrEmpty(resultDataSet.Tables[0].Rows[0]["DateRemoved"].ToString()))
                {
                    DateTime removedDate = Convert.ToDateTime(resultDataSet.Tables[0].Rows[0]["DateRemoved"].ToString());
                    if (removedDate < DateTime.Now)
                    {

                        uiMessage.showErrorMessage("Abestos cannot be viewed. It has been removed already.");
                        return;
                    }

                }


                if ((resultDataSet.Tables[0].Rows.Count > 0))
                {
                    hdnHiddenId.Value = resultDataSet.Tables[0].Rows[0]["PROPASBLEVELID"].ToString();
                    string asbriskLevelCheck;
                    int ASBRISKLEVELID = Convert.ToInt32(resultDataSet.Tables[0].Rows[0]["ASBRISKLEVELID"]);
                    asbriskLevelCheck = ("ASBRISKLEVELID =" + ASBRISKLEVELID.ToString());
                    DataRow[] dr = ddlDataSet.Tables["AsbestosLevel"].Select(asbriskLevelCheck);
                    if ((dr.Length > 0))
                    {
                        ddlAsbestosLevel.SelectedValue = resultDataSet.Tables[0].Rows[0]["ASBRISKLEVELID"].ToString();
                    }
                    else
                    {
                        uiMessage.messageText = UserMessageConstants.InvalidAsbestosLevel;
                        uiMessage.showErrorMessage(uiMessage.messageText);

                    }
                    string asbestosElementCheck;
                    int AsbestosIdCheck = Convert.ToInt32(resultDataSet.Tables[0].Rows[0]["ASBESTOSID"]);
                    asbestosElementCheck = ("ASBESTOSID =" + AsbestosIdCheck.ToString());
                    DataRow[] drc = ddlDataSet.Tables["AsbestosElements"].Select(asbestosElementCheck);
                    if ((drc.Length > 0))
                    {
                        ddlAsbestosElement.SelectedValue = resultDataSet.Tables[0].Rows[0]["ASBESTOSID"].ToString();
                    }
                    else
                    {
                        uiMessage.messageText = UserMessageConstants.InvalidAsbestosLevel;
                        uiMessage.showErrorMessage(uiMessage.messageText);
                    }
                    if (!string.IsNullOrEmpty(resultDataSet.Tables[0].Rows[0]["ASBRISKID"].ToString()))
                    {
                        ddlRisk.SelectedValue = resultDataSet.Tables[0].Rows[0]["ASBRISKID"].ToString();
                    }
                    if (!string.IsNullOrEmpty(resultDataSet.Tables[0].Rows[0]["DateAdded"].ToString()))
                    {
                        txtAdded.Text = resultDataSet.Tables[0].Rows[0]["DateAdded"].ToString();
                    }
                    else
                    {
                        txtAdded.Text = String.Empty;
                    }
                    if (!string.IsNullOrEmpty(resultDataSet.Tables[0].Rows[0]["DateRemoved"].ToString()))
                    {
                        txtRemoved.Text = resultDataSet.Tables[0].Rows[0]["DateRemoved"].ToString();
                    }
                    else
                    {
                        txtRemoved.Text = String.Empty;
                    }
                    if (!string.IsNullOrEmpty(resultDataSet.Tables[0].Rows[0]["Notes"].ToString()))
                    {
                        txtNotes.Text = resultDataSet.Tables[0].Rows[0]["Notes"].ToString();
                    }
                    else
                    {
                        txtNotes.Text = String.Empty;
                    }
                    if (resultDataSet.Tables[0].Rows[0]["RiskLevelId"] != DBNull.Value)
                    {
                        if (resultDataSet.Tables[0].Rows[0]["RiskLevelId"].ToString() != "-")
                            ddlRiskLevel.SelectedValue = resultDataSet.Tables[0].Rows[0]["RiskLevelId"].ToString();

                    }
                    if (resultDataSet.Tables[0].Rows[0]["IsUrgentActionRequired"] != DBNull.Value)
                    {
                        //rdBtnUrgentActionRequired.Items.FindByValue(resultDataSet.Tables[0].Rows[0]["IsUrgentActionRequired"].ToString()).Selected = true;

                        rdBtnUrgentActionRequired.SelectedValue = resultDataSet.Tables[0].Rows[0]["IsUrgentActionRequired"].ToString();
                    }
                    else
                    {
                        rdBtnUrgentActionRequired.SelectedValue = "False";
                    }
                    btnAddAsbestos.Enabled = false;
                    btnAmend.Enabled = true;
                    //  upPnlAsbestos.Update();
                    //  upPnlHealthAndSafetyDetail.Update();
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        protected void lnkbtnPager_Click(object sender, EventArgs e)
        {
            try
            {

                LinkButton pagebuttton = new LinkButton();
                pagebuttton = (LinkButton)sender;
                PageSortBO objPageSortBo = new PageSortBO("DESC", "AsbestosId", 1, 10);
                GridHelper.processGridPager(sender, objPageSortBo, pageSortViewState);
                loadData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #region btn cancel event
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                resetDocumentControls();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region get Image Uri
        /// <summary>
        /// get Image Uri
        /// </summary>
        /// <param name="imageName"></param>
        /// <returns></returns>
        public string getFileDirectoryPath()
        {
            getQueryString();

            var fileDirectoryPath = string.Empty;
            if (requestType == ApplicationConstants.Scheme)
            {
                fileDirectoryPath = ResolveClientUrl(ConfigHelper.GetSchemeDocUploadPath()) + id.ToString() + "/Documents/";
            }
            else if (requestType == ApplicationConstants.Block)
            {
                fileDirectoryPath = ResolveClientUrl(ConfigHelper.GetBlockDocUploadPath()) + id.ToString() + "/Documents/";
            }
            return fileDirectoryPath;
        }
        #endregion

    }
}