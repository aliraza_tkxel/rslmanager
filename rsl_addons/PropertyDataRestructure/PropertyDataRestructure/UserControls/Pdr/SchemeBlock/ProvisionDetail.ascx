﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProvisionDetail.ascx.cs" Inherits="PropertyDataRestructure.UserControls.Pdr.SchemeBlock.ProvisionDetail" %>

<%@ Register TagPrefix="attr" TagName="ItemDetail" Src="~/UserControls/Pdr/SchemeBlock/ProvisionItemDetail.ascx" %>
<%@ Register TagPrefix="attr" TagName="ProvisionNotes" Src="~/UserControls/Pdr/SchemeBlock/ProvisionNotes.ascx" %>
<%@ Register TagPrefix="attr" TagName="ProvisionPhotographs" Src="~/UserControls/Pdr/SchemeBlock/ProvisionPhotographs.ascx" %>
<%@ Register TagPrefix="attr" TagName="ProvisionDisposals" Src="~/UserControls/Pdr/SchemeBlock/ProvisionDisposals.ascx" %>
<%@ Register TagName="AssignToContractor" TagPrefix="ucAssignToContractor" Src="~/UserControls/Pdr/SchemeBlock/PurchaseOrderForm.ascx" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="500px" />
<div style="padding: 10px 10px 0px 10px;">
    <asp:LinkButton ID="lnkBtnDetailTab" OnClick="lnkBtnDetailTab_Click" CssClass="TabClicked"
        runat="server" BorderStyle="Solid" BorderColor="Black">Details: </asp:LinkButton>
    <asp:LinkButton ID="lnkBtnNotesTab" OnClick="lnkBtnNotesTab_Click" CssClass="TabInitial"
        runat="server" BorderStyle="Solid" BorderColor="Black">Notes: </asp:LinkButton>
    <asp:LinkButton ID="lnkBtnPhotographsTab" OnClick="lnkBtnPhotographsTab_Click" CssClass="TabInitial"
        runat="server" BorderStyle="Solid" BorderColor="Black">Photographs: </asp:LinkButton>
    <asp:LinkButton ID="lnkBtnDisposalsTab" OnClick="lnkBtnDisposalsTab_Click" CssClass="TabInitial"
        runat="server" BorderStyle="Solid" BorderColor="Black">Disposals: </asp:LinkButton>

    <asp:Button ID="btnRaisePO" Text="Raise a PO" BackColor="White" runat="server" OnClick="btnRaisePO_Click" Style="float: right; font-weight: 400; margin: -25px 11px 0 0px;" />
    
    <asp:Button ID="btnBookFault" Text="Book a Fault" BackColor="White" runat="server" OnClick="btnBookFault_OnClick" Style="float: right; font-weight: 400; margin: -25px 11px 0 0px;" />

    <asp:Button runat="server" ID="btnAmend" Text="Amend" Style="float: right; margin: 10px 11px 0 0px;"
        BackColor="White" Visible="false" ValidationGroup="save" OnClick="btnAmend_Click" />
    <asp:Button runat="server" ID="btnSave" Text="Save" Style="float: right; margin: 10px 11px 0 0px;"
        BackColor="White" Visible="false" ValidationGroup="save" OnClick="btnSave_Click" />

    <div style="border: 1px solid black; height: 500px; overflow: scroll; clear: both; margin-left: 2px; width: 98%; padding-left: 10px; float: left; padding-right: 10px; padding-bottom: 20px; margin-top: 7px">
        <div style="clear: both; margin-bottom: 5px;">
        </div>
        <br />
        <asp:UpdatePanel ID="pnlMainProvisions" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:MultiView ID="MainView" runat="server" ActiveViewIndex="0">
                    <asp:View ID="vwItemDetail" runat="server">
                        <attr:ItemDetail runat="server" ID="ucItemDetail"></attr:ItemDetail>
                    </asp:View>
                    <asp:View ID="vwProvisionNotes" runat="server">
                        <attr:ProvisionNotes ID="ucProvisionNotes" runat="server" />
                    </asp:View>
                    <asp:View ID="vwProvisionPhotographs" runat="server">
                        <attr:ProvisionPhotographs ID="ucProvisionPhotographs" runat="server" />
                    </asp:View>
                    <asp:View ID="vwProvisionDisposals" runat="server">
                        <attr:ProvisionDisposals ID="ucProvisionDisposals" runat="server" />
                    </asp:View>
                </asp:MultiView>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>

<asp:Label ID="lblHiddenRaisePO" runat="server" Text="" Style="display: none;" />
<cc1:ModalPopupExtender ID="mdlpopupRaisePOProvision" runat="server" PopupControlID="pnlRaisePOProvision"
    TargetControlID="lblHiddenRaisePO" Enabled="True">
</cc1:ModalPopupExtender>
<asp:Panel ID="pnlRaisePOProvision" runat="server">
    <ucAssignToContractor:AssignToContractor ID="ucAssignToContractor" runat="server" />
</asp:Panel>
