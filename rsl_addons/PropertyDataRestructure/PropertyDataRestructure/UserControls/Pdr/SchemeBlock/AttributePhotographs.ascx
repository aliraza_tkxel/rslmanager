﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AttributePhotographs.ascx.cs"
    Inherits="PropertyDataRestructure.UserControls.Pdr.SchemeBlock.AttributePhotographs" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<style type="text/css">
    .style1
    {
        width: 270px;
    }
</style>
<uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="500px" />
<asp:UpdatePanel runat="server" ID="updPnlPhotos">
    <ContentTemplate>
        <div style="float: right; width: 100%;">
            <asp:Button runat="server" ID="btnUploadPhoto" Text="Add New Photograph" Style="float: right"
                BackColor="White" UseSubmitBehavior="false" OnClick="btnUploadPhoto_Click" />
        </div>
        <asp:ListView runat="server" ID="lstViewPhotos" GroupItemCount="3" OnItemCommand="lstViewPhotos_ItemCommand">
            <LayoutTemplate>
                <table id="groupPlaceholderContainer" runat="server" border="0" cellpadding="0" cellspacing="0"
                    style="border-collapse: collapse; width: 100%;">
                    <tr id="groupPlaceholder" runat="server">
                    </tr>
                </table>
            </LayoutTemplate>
            <GroupTemplate>
                <tr id="itemPlaceholderContainer" runat="server">
                    <td id="itemPlaceholder" runat="server">
                    </td>
                </tr>
            </GroupTemplate>
            <ItemTemplate>
                <div class="itemPhoto">
                    <div style="font-size: 10px;">
                        &nbsp;Date:&nbsp;
                        <%# Eval("CreatedOn") %>
                    </div>
                    <div style="font-size: 10px;">
                        &nbsp;By:<%# Eval("CreatedBy")%></div>
                    <img src='<%# getImageUri(Eval("ImageName").ToString())%>' alt='<%# Eval("ImageName") %>'
                        width="156px" height="120px" style="margin: 5px;" />
                    <div style="float: right; font-size: 10px;">
                        <asp:ImageButton ID="imgBtnShow" runat="server" CommandName="ShowImage" CommandArgument='<%# getImageUri(Eval("ImageName").ToString())%>'
                            ImageUrl="~/Images/zoom_in.png" BorderStyle="None" Style='margin-right: -1px;
                            margin-top: -2px;' />
                    </div>
                </div>
            </ItemTemplate>
            <EmptyItemTemplate>
            </EmptyItemTemplate>
        </asp:ListView>
        <asp:Label runat="server" ID="lblDispSlideShow">   </asp:Label>
    </ContentTemplate>
</asp:UpdatePanel>
<asp:ModalPopupExtender ID="mdlPopUpAddPhoto" runat="server" DynamicServicePath=""
    Enabled="True" TargetControlID="lblDispPhotoGraph" PopupControlID="pnlAddPhotoPopup"
    DropShadow="true" CancelControlID="btnCancel" BackgroundCssClass="modalBackground">
</asp:ModalPopupExtender>
<asp:Label ID="lblDispPhotoGraph" runat="server"></asp:Label>
<asp:Label ID="lblPopUpShowImage" runat="server"></asp:Label>
<asp:Panel ID="pnlAddPhotoPopup" runat="server" BackColor="White" Width="400px">
    <asp:UpdatePanel ID="updPnlAttributes" runat="server">
        <ContentTemplate>
            <table id="pnlAddPhotoTable">
                <tr>
                    <td colspan="2" valign="top">
                    <uim:UIMessage ID="uiMessagePopUp" runat="Server" Visible="false" width="100%" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" valign="top">
                        <div style="float: left; font-weight: bold; padding-left: 10px;">
                            Add New Photograph</div>
                        <div style="clear: both">
                        </div>
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" colspan="2">
                        <div style='margin-left: 15px;'>
                            <asp:Panel ID="pnlAddPhotoMessage" runat="server" Visible="false">
                                <asp:Label ID="lblAddPhotoMessage" runat="server" Text=""></asp:Label>
                            </asp:Panel>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td align="right" valign="top" width="120px">
                        Date:<span class="Required">*</span>
                    </td>
                    <td align="left" valign="top" class="style1">
                        <asp:Image ID="imgCalendar" runat="server" Height="23px" ImageUrl="~/Images/calendar.png"
                            Style="float: right; margin-right: 65px; float: right" Width="25px" />
                        <asp:CalendarExtender ID="cntrlCalendarExtender" runat="server" TargetControlID="txtBoxDueDate"
                            Format="dd/MM/yyyy" PopupButtonID="imgCalendar" />
                        <asp:TextBox ID="txtBoxDueDate" runat="server" OnClick="this.value=''" Style="width: 168px;"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right" valign="top" width="120px">
                        Title:<span class="Required">*</span>
                    </td>
                    <td align="left" valign="top" class="style1">
                        <asp:TextBox ID="txtBoxTitle" runat="server">
                        </asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td align="right" valign="top" width="120px">
                        Upload:<span class="Required">*</span>
                    </td>
                    <td align="left" valign="top" class="style1">
                        <asp:FileUpload ID="flUploadDoc" runat="server" class="upload" />
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        &nbsp;
                    </td>
                    <td align="right" valign="top" style="text-align: right;" class="style1">
                        <input id="btnCancel" type="button" value="Cancel" style="background-color: White;" />
                        &nbsp;
                        <asp:Button ID="btnSave" runat="server" Text="Save" BackColor="White" ValidationGroup="check"
                            OnClick="btnSave_Click" />
                        &nbsp;
                    </td>
                </tr>
            </table>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnSave" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Panel>
<asp:ModalPopupExtender ID="mdlPopUpViewImage" runat="server" DynamicServicePath=""
    Enabled="True" TargetControlID="lblPopUpShowImage" PopupControlID="pnlLargeImage"
    CancelControlID="btnClose" BackgroundCssClass="modalBackground">
</asp:ModalPopupExtender>
<asp:Panel ID="pnlLargeImage" runat="server" CssClass="modalPopupImage">
    <div style='width: 100%;'>
        <asp:ImageButton ID="btnClose" runat="server" Style="position: absolute; top: -17px;
            right: -16px; width: 22px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
        <asp:Image ID="imgDynamicImage" runat="server" CssClass="imgDynamicImage" />
    </div>
</asp:Panel>
