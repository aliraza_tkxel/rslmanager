﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Provisions.ascx.cs" Inherits="PropertyDataRestructure.UserControls.Pdr.SchemeBlock.Provisions" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<%@ Register TagPrefix="provision" TagName="ProvisionDetail" Src="~/UserControls/Pdr/SchemeBlock/ProvisionDetail.ascx"  %>
<script type="text/javascript">

   function openModal() {
       $('#poModalRaiseaPO').modal('show');
       console.log("Opening the modal")
   }
    
</script>
<uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="500px" />

<asp:UpdatePanel ID="updPnlProvisions" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div style="float: left; width: 25%; border: 1px solid; height: 603px; overflow: scroll;">
            <asp:Panel ID="pnlTreeView" runat="server" >
                <asp:TreeView runat="server" ID="trVwProvisions" ShowLines="true" ExpandDepth="1"
                    ForeColor="Black" SelectedNodeStyle-BackColor="#6dcff7" 
                    PathSeparator="&gt;" onselectednodechanged="trVwProvisions_SelectedNodeChanged" >
                    
                    <Nodes>
                        <asp:TreeNode Text="Menu"></asp:TreeNode>
                    </Nodes>
                </asp:TreeView>
            </asp:Panel>
        </div>

        <div style="float: left; width: 74%; border: 1px solid; min-height: 603px;">
            <div style="margin-top: 10px;">
                <asp:Label runat="server" Text="" ID="lblBreadCrump" Style="margin-left: 15px;"></asp:Label></div>
            
                <%--ProvisionDetail user control here --%>
               <provision:ProvisionDetail runat="server" ID="ucProvisionDetail" Visible="false"    />
            <asp:Button ID="btnHidden" runat="server" Text="" Style="display: none;" />
        </div>
    </ContentTemplate>
</asp:UpdatePanel>