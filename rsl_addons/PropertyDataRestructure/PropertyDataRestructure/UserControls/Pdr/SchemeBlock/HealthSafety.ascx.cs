﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PropertyDataRestructure.Base;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_Utilities.Constants;

namespace PropertyDataRestructure.UserControls.Pdr.SchemeBlock
{
    public partial class HealthSafety : UserControlBase
    {
        #region Page Load event
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                MainSubView.ActiveViewIndex = 0;
                Asbestos.loadData();
                enableDisableTabs(true, 0, lnkBtnAsbestosTab);
            }

            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        #endregion
        #region lnkBtnAsbestosTab Click
        protected void lnkBtnAsbestosTab_Click(object sender, EventArgs e)
        {
            try
            {
                MainSubView.ActiveViewIndex = 0;
                Asbestos.loadData();
                enableDisableTabs(true, 0, lnkBtnAsbestosTab);
                Asbestos.searchData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion
        #region lnkRefurbishmentTab Click
        protected void lnkRefurbishmentTab_Click(object sender, EventArgs e)
        {
            try
            {
                MainSubView.ActiveViewIndex = 1;
                Refurbishment.loadData();
                enableDisableTabs(true, 1, lnkRefurbishmentTab);
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        public void enableDisableTabs(bool enable, int viewIndex, LinkButton lnkBtnSelected)
        {

            lnkBtnAsbestosTab.Enabled = enable;
            lnkRefurbishmentTab.Enabled = enable;


            lnkBtnAsbestosTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkRefurbishmentTab.CssClass = ApplicationConstants.TabInitialCssClass;
           
            lnkBtnSelected.CssClass = ApplicationConstants.TabClickedCssClass;
            MainSubView.ActiveViewIndex = viewIndex;
        }
    }
}