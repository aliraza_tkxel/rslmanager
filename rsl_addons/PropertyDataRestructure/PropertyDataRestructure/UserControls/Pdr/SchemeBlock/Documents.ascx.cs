﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PropertyDataRestructure.Base;
using PropertyDataRestructure.Interface;
using PDR_BusinessLogic.SchemeBlock;
using System.Data;
using AjaxControlToolkit;
using PDR_Utilities.Constants;
using PDR_BusinessObject.SchemeBlock;
using PDR_Utilities.Helpers;
using System.IO;
using PDR_DataAccess.SchemeBlock;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_BusinessObject.PageSort;
using PDR_BusinessLogic.Reports;
using PDR_DataAccess.Reports;

namespace PropertyDataRestructure.UserControls.Pdr.SchemeBlock
{
    public partial class Documents : UserControlBase, IListingPage, IAddEditPage
    {

        int id = 0;
        string requestType = string.Empty;
        Boolean isReadOnly = false;
        String pageName = "";
        #region Events
        #region Pageload event
        protected void Page_Load(object sender, EventArgs e)
        {
            pageName = System.IO.Path.GetFileName(Request.Url.ToString());
            if (pageName.Contains(PathConstants.SchemeRecordPage))
            {
                isReadOnly = true;
            }
            else
            {
                isReadOnly = false;
            }
            if (isReadOnly == true)
            {
                for (int i = 0; i < grdDocumentInfo.Rows.Count; i++)
                {
                   grdDocumentInfo.Rows[i].Cells[7].Visible = false;
                }
                div_AddDoc.Visible = false;
                div_grdDocumentInfo.Attributes.Add("class", "div_grdDocumentInfoRO");
            }
            uiMessage.hideMessage();

            Page.Form.Attributes.Add("enctype", "multipart/form-data");
            if (!IsPostBack)
            {
                //PopulateEpcCategoryTypesDropDown();
            }

        }
        #endregion
        #region ddlType Selected Index Changed event
        protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                populateDropDown(ddlType);
                ddlSubtype.Enabled = true;
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region  btn save event

        protected void Save(object sender, EventArgs e)
        {
            try
            {
                if (validateData())
                {
                    saveData();
                    populateData();
                    resetControls();
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion
        #region btn cancel event
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                resetControls();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion
        #region lnkbtn Pager click event
        protected void lnkbtnPager_Click(object sender, EventArgs e)
        {
            try
            {

                LinkButton pagebuttton = new LinkButton();
                pagebuttton = (LinkButton)sender;
                PageSortBO objPageSortBo = new PageSortBO("DESC", "DocumentId", 1, 10);
                GridHelper.processGridPager(sender, objPageSortBo, pageSortViewState);
                populateData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion
        #region "Button Delete Document Click"

        protected void btnDeleteDocument_Click(object sender, System.EventArgs e)
        {
            Button btnDeleteDocument = (Button)sender;
            DocumentsBO objPropertyDocumentBO = new DocumentsBO();
            getQueryString();
            int DocumentId = Convert.ToInt32(btnDeleteDocument.CommandArgument);
            if (DocumentId > 0)
            {
                DocumentsBL objDocumentsBL = new DocumentsBL(new DocumentsRepo());
                string documentName = objDocumentsBL.deleteDocumentByIDandGetPath(DocumentId);
                dynamic fileDirectoryPath = Server.MapPath(ResolveClientUrl(ConfigHelper.GetDocUploadPath()) + id.ToString() + "/Documents/");

                if ((Directory.Exists(fileDirectoryPath) == true))
                {
                    if ((File.Exists(fileDirectoryPath + documentName)))
                    {
                        File.Delete(fileDirectoryPath + documentName);
                    }
                }
            }

            populateData();

        }

        #endregion

        #region "Button View Document Click"

        protected void btnViewDocument_Click(object sender, System.EventArgs e)
        {
            try
            {
                Button btnViewDocument = (Button)sender;
                DocumentsBO objPropertyDocumentBO = new DocumentsBO();
                getQueryString();

                int DocumentId = Convert.ToInt32(btnViewDocument.CommandArgument);
                //int.TryParse(btnDeleteDocument.CommandArgument, objPropertyDocumentBO.DocumentId);

                if (DocumentId > 0)
                {
                    DocumentsBL objDocumentsBL = new DocumentsBL(new DocumentsRepo());
                    DataSet resultDs = new DataSet();
                    resultDs = objDocumentsBL.getDocumentInformationById(DocumentId);
                    if (resultDs.Tables[0].Rows.Count > 0)
                    {
                        dynamic fileDirectoryPath = getFileDirectoryPath();


                        string filePath = fileDirectoryPath + resultDs.Tables[0].Rows[0]["DocumentName"];

                        ExportDownloadHelper.exportFileToBrowser("application/pdf", filePath);

                    }
                }

                populateData();
            }
            catch (FileNotFoundException ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = UserMessageConstants.FileNotExist;
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #endregion
        #region IAddEditPage Implementation
        public void saveData()
        {
            getQueryString();
            string filename = flUploadDoc.FileName;
            DocumentsBO objDocumentBO = new DocumentsBO();
            if (requestType == ApplicationConstants.Scheme)
            {
                objDocumentBO.SchemeId = id;
            }
            else if (requestType == ApplicationConstants.Block)
            {
                objDocumentBO.BlockId = id;
            }
            objDocumentBO.Category = ddlDocumentCategory.SelectedItem.Text;
            objDocumentBO.DocumentTypeId = Convert.ToInt32(ddlType.SelectedItem.Value);
            objDocumentBO.DocumentSubTypeId = Convert.ToInt32(ddlSubtype.SelectedItem.Value);
            objDocumentBO.Keyword = txtKeyword.Text;
            objDocumentBO.DocumentFormat = System.IO.Path.GetExtension(filename).Replace(".", "");
            objDocumentBO.DocumentSize = Math.Ceiling(Convert.ToDecimal(flUploadDoc.PostedFile.ContentLength / 1024)).ToString() + "KB";
            objDocumentBO.UploadedBy = objSession.EmployeeId;
            objDocumentBO.ExpiryDate = DateTime.Parse(txtExpires.Text);
            objDocumentBO.DocumentDate = DateTime.Parse(txtDocumentDate.Text);

            filename = FileHelper.getUniqueFileName(filename);
            dynamic fileDirectoryPath = Server.MapPath(getFileDirectoryPath());

            if ((Directory.Exists(fileDirectoryPath) == false))
            {
                Directory.CreateDirectory(fileDirectoryPath);
            }


            objDocumentBO.DocumentName = filename;
            string filePath = fileDirectoryPath + filename;

            flUploadDoc.SaveAs(@filePath);
            objDocumentBO.DocumentPath = fileDirectoryPath;
            DocumentsBL objDocumentsBL = new DocumentsBL(new DocumentsRepo());
            objDocumentsBL.saveDocumentUpload(objDocumentBO);
        }

        public bool validateData()
        {
            if (Convert.ToInt32(this.ddlType.SelectedValue) == -1)
            {
                uiMessage.showErrorMessage(UserMessageConstants.SelectTypeValidationError);

                return false;
            }

            if (Convert.ToInt32(this.ddlSubtype.SelectedValue) == -1)
            {
                uiMessage.showErrorMessage(UserMessageConstants.SelectTitleValidationError);
                return false;
            }
            if (string.IsNullOrEmpty(txtDocumentDate.Text))
            {
                uiMessage.showErrorMessage(UserMessageConstants.SelectDocumentDateValidationError);
                return false;
            }


            if (string.IsNullOrEmpty(txtExpires.Text))
            {
                uiMessage.showErrorMessage(UserMessageConstants.SelectExpiryDateValidationError);
                return false;
            }

            if (DateTime.Parse(txtDocumentDate.Text) >= DateTime.Parse(txtExpires.Text))
            {
                uiMessage.showErrorMessage(UserMessageConstants.DateValidationError);
                return false;
            }



            if (!flUploadDoc.HasFile)
            {
                uiMessage.showErrorMessage(UserMessageConstants.SelectAFile);
                return false;
            }

            if ((flUploadDoc.HasFile & System.IO.Path.GetExtension(flUploadDoc.FileName) != ".pdf"))
            {
                uiMessage.showErrorMessage(UserMessageConstants.SelectAPDF);
                return false;
            }
            if (flUploadDoc.PostedFile.ContentLength > 20728650)
            {
                uiMessage.showErrorMessage(UserMessageConstants.FileSizeError);
                return false;
            }

            return true;
        }

        public void resetControls()
        {
            ddlSubtype.SelectedIndex = 0;
            ddlType.SelectedIndex = 0;
            txtKeyword.Text = String.Empty;
            ddlSubtype.Enabled = false;
            txtExpires.Text = String.Empty;
            txtDocumentDate.Text = String.Empty;
        }

        public void loadData()
        {
            throw new NotImplementedException();
        }

        public void populateDropDown(DropDownList ddl)
        {
            DocumentsBL objDocumentTypeBL = new DocumentsBL(new DocumentsRepo());
            DataSet resultDataSet = new DataSet();

            resultDataSet = objDocumentTypeBL.getDocumentSubtypesList(Convert.ToInt32(ddl.SelectedValue),"Scheme");
            ddlSubtype.DataSource = resultDataSet.Tables[0];
            ddlSubtype.DataValueField = "DocumentSubtypeId";
            ddlSubtype.DataTextField = "Title";
            ddlSubtype.DataBind();
        }
        public void populateTypes()
        {
            DocumentsBL objDocumentTypeBL = new DocumentsBL(new DocumentsRepo());
            DataSet resultDataSet = new DataSet();

            resultDataSet = objDocumentTypeBL.getDocumentTypesList("Scheme", Convert.ToInt32(ddlDocumentCategory.SelectedItem.Value));
            ddlType.DataSource = resultDataSet.Tables[0];
            ddlType.DataValueField = "DocumentTypeId";
            ddlType.DataTextField = "Title";
            ddlType.DataBind();
        }
        public void populateDropDowns()
        {
            DataSet resultset = new DataSet();
            ReportsBL objReportsBl = new ReportsBL(new ReportsRepo());
            objReportsBl.getCategories(ref resultset, "Scheme");
            ddlDocumentCategory.DataSource = resultset;
            ddlDocumentCategory.DataValueField = "categorytypeid";
            ddlDocumentCategory.DataTextField = "title";
            ddlDocumentCategory.DataBind();

            populateTypes();
            
        }
        #endregion

        #region IListingPage Implementation
        public void searchData()
        {
            throw new NotImplementedException();
        }

        public void populateData()
        {
            getQueryString();
            int? scheme = null;
            int? block = null;

            if (requestType == ApplicationConstants.Scheme)
            {
                scheme = id;
            }
            else if (requestType == ApplicationConstants.Block)
            {
                block = id;
            }
            int totalCount = 0;
            PageSortBO objPageSortBo = new PageSortBO("DESC", "DocumentId", 1, 10);
            if (pageSortViewState != null)
            {
                objPageSortBo = pageSortViewState;
            }
            pnlPagination.Visible = false;
            DataSet resultDataSet = new DataSet();
            DocumentsBL objDocumentsBL = new DocumentsBL(new DocumentsRepo());
            totalCount = objDocumentsBL.getPropertyDocuments(ref resultDataSet, scheme, block, ref objPageSortBo);

            grdDocumentInfo.DataSource = resultDataSet;
            grdDocumentInfo.DataBind();
            objPageSortBo.TotalRecords = totalCount;
            objPageSortBo.TotalPages = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(totalCount) / objPageSortBo.PageSize));
            pnlPagination.Visible = false;
            if (objPageSortBo.TotalPages > 0)
            {
                pnlPagination.Visible = true;
                pageSortViewState = objPageSortBo;
                GridHelper.setGridViewPager(ref pnlPagination, pageSortViewState);
            }
            if (isReadOnly == true)
            {
                for (int i = 0; i < grdDocumentInfo.Rows.Count; i++)
                {
                    grdDocumentInfo.Rows[i].Cells[7].Visible = false;
                }
            }

        }

        public void printData()
        {
            throw new NotImplementedException();
        }

        public void exportGridToExcel()
        {
            throw new NotImplementedException();
        }

        public void exportToPdf()
        {
            throw new NotImplementedException();
        }

        public void downloadData()
        {
            throw new NotImplementedException();
        }

        public void applyFilters()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region getQueryString()
        private void getQueryString()
        {

            if ((Request.QueryString[ApplicationConstants.Id] != null))
            {
                id = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);

            }

            if ((Request.QueryString[ApplicationConstants.RequestType] != null))
            {

                requestType = Request.QueryString[ApplicationConstants.RequestType];
            }
        }
        #endregion

        #region get Image Uri
        /// <summary>
        /// get Image Uri
        /// </summary>
        /// <param name="imageName"></param>
        /// <returns></returns>
        public string getFileDirectoryPath()
        {
            getQueryString();

            var fileDirectoryPath = string.Empty;
            if (requestType == ApplicationConstants.Scheme)
            {
                fileDirectoryPath = ResolveClientUrl(ConfigHelper.GetSchemeDocUploadPath()) + id.ToString() + "/Documents/";
            }
            else if (requestType == ApplicationConstants.Block)
            {
                fileDirectoryPath = ResolveClientUrl(ConfigHelper.GetBlockDocUploadPath()) + id.ToString() + "/Documents/";
            }
            return fileDirectoryPath;
        }
        #endregion

        protected void ddlDocumentCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            populateTypes();
        }

    }
}