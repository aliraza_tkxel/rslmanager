﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Properties.ascx.cs"
    Inherits="PropertyDataRestructure.UserControls.Pdr.SchemeBlock.Properties" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<script type="text/javascript">

    
    function ShowPropertyDetail(pid) {

        javascript: window.location.assign('../../../../../RSLApplianceServicing/Bridge.aspx?pg=properties&id=' + pid);
        return false;
    }
    function AddNewProperty() {
        javascript: window.location.assign('../../../../../RSLApplianceServicing/Bridge.aspx?pg=properties&src=add');
        return false;
    }
    </script>
<asp:UpdatePanel runat="server" ID="updpnlProperties" UpdateMode="Conditional" >
    <ContentTemplate>
        <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="500px" />
        <div class="pdr-wrap-PropertiesGrid" >
        <cc1:PagingGridView runat="server" ID="grdProperties" AutoGenerateColumns="False"
            BackColor="White" BorderColor="#CCCCCC" BorderStyle="Solid" BorderWidth="1px"
            CellPadding="4" ForeColor="Black" GridLines="None" Width="100%" AllowSorting="True"
            AllowPaging="false" PageSize="30" OrderBy="" 
            onrowdatabound="grdProperties_RowDataBound" OnSorting="grdProperties_Sorting" ShowHeaderWhenEmpty="true" EmptyDataText="No Record Found" >
                  <Columns>
                <asp:TemplateField HeaderText="Ref:" SortExpression="Ref" ItemStyle-Width="70px">
                    <ItemTemplate>
                        <asp:CheckBox ID="chkProperty" runat="server" onclick="" 
                            ></asp:CheckBox>
                        <asp:HiddenField runat="server" ID="hdnPropertyId" Value='<%# Eval("Ref") %>' />
                        <asp:Label ID="lblRef" runat="server" Text='<%# Bind("Ref") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle Width="20%"></ItemStyle>
                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Address:" SortExpression="Address" ItemStyle-Width="70px">
                    <ItemTemplate>
                        <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle Width="30%"></ItemStyle>
                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="PostCode:" SortExpression="PostCode">
                    <ItemTemplate>
                        <asp:Label ID="lblPostCode" runat="server" Text='<%# Bind("PostCode") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle Width="10%" />
                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Status:" SortExpression="Status">
                    <ItemTemplate>
                        <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle Width="10%" />
                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Type:" SortExpression="Type">
                    <ItemTemplate>
                        <asp:Label ID="lblType" runat="server" Text='<%# Bind("Type") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle Width="10%" />
                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Asbestos:" SortExpression="Asbestos">
                    <ItemTemplate>
                        <asp:Label ID="lblAsbestos" runat="server" Text='<%# Bind("Asbestos") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle Width="20%" />
                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                </asp:TemplateField>
                 <asp:TemplateField ShowHeader="False">
                <ItemTemplate>
                    <asp:Button ID="btnView" runat="server"  Text="View"
                        BorderStyle="Solid" BorderWidth="0px"    ></asp:Button>
                </ItemTemplate>
            </asp:TemplateField>
            </Columns>
             <AlternatingRowStyle BackColor="#E8E9EA" Wrap="True" />
                  <PagerSettings Mode="NumericFirstLast" />
        </cc1:PagingGridView>
        <asp:Panel runat="server" ID="pnlPagination" Visible="true" Style="border-top: 1px solid  #000;
                vertical-align: middle; padding: 10px 0">
                <table style="width: 57%; margin: 0 auto">
                    <tbody>
                        <tr>
                            <td>
                                <asp:LinkButton ID="lnkbtnPagerFirst" Text="" runat="server" CommandName="Page" CommandArgument="First"
                                    OnClick="lnkbtnPager_Click" Style="text-decoration: none;">&lt;&lt;First</asp:LinkButton>
                                &nbsp;
                            </td>
                            <td>
                                <asp:LinkButton ID="lnkbtnPagerPrev" Text="" runat="server" CommandName="Page" CommandArgument="Prev"
                                    OnClick="lnkbtnPager_Click" Style="text-decoration: none;">&lt;Prev</asp:LinkButton>
                                &nbsp;
                            </td>
                            <td>
                                Page:&nbsp;
                                <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="Required" />
                                &nbsp;of&nbsp;
                                <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />. Records:
                                <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                                &nbsp;to&nbsp;
                                <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                                &nbsp;of&nbsp;
                                <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                            </td>
                            <td>
                                <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page"
                                    CommandArgument="Next" OnClick="lnkbtnPager_Click" Style="text-decoration: none;" />
                                &nbsp;
                            </td>
                            <td>
                                <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page"
                                    CommandArgument="Last" OnClick="lnkbtnPager_Click" Style="text-decoration: none;" />
                                &nbsp;
                            </td>
                          
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            </div>
    </ContentTemplate>
</asp:UpdatePanel>
<div style="float:right;">
<asp:Button runat="server" ID="btnRemoveProperties" 
        Text ="Remove Selected Properties" onclick="btnRemoveProperties_Click"  />
<asp:Button runat="server" ID="btnAddProperty" Text ="Add" 
   OnClientClick="return AddNewProperty();"       />
</div>