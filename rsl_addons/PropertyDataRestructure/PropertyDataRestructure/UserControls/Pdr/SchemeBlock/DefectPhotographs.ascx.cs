﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PropertyDataRestructure.Base;
using PropertyDataRestructure.Interface;
using PDR_Utilities.Constants;
using PDR_Utilities.Managers;
using PDR_BusinessLogic.SchemeBlock;
using System.Data;
using PDR_DataAccess.SchemeBlock;
using PDR_BusinessObject.PageSort;
using PDR_Utilities.Helpers;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_BusinessObject.SchemeBlock;
using System.Globalization;
using System.Drawing;

namespace PropertyDataRestructure.UserControls.Pdr.SchemeBlock
{
    public partial class DefectPhotographs : UserControlBase
    {
        
        AttributesBL objAttributeBL = new AttributesBL(new AttributesRepo());
        public string uploadedThumbUri;
        //Public uploadedThumbUri As String = "../../Photographs/Images/"

        #region Events

        #region Page Load
        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void Page_Load(object sender, System.EventArgs e)
        {
            DataSet photosDataSet = new DataSet();
            //string Id = System.Convert.ToString(Request.QueryString[ApplicationConstants.Id]);
            string Id = objSession.HeatingMappingId.ToString();
            string requestType = System.Convert.ToString(Request.QueryString[ApplicationConstants.RequestType]);
            objAttributeBL.getSchemeBlockDefectImages(Id, requestType, ref photosDataSet);
            lstViewPhotos.DataSource = photosDataSet;
            lstViewPhotos.DataBind();
            if (requestType == ApplicationConstants.Scheme)
            {
                //uploadedThumbUri = Server.MapPath(ResolveClientUrl(ConfigHelper.GetSchemeDocUploadPath()) + Id.ToString() + "/Images/");
            }
            else if (requestType == ApplicationConstants.Block)
            {
                //uploadedThumbUri = Server.MapPath(ResolveClientUrl(ConfigHelper.GetBlockDocUploadPath()) + Id.ToString() + "/Images/");
            }
        }
        #endregion

        //#Region "Btn Upload Photo Click"
        //    ''' <summary>
        //    ''' Btn Upload Photo Click
        //    ''' </summary>
        //    ''' <param name="sender"></param>
        //    ''' <param name="e"></param>
        //    ''' <remarks></remarks>
        //    Protected Sub btnUploadPhoto_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnUploadPhoto.Click
        //        Try
        //            txtBoxDueDate.Text = String.Empty
        //            txtBoxTitle.Text = String.Empty
        //            lblFileName.Text = String.Empty
        //            uiMessageHelper.resetMessage(lblAddPhotoMessage, pnlAddPhotoMessage)
        //            mdlPopUpAddPhoto.Show()
        //        Catch ex As Exception
        //            uiMessageHelper.IsError = True
        //            uiMessageHelper.Message = ex.Message
        //            If uiMessageHelper.IsExceptionLogged = False Then
        //                ExceptionPolicy.HandleException(ex, "Exception Policy")
        //            End If
        //        Finally
        //            If uiMessageHelper.IsError = True Then
        //                uiMessageHelper.setMessage(lblAddPhotoMessage, pnlAddPhotoMessage, uiMessageHelper.Message, True)
        //            End If
        //        End Try
        //    End Sub
        //#End Region

        #region Btn Save Click event
        /// <summary>
        /// Btn Save Click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            PhotographBO objPhotographBo = new PhotographBO();
            DataSet resultDataSet = new DataSet();
            string Id = System.Convert.ToString(Request.QueryString[ApplicationConstants.Id]);
            string request = System.Convert.ToString(Request.QueryString[ApplicationConstants.RequestType]);
            int itemId = System.Convert.ToInt32(objSession.TreeItemId); //   Session.Item(SessionConstants.treeItemId)
            try
            {
                if (lblFileName.Text != string.Empty && txtBoxTitle.Text != string.Empty && txtBoxDueDate.Text != string.Empty)
                {
                    DateTime dateTime = default(DateTime);
                    bool valid = DateTime.TryParseExact(System.Convert.ToString(txtBoxDueDate.Text), "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None, out dateTime);
                    if (valid)
                    {
                        objPhotographBo.ImageName = lblFileName.Text; //Session.Item(SessionConstants.DocumentName)
                        objPhotographBo.FilePath = Server.MapPath(GeneralHelper.getImageUploadPath());
                        objPhotographBo.UploadDate = System.Convert.ToDateTime(txtBoxDueDate.Text);
                        objPhotographBo.Title = txtBoxTitle.Text;
                        objPhotographBo.Id = Id;
                        objPhotographBo.RequestType = request;
                        objPhotographBo.ItemId = itemId;
                        objPhotographBo.CreatedBy = objSession.EmployeeId;
                        objPhotographBo.IsDefaultImage = false;
                        objAttributeBL.savePhotograph(objPhotographBo);
                        objAttributeBL.getSchemeBlockImages(ref resultDataSet, Id, request, itemId);
                        lstViewPhotos.DataSource = resultDataSet;
                        lstViewPhotos.DataBind();
                    }
                    else
                    {
                        uiMessageHelper.IsError = true;
                        uiMessageHelper.Message = UserMessageConstants.notValidDateFormat;
                        mdlPopUpAddPhoto.Show();
                    }
                }
                else
                {
                    uiMessageHelper.IsError = true;
                    uiMessageHelper.Message = UserMessageConstants.FillMandatory;
                    mdlPopUpAddPhoto.Show();
                }

                //updPnlPhotos.Update()
            }
            catch (Exception ex)
            {
                uiMessageHelper.IsError = true;
                uiMessageHelper.Message = ex.Message;
                if (uiMessageHelper.IsExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessageHelper.IsError == true)
                {
                    uiMessageHelper.setMessage(ref lblAddPhotoMessage, ref pnlAddPhotoMessage, uiMessageHelper.Message, true);
                }
            }
        }
        #endregion

        #region Generate Thumbnail
        /// <summary>
        /// Generate Thumbnail
        /// </summary>
        /// <param name="strFileName"></param>
        /// <remarks></remarks>
        protected void generateThumbnail(string strFileName)
        {
            //Create a new Bitmap Image loading from location of origional file
            string FilePath = string.Empty;
            FilePath = System.Convert.ToString(GeneralHelper.getImageUploadPath());
            Bitmap bm = (Bitmap)System.Drawing.Image.FromFile(FilePath + strFileName);
            //Declare Thumbnails Height and Width
            int newWidth = 100;
            int newHeight = 100;
            //Create the new image as a blank bitmap
            Bitmap resized = new Bitmap(newWidth, newHeight);
            //Create a new graphics object with the contents of the origional image
            Graphics g = Graphics.FromImage(resized);
            //Resize graphics object to fit onto the resized image
            g.DrawImage(bm, new Rectangle(0, 0, resized.Width, resized.Height), 0, 0, bm.Width, bm.Height, GraphicsUnit.Pixel);
            //Get rid of the evidence
            g.Dispose();
            var requestType = Request.QueryString[ApplicationConstants.RequestType];
            var id = Request.QueryString[ApplicationConstants.Id];
            if (requestType == ApplicationConstants.Scheme)
            {
                FilePath = Server.MapPath(ResolveClientUrl(ConfigHelper.GetSchemeDocUploadPath()) + id.ToString() + "/Images/");
            }
            else if (requestType == ApplicationConstants.Block)
            {
                FilePath = Server.MapPath(ResolveClientUrl(ConfigHelper.GetBlockDocUploadPath()) + id.ToString() + "/Images/");
            }
            //Create new path and filename for the resized image
            string newStrFileName = FilePath + strFileName;
            //Save the new image to the same folder as the origional
            resized.Save(newStrFileName);

            resized.Dispose();
            bm.Dispose();
        }
        #endregion

        #region ckBoxPhotoUpload Checked Changed
        /// <summary>
        /// ckBoxPhotoUpload Checked Changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void ckBoxPhotoUpload_CheckedChanged(object sender, EventArgs e)
        {
            this.ckBoxPhotoUpload.Checked = false;
            try
            {
                if (ReferenceEquals(objSession.PhotoUploadName, null))
                {
                    uiMessageHelper.setMessage(ref lblAddPhotoMessage, ref pnlAddPhotoMessage, UserMessageConstants.ErrorDocumentUpload, true);
                }
                else
                {
                    lblFileName.Text = objSession.PhotoUploadName;
                }

            }
            catch (Exception ex)
            {
                uiMessageHelper.IsError = true;
                uiMessageHelper.Message = ex.Message;

                if (uiMessageHelper.IsExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }

            }
            finally
            {
                if (uiMessageHelper.IsError == true)
                {
                    uiMessageHelper.setMessage(ref lblAddPhotoMessage, ref pnlAddPhotoMessage, uiMessageHelper.Message, true);
                }
                mdlPopUpAddPhoto.Show();
                objSession.PhotoUploadName = null;
            }
        }
        #endregion

        #region btn Upload Photos Click
        /// <summary>
        /// btn Upload Photos Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void btnUploadPhotos_Click(object sender, EventArgs e)
        {
            try
            {
                mdlPopUpAddPhoto.Show();
            }
            catch (Exception ex)
            {
                uiMessageHelper.IsError = true;
                uiMessageHelper.Message = ex.Message;
                if (uiMessageHelper.IsExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessageHelper.IsError == true)
                {
                    uiMessageHelper.setMessage(ref lblAddPhotoMessage, ref pnlAddPhotoMessage, uiMessageHelper.Message, true);
                }
            }
        }
        #endregion

        #region lstView Photos Item Command
        /// <summary>
        /// lstView Photos Item Command
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void lstViewPhotos_ItemCommand(object sender, System.Web.UI.WebControls.ListViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "ShowImage")
                {
                    string fileName = System.Convert.ToString(e.CommandArgument.ToString());
                    var requestType = Request.QueryString[ApplicationConstants.RequestType];
                    var id = Request.QueryString[ApplicationConstants.Id];
                    if (requestType == ApplicationConstants.Scheme)
                    {
                        imgDynamicImage.ImageUrl = Server.MapPath(ResolveClientUrl(ConfigHelper.GetSchemeDocUploadPath()) + id.ToString() + "/Images/");
                    }
                    else if (requestType == ApplicationConstants.Block)
                    {
                        imgDynamicImage.ImageUrl = Server.MapPath(ResolveClientUrl(ConfigHelper.GetBlockDocUploadPath()) + id.ToString() + "/Images/");
                    }
                    //imgDynamicImage.ImageUrl = GeneralHelper.getImageUploadPath() + SessionManager.getPropertyId().ToString() + "/Images/" + fileName;
                    //mdlPopUpViewImage.X = 100
                    //mdlPopUpViewImage.Y = 20
                    mdlPopUpViewImage.Show();
                }
            }
            catch (Exception ex)
            {
                uiMessageHelper.IsError = true;
                uiMessageHelper.Message = ex.Message;
                if (uiMessageHelper.IsExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessageHelper.IsError == true)
                {
                    uiMessageHelper.setMessage(ref lblAddPhotoMessage, ref pnlAddPhotoMessage, uiMessageHelper.Message, true);
                }
            }
        }
        #endregion

        #region btnRefresh click event
        /// <summary>
        /// btnRefresh click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet resultdataSet = new DataSet();
                string Id = System.Convert.ToString(Request.QueryString[ApplicationConstants.Id]);
                string requestType = System.Convert.ToString(Request.QueryString[ApplicationConstants.RequestType]);
                objAttributeBL.getSchemeBlockDefectImages(Id, requestType, ref resultdataSet);
                lstViewPhotos.DataSource = resultdataSet;
                lstViewPhotos.DataBind();
            }
            catch (Exception ex)
            {
                uiMessageHelper.IsError = true;
                uiMessageHelper.Message = ex.Message;
                if (uiMessageHelper.IsExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessageHelper.IsError == true)
                {
                    uiMessageHelper.setMessage(ref lblMessage, ref pnlMessage, uiMessageHelper.Message, true);
                }
            }
        }
        #endregion
        #endregion
    }
}