﻿using System;
using System.Web.UI.WebControls;
using PDR_Utilities.Constants;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_BusinessObject.SchemeBlock;
using PropertyDataRestructure.Interface;
using PropertyDataRestructure.Base;
using PDR_Utilities.Managers;
using PDR_BusinessLogic.SchemeBlock;
using PDR_DataAccess.SchemeBlock;
using System.Data;

namespace PropertyDataRestructure.UserControls.Pdr.SchemeBlock
{
    public partial class AttributeNotes : UserControlBase, IListingPage, IAddEditPage
    {
        int id = 0;
        string requestType = string.Empty;
        public bool _readOnly = false;
        public static bool IsNotesExist = false;
        public static int NotesId = 0;

        #region Page Load event
        protected void Page_Load(object sender, EventArgs e)
        {
            string PropPageName = Request.Path.ToString();
            if (PropPageName.Contains("SchemeDashBoard"))
            {
                _readOnly = false;
            }
            else if (PropPageName.Contains("SchemeRecord"))
            {
                _readOnly = true;
            }
            if (_readOnly == true)
            {
                btnAddNotes.Visible = false;
            }
            try
            {
                uiMessage.hideMessage();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region btn Add Notes click event
        /// <summary>
        /// btn Add Notes click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAddNotes_Click(object sender, EventArgs e)
        {
            try
            {
                uiMessageNotes.hideMessage();
                txtNote.Text = string.Empty;
                chkOnApp.Checked = false;
                chkScheduling.Checked = false;
                mdlPopUpAddNote.Show();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region btn save click event
        /// <summary>
        /// btn save click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (validateData())
                {
                    saveData();
                    loadData();
                    uiMessage.messageText = UserMessageConstants.itemAddedSuccessfuly;
                }
                else
                {
                    mdlPopUpAddNote.Show();
                }

            }
            catch (Exception ex)
            {
                uiMessageNotes.isError = true;
                uiMessageNotes.messageText = ex.Message;

                if ((uiMessageNotes.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessageNotes.isError == true))
                {
                    uiMessageNotes.showErrorMessage(uiMessageNotes.messageText);
                }
            }
        }
        #endregion

        #region getQueryString()
        private void getQueryString()
        {

            if ((Request.QueryString[ApplicationConstants.Id] != null))
            {
                id = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);

            }

            if ((Request.QueryString[ApplicationConstants.RequestType] != null))
            {

                requestType = Request.QueryString[ApplicationConstants.RequestType];
            }
        }
        #endregion

        #region IListingPage Implementation
        public void loadData()
        {
            getQueryString();


            int? schemeId = null;
            int? blockId = null;

            if (requestType == ApplicationConstants.Scheme)
            {
                schemeId = id;
            }
            else if (requestType == ApplicationConstants.Block)
            {
                blockId = id;
            }

            AttributesBL objAttrBl = new AttributesBL(new AttributesRepo());
            DataSet resultDataSet = new DataSet();
            int? heatingMappingId = null;
            string itemName = objSession.TreeItemName;
            if (itemName.Contains("Boiler"))
            {
                heatingMappingId = objSession.HeatingMappingId;
            }

            resultDataSet = objAttrBl.getItemNotes(schemeId, blockId, objSession.TreeItemId, heatingMappingId);

            IsNotesExist = heatingMappingId != -1 && resultDataSet.Tables[0].Rows.Count > 0;
            SessionManager.setIsNotesExist(ref IsNotesExist);

            grdItemNotes.DataSource = resultDataSet;
            grdItemNotes.DataBind();
        }

        public void searchData()
        {
            throw new NotImplementedException();
        }

        public void populateData()
        {
            throw new NotImplementedException();
        }

        public void printData()
        {
            throw new NotImplementedException();
        }

        public void exportGridToExcel()
        {
            throw new NotImplementedException();
        }

        public void exportToPdf()
        {
            throw new NotImplementedException();
        }

        public void downloadData()
        {
            throw new NotImplementedException();
        }

        public void applyFilters()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region IAddEditPage Implementation
        public void saveData()
        {
            getQueryString();
            AttributeNotesBO objAttrNotes = new AttributeNotesBO();

            if (requestType == ApplicationConstants.Scheme)
            {
                objAttrNotes.SchemeId = id;
            }
            else if (requestType == ApplicationConstants.Block)
            {
                objAttrNotes.BlockId = id;
            }

            string itemName = objSession.TreeItemName;
            if (itemName.Contains("Boiler"))
            {
                objAttrNotes.heatingMappingId = objSession.HeatingMappingId;
            }
            objAttrNotes.ItemId = objSession.TreeItemId;
            objAttrNotes.Notes = txtNote.Text.Trim();
            objAttrNotes.CreatedBy = objSession.EmployeeId;
            objAttrNotes.ShowInScheduling = chkScheduling.Checked;
            objAttrNotes.ShowOnApp = chkOnApp.Checked;
            AttributesBL objAttrBl = new AttributesBL(new AttributesRepo());
            objAttrBl.SaveItemNotes(objAttrNotes);
        }

        public bool validateData()
        {
            if (string.IsNullOrEmpty(txtNote.Text))
            {
                uiMessageNotes.messageText = UserMessageConstants.notValidNote;
                uiMessageNotes.Visible = true;
                return false;
            }
            return true;
        }

        public void resetControls()
        {
            throw new NotImplementedException();
        }

        public void populateDropDown(DropDownList ddl)
        {
            throw new NotImplementedException();
        }

        public void populateDropDowns()
        {
            throw new NotImplementedException();
        }
        #endregion

        protected void btnShowEditNotesPopUp_OnClick(object sender, EventArgs e)
        {
            uiMessageEditNotes.hideMessage();
            Button btnEditNotes = (Button)sender;
            string[] commandArgument = btnEditNotes.CommandArgument.Split(';');
            NotesId = Convert.ToInt32(commandArgument[1]);
            txtEditNote.Text = commandArgument[0];
            chkEditOnScheduling.Checked = Convert.ToBoolean(commandArgument[2]);
            chkEditOnApp.Checked = Convert.ToBoolean(commandArgument[3]);
            mdlPopUpEditNotes.Show();
        }

        protected void btnDeleteNotes_OnClick(object sender, EventArgs e)
        {
            Button btnDelete = (Button)sender;
            string commandArgument = btnDelete.CommandArgument;
            AttributesBL objAttrBl = new AttributesBL(new AttributesRepo());
            objAttrBl.DeleteItemNotes(Convert.ToInt32(commandArgument));
            loadData();
        }

        protected void btnShowNotesTrail_OnClick(object sender, EventArgs e)
        {
            Button btnShowNotesTrail = (Button)sender;
            string commandArgument = btnShowNotesTrail.CommandArgument;
            DataSet resultDataSet = new DataSet();
            AttributesBL objAttrBl = new AttributesBL(new AttributesRepo());
            objAttrBl.GetItemNotesTrail(ref resultDataSet, Convert.ToInt32(commandArgument));
            grdItemNotesTrail.DataSource = resultDataSet;
            grdItemNotesTrail.DataBind();
            mdlPopupNotesTrail.Show();
        }

        #region Show Button For Boiler Room

        protected bool IsShowButton(bool IsActive)
        {
            //string itemName = objSession.TreeItemName;
            //if (itemName.Contains("Boiler"))
            //{
            //    if (objSession.TreeItemId != -1)
            //        return true;
            //}
            //return false;
            return IsActive;
        }

        #endregion

        protected void btnEditNotes_OnClick(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtEditNote.Text))
                {
                    uiMessageEditNotes.Visible = true;
                    uiMessageEditNotes.messageText = UserMessageConstants.notValidNote;
                    mdlPopUpEditNotes.Show();
                }
                else
                {
                    AttributesBL objAttrBl = new AttributesBL(new AttributesRepo());
                    AttributeNotesBO attributeNotesBo = new AttributeNotesBO
                    {
                        Notes = txtEditNote.Text,
                        NotesId = NotesId,
                        ShowInScheduling = chkEditOnScheduling.Checked,
                        ShowOnApp = chkEditOnApp.Checked
                    };

                    objAttrBl.UpdateItemNotes(attributeNotesBo);
                    loadData();
                    uiMessage.messageText = UserMessageConstants.ItemUpdatedSuccessfuly;
                }
            }
            catch (Exception ex)
            {
                uiMessageEditNotes.isError = true;
                uiMessageEditNotes.messageText = ex.Message;

                if ((uiMessageEditNotes.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessageEditNotes.isError)
                {
                    uiMessageEditNotes.showErrorMessage(uiMessageEditNotes.messageText);
                }
            }
        }
    }
}