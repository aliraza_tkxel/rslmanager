﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Data;
using PDR_Utilities.Constants;
using PropertyDataRestructure.Base;
using PDR_BusinessLogic.SchemeBlock;
using PDR_DataAccess.SchemeBlock;
using PDR_BusinessObject.SchemeBlock;
using PDR_Utilities.Helpers;

namespace PropertyDataRestructure.UserControls.Pdr.SchemeBlock
{
    public partial class Detectors : UserControlBase
    {
        public bool _readOnly = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            string PropPageName = Request.Path.ToString();
            if (PropPageName.Contains("SchemeDashBoard"))
            {
                _readOnly = false;
            }
            else if (PropPageName.Contains("SchemeRecord"))
            {
                _readOnly = true;
            }
            if (_readOnly == true)
            {
                btnAddDetector.Visible = false;
                btnSave.Visible = false;
            }

        }


        protected void btnAddDetector_Click(object sender, EventArgs e)
        {
            try
            {
                uiMessage.hideMessage();
                populatePowerSource();
                populateSmokeDetectorType();
                resetControl();
                mdlPopUpAddDetector.Show();

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;
                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessage.isError == true)
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
                uiMessage.isError = false;
                uiMessage.messageText = string.Empty;
            }
        }

        public void loadData()
        {
            try
            {
                int schemeId = -1;
                int blockId = -1;

                if ((Request.QueryString[ApplicationConstants.RequestType] != null))
                {

                    string requestType = Request.QueryString[ApplicationConstants.RequestType];

                    if (requestType == ApplicationConstants.Scheme)
                    {
                        schemeId = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);
                    }
                    else if (requestType == ApplicationConstants.Block)
                    {
                        blockId = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);
                    }

                }
                objSession.PowerSourceDataSet = null;

                AttributesBL objDefectsBl = new AttributesBL(new AttributesRepo());
                DataSet detectorDataSet = new DataSet();
                detectorDataSet = objDefectsBl.getDetectorByPropertyId(schemeId, blockId, objSession.TreeItemName);
                objSession.PowerSourceDataSet = detectorDataSet;
                grdDetectors.DataSource = detectorDataSet.Tables[0];
                grdDetectors.DataBind();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;
                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
        }

        private void resetControl()
        {
            txtBatteryReplaced.Text = string.Empty;
            txtInstalled.Text = string.Empty;
            txtLastTested.Text = string.Empty;
            txtLocation.Text = string.Empty;
            txtManufacturer.Text = string.Empty;
            ddlPowerSource.SelectedIndex = 0;
            txtSerialNumber.Text = string.Empty;
            txtNotes.Text = string.Empty;
        }

        #region "Populate Years"
        private void populatePowerSource()
        {
            DataSet detectorDataSet = new DataSet();
            detectorDataSet = objSession.PowerSourceDataSet;

            ddlPowerSource.DataSource = detectorDataSet.Tables["PowerSource"];
            ddlPowerSource.DataValueField = "PowerTypeId";
            ddlPowerSource.DataTextField = "PowerType";

            ddlPowerSource.DataBind();
            ddlPowerSource.Items.Insert(0, new ListItem("Please Select", ApplicationConstants.DropDownDefaultValues));
            ddlPowerSource.SelectedValue = ApplicationConstants.DropDownDefaultValues;
        }
        #endregion

        #region "Populate Years"

        private void populateSmokeDetectorType()
        {
            ddlSmokeDetectorType.Items.Clear();
            ddlNoOfSmokeDetectors.Items.Clear();

            ddlSmokeDetectorType.Items.Insert(0, new ListItem("Please Select", ApplicationConstants.DropDownDefaultValues));
            ddlSmokeDetectorType.Items.Insert(1, new ListItem("Ionisation"));
            ddlSmokeDetectorType.Items.Insert(2, new ListItem("Optical"));
            ddlSmokeDetectorType.Items.Insert(3, new ListItem("Combined"));
            ddlSmokeDetectorType.Items.Insert(4, new ListItem("Heat"));
            ddlSmokeDetectorType.SelectedValue = ApplicationConstants.DropDownDefaultValues;

            ddlNoOfSmokeDetectors.Items.Insert(0, new ListItem("Please Select", ApplicationConstants.DropDownDefaultValues));
            ddlNoOfSmokeDetectors.Items.Insert(1, new ListItem("0"));
            ddlNoOfSmokeDetectors.Items.Insert(2, new ListItem("1"));
            ddlNoOfSmokeDetectors.Items.Insert(3, new ListItem("2"));
            ddlNoOfSmokeDetectors.Items.Insert(4, new ListItem("3"));
            ddlNoOfSmokeDetectors.Items.Insert(5, new ListItem("4"));
            ddlNoOfSmokeDetectors.SelectedValue = ApplicationConstants.DropDownDefaultValues;

        }
        #endregion

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                uiMessage.hideMessage();
                DetectorsBO objDetectorsBo = new DetectorsBO();
                AttributesBL objDefectsBl = new AttributesBL(new AttributesRepo());
                int schemeId = -1;
                int blockId = -1;

                if ((Request.QueryString[ApplicationConstants.RequestType] != null))
                {

                    string requestType = Request.QueryString[ApplicationConstants.RequestType];

                    if (requestType == ApplicationConstants.Scheme)
                    {
                        schemeId = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);
                    }
                    else if (requestType == ApplicationConstants.Block)
                    {
                        blockId = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);
                    }

                }
                objDetectorsBo.SchemeId = schemeId;
                objDetectorsBo.BlockId = blockId;
                objDetectorsBo.DetectorType = objSession.TreeItemName;
                if (ValidationHelper.validateDateFormate(txtBatteryReplaced.Text))
                {
                    objDetectorsBo.BatteryReplaced = Convert.ToDateTime(txtBatteryReplaced.Text);
                }
                if (ValidationHelper.validateDateFormate(txtInstalled.Text))
                {
                    objDetectorsBo.InstalledDate = Convert.ToDateTime(txtInstalled.Text);
                }

                objDetectorsBo.IsLandlordsDetector = Convert.ToBoolean(rdBtnLandlordsDetector.SelectedValue);
                objDetectorsBo.IsPassed = Convert.ToBoolean(rdBtnPassed.SelectedValue);
                if (ValidationHelper.validateDateFormate(txtLastTested.Text))
                {
                    objDetectorsBo.LastTestedDate = Convert.ToDateTime(txtLastTested.Text);
                }
                
                objDetectorsBo.Location = txtLocation.Text;
                objDetectorsBo.Manufacturer = txtManufacturer.Text;
                objDetectorsBo.PowerSource = Convert.ToInt32(ddlPowerSource.SelectedValue);
                objDetectorsBo.SerialNumber = txtSerialNumber.Text;
                objDetectorsBo.Notes = txtNotes.Text;

                bool saveStatus = objDefectsBl.saveDetectors(objDetectorsBo, ddlSmokeDetectorType.SelectedItem.Text, ddlNoOfSmokeDetectors.SelectedItem.Text, objSession.EmployeeId);
                if (saveStatus)
                {
                    uiMessage.showInformationMessage(UserMessageConstants.SaveDetectorSuccessfully);
                }
                loadData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;
                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessage.isError == true)
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                    mdlPopUpAddDetector.Show();
                }
                uiMessage.isError = false;
                uiMessage.messageText = string.Empty;
            }
        }
    }
}