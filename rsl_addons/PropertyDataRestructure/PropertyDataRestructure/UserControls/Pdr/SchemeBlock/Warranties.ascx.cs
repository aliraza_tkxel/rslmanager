﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PropertyDataRestructure.Base;
using PropertyDataRestructure.Interface;
using PDR_BusinessLogic.Block;
using System.Data;
using PDR_BusinessObject.PageSort;
using PDR_BusinessObject.Warranties;
using PDR_DataAccess.Block;
using PDR_Utilities.Constants;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Threading;

namespace PropertyDataRestructure.UserControls.Pdr.SchemeBlock
{
    public partial class Warranties : UserControlBase, IAddEditPage, IListingPage
    {
        int id = 0;
        string requestType = string.Empty;
        Boolean isReadOnly = false;
        String pageName = "";

        #region "Events"

        #region "Page Load"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            pageName = System.IO.Path.GetFileName(Request.Url.ToString());
            if (pageName.Contains(PathConstants.SchemeRecordPage))
                isReadOnly = true;
            if (isReadOnly)
                btnAddNewWarranty.Visible = false;
            if (!IsPostBack)
            {
                //get Query String Parameters like Id and Type
                //request is  either 'Block' or 'Scheme' respectively.

                //loadData();
            }
        }
        #endregion

        #region "grdWarranties RowDeleted"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdWarranties_RowDeleted(object sender, GridViewDeletedEventArgs e)
        {

        }
        #endregion

        #region "grdWarranties RowEditing"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdWarranties_RowEditing(object sender, GridViewEditEventArgs e)
        {



        }
        #endregion

        #region "btnAddNewWarranty Click"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAddNewWarranty_Click(object sender, EventArgs e)
        {
            uiMessage.hideMessage();
            uiMessagePopup.hideMessage();  
            mdlAddWarrantyPopup.Show();
            populateDropDowns();
            resetControls();
        }
        #endregion

        #region "btnAdd Click"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                uiMessagePopup.hideMessage();
  
                Page.Validate("save");
                if (Page.IsValid && validateData())
                {
                    saveData();
                    mdlAddWarrantyPopup.Hide();
                    loadData();
                }

                //updPnlWarrantyListing.Update();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        #endregion

        #region "btnReset Click"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnReset_Click(object sender, EventArgs e)
        {
            resetControls();
        }
        #endregion

        #region "btnAmend Click"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAmend_Click(object sender, EventArgs e)
        {
            try
            {
                uiMessagePopup.hideMessage();  
                populateDropDowns();
                BlockBL objBlockBL = new BlockBL(new BlockRepo());
                DataSet warrantyDataSet = new DataSet();
                Button amend = (Button)sender;
                int warranty = Convert.ToInt32(amend.CommandArgument);
                objBlockBL.getWarrantyData(ref warrantyDataSet, warranty);
                ddlContractor.SelectedValue = warrantyDataSet.Tables[0].Rows[0]["CONTRACTOR"].ToString();
                ddlWarrantyType.SelectedValue = warrantyDataSet.Tables[0].Rows[0]["WARRANTYTYPE"].ToString();
                ddlCategory.SelectedValue = warrantyDataSet.Tables[0].Rows[0]["Category"].ToString();
                categoryIndex_changed(ddlCategory, null);
                ddlArea.SelectedValue = warrantyDataSet.Tables[0].Rows[0]["Area"].ToString();
                areaIndex_changed(ddlArea, null);
                ddlItem.SelectedValue = warrantyDataSet.Tables[0].Rows[0]["Item"].ToString();
                txtExpiry.Text = warrantyDataSet.Tables[0].Rows[0]["EXPIRYDATE"].ToString();
                txtNotes.Text = warrantyDataSet.Tables[0].Rows[0]["NOTES"].ToString();
                btnAdd.CommandArgument = amend.CommandArgument;
                //updPnlPopUp.Update();

                mdlAddWarrantyPopup.Show();

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        #endregion

        #region "imgBtn Delete Click"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void imgBtnDelete_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                BlockBL objBlockBL = new BlockBL(new BlockRepo());
                ImageButton imgBtnDelete = (ImageButton)sender;
                int warranty = Convert.ToInt32(imgBtnDelete.CommandArgument);
                objBlockBL.deleteWarranty(warranty);
                loadData();
                //updPnlWarrantyListing.Update();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #endregion

        #region "Functions"

        #region "save Data"
        public void saveData()
        {
            try
            {
                getQueryString();
                WarrantyBO objWarrantyBO = new WarrantyBO();
                BlockBL objBlockBL = new BlockBL(new BlockRepo());
                if (!string.IsNullOrEmpty(btnAdd.CommandArgument))
                {
                    objWarrantyBO.WarrantyId = Convert.ToInt32(btnAdd.CommandArgument);
                }
                else
                {
                    objWarrantyBO.WarrantyId = 0;
                }
                objWarrantyBO.WarrantyType = Convert.ToInt32(ddlWarrantyType.SelectedItem.Value);
                objWarrantyBO.Category = Convert.ToInt32(ddlCategory.SelectedItem.Value);
                if (ddlArea.SelectedItem.Value == "-1")
                {
                    objWarrantyBO.Area = null;
                }
                else
                {
                    objWarrantyBO.Area = Convert.ToInt32(ddlArea.SelectedItem.Value);
                }
                if (ddlItem.SelectedItem.Value == "-1")
                {
                    objWarrantyBO.Item = null;
                }
                else
                {
                    objWarrantyBO.Item = Convert.ToInt32(ddlItem.SelectedItem.Value);
                }
                objWarrantyBO.Contractor = Convert.ToInt32(ddlContractor.SelectedItem.Value);
                objWarrantyBO.Expiry = Convert.ToDateTime(txtExpiry.Text);
                objWarrantyBO.Notes = txtNotes.Text;

                //get these value from query string and set it here
                objWarrantyBO.Id = id;
                objWarrantyBO.RequestType = requestType;
                //add warranty for Block and Scheme.
                objBlockBL.saveWarranty(objWarrantyBO);
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region "validate Data"
        public bool validateData()
        {
            bool isValid = true;

            // Validation check to make sure that only 1 NHBC warranty can be added in the database
            if (ddlWarrantyType.SelectedItem.Text.Equals(ApplicationConstants.NHBCWarrantyText))
            {
                getQueryString();
                BlockBL objBl = new BlockBL(new BlockRepo());
                DataSet warrantyDataSet = new DataSet();
                objBl.getNHBCWarranties (ref warrantyDataSet, id, requestType);

                DataTable warranties = warrantyDataSet.Tables[0];
                if (warranties.Rows.Count > 1)
                {
                    isValid = false;
                }
                else if (warranties.Rows.Count==1)
                {
                    int dbWarrantyId = warranties.AsEnumerable().Select(w => w.Field<int>("WARRANTYID")).First();                     
                    if(string.IsNullOrEmpty(btnAdd.CommandArgument) )
                    {
                        isValid = false;
                    }else
                    {
                        int warrantyId = Convert.ToInt32(btnAdd.CommandArgument);
                        if(dbWarrantyId != warrantyId)
                        {
                            isValid = false;
                        }
                    }
                }

            }

            if (!isValid)
            {          
               uiMessagePopup.showErrorMessage(UserMessageConstants.DuplicateNHBCError);
            }
           
            return isValid;
        }
        #endregion

        #region "reset Controls"
        public void resetControls()
        {
            btnAdd.CommandArgument = string.Empty;
            uiMessagePopup.hideMessage();  
            ddlContractor.SelectedIndex = 0;
            ddlCategory.SelectedIndex = 0;
            ddlArea.SelectedIndex = 0;
            ddlItem.SelectedIndex = 0;
            ddlWarrantyType.SelectedIndex = 0;
            txtExpiry.Text = string.Empty;
            txtNotes.Text = string.Empty;
        }
        #endregion

        #region "load Data"
        public void loadData()
        {
            try
            {
                getQueryString();
                BlockBL objBlockBL = new BlockBL(new BlockRepo());
                DataSet warrantyDataSet = new DataSet();
                PageSortBO objPageSortBO = new PageSortBO("DESC", "WARRANTYTYPE", 1, 30);
                objBlockBL.getBlockWarranties(ref warrantyDataSet, id, objPageSortBO, requestType);
                grdWarranties.DataSource = warrantyDataSet;
                grdWarranties.DataBind();

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region "populate DropDown"
        public void populateDropDown(DropDownList ddl)
        {
            throw new NotImplementedException();
        }
        #endregion
        
        #region "populate DropDowns"
        public void populateDropDowns()
        {
            try
            {
                BlockBL objBlockBL = new BlockBL(new BlockRepo());
                DataSet warrantyTypeDataSet = new DataSet();
                objBlockBL.getWarrantyTypes(ref warrantyTypeDataSet);
                if (warrantyTypeDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlWarrantyType.DataSource = warrantyTypeDataSet;
                    ddlWarrantyType.DataValueField = "WARRANTYTYPEID";
                    ddlWarrantyType.DataTextField = "DESCRIPTION";
                    ddlWarrantyType.DataBind();
                    ddlWarrantyType.Items.Insert(0, new ListItem("Select Warranty", "-1"));
                }

                DataSet categoryDataSet = new DataSet();
                objBlockBL.getcategoryDropdownValue(ref categoryDataSet);
                if (categoryDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlCategory.DataSource = categoryDataSet;
                    ddlCategory.DataValueField = "LocationID";
                    ddlCategory.DataTextField = "LocationName";
                    ddlCategory.DataBind();
                }
                ddlCategory.Items.Insert(0, new ListItem("Select Category", "-1"));
                ddlArea.Items.Clear();
                ddlArea.Items.Insert(0, new ListItem("Select Area", "-1"));

                ddlItem.Items.Clear();
                ddlItem.Items.Insert(0, new ListItem("Select Item", "-1"));

                DataSet contractorDataSet = new DataSet();
                objBlockBL.getContractors(ref contractorDataSet);
                if (contractorDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlContractor.DataSource = contractorDataSet;
                    ddlContractor.DataValueField = "ORGID";
                    ddlContractor.DataTextField = "contractor";
                    ddlContractor.DataBind();
                    ddlContractor.Items.Insert(0, new ListItem("Select Contractor", "-1"));
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        #endregion

        #region "search Data"
        public void searchData()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region "populate Data"
        public void populateData()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region "print Data"
        public void printData()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region "export Grid To Excel"
        public void exportGridToExcel()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region "export To Pdf"
        public void exportToPdf()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region "download Data"
        public void downloadData()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region "apply Filters"
        public void applyFilters()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region getQueryString()
        private void getQueryString()
        {
            try
            {

                if ((Request.QueryString[ApplicationConstants.Id] != null))
                {
                    id = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);

                }

                if ((Request.QueryString[ApplicationConstants.RequestType] != null))
                {

                    requestType = Request.QueryString[ApplicationConstants.RequestType];
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        protected void categoryIndex_changed(object sender, EventArgs e)
        {
            try
            {
                ddlArea.Items.Clear();
                ddlItem.Items.Clear();
                BlockBL objBlockBL = new BlockBL(new BlockRepo());
                DataSet areaDataSet = new DataSet();
                objBlockBL.getAreaDropdownValue(ref areaDataSet, Convert.ToInt32(ddlCategory.SelectedItem.Value));
                if (areaDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlArea.DataSource = areaDataSet;
                    ddlArea.DataValueField = "AreaID";
                    ddlArea.DataTextField = "AreaName";
                    ddlArea.DataBind();

                    ddlArea.Items.Insert(0, new ListItem("All", ApplicationConstants.DropDownAllValue));
                }
                ddlArea.Items.Insert(0, new ListItem("Select Area", "-1"));
                ddlItem.Items.Insert(0, new ListItem("Select Item", "-1"));
                mdlAddWarrantyPopup.Show();
            }
            catch (ThreadAbortException ex) { }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }

            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        protected void areaIndex_changed(object sender, EventArgs e)
        {
            try
            {
                ddlItem.Items.Clear();
                BlockBL objBlockBL = new BlockBL(new BlockRepo());
                DataSet itemDataSet = new DataSet();
                objBlockBL.getItemDropdownValue(ref itemDataSet, Convert.ToInt32(ddlArea.SelectedItem.Value));
                if (itemDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlItem.DataSource = itemDataSet;
                    ddlItem.DataValueField = "ItemID";
                    ddlItem.DataTextField = "ItemName";
                    ddlItem.DataBind();

                    ddlItem.Items.Insert(0, new ListItem("All", ApplicationConstants.DropDownAllValue));
                }
                else
                {
                    if (ddlArea.SelectedItem.Value.Equals(ApplicationConstants.DropDownAllValue))
                    {
                        ddlItem.Items.Insert(0, new ListItem("All", ApplicationConstants.DropDownAllValue));
                        ddlItem.SelectedValue = ApplicationConstants.DropDownAllValue;
                    }
                }

                ddlItem.Items.Insert(0, new ListItem("Select Item", "-1"));

                mdlAddWarrantyPopup.Show();
            }
            catch (ThreadAbortException ex) { }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }

            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        //Protected Sub areaIndex_changed(ByVal sender As Object, ByVal e As EventArgs)


        #endregion
    }
}