﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AttributeNotes.ascx.cs"
    Inherits="PropertyDataRestructure.UserControls.Pdr.SchemeBlock.AttributeNotes" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="500px" />
<asp:UpdatePanel ID="updPanelNotes" runat="server">
    <ContentTemplate>
        <div style="float: right; margin: -8px 0px 0 0px;">
            <asp:Button runat="server" ID="btnAddNotes" Text="Add Notes" BackColor="White" OnClick="btnAddNotes_Click" />
        </div>
        <asp:GridView ID="grdItemNotes" runat="server" AutoGenerateColumns="false" GridLines="None"
            ShowHeaderWhenEmpty="false" Width="100%" EmptyDataText="No Record Found">
            <Columns>
                <asp:BoundField DataField="CreatedOn" HeaderText="Date:">
                    <ItemStyle Width="15%" VerticalAlign="Top" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="Notes:">
                    <ItemTemplate>
                        <asp:Label ID="Label1" Text='<%# Eval("Notes")%>' runat="server" Width="90%" Style='margin-bottom: 10px;' />
                    </ItemTemplate>
                    <ItemStyle Width="35%" />
                </asp:TemplateField>
                <asp:BoundField DataField="CreatedBy" HeaderText="By:">
                    <ItemStyle Width="25%" />
                </asp:BoundField>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:Button runat="server" ID="btnShowEditNotesPopUp" OnClick="btnShowEditNotesPopUp_OnClick" Text="Edit"
                            Visible='<%# IsShowButton(Convert.ToBoolean( Eval("ISACTIVE"))) %>' BackColor="White" CommandArgument='<%# Eval("Notes")+";"+Eval("SID")+";"+Eval("ShowInScheduling")+";"+Eval("ShowOnApp") %>' />
                        <asp:Button runat="server" ID="btnDeleteNotes" OnClick="btnDeleteNotes_OnClick" Text="Delete"
                            Visible='<%# IsShowButton(Convert.ToBoolean( Eval("ISACTIVE"))) %>' BackColor="White" CommandArgument='<%# Eval("SID").ToString() %>' />
                        <asp:Button runat="server" ID="btnShowNotesTrail" OnClick="btnShowNotesTrail_OnClick"
                            Visible='<%# IsShowButton(true) %>' Text="History" BackColor="White" CommandArgument='<%# Eval("SID").ToString() %>' />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <RowStyle BackColor="#EFF3FB"></RowStyle>
            <EditRowStyle BackColor="#2461BF"></EditRowStyle>
            <SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>
            <HeaderStyle BackColor="#ffffff" ForeColor="black" Font-Bold="True" HorizontalAlign="Left"
                CssClass="table-head"></HeaderStyle>
            <AlternatingRowStyle BackColor="White" Wrap="True"></AlternatingRowStyle>
        </asp:GridView>
        <asp:Panel ID="pnlAddNotePopup" runat="server" BackColor="White" Width="340px">
            <table id="pnlAddNoteTable" width="300px" style='margin: 20px;'>
                <tr>
                </tr>
                <tr>
                    <td colspan="2" valign="top">
                        <div style="float: left; font-weight: bold; padding-left: 10px;">
                            Add New Note</div>
                        <div style="clear: both">
                        </div>
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" colspan="2">
                        <uim:UIMessage ID="uiMessageNotes" runat="Server" Visible="false" width="300px" />
                    </td>
                </tr>
                <tr>
                <td colspan="2" >
                    <asp:CheckBox Text="Show in Scheduling" runat="server" ID="chkScheduling" />
                    <br />
                    <asp:CheckBox Text="Show on App" runat="server" ID="chkOnApp" />
                    
                </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <span style="width: 50px; margin-left: 20px;">Note:<span class="Required">*</span></span>
                    </td>
                    <td align="left" valign="top" style="width: 250px;">
                        <asp:TextBox ID="txtNote" runat="server" Width="200" TextMode="MultiLine" Rows="5"
                            MaxLength="500">
                        </asp:TextBox>
                        <asp:RegularExpressionValidator ID = "revTexbox3" runat = "server" 
                        ErrorMessage= "<br/>You must enter up to a maximum of 500 characters" ValidationExpression ="^([\S\s]{0,500})$"
                        ControlToValidate ="txtNote" Display ="Dynamic" CssClass="Required"  ValidationGroup="AddNotes"   ></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        &nbsp;
                    </td>
                    <td align="right" valign="top" style="text-align: right;" class="style1">
                        <div style='margin-right: 25px;'>
                            <input id="btnCancel" type="button" value="Cancel" style="background-color: White;" />
                            &nbsp;
                            <asp:Button ID="btnSave" runat="server" Text="Save" BackColor="White" OnClick="btnSave_Click" ValidationGroup="AddNotes" />
                            &nbsp;</div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
<asp:ModalPopupExtender ID="mdlPopUpAddNote" runat="server" DynamicServicePath=""
    Enabled="True" PopupControlID="pnlAddNotePopup" DropShadow="true" CancelControlID="btnCancel"
    TargetControlID="lblDispPhotoGraph" BackgroundCssClass="modalBackground">
</asp:ModalPopupExtender>
<asp:Label ID="lblDispPhotoGraph" runat="server"></asp:Label>

<%------- Edit Notes Section ---------%>
<asp:UpdatePanel ID="updPanelEditNotes" runat="server">
    <ContentTemplate>
        <asp:Panel ID="pnlPopUpEditNotes" runat="server" BackColor="White" Width="340px">
            <table id="Table1" width="300px" style='margin: 20px;'>
                <tr>
                </tr>
                <tr>
                    <td colspan="2" valign="top">
                        <div style="float: left; font-weight: bold; padding-left: 10px;">
                            Edit Note</div>
                        <div style="clear: both">
                        </div>
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" colspan="2" style="color: red;">
                        <uim:UIMessage ID="uiMessageEditNotes" runat="Server" width="300px" Visible="False" />
                    </td>
                </tr>
                <tr>
                <td colspan="2" >
                    <asp:CheckBox Text="Show in Scheduling" runat="server" ID="chkEditOnScheduling" />
                    <br />
                    <asp:CheckBox Text="Show on App" runat="server" ID="chkEditOnApp" />              
                </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <span style="width: 50px; margin-left: 20px;">Note:<span class="Required">*</span></span>
                    </td>
                    <td align="left" valign="top" style="width: 250px;">
                        <asp:TextBox ID="txtEditNote" runat="server" Width="200" TextMode="MultiLine" Rows="5"
                            MaxLength="500">
                        </asp:TextBox>
                        <asp:RegularExpressionValidator ID = "RegularExpressionValidator1" runat = "server" 
                        ErrorMessage= "<br/>You must enter up to a maximum of 500 characters" ValidationExpression ="^([\S\s]{0,500})$"
                        ControlToValidate ="txtEditNote" Display ="Dynamic" CssClass="Required"  ValidationGroup="EditNotes"   ></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        &nbsp;
                    </td>
                    <td align="right" valign="top" style="text-align: right;" class="style1">
                        <div style='margin-right: 25px;'>
                            <input id="btnEditCancel" type="button" value="Cancel" style="background-color: White;" />
                            &nbsp;
                            <asp:Button ID="btnEditNotes" runat="server" Text="Save" BackColor="White" OnClick="btnEditNotes_OnClick" ValidationGroup="EditNotes" />
                            &nbsp;</div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
<asp:modalpopupextender id="mdlPopUpEditNotes" runat="server" dynamicservicepath=""
    targetcontrolid="lblPopUpEditNotes" popupcontrolid="pnlPopUpEditNotes" cancelcontrolid="btnEditCancel"
    dropshadow="true" backgroundcssclass="modalBackground">
</asp:modalpopupextender>
<asp:label id="lblPopUpEditNotes" runat="server" text=""></asp:label>

<%------- Notes Trail Section ---------%>
<asp:updatepanel id="UpdatePanel1" runat="server">
    <contenttemplate>
        <asp:Panel ID="pnlPopupNotesTrail" runat="server" class="modalPopupSchedular" Style="min-width: 600px;
            max-width: 750px; border-width: 1px; border: 1px solid black">
            <div style="overflow: auto; max-height: 300px;">
                <asp:ImageButton ID="imgBtnCancelAlertPopup" runat="server" Style="position: absolute;
                    top: -12px; right: -12px; width: 22px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png"
                    BorderWidth="0" />
                <asp:GridView ID="grdItemNotesTrail" runat="server" AutoGenerateColumns="false" GridLines="None"
                    ShowHeaderWhenEmpty="false" Width="100%">
                    <Columns>
                        <asp:BoundField DataField="CreatedOn" HeaderText="Date:">
                            <ItemStyle  VerticalAlign="Top" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Notes" HeaderText="Notes">
                            <ItemStyle  VerticalAlign="Top" />
                        </asp:BoundField>
                        <asp:BoundField DataField="CreatedBy" HeaderText="By:">
                            <ItemStyle  VerticalAlign="Top"/>
                        </asp:BoundField>           
                        <asp:BoundField DataField="ShowInScheduling" HeaderText="ShowInScheduling:">
                            <ItemStyle VerticalAlign="Top"/>
                        </asp:BoundField>
                        <asp:BoundField DataField="ShowOnApp" HeaderText="ShowOnApp:">
                            <ItemStyle  VerticalAlign="Top"/>
                        </asp:BoundField>
                        <asp:BoundField DataField="NotesStatus" HeaderText="Status:">
                            <ItemStyle VerticalAlign="Top"/>
                        </asp:BoundField>
                    </Columns>
                    <HeaderStyle HorizontalAlign="Left" CssClass="gridHeader" />
                </asp:GridView>
            </div>
        </asp:Panel>
    </contenttemplate>
</asp:updatepanel>
<asp:modalpopupextender id="mdlPopupNotesTrail" runat="server" dynamicservicepath=""
    targetcontrolid="lblPopupNotesTrailClose" popupcontrolid="pnlPopupNotesTrail"
    cancelcontrolid="imgBtnCancelAlertPopup" dropshadow="true" backgroundcssclass="modalBackground">
</asp:modalpopupextender>
<asp:label id="lblPopupNotesTrailClose" runat="server" text=""></asp:label>