﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_BusinessLogic.SchemeBlock;
using PDR_DataAccess.SchemeBlock;
using PDR_Utilities.Constants;
using PropertyDataRestructure.Base;
using PropertyDataRestructure.Interface;

namespace PropertyDataRestructure.UserControls.Pdr.SchemeBlock
{
    public partial class ProvisionDisposals :  UserControlBase, IListingPage, IAddEditPage
    {
        int id = 0;
        string requestType = string.Empty;
        ProvisionsBL objProvisionBL = new ProvisionsBL(new ProvisionsRepo());
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                uiMessage.hideMessage();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
       
        #region IListingPage Implementation
        public void loadData()
        {
                int? schemeId = null;
                int? blockId = null;

                this.GetQueryStringParams();
                if (requestType == ApplicationConstants.Scheme)
                    schemeId = id;
                else if (requestType == ApplicationConstants.Block)
                    blockId = id;

                DataSet provisionsNotesDS = objProvisionBL.GetProvisionItemHistory(objSession.TreeItemId, schemeId, blockId);
                grdProvisionDisposals.DataSource = provisionsNotesDS;
                grdProvisionDisposals.DataBind();
            
        }
        public void searchData()
        {
            throw new NotImplementedException();
        }
        public void populateData()
        {
            throw new NotImplementedException();
        }
        public void printData()
        {
            throw new NotImplementedException();
        }
        public void exportGridToExcel()
        {
            throw new NotImplementedException();
        }
        public void exportToPdf()
        {
            throw new NotImplementedException();
        }
        public void downloadData()
        {
            throw new NotImplementedException();
        }
        public void applyFilters()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region IAddEditPage Implementation
        public void saveData()
        {
            throw new NotImplementedException();
        }
        public bool validateData()
        {
            throw new NotImplementedException();
        }
        public void resetControls()
        {
            throw new NotImplementedException();
        }
        public void populateDropDown(DropDownList ddl)
        {
            throw new NotImplementedException();
        }
        public void populateDropDowns()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region "Helper Functions"
        private void GetQueryStringParams()
        {
            if ((Request.QueryString[ApplicationConstants.Id] != null))
                id = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);

            if ((Request.QueryString[ApplicationConstants.RequestType] != null))
                requestType = Request.QueryString[ApplicationConstants.RequestType];

        }


        #endregion
    }
}