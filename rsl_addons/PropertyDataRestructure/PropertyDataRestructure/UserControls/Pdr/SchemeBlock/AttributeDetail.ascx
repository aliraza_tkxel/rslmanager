﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AttributeDetail.ascx.cs"
    Inherits="PropertyDataRestructure.UserControls.Pdr.SchemeBlock.AttributeDetail" %>
<%@ Register TagPrefix="attr" TagName="ItemDetail" Src="~/UserControls/Pdr/SchemeBlock/ItemDetail.ascx" %>
<%@ Register TagPrefix="attr" TagName="AttributeNotes" Src="~/UserControls/Pdr/SchemeBlock/AttributeNotes.ascx" %>
<%@ Register TagPrefix="attr" TagName="AttributePhotographs" Src="~/UserControls/Pdr/SchemeBlock/AttributePhotographs.ascx" %>
<%@ Register TagPrefix="attr" TagName="Appliances" Src="~/UserControls/Pdr/SchemeBlock/Appliance.ascx" %>
<%@ Register TagPrefix="defect" TagName="Defect" Src="~/UserControls/Pdr/SchemeBlock/HeatingDefect.ascx" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<%@ Register TagPrefix="detector" TagName="detector" Src="~/UserControls/Pdr/SchemeBlock/Detectors.ascx" %>
<uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="500px" />
<div style="padding: 10px 10px 0px 10px;">
    <div style="padding: 10px 10px 0 0px;">
        <div style="overflow: auto; padding: 0px !important;">
            <asp:LinkButton ID="lnkBtnDetailTab" OnClick="lnkBtnDetailTab_Click" CssClass="TabClicked"
                runat="server" BorderStyle="Solid">Details: </asp:LinkButton>
            <asp:LinkButton ID="lnkBtnDetectorTab" OnClick="lnkBtnDetectorTab_Click" CssClass="TabInitial"
                runat="server" BorderStyle="Solid" Visible="false">Detector: </asp:LinkButton>
            <asp:LinkButton ID="lnkBtnAppliancesTab" OnClick="lnkBtnAppliancesTab_Click" CssClass="TabClicked"
                runat="server" BorderStyle="Solid" Visible="false">Appliance: </asp:LinkButton>
            <asp:LinkButton ID="lnkBtnNotesTab" OnClick="lnkBtnNotesTab_Click" CssClass="TabInitial" Enabled="False"
                runat="server" BorderStyle="Solid">Notes: </asp:LinkButton>
            <asp:LinkButton ID="lnkBtnPhotographsTab" OnClick="lnkBtnPhotographsTab_Click" CssClass="TabInitial" Enabled="False"
                runat="server" BorderStyle="Solid">Photographs: </asp:LinkButton>
            <asp:LinkButton ID="lnkBtnDefectsTab" runat="server" CssClass="TabInitial" BorderStyle="Solid"
                Visible="false" OnClick="lnkBtnDefectsTab_Click">Defects:</asp:LinkButton>
            <asp:Button runat="server" ID="btnAmend" Text="Amend" CssClass="btn btn-xs btn-blue right"
                Style="padding: 3px 23px !important; float: right; margin: 0px 11px 0 0px;" BackColor="White"
                Visible="false" ValidationGroup="save" OnClick="btnAmend_Click" />
            <span style="display: block; height: 27px; border-bottom: 1px solid #c5c5c5"></span>
        </div>
    </div>
    <div style="border: 1px solid #c5c5c5; height: 500px; overflow: scroll; clear: both;
        width: 98%; padding-left: 10px; float: left; padding-right: 10px; padding-bottom: 20px;
        border-top: 1px solid #fff;">
        <div style="clear: both; margin-top: 5px;">
        </div>
        <br />
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:MultiView ID="MainView" runat="server" ActiveViewIndex="0">
                    <asp:View ID="View1" runat="server">
                        <attr:ItemDetail runat="server" ID="ucItemDetail"></attr:ItemDetail>
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <attr:AttributeNotes ID="ucAttributeNotes" runat="server" />
                    </asp:View>
                    <asp:View ID="View3" runat="server">
                        <attr:AttributePhotographs ID="ucAttributePhotographs" runat="server" />
                    </asp:View>
                    <asp:View ID="View4" runat="server">
                        <attr:Appliances ID="ucAppliances" runat="server" />
                    </asp:View>
                    <asp:View ID="View5" runat="server">
                        <detector:detector ID="detectorTab" runat="server" />
                    </asp:View>
                    <asp:View ID="View6" runat="server">
                        <defect:Defect ID="defectTab" runat="server" />
                    </asp:View>
                </asp:MultiView>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>
