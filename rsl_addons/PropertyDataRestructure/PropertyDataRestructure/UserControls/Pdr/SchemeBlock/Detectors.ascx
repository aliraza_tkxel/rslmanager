﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Detectors.ascx.cs" Inherits="PropertyDataRestructure.UserControls.Pdr.SchemeBlock.Detectors" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<asp:UpdatePanel runat="server" ID="updPanelDetectors">
    <ContentTemplate>
        <asp:Panel runat="server" ID="pnlDetectorMessage">
            <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="500px" />
        </asp:Panel>
        <div style="float: right; margin: 0px 8px 16px 0px;">
            <asp:Button runat="server" ID="btnAddDetector" Text="Add Detectors" BackColor="White"
                Style="height: 26px" OnClick="btnAddDetector_Click" />
        </div>
        <asp:GridView ID="grdDetectors" runat="server" AutoGenerateColumns="False" GridLines="None"
            Width="100%" ShowHeaderWhenEmpty="true" EmptyDataText="No detector found.">
            <Columns>
                <asp:BoundField DataField="Location" HeaderText="Location:">
                    <ItemStyle Width="10%" />
                </asp:BoundField>
                <asp:BoundField DataField="Manufacturer" HeaderText="Manufacturer:">
                    <ItemStyle Width="10%" />
                </asp:BoundField>
                <asp:BoundField DataField="SerialNumber" HeaderText="Serial Number: ">
                    <ItemStyle Width="10%" />
                </asp:BoundField>
                <asp:BoundField DataField="Power" HeaderText="Power:">
                    <ItemStyle Width="5%" />
                </asp:BoundField>
                <asp:BoundField DataField="Installed" HeaderText="Installed:">
                    <ItemStyle Width="10%" />
                </asp:BoundField>
                <asp:BoundField DataField="InstalledByPerson" HeaderText="By:">
                    <ItemStyle Width="15%" />
                </asp:BoundField>
            </Columns>
            <HeaderStyle HorizontalAlign="Left" CssClass="gridHeader" />
        </asp:GridView>
    </ContentTemplate>
</asp:UpdatePanel>
<asp:Panel ID="pnlAddDetectorsPopup" runat="server" BackColor="White" Width="550px"
    Style='padding: 10px;'>
    <table width="500px" style='margin: 20px;'>
        <tr>
            <td colspan="2" valign="top">
                <div style="float: left; font-weight: bold; padding-left: 10px;">
                    Add New Detector</div>
                <div style="clear: both">
                </div>
                <hr />
            </td>
        </tr>
        <tr>
            <td align="left" valign="top" colspan="2">
                <asp:Panel ID="pnlAddDetectorMessage" runat="server" Visible="false">
                    <asp:Label ID="lblAddDetectorMessage" runat="server" Text=""></asp:Label>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <span style="width: 50px; margin-left: 20px;">Smoke detector type:<span class="Required">*</span></span>
            </td>
            <td align="left" valign="top" style="width: 250px;">
                <asp:DropDownList runat="server" ID="ddlSmokeDetectorType" Width="205">
                </asp:DropDownList>
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" InitialValue="-1"
                    ErrorMessage="</br>Please select a value" Display="Dynamic" ValidationGroup="save"
                    ControlToValidate="ddlSmokeDetectorType" ForeColor="Red" />
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <span style="width: 50px; margin-left: 20px;">Number of Smoke Detectors:<span class="Required">*</span></span>
            </td>
            <td align="left" valign="top" style="width: 250px;">
                <asp:DropDownList runat="server" ID="ddlNoOfSmokeDetectors" Width="205">
                </asp:DropDownList>
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator3" InitialValue="-1"
                    ErrorMessage="</br>Please select a value" Display="Dynamic" ValidationGroup="save"
                    ControlToValidate="ddlNoOfSmokeDetectors" ForeColor="Red" />
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <span style="width: 50px; margin-left: 20px;">Location:<span class="Required">*</span></span>
            </td>
            <td align="left" valign="top" style="width: 250px;">
                <asp:TextBox ID="txtLocation" runat="server" Width="200">
                </asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ErrorMessage="</br>Please enter Location."
                    Display="Dynamic" ValidationGroup="save" ControlToValidate="txtLocation" ForeColor="Red"
                    Style="vertical-align: top;" />
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <span style="width: 50px; margin-left: 20px;">Manufacturer:<span class="Required">*</span></span>
            </td>
            <td align="left" style="width: 250px;" valign="top">
                <asp:TextBox ID="txtManufacturer" runat="server" Width="200">
                </asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ID="rfvtxtManufacturer" ErrorMessage="</br>Please enter Manufacturer."
                    Display="Dynamic" ValidationGroup="save" ControlToValidate="txtManufacturer"
                    ForeColor="Red" Style="vertical-align: top;" />
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <span style="width: 50px; margin-left: 20px;">Serial Number:<span class="Required">*</span></span>
            </td>
            <td align="left" style="width: 250px;" valign="top">
                <asp:TextBox ID="txtSerialNumber" runat="server" Width="200">
                </asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ID="rfvtxtSerialNumber" ErrorMessage="</br>Please enter Serial Number."
                    Display="Dynamic" ValidationGroup="save" ControlToValidate="txtSerialNumber"
                    ForeColor="Red" Style="vertical-align: top;" />
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <span style="width: 50px; margin-left: 20px;">Power:<span class="Required">*</span></span>
            </td>
            <td align="left" style="width: 250px;" valign="top">
                <asp:DropDownList runat="server" ID="ddlPowerSource" Width="205">
                </asp:DropDownList>
                <asp:RequiredFieldValidator runat="server" ID="rfvddlPowerSource" InitialValue="-1"
                    ErrorMessage="</br>Please select a Power" Display="Dynamic" ValidationGroup="save"
                    ControlToValidate="ddlPowerSource" ForeColor="Red" />
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <span style="width: 50px; margin-left: 20px;">Installed:<span class="Required">*</span></span>
            </td>
            <td align="left" style="width: 250px;" valign="top">
                <asp:TextBox ID="txtInstalled" runat="server" Width="200" Style="float: left;">
                </asp:TextBox>
                <asp:Image ID="imgtxtInstalled" runat="server" Height="23px" ImageUrl="~/Images/calendar.png"
                    Style="float: right" Width="25px" />
                <asp:CalendarExtender ID="caltxtInstalled" runat="server" TargetControlID="txtInstalled"
                    Format="dd/MM/yyyy" PopupButtonID="imgtxtInstalled" />
                <asp:RequiredFieldValidator runat="server" ID="rfvtxtInstalled" ErrorMessage="</br>Please enter Original Install Date."
                    Display="Dynamic" ValidationGroup="save" ControlToValidate="txtInstalled" ForeColor="Red"
                    Style="vertical-align: top;" />
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <span style="width: 50px; margin-left: 20px;">Landlords Detector:</span>
            </td>
            <td align="left" style="width: 250px;" valign="top">
                <asp:RadioButtonList runat="server" ID="rdBtnLandlordsDetector" RepeatDirection="Horizontal">
                    <asp:ListItem Text="Yes" Value="True" />
                    <asp:ListItem Text="No" Value="False" Selected="True" />
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <span style="width: 50px; margin-left: 20px;">Last Tested:</span>
            </td>
            <td align="left" style="width: 250px;" valign="top">
                <asp:TextBox ID="txtLastTested" runat="server" OnClick="this.value=''" Width="200"
                    Style="float: left;">
                </asp:TextBox>
                <asp:Image ID="imgCalendar" runat="server" Height="23px" ImageUrl="~/Images/calendar.png"
                    Style="float: right" Width="25px" />
                <asp:CalendarExtender ID="cntrlCalendarExtender" runat="server" TargetControlID="txtLastTested"
                    Format="dd/MM/yyyy" PopupButtonID="imgCalendar" />
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <span style="width: 50px; margin-left: 20px;">Battery Replaced:</span>
            </td>
            <td align="left" style="width: 250px;" valign="top">
                <asp:TextBox ID="txtBatteryReplaced" runat="server" OnClick="this.value=''" Width="200"
                    Style="float: left;">
                </asp:TextBox>
                <asp:Image ID="imgBatteryReplaced" runat="server" Height="23px" ImageUrl="~/Images/calendar.png"
                    Style="float: right" Width="25px" />
                <asp:CalendarExtender ID="calExtenderBatteryReplaced" runat="server" TargetControlID="txtBatteryReplaced"
                    Format="dd/MM/yyyy" PopupButtonID="imgBatteryReplaced" />
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <span style="width: 50px; margin-left: 20px;">Passed:</span>
            </td>
            <td align="left" style="width: 250px;" valign="top">
                <asp:RadioButtonList runat="server" ID="rdBtnPassed" RepeatDirection="Horizontal">
                    <asp:ListItem Text="Yes" Value="True" />
                    <asp:ListItem Text="No" Value="False" Selected="True" />
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                <span style="width: 50px; margin-left: 20px;">Notes:</span>
            </td>
            <td align="left" style="width: 250px;" valign="top">
                <asp:TextBox runat="server" ID="txtNotes" Width="200" TextMode="MultiLine" Rows="7"
                    MaxLength="2000" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="left" valign="top">
                &nbsp;
            </td>
            <td align="right" valign="top" style="text-align: right;" class="style1">
                <div style='margin-right: 25px;'>
                    <input id="btnCancel" type="button" value="Cancel" style="background-color: White;" />
                    &nbsp;
                    <asp:Button ID="btnSave" runat="server" Text="Save" BackColor="White" ValidationGroup="save" OnClick="btnSave_Click" />
                    &nbsp;</div>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                &nbsp;
            </td>
        </tr>
    </table>
</asp:Panel>
<asp:ModalPopupExtender ID="mdlPopUpAddDetector" runat="server" DynamicServicePath=""
    Enabled="True" TargetControlID="lblDispDetector" PopupControlID="pnlAddDetectorsPopup"
    DropShadow="true" CancelControlID="btnCancel" BackgroundCssClass="modalBackground">
</asp:ModalPopupExtender>
<asp:Label ID="lblDispDetector" runat="server"></asp:Label>