﻿using System;
using System.Web;
using System.Web.UI.WebControls;
using PropertyDataRestructure.Base;
using PropertyDataRestructure.Interface;
using PDR_BusinessObject.SchemeBlock;
using PDR_Utilities.Constants;
using System.IO;
using PDR_Utilities.Helpers;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_BusinessLogic.SchemeBlock;
using PDR_DataAccess.SchemeBlock;
using System.Data;
using PDR_Utilities.Managers;

namespace PropertyDataRestructure.UserControls.Pdr.SchemeBlock
{


    public partial class AttributePhotographs : UserControlBase, IListingPage, IAddEditPage
    {
        #region Properties
        int id = 0;
        string requestType = string.Empty;
        public static bool isPhotoGraphExist = false;
        public bool _readOnly = false;
        #endregion

        #region Page Load Event
        protected void Page_Load(object sender, EventArgs e)
        {
            string PropPageName = Request.Path.ToString();
            if (PropPageName.Contains("SchemeDashBoard"))
            {
                _readOnly = false;
            }
            else if (PropPageName.Contains("SchemeRecord"))
            {
                _readOnly = true;
            }
            if (_readOnly == true)
            {
                btnUploadPhoto.Visible = false;
            }
            try
            {
                Page.Form.Attributes.Add("enctype", "multipart/form-data");
                if (!IsPostBack)
                {

                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region btnUploadPhoto Click
        protected void btnUploadPhoto_Click(object sender, EventArgs e)
        {
            try
            {
                this.resetControls();
                mdlPopUpAddPhoto.Show();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region lstViewPhotos Item Command
        /// <summary>
        /// lstViewPhotos Item Command
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lstViewPhotos_ItemCommand(object sender, ListViewCommandEventArgs e)
        {
            if (e.CommandName == "ShowImage")
            {
                string fileName = e.CommandArgument.ToString();

                getQueryString();

                if (requestType == ApplicationConstants.Scheme)
                {
                    fileName = fileName.Substring(15);
                    var requestURL = HttpContext.Current.Request.Url;
                    string imagePath = requestURL.GetLeftPart(UriPartial.Authority) + "/" + fileName;
                    imgDynamicImage.ImageUrl = imagePath;
                }
                else
                {
                    imgDynamicImage.ImageUrl = fileName;
                }



                //mdlPopUpViewImage.X = 100
                //mdlPopUpViewImage.Y = 20
                mdlPopUpViewImage.Show();
            }
        }
        #endregion

        #region btn Save event
        /// <summary>
        /// btn Save event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (validateData())
                {
                    uiMessage.hideMessage();
                    uiMessagePopUp.hideMessage();
                    saveData();
                    loadData();
                }
                else
                {
                    mdlPopUpAddPhoto.Show();
                }

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }


        }
        #endregion

        #region IListingPage,IAddEditPage Implementation
        public void saveData()
        {
            AttributePhotographsBO objAttrPhotoBo = new AttributePhotographsBO();
            getQueryString();
            string filename = flUploadDoc.FileName;
            DocumentsBO objDocumentBO = new DocumentsBO();
            var fileDirectoryPath = string.Empty;
            if (requestType == ApplicationConstants.Scheme)
            {
                objAttrPhotoBo.SchemeId = id;
                fileDirectoryPath = Server.MapPath(ResolveClientUrl(ConfigHelper.GetSchemeDocUploadPath()) + id.ToString() + "/Images/");
            }
            else if (requestType == ApplicationConstants.Block)
            {
                objAttrPhotoBo.BlockId = id;
                fileDirectoryPath = Server.MapPath(ResolveClientUrl(ConfigHelper.GetBlockDocUploadPath()) + id.ToString() + "/Images/");
            }

            if ((Directory.Exists(fileDirectoryPath) == false))
            {
                Directory.CreateDirectory(fileDirectoryPath);
            }

            filename = FileHelper.getUniqueFileName(filename);
            objDocumentBO.DocumentName = filename;
            string filePath = fileDirectoryPath + filename;
            string itemName = objSession.TreeItemName;
            if (itemName.Contains("Boiler"))
            {
                objAttrPhotoBo.heatingMappingId = objSession.HeatingMappingId;
            }
            objAttrPhotoBo.ImageName = filename;
            objAttrPhotoBo.FilePath = filePath;
            objAttrPhotoBo.UploadDate = Convert.ToDateTime(txtBoxDueDate.Text);
            objAttrPhotoBo.Title = txtBoxTitle.Text;
            objAttrPhotoBo.ItemId = objSession.TreeItemId;
            objAttrPhotoBo.CreatedBy = objSession.EmployeeId;
            flUploadDoc.SaveAs(@filePath);
            AttributesBL objAttrBl = new AttributesBL(new AttributesRepo());
            objAttrBl.saveDocumentUpload(objAttrPhotoBo);
        }

        public bool validateData()
        {
            if (ValidationHelper.emptyString(txtBoxTitle.Text))
            {
                uiMessagePopUp.showErrorMessage(UserMessageConstants.fillMandatory);
                return false;
            }
            if (!ValidationHelper.validateDateFormate(txtBoxDueDate.Text))
            {
                uiMessagePopUp.showErrorMessage(UserMessageConstants.notValidDateFormat);
                return false;
            }
            if (!flUploadDoc.HasFile)
            {
                uiMessagePopUp.showErrorMessage(UserMessageConstants.SelectAFile);
                return false;
            }
            if (!CheckFileType(flUploadDoc.FileName))
            {
                uiMessage.showErrorMessage(UserMessageConstants.InvalidImageType);
                return false;
            }

            return true;
        }

        public void resetControls()
        {
            this.txtBoxDueDate.Text = string.Empty;
            this.txtBoxTitle.Text = string.Empty;
        }

        public void loadData()
        {
            getQueryString();


            int? schemeId = null;
            int? blockId = null;

            if (requestType == ApplicationConstants.Scheme)
            {
                schemeId = id;
            }
            else if (requestType == ApplicationConstants.Block)
            {
                blockId = id;
            }
            int? heatingMappingId = null;
            string itemName = objSession.TreeItemName;
            if (itemName.Contains("Boiler"))
            {
                heatingMappingId = objSession.HeatingMappingId;
            }

            AttributesBL objAttrBl = new AttributesBL(new AttributesRepo());
            DataSet resultDataSet = new DataSet();
            resultDataSet = objAttrBl.getPropertyImages(objSession.TreeItemId, schemeId, blockId, heatingMappingId);
            isPhotoGraphExist = heatingMappingId != -1 && resultDataSet.Tables[0].Rows.Count > 0;
            SessionManager.setIsPhotoGraphExist(ref isPhotoGraphExist);
            lstViewPhotos.DataSource = resultDataSet;
            lstViewPhotos.DataBind();

        }

        public void populateDropDown(DropDownList ddl)
        {
            throw new NotImplementedException();
        }

        public void populateDropDowns()
        {
            throw new NotImplementedException();
        }


        public void searchData()
        {
            throw new NotImplementedException();
        }

        public void populateData()
        {
            throw new NotImplementedException();
        }

        public void printData()
        {
            throw new NotImplementedException();
        }

        public void exportGridToExcel()
        {
            throw new NotImplementedException();
        }

        public void exportToPdf()
        {
            throw new NotImplementedException();
        }

        public void downloadData()
        {
            throw new NotImplementedException();
        }

        public void applyFilters()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region getQueryString()
        private void getQueryString()
        {

            if ((Request.QueryString[ApplicationConstants.Id] != null))
            {
                id = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);

            }

            if ((Request.QueryString[ApplicationConstants.RequestType] != null))
            {

                requestType = Request.QueryString[ApplicationConstants.RequestType];
            }
        }
        #endregion

        #region get Image Uri
        /// <summary>
        /// get Image Uri
        /// </summary>
        /// <param name="imageName"></param>
        /// <returns></returns>
        public string getImageUri(string imageName)
        {
            getQueryString();

            var fileDirectoryPath = string.Empty;
            if (requestType == ApplicationConstants.Scheme)
            {

                fileDirectoryPath = ResolveClientUrl(ConfigHelper.GetSchemeDocUploadPath()) + id.ToString() + "/Images/";
            }
            else if (requestType == ApplicationConstants.Block)
            {

                fileDirectoryPath = ResolveClientUrl(ConfigHelper.GetBlockDocUploadPath()) + id.ToString() + "/Images/";
            }
            return fileDirectoryPath + imageName;
        }
        #endregion

        #region check file type
        bool CheckFileType(string fileName)
        {
            string ext = Path.GetExtension(fileName);
            switch (ext.ToLower())
            {
                case ".gif":
                    return true;
                case ".jpg":
                    return true;
                case ".jpeg":
                    return true;
                case ".png":
                    return true;
                default:
                    return false;
            }
        }
        #endregion

    }
}