﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using PDR_BusinessObject.DropDown;
using PDR_BusinessLogic.SchemeBlock;
using PDR_BusinessObject.SchemeBlock;
using PDR_DataAccess.SchemeBlock;
using PDR_DataAccess.AssignToContractor;
using PDR_BusinessLogic.AssignToContractor;
using PDR_Utilities.Constants;
using PDR_Utilities.Helpers;
using PropertyDataRestructure.Base;
using PropertyDataRestructure.Interface;

namespace PropertyDataRestructure.UserControls.Pdr.SchemeBlock
{
    public partial class ProvisionItem : UserControlBase, IListingPage, IAddEditPage
    {
        public bool _readOnly = false;
        int id = 0;
        string requestType = string.Empty;
        ProvisionsBL objProvisionsBL = new ProvisionsBL(new ProvisionsRepo());
        AttributesBL objAttributeBL = new AttributesBL(new AttributesRepo());
        AssignToContractorBL objAssignToContractroBL = new AssignToContractorBL(new AssignToContractorRepo());
        protected void Page_Load(object sender, EventArgs e)
        {
            GetQueryStringParams();
            string PropPageName = Request.Path;
            if (PropPageName.Contains("SchemeDashBoard"))
            {
                _readOnly = false;
            }
            else if (PropPageName.Contains("SchemeRecord"))
            {
                _readOnly = true;
            }
            if (_readOnly == true)
            {
                pnlEditableProvisionItem.Enabled = false;
            }
            if (!Page.IsPostBack)
            {
                //ScriptManager.RegisterClientScriptBlock(this.Page, typeof(UpdatePanel), Guid.NewGuid().ToString(), "loadSelect2();", true);
                if (requestType == ApplicationConstants.Scheme)
                {
                    int schemeId = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);
                    populateBlockDropdown(schemeId);
                    populateSelectedProperties(schemeId, Convert.ToInt32(txtBlockPC.SelectedValue));
                }
                else if (requestType == ApplicationConstants.Block)
                {
                    int blockId = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);
                    PopulateBlockDropdownByBlockId(blockId);
                    populateSelectedProperties(0, blockId);
                }

                if (requestType == ApplicationConstants.Block)
                {
                    blockSpan.Visible = false;
                    txtBlockPC.Visible = false;
                }

                populateSupplierDropdown();
                this.LoadCycleTypeDDL();
                loadMenu();
                defaultControls();

            }

            //loadMenu();
            //defaultControls();

        }

        public void loadMenu()
        {
            DataSet provisionCategoriesDS = objProvisionsBL.GetProvisionCategories();
            //DataSet provisionCategoriesDS = objSession.ProvisionListDS;
            txtProvisionCategory.DataSource = provisionCategoriesDS;
            txtProvisionCategory.DataValueField = "ProvisionCategoryId";
            txtProvisionCategory.DataTextField = "CategoryName";
            txtProvisionCategory.DataBind();
            txtProvisionCategory.Items.Insert(0, new ListItem("Please Select", "-1"));
            txtProvisionCategory.SelectedIndex = 0;
        }

        public void defaultControls()
        {
            Type.Visible = true;
            Quantity.Visible = true;
            Location.Visible = true;
            Supplier.Visible = true;
            Model.Visible = true;
            Warranty.Visible = true;
            Stock.Visible = false;
            Serial.Visible = false;
            Gas.Visible = false;
            Sluice.Visible = false;
            DuctServicingDiv.Visible = false;
            Warranty.Visible = false;
            Owned.Visible = false;
            ExpiryDate.Visible = false;
            LeaveCost.Visible = false;

            ServicingRequired.Visible = true;

            ServicingProvider.Visible = true;
            LastServicingDate.Visible = true;
            ServicingCycle.Visible = true;
            NextServicingDate.Visible = true;
            ServicingPanel.Visible = true;
            ProvisionChargeTable.Visible = true;
            PAT.Visible = false;
        }

        protected void CategoryChanged(object sender, EventArgs e)
        {
            defaultControls();
            DropDownList list = sender as DropDownList;
            string selectedValue = list.SelectedValue.ToLower();
            FieldsTable.Visible = true;
            ProvisionChargeTable.Visible = true;
            this.changeInputs(selectedValue);
        }

        private void ResetFieldValidation()
        {
            rqIconSerialNumber.Visible = false;
            reqIconInstalledCost.Visible = true;
            reqIconAnnualLease.Visible = true;
            lblDescription.Text = "Description:";
            rqfvAnnualLease.Visible = true;
            rqfvAnnualLease.Enabled = true;

            rqfvInstalledCost.Visible = true;
            rqfvInstalledCost.Enabled = true;

            rqfvSerialNumber.Visible = false;
            rqfvSerialNumber.Enabled = false;
        }

        protected void changeInputs(string selectedValue)
        {
            ResetFieldValidation();
            switch (selectedValue)
            {
                case "1":
                    Serial.Visible = false;
                    Stock.Visible = true;
                    ServicingProvider.Visible = true;
                    ServicingRequired.Visible = true;
                    LastServicingDate.Visible = true;
                    ServicingCycle.Visible = true;
                    NextServicingDate.Visible = true;
                    ServicingPanel.Visible = true;
                    break;
                case "2":
                    ServicingProvider.Visible = true;
                    ServicingRequired.Visible = true;
                    LastServicingDate.Visible = true;
                    ServicingCycle.Visible = true;
                    NextServicingDate.Visible = true;
                    ServicingPanel.Visible = true;
                    break;
                case "3":
                    DuctServicingDiv.Visible = true;
                    Serial.Visible = true;
                    Stock.Visible = true;
                    Sluice.Visible = true;
                    Warranty.Visible = true;
                    Owned.Visible = true;
                    ExpiryDate.Visible = true;
                    LeaveCost.Visible = true;

                    rqfvAnnualLease.Visible = false;
                    rqfvAnnualLease.Enabled = false;

                    //rqfvInstalledCost.IsValid = false;
                    rqfvInstalledCost.Visible = false;
                    rqfvInstalledCost.Enabled = false;

                    lblDescription.Text = "Machine Type:";
                    reqIconInstalledCost.Visible = false;
                    reqIconAnnualLease.Visible = false;

                    rqfvSerialNumber.Visible = true;
                    rqfvSerialNumber.Enabled = true;
                    rqIconSerialNumber.Visible = true;
                    break;
                case "4":
                    PAT.Visible = true;
                    Serial.Visible = true;
                    Stock.Visible = true;
                    Gas.Visible = true;
                    rqfvSerialNumber.Visible = true;
                    rqfvSerialNumber.Enabled = true;
                    rqIconSerialNumber.Visible = true;
                    break;
                case "5":
                    Type.Visible = false;
                    Stock.Visible = true;
                    ServicingProvider.Visible = true;
                    ServicingRequired.Visible = true;
                    LastServicingDate.Visible = true;
                    ServicingCycle.Visible = true;
                    NextServicingDate.Visible = true;
                    ServicingPanel.Visible = true;
                    break;
                case "6":
                    ServicingProvider.Visible = true;
                    ServicingRequired.Visible = true;
                    LastServicingDate.Visible = true;
                    ServicingCycle.Visible = true;
                    NextServicingDate.Visible = true;
                    ServicingPanel.Visible = true;
                    break;
                case "7":
                    Type.Visible = false;
                    Serial.Visible = true;
                    Stock.Visible = true;
                    rqfvSerialNumber.Visible = true;
                    rqfvSerialNumber.Enabled = true;
                    rqIconSerialNumber.Visible = true;
                    break;
                case "8":
                    Type.Visible = false;
                    Stock.Visible = true;
                    PAT.Visible = true;
                    break;
                case "9":
                    Quantity.Visible = false;
                    Model.Visible = false;
                    Serial.Visible = true;
                    Warranty.Visible = true;
                    rqfvSerialNumber.Visible = true;
                    rqfvSerialNumber.Enabled = true;
                    rqIconSerialNumber.Visible = true;
                    break;
                case "10":
                    Quantity.Visible = false;
                    Model.Visible = false;
                    Warranty.Visible = true;
                    ServicingProvider.Visible = true;
                    ServicingRequired.Visible = true;
                    LastServicingDate.Visible = true;
                    ServicingCycle.Visible = true;
                    NextServicingDate.Visible = true;
                    ServicingPanel.Visible = true;
                    break;
                case "11":
                    Location.Visible = false;
                    Model.Visible = false;
                    Warranty.Visible = true;
                    break;
                case "12":
                    Quantity.Visible = false;
                    Model.Visible = false;
                    Warranty.Visible = true;
                    break;
                case "13":
                    Quantity.Visible = false;
                    Warranty.Visible = true;
                    Owned.Visible = true;
                    ExpiryDate.Visible = true;
                    LeaveCost.Visible = true;

                    //rqfvAnnualLease.IsValid = false;
                    rqfvAnnualLease.Visible = false;
                    rqfvAnnualLease.Enabled = false;

                    //rqfvInstalledCost.IsValid = false;
                    rqfvInstalledCost.Visible = false;
                    rqfvInstalledCost.Enabled = false;

                    reqIconInstalledCost.Visible = false;
                    reqIconAnnualLease.Visible = false;
                    break;
                case "14":
                    Location.Visible = false;
                    ProvisionChargeTable.Visible = false;
                    break;
                default:
                    break;
            }
        }

        public void populateData()
        {
            DataSet provisionDS = objSession.ProvisionDataSet;  //Get the saved data set from session
            if (provisionDS != null && provisionDS.Tables[0].Rows.Count > 0)
            {
                int provisionId = Convert.ToInt32(objSession.TreeItemId);//Get the Provision Item Id
                DataRow provisionItem = provisionDS.Tables[0].AsEnumerable().Where(p => ((int)p["ProvisionId"]) == provisionId).FirstOrDefault();
                if (provisionItem != null)
                {
                    this.txtProvisionCategory.Text = provisionItem["ProvisionCategoryId"] != DBNull.Value ? provisionItem["ProvisionCategoryId"].ToString() : string.Empty;
                    this.defaultControls();
                    FieldsTable.Visible = true;
                    this.changeInputs(this.txtProvisionCategory.Text);
                    this.txtDescription.Text = provisionItem["ProvisionDescription"] != DBNull.Value ? provisionItem["ProvisionDescription"].ToString() : string.Empty;

                    this.txtType.Text = provisionItem["ProvisionType"] != DBNull.Value ? provisionItem["ProvisionType"].ToString() : string.Empty;

                    this.txtQuantity.Text = provisionItem["Quantity"] != DBNull.Value ? provisionItem["Quantity"].ToString() : string.Empty;
                    this.txtLocation.Text = provisionItem["ProvisionLocation"] != DBNull.Value ? provisionItem["ProvisionLocation"].ToString() : string.Empty;
                    this.txtModel.Text = provisionItem["Model"] != DBNull.Value ? provisionItem["Model"].ToString() : string.Empty;
                    if (provisionItem["SupplierId"] != DBNull.Value)
                    {
                        this.txtSupplier.SelectedValue = provisionItem["SupplierId"] != DBNull.Value ? provisionItem["SupplierId"].ToString() : string.Empty;
                    }

                    this.txtSerialNumber.Text = provisionItem["SerialNumber"] != DBNull.Value ? provisionItem["SerialNumber"].ToString() : string.Empty;
                    this.txtStockRef.Text = provisionItem["BHG"] != DBNull.Value ? provisionItem["BHG"].ToString() : string.Empty;

                    this.txtConditionRating.Text = provisionItem["ConditionRating"] != DBNull.Value ? provisionItem["ConditionRating"].ToString() : string.Empty;
                    this.txtInstalledDate.Text = provisionItem["PurchasedDate"] != DBNull.Value ? Convert.ToDateTime(provisionItem["PurchasedDate"].ToString()).Date.ToString("d") : string.Empty;
                    this.txtInstalledCost.Text = provisionItem["PurchasedCost"] != DBNull.Value ? provisionItem["PurchasedCost"].ToString() : string.Empty;
                    this.txtReplacementDue.Text = provisionItem["ReplacementDue"] != DBNull.Value ? Convert.ToDateTime(provisionItem["ReplacementDue"].ToString()).Date.ToString("d") : string.Empty;
                    this.txtLastReplaced.Text = provisionItem["LastReplaced"] != DBNull.Value ? Convert.ToDateTime(provisionItem["LastReplaced"].ToString()).Date.ToString("d") : string.Empty;
                    this.txtLifeSpan.Text = provisionItem["LifeSpan"] != DBNull.Value ? provisionItem["LifeSpan"].ToString() : string.Empty;
                    this.txtLeaseExpiryDate.Text = provisionItem["LeaseExpiryDate"] != DBNull.Value ? Convert.ToDateTime(provisionItem["LeaseExpiryDate"].ToString()).Date.ToString("d") : string.Empty;
                    this.txtAnnualLease.Text = provisionItem["LeaseCost"] != DBNull.Value ? provisionItem["LeaseCost"].ToString() : string.Empty;
                    if (provisionItem["Warranty"] != DBNull.Value)
                    {
                        this.txtWarranty.SelectedValue = (Convert.ToBoolean(provisionItem["Warranty"]) == true ? "True" : "False");
                    }
                    if (provisionItem["Sluice"] != DBNull.Value)
                    {
                        this.txtSluice.SelectedValue = (Convert.ToBoolean(provisionItem["Sluice"]) == true ? "True" : "False");
                    }
                    if (provisionItem["PATTesting"] != DBNull.Value)
                    {
                        this.txtPatTesting.SelectedValue = (Convert.ToBoolean(provisionItem["PATTesting"]) == true ? "True" : "False");
                    }
                    if (provisionItem["GasAppliance"] != DBNull.Value)
                    {
                        this.txtGasAppliance.SelectedValue = (Convert.ToBoolean(provisionItem["GasAppliance"]) == true ? "True" : "False");
                    }

                    if (provisionItem["ProvisionCharge"] != DBNull.Value)
                    {
                        this.txtProvisionCharge.SelectedValue = (Convert.ToBoolean(provisionItem["ProvisionCharge"]) == true ? "True" : "False");
                        int schemeId = 0;
                        int blockId = 0;
                        if (requestType == ApplicationConstants.Scheme)
                        {
                            schemeId = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);
                            blockId = Convert.ToInt32(provisionItem["BlockPC"].ToString());
                            this.txtBlockPC.SelectedValue = provisionItem["BlockPC"] != DBNull.Value ? provisionItem["BlockPC"].ToString() : string.Empty;
                        }
                        else if (requestType == ApplicationConstants.Block)
                        {
                            blockId = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);
                            schemeId = GetSchemeByBlockId(id);
                            this.txtBlockPC.SelectedValue = blockId.ToString();
                        }
                        objSession.BlockId = blockId;
                        this.populateSelectedProperties(schemeId, blockId);
                    }
                    if (provisionItem["DuctServicing"] != DBNull.Value)
                    {
                        this.DuctServicing.SelectedValue = (Convert.ToBoolean(provisionItem["DuctServicing"]) == true ? "True" : "False");
                    }

                    if (provisionItem["ServicingRequired"] != DBNull.Value)
                    {
                        this.txtServicingRequired.SelectedValue = (Convert.ToBoolean(provisionItem["ServicingRequired"]) == true ? "True" : "False");
                    }
                    if (provisionItem["ServicingProviderId"] != DBNull.Value)
                    {
                        this.txtServicingProvider.SelectedValue = provisionItem["ServicingProviderId"] != DBNull.Value ? provisionItem["ServicingProviderId"].ToString() : string.Empty;
                    }
                    this.txtLastServicingDate.Text = provisionItem["lastDuctServicingDate"] != DBNull.Value ? Convert.ToDateTime(provisionItem["lastDuctServicingDate"].ToString()).Date.ToString("d") : string.Empty;
                    this.txtNextServiceDate.Text = provisionItem["nextDuctServicingDate"] != DBNull.Value ? Convert.ToDateTime(provisionItem["nextDuctServicingDate"].ToString()).Date.ToString("d") : string.Empty;
                    this.txtServiceCycle.Text = provisionItem["ductServiceCycle"] != DBNull.Value ? provisionItem["ductServiceCycle"].ToString() : string.Empty;

                    if (provisionItem["AnnualApportionmentPC"] != DBNull.Value)
                    {
                        this.txtAnnualAppointment.Text = provisionItem["AnnualApportionmentPC"] != DBNull.Value ? Convert.ToDecimal(provisionItem["AnnualApportionmentPC"].ToString()).ToString() : string.Empty;
                    }
                }
            }
        }
        public void resetControls()
        {

            //this.txtProvisionCategory.Text = string.Empty;
            //this.txtDescription.Text = string.Empty;
            //this.txtModel.Text = string.Empty;
            //this.txtAnnualLease.Text = string.Empty;
            //this.txtLeaseExpiryDate.Text = string.Empty;
            //this.txtConditionRating.Text = string.Empty;
            //this.txtType.Text = string.Empty;
            //this.txtQuantity.Text = string.Empty;
            //this.txtLocation.Text = string.Empty;
            //this.txtSluice.Text = string.Empty;
            //this.txtWarranty.Text = string.Empty;
            //this.txtOwned.Text = string.Empty;
            //this.txtSerialNumber.Text = string.Empty;
            //this.txtSupplier.Text = string.Empty;
            //this.txtInstalledDate.Text = string.Empty;
            //this.txtInstalledCost.Text = string.Empty;
            //this.txtLastReplaced.Text = string.Empty;
            //this.txtReplacementDue.Text = string.Empty;
            //this.txtLifeSpan.Text = string.Empty;
            //this.txtAnnualAppointment.Text = string.Empty;
            //this.txtNextServiceDate.Text = string.Empty;
            // this.txtLastServicingDate.Text = string.Empty;
            //this.txtProvisionCharge.Text = string.Empty;
            //this.txtServicingProvider.Text = string.Empty;
            // this.txtServicingRequired.Text = string.Empty;
            //this.txtServiceCycle.Text = string.Empty;            

            this.ClearInputs(Page.Controls);
            FieldsTable.Visible = false;
            ProvisionChargeTable.Visible = false;

        }
        private void ClearInputs(ControlCollection ctrls)
        {
            foreach (Control ctrl in ctrls)
            {
                if (ctrl is TextBox)
                    ((TextBox)ctrl).Text = string.Empty;
                else if (ctrl is DropDownList)
                    ((DropDownList)ctrl).ClearSelection();

                ClearInputs(ctrl.Controls);
            }
        }
        public void loadData()
        {
            throw new NotImplementedException();
        }
        public void searchData()
        {
            throw new NotImplementedException();
        }
        public void printData()
        {
            throw new NotImplementedException();
        }
        public void exportGridToExcel()
        {
            throw new NotImplementedException();
        }
        public void exportToPdf()
        {
            throw new NotImplementedException();
        }
        public void downloadData()
        {
            throw new NotImplementedException();
        }
        public void applyFilters()
        {
            throw new NotImplementedException();
        }

        public void saveData()
        {
            throw new NotImplementedException();
        }
        public bool validateData()
        {
            throw new NotImplementedException();
        }
        public void populateDropDown(DropDownList ddl)
        {
            throw new NotImplementedException();
        }
        public void populateDropDowns()
        {
            throw new NotImplementedException();
        }
        private void LoadCycleTypeDDL()
        {
            DataSet cycleTypeDS = objProvisionsBL.LoadCycleTypeDDL();
            //this.ddlLifeSpan.Items.Clear();
            //this.ddlLifeSpan.DataSource = cycleTypeDS;
            //this.ddlLifeSpan.DataValueField = "CycleTypeId";
            //this.ddlLifeSpan.DataTextField = "CycleType";
            //this.ddlLifeSpan.DataBind();
        }
        public string FillProvisionItemDetailObject(ref ProvisionsBO objProvisionBO, ref ProvisionChargeProperties pcProperties)
        {
            bool valid = false;
            string message = string.Empty;
            objProvisionBO.ProvisionCategoryId = Int32.Parse(this.txtProvisionCategory.Text);
            objProvisionBO.Description = this.txtDescription.Text;
            objProvisionBO.Type = Convert.ToInt32(this.txtType.SelectedValue);
            if (!string.IsNullOrEmpty(this.txtQuantity.Text))
            {
                objProvisionBO.Quantity = Convert.ToInt32(this.txtQuantity.Text);
            }

            objProvisionBO.Location = this.txtLocation.Text;
            objProvisionBO.Model = this.txtModel.Text;
            if (!string.IsNullOrEmpty(this.txtSupplier.Text))
            {
                objProvisionBO.SupplierId = Convert.ToInt32(this.txtSupplier.Text);
            }

            objProvisionBO.SerialNumber = this.txtSerialNumber.Text;
            objProvisionBO.BHG = this.txtStockRef.Text;
            objProvisionBO.GasAppliance = this.txtGasAppliance.Text;
            if (!string.IsNullOrEmpty(this.txtSluice.Text))
            {
                objProvisionBO.Sluice = bool.Parse(System.Convert.ToString(this.txtSluice.SelectedValue));
            }
            if (!string.IsNullOrEmpty(this.DuctServicing.Text))
            {
                objProvisionBO.DuctServicing = bool.Parse(System.Convert.ToString(this.DuctServicing.SelectedValue));
            }
            if (!string.IsNullOrEmpty(this.txtWarranty.Text))
            {
                objProvisionBO.Warranty = bool.Parse(System.Convert.ToString(this.txtWarranty.SelectedValue));
            }
            if (!string.IsNullOrEmpty(this.txtOwned.Text))
            {
                objProvisionBO.Owned = bool.Parse(System.Convert.ToString(this.txtOwned.SelectedValue));
            }
            //objProvisionBO.Warranty = bool.Parse(System.Convert.ToString(this.txtWarranty.SelectedValue));
            //objProvisionBO.Owned = bool.Parse(System.Convert.ToString(this.txtOwned.SelectedValue));
            objProvisionBO.Condition = Convert.ToInt32(this.txtConditionRating.Text);

            valid = ValidationHelper.validateDateFormate(this.txtInstalledDate.Text);
            if (valid)
                objProvisionBO.PurchasedDate = Convert.ToDateTime(this.txtInstalledDate.Text);
            else
            {
                message = UserMessageConstants.NotValidInstalledDateFormat + " here it is " + this.txtInstalledDate.Text;
                return message;
            }

            valid = ValidationHelper.isAmountTwoDec(this.txtInstalledCost.Text);
            if (valid)
                objProvisionBO.PurchasedCost = Convert.ToDecimal(this.txtInstalledCost.Text);
            else
            {
                if (objProvisionBO.ProvisionCategoryId == 3 || objProvisionBO.ProvisionCategoryId == 13)
                {
                    if (string.IsNullOrEmpty(txtInstalledCost.Text))
                    {
                        objProvisionBO.PurchasedCost = 0.0m;
                    }
                    else
                    {
                        message = UserMessageConstants.NotValidPurchasedCostFormat;
                    }
                }
                else
                {
                    message = UserMessageConstants.NotValidPurchasedCostFormat;
                }
            }


            if (!string.IsNullOrEmpty(this.txtLastReplaced.Text))
            {
                valid = ValidationHelper.validateDateFormate(this.txtLastReplaced.Text);
                if (valid)
                    objProvisionBO.LastReplaced = Convert.ToDateTime(this.txtLastReplaced.Text);
                else
                {
                    message = UserMessageConstants.NotValidLastReplacedDateDateFormat;
                    return message;
                }
            }

            if (!string.IsNullOrEmpty(this.txtReplacementDue.Text))
            {
                valid = ValidationHelper.validateDateFormate(this.txtReplacementDue.Text);
                if (valid)
                    objProvisionBO.ReplacementDue = Convert.ToDateTime(this.txtReplacementDue.Text);
                else
                {
                    message = UserMessageConstants.NotValidReplacementDueDateFormat;
                    return message;
                }
            }

            if (!string.IsNullOrEmpty(this.txtLeaseExpiryDate.Text))
            {
                valid = ValidationHelper.validateDateFormate(this.txtLeaseExpiryDate.Text);
                if (valid)
                    objProvisionBO.LeaseExpiryDate = Convert.ToDateTime(this.txtLeaseExpiryDate.Text);
                else
                {
                    message = UserMessageConstants.NotValidInstalledDateFormat + " hi " + this.txtLeaseExpiryDate.Text;
                    return message;
                }
            }

            if (!string.IsNullOrEmpty(this.txtLastServicingDate.Text))
            {
                valid = ValidationHelper.validateDateFormate(this.txtLastServicingDate.Text);
                if (valid)
                    objProvisionBO.LastServicingDate = Convert.ToDateTime(this.txtLastServicingDate.Text);
                else
                {
                    message = UserMessageConstants.NotValidReplacementDueDateFormat;
                    return message;
                }
            }

            if (!string.IsNullOrEmpty(this.txtNextServiceDate.Text))
            {
                valid = ValidationHelper.validateDateFormate(this.txtLastServicingDate.Text);
                if (valid)
                    objProvisionBO.NextServiceDate = Convert.ToDateTime(this.txtNextServiceDate.Text);
                else
                {
                    message = UserMessageConstants.NotValidReplacementDueDateFormat;
                    return message;
                }
            }
            if (!string.IsNullOrEmpty(this.txtServicingRequired.Text))
            {
                objProvisionBO.ServicingRequired = bool.Parse(System.Convert.ToString(this.txtServicingRequired.SelectedValue));
            }
            if (!string.IsNullOrEmpty(this.txtServicingProvider.Text))
            {
                objProvisionBO.ServicingProviderId = Convert.ToInt32(this.txtServicingProvider.SelectedValue);
            }
            if (!string.IsNullOrEmpty(this.DuctServicing.Text))
            {
                objProvisionBO.DuctServicing = bool.Parse(System.Convert.ToString(this.DuctServicing.SelectedValue));
            }
            objProvisionBO.ServiceCycle = this.txtServiceCycle.Text;


            if (!string.IsNullOrEmpty(this.txtAnnualLease.Text))
            {
                objProvisionBO.LeaseCost = Convert.ToDecimal(this.txtAnnualLease.Text);
            }
            if (objProvisionBO.ProvisionCategoryId == 3 || objProvisionBO.ProvisionCategoryId == 13)
            {
                if (string.IsNullOrEmpty(this.txtAnnualLease.Text))
                {
                    objProvisionBO.LeaseCost = 0.0m;
                }
            }
            if (!string.IsNullOrEmpty(this.txtLifeSpan.Text))
            {
                objProvisionBO.LifeSpan = Convert.ToInt32(this.txtLifeSpan.Text);
            }

            if (!string.IsNullOrEmpty(this.txtPatTesting.Text))
            {
                objProvisionBO.PATTesting = bool.Parse(System.Convert.ToString(this.txtPatTesting.SelectedValue));
            }

            if (!string.IsNullOrEmpty(this.txtProvisionCharge.Text))
            {
                objProvisionBO.ProvisionCharge = bool.Parse(System.Convert.ToString(this.txtProvisionCharge.SelectedValue));
                if (!string.IsNullOrEmpty(this.txtAnnualAppointment.Text))
                {
                    objProvisionBO.AnnualAppointmentPC = Convert.ToDecimal(this.txtAnnualAppointment.Text);
                }
                objProvisionBO.BlockPC = Convert.ToInt32(this.txtBlockPC.Text);
                int? schemeId = -1;
                int? blockId = -1;
                DataSet resultSet = new DataSet();

                pcProperties.excludedIncludedProperties = new List<ExInProperties>();

                if (requestType == ApplicationConstants.Scheme)
                {
                    schemeId = id;
                    blockId = Convert.ToInt32(txtBlockPC.SelectedValue);
                }
                else if (requestType == ApplicationConstants.Block)
                {
                    blockId = id;
                    schemeId = GetSchemeByBlockId(id);
                }

                pcProperties.SchemeId = schemeId;
                pcProperties.BlockId = blockId;
                resultSet = objAttributeBL.getPropertiesBySchemeBlock((int)schemeId, (int)blockId);
                if (blockId == 0)
                {
                    blockId = null;
                }
                foreach (var item in resultSet.Tables[0].Rows)
                {
                    var exProperties = new ExInProperties();
                    exProperties.PropertyId = (string)((DataRow)item)[0];
                    exProperties.IsIncluded = 0;
                    var indexes = txtPropertiesPC.GetSelectedIndices();
                    foreach (var index in txtPropertiesPC.GetSelectedIndices())
                    {
                        if (txtPropertiesPC.Items[index].Value == exProperties.PropertyId)
                        {
                            exProperties.IsIncluded = 1;
                            break;
                        }
                    }
                    pcProperties.excludedIncludedProperties.Add(exProperties);
                }
                //if (pcProperties.excludedIncludedProperties.Count <= 0)
                //{
                //    message = "error";
                //}
            }

            return message;
        }

        private int GetSchemeByBlockId(int blockId)
        {
            int schemeId = 0;
            AssignToContractorBL objAssignToContractorBL = new AssignToContractorBL(new AssignToContractorRepo());
            DataSet schemeDS = new DataSet();
            schemeDS = objAssignToContractorBL.GetSchemeByBlockId(blockId);

            if (schemeDS.Tables[0].Rows.Count > 0)
            {
                schemeId = int.Parse(schemeDS.Tables[0].Rows[0]["SchemeId"].ToString());
            }
            return schemeId;
        }

        #region update Next Replacement Due Date
        public void updateReplacementDue(object sender, EventArgs e)
        {
            try
            {
                DateTime answer = DateTime.Now;
                int ddlLifeSpanId = 1;
                //int ddlLifeSpanId = Convert.ToInt32(ddlLifeSpan.SelectedValue);
                if (txtLifeSpan.Text != "" && txtLastReplaced.Text != "")
                {
                    DateTime date = DateTime.ParseExact(txtLastReplaced.Text, "dd/MM/yyyy", null);
                    /*
                      switch (ddlLifeSpanId)
                    {
                        case 1:
                            answer = date.AddDays(Convert.ToInt32(txtLifeSpan.Text));
                            break;
                        case 2:
                            answer = date.AddDays(Convert.ToInt32(txtLifeSpan.Text) * 7);
                            break;
                        case 3:
                            answer = date.AddMonths(Convert.ToInt32(txtLifeSpan.Text));
                            break;
                        case 4:
                            answer = date.AddYears(Convert.ToInt32(txtLifeSpan.Text));
                            break;
                    }
                    */
                    answer = date.AddYears(Convert.ToInt32(txtLifeSpan.Text));
                    txtReplacementDue.Text = answer.ToShortDateString().ToString();
                }
                else
                    txtReplacementDue.Text = "";

                calculateAnnualApportionment();
            }
            catch (Exception ex)
            {
                txtReplacementDue.Text = "";
            }
            finally
            {
                CompareValidator1.Validate();
            }
        }

        #endregion

        #region "Text Installed Cost Text Changed"
        public void txtInstalledCost_TextChanged(object sender, EventArgs e)
        {
            calculateAnnualApportionment();
        }
        #endregion


        private void calculateAnnualApportionment()
        {
            double purchaseCost, lifeSpan, annualApportionment;
            if (double.TryParse(txtInstalledCost.Text, out purchaseCost)
                && double.TryParse(txtLifeSpan.Text, out lifeSpan))
            {
                if (lifeSpan > 0)
                {
                    annualApportionment = purchaseCost / lifeSpan;
                    txtAnnualAppointment.Text = annualApportionment.ToString("N2");
                }
                else
                {
                    txtAnnualAppointment.Text = string.Empty;
                }
            }
        }

        private void populateBlockDropdown(int schemeId)
        {
            DataSet resultData = new DataSet();
            resultData = objAttributeBL.getBlocksByScheme(schemeId);

            txtBlockPC.Items.Clear();
            txtBlockPC.DataSource = resultData;
            txtBlockPC.DataValueField = "BLOCKID";
            txtBlockPC.DataTextField = "BLOCKNAME";
            txtBlockPC.DataBind();
            //txtBlockPC.Items.Insert(0, new ListItem("Please Select", "-1"));
            txtBlockPC.SelectedIndex = 0;
        }

        private void PopulateBlockDropdownByBlockId(int blockId)
        {
            DataSet resultData = new DataSet();
            var tbl = new DataTable("dtBlock");
            tbl.Columns.Add("BLOCKID", typeof(Int32));
            tbl.Columns.Add("BLOCKNAME", typeof(string));
            tbl.Rows.Add(blockId, objSession.BlockName);
            resultData.Tables.Add(tbl);

            txtBlockPC.Items.Clear();
            txtBlockPC.DataSource = resultData;
            txtBlockPC.DataValueField = "BLOCKID";
            txtBlockPC.DataTextField = "BLOCKNAME";
            txtBlockPC.DataBind();
            txtBlockPC.SelectedIndex = 0;
        }

        private void populateSupplierDropdown()
        {
            DataSet resultData = new DataSet();
            List<DropDownBO> dropDownList = new List<DropDownBO>();
            objAssignToContractroBL.getAllContractors(ref dropDownList);
            bindDropDownList(ref txtSupplier, ref dropDownList);
            bindDropDownList(ref txtServicingProvider, ref dropDownList);

        }

        private void bindDropDownList(ref DropDownList ddlToBind, ref List<DropDownBO> dropDownItemsList, bool insertDefault = true)
        {
            ddlToBind.Items.Clear();
            ddlToBind.DataSource = dropDownItemsList;
            ddlToBind.DataTextField = ApplicationConstants.ddlDefaultDataTextField;
            ddlToBind.DataValueField = ApplicationConstants.ddlDefaultDataValueField;
            ddlToBind.DataBind();

            if (insertDefault)
            {
                ddlToBind.Items.Insert(0, new ListItem(ApplicationConstants.DropDwonDefaultText, ApplicationConstants.DropDownDefaultValue.ToString()));
            }
        }

        protected void block_changed(object sender, EventArgs e)
        {
            int schemeId = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);
            var blockId = Convert.ToInt32(txtBlockPC.SelectedValue);
            DataSet resultData = new DataSet();

            resultData = objAttributeBL.getPropertiesBySchemeBlock(schemeId, blockId);

            txtPropertiesPC.Items.Clear();
            txtPropertiesPC.DataSource = resultData;
            txtPropertiesPC.DataValueField = "PROPERTYID";
            txtPropertiesPC.DataTextField = "PROPERTYNAME";
            txtPropertiesPC.DataBind();
            int count = 0;
            for (count = 0; count < resultData.Tables[0].Rows.Count; count++)
            {
                txtPropertiesPC.Items.FindByValue(resultData.Tables[0].Rows[count][0].ToString()).Selected = true;
            }
            if (count == 0)
            {
                foreach (var item in txtPropertiesPC.Items)
                {
                    ((ListItem)item).Selected = true;
                }
            }
            //populateProvisionChargeProperties(schemeId, blockId);

        }

        #region "Populate Properties dropdown"

        private void populateSelectedProperties(int schemeId, int blockId)
        {
            DataSet resultData = new DataSet();
            resultData = objAttributeBL.getPropertiesBySchemeBlock(schemeId, blockId);
            txtPropertiesPC.Items.Clear();
            txtPropertiesPC.DataSource = resultData;
            txtPropertiesPC.Items.Clear();
            txtPropertiesPC.DataSource = resultData;
            txtPropertiesPC.DataValueField = "PROPERTYID";
            txtPropertiesPC.DataTextField = "PROPERTYNAME";
            txtPropertiesPC.DataBind();

            DataSet resultData1 = new DataSet();
            resultData1 = objProvisionsBL.getSavedProvisionChargeProperties(schemeId, blockId, objSession.TreeItemId);
            int count = 0;
            for (count = 0; count < resultData1.Tables[0].Rows.Count; count++)
            {
                txtPropertiesPC.Items.FindByValue(resultData1.Tables[0].Rows[count][0].ToString()).Selected = true;
            }
            if (count == 0)
            {
                foreach (var item in txtPropertiesPC.Items)
                {
                    ((ListItem)item).Selected = true;
                }
            }
            //ddlLookup.Items.Insert(0, New ListItem("Please Select", -1))

        }

        #endregion

        protected void ddlProperties_Load(object sender, EventArgs e)
        {
            //ScriptManager.RegisterClientScriptBlock(this.Page, typeof(UpdatePanel), Guid.NewGuid().ToString(), "loadSelect2();", true);
        }

        private void GetQueryStringParams()
        {
            if ((Request.QueryString[ApplicationConstants.Id] != null))
                id = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);
            if ((Request.QueryString[ApplicationConstants.RequestType] != null))
                requestType = Request.QueryString[ApplicationConstants.RequestType];

        }

    }
}