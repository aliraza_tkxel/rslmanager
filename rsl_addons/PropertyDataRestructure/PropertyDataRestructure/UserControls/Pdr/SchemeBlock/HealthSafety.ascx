﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HealthSafety.ascx.cs"
    Inherits="PropertyDataRestructure.UserControls.Pdr.SchemeBlock.HealthSafety" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<%@ Register TagPrefix="ucAbs" TagName="Asbestos" Src="~/UserControls/Pdr/SchemeBlock/Asbestos.ascx" %>
<%@ Register TagPrefix="ucRefurishment" TagName="Refurishment" Src="~/UserControls/Pdr/SchemeBlock/Refurbishment.ascx" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<asp:UpdatePanel runat="server" ID="updPnlHealthSafety" UpdateMode="Conditional">
    <ContentTemplate>
        <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="500px" />
        <!--Upload form starts here -->
        <div style="min-width: 1020px;">
            <div style="border-style: solid; border-color: black; border-width: 0px; margin-left: 2px;
                min-width: 300px; float: left; width: 96%;">
                <asp:LinkButton ID="lnkBtnAsbestosTab" OnClick="lnkBtnAsbestosTab_Click" CssClass="TabInitial"
                    runat="server" BorderStyle="Solid">Asbestos: </asp:LinkButton>
                <asp:LinkButton ID="lnkRefurbishmentTab" OnClick="lnkRefurbishmentTab_Click" CssClass="TabInitial"
                    runat="server" BorderStyle="Solid">Refurbishment: </asp:LinkButton>
                <br />
                <div class="pm-lbox" style="width: 100%;">
                    <asp:MultiView ID="MainSubView" runat="server">
                        <asp:View ID="SubView1" runat="server">
                            <ucAbs:Asbestos ID="Asbestos" runat="server" />
                        </asp:View>
                        <asp:View ID="SubView2" runat="server">
                            <ucRefurishment:Refurishment ID="Refurbishment" runat="server" />
                        </asp:View>
                    </asp:MultiView>
                </div>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
