﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_BusinessLogic.SchemeBlock;
using PDR_DataAccess.SchemeBlock;
using PDR_Utilities.Constants;
using PropertyDataRestructure.Base;
using PropertyDataRestructure.UserControls.Common;
using PropertyDataRestructure.Interface;

namespace PropertyDataRestructure.UserControls.Pdr.SchemeBlock
{

    public partial class Provisions : UserControlBase
    {
        ProvisionsBL objProvisionsBL = new ProvisionsBL(new ProvisionsRepo());

        int itemId = 0;
        string requestType = string.Empty;
        Boolean isReadOnly = false;
        String pageName = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            pageName = System.IO.Path.GetFileName(Request.Url.ToString());
            if (pageName.Contains(PathConstants.SchemeRecordPage))
            {
                isReadOnly = true;
            }
            else
            {
                isReadOnly = false;
            }
            try
            {
                uiMessage.hideMessage();
                ucProvisionDetail.OnChildEventOccurs += new EventHandler(ucProvisionDetail_OnChildEventOccurs);

                Button btnSave = (Button)ucProvisionDetail.FindControl("btnRaisePO");
                if (trVwProvisions.SelectedNode != null)
                {
                    btnSave.Style.Add("visibility",
                        trVwProvisions.SelectedNode.Text == "Add Provision" ? "hidden" : "visible");
                }

                if (!IsPostBack)
                {
                    int? schemeId = null, blockId = null;
                    this.GetQueryStringParams();
                    if (requestType == ApplicationConstants.Scheme)
                        schemeId = itemId;
                    else if (requestType == ApplicationConstants.Block)
                        blockId = itemId;

                    //DataSet provisionDS = objProvisionsBL.GetProvisionsData(schemeId, blockId);
                    ////DataSet provisionCategoriesDS = objProvisionsBL.GetProvisionCategories();
                    //objSession.ProvisionDataSet = provisionDS; //Save Dataset in session to load selected item data
                    ////objSession.ProvisionListDS = provisionCategoriesDS; //save Dataset in session to load ProvisionCategoriesList
                    //this.PopulateTreeNode(provisionDS);
                    //trVwProvisions.Nodes[0].Expanded = true;
                }
                else
                {
                    if (objSession.ProvisionDataSet != null)
                    {
                        if (trVwProvisions.Nodes.Count <= 0)
                            this.PopulateTreeNode(objSession.ProvisionDataSet);
                    }

                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        protected void ucProvisionDetail_OnChildEventOccurs(object sender, EventArgs e)
        {
            this.trVwProvisions.Nodes.Clear();
            this.trVwProvisions.Nodes.Add(new TreeNode("Menu"));
            this.PopulateTreeNode((DataSet)sender);
            lblBreadCrump.Text = string.Empty;

            if (trVwProvisions.SelectedNode != null)
            {
                if (trVwProvisions.SelectedNode.Depth >= 1)
                {

                    if (trVwProvisions.SelectedNode.Depth >= 2)
                        lblBreadCrump.Text = trVwProvisions.SelectedNode.Parent.Parent.Text + " > <b>" + trVwProvisions.SelectedNode.Parent.Text + " > <b>" + trVwProvisions.SelectedNode.Text + "</b>";
                    else
                        lblBreadCrump.Text = trVwProvisions.SelectedNode.Parent.Text + " > <b>" + trVwProvisions.SelectedNode.Text + "</b>";

                }
            }
        }

        #region "TreeView"
        protected void trVwProvisions_SelectedNodeChanged(object sender, EventArgs e)
        {
            try
            {
                lblBreadCrump.Text = string.Empty;
                int depth = trVwProvisions.SelectedNode.Depth;
                MultiView MainView = (MultiView)ucProvisionDetail.FindControl("MainView");
                LinkButton lnkBtnDetailTab = (LinkButton)ucProvisionDetail.FindControl("lnkBtnDetailTab");
                ucProvisionDetail.SetSelectedLinkItemStyle(ref lnkBtnDetailTab);

                Button btnSave = (Button)ucProvisionDetail.FindControl("btnSave");
                Button btnAmend = (Button)ucProvisionDetail.FindControl("btnAmend");
                UserControl ucProvisionItemDetail = (UserControl)ucProvisionDetail.FindControl("ucItemDetail");

                UserControl ucProvisionItem = (UserControl)ucProvisionItemDetail.FindControl("ucProvisionItem");
                ProvisionItem objProvisionItem = (ProvisionItem)ucProvisionItem;

                UserControl ucMSAT = (UserControl)ucProvisionItemDetail.FindControl("ucMSAT");
                MaintenanceServicingAndTesting objMST = (MaintenanceServicingAndTesting)ucMSAT;

                if (trVwProvisions.SelectedNode != null)
                {
                    if (trVwProvisions.SelectedNode.Depth >= 1)
                    {
                        if (trVwProvisions.SelectedNode.Depth >= 2)
                            lblBreadCrump.Text = trVwProvisions.SelectedNode.Parent.Parent.Text + " > <b>" + trVwProvisions.SelectedNode.Parent.Text + " > <b>" + trVwProvisions.SelectedNode.Text + "</b>";
                        else
                            lblBreadCrump.Text = trVwProvisions.SelectedNode.Parent.Text + " > <b>" + trVwProvisions.SelectedNode.Text + "</b>";

                        MainView.ActiveViewIndex = 0;
                        ucProvisionDetail.Visible = true;

                        objSession.TreeItemId = Convert.ToInt32(trVwProvisions.SelectedNode.Value); //Save Selected Node Value&Text in session for later Save Data Function
                        objSession.TreeItemName = trVwProvisions.SelectedNode.Text;
                        /*
                        if (trVwProvisions.SelectedNode.Parent != null && !trVwProvisions.SelectedNode.Parent.Value.ToLower().Equals("menu"))
                            objSession.TreeItemParentId = Convert.ToInt32(trVwProvisions.SelectedNode.Parent.Value);
                        else
                            objSession.TreeItemParentId = null;
                        */
                        if (Convert.ToInt32(trVwProvisions.SelectedNode.Value) == -1 || Convert.ToInt32(trVwProvisions.SelectedNode.Value) == -2)
                        {
                            objProvisionItem.resetControls();
                            objMST.resetControls();
                            objMST.PopulateProvisionMSTData();
                            if (isReadOnly == true)
                            {
                                btnSave.Visible = false;
                            }
                            else
                            {
                                btnSave.Visible = true;
                            }
                            btnAmend.Visible = false;
                        }
                        else
                        {
                            DataSet provisionDS = objProvisionsBL.GetProvisionsDataById(Convert.ToInt32(trVwProvisions.SelectedNode.Value));
                            objSession.ProvisionDataSet = provisionDS; //Save Dataset in session to load selected item data
                            objProvisionItem.populateData();
                            objMST.resetControls();
                            objMST.PopulateProvisionMSTData();
                            btnAmend.Text = "Amend";
                            if (isReadOnly == true)
                            {
                                btnAmend.Visible = false;
                            }
                            else
                            {
                                btnAmend.Visible = true;
                            }
                            btnSave.Visible = false;
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        public void PopulateTreeNode(DataSet provisionDS)
        {
            DataSet provisionCategoriesDS = objProvisionsBL.GetProvisionCategories();
            var s = provisionDS.Tables[0].AsEnumerable().GroupBy(r => r["ProvisionCategoryId"].ToString());

            //var provisionDS1 = from dsRow in provisionDS.Tables[0].AsEnumerable() group dsRow by dsRow["ProvisionCategoryId"];
            foreach (var dsRow in s)
            {
                TreeNode node;
                node = new TreeNode();
                node.Text = GetProvisionCategoryName(provisionCategoriesDS, Convert.ToInt32(dsRow.Key));
                node.Value = dsRow.Key;
                node.SelectAction = TreeNodeSelectAction.Expand;
                this.trVwProvisions.Nodes[0].ChildNodes.Add(node);
                foreach (DataRow item in dsRow.ToList())
                {
                    TreeNode newNode = new TreeNode();
                    newNode.Text = item["ProvisionDescription"].ToString();
                    newNode.Value = item["ProvisionId"].ToString();
                    newNode.SelectAction = TreeNodeSelectAction.Select;
                    node.ChildNodes.Add(newNode);
                    if (newNode.Value == objSession.TreeItemId.ToString())
                    {
                        newNode.Select();
                        newNode.Expand();
                        newNode.Parent.Expand();
                    }
                }
            }
            trVwProvisions.Nodes[0].Expand();
            //node.SelectAction = TreeNodeSelectAction.SelectExpand;
            //this.trVwProvisions.Nodes[0].ChildNodes.Add(node);
            /*if (node.Value == objSession.TreeItemId.ToString())
            {
                node.Select();
                node.Expand();

            }*/

            //foreach (TreeNode childnode in GetChildNode(Convert.ToInt32(dsRow["ProvisionId"]), provisionDS))
            //{
            //    if (childnode.Value == objSession.TreeItemId.ToString())
            //    {
            //        childnode.Select();
            //        childnode.Expand();
            //    }

            //    node.ChildNodes.Add(childnode);
            //}
            //}
            if (!isReadOnly)
            {
                TreeNode node;
                node = new TreeNode();
                node.Text = "Add Provision";
                node.Value = "-1";
                node.SelectAction = TreeNodeSelectAction.Select;
                this.trVwProvisions.Nodes[0].ChildNodes.Add(node);
                Button btnSave = (Button)ucProvisionDetail.FindControl("btnRaisePO");
                Button btnAmend = (Button)ucProvisionDetail.FindControl("btnAmend");
                if (btnAmend.Visible)
                    btnSave.Visible = btnAmend.Visible;
                else
                {
                    btnSave.Style.Add("visibility", "hidden");
                }
                //this.trVwProvisions.Nodes[0].Expand();
            }
        }

        private string GetProvisionCategoryName(DataSet provisionCategoriesDS, int provisionId)
        {
            foreach (DataRow dr in provisionCategoriesDS.Tables[0].Rows)
            {
                if (provisionId == Convert.ToInt32(dr["ProvisionCategoryId"].ToString()))
                {
                    return dr["CategoryName"].ToString();
                }
            }
            return "Not Found";
        }

        /// <summary>
        /// Recursively display children nodes
        /// </summary>
        /// <param name="provisionParentId"></param>
        /// <param name="provisionDS"></param>
        /// <returns></returns>
        public TreeNodeCollection GetChildNode(int provisionParentId, DataSet provisionDS)
        {
            TreeNodeCollection childtreenodes = new TreeNodeCollection();
            DataView provisionView = provisionDS.Tables[0].DefaultView;
            provisionView.RowFilter = "ProvisionParentId  = " + provisionParentId.ToString();

            if (provisionView.Count > 0)
            {
                foreach (DataRow dataRow in provisionView.ToTable().Rows)
                {
                    TreeNode childNode = new TreeNode();
                    childNode.Text = dataRow["ProvisionDescription"].ToString();
                    childNode.Value = dataRow["ProvisionId"].ToString();
                    childNode.SelectAction = TreeNodeSelectAction.SelectExpand;
                    childtreenodes.Add(childNode);
                    if (childNode.Value == objSession.TreeItemId.ToString())
                    {
                        childNode.Select();
                        childNode.Expand();
                    }
                    foreach (TreeNode childnode in GetChildNode(Convert.ToInt32(dataRow["ProvisionId"]), provisionDS))
                    {
                        childNode.ChildNodes.Add(childnode);
                        if (childnode.Value == objSession.TreeItemId.ToString())
                        {
                            childnode.Select();
                            childnode.Expand();
                        }
                    }

                }
            }
            return childtreenodes;
        }
        public void resetTreeView()
        {
            this.trVwProvisions.Nodes[0].ChildNodes.Clear();
            ucProvisionDetail.Visible = false;
        }
        #endregion

        #region "Helper Functions"
        private void GetQueryStringParams()
        {
            if ((Request.QueryString[ApplicationConstants.Id] != null))
                itemId = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);

            if ((Request.QueryString[ApplicationConstants.RequestType] != null))
                requestType = Request.QueryString[ApplicationConstants.RequestType];

        }
        #endregion

        public void DisableMEServicing()
        {
            UserControl ucProvisionItemDetail = (UserControl)ucProvisionDetail.FindControl("ucItemDetail");
            UserControl ucMSAT = (UserControl)ucProvisionItemDetail.FindControl("ucMSAT");
            ucMSAT.FindControl("tblMEServicing").Visible = false;
        }
    }
}