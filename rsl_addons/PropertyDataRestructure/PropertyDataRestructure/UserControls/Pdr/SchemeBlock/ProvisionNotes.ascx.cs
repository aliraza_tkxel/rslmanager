﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_BusinessLogic.SchemeBlock;
using PDR_BusinessObject.SchemeBlock;
using PDR_DataAccess.SchemeBlock;
using PDR_Utilities.Constants;
using PropertyDataRestructure.Base;
using PropertyDataRestructure.Interface;

namespace PropertyDataRestructure.UserControls.Pdr.SchemeBlock
{
    public partial class ProvisionNotes : UserControlBase, IListingPage, IAddEditPage
    {
        int id = 0;
        string requestType = string.Empty;
        ProvisionsBL objProvisionBL = new ProvisionsBL(new ProvisionsRepo());
        Boolean isReadOnly = false;
        String pageName = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            pageName = System.IO.Path.GetFileName(Request.Url.ToString());
            if (pageName.Contains(PathConstants.SchemeRecordPage))
            {
                isReadOnly = true;
            }
            else
            {
                isReadOnly = false;
            }
            if (isReadOnly == true)
            {
                btnAddNotes.Visible = false;
                btnSave.Visible = false;
            }
            try
            {
                uiMessage.hideMessage();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        protected void btnAddNotes_Click(object sender, EventArgs e)
        {
            try
            {
                uiMessageNotes.hideMessage();
                txtNote.Text = string.Empty;
                mdlPopUpAddNote.Show();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (validateData())
                {
                    saveData();
                    loadData();
                    txtNote.Text = string.Empty;
                    uiMessage.messageText = UserMessageConstants.itemAddedSuccessfuly;
                }
                else
                {
                    mdlPopUpAddNote.Show();
                }



            }
            catch (Exception ex)
            {
                uiMessageNotes.isError = true;
                uiMessageNotes.messageText = ex.Message;

                if ((uiMessageNotes.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessageNotes.isError == true))
                {
                    uiMessageNotes.showErrorMessage(uiMessageNotes.messageText);
                }
            }
        }

        #region IListingPage Implementation
        public void loadData()
        {
            if (objSession.TreeItemId == -1 || objSession.TreeItemId == -2 || objSession.TreeItemId == 0)
            {
                this.btnAddNotes.Visible = false;
                return;
            }
            else
            {
                if (isReadOnly == true)
                {
                    this.btnAddNotes.Visible = false;
                }
                else
                {
                    this.btnAddNotes.Visible = true;
                }
                int? schemeId = null;
                int? blockId = null;

                this.GetQueryStringParams();
                if (requestType == ApplicationConstants.Scheme)
                    schemeId = id;
                else if (requestType == ApplicationConstants.Block)
                    blockId = id;

                DataSet provisionsNotesDS = objProvisionBL.getProvisionNotes(schemeId, blockId, objSession.TreeItemId);
                grdItemNotes.DataSource = provisionsNotesDS;
                grdItemNotes.DataBind();
            }
        }

        public void searchData()
        {
            throw new NotImplementedException();
        }
        public void populateData()
        {
            throw new NotImplementedException();
        }
        public void printData()
        {
            throw new NotImplementedException();
        }
        public void exportGridToExcel()
        {
            throw new NotImplementedException();
        }
        public void exportToPdf()
        {
            throw new NotImplementedException();
        }
        public void downloadData()
        {
            throw new NotImplementedException();
        }
        public void applyFilters()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region IAddEditPage Implementation
        public void saveData()
        {
            ProvisionNotesBO objProvisionsNotes = new ProvisionNotesBO();
            this.GetQueryStringParams();
            if (requestType == ApplicationConstants.Scheme)
                objProvisionsNotes.SchemeId = id;
            else if (requestType == ApplicationConstants.Block)
                objProvisionsNotes.BlockId = id;

            objProvisionsNotes.ProvisionId = objSession.TreeItemId;
            objProvisionsNotes.Notes = txtNote.Text.Trim();
            objProvisionsNotes.CreatedBy = objSession.EmployeeId;
            objProvisionBL.SaveProvisionNotes(objProvisionsNotes);
        }
        public bool validateData()
        {
            if (txtNote.Text == string.Empty)
            {
                uiMessageNotes.messageText = UserMessageConstants.notValidNote;
                return false;
            }
            return true;
        }
        public void resetControls()
        {
            throw new NotImplementedException();
        }
        public void populateDropDown(DropDownList ddl)
        {
            throw new NotImplementedException();
        }
        public void populateDropDowns()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region "Helper Functions"
        private void GetQueryStringParams()
        {
            if ((Request.QueryString[ApplicationConstants.Id] != null))
                id = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);

            if ((Request.QueryString[ApplicationConstants.RequestType] != null))
                requestType = Request.QueryString[ApplicationConstants.RequestType];

        }


        #endregion
    }
}