﻿using System;
using System.Web.UI.WebControls;
using PropertyDataRestructure.Base;
using PDR_Utilities.Constants;
using PDR_BusinessObject.PageSort;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_BusinessLogic.SchemeBlock;
using PDR_DataAccess.SchemeBlock;

namespace PropertyDataRestructure.UserControls.Pdr.SchemeBlock
{
    public partial class HeatingDefect : UserControlBase
    {
        //PropertyBL objPropertyBl = new PropertyBL();
        AttributesBL objAttributeBL = new AttributesBL(new AttributesRepo());

        PageSortBO objPageSortBo  = new PageSortBO("ASC", "Fuel", 1, 30);
        public bool _readOnly = false;
        protected void Page_Load(object sender, System.EventArgs e)
        {
            string PageName = sender.ToString();
            if (PageName.Contains(PathConstants.SchemeRecordPage))
            {
                _readOnly = true;
            }
            else
            {
                _readOnly = false;
            }

            if (_readOnly == true)
            {
                btnAddDefect.Visible = false;
            }

            uiMessageHelper.resetMessage(ref lblMessage, ref pnlMessage);
            ucDefectManagement.showPoupAgainEvent += showPopupAgain;

            ucDefectManagement.cancelButton_ClickedEvent += ucDefectManagement_cancelButton_Clicked;

            ucDefectManagement.saveButton_ClickedEvent += ucDefectManagement_saveButton_Clicked;

            if (!IsPostBack)
            {
                this.setPRequestTypeAndId(Request.QueryString[ApplicationConstants.RequestType], Request.QueryString[ApplicationConstants.Id]);
                lnkBtnServDefectTab.CssClass = ApplicationConstants.TabClickedCssClass;
                MainView.ActiveViewIndex = 0;
            }
        }

        protected void lnkBtnServDefectTab_Click(object sender, EventArgs e)
        {
            lnkBtnServDefectTab.CssClass = ApplicationConstants.TabClickedCssClass;
            lnkBtnPhotographTab.CssClass = ApplicationConstants.TabInitialCssClass;
            MainView.ActiveViewIndex = 0;
        }

        protected void lnkBtnPhotographTab_Click(object sender, EventArgs e)
        {
            lnkBtnServDefectTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnPhotographTab.CssClass = ApplicationConstants.TabClickedCssClass;
            MainView.ActiveViewIndex = 1;
        }

        protected void btnAddDefect_Click(object sender, EventArgs e)
        {
            try
            {
                string Id = Request.QueryString[ApplicationConstants.Id];
                string RequestType = Request.QueryString[ApplicationConstants.RequestType];
                ucDefectManagement.prepareAddDefect(Id, RequestType);
            }
            catch (Exception ex)
            {
                uiMessageHelper.IsError = true;
                uiMessageHelper.Message = ex.Message;
                if (uiMessageHelper.IsExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessageHelper.IsError == true)
                {
                }

                mdlPoPUpAddDefect.Show();
            }
        }

        protected void ckBoxPhotoUpload_CheckedChanged(object sender, EventArgs e)
        {
            this.ckBoxPhotoUpload.Checked = false;
            try
            {
                if ((objSession.PhotoUploadName == null))
                {
                    uiMessageHelper.setMessage(ref lblMessage, ref pnlMessage, UserMessageConstants.ErrorDocumentUpload, true);
                }
                else
                {
                    Label lblFileName = ucDefectManagement.FindControl("lblFileName") as Label;
                    lblFileName.Text = objSession.PhotoUploadName;
                }
            }
            catch (Exception ex)
            {
                uiMessageHelper.IsError = true;
                uiMessageHelper.Message = ex.Message;
                if (uiMessageHelper.IsExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessageHelper.IsError == true)
                {
                    uiMessageHelper.setMessage(ref lblMessage, ref pnlMessage, uiMessageHelper.Message, true);
                }

                mdlPoPUpAddDefect.Show();
                objSession.PhotoUploadName = null;
            }
        }

        private void ucDefectManagement_cancelButton_Clicked(object sender, System.EventArgs e)
        {
            try
            {
                mdlPoPUpAddDefect.Hide();
            }
            catch (Exception ex)
            {
                uiMessageHelper.IsError = true;
                uiMessageHelper.Message = ex.Message;
                if (uiMessageHelper.IsExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessageHelper.IsError == true)
                {
                    uiMessageHelper.setMessage(ref lblMessage, ref pnlMessage, uiMessageHelper.Message, true);
                }
            }
        }

        private void ucDefectManagement_saveButton_Clicked(object sender, System.EventArgs e)
        {
            try
            {
                mdlPoPUpAddDefect.Hide();
                uiMessageHelper.setMessage(ref lblMessage, ref pnlMessage, UserMessageConstants.DefectSavedSuccessfuly, false);
            }
            catch (Exception ex)
            {
                uiMessageHelper.IsError = true;
                uiMessageHelper.Message = ex.Message;
                if (uiMessageHelper.IsExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessageHelper.IsError == true)
                {
                    uiMessageHelper.setMessage(ref lblMessage, ref pnlMessage, uiMessageHelper.Message, true);
                    mdlPoPUpAddDefect.Show();
                }
                else
                {
                    loadDefects();
                }
            }
        }

        private void serviceDefects_viewDefect_Clicked(object sender, System.EventArgs e)
        {
            try
            {
                ucDefectManagement.viewDefectDetails(serviceDefects.PropertyDefectId);
            }
            catch (Exception ex)
            {
                uiMessageHelper.IsError = true;
                uiMessageHelper.Message = ex.Message;
                if (uiMessageHelper.IsExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessageHelper.IsError == true)
                {
                    uiMessageHelper.setMessage(ref lblMessage, ref pnlMessage, uiMessageHelper.Message, true);
                }

                mdlPoPUpAddDefect.Show();
            }
        }

        public void showPopupAgain(object sender, System.EventArgs e)
        {
            try
            {
                mdlPoPUpAddDefect.Show();
            }
            catch (Exception ex)
            {
                uiMessageHelper.IsError = true;
                uiMessageHelper.Message = ex.Message;
                if (uiMessageHelper.IsExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessageHelper.IsError == true)
                {
                    uiMessageHelper.setMessage(ref lblMessage, ref pnlMessage, uiMessageHelper.Message, true);
                }
            }
        }

        protected void setPageSortBo(ref PageSortBO objPageSortBo)
        {
            ViewState[ViewStateConstants.PageSortBo] = objPageSortBo;
        }

        protected PageSortBO getPageSortBo()
        {
            return (PageSortBO)ViewState[ViewStateConstants.PageSortBo];
        }

        private void setPRequestTypeAndId(string requestType, string Id)
        {
            ViewState[ViewStateConstants.RequestType] = requestType;
            if (requestType == ApplicationConstants.Block)
                ViewState[ViewStateConstants.BlockId] = Id;
            else
                ViewState[ViewStateConstants.SchemeId] = Id;
        }

        private string getSchemeBlockId()
        {
            if (this.getRequestType() == ApplicationConstants.Block)
            {
                return (string)ViewState[ViewStateConstants.BlockId];
            }
            else
            {
                return (string)ViewState[ViewStateConstants.SchemeId];
            }
        }

        private string getRequestType()
        {
            return (string)ViewState[ViewStateConstants.RequestType];
        }

        public bool isApplianceHasDefect(string applianceId)
        {
            if (applianceId != "")
            {
                if ((Convert.ToInt32(applianceId) == 0))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            return false;
        }

        public void loadDefects()
        {     
            DataSet resultDataSet = new DataSet();
			objAttributeBL.getSchemeBlockAppliances(objSession.HeatingMappingId.ToString(), ref resultDataSet);
            GridView grdService = (GridView)serviceDefects.FindControl("grdServiceDefects");
            grdService.DataSource = null;
            grdService.DataBind();
            if (resultDataSet.Tables[0].Rows.Count > 0)
            {
                grdService.DataSource = resultDataSet;
                grdService.DataBind();
            }
        }

    }
}