﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PurchaseOrderForm.ascx.cs" Inherits="PropertyDataRestructure.UserControls.Pdr.SchemeBlock.PurchaseOrderForm" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>

<link href="/PropertyDataRestructure/Styles/Site.css" rel="stylesheet" />
<script src="/PropertyDataRestructure/Scripts/jquery-1.12.4.js" type="text/javascript"></script>
<script src="/PropertyDataRestructure/Scripts/jquery-ui-1.12.1.js" type="text/javascript"></script>
<script src="/PropertyDataRestructure/Scripts/bootstrap.min.js" type="text/javascript"></script>
<script src="/PropertyDataRestructure/Scripts/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="/PropertyDataRestructure/Scripts/dataTables.bootstrap.min.js" type="text/javascript"></script>
<script src="/PropertyDataRestructure/Scripts/dataTables.select.min.js" type="text/javascript"></script>
<script src="/PropertyDataRestructure/Scripts/jquery.dialog.min.js" type="text/javascript"></script>
<script src="/PropertyDataRestructure/Scripts/select2.min.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.2.2/css/select.dataTables.min.css" />
<link href="/PropertyDataRestructure/Styles/bootstrap.css" rel="stylesheet" type="text/css" />
<link href="/PropertyDataRestructure/Styles/select2.min.css" rel="stylesheet" type="text/css" />
<link href="/PropertyDataRestructure/Styles/dialog.css" rel="stylesheet" type="text/css" />
<link href="/PropertyDataRestructure/Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
<link href="/PropertyDataRestructure/Styles/cyclical.css" rel="stylesheet" type="text/css" />
<%--<script src="/PropertyDataRestructure/Scripts/Cyclical.js" type="text/javascript"></script>--%>
<link href="/PropertyDataRestructure/Styles/jquery.dialog.min.css" rel="stylesheet" type="text/css" />

<style type="text/css" media="all">
    .modalBack
    {
        background-color: Black;
        filter: alpha(opacity=30);
        opacity: 0.3;
        border: 1px solid black;
    }
</style>
<div class="modal fade" id="poModalRaiseaPO" role="dialog" data-backdrop="static"
    data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <asp:Label class="modal-title" ID="heading" runat="server">
                    Raise a PO</asp:Label>
                <%-- <h4 class="modal-title">Raise a PO</h4>--%>
                <input type="hidden" id="hdnContractorId" value="0" />
            </div>
            <asp:UpdatePanel ID="updpnlAssignToContractor" runat="server">
                <ContentTemplate>
                    <div class="modal-body" style="font-weight: 400;">
                        <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="500px" />
                        <div class="assignToContractor" style="height: 550px; overflow: auto;">
                            <div class="popupHeader">
                            </div>
                            <uim:UIMessage ID="uiMessage1" runat="Server" Visible="false" />
                            <%-- <hr />--%>
                            <div class="input_row">
                                <div class="input_label">
                                    Cost Centre:
                                </div>
                                <div class="input_Cal">
                                    <asp:DropDownList runat="server" ID="ddlCostCentre" Width="250px" OnSelectedIndexChanged="ddlCostCentre_SelectedIndexChanged"
                                        AutoPostBack="True" />
                                    <asp:RequiredFieldValidator runat="server" ID="rfvCostCentre" ControlToValidate="ddlCostCentre"
                                        InitialValue="-1" ErrorMessage="Please select a Cost Centre" Display="Dynamic"
                                        ValidationGroup="addWorkRequired" CssClass="Required" />
                                </div>
                            </div>
                            <div class="input_row">
                                <div class="input_label">
                                    Budget Head:
                                </div>
                                <div class="input_Cal">
                                    <asp:DropDownList runat="server" ID="ddlBudgetHead" Width="250px" OnSelectedIndexChanged="ddlBudgetHead_SelectedIndexChanged"
                                        AutoPostBack="True" />
                                    <asp:RequiredFieldValidator runat="server" ID="rfvBudgetHead" ControlToValidate="ddlBudgetHead"
                                        InitialValue="-1" ErrorMessage="Please select a Budget Head" Display="Dynamic"
                                        ValidationGroup="addWorkRequired" CssClass="Required" />
                                </div>
                            </div>
                            <div class="input_row">
                                <div class="input_label">
                                    Expenditure:
                                </div>
                                <div class="input_Cal">
                                    <asp:DropDownList runat="server" Width="250px" ID="ddlExpenditure" />
                                    <asp:RequiredFieldValidator runat="server" ID="rfvExpenditure" ControlToValidate="ddlExpenditure"
                                        InitialValue="-1" ErrorMessage="Please select an Expenditure" Display="Dynamic"
                                        ValidationGroup="addWorkRequired" CssClass="Required" />
                                </div>
                            </div>
                            <div class="input_row">
                                <div class="input_label">
                                    Supplier:
                                </div>
                                <div class="input_Cal">
                                    <asp:DropDownList runat="server" ID="ddlContractor" Width="250px" OnSelectedIndexChanged="ddlContractor_SelectedIndexChanged"
                                        AutoPostBack="True" />
                                    <asp:RequiredFieldValidator runat="server" ID="rfvContractor" InitialValue="-1" ErrorMessage="Please select a Contractor"
                                        Display="Dynamic" ValidationGroup="assignToContractor" ControlToValidate="ddlContractor"
                                        CssClass="Required" />
                                </div>
                            </div>
                            <div class="input_row">
                                <div class="input_label">
                                    Contact:
                                </div>
                                <div class="input_Cal">
                                    <asp:DropDownList runat="server" Width="250px" ID="ddlContact" AutoPostBack="true"
                                        OnSelectedIndexChanged="ddlContact_SelectedIndexChanged">
                                        <asp:ListItem Selected="True" Text="-- Please select contractor --" Value="-1" />
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="input_row">
                                <div class="input_label">
                                    Name:
                                </div>
                                <div class="input_Cal">
                                    <asp:TextBox runat="server" ID="POName" Width="250" EnableViewState="false">
                                    </asp:TextBox>
                                    <asp:RequiredFieldValidator runat="server" ID="rfcPONAME" ErrorMessage="Please enter a PO Name"
                                        Display="Dynamic" ValidationGroup="assignToContractor" ControlToValidate="POName"
                                        CssClass="Required" />
                                </div>
                            </div>
                            <div class="input_row">
                                <div class="input_label">
                                    <strong>Inc In S/Chge:</strong>
                                </div>
                                <div class="input_Cal">
                                    <asp:CheckBox runat="server" ID="ServiceCharge" Checked="false" />
                                </div>
                            </div>
                            <div class="input_row">
                                <div class="input_label">
                                    <strong>Property Apportionment (£):</strong>
                                </div>
                                <div class="input_Cal">
                                    <asp:TextBox runat="server" ID="txtPropertyApportionment" CausesValidation="true"
                                        Width="250" ValidationGroup="PropRequired" MaxLength="11"></asp:TextBox>
                                    <asp:RequiredFieldValidator ErrorMessage="Please enter Property Apportionment, in form XXXXXX.XXXX where X is a digit between 0 to 9."
                                        ControlToValidate="txtPropertyApportionment" ID="rfvPropertyApportionment" ValidationGroup="PropRequired"
                                        runat="server" CssClass="Required" Display="Dynamic" />
                                    <asp:RegularExpressionValidator ID="rxvPropertyApportionment" runat="server" ErrorMessage="Please enter Property Apportionment, in form XXXXXX.XXXX where X is a digit between 0 to 9."
                                        Display="Dynamic" ValidationGroup="PropRequired" CssClass="Required" ControlToValidate="txtPropertyApportionment"
                                        ValidationExpression="^[0-9]{1,6}(?:\.[0-9]{1,4})?$" />
                                    <asp:RangeValidator ID="rvPropertyApportionment" runat="server" ControlToValidate="txtPropertyApportionment"
                                        MinimumValue="0" MaximumValue="178956.9704" ErrorMessage="Please enter a value between &quot;0&quot; and &quot;178956.9704&quot;."
                                        CssClass="Required" Type="Double" Display="Dynamic" ValidationGroup="PropRequired" />
                                </div>
                                <asp:Label ID="PropErrorId" runat="server" Style="color: red;" Text="Please enter Property Apportionment"
                                    Visible="false"></asp:Label>
                            </div>
                            <hr />
                            <div class="input_row" style="overflow: auto;">
                                <div class="input_label">
                                    <strong>Scheme:</strong>
                                </div>
                                <div class="input_Cal">
                                    <asp:Label ID="lblSchemeName" runat="server" Text="" Font-Bold="true"></asp:Label>
                                </div>
                            </div>
                            <div class="input_row">
                                <div class="input_label">
                                    <strong>Block:</strong>
                                </div>
                                <div class="input_Cal">
                                    <asp:Label ID="lblBlockName" runat="server" Text="" Font-Bold="true"></asp:Label>
                                </div>
                            </div>
                            <div class="input_row">
                                <div class="input_label">
                                    <strong>Property:</strong>
                                </div>
                                <div class="input_Cal" style="overflow: auto; height: 120px; width: 400px">
                                    <asp:ListBox runat="server" ID="txtPropertiesPC1" ClientIDMode="Static" SelectionMode="Multiple"
                                        Width="400px"></asp:ListBox>
                                </div>
                            </div>
                            <hr />
                            <div class="input_row">
                                <div class="input_label">
                                    <strong>Works Required:</strong>
                                </div>
                                <div class="input_Cal">
                                    <asp:TextBox runat="server" ID="txtWorkRequired" TextMode="MultiLine" Rows="5" Width="250px" />
                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtWorkRequired"
                                        WatermarkText="Enter details here" WatermarkCssClass="searchTextDefault">
                                    </cc1:TextBoxWatermarkExtender>
                                    <asp:RequiredFieldValidator runat="server" ID="rfvWorksRequired" ErrorMessage="Please enter works required."
                                        Display="Dynamic" ValidationGroup="addWorkRequired" ControlToValidate="txtWorkRequired"
                                        ForeColor="Red" Style="vertical-align: top;" />
                                    <asp:CustomValidator ID="cvWorksRequired" runat="server" ControlToValidate="txtWorkRequired"
                                        ErrorMessage="Works Required must not exceed 4000 characters." Display="Dynamic"
                                        ForeColor="Red" ValidationGroup="addWorkRequired" EnableClientScript="False" />
                                </div>
                            </div>
                            <div class="input_row">
                                <div class="input_label">
                                    <strong>Estimate Ref:</strong>
                                </div>
                                <div class="input">
                                    <asp:TextBox runat="server" ID="txtEstimateRef" Width="250" MaxLength="200" />
                                    <cc1:TextBoxWatermarkExtender ID="txtWaterExtEstimateRef" runat="server" TargetControlID="txtEstimateRef"
                                        WatermarkText="Optional" WatermarkCssClass="searchTextDefault">
                                    </cc1:TextBoxWatermarkExtender>
                                    <asp:RegularExpressionValidator runat="server" ID="rxvEstimateRef" ErrorMessage="Works Required must not exceed 200 characters"
                                        Display="Dynamic" CssClass="Required" ControlToValidate="txtEstimateRef" ValidationGroup="addWorkRequired"
                                        ValidationExpression="^[\s\S]{0,200}$" Style="vertical-align: top;" />
                                </div>
                            </div>
                            <div class="input_row">
                                <div class="input_label">
                                    <strong>Net Cost (£):</strong>
                                </div>
                                <div class="input_small">
                                    <asp:TextBox runat="server" ID="txtNetCost" OnTextChanged="txtNetCost_TextChanged"
                                        AutoPostBack="True" CausesValidation="true" ValidationGroup="addWorkRequired"
                                        MaxLength="11"></asp:TextBox>
                                    <asp:RequiredFieldValidator ErrorMessage="Please enter net cost, in form XXXXXX.XXXX where X is a digit between 0 to 9."
                                        ControlToValidate="txtNetCost" ID="rfvNetCost" ValidationGroup="addWorkRequired"
                                        runat="server" CssClass="Required" Display="Dynamic" />
                                    <asp:RegularExpressionValidator ID="rxvNetCost" runat="server" ErrorMessage="Please enter net Cost, in form XXXXXX.XXXX where X is a digit between 0 to 9."
                                        Display="Dynamic" ValidationGroup="addWorkRequired" CssClass="Required" ControlToValidate="txtNetCost"
                                        ValidationExpression="^[0-9]{1,6}(?:\.[0-9]{1,4})?$" />
                                    <asp:RangeValidator ID="rvNetCost" runat="server" ControlToValidate="txtNetCost"
                                        MinimumValue="0" MaximumValue="178956.9704" ErrorMessage="Please enter a value between &quot;0&quot; and &quot;178956.9704&quot;."
                                        CssClass="Required" Type="Double" Display="Dynamic" ValidationGroup="addWorkRequired" />
                                </div>
                            </div>
                            <div class="input_row">
                                <div class="input_label">
                                    <strong>Vat:</strong>
                                </div>
                                <div class="input_small">
                                    <asp:DropDownList runat="server" ID="ddlVat" OnSelectedIndexChanged="ddlVat_SelectedIndexChanged"
                                        AutoPostBack="True" CausesValidation="True" />
                                    <asp:RequiredFieldValidator runat="server" ID="rfvddlVat" InitialValue="-1" ErrorMessage="Please select a Vat(Vat Rate)"
                                        Display="Dynamic" ValidationGroup="addWorkRequired" ControlToValidate="ddlVat"
                                        CssClass="Required" />
                                </div>
                            </div>
                            <div class="input_row">
                                <div class="input_label">
                                    <strong>Vat (£):</strong>
                                </div>
                                <div class="input_small">
                                    <asp:TextBox runat="server" ID="txtVat" ReadOnly="true"></asp:TextBox>
                                </div>
                            </div>
                            <div class="input_row">
                                <div class="input_label">
                                    <strong>Total (£):</strong>
                                </div>
                                <div class="input_small">
                                    <asp:TextBox runat="server" ID="txtTotal" ReadOnly="true"></asp:TextBox>
                                    <asp:Button Text="Add" runat="server" UseSubmitBehavior="false" ID="btnAdd" OnClick="btnAdd_Click"
                                        CssClass="addButton btn btn-info btn-grey btn-xs" ValidationGroup="addWorkRequired" />
                                </div>
                            </div>
                            <hr />
                            <div class="worksRequiredGrid row padding-bottom-5">
                                <asp:GridView runat="server" ID="grdWorksDeatil" OnRowDataBound="grdWorksDeatil_RowDataBound"
                                    ShowFooter="True" GridLines="Horizontal" AutoGenerateColumns="False" PageSize="100"
                                    ShowHeaderWhenEmpty="True" Width="100%" BorderStyle="None" EmptyDataText="No Record Found">
                                    <HeaderStyle Font-Bold="false" />
                                    <FooterStyle BorderColor="Black" Font-Bold="true" HorizontalAlign="Right" Wrap="False" />
                                    <RowStyle BorderColor="Gray" BorderWidth="0px" BorderStyle="None" />
                                    <Columns>
                                        <asp:BoundField HeaderText="Works Required:" FooterText="Total:" DataField="ServiceRequired"
                                            HeaderStyle-HorizontalAlign="Left" ItemStyle-CssClass="worksRequired" />
                                        <asp:BoundField HeaderText="Net:" FooterText="0.00" DataFormatString="{0:0.####}"
                                            DataField="NetCost" FooterStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="100px" />
                                        <asp:BoundField HeaderText="Vat:" HeaderStyle-HorizontalAlign="Center" FooterText="0.00"
                                            DataFormatString="{0:0.####}" DataField="Vat" FooterStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="100px" />
                                        <asp:BoundField HeaderText="Gross:" HeaderStyle-HorizontalAlign="Center" FooterText="0.00"
                                            DataFormatString="{0:0.####}" DataField="Gross" FooterStyle-HorizontalAlign="Center"
                                            ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="100px" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                            <%--<hr />--%>
                            <div class="clear" />
                            <%-- <hr />--%>
                            <br />
                        </div>
                        <asp:Panel ID="pnlNoRecordFound" runat="server" BackColor="White" Width="350px" Style="font-weight: normal;
                            font-size: 13px; padding: 10px;">
                            <b>Warning </b>
                            <br></br>
                            <hr>
                            <p>
                                No contact details exist for this Supplier, and therefore they have not received
                                an email notification that the Purchase Order has been approved. Please contact
                                the supplier directly with the Purchase Order details. To update the Supplier records
                                with contact details please contact a member of the Finance Team.
                            </p>
                            <asp:Button ID="btnOk" runat="server" Text="OK" UseSubmitBehavior="false" OnClick="btnOk_click" />
                        </asp:Panel>
                        <asp:Button ID="btnHidde" runat="server" Text="" Style="display: none;" />
                        <cc1:ModalPopupExtender ID="mdlPopupRis" runat="server" TargetControlID="btnHidde"
                            PopupControlID="pnlNoRecordFound" Enabled="true" DropShadow="true" BackgroundCssClass="modalBack">
                        </cc1:ModalPopupExtender>
                    </div>
                    <div class="modal-footer">
                        <div class="row">
                            <div class="col-md-1">
                                <asp:Button ID="btnClose" OnClick="btnClose_Click" runat="server" Text="Close" CausesValidation="false"
                                    CssClass="btn btn-info btn-grey btn-xs" />
                            </div>
                            <div class="col-md-5">
                            </div>
                            <div class="col-md-6">
                                <asp:Button ID="btnAssignToContractor" UseSubmitBehavior="false" OnClick="btnAssignToContractor_Click"
                                    runat="server" Style="text-align: right" Text="Assign to selected contractor"
                                    CssClass="btn btn-info btn-grey btn-xs" ValidationGroup="assignToContractor" />
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</div>
