﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PropertyDataRestructure.Base;
using PropertyDataRestructure.Interface;
using PDR_Utilities.Constants;
using PDR_Utilities.Managers;
using PDR_BusinessLogic.SchemeBlock;
using System.Data;
using PDR_DataAccess.SchemeBlock;
using PDR_BusinessObject.PageSort;
using PDR_Utilities.Helpers;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_BusinessObject.SchemeBlock;

namespace PropertyDataRestructure.UserControls.Pdr.SchemeBlock
{
    public partial class Appliance : UserControlBase, IListingPage, IAddEditPage
    {
        #region Properties
        int id = 0;
        string requestType = string.Empty;
        public bool _readOnly = false;
        #endregion

        #region Events
        #region Page load event
        protected void Page_Load(object sender, EventArgs e)
        {
            string PropPageName = Request.Path.ToString();
            if (PropPageName.Contains("SchemeDashBoard"))
            {
                _readOnly = false;
            }
            else if (PropPageName.Contains("SchemeRecord"))
            {
                _readOnly = true;
            }
            if (_readOnly == true)
            {
                btnAddAppliance.Visible = false;
                btnSave.Visible = false;
                for (int i = 0; i < grdAppliances.Rows.Count; i++)
                {
                    grdAppliances.Rows[i].Cells[8].Visible = false;
                }
            }
            try
            {
                uiMessage.hideMessage();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion
        #region lnkbtnPager click event
        protected void lnkbtnPager_Click(object sender, EventArgs e)
        {
            try
            {

                LinkButton pagebuttton = new LinkButton();
                pagebuttton = (LinkButton)sender;
                PageSortBO objPageSortBo = new PageSortBO("DESC", "DocumentId", 1, 10);
                GridHelper.processGridPager(sender, objPageSortBo, pageSortViewState);
                populateData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion
        #region "btn Add Appliance Click"
        /// <summary>
        /// btn Add Appliance Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAddAppliance_Click(object sender, EventArgs e)
        {
            try
            {
                this.resetControls();
                getLifeSpanLookUpValues(ref ddlLifeSpan);

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
            mdlPopUpAddAppliance.Show();
        }
        #endregion

        #region "btnAmend Click event"
        /// <summary>
        /// btnAmend Click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void btnAmend_Click(object sender, EventArgs e)
        {
            try
            {
                this.resetControls();
                Button btnAmend = (Button)sender;
                Int32 applianceId = Convert.ToInt32(btnAmend.CommandArgument);
                hdnApplianceId.Value = applianceId.ToString();
                populateData();
                mdlPopUpAddAppliance.Show();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        #endregion

        #region btn save click event
        /// <summary>
        /// btn save click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                saveData();
                uiMessage.showInformationMessage(UserMessageConstants.ApplianceSavedSuccessfully);
                loadData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion
        #endregion

        #region Implement Interface

        public void saveData()
        {

            getQueryString();

            ApplianceBO objApplianceBo = new ApplianceBO();
            if (requestType == ApplicationConstants.Scheme)
            {
                objApplianceBo.SchemeId = id;
            }
            else if (requestType == ApplicationConstants.Block)
            {
                objApplianceBo.BlockId = id;
            }
            if (Convert.ToInt32(hdnApplianceId.Value) > 0)
            {
                objApplianceBo.ExistingApplianceId = Convert.ToInt32(hdnApplianceId.Value);
            }

            if (!string.IsNullOrEmpty(hdnSelectedLocationId.Value) && Convert.ToInt32(hdnSelectedLocationId.Value) > 0)
            {
                objApplianceBo.LocationId = Convert.ToInt32(hdnSelectedLocationId.Value);
            }
            else
            {
                objApplianceBo.LocationId = -1;
            }
            objApplianceBo.Location = txtLocation.Text;
            if (!string.IsNullOrEmpty(hdnSelectedTypeId.Value) && Convert.ToInt32(hdnSelectedTypeId.Value) > 0)
            {
                objApplianceBo.TypeId = Convert.ToInt32(hdnSelectedTypeId.Value);
            }
            else
            {
                objApplianceBo.TypeId = -1;
            }
            objApplianceBo.Type = txtType.Text.Trim();

            if (!string.IsNullOrEmpty(hdnSelectedmakeId.Value) && Convert.ToInt32(hdnSelectedmakeId.Value) > 0)
            {
                objApplianceBo.MakeId = Convert.ToInt32(hdnSelectedmakeId.Value);
            }
            else
            {
                objApplianceBo.MakeId = -1;
            }
            objApplianceBo.Make = txtMake.Text.Trim();
            if (!string.IsNullOrEmpty(hdnSelectedModelId.Value) && Convert.ToInt32(hdnSelectedModelId.Value) > 0)
            {
                objApplianceBo.ModelId = Convert.ToInt32(hdnSelectedModelId.Value);
            }
            else
            {
                objApplianceBo.ModelId = -1;
            }
            objApplianceBo.Model = txtModel.Text.Trim();
            objApplianceBo.Item = txtItem.Text.Trim();
            objApplianceBo.ItemId = objSession.TreeItemId;
            objApplianceBo.Quantity = txtQuantity.Text.Trim();
            objApplianceBo.Dimensions = txtDimensions.Text.Trim();
            if (!string.IsNullOrEmpty(txtPurchaseCost.Text))
            {
                objApplianceBo.PurchaseCost = Convert.ToDecimal(txtPurchaseCost.Text.Trim());
            }
            if (Convert.ToInt32(ddlLifeSpan.SelectedValue) > 0)
            {
                objApplianceBo.LifeSpan = Convert.ToInt32(ddlLifeSpan.SelectedValue);
            }

            //objApplianceBo.Datepurchased = txtDatePurchased.Text.Trim()
            //objApplianceBo.PropertyId = Request.QueryString(PathConstants.PropertyIds);
            objApplianceBo.SerialNumber = txtSerialNumber.Text;
            if (string.IsNullOrEmpty(txtDatePurchased.Text.ToString()))
            {
                objApplianceBo.Datepurchased = System.DateTime.MinValue;
            }
            else
            {
                objApplianceBo.Datepurchased = Convert.ToDateTime(txtDatePurchased.Text);
            }

            if (string.IsNullOrEmpty(txtDateRemoved.Text.ToString()))
            {
                objApplianceBo.DateRemoved = System.DateTime.MinValue;
            }
            else
            {
                objApplianceBo.DateRemoved = Convert.ToDateTime(txtDateRemoved.Text);
            }
            objApplianceBo.Notes = txtNotes.Text.Trim();
            AttributesBL objAttrBl = new AttributesBL(new AttributesRepo());
            int x =  objAttrBl.saveAppliance(objApplianceBo);

        }

        public bool validateData()
        {
            throw new NotImplementedException();
        }

        public void resetControls()
        {
            hdnSelectedLocationId.Value = "-1";
            hdnSelectedmakeId.Value = "-1";
            hdnSelectedModelId.Value = "-1";
            hdnSelectedTypeId.Value = "-1";
            txtDatePurchased.Text = string.Empty;
            txtDateRemoved.Text = string.Empty; 
            txtDimensions.Text = string.Empty;
            txtItem.Text = string.Empty;
            txtLocation.Text = string.Empty;
            txtMake.Text = string.Empty;
            txtModel.Text = string.Empty;
            txtNotes.Text = string.Empty;
            txtPurchaseCost.Text = string.Empty;
            txtQuantity.Text = string.Empty;
            txtSerialNumber.Text = string.Empty;
            txtType.Text = string.Empty;
            ddlLifeSpan.SelectedIndex = 0;

        }

        #region getQueryString()
        private void getQueryString()
        {

            if ((Request.QueryString[ApplicationConstants.Id] != null))
            {
                id = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);

            }

            if ((Request.QueryString[ApplicationConstants.RequestType] != null))
            {

                requestType = Request.QueryString[ApplicationConstants.RequestType];
            }
        }
        #endregion

        public void loadData()
        {
            btnAddAppliance.Text = "Add " + objSession.TreeItemName;
            lblNewAppliance.Text = "Add New " + objSession.TreeItemName;
            getQueryString();
            PageSortBO objPageSortBo = new PageSortBO("DESC", "ApplianceId", 1, 10);
            if (pageSortViewState != null)
            {
                objPageSortBo = pageSortViewState;
            }
            int? schemeId = null;
            int? blockId = null;

            if (requestType == ApplicationConstants.Scheme)
            {
                schemeId = id;
            }
            else if (requestType == ApplicationConstants.Block)
            {
                blockId = id;
            }

            AttributesBL objAttrBl = new AttributesBL(new AttributesRepo());
            DataSet resultDataSet = new DataSet();
            int totalCount = objAttrBl.getAppliancesList(ref resultDataSet, ref objPageSortBo, schemeId, objSession.TreeItemId, blockId);

            grdAppliances.DataSource = resultDataSet;
            grdAppliances.DataBind();
            objPageSortBo.TotalRecords = totalCount;
            objPageSortBo.TotalPages = Convert.ToInt32(Math.Ceiling(Convert.ToDecimal(totalCount) / objPageSortBo.PageSize));
            pnlPagination.Visible = false;
            objSession.ApplianceDetailDataSet = resultDataSet;
            if (objPageSortBo.TotalPages > 0)
            {
                pnlPagination.Visible = true;
                pageSortViewState = objPageSortBo;
                GridHelper.setGridViewPager(ref pnlPagination, pageSortViewState);
            }
            if (_readOnly == true)
            {
                for (int i = 0; i < grdAppliances.Rows.Count; i++)
                {
                    grdAppliances.Rows[i].Cells[8].Visible = false;
                }
            }

        }

        public void populateDropDown(DropDownList ddl)
        {
            throw new NotImplementedException();
        }

        public void populateDropDowns()
        {
            throw new NotImplementedException();
        }


        public void searchData()
        {
            throw new NotImplementedException();
        }

        public void populateData()
        {
            DataSet resultDataSet = new DataSet();
            resultDataSet = objSession.ApplianceDetailDataSet;
            getLifeSpanLookUpValues(ref ddlLifeSpan);
            var applianceResult = (from ps in resultDataSet.Tables[0].AsEnumerable() where ps["ApplianceId"].ToString() == hdnApplianceId.Value   select ps).FirstOrDefault();
            if (applianceResult != null)
            {
                if (!DBNull.Value.Equals(applianceResult["LocationID"]))
                {
                    hdnSelectedLocationId.Value = applianceResult["LocationID"].ToString();
                }
                if (!DBNull.Value.Equals(applianceResult["MakeId"]))
                {
                    hdnSelectedmakeId.Value = applianceResult["MakeId"].ToString();
                }
                if (!DBNull.Value.Equals(applianceResult["ModelId"]))
                {
                    hdnSelectedModelId.Value = applianceResult["ModelId"].ToString();
                }
                if (!DBNull.Value.Equals(applianceResult["TypeId"]))
                {
                    hdnSelectedTypeId.Value = applianceResult["TypeId"].ToString();
                }
                if (!DBNull.Value.Equals(applianceResult["DatePurchased"]))
                {
                    txtDatePurchased.Text = applianceResult["DatePurchased"].ToString();
                }
                if (!DBNull.Value.Equals(applianceResult["DateRemoved"]))
                {
                    txtDateRemoved.Text = applianceResult["DateRemoved"].ToString();
                }
                if (!DBNull.Value.Equals(applianceResult["Dimensions"]))
                {
                    txtDimensions.Text = applianceResult["Dimensions"].ToString();
                }
                if (!DBNull.Value.Equals(applianceResult["Item"]))
                {
                    txtItem.Text = applianceResult["Item"].ToString();
                }
                if (!DBNull.Value.Equals(applianceResult["Location"]))
                {
                    txtLocation.Text = applianceResult["Location"].ToString();
                }
                if (!DBNull.Value.Equals(applianceResult["Make"]))
                {
                    txtMake.Text = applianceResult["Make"].ToString();
                }
                if (!DBNull.Value.Equals(applianceResult["Model"]))
                {
                    txtModel.Text = applianceResult["Model"].ToString();
                }
                if (!DBNull.Value.Equals(applianceResult["Type"]))
                {
                    txtType.Text = applianceResult["Type"].ToString();
                }
                if (!DBNull.Value.Equals(applianceResult["PurchaseCost"]) && Convert.ToInt32(applianceResult["PurchaseCost"]) != 0)
                {
                    txtPurchaseCost.Text = applianceResult["PurchaseCost"].ToString();
                }
                if (!DBNull.Value.Equals(applianceResult["Quantity"]))
                {
                    txtQuantity.Text = applianceResult["Quantity"].ToString();
                }
                if (!DBNull.Value.Equals(applianceResult["SerialNumber"]))
                {
                    txtSerialNumber.Text = applianceResult["SerialNumber"].ToString();
                }
                if (!DBNull.Value.Equals(applianceResult["Notes"]))
                {
                    txtNotes.Text = applianceResult["Notes"].ToString();
                }
                if (!DBNull.Value.Equals(applianceResult["LifeSpan"]) && Convert.ToInt32(applianceResult["LifeSpan"]) != 0)
                {
                    ddlLifeSpan.SelectedValue = applianceResult["LifeSpan"].ToString();
                }
            }
        }

        public void printData()
        {
            throw new NotImplementedException();
        }

        public void exportGridToExcel()
        {
            throw new NotImplementedException();
        }

        public void exportToPdf()
        {
            throw new NotImplementedException();
        }

        public void downloadData()
        {
            throw new NotImplementedException();
        }

        public void applyFilters()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region "LifeSpan Lookup Values"


        private void getLifeSpanLookUpValues(ref DropDownList ddlname)
        {
            List<LookUpBO> lstLookUp = new List<LookUpBO>();
            for (int i = 1; i <= 100; i++)
            {
                LookUpBO objLookUpBO = new LookUpBO(i, i.ToString());
                lstLookUp.Add(objLookUpBO);
            }
            PopulateLookup(ref ddlname, ref lstLookUp);
        }
        #endregion

        #region "Populate Lookup"

        /// <summary>
        /// PopulateLookup method populates the given dropdown list with values from given lookuplist.
        /// </summary>
        /// <param name="ddlLookup">DropDownList : drop down to be pouplated</param>
        /// <param name="lstLookup">LookUpList: LookUpList to Bind with dropdown list</param>
        /// <remarks>this is a utility function that is used to populated a dropdown with lookup values in lookup list.</remarks>
        /// 
        private void PopulateLookup(ref DropDownList ddlLookup, ref List<LookUpBO> lstLookup)
        {
            ddlLookup.Items.Clear();
            if (((lstLookup != null) && (lstLookup.Count > 0)))
            {
                ddlLookup.DataSource = lstLookup;
                ddlLookup.DataValueField = "LookUpValue";
                ddlLookup.DataTextField = "LookUpName";
                ddlLookup.DataBind();
                ddlLookup.Items.Insert(0, new ListItem("Please Select", "-1"));
                ddlLookup.SelectedIndex = 0;
            }
        }

        //End Region "Populate Lookup"
        #endregion
       

       
      

    }
}