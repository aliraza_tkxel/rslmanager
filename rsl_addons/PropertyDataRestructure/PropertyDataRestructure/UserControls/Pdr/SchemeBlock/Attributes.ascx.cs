﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PropertyDataRestructure.Base;
using System.Data;
using PDR_DataAccess.SchemeBlock;
using PDR_BusinessLogic.SchemeBlock;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_Utilities.Managers;
using PDR_Utilities.Constants;
using PDR_BusinessObject.SchemeBlock;
using PDR_BusinessLogic.DynamicControls;
using PropertyDataRestructure.UserControls.Common;

namespace PropertyDataRestructure.UserControls.Pdr.SchemeBlock
{
    public partial class Attributes : UserControlBase
    {
        #region Properties
        AttributesBL objAttributeBL = new AttributesBL(new AttributesRepo());

        int id = 0;
        string requestType = string.Empty;
        Boolean isReadOnly = false;
        String pageName = "";
        #endregion

        #region Events
        #region PageLoad event
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                pageName = System.IO.Path.GetFileName(Request.Url.ToString());
                if (pageName.Contains(PathConstants.SchemeRecordPage))
                {
                    isReadOnly = true;
                }
                else
                {
                    isReadOnly = false;
                }
                uiMessage.hideMessage();
                this.ucAttributeDetail.delegateInvoke = new Action(loadDynamicControls);
                this.ucAttributeDetail.delegateTreeView = new Action(reloadTreeView);
                if (IsPostBack)
                {
                    // If trVwAttributes.ExpandDepth > 1 Then
                    if (objSession.TVSelectedNodeDepth >= 2 ||
                         (objSession.TreeItemName != null && (objSession.TreeItemName.Equals("Electrics") ||
                         objSession.TreeItemName.Equals("Smoke")
                         )))
                    {
                        int itemId = objSession.TreeItemId;
                        if (itemId > 0)
                        {
                            loadDynamicControls();
                        }
                    }
                }
                else
                {
                    trVwAttributes.Nodes[0].Expanded = true;

                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion
        #region "Tree Nodes Populate"
        protected void trVwAttributes_TreeNodePopulate(object sender, TreeNodeEventArgs e)
        {
            try
            {
                if (e.Node.ChildNodes.Count == 0)
                {
                    switch ((e.Node.Depth))
                    {
                        case 0:
                            populateLocations(e.Node);
                            break;
                        case 1:
                            if (Convert.ToInt32(e.Node.Value) > 3)
                            {
                                populateItems(e.Node);
                            }
                            else
                            {
                                populateAreas(e.Node);
                            }
                            break;
                        case 2:
                            if (e.Node.Parent.Text == "Services")
                            {
                                populateSubItems(e.Node);
                            }
                            else if (e.Node.Text.CompareTo("Cleaning") == 0)
                            {
                                populateSubItems(e.Node);
                            }
                            else if (e.Node.Text.CompareTo("Water Meters") == 0)
                            {
                                populateWaterMeters(e.Node);
                            }
                            else if (e.Node.Text.CompareTo("Passenger Lift(s)") == 0)
                            {
                                populatePassengerLift(e.Node);
                            }

                            else if (e.Node.Text.CompareTo("Stair Lifts ") == 0)
                            {
                                populateStairlift(e.Node);
                            }

                            else if (e.Node.Parent.Text == "Building Systems")
                            {
                                populateSubItems(e.Node);
                            }
                            else if (e.Node.Text == "Bathroom")
                            {
                                populateSubItems(e.Node);
                            }
                            else if (e.Node.Text == "Laundry room")
                            {
                                populateSubItems(e.Node);
                                //LaundryRoomTypes(e.Node);
                            }
                            else if (e.Node.Text== "Gas")
                            {
                                populateMultipleChilds(e.Node, "Gas");
                            }
                            else if (e.Node.Text == "Electricity")
                            {
                                populateMultipleChilds(e.Node, "Electricity");
                            }
                            else if (e.Node.Text == "Boiler Room")
                            {
                                populateBoilers(e.Node);
                            }
                            else
                            {
                                populateItems(e.Node);
                            }
                            break;
                        case 3:
                            populateSubItems(e.Node);
                            if (e.Node.Text == "Extinguishers")
                            {
                                populateExtinguishers(e.Node);
                            }
                            else if (e.Node.Text == "Blankets")
                            {
                                populateBlankets(e.Node);
                            }
                            else if (e.Node.Text == "Assisted Baths")
                            {
                                populateMultipleChilds(e.Node, "Assisted Baths");
                                //populateBathroomTypes(e.Node);
                            }
                            else if (e.Node.Text == "Hoists")
                            {
                                populateMultipleChilds(e.Node, "Hoists");
                                //                                populateBathroomTypes(e.Node);
                            }
                            else if (e.Node.Text == "Washing Machines")
                            {
                                populateMultipleChilds(e.Node, "Washing Machine");
                                //LaundryRoomTypes(e.Node);
                            }
                            else if (e.Node.Text == "Tumble Dryers")
                            {
                                populateMultipleChilds(e.Node, "Tumble Dryer");
                                //LaundryRoomTypes(e.Node);
                            }
                            break;
                        case 4:
                            populateSubItems(e.Node);
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }


        #endregion

        #region Tree view selected index changed
        protected void trVwAttributes_SelectedNodeChanged(object sender, EventArgs e)
        {
            //check selected node depth to create breadcrump
            try
            {
                UserControl ucItemDetail = (UserControl)ucAttributeDetail.FindControl("ucItemDetail");
                MultiView MainView = (MultiView)ucAttributeDetail.FindControl("MainView");
                LinkButton lnkBtnDetailTab = (LinkButton)ucAttributeDetail.FindControl("lnkBtnDetailTab");
                LinkButton lnkBtnDetectorTab = (LinkButton)ucAttributeDetail.FindControl("lnkBtnDetectorTab");
                UserControl defectTab = (UserControl)ucAttributeDetail.FindControl("defectTab");
                LinkButton lnkBtnAppliancesTab = (LinkButton)ucAttributeDetail.FindControl("lnkBtnAppliancesTab");
                UserControl ucAppliances = (UserControl)ucAttributeDetail.FindControl("ucAppliances");
                Button btnAmend = (Button)ucAttributeDetail.FindControl("btnAmend");
                UserControl uiMessageDetail = (UserControl)ucAttributeDetail.FindControl("uiMessage");
                UserControl detectorTab = (UserControl)ucAttributeDetail.FindControl("detectorTab");
                LinkButton lnkBtnDefectsTab = (LinkButton)ucAttributeDetail.FindControl("lnkBtnDefectsTab");

                LinkButton lnkBtnPhotographsTab = (LinkButton)ucAttributeDetail.FindControl("lnkBtnPhotographsTab");
                LinkButton lnkBtnNotesTab = (LinkButton)ucAttributeDetail.FindControl("lnkBtnNotesTab");

                UserControl ucAttributePhotographs = (UserControl)ucAttributeDetail.FindControl("ucAttributePhotographs");
                UserControl ucAttributeNotes = (UserControl)ucAttributeDetail.FindControl("ucAttributeNotes");

                //UserControl ucAttributeDetail = (UserControl)updPnlAttributes.FindControl("ucAttributeDetail");
                //MultiView MainView = (MultiView)ucAttributeDetail.FindControl("MainView");
                //Button btnAmend = (Button)ucAttributeDetail.FindControl("btnAmend");
                //UserControl ucItemDetail = (UserControl)MainView.FindControl("ucItemDetail");
                Panel pnlOtherDetailsTab = (Panel)ucItemDetail.FindControl("pnlOtherDetailsTab");


                SessionManager.removeIsNotesExist();
                SessionManager.removeIsPhotoGraphExist();
                objSession.ChildeAttributMappingId = 0;
                lnkBtnDefectsTab.CssClass = ApplicationConstants.TabInitialCssClass;
                defectTab.Visible = false;
                lnkBtnDefectsTab.Visible = false;
                lnkBtnNotesTab.Enabled = true;
                lnkBtnPhotographsTab.Enabled = true;
                ucAttributeDetail.Visible = false;
                getQueryString();
                lnkBtnDetectorTab.Visible = false;
                uiMessage.hideMessage();
                UIMessage uiMessageAttrDetail = (UIMessage)uiMessageDetail;
                uiMessageAttrDetail.hideMessage();
                int? schemeId = null;
                int? blockId = null;

                if (requestType == ApplicationConstants.Scheme)
                {
                    schemeId = id;
                }
                else if (requestType == ApplicationConstants.Block)
                {
                    blockId = id;
                }
                lblBreadCrump.Text = string.Empty;

                if (trVwAttributes.SelectedNode.Depth > 1 || ((trVwAttributes.SelectedNode.Text == "Meters" ||
                    trVwAttributes.SelectedNode.Text == "Electrics") &&
                    trVwAttributes.SelectedNode.Parent.Text == "Menu"))
                {
                    if (trVwAttributes.SelectedNode.Depth == 1)
                    {
                        lblBreadCrump.Text = trVwAttributes.SelectedNode.Parent.Text + " > <b>" + trVwAttributes.SelectedNode.Text + "</b>";
                    }
                    else
                    {

                        lblBreadCrump.Text = trVwAttributes.SelectedNode.Parent.Parent.Text + " > <b>" + trVwAttributes.SelectedNode.Parent.Text + " > <b>" + trVwAttributes.SelectedNode.Text + "</b>";
                    }
                }
                if (trVwAttributes.SelectedNode.Depth > 1 || ((trVwAttributes.SelectedNode.Text == "Meters" ||
                    trVwAttributes.SelectedNode.Text == "Electrics") &&
                    trVwAttributes.SelectedNode.Parent.Text == "Menu"))
                {
                    bool showListView = false;
                    objSession.TVSelectedNodeDepth = trVwAttributes.SelectedNode.Depth;
                    objSession.AmendAttribute = false;
                    DataSet attributeResultDataSet = new DataSet();
                    objSession.TreeItemId = Convert.ToInt32(trVwAttributes.SelectedNode.Value);
                    objSession.TreeItemName = trVwAttributes.SelectedNode.Text;
                    DataSet itemDetailDataSet = new DataSet();
                    itemDetailDataSet = objAttributeBL.getItemsByItemId(objSession.TreeItemId);
                    //if (itemDetailDataSet.Tables[0].Rows.Count > 0) {
                    //    SessionManager.setItemDetailDataSet(itemDetailDataSet);
                    //}
                    if (!objSession.TreeItemName.Contains("Water Meter")
                    && !objSession.TreeItemName.Contains("Passenger Lift")
                    && !objSession.TreeItemName.Contains("Stair Lift")
                    && !objSession.TreeItemName.Contains("Assisted Bath")
                    && !objSession.TreeItemName.Contains("Hoist")
                    && !objSession.TreeItemName.Contains("Tumble Dryer")
                    && !objSession.TreeItemName.Contains("Washing Machine")
                    && ! objSession.TreeItemName.Contains("Gas")
                    && !objSession.TreeItemName.Contains("Electricity"))
                    {
                        var itemDetailResult = (from ps in itemDetailDataSet.Tables[0].AsEnumerable() where ps["ItemID"].ToString() == objSession.TreeItemId.ToString() select ps).FirstOrDefault();
                        if (itemDetailResult != null)
                        {
                            showListView = Convert.ToBoolean(itemDetailResult["ShowListView"]);
                        }
                    }
                    if (trVwAttributes.SelectedNode.Target == "Heating")
                    {
                        //lnkBtnDefectsTab.Visible = true;
                        //defectTab.Visible = true;
                        if (MainView.ActiveViewIndex > 1 || ((trVwAttributes.SelectedNode.Text == "Meters" || trVwAttributes.SelectedNode.Text == "Electrics") &&
                            trVwAttributes.SelectedNode.Parent.Text == "Menu"))
                        {
                            lnkBtnDetailTab.CssClass = ApplicationConstants.TabClickedCssClass;
                            MainView.ActiveViewIndex = 1;
                        }
                    }
                    if (showListView && (trVwAttributes.SelectedNode.Target != "CO" && trVwAttributes.SelectedNode.Target != "Smoke"))
                    {
                        ucAttributeDetail.setSelectedMenuItemStyle(ref lnkBtnAppliancesTab);
                        lnkBtnAppliancesTab.Visible = true;
                        lnkBtnDetailTab.Visible = false;
                        Appliance objAppliance = (Appliance)ucAppliances;
                        objAppliance.loadData();
                        MainView.ActiveViewIndex = 3;
                        ucAttributeDetail.Visible = true;
                        btnAmend.Visible = false;
                        lnkBtnAppliancesTab.Text = objSession.TreeItemName;
                    }
                    else if (trVwAttributes.SelectedNode.Target == "CO" || trVwAttributes.SelectedNode.Target == "Smoke")
                    {
                        lnkBtnDetectorTab.Visible = true;
                        lnkBtnDetailTab.Visible = false;
                        detectorTab.Visible = true;
                        MainView.ActiveViewIndex = 4;
                        Detectors detectors = (Detectors)detectorTab;
                        lnkBtnDetectorTab.CssClass = ApplicationConstants.TabClickedCssClass;
                        detectors.loadData();
                        ucAttributeDetail.Visible = true;
                    }
                    else if (trVwAttributes.SelectedNode.Target.Contains("Boiler"))
                    {
                        lnkBtnNotesTab.Enabled = false;
                        lnkBtnPhotographsTab.Enabled = false;
                        if (objSession.TreeItemId != -1)
                        {
                            lnkBtnDefectsTab.Visible = true;
                            lnkBtnNotesTab.Enabled = true;
                            lnkBtnPhotographsTab.Enabled = true;
                            defectTab.Visible = true;
                        }
                        objSession.HeatingMappingId = objSession.TreeItemId;
                        objSession.TreeItemId = objSession.BoilerRoomId;
                        attributeResultDataSet = objAttributeBL.getItemDetail(objSession.BoilerRoomId, schemeId, blockId);
                        objSession.AttributeResultDataSet = attributeResultDataSet;
                        //if (attributeResultDataSet.Tables[ApplicationConstants.Parameters].Rows.Count > 0)
                        //{
                        loadDynamicControls();
                        UserControl ucMSAT = (UserControl)ucItemDetail.FindControl("ucMSAT");
                        ucMSAT.Visible = false;
                        ucAttributeDetail.setSelectedMenuItemStyle(ref lnkBtnDetailTab);
                        lnkBtnAppliancesTab.Visible = false;
                        lnkBtnDetailTab.Visible = true;
                        MainView.ActiveViewIndex = 0;
                        ucAttributeDetail.Visible = true;
                        btnAmend.Visible = true;
                    }
                    else if (trVwAttributes.SelectedNode.Target != "CO" && trVwAttributes.SelectedNode.Target != "Smoke")
                    {
                        if (trVwAttributes.SelectedNode.Text.Contains("Add Water Meter")
                            || trVwAttributes.SelectedNode.Text.Contains("Add Passenger Lift")
                            || trVwAttributes.SelectedNode.Text.Contains("Add Stair Lift")
                            || trVwAttributes.SelectedNode.Text.Contains("Add Gas")
                            || trVwAttributes.SelectedNode.Text.Contains("Add Electricity"))
                        {
                            objSession.TreeItemId = Convert.ToInt32(trVwAttributes.SelectedNode.Parent.Value);
                            objSession.TreeItemName = trVwAttributes.SelectedNode.Parent.Text;
                        }
                        else if (trVwAttributes.SelectedNode.Text.Contains("Add Hoists"))
                        {
                            objSession.TreeItemName = "Hoists";
                        }
                        else if (trVwAttributes.SelectedNode.Text.Contains("Add Assisted Bath"))
                        {
                            objSession.TreeItemName = "Assisted Baths";
                        }
                        else if (trVwAttributes.SelectedNode.Text.Contains("Add Tumble Dryer"))
                        {
                            objSession.TreeItemName = "Tumble Dryer";
                        }
                        else if (trVwAttributes.SelectedNode.Text.Contains("Add Washing Machine"))
                        {
                            objSession.TreeItemName = "Washing Machine";
                        }
                        else if (trVwAttributes.SelectedNode.Text.Contains("Extinguishers"))
                        {
                            objSession.TreeItemName = "Extinguishers";
                        }

                        int? childeAttributMappingId = null;
//                        if (trVwAttributes.SelectedNode.Parent.Text == "Water Meters" && (!trVwAttributes.SelectedNode.Text.Contains("Add Water Meter")))

                        if (trVwAttributes.SelectedNode.Parent.Text == "Water Meters" && (!trVwAttributes.SelectedNode.Text.Contains("Add Water Meter")))
                        {
                            childeAttributMappingId = Convert.ToInt32(trVwAttributes.SelectedNode.Value);
                            objSession.TreeItemId = Convert.ToInt32(trVwAttributes.SelectedNode.Parent.Value);
                            objSession.ChildeAttributMappingId = Convert.ToInt32(childeAttributMappingId);
                        }
                        else if (trVwAttributes.SelectedNode.Parent.Text == "Gas" && (!trVwAttributes.SelectedNode.Text.Contains("Add Gas")))
                        {
                            childeAttributMappingId = Convert.ToInt32(trVwAttributes.SelectedNode.Value);
                            objSession.TreeItemId = Convert.ToInt32(trVwAttributes.SelectedNode.Parent.Value);
                            objSession.ChildeAttributMappingId = Convert.ToInt32(childeAttributMappingId);
                        }
                        else if (trVwAttributes.SelectedNode.Parent.Text == "Electricity" && (!trVwAttributes.SelectedNode.Text.Contains("Add Electricity")))
                        {
                            childeAttributMappingId = Convert.ToInt32(trVwAttributes.SelectedNode.Value);
                            objSession.TreeItemId = Convert.ToInt32(trVwAttributes.SelectedNode.Parent.Value);
                            objSession.ChildeAttributMappingId = Convert.ToInt32(childeAttributMappingId);
                        }
                        else if (trVwAttributes.SelectedNode.Parent.Text == "Passenger Lift(s)" && (!trVwAttributes.SelectedNode.Text.Contains("Add Passenger Lift")))
                        {
                            childeAttributMappingId = Convert.ToInt32(trVwAttributes.SelectedNode.Value);
                            objSession.TreeItemId = Convert.ToInt32(trVwAttributes.SelectedNode.Parent.Value);
                            objSession.ChildeAttributMappingId = Convert.ToInt32(childeAttributMappingId);
                        }

                        else if (trVwAttributes.SelectedNode.Parent.Text == "Stair Lifts " && (!trVwAttributes.SelectedNode.Text.Contains("Add Stair Lift")))
                        {
                            childeAttributMappingId = Convert.ToInt32(trVwAttributes.SelectedNode.Value);
                            objSession.TreeItemId = Convert.ToInt32(trVwAttributes.SelectedNode.Parent.Value);
                            objSession.ChildeAttributMappingId = Convert.ToInt32(childeAttributMappingId);
                        }
                        //                        else if (trVwAttributes.SelectedNode.Parent.Text == "Assisted Baths" && (!trVwAttributes.SelectedNode.Text.Contains("Add Assisted Bath")))
                        else if (trVwAttributes.SelectedNode.Parent.Text == "Assisted Baths")
                        {
                            childeAttributMappingId = Convert.ToInt32(trVwAttributes.SelectedNode.Value);
                            objSession.TreeItemId = Convert.ToInt32(trVwAttributes.SelectedNode.Parent.Value);

                        }
                        //                      else if (trVwAttributes.SelectedNode.Parent.Text == "Hoists" && (!trVwAttributes.SelectedNode.Text.Contains("Add Hoist")))

                        else if (trVwAttributes.SelectedNode.Parent.Text == "Hoists")
                        {
                            childeAttributMappingId = Convert.ToInt32(trVwAttributes.SelectedNode.Value);
                            objSession.TreeItemId = Convert.ToInt32(trVwAttributes.SelectedNode.Parent.Value);
                        }

                        //else if (trVwAttributes.SelectedNode.Parent.Text == "Laundry room" && (!trVwAttributes.SelectedNode.Text.Contains("Add Tumble Dryer")))
                        else if (trVwAttributes.SelectedNode.Parent.Text == "Washing Machines" )
                        {
                            childeAttributMappingId = Convert.ToInt32(trVwAttributes.SelectedNode.Value);
                            objSession.TreeItemId = Convert.ToInt32(trVwAttributes.SelectedNode.Parent.Value);
                        }
                        else if (trVwAttributes.SelectedNode.Parent.Text == "Tumble Dryers" )
                        {
                            childeAttributMappingId = Convert.ToInt32(trVwAttributes.SelectedNode.Value);
                            objSession.TreeItemId = Convert.ToInt32(trVwAttributes.SelectedNode.Parent.Value);
                        }
                        else if (trVwAttributes.SelectedNode.Parent.Text == "Extinguishers" && (!trVwAttributes.SelectedNode.Text.Contains("Add Extinguishers")))
                        {
                            childeAttributMappingId = Convert.ToInt32(trVwAttributes.SelectedNode.Value);
                            objSession.TreeItemId = Convert.ToInt32(trVwAttributes.SelectedNode.Parent.Value);
                            objSession.ChildeAttributMappingId = Convert.ToInt32(childeAttributMappingId);
                        }

                        else if (trVwAttributes.SelectedNode.Parent.Text == "Blankets" && (!trVwAttributes.SelectedNode.Text.Contains("Add Blankets")))
                        {
                            childeAttributMappingId = Convert.ToInt32(trVwAttributes.SelectedNode.Value);
                            objSession.TreeItemId = Convert.ToInt32(trVwAttributes.SelectedNode.Parent.Value);
                            objSession.ChildeAttributMappingId = Convert.ToInt32(childeAttributMappingId);
                        }

                        attributeResultDataSet = objAttributeBL.getItemDetail(objSession.TreeItemId, schemeId, blockId, childeAttributMappingId);
                        objSession.AttributeResultDataSet = attributeResultDataSet;
                        //if (attributeResultDataSet.Tables[ApplicationConstants.Parameters].Rows.Count > 0)
                        //{
                        loadDynamicControls();
                        TextBox Quantity = (TextBox)ucItemDetail.FindControl("txt324");
                        if (Quantity != null)
                            Quantity.Text = String.Empty;
                        //}
                        UserControl ucMSAT = (UserControl)ucItemDetail.FindControl("ucMSAT");
                        MaintenanceServicingAndTesting objMST = (MaintenanceServicingAndTesting)ucMSAT;
                        objMST.resetControls();
                        objMST.populateData();
                        if (trVwAttributes.SelectedNode.Text.Contains("Add Water Meter")
                            || trVwAttributes.SelectedNode.Text.Contains("Add Passenger Lift")
                            || trVwAttributes.SelectedNode.Text.Contains("Add Stair Lift")
                            || trVwAttributes.SelectedNode.Text.Contains("Add Hoists")
                            || trVwAttributes.SelectedNode.Text.Contains("Add Assisted Bath")
                            || trVwAttributes.SelectedNode.Text.Contains("Add Tumble Dryer")
                            || trVwAttributes.SelectedNode.Text.Contains("Add Washing Machine")
                            || trVwAttributes.SelectedNode.Text.Contains("Add Gas")
                            || trVwAttributes.SelectedNode.Text.Contains("Add Electricity"))
                        {
                            objMST.resetControls();
                        }
                        ucMSAT.Visible = true;
                        ucAttributeDetail.setSelectedMenuItemStyle(ref lnkBtnDetailTab);
                        lnkBtnAppliancesTab.Visible = false;
                        lnkBtnDetailTab.Visible = true;
                        MainView.ActiveViewIndex = 0;
                        ucAttributeDetail.Visible = true;
                        btnAmend.Visible = true;
                    }

                }
                if (isReadOnly)
                    btnAmend.Visible = false;

                AttributePhotographs attributePhotographs = (AttributePhotographs)ucAttributePhotographs;
                attributePhotographs.loadData();

                if (SessionManager.getIsPhotoGraphExist() != null)
                {
                    bool isPhotoGraphExist = Convert.ToBoolean(SessionManager.getIsPhotoGraphExist());
                    if (isPhotoGraphExist)
                    {
                        lnkBtnPhotographsTab.Text =
                        "Photographs: <img src='../../../../Images/CameraIcon.png'  width='16px' height='12px' /> ";
                    }
                    else
                    {
                        lnkBtnPhotographsTab.Text = "Photographs: ";
                    }
                }

                AttributeNotes attributeNotes = (AttributeNotes)ucAttributeNotes;
                attributeNotes.loadData();
                if (SessionManager.getIsNotesExist() != null)
                {
                    bool isNotesExist = Convert.ToBoolean(SessionManager.getIsNotesExist());
                    if (isNotesExist)
                    {
                        lnkBtnNotesTab.Text =
                        "Notes: <img src='../../../../Images/notes_icon.png'  width='12px' height='12px' /> ";
                    }
                    else
                    {
                        lnkBtnNotesTab.Text = "Notes: ";
                    }
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion


        #endregion

        #region "Populate Tree - Functions"

        #region "Populate Location"
        /// <summary>
        /// Populate Location
        /// </summary>
        /// <param name="treeNode"></param>
        /// <remarks></remarks>
        private void populateLocations(TreeNode treeNode)
        {
            //int count = 1;
            DataSet resultDataSet = new DataSet();
            try
            {
                resultDataSet = objAttributeBL.getLocations();

                if (resultDataSet.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in resultDataSet.Tables[0].Rows)
                    {
                        TreeNode newNode = new TreeNode();
                        newNode.PopulateOnDemand = true;
                        newNode.Text = row["LocationName"].ToString();
                        newNode.Value = row["LocationID"].ToString();
                        newNode.Target = row["Location"].ToString();
                        if (!newNode.Target.Equals("Item"))
                            newNode.SelectAction = TreeNodeSelectAction.None;
                        treeNode.ChildNodes.Add(newNode);
                    }

                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region "Populate Area"
        /// <summary>
        /// Populate Area
        /// </summary>
        /// <param name="treeNode"></param>
        /// <remarks></remarks>
        private void populateAreas(TreeNode treeNode)
        {
            DataSet resultDataSet = new DataSet();

            try
            {
                int locationId = Convert.ToInt32(treeNode.Value);
                resultDataSet = objAttributeBL.getAreasByLocationId(locationId);

                if (resultDataSet.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow row in resultDataSet.Tables[0].Rows)
                    {
                        TreeNode newNode = new TreeNode();
                        newNode.PopulateOnDemand = true;
                        newNode.Text = row["AreaName"].ToString();
                        newNode.Value = row["AreaID"].ToString();
                        newNode.Target = "area";
                        newNode.SelectAction = TreeNodeSelectAction.None;
                        treeNode.ChildNodes.Add(newNode);
                    }
                }

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region "Populate Items"
        /// <summary>
        /// Populate Items
        /// </summary>
        /// <param name="treeNode"></param>
        /// <remarks></remarks>
        private void populateItems(TreeNode treeNode)
        {
            DataSet resultDataSet = new DataSet();

            try
            {
                int areaId = Convert.ToInt32(treeNode.Value);
                resultDataSet = objAttributeBL.getItemsByAreaId(areaId);

                if (resultDataSet.Tables[0].Rows.Count > 0)
                {
                    // SessionManager.setItemDetailDataSet(resultDataSet);
                    foreach (DataRow row in resultDataSet.Tables[0].Rows)
                    {
                        TreeNode newNode = new TreeNode();
                        newNode.PopulateOnDemand = true;
                        newNode.Text = row["ItemName"].ToString();
                        newNode.Value = row["ItemID"].ToString();
                        newNode.Target = row["ItemName"].ToString();
                        if (row["ItemName"].ToString() == "Boiler Room")
                        {
                            newNode.SelectAction = TreeNodeSelectAction.None;
                            objSession.BoilerRoomId = Convert.ToInt32(row["ItemID"].ToString());
                        }
                        //else if (row["ItemName"].ToString() == " Cleaning")
                        //{
                        //    newNode.SelectAction = TreeNodeSelectAction.None;
                        //}
                        //else if (row["ItemName"].ToString() == "Water Meter(s)")
                        //{
                        //    newNode.SelectAction = TreeNodeSelectAction.None;
                        //}
                        //else if (row["ItemName"].ToString() == "Passenger Lift(s)")
                        //{
                        //    newNode.SelectAction = TreeNodeSelectAction.None;
                        //}
                        else if (row["ItemName"].ToString() == "Stair Lifts "
                            || row["ItemName"].ToString() == "Bathroom"
                            || row["ItemName"].ToString() == "Assisted Baths"
                            || row["ItemName"].ToString() == "Passenger Lift(s)"
                            || row["ItemName"].ToString() == "Water Meters"
                            || row["ItemName"].ToString() == " Cleaning"
                            || row["ItemName"].ToString() == " Laundry room"
                            || row["ItemName"].ToString() == " 	Extinguishers"
                            || row["ItemName"].ToString() == "	Blankets"
                            || row["ItemName"].ToString()== "Gas"
                            || row["ItemName"].ToString()== "Electricity")
                        {
                            newNode.SelectAction = TreeNodeSelectAction.None;
                        }

                        treeNode.ChildNodes.Add(newNode);
                    }
                }

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region "Populate Sub Items"
        /// <summary>
        /// Populate Sub Items
        /// </summary>
        /// <param name="treeNode"></param>
        /// <remarks></remarks>
        private void populateSubItems(TreeNode treeNode)
        {
            DataSet resultDataSet = new DataSet();

            try
            {
                int itemId = Convert.ToInt32(treeNode.Value);
                resultDataSet = objAttributeBL.getSubItemsByItemId(itemId);

                if (resultDataSet.Tables[0].Rows.Count > 0)
                {
                    //SessionManager.setItemDetailDataSet(resultDataSet);
                    foreach (DataRow row in resultDataSet.Tables[0].Rows)
                    {
                        TreeNode newNode = new TreeNode();
                        newNode.PopulateOnDemand = true;
                        newNode.Text = row["ItemName"].ToString();
                        newNode.Value = row["ItemID"].ToString();
                        newNode.Target = row["ItemName"].ToString();
                        treeNode.ChildNodes.Add(newNode);
                        if (row["ItemName"].ToString() == "Assisted Baths"
                            || row["ItemName"].ToString() == "Hoists"
                            || row["ItemName"].ToString() == "Washing Machines"
                            || row["ItemName"].ToString() == "Tumble Dryers")
                        {
                            newNode.SelectAction = TreeNodeSelectAction.None;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion


        #region "Populate Items"
        /// <summary>
        /// Populate Items
        /// </summary>
        /// <param name="treeNode"></param>
        /// <remarks></remarks>
        private void populateBoilers(TreeNode treeNode)
        {
            DataSet resultDataSet = new DataSet();
            int? schemeId = null;
            int? blockId = null;
            getQueryString();
            if (requestType == ApplicationConstants.Scheme)
            {
                schemeId = id;
            }
            else if (requestType == ApplicationConstants.Block)
            {
                blockId = id;
            }
            try
            {
                int areaId = Convert.ToInt32(treeNode.Value);
                resultDataSet = objAttributeBL.getBoilerslist(schemeId, blockId);

                if (resultDataSet.Tables[0].Rows.Count > 0)
                {
                    int count = 1;
                    foreach (DataRow row in resultDataSet.Tables[0].Rows)
                    {
                        TreeNode newNode = new TreeNode();
                        newNode.PopulateOnDemand = true;
                        newNode.Text = "Boiler " + count.ToString();
                        newNode.Value = row["HeatingMappingId"].ToString();
                        newNode.Target = "Boiler Room";
                        treeNode.ChildNodes.Add(newNode);
                        count++;
                    }
                }
                TreeNode newAddNode = new TreeNode();
                newAddNode.PopulateOnDemand = true;
                newAddNode.Text = "<img alt='' src='../../../../Images/plus.png' width=12px height=12px />  Add Boiler";
                newAddNode.Value = "-1";
                newAddNode.Target = "Boiler Room";
                newAddNode.Expanded = true;

                treeNode.ChildNodes.Add(newAddNode);


            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        private void populateWaterMeters(TreeNode treeNode)
        {
            DataSet resultDataSet = new DataSet();
            int? schemeId = null;
            int? blockId = null;
            getQueryString();
            if (requestType == ApplicationConstants.Scheme)
            {
                schemeId = id;
            }
            else if (requestType == ApplicationConstants.Block)
            {
                blockId = id;
            }
            try
            {
                int areaId = Convert.ToInt32(treeNode.Value);
                resultDataSet = objAttributeBL.getMultiAttributelistByItemId(schemeId, blockId, areaId);

                if (resultDataSet.Tables[0].Rows.Count > 0)
                {
                    int count = 1;
                    foreach (DataRow row in resultDataSet.Tables[0].Rows)
                    {
                        TreeNode newNode = new TreeNode();
                        newNode.PopulateOnDemand = true;
                        newNode.Text = "Water Meter " + count.ToString();
                        newNode.Value = row["Id"].ToString();
                        newNode.Target = "Water Meters";
                        treeNode.ChildNodes.Add(newNode);
                        count++;
                    }
                }
                TreeNode newAddNode = new TreeNode();
                newAddNode.PopulateOnDemand = true;
                newAddNode.Text = "<img alt='' src='../../../../Images/plus.png' width=12px height=12px />  Add Water Meter";
                newAddNode.Value = "-1";
                newAddNode.Target = "Water Meters";
                newAddNode.Expanded = true;

                treeNode.ChildNodes.Add(newAddNode);


            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }


        }


        private void populateExtinguishers(TreeNode treeNode)
        {
            DataSet resultDataSet = new DataSet();
            int? schemeId = null;
            int? blockId = null;
            getQueryString();
            if (requestType == ApplicationConstants.Scheme)
            {
                schemeId = id;
            }
            else if (requestType == ApplicationConstants.Block)
            {
                blockId = id;
            }
            try
            {
                int areaId = Convert.ToInt32(treeNode.Value);
                resultDataSet = objAttributeBL.getMultiAttributelistByItemId(schemeId, blockId, areaId);

                if (resultDataSet.Tables[0].Rows.Count > 0)
                {
                    int count = 1;
                    foreach (DataRow row in resultDataSet.Tables[0].Rows)
                    {
                        TreeNode newNode = new TreeNode();
                        newNode.PopulateOnDemand = true;
                        newNode.Text = row["AttributeName"].ToString() + count.ToString();
                        newNode.Value = row["Id"].ToString();
                        newNode.Target = "Extinguishers";
                        treeNode.ChildNodes.Add(newNode);
                        count++;
                    }
                }


            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }


        }

        private void populateBlankets(TreeNode treeNode)
        {
            DataSet resultDataSet = new DataSet();
            int? schemeId = null;
            int? blockId = null;
            getQueryString();
            if (requestType == ApplicationConstants.Scheme)
            {
                schemeId = id;
            }
            else if (requestType == ApplicationConstants.Block)
            {
                blockId = id;
            }
            try
            {
                int areaId = Convert.ToInt32(treeNode.Value);
                resultDataSet = objAttributeBL.getMultiAttributelistByItemId(schemeId, blockId, areaId);

                if (resultDataSet.Tables[0].Rows.Count > 0)
                {
                    int count = 1;
                    foreach (DataRow row in resultDataSet.Tables[0].Rows)
                    {
                        TreeNode newNode = new TreeNode();
                        newNode.PopulateOnDemand = true;
                        newNode.Text = row["AttributeName"].ToString() + count.ToString();
                        newNode.Value = row["Id"].ToString();
                        newNode.Target = "Extinguishers";
                        treeNode.ChildNodes.Add(newNode);
                        count++;
                    }

                }


            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }


        }

        private void populateStairlift(TreeNode treeNode)
        {
            DataSet resultDataSet = new DataSet();
            int? schemeId = null;
            int? blockId = null;
            getQueryString();
            if (requestType == ApplicationConstants.Scheme)
            {
                schemeId = id;
            }
            else if (requestType == ApplicationConstants.Block)
            {
                blockId = id;
            }
            try
            {
                int areaId = Convert.ToInt32(treeNode.Value);
                resultDataSet = objAttributeBL.getMultiAttributelistByItemId(schemeId, blockId, areaId);

                if (resultDataSet.Tables[0].Rows.Count > 0)
                {
                    int count = 1;
                    foreach (DataRow row in resultDataSet.Tables[0].Rows)
                    {
                        TreeNode newNode = new TreeNode();
                        newNode.PopulateOnDemand = true;
                        newNode.Text = "Stair Lift" + count.ToString();
                        newNode.Value = row["Id"].ToString();
                        newNode.Target = "Stair Lifts ";
                        treeNode.ChildNodes.Add(newNode);
                        count++;
                    }
                }
                TreeNode newAddNode = new TreeNode();
                newAddNode.PopulateOnDemand = true;
                newAddNode.Text = "<img alt='' src='../../../../Images/plus.png' width=12px height=12px />  Add Stair Lift";
                newAddNode.Value = "-1";
                newAddNode.Target = "Stair Lifts ";
                newAddNode.Expanded = true;

                treeNode.ChildNodes.Add(newAddNode);


            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }


        }


        private void populateMultipleChilds(TreeNode treeNode, string childName)
        {
            DataSet resultDataSet = new DataSet();
            int? schemeId = null;
            int? blockId = null;
            getQueryString();
            if (requestType == ApplicationConstants.Scheme)
            {
                schemeId = id;
            }
            else if (requestType == ApplicationConstants.Block)
            {
                blockId = id;
            }
            try
            {
                int areaId = Convert.ToInt32(treeNode.Value);
                resultDataSet = objAttributeBL.getMultiAttributelistByItemId(schemeId, blockId, areaId);

                if (resultDataSet.Tables[0].Rows.Count > 0)
                {
                    int Count = 1;
                    foreach (DataRow row in resultDataSet.Tables[0].Rows)
                    {
                        TreeNode newNode = new TreeNode();
                        newNode.PopulateOnDemand = true;
                        if (row["AttributeName"].ToString().Contains(childName))
                        {
                            newNode.Text = childName + Count.ToString();
                            newNode.Value = row["Id"].ToString();
                            Count++;
                        }
                        newNode.Target = treeNode.Parent.Text;
                        treeNode.ChildNodes.Add(newNode);
                    }
                }
                TreeNode newAddNode = new TreeNode();
                newAddNode.PopulateOnDemand = true;
                newAddNode.Text = "<img alt='' src='../../../../Images/plus.png' width=12px height=12px />  Add " + childName;
                newAddNode.Value = "-1";
                newAddNode.Target = "Bathroom";
                newAddNode.Expanded = true;
                treeNode.ChildNodes.Add(newAddNode);
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }


        }


        private void populateBathroomTypes(TreeNode treeNode)
        {
            DataSet resultDataSet = new DataSet();
            int? schemeId = null;
            int? blockId = null;
            getQueryString();
            if (requestType == ApplicationConstants.Scheme)
            {
                schemeId = id;
            }
            else if (requestType == ApplicationConstants.Block)
            {
                blockId = id;
            }
            try
            {
                int areaId = Convert.ToInt32(treeNode.Value);
                resultDataSet = objAttributeBL.getMultiAttributelistByItemId(schemeId, blockId, areaId);

                if (resultDataSet.Tables[0].Rows.Count > 0)
                {
                    int assistedBathsCount = 1;
                    int hoistsCount = 1;
                    foreach (DataRow row in resultDataSet.Tables[0].Rows)
                    {
                        TreeNode newNode = new TreeNode();
                        newNode.PopulateOnDemand = true;
                        if (row["AttributeName"].ToString().Contains("Assisted Bath"))
                        {
                            newNode.Text = "Assisted Baths" + assistedBathsCount.ToString();
                            newNode.Value = row["Id"].ToString();
                            assistedBathsCount++;
                        }
                        else if (row["AttributeName"].ToString().Contains("Hoists"))
                        {
                            newNode.Text = "Hoists" + hoistsCount.ToString();
                            newNode.Value = row["Id"].ToString();
                            hoistsCount++;
                        }
                        newNode.Target = treeNode.Parent.Text;
                        treeNode.ChildNodes.Add(newNode);
                    }
                }
                TreeNode newAddNode = new TreeNode();
                newAddNode.PopulateOnDemand = true;
                newAddNode.Text = "<img alt='' src='../../../../Images/plus.png' width=12px height=12px />  Add Assisted Bath";
                newAddNode.Value = "-1";
                newAddNode.Target = "Bathroom";
                newAddNode.Expanded = true;
                treeNode.ChildNodes.Add(newAddNode);
                newAddNode = new TreeNode();
                newAddNode.PopulateOnDemand = true;
                newAddNode.Text = "<img alt='' src='../../../../Images/plus.png' width=12px height=12px />  Add Hoists";
                newAddNode.Value = "-2";
                newAddNode.Target = "Bathroom";
                newAddNode.Expanded = true;
                treeNode.ChildNodes.Add(newAddNode);



            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }


        }


        private void LaundryRoomTypes(TreeNode treeNode)
        {
            DataSet resultDataSet = new DataSet();
            int? schemeId = null;
            int? blockId = null;
            getQueryString();
            if (requestType == ApplicationConstants.Scheme)
            {
                schemeId = id;
            }
            else if (requestType == ApplicationConstants.Block)
            {
                blockId = id;
            }
            try
            {
                int areaId = Convert.ToInt32(treeNode.Value);
                resultDataSet = objAttributeBL.getMultiAttributelistByItemId(schemeId, blockId, areaId);

                if (resultDataSet.Tables[0].Rows.Count > 0)
                {
                    int WashingMachineCount = 1;
                    int TumbleDryerCount = 1;
                    foreach (DataRow row in resultDataSet.Tables[0].Rows)
                    {
                        TreeNode newNode = new TreeNode();
                        newNode.PopulateOnDemand = true;
                        if (row["AttributeName"].ToString().Contains("Washing Machine"))
                        {
                            newNode.Text = "Washing Machine" + WashingMachineCount.ToString();
                            newNode.Value = row["Id"].ToString();
                            WashingMachineCount++;
                        }
                        else if (row["AttributeName"].ToString().Contains("Tumble Dryer"))
                        {
                            newNode.Text = "Tumble Dryer" + TumbleDryerCount.ToString();
                            newNode.Value = row["Id"].ToString();
                            TumbleDryerCount++;
                        }
                        newNode.Target = "Laundry room";
                        treeNode.ChildNodes.Add(newNode);
                    }
                }
                TreeNode newAddNode = new TreeNode();
                newAddNode.PopulateOnDemand = true;
                newAddNode.Text = "<img alt='' src='../../../../Images/plus.png' width=12px height=12px />  Add Washing Machine";
                newAddNode.Value = "-1";
                newAddNode.Target = "Laundry room";
                newAddNode.Expanded = true;
                treeNode.ChildNodes.Add(newAddNode);
                newAddNode = new TreeNode();
                newAddNode.PopulateOnDemand = true;
                newAddNode.Text = "<img alt='' src='../../../../Images/plus.png' width=12px height=12px />  Add Tumble Dryer";
                newAddNode.Value = "-2";
                newAddNode.Target = "Laundry room";
                newAddNode.Expanded = true;
                treeNode.ChildNodes.Add(newAddNode);



            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }


        }



        private void populatePassengerLift(TreeNode treeNode)
        {
            DataSet resultDataSet = new DataSet();
            int? schemeId = null;
            int? blockId = null;
            getQueryString();
            if (requestType == ApplicationConstants.Scheme)
            {
                schemeId = id;
            }
            else if (requestType == ApplicationConstants.Block)
            {
                blockId = id;
            }
            try
            {
                int areaId = Convert.ToInt32(treeNode.Value);
                resultDataSet = objAttributeBL.getMultiAttributelistByItemId(schemeId, blockId, areaId);

                if (resultDataSet.Tables[0].Rows.Count > 0)
                {
                    int count = 1;
                    foreach (DataRow row in resultDataSet.Tables[0].Rows)
                    {
                        TreeNode newNode = new TreeNode();
                        newNode.PopulateOnDemand = true;
                        newNode.Text = "Passenger Lift" + count.ToString();
                        newNode.Value = row["Id"].ToString();
                        newNode.Target = "Passenger Lift";
                        treeNode.ChildNodes.Add(newNode);
                        count++;
                    }
                }
                TreeNode newAddNode = new TreeNode();
                newAddNode.PopulateOnDemand = true;
                newAddNode.Text = "<img alt='' src='../../../../Images/plus.png' width=12px height=12px />  Add Passenger Lift";
                newAddNode.Value = "-1";
                newAddNode.Target = "Passenger Lift";
                newAddNode.Expanded = true;

                treeNode.ChildNodes.Add(newAddNode);


            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }


        }

        #endregion
        #endregion

        #region get Query String values
        private void getQueryString()
        {

            if ((Request.QueryString[ApplicationConstants.Id] != null))
            {
                id = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);

            }

            if ((Request.QueryString[ApplicationConstants.RequestType] != null))
            {

                requestType = Request.QueryString[ApplicationConstants.RequestType];
            }
        }
        #endregion

        #region "load Dynamic Controls"
        /// <summary>
        /// load Dynamic Controls
        /// </summary>
        /// <param name="itemId"></param>
        /// <remarks></remarks>
        public void loadDynamicControls()
        {
            try
            {
                UserControl ucAttributeDetail = (UserControl)updPnlAttributes.FindControl("ucAttributeDetail");
                MultiView MainView = (MultiView)ucAttributeDetail.FindControl("MainView");
                Button btnAmend = (Button)ucAttributeDetail.FindControl("btnAmend");
                UserControl ucItemDetail = (UserControl)MainView.FindControl("ucItemDetail");
                Panel pnlOtherDetailsTab = (Panel)ucItemDetail.FindControl("pnlOtherDetailsTab");
                //UserControl ucMSAT = (UserControl)ucItemDetail.FindControl("ucMSAT");
                Table tblOtherDetails = new Table();
                tblOtherDetails.ID = "tblOtherDetails";
                DataSet parameterDataSet = objSession.AttributeResultDataSet;
                pnlOtherDetailsTab.Controls.Clear();
                if (MainView.ActiveViewIndex == 0)
                {
                    btnAmend.Visible = true;
                }
                else
                {
                    btnAmend.Visible = false;
                }

                if (parameterDataSet != null && parameterDataSet.Tables[0].Rows.Count > 0)
                {
                    //Please  don't delete commented area of code.
                    /////////////////////////////////////////////////
                    //if (objSession.AmendAttribute || (parameterDataSet.Tables[ApplicationConstants.MST].Rows.Count == 0 && parameterDataSet.Tables[ApplicationConstants.PreInsertedValues].Rows.Count == 0))
                    //{

                    tblOtherDetails = ControlFactoryBL.createDynamicControls(parameterDataSet, false);
                    //MaintenanceServicingAndTesting objMST = (MaintenanceServicingAndTesting)ucMSAT;

                    //objMST.populateData();
                    //}
                    //else
                    //{
                    //    tblOtherDetails = ControlFactoryBL.createDynamicControls(parameterDataSet,true );
                    //    MaintenanceServicingAndTesting objMST = (MaintenanceServicingAndTesting)ucMSAT;
                    //    objMST.populateMSTReadOnly ();
                    //}
                }
                pnlOtherDetailsTab.Controls.Add(tblOtherDetails);
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        #endregion

        public void reloadTreeView()
        {
            TreeNode addBoilerNode = trVwAttributes.SelectedNode;
            if (addBoilerNode.Text.Contains("Boiler")
                || addBoilerNode.Text.Contains("Passenger Lift")
                || addBoilerNode.Text.Contains("Stair Lift")
                || addBoilerNode.Text.Contains("Water Meter")
                || addBoilerNode.Text.Contains("Assisted Baths")
                || addBoilerNode.Text.Contains("Hoists")
                || addBoilerNode.Text.Contains("Tumble Dryers")
                || addBoilerNode.Text.Contains("Washing Machines")
                || addBoilerNode.Text.Contains("Gas")
                || addBoilerNode.Text.Contains("Electricity")
                )
            {
                TreeNodeEventArgs addBoilerevnt = new TreeNodeEventArgs(addBoilerNode);
                trVwAttributes_SelectedNodeChanged(addBoilerNode, addBoilerevnt);
            }

            TreeNode node = trVwAttributes.SelectedNode.Parent;
            if (node.Text == "Boiler Room"
                || node.Text == "Passenger Lift(s)"
                || node.Text == "Stair Lifts "
                || node.Text == "Water Meters"
                || node.Text == "Assisted Baths"
                || node.Text == "Laundry room"
                || node.Text == "Hoists"               
                || node.Text == "Washing Machines"
                || node.Text == "Tumble Dryers"
                || node.Text== "Gas"
                || node.Text== "Electricity")
            {
                node.ChildNodes.Clear();
                node.CollapseAll();
                node.Expand();
                TreeNodeEventArgs evnt = new TreeNodeEventArgs(node);
                trVwAttributes_TreeNodePopulate(node, evnt);
                for (int i = 0; i < node.ChildNodes.Count; i++)
                {
                    if (lblBreadCrump.Text.Contains(node.ChildNodes[i].Text))
                    {
                        node.ChildNodes[i].Select();
                        
                    }

                }

            }
        }
    }
}