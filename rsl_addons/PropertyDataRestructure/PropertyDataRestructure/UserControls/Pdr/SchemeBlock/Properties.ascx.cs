﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PropertyDataRestructure.Base;
using PropertyDataRestructure.Interface;
using PDR_BusinessLogic.Block;
using System.Data;
using PDR_BusinessObject.PageSort;
using PDR_DataAccess.Block;
using PDR_Utilities.Constants;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PropertyDataRestructure.Views.Pdr.Scheme.Dashboard;
using PDR_Utilities.Helpers;

namespace PropertyDataRestructure.UserControls.Pdr.SchemeBlock
{
    public partial class Properties : UserControlBase,IListingPage
    {
        int id = 0;
        PageSortBO objPageSortBo = new PageSortBO("DESC", "Ref", 1, 30);
        string requestType = string.Empty;
        Boolean isReadOnly = false;
        String pageName = "";
        public Action<bool, int, LinkButton> delegateInvoke { get; set; }
        #region "Events"
        #region "Page Load"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            pageName = System.IO.Path.GetFileName(Request.Url.ToString());
            if (pageName.Contains(PathConstants.SchemeRecordPage))
                isReadOnly = true;
            if (isReadOnly)
            {
                btnAddProperty.Visible = false;
                btnRemoveProperties.Visible = false;
            }
            if (!IsPostBack)
            {
                //getQueryString();
                //loadData();
            }
        }
        #endregion

        #region "btn RemoveProperties Click"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRemoveProperties_Click(object sender, EventArgs e)
        {
            try
            {
                BlockBL objBlockBL = new BlockBL(new BlockRepo());
                foreach (GridViewRow row in grdProperties.Rows)
                {
                    CheckBox chkProperty = (CheckBox)row.FindControl("chkProperty");
                    if (chkProperty.Checked == true)
                    {
                        HiddenField propertyId = (HiddenField)row.FindControl("hdnPropertyId");
                        string PropertyRef = propertyId.Value;
                        objBlockBL.removeProperties(PropertyRef, requestType);
                    }
                }
                loadData();
                updpnlProperties.Update();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region "btn AddProperty Click"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAddProperty_Click(object sender, EventArgs e)
        {

        }
        #endregion
        #region Change Page Number
        /// <summary>
        ///  Change Page Number
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>  
        protected void lnkbtnPager_Click(object sender, EventArgs e)
        {
            try
            {
                PageSortBO objPageSortBo = new PageSortBO("DESC", "Ref", 1, 30);
                GridHelper.processGridPager(sender, objPageSortBo, pageSortViewState);
                loadData();

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        #endregion

        #region "grdRentAnalysis Sorting"
        protected void grdProperties_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                objPageSortBo = pageSortViewState;

                objPageSortBo.SortExpression = e.SortExpression;
                objPageSortBo.PageNumber = 1;
                grdProperties.PageIndex = 0;
                objPageSortBo.setSortDirection();

                pageSortViewState = objPageSortBo;
                loadData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion
        #endregion

        #region "Functions"

        #region "load Data"
        /// <summary>
        /// 
        /// </summary>
        public void loadData()
        {
            try
            {
                getQueryString();
                int totalCount = 0;
                BlockBL objBlockBL = new BlockBL(new BlockRepo());
                DataSet propertiesDataSet = new DataSet();
                PageSortBO objPageSortBo;
                if (pageSortViewState == null)
                {
                    objPageSortBo = new PageSortBO("DESC", "Ref", 1, 30);
                    pageSortViewState = objPageSortBo;
                }
                else
                {
                    objPageSortBo = pageSortViewState;
                }


                totalCount = objBlockBL.getProperties(ref propertiesDataSet, id, objPageSortBo, requestType);
                grdProperties.DataSource = propertiesDataSet;
                grdProperties.DataBind();
                if (propertiesDataSet.Tables[0].Rows.Count > 0)
                {
                    this.btnRemoveProperties.Enabled = true;                    
                }
                else
                {
                    this.btnRemoveProperties.Enabled = false;
                }

                objPageSortBo.TotalRecords = totalCount;
                objPageSortBo.TotalPages = (int)Math.Ceiling(Convert.ToDecimal(totalCount) / objPageSortBo.PageSize);

                if (objPageSortBo.TotalPages > 0)
                {
                    pnlPagination.Visible = true;
                }
                else
                {
                    pnlPagination.Visible = false;
                    uiMessage.showErrorMessage(UserMessageConstants.RecordNotFound);
                }
                pageSortViewState = objPageSortBo;
                GridHelper.setGridViewPager(ref pnlPagination, objPageSortBo);
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region "search Data"
        /// <summary>
        /// 
        /// </summary>
        public void searchData()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region "populate Data"
        /// <summary>
        /// 
        /// </summary>
        public void populateData()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region "printData"
        /// <summary>
        /// 
        /// </summary>
        public void printData()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region "exportGrid To Excel"
        /// <summary>
        /// 
        /// </summary>
        public void exportGridToExcel()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region "export To Pdf"
        /// <summary>
        /// 
        /// </summary>
        public void exportToPdf()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region "download Data"
        /// <summary>
        /// 
        /// </summary>
        public void downloadData()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region "apply Filters"
        /// <summary>
        /// 
        /// </summary>
        public void applyFilters()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region getQueryString()
        /// <summary>
        /// 
        /// </summary>
        private void getQueryString()
        {
            try
            {
                if ((Request.QueryString[ApplicationConstants.Id] != null))
                {
                    id = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);

                }

                if ((Request.QueryString[ApplicationConstants.RequestType] != null))
                {

                    requestType = Request.QueryString[ApplicationConstants.RequestType];
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        protected void grdProperties_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblRef = (Label) e.Row.FindControl ("lblRef"); 
                Button btnAdd = e.Row.FindControl("btnView") as Button;
                btnAdd.Attributes.Add("onclick", "return ShowPropertyDetail('" + lblRef.Text + "');");
            }
        }

        public void loadProperties(int id, string requestType)
        {
            BlockBL objBlockBL = new BlockBL(new BlockRepo());
            DataSet propertiesDataSet = new DataSet();
            PageSortBO objPageSortBO = new PageSortBO("DESC", "Ref", 1, 30);
            objBlockBL.getProperties(ref propertiesDataSet, id, objPageSortBO, requestType);
            grdProperties.DataSource = propertiesDataSet;
            grdProperties.DataBind();
            if (propertiesDataSet.Tables[0].Rows.Count > 0)
            {
                this.btnRemoveProperties.Enabled = true;
            }
            else
            {
                this.btnRemoveProperties.Enabled = false;
            }
            LinkButton lnkBtnProperties = (LinkButton)Parent.FindControl("lnkBtnProperties");
           // SchemeDashBoard objSchemeDashBoard = (SchemeDashBoard)Parent;
            ((SchemeRecord)this.Page).enableDisableTabs(true, 4, lnkBtnProperties);
            //if (this.delegateInvoke != null)
            //{
            //    this.delegateInvoke(true, 6, lnkBtnProperties);   
            //}

        }
        #endregion
    }
}