﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PropertyDataRestructure.Base;

namespace PropertyDataRestructure.UserControls.Pdr.SchemeBlock
{
    public partial class ItemDetail : UserControlBase
    {
        public bool _readOnly = false;
        protected void Page_Load(object sender, EventArgs e)
        {
            string PropPageName = Request.Path.ToString();
            if (PropPageName.Contains("SchemeDashBoard"))
            {
                _readOnly = false;
            }
            else if (PropPageName.Contains("SchemeRecord"))
            {
                _readOnly = true;
            }
            if (_readOnly == true)
            {
                pnlDetailsTab.Enabled = false;
                pnlOtherDetailsTab.Enabled = false;
                pnlOtherDetailsTab.Enabled = false;
            }
        }


    }
}