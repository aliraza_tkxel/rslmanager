﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProvisionItemDetail.ascx.cs" Inherits="PropertyDataRestructure.UserControls.Pdr.SchemeBlock.ProvisionItemDetail" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="attr" TagName="MSAT" Src="~/UserControls/Pdr/SchemeBlock/MaintenanceServicingAndTesting.ascx" %>
<%@ Register TagPrefix="attr" TagName="ProvisionItem" Src="~/UserControls/Pdr/SchemeBlock/ProvisionItem.ascx" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>

<style type="text/css">
    td, th {
    padding: 2px !important;
}
</style>

<asp:Panel runat="server" ID="pnlDetailsTab">
    <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="500px" />   
</asp:Panel>

<asp:UpdatePanel ID="updPanelItemDetail" runat="server">
    <ContentTemplate>
<attr:ProvisionItem runat="server" ID="ucProvisionItem" />
<attr:MSAT runat="server" ID="ucMSAT" />
</ContentTemplate> 
</asp:UpdatePanel>