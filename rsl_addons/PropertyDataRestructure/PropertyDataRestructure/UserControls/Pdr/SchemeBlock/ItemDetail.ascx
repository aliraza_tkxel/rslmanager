﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ItemDetail.ascx.cs"
    Inherits="PropertyDataRestructure.UserControls.Pdr.SchemeBlock.ItemDetail" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="attr" TagName="MSAT" Src="~/UserControls/Pdr/SchemeBlock/MaintenanceServicingAndTesting.ascx" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<script type="text/javascript" charset="utf-8">

  
    //Function to change color of grid row on mouseover
    function mouseIn(row) {
        row.style.backgroundColor = '#e6e6e6';
    }
    //Function to change color of grid row on mouseout
    function mouseOut(row) {
        row.style.backgroundColor = '#FFFFFF';
    }

  
    </script>
<asp:Panel runat="server" ID="pnlDetailsTab">
<uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="500px" />   
</asp:Panel>
<asp:UpdatePanel ID="updPanelItemDetail" runat="server">
    <ContentTemplate>
<asp:Panel runat="server" ID="pnlOtherDetailsTab">
    <asp:Panel ID="pnlOtherDetailsMsg" runat="server">
        <asp:Label ID="lblOtherDetailsMsg" runat="server"></asp:Label>
    </asp:Panel>
</asp:Panel>
<attr:MSAT runat="server" ID="ucMSAT" />
</ContentTemplate> 
</asp:UpdatePanel> 