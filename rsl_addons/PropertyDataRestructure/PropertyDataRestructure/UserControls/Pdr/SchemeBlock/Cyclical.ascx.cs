﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PDR_Utilities.Constants;

namespace PropertyDataRestructure.UserControls.Pdr.SchemeBlock
{
    public partial class Cyclical : System.Web.UI.UserControl
    {
        int id = 0;
        string requestType = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public void populateData()
        {
            getQueryString();
            int scheme = 0;
            int block = 0;

            if (requestType == ApplicationConstants.Scheme)
            {
                scheme = id;
            }
            else if (requestType == ApplicationConstants.Block)
            {
                block = id;
            }
            string url = "../../../../Views/CyclicalServices/CyclicalTab.aspx?schemeId=" + scheme.ToString() + "&blockId=" + block.ToString();
            cyclicalTabiFrame.Attributes.Add("src", url);
            //cyclicalTabiFrame.Load();
        }


        #region getQueryString()
        private void getQueryString()
        {

            if ((Request.QueryString[ApplicationConstants.Id] != null))
            {
                id = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);

            }

            if ((Request.QueryString[ApplicationConstants.RequestType] != null))
            {

                requestType = Request.QueryString[ApplicationConstants.RequestType];
            }
        }
        #endregion
    }
}