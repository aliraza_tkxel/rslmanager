﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Reporting.WebForms.Internal.Soap.ReportingServices2005.Execution;
using PDR_BusinessLogic.AssignToContractor;
using PDR_BusinessLogic.SchemeBlock;
using PDR_BusinessObject.AssignToContractor;
using PDR_BusinessObject.DropDown;
using PDR_BusinessObject.Expenditure;
using PDR_BusinessObject.Vat;
using PDR_DataAccess.AssignToContractor;
using PDR_DataAccess.SchemeBlock;
using PDR_Utilities.Constants;
using PDR_Utilities.Helpers;
using PropertyDataRestructure.Base;
using PropertyDataRestructure.Interface;

namespace PropertyDataRestructure.UserControls.Pdr.SchemeBlock
{
    public partial class PurchaseOrderForm : UserControlBase, IAddEditPage
    {
        int id = 0;
        string requestType;
        private int schemeId = 0;
        private int blockId = 0;
        private bool isThorughSearch = false;
        private bool isProvisionChargeIncluded = false;
        public bool IsSaved
        {
            get
            {
                bool isSavedret = false;
                if (ViewState[ViewStateConstants.IsSaved] != null)
                {
                    isSavedret = Convert.ToBoolean(ViewState[ViewStateConstants.IsSaved]);
                }
                return isSavedret;
            }
            set { ViewState[ViewStateConstants.IsSaved] = value; }
        }
        public string SetCloseButtonText
        {
            set { btnClose.Text = value; }
        }
        public event EventHandler CloseButtonClicked;

        #region Page Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                txtPropertiesPC1.Attributes.Add("disabled", "");
                //ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pops", "$('#txtPropertiesPC1').select2({ placeholder: 'Select Properties', multiple: true, });", true);
                uiMessage.hideMessage();
                GetQueryStringParams();
                heading.Text = isThorughSearch ? "Book a Fault" : "Raise a PO";
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        protected void ddlCostCentre_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.getBudgetHeadDropDownValuesByCostCentreId();
                this.showModalPopup();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        protected void btnOk_click(object sender, EventArgs e)
        {

            mdlPopupRis.Hide();
        }

        #region "ddl Contact Selected Index Change"


        protected void ddlContact_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if ((Convert.ToInt32(ddlContact.SelectedValue) != ApplicationConstants.DropDownDefaultValue))
            {
                AssignToContractorBL objAssignToContractorBl = new AssignToContractorBL(new AssignToContractorRepo());
                DataSet detailsForEmailDS = new DataSet();
                int selectedIndex = Convert.ToInt32(ddlContact.SelectedValue);
                objAssignToContractorBl.getContactEmailDetail(ref selectedIndex, ref detailsForEmailDS);

                //if ((detailsForEmailDS.Tables[ApplicationConstants.ContactEmailDetailsDt] == null) | detailsForEmailDS.Tables[ApplicationConstants.ContactEmailDetailsDt].Rows.Count == 0)
                //{
                //    showModalPopup();
                //}                
            }
            showModalPopup();
        }

        #endregion

        protected void ddlBudgetHead_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.getExpenditureDropDownValuesByBudgetHeadId();
                this.showModalPopup();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessage.isError == true)
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        protected void ddlContractor_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.getContactDropDownValuesbyContractorId();
                this.showModalPopup();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessage.isError == true)
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        protected void ddlVat_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                this.Page.Validate("addServiceRequired");
                this.calculateVatAndTotal();
                this.ddlVat.Focus();
                this.showModalPopup();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        protected void txtNetCost_TextChanged(object sender, EventArgs e)
        {
            try
            {
                this.calculateVatAndTotal();
                this.txtNetCost.Focus();
                this.showModalPopup();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessage.isError == true)
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        protected void btnClose_Click(object sender, EventArgs e)
        {
            if (CloseButtonClicked != null)
            {
                CloseButtonClicked(this, new EventArgs());
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "$('#poModalRaiseaPO').modal('hide');", true);
            }

        }
        protected void btnAssignToContractor_Click(object sender, EventArgs e)
        {
            try
            {
                assignWorkToContractor();
                if (ServiceCharge.Checked == true)
                {
                    if (string.IsNullOrEmpty(txtPropertyApportionment.Text))
                    {
                        return;
                    }
                }
                if (ddlContact.Text == "-1")
                {
                    mdlPopupRis.Show();
                }
                //else
                //    assignWorkToContractor();
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "$('#poModalRaiseaPO').modal('show');", true);
                this.showModalPopup();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessage.isError == true)
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
                showModalPopup();
            }
        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                Page.Validate("addServiceRequired");
                if (Page.IsValid)
                {
                    addPurchaseIteminServiceRequiredDt();
                }
                showModalPopup();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessage.isError == true)
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        protected void grdWorksDeatil_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Footer)
                {
                    DataTable serviceRequiredDt = getServiceRequiredDTViewState();

                    if (serviceRequiredDt.Rows.Count > 0)
                    {
                        e.Row.Cells[1].Text = serviceRequiredDt.Compute("Sum(" + ApplicationConstants.NetCostCol + ")", "").ToString();
                        e.Row.Cells[2].Text = serviceRequiredDt.Compute("Sum(" + ApplicationConstants.VatCol + ")", "").ToString();
                        e.Row.Cells[3].Text = serviceRequiredDt.Compute("Sum(" + ApplicationConstants.GrossCol + ")", "").ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }

        #endregion

        #region "Service Required Data Table"

        /// <summary>
        /// Get the Service Required Data Table from the view state if a Data Table is not there
        /// Create a new data table and add required column.
        /// This is to be used to get data table of this type for the first time.
        /// </summary>
        /// <returns>A data table containing the required columns of Service required grid.</returns>
        /// <remarks></remarks>
        private DataTable getServiceRequiredDTViewState()
        {
            DataTable serviceRequiredDt = null;
            serviceRequiredDt = ViewState[ViewStateConstants.ServiceRequiredDT] as DataTable;

            if (serviceRequiredDt == null)
            {
                serviceRequiredDt = new DataTable();

                //3- Add Work Detail Id Column with data type Integer
                if (!serviceRequiredDt.Columns.Contains(ApplicationConstants.WorkDetailIdCol))
                {
                    DataColumn insertedColumn = null;
                    insertedColumn = serviceRequiredDt.Columns.Add(ApplicationConstants.WorkDetailIdCol, typeof(int));
                    insertedColumn.DefaultValue = ApplicationConstants.NoneValue;
                    insertedColumn.AllowDBNull = false;
                }

                //1- Add Service Required Column with data type string
                if (!serviceRequiredDt.Columns.Contains(ApplicationConstants.ServiceRequiredCol))
                {
                    DataColumn insertedColumn = null;
                    insertedColumn = serviceRequiredDt.Columns.Add(ApplicationConstants.ServiceRequiredCol, typeof(string));
                    insertedColumn.AllowDBNull = false;
                    insertedColumn.DefaultValue = string.Empty;
                    //Set Maximum length for Service Required Column.
                    insertedColumn.MaxLength = ApplicationConstants.MaxStringLegthWordRequired;
                }

                //2- Add Net Cost Column with data type Decimal
                if (!serviceRequiredDt.Columns.Contains(ApplicationConstants.NetCostCol))
                {
                    DataColumn insertedColumn = null;
                    insertedColumn = serviceRequiredDt.Columns.Add(ApplicationConstants.NetCostCol, typeof(decimal));
                    insertedColumn.AllowDBNull = false;
                }

                //3- Add Vat type Column with data type Integer
                if (!serviceRequiredDt.Columns.Contains(ApplicationConstants.VatTypeCol))
                {
                    DataColumn insertedColumn = null;
                    insertedColumn = serviceRequiredDt.Columns.Add(ApplicationConstants.VatTypeCol, typeof(int));
                    insertedColumn.AllowDBNull = false;
                }

                //4- Add Vat Column with data type Decimal
                if (!serviceRequiredDt.Columns.Contains(ApplicationConstants.VatCol))
                {
                    DataColumn insertedColumn = null;
                    insertedColumn = serviceRequiredDt.Columns.Add(ApplicationConstants.VatCol, typeof(decimal));
                    insertedColumn.AllowDBNull = false;
                }

                //5- Add Gross/Total Column with data type Decimal
                if (!serviceRequiredDt.Columns.Contains(ApplicationConstants.GrossCol))
                {
                    DataColumn insertedColumn = null;
                    insertedColumn = serviceRequiredDt.Columns.Add(ApplicationConstants.GrossCol, typeof(decimal));
                    insertedColumn.AllowDBNull = false;
                }

                //6- Add PI Status Column with data type Integer
                if (!serviceRequiredDt.Columns.Contains(ApplicationConstants.PIStatusCol))
                {
                    DataColumn insertedColumn = null;
                    insertedColumn = serviceRequiredDt.Columns.Add(ApplicationConstants.PIStatusCol, typeof(int));
                    insertedColumn.AllowDBNull = false;
                    insertedColumn.DefaultValue = false;
                }

                //7- Add Expenditure ID Col with data type Integer
                if (!serviceRequiredDt.Columns.Contains(ApplicationConstants.ExpenditureIdCol))
                {
                    DataColumn insertedColumn = null;
                    insertedColumn = serviceRequiredDt.Columns.Add(ApplicationConstants.ExpenditureIdCol, typeof(int));
                    insertedColumn.AllowDBNull = false;
                }

                //8- Add Budget Head Id Col with data type Integer
                if (!serviceRequiredDt.Columns.Contains(ApplicationConstants.BudgetHeadIdCol))
                {
                    DataColumn insertedColumn = null;
                    insertedColumn = serviceRequiredDt.Columns.Add(ApplicationConstants.BudgetHeadIdCol, typeof(int));
                    insertedColumn.AllowDBNull = false;
                }

                //9- Add Cost Center Id Col with data type Integer
                if (!serviceRequiredDt.Columns.Contains(ApplicationConstants.CostCenterIdCol))
                {
                    DataColumn insertedColumn = null;
                    insertedColumn = serviceRequiredDt.Columns.Add(ApplicationConstants.CostCenterIdCol, typeof(int));
                    insertedColumn.AllowDBNull = false;
                }


            }

            return serviceRequiredDt;
        }
        private void removeServiceRequiredDTViewState()
        {
            ViewState.Remove(ViewStateConstants.ServiceRequiredDT);
        }
        private void setServiceRequiredDTViewState(DataTable dt)
        {
            ViewState[ViewStateConstants.ServiceRequiredDT] = dt;
        }

        #endregion

        #region interface implementation
        public void populateControl()
        {
            ProvisionsBL objProvisionsBL = new ProvisionsBL(new ProvisionsRepo());
            lblSchemeName.Text = objSession.SchemeName;
            // Populating dropdowns      
            this.getCostCentreDropDownValues();
            this.getVatDropDownValues();
            this.getAllContractors();
            this.getContactDropDownValuesbyContractorId();
            this.resetControls();

            DataRow provisionItem = objProvisionsBL.GetProvisionByProvisionId(objSession.TreeItemId).Tables[0].AsEnumerable().FirstOrDefault();
            if (provisionItem != null)
            {
                txtPropertyApportionment.Text = provisionItem["AnnualApportionmentPC"] != DBNull.Value ? Convert.ToDecimal(provisionItem["AnnualApportionmentPC"].ToString()).ToString() : string.Empty;
                if (provisionItem["BlockPC"] != DBNull.Value)
                {
                    blockId = Convert.ToInt32(provisionItem["BlockPC"].ToString());
                }
                if (provisionItem["SchemeId"] != DBNull.Value)
                {
                    schemeId = Convert.ToInt32(provisionItem["SchemeId"].ToString());
                }
                isProvisionChargeIncluded = Convert.ToBoolean(provisionItem["ProvisionCharge"].ToString());
            }

            PopulateBlockName();
            PopulateProperties();
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "loadSelect2();", true);
        }

        #region save data
        public void populateDropDown(DropDownList ddl)
        {
            throw new NotImplementedException();
        }
        public bool validateData()
        {
            DataTable serviceRequiredDT = getServiceRequiredDTViewState();
            bool isValid = true;
            if (!Page.IsValid)
            {
                isValid = false;
            }
            //else if (!validateContractDates())
            //{
            //    isValid = false;
            //}
            if (serviceRequiredDT.Rows.Count == 0)
            {
                uiMessage.showErrorMessage(UserMessageConstants.serviceRequiredCount);
                isValid = false;
            }

            return isValid;
        }
        //private bool validateContractDates()
        //{
        //    bool isValid = true;

        //    if (string.IsNullOrEmpty(hdnContractStart.Value)
        //        || string.IsNullOrEmpty(hdnContractEnd.Value))
        //    {
        //        isValid = false;
        //        uiMessage.showErrorMessage(UserMessageConstants.ContractStartEndDateNotAvailable);
        //    }
        //    else if (string.IsNullOrEmpty(txtContractStart.Text)
        //        || string.IsNullOrEmpty(txtContractEnd.Text))
        //    {
        //        isValid = false;
        //        uiMessage.showErrorMessage(UserMessageConstants.InvalidContractStartEndDate);
        //    }
        //    else
        //    {
        //        DateTime scopeContractStartDate = Convert.ToDateTime(hdnContractStart.Value);
        //        DateTime scopeContractEndDate = Convert.ToDateTime(hdnContractEnd.Value);
        //        DateTime serviceContractStartDate = Convert.ToDateTime(txtContractStart.Text);
        //        DateTime serviceContractEndDate = Convert.ToDateTime(txtContractEnd.Text);

        //        if (serviceContractStartDate < scopeContractStartDate ||
        //            serviceContractEndDate > scopeContractEndDate ||
        //            serviceContractEndDate < serviceContractStartDate)
        //        {
        //            uiMessage.showErrorMessage(UserMessageConstants.InvalidContractBetweenDate);
        //            isValid = false;
        //        }
        //    }
        //    return isValid;
        //}
        public void saveData()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region load data
        public void loadData()
        {
            throw new NotImplementedException();
        }
        #endregion


        #region populate drop downs
        public void populateDropDowns()
        {
            throw new NotImplementedException();
        }
        #endregion


        #endregion

        #region Helper Functions

        private void PopulateProperties()
        {
            if (requestType == ApplicationConstants.Scheme)
            {
                schemeId = id;
            }
            else if (requestType == ApplicationConstants.Block)
            {
                this.blockId = id;
                schemeId = GetSchemeByBlockId(id);
            }
            populateSelectedProperties(schemeId, blockId);
        }

        private void populateSelectedProperties(int schemeId, int blockId)
        {
            AttributesBL objAttributeBL = new AttributesBL(new AttributesRepo());
            DataSet resultData = new DataSet();

            if (!isProvisionChargeIncluded)
            {
                resultData = objAttributeBL.getPropertiesBySchemeBlock(0, 0);
                txtPropertiesPC1.Items.Clear();
                txtPropertiesPC1.DataSource = resultData;
                txtPropertiesPC1.DataValueField = "PROPERTYID";
                txtPropertiesPC1.DataTextField = "PROPERTYNAME";
                txtPropertiesPC1.DataBind();
            }
            else
            {
                ProvisionsBL objProvisionsBL = new ProvisionsBL(new ProvisionsRepo());
                resultData = objAttributeBL.getPropertiesBySchemeBlock(schemeId, blockId);
                txtPropertiesPC1.Items.Clear();
                txtPropertiesPC1.DataSource = resultData;
                txtPropertiesPC1.DataValueField = "PROPERTYID";
                txtPropertiesPC1.DataTextField = "PROPERTYNAME";
                txtPropertiesPC1.DataBind();

                DataSet resultData1 = new DataSet();
                resultData1 = objProvisionsBL.getSavedProvisionChargeProperties(schemeId, blockId, objSession.TreeItemId);
                int count = 0;
                for (count = 0; count < resultData1.Tables[0].Rows.Count; count++)
                {
                    txtPropertiesPC1.Items.FindByValue(resultData1.Tables[0].Rows[count][0].ToString()).Selected = true;
                }
                if (count == 0)
                {
                    foreach (var item in txtPropertiesPC1.Items)
                    {
                        ((ListItem)item).Selected = true;
                    }
                }
            }
        }

        private void PopulateBlockName()
        {
            if (!isProvisionChargeIncluded)
                lblBlockName.Text = "-";
            else
            {
                if (requestType == ApplicationConstants.Scheme)
                {
                    int schemeId = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);

                    AssignToContractorBL objAssignToContractorBL = new AssignToContractorBL(new AssignToContractorRepo());
                    DataSet blockNameDS = new DataSet();
                    blockNameDS = objAssignToContractorBL.GetBlockNameBySchemeId(schemeId, objSession.TreeItemId);

                    lblBlockName.Text = blockNameDS.Tables[0].Rows.Count > 0 ? blockNameDS.Tables[0].Rows[0]["BlockName"].ToString() : "-";
                }

                else if (requestType == ApplicationConstants.Block)
                {
                    lblSchemeName.Text = string.IsNullOrEmpty(objSession.SchemeName)
                        ? "No Scheme."
                        : objSession.SchemeName;
                    lblBlockName.Text = objSession.BlockName;
                }
            }
        }

        private int GetSchemeByBlockId(int blockId)
        {
            int schemeId = 0;
            AssignToContractorBL objAssignToContractorBL = new AssignToContractorBL(new AssignToContractorRepo());
            DataSet schemeDS = new DataSet();
            schemeDS = objAssignToContractorBL.GetSchemeByBlockId(blockId);

            if (schemeDS.Tables[0].Rows.Count > 0)
            {
                schemeId = int.Parse(schemeDS.Tables[0].Rows[0]["SchemeId"].ToString());
            }
            return schemeId;
        }

        private void GetQueryStringParams()
        {
            if (Request.QueryString[ApplicationConstants.Id] != null)
                id = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);

            if (Request.QueryString[ApplicationConstants.RequestType] != null)
                requestType = Request.QueryString[ApplicationConstants.RequestType];

            if (Request.Path.Contains("SchemeRecord"))
            {
                isThorughSearch = true;
            }
        }

        private void getCostCentreDropDownValues()
        {
            List<DropDownBO> dropDownList = new List<DropDownBO>();
            AssignToContractorBL objAssignToContractorBL = new AssignToContractorBL(new AssignToContractorRepo());
            objAssignToContractorBL.GetCostCentreDropDownVales(ref dropDownList);
            bindDropDownList(ref ddlCostCentre, ref dropDownList);
        }
        private void getAllContractors()
        {
            List<DropDownBO> dropDownList = new List<DropDownBO>();
            AssignToContractorBL objAssignToContractorBL = new AssignToContractorBL(new AssignToContractorRepo());
            objAssignToContractorBL.getAllContractors(ref dropDownList);
            bindDropDownList(ref ddlContractor, ref dropDownList);
        }
        public void getVatDropDownValues()
        {
            List<VatBo> vatBoList = new List<VatBo>();
            AssignToContractorBL objAssignToContractorBL = new AssignToContractorBL(new AssignToContractorRepo());
            objAssignToContractorBL.getVatDropDownValues(ref vatBoList);
            ddlVat.Items.Clear();
            ddlVat.DataSource = vatBoList;
            ddlVat.DataTextField = ApplicationConstants.ddlDefaultDataTextField;
            ddlVat.DataValueField = ApplicationConstants.ddlDefaultDataValueField;
            ddlVat.DataBind();
            ddlVat.Items.Insert(0, new ListItem(ApplicationConstants.DropDwonDefaultText, ApplicationConstants.DropDownDefaultValue.ToString()));

            // Save Vat Bo List in session, to get vat rate while adding a service required.            
            objSession.VatBOList = vatBoList;
        }
        private void getBudgetHeadDropDownValuesByCostCentreId()
        {
            int costCentreId = Convert.ToInt32(ddlCostCentre.SelectedValue);
            if (costCentreId == ApplicationConstants.DropDownDefaultValue)
            {
                resetBudgetHead();
                resetExpenditureddl();
            }
            else
            {
                List<DropDownBO> dropDownList = new List<DropDownBO>();
                AssignToContractorBL objAssignToContractorBL = new AssignToContractorBL(new AssignToContractorRepo());
                objAssignToContractorBL.GetBudgetHeadDropDownValuesByCostCentreId(ref dropDownList, costCentreId);
                bindDropDownList(ref ddlBudgetHead, ref dropDownList);
            }

        }
        private void getExpenditureDropDownValuesByBudgetHeadId()
        {
            int budgetHeadId = Convert.ToInt32(ddlBudgetHead.SelectedValue);
            int employeeId = objSession.EmployeeId;

            if (budgetHeadId == ApplicationConstants.DropDownDefaultValue)
            {
                resetExpenditureddl();
            }
            else
            {
                List<ExpenditureBO> expenditureBOList = new List<ExpenditureBO>();
                AssignToContractorBL objAssignToContractorBL = new AssignToContractorBL(new AssignToContractorRepo());
                objAssignToContractorBL.GetExpenditureDropDownValuesByBudgetHeadId(ref expenditureBOList, ref budgetHeadId, ref employeeId);

                //Save expenditure BO list in Session to get Employee LIMIT and remaining at the stage of adding an item
                // in service required list and to mark it as pending/in queue for authorization if needed.
                objSession.ExpenditureBOList = expenditureBOList;

                ddlExpenditure.Items.Clear();
                ddlExpenditure.DataSource = expenditureBOList;
                ddlExpenditure.DataTextField = ApplicationConstants.ddlDefaultDataTextField;
                ddlExpenditure.DataValueField = ApplicationConstants.ddlDefaultDataValueField;
                ddlExpenditure.DataBind();

                if (expenditureBOList.Count == 0)
                {
                    ddlExpenditure.Items.Insert(0, new ListItem(ApplicationConstants.NoExpenditureSetup, ApplicationConstants.DropDownDefaultValue.ToString()));
                }
                else
                {
                    ddlExpenditure.Items.Insert(0, new ListItem(ApplicationConstants.DropDwonDefaultText, ApplicationConstants.DropDownDefaultValue.ToString()));
                }
            }
        }
        public void getContactDropDownValuesbyContractorId()
        {
            ddlContact.Items.Clear();
            List<DropDownBO> dropDownBoList = new List<DropDownBO>();

            AssignToContractorBL objAssignToContractorBL = new AssignToContractorBL(new AssignToContractorRepo());
            DataSet resultDataset = new DataSet();

            AssignToContractorBo objAssignToContractorBo = new AssignToContractorBo();
            objAssignToContractorBo.ContractorId = Convert.ToInt32(ddlContractor.SelectedValue);
            objAssignToContractorBo.SchemeId = objSession.SchemeId;
            objAssignToContractorBo.BlockId = blockId;
            objAssignToContractorBL.GetContactDropDownValuesAndDetailsbyContractorId(ref dropDownBoList, ref resultDataset, objAssignToContractorBo);

            //if (resultDataset.Tables[0].Rows.Count > 0)
            //{
            //    hdnContractStart.Value = resultDataset.Tables[0].Rows[0]["StartDate"].ToString();
            //    hdnContractEnd.Value = resultDataset.Tables[0].Rows[0]["EndDate"].ToString();
            //}
            //else
            //{
            //    hdnContractStart.Value = string.Empty;
            //    hdnContractEnd.Value = string.Empty;
            //}

            if (dropDownBoList.Count == 0)
            {
                ddlContact.Items.Insert(0, new ListItem(ApplicationConstants.noContactFound, ApplicationConstants.DropDownDefaultValue.ToString()));
            }
            else
            {
                bindDropDownList(ref ddlContact, ref dropDownBoList);
            }
        }
        private void bindDropDownList(ref DropDownList ddlToBind, ref List<DropDownBO> dropDownItemsList, bool insertDefault = true)
        {
            ddlToBind.Items.Clear();
            ddlToBind.DataSource = dropDownItemsList;
            ddlToBind.DataTextField = ApplicationConstants.ddlDefaultDataTextField;
            ddlToBind.DataValueField = ApplicationConstants.ddlDefaultDataValueField;
            ddlToBind.DataBind();

            if (insertDefault)
            {
                ddlToBind.Items.Insert(0, new ListItem(ApplicationConstants.DropDwonDefaultText, ApplicationConstants.DropDownDefaultValue.ToString()));
            }
        }
        private void resetServiceRequired()
        {
            if (ddlVat.Items.Count > 0)
            {
                ddlVat.SelectedIndex = 0;
            }
            ddlCostCentre.SelectedIndex = 0;
            getBudgetHeadDropDownValuesByCostCentreId();
            getExpenditureDropDownValuesByBudgetHeadId();
            txtWorkRequired.Text = string.Empty;
            txtNetCost.Text = string.Empty;
            txtVat.Text = string.Empty;
            txtTotal.Text = string.Empty;
        }
        private void resetBudgetHead()
        {
            ddlBudgetHead.Items.Clear();
            if (ddlBudgetHead.Items.Count == 0)
            {
                ddlBudgetHead.Items.Insert(0, new ListItem(ApplicationConstants.DropDwonDefaultText, ApplicationConstants.DropDownDefaultValue.ToString()));
            }
        }

        private void resetExpenditureddl()
        {
            ddlExpenditure.Items.Clear();
            if (ddlExpenditure.Items.Count == 0)
            {
                ddlExpenditure.Items.Insert(0, new ListItem(ApplicationConstants.DropDwonDefaultText, ApplicationConstants.DropDownDefaultValue.ToString()));
            }
        }
        private void calculateVatAndTotal()
        {
            int vatRateId = Convert.ToInt32(ddlVat.SelectedValue);
            decimal VatRate = 0.0M;

            if (vatRateId >= 0)
            {
                VatRate = getVatRateByVatId(vatRateId);
            }

            decimal netCost = default(decimal);
            decimal defaultNetCost = 0.0M;
            if (decimal.TryParse(txtNetCost.Text.ToString().Trim(), out defaultNetCost))
            {
                netCost = (string.IsNullOrEmpty(txtNetCost.Text.Trim()) ? defaultNetCost : Convert.ToDecimal(txtNetCost.Text.Trim()));
                decimal vat = netCost * VatRate / 100;
                decimal total = netCost + vat;

                txtNetCost.Text = string.Format("{0:0.00}", netCost);
                txtVat.Text = string.Format("{0:0.00}", vat);
                txtTotal.Text = string.Format("{0:0.00}", total);
            }
            else
            {
                txtVat.Text = string.Empty;
                txtTotal.Text = string.Empty;
            }

        }
        private decimal getVatRateByVatId(int vatRateId)
        {
            decimal vatRate = 0.0M;

            List<VatBo> vatBoList = objSession.VatBOList;

            VatBo vatBo = vatBoList.Find(i => i.Id == vatRateId);

            if ((vatBo != null))
            {
                vatRate = vatBo.VatRate;
            }

            return vatRate;
        }
        public void resetControls()
        {
            //Set is Saved property to false on populate.
            this.uiMessage.hideMessage();
            this.IsSaved = false;
            this.btnAssignToContractor.Enabled = true;
            this.btnAdd.Enabled = true;
            this.txtPropertyApportionment.Text = string.Empty;
            this.ServiceCharge.Checked = false;
            this.txtEstimateRef.Text = string.Empty;
            this.POName.Text = string.Empty;
            //   this.txtEstimate.Text = string.Empty;
            this.txtWorkRequired.Text = string.Empty;
            //  this.txtContractStart.Text = string.Empty;
            //    this.txtContractEnd.Text = string.Empty;
            //     this.lblReplacementDue.Text = string.Empty;
            //     this.lblServiceCycle.Text = string.Empty;
            this.resetBudgetHead();
            this.resetExpenditureddl();
            this.resetServiceRequired();
            this.removeServiceRequiredDTViewState();
            this.bindServiceRequiredGrid();
        }

        private void bindServiceRequiredGrid()
        {
            DataTable dtServiceRequired = getServiceRequiredDTViewState();
            bool addExtraSpaces = false;

            if (dtServiceRequired.Rows.Count == 0)
            {
                foreach (DataColumn col in dtServiceRequired.Columns)
                {
                    col.AllowDBNull = true;
                }
                DataRow newRow = dtServiceRequired.NewRow();
                dtServiceRequired.Rows.Add(newRow);
                addExtraSpaces = true;
            }

            grdWorksDeatil.DataSource = dtServiceRequired;
            grdWorksDeatil.DataBind();
            if (addExtraSpaces)
            {
                grdWorksDeatil.Rows[0].Cells[0].Text = "<br /><br /><br />";
            }
        }
        private void addPurchaseIteminServiceRequiredDt()
        {
            ServiceRequiredBo objServiceRequiredBo = new ServiceRequiredBo();
            objServiceRequiredBo.WorkDetailId = ApplicationConstants.NoneValue;
            objServiceRequiredBo.ServiceRequired = txtWorkRequired.Text.Trim();
            objServiceRequiredBo.NetCost = (string.IsNullOrEmpty(txtNetCost.Text.Trim()) ? 0.0M : Convert.ToDecimal(txtNetCost.Text));
            objServiceRequiredBo.VatIdDDLValue = Convert.ToInt32(ddlVat.SelectedValue);
            objServiceRequiredBo.Vat = (string.IsNullOrEmpty(txtVat.Text.Trim()) ? 0.0M : Convert.ToDecimal(txtVat.Text.Trim()));
            objServiceRequiredBo.Total = (string.IsNullOrEmpty(txtTotal.Text.Trim()) ? 0.0M : Convert.ToDecimal(txtTotal.Text.Trim()));
            objServiceRequiredBo.ExpenditureId = Convert.ToInt32(ddlExpenditure.SelectedValue);
            objServiceRequiredBo.BudgetHeadId = Convert.ToInt32(ddlBudgetHead.SelectedValue);
            objServiceRequiredBo.CostCenterId = Convert.ToInt32(ddlCostCentre.SelectedValue);

            DataTable serviceRequiredDt = getServiceRequiredDTViewState();
            DataRow serviceRequiredRow = serviceRequiredDt.NewRow();
            serviceRequiredRow[ApplicationConstants.WorkDetailIdCol] = objServiceRequiredBo.WorkDetailId;
            serviceRequiredRow[ApplicationConstants.ExpenditureIdCol] = objServiceRequiredBo.ExpenditureId;
            serviceRequiredRow[ApplicationConstants.BudgetHeadIdCol] = objServiceRequiredBo.BudgetHeadId;
            serviceRequiredRow[ApplicationConstants.CostCenterIdCol] = objServiceRequiredBo.CostCenterId;
            serviceRequiredRow[ApplicationConstants.ServiceRequiredCol] = objServiceRequiredBo.ServiceRequired;
            serviceRequiredRow[ApplicationConstants.NetCostCol] = objServiceRequiredBo.NetCost;
            serviceRequiredRow[ApplicationConstants.VatTypeCol] = objServiceRequiredBo.VatIdDDLValue;
            serviceRequiredRow[ApplicationConstants.VatCol] = objServiceRequiredBo.Vat;
            serviceRequiredRow[ApplicationConstants.GrossCol] = objServiceRequiredBo.Total;
            serviceRequiredRow[ApplicationConstants.PIStatusCol] = getPIStatus(objServiceRequiredBo.ExpenditureId, objServiceRequiredBo.Total);

            serviceRequiredDt.Rows.Add(serviceRequiredRow);

            resetServiceRequired();
            setServiceRequiredDTViewState(serviceRequiredDt);
            bindServiceRequiredGrid();
        }
        private object getPIStatus(int expenditureId, decimal gross)
        {
            int pIStatus = 3;
            // 3 = "Work Ordered" in table F_POSTATUS
            List<ExpenditureBO> expenditureBoList = objSession.ExpenditureBOList;

            //Check if an item is costing 0 (zero) or less then it should not go to queued list.
            if (gross > 0)
            {
                ExpenditureBO ExpenditureBo = expenditureBoList.Find(i => i.Id == expenditureId);

                if ((ExpenditureBo != null))
                {
                    if ((gross > ExpenditureBo.Limit || gross > ExpenditureBo.Remaining))
                    {
                        pIStatus = 0;
                        // 0 = "Queued" in table F_POSTATUS
                    }
                }
            }

            return pIStatus;
        }
        private int getPOStatusFromWorkItemsDt(DataTable serviceRequiredDT)
        {
            int pOStatus = 3;
            // 3 = "Work Ordered" in table F_POSTATUS

            if (serviceRequiredDT.Select(ApplicationConstants.PIStatusCol + " = 0").Count() > 0)
            {
                pOStatus = 0;
                // 0 = "Queued" in table F_POSTATUS
            }

            return pOStatus;
        }
        #endregion

        #region ModalPopup
        public void showModalPopup()
        {
            ((ModalPopupExtender)this.Parent.Parent.FindControl("mdlpopupRaisePOProvision")).Show();
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "loadSelect2();", true);
        }
        public void hideModalPopup()
        {
            ((ModalPopupExtender)this.Parent.Parent.FindControl("mdlpopupRaisePOProvision")).Hide();
        }
        #endregion

        #region "Save Assign Work To Contractor"
        private void assignWorkToContractor()
        {
            DataTable serviceRequiredDT = getServiceRequiredDTViewState();

            if (validateData())
            {
                AssignToContractorBo assignToContractorBo = objSession.AssignToContractorBo;
                assignToContractorBo.ServiceRequiredDt = serviceRequiredDT;
                assignToContractorBo.AttributeTypeId = null;
                assignToContractorBo.SchemeId = objSession.SchemeId;

                //if (Convert.ToInt32(ddlBlock.SelectedValue) < 1)
                //    assignToContractorBo.BlockId = objSession.BlockId;
                //else
                //    assignToContractorBo.BlockId = Convert.ToInt32(ddlBlock.SelectedValue);
                assignToContractorBo.BlockId = blockId;
                assignToContractorBo.IncInSChge = ServiceCharge.Checked;
                if (ServiceCharge.Checked == true)
                {
                    if (string.IsNullOrEmpty(txtPropertyApportionment.Text))
                    {
                        PropErrorId.Visible = true;
                        return;
                    }
                    else
                    {
                        PropErrorId.Visible = false;
                    }
                    assignToContractorBo.PropertyApportionment = Convert.ToDecimal(txtPropertyApportionment.Text);

                }
                else
                {
                    assignToContractorBo.PropertyApportionment = null;
                }

                //Note: Need to attach property id
                //assignToContractorBo.PropertyId = Convert.ToString(ddlProperty.SelectedValue);

                assignToContractorBo.ContractorId = Convert.ToInt32(ddlContractor.SelectedValue);
                assignToContractorBo.ContactId = Convert.ToInt32(ddlContact.SelectedValue);
                assignToContractorBo.POName = POName.Text;
                assignToContractorBo.EmpolyeeId = Convert.ToInt32(ddlContact.SelectedValue);
                // assignToContractorBo.Estimate = (string.IsNullOrEmpty(txtEstimate.Text.Trim()) ? 0.0M : Convert.ToDecimal(txtEstimate.Text.Trim()));
                assignToContractorBo.EstimateRef = txtEstimateRef.Text;
                assignToContractorBo.POStatus = getPOStatusFromWorkItemsDt(serviceRequiredDT);
                assignToContractorBo.UserId = objSession.EmployeeId;
                assignToContractorBo.MSATTypeId = 12;

                //   assignToContractorBo.ContractStartDate = Convert.ToDateTime(txtContractStart.Text).ToString();
                //   assignToContractorBo.ContractEndDate = Convert.ToDateTime(txtContractEnd.Text).ToString();

                AssignToContractorBL objAssignToContractorBl = new AssignToContractorBL(new AssignToContractorRepo());
                bool isSavedStatus = objAssignToContractorBl.assignToContractorForSchemePO(assignToContractorBo);

                if (isSavedStatus)
                {
                    //Set Property Is Saved to True.
                    this.IsSaved = true;
                    //Disable btnAssignToContractor to avoid assigning the same work more than once. 
                    btnAssignToContractor.Enabled = false;
                    btnAdd.Enabled = false;

                    string message = UserMessageConstants.AssignedToContractor;
                    try
                    {
                        //checks if the PO status is not queued then send email
                        if (assignToContractorBo.POStatus == 3)
                        {
                            DataSet detailsForEmailDS = new DataSet();
                            AssignToContractorBL objAssignToContractor = new AssignToContractorBL(new AssignToContractorRepo());
                            objAssignToContractor.getSB_DetailsForProvisionEmail(ref assignToContractorBo, ref detailsForEmailDS);
                            if (detailsForEmailDS.Tables[ApplicationConstants.ContractorDetailsDt].Rows.Count > 0)
                            {
                                sendEmailtoContractor(ref assignToContractorBo);
                            }
                            else
                            {

                                message = message + UserMessageConstants.EmailToContractor;
                            }
                        }
                        else
                        {
                            sendEmailtoBudgetHolder(ref assignToContractorBo);
                        }

                        uiMessage.showInformationMessage(message);
                    }
                    catch (Exception ex)
                    {
                        message += "<br />but " + ex.Message;
                        uiMessage.showErrorMessage(message);
                    }
                }
                else
                {
                    uiMessage.showErrorMessage(UserMessageConstants.AssignedToContractorFailed);
                }
            }
            showModalPopup();
        }
        #endregion

        #region Email to Contractor

        #region "Populate Body and Send Email to Contractor"
        private void sendEmailtoContractor(ref AssignToContractorBo assignToContractorBo)
        {
            DataSet detailsForEmailDS = new DataSet();
            AssignToContractorBL objAssignToContractor = new AssignToContractorBL(new AssignToContractorRepo());
            objAssignToContractor.getSB_DetailsForProvisionEmail(ref assignToContractorBo, ref detailsForEmailDS);

            if (detailsForEmailDS != null)
            {


                if ((detailsForEmailDS.Tables[ApplicationConstants.ContractorDetailsDt] != null) && detailsForEmailDS.Tables[ApplicationConstants.ContractorDetailsDt].Rows.Count > 0)
                {
                    string email = Convert.ToString(detailsForEmailDS.Tables[ApplicationConstants.ContractorDetailsDt].Rows[0]["Email"]);
                    if (string.IsNullOrEmpty(email) || !GeneralHelper.isEmail(email))
                    {
                        throw new Exception("Unable to send email, invalid email address.");
                    }
                    else
                    {
                        StringBuilder body = new StringBuilder();
                        StreamReader reader = new StreamReader(Server.MapPath("~/Email/SB_AssignWorkToContractor.html"));
                        body.Append(reader.ReadToEnd());

                        // Set contractor detail(s) '
                        //==========================================='
                        //Populate Contractor Contact Name
                        body.Replace("{ContractorContactName}", Convert.ToString(detailsForEmailDS.Tables[ApplicationConstants.ContractorDetailsDt].Rows[0]["ContractorContactName"]));
                        body.Replace("{Details}", " Please find below details of works required:");
                        //Get sum/total of net cost from works required data table.
                        body.Replace("{NetCost}", assignToContractorBo.ServiceRequiredDt.Compute("SUM(" + ApplicationConstants.NetCostCol + ")", "").ToString());
                        //Get sum/total of Vat from works required data table.
                        body.Replace("{VAT}", assignToContractorBo.ServiceRequiredDt.Compute("SUM(" + ApplicationConstants.VatCol + ")", "").ToString());
                        //Get sum/total of Gross/Total from works required data table.
                        body.Replace("{TOTAL}", assignToContractorBo.ServiceRequiredDt.Compute("SUM(" + ApplicationConstants.GrossCol + ")", "").ToString());

                        //Get all works detail from works required data table in form of ordered list.
                        body.Replace("{WorkRequired}", getServiceRequired(assignToContractorBo.ServiceRequiredDt));
                        //==========================================='


                        // Property detail(s) (address)
                        //==========================================='

                        //Get Property Address details from property details data set
                        if ((detailsForEmailDS.Tables[ApplicationConstants.PropertyDetailsDt] != null) && detailsForEmailDS.Tables[ApplicationConstants.PropertyDetailsDt].Rows.Count > 0)
                        {
                            DataRow dr = detailsForEmailDS.Tables[ApplicationConstants.PropertyDetailsDt].Rows[0];
                            body.Replace("{TownCity}", Convert.ToString(dr["TOWNCITY"]));
                            body.Replace("{PostCode}", Convert.ToString(dr["POSTCODE"]));
                            body.Replace("{Block}", Convert.ToString(dr["Block"]));
                            body.Replace("{Scheme}", Convert.ToString(dr["Scheme"]));
                            body.Replace("{Property}", Convert.ToString(dr["Property"]));
                        }
                        else
                        {
                            // This case may not occur but it is done for completeness
                            body.Replace("{TownCity}", "");
                            body.Replace("{PostCode}", "");
                            body.Replace("{Block}", "");
                            body.Replace("{Scheme}", "N/A");
                        }

                        //==========================================='
                        // Set Purchase Order Details 
                        //==========================================='

                        if ((detailsForEmailDS.Tables[ApplicationConstants.PurchaseOrderDetailsDt] != null) && detailsForEmailDS.Tables[ApplicationConstants.PurchaseOrderDetailsDt].Rows.Count > 0)
                        {
                            DataRow dr = detailsForEmailDS.Tables[ApplicationConstants.PurchaseOrderDetailsDt].Rows[0];
                            body.Replace("{PO}", Convert.ToString(dr["ORDERID"]));
                            body.Replace("{POName}", Convert.ToString(dr["PONAME"]));
                        }

                        LinkedResource logoBroadLandRepairs = new LinkedResource(Server.MapPath("~/Images/broadland.png"));
                        logoBroadLandRepairs.ContentId = "logoBroadLandRepairs_Id";

                        body = body.Replace("{Logo_Broadland-Housing-Association}", string.Format("cid:{0}", logoBroadLandRepairs.ContentId));

                        ContentType mimeType = new ContentType("text/html");

                        AlternateView alternatevw = AlternateView.CreateAlternateViewFromString(body.ToString(), mimeType);
                        alternatevw.LinkedResources.Add(logoBroadLandRepairs);
                        //==========================================='

                        MailMessage mailMessage = new MailMessage();

                        mailMessage.Subject = ApplicationConstants.EmailSubject_SB;
                        mailMessage.To.Add(new MailAddress(detailsForEmailDS.Tables[ApplicationConstants.ContractorDetailsDt].Rows[0]["Email"].ToString(), detailsForEmailDS.Tables[ApplicationConstants.ContractorDetailsDt].Rows[0]["ContractorContactName"].ToString()));
                        // For a graphical view with logos, alternative view will be visible, body is attached for text view.
                        mailMessage.Body = body.ToString();
                        mailMessage.AlternateViews.Add(alternatevw);
                        mailMessage.IsBodyHtml = true;


                        EmailHelper.sendEmail(ref mailMessage);
                    }
                }
                else
                {

                    throw new Exception("Unable to send email, contractor details not available");

                }
            }
        }
        #endregion

        #region Email to Budget Holder

        private void sendEmailtoBudgetHolder(ref AssignToContractorBo assignToContractorBo)
        {
            DataSet detailsForEmailDS = new DataSet();
            DataSet budgetHolderDs = new DataSet();
            AssignToContractorBL objAssignToContractor = new AssignToContractorBL(new AssignToContractorRepo());
            objAssignToContractor.getSB_DetailsForProvisionEmail(ref assignToContractorBo, ref detailsForEmailDS);
            objAssignToContractor.getBudgetHolderByOrderId(ref budgetHolderDs, assignToContractorBo.OrderId, assignToContractorBo.ExpenditureId);
            if (detailsForEmailDS != null)
            {


                if ((detailsForEmailDS.Tables[ApplicationConstants.ContractorDetailsDt] != null) && detailsForEmailDS.Tables[ApplicationConstants.ContractorDetailsDt].Rows.Count > 0)
                {
                    string email = Convert.ToString(budgetHolderDs.Tables[0].Rows[0]["Email"]);
                    if (string.IsNullOrEmpty(email) || !GeneralHelper.isEmail(email))
                    {
                        throw new Exception("Unable to send email, Because budget holder's email not exists in the system.");
                    }
                    else
                    {
                        StringBuilder body = new StringBuilder();
                        StreamReader reader = new StreamReader(Server.MapPath("~/Email/SB_AssignWorkToContractor.html"));
                        body.Append(reader.ReadToEnd());

                        // Set contractor detail(s) '
                        //==========================================='
                        //Populate Contractor Contact Name
                        body.Replace("{ContractorContactName}", Convert.ToString(budgetHolderDs.Tables[0].Rows[0]["OperativeName"]));
                        body.Replace("{Details}", "Please find below details of works requiring budget approval:");
                        //Get sum/total of net cost from works required data table.
                        body.Replace("{NetCost}", assignToContractorBo.ServiceRequiredDt.Compute("SUM(" + ApplicationConstants.NetCostCol + ")", "").ToString());
                        //Get sum/total of Vat from works required data table.
                        body.Replace("{VAT}", assignToContractorBo.ServiceRequiredDt.Compute("SUM(" + ApplicationConstants.VatCol + ")", "").ToString());
                        //Get sum/total of Gross/Total from works required data table.
                        body.Replace("{TOTAL}", assignToContractorBo.ServiceRequiredDt.Compute("SUM(" + ApplicationConstants.GrossCol + ")", "").ToString());

                        //Get all works detail from works required data table in form of ordered list.
                        body.Replace("{WorkRequired}", getServiceRequired(assignToContractorBo.ServiceRequiredDt));
                        //==========================================='


                        // Property detail(s) (address)
                        //==========================================='

                        //Get Property Address details from property details data set
                        if ((detailsForEmailDS.Tables[ApplicationConstants.PropertyDetailsDt] != null) && detailsForEmailDS.Tables[ApplicationConstants.PropertyDetailsDt].Rows.Count > 0)
                        {
                            DataRow dr = detailsForEmailDS.Tables[ApplicationConstants.PropertyDetailsDt].Rows[0];
                            body.Replace("{TownCity}", Convert.ToString(dr["TOWNCITY"]));
                            body.Replace("{PostCode}", Convert.ToString(dr["POSTCODE"]));
                            body.Replace("{Block}", Convert.ToString(dr["Block"]));
                            body.Replace("{Scheme}", Convert.ToString(dr["Scheme"]));
                            body.Replace("{Property}", Convert.ToString(dr["Property"]));
                        }
                        else
                        {
                            // This case may not occur but it is done for completeness
                            body.Replace("{TownCity}", "");
                            body.Replace("{PostCode}", "");
                            body.Replace("{Block}", "");
                            body.Replace("{Scheme}", "N/A");
                        }

                        //==========================================='
                        // Set Purchase Order Details 
                        //==========================================='

                        if ((detailsForEmailDS.Tables[ApplicationConstants.PurchaseOrderDetailsDt] != null) && detailsForEmailDS.Tables[ApplicationConstants.PurchaseOrderDetailsDt].Rows.Count > 0)
                        {
                            DataRow dr = detailsForEmailDS.Tables[ApplicationConstants.PurchaseOrderDetailsDt].Rows[0];
                            body.Replace("{PO}", Convert.ToString(dr["ORDERID"]));
                            body.Replace("{POName}", Convert.ToString(dr["PONAME"]));
                        }

                        LinkedResource logoBroadLandRepairs = new LinkedResource(Server.MapPath("~/Images/broadland.png"));
                        logoBroadLandRepairs.ContentId = "logoBroadLandRepairs_Id";

                        body = body.Replace("{Logo_Broadland-Housing-Association}", string.Format("cid:{0}", logoBroadLandRepairs.ContentId));

                        ContentType mimeType = new ContentType("text/html");

                        AlternateView alternatevw = AlternateView.CreateAlternateViewFromString(body.ToString(), mimeType);
                        alternatevw.LinkedResources.Add(logoBroadLandRepairs);
                        //==========================================='

                        MailMessage mailMessage = new MailMessage();

                        mailMessage.Subject = ApplicationConstants.EmailSubject_SB;
                        mailMessage.To.Add(new MailAddress(budgetHolderDs.Tables[0].Rows[0]["Email"].ToString(), budgetHolderDs.Tables[0].Rows[0]["OperativeName"].ToString()));
                        // For a graphical view with logos, alternative view will be visible, body is attached for text view.
                        mailMessage.Body = body.ToString();
                        mailMessage.AlternateViews.Add(alternatevw);
                        mailMessage.IsBodyHtml = true;


                        EmailHelper.sendEmail(ref mailMessage);
                    }
                }
                else
                {
                    throw new Exception("Unable to send email, contractor details not available");
                }
            }
        }
        #endregion

        #region "Get Service Required - Concatenated in form of ordered list to add in email"
        private string getServiceRequired(DataTable dataTable)
        {
            StringBuilder serviceRequired = new StringBuilder();
            if (dataTable.Rows.Count == 1)
            {
                serviceRequired.Append(dataTable.Rows[0][ApplicationConstants.ServiceRequiredCol].ToString());
            }
            else
            {
                serviceRequired.Append("<ol>");
                foreach (DataRow row in dataTable.Rows)
                {
                    serviceRequired.Append("<li>" + row[ApplicationConstants.ServiceRequiredCol].ToString() + "</li>");
                }
                serviceRequired.Append("</ol>");
            }
            return serviceRequired.ToString();
        }

        #endregion

        #region "Get Risk Details - As concatenated string split on separate row."
        private string getRiskDetails(DataSet detailsForEmailDS)
        {
            StringBuilder RiskDetails = new StringBuilder("N/A");


            if ((detailsForEmailDS != null) && (detailsForEmailDS.Tables[ApplicationConstants.TenantRiskDetailsDt] != null) && detailsForEmailDS.Tables[ApplicationConstants.TenantRiskDetailsDt].Rows.Count > 0)
            {
                RiskDetails.Clear();

                foreach (DataRow row in detailsForEmailDS.Tables[ApplicationConstants.TenantRiskDetailsDt].Rows)
                {
                    RiskDetails.Append(row["CATDESC"] + (string.IsNullOrEmpty(Convert.ToString(row["SUBCATDESC"])) ? "" : ": " + row["SUBCATDESC"]) + "<br />");
                }
            }

            return RiskDetails.ToString();
        }

        #endregion

        #region "Get Vulnerability Details - As concatenated string split on separate row."

        private string getVulnerabilityDetails(DataSet detailsForEmailDS)
        {
            StringBuilder vulnerabilityDetails = new StringBuilder("N/A");


            if ((detailsForEmailDS != null) && (detailsForEmailDS.Tables[ApplicationConstants.TenantVulnerabilityDetailsDt] != null) && detailsForEmailDS.Tables[ApplicationConstants.TenantVulnerabilityDetailsDt].Rows.Count > 0)
            {
                vulnerabilityDetails.Clear();

                foreach (DataRow row in detailsForEmailDS.Tables[ApplicationConstants.TenantVulnerabilityDetailsDt].Rows)
                {
                    vulnerabilityDetails.Append(row["CATDESC"] + (string.IsNullOrEmpty(Convert.ToString(row["SUBCATDESC"])) ? "" : ": " + row["SUBCATDESC"]) + "<br />");
                }

            }

            return vulnerabilityDetails.ToString();
        }

        #endregion
        #endregion
    }
}