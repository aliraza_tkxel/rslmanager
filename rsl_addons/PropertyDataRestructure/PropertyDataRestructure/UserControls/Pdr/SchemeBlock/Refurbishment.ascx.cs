﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PropertyDataRestructure.Base;
using PropertyDataRestructure.Interface;
using PDR_BusinessLogic.Block;
using PDR_BusinessObject.SchemeBlock;
using PDR_Utilities.Constants;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_DataAccess.Block;

namespace PropertyDataRestructure.UserControls.Pdr.SchemeBlock
{
    public partial class Refurbishment : UserControlBase,IAddEditPage,IListingPage
    {
        BlockBL objBlockBL =new BlockBL(new BlockRepo());
        int id = 0;
        string requestType = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            txtRefurbishmentDate.Attributes.Add("readonly", "readonly");
           
        }
        public void saveData()
        {
            try
            {
                string errorMessage;
                getQueryString();
                RefurbishmentBO objRefurbishmentBO = new RefurbishmentBO();
                
                objRefurbishmentBO.RefurbishmentDate = Convert.ToDateTime(txtRefurbishmentDate.Text);
                objRefurbishmentBO.Notes = txtNotes.Text;
                objRefurbishmentBO.Id = id;
                objRefurbishmentBO.RequestType = requestType;
                // objRefurbishmentBO.UserId = 605 ' For Testing
                objRefurbishmentBO.UserId = objSession.EmployeeId;
                //  When go live
                errorMessage = objBlockBL.saveRefurbishment(objRefurbishmentBO);
                if ((errorMessage == String.Empty))
                {
                    uiMessage.showInformationMessage(UserMessageConstants.SuccessMessage);
                }
                else
                {
                    uiMessage.showErrorMessage(UserMessageConstants.RefurbishmentError);
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }

        public bool validateData()
        {
            throw new NotImplementedException();
        }

        public void resetControls()
        {
            txtRefurbishmentDate.Text = string.Empty;
            txtNotes.Text = string.Empty;
        }

        public void loadData()
        {
            resetControls();
            getQueryString();
            this.bindToGrid(id);
        }

        public void populateDropDown(DropDownList ddl)
        {
            throw new NotImplementedException();
        }

        public void populateDropDowns()
        {
            throw new NotImplementedException();
        }


        public void searchData()
        {
            throw new NotImplementedException();
        }

        public void populateData()
        {
            throw new NotImplementedException();
        }

        public void printData()
        {
            throw new NotImplementedException();
        }

        public void exportGridToExcel()
        {
            throw new NotImplementedException();
        }

        public void exportToPdf()
        {
            throw new NotImplementedException();
        }

        public void downloadData()
        {
            throw new NotImplementedException();
        }

        public void applyFilters()
        {
            throw new NotImplementedException();
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            saveData();
            getQueryString();
            bindToGrid(id);
            resetControls();
            UpdatePanel updHealth = (UpdatePanel)Parent.FindControl("updPnlHealthSafety");
            updHealth.Update();
        
        }


        public void bindToGrid(int Id)
        {
            DataSet resultDataSet = new DataSet();
            objBlockBL.getRefurbishmentInformation(ref resultDataSet, Id,requestType);
            grdRefurbishment.DataSource = resultDataSet;
            grdRefurbishment.DataBind();            
        }

        protected void grdRefurbishment_PageIndexChanging(object sender, System.Web.UI.WebControls.GridViewPageEventArgs e)
        {
            grdRefurbishment.PageIndex = e.NewPageIndex;
            this.bindToGrid(id);
        }

        #region getQueryString()
        private void getQueryString()
        {
            try
            {

                if ((Request.QueryString[ApplicationConstants.Id] != null))
                {
                    id = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);

                }

                if ((Request.QueryString[ApplicationConstants.RequestType] != null))
                {

                    requestType = Request.QueryString[ApplicationConstants.RequestType];
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion
    }
}