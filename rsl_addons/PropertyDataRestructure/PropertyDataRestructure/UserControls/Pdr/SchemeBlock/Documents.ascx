﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Documents.ascx.cs" Inherits="PropertyDataRestructure.UserControls.Pdr.SchemeBlock.Documents" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<style type="text/css">
    .div_AddDocRO
    {
        display:none !important;
    }
    .div_grdDocumentInfoRO
    {
        float: none !important; 
        margin-left: 2px !important;
        width: 99% !important;
    }
</style>
<uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="500px" />
<!--Upload form starts here -->
<div style="min-width: 1020px;     margin-left: -2px;">
    <div id="div_AddDoc" style="border-style: solid; border-color: black; border-width: 0px; width: 30%;
        margin-left: 2px; min-width: 300px; float: left" runat="server">
        <div class="pm-lbox" style="width: 290px;">
            <asp:UpdatePanel ID="updPnlAttributes" runat="server">
                <ContentTemplate>
                    <div class="title-bar">
                        <strong>Add a Document</strong>
                    </div>
                    <div class="add-form">
                        <div class="frow">
                            <div class="title">
                                Category:
                            </div>
                            <div class="input-area">
                                <asp:DropDownList ID="ddlDocumentCategory" runat="server" class="selectval" CssClass="selectval"
                                    AutoPostBack="true" 
                                    onselectedindexchanged="ddlDocumentCategory_SelectedIndexChanged">
                                    <asp:ListItem Value="Compliance" Text="Compliance" Enabled="true" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="General" Text="General" Enabled="true"></asp:ListItem>
                                    <asp:ListItem Value="Legal" Text="Legal" Enabled="true"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="frow">
                            <div class="title">
                                Type:
                            </div>
                            <div class="input-area">
                                <asp:DropDownList ID="ddlType" runat="server" class="selectval" CssClass="selectval"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlType_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="frow">
                            <div class="title">
                                Title:
                            </div>
                            <div class="input-area">
                                <asp:DropDownList ID="ddlSubtype" runat="server" class="selectval" CssClass="selectval"
                                    AutoPostBack="true">
                                </asp:DropDownList>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="frow">
                            <div class="title">
                                Document:
                            </div>
                            <div class="input-area">
                                <asp:TextBox runat="server" ID="txtDocumentDate"></asp:TextBox>
                                <ajaxToolkit:CalendarExtender runat="server" ID="clndrDocumentDate" TargetControlID="txtDocumentDate"
                                    PopupButtonID="imgDocumentDate" PopupPosition="Right" TodaysDateFormat="dd/MM/yyyy"
                                    Format="dd/MM/yyyy">
                                </ajaxToolkit:CalendarExtender>
                                <asp:ImageButton ImageUrl="~/Images/calendar.png" runat="server" ID="imgDocumentDate"
                                    Style="vertical-align: bottom; border: none;" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtDocumentDate"
                                    ErrorMessage="Required" CssClass="Required" ValidationGroup="Doc" Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="txtDocumentDate"
                                    ErrorMessage="<br/>Enter valid date" Operator="DataTypeCheck" Type="Date" Display="Dynamic"
                                    CssClass="Required" ValidationGroup="Doc" />
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="frow">
                            <div class="title">
                                Expiry:
                            </div>
                            <div class="input-area">
                                <asp:TextBox runat="server" ID="txtExpires"></asp:TextBox>
                                <ajaxToolkit:CalendarExtender runat="server" ID="clndrExpires" TargetControlID="txtExpires"
                                    PopupButtonID="imgExpiresDate" PopupPosition="Right" TodaysDateFormat="dd/MM/yyyy"
                                    Format="dd/MM/yyyy">
                                </ajaxToolkit:CalendarExtender>
                                <asp:ImageButton ImageUrl="~/Images/calendar.png" runat="server" ID="imgExpiresDate"
                                    Style="vertical-align: bottom; border: none;" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtExpires"
                                    ErrorMessage="Required" CssClass="Required" ValidationGroup="Doc" Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtExpires"
                                    ErrorMessage="<br/>Enter valid date" Operator="DataTypeCheck" Type="Date" Display="Dynamic"
                                    CssClass="Required" ValidationGroup="Doc" />
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="frow">
                            <div class="title">
                                Upload:
                            </div>
                            <div class="input-area" style="width: 189px;">
                                <asp:FileUpload ID="flUploadDoc" runat="server" class="upload" Style="width: 189px;" />
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="frow">
                            <div class="title">
                                Keywords:
                            </div>
                            <div class="input-area">
                                <asp:TextBox ID="txtKeyword" runat="server" TextMode="MultiLine" class="txtarea"
                                    MaxLength="255"></asp:TextBox>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="frow">
                            <asp:Button ID="btnCancel" runat="server" Text="Reset" OnClick="btnCancel_Click" />
                            <asp:Button ID="btnSavePropertyDocuments" runat="server" Text="Save" OnClick="Save" />
                            <div class="clear">
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnSavePropertyDocuments" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
    <div id="div_grdDocumentInfo" style="float: left; width: 68%; margin-left: 10px;" runat="server">
        <cc1:PagingGridView ID="grdDocumentInfo" runat="server" AllowPaging="false" AllowSorting="false"
            AutoGenerateColumns="False" BorderStyle="Solid" BorderWidth="1px" CellPadding="4"
            GridLines="None" OrderBy="" PageSize="10" Width="100%" PagerSettings-Visible="false"
            ShowHeaderWhenEmpty="true" EmptyDataText="No Record Found">
            <Columns>
                <asp:TemplateField HeaderText="Date Added:" SortExpression="CreatedDate">
                    <ItemTemplate>
                        <asp:Label ID="lblCreatedDate" runat="server" Text='<%# Bind("CreatedDate") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left" />
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="By:" SortExpression="UploadedBy">
                    <ItemTemplate>
                        <asp:Label ID="lblUploadedBy" runat="server" Text='<%# Bind("UploadedBy") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left" />
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Type:" SortExpression="DocumentName">
                    <ItemTemplate>
                        <asp:Label ID="lblTitle" runat="server" Text='<%# Bind("TypeTitle") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left" />
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Title:" SortExpression="DocumentType">
                    <ItemTemplate>
                        <asp:Label ID="lblType" runat="server" Text='<%# Bind("SubtypeTitle") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left" />
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Category:" SortExpression="Category">
                    <ItemTemplate>
                        <asp:Label ID="lblCategory" runat="server" Text='<%# Bind("Category") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left" />
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Document:" SortExpression="DocumentDate">
                    <ItemTemplate>
                        <asp:Label ID="lblDocumentDate" runat="server" Text='<%# Bind("DocumentDate") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left" />
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Expiry:" SortExpression="ExpiryDate">
                    <ItemTemplate>
                        <asp:Label ID="lblExpiryDate" runat="server" Text='<%# Bind("ExpiryDate") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Left" />
                    <ItemStyle HorizontalAlign="Left" />
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Button ID="btnDeleteDocument" runat="server" CommandArgument='<%#Eval("DocumentId")%>'
                            Text="Delete" UseSubmitBehavior="False" OnClick="btnDeleteDocument_Click" OnClientClick="if(! confirm('Are you sure you want to permanently remove the selected file?')) return false;" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right" />
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Button ID="btnViewDocument" runat="server" Text="View" UseSubmitBehavior="False"
                            CommandArgument='<%#Eval("DocumentId")%>' OnClick="btnViewDocument_Click" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right" />
                </asp:TemplateField>
            </Columns>
            <%-- Grid View Styling Start --%>
            <RowStyle BackColor="#EFF3FB"></RowStyle>
            <EditRowStyle BackColor="#2461BF"></EditRowStyle>
            <SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>
            <PagerStyle BackColor="#FAFAD2" ForeColor="Black" HorizontalAlign="Left"></PagerStyle>
            <HeaderStyle BackColor="#ffffff" ForeColor="black" Font-Bold="True" HorizontalAlign="Left"
                CssClass="table-head"></HeaderStyle>
            <AlternatingRowStyle BackColor="White" Wrap="True"></AlternatingRowStyle>
            <%-- Grid View Styling End --%>
            <PagerSettings Mode="NumericFirstLast"></PagerSettings>
        </cc1:PagingGridView>
        <asp:Panel runat="server" ID="pnlPagination" Visible="true" Style="border-top: 1px solid  #000;
            vertical-align: middle;">
            <table style="width: 100%;">
                <tbody>
                    <tr>
                        <td>
                            <asp:LinkButton ID="lnkbtnPagerFirst" Text="" runat="server" CommandName="Page" CommandArgument="First"
                                OnClick="lnkbtnPager_Click" Style="text-decoration: none;">&lt;&lt;First</asp:LinkButton>
                            &nbsp;
                        </td>
                        <td>
                            <asp:LinkButton ID="lnkbtnPagerPrev" Text="" runat="server" CommandName="Page" CommandArgument="Prev"
                                OnClick="lnkbtnPager_Click" Style="text-decoration: none;">&lt;Prev</asp:LinkButton>
                            &nbsp;
                        </td>
                        <td>
                            Page:&nbsp;
                            <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="Required" />
                            &nbsp;of&nbsp;
                            <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />. Records:
                            <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                            &nbsp;to&nbsp;
                            <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                            &nbsp;of&nbsp;
                            <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                        </td>
                        <td>
                            <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page"
                                CommandArgument="Next" OnClick="lnkbtnPager_Click" Style="text-decoration: none;" />
                            &nbsp;
                        </td>
                        <td>
                            <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page"
                                CommandArgument="Last" OnClick="lnkbtnPager_Click" Style="text-decoration: none;" />
                            &nbsp;
                        </td>
                    </tr>
                </tbody>
            </table>
        </asp:Panel>
    </div>
</div>
