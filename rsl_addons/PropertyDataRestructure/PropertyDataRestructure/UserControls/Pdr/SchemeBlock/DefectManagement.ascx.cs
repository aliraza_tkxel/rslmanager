﻿using System;
using System.Web.UI.WebControls;
using PropertyDataRestructure.Base;
using PDR_Utilities.Constants;
using PDR_BusinessLogic.SchemeBlock;
using System.Data;
using PDR_DataAccess.SchemeBlock;
using PDR_Utilities.Helpers;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_BusinessObject.SchemeBlock;
using System.Globalization;

namespace PropertyDataRestructure.UserControls.Pdr.SchemeBlock
{
    public partial class DefectManagement : UserControlBase
    {
        #region Properties

        AttributesBL objAttributeBL = new AttributesBL(new AttributesRepo());
        private const string SaveText = "Save";
        private const string AmendText = "Amend";
        bool _readOnly = false;
        #endregion

        #region Control Events

        public delegate void saveButton_ClickedEventHandler(object sender, EventArgs e);
        public event saveButton_ClickedEventHandler saveButton_ClickedEvent;

        public delegate void cancelButton_ClickedEventHandler(object sender, EventArgs e);
        public event cancelButton_ClickedEventHandler cancelButton_ClickedEvent;

        public delegate void showPoupAgainEventHandler(object sender, EventArgs e);
        public event showPoupAgainEventHandler showPoupAgainEvent;
        


        #endregion

        #region Events Handling

        #region User Control Events Handling

        private void Page_Load(object sender, System.EventArgs e)
        {
            string PageName = System.Convert.ToString(sender.ToString());
            if (PageName.Contains("properties"))
            {
                _readOnly = false;
            }
            else if (PageName.Contains("propertyrecord"))
            {
                _readOnly = true;
            }
            if (_readOnly == true)
            {
                btnSave.Visible = false;
            }
            uiMessageHelper.resetMessage(ref lblMessage, ref pnlMessage);
            //Page.Form.Attributes.Add("enctype", "multipart/form-data")
        }

        #endregion

        #region Save Button Click

        public void btnSave_Click(object sender, System.EventArgs e)
        {
            try
            {

                if (btnSave.Text == SaveText)
                {
                    var saveStatus = saveApplianceDefect();
                    if (saveStatus)
                    {
                        btnSave.Enabled = false;
                        if (saveButton_ClickedEvent != null)
                            saveButton_ClickedEvent(sender, e);
                    }
                    else
                    {
                        if (showPoupAgainEvent != null)
                            showPoupAgainEvent(sender, e);
                    }
                }
                else if (btnSave.Text == AmendText)
                {
                    prepareAmendDefect();
                    if (showPoupAgainEvent != null)
                        showPoupAgainEvent(sender, e);
                }



            }
            catch (Exception ex)
            {
                uiMessageHelper.IsError = true;
                uiMessageHelper.Message = ex.Message;

                uiMessageHelper.setMessage(ref lblMessage, ref pnlMessage, ex.Message, true);

                if (uiMessageHelper.IsExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                    if (showPoupAgainEvent != null)
                        showPoupAgainEvent(sender, e);
                }
            }
        }

        #endregion

        #region Cancel Button Click

        public void btnCancel_Click(object sender, System.EventArgs e)
        {
            if (cancelButton_ClickedEvent != null)
                cancelButton_ClickedEvent(sender, e);
        }

        #endregion

        #endregion

        #region Functions

        #region Prepare Add Defect

        public void prepareAddDefect(string id, string requestType)
        {
            lblDefectHeading.Text = "Add New Defect";
            btnSave.Text = SaveText;
            // Enable save button, it is disables after successful defect save.
            btnSave.Enabled = true;
            setPRequestTypeAndId(requestType, id);
            // Show all editable fields
            changeAllEditableFieldsVisibility(visible: true);
            changeVisibilityOfAllReadOnlyFields(visible: false);
            // Reset All editable fields to default value i.e empty textboxes etc

            populateLookUpFields(id, requestType);
            resetEditableFields();
            setDefaultTrade();

        }

        #endregion

        #region Populate Lookups

        private void populateLookUpFields(string id, string requestType)
        {

            DataSet defectsLookUpsDS = new DataSet();
            objAttributeBL.getDefectManagementLookupValues(id, requestType, ref defectsLookUpsDS);

            ddlDefectCategories.DataSource = defectsLookUpsDS.Tables[0];
            ddlDefectCategories.DataBind();

            ddlPartsOrderedBy.DataSource = defectsLookUpsDS.Tables[2];
            ddlPartsOrderedBy.DataBind();

            ddlPriority.DataSource = defectsLookUpsDS.Tables[3];
            ddlPriority.DataBind();

            DataTable dtTrade = defectsLookUpsDS.Tables[4];
            ddlTrade.DataSource = dtTrade;
            ddlTrade.DataBind();

            GeneralHelper.PopulateDropDownWithDurationHours(ddlEstimatedDuration);
        }

        #endregion

        #region Save Appliance Defect

        public bool saveApplianceDefect()
        {
            var saveStatus = false;

            int propertyDefectId = -1;
            int.TryParse(System.Convert.ToString(hidDefectId.Value), out propertyDefectId);

            ApplianceDefectBO objDefectBO = new ApplianceDefectBO();

            bool isValidated = true;

            objDefectBO.PropertyDefectId = propertyDefectId;

            objAttributeBL.getSchemeBlockDefectDetails(propertyDefectId, ref objDefectBO);

            objDefectBO.Id = getSchemeBlockId();

            objDefectBO.RequestType = getRequestType();

            // ============================== Validate Defect ==============================

            if (propertyDefectId == -1)
            {
                //Validate Defect Category
                if (isValidated && ddlDefectCategories.Items.Count > 0 && !(Convert.ToInt32(ddlDefectCategories.SelectedItem.Value) == 0))
                {
                    objDefectBO.CategoryId = Convert.ToInt32(ddlDefectCategories.SelectedValue);
                }
                else
                {
                    isValidated = false;
                    uiMessageHelper.setMessage(ref lblMessage, ref pnlMessage, UserMessageConstants.SelectCategory, true);
                    return false;
                }

                DateTime temp_result = System.Convert.ToDateTime(objDefectBO.DefectDate);
                if (!DateTime.TryParseExact(System.Convert.ToString(txtDefectDate.Text), "dd/MM/yyyy", new CultureInfo("en-GB"), DateTimeStyles.None, out temp_result))
                {
                    uiMessageHelper.setMessage(ref lblMessage, ref pnlMessage, UserMessageConstants.notValidDateFormat, true);
                    return false;
                }

                objDefectBO.DefectDate = temp_result;

                objDefectBO.IsDefectIdentified = bool.Parse(System.Convert.ToString(rbtnGrpIsDefectIdentified.SelectedValue));
                objDefectBO.DefectIdentifiedNotes = txtDefectNotes.Text;

                objDefectBO.IsRemedialActionTaken = bool.Parse(System.Convert.ToString(rbtnGrpIsRemedialActionTaken.SelectedValue));
                objDefectBO.RemedialActionNotes = txtRemedialActionNotes.Text;

                objDefectBO.IsWarningNoteIssued = bool.Parse(System.Convert.ToString(rbtnGrpIsWarningNoteIssued.SelectedValue));

                if (isValidated && txtSerialNumber.Text.Trim() == string.Empty)
                {
                    uiMessageHelper.setMessage(ref lblMessage, ref pnlMessage, UserMessageConstants.SerialNumber, true);
                    objDefectBO.SerialNumber = txtSerialNumber.Text.Trim();
                    return false;
                }
                objDefectBO.SerialNumber = txtSerialNumber.Text.Trim();
                objDefectBO.GcNumber = string.Format("{0}-{1}-{2}", txtGCNumber0.Text.Trim(), txtGCNumber1.Text.Trim(), txtGCNumber2.Text.Trim());
                objDefectBO.IsWarningTagFixed = chkBoxWarningTagFixed.Checked;

                GeneralHelper.setParseNullableBooleanValueFromStringOrDefaultToNull(ref objDefectBO.IsAppliancedDisconnected, rbtnGrpIsDisconnected.SelectedValue);
            }

            GeneralHelper.setParseNullableBooleanValueFromStringOrDefaultToNull(ref objDefectBO.IsPartsRequired, rbtnGrpIsPartsRequired.SelectedValue);
            GeneralHelper.setParseNullableBooleanValueFromStringOrDefaultToNull(ref objDefectBO.IsPartsOrdered, rbtnGrpIsPartsOrdered.SelectedValue);

            objDefectBO.PartsOrderedBy = null;
            //If objDefectBO.IsPartsOrdered Then
            if (ddlPartsOrderedBy.SelectedValue != null && ddlPartsOrderedBy.SelectedValue != string.Empty)
                objDefectBO.PartsOrderedBy = Convert.ToInt32(ddlPartsOrderedBy.SelectedValue);
            //End If

            DateTime partsDueDate = default(DateTime);
            if (txtPartsDueDate.Text != string.Empty && !DateTime.TryParseExact(System.Convert.ToString(txtPartsDueDate.Text), "dd/MM/yyyy", new CultureInfo("en-GB"), DateTimeStyles.None, out partsDueDate))
            {
                uiMessageHelper.setMessage(ref lblMessage, ref pnlMessage, UserMessageConstants.notValidDateFormat, true);
                return false;
            }
            else if (txtPartsDueDate.Text != string.Empty)
            {
                objDefectBO.PartsDueDate = partsDueDate;
            }

            objDefectBO.PartsDescription = txtPartsDescription.Text.Trim();
            objDefectBO.PartsLocation = txtPartsLocation.Text.Trim();

            objDefectBO.IsTwoPersonsJob = bool.Parse(System.Convert.ToString(rbtnGrpIsTwoPersonJob.SelectedValue));
            objDefectBO.ReasonForSecondPerson = txtReasonFor2ndPerson.Text.Trim();

            if (ddlEstimatedDuration.SelectedValue != null)
                objDefectBO.EstimatedDuration = Convert.ToDecimal(ddlEstimatedDuration.SelectedValue);
            if (ddlPriority.SelectedValue != null)
                objDefectBO.PriorityId = Convert.ToInt32(ddlPriority.SelectedValue);
            if (ddlTrade.SelectedValue != null)
                objDefectBO.TradeId = Convert.ToInt32(ddlTrade.SelectedValue);
            objDefectBO.UserId = Convert.ToInt32(objSession.EmployeeId);
            objDefectBO.HeatingMappingId = objSession.HeatingMappingId;

            // ============================== End - Validate Defect ==============================
            string filePath = string.Empty;
            //Dim fileName As String = String.Empty


            // TODO : need to uncomment
            //if (objDefectBO.RequestType == ApplicationConstants.Block)
            //    filePath = System.Convert.ToString(Server.MapPath(ResolveClientUrl(ConfigHelper.GetBlockDocUploadPath()) + objDefectBO.Id.ToString() + "/Images/"));
            //else
            //    filePath = System.Convert.ToString(Server.MapPath(ResolveClientUrl(ConfigHelper.GetSchemeDocUploadPath()) + objDefectBO.Id.ToString() + "/Images/"));
            
            //If (Directory.Exists(filePath) = False) Then
            //    Directory.CreateDirectory(filePath)
            //End If

            //fileName = Path.GetFileName(flUploadPhoto.FileName)
            //fileName = getUniqueFileName()
            //Dim filePathName As String = filePath + fileName
            //flUploadPhoto.SaveAs(filePathName)

            objDefectBO.FilePath = lblFileName.Text;
            objDefectBO.PhotoName = lblFileName.Text;
            objDefectBO.PhotosNotes = txtPhotoNotes.Text.Trim();

            objAttributeBL.saveDefect(objDefectBO);

            saveStatus = true;
            return saveStatus;

        }

        #endregion

        #region View Defect Details

        public void viewDefectDetails(int propertyDefectId)
        {
            lblDefectHeading.Text = "Defect Details";
            btnSave.Text = AmendText;
            btnSave.Enabled = true;
            changeAllEditableFieldsVisibility(visible: false);
            ApplianceDefectBO objApplianceDefectBO = new ApplianceDefectBO();
            objAttributeBL.getSchemeBlockDefectDetails(propertyDefectId, ref objApplianceDefectBO);
            changeVisibilityOfAllReadOnlyFields(visible: true);
            changeAllEditableFieldsVisibility(visible: false);
            //lblPhotoGraphNotes.Visible = True

            populateDefectDetails(objApplianceDefectBO);
            // Save appliance defect bo to fill values when modifing defect
            setAppliaceDefectBOViewState(objApplianceDefectBO);
        }

        #endregion

        #region Set Default Trade

        private void setDefaultTrade()
        {

            foreach (ListItem item in ddlTrade.Items)
            {
                if (item.Text == ApplicationConstants.DefaultDefectTrade)
                {
                    ddlTrade.SelectedValue = item.Value;
                    break;
                }
            }

        }

        #endregion

        #region Prepare Amend Defect

        private void prepareAmendDefect()
        {
            btnSave.Text = SaveText;
            changeAmendModeEditableFieldsVisibility(Visible: true);
            changeAmendModeReadonlyFieldsVisibility(Visible: false);
            populateLookUpFields(getSchemeBlockId(), getRequestType());
            populateAmendEditableFields();
        }

        #endregion

        #region Change All editable Fields visibility

        private void changeAllEditableFieldsVisibility(bool visible)
        {
            GeneralHelper.changeServerControlsVisibility(visible, ddlDefectCategories, txtDefectDate, imgCalDefectDate, rbtnGrpIsDefectIdentified, txtDefectNotes, rbtnGrpIsRemedialActionTaken, txtRemedialActionNotes, rbtnGrpIsWarningNoteIssued, txtSerialNumber, txtGCNumber0, txtGCNumber1, txtGCNumber2, chkBoxWarningTagFixed, rbtnGrpIsDisconnected, rbtnGrpIsPartsRequired, rbtnGrpIsPartsOrdered, ddlPartsOrderedBy, txtPartsDueDate, imgPartsDueDate, txtPartsDescription, txtPartsLocation, rbtnGrpIsTwoPersonJob, txtReasonFor2ndPerson, ddlEstimatedDuration, ddlPriority, ddlTrade, txtPhotoNotes, btnUploadPhotos, lblFileName, lblPhotoGraphNotes, lblPhotograph, lblPhotoNotes);
            //lblPhotograph.Visible = visible
            //lblPhotoGraphNotes.Visible = visible
        }

        #endregion

        #region Change Visibility of all ready only fields

        private void changeVisibilityOfAllReadOnlyFields(bool visible)
        {
            GeneralHelper.changeServerControlsVisibility(visible, lbldefectCategory, lbldefectCategory, lblDefectDate, lblIsDefectIdentified, lblDefectNotes, pnlDefectNotes, lblIsRemedialActionTaken, pnlRemedialActionNotes, lblRemedialActionNotes, lblIsWarningNoteIssued, lblSerialNumber, lblGcNumber, lblIsWarningTagFixed, lblIsDisconnected, lblIsPartsRequired, lblIsPartsOrdered, lblPartsOrderedBy, lblPartsDueDate, lblPartsDescription, lblPartsLocation, lblIsTwoPersonJob, lblReasonFor2ndPerson, lblEstimatedDuration, lblPriority, lblTrade, lblPhotoNotes, lblPhotoGraphNotes);
        }

        #endregion

        #region Change Visibility of amend mode editable fields

        private void changeAmendModeEditableFieldsVisibility(bool Visible)
        {
            GeneralHelper.changeServerControlsVisibility(Visible, rbtnGrpIsPartsRequired, rbtnGrpIsPartsOrdered, txtPartsDueDate, imgPartsDueDate, txtPartsDescription, txtPartsLocation, rbtnGrpIsTwoPersonJob, txtReasonFor2ndPerson, ddlEstimatedDuration, ddlPriority, ddlTrade);
        }

        #endregion

        #region Change Visibility of amend mode ready only fields

        private void changeAmendModeReadonlyFieldsVisibility(bool Visible)
        {
            GeneralHelper.changeServerControlsVisibility(Visible, lblIsPartsRequired, lblIsPartsOrdered, lblPartsDueDate, lblPartsDescription, lblPartsLocation, lblIsTwoPersonJob, lblReasonFor2ndPerson, lblEstimatedDuration, lblPriority, lblTrade);
        }

        #endregion

        #region Populate Amend Editable Fields

        private void populateAmendEditableFields()
        {
            ApplianceDefectBO objApplianceDefectBO = getAppliaceDefectBOViewState();
            if (!ReferenceEquals(objApplianceDefectBO, null))
            {

                rbtnGrpIsPartsRequired.SelectedValue = string.Empty;
                if (!ReferenceEquals(objApplianceDefectBO.IsPartsRequired, null))
                {
                    rbtnGrpIsPartsRequired.SelectedValue = objApplianceDefectBO.IsPartsRequired.ToString();
                }

                rbtnGrpIsPartsOrdered.SelectedValue = string.Empty;
                if (!ReferenceEquals(objApplianceDefectBO.IsPartsOrdered, null))
                {
                    rbtnGrpIsPartsOrdered.SelectedValue = objApplianceDefectBO.IsPartsOrdered.ToString();
                }

                txtPartsDueDate.Text = GeneralHelper.getFormatedDateDefaultString(objApplianceDefectBO.PartsDueDate);
                txtPartsDescription.Text = objApplianceDefectBO.PartsDescription.Trim();
                txtPartsLocation.Text = objApplianceDefectBO.PartsLocation;

                if (!ReferenceEquals(objApplianceDefectBO.IsTwoPersonsJob, null))
                {
                    rbtnGrpIsTwoPersonJob.SelectedValue = objApplianceDefectBO.IsTwoPersonsJob.ToString();
                }

                txtReasonFor2ndPerson.Text = objApplianceDefectBO.ReasonForSecondPerson;

                if (!ReferenceEquals(objApplianceDefectBO.EstimatedDuration, null))
                {
                    ddlEstimatedDuration.SelectedValue = Convert.ToInt32(objApplianceDefectBO.EstimatedDuration).ToString();
                }

                if (!ReferenceEquals(objApplianceDefectBO.PriorityId, null))
                {
                    ddlPriority.SelectedValue = objApplianceDefectBO.PriorityId.ToString();
                }

                if (!ReferenceEquals(objApplianceDefectBO.TradeId, null))
                {
                    ddlTrade.SelectedValue = objApplianceDefectBO.TradeId.ToString();
                }

            }
        }

        #endregion

        #region Populate Defect Details

        private void populateDefectDetails(ApplianceDefectBO objApplianceDefectBO)
        {
            hidDefectId.Value = objApplianceDefectBO.PropertyDefectId.ToString();
            lbldefectCategory.Text = objApplianceDefectBO.CategoryDescription;
            lblDefectDate.Text = GeneralHelper.getFormatedDateDefaultString(objApplianceDefectBO.DefectDate);
            lblIsDefectIdentified.Text = GeneralHelper.getYesNoStringFromValue(objApplianceDefectBO.IsDefectIdentified);
            lblDefectNotes.Text = objApplianceDefectBO.DefectIdentifiedNotes;
            lblIsRemedialActionTaken.Text = GeneralHelper.getYesNoStringFromValue(objApplianceDefectBO.IsRemedialActionTaken);
            lblRemedialActionNotes.Text = objApplianceDefectBO.RemedialActionNotes;
            lblIsWarningNoteIssued.Text = GeneralHelper.getYesNoStringFromValue(objApplianceDefectBO.IsWarningNoteIssued);

            lblSerialNumber.Text = objApplianceDefectBO.SerialNumber;
            lblGcNumber.Text = objApplianceDefectBO.GcNumber;
            lblIsWarningTagFixed.Text = GeneralHelper.getYesNoStringFromValue(objApplianceDefectBO.IsWarningTagFixed);
            lblIsDisconnected.Text = GeneralHelper.getYesNoStringFromValue(objApplianceDefectBO.IsAppliancedDisconnected);
            lblIsPartsRequired.Text = GeneralHelper.getYesNoStringFromValue(objApplianceDefectBO.IsPartsRequired);
            lblIsPartsOrdered.Text = GeneralHelper.getYesNoStringFromValue(objApplianceDefectBO.IsPartsOrdered);
            lblPartsOrderedBy.Text = objApplianceDefectBO.PartsOrderedByName;
            lblPartsDueDate.Text = GeneralHelper.getFormatedDateDefaultString(objApplianceDefectBO.PartsDueDate);
            lblPartsDescription.Text = objApplianceDefectBO.PartsDescription;
            lblPartsLocation.Text = objApplianceDefectBO.PartsLocation;
            lblIsTwoPersonJob.Text = GeneralHelper.getYesNoStringFromValue(objApplianceDefectBO.IsTwoPersonsJob);
            lblReasonFor2ndPerson.Text = objApplianceDefectBO.ReasonForSecondPerson;
            if (!ReferenceEquals(objApplianceDefectBO.EstimatedDuration, null))
            {
                lblEstimatedDuration.Text = GeneralHelper.getSingularOrPluralPostFixForNumericValues(Convert.ToDouble(objApplianceDefectBO.EstimatedDuration), "hour");
            }
            else
            {
                lblEstimatedDuration.Text = "N/A";
            }

            lblPriority.Text = objApplianceDefectBO.PriorityName;
            lblTrade.Text = objApplianceDefectBO.Trade;
            lblPhotoNotes.Text = objApplianceDefectBO.PhotosNotes;
        }

        #endregion

        #region Reset Editable Fields

        private void resetEditableFields()
        {
            foreach (var serverControl in pnldefectFilds.Controls)
            {
                if (serverControl is TextBox)
                {
                    ((TextBox)serverControl).Text = string.Empty;
                }
                if (serverControl is DropDownList)
                {
                    ((DropDownList)serverControl).SelectedIndex = 0;
                }
            }
            rbtnGrpIsDefectIdentified.SelectedIndex = 0;
            rbtnGrpIsRemedialActionTaken.SelectedIndex = 1;
            rbtnGrpIsWarningNoteIssued.SelectedIndex = 1;
            rbtnGrpIsDisconnected.SelectedIndex = 2;
            rbtnGrpIsPartsRequired.SelectedIndex = 2;
            rbtnGrpIsPartsOrdered.SelectedIndex = 2;
            rbtnGrpIsTwoPersonJob.SelectedIndex = 0;
            hidDefectId.Value = "-1";
            lblFileName.Text = string.Empty;
        }

        #endregion

        #endregion

        #region View State Functions

        #region Scheme/Block Id

        private void setPRequestTypeAndId(string requestType, string Id)
        {
            ViewState[ViewStateConstants.RequestType] = requestType;
            if (requestType == ApplicationConstants.Block)
                ViewState[ViewStateConstants.BlockId] = Id;
            else
                ViewState[ViewStateConstants.SchemeId] = Id;
        }

        private string getSchemeBlockId()
        {
            if (this.getRequestType() == ApplicationConstants.Block)
            {
                return (string)ViewState[ViewStateConstants.BlockId];
            }
            else
            {
                return (string)ViewState[ViewStateConstants.SchemeId];
            }
        }

        private string getRequestType()
        {
            return (string)ViewState[ViewStateConstants.RequestType];
        }

        #endregion

        #region Applianc Defect BO

        #region Get Appliance Defect BO

        private ApplianceDefectBO getAppliaceDefectBOViewState()
        {
            ApplianceDefectBO objApplianceDefectBO = null;
            if (!ReferenceEquals(ViewState[ViewStateConstants.ApplianceDefectBO], null))
            {
                objApplianceDefectBO = (ApplianceDefectBO)(ViewState[ViewStateConstants.ApplianceDefectBO]);
            }
            return objApplianceDefectBO;
        }

        #endregion

        #region Set Appliance Defect BO

        private void setAppliaceDefectBOViewState(ApplianceDefectBO objApplianceDefectBO)
        {
            ViewState[ViewStateConstants.ApplianceDefectBO] = objApplianceDefectBO;
        }

        #endregion

        #endregion

        #endregion
      

    }
}