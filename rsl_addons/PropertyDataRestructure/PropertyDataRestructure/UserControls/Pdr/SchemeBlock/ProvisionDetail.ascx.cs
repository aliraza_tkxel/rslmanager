﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_BusinessLogic.SchemeBlock;
using PDR_BusinessObject.SchemeBlock;
using PDR_DataAccess.SchemeBlock;
using PDR_Utilities.Constants;
using PDR_Utilities.Helpers;
using PropertyDataRestructure.Base;
using PDR_BusinessObject.AssignToContractor;
using PDR_BusinessLogic.AssignToContractor;
using PDR_DataAccess.AssignToContractor;
using PropertyDataRestructure.Interface;
using PDR_BusinessLogic.Scheme;
using PDR_DataAccess.Scheme;

namespace PropertyDataRestructure.UserControls.Pdr.SchemeBlock
{
    public partial class ProvisionDetail : UserControlBase, IAddEditPage
    {
        public event EventHandler OnChildEventOccurs;
        ProvisionsBL objProvisionsBL = new ProvisionsBL(new ProvisionsRepo());
        AttributesBL objAttributeBL = new AttributesBL(new AttributesRepo());
        AssignToContractorBL objContractorBL = new AssignToContractorBL(new AssignToContractorRepo());
        int id = 0;
        string requestType = string.Empty;
        Boolean isReadOnly = false;
        String pageName = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            pageName = System.IO.Path.GetFileName(Request.Url.ToString());
            if (pageName.Contains(PathConstants.SchemeRecordPage))
            {
                isReadOnly = true;
            }
            else
            {
                btnBookFault.Visible = false;
                isReadOnly = false;
            }
            if (isReadOnly == true)
            {
                btnRaisePO.Visible = false;
                btnAmend.Visible = false;
                btnSave.Visible = false;
            }
            try
            {
                ucAssignToContractor.CloseButtonClicked += new EventHandler(AssignToContractorBtnClose_Click);
                uiMessage.hideMessage();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        protected void lnkBtnDetailTab_Click(object sender, EventArgs e)
        {
            try
            {
                this.lnkBtnDetailTab.CssClass = ApplicationConstants.TabClickedCssClass;
                this.lnkBtnNotesTab.CssClass = ApplicationConstants.TabInitialCssClass;
                this.lnkBtnPhotographsTab.CssClass = ApplicationConstants.TabInitialCssClass;
                this.lnkBtnDisposalsTab.CssClass = ApplicationConstants.TabInitialCssClass;

                UserControl ucProvisionItem = (UserControl)ucItemDetail.FindControl("ucProvisionItem");
                ProvisionItem objProvisionItem = (ProvisionItem)ucProvisionItem;
                objProvisionItem.populateData();
                this.MainView.ActiveViewIndex = 0;
                if (isReadOnly == true)
                {
                    this.btnAmend.Visible = false;
                }
                else
                {
                    this.btnAmend.Visible = true;
                }

               
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        protected void lnkBtnNotesTab_Click(object sender, EventArgs e)
        {
            try
            {
                this.lnkBtnDetailTab.CssClass = ApplicationConstants.TabInitialCssClass;
                this.lnkBtnNotesTab.CssClass = ApplicationConstants.TabClickedCssClass;
                this.lnkBtnPhotographsTab.CssClass = ApplicationConstants.TabInitialCssClass;
                this.lnkBtnDisposalsTab.CssClass = ApplicationConstants.TabInitialCssClass;

                this.ucProvisionNotes.loadData();
                this.btnAmend.Visible = false;
                this.btnSave.Visible = false;
                this.MainView.ActiveViewIndex = 1;
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        protected void lnkBtnPhotographsTab_Click(object sender, EventArgs e)
        {
            try
            {
                this.lnkBtnDetailTab.CssClass = ApplicationConstants.TabInitialCssClass;
                this.lnkBtnNotesTab.CssClass = ApplicationConstants.TabInitialCssClass;
                this.lnkBtnPhotographsTab.CssClass = ApplicationConstants.TabClickedCssClass;
                this.lnkBtnDisposalsTab.CssClass = ApplicationConstants.TabInitialCssClass;
                this.btnAmend.Visible = false;
                this.btnSave.Visible = false;

                this.ucProvisionPhotographs.loadData();
                this.MainView.ActiveViewIndex = 2;
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        protected void lnkBtnDisposalsTab_Click(object sender, EventArgs e)
        {
            try
            {
                this.lnkBtnDetailTab.CssClass = ApplicationConstants.TabInitialCssClass;
                this.lnkBtnNotesTab.CssClass = ApplicationConstants.TabInitialCssClass;
                this.lnkBtnPhotographsTab.CssClass = ApplicationConstants.TabInitialCssClass;
                this.lnkBtnDisposalsTab.CssClass = ApplicationConstants.TabClickedCssClass;
                this.btnAmend.Visible = false;
                this.btnSave.Visible = false;
                this.ucProvisionDisposals.loadData();
                this.MainView.ActiveViewIndex = 3;
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        protected void btnAmend_Click(object sender, EventArgs e)
        {
            this.updateData();
            if (OnChildEventOccurs != null)
                OnChildEventOccurs(objSession.ProvisionDataSet, null);
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            this.saveData();
            UserControl ucProvisionItem = (UserControl)ucItemDetail.FindControl("ucProvisionItem");
            ProvisionItem objProvisionItem = (ProvisionItem)ucProvisionItem;
            objProvisionItem.populateData();
            if (OnChildEventOccurs != null)
                OnChildEventOccurs(objSession.ProvisionDataSet, null);
        }

        #region IAddEditPage Implementation
        public void saveData()
        {
            this.GetQueryStringParams();
            int? schemeId = null;
            int? blockId = null;

            if (requestType == ApplicationConstants.Scheme)
            {
                schemeId = id;
            }
            else if (requestType == ApplicationConstants.Block)
            {
                blockId = id;
                schemeId = GetSchemeByBlockId(id);
            }

            ProvisionsBO objProvisionBO = new ProvisionsBO();
            List<MSATDetailBO> msatDetailList = new List<MSATDetailBO>();
            CyclicalServiceBO cyclicalService = new CyclicalServiceBO();  
            UserControl ucMSAT = (UserControl)ucItemDetail.FindControl("ucMSAT");
            
            MaintenanceServicingAndTesting objMST = (MaintenanceServicingAndTesting)ucMSAT;            
            string result = objMST.fillMSATObject(ref msatDetailList, ref cyclicalService); //Fill MSAT Object
            ServiceChargeProperties serProperties = new ServiceChargeProperties();
            string message = objMST.fillServiceChargeObject(ref serProperties);

            UserControl ucProvisionItem = (UserControl)ucItemDetail.FindControl("ucProvisionItem");
            ProvisionItem objProvisionItem = (ProvisionItem)ucProvisionItem;

            ProvisionChargeProperties pcProperties = new ProvisionChargeProperties();
            result = objProvisionItem.FillProvisionItemDetailObject(ref objProvisionBO, ref pcProperties);
                   
            if (uiMessage.isError == false && result == string.Empty)
            {
                objProvisionBO.SchemeId = schemeId;
                objProvisionBO.BlockId = blockId;
                objProvisionBO.UpdatedBy = objSession.EmployeeId;

                if (objSession.TreeItemId == -1)
                    objProvisionBO.ProvisionParentId = null;
                else
                    objProvisionBO.ProvisionParentId = objSession.TreeItemParentId;
                result = objProvisionsBL.SaveProvsionItemDetail(objProvisionBO, GeneralHelper.convertToDataTable(msatDetailList));
                if (result == "success")
                {

                    if (objProvisionBO.ProvisionId > 0)
                    {
                        this.btnSave.Visible = false;
                        if (isReadOnly == true)
                        {
                            this.btnAmend.Visible = false;
                        }
                        else 
                        { 
                            this.btnAmend.Visible = true; 
                        }                      
                        objSession.TreeItemId = objProvisionBO.ProvisionId;
                        objProvisionsBL.SaveProvisionChargeProperties(pcProperties, GeneralHelper.convertToDataTable(pcProperties.excludedIncludedProperties), objSession.TreeItemId);
                        uiMessage.messageText = UserMessageConstants.ProvisionSavedSuccessfully;
                        uiMessage.showInformationMessage(uiMessage.messageText);
                    }
                }
                else if (result == "failed")
                {

                    uiMessage.messageText = UserMessageConstants.ProvisionNotSaved;
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }


                DataSet provisionsDS = objProvisionsBL.GetProvisionsData(schemeId, blockId);
                objSession.ProvisionDataSet = provisionsDS;

                if (message == string.Empty)
                {
                    objAttributeBL.saveServiceChargeProperties(serProperties, GeneralHelper.convertToDataTable(serProperties.excludedIncludedProperties), objSession.TreeItemId, 0);
                }
            }
            else
            {
                if (result != string.Empty)
                {
                    uiMessage.messageText = result;
                }
                uiMessage.showErrorMessage(uiMessage.messageText);
            }
       
        }

        public void updateData()
        {
            this.GetQueryStringParams();
            int? schemeId = null;
            int? blockId = null;

            if (requestType == ApplicationConstants.Scheme)
                schemeId = id;
            else if (requestType == ApplicationConstants.Block)
            {
                schemeId = GetSchemeByBlockId(id); 
                blockId = id;
            }

            ProvisionsBO objProvisionBO = new ProvisionsBO();
            List<MSATDetailBO> msatDetailList = new List<MSATDetailBO>();

            UserControl ucMSAT = (UserControl)ucItemDetail.FindControl("ucMSAT");
            MaintenanceServicingAndTesting objMST = (MaintenanceServicingAndTesting)ucMSAT;
            CyclicalServiceBO cyclicalService = new CyclicalServiceBO(); 
            string result = objMST.fillMSATObject(ref msatDetailList,ref cyclicalService); //Fill MSAT Object

            ServiceChargeProperties serProperties = new ServiceChargeProperties();
            string message = objMST.fillServiceChargeObject(ref serProperties);

            UserControl ucProvisionItem = (UserControl)ucItemDetail.FindControl("ucProvisionItem");
            ProvisionItem objProvisionItem = (ProvisionItem)ucProvisionItem;
            
            ProvisionChargeProperties pcProperties = new ProvisionChargeProperties();
            result = objProvisionItem.FillProvisionItemDetailObject(ref objProvisionBO, ref pcProperties);

            if (uiMessage.isError == false && result == string.Empty)
            {
                objProvisionBO.SchemeId = schemeId;
                objProvisionBO.BlockId = blockId;
                objProvisionBO.UpdatedBy = objSession.EmployeeId;
                objProvisionBO.ExistingProvisionId = objSession.TreeItemId;

                if (objSession.TreeItemId == -1)
                    objProvisionBO.ProvisionParentId = null;
                else
                    objProvisionBO.ProvisionParentId = objSession.TreeItemParentId;

                objProvisionsBL.SaveProvisionChargeProperties(pcProperties, GeneralHelper.convertToDataTable(pcProperties.excludedIncludedProperties), objSession.TreeItemId);

                result = objProvisionsBL.UpdateProvsionItemDetail(objProvisionBO, GeneralHelper.convertToDataTable(msatDetailList));                    
                if (result == "success")
                {
                    if (objProvisionBO.ProvisionId > 0)
                    {
                        objSession.TreeItemId = objProvisionBO.ProvisionId;
                        uiMessage.messageText = UserMessageConstants.ProvisionUpdatedSuccessfully;
                        uiMessage.showInformationMessage(uiMessage.messageText);
                    }
                }
                else if (result == "failed")
                {

                    uiMessage.messageText = UserMessageConstants.ProvisionNotUpdated;
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }

                
                DataSet provisionsDS = objProvisionsBL.GetProvisionsData(schemeId, blockId);
                objSession.ProvisionDataSet = provisionsDS;


                if (message == string.Empty)
                {
                    objAttributeBL.saveServiceChargeProperties(serProperties, GeneralHelper.convertToDataTable(serProperties.excludedIncludedProperties), objSession.TreeItemId, 0);
                }
            }
            else
            {
                if (result != string.Empty)
                {
                    uiMessage.messageText = result;
                }
                uiMessage.showErrorMessage(uiMessage.messageText);
            }

        }

       
        public bool validateData()
        {
           
            return true;

        }

        public void resetControls()
        {
            throw new NotImplementedException();
        }

        public void loadData()
        {
            throw new NotImplementedException();
        }

        public void populateDropDown(DropDownList ddl)
        {
            throw new NotImplementedException();
        }

        public void populateDropDowns()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region "Helper Functions"
        private void GetQueryStringParams()
        {
            if ((Request.QueryString[ApplicationConstants.Id] != null))
                id = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);

            if ((Request.QueryString[ApplicationConstants.RequestType] != null))
                requestType = Request.QueryString[ApplicationConstants.RequestType];

        }
        public void SetSelectedLinkItemStyle(ref LinkButton lnkBtnSelected)
        {
            this.lnkBtnDetailTab.CssClass = ApplicationConstants.TabInitialCssClass;
            this.lnkBtnDisposalsTab.CssClass = ApplicationConstants.TabInitialCssClass;
            this.lnkBtnNotesTab.CssClass = ApplicationConstants.TabInitialCssClass;
            this.lnkBtnPhotographsTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnSelected.CssClass = ApplicationConstants.TabClickedCssClass;
        }


        #endregion

        protected void AssignToContractorBtnClose_Click(object sender, EventArgs e)
        {
            try
            {
                mdlpopupRaisePOProvision.Hide();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        protected void btnRaisePO_Click(object sender, EventArgs e)
        {
            try
            {
                int? schemeId = null, blockId = null;
                this.GetQueryStringParams();
                if (requestType == ApplicationConstants.Scheme)
                {
                    schemeId = id;
                    blockId = objSession.BlockId;
                }
                else if (requestType == ApplicationConstants.Block)
                {
                    blockId = Convert.ToInt32(id);
                    DataSet blockDetailDataset = new DataSet();
                    blockDetailDataset = objContractorBL.getBlockDetailByBlockId(Convert.ToInt32(blockId));

                    schemeId = Convert.ToInt32(blockDetailDataset.Tables[0].Rows[0]["schemeId"]);
                    DataSet resultDataSet = new DataSet();
                    SchemeBL objSchemeBl = new SchemeBL(new SchemeRepo());
                    resultDataSet = objSchemeBl.getSchemeDetailBySchemeId(Convert.ToInt32(schemeId));
                    objSession.SchemeDetail = new DataSet();
                    //if (resultDataSet.Tables[0].Rows.Count > 0)
                    //{
                        objSession.SchemeDetail = resultDataSet;
                    //}

                    objSession.BlockName = blockDetailDataset.Tables[0].Rows[0]["BlockName"].ToString();
                }

                objSession.SchemeId = (schemeId == null ? 0 : Convert.ToInt32(schemeId));
                DataSet schemeDetailDataSet = new DataSet();
                schemeDetailDataSet = objSession.SchemeDetail;
                if (schemeDetailDataSet.Tables[0].Rows.Count > 0)
                {
                    objSession.SchemeName = schemeDetailDataSet.Tables[0].Rows[0]["SCHEMENAME"].ToString();
                }
                objSession.BlockId = (blockId == null ? 0 : Convert.ToInt32(blockId));
                AssignToContractorBo objAssignToContractorBo = new AssignToContractorBo();
                objAssignToContractorBo.BlockId = (blockId == null ? 0 : Convert.ToInt32(blockId));
                objAssignToContractorBo.SchemeId = (schemeId == null ? 0 : Convert.ToInt32(schemeId));
                objAssignToContractorBo.MsatType = "Scheme Block PO";
                objSession.AssignToContractorBo = objAssignToContractorBo;

                ucAssignToContractor.populateControl();

                mdlpopupRaisePOProvision.Show();
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "loadSelect2();", true);
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        private int GetSchemeByBlockId(int blockId)
        {
            int schemeId = 0;
            AssignToContractorBL objAssignToContractorBL = new AssignToContractorBL(new AssignToContractorRepo());
            DataSet schemeDS = new DataSet();
            schemeDS = objAssignToContractorBL.GetSchemeByBlockId(blockId);

            if (schemeDS.Tables[0].Rows.Count > 0)
            {
                schemeId = int.Parse(schemeDS.Tables[0].Rows[0]["SchemeId"].ToString());
            }
            return schemeId;
        }

        protected void btnBookFault_OnClick(object sender, EventArgs e)
        {
            try
            {
                int? schemeId = null, blockId = null;
                this.GetQueryStringParams();
                if (requestType == ApplicationConstants.Scheme)
                {
                    schemeId = id;
                    blockId = objSession.BlockId;
                }          

                objSession.SchemeId = (schemeId == null ? 0 : Convert.ToInt32(schemeId));
                DataSet schemeDetailDataSet = new DataSet();
                schemeDetailDataSet = objSession.SchemeDetail;
                if (schemeDetailDataSet.Tables[0].Rows.Count > 0)
                {
                    objSession.SchemeName = schemeDetailDataSet.Tables[0].Rows[0]["SCHEMENAME"].ToString();
                }
                objSession.BlockId = (blockId == null ? 0 : Convert.ToInt32(blockId));
                AssignToContractorBo objAssignToContractorBo = new AssignToContractorBo();
                objAssignToContractorBo.BlockId = (blockId == null ? 0 : Convert.ToInt32(blockId));
                objAssignToContractorBo.SchemeId = (schemeId == null ? 0 : Convert.ToInt32(schemeId));
                objAssignToContractorBo.MsatType = "Scheme Block PO";
                objSession.AssignToContractorBo = objAssignToContractorBo;

                ucAssignToContractor.populateControl();

                mdlpopupRaisePOProvision.Show();
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "Pop", "$('#poModalRaiseaPO').modal('show');", true);               
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }        
    }
}