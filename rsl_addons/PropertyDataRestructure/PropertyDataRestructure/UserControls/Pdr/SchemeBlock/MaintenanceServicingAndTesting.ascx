﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MaintenanceServicingAndTesting.ascx.cs"
    Inherits="PropertyDataRestructure.UserControls.Pdr.SchemeBlock.MaintenanceServicingAndTesting" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Panel ID="pnlEditable" runat="server" Visible="false">
    <table width="100%" cellpadding="5">
        <tr>
            <td colspan="2">
                <asp:Panel runat="server" ID="pnlPrePAT" Style="display: none;">
                    <table cellpadding="5">
                        <tr>
                            <td colspan="2" align="left">
                                <b>PAT Testing:</b>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="border-top-style: solid; border-width: thin; border-color: #c5c5c5">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 150px;">
                                PAT Testing?:
                            </td>
                            <td align="left" style="padding-left: 20px;">
                                <asp:RadioButtonList runat="server" ID="rdBtnPATTesting" RepeatDirection="Horizontal"
                                    CellPadding="2" Width="200">
                                    <asp:ListItem Text="Yes" Value="1" />
                                    <asp:ListItem Text="No" Value="0" />
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Panel runat="server" ID="pnlPATTesting" Style="display: none;">
                    <table cellpadding="5">
                        <tr>
                            <td align="right" style="width: 150px;">
                                Last Test Date:
                            </td>
                            <td align="left" style="padding-left: 20px;">
                                <asp:TextBox ID="txtTestDate" runat="server" Width="150px"></asp:TextBox>
                                <asp:CalendarExtender ID="txtRentDate_CalendarExtender" runat="server" Format="dd/MM/yyyy"
                                    PopupButtonID="imgRentCalendar" TargetControlID="txtTestDate" />
                                <asp:Image ID="imgRentCalendar" runat="server" ImageUrl="~/Images/Calendar.png" />
                                <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="txtTestDate"
                                    ErrorMessage="<br/> Enter a valid date" Operator="DataTypeCheck" Type="Date"
                                    Display="Dynamic" CssClass="Required" ValidationGroup="save" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 150px;">
                                Testing Cycle:
                            </td>
                            <td align="left" style="padding-left: 20px;">
                                Every&nbsp;
                                <asp:TextBox ID="txtTestingCycle" runat="server" Width="50px" Style="padding-right: 20px;"
                                    onkeyup="NumberOnly(this);"></asp:TextBox>
                                <asp:DropDownList runat="server" ID="ddlTestingCycle" Width="130px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 150px;">
                                Next Test Date:
                            </td>
                            <td align="left" style="padding-left: 20px;">
                                <asp:TextBox ID="txtNextTestDate" runat="server" Width="150px"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy" PopupButtonID="imgNextTestDate"
                                    TargetControlID="txtNextTestDate" />
                                <asp:Image ID="imgNextTestDate" runat="server" ImageUrl="~/Images/Calendar.png" />
                                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtNextTestDate"
                                    ErrorMessage="<br/> Enter a valid date" Operator="DataTypeCheck" Type="Date"
                                    Display="Dynamic" CssClass="Required" ValidationGroup="save" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2" align="left">
                <b>Service Item:</b>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="border-top-style: solid; border-width: thin; border-color: #c5c5c5">
                &nbsp;
            </td>
        </tr>
        <%--        add BFS Carrying Out Work --%>
        <tr>
            <td align="right" style="width: 150px;">
                BFS carrying out work:
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:RadioButtonList runat="server" ID="rdBtnBFSCarryingOutWork" RepeatDirection="Horizontal"
                    CellPadding="2" Width="200" AutoPostBack="true" OnSelectedIndexChanged="updateContractor">
                    <asp:ListItem Text="Yes" Value="1" />
                    <asp:ListItem Text="No" Value="0" />
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 150px;">
                Contractor:
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:DropDownList runat="server" ID="ddlContractor">
                    <asp:ListItem Text="N/A" Value="N/A"></asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 150px;">
                Cycle Commencement:
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:TextBox ID="txtContractCommencement" runat="server" Width="150px"></asp:TextBox>
                <asp:CalendarExtender ID="txtContractCommencement_CalendarExtender" runat="server"
                    Format="dd/MM/yyyy" PopupButtonID="imgContractCommencement" TargetControlID="txtContractCommencement" />
                <asp:Image ID="imgContractCommencement" runat="server" ImageUrl="~/Images/Calendar.png" />
                <asp:CompareValidator ID="CompareValidator7" runat="server" ControlToValidate="txtContractCommencement"
                    ErrorMessage="<br/> Enter a valid date" Operator="DataTypeCheck" Type="Date"
                    Display="Dynamic" CssClass="Required" ValidationGroup="save" />
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 150px;">
                Contract Period:
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:TextBox ID="txtContractPeriod" runat="server" Width="50px" Style="padding-right: 20px;"
                    onkeyup="NumberOnly(this);"></asp:TextBox>
                <asp:DropDownList runat="server" ID="ddlContractPeriod" Width="130px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 150px;">
                Cycle:
            </td>
            <td align="left" style="padding-left: 20px;">
                Every &nbsp;
                <asp:TextBox ID="txtCycle" runat="server" Width="50px" Style="padding-right: 20px;"
                    onkeyup="NumberOnly(this);"></asp:TextBox>
                <asp:DropDownList runat="server" ID="ddlCycle" Width="130px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td>
                <asp:Label ID="CustomCycleFrequency" CssClass="CustomCycleFrequency" runat="server"
                    Style="display: none" />
                <asp:Label ID="CustomCycleOccurance" CssClass="CustomCycleOccurance" runat="server"
                    Style="display: none" />
                <asp:Label ID="CustomCycleType" CssClass="CustomCycleType" runat="server" Style="display: none" />
                <button id="btnCustom" type="button" class="btn btn-xs btn-blue" onclick="LoadFrequencyPopUp()"
                    style="padding: 2px 20px !important;">
                    Custom</button>
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 150px;">
                Cycle Value(&pound;):
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:TextBox runat="server" ID="txtCycleCost" Width="150px" />
                <asp:RangeValidator ID="CycleCostValidator" ErrorMessage='<br/> Please enter a value between "0" and "100000.00".'
                    ControlToValidate="txtCycleCost" runat="server" MinimumValue="0" MaximumValue="100000.00"
                    Display="Dynamic" CssClass="Required" ValidationGroup="save" Type="Double" />
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 150px;">
                VAT
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:DropDownList runat="server" ID="DropDownListVATList" Width="130px" AutoPostBack="true"
                    OnSelectedIndexChanged="DropDownListVATList_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 150px;">
                Cycle Total Value(&pound;):
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:TextBox runat="server" ID="TextBoxTotalValue" Width="150px" Enabled="false" />
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 150px;">
                Current Contract:
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:Literal Text="NA" ID="ltrlCurrentContract" runat="server" />
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 150px;">
                PO Ref:
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:Literal Text="NA" ID="ltrlPoRef" runat="server" />
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="2" align="left">
                <b>Service Charge:</b>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="border-top-style: solid; border-width: thin; border-color: #c5c5c5">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 150px;">
                Service Charge Item:
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:RadioButtonList runat="server" ID="rdBtnServiceCharge" RepeatDirection="Horizontal"
                    CellPadding="2">
                    <asp:ListItem Text="Yes" Value="1" />
                    <asp:ListItem Text="No" Value="0" />
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 150px;">
                Annual Apportionment (£):
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:TextBox runat="server" ID="txtApportionment" />
                <asp:RegularExpressionValidator ID="rxvEstimate" runat="server" ErrorMessage="<br/>Please enter estimate, in following form XXXXXX.XXXX where X is a digit between 0 to 9."
                    Display="Dynamic" ValidationGroup="save" CssClass="Required" ControlToValidate="txtApportionment"
                    ValidationExpression="^[0-9]{1,6}(?:\.[0-9]{1,4})?$" />
                <asp:RangeValidator ID="rvEstimate" ErrorMessage='<br/> Please enter a value between "0" and "214748.3647".'
                    ControlToValidate="txtApportionment" runat="server" MinimumValue="0" MaximumValue="214748.3647"
                    Display="Dynamic" CssClass="Required" ValidationGroup="save" Type="Double" />
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 150px;">
                <span runat="server" id="blockSpan">Property Allocation:</span>
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:DropDownList runat="server" ID="ddlBlock" Width="130px" OnSelectedIndexChanged="blockIndex_changed"
                    AutoPostBack="true">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 150px;">
                <span runat="server" id="Span1">Properties</span>:
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:ListBox runat="server" ID="ddlProperties" Width="400px" ClientIDMode="Static"
                    OnInit="ddlProperties_Load" SelectionMode="Multiple"></asp:ListBox>
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
    </table>
    <table id="tblMEServicing" runat="server" >
        <%----------------- M&E Servicing -----------%>
        <tr>
            <td colspan="2" align="left">
                <b>M&E Servicing:</b>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="border-top-style: solid; border-width: thin; border-color: #c5c5c5">
                &nbsp;
            </td>
        </tr>
        <tr>
            <td align="right" style="width: 150px;">
                Servicing Required?:
            </td>
            <td align="left" style="padding-left: 20px;">
                <asp:RadioButtonList runat="server" ID="rdBtnServicingRequired" RepeatDirection="Horizontal"
                    CellPadding="5">
                    <asp:ListItem Text="Yes" Value="1" />
                    <asp:ListItem Text="No" Value="0" />
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Panel ID="Panel3" runat="server">
                    <table cellpadding="5">
                        <tr>
                            <td align="right" style="width: 150px;">
                                Last Service Date:
                            </td>
                            <td align="left" style="padding-left: 20px;">
                                <asp:TextBox ID="txtLastServiceDate" runat="server" Width="150px"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender4" runat="server" Format="dd/MM/yyyy" PopupButtonID="imgLastServiceDate"
                                    TargetControlID="txtLastServiceDate" />
                                <asp:Image ID="imgLastServiceDate" runat="server" ImageUrl="~/Images/Calendar.png" />
                                <asp:CompareValidator ID="CompareValidator5" runat="server" ControlToValidate="txtLastServiceDate"
                                    ErrorMessage="<br/> Enter a valid date" Operator="DataTypeCheck" Type="Date"
                                    Display="Dynamic" CssClass="Required" ValidationGroup="save" />
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 150px;">
                                Service Cycle:
                            </td>
                            <td align="left" style="padding-left: 20px;">
                                Every&nbsp;
                                <asp:TextBox ID="txtServiceCycle" runat="server" Width="50px" Style="padding-right: 20px;"
                                    onkeyup="NumberOnly(this);"></asp:TextBox>
                                <asp:DropDownList runat="server" ID="ddlServiceCycle" Width="130px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="right" style="width: 150px;">
                                Next Service Date:
                            </td>
                            <td align="left" style="padding-left: 20px;">
                                <asp:TextBox ID="txtNextServiceDate" runat="server" Width="150px"></asp:TextBox>
                                <asp:CalendarExtender ID="CalendarExtender5" runat="server" Format="dd/MM/yyyy" PopupButtonID="imgNextServiceDate"
                                    TargetControlID="txtNextServiceDate" />
                                <asp:Image ID="imgNextServiceDate" runat="server" ImageUrl="~/Images/calendar.png" />
                                <asp:CompareValidator ID="CompareValidator6" runat="server" ControlToValidate="txtNextServiceDate"
                                    ErrorMessage="<br/> Enter a valid date" Operator="DataTypeCheck" Type="Date"
                                    Display="Dynamic" CssClass="Required" ValidationGroup="save" />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Panel>
<div class="modal fade" id="frequencyPopUp" role="dialog" style="display: none">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row padding-bottom-5">
                            <div class="col-md-9">
                                <table>
                                    <tr>
                                        <td align="left" style="width: 150px;">
                                            Frequency :
                                        </td>
                                        <td align="left">
                                            <asp:DropDownList runat="server" ID="periodDropdownList" onchange="frequencySelector(this)">
                                                <asp:ListItem Enabled="true" Text="Daily" Value="Daily"></asp:ListItem>
                                                <asp:ListItem Text="Weekly" Value="Weekly"></asp:ListItem>
                                                <asp:ListItem Text="Monthly" Value="Monthly"></asp:ListItem>
                                                <asp:ListItem Text="Yearly" Value="Yearly"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Every :
                                        </td>
                                        <td>
                                            <%--<asp:TextBox ID="CustomFrequency"
TextMode="Number" runat="server" style="width:50px"></asp:TextBox>--%>
                                            <asp:TextBox runat="server" ID="CustomFrequency" Style="width: 50px" />
                                            <span id="everyStatment" runat="server" clientidmode="Static">day(s)</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div id="weeks" class="weekDays-selector" style="display: none" runat="server" clientidmode="Static">
                                                <asp:CheckBoxList ID="chkWeeklyList" runat="server" RepeatDirection="Horizontal">
                                                    <asp:ListItem Value="Mon">M</asp:ListItem>
                                                    <asp:ListItem Value="Tue">T</asp:ListItem>
                                                    <asp:ListItem Value="Wed">W</asp:ListItem>
                                                    <asp:ListItem Value="Thu">T</asp:ListItem>
                                                    <asp:ListItem Value="Fri">F</asp:ListItem>
                                                    <asp:ListItem Value="Sat">S</asp:ListItem>
                                                    <asp:ListItem Value="Sun">S</asp:ListItem>
                                                </asp:CheckBoxList>
                                            </div>
                                            <div id="days" class="weekDays-selector" style="display: none" runat="server" clientidmode="Static">
                                                <asp:CheckBoxList ID="chkDailyList" runat="server" RepeatDirection="Horizontal" RepeatColumns="7">
                                                    <asp:ListItem Value="1" onclick="OnlyOneSelected(this)">1</asp:ListItem>
                                                    <asp:ListItem Value="2" onclick="OnlyOneSelected(this)">2</asp:ListItem>
                                                    <asp:ListItem Value="3" onclick="OnlyOneSelected(this)">3</asp:ListItem>
                                                    <asp:ListItem Value="4" onclick="OnlyOneSelected(this)">4</asp:ListItem>
                                                    <asp:ListItem Value="5" onclick="OnlyOneSelected(this)">5</asp:ListItem>
                                                    <asp:ListItem Value="6" onclick="OnlyOneSelected(this)">6</asp:ListItem>
                                                    <asp:ListItem Value="7" onclick="OnlyOneSelected(this)">7</asp:ListItem>
                                                    <asp:ListItem Value="8" onclick="OnlyOneSelected(this)">8</asp:ListItem>
                                                    <asp:ListItem Value="9" onclick="OnlyOneSelected(this)">9</asp:ListItem>
                                                    <asp:ListItem Value="10" onclick="OnlyOneSelected(this)">10</asp:ListItem>
                                                    <asp:ListItem Value="11" onclick="OnlyOneSelected(this)">11</asp:ListItem>
                                                    <asp:ListItem Value="12" onclick="OnlyOneSelected(this)">12</asp:ListItem>
                                                    <asp:ListItem Value="13" onclick="OnlyOneSelected(this)">13</asp:ListItem>
                                                    <asp:ListItem Value="14" onclick="OnlyOneSelected(this)">14</asp:ListItem>
                                                    <asp:ListItem Value="15" onclick="OnlyOneSelected(this)">15</asp:ListItem>
                                                    <asp:ListItem Value="16" onclick="OnlyOneSelected(this)">16</asp:ListItem>
                                                    <asp:ListItem Value="17" onclick="OnlyOneSelected(this)">17</asp:ListItem>
                                                    <asp:ListItem Value="18" onclick="OnlyOneSelected(this)">18</asp:ListItem>
                                                    <asp:ListItem Value="19" onclick="OnlyOneSelected(this)">19</asp:ListItem>
                                                    <asp:ListItem Value="20" onclick="OnlyOneSelected(this)">20</asp:ListItem>
                                                    <asp:ListItem Value="21" onclick="OnlyOneSelected(this)">21</asp:ListItem>
                                                    <asp:ListItem Value="22" onclick="OnlyOneSelected(this)">22</asp:ListItem>
                                                    <asp:ListItem Value="23" onclick="OnlyOneSelected(this)">23</asp:ListItem>
                                                    <asp:ListItem Value="24" onclick="OnlyOneSelected(this)">24</asp:ListItem>
                                                    <asp:ListItem Value="25" onclick="OnlyOneSelected(this)">25</asp:ListItem>
                                                    <asp:ListItem Value="26" onclick="OnlyOneSelected(this)">26</asp:ListItem>
                                                    <asp:ListItem Value="27" onclick="OnlyOneSelected(this)">27</asp:ListItem>
                                                    <asp:ListItem Value="28" onclick="OnlyOneSelected(this)">28</asp:ListItem>
                                                    <asp:ListItem Value="29" onclick="OnlyOneSelected(this)">29</asp:ListItem>
                                                    <asp:ListItem Value="30" onclick="OnlyOneSelected(this)">30</asp:ListItem>
                                                    <asp:ListItem Value="31" onclick="OnlyOneSelected(this)">31</asp:ListItem>
                                                </asp:CheckBoxList>
                                            </div>
                                            <div id="month" class="yearly-selector" style="display: none" runat="server" clientidmode="Static">
                                                <asp:CheckBoxList ID="chkMonthlyList" runat="server" RepeatDirection="Horizontal"
                                                    RepeatColumns="4">
                                                    <asp:ListItem Value="Jan" onclick="OnlyOneSelected(this)">Jan</asp:ListItem>
                                                    <asp:ListItem Value="Feb" onclick="OnlyOneSelected(this)">Feb</asp:ListItem>
                                                    <asp:ListItem Value="Mar" onclick="OnlyOneSelected(this)">Mar</asp:ListItem>
                                                    <asp:ListItem Value="Apr" onclick="OnlyOneSelected(this)">Apr</asp:ListItem>
                                                    <asp:ListItem Value="May" onclick="OnlyOneSelected(this)">May</asp:ListItem>
                                                    <asp:ListItem Value="Jun" onclick="OnlyOneSelected(this)">Jun</asp:ListItem>
                                                    <asp:ListItem Value="Jul" onclick="OnlyOneSelected(this)">Jul</asp:ListItem>
                                                    <asp:ListItem Value="Aug" onclick="OnlyOneSelected(this)">Aug</asp:ListItem>
                                                    <asp:ListItem Value="Sep" onclick="OnlyOneSelected(this)">Sep</asp:ListItem>
                                                    <asp:ListItem Value="Oct" onclick="OnlyOneSelected(this)">Oct</asp:ListItem>
                                                    <asp:ListItem Value="Nov" onclick="OnlyOneSelected(this)">Nov</asp:ListItem>
                                                    <asp:ListItem Value="Dec" onclick="OnlyOneSelected(this)">Dec</asp:ListItem>
                                                </asp:CheckBoxList>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <button id="frequencyPopUpOkBtn" class="btn btn-xs
btn-blue" type="button" onclick="OkFrequencyPopUp()">
                                                OK</button>
                                        </td>
                                        <td>
                                            <button id="frequencyPopUpCloseBtn" class="btn btn-xs btn-blue" type="button" onclick="CloseFrequencyPopUp()">
                                                Close</button>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            </div>
        </div>
    </div>
</div>
