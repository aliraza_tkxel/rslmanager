﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PropertyDataRestructure.Base;
using PropertyDataRestructure.Interface;
using PDR_Utilities.Constants;
using PDR_Utilities.Managers;
using PDR_BusinessLogic.SchemeBlock;
using System.Data;
using PDR_DataAccess.SchemeBlock;
using PDR_BusinessObject.PageSort;
using PDR_Utilities.Helpers;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_BusinessObject.SchemeBlock;

namespace PropertyDataRestructure.UserControls.Pdr.SchemeBlock
{
    public partial class ServiceDefects : UserControlBase
    {
        AttributesBL objPropertyBL = new AttributesBL(new AttributesRepo());

        #region User Contraol Events

        public delegate void viewDefect_ClickedEventHandler(object sender, EventArgs e);
        private viewDefect_ClickedEventHandler viewDefect_ClickedEvent;

        public event viewDefect_ClickedEventHandler viewDefect_Clicked
        {
            add
            {
                viewDefect_ClickedEvent = (viewDefect_ClickedEventHandler)System.Delegate.Combine(viewDefect_ClickedEvent, value);
            }
            remove
            {
                viewDefect_ClickedEvent = (viewDefect_ClickedEventHandler)System.Delegate.Remove(viewDefect_ClickedEvent, value);
            }
        }


        #endregion

        #region Properties

        public int PropertyDefectId = 0;

        #endregion

        protected void Page_Load(object sender, System.EventArgs e)
        {
            uiMessageHelper.resetMessage(ref lblMessage, ref pnlMessage);
        }

        protected void View_Click(object sender, EventArgs e)
        {
            try
            {

                uiMessageHelper.resetMessage(ref lblMessage, ref pnlMessage);
                uiMessageHelper.Message = "";
                ApplianceDefectBO objApplianceDefectBO = new ApplianceDefectBO();



                Appliance objAppliance = new Appliance();
                string Id = System.Convert.ToString(Request.QueryString[ApplicationConstants.Id]);
                string requestType = System.Convert.ToString(Request.QueryString[ApplicationConstants.RequestType]);
                GridViewRow gvr = (GridViewRow)((Button)sender).Parent.Parent;
                int index = System.Convert.ToInt32(gvr.RowIndex);
                Button btnView = (Button)((Button)(grdServiceDefects.Rows[index].FindControl("btnView")));
                PropertyDefectId = System.Convert.ToInt32(btnView.CommandArgument);
                objPropertyBL.getSchemeBlockDefectDetails(PropertyDefectId, ref objApplianceDefectBO);

                //Dim btnView As Button = CType(sender, Button)
                //PropertyDefectId = CType(btnView.CommandArgument, Integer)
                //RaiseEvent viewDefect_Clicked(sender, e)

                uiMessageHelper.resetMessage(ref lblMessage, ref pnlMessage);
                uiMessageHelper.Message = "";

                // objPropertyBL.getPropertyDefectDetails(propertyDefectId, objApplianceDefectBO)

                //uiMessageHelper.resetMessage(lblMessage, pnlMessage)
                //uiMessageHelper.Message = ""
                //Dim objApplianceDefectBO As ApplianceDefectBO = New ApplianceDefectBO()
                // Dim objAppliance As Appliance = New Appliance()
                // Dim propertyId As String = Request.QueryString(PathConstants.PropertyIds)
                // Dim gvr As GridViewRow = (CType(sender, Button)).Parent.Parent
                // Dim index As Integer = gvr.RowIndex
                //Dim btnView As Button = CType((grdServiceDefects.Rows(index).FindControl("btnView")), Button)
                //Dim propertyDefectId As Integer = CType(btnView.CommandArgument, Integer)
                //objPropertyBL.getPropertyDefectDetails(PropertyDefectId, objApplianceDefectBO)
                DefectManagement ucDefectManagement = Parent.FindControl("ucDefectManagement") as DefectManagement;
                ucDefectManagement.viewDefectDetails(PropertyDefectId);
                AjaxControlToolkit.ModalPopupExtender mdlPoPUpAddDefect = Parent.FindControl("mdlPoPUpAddDefect") as AjaxControlToolkit.ModalPopupExtender;
                mdlPoPUpAddDefect.Show();
                lblDate.Text = objApplianceDefectBO.DefectDate.ToString();
                lblCategory.Text = getCategoryName(System.Convert.ToInt32(objApplianceDefectBO.CategoryId));
                lblAppliances.Text = getApplianceName(Id, requestType, System.Convert.ToInt32(objApplianceDefectBO.ApplianceId));
                if (objApplianceDefectBO.IsRemedialActionTaken == true)
                {
                    lblActionTaken.Text = "Yes";
                }
                else
                {
                    lblActionTaken.Text = "No";
                }

                lblDate.Text = objApplianceDefectBO.DefectDate.ToString();
                lblCategory.Text = getCategoryName(System.Convert.ToInt32(objApplianceDefectBO.CategoryId));
                //lblAppliances.Text = getApplianceName(propertyId, objApplianceDefectBO.ApplianceId)
                if (objApplianceDefectBO.IsRemedialActionTaken == true)
                {
                    lblActionTaken.Text = "Yes";
                }
                else
                {
                    lblActionTaken.Text = "No";
                }

                // If objApplianceDefectBO.DefectIdentified = True Then
                //lDefectIdentified.Text = "Yes"
                // Else
                // lblDefectIdentified.Text = "No"
                // End If
                if (objApplianceDefectBO.IsWarningNoteIssued == true)
                {
                    lblWarningIssued.Text = "Yes";
                }
                else
                {
                    lblWarningIssued.Text = "No";
                }



                //If objApplianceDefectBO.DefectIdentified = True Then
                //    lblDefectIdentified.Text = "Yes"
                //Else
                //    lblDefectIdentified.Text = "No"
                //End If
                if (objApplianceDefectBO.IsWarningNoteIssued == true)
                {
                    lblWarningIssued.Text = "Yes";
                }
                else
                {
                    lblWarningIssued.Text = "No";
                }
                if (objApplianceDefectBO.IsWarningTagFixed == true)
                {
                    lblWarningTag.Text = "Yes";
                }
                else
                {
                    lblWarningTag.Text = "No";
                }
                lblSerialNumber.Text = objApplianceDefectBO.SerialNumber;


                if (objApplianceDefectBO.IsWarningTagFixed == true)
                {
                    lblWarningTag.Text = "Yes";
                }
                else
                {
                    lblWarningTag.Text = "No";
                }
                lblSerialNumber.Text = objApplianceDefectBO.SerialNumber;


                lblActionNotes.Text = Server.UrlDecode(objApplianceDefectBO.RemedialActionNotes);
                lblDefectNotes.Text = Server.UrlDecode(objApplianceDefectBO.DefectIdentifiedNotes);
                lblPhotoNotes.Text = Server.UrlDecode(objApplianceDefectBO.PhotosNotes);
                mdlPoPUpDefectDetails.Show();


            }
            catch (Exception ex)
            {
                uiMessageHelper.IsError = true;
                uiMessageHelper.Message = ex.Message;

                if (uiMessageHelper.IsExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessageHelper.IsError == true)
                {
                    uiMessageHelper.setMessage(ref lblMessage, ref pnlMessage, uiMessageHelper.Message, true);
                }
            }
        }

        #region load Category DDL

        public dynamic getCategoryName(int categoryId)
        {
            DataSet resultDataSet = new DataSet();
            DataTable resdatatable = new DataTable();
            string categoryName = string.Empty;
            try
            {
                objPropertyBL.loadCategoryDDL(ref resultDataSet);

                resdatatable = resultDataSet.Tables[0];

                //query the data set using this will return the data row
                var customerQuery = (from dataRow in resdatatable.AsEnumerable()
                                     where
                                     dataRow.Field<int>("CategoryId") == categoryId
                                     select dataRow).ToList();

                int a = 0;

                //Query the data table
                DataTable datatable = customerQuery.CopyToDataTable();

                categoryName = System.Convert.ToString((Convert.IsDBNull(datatable.Rows[0].ItemArray[1]) == true) ? "" : (datatable.Rows[0].ItemArray[1]));

            }
            catch (Exception ex)
            {
                uiMessageHelper.IsError = true;
                uiMessageHelper.Message = ex.Message;

                if (uiMessageHelper.IsExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }

            }
            finally
            {
                if (uiMessageHelper.IsError == true)
                {
                    uiMessageHelper.setMessage(ref lblMessage, ref pnlMessage, uiMessageHelper.Message, true);
                }
            }
            return categoryName;
        }
        #endregion

        #region get Scheme/Block Name
        public dynamic getApplianceName(string Id, string requestType, int applianceId)
        {
            DataSet resultDataSet = new DataSet();
            DataTable resdatatable = new DataTable();
            string applianceName = string.Empty;
            try
            {
                objPropertyBL.loadSchemeBlockAppliancesDDL(Id, requestType, ref resultDataSet);
                resdatatable = resultDataSet.Tables[0];
                //query the data set using this will return the data row
                var customerQuery = (from dataRow in resdatatable.AsEnumerable() where (int)dataRow.ItemArray[0] == applianceId select dataRow).ToList();

                if (customerQuery.Count > 0)
                {
                    applianceName = System.Convert.ToString((Convert.IsDBNull(customerQuery.First().ItemArray[1]) == true) ? "" : (customerQuery.First().ItemArray[1]));
                }



            }
            catch (Exception ex)
            {
                uiMessageHelper.IsError = true;
                uiMessageHelper.Message = ex.Message;

                if (uiMessageHelper.IsExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessageHelper.IsError == true)
                {
                    uiMessageHelper.setMessage(ref lblMessage, ref pnlMessage, uiMessageHelper.Message, true);
                }
            }
            return applianceName;
        }
        #endregion

        #region get Defect Notes
        public dynamic checkNotesExists(bool DefectNotes)
        {
            if (DefectNotes == true)
            {
                return "Yes";
            }
            else
            {
                return "No";
            }
        }

        #endregion

        #region Grid Service Defects Row data bound event
        protected void grdServiceDefects_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var btnView = (Button)(e.Row.FindControl("btnView"));
                var hdnDefectType = (HiddenField)(e.Row.FindControl("hdnDefectType"));
                //If hdnDefectType.Value.Equals(ApplicationConstants.BoilerDefectType) Then
                //    btnView.Visible = False
                //End If
            }
        }
        #endregion



    }
}