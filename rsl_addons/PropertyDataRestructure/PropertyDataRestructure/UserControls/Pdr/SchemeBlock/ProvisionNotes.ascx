﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ProvisionNotes.ascx.cs" Inherits="PropertyDataRestructure.UserControls.Pdr.SchemeBlock.ProvisionNotes" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="500px" />
<asp:UpdatePanel ID="updPanelNotes" runat="server">
    <ContentTemplate>
         <div style="float: right; margin: -8px 0px 0 0px;">
            <asp:Button runat="server" ID="btnAddNotes" Text="Add Notes" BackColor="White" OnClick="btnAddNotes_Click" />
        </div>
           <asp:GridView ID="grdItemNotes" runat="server" AutoGenerateColumns="false" GridLines="None"
            ShowHeaderWhenEmpty="false" Width="100%" EmptyDataText="No Record Found">
            <Columns>
                <asp:BoundField DataField="CreatedOn" HeaderText="Date:">
                    <ItemStyle Width="15%" VerticalAlign="Top" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="Notes:">
                    <ItemTemplate>
                        <asp:Label ID="Label1" Text='<%# Eval("Notes")%>' runat="server" Width="90%" Style='margin-bottom: 10px;' />
                    </ItemTemplate>
                    <ItemStyle Width="55%" />
                </asp:TemplateField>
                <asp:BoundField DataField="CreatedBy" HeaderText="By:">
                    <ItemStyle Width="30%" />
                </asp:BoundField>
            </Columns>
            <RowStyle BackColor="#EFF3FB"></RowStyle>
            <EditRowStyle BackColor="#2461BF"></EditRowStyle>
            <SelectedRowStyle BackColor="#D1DDF1" ForeColor="#333333" Font-Bold="True"></SelectedRowStyle>
            <HeaderStyle BackColor="#ffffff" ForeColor="black" Font-Bold="True" HorizontalAlign="Left"
                CssClass="table-head"></HeaderStyle>
            <AlternatingRowStyle BackColor="White" Wrap="True"></AlternatingRowStyle>
        </asp:GridView>
        
        </ContentTemplate>
</asp:UpdatePanel>
<asp:ModalPopupExtender ID="mdlPopUpAddNote" runat="server" DynamicServicePath=""
    Enabled="True" PopupControlID="pnlAddNotePopup" DropShadow="true" CancelControlID="btnCancel"
    TargetControlID="lblDispPhotoGraph" BackgroundCssClass="modalBackground">
</asp:ModalPopupExtender>
<asp:Label ID="lblDispPhotoGraph" runat="server"></asp:Label>
<asp:Panel ID="pnlAddNotePopup" runat="server" BackColor="White" Width="340px">
            <table id="pnlAddNoteTable" width="300px" style='margin: 20px;'>
                <tr>
                </tr>
                <tr>
                    <td colspan="2" valign="top">
                        <div style="float: left; font-weight: bold; padding-left: 10px;">
                            Add New Note</div>
                        <div style="clear: both">
                        </div>
                        <hr />
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top" colspan="2">
                        <uim:UIMessage ID="uiMessageNotes" runat="Server" Visible="false" width="300px" />
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <span style="width: 50px; margin-left: 20px;">Note:<span class="Required">*</span></span>
                    </td>
                    <td align="left" valign="top" style="width: 250px;">
                        <asp:UpdatePanel ID="pnlTxtNotes" runat="server">
                            <ContentTemplate>
                        <asp:TextBox ID="txtNote" runat="server" Width="200" TextMode="MultiLine" Rows="5"
                            MaxLength="500">
                        </asp:TextBox>
                        <asp:RegularExpressionValidator ID = "revTexbox3" runat = "server" 
                        ErrorMessage= "<br/>You must enter up to a maximum of 500 characters" ValidationExpression ="^([\S\s]{0,500})$"
                        ControlToValidate ="txtNote" Display ="Dynamic" CssClass="Required"  ValidationGroup="AddNotes"   ></asp:RegularExpressionValidator>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        &nbsp;
                    </td>
                    <td align="right" valign="top" style="text-align: right;" class="style1">
                        <div style='margin-right: 25px;'>
                            <input id="btnCancel" type="button" value="Cancel" style="background-color: White;" />
                            &nbsp;
                            <asp:Button ID="btnSave" runat="server" Text="Save" BackColor="White" OnClick="btnSave_Click" ValidationGroup="AddNotes" />
                            &nbsp;</div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </asp:Panel>