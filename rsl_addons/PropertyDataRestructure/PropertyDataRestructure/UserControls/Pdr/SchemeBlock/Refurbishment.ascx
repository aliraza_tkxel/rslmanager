﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Refurbishment.ascx.cs"
    Inherits="PropertyDataRestructure.UserControls.Pdr.SchemeBlock.Refurbishment" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>

        <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="500px" />
        <div style="float: left; width: 30%; margin-left: 5px; border: 1px solid #000; padding: 5px;">
            <%--<asp:Panel ID="pnlRefurbishmentForm" runat="server" BackColor="White" Width="300px">--%>
                <table id="pnlRefurbishmentTable">
                    <tr>
                        <td align="left" valign="top" class="style3">
                            Refurbishment Date:<span class="Required">*</span>
                        </td>
                        <td align="left" class="style3" valign="top">
                            <asp:TextBox ID="txtRefurbishmentDate" runat="server" Style="width: 140px; float: left;"></asp:TextBox>
                            <asp:CalendarExtender ID="txtAdded_CalendarExtender" runat="server" Format="dd/MM/yyyy"
                                PopupButtonID="imgAddedCalendar" TargetControlID="txtRefurbishmentDate" />
                            <asp:Image ID="imgAddedCalendar" runat="server" ImageUrl="~/Images/calendar.png"
                                Style="float: right; margin-left: 5px; float: left" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtRefurbishmentDate"
                                ErrorMessage="Required" ValidationGroup="check" CssClass="Required"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" class="style2">
                            Notes:<span class="Required"></span>
                        </td>
                        <td align="left" valign="top" class="style1">
                            <asp:TextBox ID="txtNotes" runat="server" Style="width: 168px;" TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" valign="top" class="style2">
                            &nbsp;
                        </td>
                        <td align="right" valign="top" style="text-align: right;" class="style1">
                            &nbsp;&nbsp;
                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" BackColor="White" ValidationGroup="check"
                                OnClick="btnSubmit_Click" />
                            &nbsp;
                        </td>
                    </tr>
                </table>
            <%--</asp:Panel>--%>
        </div>
        <div id="divRefurbishmentGrid" runat="server" style="float: left; width: 60%; margin-left: 5px;
            border: 1px solid #000; padding: 5px;">
            <asp:GridView ID="grdRefurbishment" runat="server" AutoGenerateColumns="False" Width="100%"
                AllowPaging="True" PageSize="20" BorderStyle="Solid" BorderWidth="1px" CellPadding="4"
                GridLines="None" ShowHeaderWhenEmpty="true" EmptyDataText="No Record Found">
                <Columns>
                    <asp:BoundField HeaderText="Date of Refurbishment" DataField="REFURBISHMENT_DATE">
                        <ItemStyle Width="150px" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Entered By" DataField="USERNAME">
                        <ItemStyle Width="150px" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="Notes" DataField="NOTES">
                        <ItemStyle Width="500px" />
                    </asp:BoundField>
                </Columns>
                <HeaderStyle HorizontalAlign="Left" CssClass="table-head" />
                <RowStyle BackColor="#EFF3FB" />
                <EditRowStyle BackColor="#2461BF" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <AlternatingRowStyle BackColor="White" Wrap="True" />
                <PagerSettings FirstPageText="First" LastPageText="Last" NextPageText="   Next" PreviousPageText="   Previous" />
                <PagerStyle Width="100%" HorizontalAlign="Left" VerticalAlign="Middle" Wrap="True" />
            </asp:GridView>
        </div>

