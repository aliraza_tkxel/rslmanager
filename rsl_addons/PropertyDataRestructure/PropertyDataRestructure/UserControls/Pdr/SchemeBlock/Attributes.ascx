﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Attributes.ascx.cs" Inherits="PropertyDataRestructure.UserControls.Pdr.SchemeBlock.Attributes" %>
<%@ Register TagPrefix="attr" TagName="AttributeDetail" Src="~/UserControls/Pdr/SchemeBlock/AttributeDetail.ascx"  %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="500px" />

<asp:UpdatePanel ID="updPnlAttributes" runat="server">
    <ContentTemplate>
    
        <div style="float: left; width: 25%; border: 1px solid #c5c5c5; height: 603px; overflow: scroll;">
            <asp:Panel ID="pnlTreeView" runat="server">
                <asp:TreeView runat="server" ID="trVwAttributes" ShowLines="true" ExpandDepth="1"
                    ForeColor="Black" SelectedNodeStyle-BackColor="#6dcff7" PopulateNodesFromClient = "false"
                    PathSeparator="&gt;" onselectednodechanged="trVwAttributes_SelectedNodeChanged" 
                    ontreenodepopulate="trVwAttributes_TreeNodePopulate" EnableClientScript="false" >
                    <Nodes>
                        <asp:TreeNode PopulateOnDemand="true" Text="Menu" ></asp:TreeNode>
                    </Nodes>
                </asp:TreeView>
            </asp:Panel>
        </div>
        <div style="float: left; width: 74%; border: 1px solid #c5c5c5; min-height: 603px;">
            <div style="margin-top: 10px;">
                <asp:Label runat="server" Text="" ID="lblBreadCrump" Style="margin-left: 15px;"></asp:Label></div>
            
            <attr:AttributeDetail runat="server" ID="ucAttributeDetail" Visible="false"    />
            <asp:Button ID="btnHidden" runat="server" Text="" Style="display: none;" />
        </div>
       
    </ContentTemplate>
</asp:UpdatePanel>