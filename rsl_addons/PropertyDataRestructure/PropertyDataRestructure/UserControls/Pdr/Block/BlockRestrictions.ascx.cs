﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PropertyDataRestructure.Interface;
using PropertyDataRestructure.Base;
using PDR_Utilities.Constants;
using PDR_DataAccess.Block;
using PDR_BusinessLogic.Block;
using System.Data;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_BusinessObject.Restriction;
using PDR_Utilities.Managers;

namespace PropertyDataRestructure.UserControls.Pdr.Block
{
    public partial class BlockRestrictions : UserControlBase, IAddEditPage
    {
        #region "Events"

        #region "Page Load"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                populateDropDowns();
                loadData();
            }
            uiMessage.hideMessage();
        }
        #endregion

        #region "btn Save Restriction Click"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSaveRestriction_Click(object sender, EventArgs e)
        {
            try
            {
                Page.Validate("saveRestriction");
                if (Page.IsValid)
                {
                    saveData();
                }
                
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #endregion

        #region "Functions"

        #region "save Data"
        /// <summary>
        /// 
        /// </summary>
        public void saveData()
        {
            RestrictionBO objRestrictionBO = new RestrictionBO();
            objRestrictionBO.RestrictionId = string.IsNullOrEmpty(btnSaveRestriction.CommandArgument) ? 0 : Convert.ToInt32(btnSaveRestriction.CommandArgument);
            objRestrictionBO.PermittedPlanning = txtPermittedPlanning.Text;
            objRestrictionBO.RelevantPlanning = txtRelevantPlanning.Text;
            objRestrictionBO.RelevantTitle = txtRelevantTitle.Text;
            objRestrictionBO.RestrictionComments = txtDetailComments.Text;
            objRestrictionBO.AccessIssues = txtAccessIssues.Text;
            objRestrictionBO.MediaIssues = txtMediaIssues.Text;
            objRestrictionBO.ThirdPartyAgreement = ddlThirdParty.SelectedValue.Equals(ApplicationConstants.DropDownDefaultValues) ? null : ddlThirdParty.SelectedValue;
            objRestrictionBO.SpFundingArrangements = txtSpFunding.Text;
            objRestrictionBO.IsRegistered = ddlIsRegistered.SelectedValue.Equals(ApplicationConstants.DropDownDefaultValues) ? (int?) null : Convert.ToInt32(ddlIsRegistered.SelectedValue);
            objRestrictionBO.ManagementDetail = txtManagementDetail.Text;
            objRestrictionBO.NonBhaInsuranceDetail = txtNonBhaDetails.Text;
            objRestrictionBO.UpdatedBy = objSession.EmployeeId;

            if (validateData(objRestrictionBO))
            {
                objRestrictionBO.BlockId = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);

                BlockBL objBlockBL = new BlockBL(new BlockRepo());
                bool result = objBlockBL.SaveRestriction(ref objRestrictionBO);

                if (result)
                {
                    btnSaveRestriction.CommandArgument = objRestrictionBO.RestrictionId.ToString();

                    uiMessage.messageText = UserMessageConstants.RestrictionSavedSuccessfully;
                    uiMessage.showInformationMessage(uiMessage.messageText);
                }
                else
                {
                    uiMessage.messageText = UserMessageConstants.RestrictionNotSaved;
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }

            }

        }
        #endregion

        #region "validate Data"
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool validateData(RestrictionBO objRest)
        {
            bool isValid = true;
            string message = string.Empty;
            int id;

            if (!int.TryParse(Request.QueryString[ApplicationConstants.Id], out id))
            {
                isValid = false;
                message = UserMessageConstants.InvalidBlockId;
            }
            else if (string.IsNullOrEmpty(objRest.PermittedPlanning)
                && string.IsNullOrEmpty(objRest.RelevantPlanning)
                && string.IsNullOrEmpty(objRest.RelevantTitle)
                && string.IsNullOrEmpty(objRest.RestrictionComments)
                && string.IsNullOrEmpty(objRest.AccessIssues)
                && string.IsNullOrEmpty(objRest.MediaIssues)
                && string.IsNullOrEmpty(objRest.ThirdPartyAgreement)
                && string.IsNullOrEmpty(objRest.SpFundingArrangements)
                && objRest.IsRegistered == null
                && string.IsNullOrEmpty(objRest.ManagementDetail)
                && string.IsNullOrEmpty(objRest.NonBhaInsuranceDetail)
                )
            {
                isValid = false;
                message = UserMessageConstants.FieldRestrictionCheck;
            }

            if (isValid == false)
            {
                uiMessage.showErrorMessage(message);
            }

            return isValid;
        }
        #endregion

        #region "load Data"
        /// <summary>
        /// 
        /// </summary>
        public void loadData()
        {
            resetControls();

            if (string.IsNullOrEmpty(Request.QueryString[ApplicationConstants.Id]))
            {
                uiMessage.showErrorMessage(UserMessageConstants.InvalidBlockId);
            }else
            {
                int blockId = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);
                BlockBL objBlockBL = new BlockBL(new BlockRepo());
                DataSet blockDataSet = new DataSet();
                objBlockBL.getBlockRestrictions(ref blockDataSet, blockId);

                var dtRestriction = blockDataSet.Tables[0];

                if (dtRestriction.Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(dtRestriction.Rows[0]["ThirdPartyAgreement"].ToString()))
                    {
                        ddlThirdParty.SelectedValue = dtRestriction.Rows[0]["ThirdPartyAgreement"].ToString();
                    }

                    if (!string.IsNullOrEmpty(dtRestriction.Rows[0]["IsRegistered"].ToString()))
                    {
                        ddlIsRegistered.SelectedValue = dtRestriction.Rows[0]["IsRegistered"].ToString();
                    }

                    txtPermittedPlanning.Text = dtRestriction.Rows[0]["PermittedPlanning"].ToString();
                    txtRelevantPlanning.Text = dtRestriction.Rows[0]["RelevantPlanning"].ToString();
                    txtRelevantTitle.Text = dtRestriction.Rows[0]["RelevantTitle"].ToString();
                    txtDetailComments.Text = dtRestriction.Rows[0]["RestrictionComments"].ToString();
                    txtAccessIssues.Text = dtRestriction.Rows[0]["AccessIssues"].ToString();
                    txtMediaIssues.Text = dtRestriction.Rows[0]["MediaIssues"].ToString();
                    txtSpFunding.Text = dtRestriction.Rows[0]["SpFundingArrangements"].ToString();
                    txtManagementDetail.Text = dtRestriction.Rows[0]["ManagementDetail"].ToString();
                    txtNonBhaDetails.Text = dtRestriction.Rows[0]["NonBhaInsuranceDetail"].ToString();
                    btnSaveRestriction.CommandArgument = dtRestriction.Rows[0]["RestrictionId"].ToString();
                }
 
            }

        }
        #endregion

        #region "populate DropDowns"
        /// <summary>
        /// 
        /// </summary>
        public void populateDropDowns()
        {

            ddlThirdParty.Items.Clear();
            ddlIsRegistered.Items.Clear();

            List<ListItem> lstItems = new List<ListItem>();
            lstItems.Add(new ListItem("yes", "yes"));
            lstItems.Add(new ListItem("no", "no"));
            ddlThirdParty.DataSource = lstItems;
            ddlThirdParty.DataBind();

            ListItem defaultItem = new ListItem("Please select", ApplicationConstants.DropDownDefaultValues);
            ddlThirdParty.Items.Insert(0, defaultItem);

            BlockBL objBlockBL = new BlockBL(new BlockRepo());
            DataSet regDataSet = new DataSet();
            objBlockBL.getLandRegistrationOptions(ref regDataSet);
            ddlIsRegistered.DataSource = regDataSet;
            ddlIsRegistered.DataValueField = "RegistryStatusId";
            ddlIsRegistered.DataTextField = "Description";
            ddlIsRegistered.DataBind();
            ddlIsRegistered.Items.Insert(0, defaultItem);

        }

        public void populateDropDown(DropDownList ddl)
        {
            throw new NotImplementedException();
        }

        public bool validateData()
        {
            throw new NotImplementedException();
        }

        public void setTextboxLength()
        {
            txtPermittedPlanning.Attributes.Add("maxlength", txtPermittedPlanning.MaxLength.ToString());
            txtRelevantPlanning.Attributes.Add("maxlength", txtRelevantPlanning.MaxLength.ToString());
            txtRelevantTitle.Attributes.Add("maxlength", txtRelevantTitle.MaxLength.ToString());
            txtDetailComments.Attributes.Add("maxlength", txtDetailComments.MaxLength.ToString());
            txtAccessIssues.Attributes.Add("maxlength", txtAccessIssues.MaxLength.ToString());
            txtMediaIssues.Attributes.Add("maxlength", txtMediaIssues.MaxLength.ToString());
            txtSpFunding.Attributes.Add("maxlength", txtSpFunding.MaxLength.ToString());
            txtManagementDetail.Attributes.Add("maxlength", txtManagementDetail.MaxLength.ToString());
            txtNonBhaDetails.Attributes.Add("maxlength", txtNonBhaDetails.MaxLength.ToString());
        }

        public void resetControls()
        {
            populateDropDowns();
            txtPermittedPlanning.Text = string.Empty;
            txtRelevantPlanning.Text = string.Empty;
            txtRelevantTitle.Text = string.Empty;
            txtDetailComments.Text = string.Empty;
            txtAccessIssues.Text = string.Empty;
            txtMediaIssues.Text = string.Empty;
            txtSpFunding.Text = string.Empty;
            txtManagementDetail.Text = string.Empty;
            txtNonBhaDetails.Text = string.Empty;
            btnSaveRestriction.CommandArgument = string.Empty;
        }
        #endregion

        #endregion
    }
}