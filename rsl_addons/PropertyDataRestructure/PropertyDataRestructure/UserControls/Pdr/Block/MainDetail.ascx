﻿<%@ Control Language="C#"  AutoEventWireup="true" CodeBehind="MainDetail.ascx.cs"
    Inherits="PropertyDataRestructure.UserControls.Pdr.Block.MainDetail" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>

<asp:UpdatePanel runat="server" ID="updPnlMainDetail" >
    <ContentTemplate>
        <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="500px" />
        <div class="table-container block-main">
        <div style="border: 1px solid #c5c5c5;padding:10px;width:47%">
            <table id="tblMainDetails">
                <tr>
                    <td>
                        Select a Template:
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlBlockTemplate" Height="23px" Width="173px"
                            OnSelectedIndexChanged="ddlBlockTemplate_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        Block Reference:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtBlockReference" MaxLength="200"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Development Name:
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlDevelopment" Height="23px" Width="173px"
                            OnSelectedIndexChanged="ddlDevelopment_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        Phase:
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlPhase" Height="23px" Width="173px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        Block Name:<span class="Required">*</span>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtBlockName" MaxLength="100"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtBlockName"
                            ErrorMessage="Required" CssClass="Required" ValidationGroup="add"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        Address1:<span class="Required">*</span>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtAddress1" MaxLength="100"></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtAddress1"
                            ErrorMessage="Required" CssClass="Required" ValidationGroup="add"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        Address2:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtAddress2" MaxLength="100"></asp:TextBox>
                       
                    </td>
                </tr>
                <tr>
                    <td>
                        Address3:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtAddress3" MaxLength="100"></asp:TextBox>
                       
                    </td>
                </tr>
                <tr>
                    <td>
                        Town/City:<span class="Required">*</span>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtTownCity" MaxLength="100"></asp:TextBox>
                         <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtTownCity"
                            ErrorMessage="Required" CssClass="Required" ValidationGroup="add"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        County:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtCounty" MaxLength="100"></asp:TextBox>
                        
                    </td>
                </tr>
                <tr>
                    <td>
                        PostCode:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtPostCode" MaxLength="10"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtPostCode"
                            EnableClientScript="True" ErrorMessage="<br/>Special characters are not allowed" CssClass="Required"
                            Display="Dynamic" ValidationExpression="^[A-Za-z0-9-\/ ]+$" ValidationGroup="save" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Confirmation of Ownership:
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlOwnership" Height="23px" Width="173px">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        Build Date:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtbuildDate" Style="width: 116px !important;"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender ID="clndrbuildDate" runat="server" TargetControlID="txtbuildDate"
                            PopupButtonID="imgCalDate" PopupPosition="Right" TodaysDateFormat="dd/MM/yyyy"
                            Format="dd/MM/yyyy">
                        </ajaxToolkit:CalendarExtender>
                        <asp:ImageButton ImageUrl="~/Images/calendar.png" runat="server" ID="imgCalDate"
                            Style="vertical-align: bottom; border: none;" />
                            <br />


                              <asp:CompareValidator ID="CompareValidator7" runat="server" Style="float: left;"
                                ControlToValidate="txtbuildDate" ErrorMessage="Enter a valid date"
                                Operator="DataTypeCheck" Type="Date" Display="Dynamic" CssClass="Required" ValidationGroup="save"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        Number of Properties:
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtNoOfProperties" Style="width: 116px !important" onkeyup="NumberOnly(this);" ></asp:TextBox>
                        
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Button runat="server" ID="btnSaveBlock" Text="Save Block" Style="float: right;
                            margin-right: 20px;" OnClick="btnSaveBlock_Click" ValidationGroup="save"  />
                        <asp:Button runat="server" ID="btnCancel" Text="Cancel" Style="float: right; margin-right: 20px;"
                            OnClick="btnCancel_Click" />
                    </td>
                </tr>
            </table>
        </div>
        </div>

        <%-- MESSAGE POPUP --%>
            <asp:Button ID="btnHidden3" runat="server" Text="" Style="display: none;" />
            <ajaxToolkit:ModalPopupExtender ID="mdlPopupMessage" runat="server" TargetControlID="btnHidden3"
                PopupControlID="pnlUserMessage" Enabled="true" DropShadow="true" BackgroundCssClass="modalBackground"
                CancelControlID="btnClose">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnlUserMessage" CssClass="left" runat="server" BackColor="White" Width="350px"
                Style="border: 1px solid black; font-weight: normal; font-size: 13px;">
                <br />
                <div>
                    <table style="width: 100%; margin-top: -16px; margin-bottom: 20px;">
                        <tr style="background-color: Gray; height: 40px;">
                            <td align="center">
                                <asp:Label ID="lblMessageStatus" Font-Bold="true" ForeColor="White" runat="server" Text="Block Template" ></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                &nbsp
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Label ID="lblUserMessage" runat="server" Text ="Template selection would override the already saved values against this block. Press 'OK' to continue."></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                &nbsp
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                              <asp:Button Text="Ok" ID="btnOk" runat="server" Width="70px"  CssClass="btn" 
                                    style=" margin:2px; " onclick="btnOk_Click"/>
                                <asp:Button ID="btnClose" CssClass="btn" Width="70px" runat="server" Text="Cancel" style=" margin:2px; " />
                              
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
