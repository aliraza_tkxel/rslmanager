﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PropertyDataRestructure.Base;
using PropertyDataRestructure.Interface;
using PDR_BusinessLogic.Block;
using System.Data;
using PDR_BusinessLogic.Development;
using PDR_BusinessObject.Block;
using PDR_Utilities.Constants;
using PDR_DataAccess.Development;
using PDR_DataAccess.Block;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace PropertyDataRestructure.UserControls.Pdr.Block
{
    public partial class MainDetail : UserControlBase, IAddEditPage
    {
        
        #region "Events"
        #region "Page Load"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                populateDropDowns();
                loadData();
               	
            }
        }
        #endregion
        
        #region "ddlDevelopment SelectedIndexChanged"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlDevelopment_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int developmentId = Convert.ToInt32(ddlDevelopment.SelectedValue.ToString());
                DevelopmentBL objDevelopmentBL = new DevelopmentBL(new DevelopmentRepo());
                DataSet phaseDataSet = new DataSet();

                objDevelopmentBL.getDevelopmentPhase(ref phaseDataSet, developmentId);
                if (phaseDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlPhase.DataSource = phaseDataSet;
                    ddlPhase.DataValueField = "PHASEID";
                    ddlPhase.DataTextField = "PhaseName";
                    ddlPhase.DataBind();
                    ListItem item = new ListItem("Select Phase", "0");
                    ddlPhase.Items.Insert(0, item);
                }
                else
                {
                    ddlPhase.Items.Clear();
                    ListItem item = new ListItem("Select Phase", "0");
                    ddlPhase.Items.Insert(0, item);
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region "ddlBlockTemplate SelectedIndexChanged"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlBlockTemplate_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
              
                int templateId = Convert.ToInt32(ddlBlockTemplate.SelectedValue);
                if (templateId != 0)
                {
                    if (Request.QueryString[ApplicationConstants.Id] != null)
                    {
                        mdlPopupMessage.Show(); 
                    }
                    else
                    {
                        populateControls(templateId);  
                    }

                    
                }
                else
                {
                    resetControls();
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        #endregion

        #region "btnCancel Click"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Views/Pdr/Block/BlockList.aspx");
        }
        #endregion

        #region "btnSave Block Click"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSaveBlock_Click(object sender, EventArgs e)
        {
            try
            {
                saveData();

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #endregion

        #region "Functions"

        #region "save Data"
        /// <summary>
        /// 
        /// </summary>
        public void saveData()
        {
            try
            {
                BlockBO objSaveBlockBO = new BlockBO();
                BlockBL objBlockBL = new BlockBL(new BlockRepo());
                objSaveBlockBO.BlockReference = txtBlockReference.Text;
                if (Request.QueryString[ApplicationConstants.Id] != null)
                {
                    objSaveBlockBO.ExistingBlockId = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]); 
                }
                if (!string.IsNullOrEmpty(txtBlockName.Text) && !string.IsNullOrWhiteSpace(txtBlockName.Text))
                {
                    objSaveBlockBO.BlockName = txtBlockName.Text;
                }
                else
                {
                    uiMessage.isError = true;
                    uiMessage.messageText = UserMessageConstants.BlockNameNotFound;
                    return;
                }
                objSaveBlockBO.DevelopmentId = Convert.ToInt32(ddlDevelopment.SelectedValue);

                if (ddlPhase.SelectedValue != "")
                {
                    objSaveBlockBO.PhaseId = Convert.ToInt32(ddlPhase.SelectedValue);
                }
                if (!string.IsNullOrEmpty(txtAddress1.Text) && !string.IsNullOrWhiteSpace(txtAddress1.Text))
                {
                    objSaveBlockBO.Address1 = txtAddress1.Text;
                }
                else
                {
                    uiMessage.isError = true;
                    uiMessage.messageText = UserMessageConstants.BlockAddressNotFound;
                    return;

                }
                objSaveBlockBO.Address2 = txtAddress2.Text;
                objSaveBlockBO.Address3 = txtAddress3.Text;
                objSaveBlockBO.County = txtCounty.Text;
                objSaveBlockBO.PostCode = txtPostCode.Text;

                if (!string.IsNullOrEmpty(txtTownCity.Text) && !string.IsNullOrWhiteSpace(txtTownCity.Text))
                {
                    objSaveBlockBO.TownCity = txtTownCity.Text;
                }
                else
                {
                    uiMessage.isError = true;
                    uiMessage.messageText = UserMessageConstants.BlockTownCityNotFound;
                    return;
                }
                objSaveBlockBO.OwnerShip = Convert.ToInt32(ddlOwnership.SelectedValue);
                if (!string.IsNullOrEmpty(txtbuildDate.Text) && !string.IsNullOrWhiteSpace(txtbuildDate.Text))
                {
                    objSaveBlockBO.BuildDate = Convert.ToDateTime(txtbuildDate.Text);
                }
                if (!string.IsNullOrEmpty(txtNoOfProperties.Text) && !string.IsNullOrWhiteSpace(txtNoOfProperties.Text))
                {
                    objSaveBlockBO.NoOfProperties = Convert.ToInt32(txtNoOfProperties.Text);
                }
                string result = objBlockBL.SaveBlock(objSaveBlockBO);
                if (result == "success")
                {                  
                    if (objSaveBlockBO.ExistingBlockId > 0)
                    {
                        uiMessage.messageText = UserMessageConstants.BlockSavedSuccessfully;
                        uiMessage.showInformationMessage(uiMessage.messageText);
                    }
                    else
                    {
                        Response.Redirect(PathConstants.BlockMain + "?" + ApplicationConstants.Id + "=" + objSaveBlockBO.BlockId + "&" + ApplicationConstants.RequestType + "=" + ApplicationConstants.Block + "&" + ApplicationConstants.Source + "=new");
                    }
                }
                else if (result == "failed")
                {

                    uiMessage.messageText = UserMessageConstants.BlockNotSaved;
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
                else
                {

                    uiMessage.messageText = UserMessageConstants.DuplicateBlockError;
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        #endregion

        #region "validate Data"
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool validateData()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region "reset Controls"
        /// <summary>
        /// 
        /// </summary>
        public void resetControls()
        {
            ddlDevelopment.SelectedIndex = 0;
            ddlPhase.SelectedIndex = 0;
            txtBlockReference.Text = string.Empty;
            txtBlockName.Text = string.Empty;
            txtAddress1.Text = string.Empty;
            txtAddress2.Text = string.Empty;
            txtAddress3.Text = string.Empty;
            txtCounty.Text = string.Empty;
            txtTownCity.Text = string.Empty;
            txtPostCode.Text = string.Empty;
            txtNoOfProperties.Text = string.Empty;
            txtbuildDate.Text = string.Empty;
            ddlOwnership.SelectedIndex = 0;
        }
        #endregion

        #region "load Data"
        /// <summary>
        /// 
        /// </summary>
        public void loadData()
        {
            try
            {
                int blockId = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);
                BlockBL objBlockBL = new BlockBL(new BlockRepo());
                DataSet blockDataSet = new DataSet();
                int developmentId = 0;
                objBlockBL.getBlockData(ref blockDataSet, blockId);
                if (blockDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlDevelopment.SelectedValue = blockDataSet.Tables[0].Rows[0]["DEVELOPMENTID"].ToString();
                    developmentId = Convert.ToInt32(ddlDevelopment.SelectedValue);
                    populateDropDown(developmentId);
                    ddlPhase.SelectedValue = blockDataSet.Tables[0].Rows[0]["PHASEID"].ToString();
                    txtBlockReference.Text = blockDataSet.Tables[0].Rows[0]["BlockReference"].ToString();
                    txtBlockName.Text = blockDataSet.Tables[0].Rows[0]["BlockName"].ToString();
                    txtAddress1.Text = blockDataSet.Tables[0].Rows[0]["ADDRESS1"].ToString();
                    txtAddress2.Text = blockDataSet.Tables[0].Rows[0]["ADDRESS2"].ToString();
                    txtAddress3.Text = blockDataSet.Tables[0].Rows[0]["ADDRESS3"].ToString();
                    txtCounty.Text = blockDataSet.Tables[0].Rows[0]["COUNTY"].ToString();
                    txtTownCity.Text = blockDataSet.Tables[0].Rows[0]["TOWNCITY"].ToString();
                    txtPostCode.Text = blockDataSet.Tables[0].Rows[0]["POSTCODE"].ToString();
                    txtNoOfProperties.Text = blockDataSet.Tables[0].Rows[0]["NoOfProperties"].ToString();
                    if (!string.IsNullOrEmpty(blockDataSet.Tables[0].Rows[0]["BuildDate"].ToString()))
                    {
                        txtbuildDate.Text = Convert.ToDateTime(blockDataSet.Tables[0].Rows[0]["BuildDate"]).ToString("MM/dd/yyyy");
                    }
                    else
                    {
                        txtbuildDate.Text = string.Empty;
                    }
                    ddlOwnership.SelectedValue = blockDataSet.Tables[0].Rows[0]["ConfirmOwnership"].ToString();
                   
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region "populate DropDowns"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ddl"></param>
        /// 
        public void populateDropDown(DropDownList ddl)
        {
            throw new NotImplementedException();
        }


        public void populateDropDown(int developmentId)
        {
            try
            {

                DevelopmentBL objDevelopmentBL = new DevelopmentBL(new DevelopmentRepo());
                DataSet phaseDataSet = new DataSet();

                objDevelopmentBL.getDevelopmentPhase(ref phaseDataSet, developmentId);
                if (phaseDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlPhase.DataSource = phaseDataSet;
                    ddlPhase.DataValueField = "PHASEID";
                    ddlPhase.DataTextField = "PhaseName";
                    ddlPhase.DataBind();
                    ListItem item = new ListItem("Select Phase", "0");
                    ddlPhase.Items.Insert(0, item);
                }
                else
                {
                    ddlPhase.Items.Clear();
                    ListItem item = new ListItem("Select Phase", "0");
                    ddlPhase.Items.Insert(0, item);
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region "populate DropDowns"
        /// <summary>
        /// 
        /// </summary>
        public void populateDropDowns()
        {
            try
            {
                BlockBL objBlockBL = new BlockBL(new BlockRepo());

                DataSet TemplateDataSet = new DataSet();
                objBlockBL.getBlockTemplate(ref TemplateDataSet);
                if (TemplateDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlBlockTemplate.DataSource = TemplateDataSet;
                    ddlBlockTemplate.DataTextField = "BLOCKNAME";
                    ddlBlockTemplate.DataValueField = "BLOCKID";
                    ddlBlockTemplate.DataBind();
                    ListItem item = new ListItem("Select Template", "0");
                    ddlBlockTemplate.Items.Insert(0, item);

                }
                else
                {
                    ListItem item = new ListItem("Select Template", "0");
                    ddlBlockTemplate.Items.Insert(0, item);
                }

                DataSet DevelopmentDataSet = new DataSet();
                objBlockBL.getDevelopments(ref DevelopmentDataSet);
                if (DevelopmentDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlDevelopment.DataSource = DevelopmentDataSet;
                    ddlDevelopment.DataTextField = "DevelopmentName";
                    ddlDevelopment.DataValueField = "DevelopmentId";
                    ddlDevelopment.DataBind();
                    ListItem item = new ListItem("Select Development", "0");
                    ddlDevelopment.Items.Insert(0, item);
                    ListItem item1 = new ListItem("Select Phase", "0");
                    ddlPhase.Items.Insert(0, item1);

                }
                else
                {
                    ListItem item = new ListItem("Select Development", "0");
                    ddlDevelopment.Items.Insert(0, item);
                    ListItem item1 = new ListItem("Select Phase", "0");
                    ddlPhase.Items.Insert(0, item1);
                }
                DataSet ownershipDataSet = new DataSet();
                objBlockBL.getOwnershipData(ref ownershipDataSet);
                if (ownershipDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlOwnership.DataSource = ownershipDataSet;
                    ddlOwnership.DataTextField = "DESCRIPTION";
                    ddlOwnership.DataValueField = "OWNERSHIPID";
                    ddlOwnership.DataBind();
                    ListItem item = new ListItem("Select OwnerShip", "0");
                    ddlOwnership.Items.Insert(0, item);
                }
                else
                {
                    ListItem item = new ListItem("Select OwnerShip", "0");
                    ddlOwnership.Items.Insert(0, item);
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        protected void btnOk_Click(object sender, EventArgs e)
        {
            int templateId = Convert.ToInt32(ddlBlockTemplate.SelectedValue);
            populateControls(templateId); 
        }

        #region "populate DropDowns"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ddl"></param>
        /// 
        public void populateControls(int templateId)
        {
            BlockBL objBlockBL = new BlockBL(new BlockRepo());
            DataSet BlockTemplateDataSet = new DataSet();
            int developmentId = 0;
            objBlockBL.getBlockTemplateData(ref BlockTemplateDataSet, templateId);
            if (BlockTemplateDataSet.Tables[0].Rows.Count > 0)
            {
                developmentId = (BlockTemplateDataSet.Tables[0].Rows[0]["DEVELOPMENTID"] != DBNull.Value ? Convert.ToInt32(BlockTemplateDataSet.Tables[0].Rows[0]["DEVELOPMENTID"]) : 0);

                if (developmentId > 0)
                {
                    ddlDevelopment.SelectedValue = developmentId.ToString();
                    populateDropDown(developmentId);
                    string phaseId = BlockTemplateDataSet.Tables[0].Rows[0]["PHASEID"].ToString();
                    if (ddlPhase.Items.FindByValue(phaseId) != null)
                    {
                        ddlPhase.SelectedValue = BlockTemplateDataSet.Tables[0].Rows[0]["PHASEID"].ToString();
                    }
                }
                txtBlockReference.Text = (BlockTemplateDataSet.Tables[0].Rows[0]["BlockReference"] != DBNull.Value ? BlockTemplateDataSet.Tables[0].Rows[0]["BlockReference"].ToString() : string.Empty);
                txtBlockName.Text = (BlockTemplateDataSet.Tables[0].Rows[0]["BlockName"] != DBNull.Value ? BlockTemplateDataSet.Tables[0].Rows[0]["BlockName"].ToString() : string.Empty);
                txtAddress1.Text = (BlockTemplateDataSet.Tables[0].Rows[0]["ADDRESS1"] != DBNull.Value ? BlockTemplateDataSet.Tables[0].Rows[0]["ADDRESS1"].ToString() : string.Empty);
                txtAddress2.Text = (BlockTemplateDataSet.Tables[0].Rows[0]["ADDRESS2"] != DBNull.Value ? BlockTemplateDataSet.Tables[0].Rows[0]["ADDRESS2"].ToString() : string.Empty);
                txtAddress3.Text = (BlockTemplateDataSet.Tables[0].Rows[0]["ADDRESS3"] != DBNull.Value ? BlockTemplateDataSet.Tables[0].Rows[0]["ADDRESS3"].ToString() : string.Empty);
                txtCounty.Text = (BlockTemplateDataSet.Tables[0].Rows[0]["COUNTY"] != DBNull.Value ? BlockTemplateDataSet.Tables[0].Rows[0]["COUNTY"].ToString() : string.Empty);
                txtTownCity.Text = (BlockTemplateDataSet.Tables[0].Rows[0]["TOWNCITY"] != DBNull.Value ? BlockTemplateDataSet.Tables[0].Rows[0]["TOWNCITY"].ToString() : string.Empty);
                txtPostCode.Text = (BlockTemplateDataSet.Tables[0].Rows[0]["POSTCODE"] != DBNull.Value ? BlockTemplateDataSet.Tables[0].Rows[0]["POSTCODE"].ToString() : string.Empty);
                txtNoOfProperties.Text = (BlockTemplateDataSet.Tables[0].Rows[0]["NoOfProperties"] != DBNull.Value ? BlockTemplateDataSet.Tables[0].Rows[0]["NoOfProperties"].ToString() : string.Empty);
                txtbuildDate.Text = (BlockTemplateDataSet.Tables[0].Rows[0]["BuildDate"] != DBNull.Value ? Convert.ToDateTime(BlockTemplateDataSet.Tables[0].Rows[0]["BuildDate"]).ToShortDateString() : string.Empty);
                ddlOwnership.SelectedItem.Text = (BlockTemplateDataSet.Tables[0].Rows[0]["ConfirmOwnership"] != DBNull.Value ? BlockTemplateDataSet.Tables[0].Rows[0]["ConfirmOwnership"].ToString() : string.Empty);
            }
        }


       
        #endregion

        #endregion
    }
}