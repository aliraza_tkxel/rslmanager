﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PropertyDataRestructure.Base;
using PropertyDataRestructure.Interface;
using System.Data;
using PDR_BusinessLogic.Scheme;
using PDR_DataAccess.Scheme;
using PDR_Utilities.Managers;
using PDR_Utilities.Constants;
using PDR_BusinessLogic.Block;
using PDR_DataAccess.Block;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_BusinessLogic.AssignToContractor;
using PDR_BusinessObject.AssignToContractor;
using PDR_DataAccess.AssignToContractor;

namespace PropertyDataRestructure.UserControls.Pdr.Scheme
{
    public partial class DashBoard : UserControlBase, IListingPage, IAddEditPage
    {
        int id = 0;
        string requestType = string.Empty;
       
        #region Page load event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                ucRaisePO.CloseButtonClicked += new EventHandler(AssignToContractorBtnClose_Click);
                if (!IsPostBack)
                {
                    //populateData();
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion
        #region Report a fault
        /// <summary>
        /// Report a Fault
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnReportFault_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet schemeDetailDataSet = new DataSet();
                schemeDetailDataSet = objSession.SchemeDetail;
                if (schemeDetailDataSet.Tables[0].Rows.Count > 0)
                {
                    lblSchemeName.Text = schemeDetailDataSet.Tables[0].Rows[0]["SCHEMENAME"].ToString();
                    populateDropDowns();
                }
                updPopupReportFault.Update();
                mdlpopupScheduler.Show();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region btn Next click Event
        /// <summary>
        /// btn Next Click Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNext_Click(object sender, EventArgs e)
        {
            try
            {
                int schemeId = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);
                int blockId = Convert.ToInt32(ddlBlock.SelectedValue);
                
                string url = PathConstants.BRSFaultLocator + "?sid=" + schemeId.ToString() + "&bid=" + blockId.ToString();
                string s = "window.open('" +  ResolveClientUrl(url) + "', 'popup_window', 'width=1024,height=700,left=100,top=100,resizable=yes');";
                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), Guid.NewGuid().ToString(), s, true);
            }

            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion
        #region IListingPage Implementation
        public void loadData()
        {
            throw new NotImplementedException();
        }

        public void searchData()
        {
            throw new NotImplementedException();
        }
        public void populateData()
        {
            int schemeId = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);
            blocksSummary.populateData(schemeId);
            schemePlanned.populateData(schemeId);
            schemeRepair.populateData(schemeId);
        }

        public void printData()
        {
            throw new NotImplementedException();
        }

        public void exportGridToExcel()
        {
            throw new NotImplementedException();
        }

        public void exportToPdf()
        {
            throw new NotImplementedException();
        }

        public void downloadData()
        {
            throw new NotImplementedException();
        }

        public void applyFilters()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Function
        #region get ASB Count
        public int getSchemeASBCount(int schemeId)
        {
            int count = 0;
            DataSet resultDataSet = new DataSet();
            SchemeBL objSchemeBl = new SchemeBL(new SchemeRepo());
            resultDataSet = objSchemeBl.getSchemeASBCount(schemeId);
            if (resultDataSet.Tables[0].Rows.Count > 0)
            {
                count = Convert.ToInt32(resultDataSet.Tables[0].Rows[0]["ASB"]);
            }
            return count;
        }
        #endregion


        #endregion
        #region IAddEditPage Interface Implementation
        public void saveData()
        {
            throw new NotImplementedException();
        }

        public bool validateData()
        {
            throw new NotImplementedException();
        }

        public void resetControls()
        {
            throw new NotImplementedException();
        }

        public void populateDropDown(DropDownList ddl)
        {
            throw new NotImplementedException();
        }

        public void populateDropDowns()
        {
            int schemeId = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);
            DataSet resultDataSet = new DataSet();
            BlockBL objBlockBl = new BlockBL(new BlockRepo());
            resultDataSet = objBlockBl.getBlockListBySchemeId(schemeId);

            if (resultDataSet.Tables[0].Rows.Count > 0)
            {
                ddlBlock.DataSource = resultDataSet;
                ddlBlock.DataTextField = "BLOCKNAME";
                ddlBlock.DataValueField = "BLOCKID";
                ddlBlock.DataBind();
                ddlBlock.Items.Insert(0, new ListItem("Please Select", "0"));
            }
        }
        #endregion
        protected void btnRaisePO_Click(object sender, EventArgs e)
        {
            try
            {
                int? schemeId = null, blockId = null;
                this.GetQueryStringParams();
                if (requestType == ApplicationConstants.Scheme)
                {
                    schemeId = id;                    
                }
                else if (requestType == ApplicationConstants.Block)
                {
                    blockId = id;
                    schemeId = GetSchemeByBlockId(id);
                }


                objSession.SchemeId =  ( schemeId == null ? 0 : Convert.ToInt32(schemeId));
                DataSet schemeDetailDataSet = new DataSet();
                schemeDetailDataSet = objSession.SchemeDetail;
                if (schemeDetailDataSet.Tables[0].Rows.Count > 0)
                {
                    objSession.SchemeName = schemeDetailDataSet.Tables[0].Rows[0]["SCHEMENAME"].ToString();
                }
                objSession.BlockId = ( blockId == null ? 0 : Convert.ToInt32(blockId));
                AssignToContractorBo objAssignToContractorBo = new AssignToContractorBo();
                objAssignToContractorBo.BlockId = ( blockId == null ? 0 : Convert.ToInt32(blockId));
                objAssignToContractorBo.SchemeId = (schemeId == null ? 0 : Convert.ToInt32(schemeId));
                objAssignToContractorBo.MsatType = "Scheme Block PO";
                objSession.AssignToContractorBo = objAssignToContractorBo;
               
                ucRaisePO.populateControl();

                mdlpopupRaisePOProvision.Show();
               
              
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        protected void AssignToContractorBtnClose_Click(object sender, EventArgs e)
        {
            try
            {
                mdlpopupRaisePOProvision.Hide();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #region "Helper Functions"
        private void GetQueryStringParams()
        {
            if ((Request.QueryString[ApplicationConstants.Id] != null))
                id = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);

            if ((Request.QueryString[ApplicationConstants.RequestType] != null))
                requestType = Request.QueryString[ApplicationConstants.RequestType];

        }

        private int GetSchemeByBlockId(int blockId)
        {
            int schemeId = 0;
            AssignToContractorBL objAssignToContractorBL = new AssignToContractorBL(new AssignToContractorRepo());
            DataSet schemeDS = new DataSet();
            schemeDS = objAssignToContractorBL.GetSchemeByBlockId(blockId);

            if (schemeDS.Tables[0].Rows.Count > 0)
            {
                schemeId = int.Parse(schemeDS.Tables[0].Rows[0]["SchemeId"].ToString());
            }
            return schemeId;
        }

        #endregion
    }
}