﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SchemePlanned.ascx.cs" Inherits="PropertyDataRestructure.UserControls.Pdr.Scheme.SchemePlanned" %>

<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<asp:Panel runat="server" ID="pnlPlannedMaintenance">
    <cc1:PagingGridView ID="grdPlannedMaintenance" runat="server" AllowPaging="false" AllowSorting="false"
        AutoGenerateColumns="False" BorderStyle="Solid" BorderWidth="0px" CellPadding="4"
        GridLines="None" OrderBy="" PageSize="30" Width="100%" PagerSettings-Visible="false" ShowHeaderWhenEmpty="true" EmptyDataText="No Record Found">
        <Columns>
        <asp:TemplateField HeaderText="Components:" >
                <ItemTemplate>
                    <asp:Label ID="lblComponent" runat="server" Text='<%# Bind("Component") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="30%" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Blocks:" >
                <ItemTemplate>
                    <asp:Label ID="lblBlockName" runat="server" Text='-'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="40%" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Date:"  >
                <ItemTemplate>
                    <asp:LinkButton ID="lnkBtnDate" runat="server" CausesValidation="False" CommandArgument='<%#Eval("APPOINTMENTID") %>'
                        Text='<%# Bind("AppointmentDate") %>' CommandName="0"></asp:LinkButton>
                </ItemTemplate>
                <ItemStyle Width="20%" />
            </asp:TemplateField>
            
        </Columns>
        <EmptyDataTemplate >
            No Records Found</EmptyDataTemplate>
            <AlternatingRowStyle BackColor="#E8E9EA" Wrap="True" />
               
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#ffffff" CssClass="table-head" Font-Bold="True" ForeColor="black"
            HorizontalAlign="Left" />
    </cc1:PagingGridView>
</asp:Panel>
