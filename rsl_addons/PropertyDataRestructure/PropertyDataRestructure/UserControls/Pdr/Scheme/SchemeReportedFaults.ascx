﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SchemeReportedFaults.ascx.cs"
    Inherits="PropertyDataRestructure.UserControls.Pdr.Scheme.SchemeReportedFaults" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<asp:Panel runat="server" ID="pnlReportedFault">
    <cc1:PagingGridView ID="grdReportedFault" runat="server" AllowPaging="false" AllowSorting="false"
        AutoGenerateColumns="False" BorderStyle="Solid" BorderWidth="0px" CellPadding="4"
        GridLines="None" OrderBy="" PageSize="30" Width="100%" PagerSettings-Visible="false" ShowHeaderWhenEmpty="true" EmptyDataText="No Record Found">
        <Columns>
            <asp:TemplateField HeaderText="Reported:">
                <ItemTemplate>
                    <asp:Label ID="lblReported" runat="server" Text='<%#Eval("ReportedDate") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                <ItemStyle Width="15%" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Block:">
                <ItemTemplate>
                    <asp:Label ID="lblBlockName" runat="server" Text='<%#Eval("BlockName") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                <ItemStyle Width="15%" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Fault Details:">
                <ItemTemplate>
                    <asp:Label ID="lblLocation" runat="server" Text='<%#Eval("Description") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                <ItemStyle Width="30%" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Status:">
                <ItemTemplate>
                    <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                </ItemTemplate>
                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                <ItemStyle Width="25%" />
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>
            No Records Found</EmptyDataTemplate>
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <AlternatingRowStyle BackColor="#E8E9EA" Wrap="True" />
                
        <HeaderStyle BackColor="#ffffff" CssClass="table-head" Font-Bold="True" ForeColor="black"
            HorizontalAlign="Left" />
    </cc1:PagingGridView>
</asp:Panel>
