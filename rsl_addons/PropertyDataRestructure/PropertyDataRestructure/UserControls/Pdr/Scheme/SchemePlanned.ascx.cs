﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PDR_BusinessLogic.Scheme;
using PDR_Utilities.Managers;
using PropertyDataRestructure.Interface;
using PropertyDataRestructure.Base;
using System.Data;
using PDR_DataAccess.Scheme;

namespace PropertyDataRestructure.UserControls.Pdr.Scheme
{
    public partial class SchemePlanned : UserControlBase, IListingPage
    {
        #region Properties
        SchemeBL objSchemeBl = new SchemeBL(new SchemeRepo());
        
        //PageSortBO objPageSortBo = new PageSortBO("DESC", "ADDRESS", 1, 10);       
        string message = string.Empty;
        #endregion

        #region Events
        #region Page Load Event
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        #endregion
        #endregion

        #region IListingPage Implementation
        public void loadData()
        {
            throw new NotImplementedException();
        }

        public void searchData()
        {
            throw new NotImplementedException();
        }
        public void populateData()
        {

        }
        public void populateData(int schemeId)
        {
           
            DataSet resultDataSet = new DataSet();
            resultDataSet = objSchemeBl.getSchemePlannedAppointment (schemeId);

            grdPlannedMaintenance.DataSource = resultDataSet;
            grdPlannedMaintenance.DataBind();
           
        }

        public void printData()
        {
            throw new NotImplementedException();
        }

        public void exportGridToExcel()
        {
            throw new NotImplementedException();
        }

        public void exportToPdf()
        {
            throw new NotImplementedException();
        }

        public void downloadData()
        {
            throw new NotImplementedException();
        }

        public void applyFilters()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}