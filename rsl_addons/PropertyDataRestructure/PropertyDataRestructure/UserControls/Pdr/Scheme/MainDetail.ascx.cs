﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PropertyDataRestructure.Base;
using PropertyDataRestructure.Interface;
using PDR_BusinessLogic.Scheme;
using PDR_DataAccess.Scheme;
using PDR_Utilities.Managers;
using System.Data;
using PDR_BusinessLogic.Block;
using PDR_DataAccess.Block;
using PDR_BusinessLogic.Development;
using PDR_DataAccess.Development;
using PDR_BusinessObject.Scheme;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_Utilities.Constants;
using PropertyDataRestructure.Views.Pdr.Scheme.Dashboard;
using System.Threading;

namespace PropertyDataRestructure.UserControls.Pdr.Scheme
{
    public partial class MainDetail : UserControlBase, IAddEditPage
    {
        #region Properties
        SchemeBL objSchemeBl = new SchemeBL(new SchemeRepo());
        BlockBL objBlockBL = new BlockBL(new BlockRepo());

        string message = string.Empty;
        #endregion

      

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    //populateDropDowns();
                    //loadData();
                    
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #region "save Data"
        /// <summary>
        /// 
        /// </summary>
        public void saveData()
        {
            try
            {
                SaveSchemeBO objSchemeBO = new SaveSchemeBO();
                if (Request.QueryString[ApplicationConstants.Id] != null)
                {
                    objSchemeBO.ExistingSchemeId = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);
                }
                if (string.IsNullOrEmpty(txtSchemeName.Text) && string.IsNullOrWhiteSpace(txtSchemeName.Text))
                {
                    uiMessage.messageText = UserMessageConstants.SchemeNameNotFound;
                    uiMessage.showErrorMessage(uiMessage.messageText);
                    return;
                }
                else
                {
                    objSchemeBO.SchemeName = txtSchemeName.Text;
                }
                if (string.IsNullOrEmpty(txtSchemeCode.Text) && string.IsNullOrWhiteSpace(txtSchemeCode.Text))
                {
                    uiMessage.messageText = UserMessageConstants.SchemeCodeNotFound;
                    uiMessage.showErrorMessage(uiMessage.messageText);
                    return;

                }
                else
                {
                    objSchemeBO.SchemeCode = txtSchemeCode.Text;
                }
                objSchemeBO.DevelopmentId = Convert.ToInt32(ddlDevelopment.SelectedValue);
                if (objSchemeBO.DevelopmentId == 0)
                {
                    uiMessage.messageText = UserMessageConstants.DevelopmentNotFound;
                    uiMessage.showErrorMessage(uiMessage.messageText);
                    return;
                }
                objSchemeBO.PhaseId = Convert.ToInt32(ddlPhase.SelectedValue);
                if (objSchemeBO.PhaseId == 0)
                {
                    uiMessage.messageText = UserMessageConstants.PhaseNotFound;
                    uiMessage.showErrorMessage(uiMessage.messageText);
                    return;
                }
                foreach (ListItem l in lstBlockList.Items)
                {
                    int blockId = Convert.ToInt32(l.Value);
                    objSchemeBO.Blocks.Add(blockId);
                }
                foreach (ListItem p in lstPropertiesList.Items)
                {
                    string propertyId = p.Value.ToString();
                    objSchemeBO.Properties.Add(propertyId);
                }
                string result = objSchemeBl.saveScheme(ref objSchemeBO);
                if (result == "success")
                {
                    uiMessage.messageText = UserMessageConstants.SaveSchemeSuccessfuly;
                    uiMessage.showInformationMessage(uiMessage.messageText);
                    objSession.SchemeId = Convert.ToInt32(objSchemeBO.SchemeId);
                    if (objSchemeBO.ExistingSchemeId <= 0)
                    {
                        Response.Redirect(PathConstants.SchemeDashBoard + "?" + ApplicationConstants.Id + "=" + objSession.SchemeId + "&" + ApplicationConstants.RequestType + "=" + ApplicationConstants.Scheme + "&" + ApplicationConstants.Source + "=new");
                    }
                    
                }
                else if (result == "Exist")
                {
                    uiMessage.messageText = UserMessageConstants.DuplicateSchemeError;
                    uiMessage.showErrorMessage(uiMessage.messageText);
                    objSession.SchemeId = 0;
                }
                else
                {
                    uiMessage.messageText = UserMessageConstants.SchemeNotSaved;
                    uiMessage.showErrorMessage(uiMessage.messageText);
                    objSession.SchemeId = 0;
                }
            }
            catch (ThreadAbortException ex)
            { 
            
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        #endregion

        #region "validate Data"
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool validateData()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region " rese tControls"
        /// <summary>
        /// 
        /// </summary>
        public void resetControls()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region "load Data"
        /// <summary>
        /// 
        /// </summary>
        public void loadData()
        {
            try
            {
                int schemeId = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);
                DataSet schemeDataSet = new DataSet();
                int developmentId = 0, phaseId = 0;
                objSchemeBl.getSchemeData(ref schemeDataSet, schemeId);
                if (schemeDataSet.Tables["scheme"].Rows.Count > 0)
                {
                    txtSchemeName.Text = schemeDataSet.Tables["scheme"].Rows[0]["SCHEMENAME"].ToString();
                    ddlDevelopment.SelectedValue = schemeDataSet.Tables["scheme"].Rows[0]["DEVELOPMENTID"].ToString();
                    developmentId = Convert.ToInt32(ddlDevelopment.SelectedValue);
                    populateDropDown(ddlPhase, developmentId);
                    if (ddlPhase.Items.FindByValue(schemeDataSet.Tables["scheme"].Rows[0]["PHASEID"].ToString()) != null)
                    {
                        ddlPhase.SelectedValue = schemeDataSet.Tables["scheme"].Rows[0]["PHASEID"].ToString();
                    }
                    phaseId = Convert.ToInt32(ddlPhase.SelectedValue);
                    populateDropDown(phaseId);
                    txtSchemeCode.Text = schemeDataSet.Tables["scheme"].Rows[0]["SCHEMECODE"].ToString();
                }

                if (schemeDataSet.Tables["blocks"].Rows.Count > 0)
                {
                    lstBlockList.DataSource = schemeDataSet.Tables["blocks"];
                    lstBlockList.DataTextField = "BLOCKNAME";
                    lstBlockList.DataValueField = "BLOCKID"; 
                    lstBlockList.DataBind();
                }

                if (schemeDataSet.Tables["properties"].Rows.Count > 0)
                {
                    lstPropertiesList.DataSource = schemeDataSet.Tables["properties"];
                    lstPropertiesList.DataTextField = "PropertyAddress";
                    lstPropertiesList.DataValueField = "PROPERTYID";
                    lstPropertiesList.DataBind();
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
            //btnAddBlocks.Enabled = false;
            //btnAddProperties.Enabled = false;
        }
        #endregion

        #region "populate DropDown"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ddl"></param>
        /// 
        public void populateDropDown(DropDownList ddlPhase)
        {
            throw new NotImplementedException();
        }


        public void populateDropDown(DropDownList ddlPhase, int developmentId)
        {
            DevelopmentBL objDevelopmentBL = new DevelopmentBL(new DevelopmentRepo());
            DataSet phaseDataSet = new DataSet();
            objDevelopmentBL.getDevelopmentPhase(ref phaseDataSet, developmentId);
            if (phaseDataSet.Tables[0].Rows.Count > 0)
            {
                ddlPhase.DataSource = phaseDataSet;
                ddlPhase.DataValueField = "PHASEID";
                ddlPhase.DataTextField = "PhaseName";
                ddlPhase.DataBind();
                ListItem item = new ListItem("Select Phase", "0");
                ddlPhase.Items.Insert(0, item);
                ListItem itemAll = new ListItem("All", "-1");
                ddlPhase.Items.Insert(1, itemAll);
            }
            else
            {
                ddlPhase.Items.Clear();
                ListItem item = new ListItem("Select Phase", "0");
                ddlPhase.Items.Insert(0, item);
            }
        }


        public void populateDropDown(int phaseId)
        {
            int developmentId = 0;
            DevelopmentBL objDevelopmentBL = new DevelopmentBL(new DevelopmentRepo());
            DataSet blockPropertiesDataSet = new DataSet();
            developmentId = Convert.ToInt32(ddlDevelopment.SelectedValue);
            ddlBlock.Items.Clear();
            ddlProperties.Items.Clear();
            if (phaseId != 0)
            {
                objSchemeBl.getBlocksPropertiesbyPhaseID(ref blockPropertiesDataSet, phaseId, developmentId);
                if (blockPropertiesDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlBlock.DataSource = blockPropertiesDataSet.Tables["blocks"];
                    ddlBlock.DataValueField = "BLOCKID";
                    ddlBlock.DataTextField = "BlockName";
                    ddlBlock.DataBind();
                }

                if (blockPropertiesDataSet.Tables[1].Rows.Count > 0)
                {
                    ddlProperties.DataSource = blockPropertiesDataSet.Tables["properties"];
                    ddlProperties.DataValueField = "PROPERTYID";
                    ddlProperties.DataTextField = "PropertyAddress";
                    ddlProperties.DataBind();
                
                }
            }
            ListItem item1 = new ListItem("Select Block", "0");
            ddlBlock.Items.Insert(0, item1);
            ListItem item2 = new ListItem("Select Property", "0");
            ddlProperties.Items.Insert(0, item2);
        }
        #endregion

        #region "populate DropDowns"
        /// <summary>
        /// 
        /// </summary>
        public void populateDropDowns()
        {
            DataSet developmentDataSet = new DataSet();
            objBlockBL.getDevelopments(ref developmentDataSet);
            if (developmentDataSet.Tables[0].Rows.Count > 0)
            {
                ddlDevelopment.DataSource = developmentDataSet;
                ddlDevelopment.DataTextField = "DevelopmentName";
                ddlDevelopment.DataValueField = "DevelopmentId";
                ddlDevelopment.DataBind();
                ListItem item = new ListItem("Select Development", "0");
                ddlDevelopment.Items.Insert(0, item);
            }
            else
            {
                ListItem item = new ListItem("Select Development", "0");
                ddlDevelopment.Items.Insert(0, item);
            }

            ddlPhase.Items.Clear();
            ddlBlock.Items.Clear();
            ddlProperties.Items.Clear();


            ListItem item1 = new ListItem("Select Phase", "0");
            ddlPhase.Items.Insert(0, item1);

            ListItem item2 = new ListItem("Select Block", "0");
            ddlBlock.Items.Insert(0, item2);


            ListItem item3 = new ListItem("Select Property", "0");
            ddlProperties.Items.Insert(0, item3);
        }
        #endregion



        #region "Events"

        #region"btnAddBlocks Click"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAddBlocks_Click(object sender, EventArgs e)
        {
            if (ddlBlock.SelectedValue != "0")
            {
                int developmentId = 0, phaseId = 0, blockId = 0;
                developmentId = Convert.ToInt32(ddlDevelopment.SelectedValue);
                phaseId = Convert.ToInt32(ddlPhase.SelectedValue);
                blockId = Convert.ToInt32(ddlBlock.SelectedValue);

                bool listContainsItem = lstBlockList.Items.Contains(ddlBlock.SelectedItem);
                if (listContainsItem == false)
                {
                    lstBlockList.Items.Add(ddlBlock.SelectedItem);
                    DataSet PropertiesDataSet = new DataSet();
                    objBlockBL.getPropertiesByDevPhaseBlock(ref PropertiesDataSet, developmentId, phaseId, blockId);
                    for (int i = 0; i < PropertiesDataSet.Tables["properties"].Rows.Count; i++)
                    {
                        string pId = PropertiesDataSet.Tables["properties"].Rows[i]["PROPERTYID"].ToString();
                        string pAdd = PropertiesDataSet.Tables["properties"].Rows[i]["PropertyAddress"].ToString();
                        ListItem item = new ListItem(pId, pAdd);
                        lstPropertiesList.Items.Add(item);
                    }
                }
                else
                {
                }
            }
        }
        #endregion

        #region"btnAddProperties Click"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAddProperties_Click(object sender, EventArgs e)
        {
            if (ddlProperties.SelectedValue != "0")
            {
                bool listContainsItem = lstPropertiesList.Items.Contains(ddlProperties.SelectedItem);
                if (listContainsItem == false)
                {
                    lstPropertiesList.Items.Add(ddlProperties.SelectedItem);
                }
                else
                {
                    //add error msg for duplication addition.
                }
            }
        }
        #endregion

        #region"btnCancel Click"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(PathConstants.SchemeListPath);
        }
        #endregion

        #region"btnCreateScheme Click"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCreateScheme_Click(object sender, EventArgs e)
        {
            saveData();
        }
        #endregion

        #region"ddlDevelopment SelectedIndexChanged"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlDevelopment_SelectedIndexChanged(object sender, EventArgs e)
        {
            int developmentId = Convert.ToInt32(ddlDevelopment.SelectedValue.ToString());
            DevelopmentBL objDevelopmentBL = new DevelopmentBL(new DevelopmentRepo());
            DataSet phaseDataSet = new DataSet();

            objDevelopmentBL.getDevelopmentPhase(ref phaseDataSet, developmentId);
            if (phaseDataSet.Tables[0].Rows.Count > 0)
            {
                ddlPhase.DataSource = phaseDataSet;
                ddlPhase.DataValueField = "PHASEID";
                ddlPhase.DataTextField = "PhaseName";
                ddlPhase.DataBind();
                ListItem item = new ListItem("Select Phase", "0");
                ddlPhase.Items.Insert(0, item);
                ListItem itemAll = new ListItem("All", "-1");
                ddlPhase.Items.Insert(1, itemAll);
                ddlBlock.Items.Clear();
                ListItem item1 = new ListItem("Select Block", "0");
                ddlBlock.Items.Insert(0, item1);
                ddlProperties.Items.Clear();
                ListItem item2 = new ListItem("Select Property", "0");
                ddlProperties.Items.Insert(0, item2);
            }
            else
            {
                ddlPhase.Items.Clear();
                ListItem item = new ListItem("Select Phase", "0");
                ddlPhase.Items.Insert(0, item);
                ddlBlock.Items.Clear();
                ListItem item1 = new ListItem("Select Block", "0");
                ddlBlock.Items.Insert(0, item1);
                ddlProperties.Items.Clear();
                ListItem item2 = new ListItem("Select Property", "0");
                ddlProperties.Items.Insert(0, item2);
            }
        }
        #endregion

        #region" ddlPhase SelectedIndexChanged"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlPhase_SelectedIndexChanged(object sender, EventArgs e)
        {

            int developmentId = 0;
            developmentId = Convert.ToInt32(ddlDevelopment.SelectedValue);
            int phaseId = Convert.ToInt32(ddlPhase.SelectedValue.ToString());
            DataSet blockPropertiesDataSet = new DataSet();
            ddlBlock.Items.Clear();
            ddlProperties.Items.Clear();
            if (phaseId != 0)
            {
                objSchemeBl.getBlocksPropertiesbyPhaseID(ref blockPropertiesDataSet, phaseId, developmentId);
                if (blockPropertiesDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlBlock.DataSource = blockPropertiesDataSet.Tables["blocks"];
                    ddlBlock.DataValueField = "BLOCKID";
                    ddlBlock.DataTextField = "BlockName";
                    ddlBlock.DataBind();
                }
                if (blockPropertiesDataSet.Tables[1].Rows.Count > 0)
                {
                    ddlProperties.DataSource = blockPropertiesDataSet.Tables["properties"];
                    ddlProperties.DataValueField = "PROPERTYID";
                    ddlProperties.DataTextField = "PropertyAddress";
                    ddlProperties.DataBind();
                }
            }
            ListItem item1 = new ListItem("Select Block", "0");
            ddlBlock.Items.Insert(0, item1);
            ListItem item2 = new ListItem("Select Property", "0");
            ddlProperties.Items.Insert(0, item2);
        }
        #endregion

        #endregion


    }
}