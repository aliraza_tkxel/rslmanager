﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PropertyDataRestructure.Base;
using PropertyDataRestructure.Interface;
using PDR_BusinessLogic.Scheme;
using PDR_Utilities.Managers;
using PDR_Utilities.Constants;
using System.Data;
using PDR_DataAccess.Scheme;

namespace PropertyDataRestructure.UserControls.Pdr.Scheme
{
    public partial class SchemeReportedFaults : UserControlBase, IListingPage
    {
        #region Properties
        SchemeBL objSchemeBl = new SchemeBL(new SchemeRepo());
        
        //PageSortBO objPageSortBo = new PageSortBO("DESC", "ADDRESS", 1, 10);       
        bool IsError = false;
        string message = string.Empty;
        #endregion

        #region Events
        #region Page Load Event
        protected void Page_Load(object sender, EventArgs e)
        {
                int schemeId = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);
                populateData(schemeId);
          
        }
        #endregion
        #endregion
        #region IListingPage Implementation
        public void loadData()
        {
            throw new NotImplementedException();
        }

        public void searchData()
        {
            throw new NotImplementedException();
        }
        public void populateData()
        {

        }
        public void populateData(int schemeId)
        {

            DataSet resultDataSet = new DataSet();
            resultDataSet = objSchemeBl.getSchemeFaultsAndDefects(schemeId);

            grdReportedFault.DataSource = resultDataSet;
            grdReportedFault.DataBind();

        }

        public void printData()
        {
            throw new NotImplementedException();
        }

        public void exportGridToExcel()
        {
            throw new NotImplementedException();
        }

        public void exportToPdf()
        {
            throw new NotImplementedException();
        }

        public void downloadData()
        {
            throw new NotImplementedException();
        }

        public void applyFilters()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}