﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MainDetail.ascx.cs"
    Inherits="PropertyDataRestructure.UserControls.Pdr.Scheme.MainDetail" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<style type="text/css">
    .style1
    {
        width: 130px;
    }
</style>
<div id="mainDetail" runat="server" style="border: 1px solid #c5c5c5; border-top:none; width:99%; ">
   
    <asp:UpdatePanel runat="server" ID="updPnlSchemeMain" UpdateMode="Always" class="table-container scheme-tab-container">
        <ContentTemplate>
            <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="500px" />
             <div style="border: 1px solid black;padding:10px;width: 530px;">
            <table id="tblSchemeSetUp">
                <tr>
                    <td class="style1">
                        Scheme Name:<span class="Required">*</span>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtSchemeName" MaxLength="500" Width="176px" Style="margin-left: 0px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtSchemeName"
                            ErrorMessage="Required" CssClass="Required" ValidationGroup="add"></asp:RequiredFieldValidator>
                       
                    </td>
                </tr>
                <tr>
                    <td class="style1">
                        Scheme Code:<span class="Required">*</span>
                    </td>
                    <td>
                        <asp:TextBox runat="server" ID="txtSchemeCode" MaxLength="200" Width="176px" Style="margin-left: 0px"></asp:TextBox>
                    </td>
                    <td>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtSchemeCode"
                            ErrorMessage="Required" CssClass="Required" ValidationGroup="add"></asp:RequiredFieldValidator>
                        
                    </td>
                </tr>
                <tr>
                    <td class="style1">
                        Development Name:<span class="Required">*</span>
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlDevelopment" AutoPostBack="true" Width="180px"
                            OnSelectedIndexChanged="ddlDevelopment_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td class="style1">
                        Phase:<span class="Required">*</span>
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlPhase" AutoPostBack="true" Width="180px"
                            OnSelectedIndexChanged="ddlPhase_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td class="style1">
                        Block(s):
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlBlock" Style="width: 241px !important">
                        </asp:DropDownList>
                        <asp:Button runat="server" ID="btnAddBlocks" Text="Add" Style="margin-left: 5px;"
                            OnClick="btnAddBlocks_Click" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td class="style1">
                    </td>
                    <td>
                        <asp:ListBox runat="server" ID="lstBlockList" SelectionMode="Multiple" Height="115px"
                            Width="180px"></asp:ListBox>
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td class="style1">
                        Properties:
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlProperties" Style="width: 241px !important">
                        </asp:DropDownList>
                        <asp:Button runat="server" ID="btnAddProperties" Text="Add" Style="margin-left: 5px;"
                            OnClick="btnAddProperties_Click" />
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td class="style1">
                    </td>
                    <td>
                        <asp:ListBox runat="server" ID="lstPropertiesList" SelectionMode="Multiple" Height="115px"
                            Width="180px"></asp:ListBox>
                    </td>
                    <td>
                    </td>
                </tr>
            </table>
           
            <div id="divButtons" style="margin-left: 300px; margin-top: 5px;">
                <asp:Button runat="server" ID="btnCancel" Text="Cancel" OnClick="btnCancel_Click" />
                <asp:Button runat="server" ID="btnCreateScheme" Text="Create Scheme" Style="margin-left: 10px;"
                    OnClick="btnCreateScheme_Click" ValidationGroup="add"  />
            </div>
             </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</div>
