﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using PDR_Utilities.Constants;
using PDR_BusinessLogic.Scheme;
using PDR_BusinessObject.Scheme;
using PDR_DataAccess.Scheme;


namespace PropertyDataRestructure.UserControls.Pdr.Scheme
{
    public partial class SchemeServicing : System.Web.UI.UserControl
    {
        SchemeBL objSchemeBl = new SchemeBL(new SchemeRepo());
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int schemeId = 0;
                if ((Request.QueryString[ApplicationConstants.Id] != null))
                    schemeId = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);
                PopulateReportedFaultsDefects(schemeId);


            }
        }

        private void PopulateReportedFaultsDefects(int schemeId)
        {
            DataSet resultDataSet = new DataSet();

            objSchemeBl.GetReportedFaultsDefectsForSchemeBlock(ref resultDataSet, schemeId);

            if (resultDataSet.Tables[0].Rows.Count > 0)
            {
                var lstFaultsDefects = (from row in resultDataSet.Tables[0].AsEnumerable()
                    group row by new
                    {
                        journal = row["JournalId"],
                        type = row["InspectionType"]
                    }
                    into grp
                    select grp.FirstOrDefault()).ToList();

                List<SchemeActivitiesBO> activitiesBoList = new List<SchemeActivitiesBO>();

                foreach (var grp in lstFaultsDefects)
                {
                    var activity = grp;
                    SchemeActivitiesBO activitiesBo = new SchemeActivitiesBO();

                    if (!string.IsNullOrEmpty(activity["AppointmentDate"].ToString()))
                        activitiesBo.AppointmentDate = activity["AppointmentDate"].ToString();
                    if (!string.IsNullOrEmpty((activity["Action"].ToString())))
                        activitiesBo.Action = activity["Action"].ToString();
                    else
                        activitiesBo.Action = "N/A";

                    if (!string.IsNullOrEmpty(activity["CreateDate"].ToString()))
                        activitiesBo.CreateDate = activity["CreateDate"].ToString();
                    if (!string.IsNullOrEmpty(activity["CREATIONDATE"].ToString()))
                        activitiesBo.CREATIONDate = activity["CREATIONDATE"].ToString();


                    activitiesBo.InspectionType = activity["InspectionType"].ToString();

                    activitiesBo.JournalHistoryId = Convert.ToInt32(activity["JournalHistoryId"]);
                    activitiesBo.JournalId = Convert.ToInt32(activity["JournalId"]);
                    activitiesBo.Name = activity["Name"].ToString();
                    activitiesBo.OPERATIVENAME = activity["OPERATIVENAME"].ToString();

                    activitiesBo.AppointmentId = Convert.ToInt32(activity["APPOINTMENTID"]);
                    activitiesBo.AppointmentNotes = activity["AppointmentNotes"].ToString();
                    activitiesBo.InspectionTypeDescription = activity["InspectionTypeDescription"].ToString();

                    if (!string.IsNullOrEmpty(activity["OPERATIVETRADE"].ToString()))
                        activitiesBo.OPERATIVETRADE = activity["OPERATIVETRADE"].ToString();
                    if (!string.IsNullOrEmpty(activity["REF"].ToString()))
                        activitiesBo.REF = activity["REF"].ToString();

                    activitiesBo.Status = activity["Status"].ToString();
                    activitiesBo.IsDocumentAttached = Convert.ToBoolean(activity["IsDocumentAttached"]);
                    activitiesBo.CP12DocumentID = Convert.ToInt32(activity["CP12DocumentID"].ToString());

                    activitiesBoList.Add(activitiesBo);
                }


                this.grdReportedFaults.Visible = true;
                SetFaultsResultDataSetViewState(resultDataSet);
                grdReportedFaults.DataSource = activitiesBoList;
                grdReportedFaults.DataBind();
            }
            else
            {
                grdReportedFaults.EmptyDataText = "No Records Found.";
            }
        }

        #region "Result Faults DataSet Set/Get/Remove"

        protected void SetFaultsResultDataSetViewState(DataSet resultDataSet)
        {
            ViewState[ViewStateConstants.FaultsResultDataSet] = resultDataSet;
        }

        protected DataSet GetFaultsResultDataSetViewState()
        {
            if (ViewState[ViewStateConstants.FaultsResultDataSet] == null)
                return new DataSet();
            else
                return ViewState[ViewStateConstants.FaultsResultDataSet] as DataSet;
        }

        #endregion

        #region "Check InspectionId and Return True/False to Enable/Disable Clip Image"

        protected static bool CheckInspectionId(Object isDocumentAttached, Object cp12DocumentId)
        {
            Boolean status = false;
            if (isDocumentAttached != null)
            {
                if (Convert.ToInt32(cp12DocumentId) > 0 && Convert.ToInt32(isDocumentAttached) == 1)
                    status = true;
            }

            return status;
        }
        #endregion


        #region "Check jobsheet exist"
        protected static bool CheckJobSheetExist(object jobsheetNumber)
        {
            if (jobsheetNumber == null)
                return false;
            if (jobsheetNumber.Equals("N/A"))
                return false;
            return true;
        }
        #endregion

        protected void Show_Hide_ChildGrid(object sender, EventArgs e)
        {
            ImageButton imgShowHide = sender as ImageButton;
            GridViewRow row = imgShowHide.NamingContainer as GridViewRow;
            if (imgShowHide.CommandArgument == "Show")
            {
                row.FindControl("pnlChild").Visible = true;
                imgShowHide.CommandArgument = "Hide";
                imgShowHide.ImageUrl = "~/Images/minus.gif";
                if (grdReportedFaults != null)
                {
                    string JournalId = grdReportedFaults.DataKeys[row.RowIndex].Value.ToString();
                    GridView grdReportedFaultsChild = row.FindControl("grdReportedFaultsChild") as GridView;
                    Label lblInspectionType = (Label)row.FindControl("lblInspectionType");
                    BindActivitiesChild(JournalId, lblInspectionType.Text, grdReportedFaultsChild);
                }
            }
            else
            {
                row.FindControl("pnlChild").Visible = false;
                imgShowHide.CommandArgument = "Show";
                imgShowHide.ImageUrl = "~/Images/plus.gif";
            }
        }

        private void BindActivitiesChild(string JournalId, string inspectionType, GridView grdActivitiesChild)
        {
            DataSet resultDataSet = new DataSet();
            resultDataSet = GetFaultsResultDataSetViewState();
            DataTable filteredActivitiesDt = new DataTable();
            DataView activitiesDv = new DataView();
            activitiesDv = resultDataSet.Tables[0].AsDataView();
            activitiesDv.RowFilter = "JournalId = '" + JournalId + "' AND InspectionType = '" + inspectionType + "'";
            filteredActivitiesDt = activitiesDv.ToTable();
            grdActivitiesChild.DataSource = filteredActivitiesDt.DefaultView.ToTable(true);
            grdActivitiesChild.DataBind();
        }

        protected void ViewFault(object sender, EventArgs e)
        {
            try
            {
                LinkButton lnkBtnJobSheet = (LinkButton)sender;
                GridViewRow row = (GridViewRow)lnkBtnJobSheet.NamingContainer;
                Label lblInspectionType = (Label)row.FindControl("lblInspectionTypeDescription");
                Label lblJournalI = (Label)row.FindControl("lblJournalI");
                this.displayJobSheetInfo(System.Convert.ToString(lblJournalI.Text), System.Convert.ToString(lnkBtnJobSheet.Text), lblInspectionType.Text);
            }
            catch
            {
                throw;
            }
        }

        #region "Display Job Sheet Details"

        private void displayJobSheetInfo(string JI, string jobSheetNumber, string appointmentType)
        {
            string jsnLink = string.Empty;
            jsnLink = "../../../../../RSLJobSheetSummary/Views/JobSheets/ServicingJobSheetSummaryForSchemeBlock.aspx?jsn=" + jobSheetNumber;
            ifrmJobSheet.Attributes.Add("src", jsnLink);
            this.popupJobSheet.Show();
        }


        #endregion

    }
}