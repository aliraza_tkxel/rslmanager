﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BlocksSummary.ascx.cs"
    Inherits="PropertyDataRestructure.UserControls.Pdr.Scheme.BlocksSummary" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<asp:Panel runat="server" ID="pnlBlockSummary" Visible="true"  >
    <cc1:PagingGridView ID="grdBlockSummary" runat="server" AllowPaging="false" AllowSorting="false"
        AutoGenerateColumns="False" BorderStyle="Solid" BorderWidth="0px" CellPadding="4"
        GridLines="None" OrderBy="" PageSize="30" Width="100%" 
        PagerSettings-Visible="false" ShowHeaderWhenEmpty="true"  >
        <Columns>
            <asp:TemplateField  ItemStyle-Width="70px" ShowHeader="false"  >
                <ItemTemplate>
                    <asp:LinkButton ID="lnkBtnTotal" runat="server" CausesValidation="False" CommandArgument='<%#Eval("BlockId") %>'
                        Text='<%# Bind("PropertyType") %>' CommandName="0" OnClick="showProperties" ></asp:LinkButton>
                </ItemTemplate>
                <ItemStyle Width="60%" />
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Blocks:" ItemStyle-Width="70px">
                <ItemTemplate>
                    <asp:Label ID="lblBlockName" runat="server" Text='<%# Bind("BlockName") %>'></asp:Label>
                </ItemTemplate>
                <ItemStyle Width="40%" />
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate >
            No Records Found</EmptyDataTemplate>
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#ffffff" CssClass="table-head" Font-Bold="True" ForeColor="black"
            HorizontalAlign="Left" />
            <AlternatingRowStyle BackColor="#E8E9EA" Wrap="True" />
                <SelectedRowStyle BackColor="#FFFFCC" Font-Bold="True" />
    </cc1:PagingGridView>
</asp:Panel>
