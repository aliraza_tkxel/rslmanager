﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SchemeServicing.ascx.cs"
    Inherits="PropertyDataRestructure.UserControls.Pdr.Scheme.SchemeServicing" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<%@ Register TagPrefix="asp" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit, Version=4.1.60623.0, Culture=neutral, PublicKeyToken=28f01b0e84b6d53e" %>
<style type="text/css">
    .td-DefectFault
    {
        padding: 5px;
    }
</style>
<asp:UpdatePanel runat="server" ID="pnlReportedFault" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <asp:GridView ID="grdReportedFaults" HeaderStyle-HorizontalAlign="left" ShowHeaderWhenEmpty="true"
            runat="server" AutoGenerateColumns="False" ShowHeader="true" Width="100%" GridLines="None"
            CellSpacing="5" CellPadding="4" AllowPaging="True" PagerSettings-Visible="false"
            EmptyDataText="No Record Found" DataKeyNames="JournalId">
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:ImageButton ID="imgShow" runat="server" OnClick="Show_Hide_ChildGrid" ImageUrl="../../../Images/plus.gif"
                            CommandArgument="Show" Style="border: 0px none; width: 11px; height: 11px;" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" Width="15px" CssClass="td-DefectFault" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Reported:">
                    <ItemTemplate>
                        <asp:Label ID="lblReported" runat="server" Text='<%#Eval("CreateDate") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                    <ItemStyle Width="12%" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Ref:">
                    <ItemTemplate>
                        <asp:LinkButton ID="lnkbtnRef" Enabled='<%# CheckJobSheetExist(Eval("REF"))%>' OnClick="ViewFault"
                            runat="server" Text='<%#Eval("REF") %>'></asp:LinkButton>
                    </ItemTemplate>
                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                    <ItemStyle Width="12%" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Fault/Defect:">
                    <ItemTemplate>
                        <asp:Label ID="lblLocation" runat="server" Text='<%#Eval("Action") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                    <ItemStyle Width="30%" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Type:">
                    <ItemTemplate>
                        <asp:Label ID="lblInspectionType" runat="server" Text='<%#Eval("InspectionType") %>'></asp:Label>
                        <asp:Label ID="lblInspectionTypeDescription" Style="display: none;" runat="server"
                            Text='<%#Eval("InspectionTypeDescription") %>'></asp:Label>
                        <asp:Label ID="lblJournalI" Style="display: none;" runat="server" Text='<%#Eval("JournalId") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                    <ItemStyle Width="20%" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Status:">
                    <ItemTemplate>
                        <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                    <ItemStyle Width="25%" />
                </asp:TemplateField>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:HyperLink ID="lnkInspectionId" runat="server" Visible='<%# CheckInspectionId(Eval("isDocumentAttached"), Eval("cp12DocumentId"))%>'
                            Target="_blank" ImageUrl="../../../Images/paperclip_img.jpg" BorderStyle="None"
                            BorderWidth="0px" NavigateUrl='<%# Eval("cp12DocumentId", "~/Views/Common/Download.aspx?inspectionId={0}") %>' />
                    </ItemTemplate>
                    <ItemStyle Height="15px" />
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <tr>
                            <td colspan="100%" style="background: #ffffff">
                                <div id="div<%# Eval("JournalId") %>" style="overflow: auto; position: relative;
                                    left: 15px;">
                                    <asp:Panel ID="pnlChild" runat="server">
                                        <asp:GridView ID="grdReportedFaultsChild" HeaderStyle-HorizontalAlign="left" ShowHeaderWhenEmpty="true"
                                            runat="server" AutoGenerateColumns="False" ShowHeader="false" Width="100%" GridLines="None"
                                            CellSpacing="5" CellPadding="4" AllowPaging="True" PagerSettings-Visible="false">
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblReported" runat="server" Text='<%#Eval("CreateDate") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                                    <ItemStyle Width="12%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Label Text='<%#Eval("REF") %>' ID="lblRef" runat="server" />
                                                    </ItemTemplate>
                                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                                    <ItemStyle Width="12%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblLocation" runat="server" Text='<%#Eval("Action") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                                    <ItemStyle Width="28%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblFaultType" runat="server" Text='<%#Eval("InspectionType") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                                    <ItemStyle Width="20%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblStatus" runat="server" Text='<%#Eval("Status") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                                    <ItemStyle Width="20%" />
                                                </asp:TemplateField>
                                                <asp:TemplateField ShowHeader="False">
                                                    <ItemTemplate>
                                                        <asp:HyperLink ID="lnkInspectionId" runat="server" Visible='<%# CheckInspectionId(Eval("isDocumentAttached"), Eval("cp12DocumentId"))%>'
                                                            Target="_blank" ImageUrl="../../../Images/paperclip_img.jpg" BorderStyle="None"
                                                            BorderWidth="0px" NavigateUrl='<%# Eval("cp12DocumentId", "~/Views/Common/Download.aspx?inspectionId={0}") %>' />
                                                    </ItemTemplate>
                                                    <ItemStyle Height="7%" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </asp:Panel>
                                </div>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>
                No Records Found</EmptyDataTemplate>
            <AlternatingRowStyle BackColor="#E8E9EA" Wrap="True" />
        </asp:GridView>
        <asp:Button ID="btnHidden" runat="server" Text="" Style="display: none;" />
        <asp:ModalPopupExtender ID="popupJobSheet" runat="server" PopupControlID="pnlJobSheet"
            TargetControlID="btnHidden" CancelControlID="btnClose" BackgroundCssClass="modalBackground">
        </asp:ModalPopupExtender>
        <asp:Panel ID="pnlJobSheet" runat="server" CssClass="modalPopup" Style="height: 550px;
            width: 900px; overflow: hidden;">
            <iframe runat="server" id="ifrmJobSheet" class="ifrmJobSheet" style="height: 500px;
                overflow: auto; width: 100%; border: 0px none transparent;" />
            <div style="width: 100%; text-align: left; clear: both;">
                <div style="float: right;">
                    <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="margin_right20" />
                </div>
            </div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
