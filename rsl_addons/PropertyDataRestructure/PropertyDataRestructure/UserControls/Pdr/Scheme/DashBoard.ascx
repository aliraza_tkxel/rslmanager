﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DashBoard.ascx.cs" Inherits="PropertyDataRestructure.UserControls.Pdr.Scheme.DashBoard" %>
<%@ Register TagName="Block" TagPrefix="block" Src="~/UserControls/Pdr/Scheme/BlocksSummary.ascx" %>
<%@ Register TagName="CRM" TagPrefix="crm" Src="~/UserControls/Pdr/Scheme/CRMSummary.ascx" %>
<%@ Register TagName="PM" TagPrefix="pm" Src="~/UserControls/Pdr/Scheme/SchemePlanned.ascx" %>
<%@ Register TagName="Repair" TagPrefix="repair" Src="~/UserControls/Pdr/Scheme/SchemeReportedFaults.ascx" %>
<%@ Register TagName="Servicing" TagPrefix="servicing" Src="~/UserControls/Pdr/Scheme/SchemeServicing.ascx" %>
<%@ Register TagName="RaisePO" TagPrefix="ucRaisePO"  Src="~/UserControls/Pdr/SchemeBlock/PurchaseOrderForm.ascx"  %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
 <script type="text/javascript">

    function openModal() {
        $('#poModalRaiseaPO').modal('show');
    }
   </script>
<uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="500px" />
<div class="pdr-wrap-Dashboard-Content">
    <div class="pdr-wrap-Dashboard-left">
        <div class="pdr-wrap-Dashboard">
            <asp:Panel ID="pnlPropertyAddress" runat="server" CssClass="pnlPropertyAddress">
                &nbsp;
                <asp:Label ID="lblScheme" runat="server" Font-Bold="True" Text=""></asp:Label>
            </asp:Panel>
            <div style="border-bottom: 1px solid black; height: 0px\9; clear: both; margin-left: 2px;
                width: 99%;">
            </div>
           <%-- <asp:UpdatePanel ID="updBlocksSummary" runat="server" >
                <ContentTemplate>--%>
                    <block:Block runat="server" ID="blocksSummary"></block:Block>
                <%--</ContentTemplate>
            </asp:UpdatePanel>--%>
        </div>
        <div class="pdr-wrap-Dashboard">
            <asp:Panel ID="Panel1" runat="server" CssClass="pnlPropertyAddress">
                &nbsp;
                <asp:Label ID="lblPlanned" runat="server" Font-Bold="True" Text="Planned Maintenance:"></asp:Label>
            </asp:Panel>
            <div style="border-bottom: 1px solid black; height: 0px\9; clear: both; margin-left: 2px;
                width: 99%;">
            </div>
             <asp:UpdatePanel ID="updSchemePlanned" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <pm:PM runat="server" ID="schemePlanned"></pm:PM>
                </ContentTemplate>
            </asp:UpdatePanel>
           
        </div>
        <div class="pdr-wrap-Dashboard">
            <asp:Panel ID="pnlFacilities" runat="server" CssClass="pnlPropertyAddress">
                &nbsp;
                <asp:Label ID="lblFacilities" runat="server" Font-Bold="True" Text="Communal Facilities:"></asp:Label>
            </asp:Panel>
            <div style="border-bottom: 1px solid black; height: 0px\9; clear: both; margin-left: 2px;
                width: 99%;">
            </div>
        </div>
        <div class="pdr-wrap-Dashboard">
            <asp:Panel ID="pnlMaintenance" runat="server" CssClass="pnlPropertyAddress">
                &nbsp;
                <asp:Label ID="lblMaintenance" runat="server" Font-Bold="True" Text="Grounds Maintenance Services:"></asp:Label>
            </asp:Panel>
            <div style="border-bottom: 1px solid black; height: 0px\9; clear: both; margin-left: 2px;
                width: 99%;">
            </div>
        </div>
        <div class="pdr-wrap-Dashboard">
            <asp:Panel ID="pnlEsternalLighting" runat="server" CssClass="pnlPropertyAddress">
                &nbsp;
                <asp:Label ID="Label5" runat="server" Font-Bold="True" Text="External Lighting:"></asp:Label>
            </asp:Panel>
            <div style="border-bottom: 1px solid black; height: 0px\9; clear: both; margin-left: 2px;
                width: 99%;">
            </div>
        </div>
    </div>
    <div class="pdr-wrap-Dashboard-right" style="width: 63% !important">
        <div class="pdr-wrap-Dashboard-CRM">
         <asp:UpdatePanel ID="updReportFault" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Panel ID="Panel2" runat="server" CssClass="pnlPropertyAddress">
                <div style="float: left; padding-top: 2px;">
                    &nbsp;
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Text="Summary:"></asp:Label></div>
                <div style="float: right; padding-top: 2px; padding-right: 5px; padding-bottom: 5px;">
                    <asp:Button ID="btnReportFault" Text="Report a Fault" runat="server" OnClick="btnReportFault_Click" />
                    <asp:Button ID="btnRaisePO" Text="Raise a PO" runat="server" Visible="false"  OnClick="btnRaisePO_Click" OnClientClick="openModal();"  />
                </div>
            </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
            
            <div style="border-bottom: 1px solid black; height: 0px\9; clear: both; margin-left: 2px;
                width: 99%;">
            </div>
            <crm:CRM runat="server" ID="crmSummary"></crm:CRM>
        </div>
        <div class="pdr-wrap-Dashboard-CRM">
            <asp:Panel ID="Panel3" runat="server" CssClass="pnlPropertyAddress">
               
                <asp:Label ID="Label2" runat="server" Font-Bold="True" Text=" &nbsp;Reactive Repairs:"></asp:Label>
            </asp:Panel>
            <div style="border-bottom: 1px solid black; height: 0px\9; clear: both; margin-left: 2px;
                width: 99%;">
            </div>
             <asp:UpdatePanel ID="updSchemeRepair" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Timer ID="updSchemeRepairTimer" runat="server" Interval="60000" />
                    <repair:Repair runat="server" ID="schemeRepair"></repair:Repair>
                </ContentTemplate>
            </asp:UpdatePanel>
          
        </div>
        <div class="pdr-wrap-Dashboard-CRM">
            <asp:Panel ID="Panel4" runat="server" CssClass="pnlPropertyAddress">
                &nbsp;
                <asp:Label ID="Label3" runat="server" Font-Bold="True" Text="Servicing:"></asp:Label>
            </asp:Panel>
            <div style="border-bottom: 1px solid black; height: 0px\9; clear: both; margin-left: 2px;
                width: 99%;">
            </div>
            <servicing:Servicing runat="server" ID="schemeServicing" style="padding: 5px"></servicing:Servicing>
        </div>
    </div>
</div>
<asp:Button ID="btnHiddenEntry" UseSubmitBehavior="false" runat="server" Text=""
    Style="display: none;" />
<cc1:ModalPopupExtender ID="mdlpopupScheduler" runat="server" PopupControlID="pnlScheduler"
    TargetControlID="btnHiddenEntry" CancelControlID="btnCancel" BackgroundCssClass="modalBackground">
</cc1:ModalPopupExtender>
<asp:Panel ID="pnlScheduler" runat="server" CssClass="modalPopupSchedular" Style="
    width: 350px; border-color: #CCC; border-width: 1px;">
    <asp:UpdatePanel ID="updPopupReportFault" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
    <div style="height: auto; clear: both;">
        <table id="tblPausedFault" style="width: 350px; text-align: left; margin-right: 10px;">
            <tr>
                <td colspan="2">
                    <b>Report a Fault</b>
                </td>
            </tr>
            <td colspan="2">
                <hr class="searchFaultHr" />
                <br />
            </td>
            <tr>
                <td>
                    Scheme:
                </td>
                <td>
                   
                        <asp:Label ID="lblSchemeName" runat="server" Text=""></asp:Label>
                </td>
            </tr>
          
            <tr>
                <td>
                  <br />
                    Block:
                </td>
                <td>
                 <br />
                    <asp:DropDownList runat="server" ID="ddlBlock" Width="230px" >
                    <asp:ListItem Selected="True" Value="0" Text="Please Select"  ></asp:ListItem>   
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    
                    <br />
                </td>
            </tr>
            
        </table>
    </div>
    </ContentTemplate> 
    </asp:UpdatePanel> 
    <div style=" float:right; width:100%;    " >
 <asp:Button ID="btnNext" runat="server" CssClass="margin_right20" Text="Next >" 
            onclick="btnNext_Click"  />
       <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="margin_right20" />
      
    </div>
</asp:Panel>



<%--<asp:Panel ID="pnlRaisePO" runat="server" class="modalPopupSchedular" Style=" min-width: 600px; max-width: 700px;border-color: #CCCCCC; border-width: 1px; ">--%>
   <%-- <asp:ImageButton ID="imgBtnClosePopup" OnClick="AssignToContractorBtnClose_Click"
        runat="server" Style="position: absolute; top: -12px; right: -12px;" ImageAlign="Top"
        ImageUrl="~/Images/cross2.png" BorderWidth="0" />
   --%>
<asp:Button ID="btnHiddenRaisePO" UseSubmitBehavior="false" runat="server" Text="" Style="display: none;" />
<cc1:ModalPopupExtender ID="mdlpopupRaisePOProvision" runat="server" PopupControlID="pnlRaisePO1"
    TargetControlID="btnHiddenRaisePO"  >
</cc1:ModalPopupExtender>
<asp:Panel ID="pnlRaisePO1" runat="server" >
    <ucRaisePO:RaisePO ID="ucRaisePO" runat="server" />
</asp:Panel>

