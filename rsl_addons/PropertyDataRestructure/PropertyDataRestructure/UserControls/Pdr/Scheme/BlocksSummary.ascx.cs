﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PropertyDataRestructure.Interface;
using PDR_BusinessLogic.Scheme;
using PDR_Utilities.Managers;
using PDR_BusinessObject.PageSort;
using PropertyDataRestructure.Base;
using System.Data;
using PDR_DataAccess.Scheme;
using PropertyDataRestructure.UserControls.Pdr.SchemeBlock;
using PDR_Utilities.Constants;

namespace PropertyDataRestructure.UserControls.Pdr.Scheme
{
    public partial class BlocksSummary : UserControlBase, IListingPage
    {
        #region Properties
        SchemeBL objSchemeBl = new SchemeBL(new SchemeRepo());
       
        //PageSortBO objPageSortBo = new PageSortBO("DESC", "ADDRESS", 1, 10);       
        bool IsError = false;
        string message = string.Empty;
        #endregion
        #region Events
        #region Page Load Event
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        #endregion
        #endregion
        #region IListingPage Implementation
        public void loadData()
        {
            throw new NotImplementedException();
        }

        public void searchData()
        {
            throw new NotImplementedException();
        }
        public void populateData()
        {

        }
        public  void populateData(int schemeId)
        {
            int totalCount = 0;
            //PageSortBO objPageSortBo = base.pBase.vsPageSortBo;

            DataSet resultDataSet = new DataSet();
            resultDataSet = objSchemeBl.getBlocksBySchemeID(schemeId, ref totalCount);

            grdBlockSummary.DataSource = resultDataSet;
            grdBlockSummary.DataBind();
            //objPageSortBo.TotalRecords = totalCount;
            //objPageSortBo.TotalPages = (int)Math.Ceiling(Convert.ToDecimal(totalCount / objPageSortBo.PageSize));
            //base.pBase.vsPageSortBo = objPageSortBo;
        }

        public void printData()
        {
            throw new NotImplementedException();
        }

        public void exportGridToExcel()
        {
            throw new NotImplementedException();
        }

        public void exportToPdf()
        {
            throw new NotImplementedException();
        }

        public void downloadData()
        {
            throw new NotImplementedException();
        }

        public void applyFilters()
        {
            throw new NotImplementedException();
        }
        #endregion
        protected void showProperties(object sender, EventArgs e)
        {
            LinkButton lnkView = new LinkButton();
            lnkView = (LinkButton)sender;
            string commandArgument = lnkView.CommandArgument;
            SchemeBlock.Properties objProperties = (SchemeBlock.Properties)Parent.Parent.Parent.Parent.FindControl("ucProperties");
            objProperties.loadProperties(Convert.ToInt32(commandArgument), ApplicationConstants.Block); 
        }
       
    }
}