﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CRMSummary.ascx.cs"
    Inherits="PropertyDataRestructure.UserControls.Pdr.Scheme.CRMSummary" %>
<%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>--%>
<script type="text/javascript">

    $(window).load(function () {
        loadDashBoardAlerts();

    });

    function loadDashBoardAlerts() {
        loadCounts("getTotalASBCount", '#loadingTotalASB', '#<%= lblTotalASB.ClientID %>');
        loadCounts("getSchemeRepairsCount", '#loadingReportedRepair', '#<%= lblReportedRepair.ClientID %>');

        loadCounts("getSchemeComplaintsCount", '#loadingComplaints', '#<%= lblComplaints.ClientID%>');
        loadCounts("getSchemeArrearsCount", '#loadingArrearCase', '#<%= lblArrearCase.ClientID %>');

        loadCounts("getSchemeValuationCount", '#loadingValuation', '#<%= lblValuation.ClientID %>');
        loadCounts("getSchemeCommunalCount", '#loadingCommunalBoiler', '#<%= lblCommunalBoiler.ClientID %>');
        loadCounts("getUnassignedServicesCount", '#loadingUnAssigned', '#<%= lblUnAssignedServices.ClientID %>');

    }
</script>
<div class="middle_part" style='width: 80%;'>
    <div class=".outer-boxes-CallToAction">
        <div class="cover-box">
            <div class="img-box">
                <div class="text">
                    Total ASB
                    <br />
                    Cases:
                </div>
                <div class="number">
                    <asp:Label Text="" ID="lblTotalASB" runat="server" />
                </div>
                <div class="r-arrow">
                    <a href="#">
                        <img src="../../../../Images/Dashboard/r-arrow.png" alt="" border="0" />
                    </a>
                </div>
                <div class="number-loading" id="loadingTotalASB">
                    <img alt="Please Wait" src="../../../../Images/ajax-loader.gif" class="Loding-Image-Dashboard" />
                </div>
            </div>
            <div class="img-box">
                <div class="text">
                    Service
                    <br />
                    Complaints:
                </div>
                <div class="number">
                    <asp:Label Text="" ID="lblComplaints" runat="server" />
                </div>
                <div class="r-arrow">
                    <a href="#">
                        <img src="../../../../Images/Dashboard/r-arrow.png" alt="" border="0" /></a>
                </div>
                <div class="number-loading" id="loadingComplaints">
                    <img alt="Please Wait" src="../../../../Images/ajax-loader.gif" class="Loding-Image-Dashboard" />
                </div>
            </div>
            <div class="img-box">
                <div class="text">
                    Arrears<br />
                    Cases:
                </div>
                <div class="number">
                    <asp:Label Text="" ID="lblArrearCase" runat="server" />
                </div>
                <div class="r-arrow">
                    <a href="#">
                        <img src="../../../../Images/Dashboard/r-arrow.png" alt="" border="0" />
                    </a>
                </div>
                <div class="number-loading" id="loadingArrearCase">
                    <img alt="Please Wait" src="../../../../Images/ajax-loader.gif" class="Loding-Image-Dashboard" />
                </div>
            </div>
            <div class="img-box">
                <div class="text">
                    Unassigned Cyclical<br />
                    Services:
                </div>
                <div class="number">
                    <asp:Label Text="" ID="lblUnAssignedServices" runat="server" />
                </div>
                <div class="r-arrow">
                <a id="imgArrow">
                    <img src="../../../../Images/Dashboard/r-arrow.png" alt="" border="0" class="click"  /></a>
                </div>
                <div class="number-loading" id="loadingUnAssigned">
                    <img alt="Please Wait" src="../../../../Images/ajax-loader.gif" class="Loding-Image-Dashboard" />
                </div>
            </div>
        </div>
    </div>
    <br />
    <div class=".outer-boxes-CallToAction">
        <div class="cover-box">
            <div class="img-box">
                <div class="text">
                    Total<br />
                    Valuation:
                </div>
                <div class="number">
                    <asp:Label Text="" ID="lblValuation" runat="server" />
                </div>
                <div class="r-arrow">
                    <a href="#">
                        <img src="../../../../Images/Dashboard/r-arrow.png" alt="" border="0" />
                    </a>
                </div>
                <div class="number-loading" id="loadingValuation">
                    <img alt="Please Wait" src="../../../../Images/ajax-loader.gif" class="Loding-Image-Dashboard" />
                </div>
            </div>
            <div class="img-box">
                <div class="text">
                    Communal Boiler
                    <br />
                    Certificates Due:
                </div>
                <div class="number">
                    <asp:Label Text="" ID="lblCommunalBoiler" runat="server" />
                </div>
                <div class="r-arrow">
                    <a href="#">
                        <img src="../../../../Images/Dashboard/r-arrow.png" alt="" border="0" />
                    </a>
                </div>
                <div class="number-loading" id="loadingCommunalBoiler">
                    <img alt="Please Wait" src="../../../../Images/ajax-loader.gif" class="Loding-Image-Dashboard" />
                </div>
            </div>
            <div class="img-box">
                <div class="text">
                    Reported
                    <br />
                    Repairs:
                </div>
                <div class="number">
                    <asp:Label Text="" ID="lblReportedRepair" runat="server" />
                </div>
                <div class="r-arrow">
                    <a href="#">
                        <img src="../../../../Images/Dashboard/r-arrow.png" alt="" border="0" />
                    </a>
                </div>
                <div class="number-loading" id="loadingReportedRepair">
                    <img alt="Please Wait" src="../../../../Images/ajax-loader.gif" class="Loding-Image-Dashboard" />
                </div>
            </div>
        </div>
    </div>
    <br />
    <br />
    <div style="float: left; margin-left: 10px;">
        * Total based on the current financial year
    </div>
</div>
