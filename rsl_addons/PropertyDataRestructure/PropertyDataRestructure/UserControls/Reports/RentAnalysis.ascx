﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RentAnalysis.ascx.cs"
    Inherits="PropertyDataRestructure.UserControls.Reports.RentAnalysis" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<link href="../../Styles/Site.css" rel="stylesheet" media="screen" />
<link href="../../Styles/default.css" rel="stylesheet" type="text/css" />
 <script type="text/javascript" charset="utf-8">

     function ShowPropertyDetail(pid) {
        
         __doPostBack('<%= btnHidden.UniqueID %>', pid);
     }
     //Function to change color of grid row on mouseover
     function mouseIn(row) {
         row.style.backgroundColor = '#e6e6e6';
     }
     //Function to change color of grid row on mouseout
     function mouseOut(row) {
         row.style.backgroundColor = '#FFFFFF';
     }
    </script>
<asp:Button Text="" ID="btnHidden"  runat="server" OnClick="btnHidden_Click"  Style="display: none;"  />
<asp:UpdatePanel ID="updPanelProperties" runat="server">
    <ContentTemplate>
        <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="500px" />
        <div class="pdr-wrap-Rentgrid">
            <div class="pdr-wrap-PropertyDetailGrid" runat="server" id="divPropertyDetail">
                <asp:Panel runat="server" ID="pnlHeaderArea">
                    <div class="pdr-wrap-header">
                        <div style="float: left; width: 100%; padding-left: 10px;">
                            Portfolio Rent Analysis Report ->
                            <asp:Label Text="" ID="lblBreadCrump" runat="server" />
                        </div>
                        <div style="float: left; width: 100%; padding-left: 10px;">
                            <div style="float: left; width: 30%;">
                                Date:<asp:Label Text="" ID="lblDate" runat="server" /></div>
                            <div style="float: right; width: 60%; text-align: right;">
                                Please print as a landscape.
                                <asp:Button Text="Back" ID="btnBack" runat="server" Style="background-color: White;
                                    margin-right: 20px;" />
                                <asp:Button Text="Print All" ID="btnPrint" runat="server" Style="background-color: White;
                                    margin-right: 20px;" OnClick="btnPrint_Click" />
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <cc1:PagingGridView ID="grdRentAnalysis" runat="server" AllowPaging="false" AllowSorting="false"
                    AutoGenerateColumns="False" BorderStyle="Solid" BorderWidth="0px" CellPadding="4"
                    GridLines="None" PageSize="10" Width="95%" PagerSettings-Visible="false" 
                    onrowdatabound="grdRentAnalysis_RowDataBound" EmptyDataText="No Record Found" ShowHeaderWhenEmpty="true">
                    <Columns>
                        <asp:TemplateField HeaderText="Scheme" ItemStyle-Width="70px">
                            <ItemTemplate>
                                <asp:Label ID="lblSchemeName" runat="server" Text='<%# Bind("SCHEME") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="20%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Property Ref:" ItemStyle-Width="70px">
                            <ItemTemplate>
                                <asp:Label ID="lblProperty" runat="server" Text='<%# Bind("PROPERTYID") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="10%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Property Address:">
                            <ItemTemplate>
                                <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("ADDRESS") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="20%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Rent:">
                            <ItemTemplate>
                                <asp:Label ID="lblRent" runat="server"  Text='<%# currencyFormat(Eval("RENT").ToString()) %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="5%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Services:">
                            <ItemTemplate>
                                <asp:Label ID="lblServices" runat="server" Text='<%# currencyFormat(Eval("SERVICES").ToString()) %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="5%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Inelig Ser:">
                            <ItemTemplate>
                                <asp:Label ID="lblInelig" runat="server" Text='<%# currencyFormat(Eval("SERVICES").ToString()) %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="5%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Sup Ser:">
                            <ItemTemplate>
                                <asp:Label ID="lblSup" runat="server" Text='<%# currencyFormat(Eval("SERVICES").ToString()) %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="5%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Water:">
                            <ItemTemplate>
                                <asp:Label ID="lblWater" runat="server" Text='<%# currencyFormat(Eval("SERVICES").ToString()) %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="5%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="C/Tax:">
                            <ItemTemplate>
                                <asp:Label ID="lblCTax" runat="server" Text='<%# currencyFormat(Eval("SERVICES").ToString()) %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="5%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Garage:">
                            <ItemTemplate>
                                <asp:Label ID="lblGarage" runat="server" Text='<%# currencyFormat(Eval("SERVICES").ToString()) %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="5%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Total:">
                            <ItemTemplate>
                                <asp:Label ID="lblTotal" runat="server" Text='<%# currencyFormat(Eval("TOTALRENT").ToString()) %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="5%" />
                        </asp:TemplateField>
                    </Columns>
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <HeaderStyle BackColor="#ffffff" CssClass="table-head" Font-Bold="True" ForeColor="black"
                        HorizontalAlign="Left" />
                </cc1:PagingGridView>
                <br />
                <asp:Panel runat="server" ID="pnlPagination" Visible="true" Style="border-top: 1px solid  #000;
                    vertical-align: middle; width:100%; ">
                    <table class="acenter">
                        <tbody>
                            <tr>
                                <td>
                                    <asp:LinkButton ID="lnkbtnPagerFirst" Text="" runat="server" CommandName="Page" CommandArgument="First"
                                        OnClick="lnkbtnPager_Click" Style="text-decoration: none;">&lt;&lt;First</asp:LinkButton>
                                    &nbsp;
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkbtnPagerPrev" Text="" runat="server" CommandName="Page" CommandArgument="Prev"
                                        OnClick="lnkbtnPager_Click" Style="text-decoration: none;">&lt;Prev</asp:LinkButton>
                                    &nbsp;
                                </td>
                                <td>
                                    Page:&nbsp;
                                    <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="currentPage" />
                                    of&nbsp;
                                    <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />. Records:
                                    <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                                    to&nbsp;
                                    <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                                    of&nbsp;
                                    <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page"
                                        CommandArgument="Next" OnClick="lnkbtnPager_Click" Style="text-decoration: none;" />
                                    &nbsp;
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page"
                                        CommandArgument="Last" OnClick="lnkbtnPager_Click" Style="text-decoration: none;" />
                                    &nbsp;
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </asp:Panel>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>
