﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PropertyDataRestructure.Base;
using PropertyDataRestructure.Interface;
using PDR_Utilities.Helpers;
using PDR_BusinessObject.PageSort;
using System.Data;
using PDR_BusinessLogic.VoidInspections;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_DataAccess.VoidInspections;
using PDR_Utilities.Constants;
using System.Threading;

namespace PropertyDataRestructure.UserControls.Reports
{
    public partial class VoidProperties : UserControlBase, IListingPage
    {
        #region Properties

        PageSortBO objPageSortBo = new PageSortBO("DESC", "Ref", 1, 30);
        public int rowCount = 0;
        #endregion

        #region PageLoad Event
        /// <summary>
        /// PageLoad Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    uiMessage.hideMessage();
                }
            }
            catch (ThreadAbortException ex)
            {
                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region Change Page Number
        /// <summary>
        ///  Change Page Number
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>  
        protected void lnkbtnPager_Click(object sender, EventArgs e)
        {
            try
            {
                PageSortBO objPageSortBo = new PageSortBO("DESC", "Ref", 1, 30);
                GridHelper.processGridPager(sender, objPageSortBo, pageSortViewState);
                loadData();

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        #endregion

        #region "grdVoidProperties Sorting"
        protected void grdVoidProperties_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                objPageSortBo = pageSortViewState;
                objPageSortBo.SortExpression = e.SortExpression;
                objPageSortBo.PageNumber = 1;
                grdVoidProperties.PageIndex = 0;
                objPageSortBo.setSortDirection();
                pageSortViewState = objPageSortBo;
                this.loadData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion
        protected void imgBtnArrow_Click(object sender, EventArgs e)
        {
            try
            {
                ImageButton btnView = (ImageButton)sender;
                string pid = btnView.CommandArgument.ToString();
                string path = PathConstants.PropertyRecordPath + pid;
                Response.Redirect(path);

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        protected void grdVoidProperties_RowCreated(object sender, GridViewRowEventArgs e)
        {
            //check if it is a header row
            //since allowsorting is set to true, column names are added as command arguments to
            //the linkbuttons by DOTNET API
            if (e.Row.RowType == DataControlRowType.Header)
            {
                LinkButton btnSort;
                Image image;
                //iterate through all the header cells
                foreach (TableCell cell in e.Row.Cells)
                {
                    //check if the header cell has any child controls
                    if (cell.HasControls())
                    {
                        //get reference to the button column
                        btnSort = (LinkButton)cell.Controls[0];
                        image = new Image();
                        if (pageSortViewState != null)
                        {
                            if (btnSort.CommandArgument == pageSortViewState.SortExpression)
                            {
                                //following snippet figure out whether to add the up or down arrow
                                //based on the sortdirection
                                if (pageSortViewState.SortDirection == SortDirection.Ascending.ToString())
                                {
                                    image.ImageUrl = "~/Images/Grid/sort_asc.png";
                                }
                                else
                                {
                                    image.ImageUrl = "~/Images/Grid/sort_desc.png";
                                }
                            }
                            else
                            {
                                image.ImageUrl = "~/Images/Grid/sort_both.png";
                            }
                            cell.Controls.Add(image);
                        }
                    }
                }
            }
        }

        #region"change PageNumber"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void changePageNumber(object sender, EventArgs e)
        {
            try
            {
                Button btnGo = new Button();
                btnGo = (Button)sender;
                PageSortBO objPageSortBo;
                objPageSortBo = new PageSortBO("DESC", "Ref", 1, 30);
                objPageSortBo = pageSortViewState;
                int pageNumber = 1;
                pageNumber = Convert.ToInt32(txtPageNumber.Text);
                txtPageNumber.Text = String.Empty;
                objPageSortBo = pageSortViewState;
                if (((pageNumber >= 1)
                            && (pageNumber <= objPageSortBo.TotalPages)))
                {
                    objPageSortBo = pageSortViewState;
                    objPageSortBo.PageNumber = pageNumber;
                    pageSortViewState = objPageSortBo;
                    loadData();

                }
                else
                {
                    // uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, UserMessageConstants.InvalidPageNumber, true);
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #region IListingPage Implementation
        public void loadData()
        {
            uiMessage.hideMessage();
            string searchText = objSession.SearchText;
            bool actualReletCol_Visibility = false;
            PageSortBO objPageSortBo;
            if (pageSortViewState == null)
            {
                objPageSortBo = new PageSortBO("DESC", "Ref", 1, 30);
                pageSortViewState = objPageSortBo;
            }
            else
            {
                objPageSortBo = pageSortViewState;
            }

            int totalCount = 0;
            VoidInspectionsBL objVoidInspectionsBl = new VoidInspectionsBL(new VoidInspectionsRepo());
            DataSet resultDataSet = new DataSet();
            if (Request.QueryString["rpt"] == ApplicationConstants.PendingTermination)
            {
                totalCount = objVoidInspectionsBl.getPendingTermination(ref resultDataSet, objPageSortBo, searchText);
                actualReletCol_Visibility = true;
            }
            else if (Request.QueryString["rpt"] == ApplicationConstants.ReletDue)
            {
                totalCount = objVoidInspectionsBl.getReletAlertCount(ref resultDataSet, objPageSortBo, searchText);
            }
            else
            {
                totalCount = objVoidInspectionsBl.getAvailableProperties(ref resultDataSet, objPageSortBo, searchText);
                actualReletCol_Visibility = false;
            }

            grdVoidProperties.DataSource = resultDataSet;
            grdVoidProperties.DataBind();
            objPageSortBo.TotalRecords = totalCount;
            objPageSortBo.TotalPages = Convert.ToInt32(Math.Ceiling((Double)totalCount / objPageSortBo.PageSize));
            if (actualReletCol_Visibility)
                grdVoidProperties.Columns[8].Visible = true;
            else
                grdVoidProperties.Columns[8].Visible = false;

            //if (objPageSortBo.TotalPages > 0)
            //{
            //    pnlPagination.Visible = true;
            //}
            //else
            //{
            //    pnlPagination.Visible = false;
            //    uiMessage.showErrorMessage(UserMessageConstants.RecordNotFound);
            //}
            pageSortViewState = objPageSortBo;
            GridHelper.setGridViewPager(ref pnlPagination, objPageSortBo);
        }

        public void searchData()
        {
            PageSortBO objPageSortBo = new PageSortBO("DESC", "Ref", 1, 30);
            pageSortViewState = objPageSortBo;
            loadData();
        }
        public void populateData()
        {
            throw new NotImplementedException();
        }

        public void printData()
        {
            throw new NotImplementedException();
        }

        public void exportGridToExcel()
        {
            throw new NotImplementedException();
        }

        public void exportToPdf()
        {
            throw new NotImplementedException();
        }

        public void downloadData()
        {
            throw new NotImplementedException();
        }

        public void applyFilters()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}