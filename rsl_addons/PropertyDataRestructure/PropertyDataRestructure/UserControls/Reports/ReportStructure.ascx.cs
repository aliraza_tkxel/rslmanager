﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using PDR_BusinessLogic.Reports;
using System.Data;
using PDR_BusinessObject.Reports;
using System.Drawing;
using PDR_Utilities.Managers;
using System.IO;
using System.Text;
using PDR_DataAccess.Reports;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PropertyDataRestructure.Base;

namespace PropertyDataRestructure.UserControls.Reports
{
    public partial class ReportStructure : UserControlBase
    {
        #region Properties
        ReportsBL objReportsBL = new ReportsBL(new ReportsRepo());

        public int rowCount = 0;
        bool IsError = false;
        string message = string.Empty;
        #endregion
        #region Events
        #region Page Load Event
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    txtDateAt.Text = DateTime.Now.ToString("dd/MM/yyyy");
                    load();
                    string rpt = Request.QueryString["rpt"];
                    if (rpt == "rent")
                    {

                        lblTitle.Text = "Rent Analysis";
                    }
                    else if (rpt == "stock")
                    {

                        lblTitle.Text = "Stock Analysis";
                    }
                    else if (rpt == "void")
                    {
                        lblTitle.Text = "Void Analysis";
                    }
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        #endregion

        #region ddlPatch Selected Index Changed
        /// <summary>
        /// ddlPatch Selected Index Changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlPatch_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int patchId = Convert.ToInt32(ddlPatch.SelectedValue);

                if (patchId > 0)
                {
                    populateDevelopmentByPatchIdAndLAId(patchId, null);
                    ddlLAArea.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region ddlLAArea Selected Index Changed event
        /// <summary>
        /// ddlLAArea Selected Index Changed event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlLAArea_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int patchId = Convert.ToInt32(ddlPatch.SelectedValue);
                int LAId = Convert.ToInt32(ddlLAArea.SelectedValue);
                if (LAId > 0)
                {
                    populateDevelopmentByPatchIdAndLAId(null, LAId);
                    ddlPatch.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion
        #region btnSearch click event
        /// <summary>
        /// btnSearch click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                btnPrint.Visible = true;
                search();
                pnlAnalysisList.Visible = true;
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion
        #region grdAnalysisList Row Data Bound Event
        /// <summary>
        /// grdAnalysisList Row Data Bound Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdAnalysisList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string rpt = Request.QueryString["rpt"];
                    LinkButton lnkBtnTotal = (LinkButton)e.Row.FindControl("lnkBtnTotal");
                    LinkButton lnkBtnLet = (LinkButton)e.Row.FindControl("lnkBtnLet");
                    Label lblTotal = (Label)e.Row.FindControl("lblTotal");
                    Label lblLet = (Label)e.Row.FindControl("lblLet");
                    if (rpt == "void")
                    {
                        lnkBtnTotal.Visible = false;
                        lnkBtnLet.Visible = false;
                        lblTotal.Visible = true;
                        lblLet.Visible = true;
                    }
                    else
                    {
                        lnkBtnTotal.Visible = true;
                        lnkBtnLet.Visible = true;
                        lblTotal.Visible = false;
                        lblLet.Visible = false;
                    }
                  

                    if (e.Row.RowIndex + 1 == rowCount)
                    {
                        // e.Row.Attributes.Add(" style='border-top", " 1px solid #000'");
                        e.Row.Style.Add("border-top", "1px solid #000");
                        lnkBtnTotal.Visible = false;                        
                        lblTotal.Visible = true;
                    }

                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }


        #endregion

        #region grdAnalysisList Row Command event
        protected void grdAnalysisList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                uiMessage.hideMessage();
                string rpt = Request.QueryString["rpt"];
                LinkButton lnkView = (LinkButton)e.CommandSource;
                string commandArgument = lnkView.CommandArgument;
                string[] argument = commandArgument.Split(';');
                string assetTypeId = argument[0].ToString();

                ReportsBO objReportsBo = fillReportObject();
                objReportsBo.Asset = argument[1].ToString();
                objReportsBo.AssetType = null;
                if (assetTypeId != string.Empty)
                    objReportsBo.AssetType = Convert.ToInt32(assetTypeId);
                objReportsBo.Status = null;
                if (e.CommandName != "0")
                {
                    objReportsBo.Status = Convert.ToInt32(e.CommandName);
                    switch (e.CommandName)
                    {
                        case "1":
                            objReportsBo.Header = "Available";
                            break;
                        case "2":
                            objReportsBo.Header = "Let";
                            break;
                        default:
                            objReportsBo.Header = "Unavailable";
                            break;
                    }
                }

                if (rpt == "rent")
                {

                    showRentPopUp(objReportsBo);
                }
                else if (rpt == "stock")
                {

                    showStockPopUp(objReportsBo);
                }
                else if (rpt == "void")
                {
                    showVoidPopUp(objReportsBo);
                }
                objSession.ReportsBo = objReportsBo;
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion
        #region btn print click event
        protected void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(UpdatePanel), Guid.NewGuid().ToString(), "PrintPage('divAnalysisList')", true);
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion
        #endregion
        #region Functions
        #region populate All Drop down List
        #region Load Dropdowns
        private void load()
        {
            populatePatch();
            populateLocalAuthority();
            populateDevelopmentByPatchIdAndLAId();
        }

        #endregion

        #region populate Patch
        private void populatePatch()
        {
            DataSet resultDataSet = new DataSet();
            resultDataSet = objReportsBL.getPatch();
            ddlPatch.DataSource = resultDataSet;
            ddlPatch.DataValueField = "PATCHID";
            ddlPatch.DataTextField = "LOCATION";
            ddlPatch.DataBind();
            ddlPatch.Items.Insert(0, new ListItem("All", "-1"));
            ddlPatch.SelectedValue = "-1";

        }
        #endregion
        #region populate Local Authority
        private void populateLocalAuthority()
        {
            DataSet resultDataSet = new DataSet();
            resultDataSet = objReportsBL.getLocalAuthority();
            ddlLAArea.DataSource = resultDataSet;
            ddlLAArea.DataValueField = "LOCALAUTHORITYID";
            ddlLAArea.DataTextField = "DESCRIPTION";
            ddlLAArea.DataBind();
            ddlLAArea.Items.Insert(0, new ListItem("Please Select", "-1"));
            ddlLAArea.SelectedValue = "-1";

        }
        #endregion
        #region populate Development By PatchId And LAId
        private void populateDevelopmentByPatchIdAndLAId(int? patchId = null, int? LAId = null)
        {
            DataSet resultDataSet = new DataSet();
            resultDataSet = objReportsBL.getDevelopmentByPatchIdAndLAId(patchId, LAId);
            ddlScheme.DataSource = resultDataSet;
            ddlScheme.DataValueField = "DEVELOPMENTID";
            ddlScheme.DataTextField = "DEVELOPMENTNAME";
            ddlScheme.DataBind();
            ddlScheme.Items.Insert(0, new ListItem("Please Select", "-1"));
            ddlScheme.SelectedValue = "-1";

        }
        #endregion



        #endregion

        #region Search Method
        private void search()
        {
            ReportsBO objReportsBO = fillReportObject();
            objSession.ReportsBo = objReportsBO;
            DataSet resultDs = new DataSet();
            resultDs = objReportsBL.getProfessionalAnalysisReport(objReportsBO);
            rowCount = resultDs.Tables[0].Rows.Count;
            grdAnalysisList.DataSource = resultDs;
            grdAnalysisList.DataBind();
        }

        #endregion
        #region fill Reports Object to search data
        private ReportsBO fillReportObject()
        {
            ReportsBO objReportsBO = new ReportsBO();
            objReportsBO.ReportDate = Convert.ToDateTime(txtDateAt.Text.Trim());
            if (Convert.ToInt32(ddlPatch.SelectedValue) > 0)
            {
                objReportsBO.Patch = Convert.ToInt32(ddlPatch.SelectedValue);
            }
            if (Convert.ToInt32(ddlLAArea.SelectedValue) > 0)
            {
                objReportsBO.LocalAuthority = Convert.ToInt32(ddlLAArea.SelectedValue);
            }
            if (Convert.ToInt32(ddlScheme.SelectedValue) > 0)
            {
                objReportsBO.Scheme = Convert.ToInt32(ddlScheme.SelectedValue);
            }
            if (txtPostCode.Text != "")
            {
                objReportsBO.PostCode = txtPostCode.Text.Trim();
            }
            return objReportsBO;
        }
        #endregion

        #region show Rent PopUp
        private void showRentPopUp(ReportsBO objReportsBo)
        {
            rentAnalysisDetail.Visible = true;
            stockAnalysisDetail.Visible = false;
            voidAnalysisDetail.Visible = false;
            rentAnalysisDetail.pageSortViewState = null;
            rentAnalysisDetail.populatePropertesDetail(objReportsBo);
            mdlRentAnalysisPopup.Show();
        }
        #endregion
        #region show Stock PopUp
        private void showStockPopUp(ReportsBO objReportsBo)
        {
            rentAnalysisDetail.Visible = false;
            stockAnalysisDetail.Visible = true;
            voidAnalysisDetail.Visible = false;
            Panel pnlAssetType = (Panel)stockAnalysisDetail.FindControl("pnlAssetType");
            pnlAssetType.Visible = true;
            stockAnalysisDetail.populateProperteyAssetDetail(objReportsBo);
            mdlRentAnalysisPopup.Show();
        }

        #endregion
        #region show Void PopUp
        private void showVoidPopUp(ReportsBO objReportsBo)
        {
            rentAnalysisDetail.Visible = false;
            stockAnalysisDetail.Visible = false;
            voidAnalysisDetail.Visible = true;
            Panel pnlVoidDetail = (Panel)voidAnalysisDetail.FindControl("pnlVoidDetail");
            pnlVoidDetail.Visible = true;

            voidAnalysisDetail.populateVoidAnalysisReport(objReportsBo);
            mdlRentAnalysisPopup.Show();
        }

        #endregion





        #endregion

    }
}