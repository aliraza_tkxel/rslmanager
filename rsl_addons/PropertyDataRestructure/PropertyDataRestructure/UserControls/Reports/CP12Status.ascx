﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CP12Status.ascx.cs"
    Inherits="PropertyDataRestructure.UserControls.Reports.CP12Status" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<script type="text/javascript">
    function startDownload(documentId) {
        window.open("../../Views/Common/Download.aspx?documentId=" + documentId + "&type=" + "CP12Document");
    }
</script>
<asp:UpdatePanel runat="server" ID="updPanelVoidProperties">
    <ContentTemplate>
        <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="500px" />
        <div style=" border-bottom: 1px solid #A0A0A0; width: 100%; padding:0">
            <cc1:PagingGridView ID="grdCP12Status" runat="server" AutoGenerateColumns="False" EmptyDataText="No Records Found"
                OnRowCreated="grdCP12Status_RowCreated" OnSorting="grdCP12Status_Sorting"
                Width="100%" Style="overflow: scroll" BorderWidth="0px" CssClass="dashboard webgrid table table-responsive" 
                GridLines="None" ShowHeaderWhenEmpty="True" AllowPaging="false" AllowSorting="True" PageSize="10">
                <Columns>
                    <asp:TemplateField HeaderText="Ref:" ItemStyle-CssClass="dashboard" SortExpression="Ref">
                        <ItemTemplate>
                            <asp:Label ID="lblRef" runat="server" Text='<%# Bind("Ref") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle BorderStyle="None" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Scheme:" SortExpression="Scheme">
                        <ItemTemplate>
                            <asp:Label ID="lblScheme" runat="server" Text='<%# Eval("Scheme") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="10%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Address:" SortExpression="Address">
                        <ItemTemplate>
                            <asp:Label ID="lblAddress" runat="server" Text='<%# Eval("Address") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="15%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Status:" SortExpression="Status">
                        <ItemTemplate>
                            <asp:Label ID="lblStatus" runat="server" Text='<%# Eval("Status") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="10%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Termination:" SortExpression="Termination">
                        <ItemTemplate>
                            <asp:Label ID="lblTermination" runat="server" Text='<%# Eval("Termination") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="5%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Target Relet:" SortExpression="Relet">
                        <ItemTemplate>
                            <asp:Label ID="lblRelet" runat="server" Text='<%# Eval("Relet") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="5%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Actual Relet:" SortExpression="ActualRelet">
                        <ItemTemplate>
                            <asp:Label ID="lblActualRelet" runat="server" Text='<%# Eval("ActualRelet") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="5%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Works Completed:" SortExpression="WorksCompletionDate">
                        <ItemTemplate>
                            <asp:Label ID="lblWorksCompletionDate" runat="server" Text='<%# Eval("WorksCompletionDate") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="5%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Service Appointment:" SortExpression="ServiceAppointment">
                        <ItemTemplate>
                            <asp:Label ID="lblServiceAppointment" runat="server" Text='<%# Eval("ServiceAppointment") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="10%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="CP12 Issued:" SortExpression="CP12Issued">
                        <ItemTemplate>
                            <asp:Label ID="lblCP12Issued" runat="server" Text='<%# Eval("CP12Issued") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="10%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="CP12 Renewal:" SortExpression="CP12Renewal">
                        <ItemTemplate>
                            <asp:Label ID="lblCP12Renewal" runat="server" Text='<%# Eval("CP12Renewal") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="10%" />
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="false">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgBtnArrow" runat="server" BorderStyle="None" BorderWidth="0px"
                                OnClientClick='<%# "startDownload("+Eval("LGSRID")+");return false;" %>' ImageUrl='<%# "~/Images/aero.png" %>' />
                        </ItemTemplate>
                        <ItemStyle Width="5%" />
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle BorderColor="Black" HorizontalAlign="Left" />
            </cc1:PagingGridView>
        </div>
        <%--Pager Template Start--%>
        <asp:Panel runat="server" ID="pnlPagination" Visible="false" Style="color: Black; margin: 0 auto; width: 98%;">
            <div style="width: 100%; padding: 15px 0 30px 0px; text-align:center;">
                <div class="paging-left">
                    <span style="padding-right:10px;">
                        <asp:LinkButton ID="lnkbtnPagerFirst" runat="server" Text="" CommandName="Page" CommandArgument="First" cssClass="lnk-btn"
                            OnClick="lnkbtnPager_Click">
                            &lt;&lt;First
                        </asp:LinkButton>
                        <asp:LinkButton ID="lnkbtnPagerPrev" Text="Prev" runat="server" CommandName="Page" CommandArgument="Prev" cssClass="lnk-btn"
                            OnClick="lnkbtnPager_Click">
                            &lt;Prev
                        </asp:LinkButton>
                    </span>
                    <span style="padding-right:10px;">
                        <b>Page:</b>
                        <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="currentPage" />
                        of
                        <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />.
                    </span>
                    <span style="padding-right:20px;">
                        <b>Result:</b>
                        <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                        to
                        <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                        of
                        <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                    </span>
                    <span style="padding-right:10px;">
                        <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page" CommandArgument="Next" cssClass="lnk-btn"
                            OnClick="lnkbtnPager_Click">
                                    
                        </asp:LinkButton>
                        <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page" CommandArgument="Last" cssClass="lnk-btn"
                            OnClick="lnkbtnPager_Click">
                                        
                        </asp:LinkButton>
                    </span>
                </div>
                <div style="float: right;">
                    <span>
                        <asp:Button ID="btnExportToExcel" runat="server" Text="Export to XLS" UseSubmitBehavior="False" OnClick="btnExportToExcel_Click"
                            class="btn btn-xs btn-blue right" style="padding:1px 5px !important;" />
                    </span>
                    <asp:RangeValidator ID="rangevalidatorPageNumber" runat="server" ErrorMessage="Enter a page between 1 and "
                        ValidationGroup="pageNumber" MinimumValue="1" MaximumValue="1" ControlToValidate="txtPageNumber"
                        Type="Integer" SetFocusOnError="True" CssClass="Required" Display="Dynamic" />
                    <div class="field" style="margin-right: 10px;">
                        <asp:TextBox ID="txtPageNumber" runat="server" Width="40px" ValidationGroup="pageNumber" PlaceHolder="Page"
                        onchange="document.getElementById('txtPageNumberHidden').value = this.value;" />
                    </div>
                    <span>
                        <asp:Button Text="Go" runat="server" ID="btnGoPageNumber" ValidationGroup="pageNumber" UseSubmitBehavior="false"
                            class="btn btn-xs btn-blue" style="padding:1px 5px !important; margin-right:10px; min-width:0px;" OnClick="changePageNumber" />
                    </span>
                </div>
            </div>
        </asp:Panel>
        <%--Pager Template End--%>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnExportToExcel" />
    </Triggers>
</asp:UpdatePanel>
