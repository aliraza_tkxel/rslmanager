﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VoidNoEntry.ascx.cs"
    Inherits="PropertyDataRestructure.UserControls.Reports.VoidNoEntry" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<%@ Register TagPrefix="ucPostArrangeInspectionPopUp" TagName="ArrangePostInspectionPopUp"
    Src="~/UserControls/Void/VoidInspection/ArrangePostVoidInspectionPopUp.ascx" %>
<%@ Register TagPrefix="ucArrangeInspectionPopUp" TagName="ArrangeInspectionPopUp"
    Src="~/UserControls/Void/VoidInspection/ArrangeInspectionPopUp.ascx" %>
<%@ Register TagPrefix="ucScheduleChecks" TagName="ScheduleChecksPopUp" Src="~/UserControls/Void/GasElectricCheck/ScheduleChecksPopUp.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:UpdatePanel runat="server" ID="updPanelNoEntryList">
    <ContentTemplate>
        <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="500px" />
        <div style=" border-bottom: 1px solid #A0A0A0; width: 100%; padding:0">
            <cc1:PagingGridView ID="grdNoEntry" runat="server" AutoGenerateColumns="False" EmptyDataText="No Records Found"
                OnRowCreated="grdNoEntry_RowCreated" OnSorting="grdNoEntry_Sorting"
                Width="100%" Style="overflow: scroll" BorderWidth="0px" CssClass="dashboard webgrid table table-responsive" 
                GridLines="None" ShowHeaderWhenEmpty="True" AllowPaging="false" AllowSorting="True" PageSize="10">
                <Columns>
                    <asp:TemplateField HeaderText="JSV:" ItemStyle-CssClass="dashboard" SortExpression="Ref">
                        <ItemTemplate>
                            <asp:Label ID="lblRef" runat="server" Text='<%# Bind("Ref") %>'></asp:Label>
                            <asp:HiddenField ID="hdnLegionella" runat="server" Value='<%# Bind("Legionella") %>' />
                        </ItemTemplate>
                        <ItemStyle BorderStyle="None" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Recorded by:" SortExpression="RecordedBy">
                        <ItemTemplate>
                            <asp:Label ID="lblRecordedBy" runat="server" Text='<%# Eval("RecordedBy") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="5%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Address:" SortExpression="Address">
                        <ItemTemplate>
                            <asp:Label ID="lblAddress" runat="server" Text='<%# Eval("Address") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="10%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Type:" SortExpression="AppointmentType">
                        <ItemTemplate>
                            <asp:Label ID="lblAppointmentType" runat="server" Text='<%# Eval("AppointmentType") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="5%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Details:" SortExpression="Details">
                        <ItemTemplate>
                            <asp:Label ID="lblDetails" runat="server" Text='<%# Eval("Details") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="15%" />
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Termination:" SortExpression="Termination">
                        <ItemTemplate>
                            <asp:Label ID="lblTermination" runat="server" Text='<%# Eval("Termination") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="5%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Target Relet:" SortExpression="Relet">
                        <ItemTemplate>
                            <asp:Label ID="lblRelet" runat="server" Text='<%# Eval("Relet") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="5%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Actual Relet:" SortExpression="ActualRelet">
                        <ItemTemplate>
                            <asp:Label ID="lblActualRelet" runat="server" Text='<%# Eval("ActualRelet") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="5%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Works Completed:" SortExpression="WorksCompletionDate">
                        <ItemTemplate>
                            <asp:Label ID="lblWorksCompletionDate" runat="server" Text='<%# Eval("WorksCompletionDate") %>'></asp:Label>
                        </ItemTemplate>
                    <ItemStyle Width="5%" />
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="false">
                        <ItemTemplate>
                            <asp:Button Text="Schedule" runat="server" ID="btnSchedule" CommandArgument='<%# Eval("JournalId") %>'
                                OnClick="imgBtnSchedule_Click"
                                CssClass="btn btn-xs btn-blue" style="padding: 2px 10px !important; margin: -3px 0 0 0;" />
                        </ItemTemplate>
                        <ItemStyle Width="5%" />
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle BorderColor="Black" HorizontalAlign="Left" />
            </cc1:PagingGridView>
        </div>
        <%--Pager Template Start--%>
        <asp:Panel runat="server" ID="pnlPagination" Visible="false" Style="color: Black; margin: 0 auto; width: 98%;">
            <div style="width: 100%; padding: 15px 0 30px 0px; text-align:center;">
                <div class="paging-left">
                    <span style="padding-right:10px;">
                        <asp:LinkButton ID="lnkbtnPagerFirst" runat="server" Text="" CommandName="Page" CommandArgument="First" cssClass="lnk-btn"
                            OnClick="lnkbtnPager_Click">
                            &lt;&lt;First
                        </asp:LinkButton>
                        <asp:LinkButton ID="lnkbtnPagerPrev" Text="Prev" runat="server" CommandName="Page" CommandArgument="Prev" cssClass="lnk-btn"
                            OnClick="lnkbtnPager_Click">
                            &lt;Prev
                        </asp:LinkButton>
                    </span>
                    <span style="padding-right:10px;">
                        <b>Page:</b>
                        <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="currentPage" />
                        of
                        <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />.
                    </span>
                    <span style="padding-right:20px;">
                        <b>Result:</b>
                        <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                        to
                        <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                        of
                        <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                    </span>
                    <span style="padding-right:10px;">
                        <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page" CommandArgument="Next" cssClass="lnk-btn"
                            OnClick="lnkbtnPager_Click">
                                    
                        </asp:LinkButton>
                        <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page" CommandArgument="Last" cssClass="lnk-btn"
                            OnClick="lnkbtnPager_Click">
                                        
                        </asp:LinkButton>
                    </span>
                </div>
                <div style="float: right;">
                    <asp:RangeValidator ID="rangevalidatorPageNumber" runat="server" ErrorMessage="Enter a page between 1 and "
                        ValidationGroup="pageNumber" MinimumValue="1" MaximumValue="1" ControlToValidate="txtPageNumber"
                        Type="Integer" SetFocusOnError="True" CssClass="Required" Display="Dynamic" />
                    <div class="field" style="margin-right: 10px;">
                        <asp:TextBox ID="txtPageNumber" runat="server" Width="40px" ValidationGroup="pageNumber" PlaceHolder="Page"
                        onchange="document.getElementById('txtPageNumberHidden').value = this.value;" />
                    </div>
                    <span>
                        <asp:Button Text="Go" runat="server" ID="btnGoPageNumber" ValidationGroup="pageNumber" UseSubmitBehavior="false"
                            class="btn btn-xs btn-blue" style="padding:1px 5px !important; min-width:0px;" OnClick="changePageNumber" />
                    </span>
                </div>
            </div>
        </asp:Panel>
        <%--Pager Template End--%>
    </ContentTemplate>
</asp:UpdatePanel>
<ajax:ModalPopupExtender ID="mdlpopupInspection" runat="server" PopupControlID="pnlVoidInspection"
    TargetControlID="lblHiddenEntry" CancelControlID="imgBtnCancelPopup" BackgroundCssClass="modalBackground">
</ajax:ModalPopupExtender>
<asp:Label Text="" ID="lblHiddenEntry" runat="server" />
<asp:Panel ID="pnlVoidInspection" runat="server" CssClass="modalPopupSchedular" Style="width: 410px;
    border-color: black; border-bottom-style: solid; border-width: 1px;">
    <asp:ImageButton ID="imgBtnCancelPopup" runat="server" Style="position: absolute;
        top: -12px; right: -12px; width: 22px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png"
        BorderWidth="0" />
    <ucPostArrangeInspectionPopUp:ArrangePostInspectionPopUp ID="ucArrangePostInspectionPopUp"
        runat="server" Visible="false" />
    <ucArrangeInspectionPopUp:ArrangeInspectionPopUp ID="ucArrangeInspectionPopUp" runat="server" />
</asp:Panel>
<ajax:ModalPopupExtender ID="mdlpopupArrangeGasElectricCheck" runat="server" PopupControlID="pnlScheduleChecksPopUp"
    TargetControlID="lblGasCheckPopUp" CancelControlID="imgBtnGasCheckPopUp" BackgroundCssClass="modalBackground">
</ajax:ModalPopupExtender>
<asp:Label Text="" ID="lblGasCheckPopUp" runat="server" />
<asp:Panel ID="pnlScheduleChecksPopUp" runat="server" CssClass="modalPopupSchedular"
    Style="width: 410px; border-color: black; border-bottom-style: solid; border-width: 1px;">
    <asp:ImageButton ID="imgBtnGasCheckPopUp" runat="server" Style="position: absolute; top: -12px;
        right: -12px; width: 22px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
    <ucScheduleChecks:ScheduleChecksPopUp ID="ucScheduleChecks" runat="server"></ucScheduleChecks:ScheduleChecksPopUp>
</asp:Panel>
