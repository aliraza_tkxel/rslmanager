﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PDR_BusinessObject;
using PDR_BusinessLogic.Reports;
using System.Data;
using PDR_BusinessObject.Reports;
using PDR_Utilities.Helpers;
using AjaxControlToolkit;
using PDR_Utilities.Managers;
using PDR_BusinessObject.PageSort;
using PDR_DataAccess.Reports;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PropertyDataRestructure.Interface;
using PropertyDataRestructure.Base;
using PDR_Utilities.Constants;

namespace PropertyDataRestructure.UserControls.Reports
{
    public partial class VoidAnalysis : UserControlBase, IListingPage
    {
        #region Properties
        ReportsBL objReportsBL = new ReportsBL(new ReportsRepo());
        PageSortBO objPageSortBo = new PageSortBO("DESC", "PROPERTYID", 1, 10);
        public int rowCount = 0;
       
        #endregion

        #region Events
        #region PageLoad Event
        /// <summary>
        /// PageLoad Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        #endregion

        #region grdVoidDetail Row Data Bound Event
        /// <summary>
        /// grdVoidDetail Row Data Bound Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdVoidDetail_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (e.Row.RowIndex + 1 == rowCount)
                    {
                        // e.Row.Attributes.Add(" style='border-top", " 1px solid #000'");
                        e.Row.Style.Add("border-top", "1px solid #000");
                        LinkButton lnkBtnTotal = (LinkButton)e.Row.FindControl("lnkBtnTotal");
                        LinkButton lnkBtnLessThen3Weeks = (LinkButton)e.Row.FindControl("lnkBtnLessThen3Weeks");
                        LinkButton lnkLessThen6Weeks = (LinkButton)e.Row.FindControl("lnkLessThen6Weeks");
                        LinkButton lnkBtnLessTe6Month = (LinkButton)e.Row.FindControl("lnkBtnLessTe6Month");
                        LinkButton lnkBtnLessThen1Year = (LinkButton)e.Row.FindControl("lnkBtnLessThen1Year");
                        LinkButton lnkBtnMoreThen1Year = (LinkButton)e.Row.FindControl("lnkBtnMoreThen1Year");
                        LinkButton lnkBtnTermination = (LinkButton)e.Row.FindControl("lnkBtnTermination");
                        lnkBtnTotal.Enabled = false;
                        lnkBtnLessThen3Weeks.Enabled = false;
                        lnkLessThen6Weeks.Enabled = false;
                        lnkBtnLessTe6Month.Enabled = false;
                        lnkBtnLessThen1Year.Enabled = false;
                        lnkBtnMoreThen1Year.Enabled = false;
                        lnkBtnTermination.Enabled = false; 


                    }

                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }


        #endregion

        #region grdVoidDetail_RowCommand
        protected void grdVoidDetail_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                LinkButton lnkView = (LinkButton)e.CommandSource;
                //string propertyTypeId = lnkView.CommandArgument;
                ReportsBO objReportsBo = objSession.ReportsBo;
                string commandArgument = lnkView.CommandArgument;
                string[] argument = commandArgument.Split(';');
                if (argument[0].ToString() != null && argument[0].ToString() != "")
                {
                    objReportsBo.PropertyType = Convert.ToInt32(argument[0]);
                }
                    objReportsBo.Period = Convert.ToInt32(e.CommandName);
                lblBreadCrump.Text = lblBreadCrump.Text + " -> "  + argument[1].ToString();  
                showPropertyPopup(objReportsBo);
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region grdRentAnalysis RowDataBound
        /// <summary>
        /// grdRentAnalysis Row Data Bound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdRentAnalysis_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if ((e.Row.RowType == DataControlRowType.DataRow))
                {
                    e.Row.BackColor = System.Drawing.Color.White;

                    GridViewRow row = e.Row;
                    Label lblPropertyId = (Label)row.FindControl("lblProperty");
                    e.Row.Attributes.Add("onclick", string.Format("javascript:ShowPropertyDetail('{0}')", lblPropertyId.Text));
                    e.Row.Attributes.Add("onmouseover", "mouseIn(this);");
                    e.Row.Attributes.Add("onmouseout", "mouseOut(this);");
                    e.Row.Attributes.Add("onmouseover", "this.style.cursor='pointer'");

                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        #endregion

        #region btn print click event
        /// <summary>
        /// btn print click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnPrint_Click(object sender, EventArgs e)
        {

            try
            {
                printData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #region Change Page Number
        /// <summary>
        ///  Change Page Number
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>  
        protected void lnkbtnPager_Click(object sender, EventArgs e)
        {
            try
            {

                LinkButton pagebuttton = new LinkButton();
                pagebuttton = (LinkButton)sender;
                PageSortBO objPageSortBo = new PageSortBO("DESC", "ASSETTYPEID", 1, 10);
                GridHelper.processGridPager(sender, objPageSortBo, pageSortViewState);
                populatePropertesDetail(objSession.ReportsBo);
                //bindToGrid();
                Control ctl = this.Parent;
                ModalPopupExtender mpExtender = new ModalPopupExtender();
                mpExtender = (ModalPopupExtender)ctl.FindControl("mdlRentAnalysisPopup");
                mpExtender.Show();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        #endregion

        #region btnHidden Click
        /// <summary>
        /// btnHidden Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnHidden_Click(object sender, EventArgs e)
        {
            try
            {
                dynamic argument = Request["__EVENTARGUMENT"];

                Response.Redirect(PathConstants.PropertiesListPath + argument.ToString());
            }

            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion
        #region btnBack click Event
        /// <summary>
        /// btnBack click Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                if (objSession.showHidePopUp == showHideVoidPopUp.Properties.ToString())
                {
                    showFirstVoidPopup(objSession.StockReportsBo);
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        #endregion
        #endregion

        #region Function

        #region populate  Propertes Detail
        public void populatePropertesDetail(ReportsBO objReportsBO)
        {
            uiMessage.hideMessage(); 
            objSession.ReportsBo = objReportsBO;
            int totalCount = 0;
            PageSortBO objPageSortBo = new PageSortBO("DESC", "ASSETTYPEID", 1, 10);
            if (pageSortViewState != null)
            {
                objPageSortBo = pageSortViewState;
            }
            
            lblDate.Text = objReportsBO.ReportDate.ToShortDateString();
            DataSet resultDataSet = new DataSet();
            resultDataSet = objReportsBL.getVoidAnalysisReportDetail(objReportsBO, objPageSortBo, ref totalCount);

            grdRentAnalysis.DataSource = resultDataSet;
            grdRentAnalysis.DataBind();
            objPageSortBo.TotalRecords = totalCount;
            objPageSortBo.TotalPages = (int)Math.Ceiling(Convert.ToDecimal(totalCount) / objPageSortBo.PageSize);
            pnlPagination.Visible = false;
            if (objPageSortBo.TotalPages > 0)
            {
                pnlPagination.Visible = true;               
                GridHelper.setGridViewPager(ref pnlPagination, objPageSortBo);
            }
            pageSortViewState = objPageSortBo;
            printDivAssetTypeDetail.Visible = false;
            btnPrint.Visible = true;
            pnlVoidDetail.Visible = false;
            pnlPropertyDetail.Visible = true;
            objSession.showHidePopUp = showHideVoidPopUp.Properties.ToString();  
        }

        #endregion

        #region populate Void Analysis Report
        public void populateVoidAnalysisReport(ReportsBO objReportsBO)
        {
            uiMessage.hideMessage(); 
            if (objReportsBO.Header != null && objReportsBO.Header != string.Empty)
            {
                lblBreadCrump.Text = objReportsBO.Header + " -> " + objReportsBO.Asset;
            }
            else
            {
                lblBreadCrump.Text = objReportsBO.Asset;
            }
            lblDate.Text = objReportsBO.ReportDate.ToShortDateString();
            objSession.ReportsBo = objReportsBO;
            objSession.StockReportsBo = objReportsBO;
            DataSet resultDataSet = new DataSet();
            resultDataSet = objReportsBL.getVoidAnalysisReport(objReportsBO);
            rowCount = resultDataSet.Tables[0].Rows.Count;
            grdVoidDetail.DataSource = resultDataSet;
            grdVoidDetail.DataBind();
            pnlPropertyDetail.Visible = false;
            pnlVoidDetail.Visible = true;
            printDivAssetTypeDetail.Visible = true;
            btnPrint.Visible = false;
            pnlHeaderArea.Visible = true;
            objSession.showHidePopUp = showHideVoidPopUp.VoidDetail.ToString();
        }

        #endregion

        #region show Property
        private void showPropertyPopup(ReportsBO objReportsBo)
        {
            pnlHeaderArea.Visible = true;
            pageSortViewState = null;
            populatePropertesDetail(objReportsBo);
            Control ctl = this.Parent;
            ModalPopupExtender mpExtender = new ModalPopupExtender();
            mpExtender = (ModalPopupExtender)ctl.FindControl("mdlRentAnalysisPopup");
            mpExtender.Show();
        }

        #endregion

        #region show  AnalysisReport With Occupancy
        private void showFirstVoidPopup(ReportsBO objReportsBo)
        {

            populateVoidAnalysisReport(objReportsBo);
            Control ctl = this.Parent;
            ModalPopupExtender mpExtender = new ModalPopupExtender();
            mpExtender = (ModalPopupExtender)ctl.FindControl("mdlRentAnalysisPopup");
            mpExtender.Show();
        }

        #endregion
        #region currency Format
        public string currencyFormat(string value)
        {
            return GeneralHelper.currencyFormat(value);
        }

        #endregion
        #endregion

        #region IListingPage Implementation
        public void loadData()
        {
            throw new NotImplementedException();
        }

        public void searchData()
        {
            throw new NotImplementedException();
        }

        public void populateData()
        {
            throw new NotImplementedException();
        }

        public void printData()
        {
            grdRentAnalysis.AllowPaging = false;
            grdRentAnalysis.AllowSorting = false;
            int totalCount = 0;
            objPageSortBo = pageSortViewState;
            objPageSortBo.PageNumber = 1;
            objPageSortBo.PageSize = 65000;
            DataSet resultDataSet = new DataSet();  
            resultDataSet = objReportsBL.getVoidAnalysisReportDetail(objSession.ReportsBo, objPageSortBo, ref totalCount);
            grdRentAnalysis.DataSource = resultDataSet;
            grdRentAnalysis.DataBind();
            pnlPagination.Visible = false;
            pnlVoidDetail.Visible = false;
            pnlPropertyDetail.Visible = true;
            Control ctl = this.Parent;
            ModalPopupExtender mpExtender = new ModalPopupExtender();
            mpExtender = (ModalPopupExtender)ctl.FindControl("mdlRentAnalysisPopup");
            mpExtender.Show();
            pnlHeaderArea.Visible = false;
           // divPropertyDetail.Style.Add(" overflow", "auto");
            //divPropertyDetail.Style.Add("height", "400px");
            ScriptManager.RegisterClientScriptBlock(this.Page, typeof(UpdatePanel), Guid.NewGuid().ToString(), "PrintPage('" + divPropertyDetail.ClientID + "')", true);

        }

        public void exportGridToExcel()
        {
            throw new NotImplementedException();
        }

        public void exportToPdf()
        {
            throw new NotImplementedException();
        }

        public void downloadData()
        {
            throw new NotImplementedException();
        }

        public void applyFilters()
        {
            throw new NotImplementedException();
        }
        #endregion
        #region enum
        public enum showHideVoidPopUp
        {
            VoidDetail,
            Properties
        }
        #endregion
    }
}