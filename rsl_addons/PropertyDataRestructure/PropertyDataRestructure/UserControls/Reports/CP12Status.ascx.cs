﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PropertyDataRestructure.Base;
using PropertyDataRestructure.Interface;
using PDR_BusinessObject.PageSort;
using PDR_BusinessLogic.VoidInspections;
using System.Data;
using PDR_Utilities.Constants;
using PDR_DataAccess.VoidInspections;
using PDR_Utilities.Helpers;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_BusinessLogic.Reports;
using PDR_DataAccess.Reports;

namespace PropertyDataRestructure.UserControls.Reports
{
    public partial class CP12Status : UserControlBase, IListingPage
    {
        #region Properties

        PageSortBO objPageSortBo = new PageSortBO("DESC", "Termination", 1, 30);
        public int rowCount = 0;
        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            //ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            //scriptManager.RegisterPostBackControl(this.grdCP12Status);
        }


        #region Change Page Number
        /// <summary>
        ///  Change Page Number
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>  
        protected void lnkbtnPager_Click(object sender, EventArgs e)
        {
            try
            {
                PageSortBO objPageSortBo = new PageSortBO("DESC", "Ref", 1, 30);
                GridHelper.processGridPager(sender, objPageSortBo, pageSortViewState);
                loadData();

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        #endregion


        #region "grdCP12Status_Sorting Sorting"
        protected void grdCP12Status_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                objPageSortBo = pageSortViewState;
                objPageSortBo.SortExpression = e.SortExpression;
                objPageSortBo.PageNumber = 1;
                grdCP12Status.PageIndex = 0;
                objPageSortBo.setSortDirection();
                pageSortViewState = objPageSortBo;
                this.loadData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        protected void grdCP12Status_RowCreated(object sender, GridViewRowEventArgs e)
        {
            //check if it is a header row
            //since allowsorting is set to true, column names are added as command arguments to
            //the linkbuttons by DOTNET API
            if (e.Row.RowType == DataControlRowType.Header)
            {
                LinkButton btnSort;
                Image image;
                //iterate through all the header cells
                foreach (TableCell cell in e.Row.Cells)
                {
                    //check if the header cell has any child controls
                    if (cell.HasControls())
                    {
                        //get reference to the button column
                        btnSort = (LinkButton)cell.Controls[0];
                        image = new Image();
                        if (pageSortViewState != null)
                        {
                            if (btnSort.CommandArgument == pageSortViewState.SortExpression)
                            {
                                //following snippet figure out whether to add the up or down arrow
                                //based on the sortdirection
                                if (pageSortViewState.SortDirection == SortDirection.Ascending.ToString())
                                {
                                    image.ImageUrl = "~/Images/Grid/sort_asc.png";
                                }
                                else
                                {
                                    image.ImageUrl = "~/Images/Grid/sort_desc.png";
                                }
                            }
                            else
                            {
                                image.ImageUrl = "~/Images/Grid/sort_both.png";
                            }
                            cell.Controls.Add(image);
                        }
                    }
                }
            }
        }

        #region"change PageNumber"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void changePageNumber(object sender, EventArgs e)
        {
            try
            {
                Button btnGo = new Button();
                btnGo = (Button)sender;
                PageSortBO objPageSortBo;
                objPageSortBo = new PageSortBO("DESC", "Ref", 1, 30);
                objPageSortBo = pageSortViewState;
                int pageNumber = 1;
                pageNumber = Convert.ToInt32(txtPageNumber.Text);
                txtPageNumber.Text = String.Empty;
                objPageSortBo = pageSortViewState;
                if (((pageNumber >= 1)
                            && (pageNumber <= objPageSortBo.TotalPages)))
                {
                    objPageSortBo = pageSortViewState;
                    objPageSortBo.PageNumber = pageNumber;
                    pageSortViewState = objPageSortBo;
                    loadData();

                }
                else
                {
                    // uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, UserMessageConstants.InvalidPageNumber, true);
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #region "Export to Excel"

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            try
            {
                uiMessage.hideMessage();
                exportGridToExcel();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        public void exportGridToExcel()
        {
            try
            {
                //Get Filter Values for Exporting the Result Set

                string searchText = getSearchText();
                string propertyStatus = getPropertyStatusValue();

                PageSortBO objPageSortBo;
                if (pageSortViewState == null)
                {
                    objPageSortBo = new PageSortBO("DESC", "Type", 1, 65000);
                    pageSortViewState = objPageSortBo;
                }
                else
                {
                    objPageSortBo = pageSortViewState;
                    objPageSortBo.PageNumber = 1;
                    objPageSortBo.PageSize = 65000;
                }

                int totalCount = 0;

                ReportsBL objReportsBl = new ReportsBL(new ReportsRepo());
                DataSet resultDataSet = new DataSet();

                totalCount = objReportsBl.getCP12StatusReportReport(ref resultDataSet, objPageSortBo, propertyStatus, searchText);


                DataTable dt = resultDataSet.Tables[0];

                dt.Columns["Ref"].ColumnName = "Ref:";
                dt.Columns["Scheme"].ColumnName = "Scheme:";
                dt.Columns["Address"].ColumnName = "Address:";
                dt.Columns["Status"].ColumnName = "Status:";
                dt.Columns["Termination"].ColumnName = "Termination:";
                dt.Columns["Relet"].ColumnName = "Relet:";
                dt.Columns["ServiceAppointment"].ColumnName = "ServiceAppointment:";
                dt.Columns["CP12Issued"].ColumnName = "CP12Issued:";
                dt.Columns["CP12Renewal"].ColumnName = "CP12Renewal:";


                dt.Columns.Remove("CP12RenewalDate");
                dt.Columns.Remove("CP12IssuedDate");
                dt.Columns.Remove("TerminationDate");
                dt.Columns.Remove("ReletDate");
                dt.Columns.Remove("ServiceAppointmentDate");
                dt.Columns.Remove("LGSRID");
                dt.Columns.Remove("ValueDetail");
                dt.Columns.Remove("row");

                string fileName = "CP12_Status_Report_" + Convert.ToString(DateTime.Now.Day) + "_" + Convert.ToString(DateTime.Now.Month) + "_" + Convert.ToString(DateTime.Now.Year);
                ExportDownloadHelper.exportGridToExcel(fileName, dt);
            
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

            
        }



        #endregion




        #region IListingPage Implementation
        public void loadData()
        {
            uiMessage.hideMessage();

            string searchText = getSearchText();
            string propertyStatus = getPropertyStatusValue();

            PageSortBO objPageSortBo;
            if (pageSortViewState == null)
            {
                objPageSortBo = new PageSortBO("DESC", "Termination", 1, 30);
                pageSortViewState = objPageSortBo;
            }
            else
            {
                objPageSortBo = pageSortViewState;
            }

            int totalCount = 0;

            ReportsBL objReportsBl = new ReportsBL(new ReportsRepo());
            DataSet resultDataSet = new DataSet();

            totalCount = objReportsBl.getCP12StatusReportReport(ref resultDataSet, objPageSortBo, propertyStatus, searchText);


            grdCP12Status.DataSource = resultDataSet;
            grdCP12Status.DataBind();
            objPageSortBo.TotalRecords = totalCount;
            objPageSortBo.TotalPages = Convert.ToInt32(Math.Ceiling((Double)totalCount / objPageSortBo.PageSize));

            //if (objPageSortBo.TotalPages > 0)
            //{
            //    pnlPagination.Visible = true;
            //}
            //else
            //{
            //    pnlPagination.Visible = false;
            //    uiMessage.showErrorMessage(UserMessageConstants.RecordNotFound);
            //}
            pageSortViewState = objPageSortBo;
            GridHelper.setGridViewPager(ref pnlPagination, objPageSortBo);
        }


        public string getSearchText()
        {
            if (!(objSession.SearchText.Equals(string.Empty) || objSession.SearchText.Equals(null)))
            {
                return objSession.SearchText;
            }
            return null;
        }

        public string getPropertyStatusValue()
        {
            if (!(objSession.PropertyStatus.Equals(string.Empty) || objSession.PropertyStatus.Equals(null) || objSession.PropertyStatus.Equals("-1")))
            {
                return objSession.PropertyStatus;
            }
            return null;
        }
        public void searchData()
        {
            PageSortBO objPageSortBo = new PageSortBO("DESC", "Ref", 1, 30);
            pageSortViewState = objPageSortBo;
            loadData();
        }


        public void populateData()
        {
            throw new NotImplementedException();
        }

        public void printData()
        {
            throw new NotImplementedException();
        }

        public void exportToPdf()
        {
            throw new NotImplementedException();
        }

        public void downloadData()
        {
            throw new NotImplementedException();
        }

        public void applyFilters()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}