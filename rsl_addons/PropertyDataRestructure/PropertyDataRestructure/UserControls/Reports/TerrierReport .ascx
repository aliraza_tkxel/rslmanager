﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TerrierReport .ascx.cs"
    Inherits="PropertyDataRestructure.UserControls.Reports.TerrierReport" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<%@ Register TagName="Scheme" TagPrefix="ddlScheme" Src="~/UserControls/Common/SchemeDropDown.ascx" %>
<link href="../../Styles/Site.css" rel="stylesheet" media="screen" />
<link href="../../Styles/default.css" rel="stylesheet" />
<script type="text/javascript" charset="utf-8">

    function viewMap(pid) {

        window.open("https://www.google.co.uk/maps/place/" + pid + "", "Map" + pid, "width=500px,height=520px,left=20,top=20");
        return false;
    }

</script>
<style type="text/css">
    .terrier-Grid
    {
        float: left;
        margin: 0;
        padding: 0;
    }
    .terrier-Grid tr
    {
        font-size: 11px;
        font-family: Verdana, Arial, Helvetica;
    }
    .pdr-SelectDropDodwn
    {
        width: 200px !important;
    }
    .list-select select
    {
        border-radius: 0px;
        border: 1px solid #b1b1b1;
        height: 25px !important;
        font-size: 12px !important;
        width: 205px !important;
        margin: -3px 10px 0 0;
    }
    .styleselect{
        width:150px;
        padding:4.5px 8px !important;
        margin:-3px 0 0 0;
    }
    .dashboard, .dashboard th{
        padding: 0 !important;
        padding: 10px 0 10px 10px !important;
    }
    .dashboard th{
        background: #fff;
        border-bottom: 4px solid #8b8687;
        line-height: 0.8 !important;

    }
    .dashboard th a{
        color: #000 !important;
        font-size:12px !important;
    }
</style>
<asp:UpdatePanel ID="updPanelProperties" runat="server">
    <ContentTemplate>
        <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="500px"></uim:UIMessage>
        <div class="portlet">
            <div class="header">
                <asp:Label Text="Assets Register" CssClass="header-label" ID="lblHeading" runat="server" />
                <div class="form-control right">
                    <div class="field right" style="font-size: 12px !important; margin-right: 10px;">
                        <div class="right">
                            <asp:Panel ID="pnlSearch" runat="server" HorizontalAlign="Right" Style="float: left;">
                                <asp:TextBox ID="txtSearch" CssClass="styleselect styleselect-control" PlaceHolder="Quick Property Find"
                                    runat="server" AutoPostBack="false" UseSubmitBehavior="False" style="border-radius: 4px 0 0 4px;">
                                </asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtSearch"
                                    EnableClientScript="True" ErrorMessage="Special characters are not allowed" CssClass="Required"
                                    Display="Dynamic" ValidationExpression="^[A-Za-z0-9- ]+$" />
                            </asp:Panel>
                            <asp:Button Text="Go" ID="btnSearch" runat="server" CssClass="btn btn-xs btn-blue right" 
                                style="margin: -3px 0 0 0; min-width: 40px; border-radius: 0 4px 4px 0; padding: 3px 0 2.8px 0 !important;" OnClick="btnSearch_Click" />
                        </div>
                        <div class="right">
                            <asp:CheckBox ID="chkBoxIncludeSold" Text="Include Sold" runat="server" OnCheckedChanged="btnSearch_Click"
                                Style="float: left; margin-right: 10px;" />
                            <div class="list-select right">
                                <div class="field" style="margin:0">
                                    <ddlScheme:Scheme runat="server" ID="schemeDropDown" Style="padding: 5px; width:100px !important;"></ddlScheme:Scheme>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="portlet-body" style="padding-bottom:0">
                <div style="  width: 100%; padding:0">
                    <cc1:PagingGridView ID="grdRentAnalysis" runat="server" AllowPaging="false" AllowSorting="true"
                        AutoGenerateColumns="False" BorderStyle="Solid" BorderWidth="0px" CellPadding="4" OnRowCreated="grdRentAnalysis_RowCreated"
                        GridLines="None" PageSize="10" Width="100%" PagerSettings-Visible="false" OnSorting="grdRentAnalysis_Sorting"
                        HeaderStyle-Font-Underline="false" ShowHeaderWhenEmpty="true" CssClass="dashboard webgrid table table-responsive">
                        <Columns>
                            <asp:TemplateField HeaderText="Address:" SortExpression="ADDRESS">
                                <ItemTemplate>
                                    <asp:HyperLink runat="server" Target="_blank" NavigateUrl='<%# String.Format("/RSLApplianceServicing/Bridge.aspx?pid={0}", Eval("PROPERTYID")) %>'>
                                        <asp:Label ID="lblAddress" runat="server" Text='<%# Eval("ADDRESS") %>'></asp:Label>
                                    </asp:HyperLink>
                                </ItemTemplate>
                                <ItemStyle Width="15%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Town/City:" SortExpression="TownCITY">
                                <ItemTemplate>
                                    <asp:Label ID="lblTownCity" runat="server" Text='<%# Eval("TownCITY") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="5%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Postcode:" SortExpression="POSTCODE">
                                <ItemTemplate>
                                    <asp:HyperLink ID="lblPostcode" style = "cursor:pointer" runat="server" Text='<%# Eval("POSTCODE") %>' Target="_blank"
                                        ForeColor="blue" onClick='<%# String.Format("viewMap(\"{0}\")",Eval("POSTCODE").ToString())%>' />
                                </ItemTemplate>
                                <ItemStyle Width="8%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="EpcRating:" SortExpression="EpcRating">
                                <ItemTemplate>
                                    <asp:Label ID="lblEpcRating" runat="server" Text='<%# Eval("EpcRating") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="5%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Type:" SortExpression="TYPE">
                                <ItemTemplate>
                                    <asp:Label ID="lblType" runat="server" Text='<%# Eval("PROPTYPE") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="5%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="MV-VP:" SortExpression="OMV">
                                <ItemTemplate>
                                    <asp:Label ID="lblOMV" runat="server" Text='<%# currencyFormat(Eval("OMV").ToString())%>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="5%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="EUV-SH:" SortExpression="EUV">
                                <ItemTemplate>
                                    <asp:Label ID="lblEUV" runat="server" Text='<%# currencyFormat(Eval("EUV").ToString())%>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="5%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Rent:" SortExpression="RENT">
                                <ItemTemplate>
                                    <asp:Label ID="lblRent" runat="server" Text='<%# currencyFormat(Eval("RENT").ToString())%>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="5%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Yield:" SortExpression="YIELD">
                                <ItemTemplate>
                                    <asp:Label ID="lblYield" runat="server" Text='<%# checkYield(Eval("YIELD").ToString())%>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="5%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Annual Rent:" SortExpression="AnnualRent">
                                <ItemTemplate>
                                    <asp:Label ID="lblAnnualRent" runat="server" Text='<%# currencyFormat(Eval("AnnualRent").ToString())%>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="5%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status:" SortExpression="STATUS">
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" runat="server" Text='<%# Eval("STATUS") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="5%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Survey Date:" SortExpression="SurveyDate">
                                <ItemTemplate>
                                    <asp:Label ID="lblType" runat="server" Text='<%# Eval("SurveyDate").ToString() %>'></asp:Label>
                                    <!-- <asp:Label ID="Label1" runat="server" Text='<%# String.IsNullOrEmpty(Eval("SurveyDate").ToString()) ? "N/A" : String.Format("{0:dd/MM/yyyy}", Eval("SurveyDate")) %>'></asp:Label>-->
                                </ItemTemplate>
                                <ItemStyle Width="5%" />
                            </asp:TemplateField>  
                            <asp:TemplateField HeaderText="Decent Homes:" SortExpression="DecentHomes">
                                <ItemTemplate>
                                    <asp:Label ID="lblDecentHomes" runat="server" Text='<%# Eval("DecentHomes") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="5%" />
                            </asp:TemplateField>          
                            <asp:TemplateField HeaderText="Scheme Restrictions:" SortExpression="SchemeRestriction">
                                <ItemTemplate>
                                    <asp:Label ID="lblSchemeRestriction" runat="server" Text='<%# Eval("SchemeRestriction") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="5%" />
                            </asp:TemplateField>  
                            <asp:TemplateField HeaderText="Property Restrictions:" SortExpression="PropertyRestriction">
                                <ItemTemplate>
                                    <asp:Label ID="lblPropertyRestriction" runat="server" Text='<%# Eval("PropertyRestriction") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="5%" />
                            </asp:TemplateField> 
                                      
                            <%--                    <asp:TemplateField HeaderText="Map:">
                                <ItemTemplate>
                                    <asp:Button Text="View" ID="btnView" runat="server" UseSubmitBehavior="false" OnClientClick='<%# String.Format("viewMap(\"{0}, {1}, {2}\")",Eval("ADDRESS").ToString(),Eval("TownCITY").ToString(),Eval("POSTCODE").ToString())%>' />
                                </ItemTemplate>
                                <ItemStyle Width="5%" />
                            </asp:TemplateField>--%>
                        </Columns>
                        <HeaderStyle BackColor="#ffffff" CssClass="table-head" Font-Bold="True" ForeColor="black"
                            HorizontalAlign="Left" Font-Underline="false" />
                    </cc1:PagingGridView>
                    <asp:Panel runat="server" ID="pnlPagination" Visible="false" Style="color: Black; margin: 0 auto; width: 98%;" CssClass="pnlPaging">
                        <div style="width: 100%; padding: 15px 0 30px 0px; text-align:center;">
                            <div class="paging-left">
                                <span style="padding-right:10px;">
                                    <asp:LinkButton ID="lnkbtnPagerFirst" runat="server" Text="" CommandName="Page" CommandArgument="First" cssClass="lnk-btn"
                                        OnClick="lnkbtnPager_Click">
                                        &lt;&lt;First
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkbtnPagerPrev" Text="Prev" runat="server" CommandName="Page" CommandArgument="Prev" cssClass="lnk-btn"
                                        OnClick="lnkbtnPager_Click">
                                        &lt;Prev
                                    </asp:LinkButton>
                                </span>
                                <span style="padding-right:10px;">
                                    <b>Page:</b>
                                    <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="currentPage" />
                                    of
                                    <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />.
                                </span>
                                <span style="padding-right:20px;">
                                    <b>Result:</b>
                                    <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                                    to
                                    <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                                    of
                                    <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                                </span>
                                <span style="padding-right:10px;">
                                    <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page" CommandArgument="Next" cssClass="lnk-btn"
                                        OnClick="lnkbtnPager_Click">
                                    
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page" CommandArgument="Last" cssClass="lnk-btn"
                                        OnClick="lnkbtnPager_Click">
                                        
                                    </asp:LinkButton>
                                </span>
                            </div>
                            <div style="float: right;">
                                <span>
                                    <asp:Button ID="btnExportToExcel" runat="server" Text="Export to XLS" UseSubmitBehavior="False"
                                        class="btn btn-xs btn-blue right" style="padding:1px 5px !important;" OnClick="btnExportToExcel_Click" />
                                </span>
                                <asp:RangeValidator ID="rangevalidatorPageNumber" runat="server" ErrorMessage="Enter a page between 1 and "
                                    ValidationGroup="pageNumber" MinimumValue="1" MaximumValue="1" ControlToValidate="txtPageNumber"
                                    Type="Integer" SetFocusOnError="True" CssClass="Required" Display="Dynamic" />
                                <div class="field" style="margin-right: 10px;">
                                    <asp:TextBox ID="txtPageNumber" runat="server" Width="40px" ValidationGroup="pageNumber" PlaceHolder="Page"
                                    onchange="document.getElementById('txtPageNumberHidden').value = this.value;" />
                                </div>
                                <span>
                                    <asp:Button Text="Go" runat="server" ID="btnGoPageNumber" ValidationGroup="pageNumber" UseSubmitBehavior="false"
                                        class="btn btn-xs btn-blue" style="padding:1px 5px !important; margin-right: 10px;min-width:0px;" OnClick="changePageNumber" />
                                </span>
                                <%--<asp:RangeValidator ID="rangevalidatorPageNumber" runat="server" ErrorMessage="Enter a page between 1 and "
                                            ValidationGroup="pageNumber" MinimumValue="1" MaximumValue="1" ControlToValidate="txtPageNumber"
                                            Type="Integer" SetFocusOnError="True" CssClass="Required" />
                                        <asp:TextBox ID="txtPageNumber" runat="server" Width="25px" ValidationGroup="pageNumber"
                                            onchange="document.getElementById('txtPageNumberHidden').value = this.value;" />
                                        <asp:Button Text="Go" runat="server" ID="btnGoPageNumber" OnClick="changePageNumber"
                                            ValidationGroup="pageNumber" UseSubmitBehavior="false" />--%>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </div>
        </div>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnExportToExcel" />
    </Triggers>
</asp:UpdatePanel>
