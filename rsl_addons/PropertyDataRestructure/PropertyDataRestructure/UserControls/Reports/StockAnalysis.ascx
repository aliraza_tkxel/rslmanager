﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="StockAnalysis.ascx.cs"
    Inherits="PropertyDataRestructure.UserControls.Reports.StockAnalysis" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<link href="../../Styles/Site.css" rel="stylesheet" media="screen" />
<link href="../../Styles/default.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" charset="utf-8">

    function ShowPropertyDetail(pid) {

        __doPostBack('<%= btnHidden.UniqueID %>', pid);
    }
    //Function to change color of grid row on mouseover
    function mouseIn(row) {
        row.style.backgroundColor = '#e6e6e6';
    }
    //Function to change color of grid row on mouseout
    function mouseOut(row) {
        row.style.backgroundColor = '#FFFFFF';
    }
</script>
<asp:Button Text="" ID="btnHidden" runat="server" OnClick="btnHidden_Click" Style="display: none;" />
<asp:UpdatePanel ID="updPanelProperties" runat="server">
    <ContentTemplate>
        <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="500px" />
        <asp:Panel runat="server" ID="pnlHeaderArea">
            <div class="pdr-wrap-header">
                <div style="float: left; width: 100%; padding-left: 10px;">
                    Portfolio Stock Report ->
                    <asp:Label Text="" ID="lblBreadCrump" runat="server" />
                </div>
                <div style="float: left; width: 100%; padding-left: 10px;">
                    <div style="float: left; width: 30%;">
                        <div style="float: left;">
                            Date:<asp:Label Text="" ID="lblDate" runat="server" /></div>
                        <asp:CheckBox Text="Include Occupancy" runat="server" ID="chkOccupancy" AutoPostBack="true"
                            Style="float: right;" OnCheckedChanged="chkOccupancy_CheckedChanged" Visible="false"
                            Checked="true" />
                    </div>
                    <div style="float: right; width: 60%; text-align: right;">
                        <asp:Button Text="Back" ID="btnBack" runat="server" Style="background-color: White;
                            margin-right: 20px;" OnClick="btnBack_Click" />
                        <asp:Button Text="Print All" ID="btnPrint" runat="server" Style="background-color: White;
                            margin-right: 20px;" OnClick="btnPrint_Click" Visible="false" />
                        <div id="printDivAssetTypeDetail" runat="server" visible="false" style="float: right;">
                            <input type="button" id="btnPrintClient" value="Print" onclick="PrintPage('divAssetTypeDetail');"
                                style="background-color: White; float: right; margin-right: 20px;" />
                        </div>
                        <div id="printDivAssetTypeOccupancy" runat="server" visible="false" style="float: right;">
                            <input type="button" id="Button3" value="Print" onclick="PrintPage('divAssetTypeOccupancy');"
                                style="background-color: White; float: right; margin-right: 20px;" />
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlAssetType">
            <div class="pdr-wrap-Rentgrid-AssetTypeDetail" id="divAssetTypeDetail">
                <cc1:PagingGridView ID="grdAssetType" runat="server" AllowPaging="false" AllowSorting="false"
                    AutoGenerateColumns="False" BorderStyle="Solid" BorderWidth="0px" CellPadding="4"
                    GridLines="None" OrderBy="" PageSize="30" Width="95%" PagerSettings-Visible="false"
                    OnRowDataBound="grdAssetType_RowDataBound" OnRowCommand="grdAssetType_RowCommand"
                    EmptyDataText="No Record Found" ShowHeaderWhenEmpty="true">
                    <Columns>
                        <asp:TemplateField HeaderText="Property type:" ItemStyle-Width="70px">
                            <ItemTemplate>
                                <asp:Label ID="lblBlockName" runat="server" Text='<%# Bind("DESCRIPTION") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="10%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Total Count:" ItemStyle-Width="70px">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkBtnTotal" runat="server" CausesValidation="False" CommandArgument='<%#Eval("PROPERTYTYPEID").ToString()+";"+Eval("DESCRIPTION").ToString() %>'
                                    Text='<%# Bind("TOTAL") %>' CommandName="0"></asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle Width="10%" HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Let:">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkBtnLet" runat="server" CausesValidation="False" CommandArgument='<%#Eval("PROPERTYTYPEID").ToString()+";"+Eval("DESCRIPTION").ToString() %>'
                                    Text='<%# Bind("LET") %>' CommandName="2"></asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle Width="10%" HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Available:">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkBtnAvailable" runat="server" CausesValidation="False" CommandArgument='<%#Eval("PROPERTYTYPEID").ToString()+";"+Eval("DESCRIPTION").ToString() %>'
                                    Text='<%# Bind("AVAILABLE") %>' CommandName="1"></asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle Width="10%" HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Unavailable:">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkBtnUnAvailable" runat="server" CausesValidation="False" CommandArgument='<%#Eval("PROPERTYTYPEID").ToString()+";"+Eval("DESCRIPTION").ToString() %>'
                                    Text='<%# Bind("UNAVAILABLE") %>' CommandName="4"></asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle Width="10%" HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <HeaderStyle BackColor="#ffffff" CssClass="table-head" Font-Bold="True" ForeColor="black"
                        HorizontalAlign="Center" />
                </cc1:PagingGridView>
            </div>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlAssetTypeOccupancy">
            <div class="pdr-wrap-Rentgrid-AssetTypeDetail" id="divAssetTypeOccupancy">
                <cc1:PagingGridView ID="grdAssetTypeOccupancy" runat="server" AllowPaging="false"
                    AllowSorting="false" AutoGenerateColumns="False" BorderStyle="Solid" BorderWidth="0px"
                    CellPadding="4" GridLines="None" OrderBy="" PageSize="30" Width="100%" PagerSettings-Visible="false"
                    OnRowCommand="grdAssetTypeOccupancy_RowCommand" OnRowDataBound="grdAssetType_RowDataBound"
                    EmptyDataText="No Record Found" ShowHeaderWhenEmpty="true">
                    <Columns>
                        <asp:TemplateField HeaderText="Pers/Bed:" ItemStyle-Width="70px">
                            <ItemTemplate>
                                <asp:Label ID="lblBlockName" runat="server" Text='<%#Eval("OCCUPANCY").ToString()+""+Eval("BED").ToString() %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="10%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Total Count:" ItemStyle-Width="70px">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkBtnTotal" runat="server" CausesValidation="False" CommandArgument='<%#Eval("OCCUPANCY").ToString()+";"+Eval("BED").ToString() %>'
                                    Text='<%# Bind("TOTAL") %>' CommandName="0"></asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle Width="10%" HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Let:">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkBtnLet" runat="server" CausesValidation="False" CommandArgument='<%#Eval("OCCUPANCY").ToString()+";"+Eval("BED").ToString() %>'
                                    Text='<%# Bind("LET") %>' CommandName="2"></asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle Width="10%" HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Available:">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkBtnAvailable" runat="server" CausesValidation="False" CommandArgument='<%#Eval("OCCUPANCY").ToString()+";"+Eval("BED").ToString() %>'
                                    Text='<%# Bind("AVAILABLE") %>' CommandName="1"></asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle Width="10%" HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Unavailable:">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkBtnUnAvailable" runat="server" CausesValidation="False" CommandArgument='<%#Eval("OCCUPANCY").ToString()+";"+Eval("BED").ToString() %>'
                                    Text='<%# Bind("UNAVAILABLE") %>' CommandName="4"></asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle Width="10%" HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <HeaderStyle BackColor="#ffffff" CssClass="table-head" Font-Bold="True" ForeColor="black"
                        HorizontalAlign="Left" />
                </cc1:PagingGridView>
                <cc1:PagingGridView ID="grdAssetTypeWithOutOccupancy" runat="server" AllowPaging="false"
                    AllowSorting="false" AutoGenerateColumns="False" BorderStyle="Solid" BorderWidth="0px"
                    CellPadding="4" GridLines="None" OrderBy="" PageSize="30" Width="100%" PagerSettings-Visible="false"
                    OnRowCommand="grdAssetTypeWithOutOccupancy_RowCommand" OnRowDataBound="grdAssetType_RowDataBound"
                    EmptyDataText="No Record Found" ShowHeaderWhenEmpty="true">
                    <Columns>
                        <asp:TemplateField HeaderText="Bed:" ItemStyle-Width="70px">
                            <ItemTemplate>
                                <asp:Label ID="lblBlockName" runat="server" Text='<%# Bind("BED") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="10%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Total Count:" ItemStyle-Width="70px">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkBtnTotal" runat="server" CausesValidation="False" CommandArgument='<%#Eval("BED") %>'
                                    Text='<%# Bind("TOTAL") %>' CommandName="0"></asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle Width="10%" HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Let:">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkBtnLet" runat="server" CausesValidation="False" CommandArgument='<%#Eval("BED") %>'
                                    Text='<%# Bind("LET") %>' CommandName="2"></asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle Width="10%" HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Available:">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkBtnAvailable" runat="server" CausesValidation="False" CommandArgument='<%#Eval("BED") %>'
                                    Text='<%# Bind("AVAILABLE") %>' CommandName="1"></asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle Width="10%" HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Unavailable:">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkBtnUnAvailable" runat="server" CausesValidation="False" CommandArgument='<%#Eval("BED") %>'
                                    Text='<%# Bind("UNAVAILABLE") %>' CommandName="4"></asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle Width="10%" HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <HeaderStyle BackColor="#ffffff" CssClass="table-head" Font-Bold="True" ForeColor="black"
                        HorizontalAlign="Left" />
                </cc1:PagingGridView>
            </div>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlPropertyDetail">
            <div class="pdr-wrap-Rentgrid-AssetTypeDetail">
                <div class="pdr-wrap-PropertyDetailGrid" runat="server" id="divPropertyDetail">
                    <cc1:PagingGridView ID="grdRentAnalysis" runat="server" AllowPaging="false" AllowSorting="false"
                        AutoGenerateColumns="False" BorderStyle="Solid" BorderWidth="0px" CellPadding="4"
                        EmptyDataText="No Record Found" ShowHeaderWhenEmpty="true" GridLines="None" PageSize="10"
                        Width="100%" PagerSettings-Visible="false" OnRowDataBound="grdRentAnalysis_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="Property Ref:" ItemStyle-Width="70px" HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Label ID="lblProperty" runat="server" Text='<%# Bind("PROPERTYID") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="10%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Property Address:" HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("ADDRESS") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="20%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status:" HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Label ID="lblRent" runat="server" Text='<%# Bind("STATUSDESCRIPTION") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="5%" HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Substatus:" HeaderStyle-HorizontalAlign="Left">
                                <ItemTemplate>
                                    <asp:Label ID="lblServices" runat="server" Text='<%# Bind("SUBSTATUSDESCRIPTION") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="5%" HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <HeaderStyle BackColor="#ffffff" CssClass="table-head" Font-Bold="True" ForeColor="black"
                            HorizontalAlign="Left" />
                    </cc1:PagingGridView>
                    <br />
                    <div style="width: 100%; border-top: 1px solid;">
                        <asp:Panel runat="server" ID="pnlPagination" Visible="true" Style="width: 60%; margin: 0 auto;
                            padding: 5px 0; color: Black; vertical-align: middle;">
                            <table class="acenter">
                                <tbody>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkbtnPagerFirst" Text="" runat="server" CommandName="Page" CommandArgument="First"
                                                OnClick="lnkbtnPager_Click" Style="text-decoration: none;">&lt;&lt;First</asp:LinkButton>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lnkbtnPagerPrev" Text="" runat="server" CommandName="Page" CommandArgument="Prev"
                                                OnClick="lnkbtnPager_Click" Style="text-decoration: none;">&lt;Prev</asp:LinkButton>
                                            &nbsp;
                                        </td>
                                        <td>
                                            Page:&nbsp;
                                            <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="currentPage" />
                                            of&nbsp;
                                            <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />. Records:
                                            <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                                            to&nbsp;
                                            <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                                            of&nbsp;
                                            <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page"
                                                CommandArgument="Next" OnClick="lnkbtnPager_Click" Style="text-decoration: none;" />
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page"
                                                CommandArgument="Last" OnClick="lnkbtnPager_Click" Style="text-decoration: none;" />
                                            &nbsp;
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </asp:Panel>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
