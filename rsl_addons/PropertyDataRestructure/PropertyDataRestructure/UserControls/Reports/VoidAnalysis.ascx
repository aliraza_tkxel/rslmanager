﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VoidAnalysis.ascx.cs"
    Inherits="PropertyDataRestructure.UserControls.Reports.VoidAnalysis" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<link href="../../Styles/Site.css" rel="stylesheet" media="screen" />
<link href="../../Styles/default.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" charset="utf-8">

    function ShowPropertyDetail(pid) {

        __doPostBack('<%= btnHidden.UniqueID %>', pid);
    }
    //Function to change color of grid row on mouseover
    function mouseIn(row) {
        row.style.backgroundColor = '#e6e6e6';
    }
    //Function to change color of grid row on mouseout
    function mouseOut(row) {
        row.style.backgroundColor = '#FFFFFF';
    }
</script>
<asp:Button Text="" ID="btnHidden" runat="server" OnClick="btnHidden_Click" Style="display: none;" />
<asp:UpdatePanel ID="updPanelProperties" runat="server">
    <ContentTemplate>
        <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="500px" />
        <asp:Panel runat="server" ID="pnlHeaderArea">
            <div class="pdr-wrap-header">
                <div style="float: left; width: 100%; padding-left: 10px;">
                    Portfolio Stock Report ->
                    <asp:Label Text="" ID="lblBreadCrump" runat="server" />
                </div>
                <div style="float: left; width: 100%; padding-left: 10px;">
                    <div style="float: left; width: 30%;">
                        <div style="float: left;">
                            Date:<asp:Label Text="" ID="lblDate" runat="server" /></div>
                    </div>
                    <div style="float: right; width: 60%; text-align: right;">
                        <asp:Button Text="Back" ID="btnBack" runat="server" Style="background-color: White;
                            margin-right: 20px;" OnClick="btnBack_Click" />
                        <asp:Button Text="Print All" ID="btnPrint" runat="server" Style="background-color: White;
                            margin-right: 20px;" OnClick="btnPrint_Click" Visible="false" />
                        <div id="printDivAssetTypeDetail" runat="server" visible="false" style="float: right;">
                            <input type="button" id="btnPrintClient" value="Print" onclick="PrintPage('divVoidDetail');"
                                style="background-color: White; float: right; margin-right: 20px;" />
                        </div>
                    </div>
                </div>
            </div>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlVoidDetail">
            <div class="pdr-wrap-Rentgrid-AssetTypeDetail" id="divVoidDetail">
                <cc1:PagingGridView ID="grdVoidDetail" runat="server" AllowPaging="false" AllowSorting="false"
                    AutoGenerateColumns="False" BorderStyle="Solid" BorderWidth="0px" CellPadding="4"
                    GridLines="None" OrderBy="" PageSize="30" Width="100%" PagerSettings-Visible="false"
                    OnRowDataBound="grdVoidDetail_RowDataBound" OnRowCommand="grdVoidDetail_RowCommand"
                    EmptyDataText="No Record Found" ShowHeaderWhenEmpty="true">
                    <Columns>
                        <asp:TemplateField HeaderText="Asset type:" ItemStyle-Width="70px">
                            <ItemTemplate>
                                <asp:Label ID="lblPropertyType" runat="server" Text='<%# Bind("DESCRIPTION") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="10%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Total Count:" ItemStyle-Width="70px">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkBtnTotal" runat="server" CausesValidation="False" CommandArgument='<%#Eval("ASSETTYPEID").ToString()+";"+Eval("DESCRIPTION").ToString() %>'
                                    Text='<%# Bind("TOTALPROPERTYCOUNT") %>' CommandName="0"></asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle Width="10%" HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="&lt;3 weeks	:">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkBtnLessThen3Weeks" runat="server" CausesValidation="False"
                                    CommandArgument='<%#Eval("ASSETTYPEID").ToString()+";"+Eval("DESCRIPTION").ToString() %>'
                                    Text='<%# Bind("UNDER3WEEKS") %>' CommandName="2"></asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle Width="10%" HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="3 weeks to &lt;6 weeks:">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkLessThen6Weeks" runat="server" CausesValidation="False" CommandArgument='<%#Eval("ASSETTYPEID").ToString()+";"+Eval("DESCRIPTION").ToString() %>'
                                    Text='<%# Bind("MORETHAN3WEEKSLESSTHAN6WEEKS") %>' CommandName="3"></asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle Width="10%" HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="6 weeks to &lt;6 months:">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkBtnLessTe6Month" runat="server" CausesValidation="False" CommandArgument='<%#Eval("ASSETTYPEID").ToString()+";"+Eval("DESCRIPTION").ToString() %>'
                                    Text='<%# Bind("MORETHAN6WEEKSLESSTHAN6MONTHS") %>' CommandName="4"></asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle Width="10%" HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="6 months to &lt;6 year:">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkBtnLessThen1Year" runat="server" CausesValidation="False"
                                    CommandArgument='<%#Eval("ASSETTYPEID").ToString()+";"+Eval("DESCRIPTION").ToString() %>'
                                    Text='<%# Bind("MORETHAN6MONTHSLESSTHAN1YEAR") %>' CommandName="5"></asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle Width="10%" HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="1 year +:">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkBtnMoreThen1Year" runat="server" CausesValidation="False"
                                    CommandArgument='<%#Eval("ASSETTYPEID").ToString()+";"+Eval("DESCRIPTION").ToString() %>'
                                    Text='<%# Bind("MORETHAN1YEAR") %>' CommandName="6"></asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle Width="10%" HorizontalAlign="Center" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Pending Termination:">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkBtnTermination" runat="server" CausesValidation="False" CommandArgument='<%#Eval("ASSETTYPEID").ToString()+";"+Eval("DESCRIPTION").ToString() %>'
                                    Text='<%# Bind("PENDINGTERMINATIONS") %>' CommandName="1"></asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle Width="10%" HorizontalAlign="Center" />
                        </asp:TemplateField>
                    </Columns>
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <HeaderStyle BackColor="#ffffff" CssClass="table-head" Font-Bold="True" ForeColor="black"
                        HorizontalAlign="Left" />
                </cc1:PagingGridView>
            </div>
        </asp:Panel>
        <asp:Panel runat="server" ID="pnlPropertyDetail">
            <div class="pdr-wrap-Rentgrid-AssetTypeDetail">
                <div class="pdr-wrap-PropertyDetailGrid" runat="server" id="divPropertyDetail">
                    <cc1:PagingGridView ID="grdRentAnalysis" runat="server" AllowPaging="false" AllowSorting="false"
                        AutoGenerateColumns="False" BorderStyle="Solid" BorderWidth="0px" CellPadding="4"
                        GridLines="None" PageSize="10" Width="100%" PagerSettings-Visible="false" OnRowDataBound="grdRentAnalysis_RowDataBound"
                        EmptyDataText="No Record Found" ShowHeaderWhenEmpty="true">
                        <Columns>
                            <asp:TemplateField HeaderText="Scheme:" ItemStyle-Width="70px">
                                <ItemTemplate>
                                    <asp:Label ID="lblScheme" runat="server" Text='<%# Bind("SCHEMENAME") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="15%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Property Ref:" ItemStyle-Width="70px">
                                <ItemTemplate>
                                    <asp:Label ID="lblProperty" runat="server" Text='<%# Bind("PROPERTYID") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="10%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Property Address:">
                                <ItemTemplate>
                                    <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("ADDRESS") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="25%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Void From:">
                                <ItemTemplate>
                                    <asp:Label ID="lblVoidFrom" runat="server" Text='<%# Bind("ENDDATE") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="5%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="No of Days Void:">
                                <ItemTemplate>
                                    <asp:Label ID="lblNoOfDaysVoid" runat="server" Text='<%# Bind("NUMBOFDAYSVOID") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="10%" HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Total Rent:">
                                <ItemTemplate>
                                    <asp:Label ID="lblTotalRent" runat="server" Text='<%# currencyFormat(Eval("TOTALRENT").ToString()) %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="10%" HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Accum Rent Loss:">
                                <ItemTemplate>
                                    <asp:Label ID="lblAccumRentLoss" runat="server" Text='<%# currencyFormat(Eval("ACCUMULATEDRENT").ToString()) %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="10%" HorizontalAlign="Center"/>
                            </asp:TemplateField>
                        </Columns>
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <HeaderStyle BackColor="#ffffff" CssClass="table-head" Font-Bold="True" ForeColor="black"
                            HorizontalAlign="Left" />
                    </cc1:PagingGridView>
                    <br />
                    <div style="width: 100%; border-top: 1px solid;">
                        <asp:Panel runat="server" ID="pnlPagination" Visible="true" Style="width: 60%; margin: 0 auto;
                            padding: 5px 0; color: Black; vertical-align: middle;">
                            <table class="acenter">
                                <tbody>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkbtnPagerFirst" Text="" runat="server" CommandName="Page" CommandArgument="First"
                                                OnClick="lnkbtnPager_Click" Style="text-decoration: none;">&lt;&lt;First</asp:LinkButton>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lnkbtnPagerPrev" Text="" runat="server" CommandName="Page" CommandArgument="Prev"
                                                OnClick="lnkbtnPager_Click" Style="text-decoration: none;">&lt;Prev</asp:LinkButton>
                                            &nbsp;
                                        </td>
                                        <td>
                                            Page:&nbsp;
                                            <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="currentPage" />
                                            of&nbsp;
                                            <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />. Records:
                                            <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                                            to&nbsp;
                                            <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                                            of&nbsp;
                                            <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page"
                                                CommandArgument="Next" OnClick="lnkbtnPager_Click" Style="text-decoration: none;" />
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page"
                                                CommandArgument="Last" OnClick="lnkbtnPager_Click" Style="text-decoration: none;" />
                                            &nbsp;
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </asp:Panel>
                    </div>
                </div>
            </div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
