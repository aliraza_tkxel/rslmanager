﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PropertyDataRestructure.Base;
using PropertyDataRestructure.Interface;
using PDR_BusinessObject.PageSort;
using PDR_Utilities.Helpers;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_BusinessLogic.VoidInspections;
using PDR_DataAccess.VoidInspections;
using System.Data;
using PDR_Utilities.Constants;
using PDR_Utilities.Enums;
using System.Threading;

namespace PropertyDataRestructure.UserControls.Reports
{
    public partial class VoidNoEntry : UserControlBase, IListingPage
    {
        #region Properties

        PageSortBO objPageSortBo = new PageSortBO("DESC", "ADDRESS", 1, 30);
        public int rowCount = 0;
        #endregion

        #region PageLoad Event
        /// <summary>
        /// PageLoad Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                this.ucArrangeInspectionPopUp.delegateRefreshParentGrid = new Action(loadData);
                this.ucArrangePostInspectionPopUp.delegateRefeshParentGrid = new Action(loadData);
                this.ucScheduleChecks.delegateRefreshParentChecksGrid = new Action(loadData);
                if (!IsPostBack)
                {
                    uiMessage.hideMessage();
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region Change Page Number
        /// <summary>
        ///  Change Page Number
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>  
        protected void lnkbtnPager_Click(object sender, EventArgs e)
        {
            try
            {
                PageSortBO objPageSortBo = new PageSortBO("DESC", "ADDRESS", 1, 30);
                GridHelper.processGridPager(sender, objPageSortBo, pageSortViewState);
                this.loadData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        #endregion

        protected void grdNoEntry_RowCreated(object sender, GridViewRowEventArgs e)
        {
            //check if it is a header row
            //since allowsorting is set to true, column names are added as command arguments to
            //the linkbuttons by DOTNET API
            if (e.Row.RowType == DataControlRowType.Header)
            {
                LinkButton btnSort;
                Image image;
                //iterate through all the header cells
                foreach (TableCell cell in e.Row.Cells)
                {
                    //check if the header cell has any child controls
                    if (cell.HasControls())
                    {
                        //get reference to the button column
                        btnSort = (LinkButton)cell.Controls[0];
                        image = new Image();
                        if (pageSortViewState != null)
                        {
                            if (btnSort.CommandArgument == pageSortViewState.SortExpression)
                            {
                                //following snippet figure out whether to add the up or down arrow
                                //based on the sortdirection
                                if (pageSortViewState.SortDirection == SortDirection.Ascending.ToString())
                                {
                                    image.ImageUrl = "~/Images/Grid/sort_asc.png";
                                }
                                else
                                {
                                    image.ImageUrl = "~/Images/Grid/sort_desc.png";
                                }
                            }
                            else
                            {
                                image.ImageUrl = "~/Images/Grid/sort_both.png";
                            }
                            cell.Controls.Add(image);
                        }
                    }
                }
            }
        }

        #region"change PageNumber"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void changePageNumber(object sender, EventArgs e)
        {
            try
            {
                Button btnGo = new Button();
                btnGo = (Button)sender;
                PageSortBO objPageSortBo;
                objPageSortBo = new PageSortBO("DESC", "ADDRESS", 1, 30);
                objPageSortBo = pageSortViewState;
                int pageNumber = 1;
                pageNumber = Convert.ToInt32(txtPageNumber.Text);
                txtPageNumber.Text = String.Empty;
                objPageSortBo = pageSortViewState;
                if (((pageNumber >= 1)
                            && (pageNumber <= objPageSortBo.TotalPages)))
                {
                    objPageSortBo = pageSortViewState;
                    objPageSortBo.PageNumber = pageNumber;
                    pageSortViewState = objPageSortBo;
                    loadData();

                }
                else
                {
                    // uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, UserMessageConstants.InvalidPageNumber, true);
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #region "grdNoEntry Sorting"
        protected void grdNoEntry_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                objPageSortBo = pageSortViewState;
                objPageSortBo.SortExpression = e.SortExpression;
                objPageSortBo.PageNumber = 1;
                grdNoEntry.PageIndex = 0;
                objPageSortBo.setSortDirection();
                pageSortViewState = objPageSortBo;
                this.loadData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #region Button Schedule Event Handler
        /// <summary>
        /// Button Schedule Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void imgBtnSchedule_Click(Object sender, EventArgs e)
        {
            try
            {
                Button btnSchedule = (Button)sender;
                bool Legionella = false;
                int journalId = Convert.ToInt32(btnSchedule.CommandArgument);
                GridViewRow row = (sender as Button).NamingContainer as GridViewRow;
                Label lblAppointmentType = row.FindControl("lblAppointmentType") as Label;
                var hdnLegionella = row.FindControl("hdnLegionella") as HiddenField;
                if (hdnLegionella.Value != null && hdnLegionella.Value != "")
                {
                    Legionella = Convert.ToBoolean(hdnLegionella.Value);
                }
                if (lblAppointmentType.Text.Replace(" ", "") == PDR_Utilities.Enums.Enums.VoidAppointmentTypes.VoidInspection.ToString())
                {
                    ucArrangeInspectionPopUp.populatePreValues(journalId, Legionella, string.Empty, true);
                    ucArrangePostInspectionPopUp.Visible = false;
                    mdlpopupInspection.Show();
                }
                else if (lblAppointmentType.Text.Replace(" ", "") == PDR_Utilities.Enums.Enums.VoidAppointmentTypes.PostVoidInspection.ToString())
                {
                    ucArrangePostInspectionPopUp.populatePreValues(journalId, string.Empty, true);
                    ucArrangePostInspectionPopUp.Visible = true;
                    ucArrangeInspectionPopUp.Visible = false;
                    mdlpopupInspection.Show();
                }
                else if (lblAppointmentType.Text.Replace(" ", "") == PDR_Utilities.Enums.Enums.VoidAppointmentTypes.VoidGasCheck.ToString()
                    || lblAppointmentType.Text.Replace(" ", "") == PDR_Utilities.Enums.Enums.VoidAppointmentTypes.VoidElectricCheck.ToString())
                {
                    ucScheduleChecks.populatePreValues(journalId, lblAppointmentType.Text, string.Empty, true);
                    mdlpopupArrangeGasElectricCheck.Show();
                }
                else if (lblAppointmentType.Text.Replace(" ", "") == PDR_Utilities.Enums.Enums.VoidAppointmentTypes.VoidWorks.ToString())
                {

                    this.navigateToReScheduleWorksRequired(journalId);
                }
            }
            catch (ThreadAbortException ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region IListingPage Implementation
        public void loadData()
        {
            uiMessage.hideMessage();
            string searchText = objSession.SearchText;

            PageSortBO objPageSortBo;
            if (pageSortViewState == null)
            {
                objPageSortBo = new PageSortBO("DESC", "JournalId", 1, 30);
                pageSortViewState = objPageSortBo;
            }
            else
            {
                objPageSortBo = pageSortViewState;
            }

            int totalCount = 0;
            VoidInspectionsBL objVoidInspectionsBl = new VoidInspectionsBL(new VoidInspectionsRepo());
            DataSet resultDataSet = new DataSet();
            totalCount = objVoidInspectionsBl.getNoEntryCountAndList(ref resultDataSet, objPageSortBo, searchText);
            grdNoEntry.DataSource = resultDataSet;
            grdNoEntry.DataBind();
            objPageSortBo.TotalRecords = totalCount;
            objPageSortBo.TotalPages = Convert.ToInt32(Math.Ceiling((Double)totalCount / objPageSortBo.PageSize));

            //if (objPageSortBo.TotalPages > 0)
            //{
            //    pnlPagination.Visible = true;
            //}
            //else
            //{
            //    pnlPagination.Visible = false;
            //    uiMessage.showErrorMessage(UserMessageConstants.RecordNotFound);
            //}
            pageSortViewState = objPageSortBo;
            GridHelper.setGridViewPager(ref pnlPagination, objPageSortBo);
        }

        public void searchData()
        {
            PageSortBO objPageSortBo = new PageSortBO("DESC", "JournalId", 1, 30);
            pageSortViewState = objPageSortBo;
            loadData();
        }
        public void populateData()
        {
            throw new NotImplementedException();
        }

        public void printData()
        {
            throw new NotImplementedException();
        }

        public void exportGridToExcel()
        {
            throw new NotImplementedException();
        }

        public void exportToPdf()
        {
            throw new NotImplementedException();
        }

        public void downloadData()
        {
            throw new NotImplementedException();
        }

        public void applyFilters()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Navigate to ReSchedule Required Works page
        /// <summary>
        /// Navigate to ReSchedule Required Works page
        /// </summary>
        /// <remarks></remarks>
        public void navigateToReScheduleWorksRequired(int jid)
        {
            Response.Redirect(PathConstants.ReScheduleRequiredWorks + "?jid=" + jid.ToString());
        }
        #endregion
    }
}