﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PDR_BusinessObject;
using PDR_BusinessLogic.Reports;
using System.Data;
using PDR_BusinessObject.Reports;
using PDR_Utilities.Helpers;
using AjaxControlToolkit;
using PDR_Utilities.Managers;
using PDR_BusinessObject.PageSort;
using PDR_DataAccess.Reports;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PropertyDataRestructure.Base;
using PropertyDataRestructure.Interface;
using PDR_Utilities.Constants;

namespace PropertyDataRestructure.UserControls.Reports
{
    public partial class StockAnalysis : UserControlBase, IListingPage
    {
        #region Properties
        ReportsBL objReportsBL = new ReportsBL(new ReportsRepo());
        PageSortBO objPageSortBo = new PageSortBO("DESC", "PROPERTYID", 1, 10);
        public int rowCount = 0;
        #endregion


        #region Events
        #region PageLoad Event
        /// <summary>
        /// PageLoad Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        #endregion

        #region grdAssetType Row Data Bound Event
        /// <summary>
        /// grdAssetType Row Data Bound Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdAssetType_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                uiMessage.hideMessage();
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (e.Row.RowIndex + 1 == rowCount)
                    {
                        // e.Row.Attributes.Add(" style='border-top", " 1px solid #000'");
                        e.Row.Style.Add("border-top", "1px solid #000");
                        LinkButton lnkBtnTotal = (LinkButton)e.Row.FindControl("lnkBtnTotal");
                        //LinkButton lnkBtnLet = (LinkButton)e.Row.FindControl("lnkBtnLet");
                        //LinkButton lnkBtnAvailable = (LinkButton)e.Row.FindControl("lnkBtnAvailable");
                        //LinkButton lnkBtnUnAvailable = (LinkButton)e.Row.FindControl("lnkBtnUnAvailable"); 
                        lnkBtnTotal.Enabled = false;
                        //lnkBtnLet.Enabled = false;
                        //lnkBtnAvailable.Enabled = false;
                        //lnkBtnUnAvailable.Enabled = false;

                    }

                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }


        #endregion
        #region grdAssetType_RowCommand
        protected void grdAssetType_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                uiMessage.hideMessage();
                LinkButton lnkView = (LinkButton)e.CommandSource;

                string commandArgument = lnkView.CommandArgument;
                string[] argument = commandArgument.Split(';');
                ReportsBO objReportsBo = objSession.ReportsBo;
                if (argument[0].ToString() != null && argument[0].ToString() != "")
                {
                    objReportsBo.PropertyType = Convert.ToInt32(argument[0]);
                }
                lblBreadCrump.Text = lblBreadCrump.Text + " -> " + argument[1].ToString();
                objReportsBo.Status = null;
                if (e.CommandName != "0")
                {
                    objReportsBo.Status = Convert.ToInt32(e.CommandName);
                }

                showAnalysisPopup(objReportsBo);
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion


        #region grdAssetTypeOccupancy_RowCommand
        protected void grdAssetTypeOccupancy_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                uiMessage.hideMessage();
                ReportsBO objReportsBo = objSession.ReportsBo;
                LinkButton lnkView = (LinkButton)e.CommandSource;
                string commandArgument = lnkView.CommandArgument;
                string[] argument = commandArgument.Split(';');
                if (argument[0].ToString() != null && argument[0].ToString() != "")
                {
                    objReportsBo.Occupancy = argument[0].Remove(argument[0].IndexOf('p'));
                }
                if (argument[0].ToString() != null && argument[0].ToString() != "")
                {
                    objReportsBo.Beds = argument[1].Remove(argument[1].IndexOf('b'));
                }


                //rePopulateHeader(lblBreadCrump.Text);
                lblBreadCrump.Text = lblBreadCrump.Text + " -> " + argument[0].ToString() + argument[1].ToString();

                objReportsBo.Status = null;
                if (e.CommandName != "0")
                {
                    objReportsBo.Status = Convert.ToInt32(e.CommandName);
                }

                showPropertyPopup(objReportsBo);
                chkOccupancy.Visible = false;
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion
        #region grdAssetTypeWithOutOccupancy  RowCommand
        protected void grdAssetTypeWithOutOccupancy_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                uiMessage.hideMessage();
                LinkButton lnkView = (LinkButton)e.CommandSource;
                string propertyTypeId = lnkView.CommandArgument;
                ReportsBO objReportsBo = objSession.ReportsBo;
                if (propertyTypeId != "" && propertyTypeId != null)
                {
                    objReportsBo.Beds = propertyTypeId.Remove(propertyTypeId.IndexOf('b'));
                }
                lblBreadCrump.Text = rePopulateHeader(lblBreadCrump.Text) + " -> " + propertyTypeId.ToString();

                //if (objReportsBo.Beds == "?")
                //{
                //    objReportsBo.Beds = null;
                //}
                objReportsBo.Status = null;
                if (e.CommandName != "0")
                {
                    objReportsBo.Status = Convert.ToInt32(e.CommandName);
                }

                showPropertyPopup(objReportsBo);
                chkOccupancy.Visible = false;
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region Change Page Number
        /// <summary>
        ///  Change Page Number
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>  
        protected void lnkbtnPager_Click(object sender, EventArgs e)
        {
            try
            {
                uiMessage.hideMessage();
                LinkButton pagebuttton = new LinkButton();
                pagebuttton = (LinkButton)sender;
                PageSortBO objPageSortBo = new PageSortBO("DESC", "PROPERTYID", 1, 10);
                GridHelper.processGridPager(sender, objPageSortBo, pageSortViewState);
                populatePropertesDetail(objSession.ReportsBo);
                //bindToGrid();
                Control ctl = this.Parent;
                ModalPopupExtender mpExtender = new ModalPopupExtender();
                mpExtender = (ModalPopupExtender)ctl.FindControl("mdlRentAnalysisPopup");
                mpExtender.Show();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        #endregion

        #region btn Print Click event
        /// <summary>
        /// btn Print Click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {
                printData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region chkOccupancy CheckedChanged
        /// <summary>
        /// chkOccupancy CheckedChanged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void chkOccupancy_CheckedChanged(object sender, EventArgs e)
        {
            if (chkOccupancy.Checked)
            {
                populateAnalysisReportWithOccupancy(objSession.ReportsBo);

            }
            else
            {
                populateAnalysisReportWithOutOccupancy();
            }
            Control ctl = this.Parent;
            ModalPopupExtender mpExtender = new ModalPopupExtender();
            mpExtender = (ModalPopupExtender)ctl.FindControl("mdlRentAnalysisPopup");
            mpExtender.Show();

        }
        #endregion

        #region grdRentAnalysis RowDataBound
        /// <summary>
        /// grdRentAnalysis Row Data Bound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdRentAnalysis_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if ((e.Row.RowType == DataControlRowType.DataRow))
                {
                    e.Row.BackColor = System.Drawing.Color.White;

                    GridViewRow row = e.Row;
                    Label lblPropertyId = (Label)row.FindControl("lblProperty");
                    e.Row.Attributes.Add("onclick", string.Format("javascript:ShowPropertyDetail('{0}')", lblPropertyId.Text));
                    e.Row.Attributes.Add("onmouseover", "mouseIn(this);");
                    e.Row.Attributes.Add("onmouseout", "mouseOut(this);");
                    e.Row.Attributes.Add("onmouseover", "this.style.cursor='pointer'");

                    if (e.Row.RowIndex + 1 == rowCount)
                    {
                        Label lnkBtnTotal = (Label)e.Row.FindControl("lblAddress");
                        lnkBtnTotal.Text = string.Empty;
                    }
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        #endregion

        #region btnHidden Click
        /// <summary>
        /// btnHidden Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnHidden_Click(object sender, EventArgs e)
        {
            try
            {
                dynamic argument = Request["__EVENTARGUMENT"];

                Response.Redirect(PathConstants.PropertiesListPath + argument.ToString());
            }

            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region btnBack click Event
        /// <summary>
        /// btnBack click Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnBack_Click(object sender, EventArgs e)
        {
            if (objSession.showHidePopUp == showHidePopUp.Properties.ToString())
            {
                showAnalysisPopup(objSession.ReportsBo);
                lblBreadCrump.Text = rePopulateHeader(lblBreadCrump.Text);
            }
            else if (objSession.showHidePopUp == showHidePopUp.AssetTypeOccupacy.ToString())
            {
                showFirstAnalysisPopup(objSession.StockReportsBo);
            }

        }
        #endregion
        #endregion
        #region Function

        #region populate  Propertes Detail
        public void populatePropertesDetail(ReportsBO objReportsBO)
        {
            uiMessage.hideMessage();
            objSession.ReportsBo = objReportsBO;
            int totalCount = 0;
            PageSortBO objPageSortBo = new PageSortBO("DESC", "PROPERTYID", 1, 10);
            if (pageSortViewState != null)
            {
                objPageSortBo = pageSortViewState;
            }
            //populateHeader(objReportsBO);
            DataSet resultDataSet = new DataSet();
            resultDataSet = objReportsBL.getProfessionalAnalysisReportDetail(objReportsBO, objPageSortBo, ref totalCount);
            rowCount = resultDataSet.Tables[0].Rows.Count;
            grdRentAnalysis.DataSource = resultDataSet;
            grdRentAnalysis.DataBind();
            objPageSortBo.TotalRecords = totalCount;
            objPageSortBo.TotalPages = (int)Math.Ceiling(Convert.ToDecimal(totalCount) / objPageSortBo.PageSize);
            pnlPagination.Visible = false;
            if (objPageSortBo.TotalPages > 0)
            {
                pnlPagination.Visible = true;
                pageSortViewState = objPageSortBo;
                GridHelper.setGridViewPager(ref pnlPagination, objPageSortBo);
            }
            pnlAssetTypeOccupancy.Visible = false;
            pnlAssetType.Visible = false;
            pnlPropertyDetail.Visible = true;
            printDivAssetTypeDetail.Visible = false;
            printDivAssetTypeOccupancy.Visible = false;
            btnPrint.Visible = true;
            objSession.showHidePopUp = showHidePopUp.Properties.ToString();
        }

        #endregion
        #region populate  Propertes Assets Detail
        public void populateProperteyAssetDetail(ReportsBO objReportsBO)
        {
            uiMessage.hideMessage();
            objSession.ReportsBo = objReportsBO;
            objSession.StockReportsBo = objReportsBO;
            populateHeader(objReportsBO);
            DataSet resultDataSet = new DataSet();
            resultDataSet = objReportsBL.getProfessionalAnalysisReportSingleAssetType(objReportsBO);
            rowCount = resultDataSet.Tables[0].Rows.Count;
            grdAssetType.DataSource = resultDataSet;
            grdAssetType.DataBind();
            pnlPropertyDetail.Visible = false;
            pnlAssetTypeOccupancy.Visible = false;
            printDivAssetTypeDetail.Visible = true;
            printDivAssetTypeOccupancy.Visible = false;
            btnPrint.Visible = false;
            pnlAssetType.Visible = true;
            chkOccupancy.Visible = false;
            objSession.showHidePopUp = showHidePopUp.AssetType.ToString();
        }

        #endregion
        #region populate  Propertes Assets Detail WithOccupancy
        public void populateAnalysisReportWithOccupancy(ReportsBO objReportsBO)
        {
            objSession.ReportsBo = objReportsBO;
            uiMessage.hideMessage();
            DataSet resultDataSet = new DataSet();
            //populateHeader(objReportsBO);
            resultDataSet = objReportsBL.AnalysisReportWithOccupancy(objReportsBO);
            rowCount = resultDataSet.Tables[0].Rows.Count;
            grdAssetTypeOccupancy.DataSource = resultDataSet;
            grdAssetTypeOccupancy.DataBind();
            pnlPropertyDetail.Visible = false;
            pnlAssetType.Visible = false;
            pnlAssetTypeOccupancy.Visible = true;

            printDivAssetTypeDetail.Visible = false;
            printDivAssetTypeOccupancy.Visible = true;
            btnPrint.Visible = false;
            grdAssetTypeWithOutOccupancy.Visible = false;
            grdAssetTypeOccupancy.Visible = true;
            objSession.showHidePopUp = showHidePopUp.AssetTypeOccupacy.ToString();
        }

        #endregion

        #region populate  Propertes Assets Detail With Out Occupancy
        public void populateAnalysisReportWithOutOccupancy()
        {

            uiMessage.hideMessage();
            DataSet resultDataSet = new DataSet();
            //populateHeader(objSession.ReportsBo);
            resultDataSet = objReportsBL.AnalysisReportWithOutOccupancy(objSession.ReportsBo);
            rowCount = resultDataSet.Tables[0].Rows.Count;
            grdAssetTypeWithOutOccupancy.DataSource = resultDataSet;
            grdAssetTypeWithOutOccupancy.DataBind();
            pnlPropertyDetail.Visible = false;
            pnlAssetType.Visible = false;
            pnlAssetTypeOccupancy.Visible = true;
            grdAssetTypeOccupancy.Visible = false;
            printDivAssetTypeDetail.Visible = false;
            printDivAssetTypeOccupancy.Visible = true;
            grdAssetTypeWithOutOccupancy.Visible = true;
            btnPrint.Visible = false;
            objSession.showHidePopUp = showHidePopUp.AssetTypeOccupacy.ToString();
        }

        #endregion

        #region show  AnalysisReport With Occupancy
        private void showAnalysisPopup(ReportsBO objReportsBo)
        {
            chkOccupancy.Visible = true;
            populateAnalysisReportWithOccupancy(objReportsBo);
            Control ctl = this.Parent;
            ModalPopupExtender mpExtender = new ModalPopupExtender();
            mpExtender = (ModalPopupExtender)ctl.FindControl("mdlRentAnalysisPopup");
            mpExtender.Show();
        }

        #endregion

        #region show Property
        private void showPropertyPopup(ReportsBO objReportsBo)
        {
            pageSortViewState = null;
            populatePropertesDetail(objReportsBo);
            Control ctl = this.Parent;
            ModalPopupExtender mpExtender = new ModalPopupExtender();
            mpExtender = (ModalPopupExtender)ctl.FindControl("mdlRentAnalysisPopup");
            mpExtender.Show();
        }

        #endregion

        #region show  AnalysisReport With Occupancy
        private void showFirstAnalysisPopup(ReportsBO objReportsBo)
        {
            chkOccupancy.Visible = false;
            populateProperteyAssetDetail(objReportsBo);
            Control ctl = this.Parent;
            ModalPopupExtender mpExtender = new ModalPopupExtender();
            mpExtender = (ModalPopupExtender)ctl.FindControl("mdlRentAnalysisPopup");
            mpExtender.Show();
        }

        #endregion
        #region populate Header
        private void populateHeader(ReportsBO objReportsBO)
        {
            if (objReportsBO.Header != null && objReportsBO.Header != string.Empty)
            {
                lblBreadCrump.Text = objReportsBO.Header + " -> " + objReportsBO.Asset;
            }
            else
            {
                lblBreadCrump.Text = objReportsBO.Asset;
            }
            lblDate.Text = objReportsBO.ReportDate.ToShortDateString();
        }
        #endregion

        #region populate Header
        private string rePopulateHeader(string existigText)
        {
            string result = string.Empty;
            existigText = existigText.Replace('-', ' ');
            string[] existigTextArr = existigText.Split('>');
            Array.Resize(ref existigTextArr, existigTextArr.Length - 1);
            if (existigTextArr.Length > 0)
            {

                foreach (string x in existigTextArr)
                {
                    result = result + x + "->";
                }
                result = result.Substring(0, result.Length - 2);
            }
            return result;
        }
        #endregion
        #endregion
        #region IListingPage
        public void loadData()
        {
            throw new NotImplementedException();
        }

        public void searchData()
        {
            throw new NotImplementedException();
        }

        public void populateData()
        {
            throw new NotImplementedException();
        }

        public void printData()
        {
            grdRentAnalysis.AllowPaging = false;
            grdRentAnalysis.AllowSorting = false;
            int totalCount = 0;
            objPageSortBo = pageSortViewState;
            objPageSortBo.PageNumber = 1;
            objPageSortBo.PageSize = 65000;
            DataSet resultDataSet = new DataSet();
            resultDataSet = objReportsBL.getProfessionalAnalysisReportDetail(objSession.ReportsBo, objPageSortBo, ref totalCount);
            DataRow dr = resultDataSet.Tables[0].Rows[resultDataSet.Tables[0].Rows.Count - 1];
            dr.Delete();
            resultDataSet.Tables[0].AcceptChanges();
            grdRentAnalysis.DataSource = resultDataSet;
            grdRentAnalysis.DataBind();
            pnlPagination.Visible = false;
            pnlAssetTypeOccupancy.Visible = false;
            pnlAssetType.Visible = false;
            pnlPropertyDetail.Visible = true;
            Control ctl = this.Parent;
            ModalPopupExtender mpExtender = new ModalPopupExtender();
            mpExtender = (ModalPopupExtender)ctl.FindControl("mdlRentAnalysisPopup");
            mpExtender.Show();
            // divPropertyDetail.Style.Add(" overflow", "auto");
            //divPropertyDetail.Style.Add("height", "400px");
            ScriptManager.RegisterClientScriptBlock(this.Page, typeof(UpdatePanel), Guid.NewGuid().ToString(), "PrintPage('" + divPropertyDetail.ClientID + "')", true);

        }

        public void exportGridToExcel()
        {
            throw new NotImplementedException();
        }

        public void exportToPdf()
        {
            throw new NotImplementedException();
        }

        public void downloadData()
        {
            throw new NotImplementedException();
        }

        public void applyFilters()
        {
            throw new NotImplementedException();
        }
        #endregion


        #region enum
        public enum showHidePopUp
        {
            AssetType,
            AssetTypeOccupacy,
            Properties
        }
        #endregion

    }
}