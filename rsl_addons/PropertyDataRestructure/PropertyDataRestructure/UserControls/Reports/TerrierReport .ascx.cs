﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PropertyDataRestructure.Interface;
using PDR_BusinessLogic.Reports;
using PDR_Utilities.Managers;
using PDR_BusinessObject.PageSort;
using System.Data;
using PDR_Utilities.Helpers;
using PDR_BusinessObject.Reports;
using System.IO;
using System.Web.UI.HtmlControls;
using PropertyDataRestructure.Base;
using PDR_DataAccess.Reports;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_Utilities.Constants;
using System.Text.RegularExpressions;

namespace PropertyDataRestructure.UserControls.Reports
{
    public partial class TerrierReport : UserControlBase, IListingPage
    {

        #region Properties
        ReportsBL objReportsBL = new ReportsBL(new ReportsRepo());
        PageSortBO objPageSortBo = new PageSortBO("DESC", "ADDRESS", 1, 30);
        public int rowCount = 0;
        #endregion

        #region Events Handling

        #region PageLoad Event
        /// <summary>
        /// PageLoad Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    uiMessage.hideMessage();
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion
        
        #region Change Page Number
        /// <summary>
        ///  Change Page Number
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>  
        protected void lnkbtnPager_Click(object sender, EventArgs e)
        {
            try
            {
                PageSortBO objPageSortBo = new PageSortBO("DESC", "ADDRESS", 1, 30);
                GridHelper.processGridPager(sender, objPageSortBo, pageSortViewState);
                populateData();

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        #endregion

        protected void grdRentAnalysis_RowCreated(object sender, GridViewRowEventArgs e)
        {
            //check if it is a header row
            //since allowsorting is set to true, column names are added as command arguments to
            //the linkbuttons by DOTNET API
            if (e.Row.RowType == DataControlRowType.Header)
            {
                LinkButton btnSort;
                Image image;
                //iterate through all the header cells
                foreach (TableCell cell in e.Row.Cells)
                {
                    //check if the header cell has any child controls
                    if (cell.HasControls())
                    {
                        //get reference to the button column
                        btnSort = (LinkButton)cell.Controls[0];
                        image = new Image();
                        if (pageSortViewState != null)
                        {
                            if (btnSort.CommandArgument == pageSortViewState.SortExpression)
                            {
                                //following snippet figure out whether to add the up or down arrow
                                //based on the sortdirection
                                if (pageSortViewState.SortDirection == SortDirection.Ascending.ToString())
                                {
                                    image.ImageUrl = "~/Images/Grid/sort_asc.png";
                                }
                                else
                                {
                                    image.ImageUrl = "~/Images/Grid/sort_desc.png";
                                }
                            }
                            else
                            {
                                image.ImageUrl = "~/Images/Grid/sort_both.png";
                            }
                            cell.Controls.Add(image);
                        }
                    }
                }
            }
        }

        #region btnSearch click Event
        /// <summary>
        /// btnSearch click Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                uiMessage.hideMessage();
                searchData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion
        
        #region "grdRentAnalysis Sorting"
        protected void grdRentAnalysis_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                objPageSortBo = pageSortViewState;

                objPageSortBo.SortExpression = e.SortExpression;
                objPageSortBo.PageNumber = 1;
                grdRentAnalysis.PageIndex = 0;
                objPageSortBo.setSortDirection();

                pageSortViewState = objPageSortBo;
                populateData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #region"change PageNumber"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void changePageNumber(object sender, EventArgs e)
        {
            try
            {
                Button btnGo = new Button();
                btnGo = (Button)sender;

                objPageSortBo = new PageSortBO("DESC", "ADDRESS", 1, 30);
                objPageSortBo = pageSortViewState;
                int pageNumber = 1;
                pageNumber = Convert.ToInt32(txtPageNumber.Text);
                txtPageNumber.Text = String.Empty;
                objPageSortBo = pageSortViewState;
                if (((pageNumber >= 1)
                            && (pageNumber <= objPageSortBo.TotalPages)))
                {
                    objPageSortBo = pageSortViewState;
                    objPageSortBo.PageNumber = pageNumber;
                    pageSortViewState = objPageSortBo;
                    populateData();

                }
                else
                {
                    // uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, UserMessageConstants.InvalidPageNumber, true);
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #region "Export to Excel"

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            try
            {
                uiMessage.hideMessage();
                exportGridToExcel();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        
        #endregion

        #region "btnView Click"

        protected void btnView_Click(object sender, EventArgs e)
        {
            try
            {
                Button lnkView = (Button)sender;
                string arg = lnkView.CommandArgument;
                //https://www.google.co.uk/maps/place/6,+39+Bonds+Meadow,+Lowestoft,+Suffolk+NR32+3QL/@52.4826388,1.7156752,17z/data=!3m1!4b1!4m2!3m1!1s0x47da1b0f4f4e20db:0x9ebc32634a63fb6c
                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(UpdatePanel), Guid.NewGuid().ToString(), "viewMap('" + arg.Replace(" ","+") + "')", true);

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }




        #endregion

        #endregion

        #region Interface Implementation
        public void loadData()
        {
            throw new NotImplementedException();
        }

        public void searchData()
        {
            PageSortBO objPageSortBo = new PageSortBO("DESC", "ADDRESS", 1, 30);
            pageSortViewState = objPageSortBo;
            if (applyFilters())
            {
                populateData();
            }
        }

        public void populateData()
        {
            int totalCount = 0;
            PageSortBO objPageSortBo;
            if (pageSortViewState == null)
            {
                objPageSortBo = new PageSortBO("DESC", "ADDRESS", 1, 30);
                pageSortViewState = objPageSortBo;
            }
            else
            {
                objPageSortBo = pageSortViewState;
            }

            DataSet resultDataSet = new DataSet();
            resultDataSet = objReportsBL.getTerriorReportData(objSession.ReportsBo, objPageSortBo, ref totalCount);
            grdRentAnalysis.DataSource = resultDataSet;
            grdRentAnalysis.DataBind();

            objPageSortBo.TotalRecords = totalCount;
            objPageSortBo.TotalPages = (int)Math.Ceiling(Convert.ToDecimal(totalCount) / objPageSortBo.PageSize);

            if (objPageSortBo.TotalPages > 0)
            {
                pnlPagination.Visible = true;
            }
            else
            {
                pnlPagination.Visible = false;
                uiMessage.showErrorMessage(UserMessageConstants.RecordNotFound);
            }
            pageSortViewState = objPageSortBo;
            GridHelper.setGridViewPager(ref pnlPagination, objPageSortBo);

        }

        public void printData()
        {
            throw new NotImplementedException();
        }

        public void exportGridToExcel()
        {

            int totalCount = 0;
            objPageSortBo = pageSortViewState;
            objPageSortBo.PageNumber = 1;
            objPageSortBo.PageSize = 65000;
            DataSet resultDataSet = new DataSet();
            resultDataSet = objReportsBL.getTerriorReportDataForExcel(objSession.ReportsBo, objPageSortBo, ref totalCount);

            DataTable dt = resultDataSet.Tables[0];
            //Before changing columns headings, have to perform aggregation and then insert a dr into this datatable.

            
            dt.Columns["PROPERTYID"].ColumnName = "PROPERTYID";
            dt.Columns["ADDRESS"].ColumnName = "Address";
            dt.Columns["PROPTYPE"].ColumnName = "PropertyType";
            dt.Columns["DATEBUILT"].ColumnName = "DateBuilt";
            dt.Columns["TENURETYPE"].ColumnName = "BHA Ownership";
            dt.Columns["LeaseLength"].ColumnName = "Length of Lease";
            dt.Columns["ChargedTo"].ColumnName = "Charged to";
            dt.Columns["RENT"].ColumnName = "RENT";
            dt.Columns["SERVICECHARGE"].ColumnName = "Service Charge";
            dt.Columns["EUV"].ColumnName = "EUV-SH";
            dt.Columns["OMVST"].ColumnName = "MV-T";
            dt.Columns["RIGHTTOBUY"].ColumnName = "RightToBuy";
            dt.Columns["PURCHASELEVEL"].ColumnName = "% Sold";
            
            dt.Columns["SCHEMECODE"].ColumnName = "SCHEMECODE";
            dt.Columns["SCHEMENAME"].ColumnName = "SCHEMENAME";
            dt.Columns["TOWNCITY"].ColumnName = "TOWNCITY";
            dt.Columns["POSTCODE"].ColumnName = "POSTCODE";
            dt.Columns["NROSHAssetType"].ColumnName = "Tenure";
            dt.Columns["YIELD"].ColumnName = "Yield (%)";
            dt.Columns["TitleNumber"].ColumnName = "TitleNumber";
            dt.Columns["DeedLocation"].ColumnName = "DeedLocation";
            dt.Columns["DecentHomes"].ColumnName = "Decent Homes";
            dt.Columns["STATUS"].ColumnName = "Status";
            dt.Columns["LocalAuthority"].ColumnName = "Local Authority";
            dt.Columns["SurveyDate"].ColumnName = "Survey Date";

            dt.Columns["NHBCWarrenty"].ColumnName = "NHBC Warranty";
            dt.Columns["NHBCExpiration"].ColumnName = "Expiration of NHBC Warranty";
            dt.Columns["FloodRisk"].ColumnName = "Flood Risk";
            try
            {
                dt.Columns.Remove("S106Date");
                dt.Columns.Remove("ElectricalCertDate");
                dt.Columns.Remove("FireAlamCertDate");
                dt.Columns.Remove("DryLiningCertDate");
                dt.Columns.Remove("DampProofCertDate");
                dt.Columns.Remove("AsbestosCertDate");
                dt.Columns.Remove("EPCDate");
                dt.Columns.Remove("HealthSafetyDate");
                dt.Columns.Remove("NHBCDate");
                dt.Columns.Remove("SchemeRestriction");
                dt.Columns.Remove("PropertyRestriction");

                dt.Columns.Remove("row");
            }
            catch
            {
            }
          
            string fileName = "TerrierReport_" + Convert.ToString(DateTime.Now.Day) + "_" + Convert.ToString(DateTime.Now.Month) + "_" + Convert.ToString(DateTime.Now.Year);
            ExportDownloadHelper.exportGridToExcelTerrierReport(fileName, dt);
        }


        public void exportToPdf()
        {
            throw new NotImplementedException();
        }

        public void downloadData()
        {
            throw new NotImplementedException();
        }
        void IListingPage.applyFilters()
        {
            throw new NotImplementedException();
        }
        public bool applyFilters()
        {
            ReportsBO objReportsBO = new ReportsBO();
            bool result = true;
            objReportsBO.IsSoldChecked = chkBoxIncludeSold.Checked;
            DropDownList ddlScheme = (DropDownList)schemeDropDown.FindControl("ddlScheme");
            if (Convert.ToInt32(ddlScheme.SelectedValue) > 0)
            {
                objReportsBO.Scheme = Convert.ToInt32(ddlScheme.SelectedValue);
            }
            if (txtSearch.Text != "")
            {
                dynamic regex = new Regex("[^a-zA-Z0-9 -]");
                if (regex.IsMatch(txtSearch.Text))
                {
                    result = false;
                    uiMessage.showErrorMessage(UserMessageConstants.InvalidCheracter);
                }
                else
                {
                    objReportsBO.SearchText = txtSearch.Text.Trim();

                }

            }
            objSession.ReportsBo = objReportsBO;
            return result;
        }
        #endregion
        
        #region currency Format
        public string currencyFormat(string value)
        {
            return value;

            // GeneralHelper.currencyFormat(value);
        }

        #endregion

        #region check Yield

        public string checkYield(string value)
        {
            if (value != "")
            {

                return value;
            }
            else
            {
                return "-";
            }
        }

        #endregion
        
    }
}