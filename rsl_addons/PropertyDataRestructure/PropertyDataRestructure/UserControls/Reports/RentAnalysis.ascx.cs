﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PDR_BusinessObject;
using PDR_BusinessLogic.Reports;
using System.Data;
using PDR_BusinessObject.Reports;
using PDR_Utilities.Helpers;
using AjaxControlToolkit;
using PDR_Utilities.Managers;
using PDR_BusinessObject.PageSort;
using PDR_DataAccess.Reports;
using PropertyDataRestructure.Base;
using PropertyDataRestructure.Interface;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_Utilities.Constants;

namespace PropertyDataRestructure.UserControls.Reports
{
    public partial class RentAnalysis : UserControlBase, IListingPage
    {
        #region Properties
        ReportsBL objReportsBL = new ReportsBL(new ReportsRepo());
        PageSortBO objPageSortBo = new PageSortBO("DESC", "PROPERTYID", 1, 10);
        public int rowCount = 0;        
        #endregion


        #region Events
        #region PageLoad Event
        /// <summary>
        /// PageLoad Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        #endregion

        #region Change Page Number
        /// <summary>
        ///  Change Page Number
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>  
        protected void lnkbtnPager_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton pagebuttton = new LinkButton();
                pagebuttton = (LinkButton)sender;
                PageSortBO objPageSortBo = new PageSortBO("DESC", "PROPERTYID", 1, 10);
                GridHelper.processGridPager(sender, objPageSortBo, pageSortViewState);

                populatePropertesDetail(objSession.ReportsBo);
                //bindToGrid();
                Control ctl = this.Parent;
                ModalPopupExtender mpExtender = new ModalPopupExtender();
                mpExtender = (ModalPopupExtender)ctl.FindControl("mdlRentAnalysisPopup");
                mpExtender.Show();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        #endregion

        #region btn Print Click event
        /// <summary>
        /// btn Print Click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnPrint_Click(object sender, EventArgs e)
        {
            try
            {

                printData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region btnHidden Click
        /// <summary>
        /// btnHidden Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnHidden_Click(object sender, EventArgs e)
        {
            try
            {
                dynamic argument = Request["__EVENTARGUMENT"];

                Response.Redirect(PathConstants.PropertiesListPath + argument.ToString());
            }

            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region grdRentAnalysis RowDataBound
        /// <summary>
        /// grdRentAnalysis Row Data Bound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdRentAnalysis_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if ((e.Row.RowType == DataControlRowType.DataRow))
                {
                    e.Row.BackColor = System.Drawing.Color.White;

                    GridViewRow row = e.Row;
                    Label lblPropertyId = (Label)row.FindControl("lblProperty");
                    e.Row.Attributes.Add("onclick", string.Format("javascript:ShowPropertyDetail('{0}')", lblPropertyId.Text));
                    e.Row.Attributes.Add("onmouseover", "mouseIn(this);");
                    e.Row.Attributes.Add("onmouseout", "mouseOut(this);");
                    e.Row.Attributes.Add("onmouseover", "this.style.cursor='pointer'");

                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        #endregion

        #endregion
        #region Function
        #region populatePropertesDetail
        public void populatePropertesDetail(ReportsBO objReportsBO)
        {
            objSession.ReportsBo = objReportsBO;
            int totalCount = 0;
            PageSortBO objPageSortBo = new PageSortBO("DESC", "PROPERTYID", 1, 10);
            if (pageSortViewState != null)
            {
                objPageSortBo = pageSortViewState;
            }
            if (objReportsBO.Header != null  && objReportsBO.Header != string.Empty  )
            {
                lblBreadCrump.Text = objReportsBO.Header + " -> " + objReportsBO.Asset;
            }
            else
            {
                lblBreadCrump.Text = objReportsBO.Asset;
            }
            lblDate.Text = objReportsBO.ReportDate.ToShortDateString() ;
            DataSet resultDataSet = new DataSet();
            resultDataSet = objReportsBL.getProfessionalAnalysisReportDetail(objReportsBO, objPageSortBo, ref totalCount);
            if (resultDataSet.Tables[0] != null)
            {
                grdRentAnalysis.DataSource = resultDataSet;
                grdRentAnalysis.DataBind();
                objPageSortBo.TotalRecords = totalCount;
                objPageSortBo.TotalPages = (int)Math.Ceiling(Convert.ToDecimal(totalCount)/ objPageSortBo.PageSize);
                pnlPagination.Visible = false;
                if (objPageSortBo.TotalPages > 0)
                {
                    pnlPagination.Visible = true;
                    pageSortViewState = objPageSortBo;
                    GridHelper.setGridViewPager(ref pnlPagination, objPageSortBo);
                }
            }
            else
            {
                uiMessage.isError = true;
                uiMessage.showErrorMessage(UserMessageConstants.NoRecordFound );
            }
            pnlHeaderArea.Visible = true;
        }
        #endregion
        #region currency Format
        public string currencyFormat(string value)
        {
            return GeneralHelper.currencyFormat(value);
        }

        #endregion

        #endregion

        #region IListingPage implementation
        public void loadData()
        {
            throw new NotImplementedException();
        }

        public void searchData()
        {
            throw new NotImplementedException();
        }

        public void populateData()
        {
            throw new NotImplementedException();
        }

        public void printData()
        {
            grdRentAnalysis.AllowPaging = false;
            grdRentAnalysis.AllowSorting = false;
            int totalCount = 0;
            objPageSortBo = pageSortViewState;
            objPageSortBo.PageNumber = 1;
            objPageSortBo.PageSize = 65000;
            DataSet resultDataSet = new DataSet();
            resultDataSet = objReportsBL.getProfessionalAnalysisReportDetail(objSession.ReportsBo, objPageSortBo, ref totalCount);
            //resultDataSet = objReportsBL.getAnalysisReportDetailForPrint(objSession.ReportsBo);
            resultDataSet.Tables[0].AcceptChanges();
            grdRentAnalysis.DataSource = resultDataSet;
            grdRentAnalysis.DataBind();
            pnlPagination.Visible = false;
            Control ctl = this.Parent;
            ModalPopupExtender mpExtender = new ModalPopupExtender();
            mpExtender = (ModalPopupExtender)ctl.FindControl("mdlRentAnalysisPopup");
            mpExtender.Show();
            //divPropertyDetail.Style.Add(" overflow", "auto");
            //divPropertyDetail.Style.Add("height", "400px");
            pnlHeaderArea.Visible = false;
            ScriptManager.RegisterClientScriptBlock(this.Page, typeof(UpdatePanel), Guid.NewGuid().ToString(), "PrintPage('" + divPropertyDetail.ClientID + "')", true);
            //populatePropertesDetail(objSession.ReportsBo);
            //pnlHeaderArea.Visible = true;
            //pnlPagination.Visible = true ;


        }

        public void exportGridToExcel()
        {
            throw new NotImplementedException();
        }

        public void exportToPdf()
        {
            throw new NotImplementedException();
        }

        public void downloadData()
        {
            throw new NotImplementedException();
        }

        public void applyFilters()
        {
            throw new NotImplementedException();
        }
        #endregion


    }
}