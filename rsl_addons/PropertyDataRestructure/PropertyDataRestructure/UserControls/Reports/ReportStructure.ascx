﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ReportStructure.ascx.cs"
    Inherits="PropertyDataRestructure.UserControls.Reports.ReportStructure" %>
<%@ Register TagName="Rent" TagPrefix="rent" Src="~/UserControls/Reports/RentAnalysis.ascx" %>
<%@ Register TagName="Stock" TagPrefix="stock" Src="~/UserControls/Reports/StockAnalysis.ascx" %>
<%@ Register TagName="Void" TagPrefix="void" Src="~/UserControls/Reports/VoidAnalysis.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<link href="../../Styles/Site.css" rel="stylesheet" media="screen" />
<asp:UpdatePanel ID="updPanelProperties" runat="server">
    <ContentTemplate>
        <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="500px" />
        <!-- wrapper -->
        <div class="pdr-wrap">
            <div class="title-bar">
                <asp:Label Text="Rent Analysis" ID="lblTitle" runat="server" />
            </div>
            <div class="data-panels">
                <table class="search-params">
                    <tr>
                        <td style="padding-right: 50px;">
                            Date (As At):
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtDateAt" CssClass="pdr-DateTextBox " />
                            <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtDateAt"
                                WatermarkText="DD/MM/YYYY" />
                            <asp:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="txtDateAt"
                                Format="dd/MM/yyyy" />
                            <br />
                            <asp:CompareValidator ID="CompareValidator4" runat="server" Style="float: left;"
                                ControlToValidate="txtDateAt" ErrorMessage=" Enter a valid date" Operator="DataTypeCheck"
                                Type="Date" Display="Dynamic" CssClass="Required" ValidationGroup="search" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Patch:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlPatch" runat="server" CssClass="pdr-SelectDropDodwn " AutoPostBack="True"
                                OnSelectedIndexChanged="ddlPatch_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            LA Area:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlLAArea" runat="server" CssClass="pdr-SelectDropDodwn " AutoPostBack="True"
                                OnSelectedIndexChanged="ddlLAArea_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Scheme:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlScheme" runat="server" CssClass="pdr-SelectDropDodwn ">
                                <asp:ListItem Text="text1" />
                                <asp:ListItem Text="text2" />
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Post Code:
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtPostCode" CssClass="pdr-TextBox " />
                        </td>
                        <td>
                            <asp:Button Text="Run Report" runat="server" ID="btnSearch" CssClass="pdr-Button"
                                OnClick="btnSearch_Click" ValidationGroup="search" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div style="width: 42%; float: right; margin-top: -20px;">
            <asp:Button Text="Print" ID="btnPrint" runat="server" OnClick="btnPrint_Click" Visible="false" />
        </div>
        <br />
        <asp:Panel runat="server" ID="pnlAnalysisList" Visible="false">
            <div class="pdr-wrap-grid" id="divAnalysisList">
                <cc1:PagingGridView ID="grdAnalysisList" runat="server" AllowPaging="false" AllowSorting="false"
                    AutoGenerateColumns="False" BorderStyle="Solid" BorderWidth="0px" CellPadding="4"
                    GridLines="None" OrderBy="" PageSize="30" Width="100%" PagerSettings-Visible="false"
                    OnRowDataBound="grdAnalysisList_RowDataBound" OnRowCommand="grdAnalysisList_RowCommand"
                    EmptyDataText="No Record Found" ShowHeaderWhenEmpty="true">
                    <Columns>
                        <asp:TemplateField HeaderText="Asset type:" ItemStyle-Width="70px">
                            <ItemTemplate>
                                <asp:Label ID="lblBlockName" runat="server" Text='<%# Bind("DESCRIPTION") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="10%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Total Count:" ItemStyle-Width="70px">
                            <ItemTemplate>
                                <asp:Label ID="lblTotal" runat="server" Text='<%# Bind("TOTAL") %>' Visible="false"></asp:Label>
                                <asp:LinkButton ID="lnkBtnTotal" runat="server" CausesValidation="False" CommandArgument='<%#Eval("ASSETTYPEID").ToString()+";"+Eval("DESCRIPTION").ToString() %>'
                                    Text='<%# Bind("TOTAL") %>' CommandName="0"></asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle Width="10%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Let:">
                            <ItemTemplate>
                                <asp:Label ID="lblLet" runat="server" Text='<%# Bind("LET") %>' Visible="false"></asp:Label>
                                <asp:LinkButton ID="lnkBtnLet" runat="server" CausesValidation="False" CommandArgument='<%#Eval("ASSETTYPEID").ToString()+";"+Eval("DESCRIPTION").ToString() %>'
                                    Text='<%# Bind("LET") %>' CommandName="2"></asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle Width="10%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Available:">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkBtnAvailable" runat="server" CausesValidation="False" CommandArgument='<%#Eval("ASSETTYPEID").ToString()+";"+Eval("DESCRIPTION").ToString() %>'
                                    Text='<%# Bind("AVAILABLE") %>' CommandName="1"></asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle Width="10%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Unavailable:">
                            <ItemTemplate>
                                <asp:LinkButton ID="lnkBtnUnAvailable" runat="server" CausesValidation="False" CommandArgument='<%#Eval("ASSETTYPEID").ToString()+";"+Eval("DESCRIPTION").ToString() %>'
                                    Text='<%# Bind("UNAVAILABLE") %>' CommandName="4"></asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle Width="10%" />
                        </asp:TemplateField>
                    </Columns>
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <HeaderStyle BackColor="#ffffff" CssClass="table-head" Font-Bold="True" ForeColor="black"
                        HorizontalAlign="Left" />
                </cc1:PagingGridView>
            </div>
        </asp:Panel>
        <asp:Panel ID="pnlRentAnalysisPopup" runat="server" BackColor="White" CssClass="pdr-rentanalysis-panel">
            <asp:ImageButton ID="imgBtnCancelAddWarrantyPopup" runat="server" Style="position: absolute;
                top: -12px; right: -12px; width: 22px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png"
                BorderWidth="0" />
            <div class="pdr-rentanalysis-popup" id="printDiv">
                <rent:Rent ID="rentAnalysisDetail" runat="server"></rent:Rent>
                <stock:Stock ID="stockAnalysisDetail" runat="server"></stock:Stock>
                <void:Void runat="server" ID="voidAnalysisDetail"></void:Void>
            </div>
        </asp:Panel>
        <asp:ModalPopupExtender ID="mdlRentAnalysisPopup" runat="server" DynamicServicePath=""
            Enabled="True" PopupControlID="pnlRentAnalysisPopup" CancelControlID="imgBtnCancelAddWarrantyPopup"
            TargetControlID="lblDismiss" BackgroundCssClass="modalBackground">
        </asp:ModalPopupExtender>
        <asp:Label ID="lblDismiss" runat="server"></asp:Label>
    </ContentTemplate>
</asp:UpdatePanel>
