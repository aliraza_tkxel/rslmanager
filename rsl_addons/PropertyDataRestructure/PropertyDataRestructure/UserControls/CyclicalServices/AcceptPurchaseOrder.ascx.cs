﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PropertyDataRestructure.Base;
using PropertyDataRestructure.Interface;
using System.Data;
using PDR_BusinessLogic.CyclicalServices;
using PDR_DataAccess.CyclicalServices;
using PDR_Utilities.Constants;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_Utilities.Helpers;
using System.Text;
using System.IO;
using System.Net.Mail;
using System.Net.Mime;

namespace PropertyDataRestructure.UserControls.CyclicalServices
{
    public partial class AcceptPurchaseOrder : UserControlBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            btnCancel.Enabled = false;
            if (Request.QueryString["IsReadOnly"] != null && Request.QueryString["IsReadOnly"] == "yes")
            {
                btnSave.Enabled = false;
                btnUpdate.Enabled = false;                
            }
            else
            {
                btnSave.Enabled = true;
                btnUpdate.Enabled = true;
                
            }
            if (Request.QueryString["cancel"] != null && Request.QueryString["cancel"] == "yes")
            {
                btnCancel.Enabled = true;
            }


        }

        protected void btnAccept_Click(object sender, EventArgs e)
        {
            try
            {
                uiMessage.hideMessage();
                int orderId = int.Parse(Request.QueryString["oid"]);
                int employeeId = int.Parse(Request.QueryString["uid"]);
                string status = ddlStatus.SelectedValue.ToString();
                CyclicalServicesBL objBl = new CyclicalServicesBL(new CyclicalServicesRepo());
                string contactName = ""; string email = "";
                bool result = objBl.acceptCyclicalPurchaseOrder(orderId, status, ref contactName, ref email);
                if (result)
                {
                    if (status == "Rejected")
                    {
                        sendEmailtoOriginator(orderId, contactName, email);
                    }
                    Response.Redirect("../../Views/CyclicalServices/UpdateCyclicalPurchaseOrder.aspx?oid=" + orderId.ToString() + "&uid=" + employeeId.ToString());
                }

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }



        #region "Previous button clicked"
        /// <summary>
        /// Previous button clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void btnPrevious_Click(object sender, EventArgs e)
        {
            try
            {
                uiMessage.hideMessage();
                lblStatus.Visible = true;
                ddlStatus.Visible = false;
                btnSave.Visible = false;
                btnUpdate.Visible = true;

                int currentIndex = (int)ViewState[ViewStateConstants.CurrentIndex];
                currentIndex = currentIndex - 1;
                ViewState[ViewStateConstants.CurrentIndex] = currentIndex;
                this.setPreviousNextButtonStates(currentIndex);
                populatePurchaseOrderDetail();



            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        #endregion

        #region "Next button clicked"
        /// <summary>
        /// Next button clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void btnNext_Click(object sender, EventArgs e)
        {
            try
            {
                uiMessage.hideMessage();
                lblStatus.Visible = true;
                ddlStatus.Visible = false;
                btnSave.Visible = false;
                btnUpdate.Visible = true;
                int currentIndex = (int)ViewState[ViewStateConstants.CurrentIndex];
                currentIndex = currentIndex + 1;
                ViewState[ViewStateConstants.CurrentIndex] = currentIndex;
                this.setPreviousNextButtonStates(currentIndex);
                populatePurchaseOrderDetail();

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region populate Data
        public void populateData(int orderId, int employeeId)
        {
            DataSet resultDataSet = new DataSet();
            CyclicalServicesBL objBl = new CyclicalServicesBL(new CyclicalServicesRepo());
            resultDataSet = objBl.getPurchaseOrderContractorDetails(orderId);
            objSession.GetPurchaseOrderDetailDs = resultDataSet;
            if (resultDataSet.Tables[0].Rows.Count > 0)
            {

                int currentIndex = Convert.ToInt32(resultDataSet.Tables[0].Rows[0][ApplicationConstants.JobsheetCurrentColumn]);
                int totalJobSheets = resultDataSet.Tables[0].Rows.Count;

                ViewState.Add(ViewStateConstants.CurrentIndex, currentIndex);
                ViewState.Add(ViewStateConstants.TotalJobsheets, totalJobSheets);
                this.setPreviousNextButtonStates(currentIndex);
                populatePurchaseOrderDetail();
                if (resultDataSet.Tables[1].Rows.Count > 0)
                {
                    grdAsbestos.DataSource = resultDataSet.Tables[1];
                    grdAsbestos.DataBind();
                    lblAsbestos.Visible = false;
                    grdAsbestos.Visible = true;
                }
                else
                {
                    lblAsbestos.Visible = true;
                    grdAsbestos.Visible = false;
                }

            }
        }
        #endregion

        #region populate Purchase Order Detail
        /// <summary>
        /// populate Purchase Order Detail
        /// </summary>
        private void populatePurchaseOrderDetail()
        {

            int currentIndex = Convert.ToInt32(ViewState[ViewStateConstants.CurrentIndex]);
            DataRow drPo = this.getCurrentIndexRowFromDataset(currentIndex);
            lblPoNumber.Text = drPo["PurchaseOrderId"].ToString() + "/" + drPo["Row"].ToString().PadLeft(drPo["Row"].ToString().Length + 3, '0');
            lblCycle.Text = drPo["Cycle"].ToString();
            lblContractor.Text = drPo["Contractor"].ToString();
            lblCommencement.Text = drPo["Commencement"].ToString();
            lblContact.Text = drPo["Contact"].ToString();
            txtMoreDetails.Text = drPo["MoreDetail"].ToString();
            lblCycleDate.Text = drPo["CycleDate"].ToString();
            //txtServiceRequired.Text = drPo["ServiceRequired"].ToString();
            lblServiceRequired.Text = drPo["ServiceRequired"].ToString();
            //txtServiceRequired.ReadOnly = true;
            lblOrderedBy.Text = drPo["OrderedBy"].ToString();
            lblCycleValue.Text = "£" + Convert.ToDecimal(drPo["CycleValue"]).ToString("#,##0.00");
            lblOrdered.Text = drPo["OrderedOn"].ToString();
            lblNet.Text = "£" + Convert.ToDecimal(drPo["NetCost"]).ToString("#,##0.00");
            lblVat.Text = "£" + Convert.ToDecimal(drPo["Vat"]).ToString("#,##0.00");
            lblStatus.Text = drPo["Status"].ToString();
            lblTotalCycleValue.Text = "£" + Convert.ToDecimal(drPo["GrossCost"]).ToString("#,##0.00");
            lblScheme.Text = drPo["SchemeName"].ToString();
            lblBlock.Text = drPo["BlockName"].ToString();
            lblPostCode.Text = drPo["PostCode"].ToString();
            lblCycleCompleted.Text = drPo["CycleCompleted"].ToString();
            lblContractNetValue.Text = "£" + Convert.ToDecimal(drPo["ContractorNetCost"]).ToString("#,##0.00");
            lblContractVat.Text = "£" + Convert.ToDecimal(drPo["ContractorVat"]).ToString("#,##0.00");
            lblTotalContractValue.Text = "£" + Convert.ToDecimal(drPo["ContractorGrossCost"]).ToString("#,##0.00");
            lblNoOfCycles.Text = ViewState[ViewStateConstants.TotalJobsheets].ToString();



        }

        #endregion

        #region "Set previous next button states"

        private void setPreviousNextButtonStates(int updatedIndex)
        {
            int currentIndex = updatedIndex;
            int totalJobSheets = Convert.ToInt32(ViewState[ViewStateConstants.TotalJobsheets]);

            lblSheetNumber.Text = currentIndex.ToString();
            lblTotalSheets.Text = totalJobSheets.ToString();

            //Previous Button state

            if ((updatedIndex - 1 < 1))
            {
                btnPrevious.Enabled = false;
            }
            else
            {
                btnPrevious.Enabled = true;
            }

            //Next Button state
            if ((updatedIndex + 1 > totalJobSheets))
            {
                btnNext.Enabled = false;
            }
            else
            {
                btnNext.Enabled = true;
            }

        }
        #endregion

        #region "Get current index row from dataset"
        /// <summary>
        /// Get current index row from dataset
        /// </summary>
        /// <param name="currentIndex"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public DataRow getCurrentIndexRowFromDataset(int currentIndex)
        {

            DataSet resultDataSet = objSession.GetPurchaseOrderDetailDs;
            DataTable dtPoDetail = resultDataSet.Tables[0];
            DataRow[] drPoDetail = null;
            drPoDetail = dtPoDetail.Select("Row = " + Convert.ToString(currentIndex));
            return drPoDetail[0];

        }

        #endregion



        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {

                uiMessage.hideMessage();
                lblStatus.Visible = false;
                ddlStatus.Visible = true;
                btnSave.Visible = true;
                btnUpdate.Visible = false;


            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                txtNote.Text = string.Empty;
                txtReason.Text = string.Empty;
                mdlPopUpAddNote.Show();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }

        protected void btnCancelPo_Click(object sender, EventArgs e)
        {
            try
            {

                uiMessageCancel.hideMessage();
                if (txtReason.Text.Trim() != "")
                {
                    int orderId = int.Parse(Request.QueryString["oid"]);
                    int employeeId = int.Parse(Request.QueryString["uid"]);

                    string transactionIdentity = DateTime.Now.ToString("ddMMyyyyhhmmssffff") + "_" + employeeId.ToString();
                    CyclicalServicesBL objBl = new CyclicalServicesBL(new CyclicalServicesRepo());
                    bool result = objBl.cancelCyclicalWork(orderId, txtReason.Text.Trim(), txtNote.Text.Trim(), employeeId, transactionIdentity);
                    if (result)
                    {
                        btnCancelPo.Enabled = false;
                        btnSave.Enabled = false;
                        btnUpdate.Enabled = false;
                        uiMessageCancel.showInformationMessage("PO" + orderId.ToString() + " has been cancelled successfully!");

                    }
                }
                else
                {
                    uiMessageCancel.showErrorMessage("Please enter the reason for cancel order.");
                }

            }
            catch (Exception ex)
            {
                uiMessageCancel.isError = true;
                uiMessageCancel.messageText = ex.Message;

                if ((uiMessageCancel.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessageCancel.isError == true))
                {
                    uiMessageCancel.showErrorMessage(uiMessageCancel.messageText);
                }
                mdlPopUpAddNote.Show();
            }
        }

        private void sendEmailtoOriginator(int orderId, string contactName, string email)
        {

            if (string.IsNullOrEmpty(email) || !GeneralHelper.isEmail(email))
            {
                throw new Exception("Unable to send email, invalid email address.");
            }
            else
            {

                //==========================================='
                StringBuilder body = new StringBuilder();
                StreamReader reader = new StreamReader(Server.MapPath("~/Email/RejectCyclicalOrder.html"));
                body.Append(reader.ReadToEnd());
                body.Replace("{po}", orderId.ToString());
                body.Replace("{ContractorContactName}", contactName);
                // Attach logo images with email
                //==========================================='
                LinkedResource logo50Years = new LinkedResource(Server.MapPath("~/Images/50_Years.gif"));
                logo50Years.ContentId = "logo50Years_Id";

                body = body.Replace("{Logo_50_years}", string.Format("cid:{0}", logo50Years.ContentId));

                LinkedResource logoBroadLandRepairs = new LinkedResource(Server.MapPath("~/Images/Broadland-Housing-Association.gif"));
                logoBroadLandRepairs.ContentId = "logoBroadLandRepairs_Id";

                body = body.Replace("{Logo_Broadland-Housing-Association}", string.Format("cid:{0}", logoBroadLandRepairs.ContentId));

                ContentType mimeType = new ContentType("text/html");

                AlternateView alternatevw = AlternateView.CreateAlternateViewFromString(body.ToString(), mimeType);
                alternatevw.LinkedResources.Add(logo50Years);
                alternatevw.LinkedResources.Add(logoBroadLandRepairs);
                //==========================================='


                MailMessage mailMessage = new MailMessage();

                mailMessage.Subject = "Cyclical works order has been rejected";

                mailMessage.To.Add(new MailAddress(email, contactName));
                // For a graphical view with logos, alternative view will be visible, body is attached for text view.
                mailMessage.Body = body.ToString();
                mailMessage.AlternateViews.Add(alternatevw);
                mailMessage.IsBodyHtml = true;

                EmailHelper.sendEmail(ref mailMessage);
            }
        }


    }
}