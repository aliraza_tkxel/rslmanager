﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PropertyDataRestructure.Base;
using PropertyDataRestructure.Interface;
using System.Data;
using PDR_BusinessLogic.CyclicalServices;
using PDR_DataAccess.CyclicalServices;
using PDR_Utilities.Constants;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_BusinessObject.CyclicalServices;
using System.IO;
using PDR_Utilities.Helpers;

namespace PropertyDataRestructure.UserControls.CyclicalServices
{
    public partial class UpdatePurchaseOrder : UserControlBase
    {
         
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Page.Form.Attributes.Add("enctype", "multipart/form-data");
                Page.Form.Attributes.Add("Content-Disposition", "form-data");
                btnCancel.Enabled = false;
                if (Request.QueryString["IsReadOnly"] != null && Request.QueryString["IsReadOnly"] == "yes")
                {
                    btnSave.Enabled = false;
                    btnUpdate.Enabled = false;                    
                }
                else
                {
                    btnSave.Enabled = true;
                    btnUpdate.Enabled = true;                    
                }
                if (Request.QueryString["cancel"] != null && Request.QueryString["cancel"] == "yes")
                {
                    btnCancel.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (lblStatus.Text == "Accepted" || lblStatus.Text == "Works Completed")
                {
                    uiMessage.hideMessage();
                    lblStatus.Visible = false;
                    ddlStatus.Visible = true;
                    btnSave.Visible = true;
                    btnUpdate.Visible = false;
                }

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (ddlStatus.SelectedValue == "Works Completed")
                {
                    txtCycleCompleted.Visible = true;
                    txtCycleCompleted.Text = string.Empty;
                    imgActualCompletionDate.Visible = true;
                    lblCycleCompleted.Visible = false;
                    lblCycleCompletedHeading.Visible = true;
                    fuInvoice.Visible = false;
                }
                else if (ddlStatus.SelectedValue == "Invoice Uploaded")
                {
                    txtCycleCompleted.Visible = false;
                    imgActualCompletionDate.Visible = false;
                    lblCycleCompleted.Visible = true;
                    lblCycleCompletedHeading.Visible = true;
                    fuInvoice.Visible = true;
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (validateForm())
                {
                    CyclicalServicesBL objBl = new CyclicalServicesBL(new CyclicalServicesRepo());
                    UpdateWorksCompletedBO requestObj = new UpdateWorksCompletedBO();
                    requestObj.cmContractorId = Convert.ToInt32(hdnCMContractorId.Value);
                    requestObj.purchaseOrderId = Convert.ToInt32(hdnPurchaseOrderId.Value);
                    requestObj.purchaseOrderItemId = Convert.ToInt32(hdnPurchaseOrderItemId.Value);
                    requestObj.status = ddlStatus.SelectedValue;
                    requestObj.workDetailId = Convert.ToInt32(hdnWorkDetailId.Value);
                    requestObj.transactionIdentity = DateTime.Now.ToString("ddMMyyyyhhmmssffff") + "_" + objSession.EmployeeId;


                    if (ddlStatus.SelectedValue == "Works Completed")
                    {
                        requestObj.cycleCompleted = Convert.ToDateTime(txtCycleCompleted.Text.Trim());
                        bool result = objBl.updateWorksCompleted(requestObj, objSession.EmployeeId);
                        if (result)
                        {
                            uiMessage.showInformationMessage("Cyclical Works completed successfully.");
                        }
                    }
                    else if (ddlStatus.SelectedValue == "Invoice Uploaded")
                    {
                        string[] validFileTypes = { "bmp", "gif", "png", "jpg", "jpeg", "pdf" };
                        string ext = System.IO.Path.GetExtension(fuInvoice.PostedFile.FileName);
                        bool isValidFile = false;
                        for (int i = 0; i < validFileTypes.Length; i++)
                        {
                            if (ext.ToUpper() == "." + validFileTypes[i].ToUpper())
                            {
                                isValidFile = true;
                                break;
                            }
                        }
                        if (!isValidFile)
                        {
                            uiMessage.showErrorMessage("Invalid File. Please upload a File with extension " + string.Join(",", validFileTypes));
                            //Label1.ForeColor = System.Drawing.Color.Red;
                            //Label1.Text = "Invalid File. Please upload a File with extension " +
                            //               string.Join(",", validFileTypes);
                        }
                        else
                        {

                            string filename = fuInvoice.FileName;
                            var fileDirectoryPath = string.Empty;
                            fileDirectoryPath = ConfigHelper.GetInvoiceUploadPath();
                            if ((Directory.Exists(fileDirectoryPath) == false))
                            {
                                Directory.CreateDirectory(fileDirectoryPath);
                            }

                            filename = FileHelper.getUniqueFileName(filename);
                            string filePath = fileDirectoryPath + filename;
                            fuInvoice.SaveAs(@filePath);
                            requestObj.file = "/Invoice_Images/"+filename;

                            bool result = objBl.updateInvoiceUpload(requestObj, objSession.EmployeeId);
                            if (result)
                            {
                                uiMessage.showInformationMessage("Invoice Uploaded successfully.");
                            }
                        }
                    }
                   
                    lblStatus.Visible = true;
                    ddlStatus.Visible = false;
                    btnSave.Visible = false;
                    btnUpdate.Visible = true;
                    txtCycleCompleted.Visible = false;
                    imgActualCompletionDate.Visible = false;
                    lblCycleCompleted.Visible = true;
                    lblCycleCompletedHeading.Visible = true;
                    fuInvoice.Visible = false;
                    populatePurchaseOrderDetail();
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }



        #region "Previous button clicked"
        /// <summary>
        /// Previous button clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void btnPrevious_Click(object sender, EventArgs e)
        {
            try
            {
                uiMessage.hideMessage();
                lblStatus.Visible = true;
                ddlStatus.Visible = false;
                btnSave.Visible = false;
                btnUpdate.Visible = true;
                txtCycleCompleted.Visible = false;
                imgActualCompletionDate.Visible = false;
                lblCycleCompleted.Visible = true;
                lblCycleCompletedHeading.Visible = true;
                fuInvoice.Visible = false;
                int currentIndex = (int)ViewState[ViewStateConstants.CurrentIndex];
                currentIndex = currentIndex - 1;
                ViewState[ViewStateConstants.CurrentIndex] = currentIndex;
                this.setPreviousNextButtonStates(currentIndex);
                populatePurchaseOrderDetail();

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        #endregion

        #region "Next button clicked"
        /// <summary>
        /// Next button clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void btnNext_Click(object sender, EventArgs e)
        {
            try
            {
                uiMessage.hideMessage();
                lblStatus.Visible = true;
                ddlStatus.Visible = false;
                btnSave.Visible = false;
                btnUpdate.Visible = true;
                txtCycleCompleted.Visible = false;
                imgActualCompletionDate.Visible = false;
                lblCycleCompleted.Visible = true;
                lblCycleCompletedHeading.Visible = true;
                fuInvoice.Visible = false;
                int currentIndex = (int)ViewState[ViewStateConstants.CurrentIndex];
                currentIndex = currentIndex + 1;
                ViewState[ViewStateConstants.CurrentIndex] = currentIndex;
                this.setPreviousNextButtonStates(currentIndex);
                populatePurchaseOrderDetail();

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region populate Data
        public void populateData(int orderId, int employeeId)
        {
           
            objSession.OrderId = orderId;
            DataSet resultDataSet = new DataSet();
            CyclicalServicesBL objBl = new CyclicalServicesBL(new CyclicalServicesRepo());
            resultDataSet = objBl.getPurchaseOrderContractorDetails(orderId);
            objSession.GetPurchaseOrderDetailDs = resultDataSet;
            if (resultDataSet.Tables[0].Rows.Count > 0)
            {

                int currentIndex = Convert.ToInt32(resultDataSet.Tables[0].Rows[0][ApplicationConstants.JobsheetCurrentColumn]);
                int totalJobSheets = resultDataSet.Tables[0].Rows.Count;

                ViewState.Add(ViewStateConstants.CurrentIndex, currentIndex);
                ViewState.Add(ViewStateConstants.TotalJobsheets, totalJobSheets);
                this.setPreviousNextButtonStates(currentIndex);
                populatePurchaseOrderDetail();
                if (resultDataSet.Tables[1].Rows.Count > 0)
                {
                    grdAsbestos.DataSource = resultDataSet.Tables[1];
                    grdAsbestos.DataBind();
                    lblAsbestos.Visible = false;
                    grdAsbestos.Visible = true;
                }
                else
                {
                    lblAsbestos.Visible = true;
                    grdAsbestos.Visible = false;
                }

            }
        }
        #endregion

        #region populate Purchase Order Detail
        /// <summary>
        /// populate Purchase Order Detail
        /// </summary>
        private void populatePurchaseOrderDetail()
        {

            int currentIndex = Convert.ToInt32(ViewState[ViewStateConstants.CurrentIndex]);
            DataRow drPo = this.getCurrentIndexRowFromDataset(currentIndex);
            lblPoNumber.Text = drPo["PurchaseOrderId"].ToString() + "/" + drPo["Row"].ToString().PadLeft(drPo["Row"].ToString().Length + 3, '0');
            lblCycle.Text = drPo["Cycle"].ToString();
            lblContractor.Text = drPo["Contractor"].ToString();
            lblCommencement.Text = drPo["Commencement"].ToString();
            lblContact.Text = drPo["Contact"].ToString();
            lblCycleDate.Text = drPo["CycleDate"].ToString();
            txtServiceRequired.Text = drPo["MoreDetail"].ToString();
            txtServiceRequired.ReadOnly = true;
            lblServiceRequired.Text = drPo["ServiceRequired"].ToString();
            lblOrderedBy.Text = drPo["OrderedBy"].ToString();
            lblCycleValue.Text = "£" + Convert.ToDecimal(drPo["CycleValue"]).ToString("#,##0.00");
            lblOrdered.Text = drPo["OrderedOn"].ToString();
            lblNet.Text = "£" + Convert.ToDecimal(drPo["NetCost"]).ToString("#,##0.00");
            lblVat.Text = "£" + Convert.ToDecimal(drPo["Vat"]).ToString("#,##0.00");
            lblStatus.Text = drPo["CWStatus"].ToString();
            lblTotalCycleValue.Text = "£" + Convert.ToDecimal(drPo["GrossCost"]).ToString("#,##0.00");
            lblScheme.Text = drPo["SchemeName"].ToString();
            lblBlock.Text = drPo["BlockName"].ToString();
            lblPostCode.Text = drPo["PostCode"].ToString();
            lblCycleCompleted.Text = drPo["CycleCompleted"].ToString();
            lblContractNetValue.Text = "£" + Convert.ToDecimal(drPo["ContractorNetCost"]).ToString("#,##0.00");
            lblContractVat.Text = "£" + Convert.ToDecimal(drPo["ContractorVat"]).ToString("#,##0.00");
            lblTotalContractValue.Text = "£" + Convert.ToDecimal(drPo["ContractorGrossCost"]).ToString("#,##0.00");
            lblNoOfCycles.Text = ViewState[ViewStateConstants.TotalJobsheets].ToString();

            hdnCMContractorId.Value = drPo["CMContractorId"].ToString();
            hdnWorkDetailId.Value = drPo["WorkDetailId"].ToString();
            hdnSendApprovalLink.Value = drPo["SendApprovalLink"].ToString();
            hdnPurchaseOrderId.Value = drPo["PurchaseOrderId"].ToString();
            hdnPurchaseOrderItemId.Value = drPo["PurchaseOrderItemId"].ToString();
            btnCancel.Visible = true;
            if (drPo["CWStatus"].ToString() == "Invoice Uploaded")
            {
                string url = "https://" + HttpContext.Current.Request.ServerVariables["server_name"] + drPo["PurchaseItemImage"].ToString();
               // lnkbtnInvoice.PostBackUrl = url;
                lnkbtnInvoice.Attributes.Add("href", url);
                lblInvoice.Text = drPo["PurchaseItemImage"].ToString().Substring(16);
                div_PurchaseItemImage.Visible = true;
            }
            else if (drPo["CWStatus"].ToString() == "Cancelled")
            {
                btnCancel.Visible = false;

            }
            else
            {
                div_PurchaseItemImage.Visible = false;
            }


        }

        #endregion

        #region "Set previous next button states"

        private void setPreviousNextButtonStates(int updatedIndex)
        {
            int currentIndex = updatedIndex;
            int totalJobSheets = Convert.ToInt32(ViewState[ViewStateConstants.TotalJobsheets]);

            lblSheetNumber.Text = currentIndex.ToString();
            lblTotalSheets.Text = totalJobSheets.ToString();

            //Previous Button state

            if ((updatedIndex - 1 < 1))
            {
                btnPrevious.Enabled = false;
            }
            else
            {
                btnPrevious.Enabled = true;
            }

            //Next Button state
            if ((updatedIndex + 1 > totalJobSheets))
            {
                btnNext.Enabled = false;
            }
            else
            {
                btnNext.Enabled = true;
            }

        }
        #endregion

        #region "Get current index row from dataset"
        /// <summary>
        /// Get current index row from dataset
        /// </summary>
        /// <param name="currentIndex"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public DataRow getCurrentIndexRowFromDataset(int currentIndex)
        {
            
            DataSet resultDataSet = new DataSet();
            CyclicalServicesBL objBl = new CyclicalServicesBL(new CyclicalServicesRepo());
            resultDataSet = objBl.getPurchaseOrderContractorDetails(objSession.OrderId);            
            DataTable dtPoDetail = resultDataSet.Tables[0];
            DataRow[] drPoDetail = null;
            drPoDetail = dtPoDetail.Select("Row = " + Convert.ToString(currentIndex));
            return drPoDetail[0];

        }

        #endregion

        private bool validateForm()
        {
            bool result = false;
            if (lblStatus.Text == "Accepted" || lblStatus.Text == "Works Completed")
            {
                if (ddlStatus.SelectedValue == "Works Completed")
                {
                    result = true;
                    if (txtCycleCompleted.Text == string.Empty)
                    {
                        uiMessage.showErrorMessage("CycleCompleted field is mandatory.");
                        result = false;
                    }
                    if (lblStatus.Text != "Accepted")
                    {
                        uiMessage.showErrorMessage("Works has already Completed");
                        result = false;
                    }
                }
                else if (ddlStatus.SelectedValue == "Invoice Uploaded")
                {
                    result = true;
                    if (!fuInvoice.HasFile)
                    {
                        uiMessage.showErrorMessage("Please select PDF or JPEG file for invoice");
                        result = false;
                    }
                    if (lblStatus.Text != "Works Completed")
                    {
                        uiMessage.showErrorMessage("Please complete works before uploading the invoice, to make sure the sequence of PO.");
                        result = false;
                    }
                }

            }
            return result;
        }
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                txtNote.Text = string.Empty;
                txtReason.Text = string.Empty;
                uiMessageCancel.hideMessage();
                mdlPopUpAddNote.Show();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }

        protected void btnCancelPo_Click(object sender, EventArgs e)
        {
            try
            {

                uiMessageCancel.hideMessage();
                if (txtReason.Text.Trim() != "")
                {
                    int orderId = int.Parse(Request.QueryString["oid"]);
                    int employeeId = int.Parse(Request.QueryString["uid"]);

                    string transactionIdentity = DateTime.Now.ToString("ddMMyyyyhhmmssffff") + "_" + employeeId.ToString();
                    CyclicalServicesBL objBl = new CyclicalServicesBL(new CyclicalServicesRepo());
                    bool result = objBl.cancelCyclicalWork(orderId, txtReason.Text.Trim(), txtNote.Text.Trim(), employeeId, transactionIdentity);
                    if (result)
                    {
                        btnCancelPo.Enabled = false;
                        btnSave.Enabled = false;
                        btnUpdate.Enabled = false;
                        uiMessageCancel.showInformationMessage("PO" + orderId.ToString() + " has been cancelled successfully!");
                    }
                }
                else
                {
                    uiMessageCancel.showErrorMessage("Please enter the reason for cancel order.");
                }
            }
            catch (Exception ex)
            {
                uiMessageCancel.isError = true;
                uiMessageCancel.messageText = ex.Message;

                if ((uiMessageCancel.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessageCancel.isError == true))
                {
                    uiMessageCancel.showErrorMessage(uiMessageCancel.messageText);
                }
                mdlPopUpAddNote.Show();
            }
        }





    }
}