﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UpdatePurchaseOrder.ascx.cs"
    Inherits="PropertyDataRestructure.UserControls.CyclicalServices.UpdatePurchaseOrder" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<link href="../../Styles/Site.css" rel="stylesheet" media="screen" />
<style type="text/css">
    td
    {
        padding: 7px;
    }
    .asbestosGrid, .asbestosGrid td, .asbestosGrid th, .activityGrid, .activityGrid td, activityGrid th
    {
        border-spacing: 3px;
        padding: 0px;
        border: 0 none;
    }
    
    .asbestosGrid tr th:nth-child(n+1), .asbestosGrid tr td:nth-child(n+1), .activityGrid tr th:nth-child(n+1), .activityGrid tr td:nth-child(n+1)
    {
        padding-left: 8px;
        border: 0 none;
    }
     .modal-content
    {
        /* Bootstrap sets the size of the modal in the modal-dialog class, we need to inherit it */
        width: inherit;
        height: inherit; /* To center horizontally */
        margin: 0 auto;
        pointer-events: all;
    }
    
    
    .modal-header, h4, .close
    {
        background-color: black;
        color: white !important;
        text-align: left;
        padding-top:1px;
    }
    
    .modal-footer
    {
        /*background-color: #f9f9f9;*/
    }
    
    .modal-header
    {
        min-height: 16.428571429px;
        padding-left: 20px !important;
        border-bottom: 1px solid #e5e5e5;
        font-weight: bold;
    }
    
    .modal-dialog
    {
        width: 100%;
        padding-top: -10px;
        padding-bottom: 10px;
    }
    
    .modal-dialog-950
    {
        width: 950px;
    }
    
    .modal-footer
    {
        padding: 5px 20px 5px 20px !important;
        text-align: right !important;
        border-top: 1px solid #e5e5e5 !important;
    }
</style>
<asp:UpdatePanel runat="server" ID="updPanelAcceptPurchaseOrder">
    <ContentTemplate>
        <asp:Panel ID="pnlAcceptPurchaseOrder" ClientIDMode="Static" runat="server" Style='border: 1px Solid Black;
            width: 97.5%; float: left; padding: 0px; margin-left: 6px;'>
            <div style="text-align: left; float: left; width: 100%;">
                <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="600px" />
            </div>
            <div class="AcceptPurchaseOrder">
                &nbsp Update Purchase Order:
            </div>
            <table width="100%">
                <tbody>
                    <tr>
                        <td align="right" colspan="2" style="padding-right: 20px;">
                            <asp:Button Text="Next >" runat="server" ID="btnNext" CssClass="btn btn-xs btn-blue right"
                                Style="padding: 3px 23px !important; margin: -3px 0 0 0;" OnClick="btnNext_Click" />
                            <asp:Button Text="&lt; Previous" runat="server" ID="btnPrevious" CssClass="btn btn-xs btn-blue right"
                                Style="padding: 3px 23px !important; margin: -3px 3px 0 0;" OnClick="btnPrevious_Click" />
                        </td>
                    </tr>
                </tbody>
            </table>
            <div id="print">
                <table width="100%">
                    <tbody>
                        <tr>
                            <td colspan="2">
                                <b>Summery</b>
                            </td>
                            <td align="right" colspan="2" style="padding-right: 20px;">
                                <asp:Label ID="Label1" runat="server" Font-Bold="true" Text="Page "></asp:Label>
                                <asp:Label ID="lblSheetNumber" runat="server" Font-Bold="true" Text=""></asp:Label>
                                <asp:Label ID="Label21" runat="server" Font-Bold="true" Text=" of "></asp:Label>
                                <asp:Label ID="lblTotalSheets" runat="server" Font-Bold="true" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom: 1px solid #b5b5b5; line-height: 1px;" colspan="4">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>PO Number:</b>
                            </td>
                            <td>
                                <asp:Label ID="lblPoNumber" runat="server"></asp:Label>
                                <asp:HiddenField ID="hdnWorkDetailId" runat="server" />
                                <asp:HiddenField ID="hdnCMContractorId" runat="server" />
                                <asp:HiddenField ID="hdnSendApprovalLink" runat="server" />
                                <asp:HiddenField ID="hdnPurchaseOrderId" runat="server" />
                                <asp:HiddenField ID="hdnPurchaseOrderItemId" runat="server" />
                            </td>
                            <td>
                                <b>Cycle:</b>
                            </td>
                            <td>
                                <asp:Label ID="lblCycle" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Contractor:</b>
                            </td>
                            <td>
                                <asp:Label ID="lblContractor" runat="server"></asp:Label>
                            </td>
                            <td>
                                <b>Commencement:</b>
                            </td>
                            <td>
                                <asp:Label ID="lblCommencement" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Contact:</b>
                            </td>
                            <td>
                                <asp:Label ID="lblContact" runat="server"></asp:Label>
                            </td>
                            <td>
                                <b>Cycle Date:</b>
                            </td>
                            <td>
                                <asp:Label ID="lblCycleDate" runat="server" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Service Required:</b>
                            </td>
                            <td>
                                <asp:Label ID="lblServiceRequired" runat="server"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblCycleCompletedHeading" runat="server" Text="Cycle Completed:" Font-Bold="true"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblCycleCompleted" runat="server"></asp:Label>
                                <asp:TextBox runat="server" ID="txtCycleCompleted" Visible="false" />
                                <cc1:CalendarExtender runat="server" ID="clndrActualCompletion" TargetControlID="txtCycleCompleted"
                                    PopupButtonID="imgActualCompletionDate" PopupPosition="Right" TodaysDateFormat="dd/MM/yyyy"
                                    Format="dd/MM/yyyy">
                                </cc1:CalendarExtender>
                                <asp:ImageButton ImageUrl="~/Images/calendar.png" runat="server" ID="imgActualCompletionDate"
                                    Style="vertical-align: bottom; border: none;" Visible="false" />
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;">
                                <b>More Details:</b>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtServiceRequired" TextMode="MultiLine" Rows="5"
                                    Width="250" />
                            </td>
                            <td style="vertical-align: bottom;">
                                <b>Cycle Value:</b>
                            </td>
                            <td style="vertical-align: bottom;">
                                <asp:Label ID="lblCycleValue" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Ordered by:</b>
                            </td>
                            <td>
                                <asp:Label ID="lblOrderedBy" runat="server"></asp:Label>
                            </td>
                            <td>
                                <b>Net Value:</b>
                            </td>
                            <td>
                                <asp:Label ID="lblNet" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Ordered :</b>
                            </td>
                            <td>
                                <asp:Label ID="lblOrdered" runat="server"></asp:Label>
                            </td>
                            <td>
                                <b>VAT:</b>
                            </td>
                            <td>
                                <asp:Label ID="lblVat" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>WO Status:</b>
                            </td>
                            <td>
                                <asp:Label ID="lblStatus" runat="server"></asp:Label>
                                <asp:DropDownList runat="server" ID="ddlStatus" AutoPostBack="True" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged"
                                    Visible="false">
                                    <asp:ListItem Text="Please Select" Value="-1" />
                                    <asp:ListItem Text="Upload Invoice" Value="Invoice Uploaded" />
                                    <asp:ListItem Text="Complete Works" Value="Works Completed" />
                                </asp:DropDownList>
                            </td>
                            <td>
                                <b>Total Cycle Value:</b>
                            </td>
                            <td>
                                <asp:Label ID="lblTotalCycleValue" runat="server" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <asp:FileUpload ID="fuInvoice" runat="server" Visible="false" BorderWidth="0" />
                                <div id="div_PurchaseItemImage" runat="server">
                                    <asp:Image ImageUrl="~/Images/file.png" Width="32" Height="32" runat="server" />
                                    <a target="_blank" id="lnkbtnInvoice" runat="server">
                                        <asp:Label Text="" runat="server" ID="lblInvoice" />
                                    </a>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" style="border-bottom: 1px solid #b5b5b5">
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Scheme:</b>
                            </td>
                            <td>
                                <asp:Label ID="lblScheme" runat="server"></asp:Label>
                            </td>
                            <td>
                                <b>Number of Cycles:</b>
                            </td>
                            <td>
                                <asp:Label ID="lblNoOfCycles" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Block:</b>
                            </td>
                            <td>
                                <asp:Label ID="lblBlock" runat="server"></asp:Label>
                            </td>
                            <td>
                                <b>Contract Net Value:</b>
                            </td>
                            <td>
                                <asp:Label ID="lblContractNetValue" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Post Code:</b>
                            </td>
                            <td>
                                <asp:Label ID="lblPostCode" runat="server"></asp:Label>
                            </td>
                            <td>
                                <b>VAT:</b>
                            </td>
                            <td>
                                <asp:Label ID="lblContractVat" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                &nbsp;
                            </td>
                            <td>
                                <b>Total Contract Value:</b>
                            </td>
                            <td>
                                <asp:Label ID="lblTotalContractValue" runat="server" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="border-bottom: 1px solid #b5b5b5; line-height: 1px;" colspan="4">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Asbestos:</b>
                            </td>
                            <td>
                                <div style='overflow: auto;'>
                                    <asp:GridView ID="grdAsbestos" runat="server" CssClass="asbestosGrid" ShowHeader="false"
                                        BorderStyle="None">
                                    </asp:GridView>
                                    <asp:Label Text="N/A" runat="server" ID="lblAsbestos" Visible="false" />
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <table width="100%">
                <tbody>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td colspan="2" align="right" style="padding-right: 20px; vertical-align: bottom;">
                            <input type="button" id="btnPrintClient" value="Print Job Sheet" onclick="PrintPage('print');"
                                class="btn btn-xs btn-blue right" style="padding: 3px 23px !important; margin: -3px 0 0 0;" />
                            <asp:Button Text="Update" runat="server" ID="btnUpdate" CssClass="btn btn-xs btn-blue right"
                                Style="padding: 3px 23px !important; margin: -3px 3px 0 0;" OnClick="btnUpdate_Click" />
                            <asp:Button Text="Save" runat="server" ID="btnSave" CssClass="btn btn-xs btn-blue right"
                                Style="padding: 3px 23px !important; margin: -3px 3px 0 0;" OnClick="btnSave_Click"
                                Visible="false" />
                                <asp:Button Text="Cancel Work Order" runat="server" ID="btnCancel" CssClass="btn btn-xs btn-blue right"
                                Style="padding: 3px 23px !important; margin: -3px 3px 0 0;" OnClick="btnCancel_Click" />
                        </td>
                    </tr>
                </tbody>
            </table>
        </asp:Panel>

        <asp:Panel ID="pnlCancelPopUp" runat="server" BackColor="White">
             <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">
                       Cancel Purchase Order:</h4>                    
                </div>
            <div class="modal-body">
            <table width="450px" style='margin: 5px;'>                
                <tr>
                    <td align="left" valign="top" colspan="2">
                        <uim:UIMessage ID="uiMessageCancel" runat="Server" Visible="false" width="300px" />
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <span style="width: 50px; margin-left: 20px;">Reason:<span class="Required">*</span></span>
                    </td>
                    <td align="left" valign="top" style="width: 250px;">
                        <asp:TextBox ID="txtReason" runat="server" Width="200" MaxLength="500">
                        </asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="<br/>You must enter up to a maximum of 500 characters"
                            ValidationExpression="^([\S\s]{0,500})$" ControlToValidate="txtReason" Display="Dynamic"
                            CssClass="Required" ValidationGroup="Cancel"></asp:RegularExpressionValidator>
                    </td>
                </tr>
                <tr>
                    <td align="left" valign="top">
                        <span style="width: 50px; margin-left: 20px;">Notes:</span>
                    </td>
                    <td align="left" valign="top" style="width: 250px;">
                        <asp:TextBox ID="txtNote" runat="server" Width="200" TextMode="MultiLine" Rows="5"
                            MaxLength="1000">
                        </asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        &nbsp;
                    </td>
                </tr>
                
               
            </table>
            </div>
            <div class="modal-footer" style="text-align: right; height:20px;">
            <div style='margin-right: 25px;'>
                            <input id="btnClose" type="button" value="Close" class="btn btn-xs btn-blue right"
                                style="padding: 3px 23px !important; margin: -3px 3px 0 0;" />
                            
                            <asp:Button ID="btnCancelPo" runat="server" Text="Save" CssClass="btn btn-xs btn-blue right"
                                Style="padding: 3px 23px !important; margin: -3px 3px 0 0;" ValidationGroup="Cancel"
                                OnClick="btnCancelPo_Click" />
                             </div>
                </div>
            </div>
            <//div>
        </asp:Panel>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnSave" />
    </Triggers>
</asp:UpdatePanel>

<cc1:ModalPopupExtender ID="mdlPopUpAddNote" runat="server" DynamicServicePath=""
    Enabled="True" PopupControlID="pnlCancelPopUp" DropShadow="false" CancelControlID="btnClose"
    TargetControlID="lblDispCancel" BackgroundCssClass="modalBackground">
</cc1:ModalPopupExtender>
<asp:Label ID="lblDispCancel" runat="server"></asp:Label>
