﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SendNotificationStage1PopUp.ascx.cs"
    Inherits="PropertyDataRestructure.UserControls.Void.SendNotificationStage1PopUp" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Import Namespace="PDR_Utilities.Constants" %>
<style type="text/css">
    .padding8
    {
        padding: 0 0 8px 0;
    }
</style>
<asp:UpdatePanel runat="server" ID="updpanelArrangeInspection">
    <ContentTemplate>
        <asp:Panel runat="server" ID="pnlAddInspection">
            <div style="height: auto; clear: both; padding: 10px;">
                <table id="tblStage1Notification" style="width: 441px; text-align: left; margin-right: 10px;">
                    <tr>
                        <td colspan="2">
                            <b>British Gas Void Notification</b>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <hr class="searchFaultHr" />
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div>
                                <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="350px" />
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <b>Stage 1</b>
                            <br />
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td class="padding8">
                            Arrange Gas/Electric Check?:<span class="Required">*</span>
                        </td>
                        <td class="padding8" style="width: 208px;">
                            <asp:RadioButtonList runat="server" RepeatDirection="Horizontal" ID="rdBtnListGasElectricCheck"
                                Style='margin-left: -5px;'>
                                <asp:ListItem Text="Yes" Value="Yes" />
                                <asp:ListItem Text="No"  Value="No"/>
                            </asp:RadioButtonList>
                            <br />
                            <asp:RequiredFieldValidator ID="rfvGasElectricCheck" runat="server" ControlToValidate="rdBtnListGasElectricCheck"
                                Width="208" CssClass="Required" ValidationGroup="saveStage1" Display="Dynamic"><%=UserMessageConstants.RequiredArrangeGasOrElectricCheck%></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Address:
                        </td>
                        <td>
                            <asp:Label Text="1ShipField" ID="lblAddress" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <asp:Label Text="Sprowston" ID="lblTownCity" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <asp:Label Text="Norwich" ID="lblCounty" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="padding8">
                        </td>
                        <td class="padding8">
                            <asp:Label Text="NR1 2GA" ID="lblPostCode" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td class="padding8">
                            Current Tenant Name:
                        </td>
                        <td class="padding8">
                            <asp:Label Text="Mr a Jon" ID="lblTenantName" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Vacating Tenant F/W Address:<span class="Required">*</span>
                        </td>
                        <td>
                            <asp:TextBox ID="txtFwAddress1" runat="server" Width="200px" />
                            <ajax:TextBoxWatermarkExtender ID="txtFwAddress1WatermarkExtender" runat="server"
                                TargetControlID="txtFwAddress1" WatermarkCssClass="Watermark" WatermarkText="Address1">
                            </ajax:TextBoxWatermarkExtender>
                            <br />
                            <asp:RequiredFieldValidator ID="rfvFwAddress1" runat="server" ControlToValidate="txtFwAddress1"
                                CssClass="Required" ValidationGroup="saveStage1" Display="Dynamic"><%=UserMessageConstants.RequiredFwAddress1%></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <asp:TextBox ID="txtFwAddress2" runat="server" Width="200px" />
                            <ajax:TextBoxWatermarkExtender ID="txtFwAddress2WatermarkExtender1" runat="server"
                                TargetControlID="txtFwAddress2" WatermarkCssClass="Watermark" WatermarkText="Address2">
                            </ajax:TextBoxWatermarkExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <asp:TextBox ID="txtFwTownCity" runat="server" Width="200px" />
                            <ajax:TextBoxWatermarkExtender ID="txtFwTownCityWatermarkExtender" runat="server"
                                TargetControlID="txtFwTownCity" WatermarkCssClass="Watermark" WatermarkText="Town/City">
                            </ajax:TextBoxWatermarkExtender>
                        </td>
                    </tr>
                    <tr>
                        <td class="padding8">
                            Vacating Tenant F/W PostCode:<span class="Required">*</span>
                        </td>
                        <td class="padding8">
                            <asp:TextBox ID="txtFwPostCode" runat="server" Width="200px" />
                            <ajax:TextBoxWatermarkExtender ID="txtFwPostCodeWatermarkExtender" runat="server"
                                TargetControlID="txtFwPostCode" WatermarkCssClass="Watermark" WatermarkText="PostCode">
                            </ajax:TextBoxWatermarkExtender>
                            <br />
                            <asp:RequiredFieldValidator ID="rfvPostCode" runat="server" ControlToValidate="txtFwPostCode"
                                CssClass="Required" ValidationGroup="saveStage1" Display="Dynamic"><%=UserMessageConstants.RequiredPostCode%></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="padding8">
                            Date Occupancy Ceases:<span class="Required">*</span>
                        </td>
                        <td class="padding8">
                            <asp:TextBox ID="txtOccupancyCeaseDate" runat="server" Style="width: 116px !important;"></asp:TextBox>
                            <ajax:CalendarExtender ID="clndrOccupancyCeaseDate" runat="server" Format="dd/MM/yyyy"
                                PopupButtonID="imgCalDate" PopupPosition="Right" TargetControlID="txtOccupancyCeaseDate"
                                TodaysDateFormat="dd/MM/yyyy">
                            </ajax:CalendarExtender>
                            <asp:ImageButton ID="imgCalDate" runat="server" ImageUrl="~/Images/calendar.png"
                                Style="vertical-align: bottom; border: none;" />
                            <br />
                            <asp:RequiredFieldValidator ID="rfvOccupancyCeaseDate" runat="server" ControlToValidate="txtOccupancyCeaseDate"
                                CssClass="Required" ValidationGroup="saveStage1" Display="Dynamic"><%=UserMessageConstants.RequiredOccupancyCeaseDate%></asp:RequiredFieldValidator>
                            <asp:CompareValidator runat="server" ID="compvOccupancyCeaseDate" ControlToValidate="txtOccupancyCeaseDate"
                                Type="Date" CssClass="Required" Display="Dynamic" Operator="DataTypeCheck" ValidationGroup="saveStage1"><%=UserMessageConstants.RequireValidOccupancyCeaseDate%></asp:CompareValidator>
                        </td>
                    </tr>
                    <tr>
                        <td class="padding8">
                            British Gas Email:
                        </td>
                        <td class="padding8">
                            <asp:TextBox ID="txtBritishGasEmail" Text="voidcare@britishgas.co.uk" runat="server"
                                Width="200px" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div style="float: right; width: 100%; margin-right: -19px;">
                                <asp:Button ID="btnSaveNext" runat="server" CssClass="margin_right20" Text="Save & Next >"
                                    OnClick="btnSaveNext_Click" ValidationGroup="saveStage1" />
                                <asp:Button ID="btnSaveSend" runat="server" CssClass="margin_right20" Text="Save & Send Stage 1 Notification"
                                    OnClick="btnSaveSend_Click" ValidationGroup="saveStage1" />
                                <asp:Button ID="btnCancel" runat="server" CssClass="margin_right20" Text="Cancel" />
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
    </ContentTemplate>
    <Triggers>
    <asp:PostBackTrigger ControlID="btnSaveSend" />
    </Triggers> 
</asp:UpdatePanel>
