﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_Utilities.Constants;
using PropertyDataRestructure.Interface;
using PropertyDataRestructure.Base;
using PDR_BusinessObject.BritishGas;
using PDR_BusinessLogic.BritishGas;
using PDR_DataAccess.BritishGas;
using PropertyDataRestructure.Views.Void.BritishGasNotification;

namespace PropertyDataRestructure.UserControls.Void
{
    public partial class SendNotificationStage3PopUp : UserControlBase, IAddEditPage
    {
        #region Delegate
        public delegate void LoadStage2BritishGasDataDelegate();
        #endregion

        #region Events
        public event LoadStage2BritishGasDataDelegate loadStage2BritishGasData;

        #region Page load
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                uiMessage.hideMessage();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion        

        #region btn Save Send Click
        /// <summary>
        /// If the user selects the “Save & Send Stage 3 Notification” button the details will be emailed as a PDF attached to the email address 
        /// entered in the “British Gas Email” field. 
        /// This will be added to the British Gas Notification list found in “Voids > Reports > British Gas Notifications” list, 
        /// where the PDF will be available as a link to open and print if necessary .
        /// The line listing will have a status of 3rd Notification Sent.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSaveSend_Click(object sender, EventArgs e)
        {
            try
            {
                //keep display the current popup
                showHidePopupInParent(modalPoupName: ApplicationConstants.MdlStage1Notification, hide: false);
                bool isUpdateSuccess = updateData();
                BritishGasNotifcationBO britishGasBo = new BritishGasNotifcationBO();
                britishGasBo = objSession.BritishGasNotificationBo;
                britishGasBo.IsNotificationSent = true;
                britishGasBo.StageId = 3;
                objSession.BritishGasNotificationBo = britishGasBo;
                if (isUpdateSuccess == true)
                {
                    GenerateBritishGasNotificationPdf bGasPdf = new GenerateBritishGasNotificationPdf();
                    bGasPdf.britishGasId = britishGasBo.BritishGasNotificationId;
                    bGasPdf.britishStageId = (int)britishGasBo.StageId;
                    bGasPdf.processNotificationDocument();
                    uiMessage.showInformationMessage(UserMessageConstants.SuccessMessage);
                    btnSaveSend.Enabled = false;

                }
                else
                {
                    uiMessage.showErrorMessage(UserMessageConstants.ErrorSavingBritishGasNotification);
                    showHidePopupInParent(ApplicationConstants.MdlStage2Notification, hide: false);
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                    showHidePopupInParent(ApplicationConstants.MdlStage2Notification, hide: false);
                }
            }
        }
        #endregion

        /// <summary>
        /// This function will open the stage 1 popup
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnBack2Stage1_Click(object sender, EventArgs e)
        {
            try
            {
                //hide the stage 2 notification popup
                showHidePopupInParent(modalPoupName: ApplicationConstants.MdlStage3Notification, hide: true);
                //display the stage 1 notification popup
                showHidePopupInParent(modalPoupName: ApplicationConstants.MdlStage2Notification, hide: false);
                loadStage2BritishGasData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                    showHidePopupInParent(ApplicationConstants.MdlStage2Notification, hide: false);
                }
            }

        }

        
        #endregion

        #region Functions

        #region populate Drop Down
        public void populateDropDown(DropDownList ddl)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region populate Dropdowns
        public void populateDropDowns()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region load Data
        public void loadData()
        {
            BritishGasNotifcationBO britishGasBo = new BritishGasNotifcationBO();
            britishGasBo = objSession.BritishGasNotificationBo;            
            objSession.BritishGasNotificationBo = britishGasBo;
            this.populateControls(britishGasBo);
        }
        #endregion

        #region populate controls
        /// <summary>
        /// populate the controls with values in bo
        /// </summary>
        /// <param name="britishGasBo"></param>
        private void populateControls(BritishGasNotifcationBO britishGasBo)
        {
            txtTenantName.Text = britishGasBo.NewTenantName;
            txtDob.Text = britishGasBo.NewTenantDateOfBirth.HasValue == true ? britishGasBo.NewTenantDateOfBirth.ToString(): string.Empty;
            txtTelephone.Text = britishGasBo.NewTenantTel;
            txtMobile.Text = britishGasBo.NewTenantMobile;
            txtNewTenantOccupancyDate.Text = britishGasBo.NewTenantOccupancyDate.ToString();
            txtNewTenantPreviousAddress1.Text = britishGasBo.NewTenantPreviousAddress1;
            txtNewTenantPreviousAddress2.Text = britishGasBo.NewTenantPreviousAddress2;
            txtNewTenantPreviousPostCode.Text = britishGasBo.NewTenantPreviousPostCode;
            txtNewTenantPreviousTownCity.Text = britishGasBo.NewTenantPreviousTownCity;
            txtGasMeterReading.Text = britishGasBo.GasMeterReading.ToString();
            txtGasMeterReadingDate.Text = britishGasBo.GasMeterReadingDate.ToString();
            txtElectricMeterReading.Text = britishGasBo.ElectricMeterReading.ToString();
            txtElectricMeterReadingDate.Text = britishGasBo.ElectricMeterReadingDate.ToString();            
        }
        #endregion

        #region save Data
        public void saveData()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region update Data
        /// <summary>
        /// This function would update the british gas data in database
        /// </summary>
        public bool updateData()
        {
            Nullable<DateTime> nullDate = null;            
            Nullable<Int64> nullInt64 = null;
            BritishGasNotifcationBO britishGasBo = new BritishGasNotifcationBO();
            britishGasBo = objSession.BritishGasNotificationBo;
            britishGasBo.NewTenantName = txtTenantName.Text;
            britishGasBo.NewTenantTel = txtTelephone.Text;
            britishGasBo.NewTenantMobile = txtMobile.Text;
            britishGasBo.NewTenantOccupancyDate = txtNewTenantOccupancyDate.Text.Trim() != string.Empty ? Convert.ToDateTime(txtNewTenantOccupancyDate.Text) : nullDate;
            britishGasBo.NewTenantDateOfBirth = txtDob.Text.Trim() != string.Empty ? Convert.ToDateTime(txtDob.Text) : nullDate;
            britishGasBo.NewTenantPreviousAddress1 = txtNewTenantPreviousAddress1.Text;
            britishGasBo.NewTenantPreviousAddress2 = txtNewTenantPreviousAddress2.Text;
            britishGasBo.NewTenantPreviousPostCode = txtNewTenantPreviousPostCode.Text;
            britishGasBo.NewTenantPreviousTownCity = txtNewTenantPreviousTownCity.Text;
            britishGasBo.GasMeterReading = txtGasMeterReading.Text.Trim() != string.Empty ? Convert.ToInt64(txtGasMeterReading.Text) : nullInt64;
            britishGasBo.GasMeterReadingDate = txtGasMeterReadingDate.Text.Trim() != string.Empty ? Convert.ToDateTime(txtGasMeterReadingDate.Text) : nullDate;            
            britishGasBo.ElectricMeterReading = txtElectricMeterReading.Text.Trim() != string.Empty ? Convert.ToInt64(txtElectricMeterReading.Text) : nullInt64;
            britishGasBo.ElectricMeterReadingDate = txtElectricMeterReadingDate.Text.Trim() != string.Empty ? Convert.ToDateTime(txtElectricMeterReadingDate.Text) : nullDate;            
            britishGasBo.UserId = objSession.EmployeeId;
            britishGasBo.IsNotificationSent = true;
            BritishGasBL britishGasBl = new BritishGasBL(new BritishGasRepo());
            bool isSaved = britishGasBl.updateBritishGasStage3Data(ref britishGasBo);
            objSession.BritishGasNotificationBo = britishGasBo;
            return isSaved;
        }
        #endregion

        #region validate Data
        public bool validateData()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region reset controls
        public void resetControls()
        {
            throw new NotImplementedException();
        }
        #endregion





        #endregion
    }
}