﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PropertyDataRestructure.Base;
using PropertyDataRestructure.Interface;
using PDR_BusinessObject.MeSearch;
using PDR_BusinessObject.PageSort;
using PDR_BusinessLogic.Scheduling;
using System.Data;
using PDR_DataAccess.Scheduling;
using PDR_Utilities.Constants;
using PDR_Utilities.Helpers;
using PDR_BusinessLogic.BritishGas;
using PDR_DataAccess.BritishGas;
using PDR_BusinessObject.BritishGas;
using EO.Pdf;
using System.Drawing;
using System.IO;

namespace PropertyDataRestructure.UserControls.Void
{
    public partial class BritishGasNotificationInProgressTab : UserControlBase, IListingPage
    {
        #region Properties

        PageSortBO objPageSortBo = new PageSortBO("DESC", "NotificationStageId", 1, 30);
        public int rowCount = 0;
        public int britishGasId = 0;
        public int britishStageId = 0;
        string documentTemplateUrl = string.Empty;
        string notificationDocumentPath = string.Empty;
        string notificationRootPath = string.Empty;
        string notificationDoucmentName = string.Empty;
        byte[] notificationInByteForm = null;
        #endregion

        #region Events

        #region Page load event
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                uiMessage.hideMessage();
                //this.ucArrangeInspectionPopUp.delegateRefreshParentGrid = new Action(loadData);
                this.ucStage1Notification.loadStage2BritishGasData += new SendNotificationStage1PopUp.LoadStage2BritishGasDataDelegate(loadStage2BritishGasData);
                this.ucStage2Notification.loadStage1BritishGasData += new SendNotificationStage2PopUp.LoadStage1BritishGasDataDelegate(loadStage1BritishGasData);
                this.ucStage2Notification.loadStage3BritishGasData += new SendNotificationStage2PopUp.LoadStage3BritishGasDataDelegate(loadStage3BritishGasData);
                uiMessage.hideMessage();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region Pager Event Handler
        /// <summary>
        /// Pager event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkbtnPager_Click(object sender, EventArgs e)
        {
            try
            {
                PageSortBO objPageSortBo = new PageSortBO("DESC", "NotificationStageId", 1, 30);
                pageSortViewState = GridHelper.processGridPager(sender, objPageSortBo, pageSortViewState);
                loadData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region grd In Progress Sorting
        protected void grdInProgress_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                objPageSortBo = pageSortViewState;

                objPageSortBo.SortExpression = e.SortExpression;
                objPageSortBo.PageNumber = 1;
                grdInProgress.PageIndex = 0;
                objPageSortBo.setSortDirection();

                pageSortViewState = objPageSortBo;
                loadData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #region Button Show Notification PopUp Event Handler
        /// <summary>
        /// Button Schedule Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void imgBtnShowNotification_Click(Object sender, EventArgs e)
        {
            try
            {
                ImageButton btnSchedule = (ImageButton)sender;
                int NotificationId = Convert.ToInt32(btnSchedule.CommandArgument);


                //Another way to get the values instead of command argument.
                GridViewRow row = (sender as ImageButton).NamingContainer as GridViewRow;
                grdInProgress.SelectedIndex = row.RowIndex;
                var hdnCustomerId = grdInProgress.Rows[grdInProgress.SelectedIndex].FindControl("hdnCustomerId") as HiddenField;
                var hdnIsNotificationSent = grdInProgress.Rows[grdInProgress.SelectedIndex].FindControl("hdnIsNotificationSent") as HiddenField;
                var hdnPropertyId = grdInProgress.Rows[grdInProgress.SelectedIndex].FindControl("hdnPropertyId") as HiddenField;
                var hdnTenancyId = grdInProgress.Rows[grdInProgress.SelectedIndex].FindControl("hdnTenancyId") as HiddenField;
                var lblStage = grdInProgress.Rows[grdInProgress.SelectedIndex].FindControl("lblStage") as Label;
                int notificationStageId = Convert.ToInt32(lblStage.Text);
                BritishGasNotifcationBO britishGasBo = new BritishGasNotifcationBO();
                britishGasBo.TenancyId = Convert.ToInt32(hdnTenancyId.Value);
                britishGasBo.PropertyId = Convert.ToString(hdnPropertyId.Value);
                britishGasBo.CustomerId = Convert.ToInt32(hdnCustomerId.Value);
                britishGasBo.BritishGasNotificationId = NotificationId;
                britishGasBo.StageId = notificationStageId;
                bool isNotificationSent = Convert.ToBoolean(hdnIsNotificationSent.Value);
                britishGasBo.IsNotificationSent = isNotificationSent;
                objSession.BritishGasNotificationBo = britishGasBo;


                this.showBritishGasNotificationBo(notificationStageId, isNotificationSent);

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        private void showBritishGasNotificationBo(int NotificationStageId, bool isNotificationSent)
        {
            if (NotificationStageId == 1 && isNotificationSent == false)
            {
                mdlStage1Notification.Show();
                loadStage1BritishGasData();
            }
            else if ((NotificationStageId == 1 && isNotificationSent == true)
                || (NotificationStageId == 2 && isNotificationSent == false))
            {
                mdlStage2Notification.Show();
                loadStage2BritishGasData();
            }
            else if ((NotificationStageId == 2 && isNotificationSent == true)
               || (NotificationStageId == 3 && isNotificationSent == false))
            {
                mdlStage3Notification.Show();
                loadStage3BritishGasData();
            }
        }
        #endregion

        protected void grdInProgress_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                ImageButton lb = e.Row.FindControl("imgBtnPdf") as ImageButton;
                ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(lb);
            }
        }

        public void imgBtnPdfNotification_Click(Object sender, EventArgs e)
        {
            try
            {
                ImageButton btnSchedule = (ImageButton)sender;
                int NotificationId = Convert.ToInt32(btnSchedule.CommandArgument);
                this.britishGasId = NotificationId;

                //Another way to get the values instead of command argument.
                GridViewRow row = (sender as ImageButton).NamingContainer as GridViewRow;
                grdInProgress.SelectedIndex = row.RowIndex;

                var hdnIsNotificationSent = grdInProgress.Rows[grdInProgress.SelectedIndex].FindControl("hdnIsNotificationSent") as HiddenField;

                var lblStage = grdInProgress.Rows[grdInProgress.SelectedIndex].FindControl("lblStage") as Label;
                int notificationStageId = Convert.ToInt32(lblStage.Text);
                bool isNotificationSent = Convert.ToBoolean(hdnIsNotificationSent.Value);
                this.britishStageId = notificationStageId;
                processNotificationDocument();

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region IListingPage Implementation
        public void loadData()
        {
            BritishGasSearchBo objSearchBo = objSession.BritishGasSearchBo;
            PageSortBO objPageSortBo;
            if (pageSortViewState == null)
            {
                objPageSortBo = new PageSortBO("DESC", "NotificationStageId", 1, 30);
                pageSortViewState = objPageSortBo;
            }
            else
            {
                objPageSortBo = pageSortViewState;
            }
            objSearchBo.notificationStatus = "InProgress";
            int totalCount = 0;
            BritishGasBL objBritishGasBL = new BritishGasBL(new BritishGasRepo());
            DataSet resultDataSet = new DataSet();
            totalCount = objBritishGasBL.getBritishGasNotificationList(ref resultDataSet, objPageSortBo, objSearchBo);
            grdInProgress.DataSource = resultDataSet;
            grdInProgress.DataBind();
            objPageSortBo.TotalRecords = totalCount;
            objPageSortBo.TotalPages = Convert.ToInt32(Math.Ceiling((Double)totalCount / objPageSortBo.PageSize));

            if (objPageSortBo.TotalPages > 0)
            {
                pnlPagination.Visible = true;
            }
            else
            {
                pnlPagination.Visible = false;
                uiMessage.showErrorMessage(UserMessageConstants.RecordNotFound);
            }
            pageSortViewState = objPageSortBo;
            GridHelper.setGridViewPager(ref pnlPagination, objPageSortBo);
        }

        public void searchData()
        {
            throw new NotImplementedException();
        }

        public void populateData()
        {
            throw new NotImplementedException();
        }

        public void printData()
        {
            throw new NotImplementedException();
        }

        public void exportGridToExcel()
        {
            throw new NotImplementedException();
        }

        public void exportToPdf()
        {
            throw new NotImplementedException();
        }

        public void downloadData()
        {
            throw new NotImplementedException();
        }

        public void applyFilters()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region load Stage1 British Gas Data
        /// <summary>
        /// This function will be used to load the stage1 notification data.
        /// </summary>
        public void loadStage1BritishGasData()
        {
            ucStage1Notification.loadData();
        }
        #endregion

        #region load Stage2 British Gas Data
        /// <summary>
        /// This function will be used to load the stage2 notification data.
        /// </summary>
        public void loadStage2BritishGasData()
        {
            ucStage2Notification.populateDropDowns();
            ucStage2Notification.loadData();
        }
        #endregion


        #region load Stage3 British Gas Data
        /// <summary>
        /// This function will be used to load the stage3 notification data.
        /// </summary>
        public void loadStage3BritishGasData()
        {
            ucStage3Notification.loadData();
        }
        #endregion

        public void processNotificationDocument()
        {
            this.documentTemplateUrl = this.ToAbsoluteUrl("~/Views/Void/BritishGasNotification/BritishGasNotificationDocument.aspx?nid=" + this.britishGasId + "&sid=" + this.britishStageId.ToString());
            this.setFilePath();
            this.makeEoPdf();
            this.readNotificationDocument();
            this.downloadNotificationDocument(notificationDoucmentName, notificationDocumentPath);
        }
        public void makeEoPdf()
        {
            //Convert the Url to PDF
            EO.Pdf.Runtime.AddLicense("HOG4cai1wuCvdabw+g7kp+rp2g+9RoGkscufdePt9BDtrNzpz+eupeDn9hny" +
                "ntzCnrWfWZekzQzrpeb7z7iJWZekscufWZfA8g/jWev9ARC8W7zTv/vjn5mk" +
                "BxDxrODz/+ihbaW0s8uud4SOscufWbOz8hfrqO7CnrWfWZekzRrxndz22hnl" +
                "qJfo8h/kdpm5wN23aKm0wt6hWe3pAx7oqOXBs9+hWabCnrWfWZekzR7ooOXl" +
                "BSDxnrXo4xSwsc/Z6hLla8z83/LRe+fN59rmdrTAwB7ooOXlBSDxnrWRm+eu" +
                "peDn9hnynrWRm3Xj7fQQ7azcwp61n1mXpM0X6Jzc8gQQyJ21ucM=");

            //HtmlToPdf.Options.OutputArea = new RectangleF(0f, 1f, 8f, 20f);
            HtmlToPdf.Options.PageSize = PdfPageSizes.A4;
            //HtmlToPdf.Options.AutoFitY = HtmlToPdfAutoFitMode.ScaleToFit
            HtmlToPdf.ConvertUrl(this.documentTemplateUrl, notificationDocumentPath);

        }
        public string ToAbsoluteUrl(string relativeUrl)
        {
            if (string.IsNullOrEmpty(relativeUrl))
            {
                return relativeUrl;
            }

            if (HttpContext.Current == null)
            {
                return relativeUrl;
            }

            if (relativeUrl.StartsWith("/"))
            {
                relativeUrl = relativeUrl.Insert(0, "~");
            }
            if (!relativeUrl.StartsWith("~/"))
            {
                relativeUrl = relativeUrl.Insert(0, "~/");
            }

            dynamic url = HttpContext.Current.Request.Url;
            dynamic port = url.Port != 80 ? (":" + url.Port.ToString()) : String.Empty;

            return String.Format("{0}://{1}{2}{3}", url.Scheme, url.Host, port, VirtualPathUtility.ToAbsolute(relativeUrl));
        }
        private void readNotificationDocument()
        {
            using (FileStream stream = new FileStream(notificationDocumentPath, FileMode.Open, FileAccess.Read))
            {
                using (BinaryReader reader = new BinaryReader(stream))
                {
                    notificationInByteForm = reader.ReadBytes(Convert.ToInt32(stream.Length));
                }
            }
        }

        public void setFilePath()
        {
            //gernate unique String  
            string uniqueString = DateTime.Now.ToString("ddMMyyyyhhmmss");
            notificationRootPath = Server.MapPath("~/PDF/");
            notificationDoucmentName = "BritishGasVoidNotification" + "_" + uniqueString + ".pdf";
            notificationDocumentPath = notificationRootPath + notificationDoucmentName;

            if ((Directory.Exists(notificationRootPath) == false))
            {
                Directory.CreateDirectory(notificationRootPath);
            }
        }


        public void downloadNotificationDocument(string fileName, string completeFilePath)
        {
            Response.AppendHeader("content-disposition", "attachment; filename=" + fileName);
            Response.ContentType = "application/pdf";
            Response.WriteFile(completeFilePath);
            Response.Flush();
            Response.End();
        }


    }
}