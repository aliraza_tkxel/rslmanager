﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PropertyDataRestructure.Interface;
using PropertyDataRestructure.Base;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_BusinessObject.BritishGas;
using PDR_BusinessLogic.BritishGas;
using PDR_DataAccess.BritishGas;
using PDR_Utilities.Helpers;
using AjaxControlToolkit;
using PDR_Utilities.Constants;
using PropertyDataRestructure.Views.Void.BritishGasNotification;

namespace PropertyDataRestructure.UserControls.Void
{
    public partial class SendNotificationStage1PopUp : UserControlBase, IAddEditPage
    {
        #region Delegate
        public delegate void LoadStage2BritishGasDataDelegate();
        #endregion

        #region Events

        public event LoadStage2BritishGasDataDelegate loadStage2BritishGasData;
        #region Page load
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                uiMessage.hideMessage();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region btn Save Next Click
        /// <summary>
        /// If user selects the “Save & Next” button on British Gas Void Notification screen then 
        /// system loads the screen to send the 2nd British Gas Void Notification.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSaveNext_Click(object sender, EventArgs e)
        {
            try
            {                          
                if (updateData() == true)
                {
                    //hide the stage 1 notification
                    showHidePopupInParent(modalPoupName: ApplicationConstants.MdlStage1Notification, hide: true);
                    //display the stage 2 notification
                    showHidePopupInParent(modalPoupName: ApplicationConstants.MdlStage2Notification, hide: false);
                    loadStage2BritishGasData();
                }
                else
                {
                    //keep display the current popup
                    showHidePopupInParent(modalPoupName: ApplicationConstants.MdlStage1Notification, hide: false);
                    uiMessage.showErrorMessage(UserMessageConstants.ErrorSavingBritishGasNotification);
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    showHidePopupInParent(modalPoupName: ApplicationConstants.MdlStage1Notification, hide: false);
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region btn Save Send Click
        /// <summary>
        /// If the user selects the “Save & Send Stage 1 Notification” button the details will be emailed as a PDF attached to the email address 
        /// entered in the “British Gas Email” field. 
        /// This will be added to the British Gas Notification list found in “Voids > Reports > British Gas Notifications” list, 
        /// where the PDF will be available as a link to open and print if necessary .
        /// The line listing will have a status of 1st Notification Sent.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSaveSend_Click(object sender, EventArgs e)
        {
            try
            {
                //keep display the current popup
                showHidePopupInParent(modalPoupName: ApplicationConstants.MdlStage1Notification, hide:false);
                BritishGasNotifcationBO britishGasBo = new BritishGasNotifcationBO();
                britishGasBo = objSession.BritishGasNotificationBo;
                britishGasBo.IsNotificationSent = true;
                objSession.BritishGasNotificationBo = britishGasBo;
                bool isUpdateSuccess = updateData();
                if (isUpdateSuccess == true)
                {
                    //TODO:send notification
                    GenerateBritishGasNotificationPdf bGasPdf = new GenerateBritishGasNotificationPdf();
                    bGasPdf.britishGasId = britishGasBo.BritishGasNotificationId;
                    bGasPdf.britishStageId =(int) britishGasBo.StageId;
                    bGasPdf.processNotificationDocument();
                    uiMessage.showInformationMessage(UserMessageConstants.SuccessMessage);
                    showHidePopupInParent(modalPoupName: ApplicationConstants.MdlStage2Notification, hide: false);
                    loadStage2BritishGasData();
                    showHidePopupInParent(modalPoupName: ApplicationConstants.MdlStage1Notification, hide: true);
                }
                else
                {
                    uiMessage.showErrorMessage(UserMessageConstants.ErrorSavingBritishGasNotification);
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                    showHidePopupInParent(modalPoupName: ApplicationConstants.MdlStage1Notification, hide: false);
                }
            }
        }
        #endregion
        #endregion

        #region Functions

        #region populate Drop Down
        public void populateDropDown(DropDownList ddl)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region populate Dropdowns
        public void populateDropDowns()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region load Data
        public void loadData()
        {
            BritishGasNotifcationBO britishGasBo = new BritishGasNotifcationBO();
            britishGasBo = objSession.BritishGasNotificationBo;
            BritishGasBL britishGasBl = new BritishGasBL(new BritishGasRepo());
            britishGasBl.getBritishGasStage1Data(ref britishGasBo);
            objSession.BritishGasNotificationBo = britishGasBo;
            this.populateControls(britishGasBo);
        }
        #endregion

        #region populate controls
        /// <summary>
        /// populate the controls with values in bo
        /// </summary>
        /// <param name="britishGasBo"></param>
        private void populateControls(BritishGasNotifcationBO britishGasBo)
        {
            rdBtnListGasElectricCheck.SelectedValue = britishGasBo.IsGasElectricCheck == true ? "Yes" : "No";
            lblAddress.Text = britishGasBo.PropertyAddress1 + britishGasBo.PropertyAddress2;
            lblCounty.Text = britishGasBo.PropertyCounty;
            lblTownCity.Text = britishGasBo.PropertyCity;
            txtFwAddress1.Text = britishGasBo.TenantFwAddress1;
            txtFwAddress2.Text = britishGasBo.TenantFwAddress2;
            txtFwTownCity.Text = britishGasBo.TenantFwCity;
            txtFwPostCode.Text = britishGasBo.TenantFwPostCode;
            lblTenantName.Text = britishGasBo.CurrentTenantName;
            txtOccupancyCeaseDate.Text = britishGasBo.OccupancyCeaseDate.Value.Date.ToShortDateString();
        }
        #endregion

        #region save Data
        public void saveData()
        {
            BritishGasNotifcationBO britishGasBo = new BritishGasNotifcationBO();
            britishGasBo = objSession.BritishGasNotificationBo;
            BritishGasBL britishGasBl = new BritishGasBL(new BritishGasRepo());
            britishGasBl.saveBritishGasData(ref britishGasBo);                                    
        }
        #endregion

        #region update Data
        /// <summary>
        /// This function would update the british gas data in database
        /// </summary>
        public bool updateData()
        {
            BritishGasNotifcationBO britishGasBo = new BritishGasNotifcationBO();
            britishGasBo = objSession.BritishGasNotificationBo;
            britishGasBo.IsGasElectricCheck = rdBtnListGasElectricCheck.SelectedValue == "Yes" ? true : false;
            britishGasBo.TenantFwAddress1 = txtFwAddress1.Text;
            britishGasBo.TenantFwAddress2 = txtFwAddress2.Text;
            britishGasBo.TenantFwCity = txtFwTownCity.Text;
            britishGasBo.TenantFwPostCode = txtFwPostCode.Text;
            britishGasBo.OccupancyCeaseDate = Convert.ToDateTime(txtOccupancyCeaseDate.Text);
            britishGasBo.BritishGasEmail = txtBritishGasEmail.Text;
            britishGasBo.UserId = objSession.EmployeeId;            
            BritishGasBL britishGasBl = new BritishGasBL(new BritishGasRepo());
            bool isSaved = britishGasBl.updateBritishGasStage1Data(ref britishGasBo);
            objSession.BritishGasNotificationBo = britishGasBo;
            return isSaved;
        }
        #endregion 

        #region validate Data
        public bool validateData()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region reset controls
        public void resetControls()
        {
            throw new NotImplementedException();
        }
        #endregion        

        #endregion

    }
}