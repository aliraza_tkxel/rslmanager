﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SendNotificationStage2PopUp.ascx.cs"
    Inherits="PropertyDataRestructure.UserControls.Void.SendNotificationStage2PopUp" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Import Namespace="PDR_Utilities.Constants" %>
<style type="text/css">
    .padding12
    {
        padding-bottom: 12px;
    }
    #tblStage2Noticiation td:nth-child(1)
    {
        width: 26%;
    }
</style>
<script type="text/ecmascript">
    function ValidateIsGasMeterTypeSelected(sender, args) {
        if (args.Value > 0) {
            var gasMeterReading = document.getElementById("txtGasMeterReading").value;
            if (gasMeterReading == "") {
                return args.IsValid = false;
            }
            else {
                return args.IsValid = true;
            }

        }
        else {
            return args.IsValid = true;
        }

    }

    function ValidateIsElectricMeterTypeSelected(sender, args) {
        if (args.Value > 0) {
            var electricMeterReading = document.getElementById("txtElectricMeterReading").value;
            if (electricMeterReading == "") {
                return args.IsValid = false;
            }
            else {
                return args.IsValid = true;
            }

        }
        else {
            return args.IsValid = true;
        }

    }

    function ValidateIsMeterTypeSelected(sender, args) {
        var electricMeterType = document.getElementById("ddlElectricMeterType").value;
        if (args.Value > 0) {

            return args.IsValid = true;
        }
        else if (electricMeterType > 0) {
            return args.IsValid = true;
        }
        else {
            return args.IsValid = false;
        }

    }
</script>
<asp:UpdatePanel runat="server" ID="updpanelArrangeInspection" UpdateMode="Always">
    <ContentTemplate>
        <asp:Panel runat="server" ID="pnlAddInspection">
            <div style="height: auto; clear: both; padding: 10px;">
                <table id="tblStage2Noticiation" style="width: 600px; text-align: left; margin-right: 10px;">
                    <tr>
                        <td colspan="2">
                            <div style="float: left; width: 100%;">
                                <div style="float: left; font-weight: bold;">
                                    British Gas Void Inspection Information</div>
                                <div style="float: right; margin-right: 160px;">
                                    <asp:Button ID="btnBack2Stage1" runat="server" CssClass="margin_right" Text="< Back to Stage 1"
                                        OnClick="btnBack2Stage1_Click" />
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <hr class="searchFaultHr" style="width: 78%" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="350px" />
                            <asp:CustomValidator ID="cusValGasOrElectricMeterSelected" runat="server" ClientValidationFunction="ValidateIsMeterTypeSelected"
                                ControlToValidate="ddlGasMeterType" CssClass="Required" ValidationGroup="saveStage2"
                                Display="Dynamic"><%=UserMessageConstants.RequiredGasMeterTypeOrElectricMeterType%></asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <b>Stage 2</b>
                            <br />
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Gas Meter Type:
                        </td>
                        <td class="padding12">
                            <asp:DropDownList runat="server" ID="ddlGasMeterType" Width="283">
                                <asp:ListItem Text="Please Select" Value="-1" />
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Gas Meter Reading:<asp:CustomValidator ID="custValGasMeterReading" runat="server"
                                ClientValidationFunction="ValidateIsGasMeterTypeSelected" ControlToValidate="ddlGasMeterType"
                                CssClass="Required" ValidationGroup="saveStage2" Display="Dynamic" ErrorMessage="*"></asp:CustomValidator>
                        </td>
                        <td>
                            <asp:TextBox ID="txtGasMeterReading" runat="server" Width="100px" ClientIDMode="Static"
                                MaxLength="19" />
                            As At:
                            <asp:TextBox ID="txtGasMeterReadingDate" runat="server" Style="width: 100px !important;"></asp:TextBox>
                            <ajax:CalendarExtender ID="calExtGasMeterReadingDate" runat="server" Format="dd/MM/yyyy"
                                PopupButtonID="imgBtnGasMeterReadingDate" PopupPosition="Right" TargetControlID="txtGasMeterReadingDate"
                                TodaysDateFormat="dd/MM/yyyy">
                            </ajax:CalendarExtender>
                            <asp:ImageButton ID="imgBtnGasMeterReadingDate" runat="server" ImageUrl="~/Images/calendar.png"
                                Style="vertical-align: bottom; border: none;" />
                            <br />
                            <asp:RegularExpressionValidator ID="regExpValGasMeterReading" runat="server" ControlToValidate="txtGasMeterReading"
                                ValidationExpression="[1-9]\d*" ValidationGroup="saveStage2" CssClass="Required"><%=UserMessageConstants.MeterRedadingShouldBeDigitsOnly%></asp:RegularExpressionValidator>
                            <br />
                            <asp:CompareValidator ID="compValGasMeterReadingDate" runat="server" ControlToValidate="txtGasMeterReadingDate"
                                Operator="DataTypeCheck" Type="Date" Display="Dynamic" ValidationGroup="saveStage2"
                                CssClass="Required">Please enter the valid date</asp:CompareValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Electric Meter Type:
                        </td>
                        <td class="padding12">
                            <asp:DropDownList runat="server" ID="ddlElectricMeterType" Width="283" ClientIDMode="Static">
                                <asp:ListItem Text="Please Select" Value="-1" />
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Electric Meter Reading:<asp:CustomValidator ID="cmElectricMeterReading" runat="server"
                                ClientValidationFunction="ValidateIsElectricMeterTypeSelected" ControlToValidate="ddlElectricMeterType"
                                CssClass="Required" ValidationGroup="saveStage2" Display="Dynamic" ErrorMessage="*"></asp:CustomValidator>
                        </td>
                        <td>
                            <asp:TextBox ID="txtElectricMeterReading" runat="server" Width="100px" ClientIDMode="Static" />
                            As At:
                            <asp:TextBox ID="txtElectricMeterReadingDate" runat="server" Style="width: 100px !important;"></asp:TextBox>
                            <ajax:CalendarExtender ID="calExtElectricMeterReadingDate" runat="server" Format="dd/MM/yyyy"
                                PopupButtonID="imgBtnElectricMeterReadingDate" PopupPosition="Right" TargetControlID="txtElectricMeterReadingDate"
                                TodaysDateFormat="dd/MM/yyyy">
                            </ajax:CalendarExtender>
                            <asp:ImageButton ID="imgBtnElectricMeterReadingDate" runat="server" ImageUrl="~/Images/calendar.png"
                                Style="vertical-align: bottom; border: none;" />
                            <br />
                            <asp:RegularExpressionValidator ID="regExpValElectricMeterReading" runat="server"
                                ControlToValidate="txtElectricMeterReading" ValidationExpression="[1-9]\d*" ValidationGroup="saveStage2"
                                CssClass="Required">Meter reading should be in digits only</asp:RegularExpressionValidator>
                            <br />
                            <asp:CompareValidator ID="compValElectricMeterReadingDate" runat="server" ControlToValidate="txtElectricMeterReadingDate"
                                Operator="DataTypeCheck" Type="Date" Display="Dynamic" ValidationGroup="saveStage2"
                                CssClass="Required">Please enter the valid date</asp:CompareValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Gas Debt Amount(£):
                        </td>
                        <td>
                            <asp:TextBox ID="txtGasDebtAmount" runat="server" Width="100px" />
                            As At:
                            <asp:TextBox ID="txtGasDebtAmountDate" runat="server" Style="width: 100px !important;"></asp:TextBox>
                            <ajax:CalendarExtender ID="calExtGasDebtAmountDate" runat="server" Format="dd/MM/yyyy"
                                PopupButtonID="imgBtnGasDebtAmountDate" PopupPosition="Right" TargetControlID="txtGasDebtAmountDate"
                                TodaysDateFormat="dd/MM/yyyy">
                            </ajax:CalendarExtender>
                            <asp:ImageButton ID="imgBtnGasDebtAmountDate" runat="server" ImageUrl="~/Images/calendar.png"
                                Style="vertical-align: bottom; border: none;" />
                            <br />
                            <asp:CompareValidator runat="server" ID="compValGasDebtAmount" ControlToValidate="txtGasDebtAmount"
                                Type="Double" CssClass="Required" Display="Dynamic" ValidationGroup="saveStage2">Amount should only be in digits.
                            </asp:CompareValidator>
                            <br />
                            <asp:CompareValidator ID="compValGasDebtAmountDate" runat="server" ControlToValidate="txtGasDebtAmountDate"
                                Operator="DataTypeCheck" Type="Date" Display="Dynamic" ValidationGroup="saveStage2"
                                CssClass="Required">Please enter the valid date</asp:CompareValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Electric Debt Amount(£):
                        </td>
                        <td>
                            <asp:TextBox ID="txtElectricDebtAmount" runat="server" Width="100px" />
                            As At:
                            <asp:TextBox ID="txtElectricDebtAmountDate" runat="server" Style="width: 100px !important;"></asp:TextBox>
                            <ajax:CalendarExtender ID="calExtElectricDebtAmountDate" runat="server" Format="dd/MM/yyyy"
                                PopupButtonID="imgBtnElectricDebtAmountDate" PopupPosition="Right" TargetControlID="txtElectricDebtAmountDate"
                                TodaysDateFormat="dd/MM/yyyy">
                            </ajax:CalendarExtender>
                            <asp:ImageButton ID="imgBtnElectricDebtAmountDate" runat="server" ImageUrl="~/Images/calendar.png"
                                Style="vertical-align: bottom; border: none;" />
                            <br />
                            <asp:CompareValidator runat="server" ID="compValElectricDebtAmount" ControlToValidate="txtElectricDebtAmount"
                                Type="Double" CssClass="Required" Display="Dynamic" ValidationGroup="saveStage2">Amount should only be in digits.
                            </asp:CompareValidator>
                            <br />
                            <asp:CompareValidator ID="compValElectricDebtAmountDate" runat="server" ControlToValidate="txtElectricDebtAmountDate"
                                Operator="DataTypeCheck" Type="Date" Display="Dynamic" ValidationGroup="saveStage2"
                                CssClass="Required">Please enter the valid date</asp:CompareValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div style="float: right; margin-right: 141px;">
                                <asp:Button ID="btnSave" runat="server" CssClass="margin_right20" Text="Save & Next >"
                                    ValidationGroup="saveStage2" OnClick="btnSaveNext_Click" />
                                <asp:Button ID="btnSaveSend" runat="server" CssClass="margin_right20" Text="Save & Send Stage 2 Notification"
                                    OnClick="btnSaveSend_Click" />
                                <asp:Button ID="btnCancel" runat="server" CssClass="margin_right20" Text="Cancel" />
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
    </ContentTemplate>
    <Triggers>
        <asp:PostBackTrigger ControlID="btnSaveSend" />
    </Triggers>
</asp:UpdatePanel>
