﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PropertyDataRestructure.Interface;
using PropertyDataRestructure.Base;
using PDR_Utilities.Constants;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_BusinessObject.BritishGas;
using PDR_BusinessLogic.BritishGas;
using PDR_DataAccess.BritishGas;
using System.Data;
using AjaxControlToolkit;
using PropertyDataRestructure.Views.Void.BritishGasNotification;

namespace PropertyDataRestructure.UserControls.Void
{
    public partial class SendNotificationStage2PopUp : UserControlBase, IAddEditPage
    {
        #region Delegate
        public delegate void LoadStage1BritishGasDataDelegate();
        public delegate void LoadStage3BritishGasDataDelegate();
        #endregion

        #region Events
        public event LoadStage1BritishGasDataDelegate loadStage1BritishGasData;
        public event LoadStage3BritishGasDataDelegate loadStage3BritishGasData;

        #region Page load
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                uiMessage.hideMessage();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region btn Save Next Click
        /// <summary>
        /// If user selects the “Save & Next” button on British Gas Void Notification screen then 
        /// system loads the screen to send the 3rd British Gas Void Notification.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSaveNext_Click(object sender, EventArgs e)
        {
            try
            {
                //keep display the current popup               
                this.showHidePopupInParent(modalPoupName: ApplicationConstants.MdlStage2Notification, hide: false);

                bool isUpdateSuccess = updateData();
                if (isUpdateSuccess == true)
                {
                    //hide the stage 2 notification popup
                    showHidePopupInParent(modalPoupName: ApplicationConstants.MdlStage2Notification, hide: true);
                    //display the stage 3 notification popup
                    showHidePopupInParent(modalPoupName: ApplicationConstants.MdlStage3Notification, hide: false);
                    loadStage3BritishGasData();
                }
                else
                {
                    showHidePopupInParent(ApplicationConstants.MdlStage2Notification, hide: false);
                    uiMessage.showErrorMessage(UserMessageConstants.ErrorSavingBritishGasNotification);
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                    showHidePopupInParent(ApplicationConstants.MdlStage2Notification, hide: false);
                }
            }
        }
        #endregion

        #region btn Save Send Click
        /// <summary>
        /// If the user selects the “Save & Send Stage 2 Notification” button the details will be emailed as a PDF attached to the email address 
        /// entered in the “British Gas Email” field. 
        /// This will be added to the British Gas Notification list found in “Voids > Reports > British Gas Notifications” list, 
        /// where the PDF will be available as a link to open and print if necessary .
        /// The line listing will have a status of 2nd Notification Sent.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSaveSend_Click(object sender, EventArgs e)
        {
            try
            {
                //keep display the current popup
                showHidePopupInParent(modalPoupName: ApplicationConstants.MdlStage1Notification, hide: false);
                BritishGasNotifcationBO britishGasBo = new BritishGasNotifcationBO();
                britishGasBo = objSession.BritishGasNotificationBo;
                britishGasBo.IsNotificationSent = true;
                britishGasBo.StageId = 2;
                objSession.BritishGasNotificationBo = britishGasBo;
                bool isUpdateSuccess = updateData();
                if (isUpdateSuccess == true)
                {
                    GenerateBritishGasNotificationPdf bGasPdf = new GenerateBritishGasNotificationPdf();
                    bGasPdf.britishGasId = britishGasBo.BritishGasNotificationId;
                    bGasPdf.britishStageId = (int)britishGasBo.StageId;
                    bGasPdf.processNotificationDocument();
                    uiMessage.showInformationMessage(UserMessageConstants.SuccessMessage);
                    //display the stage 3 notification popup
                    showHidePopupInParent(modalPoupName: ApplicationConstants.MdlStage3Notification, hide: false);
                    loadStage3BritishGasData();
                    //hide the stage 2 notification popup
                    showHidePopupInParent(modalPoupName: ApplicationConstants.MdlStage2Notification, hide: true);
                }
                else
                {
                    uiMessage.showErrorMessage(UserMessageConstants.ErrorSavingBritishGasNotification);
                    showHidePopupInParent(ApplicationConstants.MdlStage2Notification, hide: false);
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                    showHidePopupInParent(ApplicationConstants.MdlStage2Notification, hide: false);
                }
            }
        }
        #endregion

        /// <summary>
        /// This function will open the stage 1 popup
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnBack2Stage1_Click(object sender, EventArgs e)
        {
            try
            {
                //hide the stage 2 notification popup
                showHidePopupInParent(modalPoupName: ApplicationConstants.MdlStage2Notification, hide: true);
                //display the stage 1 notification popup
                showHidePopupInParent(modalPoupName: ApplicationConstants.MdlStage1Notification, hide: false);
                loadStage1BritishGasData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                    showHidePopupInParent(ApplicationConstants.MdlStage2Notification, hide: false);
                }
            }

        }

        #region Validate Is Gas Meter Type Selected
        protected void ValidateIsGasMeterTypeSelected(object source, ServerValidateEventArgs args)
        {
            if (ddlGasMeterType.SelectedValue.ToString() != "-1")
            {
                if (txtGasMeterReading.Text == string.Empty)
                {
                    args.IsValid = false;
                }
                else
                {
                    args.IsValid = true;
                }
            }
        }
        #endregion
        #endregion

        #region Functions

        #region populate Drop Down
        public void populateDropDown(DropDownList ddl)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region populate Dropdowns
        public void populateDropDowns()
        {
            DataSet resultDataSet = new DataSet();
            BritishGasBL britishGasBl = new BritishGasBL(new BritishGasRepo());
            britishGasBl.getMeterTypes(ref resultDataSet);
            ddlGasMeterType.DataSource = resultDataSet.Tables["GasMeterTypes"];
            ddlGasMeterType.DataTextField = "Title";
            ddlGasMeterType.DataValueField = "Id";
            ddlGasMeterType.DataBind();

            ListItem newListItem = default(ListItem);
            newListItem = new ListItem("Please select", "-1");
            ddlGasMeterType.Items.Insert(0, newListItem);

            ddlElectricMeterType.DataSource = resultDataSet.Tables["ElectricMeterTypes"];
            ddlElectricMeterType.DataTextField = "Title";
            ddlElectricMeterType.DataValueField = "Id";
            ddlElectricMeterType.DataBind();
            ddlElectricMeterType.Items.Insert(0, newListItem);


        }
        #endregion

        #region load Data
        public void loadData()
        {
            BritishGasNotifcationBO britishGasBo = new BritishGasNotifcationBO();
            britishGasBo = objSession.BritishGasNotificationBo;
            BritishGasBL britishGasBl = new BritishGasBL(new BritishGasRepo());
            britishGasBl.getBritishGasStage2AndStage3Data(ref britishGasBo);
            objSession.BritishGasNotificationBo = britishGasBo;
            this.populateControls(britishGasBo);
        }
        #endregion

        #region populate controls
        /// <summary>
        /// populate the controls with values in bo
        /// </summary>
        /// <param name="britishGasBo"></param>
        private void populateControls(BritishGasNotifcationBO britishGasBo)
        {
            if (britishGasBo.GasMeterTypeId !=null && britishGasBo.GasMeterTypeId > 0)
            ddlGasMeterType.SelectedValue = britishGasBo.GasMeterTypeId.ToString() ;
            txtGasMeterReading.Text = britishGasBo.GasMeterReading.ToString();

            txtGasMeterReadingDate.Text = britishGasBo.GasMeterReadingDate!=null? Convert.ToDateTime( britishGasBo.GasMeterReadingDate).ToString("dd/MM/yyyy"):string.Empty ;
           if(britishGasBo.ElectricMeterTypeId !=null && britishGasBo.ElectricMeterTypeId > 0 )
            ddlElectricMeterType.SelectedValue =  britishGasBo.ElectricMeterTypeId.ToString() ;
            txtElectricMeterReading.Text = britishGasBo.ElectricMeterReading.ToString();
            txtElectricMeterReadingDate.Text = britishGasBo.ElectricMeterReadingDate != null ? Convert.ToDateTime(britishGasBo.ElectricMeterReadingDate).ToString("dd/MM/yyyy") : string.Empty;
            txtGasDebtAmount.Text = britishGasBo.GasDebtAmount.ToString();
            txtGasDebtAmountDate.Text = britishGasBo.GasDebtAmountDate != null ? Convert.ToDateTime(britishGasBo.GasDebtAmountDate).ToString("dd/MM/yyyy") : string.Empty; 
            txtElectricDebtAmount.Text = britishGasBo.ElectricDebtAmount.ToString();
            txtElectricDebtAmountDate.Text = britishGasBo.ElectricDebtAmountDate != null ? Convert.ToDateTime(britishGasBo.ElectricDebtAmountDate).ToString("dd/MM/yyyy") : string.Empty;
        }
        #endregion

        #region save Data
        public void saveData()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region update Data
        /// <summary>
        /// This function would update the british gas data in database
        /// </summary>
        public bool updateData()
        {
            Nullable<DateTime> nullDate = null;
            Nullable<decimal> nullDecimal = null;
            Nullable<Int64> nullInt64 = null;
            BritishGasNotifcationBO britishGasBo = new BritishGasNotifcationBO();
            britishGasBo = objSession.BritishGasNotificationBo;
            britishGasBo.GasMeterTypeId = Convert.ToInt32(ddlGasMeterType.SelectedValue);
            britishGasBo.GasMeterReading = txtGasMeterReading.Text.Trim() != string.Empty ? Convert.ToInt64(txtGasMeterReading.Text) : nullInt64;
            britishGasBo.GasMeterReadingDate = txtGasMeterReadingDate.Text.Trim() != string.Empty ? Convert.ToDateTime(txtGasMeterReadingDate.Text) : nullDate;
            britishGasBo.ElectricMeterTypeId = Convert.ToInt32(ddlElectricMeterType.SelectedValue);
            britishGasBo.ElectricMeterReading = txtElectricMeterReading.Text.Trim() != string.Empty ? Convert.ToInt64(txtElectricMeterReading.Text) : nullInt64;
            britishGasBo.ElectricMeterReadingDate = txtElectricMeterReadingDate.Text.Trim() != string.Empty ? Convert.ToDateTime(txtElectricMeterReadingDate.Text) : nullDate;
            britishGasBo.GasDebtAmount = txtGasDebtAmount.Text.Trim() != string.Empty ? Convert.ToDecimal(txtGasDebtAmount.Text) : nullDecimal;
            britishGasBo.GasDebtAmountDate = txtGasDebtAmountDate.Text.Trim() != string.Empty ? Convert.ToDateTime(txtGasDebtAmountDate.Text) : nullDate;
            britishGasBo.ElectricDebtAmount = txtElectricDebtAmount.Text.Trim() != string.Empty ? Convert.ToDecimal(txtElectricDebtAmount.Text) : nullDecimal;
            britishGasBo.ElectricDebtAmountDate = txtElectricDebtAmountDate.Text.Trim() != string.Empty ? Convert.ToDateTime(txtElectricDebtAmountDate.Text) : nullDate;
            britishGasBo.UserId = objSession.EmployeeId;
            britishGasBo.StageId = 2;
            BritishGasBL britishGasBl = new BritishGasBL(new BritishGasRepo());
            bool isSaved = britishGasBl.updateBritishGasStage2Data(ref britishGasBo);
            objSession.BritishGasNotificationBo = britishGasBo;
            return isSaved;
        }
        #endregion

        #region validate Data
        public bool validateData()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region reset controls
        public void resetControls()
        {
            throw new NotImplementedException();
        }
        #endregion





        #endregion
    }
}