﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="BritishGasNotificationCompleteTab.ascx.cs"
    Inherits="PropertyDataRestructure.UserControls.Void.BritishGasNotificationCompleteTab" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<link href="../../../Styles/Site.css" rel="stylesheet" media="screen" />
<link href="../../../Styles/default.css" rel="stylesheet" type="text/css" />
<asp:UpdatePanel runat="server" ID="updPanelAppointmentToBeArranged">
    <ContentTemplate>
        <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" />
        <div style="height: 580px; overflow: auto;">
            <cc1:PagingGridView ID="grdComplete" runat="server" AllowPaging="false" AllowSorting="true"
                AutoGenerateColumns="False" BorderStyle="Solid" BorderWidth="0px" CellPadding="4"
                GridLines="None" PageSize="10" Width="100%" PagerSettings-Visible="false" OnRowDataBound="grdComplete_RowDataBound"
                HeaderStyle-Font-Underline="false" ShowHeaderWhenEmpty="true" EmptyDataText="No Record Found"
                OnSorting="grdComplete_Sorting">
                <Columns>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgbtnTenantInfo" runat="server" CommandArgument='<%# Eval("CustomerId") %>'
                                ImageUrl='../../../Images/rec.png' BorderStyle="None" BorderWidth="0px" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Stage:" SortExpression="NotificationStageId">
                        <ItemTemplate>
                            <asp:Label ID="lblStage" runat="server" Text='<%# Bind("NotificationStageId") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Address:" SortExpression="Address">
                        <ItemTemplate>
                            <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Postcode:" SortExpression="Postcode">
                        <ItemTemplate>
                            <asp:Label ID="lblPostcode" runat="server" Text='<%# Bind("Postcode") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Termination:" SortExpression="Termination">
                        <ItemTemplate>
                            <asp:Label ID="lblTermination" runat="server" Text='<%# Bind("Termination") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="F/W Address:" SortExpression="FWAddress">
                        <ItemTemplate>
                            <asp:Label ID="lblFWAddress" runat="server" Text='<%# Bind("FWAddress") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Status:" SortExpression="NotificationStatusTitle">
                        <ItemTemplate>
                            <asp:Label ID="lblNotificationStatusTitle" runat="server" Text='<%# Bind("NotificationStatusTitle") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>


                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                           <asp:HiddenField ID="hdnIsNotificationSent" Value='<%# Bind("isNotificationSent") %>'
                                runat="server" />
                            <asp:HiddenField ID="hdnCustomerId" Value='<%# Bind("CustomerId") %>' runat="server" />
                            <asp:HiddenField ID="hdnPropertyId" Value='<%# Bind("PropertyId") %>' runat="server" />
                            <asp:HiddenField ID="hdnTenancyId" Value='<%# Bind("TenancyId") %>' runat="server" />
                            <asp:ImageButton ID="imgBtnPdf" runat="server" BorderStyle="None" BorderWidth="0px"
                                ImageUrl='../../../Images/pdf.png' CommandArgument='<%# Eval("NotificationId") %>'
                                OnClick="imgBtnPdfNotification_Click" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#ffffff" CssClass="table-head" Font-Bold="True" ForeColor="black"
                    HorizontalAlign="Left" Font-Underline="false" />
                <AlternatingRowStyle BackColor="#E8E9EA" Wrap="True" />
            </cc1:PagingGridView>
        </div>
        <asp:Panel runat="server" ID="pnlPagination" Visible="true" Style="border-top: 1px solid  #000;
            vertical-align: middle; padding: 10px 0">
            <table style="width: 57%; margin: 0 auto">
                <tbody>
                    <tr>
                        <td>
                            <asp:LinkButton ID="lnkbtnPagerFirst" Text="" runat="server" CommandName="Page" CommandArgument="First"
                                OnClick="lnkbtnPager_Click" Style="text-decoration: none;">&lt;&lt;First</asp:LinkButton>
                            &nbsp;
                        </td>
                        <td>
                            <asp:LinkButton ID="lnkbtnPagerPrev" Text="" runat="server" CommandName="Page" CommandArgument="Prev"
                                OnClick="lnkbtnPager_Click" Style="text-decoration: none;">&lt;Prev</asp:LinkButton>
                            &nbsp;
                        </td>
                        <td>
                            Page:&nbsp;
                            <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="currentPage" />
                            &nbsp;of&nbsp;
                            <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />. Records:
                            <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                            &nbsp;to&nbsp;
                            <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                            &nbsp;of&nbsp;
                            <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                        </td>
                        <td>
                            <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page"
                                CommandArgument="Next" OnClick="lnkbtnPager_Click" Style="text-decoration: none;" />
                            &nbsp;
                        </td>
                        <td>
                            <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page"
                                CommandArgument="Last" OnClick="lnkbtnPager_Click" Style="text-decoration: none;" />
                            &nbsp;
                        </td>
                    </tr>
                </tbody>
            </table>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
