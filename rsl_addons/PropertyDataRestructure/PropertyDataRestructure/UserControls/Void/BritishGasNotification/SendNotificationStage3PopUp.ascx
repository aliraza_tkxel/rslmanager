﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="SendNotificationStage3PopUp.ascx.cs"
    Inherits="PropertyDataRestructure.UserControls.Void.SendNotificationStage3PopUp" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Import Namespace="PDR_Utilities.Constants" %>

<script type="text/ecmascript">
    function ValidateIsTelephoneOrMobileEntered(sender, args) {
        var mobile = document.getElementById("txtMobile").value;
        if (args.Value !='') {

            return args.IsValid = true;
        }
        else if (mobile != '') {
            return args.IsValid = true;
        }
        else {
            return args.IsValid = false;
        }

    }
</script>
<asp:UpdatePanel runat="server" ID="updpanelArrangeInspection">
    <ContentTemplate>
        <asp:Panel runat="server" ID="pnlAddInspection">
            <div style="height: auto; clear: both; padding: 10px;">
                <table id="tblPausedFault" style="width: 450px; text-align: left; margin-right: 10px;">
                    <tr>
                        <td colspan="2">
                            <div style="float: left; width: 100%;">
                                <div style="float: left; width: 60%; font-weight: bold;">
                                    British Gas New Tenant Information</div>
                                <div style="margin-right: 22px;">
                                    <asp:Button ID="btnBack" runat="server" CssClass="margin_right" 
                                        Text=" < Back to Stage 2" onclick="btnBack2Stage1_Click" />
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <hr class="searchFaultHr" />
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="350px" />
                            <br />
                            <asp:CustomValidator ID="cusValTelephoneOrMobileEntered" runat="server" ClientValidationFunction="ValidateIsTelephoneOrMobileEntered"
                                ControlToValidate="txtTelephone" CssClass="Required" ValidationGroup="saveStage3"
                                Display="Dynamic"><%=UserMessageConstants.RequiredTelephoneOrMobile%></asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <b>Stage 3</b>
                            <br />
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            New Tenants Name:<span class="Required">*</span>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtTenantName" Width="276" />
                            <br />
                            <asp:RequiredFieldValidator ID="rfvTenantName" runat="server" ControlToValidate="txtTenantName"
                                Width="208" CssClass="Required" ValidationGroup="saveStage3" Display="Dynamic"><%=UserMessageConstants.RequiredTenantName%></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            DoB:
                        </td>
                        <td>
                            <asp:TextBox ID="txtDob" runat="server" Style="width: 130px !important;"></asp:TextBox>
                            <ajax:CalendarExtender ID="calExtenderDob" runat="server" Format="dd/MM/yyyy" PopupButtonID="imgBtnDob"
                                PopupPosition="Right" TargetControlID="txtDob" TodaysDateFormat="dd/MM/yyyy">
                            </ajax:CalendarExtender>
                            <asp:ImageButton ID="imgBtnDob" runat="server" ImageUrl="~/Images/calendar.png" Style="vertical-align: bottom;
                                border: none;" />
                            <br />
                            <asp:RequiredFieldValidator ID="rfvDob" runat="server" ControlToValidate="txtDob"
                                CssClass="Required" ValidationGroup="saveStage3" Display="Dynamic"><%=UserMessageConstants.RequiredDob%></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Telephone:
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtTelephone" Width="276" ClientIDMode="Static" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Mobile:
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtMobile" Width="276" ClientIDMode="Static" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Date Occupancy Begins:<span class="Required">*</span>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtNewTenantOccupancyDate" Width="130" />
                            <ajax:CalendarExtender ID="calExtNewTenantOccupancyDate" runat="server" Format="dd/MM/yyyy"
                                PopupButtonID="imgBtnNewTenantOccupancyDate" PopupPosition="Right" TargetControlID="txtNewTenantOccupancyDate"
                                TodaysDateFormat="dd/MM/yyyy">
                            </ajax:CalendarExtender>
                            <asp:ImageButton ID="imgBtnNewTenantOccupancyDate" runat="server" ImageUrl="~/Images/calendar.png"
                                Style="vertical-align: bottom; border: none;" />
                                <br />
                            <asp:RequiredFieldValidator ID="rfvOccupancyDate" runat="server" ControlToValidate="txtNewTenantOccupancyDate"
                                CssClass="Required" ValidationGroup="saveStage3" Display="Dynamic"><%=UserMessageConstants.RequiredOccupancyDate%></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Previous Address:
                        </td>
                        <td>
                            <asp:TextBox ID="txtNewTenantPreviousAddress1" runat="server" Width="276px" />
                            <ajax:TextBoxWatermarkExtender ID="txtNewTenantPreviousAddress1WatermarkExtender"
                                runat="server" TargetControlID="txtNewTenantPreviousAddress1" WatermarkCssClass="Watermark"
                                WatermarkText="Address1">
                            </ajax:TextBoxWatermarkExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <asp:TextBox ID="txtNewTenantPreviousAddress2" runat="server" Width="276px" />
                            <ajax:TextBoxWatermarkExtender ID="txtNewTenantPreviousAddress2WatermarkExtender1"
                                runat="server" TargetControlID="txtNewTenantPreviousAddress2" WatermarkCssClass="Watermark"
                                WatermarkText="Address2">
                            </ajax:TextBoxWatermarkExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <asp:TextBox ID="txtNewTenantPreviousTownCity" runat="server" Width="276px" />
                            <ajax:TextBoxWatermarkExtender ID="txtNewTenantPreviousTownCityWatermarkExtender"
                                runat="server" TargetControlID="txtNewTenantPreviousTownCity" WatermarkCssClass="Watermark"
                                WatermarkText="Town/City">
                            </ajax:TextBoxWatermarkExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <asp:TextBox ID="txtNewTenantPreviousPostCode" runat="server" Width="276px" />
                            <ajax:TextBoxWatermarkExtender ID="txtNewTenantPreviousPostCodeWatermarkExtender"
                                runat="server" TargetControlID="txtNewTenantPreviousPostCode" WatermarkCssClass="Watermark"
                                WatermarkText="PostCode">
                            </ajax:TextBoxWatermarkExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Gas Meter Reading:
                        </td>
                        <td>
                            <asp:TextBox ID="txtGasMeterReading" runat="server" Width="100px" />
                            As At:
                            <asp:TextBox ID="txtGasMeterReadingDate" runat="server" Style="width: 100px !important;"></asp:TextBox>
                            <ajax:CalendarExtender ID="calExtGasMeterReadingDate" runat="server" Format="dd/MM/yyyy"
                                PopupButtonID="imgBtnGasMeterReadingDate" PopupPosition="Right" TargetControlID="txtGasMeterReadingDate"
                                TodaysDateFormat="dd/MM/yyyy">
                            </ajax:CalendarExtender>
                            <asp:ImageButton ID="imgBtnGasMeterReadingDate" runat="server" ImageUrl="~/Images/calendar.png"
                                Style="vertical-align: bottom; border: none;" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Electric Meter Reading:
                        </td>
                        <td>
                            <asp:TextBox ID="txtElectricMeterReading" runat="server" Width="100px" />
                            As At:
                            <asp:TextBox ID="txtElectricMeterReadingDate" runat="server" Style="width: 100px !important;"></asp:TextBox>
                            <ajax:CalendarExtender ID="calExtElectricMeterReadingDate" runat="server" Format="dd/MM/yyyy"
                                PopupButtonID="imgBtnElectricMeterReadingDate" PopupPosition="Right" TargetControlID="txtElectricMeterReadingDate"
                                TodaysDateFormat="dd/MM/yyyy">
                            </ajax:CalendarExtender>
                            <asp:ImageButton ID="imgBtnElectricMeterReadingDate" runat="server" ImageUrl="~/Images/calendar.png"
                                Style="vertical-align: bottom; border: none;" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <br />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div style="float: right; width: 100%;">
                                <asp:Button ID="btnSaveSend" runat="server" CssClass="margin_right20" Text="Save & Send Stage 3 Notification"
                                    ValidationGroup="saveStage3" OnClick="btnSaveSend_Click" />
                                <asp:Button ID="btnCancel" runat="server" CssClass="margin_right20" Text="Cancel" />
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
    </ContentTemplate>
     <Triggers>
    <asp:PostBackTrigger ControlID="btnSaveSend" />
    </Triggers> 
</asp:UpdatePanel>
