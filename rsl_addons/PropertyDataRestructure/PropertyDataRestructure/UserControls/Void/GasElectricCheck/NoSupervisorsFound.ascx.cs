﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PDR_BusinessLogic.Scheduling;
using PDR_DataAccess.Scheduling;
using System.Data;
using PDR_Utilities.Constants;
using PropertyDataRestructure.Base;
using PDR_BusinessObject.TradeAppointment;
using PDR_Utilities.Helpers;
using PropertyDataRestructure.Interface;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_BusinessObject.VoidAppointment;
using AjaxControlToolkit;
using PDR_BusinessObject.BritishGas;
using System.Text;
using System.IO;
using PDR_BusinessObject.ICalFile;
using System.Net;
using System.Threading;
using PDR_BusinessObject.LookUp;

namespace PropertyDataRestructure.UserControls.Void.GasElectricCheck
{
    public partial class NoSupervisorsFound : UserControlBase
    {
        public String text;
        public Action delegateRefreshParentChecksGrid { get; set; }

        public void setText(String t)
        {
            text = t;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void Button1_Click(object sender, EventArgs e)
        {

        }
        public void populatePreValues(int journalId, string appointmentType, string appointmentNotes = null, bool IsRearrange = false)
        {
        }

        public void loadData()
        {
        }
    }
}