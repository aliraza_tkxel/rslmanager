﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PDR_BusinessLogic.Scheduling;
using PDR_DataAccess.Scheduling;
using System.Data;
using PDR_Utilities.Constants;
using PropertyDataRestructure.Base;
using PDR_BusinessObject.TradeAppointment;
using PDR_Utilities.Helpers;
using PropertyDataRestructure.Interface;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_BusinessObject.VoidAppointment;
using AjaxControlToolkit;
using PDR_BusinessObject.BritishGas;
using System.Text;
using System.IO;
using PDR_BusinessObject.ICalFile;
using System.Net;
using System.Threading;
using PDR_BusinessObject.LookUp;

namespace PropertyDataRestructure.UserControls.Void
{
    public partial class ScheduleChecksPopUp : UserControlBase, IAddEditPage
    {
        public Action delegateRefreshParentChecksGrid { get; set; }
        #region Events
        #region Page load event
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                uiMessage.hideMessage();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region btnSave Click event
        /// <summary>
        /// btnSave Click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlOperative.SelectedIndex > 0)
                {
                    saveData();
                    if (this.delegateRefreshParentChecksGrid != null)
                    {
                        this.delegateRefreshParentChecksGrid();
                    }
                    this.sendConfirmAppointmentEmail();
                    btnSave.Enabled = false;
                }
                else
                {
                    uiMessage.showErrorMessage(UserMessageConstants.SelectSupervisor);
                    uiMessage.isError = true;
                }

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
                ModalPopupExtender mdlpopupArrangeGasElectricCheck = (ModalPopupExtender)Parent.FindControl("mdlpopupArrangeGasElectricCheck");
                mdlpopupArrangeGasElectricCheck.Show();
            }
        }
        #endregion

        #region btnCancel Click event
        /// <summary>
        /// btnCancel Click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                //hide the modlpopup.
                AjaxControlToolkit.ModalPopupExtender mdlpopupArrangeGasElectricCheck = (AjaxControlToolkit.ModalPopupExtender)Parent.FindControl("mdlpopupArrangeGasElectricCheck");
                mdlpopupArrangeGasElectricCheck.Hide();

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion


        #region btnPopulate Click event
        /// <summary>
        /// btnPopulate Click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnPopulate_Click(object sender, EventArgs e)
        {
            try
            {
                string message = string.Empty;
                uiMessage.hideMessage();
                bool isValid = validateData();
                ModalPopupExtender mdlpopupArrangeGasElectricCheck = (ModalPopupExtender)Parent.FindControl("mdlpopupArrangeGasElectricCheck");

                if (isValid)
                {
                    getAvailableOperatives(DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy", null));
                   ddlOperative.Enabled = true;
                    this.loadData();
                }
                mdlpopupArrangeGasElectricCheck.Show();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                    ModalPopupExtender mdlpopupArrangeGasElectricCheck = (ModalPopupExtender)Parent.FindControl("mdlpopupArrangeGasElectricCheck");
                    mdlpopupArrangeGasElectricCheck.Show();
                }

            }
        }
        #endregion

        #region btn cancel appointment
        protected void btnCancelAppointment_Click(object sender, EventArgs e)
        {
            try
            {

                int journalId = Convert.ToInt32(hdnJournal.Value);
                this.cancelAppointment(journalId);
                if (this.delegateRefreshParentChecksGrid != null)
                {
                    this.delegateRefreshParentChecksGrid();
                }
                this.sendCancelEmail(journalId);
            }
            catch (ThreadAbortException ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
                ModalPopupExtender mdlpopupArrangeGasElectricCheck = (ModalPopupExtender)Parent.FindControl("mdlpopupArrangeGasElectricCheck");
                mdlpopupArrangeGasElectricCheck.Show();
            }
        }
        #endregion

        #endregion

        #region Populate drop downs and hdn values
        /// <summary>
        /// Populate drop downs and hdn values
        /// </summary>
        /// <remarks></remarks>

        public void populatePreValues(int journalId, string appointmentType, string appointmentNotes = null, bool IsRearrange = false)
        {

            hdnJournal.Value = journalId.ToString();
            populateDropDowns();
            resetControls();
            hdnIsRearrange.Value = null;
            btnSave.Enabled = true;
            if (IsRearrange == true)
            {
                hdnIsRearrange.Value = IsRearrange.ToString();
                txtAppointmentNotes.Text = appointmentNotes;
                btnSave.Text = ApplicationConstants.RearrangeButtonText;
                btnCancelAppointment.Visible = true;
                btnCancelAppointment.Enabled = true;
            }
            else
            {
                btnSave.Text = ApplicationConstants.SaveButtonText;
                btnCancelAppointment.Visible = false;
            }
            if (appointmentType == "Gas Check" || appointmentType.Replace(" ", "") == PDR_Utilities.Enums.Enums.VoidAppointmentTypes.VoidGasCheck.ToString())
            {
                rdBtnTypeList.Items[1].Enabled = false;
                rdBtnTypeList.Items[1].Selected = false;
                rdBtnTypeList.Items[0].Enabled = true;
                rdBtnTypeList.Items[0].Selected = true;
            }
            else
            {
                rdBtnTypeList.Items[0].Selected = false;
                rdBtnTypeList.Items[0].Enabled = false;
                rdBtnTypeList.Items[1].Enabled = true;
                rdBtnTypeList.Items[1].Selected = true;

            }


        }

        #endregion

        #region IAddEditPage Implementation
        public void saveData()
        {
            DateTime appointmentStartDateTime = DateTime.Parse(txtDate.Text + " " + ddlStartTime.SelectedValue + ":" + ddlStartTimeMin.SelectedValue);
            DateTime appointmentEndDateTime = DateTime.Parse(txtDate.Text + " " + ddlEndTime.SelectedValue + ":" + ddlEndTimeMin.SelectedValue);
            double duration = (appointmentEndDateTime - appointmentStartDateTime).TotalHours;
            VoidAppointmentBO voidBo = new VoidAppointmentBO();
            voidBo.JournalId = Convert.ToInt32(hdnJournal.Value);
            voidBo.OperativeId = Convert.ToInt32(ddlOperative.SelectedValue);
            voidBo.ChecksType = rdBtnTypeList.SelectedValue;
            voidBo.AppointmentStartDate = DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy", null);
            voidBo.AppointmentEndDate = DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy", null);
            voidBo.StartTime = appointmentStartDateTime.ToShortTimeString();
            voidBo.EndTime = appointmentEndDateTime.ToShortTimeString();
            voidBo.Duration = duration;
            voidBo.UserId = objSession.EmployeeId;
            voidBo.AppointmentNotes = txtAppointmentNotes.Text.Trim();
            VoidSchedulingBL objVoidSchedulingBl = new VoidSchedulingBL(new SchedulingRepo());
            string isSaved = string.Empty;
            int appointmentId = Convert.ToInt32(ApplicationConstants.DefaultValue);
            if (hdnIsRearrange.Value != null && hdnIsRearrange.Value != string.Empty && Convert.ToBoolean(hdnIsRearrange.Value) == true)
            {
                isSaved = objVoidSchedulingBl.reScheduleVoidInspection(voidBo);
            }
            else
            {
                isSaved = objVoidSchedulingBl.scheduleGasElectricChecks(voidBo, ref appointmentId);
            }
            if (isSaved.Equals(ApplicationConstants.NotSaved))
            {
                uiMessage.showErrorMessage(UserMessageConstants.SaveAppointmentFailed);
            }
            else
            {
                uiMessage.showInformationMessage(UserMessageConstants.SaveAppointmentSuccessfully);
            }
            this.sendPushNotification(ApplicationConstants.GasElectricChecksAppointment, voidBo.AppointmentStartDate, voidBo.StartTime, voidBo.EndTime, voidBo.OperativeId);
        }

        public bool validateData()
        {
            bool isValid = false;
            string message = string.Empty;
            DateTime appointmentStartDateTime = DateTime.Parse(txtDate.Text + " " + ddlStartTime.SelectedValue + ":" + ddlStartTimeMin.SelectedValue);
            DateTime appointmentEndDateTime = DateTime.Parse(txtDate.Text + " " + ddlEndTime.SelectedValue + ":" + ddlEndTimeMin.SelectedValue);
            if (!string.IsNullOrEmpty(txtDate.Text))
            {
                isValid = true;
                if (DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy", null) < DateTime.Today.Date)
                {
                    message = UserMessageConstants.SelectFutureDate + "<br />";
                    uiMessage.showErrorMessage(message);
                    uiMessage.isError = true;
                    isValid = false;
                }

                if (!GeneralHelper.isWeekend(DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy", null)))
                {
                    message = UserMessageConstants.WeekendDate;
                    uiMessage.showErrorMessage(message);
                    uiMessage.isError = true;
                    isValid = false;
                }

            }
            else
            {
                message = UserMessageConstants.InvalidStartDate + "<br />";
                uiMessage.showErrorMessage(message);
                uiMessage.isError = true;
                isValid = false;
            }
            if (appointmentStartDateTime.TimeOfDay >= appointmentEndDateTime.TimeOfDay)
            {
                message = UserMessageConstants.AppointmentTime + "<br />";
                uiMessage.showErrorMessage(message);
                uiMessage.isError = true;
                isValid = false;
            }
            return isValid;
        }

        public void resetControls()
        {
            txtDate.Text = string.Empty;
            txtAppointmentNotes.Text = string.Empty;
            ddlStartTime.SelectedIndex = 0;
            //ddlStartTimeMin.SelectedIndex = 0;
            ddlEndTime.SelectedIndex = 0;
            //ddlEndTimeMin.SelectedIndex = 0;           
            ddlOperative.SelectedIndex = 0;
            ddlOperative.Enabled = false;
        }

        public void loadData()
        {

            DateTime appointmentStartDateTime = DateTime.Parse(txtDate.Text + " " + ddlStartTime.SelectedValue + ":" + ddlStartTimeMin.SelectedValue);
            DateTime appointmentEndDateTime = DateTime.Parse(txtDate.Text + " " + ddlEndTime.SelectedValue + ":" + ddlEndTimeMin.SelectedValue);
            DateTime startDate = DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy", null);
            SchedulingBL schedulingBl = new SchedulingBL(new SchedulingRepo());
            //get Available Operatives Dataset from session which  stored in  getAvailableOperatives method. 
            DataSet OperativesDataSet = new DataSet();
            OperativesDataSet = objSession.AvailableOperativesDs;
            List<LookUpBO> operativesList = schedulingBl.getVoidAvailableOperative(OperativesDataSet, startDate, appointmentStartDateTime, appointmentEndDateTime);
            if (operativesList.Count == 0)
            {
                ModalPopupExtender mdlpopupArrangeGasElectricCheck = (ModalPopupExtender)Parent.FindControl("mdlpopupArrangeGasElectricCheck");

                mdlpopupArrangeGasElectricCheck.Show();

                ModalPopupExtender mdlpopupNotFound = (ModalPopupExtender)Parent.FindControl("mdlpopupNotFound");

                mdlpopupNotFound.Show();
                if (rdBtnTypeList.SelectedItem.Text == "Gas")
                {
                    Label lblNotFoundMessage = (Label)Parent.FindControl("lblNotFoundMessage");
                    lblNotFoundMessage.Text = UserMessageConstants.GasManagerNotFound;
                    throw new Exception(UserMessageConstants.OperativesNotFound);
                }
                else
                {

                    Label lblNotFoundMessage = (Label)Parent.FindControl("lblNotFoundMessage");
                    lblNotFoundMessage.Text = UserMessageConstants.ElectricManagerNotFound;

                    throw new Exception(UserMessageConstants.OperativesNotFound);
                }
            }
            else
            {
                populateDropDown(operativesList);
            }
        }

        public void populateDropDown(DropDownList ddl)
        {

        }
        public void populateDropDown(List<LookUpBO> operativesList)
        {
            ddlOperative.DataSource = operativesList;
            ddlOperative.DataValueField = "EmployeeId";
            ddlOperative.DataTextField = "FullName";
            ddlOperative.DataBind();
            ListItem newListItem = default(ListItem);
            newListItem = new ListItem("Please select", "-1");
            ddlOperative.Items.Insert(0, newListItem);
        }
        public void populateDropDowns()
        {

            GeneralHelper.populateHoursDropDowns(ref ddlStartTime);
            GeneralHelper.populateHoursDropDowns(ref ddlEndTime);
            GeneralHelper.populateMinDropDowns(ref ddlStartTimeMin);
            GeneralHelper.populateMinDropDowns(ref ddlEndTimeMin);

        }



        #endregion

        #region "get Available Operatives"
        /// <summary>
        /// this function 'll get the available operatives based on following: 
        /// trade ids of component, property id, start date 
        /// </summary>    
        /// <returns></returns>
        /// <remarks></remarks>
        private DataSet getAvailableOperatives(DateTime startDate)
        {
            SchedulingBL schedulingBl = new SchedulingBL(new SchedulingRepo());
            DataSet resultDataSet = new DataSet();
            DataSet allTradesDataSet = new DataSet();

            string expression = "val='Electrics (Full)' OR val='Electrics (Part)'";
            if (rdBtnTypeList.SelectedItem.Text   == "Gas")
            {
                expression = "val='Gas (Full)' OR val='Gas (Part)'";            
            }
            //string expression = "Id where Description = ''";
            //Get Trade Ids for Electrics(Full) and Electrics (Part).
            schedulingBl.getAllTrades(ref allTradesDataSet);
            var trades = allTradesDataSet.Tables[0].Select(expression);
            string tradeIds = string.Empty;
            foreach (DataRow t in trades)
            {
                tradeIds += t["id"].ToString() + ",";
            }

            tradeIds = tradeIds.Substring(0, tradeIds.Length - 1);

            //this function 'll get the available operatives based on following: trade ids will be null, start date
            schedulingBl.getAvailableOperatives(ref resultDataSet, tradeIds, ApplicationConstants.VoidMsatType, startDate);
            if (resultDataSet.Tables[0].Rows.Count == 0)
            {
                ModalPopupExtender mdlpopupArrangeGasElectricCheck = (ModalPopupExtender)Parent.FindControl("mdlpopupArrangeGasElectricCheck");

                mdlpopupArrangeGasElectricCheck.Show();

                ModalPopupExtender mdlpopupNotFound = (ModalPopupExtender)Parent.FindControl("mdlpopupNotFound");

                mdlpopupNotFound.Show();
                if (rdBtnTypeList.SelectedItem.Text == "Gas")
                {
                    Label lblNotFoundMessage = (Label)Parent.FindControl("lblNotFoundMessage");
                    lblNotFoundMessage.Text = UserMessageConstants.GasManagerNotFound;
                    throw new Exception(UserMessageConstants.OperativesNotFound);
                }
                else
                {
                    
                    Label lblNotFoundMessage = (Label)Parent.FindControl("lblNotFoundMessage");
                    lblNotFoundMessage.Text = UserMessageConstants.ElectricManagerNotFound;

                    throw new Exception(UserMessageConstants.OperativesNotFound);
                 }
           }
            else
            {
                objSession.AvailableOperativesDs = resultDataSet;
            }
            return resultDataSet;
        }
        #endregion

        #region cancel Appointment
        private void cancelAppointment(int journalId)
        {
            VoidSchedulingBL objSchedulingBL = new VoidSchedulingBL(new SchedulingRepo());
            int employeeId = objSession.EmployeeId;
            int isCancelled = objSchedulingBL.cancelGasElectricAppointment(journalId, employeeId);

            if (isCancelled == ApplicationConstants.Cancelled)
            {
                uiMessage.showInformationMessage(UserMessageConstants.AppointmentCancelledSuccessfully);
                btnCancelAppointment.Enabled = false;
                btnSave.Enabled = false;

            }
            else
            {
                uiMessage.showErrorMessage(UserMessageConstants.AppointmentCancelledFailed);
            }

        }
        #endregion

        #region "Send Push Notifications"

        public bool sendPushNotification(string appointmentType, DateTime appointmentDate, String appointmentStartTime, String appointmentEndTime, int operatorId)
        {
            try
            {
                string appointmentTypeDesc = string.Empty;
                string appointmentMessage = string.Empty;

                appointmentTypeDesc = "New " + appointmentType + " Scheduled";
                appointmentMessage += "BHG Property Manager" + "\\n\\n";
                appointmentMessage += appointmentTypeDesc + "\\n\\n";
                appointmentMessage += appointmentDate.ToString("d MMM yyyy");
                appointmentMessage += appointmentStartTime + " to ";
                appointmentMessage += appointmentEndTime + "\\n";

                appointmentMessage = HttpUtility.UrlEncode(appointmentMessage);

                string URL = string.Format("{0}?appointmentMessage={1}&operatorId={2}", GeneralHelper.getPushNotificationAddress(HttpContext.Current.Request.Url.AbsoluteUri), appointmentMessage, operatorId);
                WebRequest request = System.Net.WebRequest.Create(URL);
                request.Credentials = CredentialCache.DefaultCredentials;
                WebResponse response = request.GetResponse();
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                string responseFromServer = reader.ReadToEnd();
                reader.Close();
                response.Close();

                if (responseFromServer.ToString().Equals("true"))
                {
                    return true;
                }

                return false;

            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
                return false;
            }

        }

        #endregion

        #region send Cancel Appointment Email
        /// <summary>
        /// send Cancel Appointment Email
        /// </summary>
        /// <remarks></remarks>

        private void sendCancelEmail(int journalId)
        {

            DataSet resultDataset = new DataSet();
            VoidSchedulingBL objSchedulingBL = new VoidSchedulingBL(new SchedulingRepo());
            objSchedulingBL.getAppointmentDetailByJournalId(ref resultDataset, journalId);

            if (resultDataset != null && resultDataset.Tables[0].Rows.Count > 0)
            {
                string operativeName = string.Empty;
                string operativeEmail = string.Empty;
                operativeName = resultDataset.Tables[0].Rows[0]["OperativeName"].ToString();
                if (resultDataset.Tables[0].Rows[0]["Email"].Equals(DBNull.Value))
                {
                    operativeEmail = string.Empty;
                }
                else
                {
                    operativeEmail = resultDataset.Tables[0].Rows[0]["Email"].ToString();
                }
                string aptType = rdBtnTypeList.SelectedItem.Text + " Check";
                string body = this.populateCancelAppointmentEmailBody(resultDataset.Tables[0], aptType);
                string subject = aptType + " Appointment Cancelled:" + resultDataset.Tables[0].Rows[0]["Ref"].ToString();
                string recepientEmail = operativeEmail;

                try
                {

                    if (ValidationHelper.isEmail(recepientEmail))
                    {

                        EmailHelper.sendHtmlFormattedEmail(operativeName, recepientEmail, subject, body);
                    }
                    else
                    {
                        uiMessage.showErrorMessage(UserMessageConstants.AppointmentCancelledEmailError + " " + UserMessageConstants.InvalidEmail);
                    }
                }
                catch (Exception ex)
                {
                    uiMessage.isError = true;
                    uiMessage.showErrorMessage(UserMessageConstants.AppointmentCancelledEmailError + ex.Message);


                    if (uiMessage.isExceptionLogged == false)
                    {
                        ExceptionPolicy.HandleException(ex, "Exception Policy");
                    }

                }
            }
            else
            {
                uiMessage.showErrorMessage(UserMessageConstants.AppointmentCancelledEmailError + " " + UserMessageConstants.ProblemLoadingData);
            }

        }
        #endregion

        #region "Populate Email body for cancel appointment"
        /// <summary>
        /// Populate Email body
        /// </summary>
        /// <param name="objMeAppointmentBO"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        private string populateCancelAppointmentEmailBody(DataTable resultDt, string appointmentType)
        {
            StringBuilder body = new StringBuilder();
            StreamReader reader = new StreamReader(Server.MapPath("~/Email/AppointmentCancelled.html"));
            body.Append(reader.ReadToEnd().ToString());
            body.Replace("{Operative}", resultDt.Rows[0]["OperativeName"].ToString());
            body.Replace("{AppointmentType}", appointmentType);
            body.Replace("{Scheduler}", objSession.UserFullName);
            body.Replace("{Address}", resultDt.Rows[0]["Address"].ToString());
            body.Replace("{TownCity}", resultDt.Rows[0]["TOWNCITY"].ToString());
            body.Replace("{County}", resultDt.Rows[0]["COUNTY"].ToString());
            body.Replace("{PostCode}", resultDt.Rows[0]["POSTCODE"].ToString());
            body.Replace("{Termination}", resultDt.Rows[0]["Termination"].ToString());
            body.Replace("{Relet}", resultDt.Rows[0]["Relet"].ToString());
            body.Replace("{StartDate}", resultDt.Rows[0]["StartDate"].ToString());
            body.Replace("{StartTime}", resultDt.Rows[0]["StartTime"].ToString());
            body.Replace("{EndTime}", resultDt.Rows[0]["EndTime"].ToString());


            return body.ToString();
        }
        #endregion

        #region send confirm Appointment Email
        /// <summary>
        /// send confirm Appointment Email
        /// </summary>
        /// <remarks></remarks>

        private void sendConfirmAppointmentEmail()
        {
            int journalId = Convert.ToInt32(hdnJournal.Value);
            DataSet resultDataset = new DataSet();
            VoidSchedulingBL objSchedulingBL = new VoidSchedulingBL(new SchedulingRepo());
            objSchedulingBL.getAppointmentDetailByJournalId(ref resultDataset, journalId);

            if (resultDataset != null && resultDataset.Tables[0].Rows.Count > 0)
            {
                string operativeName = string.Empty;
                string operativeEmail = string.Empty;
                operativeName = resultDataset.Tables[0].Rows[0]["OperativeName"].ToString();
                if (resultDataset.Tables[0].Rows[0]["Email"].Equals(DBNull.Value))
                {
                    operativeEmail = string.Empty;
                }
                else
                {
                    operativeEmail = resultDataset.Tables[0].Rows[0]["Email"].ToString();
                }
                string aptType = rdBtnTypeList.SelectedItem.Text + " Check";
                string body = this.populateConfirmAppointmentEmailBody(resultDataset.Tables[0], aptType);
                string subject = aptType + " Appointment Arranged:" + resultDataset.Tables[0].Rows[0]["Ref"].ToString();
                string recepientEmail = operativeEmail;

                try
                {

                    if (ValidationHelper.isEmail(recepientEmail))
                    {

                        EmailHelper.sendHtmlFormattedEmail(operativeName, recepientEmail, subject, body);
                    }
                    else
                    {
                        uiMessage.showErrorMessage(UserMessageConstants.AppointmentArrangedSavedEmailError + " " + UserMessageConstants.InvalidEmail);
                    }
                }
                catch (Exception ex)
                {
                    uiMessage.isError = true;
                    uiMessage.showErrorMessage(UserMessageConstants.AppointmentArrangedSavedEmailError + ex.Message);

                    if (uiMessage.isExceptionLogged == false)
                    {
                        ExceptionPolicy.HandleException(ex, "Exception Policy");
                    }
                }
            }
            else
            {
                uiMessage.showErrorMessage(UserMessageConstants.AppointmentArrangedSavedEmailError + " " + UserMessageConstants.ProblemLoadingData);
            }

        }
        #endregion

        #region Populate Email body for Confirm appointment
        /// <summary>
        /// Populate Email body for Confirm appointment
        /// </summary>
        /// <param name="objMeAppointmentBO"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        private string populateConfirmAppointmentEmailBody(DataTable resultDt, string appointmentType)
        {
            StringBuilder body = new StringBuilder();
            StreamReader reader = new StreamReader(Server.MapPath("~/Email/VoidAppointmentArranged.html"));
            body.Append(reader.ReadToEnd().ToString());
            body.Replace("{Operative}", resultDt.Rows[0]["OperativeName"].ToString());
            body.Replace("{AppointmentType}", appointmentType);
            body.Replace("{Scheduler}", objSession.UserFullName);
            body.Replace("{Address}", resultDt.Rows[0]["Address"].ToString());
            body.Replace("{TownCity}", resultDt.Rows[0]["TOWNCITY"].ToString());
            body.Replace("{County}", resultDt.Rows[0]["COUNTY"].ToString());
            body.Replace("{PostCode}", resultDt.Rows[0]["POSTCODE"].ToString());
            body.Replace("{Termination}", resultDt.Rows[0]["Termination"].ToString());
            body.Replace("{Relet}", resultDt.Rows[0]["Relet"].ToString());
            body.Replace("{StartDate}", resultDt.Rows[0]["StartDate"].ToString());
            body.Replace("{StartTime}", resultDt.Rows[0]["StartTime"].ToString());
            body.Replace("{EndTime}", resultDt.Rows[0]["EndTime"].ToString());


            return body.ToString();
        }
        #endregion
    }
}