﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ChecksArrangedTab.ascx.cs"
    Inherits="PropertyDataRestructure.UserControls.Void.ChecksArrangedTab" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register TagPrefix="ucScheduleChecks" TagName="ScheduleChecksPopUp" Src="~/UserControls/Void/GasElectricCheck/ScheduleChecksPopUp.ascx" %>
<%@ Register TagPrefix="ucNotFound" TagName="NotFound" Src="~/UserControls/Void/GasElectricCheck/NoSupervisorsFound.ascx" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<link href="../../../Styles/Site.css" rel="stylesheet" media="screen" />
<link href="../../../Styles/default.css" rel="stylesheet" type="text/css" />
<asp:UpdatePanel runat="server" ID="updPanelAppointmentToBeArranged">
    <ContentTemplate>
        <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" />
        <div style="height: 580px; overflow: auto;">
            <cc1:PagingGridView ID="grdChecksArranged" runat="server" AllowPaging="false" AllowSorting="true"
                AutoGenerateColumns="False" BorderStyle="Solid" BorderWidth="0px" CellPadding="4"
                GridLines="None" PageSize="10" Width="100%" PagerSettings-Visible="false" HeaderStyle-Font-Underline="false"
                ShowHeaderWhenEmpty="true" EmptyDataText="No Record Found" OnSorting="grdChecksToBeArranged_Sorting">
                <Columns>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgbtnAptbaTenantInfo" runat="server" CommandArgument='<%# Eval("JournalId") %>'
                                ImageUrl='<%# "~/Images/rec.png" %>' BorderStyle="None" BorderWidth="0px" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Address:" SortExpression="Address">
                        <ItemTemplate>
                            <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
                             <asp:HiddenField ID="hdnAppointmentNotes" runat="server" Value='<%# Bind("AppointmentNotes") %>' />
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Postcode:" SortExpression="Postcode">
                        <ItemTemplate>
                            <asp:Label ID="lblPostcode" runat="server" Text='<%# Bind("Postcode") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Tenant:" SortExpression="Tenant">
                        <ItemTemplate>
                            <asp:Label ID="lblTenant" runat="server" Text='<%# Bind("Tenant") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Type:" SortExpression="Type">
                        <ItemTemplate>
                            <asp:Label ID="lblType" runat="server" Text='<%# Bind("Type") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Termination:" SortExpression="Termination">
                        <ItemTemplate>
                            <asp:Label ID="lblTermination" runat="server" Text='<%# Bind("Termination") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Relet:" SortExpression="Relet">
                        <ItemTemplate>
                            <asp:Label ID="lblRelet" runat="server" Text='<%# Bind("Relet") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Surveyor:" SortExpression="Surveyor">
                        <ItemTemplate>
                            <asp:Label ID="lblSurveyor" runat="server" Text='<%# Bind("Surveyor") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Appointment:" SortExpression="Appointment">
                        <ItemTemplate>
                            <asp:Label ID="lblAppointment" runat="server" Text='<%# Bind("Appointment") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgBtnArrow" runat="server" BorderStyle="None" BorderWidth="0px" OnClick="imgBtnSchedule_Click"
                                ImageUrl='<%# "~/Images/aero.png" %>' CommandArgument='<%# Eval("JournalId") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#ffffff" CssClass="table-head" Font-Bold="True" ForeColor="black"
                    HorizontalAlign="Left" Font-Underline="false" />
                <AlternatingRowStyle BackColor="#E8E9EA" Wrap="True" />
            </cc1:PagingGridView>
        </div>
        <asp:Panel runat="server" ID="pnlPagination" Visible="true" Style="border-top: 1px solid  #000;
            vertical-align: middle; padding: 10px 0">
            <table style="width: 57%; margin: 0 auto">
                <tbody>
                    <tr>
                        <td>
                            <asp:LinkButton ID="lnkbtnPagerFirst" Text="" runat="server" CommandName="Page" CommandArgument="First"
                                OnClick="lnkbtnPager_Click" Style="text-decoration: none;">&lt;&lt;First</asp:LinkButton>
                            &nbsp;
                        </td>
                        <td>
                            <asp:LinkButton ID="lnkbtnPagerPrev" Text="" runat="server" CommandName="Page" CommandArgument="Prev"
                                OnClick="lnkbtnPager_Click" Style="text-decoration: none;">&lt;Prev</asp:LinkButton>
                            &nbsp;
                        </td>
                        <td>
                            Page:&nbsp;
                            <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="currentPage" />
                            &nbsp;of&nbsp;
                            <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />. Records:
                            <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                            &nbsp;to&nbsp;
                            <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                            &nbsp;of&nbsp;
                            <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                        </td>
                        <td>
                            <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page"
                                CommandArgument="Next" OnClick="lnkbtnPager_Click" Style="text-decoration: none;" />
                            &nbsp;
                        </td>
                        <td>
                            <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page"
                                CommandArgument="Last" OnClick="lnkbtnPager_Click" Style="text-decoration: none;" />
                            &nbsp;
                        </td>
                    </tr>
                </tbody>
            </table>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>

<ajax:ModalPopupExtender ID="mdlpopupArrangeGasElectricCheck" runat="server" PopupControlID="pnlScheduleChecksPopUp"
    TargetControlID="lblHiddenEntry" CancelControlID="imgBtnCancelPopup" BackgroundCssClass="modalBackground">
</ajax:ModalPopupExtender>
<asp:Label Text="" ID="lblHiddenEntry" runat="server" />
<asp:Panel ID="pnlScheduleChecksPopUp" runat="server" CssClass="modalPopupSchedular"
    Style="width: 410px; border-color: black; border-bottom-style: solid; border-width: 1px;">
    <asp:ImageButton ID="imgBtnCancelPopup" runat="server" Style="position: absolute;
        top: -12px; right: -12px; width: 22px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png"
        BorderWidth="0" />
    <ucScheduleChecks:ScheduleChecksPopUp ID="ucScheduleChecks" runat="server">
    </ucScheduleChecks:ScheduleChecksPopUp>
</asp:Panel>



<asp:Label ID="lblDisplayPopup" runat="server"></asp:Label>
<ajax:ModalPopupExtender ID="mdlPopUpChecksToBeArrangedTenantInfo" runat="server"
    PopupControlID="pnlChecksToBeArrangedTenantInfo" TargetControlID="lblDisplayPopup"
    CancelControlID="imgBtnCloseTenantInfo" BackgroundCssClass="modalBackground">
</ajax:ModalPopupExtender>
<asp:Panel ID="pnlChecksToBeArrangedTenantInfo" runat="server" BackColor="#A4DAF6"
    Style="padding: 10px; border: 1px solid gray;">
    <asp:ImageButton ID="imgBtnCloseTenantInfo" runat="server" Style="position: absolute;
        top: -10px; right: -10px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
    <asp:UpdatePanel ID="updpnlAddPopupContactInfo" runat="server">
        <ContentTemplate>
            <table id="tblTenantInfo" runat="server" class="TenantInfo" style="font-weight: bold;">
                <tr>
                    <td style="width: 0px; height: 0px;">
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
<ajax:ModalPopupExtender ID="mdlpopupNotFound" runat="server" PopupControlID="pnlNotFound"
    TargetControlID="lblNotFound" BackgroundCssClass="modalBackground tempB2">
</ajax:ModalPopupExtender>
<asp:Label Text="" ID="lblNotFound" runat="server" />
<asp:Panel ID="pnlNotFound" runat="server" CssClass="modalPopupSchedular tempB2"
    Style="width: 40%; border-color: black; border-bottom-style: solid; border-width: 1px;">
    <%--<ucNotFound:NotFound ID="ucNotFound" runat="server">
    </ucNotFound:NotFound>--%>
    <asp:Label ID="lblNotFoundMessage" runat="server" Text="There are currently no supervisors with the trades Electric (Full) or Electric (Part) selected in their employee record. Please notify HR!"></asp:Label><br />
    <asp:Button ID="btnContinueError" Style="margin-top: 10px; margin-bottom: 10px; margin-left: 44%;
        text-align: center" runat="server" Height="26px" Text="Continue" />
</asp:Panel>
