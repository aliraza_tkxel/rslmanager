﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VoidInspectionToBeArrangedTab.ascx.cs"
    Inherits="PropertyDataRestructure.UserControls.Void.VoidInspectionToBeArrangedTab" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<%@ Register TagPrefix="ucArrangeInspectionPopUp" TagName="ArrangeInspectionPopUp"
    Src="~/UserControls/Void/VoidInspection/ArrangeInspectionPopUp.ascx" %>
<%@ Register TagPrefix="stg1" TagName="Stage1Notification" Src="~/UserControls/Void/BritishGasNotification/SendNotificationStage1PopUp.ascx" %>
<%@ Register TagPrefix="stg2" TagName="Stage2Notification" Src="~/UserControls/Void/BritishGasNotification/SendNotificationStage2PopUp.ascx" %>
<%@ Register TagPrefix="stg3" TagName="Stage3Notification" Src="~/UserControls/Void/BritishGasNotification/SendNotificationStage3PopUp.ascx" %>
<link href="../../../Styles/Site.css" rel="stylesheet" media="screen" />
<link href="../../../Styles/default.css" rel="stylesheet" type="text/css" />
<%@ Import Namespace="PDR_Utilities.Constants" %>
<asp:UpdatePanel runat="server" ID="updPanelAppointmentToBeArranged">
    <ContentTemplate>
        <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" />
        <div style="height: 580px; overflow: auto;">
            <cc1:PagingGridView ID="grdInspectionToBeArranged" runat="server" AllowPaging="false"
                AllowSorting="true" AutoGenerateColumns="False" BorderStyle="Solid" BorderWidth="0px"
                CellPadding="4" GridLines="None" PageSize="10" Width="100%" PagerSettings-Visible="false"
                OnSorting="grdInspectionToBeArranged_Sorting" HeaderStyle-Font-Underline="false"
                ShowHeaderWhenEmpty="true" EmptyDataText="No Record Found">
                <Columns>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:HiddenField ID="hdnPropertyId" runat="server" Value='<%# Eval("PropertyId") %>' />
                            <asp:HiddenField ID="hdnCustomerId" runat="server" Value='<%# Eval("CustomerId") %>' />
                            <asp:HiddenField ID="hdnTenancyId" runat="server" Value='<%# Eval("TenancyId") %>' />
                            <asp:ImageButton ID="imgbtnAptbaTenantInfo" runat="server" CommandArgument='<%# Eval("TenancyId") %>'
                                ImageUrl='../../../Images/rec.png' BorderStyle="None" BorderWidth="0px" OnClick="imgbtnAptbaTenantInfo_Click" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Ref:" SortExpression="JournalId">
                        <ItemTemplate>
                            <asp:Label ID="lblRef" runat="server" Text='<%# Bind("Ref") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Address" SortExpression="Address">
                        <ItemTemplate>
                            <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Postcode" SortExpression="POSTCODE">
                        <ItemTemplate>
                            <asp:Label ID="lblPostCode" runat="server" Text='<%# Bind("Postcode") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Tenant:" SortExpression="Tenant">
                        <ItemTemplate>
                            <asp:Label ID="lblTenant" runat="server" Text='<%# Bind("Tenant") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Termination:" SortExpression="Termination">
                        <ItemTemplate>
                            <asp:Label ID="lblTermination" runat="server" Text='<%# Bind("Termination") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Relet:" SortExpression="Relet">
                        <ItemTemplate>
                            <asp:Label ID="lblRelet" runat="server" Text='<%# Bind("Relet") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgBtnSchedule" runat="server" BorderStyle="None" OnClick="imgBtnSchedule_Click"
                                BorderWidth="0px" ImageUrl='../../../Images/aero.png' CommandArgument='<%# Eval("JournalId") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#ffffff" CssClass="table-head" Font-Bold="True" ForeColor="black"
                    HorizontalAlign="Left" Font-Underline="false" />
                <AlternatingRowStyle BackColor="#E8E9EA" Wrap="True" />
            </cc1:PagingGridView>
        </div>
        <asp:Panel runat="server" ID="pnlPagination" Visible="true" Style="border-top: 1px solid  #000;
            vertical-align: middle; padding: 10px 0">
            <table style="width: 57%; margin: 0 auto">
                <tbody>
                    <tr>
                        <td>
                            <asp:LinkButton ID="lnkbtnPagerFirst" Text="" runat="server" CommandName="Page" CommandArgument="First"
                                OnClick="lnkbtnPager_Click" Style="text-decoration: none;">&lt;&lt;First</asp:LinkButton>
                            &nbsp;
                        </td>
                        <td>
                            <asp:LinkButton ID="lnkbtnPagerPrev" Text="" runat="server" CommandName="Page" CommandArgument="Prev"
                                OnClick="lnkbtnPager_Click" Style="text-decoration: none;">&lt;Prev</asp:LinkButton>
                            &nbsp;
                        </td>
                        <td>
                            Page:&nbsp;
                            <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="Required" />
                            &nbsp;of&nbsp;
                            <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />. Records:
                            <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                            &nbsp;to&nbsp;
                            <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                            &nbsp;of&nbsp;
                            <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                        </td>
                        <td>
                            <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page"
                                CommandArgument="Next" OnClick="lnkbtnPager_Click" Style="text-decoration: none;" />
                            &nbsp;
                        </td>
                        <td>
                            <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page"
                                CommandArgument="Last" OnClick="lnkbtnPager_Click" Style="text-decoration: none;" />
                            &nbsp;
                        </td>
                    </tr>
                </tbody>
            </table>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
<ajax:ModalPopupExtender ID="mdlpopupInspection" runat="server" PopupControlID="pnlVoidInspection"
    TargetControlID="lblHiddenEntry" CancelControlID="imgBtnCancelPopup" BackgroundCssClass="modalBackground">
</ajax:ModalPopupExtender>
<asp:Label Text="" ID="lblHiddenEntry" runat="server" />
<asp:Panel ID="pnlVoidInspection" runat="server" CssClass="modalPopupSchedular" Style="width: 410px;
    border-color: black; border-bottom-style: solid; border-width: 1px;">
    <asp:ImageButton ID="imgBtnCancelPopup" runat="server" Style="position: absolute;
        top: -12px; right: -12px; width: 22px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png"
        BorderWidth="0" />
    <ucArrangeInspectionPopUp:ArrangeInspectionPopUp ID="ucArrangeInspectionPopUp" runat="server" />
</asp:Panel>
<asp:Label ID="lblDispAddAppointmentPopup" runat="server"></asp:Label>
<ajax:ModalPopupExtender ID="mdlPopUpAppointmentToBeArrangedPhone" runat="server"
    PopupControlID="pnlAppointmentToBeArrangedTenantInfo" TargetControlID="lblDispAddAppointmentPopup"
    CancelControlID="imgBtnCloseTenantInfo" BackgroundCssClass="modalBackground">
</ajax:ModalPopupExtender>
<asp:Panel ID="pnlAppointmentToBeArrangedTenantInfo" runat="server" BackColor="#A4DAF6"
    Style="padding: 10px; border: 1px solid gray;">
    <asp:ImageButton ID="imgBtnCloseTenantInfo" runat="server" Style="position: absolute;
        top: -10px; right: -10px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
    <asp:UpdatePanel ID="updpnlAddPopupContactInfo" runat="server">
        <ContentTemplate>
            <table id="tblTenantInfo" runat="server" class="TenantInfo" style="font-weight: bold;">
                <tr>
                    <td style="width: 0px; height: 0px;">
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
<ajax:ModalPopupExtender ID="mdlpopupAskForGasNotification" runat="server" PopupControlID="pnlAskForGasNotification"
    TargetControlID="lblHiddenAskForGasNotificationEntry"
    BackgroundCssClass="modalBackground">
</ajax:ModalPopupExtender>
<asp:Label Text="" ID="lblHiddenAskForGasNotificationEntry" runat="server" />
<asp:Panel ID="pnlAskForGasNotification" runat="server" CssClass="modalPopupSchedular"
    Style="width: 380px; border-color: black; border-bottom-style: solid; border-width: 1px;">
    <asp:UpdatePanel ID="updPopupReportFault" runat="server">
        <ContentTemplate>
            <div style="height: auto; clear: both;">
                <table id="Table1" style="width: 400px; text-align: left; margin-right: 10px;">
                    <tr>
                        <td>
                            <b>Gas Void Notification</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <hr class="InspectionArrangeHr" />
                            <br />
                        </td>
                        <tr>
                            <td>
                                The Gas Void Notification should be sent to British Gas now, or this
                                <br />
                                can be sent at a later date.Please select the appropriate button:
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-left: 38px;">
                                <asp:Button ID="btnSendGasNotificationNow" runat="server" CssClass="margin_right10"
                                    Text="Send Notification Now" OnClick="btnSendGasNotificationNow_Click" />
                                <asp:Button ID="btnSendGasNotificationLater" runat="server" CssClass="margin_right10"
                                    Text="Send Notification Later" OnClick="btnSendGasNotificationLater_Click" />
                            </td>
                        </tr>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
<ajax:ModalPopupExtender ID="mdlBritishNotificationMessage" runat="server" PopupControlID="pnlBritishNotificationMessage"
    TargetControlID="lblHiddenEntryBritishNotificationMessage" CancelControlID="btnOkBritishNotificationMessagePopup"
    BackgroundCssClass="modalBackground">
</ajax:ModalPopupExtender>
<asp:Label Text="" ID="lblHiddenEntryBritishNotificationMessage" runat="server" />
<asp:Panel ID="pnlBritishNotificationMessage" runat="server" CssClass="modalPopupSchedular"
    Style="width: 380px; border-color: black; border-bottom-style: solid; border-width: 1px;">
    <asp:UpdatePanel ID="updPanelBritishNotificationMessage" runat="server">
        <ContentTemplate>
            <div style="height: auto; clear: both; text-align: center;">
                <div>
                    <%=UserMessageConstants.ErrorSavingBritishGasNotification%></div>
                <br />
                <div>
                    <asp:Button ID="btnOkBritishNotificationMessagePopup" runat="server" Text="OK" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
<ajax:ModalPopupExtender ID="mdlStage1Notification" runat="server" PopupControlID="pnlStage1Notification"
    TargetControlID="lblHiddenStage1NotificationEntry" CancelControlID="imgBtnCancelStageINotificationPopup"
    BackgroundCssClass="modalBackground">
</ajax:ModalPopupExtender>
<asp:Label Text="" ID="lblHiddenStage1NotificationEntry" runat="server" />
<asp:Panel ID="pnlStage1Notification" runat="server" CssClass="modalPopupSchedular"
    Style="width: 450px; border-color: black; border-bottom-style: solid; border-width: 1px;">
    <asp:ImageButton ID="imgBtnCancelStageINotificationPopup" runat="server" Style="position: absolute;
        top: -12px; right: -12px; width: 22px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png"
        BorderWidth="0" />
    <stg1:Stage1Notification ID="ucStage1Notification" runat="Server" />
</asp:Panel>

<ajax:ModalPopupExtender ID="mdlStage2Notification" runat="server" PopupControlID="pnlStage2Notification"
    TargetControlID="lblHiddenStage2NotificationEntry" CancelControlID="imgBtnCancelStage2NotificationPopup"
    BackgroundCssClass="modalBackground">
</ajax:ModalPopupExtender>
<asp:Label Text="" ID="lblHiddenStage2NotificationEntry" runat="server" />
<asp:Panel ID="pnlStage2Notification" runat="server" CssClass="modalPopupSchedular"
    Style="width: 454px; border-color: black; border-bottom-style: solid; border-width: 1px;">
    <asp:ImageButton ID="imgBtnCancelStage2NotificationPopup" runat="server" Style="position: absolute;
        top: -12px; right: -12px; width: 22px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png"
        BorderWidth="0" />
    <stg2:Stage2Notification ID="ucStage2Notification" runat="Server" />
</asp:Panel>

<ajax:ModalPopupExtender ID="mdlStage3Notification" runat="server" PopupControlID="pnlStage3Notification"
    TargetControlID="lblHiddenStage3NotificationEntry" CancelControlID="imgBtnCancelStage3NotificationPopup"
    BackgroundCssClass="modalBackground">
</ajax:ModalPopupExtender>
<asp:Label Text="" ID="lblHiddenStage3NotificationEntry" runat="server" />
<asp:Panel ID="pnlStage3Notification" runat="server" CssClass="modalPopupSchedular"
    Style="width: 454px; border-color: black; border-bottom-style: solid; border-width: 1px;">
    <asp:ImageButton ID="imgBtnCancelStage3NotificationPopup" runat="server" Style="position: absolute;
        top: -12px; right: -12px; width: 22px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png"
        BorderWidth="0" />
    <stg3:Stage3Notification ID="ucStage3Notification" runat="Server" />
</asp:Panel>
