﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ArrangePostVoidInspectionPopUp.ascx.cs"
    Inherits="PropertyDataRestructure.UserControls.Void.ArrangePostVoidInspectionPopUp" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register TagPrefix="stg1" TagName="Stage1Notification" Src="~/UserControls/Void/BritishGasNotification/SendNotificationStage1PopUp.ascx" %>
<asp:UpdatePanel runat="server" ID="updpanelArrangeInspection">
    <ContentTemplate>
        <asp:Panel runat="server" ID="pnlAddInspection">
            <div style="height: auto; clear: both; padding: 10px;">
                <table id="tblPausedFault" style="width: 400px; text-align: left; margin-right: 10px;">
                    <tr>
                        <td colspan="2">
                            <b>Schedule Inspection</b>
                            <asp:HiddenField ID="hdnJournal" runat="server" />
                            <asp:HiddenField  ID="hdnIsRearrange" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <hr class="searchFaultHr" />
                            <div style="margin-left: -20px!important;">
                                <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="350px" />
                            </div>
                        </td>
                        <tr>
                            <td>
                                Type:
                            </td>
                            <td>
                                <asp:RadioButtonList ID="chkTypeList" runat="server" RepeatDirection="Vertical" Style='margin-left: -5px;'>
                                    <asp:ListItem Text="Void Inspection" Value="1"   Enabled="false"/>
                                    <asp:ListItem Text="Post Void Inspection" Value="2" Enabled="true" Selected="True" />
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Date:<span class="Required">*</span>
                            </td>
                            <td>
                                <asp:TextBox ID="txtDate" runat="server" Style="width: 116px !important;"></asp:TextBox>
                                <ajax:CalendarExtender ID="clndrbuildDate" runat="server" Format="dd/MM/yyyy" PopupButtonID="imgCalDate"
                                    PopupPosition="Right" TargetControlID="txtDate" TodaysDateFormat="dd/MM/yyyy">
                                </ajax:CalendarExtender>
                                <asp:ImageButton ID="imgCalDate" runat="server" ImageUrl="~/Images/calendar.png"
                                    Style="vertical-align: bottom; border: none;" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Start Time:<span class="Required">*</span>
                            </td>
                            <td>
                                <asp:DropDownList runat="server" ID="ddlStartTime" Width="100">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="ddlStartTime"
                                    InitialValue="-1" ErrorMessage="*" Display="Dynamic" ValidationGroup="populate"
                                    CssClass="Required" />
                                <asp:DropDownList runat="server" ID="ddlStartTimeMin" Width="100">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                End Time:<span class="Required">*</span>
                            </td>
                            <td>
                                <asp:DropDownList runat="server" ID="ddlEndTime" Width="100">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="ddlEndTime"
                                    InitialValue="-1" ErrorMessage="*" Display="Dynamic" ValidationGroup="populate"
                                    CssClass="Required" />
                                <asp:DropDownList runat="server" ID="ddlEndTimeMin" Width="100">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div style="float: right; width: 50%; margin-right: 18px;">
                                    <asp:Button ID="btnPopulate" runat="server" CssClass="margin_right20" Text="Populate Supervisor"
                                        ValidationGroup="populate" OnClick="btnPopulate_Click" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Supervisor:<span class="Required">*</span>
                            </td>
                            <td>
                                <br />
                                <asp:DropDownList ID="ddlOperative" runat="server" Width="235px" Enabled="false">
                                    <asp:ListItem Selected="True" Text="Please Select" Value="-1"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator8" ControlToValidate="ddlOperative"
                                    InitialValue="-1" ErrorMessage="*" Display="Dynamic" ValidationGroup="save" CssClass="Required" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Appointment Notes:
                            </td>
                            <td>
                                <asp:TextBox ID="txtAppointmentNotes" runat="server" TextMode="MultiLine" Height="100"
                                    Width="230px" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                               <div style="float: right; width: 100%; margin-right: 18px;">
                                    <asp:Button Text="Save" runat="server" ID="btnSave" CssClass="margin_right20" OnClick="btnSave_Click" />
                                           <asp:Button Text="Cancel Appointment" ID="btnCancelAppointment" 
                                        CssClass="margin_right20" runat="server" Visible="false" 
                                        onclick="btnCancelAppointment_Click" />
                                    <asp:Button Text="Cancel" ID="btnClose" runat="server" CssClass="margin_right20"
                                        OnClick="btnCancel_Click" />
                                </div>
                            </td>
                        </tr>
                    </tr>
                </table>
            </div>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>

