﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PostVoidInspectionArrangedTab.ascx.cs"
    Inherits="PropertyDataRestructure.UserControls.Void.PostVoidInspectionArrangedTab" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<%@ Register TagPrefix="ucArrangeInspectionPopUp" TagName="ArrangeInspectionPopUp"
    Src="~/UserControls/Void/VoidInspection/ArrangePostVoidInspectionPopUp.ascx" %>
<link href="../../Styles/Site.css" rel="stylesheet" media="screen" />
<link href="../../Styles/default.css" rel="stylesheet" type="text/css" />
<asp:UpdatePanel runat="server" ID="updPanelAppointmentToBeArranged">
    <ContentTemplate>
        <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" />
        <div style="height: 580px; overflow: auto;">
            <cc1:PagingGridView ID="grdInspectionArranged" runat="server" AllowPaging="false"
                AllowSorting="true" AutoGenerateColumns="False" BorderStyle="Solid" BorderWidth="0px"
                CellPadding="4" GridLines="None" Width="100%" PagerSettings-Visible="false" HeaderStyle-Font-Underline="false"
                OnSorting="grdInspectionArranged_Sorting" ShowHeaderWhenEmpty="true" EmptyDataText="No Record Found">
                <Columns>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgbtnAptbaTenantInfo" runat="server" CommandArgument='<%# Eval("TenancyId") %>'
                                ImageUrl='<%# "~/Images/rec.png" %>' OnClick="imgbtnAptbaTenantInfo_Click" BorderStyle="None"
                                BorderWidth="0px" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Ref:" SortExpression="JournalId">
                        <ItemTemplate>
                            <asp:Label ID="lblRef" runat="server" Text='<%# Bind("Ref") %>'></asp:Label>
                            <asp:HiddenField ID="hdnAppointmentNotes" runat="server" Value='<%# Bind("APPOINTMENTNOTES") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Address:" SortExpression="Address">
                        <ItemTemplate>
                            <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Postcode" SortExpression="POSTCODE">
                        <ItemTemplate>
                            <asp:Label ID="lblPostCode" runat="server" Text='<%# Bind("Postcode") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Termination:" SortExpression="Termination">
                        <ItemTemplate>
                            <asp:Label ID="lblTermination" runat="server" Text='<%# Bind("Termination") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Relet:" SortExpression="Relet">
                        <ItemTemplate>
                            <asp:Label ID="lblRelet" runat="server" Text='<%# Bind("Relet") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Surveyor:" SortExpression="Surveyor">
                        <ItemTemplate>
                            <asp:Label ID="lblTenant" runat="server" Text='<%# Bind("Surveyor") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Appointment:" SortExpression="Appointment">
                        <ItemTemplate>
                            <asp:Label ID="lblAppointment" runat="server" Text='<%# Bind("Appointment") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgAppointmentToBeArrangedArrow" runat="server" BorderStyle="None"
                                BorderWidth="0px" ImageUrl='<%# "~/Images/aero.png" %>' CommandArgument='<%# Eval("JournalId") %>' OnClick="imgBtnSchedule_Click" />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#ffffff" CssClass="table-head" Font-Bold="True" ForeColor="black"
                    HorizontalAlign="Left" Font-Underline="false" />
                <AlternatingRowStyle BackColor="#E8E9EA" Wrap="True" />
            </cc1:PagingGridView>
        </div>
        <asp:Panel runat="server" ID="pnlPagination" Visible="true" Style="border-top: 1px solid  #000;
            vertical-align: middle; padding: 10px 0">
            <table style="width: 57%; margin: 0 auto">
                <tbody>
                    <tr>
                        <td>
                            <asp:LinkButton ID="lnkbtnPagerFirst" Text="" runat="server" CommandName="Page" CommandArgument="First"
                                OnClick="lnkbtnPager_Click" Style="text-decoration: none;">&lt;&lt;First</asp:LinkButton>
                            &nbsp;
                        </td>
                        <td>
                            <asp:LinkButton ID="lnkbtnPagerPrev" Text="" runat="server" CommandName="Page" CommandArgument="Prev"
                                OnClick="lnkbtnPager_Click" Style="text-decoration: none;">&lt;Prev</asp:LinkButton>
                            &nbsp;
                        </td>
                        <td>
                            Page:&nbsp;
                            <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="currentPage" />
                            &nbsp;of&nbsp;
                            <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />. Records:
                            <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                            &nbsp;to&nbsp;
                            <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                            &nbsp;of&nbsp;
                            <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                        </td>
                        <td>
                            <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page"
                                CommandArgument="Next" OnClick="lnkbtnPager_Click" Style="text-decoration: none;" />
                            &nbsp;
                        </td>
                        <td>
                            <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page"
                                CommandArgument="Last" OnClick="lnkbtnPager_Click" Style="text-decoration: none;" />
                            &nbsp;
                        </td>
                    </tr>
                </tbody>
            </table>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
<asp:Label ID="lblDispAddAppointmentPopup" runat="server"></asp:Label>
<ajax:ModalPopupExtender ID="mdlPopUpAppointmentToBeArrangedPhone" runat="server"
    PopupControlID="pnlAppointmentToBeArrangedTenantInfo" TargetControlID="lblDispAddAppointmentPopup"
    CancelControlID="imgBtnCloseTenantInfo" BackgroundCssClass="modalBackground">
</ajax:ModalPopupExtender>
<asp:Panel ID="pnlAppointmentToBeArrangedTenantInfo" runat="server" BackColor="#A4DAF6"
    Style="padding: 10px; border: 1px solid gray;">
    <asp:ImageButton ID="imgBtnCloseTenantInfo" runat="server" Style="position: absolute;
        top: -10px; right: -10px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
    <asp:UpdatePanel ID="updpnlAddPopupContactInfo" runat="server">
        <ContentTemplate>
            <table id="tblTenantInfo" runat="server" class="TenantInfo" style="font-weight: bold;">
                <tr>
                    <td style="width: 0px; height: 0px;">
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>


<ajax:ModalPopupExtender ID="mdlpopupInspection" runat="server" PopupControlID="pnlVoidInspection"
    TargetControlID="lblHiddenEntry" CancelControlID="imgBtnCancelPopup" BackgroundCssClass="modalBackground">
</ajax:ModalPopupExtender>
<asp:Label Text="" ID="lblHiddenEntry" runat="server" />
<asp:Panel ID="pnlVoidInspection" runat="server" CssClass="modalPopupSchedular"
    Style="width: 410px; border-color: black; border-bottom-style: solid; border-width: 1px;">
    <asp:ImageButton ID="imgBtnCancelPopup" runat="server" Style="position: absolute;
        top: -12px; right: -12px; width: 22px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png"
        BorderWidth="0" />
    <ucArrangeInspectionPopUp:ArrangeInspectionPopUp ID="ucArrangeInspectionPopUp" runat="server" />
</asp:Panel>
