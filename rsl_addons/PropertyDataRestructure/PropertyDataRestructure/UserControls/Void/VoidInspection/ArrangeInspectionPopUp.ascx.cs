﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PDR_BusinessLogic.Scheduling;
using PDR_DataAccess.Scheduling;
using System.Data;
using PDR_Utilities.Constants;
using PropertyDataRestructure.Base;
using PDR_BusinessObject.TradeAppointment;
using PDR_Utilities.Helpers;
using PropertyDataRestructure.Interface;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_BusinessObject.VoidAppointment;
using AjaxControlToolkit;
using PDR_BusinessObject.BritishGas;
using System.Threading;
using PDR_BusinessObject.ICalFile;
using System.Text;
using System.IO;
using PDR_BusinessObject.LookUp;

namespace PropertyDataRestructure.UserControls.Void
{
    public partial class ArrangeInspectionPopUp : UserControlBase, IAddEditPage
    {
        public Action delegateRefreshParentGrid { get; set; }

        #region Events
        #region Page load event
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                uiMessage.hideMessage();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region btnSave Click event
        /// <summary>
        /// btnSave Click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (ddlOperative.SelectedIndex > 0)
                {
                    saveData();
                    //Call delegate method to refresh Parent gridview
                    if (this.delegateRefreshParentGrid != null)
                    {
                        this.delegateRefreshParentGrid();
                    }
                    //If this appointment is arranged first time then open popup to ask: 
                    //either user want to send british gas notification or not
                    if (hdnIsRearrange.Value == null || hdnIsRearrange.Value == string.Empty)
                    {
                        //open popup to ask, either user want to send british gas notification or not
                        showHideModalPopup(modalPopupName: ApplicationConstants.MdlAskForGasNotificationPopup, showPopup: true);
                    }
                    btnSave.Enabled = false;
                }
                else
                {
                    uiMessage.showErrorMessage(UserMessageConstants.SelectSupervisor);
                    uiMessage.isError = true;
                }



            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                    //if error occurs , keep this popup opened.
                    showHideModalPopup(modalPopupName: ApplicationConstants.MdlArrangeInspectionPopup, showPopup: true);
                }
                ModalPopupExtender mdlpopupInspection = (ModalPopupExtender)Parent.FindControl("mdlpopupInspection");
                mdlpopupInspection.Show();
            }
        }
        #endregion

        #region btnCancel Click event
        /// <summary>
        /// btnCancel Click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                showHideModalPopup(modalPopupName: ApplicationConstants.MdlArrangeInspectionPopup, showPopup: false);
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region btnPopulate Click event
        /// <summary>
        /// btnPopulate Click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnPopulate_Click(object sender, EventArgs e)
        {
            try
            {
                string message = string.Empty;
                uiMessage.hideMessage();
                bool isValid = validateData();


                if (isValid)
                {
                    getAvailableOperatives(DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy", null));
                    ddlOperative.Enabled = true;
                    btnSave.Enabled = true;
                    this.loadData();
                }
                showHideModalPopup(ApplicationConstants.MdlArrangeInspectionPopup, true);
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                    showHideModalPopup(ApplicationConstants.MdlArrangeInspectionPopup, true);
                }
                ModalPopupExtender mdlpopupInspection = (ModalPopupExtender)Parent.FindControl("mdlpopupInspection");
                mdlpopupInspection.Show();
            }
        }
        #endregion

        #region btnCancel Appointment
        /// <summary>
        /// btnCancel Appointment
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancelAppointment_Click(object sender, EventArgs e)
        {
            try
            {

                int journalId = Convert.ToInt32(hdnJournal.Value);
                this.cancelAppointment(journalId);
                sendCancelEmail(journalId);
                if (this.delegateRefreshParentGrid != null)
                {
                    this.delegateRefreshParentGrid();
                }

            }
            catch (ThreadAbortException ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
                ModalPopupExtender mdlpopupInspection = (ModalPopupExtender)Parent.FindControl("mdlpopupInspection");
                mdlpopupInspection.Show();
            }
        }
        #endregion
        #endregion

        #region Populate drop downs and hdn values
        /// <summary>
        /// Populate drop downs and hdn values
        /// </summary>
        /// <remarks></remarks>

        public void populatePreValues(int journalId, bool Legionella = false, string appointmentNotes = null, bool IsRearrange = false, string appointment = "", string operativeName = "", string src = "")
        {

            hdnJournal.Value = journalId.ToString();
            populateDropDowns();
            resetControls();
            hdnIsRearrange.Value = null;
            btnSave.Enabled = true;

            if (IsRearrange == true)
            {
                hdnIsRearrange.Value = IsRearrange.ToString();
                txtAppointmentNotes.Text = appointmentNotes;

                btnSave.Text = ApplicationConstants.RearrangeButtonText;
                btnCancelAppointment.Visible = true;
                btnCancelAppointment.Enabled = true;
                if (src == PathConstants.Rsl)
                {
                    AppointmentDate.InnerText = appointment.Split('<')[0];
                    AppointmentTime.InnerText = appointment.Split('>')[1];
                    OperativeName.InnerText = operativeName;
                }
                else
                {
                    ApTime.Visible = false;
                    ApDate.Visible = false;
                    OpName.Visible = false;
                }
            }
            else
            {
                ApTime.Visible = false;
                ApDate.Visible = false;
                OpName.Visible = false;
                btnSave.Text = ApplicationConstants.SaveButtonText;
                btnCancelAppointment.Visible = false;
            }

        }

        #endregion

        #region IAddEditPage Implementation
        public void saveData()
        {
            DateTime appointmentStartDateTime = DateTime.Parse(txtDate.Text + " " + ddlStartTime.SelectedValue + ":" + ddlStartTimeMin.SelectedValue);
            DateTime appointmentEndDateTime = DateTime.Parse(txtDate.Text + " " + ddlEndTime.SelectedValue + ":" + ddlEndTimeMin.SelectedValue);
            double duration = (appointmentEndDateTime - appointmentStartDateTime).TotalHours;

            VoidAppointmentBO voidBo = new VoidAppointmentBO();
            voidBo.JournalId = Convert.ToInt32(hdnJournal.Value);
            voidBo.OperativeId = Convert.ToInt32(ddlOperative.SelectedValue);
            voidBo.InspectionType = Convert.ToInt32(chkTypeList.SelectedValue);
            voidBo.AppointmentStartDate = DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy", null);
            voidBo.AppointmentEndDate = DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy", null);
            voidBo.StartTime = appointmentStartDateTime.ToShortTimeString();
            voidBo.EndTime = appointmentEndDateTime.ToShortTimeString();
            voidBo.UserId = objSession.EmployeeId;
            voidBo.Duration = duration;
            voidBo.AppointmentNotes = txtAppointmentNotes.Text.Trim();

            VoidSchedulingBL objVoidSchedulingBl = new VoidSchedulingBL(new SchedulingRepo());
            string isSaved = string.Empty;
            int appointmentId = Convert.ToInt32(ApplicationConstants.DefaultValue);
            if (hdnIsRearrange.Value != null && hdnIsRearrange.Value != string.Empty && Convert.ToBoolean(hdnIsRearrange.Value))
            {
                isSaved = objVoidSchedulingBl.reScheduleVoidInspection(voidBo);
            }
            else
            {
                isSaved = objVoidSchedulingBl.scheduleVoidInspection(voidBo, ref appointmentId);
            }
            if (isSaved.Equals(ApplicationConstants.NotSaved))
            {
                uiMessage.showErrorMessage(UserMessageConstants.SaveAppointmentFailed);
            }
            else
            {
                // Send ICal file via email
                sendConfirmAppointmentEmail(voidBo);
                uiMessage.showInformationMessage(UserMessageConstants.SaveAppointmentSuccessfully);
            }

        }

        public bool validateData()
        {
            bool isValid = false;
            string message = string.Empty;
            DateTime appointmentStartDateTime = DateTime.Parse(txtDate.Text + " " + ddlStartTime.SelectedValue + ":" + ddlStartTimeMin.SelectedValue);
            DateTime appointmentEndDateTime = DateTime.Parse(txtDate.Text + " " + ddlEndTime.SelectedValue + ":" + ddlEndTimeMin.SelectedValue);
            if (!string.IsNullOrEmpty(txtDate.Text))
            {
                isValid = true;
                if (DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy", null) < DateTime.Today.Date)
                {
                    message = UserMessageConstants.SelectFutureDate + "<br />";
                    uiMessage.showErrorMessage(message);
                    uiMessage.isError = true;
                    isValid = false;
                }

                if (!GeneralHelper.isWeekend(DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy", null)))
                {
                    message = UserMessageConstants.WeekendDate;
                    uiMessage.showErrorMessage(message);
                    uiMessage.isError = true;
                    isValid = false;
                }

            }
            else
            {
                message = UserMessageConstants.InvalidStartDate + "<br />";
                uiMessage.showErrorMessage(message);
                uiMessage.isError = true;
                isValid = false;
            }
            if (appointmentStartDateTime.TimeOfDay >= appointmentEndDateTime.TimeOfDay)
            {
                message = UserMessageConstants.AppointmentTime + "<br />";
                uiMessage.showErrorMessage(message);
                uiMessage.isError = true;
                isValid = false;
            }
            return isValid;
        }

        public void resetControls()
        {
            txtDate.Text = string.Empty;
            txtAppointmentNotes.Text = string.Empty;
            ddlStartTime.SelectedIndex = 0;
            //ddlStartTimeMin.SelectedIndex = 0;
            ddlEndTime.SelectedIndex = 0;
            //ddlEndTimeMin.SelectedIndex = 0;
            ddlOperative.Enabled = false;
            ddlOperative.SelectedIndex = 0;

        }

        public void loadData()
        {

            DateTime appointmentStartDateTime = DateTime.Parse(txtDate.Text + " " + ddlStartTime.SelectedValue + ":" + ddlStartTimeMin.SelectedValue);
            DateTime appointmentEndDateTime = DateTime.Parse(txtDate.Text + " " + ddlEndTime.SelectedValue + ":" + ddlEndTimeMin.SelectedValue);
            DateTime startDate = DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy", null);
            SchedulingBL schedulingBl = new SchedulingBL(new SchedulingRepo());
            //get Available Operatives Dataset from session which  stored in  getAvailableOperatives method. 
            DataSet OperativesDataSet = new DataSet();
            OperativesDataSet = objSession.AvailableOperativesDs;
            List<LookUpBO> operativesList = schedulingBl.getVoidAvailableOperative(OperativesDataSet, startDate, appointmentStartDateTime, appointmentEndDateTime);
            populateDropDown(operativesList);

        }

        public void populateDropDown(DropDownList ddl)
        {

        }
        public void populateDropDown(List<LookUpBO> operativesList)
        {
            ddlOperative.DataSource = operativesList;
            ddlOperative.DataValueField = "EmployeeId";
            ddlOperative.DataTextField = "FullName";
            ddlOperative.DataBind();
            ListItem newListItem = default(ListItem);
            newListItem = new ListItem("Please select", "-1");
            ddlOperative.Items.Insert(0, newListItem);

            if (operativesList.Count() == 0)
            {
                uiMessage.showErrorMessage(UserMessageConstants.OperativesNotFound);
            }
            else
            {
                uiMessage.hideMessage();
            }
        }
        public void populateDropDowns()
        {
            GeneralHelper.populateHoursDropDowns(ref ddlStartTime);
            GeneralHelper.populateHoursDropDowns(ref ddlEndTime);
            GeneralHelper.populateMinDropDowns(ref ddlStartTimeMin);
            GeneralHelper.populateMinDropDowns(ref ddlEndTimeMin);


        }



        #endregion

        #region "get Available Operatives"
        /// <summary>
        /// this function 'll get the available operatives based on following: 
        /// trade ids of component, property id, start date 
        /// </summary>    
        /// <returns></returns>
        /// <remarks></remarks>
        private DataSet getAvailableOperatives(DateTime startDate)
        {
            SchedulingBL schedulingBl = new SchedulingBL(new SchedulingRepo());
            DataSet resultDataSet = new DataSet();


            //this function 'll get the available operatives based on following: trade ids will be null, start date 
            schedulingBl.getAvailableOperatives(ref resultDataSet, string.Empty, ApplicationConstants.VoidMsatType, startDate);
            if (resultDataSet.Tables[0].Rows.Count == 0)
            {
                throw new Exception(UserMessageConstants.OperativesNotFound);
            }
            else
            {
                objSession.AvailableOperativesDs = resultDataSet;
            }
            return resultDataSet;
        }
        #endregion

        #region cancel Appointment
        private void cancelAppointment(int journalId)
        {
            VoidSchedulingBL objSchedulingBL = new VoidSchedulingBL(new SchedulingRepo());
            int employeeId = objSession.EmployeeId;
            int isCancelled = objSchedulingBL.cancelGasElectricAppointment(journalId, employeeId);

            if (isCancelled == ApplicationConstants.Cancelled)
            {
                uiMessage.showInformationMessage(UserMessageConstants.AppointmentCancelledSuccessfully);
                btnCancelAppointment.Enabled = false;
                btnSave.Enabled = false;

            }
            else
            {
                uiMessage.showErrorMessage(UserMessageConstants.AppointmentCancelledFailed);
            }

        }
        #endregion

        #region show Hide Modal Popup
        private void showHideModalPopup(string modalPopupName, bool showPopup)
        {
            AjaxControlToolkit.ModalPopupExtender mdlPopup = (AjaxControlToolkit.ModalPopupExtender)Parent.FindControl(modalPopupName);

            if (showPopup == true)
            {
                mdlPopup.Show();
            }
            else
            {
                mdlPopup.Hide();
            }

        }
        #endregion

        #region "Email Functionality"

        #region send confirm Appointment Email
        /// <summary>
        /// send confirm Appointment Email
        /// </summary>
        /// <remarks></remarks>

        private void sendConfirmAppointmentEmail(VoidAppointmentBO objVoidAppointmentBO)
        {
            int journalId = Convert.ToInt32(hdnJournal.Value);
            DataSet resultDataset = new DataSet();
            VoidSchedulingBL objSchedulingBL = new VoidSchedulingBL(new SchedulingRepo());
            objSchedulingBL.getAppointmentDetailByJournalId(ref resultDataset, journalId);

            if (resultDataset != null && resultDataset.Tables[0].Rows.Count > 0)
            {
                string operativeName = string.Empty;
                string operativeEmail = string.Empty;
                operativeName = resultDataset.Tables[0].Rows[0]["OperativeName"].ToString();
                if (resultDataset.Tables[0].Rows[0]["Email"].Equals(DBNull.Value))
                {
                    operativeEmail = string.Empty;
                }
                else
                {
                    operativeEmail = resultDataset.Tables[0].Rows[0]["Email"].ToString();
                }

                string body = this.populateConfirmAppointmentEmailBody(resultDataset.Tables[0], "Void Inspection");
                string subject = "Void Inspection Appointment Arranged:" + resultDataset.Tables[0].Rows[0]["Ref"].ToString();
                string recepientEmail = operativeEmail;

                try
                {

                    if (ValidationHelper.isEmail(recepientEmail))
                    {
                        populateiCalFile(operativeName, operativeEmail, objVoidAppointmentBO);
                        EmailHelper.sendHtmlFormattedEmailWithAttachment(operativeName, recepientEmail, subject, body, Server.MapPath(PathConstants.ICalFilePath));
                        // EmailHelper.sendHtmlFormattedEmail(operativeName, recepientEmail, subject, body);
                    }
                    else
                    {
                        uiMessage.showErrorMessage(UserMessageConstants.AppointmentArrangedSavedEmailError + " " + UserMessageConstants.InvalidEmail);
                    }
                }
                catch (Exception ex)
                {
                    uiMessage.isError = true;
                    uiMessage.showErrorMessage(UserMessageConstants.AppointmentArrangedSavedEmailError + ex.Message);

                    if (uiMessage.isExceptionLogged == false)
                    {
                        ExceptionPolicy.HandleException(ex, "Exception Policy");
                    }
                }
            }
            else
            {
                uiMessage.showErrorMessage(UserMessageConstants.AppointmentArrangedSavedEmailError + " " + UserMessageConstants.ProblemLoadingData);
            }

        }
        #endregion

        #region Populate Email body for Confirm appointment
        /// <summary>
        /// Populate Email body for Confirm appointment
        /// </summary>
        /// <param name="objMeAppointmentBO"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        private string populateConfirmAppointmentEmailBody(DataTable resultDt, string appointmentType)
        {
            StringBuilder body = new StringBuilder();
            StreamReader reader = new StreamReader(Server.MapPath("~/Email/VoidAppointmentArranged.html"));
            body.Append(reader.ReadToEnd().ToString());
            body.Replace("{Operative}", resultDt.Rows[0]["OperativeName"].ToString());
            body.Replace("{AppointmentType}", appointmentType);
            body.Replace("{Scheduler}", objSession.UserFullName);
            body.Replace("{Address}", resultDt.Rows[0]["Address"].ToString());
            body.Replace("{TownCity}", resultDt.Rows[0]["TOWNCITY"].ToString());
            body.Replace("{County}", resultDt.Rows[0]["COUNTY"].ToString());
            body.Replace("{PostCode}", resultDt.Rows[0]["POSTCODE"].ToString());
            body.Replace("{Termination}", resultDt.Rows[0]["Termination"].ToString());
            body.Replace("{Relet}", resultDt.Rows[0]["Relet"].ToString());
            body.Replace("{StartDate}", resultDt.Rows[0]["StartDate"].ToString());
            body.Replace("{StartTime}", resultDt.Rows[0]["StartTime"].ToString());
            body.Replace("{EndTime}", resultDt.Rows[0]["EndTime"].ToString());


            return body.ToString();
        }
        #endregion

        #region "Populate iCal File"
        /// <summary>
        /// 
        /// </summary>
        /// <remarks></remarks>

        public void populateiCalFile(string operativeName, string operativeEmail, VoidAppointmentBO objVoidAppointmentBO)
        {
            ICalFileBO objICalFileBo = new ICalFileBO();
            objICalFileBo.OperativeName = operativeName;
            objICalFileBo.OperativeEmail = operativeEmail;
            objICalFileBo.AppointmentStartDate = objVoidAppointmentBO.AppointmentStartDate;
            objICalFileBo.AppointmentEndDate = objVoidAppointmentBO.AppointmentEndDate;
            objICalFileBo.StartTime = objVoidAppointmentBO.StartTime;
            objICalFileBo.EndTime = objVoidAppointmentBO.EndTime;
            objICalFileBo.Summary = "Void Inspection Appointment";
            objICalFileBo.JS = "JSV" + objVoidAppointmentBO.JournalId.ToString().PadLeft(6, '0');
            GeneralHelper.populateiCalFile(objICalFileBo);
        }

        #endregion

        #region send Cancel Appointment Email
        /// <summary>
        /// send Cancel Appointment Email
        /// </summary>
        /// <remarks></remarks>

        private void sendCancelEmail(int journalId)
        {

            DataSet resultDataset = new DataSet();
            VoidSchedulingBL objSchedulingBL = new VoidSchedulingBL(new SchedulingRepo());
            objSchedulingBL.getAppointmentDetailByJournalId(ref resultDataset, journalId);

            if (resultDataset != null && resultDataset.Tables[0].Rows.Count > 0)
            {
                string operativeName = string.Empty;
                string operativeEmail = string.Empty;
                operativeName = resultDataset.Tables[0].Rows[0]["OperativeName"].ToString();
                if (resultDataset.Tables[0].Rows[0]["Email"].Equals(DBNull.Value))
                {
                    operativeEmail = string.Empty;
                }
                else
                {
                    operativeEmail = resultDataset.Tables[0].Rows[0]["Email"].ToString();
                }

                string body = this.populateCancelAppointmentEmailBody(resultDataset.Tables[0], "Void Inspection");
                string subject = "Void Inspection Appointment Cancelled:" + resultDataset.Tables[0].Rows[0]["Ref"].ToString();
                string recepientEmail = operativeEmail;

                try
                {

                    if (ValidationHelper.isEmail(recepientEmail))
                    {

                        EmailHelper.sendHtmlFormattedEmail(operativeName, recepientEmail, subject, body);
                    }
                    else
                    {
                        uiMessage.showErrorMessage(UserMessageConstants.AppointmentCancelledEmailError + " " + UserMessageConstants.InvalidEmail);
                    }
                }
                catch (Exception ex)
                {
                    uiMessage.isError = true;
                    uiMessage.showErrorMessage(UserMessageConstants.AppointmentCancelledEmailError + ex.Message);


                    if (uiMessage.isExceptionLogged == false)
                    {
                        ExceptionPolicy.HandleException(ex, "Exception Policy");
                    }

                }
            }
            else
            {
                uiMessage.showErrorMessage(UserMessageConstants.AppointmentCancelledEmailError + " " + UserMessageConstants.ProblemLoadingData);
            }

        }
        #endregion

        #region "Populate Email body for cancel appointment"
        /// <summary>
        /// Populate Email body
        /// </summary>
        /// <param name="objMeAppointmentBO"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        private string populateCancelAppointmentEmailBody(DataTable resultDt, string appointmentType)
        {
            StringBuilder body = new StringBuilder();
            StreamReader reader = new StreamReader(Server.MapPath("~/Email/AppointmentCancelled.html"));
            body.Append(reader.ReadToEnd().ToString());
            body.Replace("{Operative}", resultDt.Rows[0]["OperativeName"].ToString());
            body.Replace("{AppointmentType}", appointmentType);
            body.Replace("{Scheduler}", objSession.UserFullName);
            body.Replace("{Address}", resultDt.Rows[0]["Address"].ToString());
            body.Replace("{TownCity}", resultDt.Rows[0]["TOWNCITY"].ToString());
            body.Replace("{County}", resultDt.Rows[0]["COUNTY"].ToString());
            body.Replace("{PostCode}", resultDt.Rows[0]["POSTCODE"].ToString());
            body.Replace("{Termination}", resultDt.Rows[0]["Termination"].ToString());
            body.Replace("{Relet}", resultDt.Rows[0]["Relet"].ToString());
            body.Replace("{StartDate}", resultDt.Rows[0]["StartDate"].ToString());
            body.Replace("{StartTime}", resultDt.Rows[0]["StartTime"].ToString());
            body.Replace("{EndTime}", resultDt.Rows[0]["EndTime"].ToString());


            return body.ToString();
        }
        #endregion
        #endregion
    }
}