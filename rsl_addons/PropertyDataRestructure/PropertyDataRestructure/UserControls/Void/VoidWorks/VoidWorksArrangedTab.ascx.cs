﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PropertyDataRestructure.Base;
using PropertyDataRestructure.Interface;
using PDR_BusinessObject.MeSearch;
using PDR_BusinessObject.PageSort;
using PDR_BusinessLogic.Scheduling;
using System.Data;
using PDR_DataAccess.Scheduling;
using PDR_Utilities.Constants;
using PDR_Utilities.Helpers;
using PDR_BusinessLogic.VoidInspections;
using PDR_DataAccess.VoidInspections;
using System.Web.UI.HtmlControls;
using System.Threading;

namespace PropertyDataRestructure.UserControls.Void
{
    public partial class VoidWorksArrangedTab : UserControlBase, IListingPage
    {
        #region Properties
      
        PageSortBO objPageSortBo = new PageSortBO("DESC", "ADDRESS", 1, 30);
        public int rowCount = 0;
        #endregion

        #region Events
        #region Page load event
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
               
                 uiMessage.hideMessage();
                if (!IsPostBack)
                {
                   
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region Pager Event Handler
        /// <summary>
        /// Pager event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkbtnPager_Click(object sender, EventArgs e)
        {
            try
            {
                PageSortBO objPageSortBo = new PageSortBO("DESC", "JournalId", 1, 30);
                pageSortViewState = GridHelper.processGridPager(sender, objPageSortBo, pageSortViewState);
                loadData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region "grdInspectionArranged Sorting"
        protected void grdVoidWorksArranged_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                objPageSortBo = pageSortViewState;

                objPageSortBo.SortExpression = e.SortExpression;
                objPageSortBo.PageNumber = 1;
                grdVoidWorksArranged .PageIndex = 0;
                objPageSortBo.setSortDirection();

                pageSortViewState = objPageSortBo;
                loadData ();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #region img btn Aptba TenantInfo Click
        /// <summary>
        /// img btn Aptba TenantInfo Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void imgbtnAptbaTenantInfo_Click(Object sender, EventArgs e)
        {
            try
            {
                ImageButton btn = (ImageButton)sender;
                int tenancyID = Convert.ToInt32(btn.CommandArgument);
                //Me.setTenantsInfoByTenancyID(tenancyID)

                DataSet dstenantsInfo = new DataSet();
                VoidInspectionsBL objVoidInspectionsBl = new VoidInspectionsBL(new VoidInspectionsRepo());
                objVoidInspectionsBl.GetJointTenantsInfoByTenancyID(ref dstenantsInfo, ref tenancyID);

                if ((dstenantsInfo.Tables.Count > 0 && dstenantsInfo.Tables[0].Rows.Count > 0))
                {
                    setTenantsInfo(ref dstenantsInfo, ref tblTenantInfo);

                    mdlPopUpAppointmentToBeArrangedPhone.Show();

                }
                else
                {
                    uiMessage.showErrorMessage(UserMessageConstants.noTenantInformationFound);

                }

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region Button Schedule Event Handler
        /// <summary>
        /// Button Schedule Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void imgBtnSchedule_Click(Object sender, EventArgs e)
        {
            try
            {
                ImageButton btnSchedule = (ImageButton)sender;
                int journalId = Convert.ToInt32(btnSchedule.CommandArgument);
                navigateToReScheduleWorksRequired(journalId);



            }
            catch (ThreadAbortException Ex)
            { 
            
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #endregion

        #region IListingPage Implementation
        public void loadData()
        {
            uiMessage.hideMessage();
            string searchText = objSession.SearchText;
            int patchNum = objSession.Patch;
            PageSortBO objPageSortBo;
            if (pageSortViewState == null)
            {
                objPageSortBo = new PageSortBO("DESC", "JournalId", 1, 30);
                pageSortViewState = objPageSortBo;
            }
            else
            {
                objPageSortBo = pageSortViewState;
            }

            int totalCount = 0;
            VoidInspectionsBL objVoidInspectionsBl = new VoidInspectionsBL(new VoidInspectionsRepo());
            DataSet resultDataSet = new DataSet();
            totalCount = objVoidInspectionsBl.getVoidWorksArranged(ref resultDataSet, objPageSortBo, searchText,patchNum);
            grdVoidWorksArranged.DataSource = resultDataSet;
            grdVoidWorksArranged.DataBind();
            objPageSortBo.TotalRecords = totalCount;
            objPageSortBo.TotalPages = Convert.ToInt32(Math.Ceiling((Double)totalCount / objPageSortBo.PageSize));

            if (objPageSortBo.TotalPages > 0)
            {
                pnlPagination.Visible = true;
            }
            else
            {
                pnlPagination.Visible = false;
                uiMessage.showErrorMessage(UserMessageConstants.RecordNotFound);
            }
            pageSortViewState = objPageSortBo;
            GridHelper.setGridViewPager(ref pnlPagination, objPageSortBo);
        }

        public void searchData()
        {
            PageSortBO objPageSortBo = new PageSortBO("DESC", "ADDRESS", 1, 30);
            pageSortViewState = objPageSortBo;
            loadData();
        }

        public void populateData()
        {
            throw new NotImplementedException();
        }

        public void printData()
        {
            throw new NotImplementedException();
        }

        public void exportGridToExcel()
        {
            throw new NotImplementedException();
        }

        public void exportToPdf()
        {
            throw new NotImplementedException();
        }

        public void downloadData()
        {
            throw new NotImplementedException();
        }

        public void applyFilters()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region "Set Tenant(s) Info by TenancyId - Joint Tenancy"
        /// <summary>
        /// 'To implement joint tenants information.
        /// </summary>
        /// <param name="dsTenantsInfo"></param>
        /// <param name="tblTenantInfo"></param>
        /// <remarks></remarks>
        private void setTenantsInfo(ref DataSet dsTenantsInfo, ref HtmlTable tblTenantInfo)
        {

            if (dsTenantsInfo.Tables.Count > 0 && dsTenantsInfo.Tables[0].Rows.Count > 0)
            {
                while ((tblTenantInfo.Rows.Count > 1))
                {
                    tblTenantInfo.Rows.RemoveAt(1);
                }

                var _with1 = dsTenantsInfo.Tables[0];


                for (int counter = 0; counter <= _with1.Rows.Count - 1; counter++)
                {
                    //Add Tenant's name to table

                    HtmlTableRow newRowTenantName = new HtmlTableRow();

                    HtmlTableCell newCellTenantLabel = new HtmlTableCell();
                    if (_with1.Rows.Count > 1)
                    {
                        newCellTenantLabel.InnerText = "Tenant" + (counter + 1).ToString() + ":";
                    }
                    else
                    {
                        newCellTenantLabel.InnerText = "Tenant:";
                    }

                    newRowTenantName.Cells.Add(newCellTenantLabel);

                    HtmlTableCell newCellTenantName = new HtmlTableCell();
                    newCellTenantName.InnerText = _with1.Rows[counter]["CustomerName"].ToString();
                    newRowTenantName.Cells.Add(newCellTenantName);

                    tblTenantInfo.Rows.Add(newRowTenantName);

                    //Add Tenant's Mobile to table

                    HtmlTableRow newRowTenantMobile = new HtmlTableRow();

                    HtmlTableCell newCellTenantMobileLabel = new HtmlTableCell();
                    newCellTenantMobileLabel.InnerText = "Mobile:";
                    newRowTenantMobile.Cells.Add(newCellTenantMobileLabel);

                    HtmlTableCell newCellTenantMobileNo = new HtmlTableCell();
                    newCellTenantMobileNo.InnerText = _with1.Rows[counter]["MOBILE"].ToString();
                    newRowTenantMobile.Cells.Add(newCellTenantMobileNo);

                    tblTenantInfo.Rows.Add(newRowTenantMobile);

                    //Add tenant's Telephone to table

                    HtmlTableRow newRowTenantTel = new HtmlTableRow();

                    HtmlTableCell newCellTenantTelLabel = new HtmlTableCell();
                    newCellTenantTelLabel.InnerText = "Telephone:";
                    newRowTenantTel.Cells.Add(newCellTenantTelLabel);

                    HtmlTableCell newCellTenantTelNo = new HtmlTableCell();
                    newCellTenantTelNo.InnerText = _with1.Rows[counter]["TEL"].ToString();
                    newRowTenantTel.Cells.Add(newCellTenantTelNo);

                    tblTenantInfo.Rows.Add(newRowTenantTel);

                    //Add tenant's Email to table

                    HtmlTableRow newRowTenantEmail = new HtmlTableRow();

                    HtmlTableCell newCellTenantEmailLabel = new HtmlTableCell();
                    newCellTenantEmailLabel.InnerText = "Email:";
                    newRowTenantEmail.Cells.Add(newCellTenantEmailLabel);

                    HtmlTableCell newCellTenantEmail = new HtmlTableCell();
                    newCellTenantEmail.InnerText = _with1.Rows[counter]["EMAIL"].ToString();
                    newRowTenantEmail.Cells.Add(newCellTenantEmail);

                    tblTenantInfo.Rows.Add(newRowTenantEmail);

                    //Add a row seprator to the tenants
                    if (counter < _with1.Rows.Count - 1)
                    {
                        HtmlTableRow newRowseprator = new HtmlTableRow();

                        HtmlTableCell sepratorCell = new HtmlTableCell();
                        sepratorCell.ColSpan = 2;
                        sepratorCell.InnerHtml = "<hr style=\"width:98%; text-align:center\" />";
                        newRowseprator.Cells.Add(sepratorCell);

                        tblTenantInfo.Rows.Add(newRowseprator);
                    }

                }

            }
        }
        #endregion

        #region Navigate to ReSchedule Required Works page
        /// <summary>
        /// Navigate to ReSchedule Required Works page
        /// </summary>
        /// <remarks></remarks>
        public void navigateToReScheduleWorksRequired(int jid)
        {
            objSession.RequiredWorksDs = null;
            Response.Redirect(PathConstants.ReScheduleRequiredWorks + "?tab=aptArranged&jid=" + jid.ToString());
        }
        #endregion

        #region filter results on patch
        public void FilterResultOnPatch()
        {
            PageSortBO objPageSortBo = new PageSortBO("DESC", "ADDRESS", 1, 30);
            pageSortViewState = objPageSortBo;
            loadData();
        }

        #endregion
    }
}