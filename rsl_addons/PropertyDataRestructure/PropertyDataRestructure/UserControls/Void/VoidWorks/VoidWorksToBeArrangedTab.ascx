﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VoidWorksToBeArrangedTab.ascx.cs"
    Inherits="PropertyDataRestructure.UserControls.Void.VoidWorksToBeArrangedTab" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<link href="../../Styles/Site.css" rel="stylesheet" media="screen" />
<link href="../../Styles/default.css" rel="stylesheet" type="text/css" />
<asp:UpdatePanel runat="server" ID="updPanelAppointmentToBeArranged">
    <ContentTemplate>
        <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" />
        <div style="height: 580px; overflow: auto;">
            <cc1:PagingGridView ID="grdWorksToBeArranged" runat="server" AllowPaging="false"
                AllowSorting="true" AutoGenerateColumns="False" BorderStyle="Solid" BorderWidth="0px"
                CellPadding="4" GridLines="None" PageSize="10" Width="100%" PagerSettings-Visible="false"
                OnSorting="grdWorksToBeArranged_Sorting" HeaderStyle-Font-Underline="false" ShowHeaderWhenEmpty="true"
                EmptyDataText="No Record Found">
                <Columns>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgbtnAptbaTenantInfo" runat="server" CommandArgument='<%# Eval("TenancyId") %>'
                                ImageUrl='../../../Images/rec.png' BorderStyle="None" BorderWidth="0px" OnClick="imgbtnAptbaTenantInfo_Click" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Ref:" SortExpression="JournalId">
                        <ItemTemplate>
                            <asp:Label ID="lblRef" runat="server" Text='<%# Bind("Ref") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Scheme" SortExpression="Scheme">
                        <ItemTemplate>
                            <asp:Label ID="lblScheme" runat="server" Text='<%# Bind("Scheme") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="20%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Block:" SortExpression="Block">
                        <ItemTemplate>
                            <asp:Label ID="lblBlock" runat="server" Text='<%# Bind("Block") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="20%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Address:" SortExpression="Address">
                        <ItemTemplate>
                            <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                        <ItemStyle Width="20%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Termination:" SortExpression="Termination">
                        <ItemTemplate>
                            <asp:Label ID="lblTermination" runat="server" Text='<%# Bind("Termination") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Works:" SortExpression="Works">
                        <ItemTemplate>
                            <asp:Label ID="lblWorks" runat="server" Text='<%# Bind("Works") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Arranged:" SortExpression="Arranged">
                        <ItemTemplate>
                            <asp:Label ID="lblArranged" runat="server" Text='<%# Bind("Arranged") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                    <asp:TemplateField ShowHeader="False">
                        <ItemTemplate>
                            <asp:ImageButton ID="imgBtnSchedule" runat="server" BorderStyle="None" OnClick="imgBtnSchedule_Click"
                                BorderWidth="0px" ImageUrl='../../../Images/aero.png' CommandArgument='<%# Eval("JournalId") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#ffffff" CssClass="table-head" Font-Bold="True" ForeColor="black"
                    HorizontalAlign="Left" Font-Underline="false" />
                <AlternatingRowStyle BackColor="#E8E9EA" Wrap="True" />
            </cc1:PagingGridView>
        </div>
        <asp:Panel runat="server" ID="pnlPagination" Visible="true" Style="border-top: 1px solid  #000;
            vertical-align: middle; padding: 10px 0">
            <table style="width: 57%; margin: 0 auto">
                <tbody>
                    <tr>
                        <td>
                            <asp:LinkButton ID="lnkbtnPagerFirst" Text="" runat="server" CommandName="Page" CommandArgument="First"
                                OnClick="lnkbtnPager_Click" Style="text-decoration: none;">&lt;&lt;First</asp:LinkButton>
                            &nbsp;
                        </td>
                        <td>
                            <asp:LinkButton ID="lnkbtnPagerPrev" Text="" runat="server" CommandName="Page" CommandArgument="Prev"
                                OnClick="lnkbtnPager_Click" Style="text-decoration: none;">&lt;Prev</asp:LinkButton>
                            &nbsp;
                        </td>
                        <td>
                            Page:&nbsp;
                            <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="currentPage" />
                            &nbsp;of&nbsp;
                            <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />. Records:
                            <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                            &nbsp;to&nbsp;
                            <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                            &nbsp;of&nbsp;
                            <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                        </td>
                        <td>
                            <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page"
                                CommandArgument="Next" OnClick="lnkbtnPager_Click" Style="text-decoration: none;" />
                            &nbsp;
                        </td>
                        <td>
                            <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page"
                                CommandArgument="Last" OnClick="lnkbtnPager_Click" Style="text-decoration: none;" />
                            &nbsp;
                        </td>
                    </tr>
                </tbody>
            </table>
        </asp:Panel>
    </ContentTemplate>
</asp:UpdatePanel>
<asp:Label ID="lblDispWorksPopup" runat="server"></asp:Label>
<ajax:ModalPopupExtender ID="mdlPopUpWorksToBeArrangedPhone" runat="server" PopupControlID="pnlWorksToBeArrangedTenantInfo"
    TargetControlID="lblDispWorksPopup" CancelControlID="imgBtnCloseTenantInfo" BackgroundCssClass="modalBackground">
</ajax:ModalPopupExtender>
<asp:Panel ID="pnlWorksToBeArrangedTenantInfo" runat="server" BackColor="#A4DAF6"
    Style="padding: 10px; border: 1px solid gray;">
    <asp:ImageButton ID="imgBtnCloseTenantInfo" runat="server" Style="position: absolute;
        top: -10px; right: -10px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
    <asp:UpdatePanel ID="updpnlAddPopupContactInfo" runat="server">
        <ContentTemplate>
            <table id="tblTenantInfo" runat="server" class="TenantInfo" style="font-weight: bold;">
                <tr>
                    <td style="width: 0px; height: 0px;">
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Panel>
