﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using AjaxControlToolkit;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_BusinessLogic.AssignToContractor;
using PDR_BusinessObject.AssignToContractor;
using PDR_BusinessObject.DropDown;
using PDR_BusinessObject.Expenditure;
using PDR_BusinessObject.Vat;
using PDR_DataAccess.AssignToContractor;
using PDR_Utilities.Constants;
using PDR_Utilities.Helpers;
using PropertyDataRestructure.Base;
using PropertyDataRestructure.Interface;
using PDR_BusinessObject.RequiredWorks;
using System.Threading;


namespace PropertyDataRestructure.UserControls.Void
{
    public partial class VoidWorksAssignToContractor : UserControlBase, IAddEditPage
    {

        #region "Guide Lines to use this User Control"
        // Please follow these Guide line while referencing this user control in you page.
        //1- Add J query in Page or in master page.
        //2- Call Populate by 

        #endregion

        #region "Properties"
        int costCenter = 0;
        int BudgetHead = 0;
        int Expenditure = 0;
        bool direct = false;
        #region "Is Saved Property to use in consumer page to determine either work is assign to contractor or not"

        public bool IsSaved
        {
            get
            {
                bool isSavedret = false;
                if (ViewState[ViewStateConstants.IsSaved] != null)
                {
                    isSavedret = Convert.ToBoolean(ViewState[ViewStateConstants.IsSaved]);
                }
                return isSavedret;
            }
            set { ViewState[ViewStateConstants.IsSaved] = value; }
        }

        #endregion

        #region "Set Close Button Text - to Customize the button text from consumer"

        public string SetCloseButtonText
        {
            set { btnClose.Text = value; }
        }

        #endregion
        public Action delegateRefreshParentGrid { get; set; }
        #endregion


        #region "Events Handling"

        #region "Page Events"

        #region "Page Load"

        protected void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                uiMessage.hideMessage();
            }
            catch (ThreadAbortException ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }

            }
        }

        #endregion

        #endregion

        #region "Control Events"

        #region "Button Events"

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                Page.Validate("addServiceRequired");
                if (Page.IsValid)
                {
                    addPurchaseIteminServiceRequiredDt();
                }
                showModalPopup();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessage.isError == true)
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.delegateRefreshParentGrid != null)
                {
                    this.delegateRefreshParentGrid();
                }

            }
            catch (ThreadAbortException ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
                ModalPopupExtender mdlPopupAssignToContractor = (ModalPopupExtender)Parent.FindControl("mdlPopupAssignToContractor");
                mdlPopupAssignToContractor.Hide();
            }
        }
        protected void btnOk_click(object sender, EventArgs e){

            mdlPopupRis.Hide();
        }

        protected void btnAssignToContractor_Click(object sender, EventArgs e)
        {
            try
            {
                assignWorkToContractor();
                if (ddlContact.Text == "-1")
                    mdlPopupRis.Show();
                //else
                    
                showModalPopup();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessage.isError == true)
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #region "Drop down Events"

        #region "ddl Cost Centre Selected Index Change"

        protected void ddlCostCentre_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                getBudgetHeadDropDownValuesByCostCentreId();
                showModalPopup();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion


        #region "Get Budget Head Drop Down Values by Cost Centre Id"


        private void getBudgetHeadDropDownValuesByCostCentreId()
        {
            int costCentreId = Convert.ToInt32(3);


        }

        #endregion

        #region "Get Expenditure Drop Down Values by Budget Head Id"

        private void getExpenditureDropDownValuesByBudgetHeadId()
        {
            int budgetHeadId = Convert.ToInt32(4);
            int employeeId = objSession.EmployeeId;

        }

        #endregion

        #region "ddl Budget Head Selected Index Change"

        protected void ddlBudgetHead_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                getExpenditureDropDownValuesByBudgetHeadId();
                showModalPopup();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessage.isError == true)
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #region "ddl Vat Selected Index Change"

        protected void ddlVat_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Page.Validate("addServiceRequired");
                calculateVatAndTotal();
                ddlVat.Focus();
                showModalPopup();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #region "ddl Contractor Selected Index Change"

        protected void ddlContractor_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            getContactDropDownValuesbyContractorId();
            showModalPopup();
        }

        #endregion

        #endregion

        #region "TextBox Events"

        protected void txtNetCost_TextChanged(object sender, EventArgs e)
        {
            try
            {
                calculateVatAndTotal();
                txtNetCost.Focus();
                showModalPopup();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessage.isError == true)
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #region "Grid View Events"

        #region "Grid Works Detail Row data bound"

        protected void grdWorksDeatil_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Footer)
                {
                    DataTable serviceRequiredDt = getServiceRequiredDTViewState();

                    if (serviceRequiredDt.Rows.Count > 0)
                    {
                        e.Row.Cells[1].Text = serviceRequiredDt.Compute("Sum(" + ApplicationConstants.NetCostCol + ")", "").ToString();
                        e.Row.Cells[2].Text = serviceRequiredDt.Compute("Sum(" + ApplicationConstants.VatCol + ")", "").ToString();
                        e.Row.Cells[3].Text = serviceRequiredDt.Compute("Sum(" + ApplicationConstants.GrossCol + ")", "").ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #endregion

        #endregion

        #endregion

        #region "Functions"

        #region "Populate User Control By Passing Values from Page"

        /// <summary>
        /// Supply Required PropertyId from page in which you need to refer this user control.
        /// </summary>
        /// <param name="propertyId">Required: PropertyId of property for which you want assign a work to contractor</param>
        /// <param name="assignToContractorWorkType"></param>
        /// <param name="PMO"></param>
        /// <remarks></remarks>

        public void populateControl()
        {
            // Populating dropdowns            
            getReactiveRepairContractors();
            getCostCentreDropDownVales();
            getVatDropDownValues();
            getContactDropDownValuesbyContractorId();

            resetControls();


            List<RequiredWorksBO> checkedTempWorksList = new List<RequiredWorksBO>();
            checkedTempWorksList = objSession.RequiredWorksBOList;
            if (checkedTempWorksList.Count > 0)
            {

                foreach (RequiredWorksBO requiredWorks in checkedTempWorksList)
                {
                    txtWorkRequired.Text = txtWorkRequired.Text + requiredWorks.Location + ":" + requiredWorks.WorkDescription + Environment.NewLine;
                }


            }
            AssignToContractorBo objAssignToContractorBo = new AssignToContractorBo();
            objAssignToContractorBo.JournalId = checkedTempWorksList[0].InspectionJournalId;
            objAssignToContractorBo.RequiredWorkIds = String.Join(",", checkedTempWorksList.Select(s => s.RequiredWorksId).ToArray());
            objSession.AssignToContractorBo = null;
            objSession.AssignToContractorBo = objAssignToContractorBo;

            double tenanctNeglect = checkedTempWorksList.Sum(x => Convert.ToDouble(x.Neglect));
            txtEstimate.Text = tenanctNeglect.ToString("#.##");

        }

        #endregion

        #region "Add Purchase Items in Service Required Data Table"


        private void addPurchaseIteminServiceRequiredDt()
        {
            ServiceRequiredBo objServiceRequiredBo = new ServiceRequiredBo();
            objServiceRequiredBo.WorkDetailId = ApplicationConstants.NoneValue;
            objServiceRequiredBo.ServiceRequired = txtWorkRequired.Text.Trim();
            objServiceRequiredBo.NetCost = (string.IsNullOrEmpty(txtNetCost.Text.Trim()) ? 0.0M : Convert.ToDecimal(txtNetCost.Text));
            objServiceRequiredBo.VatIdDDLValue = Convert.ToInt32(ddlVat.SelectedValue);
            objServiceRequiredBo.Vat = (string.IsNullOrEmpty(txtVat.Text.Trim()) ? 0.0M : Convert.ToDecimal(txtVat.Text.Trim()));
            objServiceRequiredBo.Total = (string.IsNullOrEmpty(txtTotal.Text.Trim()) ? 0.0M : Convert.ToDecimal(txtTotal.Text.Trim()));
            objServiceRequiredBo.ExpenditureId = 3634;
            objServiceRequiredBo.BudgetHeadId = 717;
            objServiceRequiredBo.CostCenterId = 11;

            DataTable serviceRequiredDt = getServiceRequiredDTViewState();
            DataRow serviceRequiredRow = serviceRequiredDt.NewRow();
            serviceRequiredRow[ApplicationConstants.WorkDetailIdCol] = objServiceRequiredBo.WorkDetailId;
            serviceRequiredRow[ApplicationConstants.ExpenditureIdCol] = objServiceRequiredBo.ExpenditureId;
            serviceRequiredRow[ApplicationConstants.BudgetHeadIdCol] = objServiceRequiredBo.BudgetHeadId;
            serviceRequiredRow[ApplicationConstants.CostCenterIdCol] = objServiceRequiredBo.CostCenterId;
            serviceRequiredRow[ApplicationConstants.ServiceRequiredCol] = objServiceRequiredBo.ServiceRequired;
            serviceRequiredRow[ApplicationConstants.NetCostCol] = objServiceRequiredBo.NetCost;
            serviceRequiredRow[ApplicationConstants.VatTypeCol] = objServiceRequiredBo.VatIdDDLValue;
            serviceRequiredRow[ApplicationConstants.VatCol] = objServiceRequiredBo.Vat;
            serviceRequiredRow[ApplicationConstants.GrossCol] = objServiceRequiredBo.Total;
            serviceRequiredRow[ApplicationConstants.PIStatusCol] = getPIStatus(objServiceRequiredBo.ExpenditureId, objServiceRequiredBo.Total);
            serviceRequiredDt.Rows.Add(serviceRequiredRow);

            resetServiceRequired();
            setServiceRequiredDTViewState(serviceRequiredDt);
            bindServiceRequiredGrid();
        }

        #endregion

        #region "Get Cost Centre Drop Down Values."


        private void getCostCentreDropDownVales()
        {
            List<DropDownBO> dropDownList = new List<DropDownBO>();
            AssignToContractorBL objAssignToContractorBL = new AssignToContractorBL(new AssignToContractorRepo());
            objAssignToContractorBL.GetCostCentreDropDownVales(ref dropDownList);

            costCenter = 11;


            
                    BudgetHead = 717;
               
            int employeeId = objSession.EmployeeId;
            List<ExpenditureBO> expenditureBOList = new List<ExpenditureBO>();
            objAssignToContractorBL.GetExpenditureDropDownValuesByBudgetHeadId(ref expenditureBOList, ref BudgetHead, ref employeeId);
            objSession.ExpenditureBOList = expenditureBOList;

            Expenditure = 3634;
                
        }

        #endregion

        #region "Get Contractor Having Reactive Repair Contract"

        private void getReactiveRepairContractors()
        {
            List<DropDownBO> dropDownList = new List<DropDownBO>();
            AssignToContractorBo objAssignToContractorBo = objSession.AssignToContractorBo;

            AssignToContractorBL objAssignToContractorBL = new AssignToContractorBL(new AssignToContractorRepo());
            objAssignToContractorBL.getReactiveRepairContractors(ref dropDownList);

            bindDropDownList(ref ddlContractor, ref dropDownList);
        }

        #endregion

        #region "Get Contact DropDown Values By ContractorId"

        public void getContactDropDownValuesbyContractorId()
        {
            ddlContact.Items.Clear();
            List<DropDownBO> dropDownBoList = new List<DropDownBO>();
            DataSet resultDataset = new DataSet();
            int contractorId = Convert.ToInt32(ddlContractor.SelectedValue);
            AssignToContractorBL objAssignToContractorBL = new AssignToContractorBL(new AssignToContractorRepo());
            objAssignToContractorBL.GetContactDropDownValuesbyContractorId(ref dropDownBoList, contractorId);

            if (dropDownBoList.Count == 0)
            {
                ddlContact.Items.Insert(0, new ListItem(ApplicationConstants.noContactFound, ApplicationConstants.DropDownDefaultValue.ToString()));
            }
            else
            {
                bindDropDownList(ref ddlContact, ref dropDownBoList);
            }
        }

        #endregion

        #region "Get Vat Drop down Values - Vat Id as Value Field and Vat Name as text field."

        public void getVatDropDownValues()
        {
            List<VatBo> vatBoList = new List<VatBo>();

            AssignToContractorBL objAssignToContractorBL = new AssignToContractorBL(new AssignToContractorRepo());

            objAssignToContractorBL.getVatDropDownValues(ref vatBoList);

            ddlVat.Items.Clear();
            ddlVat.DataSource = vatBoList;
            ddlVat.DataTextField = ApplicationConstants.ddlDefaultDataTextField;
            ddlVat.DataValueField = ApplicationConstants.ddlDefaultDataValueField;
            ddlVat.DataBind();

            ddlVat.Items.Insert(0, new ListItem(ApplicationConstants.DropDwonDefaultText, ApplicationConstants.DropDownDefaultValue.ToString()));

            // Save Vat Bo List in session, to get vat rate while adding a service required.            
            objSession.VatBOList = vatBoList;
        }

        #endregion

        #region "Bind Drop Down List"

        private void bindDropDownList(ref DropDownList ddlToBind, ref List<DropDownBO> dropDownItemsList, bool insertDefault = true)
        {
            ddlToBind.Items.Clear();
            ddlToBind.DataSource = dropDownItemsList;
            ddlToBind.DataTextField = ApplicationConstants.ddlDefaultDataTextField;
            ddlToBind.DataValueField = ApplicationConstants.ddlDefaultDataValueField;
            ddlToBind.DataBind();

            if (insertDefault)
            {
                ddlToBind.Items.Insert(0, new ListItem(ApplicationConstants.DropDwonDefaultText, ApplicationConstants.DropDownDefaultValue.ToString()));
            }
        }

        #endregion

        #region "Calculate Vat and Total"

        private void calculateVatAndTotal()
        {
            int vatRateId = Convert.ToInt32(ddlVat.SelectedValue);
            decimal VatRate = 0.0M;

            if (vatRateId >= 0)
            {
                VatRate = getVatRateByVatId(vatRateId);
            }

            decimal netCost = default(decimal);
            decimal defaultNetCost = 0.0M;
            if (decimal.TryParse(txtNetCost.Text.ToString().Trim(), out defaultNetCost))
            {
                netCost = (string.IsNullOrEmpty(txtNetCost.Text.Trim()) ? defaultNetCost : Convert.ToDecimal(txtNetCost.Text.Trim()));
                decimal vat = netCost * VatRate / 100;
                decimal total = netCost + vat;

                txtNetCost.Text = string.Format("{0:0.00}", netCost);
                txtVat.Text = string.Format("{0:0.00}", vat);
                txtTotal.Text = string.Format("{0:0.00}", total);
            }
            else
            {
                txtVat.Text = string.Empty;
                txtTotal.Text = string.Empty;
            }

        }

        #endregion

        #region "Get Vat Rate by VatId"

        private decimal getVatRateByVatId(int vatRateId)
        {
            decimal vatRate = 0.0M;

            List<VatBo> vatBoList = objSession.VatBOList;

            VatBo vatBo = vatBoList.Find(i => i.Id == vatRateId);

            if ((vatBo != null))
            {
                vatRate = vatBo.VatRate;
            }

            return vatRate;
        }

        #endregion

        #region "Reset Controls (on first populate)"

        public void resetControls()
        {
            //Set is Saved property to false on populate.
            uiMessage.hideMessage();
            txtWorkRequired.Text = string.Empty;
            IsSaved = false;
            btnAssignToContractor.Enabled = true;
            btnAdd.Enabled = true;
            txtEstimateRef.Text = string.Empty;

            txtEstimate.Text = string.Empty;
            resetBudgetHead();
            resetExpenditureddl();
            resetServiceRequired();
            removeServiceRequiredDTViewState();
            bindServiceRequiredGrid();

        }

        #endregion

        #region "Bind Service Required Grid"

        private void bindServiceRequiredGrid()
        {
            DataTable dtServiceRequired = getServiceRequiredDTViewState();
            bool addExtraSpaces = false;

            if (dtServiceRequired.Rows.Count == 0)
            {
                foreach (DataColumn col in dtServiceRequired.Columns)
                {
                    col.AllowDBNull = true;
                }
                DataRow newRow = dtServiceRequired.NewRow();
                dtServiceRequired.Rows.Add(newRow);
                addExtraSpaces = true;
            }

            grdWorksDeatil.DataSource = dtServiceRequired;
            grdWorksDeatil.DataBind();
            if (addExtraSpaces)
            {
                grdWorksDeatil.Rows[0].Cells[0].Text = "<br /><br /><br />";
            }
        }

        #endregion

        #region "Assign Work To Contractor"


        private void assignWorkToContractor()
        {
            DataTable serviceRequiredDT = getServiceRequiredDTViewState();

            if (validateData())
            {
                List<RequiredWorksBO> checkedTempWorksList = new List<RequiredWorksBO>();
                checkedTempWorksList = objSession.RequiredWorksBOList;
                AssignToContractorBo assignToContractorBo = objSession.AssignToContractorBo;
                assignToContractorBo.ServiceRequiredDt = serviceRequiredDT;
                assignToContractorBo.ContractorId = Convert.ToInt32(ddlContractor.SelectedValue);
                assignToContractorBo.ContactId = Convert.ToInt32(ddlContact.SelectedValue);
                assignToContractorBo.EmpolyeeId = Convert.ToInt32(ddlContact.SelectedValue);
                assignToContractorBo.Estimate = (string.IsNullOrEmpty(txtEstimate.Text.Trim()) ? 0.0M : Convert.ToDecimal(txtEstimate.Text.Trim()));
                assignToContractorBo.EstimateRef = txtEstimateRef.Text;
                assignToContractorBo.POStatus = getPOStatusFromWorkItemsDt(serviceRequiredDT);
                assignToContractorBo.UserId = objSession.EmployeeId;
                if (assignToContractorBo.POStatus == 3)
                    direct = true;
                else
                    direct = false;
                assignToContractorBo.JournalId = checkedTempWorksList[0].InspectionJournalId;
                assignToContractorBo.InspectionJournalId = assignToContractorBo.JournalId;
                assignToContractorBo.RequiredWorkIds = String.Join(",", checkedTempWorksList.Select(s => s.RequiredWorksId).ToArray());
                AssignToContractorBL objAssignToContractorBl = new AssignToContractorBL(new AssignToContractorRepo());
                bool isSavedStatus = objAssignToContractorBl.voidWorkAssignToContractor(ref assignToContractorBo);

                if (isSavedStatus)
                {
                    //Set Property Is Saved to True.
                    this.IsSaved = true;
                    //Disable btnAssignToContractor to avoid assigning the same work more than once. 
                    btnAssignToContractor.Enabled = false;
                    btnAdd.Enabled = false;

                    string message = UserMessageConstants.AssignedToContractor;
                    try
                    {
                        if (direct)
                        {

                            DataSet detailsForEmailDS = new DataSet();
                            AssignToContractorBL objAssignToContractor = new AssignToContractorBL(new AssignToContractorRepo());
                            objAssignToContractor.getSB_DetailsForEmail(ref assignToContractorBo, ref detailsForEmailDS);
                            if (detailsForEmailDS.Tables[ApplicationConstants.ContractorDetailsDt].Rows.Count > 0)
                            {
                                sendEmailtoContractor(ref assignToContractorBo);
                            }
                            else
                            {

                                message = message + UserMessageConstants.EmailToContractor;
                            }
                            
                        }

                      
                        uiMessage.showInformationMessage(message);
                    }
                    catch (Exception ex)
                    {
                        message += "<br />but " + ex.Message;
                        uiMessage.showErrorMessage(message);
                    }
                }
                else
                {
                    uiMessage.showErrorMessage(UserMessageConstants.AssignedToContractorFailed);
                }

            }

        }

        #endregion

        #region "Helper Functions"

        #region "Validate Controls"

        public bool validateData()
        {
            DataTable serviceRequiredDT = getServiceRequiredDTViewState();
            bool isValid = true;
            if (!Page.IsValid)
            {
                isValid = false;
            }

            else if (serviceRequiredDT.Rows.Count == 0)
            {
                uiMessage.showErrorMessage(UserMessageConstants.serviceRequiredCount);
                isValid = false;
            }

            return isValid;
        }

        #endregion



        #region "Get Pi Status (Pending Status)"

        private object getPIStatus(int expenditureId, decimal gross)
        {
            int pIStatus = 3;
            direct = true;
            // 3 = "Work Ordered" in table F_POSTATUS
            List<ExpenditureBO> expenditureBoList = objSession.ExpenditureBOList;

            //Check if an item is costing 0 (zero) or less then it should not go to queued list.
            if (gross > 0)
            {
                ExpenditureBO ExpenditureBo = expenditureBoList.Find(i => i.Id == expenditureId);

                if ((ExpenditureBo != null))
                {
                    if ((gross > ExpenditureBo.Limit || gross > ExpenditureBo.Remaining))
                    {
                        direct = false;
                        pIStatus = 0;
                        // 0 = "Queued" in table F_POSTATUS
                    }
                }
            }

            return pIStatus;
        }

        #endregion

        #region "Set PO Status"

        private int getPOStatusFromWorkItemsDt(DataTable serviceRequiredDT)
        {
            int pOStatus = 3;
            // 3 = "Work Ordered" in table F_POSTATUS

            if (serviceRequiredDT.Select(ApplicationConstants.PIStatusCol + " = 0").Count() > 0)
            {
                pOStatus = 0;
                // 0 = "Queued" in table F_POSTATUS
            }

            return pOStatus;
        }

        #endregion

        #region "Populate Body and Send Email to Contractor"

        private void sendEmailtoContractor(ref AssignToContractorBo assignToContractorBo)
        {
            DataSet detailsForEmailDS = new DataSet();
            AssignToContractorBL objAssignToContractor = new AssignToContractorBL(new AssignToContractorRepo());
            objAssignToContractor.getdetailsForEmail(ref assignToContractorBo, ref detailsForEmailDS);

            if (detailsForEmailDS != null)
            {

                if ((detailsForEmailDS.Tables[ApplicationConstants.ContractorDetailsDt] != null) && detailsForEmailDS.Tables[ApplicationConstants.ContractorDetailsDt].Rows.Count > 0)
                {
                    string email = Convert.ToString(detailsForEmailDS.Tables[ApplicationConstants.ContractorDetailsDt].Rows[0]["Email"]);
                    if (string.IsNullOrEmpty(email) || !GeneralHelper.isEmail(email))
                    {
                        throw new Exception("Unable to send email, invalid email address.");
                       
                    }
                    else
                    {
                        StringBuilder body = new StringBuilder();
                        StreamReader reader = new StreamReader(Server.MapPath("~/Email/AssignVoidWorkToContractor.html"));
                        body.Append(reader.ReadToEnd());

                        //Defining Readers
                        DataRow purchaseOrderDetails = detailsForEmailDS.Tables[ApplicationConstants.PurchaseOrderDetailsDt].Rows[0];
                        //DataRow csvDetails = detailsForEmailDS.Tables[ApplicationConstants.AppointmentCSVDetailsDt].Rows[0];
                        DataRow propertyDetails = detailsForEmailDS.Tables[ApplicationConstants.PropertyDetailsDt].Rows[0];
                        DataRow supervisorDetails = detailsForEmailDS.Tables[ApplicationConstants.AppointmentSupplierDetailsDt].Rows[0];

                        // Set contractor detail(s) '
                        //==========================================='
                        //Populate Contractor Contact Name
                        body.Replace("{ContractorContactName}", Convert.ToString(detailsForEmailDS.Tables[ApplicationConstants.ContractorDetailsDt].Rows[0]["ContractorContactName"]));

                        //==========================================='
                        body.Replace("{PONumber}", Convert.ToString(purchaseOrderDetails["ORDERID"]));

                        // Populate work, estimate and cost details
                        //==========================================='
                        // Set Ref (PDR work reference)
                        //body.Replace("{OurRef}", assignToContractorBo.InspectionJournalId.ToString());

                        // Populate Estimate and Estimate Reference
                        //if (string.IsNullOrEmpty(Convert.ToString(csvDetails[0])))
                        //    body.Replace("{JSV}", "N/A");
                        //else
                        //    body.Replace("{JSV}", Convert.ToString(csvDetails[0]));

                        body.Replace("{JSV}", assignToContractorBo.RequiredWorkIds.Replace(',', '/'));
                        body.Replace("{Bedrooms}", Convert.ToString(propertyDetails["Bedrooms"]));
                        body.Replace("{ReLetDate}", Convert.ToString(purchaseOrderDetails["RELETDATE"]));
                        body.Replace("{Supervisor}", Convert.ToString(supervisorDetails[0]));
                        //body.Replace("{WorkRequired}", Convert.ToString(purchaseOrderDetails["ItemNotes"]));
                        body.Replace("{WorkRequired}", string.Join("</br>", detailsForEmailDS.Tables[ApplicationConstants.PurchaseOrderDetailsDt].AsEnumerable().Select(x => x["ItemNotes"].ToString()).ToArray()));


                        //Get sum/total of net cost from works required data table.
                        body.Replace("{NetCost}", assignToContractorBo.ServiceRequiredDt.Compute("SUM(" + ApplicationConstants.NetCostCol + ")", "").ToString());
                        //Get sum/total of Vat from works required data table.
                        body.Replace("{VAT}", assignToContractorBo.ServiceRequiredDt.Compute("SUM(" + ApplicationConstants.VatCol + ")", "").ToString());
                        //Get sum/total of Gross/Total from works required data table.
                        body.Replace("{TOTAL}", assignToContractorBo.ServiceRequiredDt.Compute("SUM(" + ApplicationConstants.GrossCol + ")", "").ToString());

                        //Get all works detail from works required data table in form of ordered list.
                        body.Replace("{ServiceRequired}", getServiceRequired(assignToContractorBo.ServiceRequiredDt));
                        //==========================================='


                        // Property detail(s) (address)
                        //==========================================='

                        //Get Property Address details from property details data set
                        if ((detailsForEmailDS.Tables[ApplicationConstants.PropertyDetailsDt] != null) && detailsForEmailDS.Tables[ApplicationConstants.PropertyDetailsDt].Rows.Count > 0)
                        {
                            DataRow dr = detailsForEmailDS.Tables[ApplicationConstants.PropertyDetailsDt].Rows[0];
                            body.Replace("{Address}", Convert.ToString(dr["FullStreetAddress"]));
                            body.Replace("{TownCity}", Convert.ToString(dr["TOWNCITY"]));
                            body.Replace("{County}", Convert.ToString(dr["COUNTY"]));
                            body.Replace("{PostCode}", Convert.ToString(dr["POSTCODE"]));
                            body.Replace("{Block}", Convert.ToString(dr["Block"]));
                            body.Replace("{Scheme}", Convert.ToString(dr["Scheme"]));
                        }
                        else
                        {
                            // This case may not occur but it is done for completeness
                            body.Replace("{Address}", "N/A");
                            body.Replace("{TownCity}", "");
                            body.Replace("{County}", "");
                            body.Replace("{PostCode}", "");
                            body.Replace("{Block}", "");
                            body.Replace("{Scheme}", "");
                        }
                        //==========================================='


                        // Set Tenant Details (name, telephone and vulnerability), in case a tenant is selected
                        //==========================================='

                        //Set Customer Name and Telephone
                        if ((detailsForEmailDS.Tables[ApplicationConstants.TenantDetailsDt] != null) && detailsForEmailDS.Tables[ApplicationConstants.TenantDetailsDt].Rows.Count > 0)
                        {
                            DataRow _row = detailsForEmailDS.Tables[ApplicationConstants.TenantDetailsDt].Rows[0];
                            body.Replace("{TenantName}", Convert.ToString(_row["FullName"]));
                            body.Replace("{Telephone}", Convert.ToString(_row["TEL"]));
                        }
                        else
                        {
                            body.Replace("{TenantName}", "N/A");
                            body.Replace("{Telephone}", "-");
                        }

                        //Get Risk And Vulnerability Details form details data set.
                        body.Replace("{RiskDetail}", getRiskDetails(detailsForEmailDS));
                        body.Replace("{VulnerabilityDetail}", getVulnerabilityDetails(detailsForEmailDS));

                        //==========================================='


                        // Attach logo images with email
                        //==========================================='
                        LinkedResource logo50Years = new LinkedResource(Server.MapPath("~/Images/50_Years.gif"));
                        logo50Years.ContentId = "logo50Years_Id";

                        body = body.Replace("{Logo_50_years}", string.Format("cid:{0}", logo50Years.ContentId));

                        LinkedResource logoBroadLandRepairs = new LinkedResource(Server.MapPath("~/Images/Broadland-Housing-Association.gif"));
                        logoBroadLandRepairs.ContentId = "logoBroadLandRepairs_Id";

                        body = body.Replace("{Logo_Broadland-Housing-Association}", string.Format("cid:{0}", logoBroadLandRepairs.ContentId));

                        ContentType mimeType = new ContentType("text/html");

                        AlternateView alternatevw = AlternateView.CreateAlternateViewFromString(body.ToString(), mimeType);
                        alternatevw.LinkedResources.Add(logo50Years);
                        alternatevw.LinkedResources.Add(logoBroadLandRepairs);
                        //==========================================='

                        MailMessage mailMessage = new MailMessage();

                        if (string.IsNullOrEmpty(assignToContractorBo.MsatType))
                            mailMessage.Subject = "Planned Work " + ApplicationConstants.EmailSubject;
                        else
                            mailMessage.Subject = assignToContractorBo.MsatType + ApplicationConstants.EmailSubject;

                        mailMessage.To.Add(new MailAddress(detailsForEmailDS.Tables[ApplicationConstants.ContractorDetailsDt].Rows[0]["Email"].ToString(), detailsForEmailDS.Tables[ApplicationConstants.ContractorDetailsDt].Rows[0]["ContractorContactName"].ToString()));
                        // For a graphical view with logos, alternative view will be visible, body is attached for text view.
                        mailMessage.Body = body.ToString();
                        mailMessage.AlternateViews.Add(alternatevw);
                        mailMessage.IsBodyHtml = true;

                        EmailHelper.sendEmail(ref mailMessage);

                    }
                }
                else
                {
                    throw new Exception("Unable to send email, contractor details not available");
                  
                }
            }
        }

        #endregion

        #region "Get Service Required - Concatenated in form of ordered list to add in email"

        private string getServiceRequired(DataTable dataTable)
        {
            StringBuilder serviceRequired = new StringBuilder();
            if (dataTable.Rows.Count == 1)
            {
                serviceRequired.Append(dataTable.Rows[0][ApplicationConstants.ServiceRequiredCol].ToString());
            }
            else
            {
                serviceRequired.Append("<ol>");
                foreach (DataRow row in dataTable.Rows)
                {
                    serviceRequired.Append("<li>" + row[ApplicationConstants.ServiceRequiredCol].ToString() + "</li>");
                }
                serviceRequired.Append("</ol>");
            }
            return serviceRequired.ToString();
        }

        #endregion

        #region "Get Risk Details And Vulnerability Details - As concatenated string split on separate row."

        private string getRiskAndVulnerabilityDetails(DataSet detailsForEmailDS)
        {
            StringBuilder RiskAndVulnerabilityDetails = new StringBuilder("N/A");


            if ((detailsForEmailDS != null))
            {
                RiskAndVulnerabilityDetails.Clear();


                if ((detailsForEmailDS.Tables[ApplicationConstants.TenantRiskDetailsDt] != null) && detailsForEmailDS.Tables[ApplicationConstants.TenantRiskDetailsDt].Rows.Count > 0)
                {
                    RiskAndVulnerabilityDetails.Append("<Stong>Risk Deatils:</Stong><br />");

                    foreach (DataRow row in detailsForEmailDS.Tables[ApplicationConstants.TenantRiskDetailsDt].Rows)
                    {
                        RiskAndVulnerabilityDetails.Append(row["CATDESC"] + (string.IsNullOrEmpty(Convert.ToString(row["SUBCATDESC"])) ? "" : ": " + row["SUBCATDESC"]) + "<br />");
                    }
                }


                if ((detailsForEmailDS.Tables[ApplicationConstants.TenantVulnerabilityDetailsDt] != null) && detailsForEmailDS.Tables[ApplicationConstants.TenantVulnerabilityDetailsDt].Rows.Count > 0)
                {
                    if (!(RiskAndVulnerabilityDetails.ToString() == string.Empty))
                    {
                        RiskAndVulnerabilityDetails.Append("<br /><br />");
                    }
                    RiskAndVulnerabilityDetails.Append("<Stong>Vulnerability Deatils:</Stong><br />");

                    foreach (DataRow row in detailsForEmailDS.Tables[ApplicationConstants.TenantVulnerabilityDetailsDt].Rows)
                    {
                        RiskAndVulnerabilityDetails.Append(row["CATDESC"] + (string.IsNullOrEmpty(Convert.ToString(row["SUBCATDESC"])) ? "" : ": " + row["SUBCATDESC"]) + "<br />");
                    }
                }
            }

            return RiskAndVulnerabilityDetails.ToString();
        }

        #endregion

        #region "Get Risk Details - As concatenated string split on separate row."

        private string getRiskDetails(DataSet detailsForEmailDS)
        {
            StringBuilder RiskDetails = new StringBuilder("N/A");


            if ((detailsForEmailDS != null) && (detailsForEmailDS.Tables[ApplicationConstants.TenantRiskDetailsDt] != null) && detailsForEmailDS.Tables[ApplicationConstants.TenantRiskDetailsDt].Rows.Count > 0)
            {
                RiskDetails.Clear();

                foreach (DataRow row in detailsForEmailDS.Tables[ApplicationConstants.TenantRiskDetailsDt].Rows)
                {
                    RiskDetails.Append(row["CATDESC"] + (string.IsNullOrEmpty(Convert.ToString(row["SUBCATDESC"])) ? "" : ": " + row["SUBCATDESC"]) + "<br />");
                }
            }

            return RiskDetails.ToString();
        }

        #endregion

        #region "Get Vulnerability Details - As concatenated string split on separate row."

        private string getVulnerabilityDetails(DataSet detailsForEmailDS)
        {
            StringBuilder vulnerabilityDetails = new StringBuilder("N/A");


            if ((detailsForEmailDS != null) && (detailsForEmailDS.Tables[ApplicationConstants.TenantVulnerabilityDetailsDt] != null) && detailsForEmailDS.Tables[ApplicationConstants.TenantVulnerabilityDetailsDt].Rows.Count > 0)
            {
                vulnerabilityDetails.Clear();

                foreach (DataRow row in detailsForEmailDS.Tables[ApplicationConstants.TenantVulnerabilityDetailsDt].Rows)
                {
                    vulnerabilityDetails.Append(row["CATDESC"] + (string.IsNullOrEmpty(Convert.ToString(row["SUBCATDESC"])) ? "" : ": " + row["SUBCATDESC"]) + "<br />");
                }

            }

            return vulnerabilityDetails.ToString();
        }

        #endregion

        #endregion

        #region "Controls Reset Functions"

        #region "Reset Expenditure Dropdown List"

        private void resetExpenditureddl()
        {
            //ddlExpenditure.Items.Clear();
            //if (ddlExpenditure.Items.Count == 0)
            //{
            //    ddlExpenditure.Items.Insert(0, new ListItem(ApplicationConstants.DropDwonDefaultText, ApplicationConstants.DropDownDefaultValue.ToString()));
            //}
        }

        #endregion

        #region "Reset Budget Head"

        private void resetBudgetHead()
        {
            //ddlBudgetHead.Items.Clear();
            //if (ddlBudgetHead.Items.Count == 0)
            //{
            //    ddlBudgetHead.Items.Insert(0, new ListItem(ApplicationConstants.DropDwonDefaultText, ApplicationConstants.DropDownDefaultValue.ToString()));
            //}
        }

        #endregion

        #region "Reset Service Required"

        private void resetServiceRequired()
        {
            if (ddlVat.Items.Count > 0)
            {
                ddlVat.SelectedIndex = 0;
            }
            //ddlCostCentre.SelectedIndex = 0;
            //getBudgetHeadDropDownValuesByCostCentreId();
            //getExpenditureDropDownValuesByBudgetHeadId();
            txtNetCost.Text = string.Empty;
            txtVat.Text = string.Empty;
            txtTotal.Text = string.Empty;
        }

        #endregion

        #endregion

        #region "Show Modal Popup"

        public void showModalPopup()
        {
            ((ModalPopupExtender)this.Parent.Parent.FindControl("mdlPopupAssignToContractor")).Show();
        }

        #endregion

        #region "ddl Contact Selected Index Change"


        protected void ddlContact_SelectedIndexChanged(object sender, System.EventArgs e) 
        {
            if ((Convert.ToInt32(ddlContact.SelectedValue) != ApplicationConstants.DropDownDefaultValue))
            {
                AssignToContractorBL objAssignToContractorBl = new AssignToContractorBL(new AssignToContractorRepo());
                DataSet detailsForEmailDS = new DataSet();
                int selectedIndex = Convert.ToInt32(ddlContact.SelectedValue);
                objAssignToContractorBl.getContactEmailDetail(ref selectedIndex,ref detailsForEmailDS);

                if ((detailsForEmailDS.Tables[ApplicationConstants.ContactEmailDetailsDt] == null) | detailsForEmailDS.Tables[ApplicationConstants.ContactEmailDetailsDt].Rows.Count == 0)
                {
                    mdlPopupRis.Show();
                }
            }


        }

        #endregion

        #region "Hide Modal Popup"

        public void hideModalPopup()
        {
            ((ModalPopupExtender)this.Parent.Parent.FindControl("mdlPopupAssignToContractor")).Hide();
        }

        #endregion

        #endregion

        #region "View State Functions"

        #region "Service Required Data Table"

        /// <summary>
        /// Get the Service Required Data Table from the view state if a Data Table is not there
        /// Create a new data table and add required column.
        /// This is to be used to get data table of this type for the first time.
        /// </summary>
        /// <returns>A data table containing the required columns of Service required grid.</returns>
        /// <remarks></remarks>
        private DataTable getServiceRequiredDTViewState()
        {
            DataTable serviceRequiredDt = null;
            serviceRequiredDt = ViewState[ViewStateConstants.ServiceRequiredDT] as DataTable;

            if (serviceRequiredDt == null)
            {
                serviceRequiredDt = new DataTable();

                //3- Add Work Detail Id Column with data type Integer
                if (!serviceRequiredDt.Columns.Contains(ApplicationConstants.WorkDetailIdCol))
                {
                    DataColumn insertedColumn = null;
                    insertedColumn = serviceRequiredDt.Columns.Add(ApplicationConstants.WorkDetailIdCol, typeof(int));
                    insertedColumn.DefaultValue = ApplicationConstants.NoneValue;
                    insertedColumn.AllowDBNull = false;
                }

                //1- Add Service Required Column with data type string
                if (!serviceRequiredDt.Columns.Contains(ApplicationConstants.ServiceRequiredCol))
                {
                    DataColumn insertedColumn = null;
                    insertedColumn = serviceRequiredDt.Columns.Add(ApplicationConstants.ServiceRequiredCol, typeof(string));
                    insertedColumn.AllowDBNull = false;
                    insertedColumn.DefaultValue = string.Empty;
                    //Set Maximum length for Service Required Column.
                    insertedColumn.MaxLength = ApplicationConstants.MaxStringLegthWordRequired;
                }

                //2- Add Net Cost Column with data type Decimal
                if (!serviceRequiredDt.Columns.Contains(ApplicationConstants.NetCostCol))
                {
                    DataColumn insertedColumn = null;
                    insertedColumn = serviceRequiredDt.Columns.Add(ApplicationConstants.NetCostCol, typeof(decimal));
                    insertedColumn.AllowDBNull = false;
                }

                //3- Add Vat type Column with data type Integer
                if (!serviceRequiredDt.Columns.Contains(ApplicationConstants.VatTypeCol))
                {
                    DataColumn insertedColumn = null;
                    insertedColumn = serviceRequiredDt.Columns.Add(ApplicationConstants.VatTypeCol, typeof(int));
                    insertedColumn.AllowDBNull = false;
                }

                //4- Add Vat Column with data type Decimal
                if (!serviceRequiredDt.Columns.Contains(ApplicationConstants.VatCol))
                {
                    DataColumn insertedColumn = null;
                    insertedColumn = serviceRequiredDt.Columns.Add(ApplicationConstants.VatCol, typeof(decimal));
                    insertedColumn.AllowDBNull = false;
                }

                //5- Add Gross/Total Column with data type Decimal
                if (!serviceRequiredDt.Columns.Contains(ApplicationConstants.GrossCol))
                {
                    DataColumn insertedColumn = null;
                    insertedColumn = serviceRequiredDt.Columns.Add(ApplicationConstants.GrossCol, typeof(decimal));
                    insertedColumn.AllowDBNull = false;
                }

                //6- Add PI Status Column with data type Integer
                if (!serviceRequiredDt.Columns.Contains(ApplicationConstants.PIStatusCol))
                {
                    DataColumn insertedColumn = null;
                    insertedColumn = serviceRequiredDt.Columns.Add(ApplicationConstants.PIStatusCol, typeof(int));
                    insertedColumn.AllowDBNull = false;
                    insertedColumn.DefaultValue = false;
                }

                //7- Add Expenditure ID Col with data type Integer
                if (!serviceRequiredDt.Columns.Contains(ApplicationConstants.ExpenditureIdCol))
                {
                    DataColumn insertedColumn = null;
                    insertedColumn = serviceRequiredDt.Columns.Add(ApplicationConstants.ExpenditureIdCol, typeof(int));
                    insertedColumn.AllowDBNull = false;
                }

                //8- Add Budget Head Id Col with data type Integer
                if (!serviceRequiredDt.Columns.Contains(ApplicationConstants.BudgetHeadIdCol))
                {
                    DataColumn insertedColumn = null;
                    insertedColumn = serviceRequiredDt.Columns.Add(ApplicationConstants.BudgetHeadIdCol, typeof(int));
                    insertedColumn.AllowDBNull = false;
                }

                //9- Add Cost Center Id Col with data type Integer
                if (!serviceRequiredDt.Columns.Contains(ApplicationConstants.CostCenterIdCol))
                {
                    DataColumn insertedColumn = null;
                    insertedColumn = serviceRequiredDt.Columns.Add(ApplicationConstants.CostCenterIdCol, typeof(int));
                    insertedColumn.AllowDBNull = false;
                }

            }

            return serviceRequiredDt;
        }

        private void removeServiceRequiredDTViewState()
        {
            ViewState.Remove(ViewStateConstants.ServiceRequiredDT);
        }

        private void setServiceRequiredDTViewState(DataTable dt)
        {
            ViewState[ViewStateConstants.ServiceRequiredDT] = dt;
        }

        #endregion

        #endregion

        #region interface implementation

        #region save data
        public void saveData()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region load data
        public void loadData()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region populate drop down
        public void populateDropDown(DropDownList ddl)
        {
            throw new NotImplementedException();
        }
        #endregion

        #region populate drop downs
        public void populateDropDowns()
        {
            throw new NotImplementedException();
        }
        #endregion

        #endregion
    }
}