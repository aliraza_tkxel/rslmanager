﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AppointmentToBeArranged.ascx.cs"
    Inherits="PropertyDataRestructure.UserControls.M_E_Servicing.AppointmentToBeArranged" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<%@ Register TagName="AssignToContractor" TagPrefix="ucAssignToContractor" Src="~/UserControls/Scheduling/AssignToContractor.ascx" %>
<link href="../../Styles/Site.css" rel="stylesheet" media="screen" />
<link href="../../Styles/default.css" rel="stylesheet" type="text/css" />
<asp:UpdatePanel ID="updPanelAppointmentToBeArranged" runat="server" UpdateMode="Always">
    <ContentTemplate>
        <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" />
        <div style="height: 580px; overflow: auto;">
            <cc1:PagingGridView ID="grdAppointmentToBeArranged" runat="server" AllowPaging="false"
                AllowSorting="true" OnSorting="grdAppointmentToBeArranged_Sorting" ShowHeaderWhenEmpty="true"
                CssClass="table-body" AutoGenerateColumns="False" BorderStyle="Solid" BorderWidth="0px"
                CellPadding="8" CellSpacing="8" GridLines="None" PageSize="10" Width="99%" PagerSettings-Visible="false" EmptyDataText="No Record Found">
                <Columns>
                    <asp:TemplateField HeaderText="Scheme" SortExpression="Scheme">
                        <ItemTemplate>
                            <asp:Label ID="lblSchemeName" runat="server" Text='<%# Bind("Scheme") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="20%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Block" SortExpression="Block">
                        <ItemTemplate>
                            <asp:Label ID="lblBlock" runat="server" Text='<%# Bind("Block") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="10%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Postcode" SortExpression="Postcode">
                        <ItemTemplate>
                            <asp:Label ID="lblPostcode" runat="server" Text='<%# Bind("Postcode") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="10%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Next Service" SortExpression="NextService">
                        <ItemTemplate>
                            <asp:Label ID="lblNextService" runat="server" Text='<%# Bind("NextService") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="10%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Days" SortExpression="Days">
                        <ItemTemplate>
                            <asp:Label ID="lblDays" runat="server" Text='<%# Bind("Days") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="70px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Attribute" SortExpression="Attribute">
                        <ItemTemplate>
                            <asp:Label ID="lblAttribute" runat="server" Text='<%# Bind("Attribute") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="15%" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Status" SortExpression="Status">
                        <ItemTemplate>
                            <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle Width="10%" />
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:HiddenField ID="hdnLifeCycle" Value='<%# Bind("Lifecycle") %>' runat="server" />
                            <asp:HiddenField ID="hdnBlockId" Value='<%# Bind("BlockId") %>' runat="server" />
                            <asp:HiddenField ID="hdnSchemeId" Value='<%# Bind("SchemeId") %>' runat="server" />
                            <asp:Button ID="btnAssignToContractor" runat="server" OnClick="btnAssignToContractor_Click"
                                CommandArgument='<%# Bind("JournalId") %>' Text="Assign to contractor"></asp:Button>
                        </ItemTemplate>
                        <ItemStyle Width="5%" />
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Button ID="btnSchedule" runat="server" OnClick="btnSchedule_Click" CommandArgument='<%# Bind("JournalId") %>'
                                Text="Schedule"></asp:Button>
                        </ItemTemplate>
                        <ItemStyle Width="5%" />
                    </asp:TemplateField>
                </Columns>
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <HeaderStyle BackColor="#ffffff" CssClass="table-head-border-style" Font-Bold="True"
                    ForeColor="black" HorizontalAlign="Left" />
            </cc1:PagingGridView>
        </div>
        <br />
        <asp:Panel runat="server" ID="pnlPagination" Visible="true" Style="border-top: 1px solid  #000;
            vertical-align: middle; text-align: center;">
            <table style="margin-left: 200px; margin-top: 7px; width: 60%;">
                <tbody>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <asp:LinkButton ID="lnkbtnPagerFirst" Text="" runat="server" CommandName="Page" CommandArgument="First"
                                OnClick="lnkbtnPager_Click" Style="text-decoration: none;">&lt;First</asp:LinkButton>
                            &nbsp;
                        </td>
                        <td>
                            <asp:LinkButton ID="lnkbtnPagerPrev" Text="" runat="server" CommandName="Page" CommandArgument="Prev"
                                OnClick="lnkbtnPager_Click" Style="text-decoration: none;">&lt;Prev</asp:LinkButton>
                            &nbsp;
                        </td>
                        <td>
                            Records:
                            <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                            to&nbsp;
                            <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                            of&nbsp;
                            <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                            &nbsp;&nbsp; Page:&nbsp;
                            <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="currentPage" />
                            of&nbsp;
                            <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />
                        </td>
                        <td>
                            <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page"
                                CommandArgument="Next" OnClick="lnkbtnPager_Click" Style="text-decoration: none;" />
                            &nbsp;
                        </td>
                        <td>
                            <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>" runat="server" CommandName="Page"
                                CommandArgument="Last" OnClick="lnkbtnPager_Click" Style="text-decoration: none;" />
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </tbody>
            </table>
        </asp:Panel>
        <%-- Modal PopUp Assign to Contractor - Start --%>
        <%-- This hidden button is added just to set as Target Control Id for modal popup extender. --%>
        <asp:Button ID="btnHiddenAssigntoContractor" runat="server" Text="" Style="display: none;" />
        <ajax:ModalPopupExtender ID="mdlPopupAssignToContractor" runat="server" TargetControlID="btnHiddenAssigntoContractor"
            PopupControlID="pnlAssignToContractor" Enabled="true" DropShadow="true" BackgroundCssClass="modalBackground">
        </ajax:ModalPopupExtender>
        <asp:Panel ID="pnlAssignToContractor" runat="server" Style="min-width: 600px; max-width: 700px;">
            <asp:ImageButton ID="imgBtnClosePopup" OnClick="AssignToContractorBtnClose_Click"
                runat="server" Style="position: absolute; top: -12px; right: -12px;" ImageAlign="Top"
                ImageUrl="~/Images/cross2.png" BorderWidth="0" />
            <div style="width: 100%; height: 630px; overflow: auto;">
                <ucAssignToContractor:AssignToContractor ID="ucAssignToContractor" runat="server" />
            </div>
        </asp:Panel>
        <%-- Modal PopUp Assign - End --%>
    </ContentTemplate>
</asp:UpdatePanel>
