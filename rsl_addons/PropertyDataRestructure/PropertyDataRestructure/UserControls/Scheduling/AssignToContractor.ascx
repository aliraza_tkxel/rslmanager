﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="AssignToContractor.ascx.cs"
    Inherits="PropertyDataRestructure.UserControls.Scheduling.AssignToContractor" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<link href="../../Styles/scheduling.css" rel="stylesheet" type="text/css" />
<style type="text/css" media="all">
   
    .modalBack
        {
            background-color: Black;
            filter: alpha(opacity=30);
            opacity: 0.3;
            border: 1px solid black;
        }
</style>
<asp:UpdatePanel ID="updpnlAssignToContractor" runat="server" UpdateMode="Conditional">
    <ContentTemplate>
        <div class="assignToContractor">
            <div class="popupHeader">
                <strong>Assign to Contractor</strong>
            </div>
            <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" />
            <hr />
            <div class="input_row">
                <div class="input_label">
                    Cost Centre:</div>
                <div class="input_Cal">
                    <asp:DropDownList runat="server" ID="ddlCostCentre" Width="250px" OnSelectedIndexChanged="ddlCostCentre_SelectedIndexChanged"
                        AutoPostBack="True" />
                    <asp:RequiredFieldValidator runat="server" ID="rfvCostCentre" ControlToValidate="ddlCostCentre"
                        InitialValue="-1" ErrorMessage="Please select a Cost Centre" Display="Dynamic"
                        ValidationGroup="addServiceRequired" CssClass="Required" />
                </div>
            </div>
            <div class="input_row">
                <div class="input_label">
                    Budget Head:</div>
                <div class="input_Cal">
                    <asp:DropDownList runat="server" ID="ddlBudgetHead" Width="250px" OnSelectedIndexChanged="ddlBudgetHead_SelectedIndexChanged"
                        AutoPostBack="True" />
                    <asp:RequiredFieldValidator runat="server" ID="rfvBudgetHead" ControlToValidate="ddlBudgetHead"
                        InitialValue="-1" ErrorMessage="Please select a Budget Head" Display="Dynamic"
                        ValidationGroup="addServiceRequired" CssClass="Required" />
                </div>
            </div>
            <div class="input_row">
                <div class="input_label">
                    Expenditure:</div>
                <div class="input_Cal">
                    <asp:DropDownList runat="server" Width="250px" ID="ddlExpenditure" />
                    <asp:RequiredFieldValidator runat="server" ID="rfvExpenditure" ControlToValidate="ddlExpenditure"
                        InitialValue="-1" ErrorMessage="Please select an Expenditure" Display="Dynamic"
                        ValidationGroup="addServiceRequired" CssClass="Required" />
                </div>
            </div>
            <div class="input_row">
                <div class="input_label">
                    Contractor:</div>
                <div class="input_Cal">
                    <asp:DropDownList runat="server" ID="ddlContractor" Width="250px" OnSelectedIndexChanged="ddlContractor_SelectedIndexChanged"
                        AutoPostBack="True" />
                    <asp:RequiredFieldValidator runat="server" ID="rfvContractor" InitialValue="-1" ErrorMessage="Please select a Contractor"
                        Display="Dynamic" ValidationGroup="assignToContractor" ControlToValidate="ddlContractor"
                        CssClass="Required" />
                </div>
            </div>
            <div class="input_row">
                <div class="input_label">
                    Contact:</div>
                <div class="input_Cal">
                    <asp:DropDownList runat="server" Width="250px" ID="ddlContact" AutoPostBack="true" OnSelectedIndexChanged="ddlContact_SelectedIndexChanged">
                        <asp:ListItem Selected="True" Text="-- Please select contractor --" Value="-1" />
                    </asp:DropDownList>
                    
                </div>
            </div>
            <hr />
            <div class="input_row">
                <div class="input_label">
                    <strong>Service Required:</strong></div>
                <div class="input_Cal">
                    <asp:Label ID="lblServiceRequired" runat="server" Text="" Font-Bold="true"></asp:Label>
                </div>
                <br />
            </div>
            <div class="input_row">
                <div class="input_label">
                    <strong>Lifecycle: </strong>
                </div>
                <div class="input_Cal">
                    <asp:Label ID="lblLifecycle" runat="server" Text="" Font-Bold="true"></asp:Label>
                </div>
                <br />
            </div>
            <div class="input_row">
                <div class="input_label">
                    <strong>Service Due:</strong></div>
                <div class="input_Cal">
                    <asp:Label ID="lblServiceDue" runat="server" Text="" Font-Bold="true"></asp:Label>
                </div>
                <br />
            </div>
            <div class="input_row">
                <div class="input_label">
                    Estimate (£):</div>
                <div class="input">
                    <asp:TextBox runat="server" ID="txtEstimate" MaxLength="11" Width="250" TextMode="SingleLine" />
                    <ajaxToolkit:TextBoxWatermarkExtender ID="txtWaterExtEstimate" runat="server" TargetControlID="txtEstimate"
                        WatermarkText="Required" WatermarkCssClass="searchTextDefault">
                    </ajaxToolkit:TextBoxWatermarkExtender>
                    <asp:RequiredFieldValidator ErrorMessage="Please enter estimate, in form XXXXXX.XXXX where X is a digit between 0 to 9."
                        ControlToValidate="txtEstimate" ID="rfvEstimate" ValidationGroup="assignToContractor"
                        runat="server" CssClass="Required" Display="Dynamic" />
                    <asp:RegularExpressionValidator ID="rxvEstimate" runat="server" ErrorMessage="Please enter estimate, in form XXXXXX.XXXX where X is a digit between 0 to 9."
                        Display="Dynamic" ValidationGroup="assignToContractor" CssClass="Required" ControlToValidate="txtEstimate"
                        ValidationExpression="^[0-9]{1,6}(?:\.[0-9]{1,4})?$" />
                    <asp:RangeValidator ID="rvEstimate" ErrorMessage='Please enter a value between "0" and "214748.3647".'
                        ControlToValidate="txtEstimate" runat="server" MinimumValue="0" MaximumValue="214748.3647"
                        Display="Dynamic" CssClass="Required" ValidationGroup="assignToContractor" Type="Double" />
                </div>
            </div>
            <div class="input_row">
                <div class="input_label">
                    Estimate Ref:</div>
                <div class="input">
                    <asp:TextBox runat="server" ID="txtEstimateRef" Width="250" MaxLength="200" />
                    <ajaxToolkit:TextBoxWatermarkExtender ID="txtWaterExtEstimateRef" runat="server"
                        TargetControlID="txtEstimateRef" WatermarkText="Optional" WatermarkCssClass="searchTextDefault">
                    </ajaxToolkit:TextBoxWatermarkExtender>
                    <asp:RegularExpressionValidator runat="server" ID="rxvEstimateRef" ErrorMessage="Works Required must not exceed 200 characters"
                        Display="Dynamic" CssClass="Required" ControlToValidate="txtEstimateRef" ValidationGroup="assignToContractor"
                        ValidationExpression="^[\s\S]{0,200}$" Style="vertical-align: top;" />
                </div>
            </div>
            <div class="input_row">
                <div class="input_label">
                    <strong>Contract Start:</strong></div>
                <div class="input_Cal">
                    <asp:HiddenField ID="hdnContractStart" runat="server" />
                    <asp:TextBox runat="server" ID="txtContractStart" Width="140px" />
                    <ajaxToolkit:TextBoxWatermarkExtender ID="txtWaterExtContractStart" runat="server"
                        TargetControlID="txtContractStart" WatermarkText="DD/MM/YYYY" WatermarkCssClass="searchTextDefault">
                    </ajaxToolkit:TextBoxWatermarkExtender>
                    <ajaxToolkit:CalendarExtender ID="calExtContractStart" TargetControlID="txtContractStart"
                        Format="dd/MM/yyyy" PopupButtonID="txtContractStart" DefaultView="Days" runat="server">
                    </ajaxToolkit:CalendarExtender>
                    <asp:CompareValidator ID="CompareValidator13" runat="server" Style="vertical-align: top;"
                        ControlToValidate="txtContractStart" ErrorMessage="Enter valid date" Operator="DataTypeCheck"
                        Type="Date" Display="Dynamic" CssClass="Required" ValidationGroup="assignToContractor" />
                </div>
            </div>
            <div class="input_row">
                <div class="input_label">
                    <strong>Contract End:</strong></div>
                <div class="input_Cal">
                    <asp:HiddenField ID="hdnContractEnd" runat="server" />
                    <asp:TextBox runat="server" ID="txtContractEnd" Width="140px" />
                    <ajaxToolkit:TextBoxWatermarkExtender ID="txtWaterExtContractEnd" runat="server"
                        TargetControlID="txtContractEnd" WatermarkText="DD/MM/YYYY" WatermarkCssClass="searchTextDefault">
                    </ajaxToolkit:TextBoxWatermarkExtender>
                    <ajaxToolkit:CalendarExtender ID="calExtContractEnd" TargetControlID="txtContractEnd"
                        Format="dd/MM/yyyy" PopupButtonID="txtContractEnd" runat="server">
                    </ajaxToolkit:CalendarExtender>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" Style="vertical-align: top;"
                        ControlToValidate="txtContractEnd" ErrorMessage="Enter valid date" Operator="DataTypeCheck"
                        Type="Date" Display="Dynamic" CssClass="Required" ValidationGroup="assignToContractor" />
                </div>
            </div>
            <div class="input_row">
                <div class="input_label">
                    Net Cost (£):</div>
                <div class="input_small">
                    <asp:TextBox runat="server" ID="txtNetCost" OnTextChanged="txtNetCost_TextChanged"
                        AutoPostBack="True" CausesValidation="true" ValidationGroup="addServiceRequired"
                        MaxLength="11"></asp:TextBox>
                    <asp:RequiredFieldValidator ErrorMessage="Please enter net cost, in form XXXXXX.XXXX where X is a digit between 0 to 9."
                        ControlToValidate="txtNetCost" ID="rfvNetCost" ValidationGroup="addServiceRequired"
                        runat="server" CssClass="Required" Display="Dynamic" />
                    <asp:RegularExpressionValidator ID="rxvNetCost" runat="server" ErrorMessage="Please enter net Cost, in form XXXXXX.XXXX where X is a digit between 0 to 9."
                        Display="Dynamic" ValidationGroup="addServiceRequired" CssClass="Required" ControlToValidate="txtNetCost"
                        ValidationExpression="^[0-9]{1,6}(?:\.[0-9]{1,4})?$" />
                    <asp:RangeValidator ID="rvNetCost" runat="server" ControlToValidate="txtNetCost"
                        MinimumValue="0" MaximumValue="178956.9704" ErrorMessage="Please enter a value between &quot;0&quot; and &quot;178956.9704&quot;."
                        CssClass="Required" Type="Double" Display="Dynamic" ValidationGroup="addServiceRequired" />
                </div>
            </div>
            <div class="input_row">
                <div class="input_label">
                    Vat:</div>
                <div class="input_small">
                    <asp:DropDownList runat="server" ID="ddlVat" OnSelectedIndexChanged="ddlVat_SelectedIndexChanged"
                        AutoPostBack="True" CausesValidation="True" />
                    <asp:RequiredFieldValidator runat="server" ID="rfvddlVat" InitialValue="-1" ErrorMessage="Please select a Vat(Vat Rate)"
                        Display="Dynamic" ValidationGroup="addServiceRequired" ControlToValidate="ddlVat"
                        CssClass="Required" />
                </div>
            </div>
            <div class="input_row">
                <div class="input_label">
                    Vat (£):</div>
                <div class="input_small">
                    <asp:TextBox runat="server" ID="txtVat" ReadOnly="true"></asp:TextBox>
                </div>
            </div>
            <div class="input_row">
                <div class="input_label">
                    <strong>Total (£):</strong></div>
                <div class="input_small">
                    <asp:TextBox runat="server" ID="txtTotal" ReadOnly="true"></asp:TextBox>
                    <asp:Button Text="Add" runat="server" UseSubmitBehavior="false" ID="btnAdd" OnClick="btnAdd_Click"
                        CssClass="addButton" ValidationGroup="addServiceRequired" />
                </div>
            </div>
            <hr />
            <div class="worksRequiredGrid">
                <asp:GridView runat="server" ID="grdWorksDeatil" OnRowDataBound="grdWorksDeatil_RowDataBound"
                    ShowFooter="True" GridLines="Horizontal" AutoGenerateColumns="False" PageSize="100"
                    ShowHeaderWhenEmpty="True" Width="100%" BorderStyle="None" EmptyDataText="No Record Found">
                    <HeaderStyle Font-Bold="false" />
                    <FooterStyle BorderColor="Black" Font-Bold="true" HorizontalAlign="Right" Wrap="False" />
                    <RowStyle BorderColor="Gray" BorderWidth="0px" BorderStyle="None" />
                    <Columns>
                        <asp:BoundField HeaderText="Service Required:" FooterText="Total:" DataField="ServiceRequired"
                            HeaderStyle-HorizontalAlign="Left" ItemStyle-CssClass="worksRequired" />
                        <asp:BoundField HeaderText="Net:" FooterText="0.00" DataFormatString="{0:0.####}"
                            DataField="NetCost" FooterStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center"
                            ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="100px" />
                        <asp:BoundField HeaderText="Vat:" HeaderStyle-HorizontalAlign="Center" FooterText="0.00"
                            DataFormatString="{0:0.####}" DataField="Vat" FooterStyle-HorizontalAlign="Center"
                            ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="100px" />
                        <asp:BoundField HeaderText="Gross:" HeaderStyle-HorizontalAlign="Center" FooterText="0.00"
                            DataFormatString="{0:0.####}" DataField="Gross" FooterStyle-HorizontalAlign="Center"
                            ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="100px" />
                    </Columns>
                </asp:GridView>
            </div>
            <hr />
            <div class="bottomButtonsContainer">
                <div class="buttonFloatLeft">
                    <asp:Button ID="btnClose" OnClick="btnClose_Click" runat="server" Text="Close" CausesValidation="false" />
                </div>
                <div class="buttonFloatRight">
                    <asp:Button ID="btnAssignToContractor" UseSubmitBehavior="false" OnClick="btnAssignToContractor_Click"
                        runat="server" Style="text-align: right" Text="Assign to selected contractor"
                        ValidationGroup="assignToContractor" />
                </div>
                <div class="clear" />
            </div>
            <div class="clear" />
            <hr />
            <br />
        </div>
          <asp:Panel ID="pnlNoRecordFound" runat="server" BackColor="White" Width="350px"
        Style="font-weight: normal; font-size: 13px; padding: 10px;">
        <b> Warning
        </b>
              <br>
        </br>
        <hr>
        <p>No contact details exist for this Supplier, and therefore they have not received an email notification that the Purchase Order has been approved.
				Please contact the supplier directly with the Purchase Order details. 
				To update the Supplier records with contact details please contact a member of the Finance Team. </p>
                  <asp:Button ID="btnOk" runat="server" Text="OK" UseSubmitBehavior="false" OnClick="btnOk_click" />
         </asp:Panel>
         <asp:Button ID="btnHidde" runat="server" Text="" Style="display: none;" />
    <ajaxToolkit:ModalPopupExtender ID="mdlPopupRis" runat="server" TargetControlID="btnHidde"
        PopupControlID="pnlNoRecordFound" Enabled="true" DropShadow="true" BackgroundCssClass="modalBack">
    </ajaxToolkit:ModalPopupExtender>
    </ContentTemplate>
</asp:UpdatePanel>
