﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PDR_Utilities.Helpers;
using PropertyDataRestructure.Interface;
using PropertyDataRestructure.Base;
using PDR_Utilities.Managers;
using PDR_BusinessObject.PageSort;
using PDR_BusinessObject.TradeAppointment;
using PDR_BusinessObject.AssignToContractor;
using PDR_BusinessLogic.Scheduling;
using PDR_BusinessLogic.AssignToContractor;
using PDR_DataAccess.AssignToContractor;
using PDR_Utilities.Constants;
using System.Data;
using PDR_DataAccess.Scheduling;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Globalization;
using PDR_BusinessObject.MeSearch;
using System.Collections.Specialized;

namespace PropertyDataRestructure.UserControls.M_E_Servicing
{
    public partial class AppointmentArranged : UserControlBase, IListingPage
    {
        #region Properties
        #endregion

        #region Events

        #region Page Load
        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                ucAssignToContractor.CloseButtonClicked += new EventHandler(AssignToContractorBtnClose_Click);

                uiMessage.hideMessage();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #region Pager Event Handler
        /// <summary>
        /// Pager event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkbtnPager_Click(object sender, EventArgs e)
        {
            try
            {
                PageSortBO objPageSortBo = new PageSortBO("DESC", "LoggedDate", 1, 30);
                pageSortViewState = GridHelper.processGridPager(sender, objPageSortBo, pageSortViewState);
                loadData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region Sorting Event Handler
        /// <summary>
        /// Sorting event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdAppointmentArranged_Sorting(Object sender, GridViewSortEventArgs e)
        {
            try
            {
                GridView grdAppointmentArranged = (GridView)sender;
                PageSortBO objPageSortBo = new PageSortBO("DESC", "LoggedDate", 1, 30);
                pageSortViewState = GridHelper.sortGrid(grdAppointmentArranged, e, objPageSortBo, pageSortViewState);
                loadData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region Button View Event Handler
        /// <summary>
        /// Button View Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btnView_Click(Object sender, EventArgs e)
        {
            try
            {
                viewDetail(sender);
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region "Assign to Contractor Control - Close Button Clicked And Also imgBtnClosePopup(Red Cross Button) Clicked"

        protected void AssignToContractorBtnClose_Click(object sender, EventArgs e)
        {
            try
            {
                mdlPopupAssignToContractor.Hide();

                //In case a work is assigned to contractor re-populate the appointment to be arranged grid as data is changed.
                if (ucAssignToContractor.IsSaved)
                {
                    PageSortBO objPageSortBo = new PageSortBO("DESC", "LoggedDate", 1, 30);
                    objPageSortBo = pageSortViewState;
                    objPageSortBo.PageNumber = 1;
                    grdAppointmentArranged.PageIndex = 0;
                    objPageSortBo.setSortDirection();
                    pageSortViewState = objPageSortBo;

                    loadData();

                    ((UpdatePanel)Parent.FindControl("updPanelMEServicing")).Update();
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #endregion

        #region Function

        #region View Detail
        /// <summary>
        /// View Detail
        /// </summary>
        public void viewDetail(Object sender)
        {
            ImageButton btnView = (ImageButton)sender;
            GridViewRow row = (GridViewRow)btnView.NamingContainer;
            HiddenField hdnStatus = (HiddenField)row.FindControl("hdnStatus");

            if (hdnStatus.Value.Trim().Equals(ApplicationConstants.AssignToContractorStatus))
            {
                openAssignToContractorDetail(sender);
            }
            else
            {
                openAppointmentArrangedSummary(sender);
            }

        }
        #endregion

        #region Open Assign To Contractor Detail
        /// <summary>
        /// Open Assign To Contractor Detail
        /// </summary>
        public void openAssignToContractorDetail(Object sender)
        {
            ImageButton btnView = (ImageButton)sender;
            GridViewRow row = (GridViewRow)btnView.NamingContainer;
            HiddenField hdnStatus = (HiddenField)row.FindControl("hdnStatus");
            Label lblAttribute = (Label)row.FindControl("lblAttribute");
            HiddenField hdnLifeCycle = (HiddenField)row.FindControl("hdnLifeCycle");
            HiddenField hdnNextService = (HiddenField)row.FindControl("hdnNextService");
            HiddenField hdnBlockId = (HiddenField)row.FindControl("hdnBlockId");
            HiddenField hdnSchemeId = (HiddenField)row.FindControl("hdnSchemeId");

            int journalId = Convert.ToInt32(btnView.CommandArgument);
            string attributeName = lblAttribute.Text;
            string lifeCycle = hdnLifeCycle.Value;
            string msatType = objSession.MeSearchBO.MsatType;
            bool amend = true;
            DataSet resultDataset = new DataSet();
            AssignToContractorBL objAssignToContractorBL = new AssignToContractorBL(new AssignToContractorRepo());
            objAssignToContractorBL.getAssignToContractorDetailByJournalId(ref resultDataset, journalId);

            DataTable contractorWorkDt = resultDataset.Tables[ApplicationConstants.ContractorWorkDt];
            DataTable contractorWorkDetailDt = resultDataset.Tables[ApplicationConstants.ContractorWorkDetailDt];

            if (contractorWorkDt.Rows.Count == 0)
            {
                uiMessage.showErrorMessage(UserMessageConstants.InvalidJournalId);
            }
            else
            {

                AssignToContractorBo objAssignToContractorBo = new AssignToContractorBo();
                objAssignToContractorBo.PdrContractorId = Convert.ToInt32(contractorWorkDt.Rows[0][ApplicationConstants.PdrContractorIdCol]);
                objAssignToContractorBo.JournalId = journalId;
                objAssignToContractorBo.MsatType = msatType;
                objAssignToContractorBo.ServiceRequired = lblAttribute.Text;
                objAssignToContractorBo.LifeCycle = hdnLifeCycle.Value;
                objAssignToContractorBo.ServiceDue = hdnNextService.Value;
                objAssignToContractorBo.ContractorId = Convert.ToInt32(contractorWorkDt.Rows[0][ApplicationConstants.ContractorIdCol]);
                objAssignToContractorBo.ContractorName = Convert.ToString(contractorWorkDt.Rows[0][ApplicationConstants.ContractorNameCol]);
                objAssignToContractorBo.ContactId = Convert.ToInt32(contractorWorkDt.Rows[0][ApplicationConstants.ContactIdCol]);
                objAssignToContractorBo.Estimate = Convert.ToDecimal(contractorWorkDt.Rows[0][ApplicationConstants.EstimateCol]);
                objAssignToContractorBo.EstimateRef = Convert.ToString(contractorWorkDt.Rows[0][ApplicationConstants.EstimateRefCol]);
                objAssignToContractorBo.ServiceRequiredDt = contractorWorkDetailDt;
                objAssignToContractorBo.SchemeId = Convert.ToInt32(hdnSchemeId.Value);
                objAssignToContractorBo.BlockId = Convert.ToInt32(hdnBlockId.Value);
                objAssignToContractorBo.ContractStartDate = contractorWorkDt.Rows[0][ApplicationConstants.ContractStartDateCol].ToString().Split(' ')[0];
                objAssignToContractorBo.ContractEndDate = contractorWorkDt.Rows[0][ApplicationConstants.ContractEndDateCol].ToString().Split(' ')[0];

                objSession.AssignToContractorBo = null;
                objSession.AssignToContractorBo = objAssignToContractorBo;

                ucAssignToContractor.populateControl(amend);
                mdlPopupAssignToContractor.Show();

            }

        }
        #endregion

        #region Open Appointment Arranged Summary
        /// <summary>
        /// Open Appointment Arranged Summary
        /// </summary>
        public void openAppointmentArrangedSummary(Object sender)
        {
            ImageButton btnView = (ImageButton)sender;
            GridViewRow row = (GridViewRow)btnView.NamingContainer;
            HiddenField hdnAppointmentId = (HiddenField)row.FindControl("hdnAppointmentId");
            Label lblAttribute = (Label)row.FindControl("lblAttribute");
            NameValueCollection pageQueryString = HttpUtility.ParseQueryString(HttpContext.Current.Request.QueryString.ToString());
            pageQueryString.Remove(PathConstants.ListingTab);
            pageQueryString.Add(PathConstants.ListingTab, ApplicationConstants.AppointmentArrangedTab);

            int journalId = Convert.ToInt32(btnView.CommandArgument);
            int appointmentId = Convert.ToInt32(hdnAppointmentId.Value);
            string attributeName = lblAttribute.Text;
            SelectedAttributeBO objSelectedAttributeBO = new SelectedAttributeBO();
            objSelectedAttributeBO.JournalId = journalId;
            objSelectedAttributeBO.AttributeName = attributeName;
            objSelectedAttributeBO.AppointmentId = appointmentId;
            objSelectedAttributeBO.MsatType = objSession.MeSearchBO.MsatType;
            objSelectedAttributeBO.MsatQueryStringPath = "?" + pageQueryString;
            objSession.SelectedAttribute = null;
            objSession.SelectedAttribute = objSelectedAttributeBO;
            Response.Redirect(PathConstants.ArrangedAppointmentSummaryPath + objSelectedAttributeBO.MsatQueryStringPath);
        }
        #endregion

        #region Load Data
        /// <summary>
        /// Load Data
        /// </summary>
        public void loadData()
        {
            MeSearchBO objMeSearchBO = objSession.MeSearchBO;

            PageSortBO objPageSortBo;
            if (pageSortViewState == null)
            {
                objPageSortBo = new PageSortBO("DESC", "LoggedDate", 1, 30);
                pageSortViewState = objPageSortBo;
            }
            else
            {
                objPageSortBo = pageSortViewState;
            }

            int totalCount = 0;
            SchedulingBL objSchedulingBL = new SchedulingBL(new SchedulingRepo());
            DataSet resultDataSet = new DataSet();
            totalCount = objSchedulingBL.getAppointmentArrangedList(ref resultDataSet, objPageSortBo, objMeSearchBO);
            grdAppointmentArranged.DataSource = resultDataSet;
            grdAppointmentArranged.DataBind();
            objPageSortBo.TotalRecords = totalCount;
            objPageSortBo.TotalPages = (int)Math.Ceiling((Double)totalCount / objPageSortBo.PageSize);

            if (objPageSortBo.TotalPages > 0)
            {
                pnlPagination.Visible = true;
            }
            else
            {
                pnlPagination.Visible = false;
               // uiMessage.showErrorMessage(UserMessageConstants.RecordNotFound);
            }
            pageSortViewState = objPageSortBo;

            GridHelper.setGridViewPager(ref pnlPagination, objPageSortBo);
        }
        #endregion

        #region Search Data
        public void searchData()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Populate Data
        public void populateData()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Print Data
        public void printData()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Export Grid to Excel
        public void exportGridToExcel()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Export to Pdf
        public void exportToPdf()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Download Data
        public void downloadData()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Apply filters
        public void applyFilters()
        {
            throw new NotImplementedException();
        }
        #endregion

        #endregion

    }
}