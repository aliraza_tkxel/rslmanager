﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PropertyDataRestructure.Interface;
using PDR_BusinessObject.PageSort;
using PDR_BusinessObject.TradeAppointment;
using PDR_BusinessObject.AssignToContractor;
using PropertyDataRestructure.Base;
using PDR_Utilities.Helpers;
using PDR_BusinessLogic.Scheduling;
using PDR_Utilities.Constants;
using System.Data;
using PDR_DataAccess.Scheduling;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_BusinessObject.MeSearch;
using System.Collections.Specialized;

namespace PropertyDataRestructure.UserControls.M_E_Servicing
{
    public partial class AppointmentToBeArranged : UserControlBase, IListingPage
    {
        #region Properties
        #endregion

        #region Events

        #region Page Load
        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {            
            try
            {
                if (!IsPostBack) {
                    ucAssignToContractor.CloseButtonClicked += new EventHandler(AssignToContractorBtnClose_Click);
                }
                
                uiMessage.hideMessage();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessage.isError == true)
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #region Pager Event Handler
        /// <summary>
        /// Pager event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkbtnPager_Click(object sender, EventArgs e)
        {
            try
            {
                PageSortBO objPageSortBo = new PageSortBO("DESC", "LoggedDate", 1, 30);
                pageSortViewState = GridHelper.processGridPager(sender, objPageSortBo, pageSortViewState);
                loadData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region Sorting Event Handler
        /// <summary>
        /// Sorting event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdAppointmentToBeArranged_Sorting(Object sender, GridViewSortEventArgs e) 
        {
            try
            {
                GridView grdAppointmentToBeArranged = (GridView) sender;
                PageSortBO objPageSortBo = new PageSortBO("DESC", "LoggedDate", 1, 30);
                pageSortViewState = GridHelper.sortGrid(grdAppointmentToBeArranged, e, objPageSortBo, pageSortViewState);
                loadData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region Button Assign To Contractor Event Handler
        /// <summary>
        /// Button Assign To Contractor Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btnAssignToContractor_Click(Object sender, EventArgs e)
        {
            try
            {
                Button btnAssignToContractor = (Button) sender;
                GridViewRow row = (GridViewRow)btnAssignToContractor.NamingContainer;
                Label lblAttribute = (Label)row.FindControl("lblAttribute");
                Label lblNextService = (Label)row.FindControl("lblNextService");                
                HiddenField hdnLifeCycle = (HiddenField)row.FindControl("hdnLifeCycle");
                HiddenField hdnBlockId = (HiddenField)row.FindControl("hdnBlockId");
                HiddenField hdnSchemeId = (HiddenField)row.FindControl("hdnSchemeId");
                MeSearchBO objMeSearchBO = objSession.MeSearchBO; 

                int journalId = Convert.ToInt32(btnAssignToContractor.CommandArgument);
                string msatType = objMeSearchBO.MsatType;
                bool amend = false;

                AssignToContractorBo objAssignToContractorBo = new AssignToContractorBo();
                objAssignToContractorBo.JournalId = journalId;
                objAssignToContractorBo.MsatType = msatType;
                objAssignToContractorBo.ServiceRequired = lblAttribute.Text;
                objAssignToContractorBo.LifeCycle = hdnLifeCycle.Value;
                objAssignToContractorBo.ServiceDue = lblNextService.Text;
                objAssignToContractorBo.SchemeId = Convert.ToInt32(hdnSchemeId.Value);
                objAssignToContractorBo.BlockId = Convert.ToInt32(hdnBlockId.Value);

                objSession.AssignToContractorBo = null;
                objSession.AssignToContractorBo = objAssignToContractorBo;

                ucAssignToContractor.populateControl(amend);
                mdlPopupAssignToContractor.Show();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region Button Schedule Event Handler
        /// <summary>
        /// Button Schedule Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btnSchedule_Click(Object sender, EventArgs e)
        {
            try
            {
                Button btnSchedule = (Button)sender;
                int journalId = Convert.ToInt32(btnSchedule.CommandArgument);
                GridViewRow row = (GridViewRow)btnSchedule.NamingContainer;
                Label lblAttribute = (Label)row.FindControl("lblAttribute");
                Label lblBlock = (Label)row.FindControl("lblBlock");
                Label lblScheme = (Label)row.FindControl("lblSchemeName");
                SelectedAttributeBO objSelectedAttributeBO = new SelectedAttributeBO();
                NameValueCollection pageQueryString = HttpUtility.ParseQueryString(HttpContext.Current.Request.QueryString.ToString());
                pageQueryString.Remove(PathConstants.ListingTab);
                pageQueryString.Add(PathConstants.ListingTab, ApplicationConstants.AppointmentToBeArrangedTab);

                MeSearchBO objMeSearchBO = objSession.MeSearchBO; 

                objSelectedAttributeBO.JournalId = journalId;
                objSelectedAttributeBO.MsatType = objMeSearchBO.MsatType;
                objSelectedAttributeBO.AttributeName = Convert.ToString(lblAttribute.Text);
                objSelectedAttributeBO.SchemeName = (lblScheme.Text.Equals("-") ? ApplicationConstants.NonSelected : lblScheme.Text);
                objSelectedAttributeBO.BlockName = (lblBlock.Text.Equals("-") ? ApplicationConstants.NonSelected : lblBlock.Text);
                objSelectedAttributeBO.MsatQueryStringPath = "?" + pageQueryString;
                objSession.SelectedAttribute = null;
                objSession.SelectedAttribute = objSelectedAttributeBO;

                Response.Redirect(PathConstants.ScheduleMeServicing + objSelectedAttributeBO.MsatQueryStringPath);

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region "Assign to Contractor Control - Close Button Clicked And Also imgBtnClosePopup(Red Cross Button) Clicked"

        protected void AssignToContractorBtnClose_Click(object sender, EventArgs e)
        {
            try
            {
                mdlPopupAssignToContractor.Hide();

                //In case a work is assigned to contractor re-populate the appointment to be arranged grid as data is changed.
                if (ucAssignToContractor.IsSaved)
                {
                    PageSortBO objPageSortBo = new PageSortBO("DESC", "LoggedDate", 1, 30);
                    objPageSortBo = pageSortViewState;
                    objPageSortBo.PageNumber = 1;
                    grdAppointmentToBeArranged.PageIndex = 0;
                    objPageSortBo.setSortDirection();
                    pageSortViewState = objPageSortBo;

                    loadData();

                    ((UpdatePanel)Parent.FindControl("updPanelMEServicing")).Update();
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #endregion

        #region Function

        #region Load Data
        /// <summary>
        /// Load Data
        /// </summary>
        public void loadData()
        {
            MeSearchBO objMeSearchBO = objSession.MeSearchBO;

            PageSortBO objPageSortBo;
            if (pageSortViewState == null)
            {
                objPageSortBo = new PageSortBO("DESC", "LoggedDate", 1, 30);
                pageSortViewState = objPageSortBo;
            }
            else {
                objPageSortBo = pageSortViewState;
            }
          
            int totalCount = 0;
            SchedulingBL objSchedulingBL = new SchedulingBL(new SchedulingRepo());
            DataSet resultDataSet = new DataSet();
            totalCount = objSchedulingBL.getAppointmentToBeArrangedList(ref resultDataSet, objPageSortBo, objMeSearchBO);
            grdAppointmentToBeArranged.DataSource = resultDataSet;
            grdAppointmentToBeArranged.DataBind();
            objPageSortBo.TotalRecords = totalCount;
            objPageSortBo.TotalPages = Convert.ToInt32(Math.Ceiling((Double) totalCount / objPageSortBo.PageSize));

            if (objPageSortBo.TotalPages > 0)
            {
                pnlPagination.Visible = true;                
            }
            else {
                pnlPagination.Visible = false;
               // uiMessage.showErrorMessage(UserMessageConstants.RecordNotFound);
            }
            pageSortViewState = objPageSortBo;
            GridHelper.setGridViewPager(ref pnlPagination, objPageSortBo);
        }
        #endregion

        #region Search Data
        public void searchData()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Populate Data
        public void populateData()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Print Data
        public void printData()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Export Grid To Excel
        public void exportGridToExcel()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Export To Pdf
        public void exportToPdf()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Download Data
        public void downloadData()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Apply Filters
        public void applyFilters()
        {
            throw new NotImplementedException();
        }
        #endregion

        #endregion

    }
}