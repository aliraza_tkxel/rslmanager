﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PropertyDataRestructure.Interface;
using PDR_BusinessLogic.Scheme;
using System.Data;
using PDR_DataAccess.Scheme;

namespace PropertyDataRestructure.UserControls.Common
{
    public partial class SchemeDropDown : System.Web.UI.UserControl
    {
        bool IsError = false;
        string message = string.Empty;
        #region Events
        #region Page Load event
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    loadData();
                }
            }
            catch (Exception ex)
            {
                IsError = true;
                message = ex.Message;
                //if ((uiMessageHelper.IsExceptionLogged == false))
                //{
                //    ExceptionPolicy.HandleException(ex, "Exception Policy");
                //}
            }
            finally
            {
                if ((IsError == true))
                {
                    pnlMessage.Visible = true;
                    lblMessage.Text = message;

                }
            }
        }
        #endregion
        #endregion

        #region Functions
        #region Load data
        public void loadData()
        {
            SchemeBL objSchemeBl = new SchemeBL(new SchemeRepo());
            DataSet resultDataSet = new DataSet();
            resultDataSet = objSchemeBl.populateSchemeDropDown();
            ddlScheme.DataSource = resultDataSet;
            ddlScheme.DataValueField = "SCHEMEID";
            ddlScheme.DataTextField = "SCHEMENAME";
            ddlScheme.DataBind();
            ddlScheme.Items.Insert(0, new ListItem("Select Scheme", "-1"));
            ddlScheme.SelectedValue = "-1";

        }
        #endregion
        #endregion



    }
}