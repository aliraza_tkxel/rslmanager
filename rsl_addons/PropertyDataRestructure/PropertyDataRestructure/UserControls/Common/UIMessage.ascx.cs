﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PropertyDataRestructure.Base;
using System.ComponentModel;
using System.Drawing;

namespace PropertyDataRestructure.UserControls.Common
{
    public partial class UIMessage : UserControlBase
    {
        private const string DefaultWidth = "400px";

        private const string StatusViewState = "_STY";

        public UIMessageControlType bubbleType
        {
            get
            {
                object item = this.ViewState["_STY"];
                if (item == null)
                {
                    return UIMessageControlType.Information;
                }
                return (UIMessageControlType)item;
            }
            set
            {
                this.ViewState["_STY"] = value;
                UIMessageControlType statusControlType = value;
                switch (statusControlType)
                {
                    case UIMessageControlType.Information:
                        {
                            this.StatusBox.Attributes["class"] = "StatusInfo";
                            return;
                        }
                    case UIMessageControlType.Warning:
                        {
                            this.StatusBox.Attributes["class"] = "StatusWarning";
                            return;
                        }
                    case UIMessageControlType.Error:
                        {
                            this.StatusBox.Attributes["class"] = "StatusError";
                            return;
                        }
                    default:
                        {
                            return;
                        }
                }
            }
        }

        [Bindable(true)]
        [Category("Appearance")]
        [Description("Text color.")]
        public Color  messageTextColor
        {
            get
            {
                return this.StatusLabel.ForeColor;
            }
            set
            {
                this.StatusLabel.ForeColor = value;
            }
        }

        [Bindable(true)]
        [Category("Appearance")]
        [Description("Text that appears inside the text entry field.")]
        public string messageText
        {
            get
            {
                return this.StatusLabel.Text;
            }
            set
            {
                this.StatusLabel.Text = value;                
            }
        }

        [Bindable(true)]
        [Category("Appearance")]
        [DefaultValue("400px")]
        [Description("Width of the control in CSS units.")]
        public string width
        {
            get
            {
                object item = this.ViewState["width"];
                if (item == null)
                {
                    return "400px";
                }
                return (string)item;
            }
            set
            {
                this.ViewState["width"] = value;
            }
        }

        public bool isExceptionLogged = false;
        public bool isError = false;

        public void addLinkButton(string text, string clientEvent)
        {
            Literal literal = new Literal();
            literal.Text = string.Format("<span style=\"color:#999;\">&nbsp;[&nbsp;<a href=\"javascript:{1}\">{0}</a>&nbsp;]</span>", text, clientEvent);
            this.StatusBox.Controls.AddAt(2, literal);
        }

        

        public void hideMessage()
        {
            this.Visible = false;
        }                

        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            this.StatusBox.Style["width"] = this.width;
        }        

        public void showErrorMessage(string text)
        {
            this.Visible = true;
            this.bubbleType = UIMessageControlType.Error;            
            this.messageText = text;
            this.messageTextColor = Color.Red;
        }

        public void showInformationMessage(string text)
        {
            this.Visible = true;
            this.bubbleType = UIMessageControlType.Information;
            this.messageText = text;
            this.messageTextColor = Color.Green;        
        }

        public void showWarningMessage(string text)
        {
            this.Visible = true;
            this.bubbleType = UIMessageControlType.Warning;
            this.messageText = text;            
        }
                
        protected void Page_Load(object sender, EventArgs e)
        {

        }


    }

    public enum UIMessageControlType
    {
        Information,
        Warning,
        Error
    }
    
}