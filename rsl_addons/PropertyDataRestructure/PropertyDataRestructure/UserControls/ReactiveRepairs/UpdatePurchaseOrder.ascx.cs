﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PropertyDataRestructure.Base;
using PropertyDataRestructure.Interface;
using System.Data;
using PDR_Utilities.Constants;
using PDR_BusinessLogic.PurchaseOrder;
using PDR_DataAccess.PurchaseOrder;
using PDR_BusinessObject.PageSort;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_Utilities.Helpers;
using PDR_Utilities.Managers;
using System.Globalization;
using PDR_BusinessObject.CompletePurchaseOrder;
using System.Text;
using System.Globalization;
namespace PropertyDataRestructure.UserControls.ReactiveRepairs
{
    public partial class UpdatePurchaseOrder : UserControlBase, IAddEditPage//, IListingPage
    {
        PurchaseOrderBL objPurchaseOrderBL = new PurchaseOrderBL(new PurchaseOrderRepo());
        CompletePurchaseOrderBO objCompletePurchaseOrderBO = new CompletePurchaseOrderBO();
        PageSortBO objPageSortBo = new PageSortBO("DESC", "ADDRESS", 1, 1);

        private int totalCount;
        //private int purchaseOrderID; //= 265665;
        //private int userID;  //= 760;

        #region Page load Event
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                if (!IsPostBack)
                {
                    btnUpdatePurchaseOrder.Visible = false;
                    uiMessage.hideMessage();
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region Populate data on main page

        public void populateData(int orderID, int employeeId)
        {
            DataSet resultDataSet = new DataSet();
            objCompletePurchaseOrderBO.EmpId = employeeId;
            objCompletePurchaseOrderBO.OrderId = orderID;
            PageSortBO objPageSortBo;
            if (pageSortViewState == null)
            {
                objPageSortBo = new PageSortBO("DESC", "ADDRESS", 1, 1);
                pageSortViewState = objPageSortBo;
            }
            else
            {
                objPageSortBo = pageSortViewState;
            }
            totalCount = 0;
            resultDataSet = objPurchaseOrderBL.UpdatePurchaseOrder(objCompletePurchaseOrderBO.OrderId, objPageSortBo, ref totalCount);

            populateVulnerabalityDetails(resultDataSet);
            populateAsbestosDetails(resultDataSet);
            populatePropertyRiskData(resultDataSet);
            populateContractorDetails(resultDataSet);
            populateReportedFaultVatCost(resultDataSet, objPageSortBo);
            populatePoStatus(resultDataSet);
            objSession.setEmailDetails(ref objCompletePurchaseOrderBO);//set email data in session
            //pagination handling 
            objPageSortBo.TotalRecords = totalCount;
            objPageSortBo.TotalPages = (int)Math.Ceiling(Convert.ToDecimal(totalCount) / objPageSortBo.PageSize);

            if (objPageSortBo.TotalPages > 0)
            {
                pnlPagination.Visible = true;
            }
            else
            {
                pnlPagination.Visible = false;
                uiMessage.showErrorMessage(UserMessageConstants.RecordNotFound);
            }
            pageSortViewState = objPageSortBo;

            GridHelper.setGridViewPager(ref pnlPagination, objPageSortBo);
        }
        #endregion

        #region populate PoStatus

        private void populatePoStatus(DataSet resultDataSet)
        {
            if (resultDataSet.Tables[ApplicationConstants.PoStatusForUpdatePoPage].Rows.Count > 0)
            {
                ddlPoStatus.DataSource = resultDataSet.Tables[ApplicationConstants.PoStatusForUpdatePoPage];
                ddlPoStatus.DataTextField = "val";
                ddlPoStatus.DataValueField = "id";
                ddlPoStatus.DataBind();
            }
        }
        #endregion

        #region populate Vulnerabality Details
        private void populateVulnerabalityDetails(DataSet resultDataSet)
        {
            if (resultDataSet.Tables[ApplicationConstants.VulnerabalityDetailsForContractorsAcceptPOPage].Rows.Count > 0)
            {
                grdVulnerbility.DataSource = resultDataSet.Tables[ApplicationConstants.VulnerabalityDetailsForContractorsAcceptPOPage];
                grdVulnerbility.DataBind();
            }
            else
            {
                lblNoVulnerbility.Visible = true;
            }
        }
        #endregion

        #region populate Asbestos Details
        private void populateAsbestosDetails(DataSet resultDataSet)
        {
            if (resultDataSet.Tables[ApplicationConstants.AsbestosDetailsForContractorsAcceptPOPage].Rows.Count > 0)
            {
                grdAsbestos.DataSource = resultDataSet.Tables[ApplicationConstants.AsbestosDetailsForContractorsAcceptPOPage];
                grdAsbestos.DataBind();
            }
            else
            {
                LinkButtonAsbDetails.Visible = false;
                lblNoAsbestos.Visible = true;
            }
        }
        #endregion

        #region Populate Property Risk Data
        private void populatePropertyRiskData(DataSet resultDataSet)
        {
            if (resultDataSet.Tables[ApplicationConstants.PropertyRiskDataTableForContractorsAcceptPOPage].Rows.Count > 0)
            {
                grdRisk.DataSource = resultDataSet.Tables[ApplicationConstants.PropertyRiskDataTableForContractorsAcceptPOPage];
                grdRisk.DataBind();
            }
            else
            {
                lblNoRisk.Visible = true;
            }
        }
        #endregion

        #region populate Reported Fault Vat Cost
        private void populateReportedFaultVatCost(DataSet resultDataSet, PageSortBO objPageSortBo)
        {
            if (resultDataSet.Tables[ApplicationConstants.ReportedFaultVatCostForContractorsAcceptPOPage].Rows.Count > 0)
            {
                int num = objPageSortBo.PageNumber - 1;
                DataRow row = resultDataSet.Tables[ApplicationConstants.ReportedFaultVatCostForContractorsAcceptPOPage].Rows[num];
                objCompletePurchaseOrderBO.WorksRequired = row["WorksRequired"] != null && row["WorksRequired"].ToString() != string.Empty ? row["WorksRequired"].ToString() : "N/A";
                objCompletePurchaseOrderBO.ReportedFault = row["ReportedFault"] != null && row["ReportedFault"].ToString() != string.Empty ? row["ReportedFault"].ToString() : "N/A";
                objCompletePurchaseOrderBO.DueDate = row["DueDate"] != null && row["DueDate"].ToString() != string.Empty ? row["DueDate"].ToString() : "N/A";
                objCompletePurchaseOrderBO.NetCost = row["NetCost"] != null && row["NetCost"].ToString() != string.Empty ? row["NetCost"].ToString() : "N/A";
                objCompletePurchaseOrderBO.VAT = row["Vat"] != null && row["Vat"].ToString() != string.Empty ? row["Vat"].ToString() : "N/A";
                objCompletePurchaseOrderBO.Total = row["Total"] != null && row["Total"].ToString() != string.Empty ? row["Total"].ToString() : "N/A";
                objCompletePurchaseOrderBO.EstimatedCompletionDate = row["EstimatedCompletion"] != null && row["EstimatedCompletion"].ToString() != string.Empty ? row["EstimatedCompletion"].ToString() : "N/A";

                lblReportedFault.Text = objCompletePurchaseOrderBO.ReportedFault;
                lblWorksRequired.Text = objCompletePurchaseOrderBO.WorksRequired;
                txtBoxDueDate.Text = objCompletePurchaseOrderBO.DueDate;
                txtBoxNetCost.Text = objCompletePurchaseOrderBO.NetCost;
                txtBoxVat.Text = objCompletePurchaseOrderBO.VAT;
                txtBoxTotal.Text = objCompletePurchaseOrderBO.Total;
                txtBoxEstimatedCompletion.Text = objCompletePurchaseOrderBO.EstimatedCompletionDate;

            }
        }
        #endregion

        #region populate Contractor Details
        private void populateContractorDetails(DataSet resultDataSet)
        {
            if (resultDataSet.Tables[ApplicationConstants.ContractorDetailsForContractorsAcceptPOPage].Rows.Count > 0)
            {
                DataRow row = resultDataSet.Tables[ApplicationConstants.ContractorDetailsForContractorsAcceptPOPage].Rows[0];
                objCompletePurchaseOrderBO.ContactorName = row["NAME"] != null && row["NAME"].ToString() != string.Empty ? row["NAME"].ToString() : "N/A";
                objCompletePurchaseOrderBO.ContactName = row["ContactPerson"] != null && row["ContactPerson"].ToString() != string.Empty ? row["ContactPerson"].ToString() : "N/A";
                objCompletePurchaseOrderBO.ContactorEmailAddress = row["Email"] != null && row["Email"].ToString() != string.Empty ? row["Email"].ToString() : "N/A";

                objCompletePurchaseOrderBO.PoRef = row["PORef"] != null && row["PORef"].ToString() != string.Empty ? row["PORef"].ToString() : "N/A";
                objCompletePurchaseOrderBO.JsRef = row["JSRef"] != null && row["JSRef"].ToString() != string.Empty ? row["JSRef"].ToString() : "N/A";
                objCompletePurchaseOrderBO.OrderedBy = row["OrderedBy"] != null && row["OrderedBy"].ToString() != string.Empty ? row["OrderedBy"].ToString() : "N/A";
                objCompletePurchaseOrderBO.DirectDial = row["DirectDial"] != null && row["DirectDial"].ToString() != string.Empty ? row["DirectDial"].ToString() : "N/A";
                objCompletePurchaseOrderBO.TanentName = row["TenantName"] != null && row["TenantName"].ToString() != string.Empty ? row["TenantName"].ToString() : "N/A";
                objCompletePurchaseOrderBO.TanentAddress = row["Address"] != null && row["Address"].ToString() != string.Empty ? row["Address"].ToString() : "N/A";
                objCompletePurchaseOrderBO.TownCity = row["TOWNCITY"] != null && row["TOWNCITY"].ToString() != string.Empty ? row["TOWNCITY"].ToString() : "N/A";
                objCompletePurchaseOrderBO.PostCode = row["POSTCODE"] != null && row["POSTCODE"].ToString() != string.Empty ? row["POSTCODE"].ToString() : "N/A";
                objCompletePurchaseOrderBO.Country = row["COUNTRY"] != null && row["COUNTRY"].ToString() != string.Empty ? row["COUNTRY"].ToString() : "N/A";

                objCompletePurchaseOrderBO.Telephone = row["Telephone"] != null && row["Telephone"].ToString() != string.Empty ? row["Telephone"].ToString() : "N/A";



                lblFaultLogID.Text = row["FaultLogId"].ToString();
                lblContractor.Text = objCompletePurchaseOrderBO.ContactorName;
                lblContactName.Text = objCompletePurchaseOrderBO.ContactName;
                lblPoRef.Text = objCompletePurchaseOrderBO.PoRef;
                lblJsRef.Text = objCompletePurchaseOrderBO.JsRef;
                lblOrderedBy.Text = objCompletePurchaseOrderBO.OrderedBy;
                lblDirectDial.Text = objCompletePurchaseOrderBO.DirectDial;
                lblTenant.Text = objCompletePurchaseOrderBO.TanentName;
                lblAddress.Text = objCompletePurchaseOrderBO.TanentAddress;
                lblDirectDialScheduler.Text = objCompletePurchaseOrderBO.Telephone;
            }
        }
        #endregion


        #region functions from IAddEditPage interface

        public void loadData()
        {
            throw new NotImplementedException();
        }
        public void printData()
        {
            throw new NotImplementedException();
        }

        public void exportGridToExcel()
        {
            throw new NotImplementedException();
        }

        public void exportToPdf()
        {
            throw new NotImplementedException();
        }

        public void downloadData()
        {
            throw new NotImplementedException();
        }

        public void applyFilters()
        {
            throw new NotImplementedException();
        }

        public void populateDropDown(DropDownList ddl)
        {
            throw new NotImplementedException();
        }

        public void populateDropDowns()
        {
            throw new NotImplementedException();
        }

        public void saveData()
        {
            throw new NotImplementedException();
        }

        public bool validateData()
        {
            throw new NotImplementedException();
        }

        public void resetControls()
        {
            throw new NotImplementedException();
        }

        #endregion

        #region update button action

        protected void btnUpdatePurchaseOrder_Click(object sender, EventArgs e)
        {
            updateFault();
            btnUpdatePurchaseOrder.Visible = false;
            ddlPoStatus.Enabled = false;
            // reBindGridForLastPage();
            //  Response.Redirect(this);
        }
        #endregion

        #region Change Page Number for pagination

        protected void lnkbtnPager_Click(object sender, EventArgs e)
        {
            try
            {
                PageSortBO objPageSortBo = new PageSortBO("DESC", "ADDRESS", 1, 1);
                GridHelper.processGridPager(sender, objPageSortBo, pageSortViewState);
                populateData(objSession.getEmailDetails().OrderId, objSession.getEmailDetails().EmpId);

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        #endregion

        #region drop down list action On both Popups
        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            btnUpdatePurchaseOrder.Visible = true;
            if (ddlPoStatus.SelectedItem.Text == "Complete")
            {
                popupCompletedWork.Show();
                populateHoursAndMinsDayNightForWorkCompleted();
            }
            else if (ddlPoStatus.SelectedItem.Text == "No Entry")
            {
                popupNoEntry.Show();
                populateHoursAndMinsDayNightForNoEntry();
            }
            else if (ddlPoStatus.SelectedItem.Text == "Cancelled")
            {
                popupPrompt.Show();
            }
        }
        #endregion

        #region  populate drop down values of No entry Popup
        public void populateHoursAndMinsDayNightForNoEntry()
        {
            ddlNoEntryHours.Items.Clear();
            ddlNoEntryMin.Items.Clear();
            ddlNoEntryFormate.Items.Clear();
            for (int i = 1; i <= 12; i++)
            {
                ddlNoEntryHours.Items.Add(i.ToString("D2"));
            }
            for (int i = 1; i <= 59; i++)
            {
                ddlNoEntryMin.Items.Add(i.ToString("D2"));
            }
            ddlNoEntryFormate.Items.Add("AM");
            ddlNoEntryFormate.Items.Add("PM");
        }

        #endregion

        #region populate drop down values of Work completed Popup
        public void populateHoursAndMinsDayNightForWorkCompleted()
        {
            ddlHours.Items.Clear();
            ddlMin.Items.Clear();
            ddlFormate.Items.Clear();
            for (int i = 1; i <= 12; i++)
            {
                ddlHours.Items.Add(i.ToString("D2"));
            }
            for (int i = 1; i <= 59; i++)
            {
                ddlMin.Items.Add(i.ToString("D2"));
            }
            ddlFormate.Items.Add("AM");
            ddlFormate.Items.Add("PM");
        }
        #endregion

        #region btnAdd Add repair click event
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                HiddenField hdnSelectedRepairId = (HiddenField)this.Parent.FindControl("hdnSelectedRepairId");
                string faultRepairListID = hdnSelectedRepairId.Value;
                DataTable dtRepair = new DataTable();
                dtRepair = SessionManager.getRepairsDataTable();
                if (txtSearch.Text != string.Empty & faultRepairListID != string.Empty)
                {
                    if (dtRepair.Columns.Count == 0)
                    {
                        dtRepair.Columns.Add(new DataColumn(ApplicationConstants.FaultRepairListID));
                        dtRepair.Columns.Add(new DataColumn(ApplicationConstants.FaultRepairDescription));
                    }

                    var repairResult = (from ps in dtRepair.AsEnumerable() where ps[ApplicationConstants.FaultRepairListID].ToString() == faultRepairListID select ps);
                    if (repairResult.Count() > 0)
                    {
                        uiMessageOnPopUp.isError = true;
                        uiMessageOnPopUp.messageText = UserMessageConstants.RepairAlreadyAdded; ;
                        uiMessageOnPopUp.showErrorMessage(uiMessageOnPopUp.messageText);
                    }
                    else
                    {
                        uiMessageOnPopUp.isError = false;
                        uiMessageOnPopUp.hideMessage();
                        //Create a new row
                        dynamic dr = dtRepair.NewRow();
                        dr[ApplicationConstants.FaultRepairListID] = faultRepairListID;
                        dr[ApplicationConstants.FaultRepairDescription] = txtSearch.Text;
                        dtRepair.Rows.Add(dr);
                        SessionManager.setRepairsDataTable(ref dtRepair);
                    }
                    repairsGridContainer.Visible = true;
                }
                grdRepairs.DataSource = dtRepair;
                grdRepairs.DataBind();
                txtSearch.Text = string.Empty;

            }
            catch (Exception ex)
            {
                uiMessageOnPopUp.isError = true;
                uiMessageOnPopUp.messageText = ex.Message;
            }
            finally
            {
                popupCompletedWork.Show();
            }
        }


        #endregion

        #region grdRepairs Row deleting event

        protected void grdRepairs_RowDeleting(object sender, System.Web.UI.WebControls.GridViewDeleteEventArgs e)
        {
            try
            {
                DataTable dtRepairs = new DataTable();
                dtRepairs = SessionManager.getRepairsDataTable();

                if (dtRepairs != null && dtRepairs.Rows.Count > 0)
                {
                    dtRepairs.Rows.RemoveAt(e.RowIndex);
                    dtRepairs.AcceptChanges();
                    SessionManager.setRepairsDataTable(ref dtRepairs);
                    grdRepairs.DataSource = dtRepairs;
                    grdRepairs.DataBind();
                }

            }
            catch (Exception ex)
            {
                uiMessageOnPopUp.isError = true;
                uiMessageOnPopUp.messageText = ex.Message;
            }
            finally
            {
                popupCompletedWork.Show();
            }
        }
        #endregion

        #region save button action on work completed popup

        protected void btnSave_WorkComleted_Click(object sender, EventArgs e)
        {
            DataTable dtRepair = SessionManager.getRepairsDataTable();
            if (dtRepair.Rows.Count == 0)
            {
                uiMessageOnPopUp.isError = true;
                uiMessageOnPopUp.showErrorMessage(UserMessageConstants.AddAtLeastRepair);
                popupCompletedWork.Show();
            }
            else if ((ddlFollowOnWorks.SelectedItem.Text == "Yes") && (txtBoxFollowOnNotes.Text == "" || txtBoxFollowOnNotes.Text == null))
            {
                uiMessageOnPopUp.isError = true;
                uiMessageOnPopUp.showErrorMessage(UserMessageConstants.EnterFollowOnNotes);
                popupCompletedWork.Show();
            }
            else if ((txtCompleted.Text == "DD/MM/YYY" || txtCompleted.Text == string.Empty))
            {
                uiMessageOnPopUp.isError = true;
                uiMessageOnPopUp.showErrorMessage(UserMessageConstants.InvalidCompletedDate);
                popupCompletedWork.Show();
            }
            else
            {
                uiMessageOnPopUp.isError = false;
                uiMessageOnPopUp.hideMessage();
                SaveCompleteFaultData();
            }

        }

        #endregion

        #region save button action on No entry popup
        protected void btnSave_NoEntry_Click(object sender, EventArgs e)
        {
            if ((txtNoEntry.Text == "DD/MM/YYY" || txtNoEntry.Text == string.Empty))
            {
                uiMessageOnNoEntryPopup.isError = true;
                uiMessageOnNoEntryPopup.showErrorMessage(UserMessageConstants.InvalidNoEntryDate);
                popupNoEntry.Show();
            }
            else
            {
                uiMessageOnNoEntryPopup.isError = false;
                uiMessageOnNoEntryPopup.hideMessage();
            }
        }

        #endregion

        #region updating fault status
        public void updateFault()
        {
            try
            {
                if (ddlPoStatus.SelectedItem.Text == "No Entry")
                {
                    this.SaveNoEntryData();
                }
                else if (ddlPoStatus.SelectedItem.Text == "Cancelled")
                {
                    this.SaveCancelledData();
                }

            }
            catch (Exception ex)
            {

            }
            finally
            {

            }


        }




        #endregion


        #region  Save Complete Fault Data

        public void SaveCompleteFaultData()
        {
            DateTime selectTime = new DateTime();
            DateTime currTime = new DateTime();
            try
            {
                CompletePurchaseOrderBO objCompletePurchaseOrderBO = new CompletePurchaseOrderBO();
                objCompletePurchaseOrderBO.FaultLogId = Convert.ToInt32(lblFaultLogID.Text);
                objCompletePurchaseOrderBO.InspectionDate = Convert.ToDateTime(txtCompleted.Text);
                objCompletePurchaseOrderBO.Time = ddlHours.SelectedValue + ":" + ddlMin.SelectedValue + " " + ddlFormate.SelectedValue;
                selectTime = DateTime.Parse(objCompletePurchaseOrderBO.Time);
                objCompletePurchaseOrderBO.UserId = objSession.getEmailDetails().EmpId;
                if (ddlFollowOnWorks.SelectedItem.Text == "Yes")
                {
                    objCompletePurchaseOrderBO.followOnStatus = true;
                }
                else
                {
                    objCompletePurchaseOrderBO.followOnStatus = false;
                }
                objCompletePurchaseOrderBO.FollowOnNotes = txtBoxFollowOnNotes.Text;

                objCompletePurchaseOrderBO.RepairNotes = txtBoxRepairNotes.Text;
                int selectTimeinInt = 0;
                currTime = DateTime.Parse(DateTime.Now.ToString());
                selectTimeinInt = selectTime.CompareTo(currTime);

                if (objCompletePurchaseOrderBO.InspectionDate.Date == DateTime.Now.Date)
                {
                    if (selectTimeinInt > 0)
                    {
                        uiMessage.isError = true;
                        uiMessage.messageText = UserMessageConstants.TimenotgreaterthancurrTime;
                        return;
                    }
                }
                StringBuilder sb = new StringBuilder();
                DataTable dtRepair = SessionManager.getRepairsDataTable();
                foreach (DataRow row in dtRepair.Rows)
                {
                    sb.Append(row[ApplicationConstants.FaultRepairListID]);
                    sb.Append(",");
                }
                objCompletePurchaseOrderBO.FaultRepairIdList = sb.ToString().Substring(0, sb.ToString().Length - 1);
                SessionManager objSessionManager = new SessionManager();
                objPurchaseOrderBL.SaveWorkCompletionData(ref objCompletePurchaseOrderBO);
                uiMessage.isError = false;

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;
                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessage.isError == true)
                {
                    uiMessage.messageText = UserMessageConstants.ErrorOccueInSavingData;
                    uiMessage.showErrorMessage(UserMessageConstants.ErrorOccueInSavingData);
                }
                else
                {
                    uiMessage.messageText = UserMessageConstants.dataSavedSuccessfully;
                    uiMessage.showInformationMessage(UserMessageConstants.dataSavedSuccessfully);
                }

            }
        }
        #endregion

        #region save no entry data
        public void SaveNoEntryData()
        {
            try
            {
                int faultLogId = int.Parse(lblFaultLogID.Text);
                //DateTime noEntryDateTime = DateTime.ParseExact(txtNoEntry.Text + " " + ddlNoEntryHours.SelectedValue + ":" + ddlNoEntryMin.SelectedValue + " " + ddlNoEntryFormate.SelectedValue, "dd/mm/yyyy", new CultureInfo("en-GB"));

                DateTime noEntryDateTime = objCompletePurchaseOrderBO.InspectionDate = Convert.ToDateTime(txtNoEntry.Text);

                objPurchaseOrderBL.SaveNoEntryData(faultLogId, noEntryDateTime);
                uiMessage.isError = false;
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessage.isError == true)
                {
                    uiMessage.messageText = UserMessageConstants.ErrorOccueInSavingData;
                    uiMessage.showErrorMessage(UserMessageConstants.ErrorOccueInSavingData);
                }
                else
                {
                    uiMessage.messageText = UserMessageConstants.dataSavedSuccessfully;
                    uiMessage.showInformationMessage(UserMessageConstants.dataSavedSuccessfully);
                }
            }
        }
        #endregion

        #region save cancelled data
        public void SaveCancelledData()
        {
            try
            {
                int faultLogId = int.Parse(lblFaultLogID.Text);
                objPurchaseOrderBL.SaveCancelledData(faultLogId, objSession.getEmailDetails().OrderId);
                uiMessage.isError = false;
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessage.isError == true)
                {
                    uiMessage.messageText = UserMessageConstants.ErrorOccueInSavingData;
                    uiMessage.showErrorMessage(UserMessageConstants.ErrorOccueInSavingData);
                }
                else
                {
                    uiMessage.messageText = UserMessageConstants.dataSavedSuccessfully;
                    uiMessage.showInformationMessage(UserMessageConstants.dataSavedSuccessfully);
                }
            }
        }

        #region prompt popup actions
        protected void PromptPopup_ButtonYes_Click(object sender, EventArgs e)
        {
            SaveCancelledData();
        }
        #endregion
        #endregion




        #region Asbestos details popup
        protected void LinkButtonAsbDetails_Click(object sender, EventArgs e)
        {
            popupAsbDetails.Show();
        }

        #endregion


























    }
}