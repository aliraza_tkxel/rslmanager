﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="PurchaseOrder.ascx.cs"
    Inherits="PropertyDataRestructure.UserControls.ReactiveRepairs.PurchaseOrder" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<link href="../../Styles/Site.css" rel="stylesheet" media="screen" />
<asp:UpdatePanel runat="server" ID="updPanelAcceptPurchaseOrder">
    <ContentTemplate>
        <asp:Panel runat="server" Style='border: 1px Solid Black; width: 800px;'>
            <div class="AcceptPurchaseOrder">
                &nbsp Accept Purchase Order:
            </div>
            <div>
                <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" />
            </div>
            <table class="ContractorTable">
                <tbody>
                    <tr>
                        <td>
                            Contractor:
                        </td>
                        <td>
                            <asp:Label ID="lblContractor" runat="server"></asp:Label>
                        </td>
                        <td align="right" style="padding-right:50px;">
                            <asp:Button Text="Accept Work" runat="server" ID="btnAcceptPurchaseOrder" Height="25px"
                                OnClick="btnAcceptPurchaseOrder_Click" CssClass="pdr-Button" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Contact Name:
                        </td>
                        <td>
                            <asp:Label ID="lblContactName" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            PO Ref:
                        </td>
                        <td>
                            <asp:Label ID="lblPoRef" runat="server"></asp:Label>
                        </td>
                        <td align="right" style="padding-right:50px;">
                            <asp:Button Visible="false" Text="Reject Work" runat="server" Height="25px" ID="btnRejectPurchaseOrder"
                                OnClick="btnRejectPurchaseOrder_Click" CssClass="pdr-Button" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            JS Ref
                        </td>
                        <td>
                            <asp:Label ID="lblJsRef" runat="server"></asp:Label>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Status:
                        </td>
                        <td>
                            <asp:Label ID="lblPoStatus" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Ordered By:
                        </td>
                        <td>
                            <asp:Label ID="lblOrderedBy" runat="server" Text=""> </asp:Label>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Direct Dial:
                        </td>
                        <td>
                            <asp:Label ID="lblDirectDial" runat="server" Text=""></asp:Label>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                </tbody>
            </table>
            <asp:Panel runat="server" Style='border: 1px Green; margin-top: 10px;'>
                <table class="TanentAdressTable">
                    <tbody>
                        <tr>
                            <td>
                                Address:
                            </td>
                            <td rowspan="2">
                                <asp:Label ID="lblAddress" runat="server" Text=""></asp:Label>
                            </td>
                            <td style="width: 10px; height: 10px">
                                Tenant:
                            </td>
                            <td>
                                <asp:Label ID="lblTenant" runat="server" Text=""></asp:Label>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                Telephone:
                            </td>
                            <td>
                                <asp:Label ID="lblDirectDialScheduler" runat="server" Text=""></asp:Label>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                Risk:
                            </td>
                            <td>
                                <div style='overflow: auto;'>
                                    <asp:GridView ID="grdRisk" runat="server" AutoGenerateColumns="False" ShowHeader="false"
                                        CssClass="asbestosGrid">
                                        <Columns>
                                            <%-- <asp:BoundField DataField="CATDESC" />--%>
                                            <asp:BoundField DataField="SUBCATDESC" />
                                        </Columns>
                                    </asp:GridView>
                                    <asp:Label ID="lblNoRisk" runat="server" Text="N/A" Visible="false"></asp:Label>
                                </div>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                Vulnerability:
                            </td>
                            <td>
                                <div style='overflow: auto;'>
                                    <asp:GridView ID="grdVulnerbility" runat="server" AutoGenerateColumns="False" ShowHeader="false"
                                        CssClass="asbestosGrid" GridLines="None">
                                        <Columns>
                                            <%-- <asp:BoundField DataField="CATDESC" />--%>
                                            <asp:BoundField DataField="SUBCATDESC" />
                                        </Columns>
                                    </asp:GridView>
                                    <asp:Label ID="lblNoVulnerbility" runat="server" Text="N/A" Visible="false"></asp:Label>
                                </div>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                Asbestos:
                            </td>
                            <td>
                                <asp:LinkButton ID="LinkButtonAsbDetails" runat="server" OnClick="LinkButtonAsbDetails_Click"><img src="../../Images/warning.png" />Click here for more details</asp:LinkButton>
                                <asp:Label ID="lblNoAsbestos" runat="server" Text="N/A" Visible="false"></asp:Label>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <div class="RequiredFieldDiv">
                <div style="float: left; padding: 5px; width: 100%;">
                    Reported Fault:
                    <asp:Label ID="lblReportedFault" runat="server" Font-Bold="true"></asp:Label>
                </div>
                <div style="float: left; padding: 5px; width: 100%;">
                    <div style="float: left; padding-bottom: 5px; width: 90%; height:auto;">Works Required:</div>
                    <br />
                    <div class="WorkDetailsDiv">
                        <asp:Panel runat="server" ID="pnlPagination" Visible="true" class="paginationDecoration">
                            <table>
                                <tbody>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkbtnPagerFirst" Text="" runat="server" CommandName="Page" Visible="false"
                                                CommandArgument="First" OnClick="lnkbtnPager_Click">&lt;&lt;First</asp:LinkButton>
                                            &nbsp;
                                        </td>
                                        <td>
                                            Page:&nbsp;
                                            <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="currentPage" />
                                            &nbsp;of&nbsp;
                                            <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />
                                            <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" Visible="false" />
                                            <%-- &nbsp;to&nbsp;--%>
                                            <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" Visible="false" />
                                            <%-- &nbsp;of&nbsp;--%>
                                            <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" Visible="false" />
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lnkbtnPagerPrev" Text="" runat="server" CommandName="Page" CommandArgument="Prev"
                                                OnClick="lnkbtnPager_Click">&lt;Prev</asp:LinkButton>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page"
                                                CommandArgument="Next" OnClick="lnkbtnPager_Click" />
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" Visible="false"
                                                CommandName="Page" CommandArgument="Last" OnClick="lnkbtnPager_Click" />
                                            &nbsp;
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </asp:Panel>
                        <div style="width: 100%; overflow: auto; height:100px; padding:5px;">
                            <asp:Label ID="lblWorksRequired" runat="server" Font-Bold="true"></asp:Label>
                        </div>
                    </div>
                    <table class="WorkEstimateTable">
                        <tbody>
                            <tr>
                                <td>
                                    Due Date:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtBoxDueDate" runat="server" ReadOnly="true"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Net Cost(£)
                                </td>
                                <td>
                                    <asp:TextBox ID="txtBoxNetCost" runat="server" ReadOnly="true"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Vat(£):
                                </td>
                                <td>
                                    <asp:TextBox ID="txtBoxVat" runat="server" ReadOnly="true"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Total(£):
                                </td>
                                <td>
                                    <asp:TextBox ID="txtBoxTotal" runat="server" ReadOnly="true"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Estimated Completion:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtBoxEstimatedCompletion" runat="server" ReadOnly="true"></asp:TextBox>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </asp:Panel>
        <%-- Asbestos Details Popup--%>
        <asp:Panel ID="pnlAsbestosPopup" runat="server" Visible="true" CssClass="modalPopup"
            align="center" Style="width: 30%;">
            <div>
                <table>
                    <tr>
                        <td>
                           <b> Asbestos Datails</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div style='overflow: auto;'>
                                <asp:GridView ID="grdAsbestos" runat="server" CssClass="asbestosGrid" >
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                    <tr>
                       
                        <td>
                            <asp:Button runat="server" ID="AsbestosPopupCloseButton" Text="Close" />
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
        <%-- Model Popup--%>
        <cc1:ModalPopupExtender ID="popupAsbDetails" runat="server" PopupControlID="pnlAsbestosPopup"
            TargetControlID="LabelForAsbestosPopup" BackgroundCssClass="modalBackground">
        </cc1:ModalPopupExtender>
        <asp:Label ID="LabelForAsbestosPopup" runat="server"></asp:Label>
    </ContentTemplate>
</asp:UpdatePanel>
