﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PropertyDataRestructure.Base;
using PropertyDataRestructure.Interface;
using System.Data;
using PDR_Utilities.Constants;
using PDR_BusinessLogic.PurchaseOrder;
using PDR_DataAccess.PurchaseOrder;
using PDR_BusinessObject.PageSort;
using PDR_BusinessObject.CompletePurchaseOrder;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_Utilities.Helpers;
using System.Text;
using System.IO;
using System.Net.Mail;
using System.Net.Mime;
using PDR_BusinessObject.AssignToContractor;
using PDR_Utilities.Managers;
using System.Globalization;
namespace PropertyDataRestructure.UserControls.ReactiveRepairs
{
    public partial class PurchaseOrder : UserControlBase, IAddEditPage//, IListingPage
    {
        PurchaseOrderBL objPurchaseOrderBL = new PurchaseOrderBL(new PurchaseOrderRepo());
        CompletePurchaseOrderBO objCompletePurchaseOrderBO = new CompletePurchaseOrderBO();
        PageSortBO objPageSortBo = new PageSortBO("DESC", "ADDRESS", 1, 1);

        private int totalCount;
        private bool isAccepted = true;


        #region Page load event
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    //uiMessage.hideMessage();

                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region PO details population
        public void populateData(int orderID, int employeeId)
        {
            PageSortBO objPageSortBo;
            DataSet resultDataSet = new DataSet();


            objCompletePurchaseOrderBO.EmpId = employeeId;
            objCompletePurchaseOrderBO.OrderId = orderID;
            if (pageSortViewState == null)
            {
                objPageSortBo = new PageSortBO("DESC", "ADDRESS", 1, 1);
                pageSortViewState = objPageSortBo;
            }
            else
            {
                objPageSortBo = pageSortViewState;
            }
            totalCount = 0;
            resultDataSet = objPurchaseOrderBL.GetPurchaseOrder(objCompletePurchaseOrderBO.OrderId, objPageSortBo, ref totalCount);
            populatePropertyRiskData(resultDataSet);
            populateVulnerabalityDetails(resultDataSet);
            populateAsbestosDetails(resultDataSet);
            populateContractorDetails(resultDataSet);
            populateReportedFaultVatCost(objPageSortBo, resultDataSet);
            objSession.setEmailDetails(ref objCompletePurchaseOrderBO);//set email data in session
            objSession.GetPurchaseOrderDetailDs = resultDataSet;
            //pagination handling 
            objPageSortBo.TotalRecords = totalCount;
            objPageSortBo.TotalPages = (int)Math.Ceiling(Convert.ToDecimal(totalCount) / objPageSortBo.PageSize);

            if (objPageSortBo.TotalPages > 0)
            {
                pnlPagination.Visible = true;
            }
            else
            {
                pnlPagination.Visible = false;
                uiMessage.showErrorMessage(UserMessageConstants.RecordNotFound);
            }
            pageSortViewState = objPageSortBo;

            GridHelper.setGridViewPager(ref pnlPagination, objPageSortBo);



        }
        #region populate Reported Fault Vat Cost
        private void populateReportedFaultVatCost(PageSortBO objPageSortBo, DataSet resultDataSet)
        {
            if (resultDataSet.Tables[ApplicationConstants.ReportedFaultVatCostForContractorsAcceptPOPage].Rows.Count > 0)
            {
                int num = objPageSortBo.PageNumber - 1;
                DataRow row = resultDataSet.Tables[ApplicationConstants.ReportedFaultVatCostForContractorsAcceptPOPage].Rows[num];
                objCompletePurchaseOrderBO.WorksRequired = row["WorksRequired"] != null && row["WorksRequired"].ToString() != string.Empty ? row["WorksRequired"].ToString() : "N/A";
                objCompletePurchaseOrderBO.ReportedFault = row["ReportedFault"] != null && row["ReportedFault"].ToString() != string.Empty ? row["ReportedFault"].ToString() : "N/A";
                objCompletePurchaseOrderBO.DueDate = row["DueDate"] != null && row["DueDate"].ToString() != string.Empty ? row["DueDate"].ToString() : "N/A";
                objCompletePurchaseOrderBO.NetCost = row["NetCost"] != null && row["NetCost"].ToString() != string.Empty ? row["NetCost"].ToString() : "N/A";
                objCompletePurchaseOrderBO.VAT = row["Vat"] != null && row["Vat"].ToString() != string.Empty ? row["Vat"].ToString() : "N/A";
                objCompletePurchaseOrderBO.Total = row["Total"] != null && row["Total"].ToString() != string.Empty ? row["Total"].ToString() : "N/A";
                objCompletePurchaseOrderBO.EstimatedCompletionDate = row["EstimatedCompletion"] != null && row["EstimatedCompletion"].ToString() != string.Empty ? row["EstimatedCompletion"].ToString() : "N/A";


                objCompletePurchaseOrderBO.TotalNetCost = row["TotalNetCost"] != null && row["TotalNetCost"].ToString() != string.Empty ? row["TotalNetCost"].ToString() : "N/A";
                objCompletePurchaseOrderBO.TotalVAT = row["TotalVat"] != null && row["TotalVat"].ToString() != string.Empty ? row["TotalVat"].ToString() : "N/A";
                objCompletePurchaseOrderBO.TotalGROSS = row["TotalGross"] != null && row["TotalGross"].ToString() != string.Empty ? row["TotalGross"].ToString() : "N/A";




                lblReportedFault.Text = objCompletePurchaseOrderBO.ReportedFault;
                lblWorksRequired.Text = objCompletePurchaseOrderBO.WorksRequired;
                txtBoxDueDate.Text = objCompletePurchaseOrderBO.DueDate;
                txtBoxNetCost.Text = objCompletePurchaseOrderBO.NetCost;
                txtBoxVat.Text = objCompletePurchaseOrderBO.VAT;
                txtBoxTotal.Text = objCompletePurchaseOrderBO.Total;
                txtBoxEstimatedCompletion.Text = objCompletePurchaseOrderBO.EstimatedCompletionDate;
            }
        }
        #endregion

        #region populate Contractor Details
        private void populateContractorDetails(DataSet resultDataSet)
        {
            if (resultDataSet.Tables[ApplicationConstants.ContractorDetailsForContractorsAcceptPOPage].Rows.Count > 0)
            {

                DataRow row = resultDataSet.Tables[ApplicationConstants.ContractorDetailsForContractorsAcceptPOPage].Rows[0];
                objCompletePurchaseOrderBO.ContactorName = row["NAME"] != null && row["NAME"].ToString() != string.Empty ? row["NAME"].ToString() : "N/A";
                objCompletePurchaseOrderBO.ContactName = row["ContactPerson"] != null && row["ContactPerson"].ToString() != string.Empty ? row["ContactPerson"].ToString() : "N/A";
                objCompletePurchaseOrderBO.ContactorEmailAddress = row["Email"] != null && row["Email"].ToString() != string.Empty ? row["Email"].ToString() : "N/A";
                objCompletePurchaseOrderBO.SupplierEmailAddress = objPurchaseOrderBL.GetSchedulerEmailId(objCompletePurchaseOrderBO.OrderId);


                objCompletePurchaseOrderBO.PoRef = row["PORef"] != null && row["PORef"].ToString() != string.Empty ? row["PORef"].ToString() : "N/A";
                objCompletePurchaseOrderBO.JsRef = row["JSRef"] != null && row["JSRef"].ToString() != string.Empty ? row["JSRef"].ToString() : "N/A";
                objCompletePurchaseOrderBO.PoStatus = row["PoStatus"] != null && row["PoStatus"].ToString() != string.Empty ? row["PoStatus"].ToString() : "N/A";
                objCompletePurchaseOrderBO.OrderedBy = row["OrderedBy"] != null && row["OrderedBy"].ToString() != string.Empty ? row["OrderedBy"].ToString() : "N/A";
                objCompletePurchaseOrderBO.DirectDial = row["DirectDial"] != null && row["DirectDial"].ToString() != string.Empty ? row["DirectDial"].ToString() : "N/A";
                objCompletePurchaseOrderBO.TanentName = row["TenantName"] != null && row["TenantName"].ToString() != string.Empty ? row["TenantName"].ToString() : "N/A";
                objCompletePurchaseOrderBO.TanentAddress = row["Address"] != null && row["Address"].ToString() != string.Empty ? row["Address"].ToString() : "N/A";
                objCompletePurchaseOrderBO.TownCity = row["TOWNCITY"] != null && row["TOWNCITY"].ToString() != string.Empty ? row["TOWNCITY"].ToString() : "N/A";
                objCompletePurchaseOrderBO.PostCode = row["POSTCODE"] != null && row["POSTCODE"].ToString() != string.Empty ? row["POSTCODE"].ToString() : "N/A";
                objCompletePurchaseOrderBO.Country = row["COUNTRY"] != null && row["COUNTRY"].ToString() != string.Empty ? row["COUNTRY"].ToString() : "N/A";


                objCompletePurchaseOrderBO.Telephone = row["Telephone"] != null && row["Telephone"].ToString() != string.Empty ? row["Telephone"].ToString() : "N/A";

                lblContractor.Text = objCompletePurchaseOrderBO.ContactorName;
                lblContactName.Text = objCompletePurchaseOrderBO.ContactName;
                lblPoRef.Text = objCompletePurchaseOrderBO.PoRef;
                lblJsRef.Text = objCompletePurchaseOrderBO.JsRef;
                lblPoStatus.Text = objCompletePurchaseOrderBO.PoStatus;
                lblOrderedBy.Text = objCompletePurchaseOrderBO.OrderedBy;
                lblDirectDial.Text = objCompletePurchaseOrderBO.DirectDial;
                lblTenant.Text = objCompletePurchaseOrderBO.TanentName;
                lblAddress.Text = objCompletePurchaseOrderBO.TanentAddress;
                lblDirectDialScheduler.Text = objCompletePurchaseOrderBO.Telephone;

            }
        }

        #endregion

        #region populate Asbestos Details
        private void populateAsbestosDetails(DataSet resultDataSet)
        {
            if (resultDataSet.Tables[ApplicationConstants.AsbestosDetailsForContractorsAcceptPOPage].Rows.Count > 0)
            {

                grdAsbestos.DataSource = resultDataSet.Tables[ApplicationConstants.AsbestosDetailsForContractorsAcceptPOPage];
                grdAsbestos.DataBind();
                objCompletePurchaseOrderBO.AsbestosDetails = getAsbestosDetails(resultDataSet);
            }
            else
            {
                LinkButtonAsbDetails.Visible = false;
                lblNoAsbestos.Visible = true;
            }
        }
        #endregion


        #region populate Vulnerabality Details
        private void populateVulnerabalityDetails(DataSet resultDataSet)
        {
            if (resultDataSet.Tables[ApplicationConstants.VulnerabalityDetailsForContractorsAcceptPOPage].Rows.Count > 0)
            {
                grdVulnerbility.DataSource = resultDataSet.Tables[ApplicationConstants.VulnerabalityDetailsForContractorsAcceptPOPage];
                grdVulnerbility.DataBind();
            }
            else
            {
                lblNoVulnerbility.Visible = true;
            }
        }
        #endregion

        #region populate Property Risk Data
        private void populatePropertyRiskData(DataSet resultDataSet)
        {
            if (resultDataSet.Tables[ApplicationConstants.PropertyRiskDataTableForContractorsAcceptPOPage].Rows.Count > 0)
            {
                grdRisk.DataSource = resultDataSet.Tables[ApplicationConstants.PropertyRiskDataTableForContractorsAcceptPOPage];
                grdRisk.DataBind();
            }
            else
            {
                lblNoRisk.Visible = true;
            }
        }
        #endregion

        #endregion

        #region functions from IAddEditPage
        public void loadData()
        {
            throw new NotImplementedException();
        }

        public void printData()
        {
            throw new NotImplementedException();
        }

        public void exportGridToExcel()
        {
            throw new NotImplementedException();
        }

        public void exportToPdf()
        {
            throw new NotImplementedException();
        }

        public void downloadData()
        {
            throw new NotImplementedException();
        }

        public void applyFilters()
        {
            throw new NotImplementedException();
        }

        public void populateDropDown(DropDownList ddl)
        {
            throw new NotImplementedException();
        }

        public void populateDropDowns()
        {
            throw new NotImplementedException();
        }

        public void saveData()
        {
            throw new NotImplementedException();
        }

        public bool validateData()
        {
            throw new NotImplementedException();
        }
        public void resetControls()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region Change Page Number

        protected void lnkbtnPager_Click(object sender, EventArgs e)
        {
            try
            {
                //PageSortBO objPageSortBo = new PageSortBO("DESC", "ADDRESS", 1, 1);
                GridHelper.processGridPager(sender, objPageSortBo, pageSortViewState);
                populateReportedFaultVatCost(pageSortViewState, objSession.GetPurchaseOrderDetailDs);
                //populateData(objSession.getEmailDetails().OrderId, objSession.getEmailDetails().EmpId);
                GridHelper.setGridViewPager(ref pnlPagination, pageSortViewState);


            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        #endregion

        #region Button Actions
        protected void btnAcceptPurchaseOrder_Click(object sender, EventArgs e)
        {
            try
            {
                btnAcceptPurchaseOrder.Enabled = false;
                isAccepted = true;
                btnRejectPurchaseOrder.Visible = false;
                uiMessage.showInformationMessage(UserMessageConstants.AcceptWork);
                objPurchaseOrderBL.ChangeWorkStatus(objSession.getEmailDetails().OrderId, isAccepted);
                sendEmailtoContractor();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        protected void btnRejectPurchaseOrder_Click(object sender, EventArgs e)
        {

            try
            {
                btnRejectPurchaseOrder.Enabled = false;
                isAccepted = false;
                btnAcceptPurchaseOrder.Visible = false;
                uiMessage.showInformationMessage(UserMessageConstants.RejectWork);
                objPurchaseOrderBL.ChangeWorkStatus(objSession.getEmailDetails().OrderId, isAccepted);
                sendEmailtoContractor();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #region Asbestos details Popup
        protected void LinkButtonAsbDetails_Click(object sender, EventArgs e)
        {
            popupAsbDetails.Show();
        }
        #endregion

        #region get risk details
        public string getRiskDetails(DataSet detailsForEmailDS)
        {
            StringBuilder RiskDetails = new StringBuilder("N/A");


            if ((detailsForEmailDS != null) && (detailsForEmailDS.Tables[ApplicationConstants.PropertyRiskDataTableForContractorsAcceptPOPage] != null) && detailsForEmailDS.Tables[ApplicationConstants.PropertyRiskDataTableForContractorsAcceptPOPage].Rows.Count > 0)
            {
                RiskDetails.Clear();

                foreach (DataRow row in detailsForEmailDS.Tables[ApplicationConstants.PropertyRiskDataTableForContractorsAcceptPOPage].Rows)
                {
                    RiskDetails.Append(row["CATDESC"] + (string.IsNullOrEmpty(Convert.ToString(row["SUBCATDESC"])) ? "" : ": " + row["SUBCATDESC"]) + "<br />");
                }
            }

            return RiskDetails.ToString();
        }
        #endregion

        #region get Asbestos details
        public string getAsbestosDetails(DataSet detailsForEmailDS)
        {
            StringBuilder AsbDetails = new StringBuilder("N/A");
            if ((detailsForEmailDS != null) && (detailsForEmailDS.Tables[ApplicationConstants.AsbestosDetailsForContractorsAcceptPOPage] != null) && detailsForEmailDS.Tables[ApplicationConstants.AsbestosDetailsForContractorsAcceptPOPage].Rows.Count > 0)
            {
                AsbDetails.Clear();

                foreach (DataRow row in detailsForEmailDS.Tables[ApplicationConstants.AsbestosDetailsForContractorsAcceptPOPage].Rows)
                {
                    AsbDetails.Append(row["ASBRISKLEVELDESCRIPTION"] + (string.IsNullOrEmpty(Convert.ToString(row["RISKDESCRIPTION"])) ? "" : ": " + row["RISKDESCRIPTION"]) + "<br />");
                }
            }

            return AsbDetails.ToString();
        }
        #endregion

        #region get Vulnerability details
        public string getVulnerabilityDetails(DataSet detailsForEmailDS)
        {
            StringBuilder VulnerabilityDetails = new StringBuilder("N/A");


            if ((detailsForEmailDS != null) && (detailsForEmailDS.Tables[ApplicationConstants.VulnerabalityDetailsForContractorsAcceptPOPage] != null) && detailsForEmailDS.Tables[ApplicationConstants.VulnerabalityDetailsForContractorsAcceptPOPage].Rows.Count > 0)
            {
                VulnerabilityDetails.Clear();

                foreach (DataRow row in detailsForEmailDS.Tables[ApplicationConstants.VulnerabalityDetailsForContractorsAcceptPOPage].Rows)
                {
                    VulnerabilityDetails.Append(row["CATDESC"] + (string.IsNullOrEmpty(Convert.ToString(row["SUBCATDESC"])) ? "" : ": " + row["SUBCATDESC"]) + "<br />");
                }
            }

            return VulnerabilityDetails.ToString();
        }
        #endregion


        #region Populate Body and Send Email to Contractor

        public void sendEmailtoContractor()
        {

            string socket = string.Empty;
            string emailId = string.Empty;
            string status = string.Empty;
            string subject = string.Empty;
            string header = string.Empty;
            string acceptUrl = string.Empty;
            if (Request.ServerVariables["HTTPS"] == "on")
            {
                socket = "https";
            }
            else
            {
                socket = "http";
            }


            if (isAccepted == true)
            {
                emailId = objSession.getEmailDetails().ContactorEmailAddress;
                status = "Accepted by Contractor";
                subject = "Purchase Order for Reactive Repair Work - Accepted";
                header = "Please find below the details of the Reactive Repair work that was Accepted today:";
                acceptUrl = socket + ":" + "//" + Request.ServerVariables["server_name"] + "/PropertyDataRestructure/Views/ReactiveRepairs/UpdateFaultPurchaseOrder.aspx?oid=" + objSession.getEmailDetails().OrderId + "&uid=" + objSession.getEmailDetails().EmpId;
                btnAcceptPurchaseOrder.Visible = false;
            }
            else
            {
                emailId = objSession.getEmailDetails().SupplierEmailAddress;
                status = "Rejected by Contractor";
                subject = "Purchase Order for Reactive Repair Work - Rejected";
                header = "Please find below the details of the Reactive Repair work that was Rejected today:";
                btnRejectPurchaseOrder.Visible = false;
            }

            try
            {
                #region conversion of dataSets to string
                StringBuilder worksRequired = new StringBuilder();
                StringBuilder RiskDetail = new StringBuilder();
                StringBuilder VulnerabilityDetails = new StringBuilder();
                //converting work require dataSet to string
                foreach (DataRow row in objSession.GetPurchaseOrderDetailDs.Tables[ApplicationConstants.ReportedFaultVatCostForContractorsAcceptPOPage].Rows)
                {
                    worksRequired.Append(row["WorksRequired"].ToString() +"<br />");

                }
                //converting risk dataSet to string
                foreach (DataRow row in objSession.GetPurchaseOrderDetailDs.Tables[ApplicationConstants.PropertyRiskDataTableForContractorsAcceptPOPage].Rows)
                {
                    RiskDetail.Append(row["SUBCATDESC"].ToString() + "<br />");
                }

                //converting Vulnerability dataSet to string
                foreach (DataRow row in objSession.GetPurchaseOrderDetailDs.Tables[ApplicationConstants.VulnerabalityDetailsForContractorsAcceptPOPage].Rows)
                {
                    VulnerabilityDetails.Append(row["SUBCATDESC"].ToString() + "<br />");
                }

                #endregion

                if (emailId != "" && emailId != null)
                {
                    StringBuilder body = new StringBuilder();
                    StreamReader reader = new StreamReader(Server.MapPath("~/Email/AcceptedWorkByContractor.html"));
                    body.Append(reader.ReadToEnd());
                    body.Replace("{header}", header);
                    body.Replace("{ContractorContactName}", objSession.getEmailDetails().ContactName);
                    body.Replace("{PoRef}", objSession.getEmailDetails().PoRef);
                    body.Replace("{JsRef}", objSession.getEmailDetails().JsRef);
                    body.Replace("{Status}", status);
                    body.Replace("{OrderedBy}", objSession.getEmailDetails().OrderedBy);
                    body.Replace("{DirectDail}", objSession.getEmailDetails().DirectDial ?? "N/A");

                    body.Replace("{NetCost}", Math.Round(decimal.Parse(objSession.getEmailDetails().TotalNetCost .ToString()), 2).ToString());
                    body.Replace("{VAT}", Math.Round(decimal.Parse(objSession.getEmailDetails().TotalVAT.ToString()), 2).ToString());
                    body.Replace("{TOTAL}", Math.Round(decimal.Parse(objSession.getEmailDetails().TotalGROSS .ToString()), 2).ToString());
                    body.Replace("{ReportedFault}", objSession.getEmailDetails().ReportedFault);
                    body.Replace("{WorksRequire}", worksRequired.ToString());
                    body.Replace("{EstimatedCompletion}", objSession.getEmailDetails().EstimatedCompletionDate  ?? "N/A");
                    body.Replace("{Address}", objSession.getEmailDetails().TanentAddress);
                    body.Replace("{TenantName}", objSession.getEmailDetails().TanentName);
                    body.Replace("{TownCity}", objSession.getEmailDetails().TownCity);
                    body.Replace("{Telephone}", objSession.getEmailDetails().Telephone  ?? "N/A");
                    body.Replace("{PostCode}", objSession.getEmailDetails().PostCode);
                    body.Replace(" {Country}", objSession.getEmailDetails().Country);
                    body.Replace("{AsbDetails}", objSession.getEmailDetails().AsbestosDetails);
                   // body.Replace("{VulnerabilityDetail}", objSession.getEmailDetails().VulnerabilityDetails ?? "N/A");                
                    if (string.IsNullOrEmpty(RiskDetail.ToString()) == true)
                    {
                        body.Replace("{RiskDetail}", "N/A");
                    }
                    else
                    {
                        body.Replace("{RiskDetail}", RiskDetail.ToString());
                    }





                    if (string.IsNullOrEmpty(VulnerabilityDetails.ToString()) == true)
                    {
                        body.Replace("{VulnerabilityDetail}", "N/A");
                    }
                    else
                    {
                        body.Replace("{VulnerabilityDetail}", VulnerabilityDetails.ToString());
                    }
                   
                    body.Replace("{#}", acceptUrl);
                    LinkedResource logo50Years = new LinkedResource(Server.MapPath("~/Images/50_Years.gif"));
                    logo50Years.ContentId = "logo50Years_Id";

                    body = body.Replace("{Logo_50_years}", string.Format("cid:{0}", logo50Years.ContentId));

                    LinkedResource logoBroadLandRepairs = new LinkedResource(Server.MapPath("~/Images/Broadland-Housing-Association.gif"));
                    logoBroadLandRepairs.ContentId = "logoBroadLandRepairs_Id";

                    body = body.Replace("{Logo_Broadland-Housing-Association}", string.Format("cid:{0}", logoBroadLandRepairs.ContentId));

                    ContentType mimeType = new ContentType("text/html");

                    AlternateView alternatevw = AlternateView.CreateAlternateViewFromString(body.ToString(), mimeType);
                    alternatevw.LinkedResources.Add(logo50Years);
                    alternatevw.LinkedResources.Add(logoBroadLandRepairs);


                    MailMessage mailMessage = new MailMessage();
                    mailMessage.Subject = subject;
                    mailMessage.To.Add(new MailAddress(emailId));
                    mailMessage.Body = body.ToString();
                    mailMessage.AlternateViews.Add(alternatevw);
                    mailMessage.IsBodyHtml = true;
                  //  uiMessage.isError = true;
                    //uiMessage.showErrorMessage(mailMessage.Body);
                    EmailHelper.sendEmail(ref mailMessage);
                }
                else
                {
                    throw new Exception("Unable to send email, contractor details not available");
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.showErrorMessage(ex.Message);
            }




        }

        #endregion

    }
}