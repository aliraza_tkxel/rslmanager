﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UpdatePurchaseOrder.ascx.cs"
    Inherits="PropertyDataRestructure.UserControls.ReactiveRepairs.UpdatePurchaseOrder" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<link href="../../Styles/Site.css" rel="stylesheet" media="screen" />
<asp:UpdatePanel runat="server" ID="updPanelUpdatePurchaseOrder">
    <ContentTemplate>
        <asp:Panel ID="Panel1" runat="server" Style='border: 1px Solid Black; width:800px;'>
            <div class="AcceptPurchaseOrder">
              &nbsp   Accept Purchase Order:
            </div>
            <div>
                <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" />
                
            </div>
            <table class="ContractorTable">
                <tbody>
                    <tr>
                        <td>
                            Contractor:
                        </td>
                        <td>
                            <asp:Label ID="lblContractor" runat="server"></asp:Label>
                        </td>
                        <td class="buttonDecoration">
                            <asp:Button Text="Update" runat="server" ID="btnUpdatePurchaseOrder" OnClick="btnUpdatePurchaseOrder_Click" UseSubmitBehavior="false"  />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Contact Name:
                        </td>
                        <td>
                            <asp:Label ID="lblContactName" runat="server"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            PO Ref:
                        </td>
                        <td>
                            <asp:Label ID="lblPoRef" runat="server"></asp:Label>
                        </td>
                        <td>
                        </td>
                    </tr>
                     <tr>
                        <td>
                            JS Ref
                        </td>
                        <td>
                            <asp:Label ID="lblJsRef" runat="server"></asp:Label>
                        </td>
                        <td>
                         <asp:Label ID="lblFaultLogID" runat="server" Visible="false" />
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Status:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlPoStatus" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlStatus_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                    </tr>
                   
                    <tr>
                        <td>
                            Ordered By:
                        </td>
                        <td>
                            <asp:Label ID="lblOrderedBy" runat="server" Text=""> </asp:Label>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Direct Dial:
                        </td>
                        <td>
                            <asp:Label ID="lblDirectDial" runat="server" Text=""></asp:Label>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                </tbody>
            </table>
            <asp:Panel ID="Panel2" runat="server" Style='border: 1px Green; margin-top: 10px;'>
                <table class="TanentAdressTable">
                    <tbody>
                        <tr>
                            <td>
                                Address:
                            </td>
                            <td  rowspan="2">
                                <asp:Label ID="lblAddress" runat="server" Text=""></asp:Label>
                            </td>
                            <td >
                                Tenant:
                            </td>
                            <td>
                                <asp:Label ID="lblTenant" runat="server" Text=""></asp:Label>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                           
                            <td>
                                Telephone:
                            </td>
                            <td>
                                <asp:Label ID="lblDirectDialScheduler" runat="server" Text=""></asp:Label>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                Risk:
                            </td>
                            <td>
                                <div style='overflow: auto;'>
                                    <asp:GridView ID="grdRisk" runat="server" AutoGenerateColumns="False" ShowHeader="false"
                                        CssClass="asbestosGrid">
                                        <Columns>
                                            <%-- <asp:BoundField DataField="CATDESC" />--%>
                                            <asp:BoundField DataField="SUBCATDESC" />
                                        </Columns>
                                    </asp:GridView>
                                     <asp:Label ID="lblNoRisk" runat="server" Text="N/A" Visible ="false"></asp:Label>
                                </div>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                Vulnerability:
                            </td>
                            <td>
                                <div style='overflow: auto;'>
                                    <asp:GridView ID="grdVulnerbility" runat="server" AutoGenerateColumns="False" ShowHeader="false"
                                        CssClass="asbestosGrid" GridLines="None">
                                        <Columns>
                                            <%-- <asp:BoundField DataField="CATDESC" />--%>
                                            <asp:BoundField DataField="SUBCATDESC" />
                                        </Columns>
                                    </asp:GridView>
                                     <asp:Label ID="lblNoVulnerbility" runat="server" Text="N/A" Visible ="false"></asp:Label>
                                </div>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                            <td>
                                Asbestos:
                            </td>
                            <td>
                             <asp:LinkButton ID="LinkButtonAsbDetails" runat="server" 
                                    onclick="LinkButtonAsbDetails_Click"><img src="../../Images/warning.png" />Click here for more details</asp:LinkButton>
                                <asp:Label ID="lblNoAsbestos" runat="server" Text="N/A" Visible ="false"></asp:Label>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
            <div class="RequiredFieldDiv">
                 <div style="float: left; padding: 5px; width: 100%;">
                    Reported Fault:
                    <asp:Label ID="lblReportedFault" runat="server" Font-Bold="true"></asp:Label>
                </div>
                <div style="float: left; padding: 5px; width: 100%;">
                    <div style="float: left; padding-bottom: 5px; width: 90%; height:auto;">Works Required:</div>
                    <br />
                    <div class="WorkDetailsDiv">
                        <asp:Panel runat="server" ID="pnlPagination" Visible="true" class="paginationDecoration">
                            <table>
                                <tbody>
                                    <tr>
                                        <td>
                                            <asp:LinkButton ID="lnkbtnPagerFirst" Text="" runat="server" CommandName="Page" Visible="false"
                                                CommandArgument="First" OnClick="lnkbtnPager_Click">&lt;&lt;First</asp:LinkButton>
                                            &nbsp;
                                        </td>
                                        <td>
                                            Page:&nbsp;
                                            <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="currentPage" />
                                            &nbsp;of&nbsp;
                                            <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />
                                            <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" Visible="false" />
                                            <%-- &nbsp;to&nbsp;--%>
                                            <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" Visible="false" />
                                            <%-- &nbsp;of&nbsp;--%>
                                            <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" Visible="false" />
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lnkbtnPagerPrev" Text="" runat="server" CommandName="Page" CommandArgument="Prev"
                                                OnClick="lnkbtnPager_Click">&lt;Prev</asp:LinkButton>
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page"
                                                CommandArgument="Next" OnClick="lnkbtnPager_Click" />
                                            &nbsp;
                                        </td>
                                        <td>
                                            <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" Visible="false"
                                                CommandName="Page" CommandArgument="Last" OnClick="lnkbtnPager_Click" />
                                            &nbsp;
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </asp:Panel>
                          <div style="width: 100%; overflow: auto; height:100px; padding:5px;">
                            <asp:Label ID="lblWorksRequired" runat="server" Font-Bold="true"></asp:Label>
                        </div>
                    </div>
              
                <table class="WorkEstimateTable">
                    <tbody>
                        <tr>
                            <td>
                                Due Date:
                            </td>
                            <td>
                                <asp:TextBox ID="txtBoxDueDate" runat="server" ReadOnly="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Net Cost(&pound;)
                            </td>
                            <td>
                                <asp:TextBox ID="txtBoxNetCost" runat="server" ReadOnly="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Vat(&pound;):
                            </td>
                            <td>
                                <asp:TextBox ID="txtBoxVat" runat="server" ReadOnly="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Total(£):
                            </td>
                            <td>
                                <asp:TextBox ID="txtBoxTotal" runat="server" ReadOnly="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Estimated Completion:
                            </td>
                            <td>
                                <asp:TextBox ID="txtBoxEstimatedCompletion" runat="server" ReadOnly="true"></asp:TextBox>
                            </td>
                        </tr>
                    </tbody>
                </table>  </div>
            </div>
        </asp:Panel>
        <%-- Pop up Completed Fault Panel--%>
        <br />
        <br />
        <br />
        <asp:Panel ID="pnlCompletionDetail" runat="server" Visible="true" CssClass="modalPopup"
            align="center" Style="width: 40%; ">
            <div>
                <table  width="100%"  >
                    <tr>
                        <td>
                            Complete Fault:
                        </td>
                       

                        <td style="width:70px;">
                            <asp:Button runat="server" ID="btnCancel" Text="Cancel"  Style="float: left;" UseSubmitBehavior="false"/>
                        </td>
                         <td style="width:70px;">
                            <asp:Button runat="server" ID="btnSave" Text="Save"  Style="float: left;" 
                                 onclick="btnSave_WorkComleted_Click" UseSubmitBehavior="false"/>
                        </td>
                    </tr>
                   <tr>
                   <td colspan="3"> <hr /></td>
                   </tr>
                </table>

                <table width="100%" style='margin: 10px;'>
                    <tr>
                        <td colspan="2">
                        <div>
                <uim:UIMessage ID="uiMessageOnPopUp" runat="Server" Visible="true"/>
               
            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Completed:
                        </td>
                        <td>
                            <asp:TextBox ID="txtCompleted" runat="server" Style="float: left;"></asp:TextBox>
                            <asp:Image ID="imgCalendar" runat="server" Height="23px" Width="25px" ImageUrl="~/Images/calendar.png"
                                Style="float: left;" />
                            <cc1:CalendarExtender ID="cntrlCalendarExtender" runat="server" TargetControlID="txtCompleted"
                                Format="dd/MM/yyyy" PopupButtonID="imgCalendar" />
                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtCompleted"
                                WatermarkText="DD/MM/YYYY">
                            </cc1:TextBoxWatermarkExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Time:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlHours" runat="server" Width="50px">
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddlMin" runat="server" Width="50px">
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddlFormate" runat="server" Width="50px"></asp:DropDownList>
                                
                            
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            Repair:
                        </td>
                        <td>
                            <asp:TextBox ID="txtSearch" runat="server" Width="92%" AutoPostBack="false" AutoCompleteType="Search"></asp:TextBox>
                            <cc1:AutoCompleteExtender ID="autoComplete1" runat="server" EnableCaching="true"
                                BehaviorID="AutoCompleteEx" TargetControlID="txtSearch" ServicePath="../../Views/ReactiveRepairs/UpdateFaultPurchaseOrder.aspx"
                                ServiceMethod="getRepairSearchResult" CompletionInterval="1000" CompletionSetCount="20"
                                CompletionListCssClass="autocomplete_completionListElement" CompletionListElementID="complitionList"
                                CompletionListItemCssClass="autocomplete_listItem" CompletionListHighlightedItemCssClass="autocomplete_highlightedListItem"
                                DelimiterCharacters=";, :" ShowOnlyCurrentWordInCompletionListItem="true" OnClientItemSelected="ClientItemSelected">
                            </cc1:AutoCompleteExtender>
                            <div id="complitionList">
                            </div>
                            <br />
                            <div style='float: right; margin-right: 5%; margin-top: 2px;'>
                                <asp:Button ID="btnAdd" runat="server" Text="Add" align="right" 
                                    onclick="btnAdd_Click" /></div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <div class="repairsGridContainer" id="repairsGridContainer" runat="server" visible="false">
                                <asp:GridView ID="grdRepairs" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                    BorderStyle="Solid" BorderWidth="1" BorderColor="Black" GridLines="None" Width="98%" 
                                    onrowdeleting="grdRepairs_RowDeleting">
                                    <Columns>
                                        <asp:BoundField DataField="Description">
                                            <ItemStyle Width="80%" />
                                        </asp:BoundField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="imgDelete" runat="server" CommandName="Delete"><img src="../../Images/cross2.png" alt="Delete Fault"  style="border:none; width:16px; " />  </asp:LinkButton>
                                            </ItemTemplate>
                                            <ItemStyle Width="10%" HorizontalAlign="Right" />
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            Repair Notes:
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtBoxRepairNotes" TextMode="MultiLine" Style="width: 230px;
                                height: 40px;"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Follow on works required?:
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlFollowOnWorks">
                                <asp:ListItem Value="1" Text="Yes">Yes</asp:ListItem>
                                <asp:ListItem Value="0" Text="No">No</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td valign="top">
                            Follow on notes:
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtBoxFollowOnNotes" TextMode="MultiLine" Style="width: 230px;
                                height: 40px;"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            <div style='float: right; margin-right: 5%;'>
                                
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
         <%-- No Entry Panel--%>

         <asp:Panel ID="pnlNoEntryDetail" runat="server" Visible="true" CssClass="modalPopup" align="center" Style="width: 30%; ">
            <div>
                <table width="100%" >
                    <tr   colspan="2" align="center">
                        <td>
                        <b>No Entry:</b>
                            
                        </td>
                       
                    </tr>
                    <tr>
                        <td colspan="2">
                            <uim:UIMessage ID="uiMessageOnNoEntryPopup" runat="Server" Visible="false"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Completed:
                        </td>
                        <td>
                            <asp:TextBox ID="txtNoEntry" runat="server" Style="float: left;"></asp:TextBox>
                            <asp:Image ID="imgNoEntryCalendar" runat="server" Height="23px" Width="25px" ImageUrl="~/Images/calendar.png"
                                Style="float: left;" />
                            <cc1:CalendarExtender ID="calExtNoEntry" runat="server" TargetControlID="txtNoEntry"
                                Format="dd/MM/yyyy" PopupButtonID="imgNoEntryCalendar" />
                            <cc1:TextBoxWatermarkExtender ID="txtWater" runat="server" TargetControlID="txtNoEntry"
                                WatermarkText="DD/MM/YYYY">
                            </cc1:TextBoxWatermarkExtender>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Time:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlNoEntryHours" runat="server" Width="50px">
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddlNoEntryMin" runat="server" Width="50px">
                            </asp:DropDownList>
                            <asp:DropDownList ID="ddlNoEntryFormate" runat="server" Width="50px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                    <td></td>
                    </tr>
                    <tr>
                    <td></td>
                     <td>
                     <asp:Button runat="server" ID="btnSaveNoEntry" Text="Save"   onclick="btnSave_NoEntry_Click" UseSubmitBehavior="false" />
                     &nbsp &nbsp &nbsp
                      <asp:Button runat="server" ID="btnCancelNoEntry" Text="Cancel"  />
                               
                        </td>
                       
                        
                    </tr>
                </table>
            </div>
    </asp:Panel>
    <%-- Prompt Popup  Panel--%>
   <asp:Panel ID="pnlPromptPopup" runat="server" Visible="true" CssClass="modalPopup" align="center" Style="width: 30%; ">
            
            <b>Are you sure you want to cancel this Fault:</b> 
            <div style="text-align:center;padding-top:10px">
                <table style="width: 100%">
                   <tr>
                        <td style="width:100px;padding-left:35px">
                        <asp:Button runat="server" ID="PromptPopup_ButtonYes" Text="Yes" 
                                onclick="PromptPopup_ButtonYes_Click" />      
                        </td>
                        
                         <td style="width:100px;padding-right:35px">
                             <asp:Button runat="server" ID="PromptPopup_ButtonNo" Text="No" UseSubmitBehavior="false" /> 
                        </td>
                       
                   </tr>
                 
                </table>
            </div>
    </asp:Panel>
       <%-- Asbestos Details Popup--%>
    <asp:Panel ID="pnlAsbestosPopup" runat="server" Visible="true" CssClass="modalPopup" align="center" Style="width: 30%; ">
            <div>
                <table>
                <tr>
                <td>
                <b>Asbestos Datails</b>
                </td>
                </tr>
                <tr>
                     <td>
                         <div style='overflow:auto;'>
                            <asp:GridView ID="grdAsbestos" runat="server" CssClass="asbestosGrid" ></asp:GridView>
                         </div>
                    </td>
                </tr>

                    <tr>
                  
                        <td>
                        
                        <asp:Button runat="server" ID="AsbestosPopupCloseButton" Text="Close" />          
                        </td>
                   </tr>
                 
                </table>
            </div>
    </asp:Panel>


    <!--Model popup for completed work-->

        <cc1:ModalPopupExtender ID="popupCompletedWork" runat="server" PopupControlID="pnlCompletionDetail"
            TargetControlID="lblDismiss" BackgroundCssClass="modalBackground">
        </cc1:ModalPopupExtender>

          <!--Model popup for No Entry-->
           <cc1:ModalPopupExtender ID="popupNoEntry" runat="server" PopupControlID="pnlNoEntryDetail"
            TargetControlID="LabelForNoEntryPopup" BackgroundCssClass="modalBackground">
        </cc1:ModalPopupExtender>

<!--Model popup for Prompt -->

 <cc1:ModalPopupExtender ID="popupPrompt" runat="server" PopupControlID="pnlPromptPopup"
            TargetControlID="LabelForPromptPopup" BackgroundCssClass="modalBackground">
        </cc1:ModalPopupExtender>


         <asp:Label ID="lblDismiss" runat="server"></asp:Label>
         <asp:Label ID="LabelForNoEntryPopup" runat="server"></asp:Label>
         <asp:Label ID="LabelForPromptPopup" runat="server"></asp:Label>
       
   
     <%-- Model Popup for asbestos--%>
     <cc1:ModalPopupExtender ID="popupAsbDetails" runat="server" PopupControlID="pnlAsbestosPopup"
            TargetControlID="LabelForAsbestosPopup" BackgroundCssClass="modalBackground">
        </cc1:ModalPopupExtender>
     <asp:Label ID="LabelForAsbestosPopup" runat="server"></asp:Label>
    </ContentTemplate>
</asp:UpdatePanel>
