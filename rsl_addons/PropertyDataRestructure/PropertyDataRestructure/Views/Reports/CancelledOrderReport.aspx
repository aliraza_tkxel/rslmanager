﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Pdr.Master" AutoEventWireup="true"
    CodeBehind="CancelledOrderReport.aspx.cs" Inherits="PropertyDataRestructure.Views.Reports.CancelledOrderReport" %>

<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../../Scripts/jquery-1.12.4.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-ui-1.12.1.js" type="text/javascript"></script>
    <%--    <script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/select/1.2.2/js/dataTables.select.min.js"
        type="text/javascript"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        type="text/javascript"></script>--%>
    <script src="../../Scripts/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="../../Scripts/dataTables.bootstrap.min.js" type="text/javascript"></script>
    <script src="../../Scripts/dataTables.select.min.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery.dialog.min.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.2.2/css/select.dataTables.min.css">
    <link href="../../Styles/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/dialog.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/cyclical.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/CancelledOrder.js" type="text/javascript"></script>
    <link href="../../Styles/jquery.dialog.min.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
  <div style=" width:100%; margin-top:10px;">
    <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="400px" />
    <div style="padding: 10px 10px; background-color: #000; font-family: Arial, Helvetica, sans-serif;
        font-size: 16px; color: #fff;">
        <asp:Label Text="Cancelled Order Report" ID="lblHeading" runat="server" />
    </div>
    <asp:UpdatePanel ID="updPnlCyclicalList" runat="server">
        <ContentTemplate>
              <asp:HiddenField ID="hdnSession" runat="server" ClientIDMode="Static" />
            <div style="border: 1px solid black; height: 672px; padding: 5px;">
                <table id="dtCancelled" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>
                                OrderId
                            </th>
                            <th>
                                Date
                            </th>
                            <th>
                                PO Number
                            </th>
                            <th>
                                Cancelled By
                            </th>
                            <th>
                                Reason
                            </th>
                            <th>
                                Scheme
                            </th>
                            <th>
                                Block
                            </th>
                            <th>
                                Attribute
                            </th>                             
                            <th>
                                Contractor
                            </th>
                            <th>
                            </th>
                        </tr>
                    </thead>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
     </div>
     <div class="modal fade" id="AcceptPopUp" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12" style="min-height: 500px;">
                            <iframe id="acceptiFrame" src="" width="100%" height="500px" frameborder="0" style="border: block">
                            </iframe>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <button id="btnClose" class="btn btn-info btn-grey btn-xs pull-right" type="button">
                                Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
