﻿<%@ Page Title="" Language="C#" Culture="en-GB" MasterPageFile="~/MasterPage/Pdr.Master"
    AutoEventWireup="true" CodeBehind="ServiceChargeReport.aspx.cs" Inherits="PropertyDataRestructure.Views.Reports.ServiceChargeReport" %>

<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">


    <style>
        .modalPopup
        {
            background-color: #FFFFFF;
            border-width: 3px;
            border-style: solid;
            border-color: black;
            padding: 0px;
            width:600px;
            height:500px;      
            position:relative;                    
        }
        
        .associatedSchemesModalPopup
        {
            background-color: #FFFFFF;
            border-width: 3px;
            border-style: solid;
            border-color: black;
            padding: 0px;
            width:750px;
            height:500px;      
            position:relative;        
        }
        
        
        .line{
            border:2px solid;
            border-radius:2px;
        }
        .dashboard th{
            background: #fff;
            border-bottom: 4px solid #8b8687;
            font-weight:normal;
        }
        .boldhd 
        {
            font-weight:bold;
            
        }
        .dashboard th a{
            color: #000 !important;
        }
        .dashboard th img{
            float:right;
        }
        .text_div{
            width:238px;
        }
        .text_div input{
            width:170px !important;
        }
        hr{
            /* color: #848484; */
            background-color: #CCCCCC;
            border: 0 none;
            color: #A0A0A0;
            height: 1px;
            margin: 0px;
        } 
        
        .txt-width 
        {
          width:240px;     
             
         } 
         .select_div select 
        {
          width:132px !important;     
             
         }
         
        .count-link-button
        {
            color:Black;
            text-decoration:underline;
        }
       
    </style>

    <script type="text/javascript">


    function showDetail(detail, ref) {
        
        if (detail.search("PO ") != -1) {
            showPODetail(ref);
        } else { 
            showGeneralJournalDetail(ref);
        }

        return false;
    }

    function showPODetail(Order_id) {        
        var purchaseOrderUrl =  '<%= ResolveClientUrl("~/../Finance/Popups/PurchaseOrder.asp")%>'
        var queryStringParams = "?OrderID=" + Order_id + "&Random=" + new Date() ;

	        if (window.showModelessDialog) {        // Internet Explorer
	            window.showModalDialog(purchaseOrderUrl + queryStringParams , "_blank", "dialogHeight: 460px; dialogWidth: 850px; status: No; resizable: No;");
	         } 
	        else {
	            window.open(purchaseOrderUrl + queryStringParams, "", "width=850px, height=460px, alwaysRaised=yes, scrollbars=no");
	        }
	}

	function showGeneralJournalDetail(TXN_ID) {

        var generalJournalPopupUrl = '<%= ResolveClientUrl("~/../Accounts/NominalLedger/Popups/viewGJ.asp") %>';
	    var queryStringParams = "?TXNID=" + TXN_ID + "&Random=" + new Date();

        if (window.showModelessDialog) {        // Internet Explorer
            window.showModelessDialog(generalJournalPopupUrl + queryStringParams , "_blank", "dialogHeight: 440px; dialogWidth: 750px; status: No; resizable: No;")
        }
        else {
            window.open(generalJournalPopupUrl + queryStringParams, "", "width=850px, height=460px, alwaysRaised=yes, scrollbars=no");
        }
    }

    </script>

</asp:Content>
<asp:Content ID="ContentProvisioningReport" ContentPlaceHolderID="ContentPlaceHolder1"
    runat="server">
    <asp:UpdateProgress ID="updateProgressReportsArea" runat="server" DisplayAfter="10">
        <ProgressTemplate>
            <div class="loading-image">
                <img alt="Please Wait" src= '<%= ResolveClientUrl("~/Images/menu/progress.gif")%>'/>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="400px" />
    <div class="portlet">
        <div class="header">
            <span class="header-label">Service Charge Report</span>
        </div>
        <div class="portlet-body" style="font-size: 12px; overflow:inherit; padding-bottom:0;">
            <asp:UpdatePanel ID="updPnlServiceCharge" runat="server" >
                <ContentTemplate>
                    <div style="overflow:auto;">
                        <div class="form-control">
                            <div class="select_div">
                                <div class="label">
                                    <asp:Label ID="Label2" runat= "server" Text ="Financial Year:" />
                                </div>
                                <div class="field">
                                     <asp:DropDownList ID="ddlfiscal" runat="server" class="selectval" CssClass="selectval"
                                      AutoPostBack="True" >
                                     </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="form-control">
                            <div class="select_div">
                                <div class="label">
                                    <asp:Label ID="lblSchemeNam" runat= "server" Text ="Scheme:" />
                                </div>
                                <div class="field">
                                    <asp:DropDownList ID="ddlScheme" runat="server" class="selectval" CssClass="selectval" OnSelectedIndexChanged="ddlScheme_SelectedIndexChanged"
                                      AutoPostBack="True" >
                                     </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="form-control">
                            <div class="select_div">
                                <div class="label">
                                    <asp:Label ID="lblBlockName" runat= "server" Text ="Block:" />
                                </div>
                                <div class="field">
                                    <asp:DropDownList ID="ddlBlock" runat="server" class="selectval" CssClass="selectval"
                                      >
                                     </asp:DropDownList>
                                </div>
                            </div>
                        </div>
                        <div class="form-control">
                            <div class="select_div">
                                <div class="label">
                                    <asp:Label ID="lblItemName" runat= "server" Text ="Attribute:" />
                                </div>
                                <div class="field">
                                    <%--<asp:TextBox ID="txtItemName" runat="server" placeholder="Attribute" ></asp:TextBox>--%>
                                    <div class="field">
                                     <asp:DropDownList ID="ddlAttr" runat="server" class="selectval" CssClass="selectval">
                                     </asp:DropDownList>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div style="overflow:auto;margin-left:10px">
                    
                    <div class="form-control right">
                            <div class="select_div right" style="padding-top:12px;">
                                <div class="field right" style="margin-left:0;">
                                    <asp:Button ID="btnFilter" Text="Filter" runat="server" UseSubmitBehavior="False" OnClick="btnFilter_Click"
                                        CssClass="btn btn-xs btn-blue right" style="padding:3px 23px !important; margin:-3px 0 0 0;" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div style=" border-bottom: 1px solid #A0A0A0; width: 100%; padding:0">
                        <cc1:PagingGridView ID="grdServiceCharge" runat="server" AutoGenerateColumns="False" EmptyDataText="No Records Found"
                            OnSorting="grdServiceCharge_Sorting" OnRowCreated="grdServiceCharge_RowCreated"  OnRowDataBound="grdServiceCharge_OnRowDataBound"
                            Width="100%" Style="overflow: scroll" BorderWidth="0px" CssClass="dashboard webgrid table table-responsive" 
                            GridLines="None" ShowHeaderWhenEmpty="True" AllowPaging="false" AllowSorting="True" PageSize="20">
                            <Columns>
                                <asp:TemplateField HeaderText="Scheme Name:" ItemStyle-CssClass="dashboard" SortExpression="SchemeName">
                                    <ItemTemplate>
                                        <asp:Label ID="lblScheme" runat="server" Text='<%# Bind("SchemeName") %>'></asp:Label>
                                        <asp:LinkButton runat="server"  ID = "lnkBtnScheme"
                                         CommandArgument='<%# Eval("ItemId").ToString()%>'
                                         OnClick="lnkBtnScheme_Click" Text='<%# Bind("SchemeName") %>' ></asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle BorderStyle="None" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Block Name:" SortExpression="BlockName">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBlockName" runat="server" Text='<%# Bind("BlockName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField> 
                                <asp:TemplateField HeaderText="Number of Properties (Included)" SortExpression="InPropCount"
                                    ItemStyle-Width="50px">
                                    <ItemTemplate>
                                    <asp:Label ID="lblInPropCount" runat="server" Text='<%# Bind("InPropCount") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Details:" SortExpression="Details">
                                    <ItemTemplate>    
                                        <asp:HiddenField ID="hdnRef" runat="server" Value='<%# Bind("Ref") %>' />                                                                                                                   
                                        <asp:LinkButton ID="lnkBtnDetail" runat="server" Text='<%# Bind("Details") %>' ></asp:LinkButton>                                        
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Attribute/PO Item:" SortExpression="ItemName">
                                    <ItemTemplate>
                                        <asp:Label ID="lblItemId" runat="server" Text='<%# Bind("ItemId") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblItemName" runat="server" Text='<%# Bind("ItemName") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                
                                <asp:TemplateField HeaderText="Budget:" SortExpression="Budget">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAnnualApportionment" runat="server" Text='<%# Bind("Budget") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Apportionment (Budget):" SortExpression="ApportionmentBudget"  HeaderStyle-Font-Bold="true">
                                    <ItemTemplate>
                                        <asp:Label ID="lblApportionmentBudget" runat="server" Font-Bold="true" Text='<%# Bind("ApportionmentBudget") %>'></asp:Label>
                                    </ItemTemplate>
                                   
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Actual (Total):" SortExpression="ActualTotal">
                                    <ItemTemplate>
                                        <asp:Label ID="lblActualTotal" runat="server" Text='<%# Bind("ActualTotal") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Apportionment (Actual):" SortExpression="ApportionmentActual"  HeaderStyle-Font-Bold="true" >
                                    
                                    <ItemTemplate>
                                        <asp:Label ID="lblApportionmentActual" runat="server" Font-Bold="true" Text='<%# Bind("ApportionmentActual") %>'></asp:Label>
                                    
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Excluded Properties" SortExpression="ExPropCount"
                                    ItemStyle-Width="70px">
                                    <ItemTemplate>
                                        <div style="color:blue; cursor:pointer;">
                                            <asp:HiddenField ID="hdnType" runat="server" Value='<%# Bind("Type") %>' />      
                                            <asp:button ID="btnExPropCount" runat="server" Text='<%# Bind("ExPropCount") %>' 
                                                        CssClass="btn btn-xs btn-blue right" style="padding:3px 23px !important; margin:-3px 0 0 0;"
                                                        CommandArgument='<%# Eval("SchemeId").ToString()+";"+Eval("BlockId").ToString()+";"+Eval("ItemId").ToString()+";"+Eval("IsServiceChargePO").ToString()+";"+Eval("ChildItemId").ToString()%>'
                                                        OnClick="btnExPropCount_Click"></asp:button>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle BorderColor="Black" HorizontalAlign="Left" />
                        </cc1:PagingGridView>
                    </div>
                    <asp:Panel runat="server" ID="pnlPagination" Visible="false" Style="color: Black; margin: 0 auto; width: 98%;">
                        <div style="width: 100%; padding: 15px 0 30px 0px; text-align:center;">
                            <div class="paging-left">
                                <span style="padding-right:10px;">
                                    <asp:LinkButton ID="lnkbtnPagerFirst" runat="server" Text="" CommandName="Page" CommandArgument="First" cssClass="lnk-btn"
                                        OnClick="lnkbtnPager_Click">
                                        &lt;&lt;First
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkbtnPagerPrev" Text="Prev" runat="server" CommandName="Page" CommandArgument="Prev" cssClass="lnk-btn"
                                        OnClick="lnkbtnPager_Click">
                                        &lt;Prev
                                    </asp:LinkButton>
                                </span>
                                <span style="padding-right:10px;">
                                    <b>Page:</b>
                                    <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="currentPage" />
                                    of
                                    <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />.
                                </span>
                                <span style="padding-right:20px;">
                                    <b>Result:</b>
                                    <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                                    to
                                    <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                                    of
                                    <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                                </span>
                                <span style="padding-right:10px;">
                                    <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page" CommandArgument="Next" cssClass="lnk-btn"
                                        OnClick="lnkbtnPager_Click">
                                    
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page" CommandArgument="Last" cssClass="lnk-btn"
                                        OnClick="lnkbtnPager_Click">
                                        
                                    </asp:LinkButton>
                                </span>
                            </div>
                            <div style="float: right;">
                                 <span>
                                    <asp:Button ID="btnExportToExcel" runat="server" Text="Export to XLS" UseSubmitBehavior="False"
                                        class="btn btn-xs btn-blue right" style="padding:1px 5px !important;" OnClick="btnExportToExcel_Click" />
                                </span>
                                <asp:RangeValidator ID="rangevalidatorPageNumber" runat="server" ErrorMessage="Enter a page between 1 and "
                                    ValidationGroup="pageNumber" MinimumValue="1" MaximumValue="1" ControlToValidate="txtPageNumber"
                                    Type="Integer" SetFocusOnError="True" CssClass="Required" Display="Dynamic" />
                                <div class="field" style="margin-right: 10px;">
                                    <asp:TextBox ID="txtPageNumber" runat="server" Width="40px" ValidationGroup="pageNumber" PlaceHolder="Page"
                                    onchange="document.getElementById('txtPageNumberHidden').value = this.value;" />
                                </div>
                                <span>
                                    <asp:Button Text="Go" runat="server" ID="btnGoPageNumber" ValidationGroup="pageNumber" UseSubmitBehavior="false"
                                        class="btn btn-xs btn-blue" style="padding:1px 5px !important; margin-right: 10px;min-width:0px;" OnClick="changePageNumber" />
                                </span>
                            </div>
                        </div>
                    </asp:Panel>

                    <%--Associated Excluded Properties Popup Start --%>
                    <asp:Button ID="btnHiddenShowProperties" runat="server" Text="" Style="display: none;" />
                    <asp:ModalPopupExtender ID="mdlPopupShowProperties" runat="server" TargetControlID="btnHiddenShowProperties" CancelControlID="imgBtnCancelAddWarrantyPopup"
                        PopupControlID="pnlShowProperties" Enabled="true" DropShadow="true" BackgroundCssClass="modalBackground">
                    </asp:ModalPopupExtender>
                    <asp:Panel ID="pnlShowProperties" runat="server" CssClass="modalPopup">
                        <div id="Div2" runat="server">
                            <asp:ImageButton ID="imgBtnCancelAddWarrantyPopup" runat="server" Style="position: absolute;
                                top: -1px; right: -1px; width: 22px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png"
                                BorderWidth="0" />
                            <div class="header">
                                <span class="header-label">Excluded Properties</span>
                            </div>
                            <div style="width: 100%; padding: 0;max-height: 444px; overflow:auto;">
                            <cc1:PagingGridView ID="grdExcludedProperties" runat="server" AutoGenerateColumns="False"
                                ShowHeader="false" EmptyDataText="No Records Found" Width="100%" Style="overflow: scroll"
                                BorderWidth="0px" CssClass="dashboard webgrid table table-responsive" GridLines="None"
                                AllowPaging="false" AllowSorting="False">
                                <Columns>
                                    <asp:TemplateField ItemStyle-CssClass="dashboard" ItemStyle-Width="100%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBlocks" runat="server" Text='<%# Bind("ADDRESS") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle BorderStyle="None" />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle BorderColor="Black" HorizontalAlign="Left" />
                            </cc1:PagingGridView>
                            </div>
                        </div>

                    </asp:Panel>
                     <%--Associated Excluded Properties Popup End --%>

                    <%--Associated Scheme/Block Popup Start --%>
                    <asp:Button ID="btnAssociatedSchemeBlock" runat="server" Text="" Style="display: none;" />
                    <asp:ModalPopupExtender ID="mdlPopupAsscoiatedSchemes" runat="server" TargetControlID="btnAssociatedSchemeBlock" CancelControlID="imgBtnCancelAssociatedSchemesPopup"
                        PopupControlID="pnlAssociatedSchemes" Enabled="true" DropShadow="true" BackgroundCssClass="modalBackground">
                    </asp:ModalPopupExtender>
                    <asp:Panel ID="pnlAssociatedSchemes" runat="server" CssClass="associatedSchemesModalPopup">
                        <div runat="server">
                            <asp:ImageButton ID="imgBtnCancelAssociatedSchemesPopup" runat="server" Style="position: absolute;
                                top: -1px; right: -1px; width: 22px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png"
                                BorderWidth="0" />
                            <div class="header">
                                <span class="header-label">Associated Schemes/Blocks</span>
                            </div>
                            <div style="overflow: auto;">
                                <div class="form-control">
                                    <div class="select_div">
                                        <div class="label" style=" line-height:15px;">
                                            <asp:Label ID="Label3" runat="server" Text="Total Number of Schemes: " />
                                        </div>
                                        <div class="field">
                                            <asp:Label ID="lblTotalNumberOfSchemes" runat="server" Text="" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-control">
                                    <div class="select_div">
                                        <div class="label" style=" line-height:15px;">
                                            <asp:Label ID="Label5" runat="server" Text="Total Number of Blocks: " />
                                        </div>
                                        <div class="field">
                                            <asp:Label ID="lblTotalNumberOfBlocks" runat="server" Text="" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <hr />
                            <asp:UpdatePanel ID="updPanelAssociatedSchemesBlocks" runat="server">
                                <ContentTemplate>
                                    <div style="border-bottom: 1px solid #A0A0A0; width: 100%; padding: 0; max-height: 427px;
                                        overflow: auto;">
                                        <cc1:PagingGridView ID="grdAssociatedSchemes" runat="server" AutoGenerateColumns="False"
                                            OnRowDataBound="grdAssociatedSchemes_OnRowDataBound" EmptyDataText="No Records Found"
                                            Width="100%" Style="overflow: scroll" BorderWidth="0px" CssClass="dashboard webgrid table table-responsive"
                                            GridLines="None" ShowHeaderWhenEmpty="True" AllowPaging="false" AllowSorting="false">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Scheme:" ItemStyle-CssClass="dashboard" SortExpression="SchemeName"
                                                    ItemStyle-Width="40%">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblScheme" runat="server" Text='<%# Bind("SchemeName") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle BorderStyle="None" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Block:" SortExpression="BlockCount" ItemStyle-Width="10%"
                                                    HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkBtnBlockCount" CssClass="count-link-button" runat="server"
                                                            CommandArgument='<%# Eval("OrderItemId").ToString()+";"+Eval("SchemeId").ToString()+";"+Eval("SchemeName").ToString() %>'
                                                            Text='<%# Bind("BlockCount") %>' OnClick="lnkBtnBlockCount_Click"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="No of Properties (Inc):" SortExpression="PropInCount"
                                                    ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="20%">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkBtnInPropCount" CssClass="count-link-button" OnClick="lnkBtnInPropCount_Click"
                                                            runat="server" CommandArgument='<%# Eval("OrderItemId").ToString()+";"+Eval("SchemeId").ToString()+";"+Eval("SchemeName").ToString() %>'
                                                            Text='<%# Bind("PropInCount") %>'></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="No of Properties (Exc):" SortExpression="PropExcCount"
                                                    ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="20%">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkBtnExPropCount" runat="server" CssClass="count-link-button"
                                                            OnClick="lnkBtnExPropCount_Click" CommandArgument='<%# Eval("OrderItemId").ToString()+";"+Eval("SchemeId").ToString()+";"+Eval("SchemeName").ToString() %>'
                                                            Text='<%# Bind("PropExcCount") %>'></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle BorderColor="Black" HorizontalAlign="Left" />
                                        </cc1:PagingGridView>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </div>
                    </asp:Panel>
                    <%--Associated Scheme/Block Popup End --%>



                    <%--Associated Properties Popup Start --%>
                    <asp:Button ID="btnAssociatedProperties" runat="server" Text="" Style="display: none;" />
                    <asp:ModalPopupExtender ID="mdlPopupAsscoiatedProperties" runat="server" TargetControlID="btnAssociatedProperties" CancelControlID="imgBtnCancelAssociatedPropertiesPopup"
                        PopupControlID="pnlAssociatedProperties" Enabled="true" DropShadow="true" BackgroundCssClass="modalBackground">
                    </asp:ModalPopupExtender>
                    <asp:Panel ID="pnlAssociatedProperties" runat="server" CssClass="associatedSchemesModalPopup">
                        <div  runat="server" style="max-height: 500px; overflow: auto;">
                            <asp:ImageButton ID="imgBtnCancelAssociatedPropertiesPopup" runat="server" Style="position: absolute;
                                top: -1px; right: -1px; width: 22px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png"
                                BorderWidth="0" />
                            <div class="header">
                                <span class="header-label">Associated Properties</span>
                            </div>                            
                            <div style="overflow: auto;">
                                <div class="form-control">
                                    <div class="select_div">
                                        <div class="label" style=" line-height:15px;">
                                            <asp:Label ID="Label4" runat="server" Text="Scheme Name: " />
                                        </div>
                                        <div class="field">
                                            <asp:Label ID="lblSchemeName" runat="server" Text="" />
                                        </div>
                                    </div>
                                </div>                                
                            </div>
                            <hr />                            
                            <uim:UIMessage ID="uiMessagePropertyPopup" runat="Server" Visible="false" width="400px"/>
                            <div style="padding-top: 25px; padding-bottom:25px; margin-left:42px;">
                                <table>
                                    <thead>
                                        <tr>
                                            <th style=" text-align:center; font-weight:bold;">
                                                No of Properties (Inc)
                                            </th>
                                            <th>
                                                &nbsp;
                                            </th>
                                            <th style=" text-align:center; font-weight:bold;">
                                                No of Properties (Exc):
                                            </th>
                                        </tr>
                                    </thead>
                                    <tr>
                                        <td>
                                            &nbsp
                                        </td>
                                        <td>
                                            &nbsp
                                        </td>
                                        <td>
                                            &nbsp
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:ListBox runat="server" ID="ddlIncludedProperties"  Width="300px" Height="275px"
                                                ClientIDMode="Static" SelectionMode="Multiple"></asp:ListBox>
                                        </td>
                                        <td>
                                            <table>
                                                <tr>
                                                    <td>
                                                        <asp:Button ID="btnExcludeProperty" runat="server" Text=" > " OnClick="btnExcludeProperty_Click" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Button ID="btnIncludeProperty" runat="server" Text=" < " OnClick="btnIncludeProperty_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <asp:ListBox runat="server" ID="ddlExcludedProperties" Width="300px" Height="275px"
                                                ClientIDMode="Static" SelectionMode="Multiple"></asp:ListBox>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <hr />
                            <div style="text-align: right; margin-top:10px; margin-right:42px;">
                                <asp:Button ID="btnSaveAmends" runat="server" Text="Save Amends" UseSubmitBehavior="False"
                                    class="btn btn-xs btn-blue right" Style="padding: 5px 20px !important;" OnClick="btnSaveAmends_Click" />
                            </div>
                        </div>
                    </asp:Panel>
                    <%--Associated Properties Popup End --%>


                     <%--Associated Blocks Popup Start --%>
                    <asp:Button ID="btnAssociatedBlocks" runat="server" Text="" Style="display: none;" />
                    <asp:ModalPopupExtender ID="mdlPopupAssociatedBlocks" runat="server" TargetControlID="btnAssociatedBlocks" CancelControlID="imgBtnCancelAssociatedBlocksPopup"
                        PopupControlID="pnlAssociatedBlocks" Enabled="true" DropShadow="true" BackgroundCssClass="modalBackground">
                    </asp:ModalPopupExtender>
                    <asp:Panel ID="pnlAssociatedBlocks" runat="server" CssClass="associatedSchemesModalPopup">
                        <div id="Div1" runat="server" style="max-height: 500px; overflow: auto;">
                            <asp:ImageButton ID="imgBtnCancelAssociatedBlocksPopup" 
                                runat="server" Style="position: absolute; top: -1px; right: -1px; width: 22px;"
                                ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
                            <div class="header">
                                <span class="header-label">Associated Blocks</span>
                            </div>
                            <uim:UIMessage ID="uiMessageBlocksPopup" runat="Server" Visible="false" width="400px" />
                            <cc1:PagingGridView ID="grdSchemeBlocks" runat="server" AutoGenerateColumns="False"
                                ShowHeader="false" EmptyDataText="No Records Found" Width="100%" Style="overflow: scroll"
                                BorderWidth="0px" CssClass="dashboard webgrid table table-responsive" GridLines="None"
                                AllowPaging="false" AllowSorting="False">
                                <Columns>
                                    <asp:TemplateField ItemStyle-CssClass="dashboard" ItemStyle-Width="100%">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBlocks" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle BorderStyle="None" />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle BorderColor="Black" HorizontalAlign="Left" />
                            </cc1:PagingGridView>
                        </div>
                    </asp:Panel>
                    <%--Associated Properties Popup End --%>

                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnExportToExcel" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
