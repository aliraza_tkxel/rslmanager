﻿using System;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using PDR_BusinessLogic.Reports;
using PDR_DataAccess.Reports;
using PDR_Utilities.Constants;
using System.Collections.Generic;
using System.Linq;

namespace PropertyDataRestructure.Views.Reports
{
    public partial class RentAccountSummary : Page
    {
        #region Page load event
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string tenancyId = Request.QueryString[PathConstants.TenancyId];
                string customerId = Request.QueryString[PathConstants.RentAccountCustomerId];
                PopulateRentAccountTransactions(customerId, tenancyId);
            }
        }
        #endregion

        #region "Populate Reports via Query string"

        private void PopulateRentAccountTransactions(string customerId, string tenancyId)
        {
            ReportsBL objReportsBl = new ReportsBL(new ReportsRepo());
            DataSet resultDataSet = new DataSet();
            DataSet CustomerAccountRechargeDataSet = new DataSet();
            DataSet customerInfoDataSet = new DataSet();
            DataSet customerLastPaymentDataSet = new DataSet();
            DataSet incomeRecoveryOfficerDataSet = new DataSet();

            tenancyIdRef.InnerText = tenancyId;
            quoteRef.InnerText = tenancyId;
            var date = DateTime.Now;
            currentDate.InnerText = date.ToString("dd MMMM yyyy");
            balanceAt.InnerText = currentDate.InnerText;

            objReportsBl.GetIncomeRecoveryOfficer(ref incomeRecoveryOfficerDataSet, tenancyId);
            objReportsBl.GetCustomerInfo(ref customerInfoDataSet, Convert.ToInt32(tenancyId));
            objReportsBl.GetCustomerAccountData(ref resultDataSet, tenancyId);
            objReportsBl.GetCustomerLastPayment(ref customerLastPaymentDataSet, customerId, tenancyId);
            objReportsBl.GetCustomerAccountRecharge(ref CustomerAccountRechargeDataSet, tenancyId);

            if (CustomerAccountRechargeDataSet.Tables[0].Rows.Count > 0)
            {
                decimal rechargeAccountBalance = !string.IsNullOrEmpty(CustomerAccountRechargeDataSet.Tables[0].Rows[CustomerAccountRechargeDataSet.Tables[0].Rows.Count - 1]["BREAKDOWN"].ToString()) ? Math.Round(Convert.ToDecimal(CustomerAccountRechargeDataSet.Tables[0].Rows[CustomerAccountRechargeDataSet.Tables[0].Rows.Count - 1]["BREAKDOWN"].ToString()), 2) : 0.00m;
                if (rechargeAccountBalance < 0)
                    lblRechargeAccountBalance.ForeColor = Color.Red;
                lblRechargeAccountBalance.Text = "£" + Math.Abs(rechargeAccountBalance).ToString(CultureInfo.InvariantCulture);
            }
            else
            {
                lblRechargeAccountBalance.Text = "£0.00";
            }

            int dataCount = resultDataSet.Tables[0].Rows.Count;
            string transcationDate = string.Empty;
            string breakDown = string.Empty;

            if (dataCount > 0)
            {
                foreach (DataRow row in resultDataSet.Tables[0].Rows)
                {
                    row["DEBIT"] = !string.IsNullOrEmpty(row["DEBIT"].ToString()) ? Math.Abs(Math.Round(Convert.ToDecimal(row["DEBIT"].ToString()), 2)).ToString(CultureInfo.InvariantCulture) : "";
                    row["CREDIT"] = !string.IsNullOrEmpty(row["CREDIT"].ToString()) ? Math.Abs(Math.Round(Convert.ToDecimal(row["CREDIT"].ToString()), 2)).ToString(CultureInfo.InvariantCulture) : "";
                    row["BREAKDOWN"] = !string.IsNullOrEmpty(row["BREAKDOWN"].ToString()) ? Math.Abs(Math.Round(Convert.ToDecimal(row["BREAKDOWN"].ToString()), 2)).ToString(CultureInfo.InvariantCulture) : "";
                }
                if (dataCount == 7)
                {
                    transcationDate = resultDataSet.Tables[0].Rows[6]["F_TRANSACTIONDATE_TEXT"].ToString();
                    breakDown = resultDataSet.Tables[0].Rows[6]["BREAKDOWN"].ToString();
                    resultDataSet.Tables[0].Rows[6].Delete();
                    rentAccountTransactionRpt.DataSource = resultDataSet;
                    rentAccountTransactionRpt.DataBind();

                    Control FooterTemplate =
                        rentAccountTransactionRpt.Controls[rentAccountTransactionRpt.Controls.Count - 1].Controls[0];
                    Control trBlanceForward = FooterTemplate.FindControl("trBlanceForward") as Control;
                    Label lblBalanceForwardDate = trBlanceForward.FindControl("lblBalanceForwardDate") as Label;
                    Label lblBalanceForward = trBlanceForward.FindControl("lblBalanceForward") as Label;
                    trBlanceForward.Visible = true;
                    if (lblBalanceForwardDate != null)
                        lblBalanceForwardDate.Text = "from " + transcationDate;
                    if (lblBalanceForward != null)
                        lblBalanceForward.Text = "£" + Math.Abs(Math.Round(Convert.ToDecimal(breakDown), 2));
                }
                else
                {
                    rentAccountTransactionRpt.DataSource = resultDataSet;
                    rentAccountTransactionRpt.DataBind();
                    Control FooterTemplate =
                        rentAccountTransactionRpt.Controls[rentAccountTransactionRpt.Controls.Count - 1].Controls[0];
                    Control trBlanceForward = FooterTemplate.FindControl("trBlanceForward") as Control;
                    Label lblBalanceForward = trBlanceForward.FindControl("lblBalanceForward") as Label;
                    lblBalanceForward.Text = "£0.00";
                }
            }
            else
            {
                NoRecentTranscation.Visible = true;
            }

            DataTable dt = incomeRecoveryOfficerDataSet.Tables[0];
            //check if the datatable has rows
            if (dt.Rows.Count > 0)
            {
                IncomeOfficerName.InnerText = dt.Rows[0]["FullName"].ToString();
                IncomeOfficerTel.InnerText = dt.Rows[0]["HomeTel"].ToString();
                IncomeOfficerMobile.InnerText = dt.Rows[0]["WorkMobile"].ToString();
                IncomeOfficerEmail.InnerText = dt.Rows[0]["WorkEmail"].ToString();
            }

            if (customerInfoDataSet.Tables[0].Rows.Count > 0 && customerInfoDataSet.Tables[1].Rows.Count > 0)
            {
                decimal toPay = !string.IsNullOrEmpty(customerInfoDataSet.Tables[0].Rows[0]["OwedToBHA"].ToString()) ? Math.Round(Convert.ToDecimal(customerInfoDataSet.Tables[0].Rows[0]["OwedToBHA"].ToString()), 2) :  0.00m;
                decimal rentAccount = !string.IsNullOrEmpty(customerInfoDataSet.Tables[0].Rows[0]["RentBalance"].ToString()) ? Math.Round(Convert.ToDecimal(customerInfoDataSet.Tables[0].Rows[0]["RentBalance"].ToString()), 2) : 0.00m;
                if (toPay < 0)
                    lblToPay.ForeColor = Color.Red;
                lblToPay.Text = "£" + Math.Abs(toPay).ToString(CultureInfo.InvariantCulture);
                if (rentAccount < 0)
                    lblRAB.ForeColor = Color.Red;
                lblRAB.Text = "£" + Math.Abs(rentAccount).ToString(CultureInfo.InvariantCulture);

                List<string> lstCustName = (from DataRow row in customerInfoDataSet.Tables[1].Rows select row["CustomerName"].ToString()).ToList();

                customerName.InnerText = string.Join(", ", lstCustName);
                string address = (!string.IsNullOrEmpty(customerInfoDataSet.Tables[0].Rows[0]["HOUSENUMBER"].ToString())
                    ? customerInfoDataSet.Tables[0].Rows[0]["HOUSENUMBER"].ToString()
                    : "") + " " + (!string.IsNullOrEmpty(customerInfoDataSet.Tables[0].Rows[0]["ADDRESS1"].ToString())
                    ? customerInfoDataSet.Tables[0].Rows[0]["ADDRESS1"].ToString()
                    : "") ;

                address1.InnerText = address;
                address2.InnerText = !string.IsNullOrEmpty(customerInfoDataSet.Tables[0].Rows[0]["ADDRESS2"].ToString()) ? customerInfoDataSet.Tables[0].Rows[0]["ADDRESS2"].ToString() : "";
                townCity.InnerText = !string.IsNullOrEmpty(customerInfoDataSet.Tables[0].Rows[0]["TOWNCITY"].ToString()) ? customerInfoDataSet.Tables[0].Rows[0]["TOWNCITY"].ToString() : "";

                county.InnerText = !string.IsNullOrEmpty(customerInfoDataSet.Tables[0].Rows[0]["COUNTY"].ToString()) ? customerInfoDataSet.Tables[0].Rows[0]["COUNTY"].ToString() : "";
                postCode.InnerText = !string.IsNullOrEmpty(customerInfoDataSet.Tables[0].Rows[0]["POSTCODE"].ToString()) ? customerInfoDataSet.Tables[0].Rows[0]["POSTCODE"].ToString() : "";
                
            }
            else
            {
                lblToPay.Text = "£0.00";
                lblRAB.Text = "£0.00";
            }

            if (customerLastPaymentDataSet.Tables[0].Rows.Count > 0)
            {
                decimal estimatedHousing = !string.IsNullOrEmpty(customerLastPaymentDataSet.Tables[0].Rows[0]["EstimatedHBDue"].ToString()) ? Math.Round(Convert.ToDecimal(customerLastPaymentDataSet.Tables[0].Rows[0]["EstimatedHBDue"].ToString()), 2) : 0.00m;
                if (estimatedHousing < 0)
                    lblRAB.ForeColor = Color.Red;
                lblEHB.Text = "£" + Math.Abs(estimatedHousing).ToString(CultureInfo.InvariantCulture);
            }
            else
            {
                lblEHB.Text = "£0.00";
            }                  
        }

        #endregion
    }
}