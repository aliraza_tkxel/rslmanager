﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Pdr.Master" AutoEventWireup="true"
    CodeBehind="UnassignedCyclicalReport.aspx.cs" Inherits="PropertyDataRestructure.Views.Reports.UnassignedCyclicalReport" %>

<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../../Scripts/jquery-1.12.4.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-ui-1.12.1.js" type="text/javascript"></script>
    <%--    <script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/select/1.2.2/js/dataTables.select.min.js"
        type="text/javascript"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        type="text/javascript"></script>--%>
    <script src="../../Scripts/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="../../Scripts/dataTables.bootstrap.min.js" type="text/javascript"></script>
    <script src="../../Scripts/dataTables.select.min.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery.dialog.min.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.2.2/css/select.dataTables.min.css">
    <link href="../../Styles/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/dialog.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/cyclical.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/UnassignedCyclicalServices.js" type="text/javascript"></script>
    <link href="../../Styles/jquery.dialog.min.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <%--  <asp:UpdateProgress ID="updateProgressReportsArea" runat="server" DisplayAfter="10">
        <ProgressTemplate>
            <div class="loading-image">
                <img alt="Please Wait" src="../../../Images/menu/progress.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>--%>
    <div style=" width:100%; margin-top:10px;">
    <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="400px" />
    <div style="padding: 10px 10px; background-color: #000; font-family: Arial, Helvetica, sans-serif;
        font-size: 16px; color: #fff;">
        <asp:Label Text="Unassigned Cyclical Services Report" ID="lblHeading" runat="server" />
    </div>
   
    <asp:UpdatePanel ID="updPnlCyclicalList" runat="server">
        <ContentTemplate>
            <asp:HiddenField ID="hdnschemeId" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="hdnblockId" runat="server" ClientIDMode="Static" />
            <asp:HiddenField ID="hdnSession" runat="server" ClientIDMode="Static" />
            <div style="border: 1px solid black; height: 672px; padding: 5px;">
                <table id="dtCyclicalTab" style="width: 100%;">
                    <thead>
                        <tr>
                            <th>
                                ServiceItemId
                            </th>
                            <th>
                                Scheme
                            </th>
                            <th>
                                Block
                            </th>
                            <th>
                                Attribute
                            </th>
                            <th>
                                Cycle (Every)
                            </th>
                            <th>
                                Commencement
                            </th>
                            <th>
                                Recent Completion
                            </th>
                            <th>
                                Cycle Remaining
                            </th>
                            <th>
                                Next Cycle
                            </th>
                            <th>
                                Contractor
                            </th>                             
                             
                        </tr>
                    </thead>
                </table>
            </div>
            
        </ContentTemplate>
    </asp:UpdatePanel>
    </div>
</asp:Content>
