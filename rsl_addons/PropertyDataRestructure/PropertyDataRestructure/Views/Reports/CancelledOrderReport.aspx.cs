﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PropertyDataRestructure.Base;
using System.Web.Services;
using PDR_BusinessObject.CyclicalServices;
using System.Web.Script.Services;
using System.Data;
using PDR_BusinessLogic.CyclicalServices;
using PDR_DataAccess.CyclicalServices;

namespace PropertyDataRestructure.Views.Reports
{
    public partial class CancelledOrderReport : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hdnSession.Value = objSession.EmployeeId.ToString();

        }
        #region Populate Cyclical Tab
        /// <summary>    
        /// Populate Cyclical Tab    
        /// </summary>    
        /// <returns>Return data</returns>    
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object PopulateCancelledOrder()
        {
            // Initialization.    
            CancelledOrderBO response = new CancelledOrderBO();
            try
            {
                // Initialization.    
                string search = HttpContext.Current.Request.Params["search[value]"];
                string draw = HttpContext.Current.Request.Params["draw"];
                string orderId = HttpContext.Current.Request.Params["order[0][column]"];
                string order = HttpContext.Current.Request.Params["columns[" + orderId + "][data]"];
                string orderDir = HttpContext.Current.Request.Params["order[0][dir]"];
                int startRec = Convert.ToInt32(HttpContext.Current.Request.Params["start"]);
                int pageSize = Convert.ToInt32(HttpContext.Current.Request.Params["length"]);
                
                int take = 0, skip = 0;
                if (startRec == 0)
                {
                    take = pageSize * 1;
                    skip = startRec;
                }
                else
                {
                    take = pageSize * startRec;
                    skip = startRec + 1;
                }
                DataSet resultDataset = new DataSet();
                CyclicalServicesBL objBl = new CyclicalServicesBL(new CyclicalServicesRepo());
                int total = objBl.getCancelledOrder(ref resultDataset, search, skip, take, order, orderDir);
                response.draw = Convert.ToInt32(draw);
                response.recordsFiltered = total;
                response.recordsTotal = total;
                List<CancelledOrderDetailBO> data = new List<CancelledOrderDetailBO>();
                var schemeData = (from DataRow dr in resultDataset.Tables[0].Rows
                                  select new CancelledOrderDetailBO()
                                  {
                                      OrderId = dr["OrderId"].ToString(),
                                      CancelDate = dr["CancelDate"].ToString(),
                                      PONumber = dr["PONumber"].ToString(),
                                      CancelledBy = dr["CancelledBy"].ToString(),
                                      Reason = dr["Reason"].ToString(),
                                      Contractor = dr["Contractor"].ToString(),
                                      Block = dr["Block"].ToString(),
                                      Attribute = dr["Attribute"].ToString(),                                       
                                      Scheme = dr["Scheme"].ToString() 
                                  }).ToList();

                if (schemeData.Count > 0)
                {
                    response.data = schemeData;
                }
                else
                {
                    response.data = data;
                }

            }
            catch (Exception ex)
            {
                // Info    
                Console.Write(ex);
            }
            // Return info.    
            return response;
        }
        #endregion
    }
}