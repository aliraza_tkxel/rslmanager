﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PropertyDataRestructure.Base;

namespace PropertyDataRestructure.Views.Reports
{
    public partial class ReportArea : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                populateReports();
            }
        }

        #region "Populate Reports via Query string"

        public void populateReports()
        {
            if (Request.QueryString["rpt"] == "terrier")
            {
                objSession.ReportsBo = null;
                reportStructure.Visible = false;
                terriorReport.Visible = true ;
                terriorReport.populateData();               
            }
            else //if (Request.QueryString["rpt"] == "ne")
            {
                objSession.ReportsBo = null;
                reportStructure.Visible = true;
                terriorReport.Visible = false; 
               
            }
            

        }

        #endregion
    }
}