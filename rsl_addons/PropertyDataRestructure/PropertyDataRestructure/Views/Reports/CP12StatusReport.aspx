﻿<%@ Page Title="" Language="C#" Culture="en-GB" MasterPageFile="~/MasterPage/Pdr.Master"
    AutoEventWireup="true" CodeBehind="CP12StatusReport.aspx.cs" Inherits="PropertyDataRestructure.Views.Reports.CP12StatusReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<%@ Register TagPrefix="ucCP12StatusReport" TagName="VoidProperties" Src="~/UserControls/Reports/CP12Status.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        //setup before functions
        var typingTimer;                //timer identifier
        var doneTypingInterval = 1000;  //time in ms, 5 second for example

        function TypingInterval() {

            clearTimeout(typingTimer);

            typingTimer = setTimeout(searchTextChanged, doneTypingInterval);

        }

        function searchTextChanged() {
            __doPostBack('<%= txtSearch.ClientID %>', '');
        }
        
    </script>
    <style type="text/css">
        .DropDownPanel
        {
            margin-right: 20px;
        }
        .StatusDropDown
        {
            width: 255px;
            height: 22px;
        }
        .select_div
        {
            margin: -13.5px 0 0 0;
        }
        .dashboard th{
            background: #fff;
            border-bottom: 4px solid #8b8687;
        }
        .dashboard th a{
            color: #000 !important;
        }
        .dashboard th img{
            float:right;
        }
        .text_div input{
           width: 150px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="updateProgressReportsArea" runat="server" DisplayAfter="10">
        <ProgressTemplate>
            <div class="loading-image">
                <img alt="Please Wait" src="../../Images/menu/progress.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="updPanelReportArea" runat="server">
        <ContentTemplate>
            <div class="portlet">
                <div class="header pnlHeading">
                    <asp:Label ID="lblReportHeading" CssClass="header-label" Text="" runat="server" Font-Bold="true" />
                    <div class="field right">
                        <asp:Panel ID="pnlSearch" runat="server" HorizontalAlign="Right" Style="float: left;" Visible="True">
                            <div class="form-control">
                                <div class="select_div">
                                    <div class="field">
                                        <asp:DropDownList ID="ddlPropertyStatus" runat="server" AutoPostBack="true" CssClass="StatusDropDown"
                                            OnSelectedIndexChanged="ddlPropertyStatus_SelectedIndexChanged">
                                            <asp:ListItem Value="-1">Property Status</asp:ListItem>
                                            <asp:ListItem Value="Let">Let</asp:ListItem>
                                            <asp:ListItem Value="Available to rent">Available to Rent</asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <asp:TextBox ID="txtSearch" AutoPostBack="false" style="padding:3px 10px !important; margin:-4px 10px 0 0;" AutoCompleteType="Search" 
                                class="searchbox styleselect-control searchbox right" onkeyup="TypingInterval();" OnTextChanged="txtSearch_TextChanged" PlaceHolder="Quick find" runat="server">
                            </asp:TextBox>
                            <ajax:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
                                TargetControlID="txtSearch" WatermarkText="Quick find" WatermarkCssClass="searchbox searchText">
                            </ajax:TextBoxWatermarkExtender>
                        </asp:Panel>
                    </div>
                </div>
                <div class="portlet-body" style="font-size: 12px; padding-bottom:0;">
                    <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="500px" />
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                        <ContentTemplate>
                            <ucCP12StatusReport:VoidProperties ID="ucCP12StatusReport" runat="server">
                            </ucCP12StatusReport:VoidProperties>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
