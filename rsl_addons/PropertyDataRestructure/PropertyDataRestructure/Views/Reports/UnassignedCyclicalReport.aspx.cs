﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PropertyDataRestructure.Base;
using System.Data;
using PDR_BusinessLogic.CyclicalServices;
using PDR_BusinessObject.CyclicalServices;
using PDR_DataAccess.CyclicalServices;
using System.Web.Services;
using System.Web.Script.Services;

namespace PropertyDataRestructure.Views.Reports
{
    public partial class UnassignedCyclicalReport : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {            
            hdnSession.Value = objSession.EmployeeId.ToString();
            if ((Request.QueryString["sid"] != null))
            {
                hdnschemeId.Value = Request.QueryString["sid"].ToString();
            }
            else
            {
                hdnschemeId.Value = "0";
            }
        }

        #region Populate Cyclical Tab
        /// <summary>    
        /// Populate Cyclical Tab    
        /// </summary>    
        /// <returns>Return data</returns>    
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object PopulateUnassignedServices()
        {
            // Initialization.    
            CyclicalTabResponseBO response = new CyclicalTabResponseBO();
            try
            {
                // Initialization.    
                string search = HttpContext.Current.Request.Params["search[value]"];
                string draw = HttpContext.Current.Request.Params["draw"];
                string orderId = HttpContext.Current.Request.Params["order[0][column]"];
                string order = HttpContext.Current.Request.Params["columns[" + orderId + "][data]"];
                string orderDir = HttpContext.Current.Request.Params["order[0][dir]"];
                int startRec = Convert.ToInt32(HttpContext.Current.Request.Params["start"]);
                int pageSize = Convert.ToInt32(HttpContext.Current.Request.Params["length"]);
                int schemeId = 0;
                if (HttpContext.Current.Request.Params["schemeId"] != null)
                {
                    schemeId = Convert.ToInt32(HttpContext.Current.Request.Params["schemeId"]);
                }
                int take = 0, skip = 0;
                if (startRec == 0)
                {
                    take = pageSize * 1;
                    skip = startRec;
                }
                else
                {
                    take = pageSize * startRec;
                    skip = startRec + 1;
                }
                DataSet resultDataset = new DataSet();
                CyclicalServicesBL objBl = new CyclicalServicesBL(new CyclicalServicesRepo());
                int total = objBl.getUnassignedServices(ref resultDataset, search, skip, take, order, orderDir, 0, schemeId);
                response.draw = Convert.ToInt32(draw);
                response.recordsFiltered = total;
                response.recordsTotal = total;
                List<CyclicalTabDetailBO> data = new List<CyclicalTabDetailBO>();
                var schemeData = (from DataRow dr in resultDataset.Tables[0].Rows
                                  select new CyclicalTabDetailBO()
                                  {
                                      ServiceRequired = dr["ServiceRequired"].ToString(),
                                      ServiceItemId = (dr["ServiceItemId"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ServiceItemId"])),
                                      Cycle = dr["CYCLE"].ToString(),
                                      RemainingCycle = (dr["RemainingCycle"] == DBNull.Value ? 0 : Convert.ToInt32(dr["RemainingCycle"])),
                                      BlockName = dr["BlockName"].ToString(),
                                      Contractor = dr["Contractor"].ToString(),
                                      PORef = dr["PORef"].ToString(),
                                      RecentCompletion = dr["RecentCompletion"].ToString(),
                                      NextCycle = dr["NextCycle"].ToString(),
                                      SchemeName = dr["SchemeName"].ToString(),
                                      Commencement = dr["Commencement"].ToString()
                                  }).ToList();

                if (schemeData.Count > 0)
                {
                    response.data = schemeData;
                }
                else
                {
                    response.data = data;
                }

            }
            catch (Exception ex)
            {
                // Info    
                Console.Write(ex);
            }
            // Return info.    
            return response;
        }
        #endregion
    }
}