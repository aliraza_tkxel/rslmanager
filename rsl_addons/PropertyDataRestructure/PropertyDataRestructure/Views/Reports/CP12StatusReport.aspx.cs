﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_BusinessObject.PageSort;
using PDR_Utilities.Helpers;
using PDR_Utilities.Constants;
using PDR_BusinessLogic.VoidInspections;
using System.Data;
using PropertyDataRestructure.Base;

namespace PropertyDataRestructure.Views.Reports
{
    public partial class CP12StatusReport : PageBase
    {
        #region Page load event
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                uiMessage.hideMessage();
                if (!IsPostBack)
                {
                    objSession.SearchText = null;
                    objSession.PropertyStatus = null;

                }
                else
                {
                    objSession.SearchText = txtSearch.Text;
                    objSession.PropertyStatus = ddlPropertyStatus.SelectedValue;
                }
                populateReports();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion


        #region "Populate Reports"

        public void populateReports()
        {
            ucCP12StatusReport.loadData();
            ucCP12StatusReport.Visible = true;
            lblReportHeading.Text = ApplicationConstants.CP12StatusReport;
        }

        #endregion



        protected void ddlPropertyStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            string propertyStatus= ddlPropertyStatus.SelectedValue;
            objSession.PropertyStatus = propertyStatus;
            //loadDocuments();

        }

        #region txtSearchBox TextChanged
        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {

                //searchResults();

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion
 
    }
}