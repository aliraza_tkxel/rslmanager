﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PropertyDataRestructure.Base;
using PDR_Utilities.Constants;

namespace PropertyDataRestructure.Views.Reports
{
    public partial class VoidReportArea : PageBase
    {
        #region Page load event
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                uiMessage.hideMessage();
                if (!IsPostBack)
                {
                    objSession.SearchText = null;
                    populateReports();
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region txtSearchBox TextChanged
        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {

                searchResults();

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region Functions
        #region Search Results
        public void searchResults()
        {
            objSession.SearchText = txtSearch.Text.Trim();
            if (Request.QueryString["rpt"] == ApplicationConstants.NoEntry)
            {
                ucNoEntryReport.searchData();
                ucNoEntryReport.Visible = true;
                ucVoidProperties.Visible = false;
            }
            else if (Request.QueryString["rpt"] == ApplicationConstants.PendingTermination || Request.QueryString["rpt"] == ApplicationConstants.ReletDue || Request.QueryString["rpt"] == ApplicationConstants.AvailableProperties)
            {
                ucVoidProperties.searchData();
                ucVoidProperties.Visible = true;
                ucNoEntryReport.Visible = false;
            }

        }
        #endregion

        #region "Populate Reports via Query string"

        public void populateReports()
        {

            if (Request.QueryString["rpt"] == ApplicationConstants.NoEntry)
            {
                ucNoEntryReport.loadData();
                ucNoEntryReport.Visible = true;
                ucVoidProperties.Visible = false;
                lblReportHeading.Text = ApplicationConstants.NoEntryHeading;
            }
            else if (Request.QueryString["rpt"] == ApplicationConstants.PendingTermination || Request.QueryString["rpt"] == ApplicationConstants.ReletDue || Request.QueryString["rpt"] == ApplicationConstants.AvailableProperties)
            {
                ucVoidProperties.loadData();
                ucVoidProperties.Visible = true;
                ucNoEntryReport.Visible = false;
                if (Request.QueryString["rpt"] == ApplicationConstants.PendingTermination)
                {
                    lblReportHeading.Text = ApplicationConstants.PendingTerminationHeading;
                }
                else if (Request.QueryString["rpt"] == ApplicationConstants.ReletDue)
                {
                    lblReportHeading.Text = ApplicationConstants.ReletDueHeading;
                }
                else if (Request.QueryString["rpt"] == ApplicationConstants.AvailableProperties)
                {
                    lblReportHeading.Text = ApplicationConstants.AvailablePropertiesHeading;
                }
            }



        }

        #endregion
        #endregion
    }
}