﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PDR_Utilities.Constants;
using PDR_BusinessLogic.SchemeBlock;
using System.Data;
using PDR_DataAccess.SchemeBlock;
using PDR_DataAccess.Reports;
using PDR_BusinessLogic.Reports;
using PropertyDataRestructure.Base;
using PDR_BusinessObject.PageSort;
using PDR_Utilities.Helpers;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace PropertyDataRestructure.Views.Reports
{
    public partial class ComplianceDocuments : PageBase
    {
        PageSortBO objPageSortBo = new PageSortBO("DESC", "Type", 1, 30);
        public int rowCount = 0;
        public bool isViewEnabled = false; 

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                populateDropDowns();
                loadDocuments();
            }
            else
            {
                //showActiveTab();
            }
        }

        private void populateDropDowns()
        {
            /*Populate Type DropDown*/
            DocumentsBL objDocumentTypeBL = new DocumentsBL(new DocumentsRepo());
            DataSet resultDataSet = new DataSet();
            
            try
            {
                loadCategory();
                loadDocumentTypes();
                //resultDataSet = objDocumentTypeBL.getDocumentTypesList(ddlReportFor.SelectedItem.Text, Convert.ToInt32(ddlCategory.SelectedItem.Value));
                //ddlType.DataSource = resultDataSet.Tables[0];
                //ddlType.DataValueField = "DocumentTypeId";
                //ddlType.DataTextField = "Title";
                //ddlType.DataBind();
                //ddlType.Items.FindByValue("-1").Text = "Select Document Type";

                /*Populate Title DropDown with #Select Document Title# */
                ListItem newListItem = default(ListItem);
                newListItem = new ListItem("Select Document Title", "-1");
                ddlTitle.Items.Insert(0, newListItem);
            }
            catch (Exception ex)
            {

            }


        }
        private void loadDocumentTypes()
        {
            DocumentsBL objDocumentTypeBL = new DocumentsBL(new DocumentsRepo());
            DataSet resultDataSet = new DataSet();
            resultDataSet = objDocumentTypeBL.getDocumentTypesList(ddlReportFor.SelectedItem.Text, Convert.ToInt32(ddlCategory.SelectedItem.Value));
            ddlType.DataSource = resultDataSet.Tables[0];
            ddlType.DataValueField = "DocumentTypeId";
            ddlType.DataTextField = "Title";
            ddlType.DataBind();
            ddlType.Items.FindByValue("-1").Text = "Select Document Type";

        }

      

        #region "Load Development/Scheme/Property DataSets"
        private void loadDevDocuments()
        {

        }
        #endregion


        #region Show Tabs Code

        //private void showDevDocsTab()
        //{
        //    lnkBtnDevDocsTab.CssClass = ApplicationConstants.TabClickedCssClass;
        //    lnkBtnSchemeDocsTab.CssClass = ApplicationConstants.TabInitialCssClass;
        //    lnkBtnPropDocsTab.CssClass = ApplicationConstants.TabInitialCssClass;
        //    MainView.ActiveViewIndex = 0;
        //    ucDevDocsTab.loadData();
        //}
        //private void showSchemeDocsTab()
        //{
        //    lnkBtnDevDocsTab.CssClass = ApplicationConstants.TabInitialCssClass;
        //    lnkBtnSchemeDocsTab.CssClass = ApplicationConstants.TabClickedCssClass;
        //    lnkBtnPropDocsTab.CssClass = ApplicationConstants.TabInitialCssClass;
        //    MainView.ActiveViewIndex = 1;
        //    ucSchemeDocsTab.loadData();
        //}
        //private void showPropDocsTab()
        //{
        //    lnkBtnDevDocsTab.CssClass = ApplicationConstants.TabInitialCssClass;
        //    lnkBtnSchemeDocsTab.CssClass = ApplicationConstants.TabInitialCssClass;
        //    lnkBtnPropDocsTab.CssClass = ApplicationConstants.TabClickedCssClass;
        //    MainView.ActiveViewIndex = 2;
        //    ucPropDocsTab.loadData();
        //}

        //private void showActiveTab()
        //{
        //    //Check which Tab is Active. Then call corresponding's loadData Method.
        //    if (MainView.ActiveViewIndex == 0)
        //    {

        //        showDevDocsTab();
        //    }
        //    else if (MainView.ActiveViewIndex == 1)
        //    {
        //        showSchemeDocsTab();
        //    }
        //    else
        //    {
        //        showPropDocsTab();
        //    }
        //}
        #endregion


        #region "ddlType & ddlTitle and other Filter Events"

        protected void BindTitleDdl(int DocumentTypeId)
        {
            DocumentsBL objDocumentTypeBL = new DocumentsBL(new DocumentsRepo());
            DataSet resultDataSet = new DataSet();
            resultDataSet = objDocumentTypeBL.getDocumentSubtypesList(DocumentTypeId, ddlReportFor.SelectedItem.Text);
            ddlTitle.DataSource = resultDataSet.Tables[0];
            ddlTitle.DataValueField = "DocumentSubtypeId";
            ddlTitle.DataTextField = "Title";
            ddlTitle.DataBind();
            ddlTitle.Items.FindByValue("-1").Text = "Select Document Title";
        }

        protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            int documentTypeId = Convert.ToInt32(ddlType.SelectedValue);
            objSession.Type = documentTypeId;
            if (documentTypeId == -1)
            {
                ddlTitle.Items.Clear(); // Instead of clearing it, add only
                ListItem newListItem = default(ListItem);
                newListItem = new ListItem("Select Document Title", "-1");
                ddlTitle.Items.Insert(0, newListItem);
                objSession.Title = null;
            }
            else
            {
                BindTitleDdl(documentTypeId);
            }
            //loadDocuments();

        }
        //protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    string reportFor = ddlReportFor.SelectedItem.Text;

        //}

        protected void ddlTitle_SelectedIndexChanged(object sender, EventArgs e)
        {
            int documentTitleId = Convert.ToInt32(ddlTitle.SelectedValue);
            objSession.Title = documentTitleId.ToString();//Convert.ToString(ddlTitle.SelectedItem);
            loadDocuments();

        }

        protected void ddlCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadDocumentTypes();
            loadDocuments();
        }

        private void loadCategory()
        {
            string reportFor = ddlReportFor.SelectedItem.Text;
            ReportsBL objReportsBl = new ReportsBL(new ReportsRepo());
            DataSet resultset = new DataSet();

            objReportsBl.getCategories(ref resultset, reportFor);
            ddlCategory.DataSource = resultset;
            ddlCategory.DataValueField = "categorytypeid";
            ddlCategory.DataTextField = "title";
            ddlCategory.DataBind();
        }

        protected void ddlReportFor_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadCategory();
            loadDocumentTypes();
            loadDocumentTypes();
        }

        protected void rblDateType_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadDocuments();
        }

        protected void rblUploadedOrNot_SelectedIndexChanged(object sender, EventArgs e)
        {
            loadDocuments();
        }

        protected void txtDocumentDate_TextChanged(object sender, EventArgs e)
        {
            DateTime fromDate, toDate;
            if (txtFromDate.Text.Trim().Length != 0 || txtToDate.Text.Trim().Length != 0)
            {
                if ((DateTime.TryParse(txtFromDate.Text, out fromDate)).Equals(false)
                    || (DateTime.TryParse(txtToDate.Text, out toDate).Equals(false)))
                {
                    lblDateFormatError.Text = UserMessageConstants.DateInvalid;
                }
                else
                {
                    lblDateFormatError.Text = String.Empty;
                    //loadDocuments();
                }
            }
            else
            {
                lblDateFormatError.Text = String.Empty;
                loadDocuments();
            }

        }

        protected void chkBoxExpired_CheckedChanged(object sender, EventArgs e)
        {
            //bool isExpired = chkBoxExpired.Checked;
            //objSession.Expiry = isExpired;
        }

        protected void btnSearch_Clicked(object sender, EventArgs e)
        {
            //objPageSortBo.PageNumber = 1;
            pageSortViewState.PageNumber = 1;
            loadDocuments();

        }

        protected void loadDocuments()
        {
            DateTime tempFromDate, tempToDate;
            if (txtFromDate.Text.Trim().Length != 0 || txtToDate.Text.Trim().Length != 0)
            {
                if ((DateTime.TryParse(txtFromDate.Text, out tempFromDate)).Equals(false)
                    || (DateTime.TryParse(txtToDate.Text, out tempToDate).Equals(false)))
                {
                    lblDateFormatError.Text = UserMessageConstants.DateInvalid;
                    return;
                }
                else
                {
                    lblDateFormatError.Text = String.Empty;

                }
            }
            else
            {
                lblDateFormatError.Text = String.Empty;
            }

            if (rblUploadedOrNot.SelectedValue == "Uploaded")
            {
                isViewEnabled = true;
            }
            //Get Filter Values
            string reportFor = ddlReportFor.SelectedValue;
            string category = ddlCategory.SelectedItem.Text;
            int type = int.Parse(ddlType.SelectedValue);
            string strTitle = ddlTitle.SelectedItem.Text;
            int intTitle = -1;
            if (type != -1)
            {
                try
                {
                    intTitle = int.Parse(ddlTitle.SelectedValue);
                }
                catch (Exception ex) //In case, if Type don't have any Sub Types. Then -1 will be sent.
                {
                }

            }


            string dateType = rblDateType.SelectedValue;
            string fromDate = txtFromDate.Text.Trim();
            string toDate = txtToDate.Text.Trim();
            string uploadedOrNot = rblUploadedOrNot.SelectedValue;
            string searchedText = txtQuickFind.Text.Trim();

            PageSortBO objPageSortBo;
            if (pageSortViewState == null)
            {
                objPageSortBo = new PageSortBO("DESC", "Type", 1, 30);
                pageSortViewState = objPageSortBo;
            }
            else
            {
                objPageSortBo = pageSortViewState;
            }

            int totalCount = 0;
            ReportsBL objReportsBl = new ReportsBL(new ReportsRepo());
            DataSet resultDataSet = new DataSet();
            totalCount = objReportsBl.getDocuments(ref resultDataSet, objPageSortBo, reportFor, category, type, intTitle, strTitle, dateType, fromDate, toDate, uploadedOrNot, searchedText, false);
            grdDocuments.DataSource = resultDataSet;
            grdDocuments.DataBind();
            objPageSortBo.TotalRecords = totalCount;
            objPageSortBo.TotalPages = Convert.ToInt32(Math.Ceiling((Double)totalCount / objPageSortBo.PageSize));

            if (objPageSortBo.TotalPages > 0)
            {
                pnlPagination.Visible = true;
            }
            else
            {
                pnlPagination.Visible = false;
                //uiMessage.showErrorMessage(UserMessageConstants.RecordNotFound);
            }

            if (resultDataSet.Tables[0].Rows.Count < 1)
            {
                uiMessage.showErrorMessage(UserMessageConstants.RecordNotFound);
            }
            else
            {
                uiMessage.hideMessage();
            }
            pageSortViewState = objPageSortBo;
            GridHelper.setGridViewPager(ref pnlPagination, objPageSortBo);


        }
        #endregion

        #region GridView Sorting
        protected void grdDocuments_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                objPageSortBo = pageSortViewState;

                objPageSortBo.SortExpression = e.SortExpression;
                objPageSortBo.PageNumber = 1;
                grdDocuments.PageIndex = 0;
                objPageSortBo.setSortDirection();

                pageSortViewState = objPageSortBo;
                loadDocuments();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region "Pagination Controls"
        protected void lnkbtnPager_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton lbPager = (LinkButton)sender;
                if (lbPager.CommandName.Equals("Page") && lbPager.CommandArgument.Equals("First"))
                {
                    objPageSortBo.PageNumber = 1;
                }
                else if (lbPager.CommandName.Equals("Page") && lbPager.CommandArgument.Equals("Last"))
                {
                    objPageSortBo.PageNumber = objPageSortBo.TotalPages;
                }
                else if (lbPager.CommandName.Equals("Page") && lbPager.CommandArgument.Equals("Next"))
                {
                    objPageSortBo.PageNumber += 1;
                }
                else if (lbPager.CommandName.Equals("Page") && lbPager.CommandArgument.Equals("Prev"))
                {
                    objPageSortBo.PageNumber -= 1;
                }
                objPageSortBo.PageNumber -= 1;

                //setPageSortBoViewState(objPageSortBo);
                pageSortViewState = GridHelper.processGridPager(sender, objPageSortBo, pageSortViewState);
                loadDocuments();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region "Export to Excel"

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            try
            {
                uiMessage.hideMessage();
                exportGridToExcel();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        public void exportGridToExcel()
        {
            //Get Filter Values for Exporting the Result Set
            string reportFor = ddlReportFor.SelectedValue;
            string category = ddlCategory.SelectedItem.Text;
            int type = int.Parse(ddlType.SelectedValue);
            string strTitle = ddlTitle.SelectedItem.Text;
            int intTitle = -1;
            if (type == -1)
            {
                intTitle = int.Parse(ddlTitle.SelectedValue);
            }

            string dateType = rblDateType.SelectedValue;
            string fromDate = txtFromDate.Text.Trim();
            string toDate = txtToDate.Text.Trim();
            string uploadedOrNot = rblUploadedOrNot.SelectedValue;
            string searchedText = txtQuickFind.Text.Trim();

            PageSortBO objPageSortBo;
            if (pageSortViewState == null)
            {
                objPageSortBo = new PageSortBO("DESC", "Type", 1, 65000);
                pageSortViewState = objPageSortBo;
            }
            else
            {
                objPageSortBo = pageSortViewState;
                objPageSortBo.PageNumber = 1;
                objPageSortBo.PageSize = 65000;
            }

            int totalCount = 0;
            ReportsBL objReportsBl = new ReportsBL(new ReportsRepo());
            DataSet resultDataSet = new DataSet();
            totalCount = objReportsBl.getDocuments(ref resultDataSet, objPageSortBo, reportFor, category, type, intTitle, strTitle, dateType, fromDate, toDate, uploadedOrNot, searchedText, false);


            DataTable dt = resultDataSet.Tables[0];

            dt.Columns["Added"].ColumnName = "Added:";
            dt.Columns["Type"].ColumnName = "Type:";
            dt.Columns["Title"].ColumnName = "Title:";
            dt.Columns["By"].ColumnName = "By:";
            dt.Columns["Document"].ColumnName = "Document:";
            dt.Columns["Expiry"].ColumnName = "Expiry:";
            dt.Columns["Address"].ColumnName = "Address:";
            try
            {
                dt.Columns.Remove("SortWithBy");
                dt.Columns.Remove("AddedSort");
                dt.Columns.Remove("DocumentSort");
                dt.Columns.Remove("ExpirySort");
                dt.Columns.Remove("DocumentId");
                dt.Columns.Remove("row");
            }
            catch
            {
            }

            string fileName = reportFor + "Report_" + Convert.ToString(DateTime.Now.Day) + "_" + Convert.ToString(DateTime.Now.Month) + "_" + Convert.ToString(DateTime.Now.Year);
            ExportDownloadHelper.exportGridToExcel(fileName, dt);
        }



        #endregion

        #region "Uploaded & NotUploaded Buttons Click Events"

        protected void btnUploaded_Clicked(Object sender, EventArgs e)
        {
            //string type = ddlType.SelectedItem.ToString();
            //string title = ddlType.SelectedItem.ToString();
            //int by = int.Parse(ddlType.SelectedValue); 
            //string documentDate = txtDocumentDate.Text;


            //objSession.Document = txtDocumentDate.Text;
            //objSession.Expiry = chkBoxExpired.Checked;

            //Remove search text
            objSession.SearchText = null;


            //if(chkBoxExpired.Checked){
            //    isExpired = true;
            //}

            //Check which Tab is Active. Then call corresponding's loadData Method.
            //if (MainView.ActiveViewIndex == 0)
            //{

            //    ucDevDocsTab.loadData();
            //}
            //else if (MainView.ActiveViewIndex == 1)
            //{
            //    ucSchemeDocsTab.loadData();
            //}
            //else
            //{
            //    //ucPropDocsTab.loadData();
            //}

        }

        protected void btnNotUploaded_Clicked(Object sender, EventArgs e)
        {

        }

        protected void imgSearchbtn_Clicked(Object sender, EventArgs e)
        {
            //Remove other session values.
            objSession.Type = 0;
            objSession.Title = null;
            objSession.By = -1;
            objSession.Document = null;
            objSession.Expiry = false;

            //objSession.SearchText = txtSearchBox.Text.Trim();

            //if (MainView.ActiveViewIndex == 0)
            //{

            //    ucDevDocsTab.loadData();
            //}
            //else if (MainView.ActiveViewIndex == 1)
            //{
            //    //ucSchemeDocsTab.loadData();
            //}
            //else
            //{
            //    //ucPropDocsTab.loadData();
            //}
        }
        #endregion
    }
}