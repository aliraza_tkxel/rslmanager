﻿<%@ Page Title="" Language="C#" AutoEventWireup="True" MasterPageFile="~/MasterPage/Blank.Master"
    CodeBehind="RentAccountSummary.aspx.cs" Inherits="PropertyDataRestructure.Views.Reports.RentAccountSummary" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <meta http-equiv="X-UA-Compatible" content="IE=E10" />
    <link href="../../Styles/normalize.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        function PrintStatement() {
            var button = document.getElementById('PrintButton');
            button.hidden = true;
            button.style.display = 'none';
            window.print();
        }
    </script>
    <style type="text/css" media="print">
        .summary-table
        {
            border-collapse: collapse;
            font: 12PT ARIAL;
            width: 100%;
            border-spacing: -1px !important;
        }
    </style>
    <style type="text/css">
        .span-block
        {
            text-align: center;
            display: inline-block;
            min-width: 110px;
            border: 2px solid black;
            font-weight: bold;
            padding: 6px;
            font: 12PT ARIAL;
        }
        .tr-color:nth-child(odd)
        {
            background-color: #b3ffff;
        }
        
        td
        {
            font: 12PT ARIAL;
        }
        
        .background-image
        {
            margin: 2px;
            float: left;
            color: white;
            width: 225px;
            height: 29px;
            padding-top: 7px;
            padding-left: 14px;
            font: 12PT ARIAL;
            background-repeat: no-repeat;
            background-size: 100% 100%;
        }
        .summary-table
        {
            border-collapse: collapse;
            font: 12PT ARIAL;
            width: 100%;
            border-spacing: -1px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="ContentPlaceHolder1">
    <div style='margin-top: 0px !important;'>
        <p>
        </p>
        <table border="0" width="100%">
            <tbody>
                <tr>
                    <td style='font: 12PT ARIAL'>
                        <div>
                            <button clientidmode="Static" id="PrintButton" onclick="PrintStatement();">
                                Print Statement</button>
                        </div>
                        <div id='return_address' style='overflow: hidden; margin-bottom: 4px;'>
                            <div style='font: 12PT ARIAL; float: left; padding-top: 70px;'>
                                Your Account Summary
                            </div>
                            <div style='float: right;'>
                                &nbsp;
                                <img src='../../Images/RentAccount/BHALOGOLETTER.gif' style='text-align: right; width: 138px;
                                    height: 117px;' /></div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                        <table width='100%' id="tblCustomerInfo" runat="server">
                            <tr>
                                <td style='font: 12PT ARIAL; padding-left: 20mm; padding-top: 10px'>
                                    <span id="customerName" runat="server"></span>
                                </td>
                            </tr>
                            <tr>
                                <td style='font: 12PT ARIAL; padding-left: 20mm;'>
                                    <span id="address1" runat="server"></span>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 55%">
                                    <table id="Table1" runat="server">
                                        <tr>
                                            <td style='font: 12PT ARIAL; padding-left: 20mm;'>
                                                <span id="address2" runat="server"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style='font: 12PT ARIAL; padding-left: 20mm;'>
                                                <span id="townCity" runat="server"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style='font: 12PT ARIAL; padding-left: 20mm;'>
                                                <span id="county" runat="server"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style='font: 12PT ARIAL; padding-left: 20mm;'>
                                                <span id="postCode" runat="server"></span>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td>
                                    <table id="Table2" runat="server">
                                        <tr>
                                            <td colspan="2" style='text-align: right; padding-left: 17mm'>
                                                <span style='width: 150px; font: 12PT ARIAL'>Your Tenancy Ref: </span>
                                            </td>
                                            <td colspan="1" style='text-align: right; font: 12PT ARIAL;'>
                                                <span id="tenancyIdRef" runat="server"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style='text-align: right; font: 12PT ARIAL'>
                                                Date:
                                            </td>
                                            <td colspan="1" style='font: 12PT ARIAL'>
                                                <span style="margin-left: 5px;" id="currentDate" runat="server"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style='text-align: right; font: 12PT ARIAL'>
                                                Balance as at:
                                            </td>
                                            <td colspan="1" style='font: 12PT ARIAL'>
                                                <span id="balanceAt" runat="server" style="margin-left: 5px;"></span>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr height="31px">
                </tr>
                <tr>
                    <td style='font: 12PT ARIAL'>
                        <div style='width: 62%; float: left'>
                            <div style='width: 100%'>
                                <div style='width: 100%; float: left; margin-top: 33px;'>
                                    <div class="background-image" style='background-image: url(../../Images/RentAccount/arrow_icon_red.png);'>
                                        Rent Account Balance
                                    </div>
                                    <div style='margin-left: 10px; float: left; padding: 3px;'>
                                        <asp:Label ID="lblRAB" runat="server" class="span-block" />
                                    </div>
                                </div>
                                <div style='width: 100%; float: left;'>
                                    <div class="background-image" style='background-image: url(../../Images/RentAccount/arrow_icon_green.png)'>
                                        Estimated Housing Benefit
                                    </div>
                                    <div style='margin-left: 10px; float: left; padding: 3px;'>
                                        <asp:Label ID="lblEHB" runat="server" class="span-block" />
                                    </div>
                                </div>
                                <div style='width: 100%; float: left;'>
                                    <div class="background-image" style='background-image: url(../../Images/RentAccount/arrow_icon_red.png);'>
                                        <div>
                                            <span style="display: inline-block;">To Pay </span><span style="display: inline-block;
                                                font: 10PT ARIAL; position: absolute; margin-top: -5px; margin-left: 35px">(After
                                                Estimated<br />
                                                Housing Benefit)</span>
                                        </div>
                                    </div>
                                    <div style='margin-left: 10px; float: left; padding: 3px;'>
                                        <asp:Label ID="lblToPay" runat="server" class="span-block" />
                                    </div>
                                </div>
                                <div style='width: 100%; float: left;'>
                                    <div class="background-image" style='background-image: url(../../Images/RentAccount/arrow_icon_red.png);'>
                                        Recharge Account Balance
                                    </div>
                                    <div style='margin-left: 10px; float: left; padding: 3px;'>
                                        <asp:Label ID="lblRechargeAccountBalance" class="span-block" runat="server" />
                                    </div>
                                </div>
                                <div style='width: 100%; float: left; font: 12PT ARIAL; margin-top: 8px; margin-bottom: 10px;'>
                                    Recent Rent Account Transactions:
                                </div>
                                <div style="width: 99%">
                                    <div style="text-align: center" id="NoRecentTranscation" runat="server" visible="False">
                                        <strong>No Recent Rent Account Transactions</strong>
                                    </div>
                                    <asp:Repeater ID="rentAccountTransactionRpt" runat="server">
                                        <HeaderTemplate>
                                            <table cellspacing="0" cellpadding="0" class="summary-table">
                                                <tr>
                                                    <td align='center' style='font: 10PT ARIAL; padding: 2px; text-align: left; border: 1px solid black;'
                                                        valign='top'>
                                                        Date
                                                    </td>
                                                    <td align='center' style='font: 10PT ARIAL; padding: 2px; text-align: left; border: 1px solid black;'
                                                        valign='top'>
                                                        Type
                                                    </td>
                                                    <td align='center' style='font: 10PT ARIAL; padding: 2px; text-align: left; border: 1px solid black;'
                                                        valign='top'>
                                                        Money Due
                                                    </td>
                                                    <td align='center' style='font: 10PT ARIAL; padding: 2px; text-align: left; border: 1px solid black;'
                                                        valign='top'>
                                                        Money Received
                                                    </td>
                                                    <td align='center' style='font: 10PT ARIAL; padding: 2px; text-align: left; border: 1px solid black;'
                                                        valign='top'>
                                                        Balance
                                                    </td>
                                                </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr class="tr-color">
                                                <td style='font: 10PT ARIAL; text-align: center'>
                                                    <asp:Label runat="server" ID="Label1" Text='<%# Eval("F_TRANSACTIONDATE_TEXT") %>' />
                                                </td>
                                                <td style='font: 10PT ARIAL; text-align: left; padding-left: 1px'>
                                                    <asp:Label runat="server" ID="Label2" Text='<%# Eval("PAYMENTTYPE").ToString().Length > 1 ? Eval("PAYMENTTYPE") : Eval("ITEMTYPE") %>' />
                                                </td>
                                                <td style='font: 10PT ARIAL; text-align: center;'>
                                                    <asp:Label runat="server" ID="Label3" Text='<%# Eval("DEBIT") %>' />
                                                </td>
                                                <td style='font: 10PT ARIAL; text-align: center;'>
                                                    <asp:Label runat="server" ID="Label4" Text='<%# Eval("CREDIT") %>' />
                                                </td>
                                                <td style='font: 10PT ARIAL; text-align: center'>
                                                    <asp:Label runat="server" ID="Label5" Text='<%# "£" + Eval("BREAKDOWN") %>' />
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <tr id="trBlanceForward" style="border: 1px solid black;" runat="server">
                                                <td colspan="4" style="padding: 5px; font: 10PT ARIAL;">
                                                    Balance carried forward
                                                    <asp:Label ID="lblBalanceForwardDate" runat="server"></asp:Label>
                                                </td>
                                                <td colspan="1" style="text-align: center; padding: 5px; font: 10PT ARIAL;">
                                                    <asp:Label ID="lblBalanceForward" runat="server"></asp:Label>
                                                </td>
                                            </tr>
                                            </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </div>
                                <div style="margin-top: 18px;">
                                    <span style='font: 12PT ARIAL'><strong>Lets talk ... </strong></span>
                                    <br />
                                    <span style='font: 12PT ARIAL; float: left;'>If you are struggling to pay your rent
                                        or need assistance, please get in touch with me. I’m here for you:<br />
                                        <span id="IncomeOfficerName" runat="server"></span>,<i> Income Recovery Officer</i>
                                    </span>
                                    <br />
                                    <table id="tblIncomeOfficer" style='font: 12PT ARIAL;' runat="server">
                                        <tr>
                                            <td>
                                                Tel:
                                            </td>
                                            <td>
                                                <span id="IncomeOfficerTel" runat="server"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Mobile:
                                            </td>
                                            <td>
                                                <span id="IncomeOfficerMobile" runat="server"></span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Email:
                                            </td>
                                            <td>
                                                <span id="IncomeOfficerEmail" runat="server"></span>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div style='width: 35%; font: 12PT ARIAL !important; vertical-align: top; float: left;
                            border-left: 1px solid black;'>
                            <div style='width: 100%; margin-left: 4px;'>
                                <div style='width: 100%; float: left; margin-left: 4px; line-height: 20px; font: 12PT ARIAL;'>
                                    <span style='font: 12PT ARIAL; display: block; padding-bottom: 9px; padding-top: 2px'>
                                        Ways to pay us</span> <span style="font: 12PT ARIAL;"><b>Login to Tenants Online:</b></span><br />
                                    <span>tenantsonline.broadlandgroup.org</span><br />
                                    <span style="margin-top: 5.5px; display: block; font: 11PT ARIAL;"><b>Direct Debit:</b>
                                        Call 0303 303 0003</span> <span style="margin-top: 5.5px; display: block; font: 11PT ARIAL;">
                                            <b>Standing Order/Online Banking:</b><br />
                                        </span>Sort code: 60-15-31<br />
                                    Account: 21481229<br />
                                    Quote Ref: <b id="quoteRef" runat="server"></b>
                                    <br />
                                    <span style="margin-top: 5.5px; display: block">For alternative payment methods<br />
                                        please contact us.<br />
                                    </span>
                                </div>
                                <div>
                                    <div style='width: 100%; float: left; margin-top: 4px; margin-left: 4px;'>
                                        <span style='font: 12PT ARIAL; float: left; display: block; padding-bottom: 4px;
                                            padding-top: 10px'>How to get in touch:</span>
                                    </div>
                                    <div style='width: 100%; float: left; margin-top: 10px; margin-left: 3px;'>
                                        <div style='width: 9%; margin-right: 6.5px; float: left; border: 0px;'>
                                            <img height='29px' src='../../Images/RentAccount/iconlogin.png' width='29px' /></div>
                                        <div style='width: 80%; margin-left: 4px; float: left;'>
                                            <span style='font: 12PT ARIAL;'>Login to Tenants Online<br />
                                            </span><span style='font: 11PT ARIAL;'>tenantsonline.broadlandgroup.org</span>
                                        </div>
                                    </div>
                                    <div style='width: 100%; float: left; margin-top: 6px; margin-left: 3px;'>
                                        <div style='width: 9%; margin-right: 6.5px; float: left; border: 0px;'>
                                            <img height='29px' src='../../Images/RentAccount/iconTel.png' width='29px' /></div>
                                        <div style='width: 80%; margin-left: 4px; float: left;'>
                                            <span style='font: 12PT ARIAL;'>Call us<br />
                                                0303 303 0003</span>
                                        </div>
                                    </div>
                                    <div style='width: 100%; float: left; margin-top: 6px; margin-left: 3px;'>
                                        <div style='width: 9%; margin-right: 6.5px; float: left; border: 0px;'>
                                            <img height='29px' src='../../Images/RentAccount/iconMail.png' width='29px' /></div>
                                        <div style='width: 80%; margin-left: 4px; float: left;'>
                                            <span style='font: 12PT ARIAL;'>Email us<br />
                                                rent@broadlandgroup.org</span>
                                        </div>
                                    </div>
                                    <div style='width: 100%; float: left; margin-top: 6px; margin-left: 3px;'>
                                        <div style='width: 9%; margin-right: 6.5px; float: left; border: 0px;'>
                                            <img height='29px' src='../../Images/RentAccount/iconPC.png' width='29px' /></div>
                                        <div style='width: 80%; margin-left: 4px; float: left;'>
                                            <span style='font: 12PT ARIAL;'>Visit us online<br />
                                                www.broadlandgroup.org</span>
                                        </div>
                                    </div>
                                    <div style='width: 100%; float: left; margin-top: 6px; margin-left: 3px;'>
                                        <div style='width: 9%; margin-right: 6.5px; float: left; border: 0px;'>
                                            <img height='29px' src='../../Images/RentAccount/iconFB.png' width='29px' /></div>
                                        <div style='width: 80%; margin-left: 4px; float: left;'>
                                            <span style='font: 12PT ARIAL;'>Find us on Facebook<br />
                                                @Broadland</span>
                                        </div>
                                    </div>
                                    <div style='width: 100%; float: left; margin-top: 6px; margin-left: 3px;'>
                                        <div style='width: 9%; margin-right: 6.5px; float: left; border: 0px;'>
                                            <img height='29px' src='../../Images/RentAccount/iconTwt.png' width='29px' /></div>
                                        <div style='width: 80%; margin-left: 4px; float: left;'>
                                            <span style='font: 12PT ARIAL;'>Find us on Twitter<br />
                                                @BroadlandHsg</span>
                                        </div>
                                    </div>
                                    <div style='width: 100%; float: left; margin-top: 6px; margin-left: 3px;'>
                                        <div style='width: 9%; margin-right: 6.5px; float: left; border: 0px;'>
                                            <img height='29px' src='../../Images/RentAccount/iconEdit.png' width='29px' /></div>
                                        <div style='width: 80%; margin-left: 4px; float: left;'>
                                            <span style='font: 12PT ARIAL;'>Write to us at<br />
                                                Broadland Housing Group<br />
                                                NCFC<br />
                                                Carrow Road<br />
                                                Norwich<br />
                                                NR1 1HU</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
        <p style='page-break-after: always'>
        </p>
        <br />
        <br />
        <br />
        <table border="0" width="100%" style="min-height: 100%">
            <tbody>
                <tr>
                    <td style='font: 12PT ARIAL'>
                        <div style='width: 62%; float: left'>
                            <div style="width: 100%">
                                <span style='font: 12PT ARIAL; float: left; font-weight: bold'>Understanding Your Account
                                    Summary </span>
                                <br />
                                <br />
                                <br />
                                <div style='width: 100%; border-right: 1px solid black; position: relative'>
                                    <div style="width: 98%; font: 12pt ARIAL;">
                                        <strong>Rent Account Balance</strong>
                                        <br />
                                        This is the total amount you owe us and are responsible for paying. If your account
                                        is in credit, we would encourage you to keep this in the event of any change of
                                        circumstances. Should you want a refund, please contact your Income Recovery Officer
                                        to discuss further.<br />
                                        <br />
                                        <strong>Estimated Housing Benefit Due</strong>
                                        <br />
                                        For those in receipt of Housing Benefit, this is the amount estimated to be received
                                        up-to the end of the month, in which your account summary is dated.<br />
                                        <br />
                                        Please note this is only an estimate which is based upon the last amount received,
                                        and assumes your circumstances have not changed. Housing Benefit may vary, therefore
                                        changing the amount ‘to pay’.<br />
                                        <br />
                                        <strong>To Pay</strong>
                                        <br />
                                        This is the amount to pay after any estimated Housing Benefit due. Please note,
                                        if Housing Benefit is not received or the amount received varies, you are still
                                        responsible for the full rent account balance.<br />
                                        <br />
                                        <strong>Recharge Balance</strong>
                                        <br />
                                        The amount outstanding on your recharge account as at the date the summary is dated.<br />
                                        <br />
                                        <strong>Payments made within the last 3 working days may not appear on your statement.</strong><br />
                                        <br />
                                        <strong>All our documents can be supplied in large print, Braille, audio tape and in
                                            languages other than English. Please contact 0303 303 0003 if you require this service.</strong><br />
                                        <br />
                                        For a full rent statement, please log into your Tenant’s Online Account or alternatively,
                                        please call
                                        <br />
                                        us on 0303 303 0003 to request one.
                                        <div style="height: 95px; width: 100%;">
                                        </div>
                                        <div id='bottom' style='width: 62%; position: absolute; bottom: 0; text-align: right;
                                            font: 10pt ARIAL; left: 102%'>
                                            <div style="padding-right: 10.5px">
                                                Part of the Broadland Housing Group,
                                                <br />
                                                incorporating Broadland Housing
                                                <br />
                                                Association Limited, Broadland Merdian,
                                                <br />
                                                Broadland St Benedicts Limited and
                                                <br />
                                                Broadland Development Services.
                                                <br />
                                                <br />
                                                Broadland Housing Association Limited:
                                                <br />
                                                Registered address NCFC, Carrow Road,
                                                <br />
                                                Norwich, NR1 1HU. Registered in England
                                                <br />
                                                and Wales as a Registered Society under
                                                <br />
                                                the Co-operative and Community Benefit
                                                <br />
                                                Societies Act 2014 as a non profit making
                                                <br />
                                                housing association with charitable status.
                                                <br />
                                                Registered Society No. 16274R. HCA
                                                <br />
                                                Registration No. L0026
                                                <br />
                                                <br />
                                                <div style='width: 35%; display: inline-block; margin-right: 2px; float: left'>
                                                    <img style="height: auto; width: 100%" src='../../Images/RentAccount/bottom_logo-1.png' /></div>
                                                <div style='width: 15%; display: inline-block; margin-right: 2px; float: left'>
                                                    <img style="height: auto; width: 100%" src='../../Images/RentAccount/bottom_logo-2.png' /></div>
                                                <div style='margin-right: 0px; width: 48%; display: inline-block; float: left'>
                                                    <img style="height: auto; width: 100%" src='../../Images/RentAccount/bottom_logo-3.png' /></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div style='width: 36%; vertical-align: top; float: left;'>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</asp:Content>
