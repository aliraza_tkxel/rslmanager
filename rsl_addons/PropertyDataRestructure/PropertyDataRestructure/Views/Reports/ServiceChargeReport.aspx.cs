﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_BusinessLogic.SchemeBlock;
using PDR_BusinessObject.PageSort;
using PDR_DataAccess.SchemeBlock;
using PDR_Utilities.Constants;
using PDR_Utilities.Helpers;
using PropertyDataRestructure.Base;
using PropertyDataRestructure.Interface;
using PDR_BusinessLogic.Scheme;
using PDR_DataAccess.Scheme;
using PDR_BusinessLogic.Block;
using PDR_DataAccess.Block;
using PDR_DataAccess.Scheduling;
using PDR_BusinessLogic.Scheduling;
using PDR_BusinessLogic.Reports;
using PDR_DataAccess.Reports;

namespace PropertyDataRestructure.Views.Reports
{
    public partial class ServiceChargeReport : PageBase, IListingPage
    {
        PageSortBO objPageSortBo = new PageSortBO("DESC", "DateCreated", 1, 20);
        AttributesBL objAttrBL = new AttributesBL(new AttributesRepo());
        SchedulingBL objSchedulingBl = new SchedulingBL(new SchedulingRepo());
        SchemeBL objSchemeBl = new SchemeBL(new SchemeRepo());   
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    populateDropDowns();
                    populateData();
                    loadData();
                }
                resetMessagePopups();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #region IListing Implementation
        public void loadData()
        {
            
            int fiscalYear = Convert.ToInt32(ddlfiscal.SelectedValue);
            Int32 totalCount = 0;                                    
            PageSortBO objPageSortBo;
            if (pageSortViewState == null)
            {
                objPageSortBo = new PageSortBO("desc", "DateCreated", 1, 20);
                pageSortViewState = objPageSortBo;
            }
            else
            {
                objPageSortBo = pageSortViewState;
            }
            
            DataSet provisionsListDS = new DataSet();
            int schemeId = Convert.ToInt32(ddlScheme.SelectedValue);
            int blockId = Convert.ToInt32(ddlBlock.SelectedValue);
            int itemId = Convert.ToInt32(ddlAttr.SelectedValue);

            totalCount = objAttrBL.GetServiceChargeReport(ref provisionsListDS, objPageSortBo, schemeId, blockId,itemId, fiscalYear);
            grdServiceCharge.DataSource = provisionsListDS;
            objSession.ServiceChargeDS = provisionsListDS;
            grdServiceCharge.DataBind();
            objPageSortBo.TotalRecords = totalCount;
            objPageSortBo.TotalPages = Convert.ToInt32(Math.Ceiling((Double)totalCount / objPageSortBo.PageSize));
            if (objPageSortBo.TotalPages > 0)
            {
                pnlPagination.Visible = true;
            }
            else
            {
                pnlPagination.Visible = false;
                uiMessage.showErrorMessage(UserMessageConstants.RecordNotFound);
            }
            pageSortViewState = objPageSortBo;
            GridHelper.setGridViewPager(ref pnlPagination, objPageSortBo);
        }

        public void searchData()
        {
            throw new NotImplementedException();
        }

        public void populateData()
        {

        }
        public void printData()
        {
            throw new NotImplementedException();
        }

        public void exportGridToExcel()
        {
            int totalCount = 0;
            objPageSortBo = pageSortViewState;
            objPageSortBo.PageNumber = 1;

            DataSet provisionsListDS = new DataSet();
            int schemeId = Convert.ToInt32(ddlScheme.SelectedValue);
            int blockId = Convert.ToInt32(ddlBlock.SelectedValue);
            int fiscalYear = Convert.ToInt32(ddlfiscal.SelectedValue);
            int itemId = Convert.ToInt32(ddlAttr.SelectedValue);

            totalCount = objAttrBL.GetServiceChargeReport(ref provisionsListDS, objPageSortBo, schemeId, blockId, itemId, fiscalYear, fetchAllRecords: true);
            DataTable dt = provisionsListDS.Tables[0].DefaultView.ToTable();
            dt = provisionsListDS.Tables[0];

            dt.Columns["SchemeName"].ColumnName = "Scheme Name:";
            dt.Columns["BlockName"].ColumnName = "Block Name:";
            dt.Columns["InPropCount"].ColumnName = "Number of Properties(Included):";
            dt.Columns["Details"].ColumnName = "Details:";
            dt.Columns["ItemName"].ColumnName = "Attribute/PO Item:";            
            dt.Columns["Budget"].ColumnName = "Budget:";
            dt.Columns["ApportionmentBudget"].ColumnName = "Apportionment (Budget):";
            dt.Columns["ActualTotal"].ColumnName = "Actual (Total):";
            dt.Columns["ApportionmentActual"].ColumnName = "Apportionment (Actual):";
            dt.Columns["ExPropCount"].ColumnName = "Excluded Properties:";
            dt.Columns.Remove("row");
            dt.Columns.Remove("ItemId");
            dt.Columns.Remove("PropertyCount");
            dt.Columns.Remove("Ref");
            dt.Columns.Remove("SchemeId");
            dt.Columns.Remove("BlockId");
            dt.Columns.Remove("DateCreated");
            dt.Columns.Remove("Type");
            dt.Columns.Remove("BudgetSort");
            dt.Columns.Remove("ApportionmentBudgetSort");
            dt.Columns.Remove("ActualTotalSort");
            dt.Columns.Remove("ApportionmentActualSort");
            dt.Columns.Remove("IsServiceChargePO");
            dt.Columns.Remove("ChildItemId");  

            string fileName = "ServiceChargeReport_" + Convert.ToString(DateTime.Now.Day) + "_" + Convert.ToString(DateTime.Now.Month) + "_" + Convert.ToString(DateTime.Now.Year);
            ExportDownloadHelper.exportGridToExcel(fileName, dt);

        }

        public void exportToPdf()
        {
            throw new NotImplementedException();
        }

        public void downloadData()
        {
            throw new NotImplementedException();
        }

        public void applyFilters()
        {
            throw new NotImplementedException();
        }
        #endregion
        #region lnkbtnPager Click
        protected void lnkbtnPager_Click(object sender, EventArgs e)
        {
            try
            {
                LinkButton pagebuttton = new LinkButton();
                pagebuttton = (LinkButton)sender;
                PageSortBO objPageSortBo = new PageSortBO("DESC", "DateCreated", 1, 20);
                GridHelper.processGridPager(sender, objPageSortBo, pageSortViewState);
                loadData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #region "Grid Service Charge Row Data Bound"

        protected void grdServiceCharge_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                Label lblScheme = (Label)e.Row.FindControl("lblScheme");
                LinkButton lnkBtnScheme = (LinkButton)e.Row.FindControl("lnkBtnScheme");
                LinkButton lnkBtnDetail = (LinkButton)e.Row.FindControl("lnkBtnDetail");
                HiddenField hdnRef = (HiddenField)e.Row.FindControl("hdnRef");
                HiddenField hdnType = (HiddenField)e.Row.FindControl("hdnType");

                if (hdnType.Value.Equals(ApplicationConstants.PurchaseItemServiceCharge))
                {
                    lnkBtnScheme.Visible = true;
                    lblScheme.Visible = false;
                }
                else
                {
                    lnkBtnScheme.Visible = false;
                    lblScheme.Visible = true;
                }

                lnkBtnDetail.OnClientClick = string.Format("showDetail('{0}','{1}');", lnkBtnDetail.Text, hdnRef.Value);
                if (lnkBtnDetail.Text.Equals("-"))
                {
                    lnkBtnDetail.Enabled = false;
                }

            }
        }

        #endregion

        #region "Associated Schemes Grid Row Data Bound"

        protected void grdAssociatedSchemes_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //LinkButton lnkBtnBlockCount = (LinkButton)e.Row.FindControl("lnkBtnBlockCount");
                //LinkButton lnkBtnInPropCount = (LinkButton)e.Row.FindControl("lnkBtnInPropCount");
                //LinkButton lnkBtnExPropCount = (LinkButton)e.Row.FindControl("lnkBtnExPropCount");

                //lnkBtnBlockCount.Enabled = Convert.ToInt32(lnkBtnBlockCount.Text) > 0 ? true : false;
                //lnkBtnInPropCount.Enabled = Convert.ToInt32(lnkBtnInPropCount.Text) > 0 ? true : false;
                //lnkBtnExPropCount.Enabled = Convert.ToInt32(lnkBtnExPropCount.Text) > 0 ? true : false;
                
            }
        }

        #endregion

        private void resetMessagePopups()
        {
            uiMessageBlocksPopup.hideMessage();
            uiMessage.hideMessage();
            uiMessagePropertyPopup.hideMessage();
        }
        
        protected void btnFilter_Click(object sender, EventArgs e)
        {
            PageSortBO objPageSortBo = new PageSortBO("desc", "DateCreated", 1, 20);
            pageSortViewState = objPageSortBo;
            loadData();
        }
        protected void grdServiceCharge_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                PageSortBO objPageSortBo;
                if (pageSortViewState == null)
                {
                    objPageSortBo = new PageSortBO("DESC", "DateCreated", 1, 20);
                    pageSortViewState = objPageSortBo;
                }
                else
                {
                    objPageSortBo = pageSortViewState;
                }
                pageSortViewState = GridHelper.sortGrid(grdServiceCharge, e, objPageSortBo, pageSortViewState);
                loadData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }

        #region"change PageNumber"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void changePageNumber(object sender, EventArgs e)
        {
            try
            {
                Button btnGo = new Button();
                btnGo = (Button)sender;
                PageSortBO objPageSortBo;
                objPageSortBo = new PageSortBO("DESC", "DateCreated", 1, 20);
                objPageSortBo = pageSortViewState;
                int pageNumber = 1;
                pageNumber = Convert.ToInt32(txtPageNumber.Text);
                txtPageNumber.Text = String.Empty;
                objPageSortBo = pageSortViewState;
                if (((pageNumber >= 1)
                            && (pageNumber <= objPageSortBo.TotalPages)))
                {
                    objPageSortBo = pageSortViewState;
                    objPageSortBo.PageNumber = pageNumber;
                    pageSortViewState = objPageSortBo;
                    loadData();

                }
                else
                {
                    // uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, UserMessageConstants.InvalidPageNumber, true);
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion
        protected void grdServiceCharge_RowCreated(object sender, GridViewRowEventArgs e)
        {
            //check if it is a header row
            //since allowsorting is set to true, column names are added as command arguments to
            //the linkbuttons by DOTNET API
            if (e.Row.RowType == DataControlRowType.Header)
            {
                LinkButton btnSort;
                Image image;
                //iterate through all the header cells
                foreach (TableCell cell in e.Row.Cells)
                {
                    //check if the header cell has any child controls
                    if (cell.HasControls())
                    {
                        //get reference to the button column
                        btnSort = (LinkButton)cell.Controls[0];
                        image = new Image();
                        if (pageSortViewState != null)
                        {
                            if (btnSort.CommandArgument == pageSortViewState.SortExpression)
                            {
                                //following snippet figure out whether to add the up or down arrow
                                //based on the sortdirection
                                if (pageSortViewState.SortDirection == SortDirection.Ascending.ToString())
                                {
                                    image.ImageUrl = "~/Images/Grid/sort_asc.png";
                                }
                                else
                                {
                                    image.ImageUrl = "~/Images/Grid/sort_desc.png";
                                }
                            }
                            else
                            {
                                image.ImageUrl = "~/Images/Grid/sort_both.png";
                            }
                            cell.Controls.Add(image);
                        }
                    }
                }
            }
        }


        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            try
            {
                uiMessage.hideMessage();
                this.exportGridToExcel();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        private string GetFilterString()
        {
           // string itemName = String.IsNullOrWhiteSpace(txtItemName.Text) ? string.Empty : txtItemName.Text.ToString();
          //  string schemeName = String.IsNullOrWhiteSpace(txtSchemeName.Text) ? string.Empty : txtSchemeName.Text.ToString();
           // string blockName = String.IsNullOrWhiteSpace(txtBlockName.Text) ? string.Empty : txtBlockName.Text.ToString();

            StringBuilder filterString = new StringBuilder();
            filterString.Append("1=1");
            if (Convert.ToInt32(ddlScheme.SelectedValue) > 0)
            {
                filterString.Append(string.Format(" AND   {0} = {1} ", "m.SchemeId", Convert.ToInt32(ddlScheme.SelectedValue)));
            }
            if (Convert.ToInt32(ddlBlock.SelectedValue) > 0)
            {
                filterString.Append(string.Format("AND  {0} = {1} ", "m.BlockId", Convert.ToInt32(ddlBlock.SelectedValue)));
            }
            if (Convert.ToInt32(ddlAttr.SelectedValue) > 0)
            {
                filterString.Append(string.Format(" AND {0} = {1} ", "m.ItemId", Convert.ToInt32(ddlAttr.SelectedValue)));
            }

            return filterString.ToString();
        }
        private string GetFilterStringDC()
        {
            // string itemName = String.IsNullOrWhiteSpace(txtItemName.Text) ? string.Empty : txtItemName.Text.ToString();
            //  string schemeName = String.IsNullOrWhiteSpace(txtSchemeName.Text) ? string.Empty : txtSchemeName.Text.ToString();
            // string blockName = String.IsNullOrWhiteSpace(txtBlockName.Text) ? string.Empty : txtBlockName.Text.ToString();

            StringBuilder filterString = new StringBuilder();
            filterString.Append("1=1");
            if (Convert.ToInt32(ddlScheme.SelectedValue) > 0)
            {
                filterString.Append(string.Format(" AND   {0} = {1} ", "S.SchemeId", Convert.ToInt32(ddlScheme.SelectedValue)));
            }
            if (Convert.ToInt32(ddlBlock.SelectedValue) > 0)
            {
                filterString.Append(string.Format("AND  {0} = {1} ", "B.BlockId", Convert.ToInt32(ddlBlock.SelectedValue)));
            }
            return filterString.ToString();
        }

        protected void btnExPropCount_Click(object sender, EventArgs e)
        {

            Button button = (Button)sender;
            var commArg = button.CommandArgument.Split(';');
            DataSet resultSet = new DataSet();

            bool isServiceChargePO = Convert.ToBoolean(commArg[3]);
            int itemId = Convert.ToInt32(commArg[2]);


            GridViewRow row = (GridViewRow)button.NamingContainer;
            HiddenField hdnRef = (HiddenField)row.FindControl("hdnRef");
            HiddenField hdnType = (HiddenField)row.FindControl("hdnType");
            string serviceChargeSource = hdnType.Value;

            if (serviceChargeSource.Equals(ApplicationConstants.PurchaseItemServiceCharge))
            {
                resultSet = objAttrBL.getPurchaseItemExcludedProperties(itemId);
            }
            else if (serviceChargeSource.Equals(ApplicationConstants.GeneralJournalServiceCharge))
            {
                int txnId = Convert.ToInt32(hdnRef.Value);
                resultSet = objAttrBL.getGeneralJournalExcludedProperties(txnId);
            }
            else
            {
                int schemeId = string.IsNullOrEmpty(commArg[0]) ? 0 : Convert.ToInt32(commArg[0]);
                int blockId = string.IsNullOrEmpty(commArg[1]) ? 0 : Convert.ToInt32(commArg[1]);
                int childItemId = Convert.ToInt32(commArg[4]);
                resultSet = objAttrBL.getExPropertiesBySchemeBlock(schemeId, blockId, itemId, childItemId);
            }
                      
            grdExcludedProperties.DataSource = resultSet.Tables[0];
            grdExcludedProperties.DataBind();
            mdlPopupShowProperties.Show();
        }

        protected void lnkBtnScheme_Click(object sender, EventArgs e)
        {

            LinkButton lnkBtnScheme = (LinkButton)sender;
            var commArg = lnkBtnScheme.CommandArgument;
            int orderItemId = Convert.ToInt32(commArg);
            showItemAssociatedSchemes(orderItemId);            
        }

        private void showItemAssociatedSchemes(int orderItemId)
        {
            ReportsBL objReportsBL = new ReportsBL(new ReportsRepo());
            DataSet resultSet = new DataSet();
            resultSet = objReportsBL.getServiceChargeAssociatedSchemes(orderItemId);

            DataTable associatedSchemesDt = resultSet.Tables[0];
            grdAssociatedSchemes.DataSource = associatedSchemesDt;
            grdAssociatedSchemes.DataBind();

            lblTotalNumberOfSchemes.Text = associatedSchemesDt.AsEnumerable().Count().ToString();
            lblTotalNumberOfBlocks.Text = associatedSchemesDt.AsEnumerable().Sum(c => c.Field<int>("BlockCount")).ToString();

            mdlPopupAsscoiatedSchemes.Show();
        }

        protected void lnkBtnInPropCount_Click(object sender, EventArgs e)
        {

            LinkButton lnkBtnScheme = (LinkButton)sender;
            var commArg = lnkBtnScheme.CommandArgument.Split(';');
            btnSaveAmends.CommandArgument = lnkBtnScheme.CommandArgument;
            int orderItemId = Convert.ToInt32(commArg[0]);
            int schemeId = Convert.ToInt32(commArg[1]);
            string schemeName = commArg[2];

            ReportsBL objReportsBL = new ReportsBL(new ReportsRepo());
            DataSet resultSet = new DataSet();
            resultSet = objReportsBL.getServiceChargeIncExcPropertiesByScheme(schemeId, orderItemId);

            DataTable associatedProperties = resultSet.Tables[0];
            populateIncludedProperties(associatedProperties);
            populateExcludedProperties(associatedProperties);

            lblSchemeName.Text = schemeName;            

            mdlPopupAsscoiatedSchemes.Show();
            mdlPopupAsscoiatedProperties.Show();
        }


        protected void lnkBtnBlockCount_Click(object sender, EventArgs e)
        {

            LinkButton lnkBtnBlockCount = (LinkButton)sender;
            var commArg = lnkBtnBlockCount.CommandArgument.Split(';');
            imgBtnCancelAssociatedBlocksPopup.CommandArgument = lnkBtnBlockCount.CommandArgument;
            int orderItemId = Convert.ToInt32(commArg[0]);
            int schemeId = Convert.ToInt32(commArg[1]);
            string schemeName = commArg[2];

            ReportsBL objReportsBL = new ReportsBL(new ReportsRepo());
            DataSet resultSet = new DataSet();
            resultSet = objReportsBL.getServiceChargeSchemeBlocks(schemeId,orderItemId);

            DataTable associatedBlocks = resultSet.Tables[0];
            grdSchemeBlocks.DataSource = associatedBlocks;
            grdSchemeBlocks.DataBind();                      

            mdlPopupAsscoiatedSchemes.Show();
            mdlPopupAssociatedBlocks.Show();
        }

        

        protected void lnkBtnExPropCount_Click(object sender, EventArgs e)
        {
            LinkButton lnkBtnScheme = (LinkButton)sender;
            btnSaveAmends.CommandArgument = lnkBtnScheme.CommandArgument;
            var commArg = lnkBtnScheme.CommandArgument.Split(';');
            int orderItemId = Convert.ToInt32(commArg[0]);
            int schemeId = Convert.ToInt32(commArg[1]);
            string schemeName = commArg[2];

            ReportsBL objReportsBL = new ReportsBL(new ReportsRepo());
            DataSet resultSet = new DataSet();
            resultSet = objReportsBL.getServiceChargeIncExcPropertiesByScheme(schemeId, orderItemId);

            DataTable associatedProperties = resultSet.Tables[0];
            populateIncludedProperties(associatedProperties);
            populateExcludedProperties(associatedProperties);

            lblSchemeName.Text = schemeName;

            mdlPopupAsscoiatedSchemes.Show();
            mdlPopupAsscoiatedProperties.Show();
        }


        #region "ImgBtn Cancel Associated Properties Popup"
        protected void imgBtnCancelAssociatedPropertiesPopup_Click(object sender, EventArgs e)
        {
            mdlPopupAsscoiatedSchemes.Show();            
        }
        #endregion

        #region "ImgBtn Cancel Associated Blocks Popup"
        protected void imgBtnCancelAssociatedBlocksPopup_Click(object sender, EventArgs e)
        {
            mdlPopupAsscoiatedSchemes.Show();
        }
        #endregion


        #region "Save Amends Buttons"
        protected void btnSaveAmends_Click(object sender, EventArgs e)
        {

            try
            {
                // Get excluded properties
                DataTable excPropertyDt = new DataTable();
                excPropertyDt.Columns.Add(new DataColumn("propertyId",typeof(string)));

                foreach (ListItem property in ddlExcludedProperties.Items)
                {
                    excPropertyDt.Rows.Add(property.Value);
                }

                // Get SchemeId, PurchaseOrderItemId
                Button btnSaveAmends = (Button)sender;
                var commArg = btnSaveAmends.CommandArgument.Split(';');
                int orderItemId = Convert.ToInt32(commArg[0]);
                int schemeId = Convert.ToInt32(commArg[1]);

                ReportsBL objReportsBL = new ReportsBL(new ReportsRepo());
                var isSaved = objReportsBL.saveExcludedProperties(excPropertyDt,schemeId, orderItemId);

                if (isSaved)
                {
                    loadData();
                    showItemAssociatedSchemes(orderItemId);
                }
                else
                {
                    uiMessagePropertyPopup.showErrorMessage(UserMessageConstants.UnableToSaveChange);
                    mdlPopupAsscoiatedSchemes.Show();
                    mdlPopupAsscoiatedProperties.Show();
                }

            }
            catch (Exception ex)
            {
                uiMessagePropertyPopup.isError = true;
                uiMessagePropertyPopup.messageText = ex.Message;

                if ((uiMessagePropertyPopup.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessagePropertyPopup.isError == true))
                {
                    uiMessagePropertyPopup.showErrorMessage(uiMessagePropertyPopup.messageText);
                }
            }

        }
        #endregion

        #region "Exclude property button"
        protected void btnExcludeProperty_Click(object sender, EventArgs e)
        {

            try
            {

                var selectedProperties = (from ListItem li in ddlIncludedProperties.Items
                                         where li.Selected == true
                                         select li).ToList<ListItem>();

                if (selectedProperties.Count>0)
                {
                    foreach (ListItem property in selectedProperties)
                    {
                        ddlIncludedProperties.Items.Remove(property);
                        ddlExcludedProperties.Items.Add(property);
                    }
                }
                else
                {
                    uiMessagePropertyPopup.showErrorMessage(UserMessageConstants.SelectIncludedProperty);
                }                

                mdlPopupAsscoiatedSchemes.Show();
                mdlPopupAsscoiatedProperties.Show();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        #endregion

        #region "Include property button"
        protected void btnIncludeProperty_Click(object sender, EventArgs e)
        {

            try
            {

                var selectedProperties = (from ListItem li in ddlExcludedProperties.Items
                                          where li.Selected == true
                                          select li).ToList<ListItem>();

                if (selectedProperties.Count > 0)
                {
                    foreach (ListItem property in selectedProperties)
                    {
                        ddlExcludedProperties.Items.Remove(property);
                        ddlIncludedProperties.Items.Add(property);
                    }
                }
                else
                {
                    uiMessagePropertyPopup.showErrorMessage(UserMessageConstants.SelectExcludedProperty);
                }   

                mdlPopupAsscoiatedSchemes.Show();
                mdlPopupAsscoiatedProperties.Show();

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        #endregion

        private void populateIncludedProperties(DataTable associatedProperties)
        {
            var resultRows = associatedProperties.AsEnumerable().Where(r => r.Field<int>("isExcluded") == 0);
            DataTable incPropTble = resultRows.Any() ? resultRows.CopyToDataTable() : associatedProperties.Clone();

            ddlIncludedProperties.Items.Clear();
            ddlIncludedProperties.DataSource = incPropTble;
            ddlIncludedProperties.DataValueField = "propertyId";
            ddlIncludedProperties.DataTextField = "propertyAddress";
            ddlIncludedProperties.DataBind();

        }

        private void populateExcludedProperties(DataTable associatedProperties)
        {
            var resultRows = associatedProperties.AsEnumerable().Where(r => r.Field<int>("isExcluded") == 1);
            DataTable excPropTble = resultRows.Any() ? resultRows.CopyToDataTable() : associatedProperties.Clone();

            ddlExcludedProperties.Items.Clear();
            ddlExcludedProperties.DataSource = excPropTble;
            ddlExcludedProperties.DataValueField = "propertyId";
            ddlExcludedProperties.DataTextField = "propertyAddress";
            ddlExcludedProperties.DataBind();
        }

        #region populate dropdowns

        private void populateDropDowns()
        {
            /*Populate fiscal Year DropDown*/
           
            DataSet resultData = new DataSet();
            DataSet resultDataSet = new DataSet();
            DataSet resultDs = new DataSet();
            DataSet blockDataSet = new DataSet();
            resultDataSet = objAttrBL.getFiscalYears();
            objSchedulingBl.getAllAttributes(ref resultDs);
            resultData = objSchemeBl.populateSchemeDropDown();
            blockDataSet = objSchemeBl.populateBlockDropDown(-1);
            ddlfiscal.DataSource = resultDataSet.Tables[0];
            ddlfiscal.DataValueField = "YRANGE";
            ddlfiscal.DataTextField = "YEARSHORT";
            ddlfiscal.DataBind();
            ddlfiscal.ClearSelection(); //making sure the previous selection has been cleared
            string currentFiscal = Convert.ToString(resultDataSet.Tables[0].Compute("max([YRANGE])", string.Empty));
            ddlfiscal.Items.FindByValue(currentFiscal).Selected = true;
            ddlAttr.DataSource = resultDs.Tables[0];
            ddlAttr.DataValueField = "ItemID";
            ddlAttr.DataTextField = "ItemName";
            ddlAttr.DataBind();
            ddlAttr.Items.Insert(0, new ListItem("Select Attribute", "-1"));
            ddlAttr.ClearSelection(); //making sure the previous selection has been cleared
            ddlScheme.DataSource = resultData.Tables[0];
            ddlScheme.DataValueField ="SCHEMEID";
            ddlScheme.DataTextField = "SCHEMENAME";
            ddlScheme.DataBind();
            ddlScheme.Items.Insert(0, new ListItem("Select Scheme", "-1"));
            ddlScheme.ClearSelection(); //making sure the previous selection has been cleared
            ddlScheme_SelectedIndexChanged(null, null);

            //ddlBlock.DataSource = blockDataSet.Tables[0];
            //ddlBlock.DataValueField = "BLOCKID";
            //ddlBlock.DataTextField = "BLOCKNAME";
            //ddlBlock.DataBind();
            //ddlBlock.Items.Insert(0, new ListItem("Select Block", "-1"));
            //ddlBlock.ClearSelection(); //making sure the previous selection has been cleared
            
        }

        #endregion

        protected void ddlScheme_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataSet blockDataSet = new DataSet();
            blockDataSet = objSchemeBl.populateBlockDropDown(Convert.ToInt32(ddlScheme.SelectedValue));
            ddlBlock.DataSource = blockDataSet.Tables[0];
            ddlBlock.DataValueField = "BLOCKID";
            ddlBlock.DataTextField = "BLOCKNAME";
            ddlBlock.DataBind();
            ddlBlock.Items.Insert(0, new ListItem("Select Block", "-1"));
        }

    }
}