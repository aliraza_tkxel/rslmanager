﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Pdr.Master" AutoEventWireup="true"
    CodeBehind="ReportArea.aspx.cs" Inherits="PropertyDataRestructure.Views.Reports.ReportArea" EnableEventValidation="false" %>
    <%@ Register TagPrefix="uc1" TagName="ReportStructure" Src="~/UserControls/Reports/ReportStructure.ascx" %>
    <%@ Register TagName="Terrior" TagPrefix="uc2" Src="~/UserControls/Reports/TerrierReport .ascx"     %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

<script language="javascript" type="text/javascript">
    function PrintPage(id) {       
        var printContent = document.getElementById(id);
        var printWindow = window.open("All Records", "Print Panel", 'left=50000,top=50000,width=0,height=0');
        printWindow.document.write(printContent.innerHTML);
        printWindow.document.close();
        printWindow.focus();
        printWindow.print();
    }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       <%-- <asp:UpdateProgress ID="updateProgressReportsArea" runat="server" DisplayAfter="10">
        <ProgressTemplate>
            <div class="loading-image">
                <img alt="Please Wait" src="../../Images/menu/progress.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>--%>
    <asp:UpdatePanel ID="updPanelReportArea" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
        <uc1:ReportStructure ID="reportStructure" runat="server"  Visible="false"></uc1:ReportStructure>       
         </ContentTemplate>
   </asp:UpdatePanel>
 <uc2:Terrior ID="terriorReport" runat="server" Visible="false" ></uc2:Terrior>
</asp:Content>
