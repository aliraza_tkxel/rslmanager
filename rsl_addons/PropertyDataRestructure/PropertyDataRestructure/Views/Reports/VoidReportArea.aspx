﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Pdr.Master" AutoEventWireup="true"
    CodeBehind="VoidReportArea.aspx.cs" Inherits="PropertyDataRestructure.Views.Reports.VoidReportArea" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<%@ Register TagPrefix="ucReport" TagName="NoEntry" Src="~/UserControls/Reports/VoidNoEntry.ascx" %>
<%@ Register TagPrefix="ucReport" TagName="VoidProperties" Src="~/UserControls/Reports/VoidProperties.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        //setup before functions
        var typingTimer;                //timer identifier
        var doneTypingInterval = 1000;  //time in ms, 5 second for example

        function TypingInterval() {

            clearTimeout(typingTimer);

            typingTimer = setTimeout(searchTextChanged, doneTypingInterval);

        }

        function searchTextChanged() {
            __doPostBack('<%= txtSearch.ClientID %>', '');
        }
        
    </script>
    <style type="text/css">
        th
        {
            text-align: left;
        }
        .dashboard th{
            background: #fff;
            border-bottom: 4px solid #8b8687;
        }
        .dashboard th a{
            color: #000 !important;
        }
        .dashboard th img{
            float:right;
        }
        .text_div input{
           width: 150px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="updateProgressReportsArea" runat="server" DisplayAfter="10">
        <ProgressTemplate>
            <div class="loading-image">
                <img alt="Please Wait" src="../../Images/menu/progress.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="portlet">
        <div class="header pnlHeading">
            <asp:Label ID="lblReportHeading" CssClass="header-label" Text="" runat="server" Font-Bold="true" />
            <div class="field right">
                <asp:Panel ID="pnlSearch" runat="server" HorizontalAlign="Right" Style="float: left;" Visible="True">
                    <asp:TextBox ID="txtSearch" AutoPostBack="false" style="padding:3px 10px !important; margin:-4px 10px 0 0;" AutoCompleteType="Search" 
                        class="searchbox styleselect-control searchbox right" onkeyup="TypingInterval();" OnTextChanged="txtSearch_TextChanged" PlaceHolder="Quick find" runat="server">
                    </asp:TextBox>
                    <ajax:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server"
                        TargetControlID="txtSearch" WatermarkText="Quick find" WatermarkCssClass="searchbox searchText">
                    </ajax:TextBoxWatermarkExtender>
                    <%--<asp:Button runat="server" ID="btnGo" UseSubmitBehavior="false" Text="GO" OnClick="btnGO_Click"
                        CssClass="btn btn-xs btn-blue" style="padding: 2px 10px !important; margin: -3px 0 0 0; position: absolute; margin: 38px 0px 0 -51px;" />--%>
                </asp:Panel>
            </div>
        </div>
        <div class="portlet-body" style="font-size: 12px; padding-bottom:0;">
            <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="500px" />
            <asp:UpdatePanel ID="updPanelReportArea" runat="server">
                <ContentTemplate>
                    <ucReport:NoEntry ID="ucNoEntryReport" runat="server" Visible="false"></ucReport:NoEntry>
                    <ucReport:VoidProperties ID="ucVoidProperties" runat="server" Visible="false"></ucReport:VoidProperties>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
