﻿using System;
using System.Data;
using System.Text;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_BusinessLogic.SchemeBlock;
using PDR_BusinessObject.PageSort;
using PDR_DataAccess.SchemeBlock;
using PDR_Utilities.Constants;
using PDR_Utilities.Helpers;
using PropertyDataRestructure.Base;
using PropertyDataRestructure.Interface;

namespace PropertyDataRestructure.Views.Reports
{
    public partial class ProvisioningReport : PageBase, IListingPage
    {
        PageSortBO objPageSortBo = new PageSortBO("DESC", "PROVISIONID", 1, 20);
        ProvisionsBL objProvisionBL = new ProvisionsBL(new ProvisionsRepo());
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    LoadCategory();
                    loadData();
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #region IListing Implementation
        public void loadData()
        {
            String filterText = this.GetFilterString();
            Int32 totalCount = 0;
            PageSortBO objPageSortBo;
            if (pageSortViewState == null)
            {
                objPageSortBo = new PageSortBO("DESC", "PROVISIONID", 1, 20);
                pageSortViewState = objPageSortBo;
            }
            else
            {
                objPageSortBo = pageSortViewState;
            }

            DataSet provisionsListDS = new DataSet();
            totalCount = objProvisionBL.GetProvisioningReport(ref provisionsListDS, objPageSortBo, filterText, Convert.ToInt32(ddlCategory.SelectedValue));


            grdProvisionsList.DataSource = provisionsListDS;
            objSession.ProvisionListDS = provisionsListDS;
            grdProvisionsList.DataBind();
            objPageSortBo.TotalRecords = totalCount;
            objPageSortBo.TotalPages = Convert.ToInt32(Math.Ceiling((Double)totalCount / objPageSortBo.PageSize));
            if (objPageSortBo.TotalPages > 0)
            {
                pnlPagination.Visible = true;
            }
            else
            {
                pnlPagination.Visible = false;
                uiMessage.showErrorMessage(UserMessageConstants.RecordNotFound);
            }
            pageSortViewState = objPageSortBo;
            GridHelper.setGridViewPager(ref pnlPagination, objPageSortBo);
        }

        public void searchData()
        {
            throw new NotImplementedException();
        }

        public void populateData()
        {
            throw new NotImplementedException();
        }

        public void printData()
        {
            throw new NotImplementedException();
        }

        public void exportGridToExcel()
        {

            String filterText = this.GetFilterString();
            Int32 totalCount = 0;
            PageSortBO objPageSortBo;
            if (pageSortViewState == null)
            {
                objPageSortBo = new PageSortBO("DESC", "PROVISIONID", 1, 20);
                pageSortViewState = objPageSortBo;
            }
            else
            {
                objPageSortBo = pageSortViewState;
            }
            objPageSortBo.PageNumber = 1;
            objPageSortBo.PageSize = 65000;
            DataSet provisionsListDS = new DataSet();
            totalCount = objProvisionBL.GetProvisioningReport(ref provisionsListDS, objPageSortBo, filterText, Convert.ToInt32(ddlCategory.SelectedValue));

            DataTable dt = provisionsListDS.Tables[0];

            dt.Columns["SCHEMENAME"].ColumnName = "Scheme:";
            dt.Columns["BLOCKNAME"].ColumnName = "Block:";
            dt.Columns["CATEGORYNAME"].ColumnName = "Category:";
            dt.Columns["PROVISIONDESCRIPTION"].ColumnName = "Description:";
            dt.Columns["PURCHASEDDATE"].ColumnName = "Purchase Date:";
            dt.Columns["LIFESPAN"].ColumnName = "Lifespan:";
            dt.Columns["REPLACEMENTDUE"].ColumnName = "Replacement Due:";
            dt.Columns["BUDGET"].ColumnName = "BUDGET:";
            dt.Columns["APPORTIONMENTBUDGET"].ColumnName = "Apportionment(Budget):";
            dt.Columns["ACTUALTOTAL"].ColumnName = "Actual Total:";
            dt.Columns["APPORTIONMENTACTUAL"].ColumnName = "Apportionment(Actual):";
            dt.Columns.Remove("row");
            dt.Columns.Remove("PROVISIONID");

            string fileName = "ProvisioningReport_" + Convert.ToString(DateTime.Now.Day) + "_" + Convert.ToString(DateTime.Now.Month) + "_" + Convert.ToString(DateTime.Now.Year);
            ExportDownloadHelper.exportGridToExcel(fileName, dt);
        }

        public void exportToPdf()
        {
            throw new NotImplementedException();
        }

        public void downloadData()
        {
            throw new NotImplementedException();
        }

        public void applyFilters()
        {
            throw new NotImplementedException();
        }
        #endregion
        #region lnkbtnPager Click
        protected void lnkbtnPager_Click(object sender, EventArgs e)
        {
            try
            {

                LinkButton pagebuttton = new LinkButton();
                pagebuttton = (LinkButton)sender;
                PageSortBO objPageSortBo = new PageSortBO("DESC", "PROVISIONID", 1, 20);
                GridHelper.processGridPager(sender, objPageSortBo, pageSortViewState);
                loadData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        protected void btnFilter_Click(object sender, EventArgs e)
        {
            string provisionName = String.IsNullOrWhiteSpace(txtProvisionDescription.Text) ? string.Empty : txtProvisionDescription.Text.ToString();
            string schemeName = String.IsNullOrWhiteSpace(txtSchemeName.Text) ? string.Empty : txtSchemeName.Text.ToString();
            string blockName = String.IsNullOrWhiteSpace(txtBlockName.Text) ? string.Empty : txtBlockName.Text.ToString();

            StringBuilder filterString = new StringBuilder();
            if (provisionName != string.Empty)
                filterString.Append(string.Format("[{0}] LIKE '%{1}%' ", "PROVISIONDESCRIPTION", provisionName));
            if (schemeName != string.Empty)
            {
                if (filterString.Length > 0)
                    filterString.Append(string.Format(" AND [{0}] LIKE '%{1}%' ", "SCHEMENAME", schemeName));
                else
                    filterString.Append(string.Format(" [{0}] LIKE '%{1}%' ", "SCHEMENAME", schemeName));
            }
            if (blockName != string.Empty)
            {
                if (filterString.Length > 0)
                    filterString.Append(string.Format(" AND [{0}] LIKE '%{1}%' ", "BLOCKNAME", blockName));
                else
                    filterString.Append(string.Format(" [{0}] LIKE '%{1}%' ", "BLOCKNAME", blockName));
            }
            if (objSession.ProvisionListDS != null)
            {
                if (filterString.Length > 0)
                {
                    DataTable gridTable = (DataTable)objSession.ProvisionListDS.Tables[0];
                    gridTable.DefaultView.RowFilter = filterString.ToString();
                    grdProvisionsList.DataSource = gridTable;
                }
                else
                {
                    grdProvisionsList.DataSource = objSession.ProvisionListDS;
                }
                grdProvisionsList.DataBind();
            }
            else
            {
                loadData();
            }

        }
        protected void grdProvisionsList_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                PageSortBO objPageSortBo;
                if (pageSortViewState == null)
                {
                    objPageSortBo = new PageSortBO("DESC", "PROVISIONID", 1, 20);
                    pageSortViewState = objPageSortBo;
                }
                else
                {
                    objPageSortBo = pageSortViewState;
                }
                pageSortViewState = GridHelper.sortGrid(grdProvisionsList, e, objPageSortBo, pageSortViewState);
                loadData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }

        #region"change PageNumber"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void changePageNumber(object sender, EventArgs e)
        {
            try
            {
                Button btnGo = new Button();
                btnGo = (Button)sender;
                PageSortBO objPageSortBo;
                objPageSortBo = new PageSortBO("DESC", "PROVISIONID", 1, 20);
                objPageSortBo = pageSortViewState;
                int pageNumber = 1;
                pageNumber = Convert.ToInt32(txtPageNumber.Text);
                txtPageNumber.Text = String.Empty;
                objPageSortBo = pageSortViewState;
                if (((pageNumber >= 1)
                            && (pageNumber <= objPageSortBo.TotalPages)))
                {
                    objPageSortBo = pageSortViewState;
                    objPageSortBo.PageNumber = pageNumber;
                    pageSortViewState = objPageSortBo;
                    loadData();

                }
                else
                {
                    // uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, UserMessageConstants.InvalidPageNumber, true);
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        protected void grdProvisionsList_RowCreated(object sender, GridViewRowEventArgs e)
        {
            //check if it is a header row
            //since allowsorting is set to true, column names are added as command arguments to
            //the linkbuttons by DOTNET API
            if (e.Row.RowType == DataControlRowType.Header)
            {
                LinkButton btnSort;
                Image image;
                //iterate through all the header cells
                foreach (TableCell cell in e.Row.Cells)
                {
                    //check if the header cell has any child controls
                    if (cell.HasControls())
                    {
                        //get reference to the button column
                        btnSort = (LinkButton)cell.Controls[0];
                        image = new Image();
                        if (pageSortViewState != null)
                        {
                            if (btnSort.CommandArgument == pageSortViewState.SortExpression)
                            {
                                //following snippet figure out whether to add the up or down arrow
                                //based on the sortdirection
                                if (pageSortViewState.SortDirection == SortDirection.Ascending.ToString())
                                {
                                    image.ImageUrl = "~/Images/Grid/sort_asc.png";
                                }
                                else
                                {
                                    image.ImageUrl = "~/Images/Grid/sort_desc.png";
                                }
                            }
                            else
                            {
                                image.ImageUrl = "~/Images/Grid/sort_both.png";
                            }
                            cell.Controls.Add(image);
                        }
                    }
                }
            }
        }

        protected void btnExportToExcel_Click(object sender, EventArgs e)
        {
            try
            {
                uiMessage.hideMessage();
                this.exportGridToExcel();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        private string GetFilterString()
        {
            string provisionName = String.IsNullOrWhiteSpace(txtProvisionDescription.Text) ? string.Empty : txtProvisionDescription.Text.ToString();
            string schemeName = String.IsNullOrWhiteSpace(txtSchemeName.Text) ? string.Empty : txtSchemeName.Text.ToString();
            string blockName = String.IsNullOrWhiteSpace(txtBlockName.Text) ? string.Empty : txtBlockName.Text.ToString();

            StringBuilder filterString = new StringBuilder();
            if (provisionName != string.Empty)
                filterString.Append(string.Format("  {0} LIKE '%{1}%' ", "PROVISIONNAME", provisionName));
            if (schemeName != string.Empty)
            {
                if (filterString.Length > 0)
                    filterString.Append(string.Format(" AND {0} LIKE '%{1}%' ", "SCHEMENAME", schemeName));
                else
                    filterString.Append(string.Format(" {0} LIKE '%{1}%' ", "SCHEMENAME", schemeName));
            }
            if (blockName != string.Empty)
            {
                if (filterString.Length > 0)
                    filterString.Append(string.Format(" AND {0} LIKE '%{1}%' ", "BLOCKNAME", blockName));
                else
                    filterString.Append(string.Format("  {0} LIKE '%{1}%' ", "BLOCKNAME", blockName));
            }

            return filterString.ToString();
        }
        protected void lblExPropCount_Click(object sender, EventArgs e)
        {

            Button button = (Button)sender;
            var commArg = button.CommandArgument.Split(';');
            DataSet resultSet = new DataSet();
            resultSet = objProvisionBL.getExPropertiesBySchemeBlock(Convert.ToInt32(commArg[0]), Convert.ToInt32(commArg[1]), Convert.ToInt32(commArg[2]));
            if (resultSet.Tables[0].Rows.Count > 0)
            {
                noRecord.Visible = false;
            }
            else
            {
                noRecord.Visible = true;
            }
            PropertiesRepeater.DataSource = resultSet.Tables[0];
            PropertiesRepeater.DataBind();
            mdlPopupShowProperties.Show();
        }

        private void LoadCategory()
        {
            DataSet provisionCategoriesDS = objProvisionBL.GetProvisionCategories();
            //DataSet provisionCategoriesDS = objSession.ProvisionListDS;
            ddlCategory.DataSource = provisionCategoriesDS;
            ddlCategory.DataValueField = "ProvisionCategoryId";
            ddlCategory.DataTextField = "CategoryName";
            ddlCategory.DataBind();
            ddlCategory.Items.Insert(0, new ListItem("Please Select", "0"));
            ddlCategory.SelectedIndex = 0;
        }

        protected void ddlCategory_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            loadData();
        }
    }
}