﻿<%@ Page Title="" Language="C#" Culture="en-GB" MasterPageFile="~/MasterPage/Pdr.Master"
    AutoEventWireup="true" CodeBehind="ComplianceDocuments.aspx.cs" Inherits="PropertyDataRestructure.Views.Reports.ComplianceDocuments" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<asp:Content runat="server" ID="Documents" ContentPlaceHolderID="ContentPlaceHolder1">
    <script type="text/javascript">
        function startDownload(documentId) {
            var ddlReportFor = document.getElementById("<%=ddlReportFor.ClientID %>");
            var selectedText = ddlReportFor.options[ddlReportFor.selectedIndex].innerHTML;
            window.open("../../Views/Common/Download.aspx?documentId=" + documentId + "&type=" + selectedText);
        }
    </script>
    <asp:UpdateProgress ID="updateProgressReportsArea" runat="server" DisplayAfter="10">
        <ProgressTemplate>
            <div class="loading-image">
                 <img alt="Please Wait" src="../../Images/menu/progress.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel runat="server" ID="updPanelSearch">
        <ContentTemplate>
       
    <div style="padding: 10px 10px; background-color: #000; font-family: Arial, Helvetica, sans-serif;
        font-size: 16px; color: #fff;">
        <asp:Label Text="Compliance Documents Report" ID="lblHeading" runat="server" />
    </div>
    <hr style="margin-top: 0.5em" />
    <p>
        <span>&nbsp;</span><asp:Label Text="Development/Scheme/Property : " runat="server"></asp:Label>
        <span>
            <asp:DropDownList ID="ddlReportFor" runat="server" OnSelectedIndexChanged="ddlReportFor_SelectedIndexChanged" AutoPostBack="true">
                <asp:ListItem Value="Development">Development</asp:ListItem>
                <asp:ListItem Value="Scheme">Scheme</asp:ListItem>
                <asp:ListItem Value="Property">Property</asp:ListItem>
            </asp:DropDownList>
        </span>
    </p>
    <div style="border: 1px solid black; padding: 10px; width: 75%; margin-left: 3px;">
        <table style="line-height: 25px; white-space: normal; width: 100%">
            <tr>
                <td>
                    Category:
                </td>
                <td>
                    <asp:DropDownList ID="ddlCategory" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged" runat="server" AutoPostBack="true" Width="100px">
                    </asp:DropDownList>
                </td>
                <td>
                </td>
                <td style="padding-left: 46px;">
                    Type:
                </td>
                <td>
                    <asp:DropDownList ID="ddlType" runat="server" class="selectval" CssClass="selectval"
                        AutoPostBack="True" OnSelectedIndexChanged="ddlType_SelectedIndexChanged" Width="180px">
                    </asp:DropDownList>
                </td>
                <td>
                    Title:
                </td>
                <td>
                    <asp:DropDownList ID="ddlTitle" runat="server" class="selectval" CssClass="selectval"
                       Width="180px">
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
        <table>
            <br>
            </br>
            <tr>
                <td style="padding-right: 20px">
                    <asp:Label ID="Label1" Text="Date :" runat="server"></asp:Label>
                </td>
                <td>
                    <asp:RadioButtonList ID="rblDateType" runat="server" AutoPostBack="false" RepeatDirection="Horizontal"
                        CssClass="date-type-radio">
                        <asp:ListItem Text="Document" Value="Document" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Expiry" Value="Expiry" style="padding-left: 20px"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
                <td style="padding-left: 38px;">
                    <asp:Label ID="Label2" Text="From : " runat="server"></asp:Label>
                </td>
                <td>
                    <span>
                        <asp:TextBox runat="server" ID="txtFromDate" 
                            Style="width: 93px !important;" AutoPostBack="false"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender runat="server" ID="CalendarExtender2" TargetControlID="txtFromDate"
                            PopupButtonID="imgFromDate" PopupPosition="Right" TodaysDateFormat="dd/MM/yyyy"
                            Format="dd/MM/yyyy">
                        </ajaxToolkit:CalendarExtender>
                        <asp:ImageButton ImageUrl="~/Images/calendar.png" runat="server" ID="imgFromDate"
                            Style="vertical-align: bottom; border: none;" />
                    </span>
                </td>
                <td style="padding-left: 20px">
                    <asp:Label ID="Label3" Text="To : " runat="server"></asp:Label>
                </td>
                <td>
                    <span>
                        <asp:TextBox runat="server" ID="txtToDate"
                            Style="width: 93px !important;" AutoPostBack="false"></asp:TextBox>
                        <ajaxToolkit:CalendarExtender runat="server" ID="CalendarExtender1" TargetControlID="txtToDate"
                            PopupButtonID="imgToDate" PopupPosition="Right" TodaysDateFormat="dd/MM/yyyy"
                            Format="dd/MM/yyyy">
                        </ajaxToolkit:CalendarExtender>
                        <asp:ImageButton ImageUrl="~/Images/calendar.png" runat="server" ID="imgToDate" Style="vertical-align: bottom;
                            border: none;" />
                    </span>
                </td>
                <td>
                    <asp:Label ID="lblDateFormatError" runat="server" Style="color: Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                </td>
            </tr>
        </table>
        <div style="background-color: lightgray; padding-top: 10px; margin-top: 5px; width: 100%;
            height: 35px; margin-left: -10px; width: 102.6%;">
            <asp:RadioButtonList RepeatLayout="Flow" runat="server" ID="rblUploadedOrNot" RepeatDirection="Horizontal"
                AutoPostBack="false" >
                <asp:ListItem Selected="True" Text="Uploaded" Value="Uploaded"></asp:ListItem>
                <asp:ListItem Text="Not Uploaded" Value="NotUploaded"></asp:ListItem>
            </asp:RadioButtonList>
            <asp:TextBox runat="server" Columns="60" ID="txtQuickFind" placeholder="Quick Find ....."
                Style="margin-left: 5%;"></asp:TextBox>
            <asp:Button runat="server" ID="btnSearch" Text="Go" OnClick="btnSearch_Clicked" Style="margin-left: 3%;" />
        </div>
    </div>
     </ContentTemplate>
    </asp:UpdatePanel>

    <asp:UpdatePanel runat="server" ID="updPanelAppointmentToBeArranged">
        <ContentTemplate>
            <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" />
            <div style="height: 580px; overflow: auto;">
                <cc1:PagingGridView ID="grdDocuments" runat="server" AllowPaging="false" AllowSorting="true"
                    AutoGenerateColumns="False" BorderStyle="Solid" BorderWidth="0px" CellPadding="4"
                    GridLines="None" PageSize="10" Width="100%" PagerSettings-Visible="false" OnSorting="grdDocuments_Sorting"
                    HeaderStyle-Font-Underline="false" ShowHeaderWhenEmpty="true" >
                    <columns>
                    <asp:TemplateField HeaderText="Added" SortExpression="Added">
                        <ItemTemplate>
                            <asp:Label ID="lblAdded" runat="server" Text='<%# Bind("Added") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Type" SortExpression="Type">
                        <ItemTemplate>
                            <asp:Label ID="lblType" runat="server" Text='<%# Bind("Type") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Title" SortExpression="Title">
                        <ItemTemplate>
                            <asp:Label ID="lblTitle" runat="server" Text='<%# Bind("Title") %>'></asp:Label>
                        </ItemTemplate>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="By" SortExpression="By">
                        <ItemTemplate>
                            <asp:Label ID="lblBy" runat="server" Text='<%# Bind("By") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Document" SortExpression="Document">
                        <ItemTemplate>
                            <asp:Label ID="lblDocument" runat="server" Text='<%# Bind("Document") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Expiry" SortExpression="Expiry">
                        <ItemTemplate>
                            <asp:Label ID="lblExpiry" runat="server" Text='<%# Bind("Expiry") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Address" SortExpression="Address">
                        <ItemTemplate>
                            <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Postcode" SortExpression="Postcode">
                        <ItemTemplate>
                            <asp:Label ID="lblPostcode" runat="server" Text='<%# Bind("Postcode") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                
                <asp:TemplateField>
                    <ItemTemplate>
                       <asp:Button ID="btnViewDocument" runat="server" Text="View" UseSubmitBehavior="False" Enabled='<%# isViewEnabled %>'
                            OnClientClick='<%# "startDownload("+Eval("documentId")+");return false;" %>' />
                            
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right" />
                </asp:TemplateField>

                </columns>
                    <selectedrowstyle backcolor="#D1DDF1" font-bold="True" forecolor="#333333" />
                    <headerstyle backcolor="#ffffff" cssclass="table-head" font-bold="True" forecolor="black"
                        horizontalalign="Left" font-underline="false" />
                    <alternatingrowstyle backcolor="#E8E9EA" wrap="True" />
                </cc1:PagingGridView>
            </div>
            <asp:Panel runat="server" ID="pnlPagination" Visible="true" Style="border-top: 1px solid  #000;
                vertical-align: middle; padding: 10px 0">
                <table style="width: 57%; margin: 0 auto">
                    <tbody>
                        <tr>
                            <td>
                                <asp:LinkButton ID="lnkbtnPagerFirst" Text="" runat="server" CommandName="Page" CommandArgument="First"
                                    OnClick="lnkbtnPager_Click" Style="text-decoration: none;">&lt;&lt;First</asp:LinkButton>
                                &nbsp;
                            </td>
                            <td>
                                <asp:LinkButton ID="lnkbtnPagerPrev" Text="" runat="server" CommandName="Page" CommandArgument="Prev"
                                    OnClick="lnkbtnPager_Click" Style="text-decoration: none;">&lt;Prev</asp:LinkButton>
                                &nbsp;
                            </td>
                            <td>
                                Page:&nbsp;
                                <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="Required" />
                                &nbsp;of&nbsp;
                                <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />. Records:
                                <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                                &nbsp;to&nbsp;
                                <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                                &nbsp;of&nbsp;
                                <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                            </td>
                            <td>
                                <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page"
                                    CommandArgument="Next" OnClick="lnkbtnPager_Click" Style="text-decoration: none;" />
                                &nbsp;
                            </td>
                            <td>
                                <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page"
                                    CommandArgument="Last" OnClick="lnkbtnPager_Click" Style="text-decoration: none;" />
                                &nbsp;
                            </td>
                            <td align="right">
                                <asp:Button Text="Export to XLS" runat="server" ID="btnExportToExcel" OnClick="btnExportToExcel_Click" />
                            </td>
                        </tr>
                    </tbody>
                </table>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportToExcel" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
