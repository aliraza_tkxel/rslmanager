﻿<%@ Page Title="" Language="C#" Culture="en-GB" MasterPageFile="~/MasterPage/Pdr.Master"
    AutoEventWireup="true" CodeBehind="ProvisioningReport.aspx.cs" Inherits="PropertyDataRestructure.Views.Reports.ProvisioningReport" %>

<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .dashboard th
        {
            background: #fff;
            border-bottom: 4px solid #8b8687;
        }
        .dashboard th a
        {
            color: #000 !important;
        }
        .dashboard th img
        {
            float: right;
        }
        .text_div
        {
            width: 265px;
        }
        .text_div input
        {
            width: 115px !important;
        }
        hr
        {
            /* color: #848484; */
            background-color: #CCCCCC;
            border: 0 none;
            color: #A0A0A0;
            height: 1px;
            margin: 0px;
        }
    </style>
</asp:Content>
<asp:Content ID="ContentProvisioningReport" ContentPlaceHolderID="ContentPlaceHolder1"
    runat="server">
    <asp:UpdateProgress ID="updateProgressReportsArea" runat="server" DisplayAfter="10">
        <ProgressTemplate>
            <div class="loading-image">
                <img alt="Please Wait" src="../../../Images/menu/progress.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="400px" />
    <div class="portlet">
        <div class="header">
            <span class="header-label">Provisioning Report</span>
        </div>
        <div class="portlet-body" style="font-size: 12px; overflow: inherit; padding-bottom: 0;">
            <asp:UpdatePanel ID="updPnlProvisionsList" runat="server">
                <ContentTemplate>
                    <div style="overflow: auto;">
                        <div class="form-control" style="width:200px">
                            <div class="text_div">
                                <div class="label">
                                    <asp:Label ID="lblSchemeNam" runat="server" Text="Scheme:" />
                                </div>
                                <div class="field">
                                    <asp:TextBox ID="txtSchemeName" runat="server" placeholder="Scheme Name"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="form-control" style="width:200px">
                            <div class="text_div">
                                <div class="label">
                                    <asp:Label ID="lblBlockName" runat="server" Text="Block:" />
                                </div>
                                <div class="field">
                                    <asp:TextBox ID="txtBlockName" runat="server" placeholder="Block Name"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="form-control" style="width:200px">
                            <div class="text_div">
                                <div class="label">
                                    <asp:Label ID="lblProvisionDescription" runat="server" Text="Provision:" />
                                </div>
                                <div class="field">
                                    <asp:TextBox ID="txtProvisionDescription" runat="server" placeholder="Provision"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="form-control">
                            <div class="select_div">
                                <div class="label">
                                    <asp:Label ID="lblCategory" runat="server" Text="Category:" />
                                </div>
                                <div class="field right">
                                    <asp:DropDownList runat="server" ID="ddlCategory" AutoPostBack="True"
                                     OnSelectedIndexChanged="ddlCategory_OnSelectedIndexChanged" />
                                </div>
                            </div>
                        </div>
                        <div class="form-control right">
                            <div class="select_div right" style="padding-top: 12px;">
                                <div class="field right" style="margin-left: 0;">
                                    <asp:Button ID="btnFilter" Text="Filter" runat="server" UseSubmitBehavior="true"
                                        OnClick="btnFilter_Click" CssClass="btn btn-xs btn-blue right" Style="padding: 3px 23px !important;
                                        margin: -3px 0 0 0;" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <div style="border-bottom: 1px solid #A0A0A0; width: 100%; padding: 0">
                        <cc1:PagingGridView ID="grdProvisionsList" runat="server" AutoGenerateColumns="False"
                            EmptyDataText="No Records Found" OnSorting="grdProvisionsList_Sorting" OnRowCreated="grdProvisionsList_RowCreated"
                            Width="100%" Style="overflow: scroll" BorderWidth="0px" CssClass="dashboard webgrid table table-responsive"
                            GridLines="None" ShowHeaderWhenEmpty="True" AllowPaging="false" AllowSorting="True"
                            PageSize="20">
                            <Columns>
                                <asp:TemplateField HeaderText="Scheme Name:" ItemStyle-CssClass="dashboard" SortExpression="SCHEMENAME">
                                    <ItemTemplate>
                                        <asp:Label ID="lblScheme" runat="server" Text='<%# Bind("SCHEMENAME") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle BorderStyle="None" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Block Name:" SortExpression="BLOCKNAME">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBlockName" runat="server" Text='<%# Bind("BLOCKNAME") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                
                                <asp:TemplateField HeaderText="Replacement Due:" SortExpression="REPLACEMENTDUE">
                                    <ItemTemplate>
                                        <asp:Label ID="lblReplacementDue" runat="server" Text='<%# Bind("REPLACEMENTDUE") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                </asp:TemplateField>                                                               

                                <asp:TemplateField HeaderText="Category:" SortExpression="CATEGORYNAME">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCategory" runat="server" Text='<%# Bind("CATEGORYNAME") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Description:" SortExpression="PROVISIONDESCRIPTION">
                                    <ItemTemplate>
                                        <asp:Label ID="lblProvisionName" runat="server" Text='<%# Bind("PROVISIONDESCRIPTION") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="No. of Properties (Included) :" SortExpression="InPropCount">
                                    <ItemTemplate>
                                        <asp:Label ID="lblIncludedCount" runat="server" Text='<%# Bind("InPropCount")  %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Budget :" SortExpression="BUDGET">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBUDGET" runat="server" Text='<%# Bind("BUDGET") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Apportiontment (Budget):" SortExpression="APPORTIONMENTBUDGET">
                                    <ItemTemplate>
                                        <asp:Label ID="lblApportionmentBudget" runat="server" Text='<%# Bind("APPORTIONMENTBUDGET") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Actual (Total):" SortExpression="ActualTotal">
                                    <ItemTemplate>
                                        <asp:Label ID="lblActualTotal" runat="server" Text='<%# Bind("ACTUALTOTAL") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Apportiontment (Actual):" SortExpression="APPORTIONMENTACTUAL">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAppointmentActual" runat="server" Text='<%# string.Format("{0:n2}",Eval("APPORTIONMENTACTUAL")) %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Excluded Properties:" SortExpression="ExPropCount">
                                    <ItemTemplate>
                                        <div style="color: blue; cursor: pointer;">
                                            <asp:Button ID="btnExcluded" runat="server" UseSubmitBehavior="false" Text='<%# Bind("ExPropCount") %>'
                                                CommandArgument='<%# Eval("SCHEMEID").ToString()+";"+Eval("BLOCKID").ToString()+";"+Eval("ItemID").ToString()%>'
                                                CssClass="btn btn-xs btn-blue right" OnClick="lblExPropCount_Click" Style="padding: 3px 23px !important;
                                                margin: -3px 0 0 0;"></asp:Button>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle BorderColor="Black" HorizontalAlign="Left" />
                        </cc1:PagingGridView>
                    </div>
                    <asp:Panel runat="server" ID="pnlPagination" Visible="false" Style="color: Black;
                        margin: 0 auto; width: 98%;">
                        <div style="width: 100%; padding: 15px 0 30px 0px; text-align: center;">
                            <div class="paging-left">
                                <span style="padding-right: 10px;">
                                    <asp:LinkButton ID="lnkbtnPagerFirst" runat="server" Text="" CommandName="Page" CommandArgument="First"
                                        CssClass="lnk-btn" OnClick="lnkbtnPager_Click">
                                        &lt;&lt;First
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkbtnPagerPrev" Text="Prev" runat="server" CommandName="Page"
                                        CommandArgument="Prev" CssClass="lnk-btn" OnClick="lnkbtnPager_Click">
                                        &lt;Prev
                                    </asp:LinkButton>
                                </span><span style="padding-right: 10px;"><b>Page:</b>
                                    <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="currentPage" />
                                    of
                                    <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />. </span><span style="padding-right: 20px;">
                                        <b>Result:</b>
                                        <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                                        to
                                        <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                                        of
                                        <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                                    </span><span style="padding-right: 10px;">
                                        <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page"
                                            CommandArgument="Next" CssClass="lnk-btn" OnClick="lnkbtnPager_Click">
                                    
                                        </asp:LinkButton>
                                        <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page"
                                            CommandArgument="Last" CssClass="lnk-btn" OnClick="lnkbtnPager_Click">
                                        
                                        </asp:LinkButton>
                                    </span>
                            </div>
                            <div style="float: right;">
                                <span>
                                    <asp:Button ID="btnExportToExcel" runat="server" Text="Export to XLS" UseSubmitBehavior="False"
                                        class="btn btn-xs btn-blue right" Style="padding: 1px 5px !important;" OnClick="btnExportToExcel_Click" />
                                </span>
                                <asp:RangeValidator ID="rangevalidatorPageNumber" runat="server" ErrorMessage="Enter a page between 1 and "
                                    ValidationGroup="pageNumber" MinimumValue="1" MaximumValue="1" ControlToValidate="txtPageNumber"
                                    Type="Integer" SetFocusOnError="True" CssClass="Required" Display="Dynamic" />
                                <div class="field" style="margin-right: 10px;">
                                    <asp:TextBox ID="txtPageNumber" runat="server" Width="40px" ValidationGroup="pageNumber"
                                        PlaceHolder="Page" onchange="document.getElementById('txtPageNumberHidden').value = this.value;" />
                                </div>
                                <span>
                                    <asp:Button Text="Go" runat="server" ID="btnGoPageNumber" ValidationGroup="pageNumber"
                                        UseSubmitBehavior="false" class="btn btn-xs btn-blue" Style="padding: 1px 5px !important;
                                        margin-right: 10px; min-width: 0px;" OnClick="changePageNumber" />
                                </span>
                            </div>
                        </div>
                    </asp:Panel>
                    <asp:Button ID="btnHiddenShowProperties" runat="server" Text="" Style="display: none;" />
                    <asp:ModalPopupExtender ID="mdlPopupShowProperties" runat="server" TargetControlID="btnHiddenShowProperties"
                        PopupControlID="pnlShowProperties" Enabled="true" DropShadow="true" BackgroundCssClass="modalBackground">
                    </asp:ModalPopupExtender>
                    <asp:Panel ID="pnlShowProperties" runat="server" CssClass="modalPopup" Style="width: 500px;
                        height: 600px; overflow: auto;">
                        <asp:ImageButton ID="imgBtnCancelAddWarrantyPopup" runat="server" Style="position: absolute;
                            top: -1px; right: -1px; width: 22px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png"
                            BorderWidth="0" />
                        <asp:Label ID="Label1" runat="server" Font-Bold="true" Font-Size="Medium">Excluded Properties</asp:Label>
                        <hr class="line" />
                        <asp:Repeater ID="PropertiesRepeater" runat="server">
                            <HeaderTemplate>
                                <ul>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <li>
                                    <asp:Label runat="server" ID="Label1" Text='<%# Eval("ADDRESS") %>' />
                                </li>
                            </ItemTemplate>
                            <FooterTemplate>
                                </ul>
                            </FooterTemplate>
                        </asp:Repeater>
                        <asp:Label ID="noRecord" runat="server" Visible="false">No Record Found</asp:Label>
                    </asp:Panel>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnExportToExcel" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
