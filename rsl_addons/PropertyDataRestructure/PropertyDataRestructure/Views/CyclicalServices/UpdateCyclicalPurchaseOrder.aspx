﻿<%@ Page Title="" Language="C#" Culture="en-GB" MasterPageFile="~/MasterPage/Blank.Master" AutoEventWireup="true" EnableEventValidation="false"
    CodeBehind="UpdateCyclicalPurchaseOrder.aspx.cs" Inherits="PropertyDataRestructure.Views.CyclicalServices.UpdateCyclicalPurchaseOrder" %>
    
<%@ Register TagPrefix="uc1" TagName="PurchaseOrder" Src="~/UserControls/CyclicalServices/UpdatePurchaseOrder.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Styles/masterpage.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Site.css" rel="stylesheet" media="screen" />
    <link href="../../Styles/masterpage.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Navigation.css" rel="stylesheet" media="screen" />
    <link href="../../Styles/Dashboard.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .menue
        {
            width: 98%; /*   margin-top: 1px;*/
            background-image: url(../../Images/menu/m-bg.png);
            height: 63px;
            float: left;
            color: #cdcdcd;
            padding-left: 10px;
        }
    </style>
    <script language="javascript" type="text/javascript">
    function PrintPage(id) {
       
        var printContent = document.getElementById(id);
        var printWindow = window.open("All Records", "Print Panel", 'left=50000,top=50000,width=0,height=0');
        printWindow.document.write(printContent.innerHTML);
        printWindow.document.close();
        printWindow.focus();
        printWindow.print();
    }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdateProgress ID="updateProgressReportsArea" runat="server" DisplayAfter="10">
        <ProgressTemplate>
            <div class="loading-image">
                <img alt="Please Wait" src="../../Images/menu/progress.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="updPanelPurchaseOrder" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <div id="superdiv">
                <div class="menue"> <div class="menue-select">Cyclical Works
                </div></div>
                <div>
                    <uc1:PurchaseOrder ID="ucPurchaseOrder" runat="server" Visible="true"></uc1:PurchaseOrder>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
