﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Blank.Master" AutoEventWireup="true"
    CodeBehind="AllocatedServices.aspx.cs" Inherits="PropertyDataRestructure.Views.CyclicalServices.AllocatedServices" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../../Scripts/jquery-1.12.4.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-ui-1.12.1.js" type="text/javascript"></script>
    <%--    <script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/select/1.2.2/js/dataTables.select.min.js"
        type="text/javascript"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
        type="text/javascript"></script>--%>
    <script src="../../Scripts/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="../../Scripts/dataTables.bootstrap.min.js" type="text/javascript"></script>
    <script src="../../Scripts/dataTables.select.min.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery.dialog.min.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.2.2/css/select.dataTables.min.css">
    <link href="../../Styles/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/dialog.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/cyclical.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/Allocated.js" type="text/javascript"></script>
    <link href="../../Styles/jquery.dialog.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        jQuery.ajaxSetup({
            cache: false,
            beforeSend: function () {
                $('#loader').show();
            },
            complete: function () {
                $('#loader').hide();
            },
            success: function () { }
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="border: 1px solid #C2C2C2; min-height: 672px; margin-top: -1px;">
        <table id="dtAllocated" style="width: 100%;">
            <thead>
                <tr>
                    <th>
                        ServiceItemId
                    </th>
                    <th>
                        PO Ref
                    </th>
                    <th>
                        Scheme
                    </th>
                    <th>
                        Block
                    </th>
                    <th>
                        Post Code
                    </th>
                    <th>
                        Commencement
                    </th>
                    <th>
                        Contractor
                    </th>
                    <th>
                        Contract Start/End
                    </th>
                    <th>
                        Service required
                    </th>
                    <th>
                        Status
                    </th>
                    <th>
                    </th>
                </tr>
            </thead>
        </table>
    </div>
    <div class="modal fade" id="AcceptPopUp" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12" style="min-height: 500px;">
                            <iframe id="acceptiFrame" src="" width="100%" height="500px" frameborder="0" style="border: block">
                            </iframe>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <button id="btnClose" class="btn btn-info btn-grey btn-xs pull-right" type="button">
                                Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
