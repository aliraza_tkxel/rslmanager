﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using System.Web.Script.Services;
using PDR_BusinessObject.CyclicalServices;
using PDR_BusinessLogic.CyclicalServices;
using PDR_DataAccess.CyclicalServices;
using System.Data;
using PDR_Utilities.Helpers;
using System.Web.Services;
using PDR_BusinessLogic.Scheduling;
using PDR_DataAccess.Scheduling;
using PDR_BusinessObject.DropDown;
using PDR_BusinessLogic.Scheme;
using PDR_DataAccess.Scheme;

namespace PropertyDataRestructure.Views.CyclicalServices
{
    public partial class ToBeAllocated : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        //#region Populate To Be Allocated.
        ///// <summary>    
        ///// GET: Default.aspx/GetData    
        ///// </summary>    
        ///// <returns>Return data</returns>    
        //[WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        //public static object PopulateToBeAllocated()
        //{
        //    // Initialization.    
        //    ToBeAllocatedResponseBO response = new ToBeAllocatedResponseBO();
        //    try
        //    {
        //        // Initialization.    
        //        string search = HttpContext.Current.Request.Params["search[value]"];
        //        string draw = HttpContext.Current.Request.Params["draw"];
        //        string order = HttpContext.Current.Request.Params["order[0][column]"];
        //        string orderDir = HttpContext.Current.Request.Params["order[0][dir]"];
        //        int startRec = Convert.ToInt32(HttpContext.Current.Request.Params["start"]);
        //        int pageSize = Convert.ToInt32(HttpContext.Current.Request.Params["length"]);

        //        int take = 0, skip = 0;
        //        if (startRec == 0)
        //        {
        //            take = pageSize * 1;
        //            skip = startRec;
        //        }
        //        else
        //        {
        //            take = pageSize * startRec;
        //            skip = startRec + 1;
        //        }

               

        //        DataSet resultDataset = new DataSet();
        //        CyclicalServicesBL objBl = new CyclicalServicesBL(new CyclicalServicesRepo());
        //        int total = objBl.getServicesToBeAllocated(ref resultDataset, search, skip, take, "CreatedDate", orderDir);
        //        response.draw = Convert.ToInt32(draw);
        //        response.recordsFiltered = total;
        //        response.recordsTotal = total;
        //        List<ServicesItemDetailBO> data = new List<ServicesItemDetailBO>();
        //        var schemeData = (from DataRow dr in resultDataset.Tables[0].Rows
        //                          select new ServicesItemDetailBO()
        //                          {
        //                              Attribute = dr["Attribute"].ToString(),
        //                              BlockId = (dr["BlockId"] == DBNull.Value ? 0 : Convert.ToInt32(dr["BlockId"])),
        //                              SchemeId = (dr["SchemeId"] == DBNull.Value ? 0 : Convert.ToInt32(dr["SchemeId"])),
        //                              ServiceItemId = (dr["ServiceItemId"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ServiceItemId"])),
        //                              StatusId = (dr["statusId"] == DBNull.Value ? 0 : Convert.ToInt32(dr["statusId"])),
        //                              ContractorId = (dr["ContractorId"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ContractorId"])),
        //                              BlockName = dr["BlockName"].ToString(),
        //                              Contractor = dr["Contractor"].ToString(),
        //                              CyclePeriod = dr["CyclePeriod"].ToString(),
        //                              PostCode = dr["PostCode"].ToString(),
        //                              SchemeName = dr["SchemeName"].ToString(),
        //                              Status = dr["Status"].ToString()
        //                          }).ToList();

        //        if (schemeData.Count > 0)
        //        {
        //            response.data = schemeData;
        //        }
        //        else
        //        {
        //            response.data = data;
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        // Info    
        //        Console.Write(ex);
        //    }
        //    // Return info.    
        //    return response;
        //}
        //#endregion
        //#region populate Attribute
        ///// <summary>
        ///// populate Attribute
        ///// </summary>
        ///// <returns></returns>
        //[WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        //public static object populateAttribute()
        //{
        //    SchedulingBL objSchedulingBl = new SchedulingBL(new SchedulingRepo());
        //    DataSet resultDs = new DataSet();
        //    objSchedulingBl.getAllAttributes(ref resultDs);
        //    List<Select2DropDownBO> response = new List<Select2DropDownBO>();
        //    var schemeData = (from DataRow dr in resultDs.Tables[0].Rows
        //                      select new Select2DropDownBO()
        //                      {
        //                          id = Convert.ToInt32(dr["ItemID"]),
        //                          text = dr["ItemName"].ToString()
        //                      }).ToList();
        //    if (schemeData.Count > 0)
        //    {
        //        response = schemeData;
        //    }

        //    return response;
        //}
        //#endregion


        //#region populate Scheme
        ///// <summary>
        ///// populate Attribute
        ///// </summary>
        ///// <returns></returns>
        //[WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        //public static object populateScheme()
        //{
        //    SchemeBL objSchemeBl = new SchemeBL(new SchemeRepo());
        //    DataSet resultDs = new DataSet();
        //    resultDs = objSchemeBl.populateSchemeDropDown();
        //    List<Select2DropDownBO> response = new List<Select2DropDownBO>();
        //    var schemeData = (from DataRow dr in resultDs.Tables[0].Rows
        //                      select new Select2DropDownBO()
        //                      {
        //                          id = Convert.ToInt32(dr["SCHEMEID"]),
        //                          text = dr["SCHEMENAME"].ToString()
        //                      }).ToList();
        //    if (schemeData.Count > 0)
        //    {
        //        response = schemeData;
        //    }

        //    return response;
        //}
        //#endregion

    }
}