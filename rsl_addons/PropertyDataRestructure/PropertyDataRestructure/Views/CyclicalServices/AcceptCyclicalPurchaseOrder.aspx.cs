﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PDR_BusinessLogic.UserAccessBL;
using PDR_Utilities.Managers;
using System.Data;
using PDR_BusinessLogic.PurchaseOrder;
using PDR_DataAccess.PurchaseOrder;
using PDR_DataAccess.UserAccess;
using PDR_Utilities.Constants;
using PDR_BusinessLogic.CyclicalServices;
using PDR_DataAccess.CyclicalServices;

namespace PropertyDataRestructure.Views.CyclicalServices
{
    public partial class AcceptCyclicalPurchaseOrder : System.Web.UI.Page
    {
        private int employeeId;
        private int orderID;
        protected void Page_Load(object sender, EventArgs e)
        {
            employeeId = int.Parse(Request.QueryString["uid"]);
            orderID = int.Parse(Request.QueryString["oid"]);
            string cancel = string.Empty;
            if (Request.QueryString["cancel"] != null)
            {
                cancel = Request.QueryString["cancel"].ToString();
            }
          
            if (!IsPostBack)
            {
                string status = checkStatus(orderID);
                if (status == ApplicationConstants.Assigned)
                {
                    populateData();
                }
                else if (status == "Accepted" || status == "Works Completed" || status == "Invoice Uploaded" || status == "Cancelled")
                {
                    if (Request.QueryString["IsReadOnly"] != null && Request.QueryString["IsReadOnly"] == "yes")
                    {
                        Response.Redirect("UpdateCyclicalPurchaseOrder.aspx?IsReadOnly=yes&oid=" + orderID.ToString() + "&uid=" + employeeId.ToString());
                    }
                    else
                    {
                        Response.Redirect("UpdateCyclicalPurchaseOrder.aspx?oid=" + orderID.ToString() + "&uid=" + employeeId.ToString() + "&cancel=" + cancel);
                    }
                }
                else
                {
                    Response.Redirect("~/Views/ReactiveRepairs/PageExpireMessage.aspx");
                }

            }
        }
        public void populateData()
        {
            UserAccessBL objUserAccessBL = new UserAccessBL(new UserAccessRepo());
            SessionManager objSessionManager = new SessionManager();
            DataSet resultDs = new DataSet();
            resultDs = objUserAccessBL.getEmployeeById(employeeId);
            if (resultDs.Tables[0].Rows.Count > 0)
            {
                objSessionManager.UserFullName = resultDs.Tables[0].Rows[0]["FullName"].ToString();
                objSessionManager.EmployeeId = Convert.ToInt32(resultDs.Tables[0].Rows[0]["EmployeeId"]);
                objSessionManager.UserType = Convert.ToString(resultDs.Tables[0].Rows[0]["UserType"]);
            }
            else
            { objSessionManager.EmployeeId = employeeId; }
            ucPurchaseOrder.Visible = true;
            ucPurchaseOrder.populateData(orderID, employeeId);
        }

        public string checkStatus(int Order_Id)
        {
            CyclicalServicesBL objBl = new CyclicalServicesBL(new CyclicalServicesRepo());
            return objBl.getPoStatusForSessionCreation(Order_Id);
        }
    }
}