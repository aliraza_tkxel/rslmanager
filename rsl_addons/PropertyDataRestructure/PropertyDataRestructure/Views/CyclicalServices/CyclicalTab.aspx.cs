﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PropertyDataRestructure.Base;
using PDR_Utilities.Managers;

namespace PropertyDataRestructure.Views.CyclicalServices
{
    public partial class CyclicalTab : System.Web.UI.Page
    {
        protected SessionManager objSession = new SessionManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            
            //if (!IsPostBack)
            //{
                hdnschemeId.Value = Request.QueryString["schemeId"].ToString();
                hdnblockId.Value = Request.QueryString["blockId"].ToString();
                hdnSession.Value = objSession.EmployeeId.ToString();
            //}
        }
    }
}