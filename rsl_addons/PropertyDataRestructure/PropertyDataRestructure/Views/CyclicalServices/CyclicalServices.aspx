﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Pdr.Master" AutoEventWireup="true"
    CodeBehind="CyclicalServices.aspx.cs" Inherits="PropertyDataRestructure.Views.CyclicalServices.CyclicalServices" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../../Scripts/jquery-1.12.4.min.js" type="text/javascript"></script>
    <%-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="https://cdn.datatables.net/1.10.10/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
     <script src="https://cdn.datatables.net/select/1.2.2/js/dataTables.select.min.js" type="text/javascript"></script>
    --%>
    <script src="../../Scripts/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="../../Scripts/dataTables.bootstrap.min.js" type="text/javascript"></script>
    <script src="../../Scripts/dataTables.select.min.js" type="text/javascript"></script>
    <link href="../../Styles/bootstrap.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/modernizr-2.6.2.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-ui-1.12.1.js" type="text/javascript"></script>
    <link href="../../Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/cyclical.css" rel="stylesheet" type="text/css" />
    <link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/ui-lightness/jquery-ui.css"
        rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        $(document).ready(function () {
            var aptArranged = getUrlVars()["aptArranged"];
            $('#tabs').tabs();
            if (aptArranged == "true") {
                $("#divContainer").load("../../Views/CyclicalServices/AllocatedServices.aspx");
                $('.tab-link').removeClass('ui-state-active');
                $('.tab-link').removeClass('ui-tabs-active');
                $('.tab-link').removeClass('active');
                //$('.tab-content').removeClass('current');
                $($('.tab-link')[1]).addClass('ui-state-active');
                $($('.tab-link')[1]).addClass('ui-tabs-active');
                $("#ui-id-2").addClass('active');
            }
            else {
                $("#divContainer").load("../../Views/CyclicalServices/ToBeAllocated.aspx");
            }

            //$("#mnuAllocated").load("../../Views/CyclicalServices/AllocatedServices.aspx");

            //$('.tabselect').click(function(){})
            function switchTabs(event, ui) {
                event.preventDefault();
            }


        });

        function getUrlVars() {
            var vars = [], hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');

            for (var i = 0; i < hashes.length; i++) {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        }

        $(document).on('click', '.tabselect', function (e) {
            var url = $(this).data('url');
            $('#divContainer').load(url);
            var tab_id = $(this).attr('data-tab');

            $('.tab-link').removeClass('ui-state-active');
            $('.tab-link').removeClass('ui-tabs-active');
            $('.tab-link').removeClass('active');
            //$('.tab-content').removeClass('current');

            $(this).parent().addClass('ui-state-active');

            $(this).parent().addClass('ui-tabs-active');
            $(this).parent().addClass('active');
            console.log(tab_id);
            $("#" + tab_id).addClass('active');
            //  e.preventDefault(); // prevent the default action
            //e.stopPropagation(); // stop the click from bubbling
            // $(this).closest('.tabs').find('.active').removeClass('active').addClass('inactive');
            //$(this).addClass('active').removeClass('inactive');
        });
  </script>
    <style type="text/css">
        .tabs ul
        {
            margin: 0px;
            padding: 0px;
            list-style: none;
        }
        .tabs ul li
        {
            background: none;
            color: #222;
            display: inline-block;
            padding: 10px 15px;
            cursor: pointer;
        }
        
        .tabs ul li.current
        {
            background: #ededed;
            color: #222;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   <div style="Border:1px solid  #8e8e8e; width:100%; margin-top:10px;">
    <p style="background-color: Black; height: 40px; text-align: justify; font-weight: bold;
        margin: 0 0 6px; font-size: 15px; padding: 8px;">
        <font color="white">
            <asp:Label runat="server" ID="lblHeading" Text="Cyclical Services"></asp:Label></font>
        <asp:HiddenField ID="hdnSession" runat="server" ClientIDMode="Static" />
    </p>

    <div class="loading-image" style="display: none;" id="loader">
        <img src="../../Images/menu/progress.gif" alt="In Progress" />
    </div>
    <div style="width:100%; padding:3px;">
    <div style="width: 100%;">
        <div id="tabs" style="padding: 0; width: 50%">
            <ul>
                <li class="tab-link active"><a href="#mnuTobeAllocated" class="ajaxTabs tabselect"
                    data-tab="tab-1" data-url="../../Views/CyclicalServices/ToBeAllocated.aspx">Services
                    to be Allocated:</a></li>
                <li class="tab-link"><a href="#mnuAllocated" class="ajaxTabs tabselect" data-tab="tab-2"
                    data-url="../../Views/CyclicalServices/AllocatedServices.aspx">Allocated Services:</a></li>
            </ul>
        </div>
        <div style="padding: 0; width: 50%">
        </div>
    </div>
    <div style="height: auto; margin: 0; padding: 0; width: 100%;">
        <div id="divContainer">
        </div>
        <div id="mnuTobeAllocated">
        </div>
        <div id="mnuAllocated">
        </div>
    </div></div></div>
</asp:Content>
