﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Services;
using System.Web.Script.Services;
using PDR_BusinessLogic.Scheme;
using System.Data;
using PDR_BusinessObject.DropDown;
using PDR_DataAccess.Scheme;
using PDR_BusinessLogic.Scheduling;
using PDR_BusinessLogic.CyclicalServices;
using PDR_BusinessObject.CyclicalServices;
using PDR_DataAccess.CyclicalServices;
using PDR_DataAccess.Scheduling;
using PropertyDataRestructure.Base;
using PDR_BusinessObject.Expenditure;
using PDR_BusinessLogic.AssignToContractor;
using PDR_DataAccess.AssignToContractor;
using PDR_Utilities.Constants;
using PDR_BusinessObject.Vat;

namespace PropertyDataRestructure.Views.CyclicalServices
{
    public partial class CyclicalServices : PageBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hdnSession.Value = objSession.EmployeeId.ToString();
            //CyclicalServicesBL objBl = new CyclicalServicesBL(new CyclicalServicesRepo());
            //objBl.sendEmailtoContractor(1025, 8, 1, 1);
        }

        #region Populate To Be Allocated.
        /// <summary>    
        /// GET: Default.aspx/GetData    
        /// </summary>    
        /// <returns>Return data</returns>    
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object PopulateToBeAllocated()
        {
            // Initialization.    
            ToBeAllocatedResponseBO response = new ToBeAllocatedResponseBO();
            try
            {
                // Initialization.    
                string search = HttpContext.Current.Request.Params["search[value]"];
                string draw = HttpContext.Current.Request.Params["draw"];
                string orderId = HttpContext.Current.Request.Params["order[0][column]"];
                string order = HttpContext.Current.Request.Params["columns[" + orderId + "][data]"];
                string orderDir = HttpContext.Current.Request.Params["order[0][dir]"];
                int startRec = Convert.ToInt32(HttpContext.Current.Request.Params["start"]);
                int pageSize = Convert.ToInt32(HttpContext.Current.Request.Params["length"]);
                int attributeId = Convert.ToInt32(HttpContext.Current.Request.Params["attributeId"]);
                int schemeId = Convert.ToInt32(HttpContext.Current.Request.Params["schemeId"]);

               
                //sortBy = model.columns[model.order[0].column].data;
                int take = 0, skip = 0;
                if (startRec == 0)
                {
                    take = pageSize * 1;
                    skip = startRec;
                }
                else
                {
                    take = pageSize * startRec;
                    skip = startRec + 1;
                }



                DataSet resultDataset = new DataSet();
                CyclicalServicesBL objBl = new CyclicalServicesBL(new CyclicalServicesRepo());
                int total = objBl.getServicesToBeAllocated(ref resultDataset, search, skip, take, order, orderDir, schemeId, attributeId);
                response.draw = Convert.ToInt32(draw);
                response.recordsFiltered = total;
                response.recordsTotal = total;
                List<ServicesItemDetailBO> data = new List<ServicesItemDetailBO>();
                var schemeData = (from DataRow dr in resultDataset.Tables[0].Rows
                                  select new ServicesItemDetailBO()
                                  {
                                      Attribute = dr["Attribute"].ToString(),
                                      BlockId = (dr["BlockId"] == DBNull.Value ? 0 : Convert.ToInt32(dr["BlockId"])),
                                      SchemeId = (dr["SchemeId"] == DBNull.Value ? 0 : Convert.ToInt32(dr["SchemeId"])),
                                      ServiceItemId = (dr["ServiceItemId"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ServiceItemId"])),
                                      StatusId = (dr["statusId"] == DBNull.Value ? 0 : Convert.ToInt32(dr["statusId"])),
                                      ContractorId = (dr["ContractorId"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ContractorId"])),
                                      BlockName = dr["BlockName"].ToString(),
                                      Contractor = dr["Contractor"].ToString(),
                                      CyclePeriod = dr["CyclePeriod"].ToString(),
                                      PostCode = dr["PostCode"].ToString(),
                                      SchemeName = dr["SchemeName"].ToString(),
                                      Status = dr["Status"].ToString(),
                                      Commencement = dr["Commencement"].ToString(),
                                      ContractStart = dr["ContractStart"].ToString(),
                                      VAT=dr["VAT"].ToString(),
                                      totalValue=(dr["TotalValue"]==DBNull.Value?0:Convert.ToInt32(dr["TotalValue"]))
                                  }).ToList();

                if (schemeData.Count > 0)
                {
                    response.data = schemeData;
                }
                else
                {
                    response.data = data;
                }

            }
            catch (Exception ex)
            {
                // Info    
                Console.Write(ex);
            }
            // Return info.    
            return response;
        }
        #endregion

        #region populate Attribute
        /// <summary>
        /// populate Attribute
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object populateAttribute()
        {
            SchedulingBL objSchedulingBl = new SchedulingBL(new SchedulingRepo());
            DataSet resultDs = new DataSet();
            objSchedulingBl.getAllAttributes(ref resultDs);
            List<Select2DropDownBO> response = new List<Select2DropDownBO>();
            var schemeData = (from DataRow dr in resultDs.Tables[0].Rows
                              select new Select2DropDownBO()
                              {
                                  id = Convert.ToInt32(dr["ItemID"]),
                                  text = dr["ItemName"].ToString()
                              }).ToList();
            if (schemeData.Count > 0)
            {
                response.Add(new Select2DropDownBO() { id = 0, text = "All" });
                response.AddRange(schemeData);
            }

            return response;
        }
        #endregion


        #region populate Scheme
        /// <summary>
        /// populate Attribute
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object populateScheme()
        {
            SchemeBL objSchemeBl = new SchemeBL(new SchemeRepo());
            DataSet resultDs = new DataSet();
            resultDs = objSchemeBl.populateSchemeDropDown();
            List<Select2DropDownBO> response = new List<Select2DropDownBO>();
            var schemeData = (from DataRow dr in resultDs.Tables[0].Rows
                              select new Select2DropDownBO()
                              {
                                  id = Convert.ToInt32(dr["SCHEMEID"]),
                                  text = dr["SCHEMENAME"].ToString()
                              }).ToList();
            if (schemeData.Count > 0)
            {
                response.Add(new Select2DropDownBO() { id = 0, text = "All" });
                response.AddRange(schemeData);
            }

            return response;
        }
        #endregion


        #region populate Cost Cenre
        /// <summary>
        /// populate Attribute
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object populateCostCentre()
        {

            DataSet resultDataset = new DataSet();
            CyclicalServicesBL objBl = new CyclicalServicesBL(new CyclicalServicesRepo());
            objBl.GetCostCentreDropDownVales(ref resultDataset);
            List<Select2DropDownBO> response = new List<Select2DropDownBO>();
            var schemeData = (from DataRow dr in resultDataset.Tables[0].Rows
                              select new Select2DropDownBO()
                              {
                                  id = Convert.ToInt32(dr["id"]),
                                  text = dr["description"].ToString()
                              }).ToList();
            if (schemeData.Count > 0)
            {
                //response.Add(new Select2DropDownBO() { id = 0, text = "All" });
                response.AddRange(schemeData);
            }

            return response;
        }
        #endregion

        #region populate Budget Head
        /// <summary>
        /// populate Attribute
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object populateBudgetHead(int costCentreId)
        {

            DataSet resultDataset = new DataSet();
            CyclicalServicesBL objBl = new CyclicalServicesBL(new CyclicalServicesRepo());
            objBl.GetBudgetHeadDropDownValuesByCostCentreId(ref resultDataset, costCentreId);
            List<Select2DropDownBO> response = new List<Select2DropDownBO>();
            var schemeData = (from DataRow dr in resultDataset.Tables[0].Rows
                              select new Select2DropDownBO()
                              {
                                  id = Convert.ToInt32(dr["id"]),
                                  text = dr["description"].ToString()
                              }).ToList();
            if (schemeData.Count > 0)
            {
                //response.Add(new Select2DropDownBO() { id = 0, text = "All" });
                response.AddRange(schemeData);
            }

            return response;
        }
        #endregion


        #region populate Expenditure
        /// <summary>
        /// populate Attribute
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object populateExpenditure(int budgetHeadId, int employeeId)
        {
            List<ExpenditureBO> expenditureBOList = new List<ExpenditureBO>();
            AssignToContractorBL objAssignToContractorBL = new AssignToContractorBL(new AssignToContractorRepo());
            objAssignToContractorBL.GetExpenditureDropDownValuesByBudgetHeadId(ref expenditureBOList, ref budgetHeadId, ref employeeId);


            List<Select2DropDownBO> response = new List<Select2DropDownBO>();
            var schemeData = (from ExpenditureBO ex in expenditureBOList
                              select new Select2DropDownBO()
                              {
                                  id = Convert.ToInt32(ex.Id),
                                  text = ex.Description.ToString()
                              }).ToList();
            if (schemeData.Count > 0)
            {

                response.AddRange(schemeData);
            }
            else
            {
                response.Add(new Select2DropDownBO() { id = 0, text = ApplicationConstants.NoExpenditureSetup });
            }

            return response;
        }
        #endregion

        #region populate Contractor Contact
        /// <summary>
        /// populate Attribute
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object populateContractorContact(int contractorId)
        {

            DataSet resultDataset = new DataSet();
            CyclicalServicesBL objBl = new CyclicalServicesBL(new CyclicalServicesRepo());
            objBl.GetContactDropDownValuesbyContractorId(ref resultDataset, contractorId);
            List<Select2DropDownBO> response = new List<Select2DropDownBO>();
            var contactData = (from DataRow dr in resultDataset.Tables[0].Rows
                               select new Select2DropDownBO()
                               {
                                   id = Convert.ToInt32(dr["id"]),
                                   text = dr["description"].ToString()
                               }).ToList();
            if (contactData.Count > 0)
            {
                response.AddRange(contactData);
            }
            else
            {
                response.Add(new Select2DropDownBO() { id = 0, text = ApplicationConstants.noContactFound });
            }
            return response;
        }
        #endregion


        #region "Get Vat Drop down Values - Vat Id as Value Field and Vat Name as text field."
        /// <summary>
        /// populate Attribute
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object PopulateVat()
        {

            List<VatBo> vatBoList = new List<VatBo>();

            AssignToContractorBL objAssignToContractorBL = new AssignToContractorBL(new AssignToContractorRepo());

            objAssignToContractorBL.getVatDropDownValues(ref vatBoList);
            List<Select2DropDownBO> response = new List<Select2DropDownBO>();
            var contactData = (from VatBo v in vatBoList
                               select new Select2DropDownBO()
                               {
                                   id = Convert.ToInt32(v.Id),
                                   text = v.Description
                               }).ToList();
            if (contactData.Count > 0)
            {
                response.AddRange(contactData);
            }
            return response;
        }
        #endregion

        #region "Populate Services detail"
        /// <summary>
        /// populate Attribute
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object PopulateServices(string serviceItemIds)
        {

            DataSet resultDataset = new DataSet();
            CyclicalServicesBL objBl = new CyclicalServicesBL(new CyclicalServicesRepo());
            objBl.getServiceItemDetails(ref resultDataset, serviceItemIds);
            List<CyclicalServicesDetailBO> response = new List<CyclicalServicesDetailBO>();
            var contactData = (from DataRow dr in resultDataset.Tables[0].Rows
                               select new CyclicalServicesDetailBO()
                               {
                                   serviceItemId = Convert.ToInt32(dr["ServiceItemId"]),
                                   scheme = dr["SchemeBlock"].ToString(),
                                   commencement = dr["Commencement"].ToString(),
                                   cycle = dr["Cycle"].ToString(),
                                   cycles = Convert.ToInt32(dr["Cycles"]),
                                   cycleValue = Convert.ToDouble(dr["CycleValue"]),
                                   value = Convert.ToDouble(dr["Value"]),
                                   cycleDays = Convert.ToInt32(dr["CycleDays"]),
                                   endDate = dr["EndDate"].ToString(),
                                   cycleType = dr["cycleType"].ToString(),
                                   cyclePeriod = Convert.ToInt32(dr["cyclePeriod"].ToString())
                               }).ToList();
            if (contactData.Count > 0)
            {
                response.AddRange(contactData);
            }

            return response;
        }
        #endregion

        #region Update Cycle Value
        /// <summary>
        /// populate Attribute
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static void UpdateCycleValue(double cycleValue, int serviceItemId)
        {
            DataSet resultDataset = new DataSet();
            CyclicalServicesBL objBl = new CyclicalServicesBL(new CyclicalServicesRepo());
            objBl.UpdateCycleValue(cycleValue, serviceItemId);

        }
        #endregion

        #region Update Cycle Value
        /// <summary>
        /// populate Attribute
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GeneratePo(GeneratePoRequestBO request)
        {
            DataSet resultDataset = new DataSet();
            CyclicalServicesBL objBl = new CyclicalServicesBL(new CyclicalServicesRepo());
            objBl.GeneratePo(request);
            return "Success";
        }
        #endregion

        #region Populate To Be Allocated.
        /// <summary>    
        /// GET: Default.aspx/GetData    
        /// </summary>    
        /// <returns>Return data</returns>    
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object PopulateServicesAllocated()
        {
            // Initialization.    
            ServicesAllocatedResponseBO response = new ServicesAllocatedResponseBO();
            try
            {
                // Initialization.    
                string search = HttpContext.Current.Request.Params["search[value]"];
                string draw = HttpContext.Current.Request.Params["draw"];
                string orderId = HttpContext.Current.Request.Params["order[0][column]"];
                string order = HttpContext.Current.Request.Params["columns[" + orderId + "][data]"];
                string orderDir = HttpContext.Current.Request.Params["order[0][dir]"];
                int startRec = Convert.ToInt32(HttpContext.Current.Request.Params["start"]);
                int pageSize = Convert.ToInt32(HttpContext.Current.Request.Params["length"]);

                int take = 0, skip = 0;
                if (startRec == 0)
                {
                    take = pageSize * 1;
                    skip = startRec;
                }
                else
                {
                    take = pageSize * startRec;
                    skip = startRec + 1;
                }
                DataSet resultDataset = new DataSet();
                CyclicalServicesBL objBl = new CyclicalServicesBL(new CyclicalServicesRepo());
                int total = objBl.getServicesAllocated(ref resultDataset, search, skip, take, order, orderDir);
                response.draw = Convert.ToInt32(draw);
                response.recordsFiltered = total;
                response.recordsTotal = total;
                List<AllocatedServicesDetailBO> data = new List<AllocatedServicesDetailBO>();
                var schemeData = (from DataRow dr in resultDataset.Tables[0].Rows
                                  select new AllocatedServicesDetailBO()
                                  {
                                      ServiceRequired = dr["ServiceRequired"].ToString(),
                                      ServiceItemId = (dr["ServiceItemId"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ServiceItemId"])),
                                      StatusId = (dr["statusId"] == DBNull.Value ? 0 : Convert.ToInt32(dr["statusId"])),
                                      ContractorId = (dr["ContractorId"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ContractorId"])),
                                      BlockName = dr["BlockName"].ToString(),
                                      Contractor = dr["Contractor"].ToString(),
                                      PORef = dr["PORef"].ToString(),
                                      PostCode = dr["PostCode"].ToString(),
                                      SchemeName = dr["SchemeName"].ToString(),
                                      Commencement = dr["Commencement"].ToString(),
                                      Status = dr["Status"].ToString(),
                                      ContractStart = dr["ContractStart"].ToString()
                                  }).ToList();

                if (schemeData.Count > 0)
                {
                    response.data = schemeData;
                }
                else
                {
                    response.data = data;
                }

            }
            catch (Exception ex)
            {
                // Info    
                Console.Write(ex);
            }
            // Return info.    
            return response;
        }
        #endregion


        #region Populate Cyclical Tab
        /// <summary>    
        /// Populate Cyclical Tab    
        /// </summary>    
        /// <returns>Return data</returns>    
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = true)]
        public static object PopulateCyclicalTab()
        {
            // Initialization.    
            CyclicalTabResponseBO response = new CyclicalTabResponseBO();
            try
            {
                // Initialization.    
                string search = HttpContext.Current.Request.Params["search[value]"];
                string draw = HttpContext.Current.Request.Params["draw"];
                string orderId = HttpContext.Current.Request.Params["order[0][column]"];
                string order = HttpContext.Current.Request.Params["columns[" + orderId + "][data]"];
                string orderDir = HttpContext.Current.Request.Params["order[0][dir]"];
                int startRec = Convert.ToInt32(HttpContext.Current.Request.Params["start"]);
                int pageSize = Convert.ToInt32(HttpContext.Current.Request.Params["length"]);
                int blockId = Convert.ToInt32(HttpContext.Current.Request.Params["blockId"]);
                int schemeId = Convert.ToInt32(HttpContext.Current.Request.Params["schemeId"]);

                int take = 0, skip = 0;
                if (startRec == 0)
                {
                    take = pageSize * 1;
                    skip = startRec;
                }
                else
                {
                    take = pageSize * startRec;
                    skip = startRec + 1;
                }
                DataSet resultDataset = new DataSet();
                CyclicalServicesBL objBl = new CyclicalServicesBL(new CyclicalServicesRepo());
                int total = objBl.getCyclicalTab(ref resultDataset, search, skip, take, order, orderDir, schemeId, blockId);
                response.draw = Convert.ToInt32(draw);
                response.recordsFiltered = total;
                response.recordsTotal = total;
                List<CyclicalTabDetailBO> data = new List<CyclicalTabDetailBO>();
                var schemeData = (from DataRow dr in resultDataset.Tables[0].Rows
                                  select new CyclicalTabDetailBO()
                                  {
                                      ServiceRequired = dr["ServiceRequired"].ToString(),
                                      ServiceItemId = (dr["ServiceItemId"] == DBNull.Value ? 0 : Convert.ToInt32(dr["ServiceItemId"])),
                                      Cycle = dr["CYCLE"].ToString(),
                                      RemainingCycle = (dr["RemainingCycle"] == DBNull.Value ? 0 : Convert.ToInt32(dr["RemainingCycle"])),
                                      BlockName = dr["BlockName"].ToString(),
                                      Contractor = dr["Contractor"].ToString(),
                                      PORef = dr["PORef"].ToString(),
                                      RecentCompletion = dr["RecentCompletion"].ToString(),
                                      NextCycle = dr["NextCycle"].ToString(),
                                      SchemeName = dr["SchemeName"].ToString(),
                                      Commencement = dr["Commencement"].ToString() 
                                  }).ToList();

                if (schemeData.Count > 0)
                {
                    response.data = schemeData;
                }
                else
                {
                    response.data = data;
                }

            }
            catch (Exception ex)
            {
                // Info    
                Console.Write(ex);
            }
            // Return info.    
            return response;
        }
        #endregion

    }
}