﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PropertyDataRestructure.Views.CyclicalServices
{
    public partial class UpdateCyclicalPurchaseOrder : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            int employeeId = int.Parse(Request.QueryString["uid"]);
            int orderId = int.Parse(Request.QueryString["oid"]);
            if (!IsPostBack)
            {
                ucPurchaseOrder.Visible = true;
                ucPurchaseOrder.populateData(orderId, employeeId);
            }
        }
    }
}