﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Blank.Master" AutoEventWireup="true"
    CodeBehind="ToBeAllocated.aspx.cs" Inherits="PropertyDataRestructure.Views.CyclicalServices.ToBeAllocated" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../../Scripts/jquery-1.12.4.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery-ui-1.12.1.js" type="text/javascript"></script>
    <script src="../../Scripts/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="../../Scripts/dataTables.bootstrap.min.js" type="text/javascript"></script>
    <script src="../../Scripts/dataTables.select.min.js" type="text/javascript"></script>
    <script src="../../Scripts/jquery.dialog.min.js" type="text/javascript"></script>
    <script src="../../Scripts/select2.min.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/select/1.2.2/css/select.dataTables.min.css">
    <link href="../../Styles/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/dialog.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/cyclical.css" rel="stylesheet" type="text/css" />
    <script src="../../Scripts/Cyclical.js" type="text/javascript"></script>
    <link href="../../Styles/jquery.dialog.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">
        jQuery.ajaxSetup({
            cache: false,
            beforeSend: function () {
                $('#loader').show();
            },
            complete: function () {
                $('#loader').hide();
            },
            success: function () { }
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div style="border: 1px solid #C2C2C2; min-height: 672px; width: 100%; margin-top: -1px;">
        <table id="dt-Services-to-be-Allocated" style="width: 100%;">
            <thead>
                <tr>
                    <th>
                        <input name="select_all" value="1" type="checkbox">
                    </th>
                    <th>
                        ServiceItemId
                    </th>
                    <th>
                        ContractorId
                    </th>
                    <th>
                        Scheme
                    </th>
                    <th>
                        Block
                    </th>
                    <th>
                        Post Code
                    </th>
                    <th>Commencement</th>
                    <th>
                        Cycle Period
                    </th>
                    <th>
                        Attribute
                    </th>
                    <th>
                        Contractor
                    </th>
                    <th>
                        Contract Start/End
                    </th>
                    <th>
                        Status
                    </th>
                    <th class="hidden">
                        VAT
                    </th>
                    <th class="hidden">
                        Total Value
                    </th>

                </tr>
            </thead>
        </table>
    </div>
    <div style="width: 100%; float: right; margin: 5px;">
        <button id="btnContractor" class="btn btn-info btn-xs pull-right" type="button" style="position: absolute; top: 110px; right: 3%;">
            Assign Selected Work to Contractor</button>
    </div>
    <div class="modal fade" id="contractorDetail" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">
                        Assign Cyclical Works to Contractor</h4>
                    <input type="hidden" id="hdnContractorId" value="0" />
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row padding-bottom-5">
                                <div class="col-md-3">
                                    Cost Centre:<span class='required'>*</span>
                                </div>
                                <div class="col-md-9">
                                    <select id='ddlCostCentre' style='width: 250px;' class="form-control">
                                        <option></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row padding-bottom-5">
                                <div class="col-md-3">
                                    Budget Head:<span class='required'>*</span>
                                </div>
                                <div class="col-md-9">
                                    <select id='ddlBudget' style='width: 250px;' class="form-control">
                                        <option></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row padding-bottom-5">
                                <div class="col-md-3">
                                    Expenditure:<span class='required'>*</span>
                                </div>
                                <div class="col-md-9">
                                    <select id='ddlExpenditure' style='width: 250px;' class="form-control">
                                        <option></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row padding-bottom-5">
                                <div class="col-md-3">
                                    Contractor:
                                </div>
                                <div class="col-md-9">
                                    <label id="lblContractor" for="Contractor">
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row padding-bottom-5">
                                <div class="col-md-3">
                                    Contact:
                                </div>
                                <div class="col-md-9">
                                    <select id='ddlContact' style='width: 250px;' class="form-control">
                                        <option></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row padding-bottom-5">
                                <div class="col-md-5">
                                    Send Approval link to Contractor?:<span class='required'>*</span>
                                </div>
                                <div class="col-md-5" style="text-align:left;">
                                    <span style="padding-right: 30px;">
                                        <input type="radio" name="rdbtnApproval" value="true" checked id="rdbtnApproval" />&nbsp;Yes
                                    </span><span>
                                        <input type="radio" name="rdbtnApproval" value="false" id="rdbtnApprovalNo" />&nbsp;No
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin: 2%; border-top-style: solid; border-width: thin;
                            border-color: #d2cece; width: 95%; line-height: 0px;">
                            &nbsp;</div>
                        <div class="col-md-12">
                            <div class="row padding-bottom-5">
                                <div class="col-md-3">
                                    Required Works:
                                </div>
                                <div class="col-md-9">
                                    <label id="lblRequiredWorks" for="RequiredWorks">
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row padding-bottom-5">
                                <div class="col-md-3">
                                    More Details:
                                </div>
                                <div class="col-md-9">
                                    <textarea style="width: 250px; font-style:italic;" class="form-control" rows="5" id="txtMoreDetail" placeholder="Enter details here" ></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div style='background-color: #f1f1f1; width: 98%; padding: 1%;'>
                                <table id="tblAttribute" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>
                                                ServiceItemId
                                            </th>
                                            <th>
                                                Scheme(s)/Block(s)
                                            </th>
                                            <th>
                                                Commence
                                            </th>
                                            <th>
                                                Cycle Value
                                            </th>
                                            <th>
                                            </th>
                                            <th>
                                                Cycle
                                            </th>
                                            <th>
                                                Cycles
                                            </th>
                                            <th>
                                                Value(&pound;)
                                            </th>
                                            <th>
                                                Cycle Days
                                            </th>
                                            <th>
                                                End Date
                                            </th>
                                            <th>
                                                Cycle Type
                                            </th>
                                            <th>
                                                Cycle Period
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin: 2%; border-top-style: solid; border-width: thin;
                            border-color: #d2cece; width: 95%; line-height: 0px;">
                            &nbsp;</div>
                        <div class="col-md-12">
                            <div class="row padding-bottom-5">
                                <div class="col-md-3">
                                    Net Cost:
                                </div>
                                <div class="col-md-9">
                                   &pound; <label id="lblNetCost" for="NetCost">
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row padding-bottom-5">
                                <div class="col-md-3">
                                    Vat:
                                </div>
                                <div class="col-md-9">
                                    <select id='ddlVat' style='width: 250px;' class="form-control" required>
                                        <option></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row padding-bottom-5">
                                <div class="col-md-3">
                                    Vat(&pound;):
                                </div>
                                <div class="col-md-9">
                                    <input type="text" id="txtVat" class="form-control" style='width: 250px;' />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row padding-bottom-5">
                                <div class="col-md-3">
                                    Total(&pound;):
                                </div>
                                <div class="col-md-7">
                                    <input type="text" id="txtTotal" class="form-control" style='width: 250px;' />
                                </div>
                                <div class="col-md-2">
                                    <button id="btnAdd" class="btn btn-info btn-grey btn-xs pull-left" type="button">
                                        Add</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin: 2%; border-top-style: solid; border-width: thin;
                            border-color: #d2cece; width: 95%; line-height: 0px;">
                            &nbsp;</div>
                        <div class="col-md-12" id="dvtblWorksRequired">
                            <div style='background-color: #f1f1f1; width: 98%; padding: 1%;'>
                                <table id="tblWorksRequired" style="width: 100%;">
                                    <thead>
                                        <tr>
                                            <th>
                                                Service ItemId
                                            </th>
                                            <th>
                                                Works Required:
                                            </th>
                                            <th>
                                                Net:
                                            </th>
                                            <th>
                                                Vat:
                                            </th>
                                            <th>
                                                Gross:
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                    <tfoot align="right">
                                        <tr>
                                            <th>
                                            </th>
                                            <th align="right">
                                            </th>
                                            <th>
                                            </th>
                                            <th>
                                            </th>
                                            <th>
                                            </th>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-12" style="margin: 2%; border-top-style: solid; border-width: thin;
                            border-color: #d2cece; width: 95%; line-height: 0px;">
                            &nbsp;</div>
                        <div class="col-md-12">
                            <div class="col-md-9">
                                <button id="btnClose" class="btn btn-info btn-grey btn-xs pull-left" type="button">
                                    Close</button>
                            </div>
                            <div class="col-md-3">
                                <button id="btnGeneratePo" class="btn btn-info btn-grey btn-xs pull-right" type="button">
                                    Generate PO(s)</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
