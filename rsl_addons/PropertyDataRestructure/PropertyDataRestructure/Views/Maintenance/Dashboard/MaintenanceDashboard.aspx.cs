﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PropertyDataRestructure.Base;
using PDR_Utilities.Constants;
using PDR_Utilities.Helpers;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_BusinessLogic.Maintenance;
using PDR_DataAccess.Maintenance;
using System.Data;
using PDR_BusinessObject.PageSort;
using PDR_DataAccess.CyclicalServices;
using PDR_BusinessLogic.CyclicalServices;

namespace PropertyDataRestructure.Views.Maintenance.Dashboard
{
    public partial class MaintenanceDashboard : PageBase
    {

        #region Events

        #region "Page Load"
        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                uiMessage.hideMessage();
                if (!IsPostBack)
                {
                    loadData();
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #region "Maintenance Type Dropdown Selected Index Changed"
        /// <summary>
        /// Maintenance Type Dropdown Selected Index Changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param> 
        protected void ddlMaintenanceType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                updateData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #region "Scheme Dropdown Selected Index Changed"
        /// <summary>
        /// Scheme Dropdown Selected Index Changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param> 
        protected void ddlScheme_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                populateBlockDropdownList();
                updateData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #region "Block Dropdown Selected Index Changed"
        /// <summary>
        /// Block Dropdown Selected Index Changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param> 
        protected void ddlBlock_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                updateData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #region "Maintenance Status Dropdown Selected Index Changed"
        /// <summary>
        /// Maintenance Status Dropdown Selected Index Changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param> 
        protected void ddlStatusType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                pageSortViewState = null;
                populateMaintenanceStatusData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #region "No Entry Link button event"
        /// <summary>
        /// No Entry Link button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param> 
        protected void lnkBtnNoEntries_Click(object sender, EventArgs e)
        {
            try
            {
                pageSortViewState = null;
                ddlStatusType.SelectedValue = ApplicationConstants.MaintenanceStatusNoEntries;
                populateMaintenanceStatusData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #region "Overdue button event"
        /// <summary>
        /// Overdue button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param> 
        protected void lnkBtnOverdue_Click(object sender, EventArgs e)
        {
            try
            {
                pageSortViewState = null;
                ddlStatusType.SelectedValue = ApplicationConstants.MaintenanceStatusOverdueServices;
                populateMaintenanceStatusData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #region "Appointment arranged button event"
        /// <summary>
        /// Appointment arranged button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param> 
        protected void lnkBtnAppointmentsArranged_Click(object sender, EventArgs e)
        {
            try
            {
               int aptArrangedCount = Convert.ToInt32(lblAppointmentArrangedCount.Text);

               if (aptArrangedCount > 0)
                {
                    navigateToSchedulingArea(ApplicationConstants.AppointmentArrangedTab);
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #region "Appointment to be Arranged Link button event"
        /// <summary>
        /// Appointment to be Arranged Link button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param> 
        protected void lnkBtnAppointmentToBeArranged_Click(object sender, EventArgs e)
        {
            try
            {
                 int aptToBeArrangedCount = Convert.ToInt32(lblAppointmentToBeArrangedCount.Text);

                 if (aptToBeArrangedCount > 0)
                 {
                     navigateToSchedulingArea(ApplicationConstants.AppointmentToBeArrangedTab);
                 }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #region Pager Event Handler
        /// <summary>
        /// Pager event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkbtnPager_Click(object sender, EventArgs e)
        {
            try
            {
                PageSortBO objPageSortBo = new PageSortBO("DESC", "Address", 1, 10);
                pageSortViewState = GridHelper.processGridPager(sender, objPageSortBo, pageSortViewState);
                populateMaintenanceStatusData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region Sorting Event Handler
        /// <summary>
        /// Sorting event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdMaintenanceStatus_Sorting(Object sender, GridViewSortEventArgs e)
        {
            try
            {
                GridView grdMaintenanceStatus = (GridView)sender;
                PageSortBO objPageSortBo = new PageSortBO("DESC", "Address", 1, 10);
                pageSortViewState = GridHelper.sortGrid(grdMaintenanceStatus, e, objPageSortBo, pageSortViewState);
                populateMaintenanceStatusData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #endregion

        #region Functions

        #region "Navigate To Scheduling Area"
        /// <summary>
        /// "Navigate To Scheduling Area"
        /// </summary>
        public void navigateToSchedulingArea(string tab)
        {
            string schemeId = ddlScheme.SelectedValue;
            string blockId = ddlBlock.SelectedValue;
            string attributeTypeId = ddlMaintenanceType.SelectedValue;

            string path = string.Empty;
            if (tab.Equals(ApplicationConstants.AppointmentArrangedTab))
            {
                path = PathConstants.CyclicalServicingPath + "?" + ApplicationConstants.AppointmentArrangedTab+"=true";
            }
            else
            {
                path = PathConstants.CyclicalServicingPath;
            }
            Response.Redirect(path);

        }
        #endregion

        #region "Format Date"
        public object FormatDate(object input)
        {

            if (input != System.DBNull.Value)
            {
                return string.Format("{0:d}", input);
            }
            else
            {
                return string.Empty;
            }

        }
        #endregion

        #region "Load Data"
        /// <summary>
        /// Load Data
        /// </summary>
        public void loadData()
        {
            populateAllDropdownList();
            updateData();
        }
        #endregion

        #region "Populate All drop down list"
        public void populateAllDropdownList()
        {
            populateMaintenanceTypeDropdownList();
            populateSchemesDropdownList();
            populateBlockDropdownList();
            populateMaintenanceStatusDropdownList();
        }
        #endregion

        #region "Update data"
        /// <summary>
        /// Update data
        /// </summary>
        public void updateData()
        {
            pageSortViewState = null;
            populateNoEntryCount();
            populateOverdueCount();
            populateAppointmentToBeArrangedCount();
            populateAppointmentsArrangedCount();

            populateMaintenanceStatusData();
        }
        #endregion

        #region Populate Maintenance Type Dropdown List
        /// <summary>
        /// Populate Maintenance Type Dropdown List
        /// </summary>
        /// <remarks></remarks>
        public void populateMaintenanceTypeDropdownList()
        {
            ddlMaintenanceType.Items.Clear();

            MaintenanceBL objMaintenanceBL = new MaintenanceBL(new MaintenanceRepo());
            DataSet resultDataSet = new DataSet();
            objMaintenanceBL.getMaintenanceTypes(ref resultDataSet);

            ddlMaintenanceType.DataSource = resultDataSet;
            ddlMaintenanceType.DataValueField = "AttributeTypeId";
            ddlMaintenanceType.DataTextField = "AttributeType";
            ddlMaintenanceType.DataBind();

            ListItem newListItem = default(ListItem);
            newListItem = new ListItem("All", "-1");
            ddlMaintenanceType.Items.Insert(0, newListItem);

        }

        #endregion

        #region Populate Schemes Dropdown List
        /// <summary>
        /// Populate Schemes Dropdown List
        /// </summary>
        /// <remarks></remarks>
        public void populateSchemesDropdownList()
        {
            ddlScheme.Items.Clear();

            MaintenanceBL objMaintenanceBL = new MaintenanceBL(new MaintenanceRepo());
            DataSet resultDataSet = new DataSet();
            objMaintenanceBL.getAllSchemes(ref resultDataSet);

            ddlScheme.DataSource = resultDataSet;
            ddlScheme.DataValueField = "Id";
            ddlScheme.DataTextField = "Title";
            ddlScheme.DataBind();

            ListItem newListItem = default(ListItem);
            newListItem = new ListItem("All Schemes", "-1");
            ddlScheme.Items.Insert(0, newListItem);

        }

        #endregion

        #region Populate Block Dropdown List
        /// <summary>
        /// Populate Block Dropdown List
        /// </summary>
        /// <remarks></remarks>
        public void populateBlockDropdownList()
        {
            ddlBlock.Items.Clear();

            MaintenanceBL objMaintenanceBL = new MaintenanceBL(new MaintenanceRepo());
            DataSet resultDataSet = new DataSet();
            int selectedSchemeId = Convert.ToInt32(ddlScheme.SelectedValue);
            objMaintenanceBL.getBlocksBySchemeId(selectedSchemeId, ref resultDataSet);

            ddlBlock.DataSource = resultDataSet;
            ddlBlock.DataValueField = "BLOCKID";
            ddlBlock.DataTextField = "BLOCKNAME";
            ddlBlock.DataBind();

            ListItem newListItem = default(ListItem);
            newListItem = new ListItem("All Blocks", "-1");
            ddlBlock.Items.Insert(0, newListItem);

        }

        #endregion

        #region Populate Maintenance Status Dropdown List
        /// <summary>
        /// Populate Maintenance Status Dropdown List
        /// </summary>
        /// <remarks></remarks>
        public void populateMaintenanceStatusDropdownList()
        {
            ddlStatusType.Items.Clear();

            ddlStatusType.Items.Add(new ListItem("No Entries", ApplicationConstants.MaintenanceStatusNoEntries));
            ddlStatusType.Items.Add(new ListItem("Overdue Services", ApplicationConstants.MaintenanceStatusOverdueServices));
            ddlStatusType.Items.Add(new ListItem("Appointments Arranged", ApplicationConstants.MaintenanceStatusAppointmentsArranged));
            ddlStatusType.Items.Add(new ListItem("Appointments to be Arranged", ApplicationConstants.MaintenanceStatusAppointmentsToBeArranged));

            ddlStatusType.SelectedValue = ApplicationConstants.MaintenanceStatusNoEntries;
        }

        #endregion

        #region Populate No Entry Count
        /// <summary>
        /// Populate No Entry Count
        /// </summary>
        /// <remarks></remarks>
        public void populateNoEntryCount()
        {
            MaintenanceBL objMaintenanceBL = new MaintenanceBL(new MaintenanceRepo());
            DataSet resultDataSet = new DataSet();

            int schemeId = Convert.ToInt32(ddlScheme.SelectedValue);
            int blockId = Convert.ToInt32(ddlBlock.SelectedValue);
            int maintenanceType = Convert.ToInt32(ddlMaintenanceType.SelectedValue);
            PageSortBO objPageSortBo = new PageSortBO("DESC", "Address", 1, 10);

            int count = objMaintenanceBL.getNoEntryData(ref resultDataSet,objPageSortBo, schemeId, blockId, maintenanceType);
            lblNoEntryCount.Text = Convert.ToString(count);

        }

        #endregion

        #region Populate Overdue Count
        /// <summary>
        /// Populate Overdue Count
        /// </summary>
        /// <remarks></remarks>
        public void populateOverdueCount()
        {
            MaintenanceBL objMaintenanceBL = new MaintenanceBL(new MaintenanceRepo());
            DataSet resultDataSet = new DataSet();

            int schemeId = Convert.ToInt32(ddlScheme.SelectedValue);
            int blockId = Convert.ToInt32(ddlBlock.SelectedValue);
            int maintenanceType = Convert.ToInt32(ddlMaintenanceType.SelectedValue);
            PageSortBO objPageSortBo = new PageSortBO("DESC", "Address", 1, 10);

            int count = objMaintenanceBL.getOverdueData(ref resultDataSet, objPageSortBo, schemeId, blockId, maintenanceType);
            lblOverDueCount.Text = Convert.ToString(count);      
        }

        #endregion

        #region Populate Appointment To Be Arranged Count
        /// <summary>
        /// Populate Appointment To Be Arranged Count
        /// </summary>
        /// <remarks></remarks>
        public void populateAppointmentToBeArrangedCount()
        {
            MaintenanceBL objMaintenanceBL = new MaintenanceBL(new MaintenanceRepo());
            DataSet resultDataSet = new DataSet();

            int schemeId = Convert.ToInt32(ddlScheme.SelectedValue);
            int blockId = Convert.ToInt32(ddlBlock.SelectedValue);
            int maintenanceType = Convert.ToInt32(ddlMaintenanceType.SelectedValue);
            PageSortBO objPageSortBo = new PageSortBO("DESC", "Address", 1, 10);
            CyclicalServicesBL objBl = new CyclicalServicesBL(new CyclicalServicesRepo());
            int total = objBl.getServicesToBeAllocated(ref resultDataSet, "", 0, 20, "serviceItemId", "desc", 0, 0);
            lblAppointmentToBeArrangedCount.Text = Convert.ToString(total);
        }

        #endregion

        #region Populate Appointments Arranged Count
        /// <summary>
        /// Populate Appointments Arranged Count
        /// </summary>
        /// <remarks></remarks>
        public void populateAppointmentsArrangedCount()
        {
            MaintenanceBL objMaintenanceBL = new MaintenanceBL(new MaintenanceRepo());
            DataSet resultDataSet = new DataSet();

            int schemeId = Convert.ToInt32(ddlScheme.SelectedValue);
            int blockId = Convert.ToInt32(ddlBlock.SelectedValue);
            int maintenanceType = Convert.ToInt32(ddlMaintenanceType.SelectedValue);
            PageSortBO objPageSortBo = new PageSortBO("DESC", "Address", 1, 10);

            CyclicalServicesBL objBl = new CyclicalServicesBL(new CyclicalServicesRepo());
            int total = objBl.getServicesAllocated(ref resultDataSet, "", 0, 20, "serviceItemId", "desc");
            lblAppointmentArrangedCount.Text = Convert.ToString(total); 
        }

        #endregion

        #region Populate Maintenance Status Data
        /// <summary>
        /// Populate Maintenance Status Data
        /// </summary>
        /// <remarks></remarks>
        public void populateMaintenanceStatusData()
        {
            MaintenanceBL objMaintenanceBL = new MaintenanceBL(new MaintenanceRepo());
            DataSet resultDataSet = new DataSet();

            int schemeId = Convert.ToInt32(ddlScheme.SelectedValue);
            int blockId = Convert.ToInt32(ddlBlock.SelectedValue);
            int maintenanceType = Convert.ToInt32(ddlMaintenanceType.SelectedValue);
            int totalCount = 0;

            PageSortBO objPageSortBo;
            if (pageSortViewState == null)
            {
                objPageSortBo = new PageSortBO("DESC", "Address", 1, 10);
                pageSortViewState = objPageSortBo;
            }
            else
            {
                objPageSortBo = pageSortViewState;
            }

            switch (ddlStatusType.SelectedValue)
            {
                case ApplicationConstants.MaintenanceStatusNoEntries:
                    totalCount = objMaintenanceBL.getNoEntryData(ref resultDataSet, objPageSortBo, schemeId, blockId, maintenanceType,false);
                    break;
                case ApplicationConstants.MaintenanceStatusOverdueServices:
                    totalCount = objMaintenanceBL.getOverdueData(ref resultDataSet, objPageSortBo, schemeId, blockId, maintenanceType, false);
                    break;
                case ApplicationConstants.MaintenanceStatusAppointmentsArranged:
                    totalCount = objMaintenanceBL.getAppointmentsArrangedData(ref resultDataSet, objPageSortBo, schemeId, blockId, maintenanceType, false);
                    break;
               case ApplicationConstants.MaintenanceStatusAppointmentsToBeArranged:
                    totalCount = objMaintenanceBL.getAppointmentsToBeArrangedData(ref resultDataSet, objPageSortBo, schemeId, blockId, maintenanceType, false);
                    break;
            }

            grdMaintenanceStatus.DataSource = resultDataSet;
            grdMaintenanceStatus.DataBind();
            objPageSortBo.TotalRecords = totalCount;
            objPageSortBo.TotalPages = Convert.ToInt32(Math.Ceiling((Double)totalCount / objPageSortBo.PageSize));

            pageSortViewState = objPageSortBo;
            GridHelper.setGridViewPager(ref pnlPagination, objPageSortBo);

        }

        #endregion

        #endregion

    }
}