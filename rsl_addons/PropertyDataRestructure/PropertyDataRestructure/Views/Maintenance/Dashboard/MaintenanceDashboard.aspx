﻿<%@ Page Language="C#" AutoEventWireup="true" Culture="en-GB" MasterPageFile="~/MasterPage/Pdr.Master"
    CodeBehind="MaintenanceDashboard.aspx.cs" Inherits="PropertyDataRestructure.Views.Maintenance.Dashboard.MaintenanceDashboard" %>

<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style>
        .box{
            margin: 2px 10px 2px 2px !important;
        }
        .select_div select {
            width:185px !important;
        }
        .label-b {
            line-height:22px;
        }
    </style>
    <asp:UpdateProgress ID="updateProgressReportsArea" runat="server" DisplayAfter="10">
        <ProgressTemplate>
            <div class="loading-image">
                <img alt="Please Wait" src="../../../Images/menu/progress.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <link href="../../../Styles/Dashboard.css" rel="stylesheet" type="text/css" />
    <asp:Panel ID="pnlDashboardOuter" runat="server" UpdateMode="Conditional">
        <div class="wrapper">
            <div class="main_div">
                <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="500px" />
                <asp:UpdatePanel ID="updPanelMain" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div class="portlet" style="width:80%">
                            <div class="header"style="width:100%" >
                                <span style=" padding: 0 10px; font-weight: bold; ">Search:</span>
                            </div>
                            <div>
                                <div class="portlet-body">
                                    <asp:Panel ID="pnlPatch" runat="server">
                                        <div class="form-control" >
                                            <div class="select_div">
                                                <div class="label">
                                                    Maintenance type:
                                                </div>
                                                <div class="field">
                                                    <asp:DropDownList ID="ddlMaintenanceType" class="styleselect styleselect-control" runat="server" AutoPostBack="true"
                                                        OnSelectedIndexChanged="ddlMaintenanceType_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-control">
                                            <div class="select_div">
                                                <div class="label">
                                                    Scheme:
                                                </div>
                                                <div class="field">
                                                    <asp:DropDownList ID="ddlScheme" class="styleselect styleselect-control" runat="server" AutoPostBack="true"
                                                        OnSelectedIndexChanged="ddlScheme_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-control">
                                            <div class="select_div">
                                                <div class="label">
                                                    Block:
                                                </div>
                                                <div class="field">
                                                    <asp:DropDownList ID="ddlBlock" class="styleselect styleselect-control" runat="server" AutoPostBack="true"
                                                        OnSelectedIndexChanged="ddlBlock_SelectedIndexChanged">
                                                    </asp:DropDownList>
                                                </div>
                                            </div>
                                        </div>
                                        <br />
                                    </asp:Panel>
                                </div>
                            </div>
                        </div>
                        <div class="left_part" style="width:33%">
                            <asp:Panel runat="server" ID="pnlMaintenanceCounts">
                                <div class="left-boxes">
                                    <div class="outer-boxes" style="overflow:hidden; margin-top:10px">
                                        <div class="box">
                                            <div class="text">
                                                No
                                            <br />
                                                Entries:
                                            </div>
                                            <div class="number">
                                                <asp:Label ID="lblNoEntryCount" runat="server"> -1 </asp:Label>
                                            </div>
                                            <div class="arrow">
                                                <asp:LinkButton ID="lnkBtnNoEntries" runat="server" OnClick="lnkBtnNoEntries_Click">
                                            <img src="../../../Images/Dashboard/arrow.jpg" alt="" border="0" />
                                                </asp:LinkButton>
                                            </div>
                                        </div>
                                        <div class="box">
                                            <div class="text">
                                                Overdue
                                            <br />
                                                Services:
                                            </div>
                                            <div class="number">
                                                <asp:Label ID="lblOverDueCount" runat="server"> -1 </asp:Label>
                                            </div>
                                            <div class="arrow">
                                                <asp:LinkButton ID="lnkBtnOverdue" runat="server" OnClick="lnkBtnOverdue_Click">
                                            <img src="../../../Images/Dashboard/arrow.jpg" alt="" border="0" />
                                                </asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="outer-boxes" style="overflow:hidden; margin-top:10px">
                                        <div class="box">
                                            <div class="text">
                                                Appointments
                                            <br />
                                                to be Arranged:
                                            </div>
                                            <div class="number">
                                                <asp:Label ID="lblAppointmentToBeArrangedCount" runat="server"> -1 </asp:Label>
                                            </div>
                                            <div class="arrow">
                                                <asp:LinkButton ID="lnkBtnAppointmentToBeArranged" runat="server" OnClick="lnkBtnAppointmentToBeArranged_Click">
                                            <img src="../../../Images/Dashboard/arrow.jpg" alt="" border="0" />
                                                </asp:LinkButton>
                                            </div>
                                        </div>
                                        <div class="box">
                                            <div class="text">
                                                Appointments
                                            <br />
                                                Arranged:
                                            </div>
                                            <div class="number">
                                                <asp:Label ID="lblAppointmentArrangedCount" runat="server"> -1 </asp:Label>
                                            </div>
                                            <div class="arrow">
                                                <asp:LinkButton ID="lnkBtnAppointmentsArranged" runat="server" OnClick="lnkBtnAppointmentsArranged_Click">
                                            <img src="../../../Images/Dashboard/arrow.jpg" alt="" border="0" />
                                                </asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </asp:Panel>
                        </div>
                        <asp:Panel runat="server" ID="pnlRightside">
                            <div class="right_part" style="width:47%">
                                <div class="box-left-b" style="min-height: 238px;">
                                    <div class="boxborder-s-right">
                                        <div class="select_div-b">
                                            <div class="label-b" style="color: white;">
                                                Maintenance Status:
                                            </div>
                                            <div class="field right">
                                                <asp:DropDownList class="styleselect" ID="ddlStatusType" OnSelectedIndexChanged="ddlStatusType_SelectedIndexChanged"
                                                    style="padding: 0 0 0 5px; border-radius: 0px; border: 1px solid #b1b1b1;
                                                    height: 25px !important;
                                                    font-size: 12px !important;
                                                    width: 205px !important;" 
                                                    runat="server" AutoPostBack="True">
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box-s" style="text-align: center">
                                        <div style="border-bottom:  1px solid #A0A0A0;">
                                            <cc1:PagingGridView ID="grdMaintenanceStatus" runat="server" AutoGenerateColumns="False"
                                                Style="overflow: scroll; width: 100%;" OnSorting="grdMaintenanceStatus_Sorting"
                                                BorderWidth="0px" EmptyDataText="No Records Found" CssClass="dashboard webgrid table table-responsive" GridLines="None"
                                                AllowSorting="true" CellPadding="10" CellSpacing="5" ShowHeaderWhenEmpty="True"
                                                AllowPaging="false">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Address:" HeaderStyle-Width="170" SortExpression="Address">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle BorderStyle="None" />
                                                        <ItemStyle Width="190" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Status:" HeaderStyle-Width="100" SortExpression="StatusTitle">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("StatusTitle") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="100" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Next service:" HeaderStyle-Width="100" SortExpression="NextDate">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblNextService" runat="server" Text='<%# FormatDate(Eval("NextDate")) %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ControlStyle BorderStyle="None" />
                                                        <ItemStyle BorderStyle="None" />
                                                        <ItemStyle Width="80" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                </Columns>
                                                <HeaderStyle BorderColor="Black" HorizontalAlign="Left" />
                                                <HeaderStyle CssClass="table-head-bottom-border-style" Font-Bold="True"
                                                    HorizontalAlign="Left" />
                                            </cc1:PagingGridView>
                                        </div>
                                        <asp:Panel runat="server" ID="pnlPagination" Visible="true" Style="vertical-align: middle; text-align: center;">
                                            <table style="margin-top: 7px; width: 100%;">
                                                <tbody>
                                                    <tr>
                                                        <td>&nbsp;
                                                        </td>
                                                        <td>Results:&nbsp;
                                                            <asp:Label Text="0" runat="server" ID="lblPagerRecordEnd" />
                                                            of&nbsp;
                                                            <asp:Label Text="0" runat="server" ID="lblPagerRecordTotal" />
                                                        </td>
                                                        <td>Page:&nbsp;
                                                            <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="currentPage" />
                                                            of&nbsp;
                                                            <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />
                                                        </td>
                                                        <td>
                                                            <asp:LinkButton ID="lnkbtnPagerPrev" Text="" runat="server" CommandName="Page" CommandArgument="Prev"
                                                                OnClick="lnkbtnPager_Click" Style="text-decoration: none;">&lt; Previous &nbsp; </asp:LinkButton>
                                                            &nbsp;
                                                            <asp:LinkButton ID="lnkbtnPagerNext" Text="Next >" runat="server" CommandName="Page"
                                                                CommandArgument="Next" OnClick="lnkbtnPager_Click" Style="text-decoration: none;" />
                                                            &nbsp;
                                                        </td>
                                                        <td>&nbsp;
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </asp:Panel>
                                    </div>
                                </div>
                            </div>
                        </asp:Panel>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
