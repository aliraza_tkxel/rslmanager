﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PDR_Utilities.Constants;
using System.Data;
using PDR_BusinessLogic.SchemeBlock;
using PDR_DataAccess.SchemeBlock;
using AjaxControlToolkit;
using PDR_Utilities.Managers;
using PropertyDataRestructure.Base;
using PDR_BusinessLogic.Block;
using PDR_DataAccess.Block;

namespace PropertyDataRestructure.Views.Pdr.Block
{
    public partial class BlockMain : PageBase
    {
        public CheckBox ckBoxDefectPhotoUpload;

        #region"Events"
        /// <summary>
        /// Events of this page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void Page_Load(object sender, EventArgs e)
        {
            ckBoxDefectPhotoUpload = new CheckBox();
            UserControl attributesControl = (UserControl)MainView.FindControl("Attributes");
            UserControl ucAttributeDetail = (UserControl)ucAttribute.FindControl("ucAttributeDetail");
            UserControl detailMainView = (UserControl)ucAttributeDetail.FindControl("defectTab");
            ckBoxDefectPhotoUpload = (CheckBox)detailMainView.FindControl("ckBoxPhotoUpload");
            if (!IsPostBack)
            {
                lnkBtnMainDetailTab.Visible = true;
                lnkBtnBlockRestriction.Visible = true;
                lnkBtnAttributesTab.Visible = true;
                lnkBtnWarranties.Visible = true;
                lnkBtnProperties.Visible = true;
                lnkBtnHealthSafetyTab.Visible = true;
                lnkBtnDocuments.Visible = true;
                lnkBtnProvisionsTab.Visible = true;

                int blockId = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);
                string source = Request.QueryString[ApplicationConstants.Source];
                if (blockId != 0)
                {
                    setupFullAddress(blockId);
                    //objSession.EmployeeId = 943;
                    if (source != null && source == "new")
                    {
                        enableDisableTabs(true, 1, lnkBtnAttributesTab);
                    }
                    else
                    {
                        enableDisableTabs(true, 0, lnkBtnMainDetailTab);
                    }
                }
                else
                {
                    enableDisableTabs(false, 0, lnkBtnMainDetailTab);
                }
            }

        }

        #region "lnkBtn MainDetail Tab click event"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkBtnMainDetailTab_Click(object sender, EventArgs e)
        {
            lnkBtnMainDetailTab.CssClass = ApplicationConstants.TabClickedCssClass;
            lnkBtnAttributesTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnProvisionsTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnWarranties.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnProperties.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnHealthSafetyTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnDocuments.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnBlockRestriction.CssClass = ApplicationConstants.TabInitialCssClass;
            MainView.ActiveViewIndex = 0;
            objSession.TreeItemId = 0;
            objSession.IsProvisionClicked = false;
        }
        #endregion

        #region "lnkBtn Attributes Tab click event"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkBtnAttributesTab_Click(object sender, EventArgs e)
        {
            lnkBtnMainDetailTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnAttributesTab.CssClass = ApplicationConstants.TabClickedCssClass;
            lnkBtnProvisionsTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnWarranties.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnProperties.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnHealthSafetyTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnDocuments.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnBlockRestriction.CssClass = ApplicationConstants.TabInitialCssClass;
            MainView.ActiveViewIndex = 1;
            objSession.TreeItemId = 0;
            objSession.IsProvisionClicked = false;
        }
        #endregion

        #region "lnkBtn Provisions Tab click event"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkBtnProvisionsTab_Click(object sender, EventArgs e)
        {
            lnkBtnMainDetailTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnAttributesTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnProvisionsTab.CssClass = ApplicationConstants.TabClickedCssClass;
            lnkBtnWarranties.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnProperties.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnHealthSafetyTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnDocuments.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnBlockRestriction.CssClass = ApplicationConstants.TabInitialCssClass;
            MainView.ActiveViewIndex = 2;
            objSession.ProvisionDataSet = null;
            objSession.TreeItemId = 0;
            objSession.IsProvisionClicked = true;
            ucProvision.DisableMEServicing();

            ProvisionsBL objProvisionsBL = new ProvisionsBL(new ProvisionsRepo());
            int blockId = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);
            DataSet provisionDS = objProvisionsBL.GetProvisionsData(null, blockId);
            //Save Dataset in session to load selected item data
            objSession.ProvisionDataSet = provisionDS; 
            ucProvision.resetTreeView();
            ucProvision.PopulateTreeNode(provisionDS);
        }
        #endregion

        #region "lnkBtn Restrictions Tab click event"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkBtnRestrictionsTab_Click(object sender, EventArgs e)
        {
            lnkBtnMainDetailTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnAttributesTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnProvisionsTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnWarranties.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnProperties.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnHealthSafetyTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnDocuments.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnBlockRestriction.CssClass = ApplicationConstants.TabClickedCssClass;
            MainView.ActiveViewIndex = 7;
            BlockRestriction.loadData();
            objSession.IsProvisionClicked = false;
        }
        #endregion

        #region "lnkBtn Warranties click event"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkBtnWarranties_Click(object sender, EventArgs e)
        {

            lnkBtnMainDetailTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnAttributesTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnProvisionsTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnDocuments.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnWarranties.CssClass = ApplicationConstants.TabClickedCssClass;
            lnkBtnProperties.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnHealthSafetyTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnBlockRestriction.CssClass = ApplicationConstants.TabInitialCssClass;
            MainView.ActiveViewIndex = 3;
            Warranties.loadData();
            objSession.IsProvisionClicked = false;
        }
        #endregion

        #region "lnkBtn HealthSafety Tab click event"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkBtnHealthSafetyTab_Click(object sender, EventArgs e)
        {

            lnkBtnMainDetailTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnAttributesTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnProvisionsTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnWarranties.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnProperties.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnDocuments.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnHealthSafetyTab.CssClass = ApplicationConstants.TabClickedCssClass;
            lnkBtnBlockRestriction.CssClass = ApplicationConstants.TabInitialCssClass;
            MainView.ActiveViewIndex = 4;
            objSession.IsProvisionClicked = false;
        }
        #endregion

        #region "lnkBtn Properties click event"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkBtnProperties_Click(object sender, EventArgs e)
        {
            lnkBtnMainDetailTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnAttributesTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnProvisionsTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnWarranties.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnDocuments.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnHealthSafetyTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnProperties.CssClass = ApplicationConstants.TabClickedCssClass;
            lnkBtnBlockRestriction.CssClass = ApplicationConstants.TabInitialCssClass;
            MainView.ActiveViewIndex = 5;
            Properties.loadData();
            objSession.IsProvisionClicked = false;
        }
        #endregion

        #region "lnkBtn Documents click event"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkBtnDocuments_Click(object sender, EventArgs e)
        {
            lnkBtnMainDetailTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnAttributesTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnProvisionsTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnWarranties.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnProperties.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnDocuments.CssClass = ApplicationConstants.TabClickedCssClass;
            lnkBtnBlockRestriction.CssClass = ApplicationConstants.TabInitialCssClass;
            Documents.populateDropDowns();
            Documents.populateData();
            MainView.ActiveViewIndex = 6;
            objSession.IsProvisionClicked = false;
        }
        #endregion

        #region "btnLocation click event"
        /// <summary>
        /// btnLocation click event used on add appliance location field
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void btnLocation_Click(object sender, EventArgs e)
        {
            int locationId = Convert.ToInt32(Request["__EVENTARGUMENT"]);
            UserControl attributesControl = (UserControl)MainView.FindControl("Attributes");
            UserControl ucAttributeDetail = (UserControl)ucAttribute.FindControl("ucAttributeDetail");
            MultiView detailMainView = (MultiView)ucAttributeDetail.FindControl("MainView");
            UserControl ucApplianceTab = (UserControl)detailMainView.FindControl("ucAppliances");
            HiddenField hdnSelectedLocationId = (HiddenField)ucApplianceTab.FindControl("hdnSelectedLocationId");
            hdnSelectedLocationId.Value = locationId.ToString();
            AjaxControlToolkit.ModalPopupExtender mdlPopUpAddAppliance = (AjaxControlToolkit.ModalPopupExtender)ucApplianceTab.FindControl("mdlPopUpAddAppliance");
            mdlPopUpAddAppliance.Show();
        }
        #endregion

        #region "btnType click event"
        /// <summary>
        /// btnType click event used on add appliance location field
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void btnType_Click(object sender, EventArgs e)
        {
            int typeId = Convert.ToInt32(Request["__EVENTARGUMENT"]);
            UserControl attributesControl = (UserControl)MainView.FindControl("Attributes");
            UserControl ucAttributeDetail = (UserControl)ucAttribute.FindControl("ucAttributeDetail");
            MultiView detailMainView = (MultiView)ucAttributeDetail.FindControl("MainView");
            UserControl ucApplianceTab = (UserControl)detailMainView.FindControl("ucAppliances");
            HiddenField hdnSelectedTypeId = (HiddenField)ucApplianceTab.FindControl("hdnSelectedTypeId");
            hdnSelectedTypeId.Value = typeId.ToString();
            AjaxControlToolkit.ModalPopupExtender mdlPopUpAddAppliance = (AjaxControlToolkit.ModalPopupExtender)ucApplianceTab.FindControl("mdlPopUpAddAppliance");
            mdlPopUpAddAppliance.Show();
        }
        #endregion

        #region "btnMake click event"
        /// <summary>
        /// btnMake click event used on add appliance location field
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void btnMake_Click(object sender, EventArgs e)
        {
            int typeId = Convert.ToInt32(Request["__EVENTARGUMENT"]);
            UserControl attributesControl = (UserControl)MainView.FindControl("Attributes");
            UserControl ucAttributeDetail = (UserControl)ucAttribute.FindControl("ucAttributeDetail");
            MultiView detailMainView = (MultiView)ucAttributeDetail.FindControl("MainView");
            UserControl ucApplianceTab = (UserControl)detailMainView.FindControl("ucAppliances");
            HiddenField hdnSelectedmakeId = (HiddenField)ucApplianceTab.FindControl("hdnSelectedmakeId");
            hdnSelectedmakeId.Value = typeId.ToString();
            AjaxControlToolkit.ModalPopupExtender mdlPopUpAddAppliance = (AjaxControlToolkit.ModalPopupExtender)ucApplianceTab.FindControl("mdlPopUpAddAppliance");
            mdlPopUpAddAppliance.Show();
        }
        #endregion

        #region "btnModel click event"
        /// <summary>
        /// btnMake click event used on add appliance location field
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void btnModel_Click(object sender, EventArgs e)
        {
            int typeId = Convert.ToInt32(Request["__EVENTARGUMENT"]);
            UserControl attributesControl = (UserControl)MainView.FindControl("Attributes");
            UserControl ucAttributeDetail = (UserControl)ucAttribute.FindControl("ucAttributeDetail");
            MultiView detailMainView = (MultiView)ucAttributeDetail.FindControl("MainView");
            UserControl ucApplianceTab = (UserControl)detailMainView.FindControl("ucAppliances");
            HiddenField hdnSelectedModelId = (HiddenField)ucApplianceTab.FindControl("hdnSelectedModelId");
            hdnSelectedModelId.Value = typeId.ToString();
            AjaxControlToolkit.ModalPopupExtender mdlPopUpAddAppliance = (AjaxControlToolkit.ModalPopupExtender)ucApplianceTab.FindControl("mdlPopUpAddAppliance");
            mdlPopUpAddAppliance.Show();
        }
        #endregion


        #endregion

        #region "Functions"

        #region "Search Location"
        /// <summary>
        /// Returns properties 
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        [System.Web.Services.WebMethod()]
        public static List<string> getApplianceLocations(string prefixText, int count)
        {

            DataSet resultDataset = new DataSet();
            AttributesBL objAttrBl = new AttributesBL(new AttributesRepo());
            // Dim objReportsBl As ReportsBL = New ReportsBL()
            resultDataset = objAttrBl.getApplianceLocations(prefixText);

            List<string> result = new List<string>();

            foreach (DataRow row in resultDataset.Tables[0].Rows)
            {
                //result.Add(String.Format("{0}/{1}", row("Description"), row("FaultRepairListID")))
                string item = AutoCompleteExtender.CreateAutoCompleteItem(row[ApplicationConstants.DefaultDropDownDataTextField].ToString(), row[ApplicationConstants.DefaultDropDownDataValueField].ToString());
                result.Add(item);
            }

            return result;

        }

        #endregion

        #region "Search Appliance Type"
        /// <summary>
        /// Returns properties 
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        [System.Web.Services.WebMethod()]
        public static List<string> getApplianceType(string prefixText, int count)
        {

            DataSet resultDataset = new DataSet();
            AttributesBL objAttrBl = new AttributesBL(new AttributesRepo());
            // Dim objReportsBl As ReportsBL = New ReportsBL()
            resultDataset = objAttrBl.getApplianceType(prefixText);

            List<string> result = new List<string>();

            foreach (DataRow row in resultDataset.Tables[0].Rows)
            {
                //result.Add(String.Format("{0}/{1}", row("Description"), row("FaultRepairListID")))
                string item = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(row[ApplicationConstants.DefaultDropDownDataTextField].ToString(), row[ApplicationConstants.DefaultDropDownDataValueField].ToString());
                result.Add(item);
            }

            return result;

        }

        #endregion

        #region "Search Appliance Manufacturer"
        /// <summary>
        /// Returns properties 
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        [System.Web.Services.WebMethod()]
        public static List<string> getApplianceManufacturer(string prefixText, int count)
        {

            DataSet resultDataset = new DataSet();
            AttributesBL objAttrBl = new AttributesBL(new AttributesRepo());
            // Dim objReportsBl As ReportsBL = New ReportsBL()
            resultDataset = objAttrBl.getMake(prefixText);

            List<string> result = new List<string>();

            foreach (DataRow row in resultDataset.Tables[0].Rows)
            {
                //result.Add(String.Format("{0}/{1}", row("Description"), row("FaultRepairListID")))
                string item = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(row[ApplicationConstants.DefaultDropDownDataTextField].ToString(), row[ApplicationConstants.DefaultDropDownDataValueField].ToString());
                result.Add(item);
            }

            return result;

        }

        #endregion

        #region "Search Appliance Model"
        /// <summary>
        /// Returns properties 
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        [System.Web.Services.WebMethod()]
        public static List<string> getApplianceModel(string prefixText, int count)
        {

            DataSet resultDataset = new DataSet();
            AttributesBL objAttrBl = new AttributesBL(new AttributesRepo());
            resultDataset = objAttrBl.getModelsList(prefixText);

            List<string> result = new List<string>();

            foreach (DataRow row in resultDataset.Tables[0].Rows)
            {
                //result.Add(String.Format("{0}/{1}", row("Description"), row("FaultRepairListID")))
                string item = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(row[ApplicationConstants.DefaultDropDownDataTextField].ToString(), row[ApplicationConstants.DefaultDropDownDataValueField].ToString());
                result.Add(item);
            }

            return result;

        }

        #endregion

        #region "Enable Disable Tabs"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="enable"></param>
        /// <param name="viewIndex"></param>
        /// <param name="lnkBtnSelected"></param>
        public void enableDisableTabs(bool enable, int viewIndex, LinkButton lnkBtnSelected)
        {

            lnkBtnMainDetailTab.Enabled = enable;
            lnkBtnAttributesTab.Enabled = enable;
            lnkBtnWarranties.Enabled = enable;
            lnkBtnProperties.Enabled = enable;
            lnkBtnHealthSafetyTab.Enabled = enable;
            lnkBtnDocuments.Enabled = enable;
            lnkBtnProvisionsTab.Enabled = enable;
            lnkBtnBlockRestriction.Enabled = enable;

            lnkBtnMainDetailTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnAttributesTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnWarranties.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnProperties.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnHealthSafetyTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnDocuments.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnProvisionsTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnBlockRestriction.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnSelected.CssClass = ApplicationConstants.TabClickedCssClass;
            MainView.ActiveViewIndex = viewIndex;
        }
        #endregion


        #region "populate DropDowns"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ddl"></param>
        /// 
        public void setupFullAddress(int blockId)
        {
            BlockBL objBlockBL = new BlockBL(new BlockRepo());
            DataSet resultDataset = new DataSet();

            objBlockBL.getBlockTemplateData(ref resultDataset, blockId);
            if (resultDataset.Tables[0].Rows.Count > 0)
            {

                string blockRef = (resultDataset.Tables[0].Rows[0]["BlockReference"] != DBNull.Value ? resultDataset.Tables[0].Rows[0]["BlockReference"].ToString() : string.Empty);
                string blockName = (resultDataset.Tables[0].Rows[0]["BlockName"] != DBNull.Value ? resultDataset.Tables[0].Rows[0]["BlockName"].ToString() : string.Empty);
                string address1 = (resultDataset.Tables[0].Rows[0]["ADDRESS1"] != DBNull.Value ? resultDataset.Tables[0].Rows[0]["ADDRESS1"].ToString() : string.Empty);
                string address2 = (resultDataset.Tables[0].Rows[0]["ADDRESS2"] != DBNull.Value ? resultDataset.Tables[0].Rows[0]["ADDRESS2"].ToString() : string.Empty);

                string county = (resultDataset.Tables[0].Rows[0]["COUNTY"] != DBNull.Value ? resultDataset.Tables[0].Rows[0]["COUNTY"].ToString() : string.Empty);
                string townCity = (resultDataset.Tables[0].Rows[0]["TOWNCITY"] != DBNull.Value ? resultDataset.Tables[0].Rows[0]["TOWNCITY"].ToString() : string.Empty);
                string postcode = (resultDataset.Tables[0].Rows[0]["POSTCODE"] != DBNull.Value ? resultDataset.Tables[0].Rows[0]["POSTCODE"].ToString() : string.Empty);

                var adrsArray = new[] { blockRef, blockName, address1, address2, county, townCity, postcode };
                string fullAddress = string.Join(", ", adrsArray.Where(s => !string.IsNullOrEmpty(s)));
                lblHeading.Text = string.Format("Block > {0}", fullAddress);
            }
        }



        #endregion

        #endregion
    }
}