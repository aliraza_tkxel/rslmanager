﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PDR_BusinessObject;
using PDR_BusinessLogic;
using PDR_BusinessLogic.Block;
using System.Data;
using PDR_BusinessObject.PageSort;
using PDR_DataAccess.Block;
using PropertyDataRestructure.Base;
using PropertyDataRestructure.Interface;
using PDR_Utilities.Helpers;
using PDR_Utilities.Constants;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace PropertyDataRestructure.Views.Pdr.Block
{
    public partial class BlockList : PageBase, IListingPage
    {        
        protected void Page_Load(object sender, EventArgs e)
        {
            uiMessage.hideMessage();
            try
            {
                
                if (!IsPostBack)
                {
                    PageSortBO objPageSortBo = new PageSortBO("DESC", "BlockName", 1, 30);
                    pageSortViewState = objPageSortBo;
                    loadData();
                    

                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }        

        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                pageSortViewState = null;
                loadData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #region "grdSchemeList Sorting"
        protected void grdSchemeList_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                PageSortBO objPageSortBo;
                if (pageSortViewState == null)
                {
                    objPageSortBo = new PageSortBO("DESC", "BlockName", 1, 30);
                    pageSortViewState = objPageSortBo;
                }
                else
                {
                    objPageSortBo = pageSortViewState;
                }
                pageSortViewState = GridHelper.sortGrid(grdSchemeList, e, objPageSortBo, pageSortViewState);
                loadData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        protected void grdSchemeList_RowCreated(object sender, GridViewRowEventArgs e)
        {
            //check if it is a header row
            //since allowsorting is set to true, column names are added as command arguments to
            //the linkbuttons by DOTNET API
            if (e.Row.RowType == DataControlRowType.Header)
            {
                LinkButton btnSort;
                Image image;
                //iterate through all the header cells
                foreach (TableCell cell in e.Row.Cells)
                {
                    //check if the header cell has any child controls
                    if (cell.HasControls())
                    {
                        //get reference to the button column
                        btnSort = (LinkButton)cell.Controls[0];
                        image = new Image();
                        if (pageSortViewState != null)
                        {
                            if (btnSort.CommandArgument == pageSortViewState.SortExpression)
                            {
                                //following snippet figure out whether to add the up or down arrow
                                //based on the sortdirection
                                if (pageSortViewState.SortDirection == SortDirection.Ascending.ToString())
                                {
                                    image.ImageUrl = "~/Images/Grid/sort_asc.png";
                                }
                                else
                                {
                                    image.ImageUrl = "~/Images/Grid/sort_desc.png";
                                }
                            }
                            else
                            {
                                image.ImageUrl = "~/Images/Grid/sort_both.png";
                            }
                            cell.Controls.Add(image);
                        }
                    }
                }
            }
        }

        #endregion
        #region"change PageNumber"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void changePageNumber(object sender, EventArgs e)
        {
            try
            {
                Button btnGo = new Button();
                btnGo = (Button)sender;
                PageSortBO objPageSortBo;
                objPageSortBo = new PageSortBO("DESC", "BlockName", 1, 30);
                objPageSortBo = pageSortViewState;
                int pageNumber = 1;
                pageNumber = Convert.ToInt32(txtPageNumber.Text);
                txtPageNumber.Text = String.Empty;
                objPageSortBo = pageSortViewState;
                if (((pageNumber >= 1)
                            && (pageNumber <= objPageSortBo.TotalPages)))
                {
                    objPageSortBo = pageSortViewState;
                    objPageSortBo.PageNumber = pageNumber;
                    pageSortViewState = objPageSortBo;
                    loadData();

                }
                else
                {
                    // uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, UserMessageConstants.InvalidPageNumber, true);
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        protected void lnkbtnPager_Click(object sender, EventArgs e)
        {
            try
            {
                PageSortBO objPageSortBo;
                if (pageSortViewState == null)
                {
                    objPageSortBo = new PageSortBO("DESC", "BlockName", 1, 30);
                    pageSortViewState = objPageSortBo;
                }
                else
                {
                    objPageSortBo = pageSortViewState;
                }
                GridHelper.processGridPager(sender, objPageSortBo, pageSortViewState);                
                loadData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }        

        public void loadData()
        {

            PageSortBO objPageSortBo;
            if (pageSortViewState == null)
            {
                objPageSortBo = new PageSortBO("DESC", "BlockName", 1, 30);
                pageSortViewState = objPageSortBo;
            }
            else
            {
                objPageSortBo = pageSortViewState;
            }

            int totalCount = 0;
            String searchText = txtSearch.Text;
            if (!string.IsNullOrEmpty(txtSearch.Text))
            {
                objPageSortBo = new PageSortBO("DESC", "BlockID", 1, 30);
            } 
            BlockBL objBlockBL = new BlockBL(new BlockRepo());
            DataSet resultDataSet = new DataSet();
            totalCount = objBlockBL.getBlockList(ref resultDataSet, objPageSortBo, searchText);
            grdSchemeList.DataSource = resultDataSet;
            grdSchemeList.DataBind();
            objPageSortBo.TotalRecords = totalCount;
            objPageSortBo.TotalPages = Convert.ToInt32(Math.Ceiling((Double)totalCount / objPageSortBo.PageSize));

            if (objPageSortBo.TotalPages > 0)
            {
                pnlPagination.Visible = true;
            }
            else
            {
                pnlPagination.Visible = false;
            }
            pageSortViewState = objPageSortBo;
            GridHelper.setGridViewPager(ref pnlPagination, objPageSortBo);
        }

        public void searchData()
        {
            throw new NotImplementedException();
        }

        public void populateData()
        {
            throw new NotImplementedException();
        }

        public void printData()
        {
            throw new NotImplementedException();
        }

        public void exportGridToExcel()
        {
            throw new NotImplementedException();
        }

        public void exportToPdf()
        {
            throw new NotImplementedException();
        }

        public void downloadData()
        {
            throw new NotImplementedException();
        }

        public void applyFilters()
        {
            throw new NotImplementedException();
        }

        protected void btnAddNewBlock_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Views/Pdr/Block/BlockMain.aspx");
        }

        protected void grdSchemeList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if ((e.Row.RowType == DataControlRowType.DataRow))
                {
                    //e.Row.BackColor = System.Drawing.Color.White;

                    GridViewRow row = e.Row;
                    Label lblBlockId = (Label)row.FindControl("lblBlockId");
                    e.Row.Attributes.Add("onclick", string.Format("javascript:ShowPropertyDetail('{0}')", lblBlockId.Text));
                    //e.Row.Attributes.Add("onmouseover", "mouseIn(this);");
                    //e.Row.Attributes.Add("onmouseout", "mouseOut(this);");
                    e.Row.Attributes.Add("onmouseover", "this.style.cursor='pointer'");

                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
    }
}