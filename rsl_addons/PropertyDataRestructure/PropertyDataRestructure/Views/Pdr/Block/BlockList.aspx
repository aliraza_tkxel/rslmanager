﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Pdr.Master" AutoEventWireup="true"
    CodeBehind="BlockList.aspx.cs" Inherits="PropertyDataRestructure.Views.Pdr.Block.BlockList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .dashboard th{
            background: #fff;
            border-bottom: 4px solid #8b8687;
        }
        .dashboard th a{
            color: #000 !important;
        }
        .dashboard th img{
            float:right;
        }
    </style>
    <script type="text/javascript">

        //setup before functions
        var typingTimer;                //timer identifier
        var doneTypingInterval = 1000;  //time in ms, 5 second for example

        function TypingInterval() {
            //alert("test");
            clearTimeout(typingTimer);
            //if ($("#<%= txtSearch.ClientID %>").val()) {
            typingTimer = setTimeout(searchTextChanged, doneTypingInterval);
            //}
        }

        function searchTextChanged() {
            __doPostBack('<%= txtSearch.ClientID %>', '');
        }
        function ShowPropertyDetail(pid) {

            javascript: window.location.assign('BlockMain.aspx?id=' + pid + '&requestType=block');
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:UpdateProgress ID="updateProgressReportsArea" runat="server" DisplayAfter="10">
        <ProgressTemplate>
            <div class="loading-image">
                <img alt="Please Wait" src="../../../Images/menu/progress.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    
    <asp:UpdatePanel ID="updPnlSchemeList" runat="server" style="margin-bottom: 20px;">
        <ContentTemplate>          
            <div class="portlet">
                <uim:UIMessage ID="uiMessage" runat="Server" width="100%" Visible="false"  />
                <div class="header">
                    <span class="header-label">Blocks</span>
                    <div class="form-control right">
                        <div class="field">
                            <asp:TextBox ID="txtSearch" style="padding:4px 23px !important; margin:-3px 10px 0 0;" class="styleselect styleselect-control" 
                                runat="server" AutoPostBack="false" AutoCompleteType="Search" OnTextChanged="txtSearch_TextChanged" onkeyup="TypingInterval();">
                            </asp:TextBox>
                            <asp:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="txtSearch"
                                WatermarkText="Search" WatermarkCssClass="searchbox searchText">
                            </asp:TextBoxWatermarkExtender>
                        </div>
                    </div>
                    <div class="field right">
                        <asp:Button ID="btnAddNewBlock" class="btn btn-xs btn-blue right" UseSubmitBehavior="false"  OnClick="btnAddNewBlock_Click" style="padding:3px 23px !important; margin:-3px 0 0 0;" runat="server" Text="Add New Block" />
                    </div>
                </div>
                <div class="portlet-body" style="font-size: 12px; overflow:inherit; padding-bottom:0;">
                    <div style=" border-bottom: 1px solid #A0A0A0; width: 100%; padding:0">
                        <cc1:PagingGridView ID="grdSchemeList" runat="server" AutoGenerateColumns="False" OnSorting="grdSchemeList_Sorting"
                            OnRowDataBound="grdSchemeList_RowDataBound" AllowSorting="True" PageSize="30" OnRowCreated="grdSchemeList_RowCreated"
                            Width="100%" Style="overflow: scroll" BorderWidth="0px" CssClass="dashboard webgrid table table-responsive" EmptyDataText="No Records Found"
                            GridLines="None" ShowHeaderWhenEmpty="True" AllowPaging="false">
                            <Columns>
                                <asp:TemplateField HeaderText="Block Name:" ItemStyle-CssClass="dashboard" SortExpression="BlockName">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBlockId" runat="server" Text='<%# Bind("BLOCKID") %>' Visible="false"  ></asp:Label>
                                        <asp:Label ID="lblBlockName" runat="server" Text='<%# Bind("BlockName") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle BorderStyle="None" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Development:" SortExpression="Development">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDevelopment" runat="server" Text='<%# Bind("Development") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Scheme" SortExpression="Scheme">
                                    <ItemTemplate>
                                        <asp:Label ID="lblScheme" runat="server" Text='<%# Bind("Scheme") %>'></asp:Label>
                                    </ItemTemplate>
                                   <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Phase:" SortExpression="Phase">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPhase" runat="server" Text='<%# Bind("Phase") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Address:" SortExpression="Address">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Properties:" SortExpression="Properties">
                                    <ItemTemplate>
                                        <asp:Label ID="lblProperties" runat="server" Text='<%# Bind("Properties") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle BorderColor="Black" HorizontalAlign="Left" />
                        </cc1:PagingGridView>
                    </div>
                    <asp:Panel runat="server" ID="pnlPagination" Visible="false" Style="color: Black; margin: 0 auto; width: 98%;">
                        <div style="width: 100%; padding: 15px 0 30px 0px; text-align:center;">
                            <div class="paging-left">
                                <span style="padding-right:10px;">
                                    <asp:LinkButton ID="lnkbtnPagerFirst" runat="server" Text="" CommandName="Page" CommandArgument="First" cssClass="lnk-btn"
                                        OnClick="lnkbtnPager_Click">
                                        &lt;&lt;First
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkbtnPagerPrev" Text="Prev" runat="server" CommandName="Page" CommandArgument="Prev" cssClass="lnk-btn"
                                        OnClick="lnkbtnPager_Click">
                                        &lt;Prev
                                    </asp:LinkButton>
                                </span>
                                <span style="padding-right:10px;">
                                    <b>Page:</b>
                                    <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="currentPage" />
                                    of
                                    <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />.
                                </span>
                                <span style="padding-right:20px;">
                                    <b>Result:</b>
                                    <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                                    to
                                    <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                                    of
                                    <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                                </span>
                                <span style="padding-right:10px;">
                                    <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page" CommandArgument="Next" cssClass="lnk-btn"
                                        OnClick="lnkbtnPager_Click">
                                    
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page" CommandArgument="Last" cssClass="lnk-btn"
                                        OnClick="lnkbtnPager_Click">
                                        
                                    </asp:LinkButton>
                                </span>
                            </div>
                            <div style="float: right;">
                                <asp:RangeValidator ID="rangevalidatorPageNumber" runat="server" ErrorMessage="Enter a page between 1 and "
                                    ValidationGroup="pageNumber" MinimumValue="1" MaximumValue="1" ControlToValidate="txtPageNumber"
                                    Type="Integer" SetFocusOnError="True" CssClass="Required" Display="Dynamic" />
                                <div class="field" style="margin-right: 10px;">
                                    <asp:TextBox ID="txtPageNumber" runat="server" Width="40px" ValidationGroup="pageNumber" PlaceHolder="Page"
                                    onchange="document.getElementById('txtPageNumberHidden').value = this.value;" />
                                </div>
                                <asp:Button Text="Go" runat="server" ID="btnGoPageNumber" ValidationGroup="pageNumber" UseSubmitBehavior="false"
                                    class="btn btn-xs btn-blue right" style="padding:1px 5px !important; min-width:0px;" OnClick="changePageNumber" />
                            </div>
                        </div>
                    </asp:Panel>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
