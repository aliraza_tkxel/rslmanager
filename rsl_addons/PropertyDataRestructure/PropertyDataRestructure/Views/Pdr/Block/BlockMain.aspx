﻿<%@ Page Title="" Language="C#" Culture="en-GB" MasterPageFile="~/MasterPage/Pdr.Master"
    AutoEventWireup="true" CodeBehind="BlockMain.aspx.cs" EnableEventValidation="false"
    Inherits="PropertyDataRestructure.Views.Pdr.Block.BlockMain" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register TagPrefix="block" TagName="MainDetail" Src="~/UserControls/Pdr/Block/MainDetail.ascx" %>
<%@ Register TagPrefix="block" TagName="BlockRestrictions" Src="~/UserControls/Pdr/Block/BlockRestrictions.ascx" %>
<%@ Register TagPrefix="block" TagName="Attributes" Src="~/UserControls/Pdr/SchemeBlock/Attributes.ascx" %>
<%@ Register TagPrefix="block" TagName="Provisions" Src="~/UserControls/Pdr/SchemeBlock/Provisions.ascx" %>
<%@ Register TagPrefix="block" TagName="Warranties" Src="~/UserControls/Pdr/SchemeBlock/Warranties.ascx" %>
<%@ Register TagPrefix="block" TagName="HealthSafety" Src="~/UserControls/Pdr/SchemeBlock/HealthSafety.ascx" %>
<%@ Register TagPrefix="block" TagName="Properties" Src="~/UserControls/Pdr/SchemeBlock/Properties.ascx" %>
<%@ Register TagPrefix="block" TagName="Documents" Src="~/UserControls/Pdr/SchemeBlock/Documents.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../../../Scripts/jquery-1.12.4.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.css"
        rel="stylesheet" />
    <link href="../../../Styles/frequency%20popup%20bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="../../../Styles/dialog.css" rel="stylesheet" type="text/css" />
    <link href="../../../Styles/scheduling.css" rel="stylesheet" />
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
    <script src="../../../Scripts/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../../Scripts/jquery.dialog.min.js" type="text/javascript"></script>
    <script type="text/javascript">        

        function ShowWorksRequired(control, Id) {
            document.getElementById(Id).style.display = 'table-row';
        }
        function HideWorksRequired(control, Id) {
            document.getElementById(Id).style.display = 'none';
        }
        function NumberOnly(field) {
            var re = /^[0-9.]*$/;
            // var re = /^[1-9]\d*(\.\d+)?$/;
            if (!re.test(field.value)) {
                //alert(type +' Must Be Numeric');
                field.value = field.value.replace(/[^0-9.,]/g, "");
            }
        }
        function ShowHidePatTesting(rbl, pnl) {

            var selectedvalue = $("#" + rbl.id + " input:radio:checked").val();
            if (selectedvalue == 1) {
                document.getElementById(pnl).style.display = 'table-row';
            }
            else {
                document.getElementById(pnl).style.display = 'none';
            }
        }

        function fireDefectPhotoUploadCkBoxEvent() {
            document.getElementById('<%=ckBoxDefectPhotoUpload.ClientID%>').checked = true;
            setTimeout('__doPostBack(\'<%=ckBoxDefectPhotoUpload.UniqueID%>\',\'\')', 0);
        }

        function openPhotoUploadWindow(type) {
            var url = '../../Common/UploadDocument.aspx?type=photo';

            if (type == 'defect') {
                url = url + '&photoType=defect';
            }
            if (type == 'summaryPhoto') {
                url = url + '&summaryPhoto=summaryPhoto';
            }

            wd = window.open(url, 'new', 'directories=no,height=170,width=530,resizable=no,scrollbars=no,status=no,titlebar=no,toolbar=no');

        }

        function LocationItemSelected(sender, e) {

            __doPostBack('<%= btnLocation.UniqueID %>', e.get_value());
        }
        function typeItemSelected(sender, e) {

            __doPostBack('<%= btnType.UniqueID %>', e.get_value());
        }
        function makeItemSelected(sender, e) {

            __doPostBack('<%= btnMake.UniqueID %>', e.get_value());
        }
        function modelItemSelected(sender, e) {

            __doPostBack('<%= btnModel.UniqueID %>', e.get_value());
        }
        function NumberOnly(field) {
            var re = /^[0-9]*$/;
            // var re = /^[1-9]\d*(\.\d+)?$/;
            if (!re.test(field.value)) {
                //alert(type +' Must Be Numeric');
                field.value = field.value.replace(/[^0-9.,]/g, "");
            }
        }
        function loadSelect2() {
            $('#ddlProperties').select2({
                placeholder: "Select Properties",
                multiple: true
            });
            $('#txtPropertiesPC').select2({
                placeholder: "Select Properties",
                multiple: true
            });
            $('#txtPropertiesPC1').select2({
                placeholder: "Select Properties",
                multiple: true
            });
            $('#poModalRaiseaPO').modal('show');
        }
        //////////////////////////////////////////
        function LoadFrequencyPopUp()
        {
        $('#frequencyPopUp').modal('show');
        }
        function CloseFrequencyPopUp() 
        {
        $('#frequencyPopUp').modal('hide');
        }
        function frequencySelector(ddlFrequencySelector)
        {
         var x=ddlFrequencySelector.value;
         if(x=="Weekly")
         {
         $("#everyStatment").text("week(s) on");
         $('#days').hide();
         $('#month').hide();
         $('#weeks').show();
         }
         else if(x=="Monthly")
         {
         $("#everyStatment").text("months(s)");
         $('#days').show();
         $('#month').hide();
         $('#weeks').hide();
         }
         else if(x=="Yearly")
         {
         $("#everyStatment").text("year(s) in");
         $('#days').hide();
         $('#month').show();
         $('#weeks').hide();
         }
         else 
         {
         $("#everyStatment").text("day(s)");
         $('#days').hide();
         $('#month').hide();
         $('#weeks').hide();
         }
        }
       
        function OkFrequencyPopUp()
        { 
        
            $('#frequencyPopUp').modal('hide');
        }
        function OnlyOneSelected(checkbox)
        {
        var checkBoxes=$('#weeks input:checked');
        for (var i = 0; i < checkBoxes.length; i++)
        {
        $(checkBoxes[i]).prop('checked', false);
        }
        checkBoxes=$('#days input:checked');
        for (var i = 0; i < checkBoxes.length; i++)
        {
        $(checkBoxes[i]).prop('checked', false);
        }
        checkBoxes=$('#month input:checked');
        for (var i = 0; i < checkBoxes.length; i++)
        {
        $(checkBoxes[i]).prop('checked', false);
        }

        $(checkbox).prop('checked', true);
        }

        
        //////////////////////////////////////////
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="updateProgressReportsArea" runat="server" DisplayAfter="10">
        <ProgressTemplate>
            <div class="loading-image">
                <img alt="Please Wait" src="../../../Images/menu/progress.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:Button ID="btnHidden" runat="server" Text="" UseSubmitBehavior="false" Style="display: none;" />
    <asp:Button ID="btnLocation" runat="server" Text="" OnClick="btnLocation_Click" UseSubmitBehavior="false"
        Style="display: none;" />
    <asp:Button ID="btnType" runat="server" Text="" OnClick="btnType_Click" UseSubmitBehavior="false"
        Style="display: none;" />
    <asp:Button ID="btnMake" runat="server" Text="" OnClick="btnMake_Click" UseSubmitBehavior="false"
        Style="display: none;" />
    <asp:Button ID="btnModel" runat="server" Text="" OnClick="btnModel_Click" UseSubmitBehavior="false"
        Style="display: none;" />
    <div style="border: 1px solid  #8e8e8e; width: 100%; margin: 0px; background-color: #fdfdfd;">
        <div style="width: 100%;">
            <p style="background-color: Black; height: 30px; text-align: justify; font-weight: bold;
                margin: 0 0 6px; font-size: 15px; padding-left: 8px; padding-top: 8px;">
                <font color="white">
                    <asp:Label runat="server" ID="lblHeading" Text=""></asp:Label></font>
            </p>
        </div>
        <div style="width: 100%; padding: 3px;">
            <table style="width: 100%;">
                <tr>
                    <td>
                        <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                            <asp:Label ID="lblMessage" runat="server">
                            </asp:Label>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td style="padding-left: 0px; padding-right: 1px;">
                        <div style="padding: 10px 10px 0 0px;">
                            <div style="overflow: auto; padding: 0px !important;">
                                <asp:LinkButton ID="lnkBtnMainDetailTab" OnClick="lnkBtnMainDetailTab_Click" CommandArgument="lnkBtnMainDetailTab_Click"
                                    CssClass="TabClicked" runat="server" Visible="false" BorderStyle="Solid">Main Detail: </asp:LinkButton>
                                <asp:LinkButton ID="lnkBtnAttributesTab" OnClick="lnkBtnAttributesTab_Click" CssClass="TabInitial"
                                    runat="server" Visible="false" BorderStyle="Solid">Attributes: </asp:LinkButton>
                                <asp:LinkButton ID="lnkBtnProvisionsTab" OnClick="lnkBtnProvisionsTab_Click" CssClass="TabInitial"
                                    runat="server" Visible="false" BorderStyle="Solid">Provisions: </asp:LinkButton>
                                <asp:LinkButton ID="lnkBtnWarranties" OnClick="lnkBtnWarranties_Click" CssClass="TabInitial"
                                    runat="server" Visible="false" BorderStyle="Solid">Warranties: </asp:LinkButton>
                                <asp:LinkButton ID="lnkBtnHealthSafetyTab" OnClick="lnkBtnHealthSafetyTab_Click"
                                    CssClass="TabInitial" runat="server" Visible="false" BorderStyle="Solid">Health & Safety: </asp:LinkButton>
                                <asp:LinkButton ID="lnkBtnProperties" OnClick="lnkBtnProperties_Click" CssClass="TabInitial"
                                    runat="server" Visible="false" BorderStyle="Solid">Properties: </asp:LinkButton>
                                <asp:LinkButton ID="lnkBtnDocuments" OnClick="lnkBtnDocuments_Click" CssClass="TabInitial"
                                    runat="server" Visible="false" BorderStyle="Solid">Documents: </asp:LinkButton>
                                <asp:LinkButton ID="lnkBtnBlockRestriction" OnClick="lnkBtnRestrictionsTab_Click"
                                    CommandArgument="lnkBtnBlockRestrictionTab_Click" CssClass="TabClicked" runat="server"
                                    Visible="false" BorderStyle="Solid">Restriction: </asp:LinkButton>
                                <span style="display: block; height: 27px; border-bottom: 1px solid #c5c5c5"></span>
                            </div>
                        </div>
                        <div style="clear: both; margin-bottom: 5px;">
                        </div>
                        <asp:MultiView ID="MainView" runat="server">
                            <asp:View ID="View1" runat="server">
                                <block:MainDetail ID="MainDetail" runat="server" />
                            </asp:View>
                            <asp:View ID="View2" runat="server">
                                <block:Attributes ID="ucAttribute" runat="server" />
                            </asp:View>
                            <asp:View ID="View3" runat="server">
                                <block:Provisions ID="ucProvision" runat="server" />
                            </asp:View>
                            <asp:View ID="View7" runat="server">
                                <block:Warranties ID="Warranties" runat="server" />
                            </asp:View>
                            <asp:View ID="View4" runat="server">
                                <block:HealthSafety ID="HealthSafety" runat="server" />
                            </asp:View>
                            <asp:View ID="View5" runat="server">
                                <block:Properties ID="Properties" runat="server" />
                            </asp:View>
                            <asp:View ID="View6" runat="server">
                                <block:Documents ID="Documents" runat="server" />
                            </asp:View>
                            <asp:View ID="View8" runat="server">
                                <block:BlockRestrictions ID="BlockRestriction" runat="server" />
                            </asp:View>
                        </asp:MultiView>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
