﻿<%@ Page Title="" Language="C#" Culture="en-GB" MasterPageFile="~/MasterPage/Pdr.Master"
    AutoEventWireup="true" EnableEventValidation="false" CodeBehind="SchemeDashBoard.aspx.cs"
    Inherits="PropertyDataRestructure.Views.Pdr.Scheme.Dashboard.SchemeDashBoard" %>

<%@ Register TagPrefix="scheme" TagName="Warranties" Src="~/UserControls/Pdr/SchemeBlock/Warranties.ascx" %>
<%@ Register TagPrefix="scheme" TagName="SchemeRestrictions" Src="~/UserControls/Pdr/Scheme/SchemeRestrictions.ascx" %>
<%@ Register TagName="DashBoard" TagPrefix="scheme" Src="~/UserControls/Pdr/Scheme/DashBoard.ascx" %>
<%@ Register TagName="Document" TagPrefix="scheme" Src="~/UserControls/Pdr/SchemeBlock/Documents.ascx" %>
<%@ Register TagPrefix="scheme" TagName="Properties" Src="~/UserControls/Pdr/SchemeBlock/Properties.ascx" %>
<%@ Register TagName="MainDetail" TagPrefix="scheme" Src="~/UserControls/Pdr/Scheme/MainDetail.ascx" %>
<%@ Register TagName="HealthSafety" TagPrefix="scheme" Src="~/UserControls/Pdr/SchemeBlock/HealthSafety.ascx" %>
<%@ Register TagName="Provision" TagPrefix="scheme" Src="~/UserControls/Pdr/SchemeBlock/Provisions.ascx" %>
<%@ Register TagName="Attribute" TagPrefix="scheme" Src="~/UserControls/Pdr/SchemeBlock/Attributes.ascx" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../../../../Scripts/jquery-1.12.4.js"></script>
    <script src="../../../../Scripts/jquery-ui-1.12.1.js" type="text/javascript"></script>
    <script src="../../../../Scripts/bootstrap.min.js" type="text/javascript"></script>
    <script src="../../../../Scripts/jquery.dialog.min.js" type="text/javascript"></script>
    <link href="../../../../Styles/scheduling.css" rel="stylesheet" />
    <%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>--%>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.css"
        rel="stylesheet" />
    <%--    <link href="../../../../Styles/bootstrap.css" rel="stylesheet" type="text/css" />--%>
    <link href="../../../../Styles/frequency%20popup%20bootstrap.css" rel="stylesheet"
        type="text/css" />
    <link href="../../../../Styles/dialog.css" rel="stylesheet" type="text/css" />
    <link href="../../../../Styles/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../../../../Styles/jquery.dialog.min.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/js/select2.min.js"></script>
    <script type="text/javascript">
        function loadCounts(url, loading, label) {

            var hv = $("#" + '<%= hdnSchemeId.ClientID %>').val();
            if (hv > 0) {
                var postData = JSON.stringify({ schemeId: hv
                });
                $.ajax({
                    type: "POST",
                    url: "SchemeDashBoard.aspx/" + url,
                    data: postData,
                    contentType: "application/json",
                    dataType: "json",
                    beforeSend: function (xhr) {
                        $(loading).show();
                    },
                    complete: function () {
                        $(loading).hide();
                    },

                    success: function (result) {
                        $(label).text(result.d);
                    }
                });
            }
        }
        function LoadFrequencyPopUp() {
            $('#frequencyPopUp').modal('show');
        }
        function CloseFrequencyPopUp() {
            $('#frequencyPopUp').modal('hide');
        }

        function frequencySelector(ddlFrequencySelector) {
            var x = ddlFrequencySelector.value;
            if (x == "Weekly") {
                $("#everyStatment").text("week(s) on");
                $('#days').hide();
                $('#month').hide();
                $('#weeks').show();
            }
            else if (x == "Monthly") {
                $("#everyStatment").text("months(s)");
                $('#days').show();
                $('#month').hide();
                $('#weeks').hide();
            }
            else if (x == "Yearly") {
                $("#everyStatment").text("year(s) in");
                $('#days').hide();
                $('#month').show();
                $('#weeks').hide();
            }
            else {
                $("#everyStatment").text("day(s)");
                $('#days').hide();
                $('#month').hide();
                $('#weeks').hide();
            }
        }

        function OkFrequencyPopUp() {

            $('#frequencyPopUp').modal('hide');
        }
        function OnlyOneSelected(checkbox) {
            var checkBoxes = $('#weeks input:checked');
            for (var i = 0; i < checkBoxes.length; i++) {
                $(checkBoxes[i]).prop('checked', false);
            }
            checkBoxes = $('#days input:checked');
            for (var i = 0; i < checkBoxes.length; i++) {
                $(checkBoxes[i]).prop('checked', false);
            }
            checkBoxes = $('#month input:checked');
            for (var i = 0; i < checkBoxes.length; i++) {
                $(checkBoxes[i]).prop('checked', false);
            }

            $(checkbox).prop('checked', true);
        }

        function ShowWorksRequired(control, Id) {
            document.getElementById(Id).style.display = 'table-row';
        }
        function HideWorksRequired(control, Id) {
            document.getElementById(Id).style.display = 'none';
        }
        function NumberOnly(field) {
            var re = /^[0-9.]*$/;
            // var re = /^[1-9]\d*(\.\d+)?$/;
            if (!re.test(field.value)) {
                //alert(type +' Must Be Numeric');
                field.value = field.value.replace(/[^0-9.,]/g, "");
            }
        }
        function ShowHidePatTesting(rbl, pnl) {

            var selectedvalue = $("#" + rbl.id + " input:radio:checked").val();
            if (selectedvalue == 1) {
                document.getElementById(pnl).style.display = 'table-row';
            }
            else {
                document.getElementById(pnl).style.display = 'none';
            }
        }
        function LocationItemSelected(sender, e) {

            __doPostBack('<%= btnLocation.UniqueID %>', e.get_value());
        }
        function typeItemSelected(sender, e) {

            __doPostBack('<%= btnType.UniqueID %>', e.get_value());
        }
        function makeItemSelected(sender, e) {

            __doPostBack('<%= btnMake.UniqueID %>', e.get_value());
        }
        function modelItemSelected(sender, e) {

            __doPostBack('<%= btnModel.UniqueID %>', e.get_value());
        }
        function loadSelect2() {
            $('#ddlProperties').select2({
                placeholder: "Select Properties",
                multiple: true
            });
            $('#txtPropertiesPC').select2({
                placeholder: "Select Properties",
                multiple: true
            });
            $('#txtPropertiesPC1').select2({
                placeholder: "Select Properties",
                multiple: true
            });
            $('#poModalRaiseaPO').modal('show');
        }

        function fireDefectPhotoUploadCkBoxEvent() {
            document.getElementById('<%=ckBoxDefectPhotoUpload.ClientID%>').checked = true;
            setTimeout('__doPostBack(\'<%=ckBoxDefectPhotoUpload.UniqueID%>\',\'\')', 0);
        }

        function openPhotoUploadWindow(type) {
            var url = '../../../Common/UploadDocument.aspx?type=photo';

            if (type == 'defect') {
                url = url + '&photoType=defect';
            }
            if (type == 'summaryPhoto') {
                url = url + '&summaryPhoto=summaryPhoto';
            }

            wd = window.open(url, 'new', 'directories=no,height=170,width=530,resizable=no,scrollbars=no,status=no,titlebar=no,toolbar=no');
        }
    
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="updateProgressReportsArea" runat="server" DisplayAfter="10">
        <ProgressTemplate>
            <div class="loading-image">
                <img alt="Please Wait" src="../../../../Images/menu/progress.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div id="Container">
        <%--    <asp:UpdatePanel ID="updPanelPropertyRecord" runat="server" UpdateMode="Always">
        <ContentTemplate>--%>
        <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="500px" />
        <asp:HiddenField runat="server" ID="hdnSchemeId" />
        <asp:Button ID="btnHidden" runat="server" Text="" UseSubmitBehavior="false" Style="display: none;" />
        <asp:Button ID="btnLocation" runat="server" Text="" OnClick="btnLocation_Click" UseSubmitBehavior="false"
            Style="display: none;" />
        <asp:Button ID="btnType" runat="server" Text="" OnClick="btnType_Click" UseSubmitBehavior="false"
            Style="display: none;" />
        <asp:Button ID="btnMake" runat="server" Text="" OnClick="btnMake_Click" UseSubmitBehavior="false"
            Style="display: none;" />
        <asp:Button ID="btnModel" runat="server" Text="" OnClick="btnModel_Click" UseSubmitBehavior="false"
            Style="display: none;" />
        <div style="border-bottom-style: solid; border-width: thin; border-color: #000000">
            <asp:Panel ID="pnlPropertyAddress" runat="server" CssClass="pnlPropertyAddress">
                &nbsp;&nbsp;&nbsp;
                <asp:Label ID="lblScheme" runat="server" Font-Bold="True" Text=""></asp:Label>
                <asp:Label ID="lblSchemeAddres" runat="server" Font-Bold="True"></asp:Label>
            </asp:Panel>
        </div>
        <div style="border: 1px solid  #8e8e8e; width: 100%; margin: 0px; background-color: #fdfdfd;">
            <div style="width: 100%;">
                <p style="background-color: Black; height: 30px; text-align: justify; font-weight: bold;
                    margin: 0 0 6px; font-size: 13px; padding-left: 8px; padding-top: 8px;">
                    <font color="white">
                        <asp:Label runat="server" ID="lblHeading" Text="Scheme Set Up:"></asp:Label></font>
                </p>
            </div>
            <div style="width: 100%; padding: 3px;">
                <table style="width: 100%;">
                    <tr>
                        <td>
                            <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                                <asp:Label ID="lblMessage" runat="server">
                                </asp:Label>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding-left: 0px; padding-right: 1px; padding-top: 4px;">
                            <div style="padding: 10px 10px 0 0px;">
                                <div style="overflow: auto; padding: 0px !important;">
                                    <asp:LinkButton ID="lnkBtnDashBoardTab" OnClick="lnkBtnDashBoardTab_Click" CssClass="TabClicked"
                                        runat="server" BorderStyle="Solid">DashBoard: </asp:LinkButton>
                                    <asp:LinkButton ID="lnkBtnMainDetailTab" OnClick="lnkBtnMainDetailTab_Click" CssClass="TabInitial"
                                        runat="server" BorderStyle="Solid">Main Details: </asp:LinkButton>
                                    <asp:LinkButton ID="lnkBtnAttributesTab" OnClick="lnkBtnAttributesTab_Click" CssClass="TabInitial"
                                        runat="server" BorderStyle="Solid">Attributes: </asp:LinkButton>
                                    <asp:LinkButton ID="lnkBtnProvisionsTab" OnClick="lnkBtnProvisionsTab_Click" CssClass="TabInitial"
                                        runat="server" BorderStyle="Solid">Provisions: </asp:LinkButton>
                                    <asp:LinkButton ID="lnkBtnWarrantiesTab" OnClick="lnkBtnWarrantiesTab_Click" CssClass="TabInitial"
                                        runat="server" BorderStyle="Solid">Warranties: </asp:LinkButton>
                                    <asp:LinkButton ID="lnkBtnHealthSafety" OnClick="lnkBtnHealthSafety_Click" CssClass="TabInitial"
                                        runat="server" BorderStyle="Solid">Health & Safety: </asp:LinkButton>
                                    <asp:LinkButton ID="lnkBtnProperties" OnClick="lnkBtnProperties_Click" CssClass="TabInitial"
                                        runat="server" BorderStyle="Solid">Properties: </asp:LinkButton>
                                    <asp:LinkButton ID="lnkBtnDocuments" OnClick="lnkBtnDocuments_Click" CssClass="TabInitial"
                                        runat="server" BorderStyle="Solid">Documents: </asp:LinkButton>
                                    <asp:LinkButton ID="lnkBtnSchemeRestriction" OnClick="lnkBtnRestrictionsTab_Click"
                                        CommandArgument="lnkBtnSchemeRestrictionTab_Click" CssClass="TabClicked" runat="server"
                                        BorderStyle="Solid">Restriction: </asp:LinkButton>
                                    <span style="display: block; height: 27px; border-bottom: 1px solid #c5c5c5"></span>
                                </div>
                            </div>
                            <div style="clear: both; margin-bottom: 5px;">
                            </div>
                            <asp:MultiView ID="MainView" runat="server" ActiveViewIndex="0">
                                <asp:View ID="View1" runat="server">
                                    <scheme:DashBoard runat="server" ID="ucDashBoard" />
                                </asp:View>
                                <asp:View ID="View2" runat="server">
                                    <scheme:MainDetail runat="server" ID="ucMainDetail" />
                                </asp:View>
                                <asp:View runat="server" ID="View3">
                                    <scheme:Attribute runat="server" ID="ucAttribute" />
                                </asp:View>
                                <asp:View ID="View4" runat="server">
                                    <scheme:Provision runat="server" ID="ucProvisions" />
                                </asp:View>
                                <asp:View ID="View5" runat="server">
                                    <scheme:Warranties ID="ucWarranties" runat="server" />
                                </asp:View>
                                <asp:View ID="View6" runat="server">
                                    <scheme:HealthSafety ID="ucHealthSafety" runat="server" />
                                </asp:View>
                                <asp:View ID="View7" runat="server">
                                    <scheme:Properties ID="ucProperties" runat="server" />
                                </asp:View>
                                <asp:View ID="View8" runat="server">
                                    <scheme:Document runat="server" ID="ucDocument" />
                                </asp:View>
                                <asp:View ID="View9" runat="server">
                                    <scheme:SchemeRestrictions ID="ucSchemeRestriction" runat="server" />
                                </asp:View>
                            </asp:MultiView>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</asp:Content>
