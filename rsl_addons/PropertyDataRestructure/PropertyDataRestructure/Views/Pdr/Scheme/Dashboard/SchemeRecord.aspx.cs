﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PDR_Utilities.Constants;
using System.Data;
using PDR_BusinessLogic.Scheme;
using PropertyDataRestructure.Base;
using PropertyDataRestructure.Interface;
using PDR_DataAccess.Scheme;
using AjaxControlToolkit;
using PDR_Utilities.Managers;
using PDR_BusinessLogic.SchemeBlock;
using PDR_DataAccess.SchemeBlock;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_BusinessLogic.CyclicalServices;
using PDR_DataAccess.CyclicalServices;

namespace PropertyDataRestructure.Views.Pdr.Scheme.Dashboard
{
    public partial class SchemeRecord : PageBase, IListingPage
    {
        #region Properties
        #endregion

        #region Page Load Event
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                this.ucProperties.delegateInvoke = new Action<bool, int, LinkButton>(enableDisableTabs);

                if (!IsPostBack)
                {
                    objSession.TreeItemId = 0;
                    uiMessage.hideMessage();
                    int schemeId = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);
                    string source = Request.QueryString[ApplicationConstants.Source];
                    if (schemeId != 0)
                    {
                        hdnSchemeId.Value = schemeId.ToString();
                        populateData();

                        if (source != null && source == "search")
                        {
                            lnkBtnDashBoardTab.Visible = true;
                            enableDisableTabs(true, 0, lnkBtnDashBoardTab);
                            ucDashBoard.populateData();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        #endregion
        #region "lnk Btn DashBoard Tab Click"
        protected void lnkBtnDashBoardTab_Click(object sender, EventArgs e)
        {
            try
            {
                lnkBtnDashBoardTab.CssClass = ApplicationConstants.TabClickedCssClass;
                lnkBtnAttributesTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnWarrantiesTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnProperties.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnDocuments.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnSchemeRestrictions.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnProvisionsTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnCyclical.CssClass = ApplicationConstants.TabInitialCssClass;
                MainView.ActiveViewIndex = 0;
                objSession.IsProvisionClicked = false;
                objSession.TreeItemId = 0;
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion


        #region "link Button Attributes Tab Click"

        protected void lnkBtnAttributesTab_Click(object sender, EventArgs e)
        {
            try
            {
                lnkBtnDashBoardTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnAttributesTab.CssClass = ApplicationConstants.TabClickedCssClass;
                lnkBtnWarrantiesTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnProperties.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnDocuments.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnSchemeRestrictions.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnProvisionsTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnCyclical.CssClass = ApplicationConstants.TabInitialCssClass;
                MainView.ActiveViewIndex = 1;
                objSession.IsProvisionClicked = false;
                objSession.TreeItemId = 0;
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region lnkBtnProvisionsTab_Click
        protected void lnkBtnProvisionsTab_Click(object sender, EventArgs e)
        {
            try
            {
                lnkBtnDashBoardTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnAttributesTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnProvisionsTab.CssClass = ApplicationConstants.TabClickedCssClass;
                lnkBtnWarrantiesTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnProperties.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnDocuments.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnSchemeRestrictions.CssClass = ApplicationConstants.TabInitialCssClass;
                objSession.ProvisionDataSet = null;
                MainView.ActiveViewIndex = 2;
                lnkBtnCyclical.CssClass = ApplicationConstants.TabInitialCssClass;

                objSession.TreeItemId = 0;
                objSession.IsProvisionClicked = true;
                ucProvisions.DisableMEServicing();


                ProvisionsBL objProvisionsBL = new ProvisionsBL(new ProvisionsRepo());
                int schemeId = Convert.ToInt32(Request.QueryString[ApplicationConstants.Id]);
                DataSet provisionDS = objProvisionsBL.GetProvisionsData(schemeId, null);
                objSession.ProvisionDataSet = provisionDS; //Save Dataset in session to load selected item data
                ucProvisions.resetTreeView();
                ucProvisions.PopulateTreeNode(provisionDS);
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region "link Button Warranties Tab Click"
        protected void lnkBtnWarrantiesTab_Click(object sender, EventArgs e)
        {
            try
            {
                lnkBtnDashBoardTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnAttributesTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnDocuments.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnWarrantiesTab.CssClass = ApplicationConstants.TabClickedCssClass;
                lnkBtnProperties.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnSchemeRestrictions.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnProvisionsTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnCyclical.CssClass = ApplicationConstants.TabInitialCssClass;
                MainView.ActiveViewIndex = 3;
                ucWarranties.loadData();
                objSession.IsProvisionClicked = false;
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion
        protected void lnkBtnCyclical_Click(object sender, EventArgs e)
        {
            try
            {
                lnkBtnDashBoardTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnAttributesTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnWarrantiesTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnProperties.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnDocuments.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnSchemeRestrictions.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnProvisionsTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnCyclical.CssClass = ApplicationConstants.TabClickedCssClass;

                ucCyclical.populateData();
                MainView.ActiveViewIndex = 4;
                objSession.IsProvisionClicked = false;
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #region "link Button Properties Tab Click"
        protected void lnkBtnProperties_Click(object sender, EventArgs e)
        {
            try
            {
                lnkBtnDashBoardTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnAttributesTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnWarrantiesTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnDocuments.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnProperties.CssClass = ApplicationConstants.TabClickedCssClass;
                lnkBtnSchemeRestrictions.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnProvisionsTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnCyclical.CssClass = ApplicationConstants.TabInitialCssClass;
                MainView.ActiveViewIndex = 5;
                ucProperties.loadData();
                objSession.IsProvisionClicked = false;
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region "link Button Documents Tab Click"
        protected void lnkBtnDocuments_Click(object sender, EventArgs e)
        {
            try
            {
                lnkBtnDashBoardTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnAttributesTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnWarrantiesTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnProperties.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnDocuments.CssClass = ApplicationConstants.TabClickedCssClass;
                lnkBtnProvisionsTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnSchemeRestrictions.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnCyclical.CssClass = ApplicationConstants.TabInitialCssClass;
                ucDocument.populateDropDowns();
                ucDocument.populateData();
                MainView.ActiveViewIndex = 6;
                objSession.IsProvisionClicked = false;
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region "link Button Scheme Restrictions Tab Click"
        protected void lnkBtnSchemeRestrictions_Click(object sender, EventArgs e)
        {
            try
            {
                lnkBtnDashBoardTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnAttributesTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnWarrantiesTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnProperties.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnDocuments.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnProvisionsTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnSchemeRestrictions.CssClass = ApplicationConstants.TabClickedCssClass;
                lnkBtnCyclical.CssClass = ApplicationConstants.TabInitialCssClass;
                ucSchemeRestrictions.loadDataReadOnlyView() ;
                MainView.ActiveViewIndex = 7;
                objSession.IsProvisionClicked = false;
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion



        #region "btnLocation click event"
        /// <summary>
        /// btnLocation click event used on add appliance location field
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void btnLocation_Click(object sender, EventArgs e)
        {
            try
            {
                int locationId = Convert.ToInt32(Request["__EVENTARGUMENT"]);
                //UserControl attributesControl = (UserControl)MainView.FindControl("Attributes");
                UserControl ucAttributeDetail = (UserControl)ucAttribute.FindControl("ucAttributeDetail");
                MultiView detailMainView = (MultiView)ucAttributeDetail.FindControl("MainView");
                UserControl ucApplianceTab = (UserControl)detailMainView.FindControl("ucAppliances");
                HiddenField hdnSelectedLocationId = (HiddenField)ucApplianceTab.FindControl("hdnSelectedLocationId");
                hdnSelectedLocationId.Value = locationId.ToString();
                AjaxControlToolkit.ModalPopupExtender mdlPopUpAddAppliance = (AjaxControlToolkit.ModalPopupExtender)ucApplianceTab.FindControl("mdlPopUpAddAppliance");
                mdlPopUpAddAppliance.Show();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region "btnType click event"
        /// <summary>
        /// btnType click event used on add appliance location field
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void btnType_Click(object sender, EventArgs e)
        {
            try
            {
                int typeId = Convert.ToInt32(Request["__EVENTARGUMENT"]);
                UserControl attributesControl = (UserControl)MainView.FindControl("Attributes");
                UserControl ucAttributeDetail = (UserControl)ucAttribute.FindControl("ucAttributeDetail");
                MultiView detailMainView = (MultiView)ucAttributeDetail.FindControl("MainView");
                UserControl ucApplianceTab = (UserControl)detailMainView.FindControl("ucAppliances");
                HiddenField hdnSelectedTypeId = (HiddenField)ucApplianceTab.FindControl("hdnSelectedTypeId");
                hdnSelectedTypeId.Value = typeId.ToString();
                AjaxControlToolkit.ModalPopupExtender mdlPopUpAddAppliance = (AjaxControlToolkit.ModalPopupExtender)ucApplianceTab.FindControl("mdlPopUpAddAppliance");
                mdlPopUpAddAppliance.Show();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region "btnMake click event"
        /// <summary>
        /// btnMake click event used on add appliance location field
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void btnMake_Click(object sender, EventArgs e)
        {
            try
            {
                int typeId = Convert.ToInt32(Request["__EVENTARGUMENT"]);
                UserControl attributesControl = (UserControl)MainView.FindControl("Attributes");
                UserControl ucAttributeDetail = (UserControl)ucAttribute.FindControl("ucAttributeDetail");
                MultiView detailMainView = (MultiView)ucAttributeDetail.FindControl("MainView");
                UserControl ucApplianceTab = (UserControl)detailMainView.FindControl("ucAppliances");
                HiddenField hdnSelectedmakeId = (HiddenField)ucApplianceTab.FindControl("hdnSelectedmakeId");
                hdnSelectedmakeId.Value = typeId.ToString();
                AjaxControlToolkit.ModalPopupExtender mdlPopUpAddAppliance = (AjaxControlToolkit.ModalPopupExtender)ucApplianceTab.FindControl("mdlPopUpAddAppliance");
                mdlPopUpAddAppliance.Show();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region "btnModel click event"
        /// <summary>
        /// btnMake click event used on add appliance location field
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void btnModel_Click(object sender, EventArgs e)
        {
            try
            {
                int typeId = Convert.ToInt32(Request["__EVENTARGUMENT"]);
                UserControl attributesControl = (UserControl)MainView.FindControl("Attributes");
                UserControl ucAttributeDetail = (UserControl)ucAttribute.FindControl("ucAttributeDetail");
                MultiView detailMainView = (MultiView)ucAttributeDetail.FindControl("MainView");
                UserControl ucApplianceTab = (UserControl)detailMainView.FindControl("ucAppliances");
                HiddenField hdnSelectedModelId = (HiddenField)ucApplianceTab.FindControl("hdnSelectedModelId");
                hdnSelectedModelId.Value = typeId.ToString();
                AjaxControlToolkit.ModalPopupExtender mdlPopUpAddAppliance = (AjaxControlToolkit.ModalPopupExtender)ucApplianceTab.FindControl("mdlPopUpAddAppliance");
                mdlPopUpAddAppliance.Show();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion


        #region "get Total ASB Count"
        /// <summary>
        /// Returns the count of Total ASB Count with in scheme.
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        [System.Web.Services.WebMethod()]
        public static string getTotalASBCount(string schemeId)
        {
            int count = 0;
            DataSet resultDataSet = new DataSet();
            SchemeBL objSchemeBl = new SchemeBL(new SchemeRepo());
            resultDataSet = objSchemeBl.getSchemeASBCount(Convert.ToInt32(schemeId));
            if (resultDataSet.Tables[0].Rows.Count > 0)
            {
                count = Convert.ToInt32(resultDataSet.Tables[0].Rows[0]["ASB"]);
            }
            return count.ToString();
        }
        #endregion

        #region "get Total Repair Count"
        /// <summary>
        /// Returns the count of Total Repairs with in scheme.
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        [System.Web.Services.WebMethod()]
        public static string getSchemeRepairsCount(string schemeId)
        {
            int count = 0;
            DataSet resultDataSet = new DataSet();
            SchemeBL objSchemeBl = new SchemeBL(new SchemeRepo());
            resultDataSet = objSchemeBl.getSchemeRepairsCount(Convert.ToInt32(schemeId));
            if (resultDataSet.Tables[0].Rows.Count > 0)
            {
                count = Convert.ToInt32(resultDataSet.Tables[0].Rows[0]["RepairCount"]);
            }
            return count.ToString();
        }
        #endregion

        #region "get Total Complaints Count"
        /// <summary>
        /// Returns the count of Total Complaints with in scheme.
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        [System.Web.Services.WebMethod()]
        public static string getSchemeComplaintsCount(string schemeId)
        {
            int count = 0;
            //DataSet resultDataSet = new DataSet();
            //SchemeBL objSchemeBl = new SchemeBL();
            //resultDataSet = objSchemeBl.getSchemeRepairsCount(Convert.ToInt32(schemeId));
            //if (resultDataSet.Tables[0].Rows.Count > 0)
            //{
            //    count = Convert.ToInt32(resultDataSet.Tables[0].Rows[0]["RepairCount"]);
            //}
            return count.ToString();
        }
        #endregion

        #region "get Total Valuation Count"
        /// <summary>
        /// Returns the count of Total Valuation with in scheme.
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        [System.Web.Services.WebMethod()]
        public static string getSchemeValuationCount(string schemeId)
        {
            int count = 0;
            //DataSet resultDataSet = new DataSet();
            //SchemeBL objSchemeBl = new SchemeBL();
            //resultDataSet = objSchemeBl.getSchemeRepairsCount(Convert.ToInt32(schemeId));
            //if (resultDataSet.Tables[0].Rows.Count > 0)
            //{
            //    count = Convert.ToInt32(resultDataSet.Tables[0].Rows[0]["RepairCount"]);
            //}
            return count.ToString();
        }
        #endregion

        #region "get Total Arrears Count"
        /// <summary>
        /// Returns the count of Total Arrears with in scheme.
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        [System.Web.Services.WebMethod()]
        public static string getSchemeArrearsCount(string schemeId)
        {
            int count = 0;
            //DataSet resultDataSet = new DataSet();
            //SchemeBL objSchemeBl = new SchemeBL();
            //resultDataSet = objSchemeBl.getSchemeRepairsCount(Convert.ToInt32(schemeId));
            //if (resultDataSet.Tables[0].Rows.Count > 0)
            //{
            //    count = Convert.ToInt32(resultDataSet.Tables[0].Rows[0]["RepairCount"]);
            //}
            return count.ToString();
        }
        #endregion

        #region "get Total Communal Boiler Count"
        /// <summary>
        /// Returns the count of Total Communal Boiler with in scheme.
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        [System.Web.Services.WebMethod()]
        public static string getSchemeCommunalCount(string schemeId)
        {
            int count = 0;
            //DataSet resultDataSet = new DataSet();
            //SchemeBL objSchemeBl = new SchemeBL();
            //resultDataSet = objSchemeBl.getSchemeRepairsCount(Convert.ToInt32(schemeId));
            //if (resultDataSet.Tables[0].Rows.Count > 0)
            //{
            //    count = Convert.ToInt32(resultDataSet.Tables[0].Rows[0]["RepairCount"]);
            //}
            return count.ToString();
        }
        #endregion

        #region "get Unassigned Services Count"
        /// <summary>
        /// get Unassigned Services Count
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        [System.Web.Services.WebMethod()]
        public static string getUnassignedServicesCount(string schemeId)
        {
            int count = 0;
            DataSet resultDataset = new DataSet();
            CyclicalServicesBL objBl = new CyclicalServicesBL(new CyclicalServicesRepo());
            count = objBl.getUnassignedServices(ref resultDataset, "", 1, 100000, "ServiceItemId", "DESC", 1, Convert.ToInt32(schemeId));

            return count.ToString();
        }
        #endregion

        #region IListingPage Implementation
        public void loadData()
        {
            throw new NotImplementedException();
        }

        public void searchData()
        {
            throw new NotImplementedException();
        }

        public void populateData()
        {
            DataSet resultDataSet = new DataSet();
            SchemeBL objSchemeBl = new SchemeBL(new SchemeRepo());
            resultDataSet = objSchemeBl.getSchemeDetailBySchemeId(Convert.ToInt32(hdnSchemeId.Value));
            if (resultDataSet.Tables[0].Rows.Count > 0)
            {
                objSession.SchemeDetail = resultDataSet;
                lblScheme.Text = "Scheme > " + resultDataSet.Tables[0].Rows[0]["SCHEMENAME"].ToString();
                string Address = resultDataSet.Tables[0].Rows[0]["Address1"].ToString() +
                    (resultDataSet.Tables[0].Rows[0]["Town"] != DBNull.Value && resultDataSet.Tables[0].Rows[0]["Town"] != string.Empty ? "," + resultDataSet.Tables[0].Rows[0]["Town"] : string.Empty) +
                     (resultDataSet.Tables[0].Rows[0]["PostCode"] != DBNull.Value && resultDataSet.Tables[0].Rows[0]["PostCode"] != string.Empty ? "," + resultDataSet.Tables[0].Rows[0]["PostCode"] : string.Empty);
                lblSchemeAddres.Text = Address;
                Label lblSchemeName = (Label)ucDashBoard.FindControl("lblScheme");
                lblSchemeName.Text = resultDataSet.Tables[0].Rows[0]["SCHEMENAME"].ToString();
            }
        }


        public void printData()
        {
            throw new NotImplementedException();
        }

        public void exportGridToExcel()
        {
            throw new NotImplementedException();
        }

        public void exportToPdf()
        {
            throw new NotImplementedException();
        }

        public void downloadData()
        {
            throw new NotImplementedException();
        }

        public void applyFilters()
        {
            throw new NotImplementedException();
        }

        public void enableDisableTabs(bool enable, int viewIndex, LinkButton lnkBtnSelected)
        {
            lnkBtnDashBoardTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnAttributesTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnWarrantiesTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnProperties.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnDocuments.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnSchemeRestrictions.CssClass = ApplicationConstants.TabInitialCssClass;  
            lnkBtnProvisionsTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnAttributesTab.Enabled = enable;
            lnkBtnWarrantiesTab.Enabled = enable;
            lnkBtnProperties.Enabled = enable;
            lnkBtnDocuments.Enabled = enable;
            lnkBtnProvisionsTab.Enabled = enable;
            lnkBtnSchemeRestrictions.Enabled = enable; 
            lnkBtnSelected.CssClass = ApplicationConstants.TabClickedCssClass;
            MainView.ActiveViewIndex = viewIndex;
        }

        #endregion

        #region "Search Location"
        /// <summary>
        /// Returns properties 
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        [System.Web.Services.WebMethod()]
        public static List<string> getApplianceLocations(string prefixText, int count)
        {
            try
            {
                DataSet resultDataset = new DataSet();
                AttributesBL objAttrBl = new AttributesBL(new AttributesRepo());
                // Dim objReportsBl As ReportsBL = New ReportsBL()
                resultDataset = objAttrBl.getApplianceLocations(prefixText);

                List<string> result = new List<string>();

                foreach (DataRow row in resultDataset.Tables[0].Rows)
                {
                    //result.Add(String.Format("{0}/{1}", row("Description"), row("FaultRepairListID")))
                    string item = AutoCompleteExtender.CreateAutoCompleteItem(row[ApplicationConstants.DefaultDropDownDataTextField].ToString(), row[ApplicationConstants.DefaultDropDownDataValueField].ToString());
                    result.Add(item);
                }

                return result;
            }
            catch (Exception ex)
            {

                ExceptionPolicy.HandleException(ex, "Exception Policy");
                throw ex;
            }

        }

        #endregion

        #region "Search Appliance Type"
        /// <summary>
        /// Returns properties 
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        [System.Web.Services.WebMethod()]
        public static List<string> getApplianceType(string prefixText, int count)
        {

            DataSet resultDataset = new DataSet();
            AttributesBL objAttrBl = new AttributesBL(new AttributesRepo());
            // Dim objReportsBl As ReportsBL = New ReportsBL()
            resultDataset = objAttrBl.getApplianceType(prefixText);

            List<string> result = new List<string>();

            foreach (DataRow row in resultDataset.Tables[0].Rows)
            {
                //result.Add(String.Format("{0}/{1}", row("Description"), row("FaultRepairListID")))
                string item = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(row[ApplicationConstants.DefaultDropDownDataTextField].ToString(), row[ApplicationConstants.DefaultDropDownDataValueField].ToString());
                result.Add(item);
            }

            return result;

        }

        #endregion

        #region "Search Appliance Manufacturer"
        /// <summary>
        /// Returns properties 
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        [System.Web.Services.WebMethod()]
        public static List<string> getApplianceManufacturer(string prefixText, int count)
        {

            DataSet resultDataset = new DataSet();
            AttributesBL objAttrBl = new AttributesBL(new AttributesRepo());
            // Dim objReportsBl As ReportsBL = New ReportsBL()
            resultDataset = objAttrBl.getMake(prefixText);

            List<string> result = new List<string>();

            foreach (DataRow row in resultDataset.Tables[0].Rows)
            {
                //result.Add(String.Format("{0}/{1}", row("Description"), row("FaultRepairListID")))
                string item = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(row[ApplicationConstants.DefaultDropDownDataTextField].ToString(), row[ApplicationConstants.DefaultDropDownDataValueField].ToString());
                result.Add(item);
            }

            return result;

        }

        #endregion

        #region "Search Appliance Model"
        /// <summary>
        /// Returns properties 
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        [System.Web.Services.WebMethod()]
        public static List<string> getApplianceModel(string prefixText, int count)
        {

            DataSet resultDataset = new DataSet();
            AttributesBL objAttrBl = new AttributesBL(new AttributesRepo());
            resultDataset = objAttrBl.getModelsList(prefixText);

            List<string> result = new List<string>();

            foreach (DataRow row in resultDataset.Tables[0].Rows)
            {
                //result.Add(String.Format("{0}/{1}", row("Description"), row("FaultRepairListID")))
                string item = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(row[ApplicationConstants.DefaultDropDownDataTextField].ToString(), row[ApplicationConstants.DefaultDropDownDataValueField].ToString());
                result.Add(item);
            }

            return result;

        }

        #endregion
    }
}