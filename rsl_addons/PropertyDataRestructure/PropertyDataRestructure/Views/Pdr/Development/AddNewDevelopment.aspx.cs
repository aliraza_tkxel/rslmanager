﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PDR_BusinessLogic.Development;
using System.Data;
using PDR_BusinessObject.Phase;
using System.IO;
using AjaxControlToolkit;
using PDR_Utilities.Constants;
using PDR_BusinessObject.Development;
using PDR_Utilities.Managers;
using PDR_Utilities.Helpers;
using System.Drawing;

using PDR_BusinessObject.SchemeBlock;
using PDR_DataAccess.Development;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PropertyDataRestructure.Base;
using PropertyDataRestructure.Interface;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using PDR_BusinessLogic.Reports;
using PDR_DataAccess.Reports;
using PDR_BusinessLogic.SchemeBlock;
using PDR_DataAccess.SchemeBlock;


namespace PropertyDataRestructure.Views.Pdr.Development
{
    public partial class AddNewDevelopment : PageBase, IAddEditPage
    {
        #region"Properties"
        /// <summary>
        /// 
        /// </summary>
        //SessionManager objSession = new SessionManager();

        List<int> PhaseList = new List<int>();
        List<PhaseBO> objPhaseList = new List<PhaseBO>();
        DevelopmentBL objDevelopmentBL = new DevelopmentBL(new DevelopmentRepo());
        bool IsError = false;
        string message = string.Empty;


        #endregion

        #region "Events Handling"

        /// <summary>
        /// All Events of Add New Development
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 

        #region "Page Load Event"

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {

                    //Populate Controls
                    //objSession.DocIdList = DocIdList;
                    populateDropDowns();
                    populateDevelopmentInfo();
                    populatePhaseDropdown();
                    populateBlockData();
                    populateDevelopmentDocuments();
                    if (Request.QueryString[PathConstants.DevelopmentId] == null)
                    {
                        objSession.DocIdList = null;
                    }
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        #endregion

        #region "Add New Phase Btn Click"
        /// <summary>
        /// Add New Phase Btn click, reset all control for new data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAddNewPhase_Click(object sender, EventArgs e)
        {
            txtPhaseName.Text = String.Empty;
            txtConstructionStart.Text = String.Empty;
            txtAnticipatedCompletion.Text = String.Empty;
            txtActualCompletion.Text = String.Empty;
            txtManagement.Text = String.Empty;
            if (ddlDevelopmentPhase.SelectedItem != null)
            {
                ddlDevelopmentPhase.SelectedIndex = 0;
            }
            ddlScheme.SelectedIndex = 0;
        }
        #endregion

        #region "Save Phase"
        /// <summary>
        /// Save Phase Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSavePhase_Click(object sender, EventArgs e)
        {
            try
            {
                DataSet phaseDataSet = new DataSet();
                PhaseBO objPhase = new PhaseBO();

                int developmentId = Convert.ToInt32(ViewState[ViewStateConstants.DevelopmenId]);
                if (developmentId > 0)
                {
                    objPhase.DevelopmentId = developmentId;
                }
                else
                {
                    objPhase.DevelopmentId = 0;
                }


                PhaseList = (List<int>)ViewState[ViewStateConstants.PhaseIdList];

                if (PhaseList == null)
                {
                    PhaseList = new List<int>();
                }

                int PhaseId = 0;
                if (ddlDevelopmentPhase.SelectedItem != null)
                {
                    objPhase.PhaseId = Convert.ToInt32(ddlDevelopmentPhase.SelectedItem.Value);
                }
                else
                {
                    objPhase.PhaseId = 0;
                }

                if (!string.IsNullOrEmpty(txtPhaseName.Text))
                {
                    objPhase.PhaseName = txtPhaseName.Text;
                }
                else
                {
                    uiMessage.isError = true;
                    uiMessage.messageText = UserMessageConstants.PhaseNameNotFound;
                    return;
                }
                if (!string.IsNullOrEmpty(txtConstructionStart.Text))
                {
                    objPhase.ConstructionStart = Convert.ToDateTime(txtConstructionStart.Text);
                }
                if (!string.IsNullOrEmpty(txtActualCompletion.Text))
                {
                    objPhase.ActualCompletion = Convert.ToDateTime(txtActualCompletion.Text);
                }
                if (!string.IsNullOrEmpty(txtAnticipatedCompletion.Text))
                {
                    objPhase.AnticipatedCompletion = Convert.ToDateTime(txtAnticipatedCompletion.Text);
                }
                if (!string.IsNullOrEmpty(txtManagement.Text))
                {
                    objPhase.Management = Convert.ToDateTime(txtManagement.Text);
                }



                objPhase.SchemeId = Convert.ToInt32(ddlScheme.SelectedItem.Value);
                objDevelopmentBL.SavePhase(objPhase, ref PhaseId);

                objPhase.PhaseId = PhaseId;



                PhaseList.Add(PhaseId);

                ViewState.Add(ViewStateConstants.PhaseIdList, PhaseList);

                //if (objSession.PhaseIdList == null)
                //{
                //    objSession.PhaseIdList = PhaseList;
                //}
                //else
                //{
                //    objSession.PhaseIdList.Add(PhaseId);
                //}
                ddlDevelopmentPhase.Enabled = true;
                PopulatePhaseDDL(PhaseList);
                // populateDropDowns();
                updPnlPhase.Update();
                uiMessage.messageText = UserMessageConstants.PhaseSavedSuccessfuly;
                uiMessage.showInformationMessage(uiMessage.messageText);
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region "Populate Phase on selecting from Dropdown"
        /// <summary>
        /// Populate Phase data on selecting phase from dropdown
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlDevelopmentPhase_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                int phaseId = Convert.ToInt32(ddlDevelopmentPhase.SelectedItem.Value);
                DevelopmentBL objDevelopmentBL = new DevelopmentBL(new DevelopmentRepo());
                DataSet resultData = new DataSet();
                if (phaseId != 0)
                {

                    objDevelopmentBL.getPhaseInfo(ref resultData, phaseId);
                    if (resultData.Tables[0].Rows.Count > 0)
                    {
                        txtPhaseName.Text = resultData.Tables[0].Rows[0]["PhaseName"].ToString();
                        if (!string.IsNullOrEmpty(resultData.Tables[0].Rows[0]["ConstructionStart"].ToString()))
                        {
                            txtConstructionStart.Text = Convert.ToDateTime(resultData.Tables[0].Rows[0]["ConstructionStart"]).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            txtConstructionStart.Text = string.Empty;
                        }
                        if (!string.IsNullOrEmpty(resultData.Tables[0].Rows[0]["AnticipatedCompletion"].ToString()))
                        {
                            txtActualCompletion.Text = Convert.ToDateTime(resultData.Tables[0].Rows[0]["AnticipatedCompletion"]).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            txtActualCompletion.Text = string.Empty;
                        }
                        if (!string.IsNullOrEmpty(resultData.Tables[0].Rows[0]["ActualCompletion"].ToString()))
                        {
                            txtAnticipatedCompletion.Text = Convert.ToDateTime(resultData.Tables[0].Rows[0]["ActualCompletion"]).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            txtAnticipatedCompletion.Text = string.Empty;
                        }
                        if (!string.IsNullOrEmpty(resultData.Tables[0].Rows[0]["HandoverintoManagement"].ToString()))
                        {
                            txtManagement.Text = Convert.ToDateTime(resultData.Tables[0].Rows[0]["HandoverintoManagement"]).ToString("dd/MM/yyyy");
                        }
                        else
                        {
                            txtManagement.Text = string.Empty;
                        }
                        ddlScheme.SelectedValue = resultData.Tables[0].Rows[0]["SCHEMEID"].ToString();
                    }
                }
                else
                {
                    txtPhaseName.Text = String.Empty;
                    txtConstructionStart.Text = String.Empty;
                    txtAnticipatedCompletion.Text = String.Empty;
                    txtActualCompletion.Text = String.Empty;
                    txtManagement.Text = String.Empty;
                    ddlDevelopmentPhase.SelectedIndex = 0;
                    ddlScheme.SelectedIndex = 0;
                }
                updPnlPhase.Update();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region "A sync File Upload Event"
        /// <summary>
        /// A sync File Upload Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void AsyncFileUpload1_UploadedComplete(object sender, AjaxFileUploadEventArgs e)
        {
            //lblUserMessage.Text = e.FileName;

            List<int> DocList = new List<int>();
            try
            {
                uiMessageDoc.hideMessage();
                //if (validateData())
                {
                    string filename = e.FileName;
                    int DocId = 0;


                    string strDestPath = ConfigHelper.GetDevelopmentDocUploadPath(); ;
                    //AsyncFileUpload1.SaveAs(@strDestPath + filename);
                    DevelopmentBL objDevelopmentBL = new DevelopmentBL(new DevelopmentRepo());
                    DevelopmentDocumentsBO objDevelopmentDocs = new DevelopmentDocumentsBO();
                    filename = FileHelper.getUniqueFileName(filename);
                    dynamic fileDirectoryPath = Server.MapPath(strDestPath);

                    if ((Directory.Exists(fileDirectoryPath) == false))
                    {
                        Directory.CreateDirectory(fileDirectoryPath);
                    }

                    objDevelopmentDocs.DocPath = Server.MapPath(strDestPath);
                    objDevelopmentDocs.FileType = System.IO.Path.GetExtension(filename).Replace(".", "");
                    objDevelopmentDocs.UserId = objSession.EmployeeId;
                    objDevelopmentDocs.FileName = filename;
                    objDevelopmentDocs.Title = ddlType.SelectedValue;
                    objDevelopmentDocs.Expiry = Convert.ToDateTime(txtExpires.Text.Trim());
                    //lblUserMessage.Text = fileDirectoryPath + filename;

                    // AsyncFileUpload1.SaveAs(@fileDirectoryPath + filename);
                    DocId = objDevelopmentBL.SaveDocument(objDevelopmentDocs);
                    DocList.Add(DocId);

                    if (objSession.DocIdList == null)
                    {
                        objSession.DocIdList = DocList;
                    }
                    else
                    {
                        objSession.DocIdList.Add(DocId);
                    }
                    populateDocuments(objSession.DocIdList);
                    resetControls();
                    //ViewState.Add(ViewStateConstants.DocIdList, DocList);
                    //DocList = ViewState[ViewStateConstants.DocIdList] as List<int>; 
                }

            }
            catch (SqlTypeException ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = UserMessageConstants.EnterValidTitleandDate;

            }

            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
                updPnlDocuments.Update();
            }
        }
        #endregion

        #region "Btn Cancel Click"
        /// <summary>
        /// Btn Cancel Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("~/Views/Pdr/Development/DevelopmentList.aspx");
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region "Btn Add Development"
        /// <summary>
        /// Add Development Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAddDevelopment_Click(object sender, EventArgs e)
        {
            try
            {
                saveData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessage.isError == true)
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region "btn Add GrantAmount Click"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAddGrantAmount_Click(object sender, EventArgs e)
        {
            float grantAmount;

            if (Convert.ToInt32(ddlFundingSource.SelectedValue) <= 0)
            {
                uiMessage.showErrorMessage(UserMessageConstants.SelectFundingSource);
                return;
            }

            if (txtGrantAmountOpt.Text.Length == 0)
            {
                uiMessage.showErrorMessage(UserMessageConstants.EnterGrantAmount);
                return;
            }

            if (!float.TryParse(txtGrantAmountOpt.Text, out grantAmount))
            {
                uiMessage.showErrorMessage(UserMessageConstants.OnlyDigitsAllowed);
                return;
            }

            DataTable fundingDt = objSession.FundingSourceDt;
            fundingDt.Rows.Add(ApplicationConstants.NoneValue, Convert.ToInt32(ddlFundingSource.SelectedValue), ddlFundingSource.SelectedItem.Text, grantAmount);
            objSession.FundingSourceDt = fundingDt;
            grdGrantAmount.DataSource = fundingDt;
            grdGrantAmount.DataBind();

            txtGrantAmountOpt.Text = string.Empty;
            ddlFundingSource.SelectedIndex = 0;

        }
        #endregion

        #region"btn AddDocument Click"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAddDocument_Click(object sender, EventArgs e)
        {
            List<int> DocList = new List<int>();
            try
            {
                uiMessageDoc.hideMessage();
                if (validateData())
                {
                    string filename = flUploadDoc.FileName;
                    int DocId = 0;
                    string strDestPath = ConfigHelper.GetDevelopmentDocUploadPath();
                    DevelopmentBL objDevelopmentBL = new DevelopmentBL(new DevelopmentRepo());
                    DevelopmentDocumentsBO objDevelopmentDocs = new DevelopmentDocumentsBO();
                    filename = FileHelper.getUniqueFileName(filename);
                    dynamic fileDirectoryPath = Server.MapPath(strDestPath);

                    if ((Directory.Exists(fileDirectoryPath) == false))
                    {
                        Directory.CreateDirectory(fileDirectoryPath);
                    }

                    objDevelopmentDocs.DocPath = fileDirectoryPath;

                    objDevelopmentDocs.DocPath = fileDirectoryPath;
                    objDevelopmentDocs.FileType = System.IO.Path.GetExtension(filename).Replace(".", "");
                    objDevelopmentDocs.UserId = objSession.EmployeeId;
                    objDevelopmentDocs.FileName = filename;
                    objDevelopmentDocs.Category = ddlDocumentCategory.SelectedValue;
                    objDevelopmentDocs.DocumentTypeId = Convert.ToInt32(ddlType.SelectedItem.Value);//ddlType.SelectedItem.Value
                    objDevelopmentDocs.Title = ddlTitle.SelectedItem.Text;
                    objDevelopmentDocs.Expiry = Convert.ToDateTime(txtExpires.Text.Trim());

                    objDevelopmentDocs.DocumentDate = Convert.ToDateTime(txtDocumrntDate.Text);
                    objDevelopmentDocs.UploadedDate = DateTime.Now;
                    objDevelopmentDocs.DevelopmentId = Convert.ToInt32(Request.QueryString["did"]);

                    string filePath = fileDirectoryPath + filename;
                    flUploadDoc.SaveAs(@filePath);
                    DocId = objDevelopmentBL.SaveDocument(objDevelopmentDocs);
                    DocList.Add(DocId);

                    if (objSession.DocIdList == null)
                    {
                        objSession.DocIdList = DocList;
                    }
                    else
                    {
                        objSession.DocIdList.Add(DocId);
                    }
                    populateDocuments(objSession.DocIdList);
                }

            }
            #region old data

            #endregion
            catch (Exception ex)
            {
                uiMessageDoc.isError = true;
                uiMessageDoc.messageText = ex.Message;

                if ((uiMessageDoc.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessageDoc.isError == true))
                {
                    uiMessageDoc.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region "Button View Document Click"

        protected void btnViewDocument_Click(object sender, System.EventArgs e)
        {
            try
            {
                Button btnViewDocument = (Button)sender;
                int DocumentId = Convert.ToInt32(btnViewDocument.CommandArgument);

                if (DocumentId > 0)
                {

                    DevelopmentBL objDevelopmentBL = new DevelopmentBL(new DevelopmentRepo());
                    DataSet resultDs = new DataSet();
                    resultDs = objDevelopmentBL.getDocumentInformationById(DocumentId);


                    if (resultDs.Tables[0].Rows.Count > 0)
                    {
                        dynamic fileDirectoryPath = ResolveClientUrl(ConfigHelper.GetDevelopmentDocUploadPath());
                        string filePath = fileDirectoryPath + resultDs.Tables[0].Rows[0]["DocumentName"];
                        ExportDownloadHelper.exportFileToBrowser("application/pdf", filePath);

                    }
                }

                populateDevelopmentDocuments();
            }
            catch (FileNotFoundException ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = UserMessageConstants.FileNotExist;
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #region "Button Delete Document Click"

        protected void btnDeleteDocument_Click(object sender, System.EventArgs e)
        {
            Button btnDeleteDocument = (Button)sender;
            int DocumentId = Convert.ToInt32(btnDeleteDocument.CommandArgument);
            if (DocumentId > 0)
            {
                DevelopmentBL objDevelopmentBL = new DevelopmentBL(new DevelopmentRepo());
                string[] documentInfo = objDevelopmentBL.deleteDocumentByIDandGetPath(DocumentId);

                string fileName = documentInfo[0];
                //string fileDirectory = documentInfo[1].Replace("\\\\", "/");

                string fileDirectory = ConfigHelper.GetDevelopmentDocUploadPath();
                dynamic fileDirectoryPath = Server.MapPath(fileDirectory);

                if ((Directory.Exists(fileDirectoryPath) == true))
                {
                    if ((File.Exists(fileDirectoryPath + fileName)))
                    {
                        File.Delete(fileDirectoryPath + fileName);
                    }
                }
            }

            populateDevelopmentDocuments();

        }

        #endregion

        #region ddlType Selected Index Changed event
        //protected void ddlType__SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {

        //        populateDropDown(ddlType);//ddlType
        //        ddlType.Enabled = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        uiMessage.isError = true;
        //        uiMessage.messageText = ex.Message;

        //        if ((uiMessage.isExceptionLogged == false))
        //        {
        //            ExceptionPolicy.HandleException(ex, "Exception Policy");
        //        }
        //    }
        //    finally
        //    {
        //        if ((uiMessage.isError == true))
        //        {
        //            uiMessage.showErrorMessage(uiMessage.messageText);
        //        }
        //    }
        //}
        #endregion


        #region PostBack Bind on GridView Data Binding
        protected void PostBackBind_DataBinding(object sender, EventArgs e)
        {

            Button viewDocBtn = (Button)sender;
            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            scriptManager.RegisterPostBackControl(viewDocBtn);
        }
        #endregion

        #endregion


        #region ddlType Event

        protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
        {
            int documentTypeId = Convert.ToInt32(ddlType.SelectedValue);
            if (documentTypeId > -1)
            {
                ddlTitle.Enabled = true;
            }
            BindDocumentSubTypes(documentTypeId,"Development");
        }

        protected void BindDocumentSubTypes(int DocumentTypeId, string reportFor)
        {
            DocumentsBL objDocumentTypeBL = new DocumentsBL(new DocumentsRepo());
            DataSet resultDataSet = new DataSet();
            resultDataSet = objDocumentTypeBL.getDocumentSubtypesList(DocumentTypeId, "Development");
            ddlTitle.DataSource = resultDataSet.Tables[0];
            ddlTitle.DataValueField = "DocumentSubtypeId";
            ddlTitle.DataTextField = "Title";
            ddlTitle.DataBind();
        }

        #endregion



        #region "Functions"

        #region "populate development info"
        /// <summary>
        /// populate development info
        /// </summary>
        /// 
        private void populateDevelopmentInfo()
        {
            try
            {
                int developmentId = ApplicationConstants.NoneValue;
                if (Request.QueryString[PathConstants.DevelopmentId] != null)
                {
                    developmentId = Convert.ToInt32(Request.QueryString[PathConstants.DevelopmentId]);
                    btnAddDevelopment.Text = ApplicationConstants.AmendDevelopment;
                    lblDevelopmentSetUp.Text = "";
                    lblDevelopmentSetUp.Text = ApplicationConstants.AmendDevelopment + ":";
                    ViewState[ViewStateConstants.DevelopmenId] = developmentId;
                }

                DevelopmentBL objDevelopmentBL = new DevelopmentBL(new DevelopmentRepo());
                DataSet resultDataSet = new DataSet();
                objDevelopmentBL.GetDevelopmentDetail(ref resultDataSet, developmentId);

                DataTable developmentDetailDt = resultDataSet.Tables[ApplicationConstants.DevelopmentDetailDt];
                DataTable developmentFundingDt = resultDataSet.Tables[ApplicationConstants.DevelopmentFundingDt];
                objSession.FundingSourceDt = null;

                if (developmentDetailDt.Rows.Count > 0)
                {
                    txtDevelopmentName.Text = developmentDetailDt.Rows[0][ApplicationConstants.DevelopmentNameCol].ToString();
                    ddlPatch.SelectedValue = developmentDetailDt.Rows[0][ApplicationConstants.PatchIdCol].ToString();
                    txtAddress1.Text = developmentDetailDt.Rows[0][ApplicationConstants.Address1Col].ToString();
                    txtAddress2.Text = developmentDetailDt.Rows[0][ApplicationConstants.Address2Col].ToString();
                    txtTownCity.Text = developmentDetailDt.Rows[0][ApplicationConstants.TownCol].ToString();
                    txtCounty.Text = developmentDetailDt.Rows[0][ApplicationConstants.CountyCol].ToString();
                    txtPostCode.Text = developmentDetailDt.Rows[0][ApplicationConstants.PostcodeCol].ToString();
                    txtArchitect.Text = developmentDetailDt.Rows[0][ApplicationConstants.ArchitectCol].ToString();
                    txtProjectManager.Text = developmentDetailDt.Rows[0][ApplicationConstants.ProjectManagerCol].ToString();
                    ddlDevelopmentType.SelectedValue = developmentDetailDt.Rows[0][ApplicationConstants.DevelopmentTypeCol].ToString();
                    ddlCompany.SelectedValue = developmentDetailDt.Rows[0][ApplicationConstants.CompanyIdCol].ToString();
                    ddlDevelopmentStatus.SelectedValue = developmentDetailDt.Rows[0][ApplicationConstants.DevelopmentStatusCol].ToString();
                    txtLandValue.Text = developmentDetailDt.Rows[0][ApplicationConstants.LandValueCol].ToString();

                    if (developmentDetailDt.Rows[0][ApplicationConstants.LandValueDateCol] != DBNull.Value)//new code
                    {
                        txtLandValueDate.Text = developmentDetailDt.Rows[0][ApplicationConstants.LandValueDateCol].ToString();
                    }

                    txtLandPrice.Text = developmentDetailDt.Rows[0][ApplicationConstants.LandPurchasePriceCol].ToString();

                    if (developmentDetailDt.Rows[0][ApplicationConstants.PurchaseDateCol] != DBNull.Value)
                    {
                        txtPurchaseDate.Text = developmentDetailDt.Rows[0][ApplicationConstants.PurchaseDateCol].ToString();
                    }

                    txtGrantAmount.Text = developmentDetailDt.Rows[0][ApplicationConstants.GrantAmountCol].ToString();
                    txtBorrowedAmount.Text = developmentDetailDt.Rows[0][ApplicationConstants.BorrowedAmountCol].ToString();
                    if (developmentDetailDt.Rows[0][ApplicationConstants.OutlinePlanningApplicationCol] != DBNull.Value)
                    {
                        txtOutlinePlanningApplication.Text = developmentDetailDt.Rows[0][ApplicationConstants.OutlinePlanningApplicationCol].ToString();
                    }

                    if (developmentDetailDt.Rows[0][ApplicationConstants.DetailedPlanningApplicationCol] != DBNull.Value)
                    {
                        txtDetailedPlanningApplication.Text = developmentDetailDt.Rows[0][ApplicationConstants.DetailedPlanningApplicationCol].ToString();
                    }

                    if (developmentDetailDt.Rows[0][ApplicationConstants.OutlinePlanningApprovalCol] != DBNull.Value)
                    {
                        txtOutlinePlanningApproval.Text = developmentDetailDt.Rows[0][ApplicationConstants.OutlinePlanningApprovalCol].ToString();
                    }

                    if (developmentDetailDt.Rows[0][ApplicationConstants.DetailedPlanningApprovalCol] != DBNull.Value)
                    {
                        txtDetailedPlanningApproval.Text = developmentDetailDt.Rows[0][ApplicationConstants.DetailedPlanningApprovalCol].ToString();
                    }

                    if (developmentDetailDt.Rows[0][ApplicationConstants.LocalAuthorityCol] != DBNull.Value && (int)developmentDetailDt.Rows[0][ApplicationConstants.LocalAuthorityCol] != -1)
                    {
                        ddlLocalAuthority.SelectedValue = developmentDetailDt.Rows[0][ApplicationConstants.LocalAuthorityCol].ToString();
                    }

                    txtNoofUnits.Text = developmentDetailDt.Rows[0][ApplicationConstants.numberOfUnitCol].ToString();

                    objSession.FundingSourceDt = developmentFundingDt;
                    grdGrantAmount.DataSource = developmentFundingDt;
                    grdGrantAmount.DataBind();

                }

                if (Request.QueryString[PathConstants.DevelopmentId] != null && developmentDetailDt.Rows.Count == 0)
                {
                    uiMessage.showErrorMessage(UserMessageConstants.InvalidDevelopmentId);
                }

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #region "populate phase dropdown"
        /// <summary>
        /// populate phase dropdown
        /// </summary>
        /// 
        private void populatePhaseDropdown()
        {
            try
            {
                int developmentId = ApplicationConstants.NoneValue;
                if (Request.QueryString[PathConstants.DevelopmentId] != null)
                {
                    developmentId = Convert.ToInt32(Request.QueryString[PathConstants.DevelopmentId]);
                }

                DevelopmentBL objDevelopmentBL = new DevelopmentBL(new DevelopmentRepo());
                DataSet phaseDataSet = new DataSet();
                objDevelopmentBL.getDevelopmentPhase(ref phaseDataSet, developmentId);

                foreach (DataRow row in phaseDataSet.Tables[0].Rows)
                {
                    PhaseList.Add(Convert.ToInt32(row[ApplicationConstants.PhaseIdCol]));
                }

                if (PhaseList.Count > 0)
                {
                    PopulatePhaseDDL(PhaseList);
                }


            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #region "populate development documents"
        /// <summary>
        /// populate phase dropdown
        /// </summary>
        /// 
        private void populateDevelopmentDocuments()
        {
            try
            {
                int developmentId = ApplicationConstants.NoneValue;
                if (Request.QueryString[PathConstants.DevelopmentId] != null)
                {
                    developmentId = Convert.ToInt32(Request.QueryString[PathConstants.DevelopmentId]);
                }

                DevelopmentBL objDevelopmentBL = new DevelopmentBL(new DevelopmentRepo());
                DataSet documentsDataSet = new DataSet();
                objDevelopmentBL.getDevelopmentsDocumentsData(ref documentsDataSet, developmentId);
                List<int> DocIdList = new List<int>();
                foreach (DataRow row in documentsDataSet.Tables[0].Rows)
                {
                    DocIdList.Add(Convert.ToInt32(row[ApplicationConstants.DevelopmentDocumentIdCol]));
                }
                objSession.DocIdList = DocIdList;
                populateDocuments(DocIdList);

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #region "populate Block Data"
        /// <summary>
        /// populate Development Block Data
        /// </summary>
        /// 
        private void populateBlockData()
        {
            try
            {

                int developmentId = ApplicationConstants.NoneValue;
                if (Request.QueryString[PathConstants.DevelopmentId] != null)
                {
                    developmentId = Convert.ToInt32(Request.QueryString[PathConstants.DevelopmentId]);
                }

                DevelopmentBL objDevelopmentBL = new DevelopmentBL(new DevelopmentRepo());
                DataSet blockDataSet = new DataSet();
                objDevelopmentBL.getBlocksData(ref blockDataSet, developmentId);
                grdBlocks.DataSource = blockDataSet;
                grdBlocks.DataBind();

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #region"populateDocuments"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="DocList"></param>
        /// 

        private void populateDocuments(List<int> DocList)
        {
            try
            {
                DevelopmentBL objDevelopmentBL = new DevelopmentBL(new DevelopmentRepo());
                DataSet documentDataSet = new DataSet();
                DataRow[] dr;
                DataTable result = new DataTable();
                string csvString = string.Empty;
                string expression = String.Empty;
                if (DocList.Count != 0)
                {
                    if (DocList.Count > 1)
                    {
                        csvString = Convert.ToString(0);
                        foreach (int r in DocList)
                        {
                            csvString = csvString + "," + r.ToString();
                        }
                        expression = "DocumentID in (" + csvString.ToString() + ") ";
                    }
                    else
                    {
                        csvString = DocList[0].ToString();
                        expression = "DocumentID = " + csvString.ToString();
                    }

                }
                else
                {
                    expression = "DocumentID = 0 ";
                }

                objDevelopmentBL.getDocumentsData(ref documentDataSet);
                dr = documentDataSet.Tables[0].Select(expression);
                if (dr.Length > 0)
                {
                    result = dr.CopyToDataTable();
                }
                grdDocuments.DataSource = result;
                grdDocuments.DataBind();
                updPnlDocuments.Update();

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region"Populate PhaseDDL "
        /// <summary>
        /// 
        /// </summary>
        /// <param name="PhaseList"></param>
        void PopulatePhaseDDL(List<int> phaseList)
        {
            string csvString = string.Empty;
            string expression = String.Empty;
            if (phaseList.Count > 1)
            {
                csvString = Convert.ToString(0);
                foreach (int r in phaseList)
                {
                    csvString = csvString + "," + r.ToString();
                }
                expression = "PhaseID in (" + csvString.ToString() + ") ";
            }
            else
            {
                csvString = phaseList[0].ToString();
                expression = "PhaseID = " + csvString.ToString();
            }

            DevelopmentBL objDevelopmentBL = new DevelopmentBL(new DevelopmentRepo());
            DataSet phaseDataSet = new DataSet();
            objDevelopmentBL.getDevelopmentPhase(ref phaseDataSet, -1);
            DataRow[] dr = phaseDataSet.Tables[0].Select(expression);
            DataTable result = dr.CopyToDataTable();
            ddlDevelopmentPhase.DataSource = result;
            ddlDevelopmentPhase.DataTextField = "PhaseName";
            ddlDevelopmentPhase.DataValueField = "PHASEID";
            ddlDevelopmentPhase.DataBind();
            ListItem item = new ListItem("Select Phase", "0");
            ddlDevelopmentPhase.Items.Insert(0, item);
            ddlDevelopmentPhase.Enabled = true;

        }
        #endregion



        #endregion

        #region "IAddEditPage Functions"

        #region "save Data "
        /// <summary>
        /// Save Development data
        /// </summary>
        public void saveData()
        {
            try
            {
                double LandValue, GrantAmount, BorrowedAmount, LandPurchasePrice;
                Nullable<DateTime> LandValueDate;
                int developmentId;
                DevelopmentBL objDevelopmentBL = new DevelopmentBL(new DevelopmentRepo());
                DevelopmentBO objSaveDevelopmentBO = new DevelopmentBO();
                DataTable fundingDt = objSession.FundingSourceDt;

                developmentId = ApplicationConstants.NoneValue;
                if (Request.QueryString[PathConstants.DevelopmentId] != null)
                {
                    developmentId = Convert.ToInt32(Request.QueryString[PathConstants.DevelopmentId]);
                }
                objSaveDevelopmentBO.DevelopmentId = developmentId;

                if (!string.IsNullOrEmpty(txtDevelopmentName.Text) && !string.IsNullOrWhiteSpace(txtDevelopmentName.Text))
                {
                    objSaveDevelopmentBO.DevelopmentName = txtDevelopmentName.Text;
                }
                else
                {
                    uiMessage.messageText = UserMessageConstants.DevelopmentNameNotFound;
                    uiMessage.showErrorMessage(uiMessage.messageText);
                    return;
                }
                objSaveDevelopmentBO.Patch = Convert.ToInt32(ddlPatch.SelectedValue);
                if (objSaveDevelopmentBO.Patch <= 0)
                {
                    uiMessage.messageText = UserMessageConstants.PatchNotFound;
                    uiMessage.showErrorMessage(uiMessage.messageText);
                    return;
                }
                objSaveDevelopmentBO.LocalAuthorityId = ddlLocalAuthority.SelectedIndex;
                if (objSaveDevelopmentBO.LocalAuthorityId <= 0)
                {
                    uiMessage.messageText = UserMessageConstants.LocalAuthority;
                    uiMessage.showErrorMessage(uiMessage.messageText);
                    return;
                }
                if (!string.IsNullOrEmpty(txtAddress1.Text) && !string.IsNullOrWhiteSpace(txtAddress1.Text))
                {
                    objSaveDevelopmentBO.Address1 = txtAddress1.Text;
                }
                else
                {
                    uiMessage.messageText = UserMessageConstants.Address1NotFound;
                    uiMessage.showErrorMessage(uiMessage.messageText);
                    return;
                }
                objSaveDevelopmentBO.Address2 = txtAddress2.Text;
                objSaveDevelopmentBO.County = txtCounty.Text;
                if (!string.IsNullOrEmpty(txtTownCity.Text) && !string.IsNullOrWhiteSpace(txtTownCity.Text))
                {
                    objSaveDevelopmentBO.TownCity = txtTownCity.Text;
                }
                else
                {
                    uiMessage.messageText = UserMessageConstants.TownCityNotFound;
                    uiMessage.showErrorMessage(uiMessage.messageText);
                    return;
                }
                objSaveDevelopmentBO.PostCode = txtPostCode.Text;
                objSaveDevelopmentBO.Architect = txtArchitect.Text;
                objSaveDevelopmentBO.ProjectManager = txtProjectManager.Text;
                objSaveDevelopmentBO.DevelopmentType = Convert.ToInt32(ddlDevelopmentType.SelectedValue);
                objSaveDevelopmentBO.CompanyId = Convert.ToInt32(ddlCompany.SelectedValue);
                objSaveDevelopmentBO.DevelopmentStatus = Convert.ToInt32(ddlDevelopmentStatus.SelectedValue);
                objSaveDevelopmentBO.UserId = objSession.EmployeeId;

                if (string.IsNullOrEmpty(txtLandValue.Text))
                {
                    LandValue = ApplicationConstants.NoneValue;
                }
                else
                {
                    double.TryParse(txtLandValue.Text, out LandValue);
                }

                objSaveDevelopmentBO.LandValue = LandValue;

                if (string.IsNullOrEmpty(txtLandValueDate.Text))//my work
                {
                    LandValueDate = null;
                }
                else
                {
                    LandValueDate = DateTime.Parse(txtLandValueDate.Text);
                }

                objSaveDevelopmentBO.ValueDate = LandValueDate;
                // objSaveDevelopmentBO.LandValue = LandValue;//end my work






                if (string.IsNullOrEmpty(txtLandPrice.Text))
                {
                    LandPurchasePrice = ApplicationConstants.NoneValue;
                }
                else
                {
                    double.TryParse(txtLandPrice.Text, out LandPurchasePrice);
                }


                objSaveDevelopmentBO.LandPurchasePrice = LandPurchasePrice;

                if (!string.IsNullOrEmpty(txtPurchaseDate.Text) && !string.IsNullOrWhiteSpace(txtPurchaseDate.Text))
                {
                    objSaveDevelopmentBO.PurchaseDate = Convert.ToDateTime(txtPurchaseDate.Text);
                }

                if (string.IsNullOrEmpty(txtGrantAmount.Text))
                {
                    GrantAmount = ApplicationConstants.NoneValue;
                }
                else
                {
                    double.TryParse(txtGrantAmount.Text, out GrantAmount);
                }

                if (string.IsNullOrEmpty(txtBorrowedAmount.Text))
                {
                    BorrowedAmount = ApplicationConstants.NoneValue;
                }
                else
                {
                    double.TryParse(txtBorrowedAmount.Text, out BorrowedAmount);
                }

                objSaveDevelopmentBO.GrantAmount = GrantAmount;
                objSaveDevelopmentBO.BorrowedAmount = BorrowedAmount;
                if (!string.IsNullOrEmpty(txtOutlinePlanningApplication.Text) && !string.IsNullOrWhiteSpace(txtOutlinePlanningApplication.Text))
                {
                    objSaveDevelopmentBO.OutlinePlanningApplication = Convert.ToDateTime(txtOutlinePlanningApplication.Text);
                }
                if (!string.IsNullOrEmpty(txtOutlinePlanningApproval.Text) && !string.IsNullOrWhiteSpace(txtOutlinePlanningApproval.Text))
                {
                    objSaveDevelopmentBO.OutlinePlanningApproval = Convert.ToDateTime(txtOutlinePlanningApproval.Text);
                }
                if (!string.IsNullOrEmpty(txtDetailedPlanningApplication.Text) && !string.IsNullOrWhiteSpace(txtDetailedPlanningApplication.Text))
                {
                    objSaveDevelopmentBO.DetailedPlanningApplication = Convert.ToDateTime(txtDetailedPlanningApplication.Text);
                }
                if (!string.IsNullOrEmpty(txtDetailedPlanningApproval.Text) && !string.IsNullOrWhiteSpace(txtDetailedPlanningApproval.Text))
                {
                    objSaveDevelopmentBO.DetailedPlanningApproval = Convert.ToDateTime(txtDetailedPlanningApproval.Text);
                }

                if (string.IsNullOrEmpty(txtNoofUnits.Text))
                {
                    objSaveDevelopmentBO.NoofUnits = ApplicationConstants.NoneValue;
                }
                else
                {
                    objSaveDevelopmentBO.NoofUnits = Convert.ToInt32(txtNoofUnits.Text);
                }

                if (fundingDt.Rows.Count > 0)
                {
                    DataRow[] rows = fundingDt.Select(ApplicationConstants.DevelopmentFundingIdCol + " = -1");
                    if (rows.Count() > 0)
                    {
                        objSaveDevelopmentBO.FundingSourceDt = rows.CopyToDataTable();
                    }
                }
                else
                {
                    objSaveDevelopmentBO.FundingSourceDt = fundingDt;
                }

                objSaveDevelopmentBO.LocalAuthorityId = int.Parse(ddlLocalAuthority.SelectedValue);


                PhaseList = (List<int>)ViewState[ViewStateConstants.PhaseIdList];
                if (PhaseList == null)
                {
                    PhaseList = new List<int>();
                }
                string result = objDevelopmentBL.SaveDevelpoment(ref objSaveDevelopmentBO, objSession.DocIdList, PhaseList);
                if (result == "Exist")
                {
                    uiMessage.messageText = UserMessageConstants.DuplicateDevelopmentError;
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
                else if (result == "Failed")
                {
                    uiMessage.messageText = "failed";
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
                else
                {
                    ViewState[ViewStateConstants.DevelopmenId] = objSaveDevelopmentBO.DevelopmentId;
                    uiMessage.messageText = UserMessageConstants.DevelopmentSaveSuccessfuly;
                    uiMessage.showInformationMessage(uiMessage.messageText);
                    //Response.Redirect("~/Views/Pdr/Development/DevelopmentList.aspx");
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessage.isError == true)
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region "validate Data"
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public bool validateData()
        {
            // ValidationHelper validHelper = new ValidationHelper();
            if (Convert.ToInt32(ddlType.SelectedItem.Value) == -1)   //ddlType.SelectedItem.Value
            {
                uiMessageDoc.showErrorMessage(UserMessageConstants.SelectValidDocuentType);

                return false;
            }
            if (string.IsNullOrEmpty(txtDocumrntDate.Text.Trim()))
            {
                uiMessageDoc.showErrorMessage(UserMessageConstants.EnterValidDocumentDate);
                return false;
            }
            if (!(ValidationHelper.validateDateFormate(txtDocumrntDate.Text.Trim())))
            {
                uiMessageDoc.showErrorMessage(UserMessageConstants.DocumentDateFormateError);
                return false;
            }

            if (string.IsNullOrEmpty(txtExpires.Text.Trim()))
            {
                uiMessageDoc.showErrorMessage(UserMessageConstants.EnterValidTitleandDate);
                return false;
            }




            if (DateTime.Parse(txtDocumrntDate.Text) >= DateTime.Parse(txtExpires.Text))
            {
                uiMessageDoc.showErrorMessage(UserMessageConstants.DateValidationError);
                return false;
            }

            if (!flUploadDoc.HasFile)
            {
                uiMessageDoc.showErrorMessage(UserMessageConstants.SelectAFile);
                return false;
            }
            if (flUploadDoc.PostedFile.ContentLength > 20728650)
            {
                uiMessageDoc.showErrorMessage(UserMessageConstants.FileSizeError);
                return false;
            }




            return true;

        }
        #endregion

        #region "reset Controls"
        /// <summary>
        /// Reset Documents Controls
        /// </summary>
        public void resetControls()
        {
            //txtTitle.Text = string.Empty;
            txtExpires.Text = string.Empty;
        }
        #endregion

        #region "load Data "
        /// <summary>
        /// 
        /// </summary>
        public void loadData()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region "populate DropDown "
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ddl"></param>
        public void populateDropDown(DropDownList ddl)
        {
            DocumentsBL objDocumentTypeBL = new DocumentsBL(new DocumentsRepo());
            DataSet resultDataSet = new DataSet();
            resultDataSet = objDocumentTypeBL.getDocumentSubtypesList(Convert.ToInt32(ddl.SelectedValue),"Development");
            ddlType.DataSource = resultDataSet.Tables[0];
            ddlType.DataValueField = "DocumentSubtypeId";
            ddlType.DataTextField = "Title";
            ddlType.DataBind();


        }
        #endregion

        #region "populate DropDowns "
        /// <summary>
        /// Popluate all Dropdown on this page
        /// </summary>
        public void populateDropDowns()
        {
            try
            {
                

                DevelopmentBL objDevelopmentBL = new DevelopmentBL(new DevelopmentRepo());

                DataSet resultset = new DataSet();
                ReportsBL objReportsBl = new ReportsBL(new ReportsRepo());
                objReportsBl.getCategories(ref resultset, "Development");
                ddlDocumentCategory.DataSource = resultset;
                ddlDocumentCategory.DataValueField = "categorytypeid";
                ddlDocumentCategory.DataTextField = "title";
                ddlDocumentCategory.DataBind();

               // BindDocumentSubTypes(documentTypeId, "Development");

                DataSet SchemeDataSet = new DataSet();
                objDevelopmentBL.getScheme(ref SchemeDataSet, -1);
                if (SchemeDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlScheme.DataSource = SchemeDataSet;
                    ddlScheme.DataValueField = "SCHEMEID";
                    ddlScheme.DataTextField = "SCHEMENAME";
                    ddlScheme.DataBind();
                    ListItem item = new ListItem("Select Scheme", "0");
                    ddlScheme.Items.Insert(0, item);
                }

                DataSet PatchDataSet = new DataSet();
                objDevelopmentBL.getPatch(ref PatchDataSet);
                if (PatchDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlPatch.DataSource = PatchDataSet;
                    ddlPatch.DataValueField = "PATCHID";
                    ddlPatch.DataTextField = "LOCATION";
                    ddlPatch.DataBind();
                    ListItem item = new ListItem("Select Patch", "0");
                    ddlPatch.Items.Insert(0, item);
                }
                else
                {
                    ListItem item = new ListItem("Select Patch", "0");
                    ddlPatch.Items.Insert(0, item);
                }

                DataSet CompanyDataSet = new DataSet();
                objDevelopmentBL.getCompany(ref CompanyDataSet);
                if (CompanyDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlCompany.DataSource = CompanyDataSet;
                    ddlCompany.DataValueField = "COMPANYID";
                    ddlCompany.DataTextField = "DESCRIPTION";
                    ddlCompany.DataBind();
                    ListItem item = new ListItem("Select Company", "0");
                    ddlCompany.Items.Insert(0, item);
                }
                else
                {
                    ListItem item = new ListItem("Select Company", "0");
                    ddlCompany.Items.Insert(0, item);
                }

                DataSet DevelopmentTypeDataSet = new DataSet();
                objDevelopmentBL.getDevelopmentType(ref DevelopmentTypeDataSet);
                if (DevelopmentTypeDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlDevelopmentType.DataSource = DevelopmentTypeDataSet;
                    ddlDevelopmentType.DataValueField = "DEVELOPMENTTYPEID";
                    ddlDevelopmentType.DataTextField = "DESCRIPTION";
                    ddlDevelopmentType.DataBind();
                    ListItem item = new ListItem("Select Type", "0");
                    ddlDevelopmentType.Items.Insert(0, item);
                }
                else
                {
                    ListItem item = new ListItem("Select Type", "0");
                    ddlDevelopmentType.Items.Insert(0, item);
                }
                DataSet DevelopmentStatusDataSet = new DataSet();
                objDevelopmentBL.getDevelopmentStatus(ref DevelopmentStatusDataSet);
                if (DevelopmentStatusDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlDevelopmentStatus.DataSource = DevelopmentStatusDataSet;
                    ddlDevelopmentStatus.DataValueField = "DEVELOPMENTSTATUSID";
                    ddlDevelopmentStatus.DataTextField = "STATUS";
                    ddlDevelopmentStatus.DataBind();
                    ListItem item = new ListItem("Select Status", "0");
                    ddlDevelopmentStatus.Items.Insert(0, item);
                }
                else
                {
                    ListItem item = new ListItem("Select Status", "0");
                    ddlDevelopmentStatus.Items.Insert(0, item);
                }
                DataSet FuncdingSourceDataSet = new DataSet();
                objDevelopmentBL.getFundingSource(ref FuncdingSourceDataSet);
                if (FuncdingSourceDataSet.Tables[0].Rows.Count > 0)
                {
                    ddlFundingSource.DataSource = FuncdingSourceDataSet;
                    ddlFundingSource.DataValueField = "FUNDINGAUTHORITYID";
                    ddlFundingSource.DataTextField = "DESCRIPTION";
                    ddlFundingSource.DataBind();
                    ListItem item = new ListItem("Select Source", "0");
                    ddlFundingSource.Items.Insert(0, item);
                }
                else
                {
                    ListItem item = new ListItem("Select Source", "0");
                    ddlFundingSource.Items.Insert(0, item);
                }

                if (ddlDevelopmentPhase.SelectedItem == null)
                {
                    ListItem item = new ListItem("Select Phase", "0");
                    ddlDevelopmentPhase.Items.Insert(0, item);
                }

                var objReportsBL = new ReportsBL(new ReportsRepo());
                DataSet localAuthorityDataSet = objReportsBL.getLocalAuthority();
                ddlLocalAuthority.DataSource = localAuthorityDataSet;
                ddlLocalAuthority.DataValueField = "LOCALAUTHORITYID";
                ddlLocalAuthority.DataTextField = "DESCRIPTION";
                ddlLocalAuthority.DataBind();
                ddlLocalAuthority.Items.Insert(0, new ListItem("Select Local Authority", "-1"));

                DocumentsBL objDocumentTypeBL = new DocumentsBL(new DocumentsRepo());
                DataSet resultDataSet = new DataSet();

                resultDataSet = objDocumentTypeBL.getDocumentTypesList("Development", Convert.ToInt32(ddlDocumentCategory.SelectedItem.Value));
                ddlType.DataSource = resultDataSet.Tables[0];
                ddlType.DataValueField = "DocumentTypeId";
                ddlType.DataTextField = "Title";
                ddlType.DataBind();
                //ddlType.DataSource = resultDataSet.Tables[0];
                //ddlType.DataValueField = "DocumentTypeId";
                //ddlType.DataTextField = "Title";
                //ddlType.DataBind();


            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        public List<int> DocIdList
        {
            get
            {
                if (ViewState[ViewStateConstants.DocIdList] == null)
                {
                    return new List<int>();
                }
                return (List<int>)ViewState[ViewStateConstants.DocIdList];
            }
            set
            {
                ViewState[ViewStateConstants.DocIdList] = value;
            }
        }

        #endregion
    }
}