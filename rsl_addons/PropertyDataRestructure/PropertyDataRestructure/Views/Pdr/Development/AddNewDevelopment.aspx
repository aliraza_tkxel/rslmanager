﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Pdr.Master" AutoEventWireup="true"
    CodeBehind="AddNewDevelopment.aspx.cs" Culture="en-GB" Inherits="PropertyDataRestructure.Views.Pdr.Development.AddNewDevelopment" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<%@ Import Namespace="PDR_Utilities.Constants" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" language="javascript">
        function uploadComplete(sender) {

            //$get("<%=lblStatus.ClientID%>").innerHTML = "File Uploaded Successfully";
            $get("<%=btnUpdate.ClientID%>").click(); // new file
        }
        
    </script>
  
    <style type="text/css">
        .ajax__calendar_body
            {
             width: 190px !important;
            }    
        .ajax__calendar_container
            {       
              width: 205px !important;
            } 

    </style>        

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <p style="background-color: Black; height: 22px; text-align: justify; font-family: Tahoma;
        font-weight: bold; margin: 0 0 6px; font-size: 15px; padding: 8px;">
        <asp:Label Text="Development Set Up:" ID="lblDevelopmentSetUp" ForeColor="White"
            runat="server" />
    </p>
    <asp:UpdateProgress ID="updateProgressReportsArea" runat="server" DisplayAfter="10">
        <ProgressTemplate>
            <div class="loading-image">
                <img alt="Please Wait" src="../../../Images/menu/progress.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <div class="devLeftSection-divider resltables-left">
        <asp:UpdatePanel runat="server" ID="updPnlDevelopment" UpdateMode="Always">
            <ContentTemplate>
                <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="500px" />
                <table style="float: left;">
                    <tr>
                        <td>
                            Development Name:<span class="Required">*</span>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtDevelopmentName" Style="width: 160px;"></asp:TextBox>
                            <%--<asp:RegularExpressionValidator ID="revDevelopmentName" runat="server" ControlToValidate="txtDevelopmentName"
                                EnableClientScript="True" ErrorMessage="Special characters are not allowed" CssClass="Required"
                                Display="Dynamic" ValidationExpression="^[A-Za-z0-9- ]+$" />--%>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDevelopmentName"
                                ErrorMessage="Required" CssClass="Required" ValidationGroup="save" Display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Patch Name:<span class="Required">*</span>
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlPatch" Style="width: 160px;">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Address1:<span class="Required">*</span>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtAddress1" Style="width: 160px;"></asp:TextBox>
                            <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtAddress1"
                                EnableClientScript="True" ErrorMessage="Special characters are not allowed" CssClass="Required"
                                Display="Dynamic" ValidationExpression="^[A-Za-z0-9- ]+$" />--%>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtAddress1"
                                ErrorMessage="Required" CssClass="Required" ValidationGroup="save" Display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Address2:
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtAddress2" Style="width: 160px;"></asp:TextBox>
                            <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtAddress2"
                                EnableClientScript="True" ErrorMessage="Special characters are not allowed" CssClass="Required"
                                Display="Dynamic" ValidationExpression="^[A-Za-z0-9- ]+$" />--%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Town/City:<span class="Required">*</span>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtTownCity" Style="width: 160px;"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtTownCity"
                                ErrorMessage="Required" CssClass="Required" ValidationGroup="add" Display="Dynamic"></asp:RequiredFieldValidator>
                            <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtTownCity"
                                EnableClientScript="True" ErrorMessage="Special characters are not allowed" CssClass="Required"
                                Display="Dynamic" ValidationExpression="^[A-Za-z0-9- ]+$" />--%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            County:
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtCounty" Style="width: 160px;"></asp:TextBox>
                            <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtCounty"
                                EnableClientScript="True" ErrorMessage="Special characters are not allowed" CssClass="Required"
                                Display="Dynamic" ValidationExpression="^[A-Za-z0-9- ]+$" />--%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            PostCode:
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtPostCode" Style="width: 160px;"></asp:TextBox>
                            <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtPostCode"
                                EnableClientScript="True" ErrorMessage="Special characters are not allowed" CssClass="Required"
                                Display="Dynamic" ValidationExpression="^[A-Za-z0-9- ]+$" />--%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Architect:
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtArchitect" Style="width: 160px;"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Project Manager:
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtProjectManager" Style="width: 160px;"></asp:TextBox>
                        </td>
                    </tr>
                <tr>
                    <td>
                        Company:
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="ddlCompany" Style="width: 160px;">
                        </asp:DropDownList>
                    </td>
                </tr>
                    <tr>
                        <td>
                            Development Type:
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlDevelopmentType" Style="width: 160px;">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Development Status:
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlDevelopmentStatus" Style="width: 160px;">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Local Authority: <span class="Required">*</span>
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlLocalAuthority" Style="width: 160px;">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="AuthorityValidator" InitialValue="-1" runat="server" ControlToValidate="ddlLocalAuthority"
                                ErrorMessage="Required" CssClass="Required" ValidationGroup="save" Display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <caption>
                        <tr>
                            <td>
                                Land Value(£):
                            </td>
                            <td>
                                <asp:TextBox ID="txtLandValue" runat="server" Style="width: 70px;"></asp:TextBox>
                                &nbsp;
                                <asp:TextBox ID="txtLandValueDate" runat="server" Style="width: 80px;"></asp:TextBox>
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MM/yyyy"
                                    PopupButtonID="ImageButton2" PopupPosition="Right" TargetControlID="txtLandValueDate"
                                    TodaysDateFormat="dd/MM/yyyy">
                                </ajaxToolkit:CalendarExtender>
                                <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/Images/calendar.png"
                                    Style="vertical-align: bottom; border: none;" />
                                <br />
                                <asp:CompareValidator ID="CompareValidator16" runat="server" ControlToValidate="txtLandValueDate"
                                    CssClass="Required" Display="Dynamic" ErrorMessage="Enter valid date" Operator="DataTypeCheck"
                                    Style="float: left;" Type="Date" ValidationGroup="save" />
                                <br />
                                <asp:CompareValidator ID="CompareValidator12" runat="server" ControlToValidate="txtLandValue"
                                    CssClass="Required" Display="Dynamic" ErrorMessage="Only digits allowed" Type="Double"
                                    ValidationGroup="save">
                                </asp:CompareValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtLandValue"
                                    Display="Dynamic" ErrorMessage="&lt;br/&gt;Please enter valid  non-negative number."
                                    ForeColor="Red" ValidationExpression="^[0-9]*$" ValidationGroup="save" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Purchase Date:
                            </td>
                            <td>
                                <asp:TextBox ID="txtPurchaseDate" runat="server" Style="width: 160px;"></asp:TextBox>
                                <ajaxToolkit:CalendarExtender Animated="False"  ID="calExtPurchaseDate" runat="server" Format="dd/MM/yyyy"
                                    PopupButtonID="ImageButton1" PopupPosition="Right" TargetControlID="txtPurchaseDate"
                                    TodaysDateFormat="dd/MM/yyyy">
                                </ajaxToolkit:CalendarExtender>
                                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/calendar.png"
                                    Style="vertical-align: bottom; border: none;" />
                                <br />
                                <asp:CompareValidator ID="CompareValidator13" runat="server" ControlToValidate="txtPurchaseDate"
                                    CssClass="Required" Display="Dynamic" ErrorMessage="Enter valid date" Operator="DataTypeCheck"
                                    Style="float: left;" Type="Date" ValidationGroup="save" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Land Purchase Price:
                            </td>
                            <td>
                                <asp:TextBox ID="txtLandPrice" runat="server" Style="width: 160px;"></asp:TextBox>
                                <br />
                                <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="txtLandPrice"
                                    CssClass="Required" Display="Dynamic" ErrorMessage="Only digits allowed" Type="Double"
                                    ValidationGroup="save">
                                </asp:CompareValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtLandPrice"
                                    Display="Dynamic" ErrorMessage="&lt;br/&gt;Please enter valid non-negative number ."
                                    ForeColor="Red" ValidationExpression="^[0-9]*$" ValidationGroup="save" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Grant Amount(£):
                            </td>
                            <td>
                                <asp:TextBox ID="txtGrantAmount" runat="server" Style="width: 160px;"></asp:TextBox>
                                <br />
                                <asp:CompareValidator ID="CompareValidator11" runat="server" ControlToValidate="txtGrantAmount"
                                    CssClass="Required" Display="Dynamic" ErrorMessage="Only digits allowed" Type="Double"
                                    ValidationGroup="save">
                                </asp:CompareValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtGrantAmount"
                                    Display="Dynamic" ErrorMessage="&lt;br/&gt;Please enter valid non-negative number."
                                    ForeColor="Red" ValidationExpression="^[0-9]*$" ValidationGroup="save" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Borrowed Amount(£):
                            </td>
                            <td>
                                <asp:TextBox ID="txtBorrowedAmount" runat="server" Style="width: 160px;"></asp:TextBox>
                                <br />
                                <asp:CompareValidator ID="CompareValidator10" runat="server" ControlToValidate="txtBorrowedAmount"
                                    CssClass="Required" Display="Dynamic" ErrorMessage="Only digits allowed" Type="Double"
                                    ValidationGroup="save">
                                </asp:CompareValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtBorrowedAmount"
                                    Display="Dynamic" ErrorMessage="&lt;br/&gt;Please enter valid non-negative number."
                                    ForeColor="Red" ValidationExpression="^[0-9]*$" ValidationGroup="save" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Outline Planning Application:
                            </td>
                            <td>
                                <asp:TextBox ID="txtOutlinePlanningApplication" runat="server" Style="width: 160px !important;"></asp:TextBox>
                                <ajaxToolkit:CalendarExtender ID="clndrOutlinePlanningApplication" runat="server"
                                    Format="dd/MM/yyyy" PopupButtonID="imgCalDate" PopupPosition="Right" TargetControlID="txtOutlinePlanningApplication"
                                    TodaysDateFormat="dd/MM/yyyy">
                                </ajaxToolkit:CalendarExtender>
                                <asp:ImageButton ID="imgCalDate" runat="server" ImageUrl="~/Images/calendar.png"
                                    Style="vertical-align: bottom; border: none;" />
                                <br />
                                <asp:CompareValidator ID="CompareValidator5" runat="server" ControlToValidate="txtOutlinePlanningApplication"
                                    CssClass="Required" Display="Dynamic" ErrorMessage="Enter valid date" Operator="DataTypeCheck"
                                    Style="float: left;" Type="Date" ValidationGroup="save" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Detailed Planning Application:
                            </td>
                            <td>
                                <asp:TextBox ID="txtDetailedPlanningApplication" runat="server" Style="width: 160px !important;"></asp:TextBox>
                                <ajaxToolkit:CalendarExtender ID="clndrDetailedPlanningApplication" runat="server"
                                    Format="dd/MM/yyyy" PopupButtonID="imgCalDate2" PopupPosition="Right" TargetControlID="txtDetailedPlanningApplication"
                                    TodaysDateFormat="dd/MM/yyyy">
                                </ajaxToolkit:CalendarExtender>
                                <asp:ImageButton ID="imgCalDate2" runat="server" ImageUrl="~/Images/calendar.png"
                                    Style="vertical-align: bottom; border: none;" />
                                <br />
                                <asp:CompareValidator ID="CompareValidator6" runat="server" ControlToValidate="txtDetailedPlanningApplication"
                                    CssClass="Required" Display="Dynamic" ErrorMessage="Enter valid date" Operator="DataTypeCheck"
                                    Style="float: left;" Type="Date" ValidationGroup="save" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Outline Planning Approval:
                            </td>
                            <td>
                                <asp:TextBox ID="txtOutlinePlanningApproval" runat="server" Style="width: 160px !important;"></asp:TextBox>
                                <ajaxToolkit:CalendarExtender ID="clndrOutlinePlanningApproval" runat="server" Format="dd/MM/yyyy"
                                    PopupButtonID="imgCalDate3" PopupPosition="Right" TargetControlID="txtOutlinePlanningApproval"
                                    TodaysDateFormat="dd/MM/yyyy">
                                </ajaxToolkit:CalendarExtender>
                                <asp:ImageButton ID="imgCalDate3" runat="server" ImageUrl="~/Images/calendar.png"
                                    Style="vertical-align: bottom; border: none;" />
                                <br />
                                <asp:CompareValidator ID="CompareValidator7" runat="server" ControlToValidate="txtOutlinePlanningApproval"
                                    CssClass="Required" Display="Dynamic" ErrorMessage="Enter valid date" Operator="DataTypeCheck"
                                    Style="float: left;" Type="Date" ValidationGroup="save" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Detailed Planning Approval:
                            </td>
                            <td>
                                <asp:TextBox ID="txtDetailedPlanningApproval" runat="server" Style="width: 160px !important;"></asp:TextBox>
                                <ajaxToolkit:CalendarExtender ID="clndrDetailedPlanningApproval" runat="server" Format="dd/MM/yyyy"
                                    PopupButtonID="imgCalDate4" PopupPosition="Right" TargetControlID="txtDetailedPlanningApproval"
                                    TodaysDateFormat="dd/MM/yyyy">
                                </ajaxToolkit:CalendarExtender>
                                <asp:ImageButton ID="imgCalDate4" runat="server" ImageUrl="~/Images/calendar.png"
                                    Style="vertical-align: bottom; border: none;" />
                                <br />
                                <asp:CompareValidator ID="CompareValidator8" runat="server" ControlToValidate="txtDetailedPlanningApproval"
                                    CssClass="Required" Display="Dynamic" ErrorMessage="Enter valid date" Operator="DataTypeCheck"
                                    Style="float: left;" Type="Date" ValidationGroup="save" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Total Number of Units:
                            </td>
                            <td>
                                <asp:TextBox ID="txtNoofUnits" runat="server" Style="width: 160px;"></asp:TextBox>
                                <asp:CompareValidator ID="CompareValidator9" runat="server" ControlToValidate="txtNoofUnits"
                                    CssClass="Required" Display="Dynamic" ErrorMessage="Only digits allowed" Type="Double"
                                    ValidationGroup="save">
                                </asp:CompareValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtNoofUnits"
                                    Display="Dynamic" ErrorMessage="&lt;br/&gt;Please enter valid non-negative number."
                                    ForeColor="Red" ValidationExpression="^[0-9]*$" ValidationGroup="save" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Funding Source:
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlFundingSource" runat="server" Style="width: 160px;">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Grant Amount(£):
                            </td>
                            <td>
                                <asp:TextBox ID="txtGrantAmountOpt" runat="server" Style="width: 160px !important"></asp:TextBox>
                                <asp:Button ID="btnAddGrantAmount" runat="server" OnClick="btnAddGrantAmount_Click"
                                    Text="Add" UseSubmitBehavior="false" ValidationGroup="addfunds" />
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ControlToValidate="txtGrantAmountOpt"
                                    Display="Dynamic" ErrorMessage="&lt;br/&gt;Please enter valid non-negative number."
                                    ForeColor="Red" ValidationExpression="^[0-9]*$" ValidationGroup="save" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                                <div style="max-height: 105px; height: 105px; width: 73.5%; overflow: auto; border: 1px solid gray;">
                                    <asp:GridView ID="grdGrantAmount" runat="server" AllowPaging="false" AllowSorting="false"
                                        AutoGenerateColumns="False" GridLines="None" ShowHeader="false" Width="90%">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblFundingAuthority" runat="server" Style="float: left;" Text='<%# Bind("FUNDINGAUTHORITY") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="80%" />
                                                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="lblGrantAmount" runat="server" Text='<%# Bind("FUNDGRANTAMOUNT") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="20%" />
                                                <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </td>
                        </tr>
                    </caption>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    <div class="devRightSection-divider resltables-right">
        <asp:UpdatePanel ID="updPnlPhase" runat="server" UpdateMode="Conditional" class="table-container"
            style="padding: 0px;">
            <ContentTemplate>
                <table id="tblPhase" cellpadding="5">
                    <tr style="border-bottom: 1px solid black;">
                        <td>
                            <strong>Development Phase:</strong>
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlDevelopmentPhase" Enabled="false" Style="width: 121px;"
                                OnSelectedIndexChanged="ddlDevelopmentPhase_SelectedIndexChanged" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Phase Name:<span class="Required">*</span>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtPhaseName" MaxLength="50"></asp:TextBox>
                            <br />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtPhaseName"
                                EnableClientScript="True" ErrorMessage="Special characters are not allowed" CssClass="Required"
                                Display="Dynamic" ValidationExpression="^[A-Za-z0-9- ]+$" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtPhaseName"
                                ErrorMessage="Required" CssClass="Required" ValidationGroup="save" Display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Construction Start:
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtConstructionStart" Style="width: 216px !important;"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender ID="clndrConstructionStart" runat="server" TargetControlID="txtConstructionStart"
                                PopupButtonID="imgConstructionStartDate" PopupPosition="Right" TodaysDateFormat="dd/MM/yyyy"
                                Format="dd/MM/yyyy">
                            </ajaxToolkit:CalendarExtender>
                            <asp:ImageButton ImageUrl="~/Images/calendar.png" runat="server" ID="imgConstructionStartDate"
                                Style="vertical-align: bottom; border: none;" />
                            <br />
                            <asp:CompareValidator ID="CompareValidator14" runat="server" Style="float: left;"
                                ControlToValidate="txtConstructionStart" ErrorMessage="Enter valid date" Operator="DataTypeCheck"
                                Type="Date" Display="Dynamic" CssClass="Required" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Anticipated Completion:
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtAnticipatedCompletion" Style="width: 216px !important;"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender runat="server" ID="clndrAnticipatedCompletion" TargetControlID="txtAnticipatedCompletion"
                                PopupButtonID="imgAnticipatedCompletionDate" PopupPosition="Right" TodaysDateFormat="dd/MM/yyyy"
                                Format="dd/MM/yyyy">
                            </ajaxToolkit:CalendarExtender>
                            <asp:ImageButton ImageUrl="~/Images/calendar.png" runat="server" ID="imgAnticipatedCompletionDate"
                                Style="vertical-align: bottom; border: none;" />
                            <br />
                            <asp:CompareValidator ID="CompareValidator15" runat="server" Style="float: left;"
                                ControlToValidate="txtAnticipatedCompletion" ErrorMessage="Enter valid date"
                                Operator="DataTypeCheck" Type="Date" Display="Dynamic" CssClass="Required" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Actual Completion:
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtActualCompletion" Style="width: 216px !important;"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender runat="server" ID="clndrActualCompletion" TargetControlID="txtActualCompletion"
                                PopupButtonID="imgActualCompletionDate" PopupPosition="Right" TodaysDateFormat="dd/MM/yyyy"
                                Format="dd/MM/yyyy">
                            </ajaxToolkit:CalendarExtender>
                            <asp:ImageButton ImageUrl="~/Images/calendar.png" runat="server" ID="imgActualCompletionDate"
                                Style="vertical-align: bottom; border: none;" />
                            <br />
                            <asp:CompareValidator ID="CompareValidator1" runat="server" Style="float: left;"
                                ControlToValidate="txtActualCompletion" ErrorMessage="Enter valid date" Operator="DataTypeCheck"
                                Type="Date" Display="Dynamic" CssClass="Required" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Handover into Management:
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtManagement" Style="width: 216px !important;"></asp:TextBox>
                            <ajaxToolkit:CalendarExtender runat="server" ID="clndrManagement" TargetControlID="txtManagement"
                                PopupButtonID="imgManagementDate" PopupPosition="Right" TodaysDateFormat="dd/MM/yyyy"
                                Format="dd/MM/yyyy">
                            </ajaxToolkit:CalendarExtender>
                            <asp:ImageButton ImageUrl="~/Images/calendar.png" runat="server" ID="imgManagementDate"
                                Style="vertical-align: bottom; border: none;" />
                            <br />
                            <asp:CompareValidator ID="CompareValidator2" runat="server" Style="float: left;"
                                ControlToValidate="txtManagement" ErrorMessage="Enter valid date" Operator="DataTypeCheck"
                                Type="Date" Display="Dynamic" CssClass="Required" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Scheme Name:
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlScheme" Style="width: 121px;">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="float: left;">
                            <asp:Button runat="server" ID="btnAddNewPhase" Text="Add New Phase" OnClick="btnAddNewPhase_Click" />
                        </td>
                        <td>
                            <asp:Button runat="server" ID="btnSavePhase" Text="Save" Style="float: right;" OnClick="btnSavePhase_Click" />
                        </td>
                    </tr>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="table-container">
            <b>Blocks:</b><br />
            <br />
            <div style="height: 125px; overflow: auto;">
                <asp:GridView runat="server" AllowSorting="false" AllowPaging="false" AutoGenerateColumns="False"
                    ShowHeaderWhenEmpty="true" ID="grdBlocks" EmptyDataText="No Record Found" class="blocks-grid">
                    <Columns>
                        <asp:TemplateField HeaderText="Name:" SortExpression="BLOCKNAME">
                            <ItemTemplate>
                                <asp:Label ID="lblBlockName" runat="server" Text='<%# Bind("BLOCKNAME") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="40%" />
                            <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Address:" SortExpression="BlockAddress">
                            <ItemTemplate>
                                <asp:Label ID="lblBlockAddress" runat="server" Text='<%# Bind("BlockAddress") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="60%" />
                            <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
        <div class="table-container">
            <b>Documents:</b>
            <hr />
            <asp:UpdatePanel runat="server" ID="updPnlDocuments" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:Label ID="lblStatus" runat="server" Style="font-family: Arial; font-size: small;"></asp:Label>
                    <uim:UIMessage ID="uiMessageDoc" runat="Server" Visible="false" width="500px" />
                    <table id="tblDocuments" style="border-collapse: collapse;">
                        <tr>
                            <td>
                                Category:<span class="Required">*</span><!--Category-->
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlDocumentCategory" runat="server" class="selectval" CssClass="selectval"
                                    AutoPostBack="False" 
                                    >
                                   <%-- <asp:ListItem Value="Compliance" Text="Compliance" Enabled="true" Selected="True"></asp:ListItem>
                                    <asp:ListItem Value="General" Text="General" Enabled="true"></asp:ListItem>
                                    <asp:ListItem Value="Legal" Text="Legal" Enabled="true"></asp:ListItem>--%>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Type:<span class="Required">*</span>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlType" runat="server" class="selectval" CssClass="selectval"
                                    AutoPostBack="True" OnSelectedIndexChanged="ddlType_SelectedIndexChanged">
                                </asp:DropDownList>
                                <!--ddlType_SelectedIndexChanged
                                
                                 OnSelectedIndexChanged="ddlType_SelectedIndexChanged"
                                -->
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Title:<span class="Required">*</span>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlTitle" runat="server" class="selectval" CssClass="selectval">
                                </asp:DropDownList>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Document:<span class="Required">*</span>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtDocumrntDate" Style="width: 150px !important;"></asp:TextBox>
                                <ajaxToolkit:CalendarExtender runat="server" ID="CalendarExtender2" TargetControlID="txtDocumrntDate"
                                    PopupButtonID="imgDocumrntDate"  Animated="False" TodaysDateFormat="dd/MM/yyyy"
                                    Format="dd/MM/yyyy">
                                </ajaxToolkit:CalendarExtender>
                                <asp:ImageButton ImageUrl="~/Images/calendar.png" runat="server" ID="imgDocumrntDate"
                                    Style="vertical-align: bottom; border: none;" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtDocumrntDate"
                                    ErrorMessage="Required" CssClass="Required" ValidationGroup="Doc" Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="CompareValidator17" runat="server" ControlToValidate="txtDocumrntDate"
                                    ErrorMessage="<br/>Enter valid date" Operator="DataTypeCheck" Type="Date" Display="Dynamic"
                                    CssClass="Required" ValidationGroup="Doc" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Expires:<span class="Required">*</span>
                            </td>
                            <td>
                                <asp:TextBox runat="server" ID="txtExpires" Style="width: 150px !important;"></asp:TextBox>

                                <ajaxToolkit:CalendarExtender runat="server" Animated="False" ID="clndrExpires" TargetControlID="txtExpires"
                                    PopupButtonID="imgExpiresDate" PopupPosition="Right" TodaysDateFormat="dd/MM/yyyy"
                                    Format="dd/MM/yyyy">
                                </ajaxToolkit:CalendarExtender>
                                

                                <asp:ImageButton ImageUrl="~/Images/calendar.png" runat="server" ID="imgExpiresDate"
                                    Style="vertical-align: bottom; border: none;" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtExpires"
                                    ErrorMessage="Required" CssClass="Required" ValidationGroup="Doc" Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="txtExpires"
                                    ErrorMessage="<br/>Enter valid date" Operator="DataTypeCheck" Type="Date" Display="Dynamic"
                                    CssClass="Required" ValidationGroup="Doc" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Browse:<span class="Required">*</span>
                            </td>
                            <td>
                                <div class="input-area" style="float: left; width: 90% !important;">
                                    <asp:FileUpload ID="flUploadDoc" runat="server" class="upload" Style="word-wrap: break-word !important;" />
                                    <asp:Button ID="btnUpdate" runat="server" Text="Upload" EnableViewState="true" ViewStateMode="Enabled"
                                        OnClick="btnAddDocument_Click" ValidationGroup="Doc" Style="float: right; margin-left: 5%;
                                        margin-top: 5px;" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                    <div style="max-height: 140px; height: 140px; overflow: auto;">
                        <asp:GridView runat="server" ID="grdDocuments" AllowSorting="false" AllowPaging="false"
                            AutoGenerateColumns="False" ShowHeader="true" EmptyDataText="No Record Found"
                            class="document-grid" ShowHeaderWhenEmpty="true">
                            <Columns>
                                <asp:TemplateField HeaderText="Dated Added:">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUploadedDate" runat="server" Style="float: left;" Text='<%# Bind("UploadedDate") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="10%" HorizontalAlign="Left" />
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="By:">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUploadedBy" runat="server" Style="float: left;" Text='<%# Bind("CreatedBy") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="10%" HorizontalAlign="Left" />
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Type:">
                                    <ItemTemplate>
                                        <asp:Label ID="lblFileType" runat="server" Text='<%# Bind("FileType") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="10%" HorizontalAlign="Left" />
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Title:">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTitle" runat="server" Style="float: left;" Text='<%# Bind("Title") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="10%" HorizontalAlign="Left" />
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Category:">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCategory" runat="server" Style="float: left;" Text='<%# Bind("Category") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="10%" HorizontalAlign="Left" />
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Document:">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDocumentDate" runat="server" Style="float: left;" Text='<%# Bind("DocumentDate") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="10%" HorizontalAlign="Left" />
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Expiry:">
                                    <ItemTemplate>
                                        <asp:Label ID="lblExpires" runat="server" Text='<%# Bind("Expire") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="10%" HorizontalAlign="Left" />
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Button ID="btnDeleteDocument" runat="server" CommandArgument='<%#Eval("DocumentId")%>'
                                            Text="Delete" UseSubmitBehavior="False" OnClick="btnDeleteDocument_Click" OnClientClick="if(! confirm('Are you sure you want to permanently remove the selected file?')) return false;" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Right" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Button ID="btnViewDocument" runat="server" Text="View" UseSubmitBehavior="False"
                                            CommandArgument='<%#Eval("DocumentId")%>' OnClick="btnViewDocument_Click"  OnDataBinding = "PostBackBind_DataBinding"/>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Right" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:PostBackTrigger ControlID="btnUpdate" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
        <br />
        <asp:UpdatePanel runat="server" ID="updPnlButtons" UpdateMode="Always">
            <ContentTemplate>
                <div style="float: right;">
                    <asp:Button runat="server" ID="btnCancel" Text="Cancel" OnClick="btnCancel_Click" />
                    &nbsp &nbsp
                    <asp:Button runat="server" ID="btnAddDevelopment" Text="Save New Development" OnClick="btnAddDevelopment_Click" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</asp:Content>
