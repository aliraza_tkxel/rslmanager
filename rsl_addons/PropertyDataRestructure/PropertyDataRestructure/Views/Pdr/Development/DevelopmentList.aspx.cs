﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PDR_BusinessLogic.Development;
using PDR_BusinessObject;
using System.Data;
using PagingGridView;
using PDR_Utilities;
using PDR_BusinessObject.PageSort;
using PDR_DataAccess.Development;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PropertyDataRestructure.Base;
using PDR_Utilities.Helpers;
using PropertyDataRestructure.Interface;
using PDR_Utilities.Constants;

namespace PropertyDataRestructure.Views.Pdr.Development
{
    public partial class DevelopmentList : PageBase , IListingPage
    {
        PageSortBO objPageSortBo = new PageSortBO("Desc", "DevelopmentName", 1, 30);

        #region"Events"
        
        #region "Page Load"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    populateCompanyDropdownList();
                    loadData(); 
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #region Sorting Event Handler
        /// <summary>
        /// Sorting event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdDevelopmentList_Sorting(Object sender, GridViewSortEventArgs e)
        {
            try
            {
                objPageSortBo = pageSortViewState;

                objPageSortBo.SortExpression = e.SortExpression;
                objPageSortBo.PageNumber = 1;
                grdDevelopmentList.PageIndex = 0;
                objPageSortBo.setSortDirection();

                pageSortViewState = objPageSortBo;
                loadData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region"txtSearch TextChanged"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void txtSearch_TextChanged(object sender, EventArgs e)
        {
            try
            {
                pageSortViewState = null;
                loadData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #region "Company Dropdown Selected Index Changed"
        /// <summary>
        /// Company Dropdown Selected Index Changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param> 
        protected void ddlCompany_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                pageSortViewState = null;
                loadData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #region"change PageNumber"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void changePageNumber(object sender, EventArgs e)
        {
            try
            {
                Button btnGo = new Button();
                btnGo = (Button)sender;

                objPageSortBo = new PageSortBO("DESC", "DevelopmentName", 1, 30);
                objPageSortBo = pageSortViewState;
                int pageNumber = 1;
                pageNumber = Convert.ToInt32(txtPageNumber.Text);
                txtPageNumber.Text = String.Empty;
                objPageSortBo = pageSortViewState;
                if (((pageNumber >= 1)
                            && (pageNumber <= objPageSortBo.TotalPages)))
                {
                    objPageSortBo = pageSortViewState;
                    objPageSortBo.PageNumber = pageNumber;
                    pageSortViewState = objPageSortBo;
                    loadData();

                }
                else
                {
                    // uiMessageHelper.setMessage(lblMessageControl, pnlMessageControl, UserMessageConstants.InvalidPageNumber, true);
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #region"lnkbtnPager Click"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkbtnPager_Click(object sender, EventArgs e)
        {
            try
            {
                objPageSortBo = new PageSortBO("DESC", "DevelopmentName", 1, 30);
                GridHelper.processGridPager(sender, objPageSortBo, pageSortViewState);
                loadData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #region"btn AddNewDevelopment Click"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAddNewDevelopment_Click(object sender, EventArgs e)
        {
            string url = "~/Views/Pdr/Development/AddNewDevelopment.aspx";
            Response.Redirect(url);
        }

        #endregion

        #region "Development List row Databound"
        protected void grdDevelopmentList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    GridViewRow row = e.Row;
                    Label lblDevelopmentId = (Label)row.FindControl("lblDevelopmentId");
                    e.Row.Attributes.Add("onclick", string.Format("javascript:ShowDevelopmentDetail('{0}')", lblDevelopmentId.Text));
                    e.Row.Attributes.Add("onmouseover", "this.style.cursor='pointer'");
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        #endregion


        protected void grdDevelopmentList_RowCreated(object sender, GridViewRowEventArgs e)
        {
            //check if it is a header row
            //since allowsorting is set to true, column names are added as command arguments to
            //the linkbuttons by DOTNET API
            if (e.Row.RowType == DataControlRowType.Header)
            {
                LinkButton btnSort;
                Image image;
                //iterate through all the header cells
                foreach (TableCell cell in e.Row.Cells)
                {
                    //check if the header cell has any child controls
                    if (cell.HasControls())
                    {
                        //get reference to the button column
                        btnSort = (LinkButton)cell.Controls[0];
                        image = new Image();
                        if (pageSortViewState != null)
                        {
                            if (btnSort.CommandArgument == pageSortViewState.SortExpression)
                            {
                                //following snippet figure out whether to add the up or down arrow
                                //based on the sortdirection
                                if (pageSortViewState.SortDirection == SortDirection.Ascending.ToString())
                                {
                                    image.ImageUrl = "~/Images/Grid/sort_asc.png";
                                }
                                else
                                {
                                    image.ImageUrl = "~/Images/Grid/sort_desc.png";
                                }
                            }
                            else
                            {
                                image.ImageUrl = "~/Images/Grid/sort_both.png";
                            }
                            cell.Controls.Add(image);
                        }
                    }
                }
            }
        }



        #endregion

        #region Populate Maintenance Type Dropdown List
        /// <summary>
        /// Populate Maintenance Type Dropdown List
        /// </summary>
        /// <remarks></remarks>
        public void populateCompanyDropdownList()
        {
            ddlCompany.Items.Clear();

            DevelopmentBL objMaintenanceBL = new DevelopmentBL(new DevelopmentRepo());
            DataSet resultDataSet = new DataSet();
            objMaintenanceBL.getCompany(ref resultDataSet);

            ddlCompany.DataSource = resultDataSet;
            ddlCompany.DataValueField = "CompanyId";
            ddlCompany.DataTextField = "Description";
            ddlCompany.DataBind();

            ListItem newListItem = default(ListItem);
            newListItem = new ListItem("All", "-1");
            ddlCompany.Items.Insert(0, newListItem);

        }

        #endregion


        #region " IListing Page Events"

        public void loadData()
        {
            

            int totalCount = 0;
            if (pageSortViewState == null)
            {
                objPageSortBo = new PageSortBO("DESC", "DevelopmentName", 1, 30);
                pageSortViewState = objPageSortBo;
            }
            else
            {
                objPageSortBo = pageSortViewState;
            }
            String searchText = txtSearch.Text ;
            String searchCompany = ddlCompany.SelectedValue.ToString();

            DevelopmentBL objDevelopmentBL = new DevelopmentBL(new DevelopmentRepo());
            DataSet resultDataSet = new DataSet();
            totalCount = objDevelopmentBL.getDevelopmentList(ref resultDataSet, objPageSortBo, searchText,searchCompany);
            grdDevelopmentList.DataSource = resultDataSet;
            grdDevelopmentList.DataBind();
            objPageSortBo.TotalRecords = totalCount;
            objPageSortBo.TotalPages = Convert.ToInt32(Math.Ceiling((Double)totalCount / objPageSortBo.PageSize));

            if (objPageSortBo.TotalPages > 0)
            {
                pnlPagination.Visible = true;
            }
            else
            {
                pnlPagination.Visible = false;
                uiMessage.showErrorMessage(UserMessageConstants.RecordNotFound);
            }
            pageSortViewState = objPageSortBo;

            GridHelper.setGridViewPager(ref pnlPagination, objPageSortBo);

         
        }

        public void populateData()
        {
            throw new NotImplementedException();
        }


        public void searchData()
        {
            throw new NotImplementedException();
        }

        public void printData()
        {
            throw new NotImplementedException();
        }

        public void exportGridToExcel()
        {
            throw new NotImplementedException();
        }

        public void exportToPdf()
        {
            throw new NotImplementedException();
        }

        public void downloadData()
        {
            throw new NotImplementedException();
        }

        public void applyFilters()
        {
            throw new NotImplementedException();
        }


        #endregion
    }
}