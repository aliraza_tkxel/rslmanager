﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Text;
using System.IO;
using System.Net;
using System.Web.UI;
using System.Web.UI.WebControls;
using PropertyDataRestructure.Base;
using PDR_Utilities.Constants;
using PDR_Utilities.Helpers;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_BusinessObject.Notes;
using PDR_BusinessObject.MeAppointment;
using PDR_BusinessObject.Customer;
using PDR_DataAccess.Scheduling;
using PDR_DataAccess.Customer;
using PDR_BusinessLogic.Scheduling;
using PDR_BusinessLogic.CustomerBL;
using PDR_BusinessObject.TradeAppointment;
using System.Text.RegularExpressions;

namespace PropertyDataRestructure.Views.Scheduling
{

    public partial class ToBeArrangedAppointmentSummary : PageBase
    {

        #region "Properties"

        #endregion

        #region "Events"

        #region "Page Load Event"
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>

        protected void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                uiMessage.hideMessage();

                if (!IsPostBack)
                {
                    prePopulatedValues();
                }

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessage.isError == true)
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }

        #endregion

        #region "Button Update Customer Detail Click Event"
        /// <summary>
        /// Button Update Customer Detail Click Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void btnUpdateCustomerDetails_Click(object sender, EventArgs e)
        {
            try
            {
                updateAddress();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessage.isError == true)
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #region "Button Schedule Appointment Click Event"
        /// <summary>
        /// Button Schedule Appointment Click Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void btnScheduleAppointment_Click(object sender, EventArgs e)
        {

            try
            {
                NotesBO objNotesBO = new NotesBO();
                objNotesBO.CustomerNotes = txtCustAppointmentNotes.Text;
                objNotesBO.AppointmentNotes = txtJobSheetNotes.Text;

                Validator<NotesBO> objValidator = valFactory.CreateValidator<NotesBO>();
                ValidationResults results = objValidator.Validate(objNotesBO);

                if (results.IsValid)
                {
                    scheduleAppointment();
                }

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessage.isError == true)
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #region "Button Back Click Event"
        /// <summary>
        /// Button Back Click Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                navigateToScheduleMeServicing();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessage.isError == true)
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #endregion

        #region "Function"

        #region "Navigate to Schedule ME Servicing"
        /// <summary>
        /// Navigate to ME Servicing
        /// </summary>
        /// <remarks></remarks>
        public void navigateToScheduleMeServicing()
        {
            Response.Redirect(PathConstants.BackToScheduleMeServicingPath + objSession.SelectedAttribute.MsatQueryStringPath + "&" + PathConstants.ToBeArrangedJobsheetSrc);
        }

        #endregion

        #region "Schedule Appointment"
        /// <summary>
        /// Schedule Appointment
        /// </summary>
        /// <remarks></remarks>
        public void scheduleAppointment()
        {
            MeAppointmentBO objMeAppointmentBO = objSession.MeAppointmentBO;
            List<ConfirmMeAppointmentBO> confirmMeAptBoList = new List<ConfirmMeAppointmentBO>();
            confirmMeAptBoList = objSession.ConfirmMeAppointmetBlockList;
            ConfirmMeAppointmentBO objConfirmMeAppointmentBO = new ConfirmMeAppointmentBO();
            SchedulingBL objSchedulingBL = new SchedulingBL(new SchedulingRepo());
            string isSaved = string.Empty;
            int journalId = objMeAppointmentBO.JournalId;
            int appointmentId = Convert.ToInt32(ApplicationConstants.DefaultValue);

            if (objMeAppointmentBO == null)
            {
                uiMessage.showErrorMessage(UserMessageConstants.ProblemLoadingData);
                btnScheduleAppointment.Enabled = false;
                btnUpdateCustomerDetails.Enabled = false;

            }
            else
            {
                objMeAppointmentBO.CustomerNotes = txtCustAppointmentNotes.Text;
                objMeAppointmentBO.AppointmentNotes = txtJobSheetNotes.Text;
                objMeAppointmentBO.StartTime = objMeAppointmentBO.StartDate.ToString("hh:mm tt");
                objMeAppointmentBO.EndTime = objMeAppointmentBO.EndDate.ToString("hh:mm tt");

                isSaved = objSchedulingBL.scheduleMeWorkAppointment(objMeAppointmentBO, journalId, ref appointmentId);

                if (isSaved.Equals(ApplicationConstants.NotSaved))
                {
                    uiMessage.showErrorMessage(UserMessageConstants.SaveAppointmentFailed);
                }
                else
                {
                    uiMessage.showInformationMessage(UserMessageConstants.SaveAppointmentSuccessfully);
                    btnScheduleAppointment.Enabled = false;
                    btnUpdateCustomerDetails.Enabled = false;
                    txtCustAppointmentNotes.Attributes.Add("readonly", "readonly");
                    txtJobSheetNotes.Attributes.Add("readonly", "readonly");
                    hid_isAppointmentCreated.Value = Convert.ToString(1);
                    objMeAppointmentBO.AppointmentId = appointmentId;
                    lblJsn.Text = appointmentId.ToString().PadLeft(6, '0');

                    objSession.MeAppointmentBO = objMeAppointmentBO;
                    // set confirm appointment in session
                    objConfirmMeAppointmentBO.JournalId = journalId;
                    objConfirmMeAppointmentBO.AppointmentId = appointmentId;
                    objConfirmMeAppointmentBO.Trade = objMeAppointmentBO.TradeName;
                    objConfirmMeAppointmentBO.TradeId = objMeAppointmentBO.TradeId;
                    objConfirmMeAppointmentBO.Duration = objMeAppointmentBO.Duration;
                    objConfirmMeAppointmentBO.DurationString = GeneralHelper.appendHourLabel(objMeAppointmentBO.Duration);

                    objConfirmMeAppointmentBO.tempAppointmentDtBo.operativeId = objMeAppointmentBO.OperativeId;
                    objConfirmMeAppointmentBO.tempAppointmentDtBo.operative = objMeAppointmentBO.OperativeName;
                    objConfirmMeAppointmentBO.tempAppointmentDtBo.startTime = GeneralHelper.getHrsAndMinString(Convert.ToDateTime(objMeAppointmentBO.StartTime));
                    objConfirmMeAppointmentBO.tempAppointmentDtBo.endTime = GeneralHelper.getHrsAndMinString(Convert.ToDateTime(objMeAppointmentBO.EndTime));
                    objConfirmMeAppointmentBO.tempAppointmentDtBo.startDateString = GeneralHelper.convertDateToCustomString(objMeAppointmentBO.StartDate);
                    objConfirmMeAppointmentBO.tempAppointmentDtBo.endDateString = GeneralHelper.convertDateToCustomString(objMeAppointmentBO.EndDate);
                    objConfirmMeAppointmentBO.tempAppointmentDtBo.tradeId = Convert.ToInt32(objMeAppointmentBO.TradeId);
                    objConfirmMeAppointmentBO.tempAppointmentDtBo.addNewDataRow();
                    confirmMeAptBoList.Add(objConfirmMeAppointmentBO);
                    objSession.ConfirmMeAppointmetBlockList = confirmMeAptBoList;

                    //remove tem ME appointment from session which has been completed
                    List<TempMeAppointmentBO> tempMeAptList = new List<TempMeAppointmentBO>();
                    List<TempMeAppointmentBO> updatedTempMeAptList = new List<TempMeAppointmentBO>();
                    tempMeAptList = objSession.TempMeAppointmentBOList;

                    foreach (TempMeAppointmentBO item in tempMeAptList)
                    {
                        if (!item.TradeId.Equals(objMeAppointmentBO.TradeId))
                        {
                            updatedTempMeAptList.Add(item);
                        }
                    }
                    //set updated Me appointment in session
                    objSession.TempMeAppointmentBOList = updatedTempMeAptList;
                    
                    // Send ICal file via email
                    sendICalFileViaEmail(objMeAppointmentBO);
                    
                    // Send push notification
                    pushNotificationAppliance(objMeAppointmentBO.MsatType, objMeAppointmentBO.StartDate, objMeAppointmentBO.StartTime
                        , objMeAppointmentBO.EndTime, objMeAppointmentBO.OperativeId);
                }
            }


        }

        #endregion

        #region "Pre Populate Values"
        /// <summary>
        /// Pre Populate Values
        /// </summary>
        /// <remarks></remarks>
        public void prePopulatedValues()
        {
            MeAppointmentBO objMeAppointmentBO = objSession.MeAppointmentBO;
            SelectedAttributeBO objSelectedAttributeBO = objSession.SelectedAttribute;

            if (objMeAppointmentBO == null || objSelectedAttributeBO == null)
            {
                uiMessage.showErrorMessage(UserMessageConstants.ProblemLoadingData);
                btnScheduleAppointment.Enabled = false;
                btnUpdateCustomerDetails.Enabled = false;
            }
            else
            {
                this.populateAppointmentInfo();
                this.populatePropertyInfo();
                this.populateNotes();
                this.checkAppointmentCreated();
            }

        }

        #endregion

        #region "Check appointment created."
        /// <summary>
        /// Check appointment created.
        /// </summary>
        /// <remarks></remarks>

        public void checkAppointmentCreated()
        {
            if (!hid_isAppointmentCreated.Value.Equals("0"))
            {
                btnScheduleAppointment.Enabled = false;
                btnUpdateCustomerDetails.Enabled = false;
                txtCustAppointmentNotes.Attributes.Add("readonly", "readonly");
                txtJobSheetNotes.Attributes.Add("readonly", "readonly");
            }

        }

        #endregion

        #region "Populate Appointment Info"
        /// <summary>
        /// Populate Appointment Info
        /// </summary>
        /// <remarks></remarks>
        public void populateAppointmentInfo()
        {
            MeAppointmentBO objMeAppointmentBO = objSession.MeAppointmentBO;

            if (objMeAppointmentBO.AppointmentId == ApplicationConstants.NoneValue)
            {
                lblJsn.Text = ApplicationConstants.NotAvailable;
            }
            else
            {
                lblJsn.Text = objMeAppointmentBO.AppointmentId.ToString().PadLeft(6, '0');
            }
            lblHeading.Text = objMeAppointmentBO.MsatType;
            lblWorkType.Text = objMeAppointmentBO.MsatType;
            lblAppointmentType.Text = objMeAppointmentBO.MsatType + " : " + objSession.SelectedAttribute.AttributeName;
            lblTrade.Text = objMeAppointmentBO.TradeName;
            lblOperative.Text = objMeAppointmentBO.OperativeName;
            lblDuration.Text = GeneralHelper.appendHourLabel(objMeAppointmentBO.Duration);
            lblDurationTotal.Text = GeneralHelper.appendHourLabel(getTotalDuration());
            DateTime startDateTime = Convert.ToDateTime(objMeAppointmentBO.StartDate + " " + objMeAppointmentBO.StartTime);
            DateTime endDateTime = Convert.ToDateTime(objMeAppointmentBO.EndDate + " " + objMeAppointmentBO.EndTime);
            lblStartDate.Text = Convert.ToDateTime(objMeAppointmentBO.StartDate).ToString("HH:mm") + " " + GeneralHelper.getDateWithWeekdayFormat(startDateTime);
            lblEndDate.Text = Convert.ToDateTime(objMeAppointmentBO.EndDate).ToString("HH:mm") + " " + GeneralHelper.getDateWithWeekdayFormat(endDateTime);

        }

        #endregion

        #region "Get Total Duration"
        /// <summary>
        /// Get Total Duration
        /// </summary>
        /// <remarks></remarks>

        public double getTotalDuration()
        {
            DataTable dtTrades = objSession.TradesDataTable;
            double totalDuration = 0;
            foreach (DataRow row in dtTrades.Rows)
            {
                totalDuration = totalDuration + Convert.ToDouble(row[ApplicationConstants.DurationsColumn]);
            }

            return totalDuration;
        }

        #endregion

        #region "Populate Property Info"
        /// <summary>
        /// Populate Property Info
        /// </summary>
        /// <remarks></remarks>

        public void populatePropertyInfo()
        {
            MeAppointmentBO objMeAppointmentBO = objSession.MeAppointmentBO;
            SchedulingBL objSchedulingBL = new SchedulingBL(new SchedulingRepo());
            int journalId = objMeAppointmentBO.JournalId;
            objMeAppointmentBO.UserId = objSession.EmployeeId;

            if (journalId == ApplicationConstants.NoneValue)
            {
                uiMessage.showErrorMessage(UserMessageConstants.ProblemLoadingData);
                btnScheduleAppointment.Enabled = false;
                btnUpdateCustomerDetails.Enabled = false;
            }
            else
            {
                DataSet resultDataset = new DataSet();
                objSchedulingBL.getPropertyDetailByJournalId(ref resultDataset, journalId,false);
                DataTable resultDt = resultDataset.Tables[0];

                if (resultDt.Rows.Count == 0)
                {
                    uiMessage.showErrorMessage(UserMessageConstants.InvalidJournalId);
                    btnScheduleAppointment.Enabled = false;
                    btnUpdateCustomerDetails.Enabled = false;
                }
                else
                {
                    //Property Information
                    lblScheme.Text = Convert.ToString(resultDt.Rows[0]["SchemeName"]);
                    lblBlock.Text = Convert.ToString(resultDt.Rows[0]["BlockName"]);
                    lblAddress.Text = Convert.ToString(resultDt.Rows[0]["HOUSENUMBER"] + " " + resultDt.Rows[0]["ADDRESS1"] + " " + resultDt.Rows[0]["ADDRESS2"]);
                    lblTowncity.Text = Convert.ToString(resultDt.Rows[0]["TOWNCITY"]);
                    lblCounty.Text = Convert.ToString(resultDt.Rows[0]["COUNTY"]);
                    lblPostcode.Text = Convert.ToString(resultDt.Rows[0]["POSTCODE"]);

                    //Customer Information
                    hdnCustomerId.Value = Convert.ToString(resultDt.Rows[0]["CustomerId"]);
                    lblCustomerName.Text = Convert.ToString(resultDt.Rows[0]["TenantName"]);
                    lblCustomerTelephone.Text = Convert.ToString(resultDt.Rows[0]["Telephone"]);
                    lblCustomerMobile.Text = Convert.ToString(resultDt.Rows[0]["Mobile"]);
                    lblCustomerEmail.Text = Convert.ToString(resultDt.Rows[0]["Email"]);
                    objMeAppointmentBO.TenancyId = Convert.ToInt32(resultDt.Rows[0]["TenancyId"]);

                    if (objMeAppointmentBO.TenancyId == ApplicationConstants.NoneValue)
                    {
                        btnUpdateCustomerDetails.Enabled = false;
                    }
                }

            }

            objSession.MeAppointmentBO = objMeAppointmentBO;

        }

        #endregion

        #region "Populate Notes"
        /// <summary>
        /// Populate Notes
        /// </summary>
        /// <remarks></remarks>
        public void populateNotes()
        {
            MeAppointmentBO objMeAppointmentBO = objSession.MeAppointmentBO;
            txtCustAppointmentNotes.Text = objMeAppointmentBO.CustomerNotes;
            txtJobSheetNotes.Text = objMeAppointmentBO.AppointmentNotes;
        }

        #endregion

        #region "Update Customer Address"
        /// <summary>
        /// Update Customer Address
        /// </summary>
        /// <remarks></remarks>

        public void updateAddress()
        {
            //Update Enable

            if (btnUpdateCustomerDetails.Text == ApplicationConstants.UpdateCustomerDetails)
            {
                txtCustomerTelephone.Text = lblCustomerTelephone.Text;
                lblCustomerTelephone.Visible = false;
                txtCustomerTelephone.Visible = true;

                txtCustomerMobile.Text = lblCustomerMobile.Text;
                lblCustomerMobile.Visible = false;
                txtCustomerMobile.Visible = true;

                txtCustomerEmail.Text = lblCustomerEmail.Text;
                lblCustomerEmail.Visible = false;
                txtCustomerEmail.Visible = true;

                btnUpdateCustomerDetails.Text = ApplicationConstants.SaveChanges;

            }
            else
            {
                //Save Changes

                if (lblCustomerTelephone.Text != txtCustomerTelephone.Text | lblCustomerMobile.Text != txtCustomerMobile.Text | lblCustomerEmail.Text != txtCustomerEmail.Text)
                {
                    //If Not isError Then
                    CustomerBL objCustomerBL = new CustomerBL(new CustomerRepo());
                    CustomerBO objCustomerBO = new CustomerBO();

                    objCustomerBO.CustomerId = Convert.ToInt32(hdnCustomerId.Value);
                    objCustomerBO.Telephone = txtCustomerTelephone.Text;
                    objCustomerBO.Mobile = txtCustomerMobile.Text;
                    objCustomerBO.Email = txtCustomerEmail.Text;

                    Validator<CustomerBO> objValidator = valFactory.CreateValidator<CustomerBO>();
                    ValidationResults results = objValidator.Validate(objCustomerBO);


                    if (results.IsValid)
                    {
                        objCustomerBL.updateAddress(objCustomerBO);
                        lblCustomerTelephone.Text = txtCustomerTelephone.Text;
                        lblCustomerMobile.Text = txtCustomerMobile.Text;
                        lblCustomerEmail.Text = txtCustomerEmail.Text;
                        txtCustomerTelephone.Visible = false;
                        lblCustomerTelephone.Visible = true;
                        txtCustomerMobile.Visible = false;
                        lblCustomerMobile.Visible = true;
                        txtCustomerEmail.Visible = false;
                        lblCustomerEmail.Visible = true;
                        btnUpdateCustomerDetails.Text = ApplicationConstants.UpdateCustomerDetails;
                        uiMessage.showInformationMessage(UserMessageConstants.UserSavedSuccessfuly);
                    }
                    else
                    {
                        dynamic message = string.Empty;
                        foreach (ValidationResult result in results)
                        {
                            message += result.Message;
                            break; // TODO: might not be correct. Was : Exit For
                        }
                        uiMessage.showErrorMessage(message);
                    }



                }
                else
                {
                    txtCustomerTelephone.Visible = false;
                    lblCustomerTelephone.Visible = true;

                    txtCustomerMobile.Visible = false;
                    lblCustomerMobile.Visible = true;

                    txtCustomerEmail.Visible = false;
                    lblCustomerEmail.Visible = true;

                    btnUpdateCustomerDetails.Text = ApplicationConstants.UpdateCustomerDetails;

                }

            }

        }

        #endregion

        #region "Email Functionality"

        #region "Send iCal File via Email"
        /// <summary>
        /// Send iCal File via Email
        /// </summary>
        /// <remarks></remarks>

        private void sendICalFileViaEmail(MeAppointmentBO objMeAppointmentBO)
        {

            DataSet resultDataset = new DataSet();
            SchedulingBL objSchedulingBL = new SchedulingBL(new SchedulingRepo());
            objSchedulingBL.getOperativeInformation(ref resultDataset, objMeAppointmentBO.OperativeId);

            string operativeName = string.Empty;
            string operativeEmail = string.Empty;
            operativeName = resultDataset.Tables[0].Rows[0]["Name"].ToString();
            if (resultDataset.Tables[0].Rows[0]["Email"].Equals(DBNull.Value))
            {
                operativeEmail = string.Empty;
            }
            else
            {
                operativeEmail = resultDataset.Tables[0].Rows[0]["Email"].ToString();
            }

            string body = this.populateBody(objMeAppointmentBO);
            string subject = objMeAppointmentBO.MsatType + " Appointment Arranged";
            string recepientEmail = operativeEmail;

            try
            {

                if (isEmail(recepientEmail))
                {
                    populateiCalFile(operativeName, operativeEmail, objMeAppointmentBO);
                    EmailHelper.sendHtmlFormattedEmailWithAttachment(operativeName, recepientEmail, subject, body, Server.MapPath(PathConstants.ICalFilePath));
                }
                else
                {
                    uiMessage.showErrorMessage(UserMessageConstants.AppointmentArrangedSavedEmailError + " " + UserMessageConstants.InvalidEmail);
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.showErrorMessage(UserMessageConstants.AppointmentArrangedSavedEmailError + ex.Message);


                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }

            }

        }
        #endregion

        #region "Populate Email body"
        /// <summary>
        /// Populate Email body
        /// </summary>
        /// <param name="objMeAppointmentBO"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        private string populateBody(MeAppointmentBO objMeAppointmentBO)
        {
            StringBuilder body = new StringBuilder();
            StreamReader reader = new StreamReader(Server.MapPath("~/Email/AppointmentArranged.html"));
            body.Append(reader.ReadToEnd().ToString());
            body.Replace("{JSN}", "JSN"+objMeAppointmentBO.AppointmentId.ToString().PadLeft(6, '0'));
            body.Replace("{AppointmentTime}", objMeAppointmentBO.StartTime + " - " + objMeAppointmentBO.EndTime);
            body.Replace("{AppointmentDate}", objMeAppointmentBO.StartDate.ToString("dd'/'MM'/'yyyy"));
            body.Replace("{Address}", lblAddress.Text);
            body.Replace("{TownCity}", lblTowncity.Text);
            body.Replace("{County}", lblCounty.Text);
            body.Replace("{PostCode}", lblPostcode.Text);
            body.Replace("{Block}", lblBlock.Text);
            body.Replace("{Scheme}", lblScheme.Text);
            return body.ToString();
        }
        #endregion

        #region "Validate Email"
        /// <summary>
        /// Validate Email
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static bool isEmail(string value)
        {
            Regex rgx = new Regex(RegularExpConstants.emailExp);
            if (rgx.IsMatch(value))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region "Populate iCal File"
        /// <summary>
        /// 
        /// </summary>
        /// <remarks></remarks>

        public void populateiCalFile(string operativeName, string operativeEmail, MeAppointmentBO objMeAppointmentBO)
        {

            DateTime startTime = DateTime.Parse(objMeAppointmentBO.StartDate.ToString("dd/MM/yyyy") + " " + objMeAppointmentBO.StartTime);
            DateTime endTime = DateTime.Parse(objMeAppointmentBO.EndDate.ToString("dd/MM/yyyy") + " " + objMeAppointmentBO.EndTime);

            StringBuilder iCalString = new StringBuilder();
            iCalString.AppendLine("BEGIN:VCALENDAR");
            iCalString.AppendLine("PRODID:-//Apple Inc.//iCal 5.0.2//EN");
            iCalString.AppendLine("VERSION:2.0");
            iCalString.AppendLine("CALSCALE:GREGORIAN");
            iCalString.AppendLine("METHOD:PUBLISH");
            iCalString.AppendLine("X-WR-CALNAME:BHGcalendar");
            iCalString.AppendLine("X-WR-TIMEZONE:" + TimeZone.CurrentTimeZone.ToString());
            iCalString.AppendLine("X-WR-CALDESC:BHG Calendar");
            iCalString.AppendLine("BEGIN:VEVENT");
            iCalString.AppendLine("ATTENDEEROLE=REQ-PARTICIPANTCN=" + operativeName + ":MAILTO:" + operativeEmail);
            iCalString.AppendLine("DTSTART:" + startTime.ToUniversalTime().ToString("yyyyMMddTHHmmssZ"));
            iCalString.AppendLine("DTEND:" + endTime.ToUniversalTime().ToString("yyyyMMddTHHmmssZ"));
            iCalString.AppendLine("DTSTAMP:20130127T040705Z");
            iCalString.AppendLine("UID:" + Convert.ToString(DateTime.Now.Ticks));
            iCalString.AppendLine("CREATED:" + DateTime.Now.ToUniversalTime().ToString("yyyyMMddTHHmmssZ"));
            iCalString.AppendLine("SUMMARY:" + objMeAppointmentBO.MsatType + " appointment");
            iCalString.AppendLine("DESCRIPTION:" + objMeAppointmentBO.MsatType + " appointment has been arranged with ref JSN" + objMeAppointmentBO.AppointmentId.ToString().PadLeft(6, '0')+". ");
            iCalString.AppendLine("LAST-MODIFIED:" + DateTime.Now.ToUniversalTime().ToString("yyyyMMddTHHmmssZ"));
            iCalString.AppendLine("SEQUENCE:0");
            iCalString.AppendLine("STATUS:CONFIRMED");
            iCalString.AppendLine("TRANSP:TRANSPARENT");
            iCalString.AppendLine("END:VEVENT");
            iCalString.AppendLine("END:VCALENDAR");

            TextWriter iCalTextWriter = new StreamWriter(Server.MapPath(PathConstants.ICalFilePath));
            iCalTextWriter.WriteLine(iCalString);
            iCalTextWriter.Close();
        }

        #endregion

        #endregion

        #region "Send Push Notifications"

        public bool pushNotificationAppliance(string appointmentType, DateTime appointmentDate, String appointmentStartTime, String appointmentEndTime, int operatorId)
        {
            try
            {
                string appointmentTypeDesc = string.Empty;
                string appointmentMessage = string.Empty;

                appointmentTypeDesc = "New " + appointmentType + " Scheduled";
                appointmentMessage += "BHG Property Manager" + "\\n\\n";
                appointmentMessage += appointmentTypeDesc + "\\n\\n";
                appointmentMessage += appointmentDate.ToString("d MMM yyyy");
                appointmentMessage += appointmentStartTime + " to ";
                appointmentMessage += appointmentEndTime + "\\n";
 
                appointmentMessage = HttpUtility.UrlEncode(appointmentMessage);

                string URL = string.Format("{0}?appointmentMessage={1}&operatorId={2}", GeneralHelper.getPushNotificationAddress(HttpContext.Current.Request.Url.AbsoluteUri), appointmentMessage, operatorId);
                WebRequest request = System.Net.WebRequest.Create(URL);
                request.Credentials = CredentialCache.DefaultCredentials;
                WebResponse response = request.GetResponse();
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                string responseFromServer = reader.ReadToEnd();
                reader.Close();
                response.Close();

                if (responseFromServer.ToString().Equals("true"))
                {
                    return true;
                }

                return false;

            }
            catch (Exception ex)
            {
                return false;
            }

        }

        #endregion

        #endregion

    }

}