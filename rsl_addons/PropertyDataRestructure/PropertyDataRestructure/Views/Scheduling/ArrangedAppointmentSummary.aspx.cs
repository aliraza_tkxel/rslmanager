﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PDR_Utilities.Helpers;
using PDR_Utilities.Constants;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_BusinessLogic;
using PropertyDataRestructure.Base;
using PDR_BusinessObject;
using PDR_BusinessObject.TradeAppointment;
using PDR_BusinessObject.MeAppointment;
using PDR_BusinessObject.Notes;
using PDR_DataAccess.Scheduling;
using PDR_BusinessLogic.Scheduling;
using System.Globalization;
using System.Data;
using PDR_DataAccess.Customer;
using PDR_BusinessLogic.CustomerBL;
using PDR_BusinessObject.Customer;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using Microsoft.Practices.EnterpriseLibrary.Validation;

namespace PropertyDataRestructure.Views.Scheduling
{
    public partial class ArrangedAppointmentSummary : PageBase
    {

        #region "Events"

        #region "Page Load Event"
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>

        protected void Page_Load(object sender, System.EventArgs e)
        {
            try
            {

                if (!IsPostBack)
                {
                    if (getvaluesFromSession())
                    {
                        this.getSetAllAppointmentData();
                    }
                    else
                    {
                        disableAllControls();
                    }
                }

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessage.isError == true)
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }

        #endregion

        #region "Button Update Customer Detail Click Event"
        /// <summary>
        /// Button Update Customer Detail Click Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void btnUpdateCustomerDetails_Click(object sender, EventArgs e)
        {
            try
            {
                updateAddress();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessage.isError == true)
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #region "Button Back Click Event"
        /// <summary>
        /// Button Back Click Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                this.navigateToMeServicingWorks();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessage.isError == true)
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #region "Previous button clicked"
        /// <summary>
        /// Previous button clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void btnPrevious_Click(object sender, EventArgs e)
        {
            try
            {
                uiMessage.hideMessage();
                int currentIndex = Convert.ToInt32(ViewState[ViewStateConstants.CurrentIndex]);
                currentIndex = currentIndex - 1;
                ViewState[ViewStateConstants.CurrentIndex] = currentIndex;
                this.setPreviousNextButtonStates(currentIndex);
                populateAppointmentInfo();
                populatePropertyInfo();

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessage.isError == true)
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        #endregion

        #region "Next button clicked"
        /// <summary>
        /// Next button clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void btnNext_Click(object sender, EventArgs e)
        {
            try
            {
                uiMessage.hideMessage();
                int currentIndex = Convert.ToInt32(ViewState[ViewStateConstants.CurrentIndex]);
                currentIndex = currentIndex + 1;
                ViewState[ViewStateConstants.CurrentIndex] = currentIndex;
                this.setPreviousNextButtonStates(currentIndex);
                populateAppointmentInfo();
                populatePropertyInfo();

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessage.isError == true)
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region "Cancel appointment button clicked"
        /// <summary>
        /// Cancel appointment button clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void btnCancelAppointment_click(object sender, EventArgs e)
        {

            try
            {
                openCancelAppointmentPopup();

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessage.isError == true)
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region "Click here button clicked"
        /// <summary>
        /// Click here button clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void lnkBtnClickHere_click(object sender, EventArgs e)
        {

            try
            {
                navigateToMeServicingWorks();

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessage.isError == true)
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region "Confirm cancel appointments button clicked"
        /// <summary>
        /// Confirm cancel appointments button clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void btnConfirmCancel_click(object sender, EventArgs e)
        {
            try
            {
                cancelAppointment();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessage.isError == true)
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region "Popup back button clicked"
        /// <summary>
        /// Popup back button clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void btnBackPopup_click(object sender, EventArgs e)
        {

            try
            {
                mdlPopUpCancelAppointment.Hide();

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessage.isError == true)
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region "Update notes button clicked"
        /// <summary>
        /// Update notes button clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void btnUpdateNotes_click(object sender, EventArgs e)
        {

            try
            {
                updateNotes();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessage.isError == true)
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #endregion

        #region "Function"

        #region "Cancel Functions"

        #region "Cancel appointment"
        /// <summary>
        /// Cancel appointment
        /// </summary>
        /// <remarks></remarks>

        public void cancelAppointment()
        {
            if (txtCancelReason.Text.Trim().Length == 0)
            {
                uiMessagePopup.showErrorMessage(UserMessageConstants.EnterReason);
            }
            else
            {
                SchedulingBL objSchedulingBL = new SchedulingBL(new SchedulingRepo());
                int journalId = objSession.SelectedAttribute.JournalId;
                string reason = txtCancelReason.Text;
                int isCancelled = 0;
                isCancelled = objSchedulingBL.cancelAppointment(journalId, reason);

                if (isCancelled == ApplicationConstants.Cancelled)
                {
                    uiMessagePopup.showInformationMessage(UserMessageConstants.AppointmentCancelledSuccessfully);
                    pnlConfirmButton.Visible = false;
                    pnlCustomerMessage.Visible = true;
                    txtCancelReason.Attributes.Add("readonly", "readonly");
                    this.disableAllControls();
                }
                else
                {
                    uiMessagePopup.showErrorMessage(UserMessageConstants.AppointmentCancelledFailed);
                }

            }

            mdlPopUpCancelAppointment.Show();

        }

        #endregion

        #region "Reset popup controls"

        public void resetPopupControl()
        {
            txtCancelReason.Text = string.Empty;
            uiMessagePopup.hideMessage();
        }

        #endregion

        #region "Open cancel appointment popup"
        /// <summary>
        /// Open cancel appointment popup
        /// </summary>
        /// <remarks></remarks>

        public void openCancelAppointmentPopup()
        {
            this.resetPopupControl();

            MeSchedulingBO objMeSchedulingBO = objSession.MeSchedulingBO;
            int currentIndex = Convert.ToInt32(ViewState[ViewStateConstants.CurrentIndex]);
            int associatedAppointementsCount = 0;
            DataRow[] drAssociatedAppointements = objMeSchedulingBO.ArrangedTradesAppointmentsDt.Select("Row <> " + Convert.ToString(currentIndex));

            DataRow drAppointment = this.getCurrentIndexRowFromDataset(currentIndex);
            int appointmentId = Convert.ToInt32(drAppointment[ApplicationConstants.AppointmentIdColumn]);
            lblCancelledJsn.Text = appointmentId.ToString().PadLeft(6, '0');

            if (drAssociatedAppointements.Count() > 0)
            {
                DataTable dtAssociatedAppointments = drAssociatedAppointements.CopyToDataTable();
                grdAssociatedAppointments.DataSource = dtAssociatedAppointments;
                grdAssociatedAppointments.DataBind();
                pnlAssociatedAppointments.Visible = true;
                associatedAppointementsCount = drAssociatedAppointements.Count();
            }
            else
            {
                pnlAssociatedAppointments.Visible = false;
            }
            lblAssociatedAppointmentsCount.Text = Convert.ToString(associatedAppointementsCount);
            mdlPopUpCancelAppointment.Show();

        }

        #endregion

        #endregion

        #region "Get values from Query String"
        /// <summary>
        /// Get values from Query String
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public bool getvaluesFromSession()
        {
            bool validData = false;
            SelectedAttributeBO objSelectedAttributeBO = objSession.SelectedAttribute;

            if (objSession.SelectedAttribute != null)
            {
                ViewState.Add(ViewStateConstants.JournalId, objSelectedAttributeBO.JournalId);
                ViewState.Add(ViewStateConstants.AppointmentId, objSelectedAttributeBO.AppointmentId);
                validData = true;
            }
            else
            {
                uiMessage.showErrorMessage(UserMessageConstants.ProblemLoadingData);
            }
            return validData;

        }

        #endregion

        #region "Disable all controls"
        /// <summary>
        /// 
        /// </summary>
        /// <remarks></remarks>
        public void disableAllControls()
        {
            btnPrevious.Enabled = false;
            btnNext.Enabled = false;
            btnUpdateNotes.Enabled = false;
            btnCancelAppointment.Enabled = false;
            btnUpdateCustomerDetails.Enabled = false;
            txtCustAppointmentNotes.Attributes.Add("readonly", "readonly");
            txtJobSheetNotes.Attributes.Add("readonly", "readonly");
        }

        #endregion

        #region "Pre Populate Values"
        /// <summary>
        /// get set all appointment data
        /// </summary>
        /// <remarks></remarks>

        public void getSetAllAppointmentData()
        {
            //Removing pre existing Arranged Appointments Dataset
            MeSchedulingBO objMeSchedulingBO = new MeSchedulingBO();
            objMeSchedulingBO = objSession.MeSchedulingBO;
            objMeSchedulingBO.ArrangedTradesAppointmentsDt.Clear();

            int journalId = Convert.ToInt32(ViewState[ViewStateConstants.JournalId]);
            SchedulingBL objSchedulingBL = new SchedulingBL(new SchedulingRepo());
            DataSet resultDataset = new DataSet();
            objSchedulingBL.getArrangedAppointmentsDetail(ref resultDataset, journalId);

            if (resultDataset.Tables[0].Rows.Count == 0)
            {
                uiMessage.showErrorMessage(UserMessageConstants.NoRecordFound);
                disableAllControls();
            }
            else
            {
                //set all appointments of component's trade in session
                objMeSchedulingBO.ArrangedTradesAppointmentsDt = resultDataset.Tables[0].Copy();
                objMeSchedulingBO.PropertyInfoDt = resultDataset.Tables[1].Copy();
                //save the data in session 
                objSession.MeSchedulingBO = objMeSchedulingBO;
                this.populateSummaryFromSession();
            }

        }

        #endregion

        #region "Populate summary from session"
        /// <summary>
        /// Populate summary from session
        /// </summary>
        /// <remarks></remarks>
        public void populateSummaryFromSession()
        {
            MeSchedulingBO objMeSchedulingBO = new MeSchedulingBO();
            objMeSchedulingBO = objSession.MeSchedulingBO;
            DataTable dtAppointments = objMeSchedulingBO.ArrangedTradesAppointmentsDt;
            DataRow[] drAppointment = null;

            drAppointment = dtAppointments.Select("AppointmentId = '" + ViewState[ViewStateConstants.AppointmentId].ToString() + "'");

            if (drAppointment.Count() > 0)
            {
                int currentIndex = Convert.ToInt32(drAppointment[0][ApplicationConstants.JobsheetCurrentColumn]);
                int totalJobSheets = dtAppointments.Rows.Count;

                ViewState.Add(ViewStateConstants.CurrentIndex, currentIndex);
                ViewState.Add(ViewStateConstants.TotalJobsheets, totalJobSheets);
                this.setPreviousNextButtonStates(currentIndex);
                this.populateAppointmentInfo();
                this.populatePropertyInfo();

            }
            else
            {
                uiMessage.showErrorMessage(UserMessageConstants.InvalidJsn);
                disableAllControls();
            }

        }

        #endregion

        #region "Set previous next button states"

        private void setPreviousNextButtonStates(int updatedIndex)
        {
            int currentIndex = updatedIndex;
            int totalJobSheets = Convert.ToInt32(ViewState[ViewStateConstants.TotalJobsheets]);

            lblSheetNumber.Text = Convert.ToString(currentIndex);
            lblTotalSheets.Text = Convert.ToString(totalJobSheets);

            //Previous Button state

            if (updatedIndex - 1 < 1)
            {
                btnPrevious.Enabled = false;
            }
            else
            {
                btnPrevious.Enabled = true;
            }

            //Next Button state
            if (updatedIndex + 1 > totalJobSheets)
            {
                btnNext.Enabled = false;
            }
            else
            {
                btnNext.Enabled = true;
            }

        }
        #endregion

        #region "Populate Appointment Info"
        /// <summary>
        /// Populate Appointment Info
        /// </summary>
        /// <remarks></remarks>

        public void populateAppointmentInfo()
        {
            int currentIndex = Convert.ToInt32(ViewState[ViewStateConstants.CurrentIndex]);
            DataRow drAppointment = this.getCurrentIndexRowFromDataset(currentIndex);


            lblHeading.Text = objSession.SelectedAttribute.MsatType;
            lblWorkType.Text = objSession.SelectedAttribute.MsatType;
            lblJsnHeader.Text = Convert.ToString(drAppointment[ApplicationConstants.AppointmentIdColumn]).PadLeft(6, '0');
            lblItemName.Text = Convert.ToString(drAppointment[ApplicationConstants.ItemNameColumn]);
            lblTrade.Text = Convert.ToString(drAppointment[ApplicationConstants.TradesColumn]);

            //lblJsn.Text = Convert.ToString(drAppointment[ApplicationConstants.AppointmentIdColumn]).PadLeft(6, '0');
            lblStatus.Text = Convert.ToString(drAppointment[ApplicationConstants.InterimStatusColumn]);
            lblOperative.Text = Convert.ToString(drAppointment[ApplicationConstants.OperativeColumn]);

            lblDuration.Text = GeneralHelper.appendHourLabel(Convert.ToDouble(drAppointment[ApplicationConstants.DurationsColumn]));
            lblDurationTotal.Text = GeneralHelper.appendHourLabel(Convert.ToDouble(drAppointment[ApplicationConstants.TotalDurationColumn]));

            DateTime startDateTime = DateTime.ParseExact(drAppointment[ApplicationConstants.StartDateColumn] + " " + drAppointment[ApplicationConstants.StartTimeColumn], "dd/MM/yyyy HH:mm", CultureInfo.CreateSpecificCulture("en-UK"));
            DateTime endDateTime = DateTime.ParseExact(drAppointment[ApplicationConstants.EndDateColumn] + " " + drAppointment[ApplicationConstants.EndTimeColumn], "dd/MM/yyyy HH:mm", CultureInfo.CreateSpecificCulture("en-UK"));

            lblStartDate.Text = Convert.ToDateTime(startDateTime).ToString("HH:mm") + " " + GeneralHelper.getDateWithWeekdayFormat(startDateTime);
            lblEndDate.Text = Convert.ToDateTime(endDateTime).ToString("HH:mm") + " " + GeneralHelper.getDateWithWeekdayFormat(endDateTime);

            txtCustAppointmentNotes.Text = Convert.ToString(drAppointment[ApplicationConstants.CustomerNotesColumn]);
            txtJobSheetNotes.Text = Convert.ToString(drAppointment[ApplicationConstants.JobsheetNotesColumn]);

            lblMsatType.Text = Convert.ToString(drAppointment[ApplicationConstants.MsatTypeColumn]);

        }

        #endregion

        #region "Populate Property Info"
        /// <summary>
        /// Populate Property Info
        /// </summary>
        /// <remarks></remarks>

        public void populatePropertyInfo()
        {
            SelectedAttributeBO objSelectedAttributeBO = objSession.SelectedAttribute;
            int journalId = objSelectedAttributeBO.JournalId;

            if (journalId == ApplicationConstants.NoneValue)
            {
                uiMessage.showErrorMessage(UserMessageConstants.ProblemLoadingData);
                this.disableAllControls();
            }
            else
            {
                DataTable resultDt = new DataTable();
                MeSchedulingBO objMeSchedulingBO = new MeSchedulingBO();
                objMeSchedulingBO = objSession.MeSchedulingBO;
                resultDt = objMeSchedulingBO.PropertyInfoDt;

                if (resultDt.Rows.Count == 0)
                {
                    uiMessage.showErrorMessage(UserMessageConstants.InvalidJournalId);
                    this.disableAllControls();
                }
                else
                {
                    //Property Information
                    lblScheme.Text = Convert.ToString(resultDt.Rows[0]["SchemeName"]);
                    lblBlock.Text = Convert.ToString(resultDt.Rows[0]["BlockName"]);
                    lblAddress.Text = Convert.ToString(resultDt.Rows[0]["HOUSENUMBER"] + " " + resultDt.Rows[0]["ADDRESS1"] + " " + resultDt.Rows[0]["ADDRESS2"]);
                    lblTowncity.Text = Convert.ToString(resultDt.Rows[0]["TOWNCITY"]);
                    lblCounty.Text = Convert.ToString(resultDt.Rows[0]["COUNTY"]);
                    lblPostcode.Text = Convert.ToString(resultDt.Rows[0]["POSTCODE"]);

                    //Customer Information
                    hdnCustomerId.Value = Convert.ToString(resultDt.Rows[0]["CustomerId"]);
                    lblCustomerName.Text = Convert.ToString(resultDt.Rows[0]["TenantName"]);
                    lblCustomerTelephone.Text = Convert.ToString(resultDt.Rows[0]["Telephone"]);
                    lblCustomerMobile.Text = Convert.ToString(resultDt.Rows[0]["Mobile"]);
                    lblCustomerEmail.Text = Convert.ToString(resultDt.Rows[0]["Email"]);
                    int tenancyId = Convert.ToInt32(resultDt.Rows[0]["TenancyId"]);

                    if (tenancyId == -1)
                    {
                        btnUpdateCustomerDetails.Enabled = false;
                    }
                }

            }

        }

        #endregion

        #region "Get current index row from dataset"
        /// <summary>
        /// Get current index row from dataset
        /// </summary>
        /// <param name="currentIndex"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public DataRow getCurrentIndexRowFromDataset(int currentIndex)
        {

            MeSchedulingBO objMeSchedulingBO = new MeSchedulingBO();
            objMeSchedulingBO = objSession.MeSchedulingBO;
            DataTable dtAppointments = objMeSchedulingBO.ArrangedTradesAppointmentsDt;
            DataRow[] drAppointment = null;
            drAppointment = dtAppointments.Select("Row = " + Convert.ToString(currentIndex));
            return drAppointment[0];

        }

        #endregion

        #region "Navigate to M&E Servicing Works"
        /// <summary>
        /// Navigate to M&E Servicing Works
        /// </summary>
        /// <remarks></remarks>
        public void navigateToMeServicingWorks()
        {
            string navigationUrl = PathConstants.MeServicingPath + objSession.SelectedAttribute.MsatQueryStringPath; ;
            Response.Redirect(navigationUrl);
        }

        #endregion

        #region "Update Customer Address"
        /// <summary>
        /// Update Customer Address
        /// </summary>
        /// <remarks></remarks>

        public void updateAddress()
        {
            //Update Enable

            if (btnUpdateCustomerDetails.Text == ApplicationConstants.UpdateCustomerDetails)
            {
                txtCustomerTelephone.Text = lblCustomerTelephone.Text;
                lblCustomerTelephone.Visible = false;
                txtCustomerTelephone.Visible = true;

                txtCustomerMobile.Text = lblCustomerMobile.Text;
                lblCustomerMobile.Visible = false;
                txtCustomerMobile.Visible = true;

                txtCustomerEmail.Text = lblCustomerEmail.Text;
                lblCustomerEmail.Visible = false;
                txtCustomerEmail.Visible = true;

                btnUpdateCustomerDetails.Text = ApplicationConstants.SaveChanges;

            }
            else
            {
                //Save Changes

                if (lblCustomerTelephone.Text != txtCustomerTelephone.Text | lblCustomerMobile.Text != txtCustomerMobile.Text | lblCustomerEmail.Text != txtCustomerEmail.Text)
                {
                    //If Not isError Then
                    CustomerBL objCustomerBL = new CustomerBL(new CustomerRepo());
                    CustomerBO objCustomerBO = new CustomerBO();

                    objCustomerBO.CustomerId = Convert.ToInt32(hdnCustomerId.Value);
                    objCustomerBO.Telephone = txtCustomerTelephone.Text;
                    objCustomerBO.Mobile = txtCustomerMobile.Text;
                    objCustomerBO.Email = txtCustomerEmail.Text;

                    Validator<CustomerBO> objValidator = valFactory.CreateValidator<CustomerBO>();
                    ValidationResults results = objValidator.Validate(objCustomerBO);

                    if (results.IsValid)
                    {
                        objCustomerBL.updateAddress(objCustomerBO);
                        lblCustomerTelephone.Text = txtCustomerTelephone.Text;
                        lblCustomerMobile.Text = txtCustomerMobile.Text;
                        lblCustomerEmail.Text = txtCustomerEmail.Text;
                        txtCustomerTelephone.Visible = false;
                        lblCustomerTelephone.Visible = true;
                        txtCustomerMobile.Visible = false;
                        lblCustomerMobile.Visible = true;
                        txtCustomerEmail.Visible = false;
                        lblCustomerEmail.Visible = true;
                        btnUpdateCustomerDetails.Text = ApplicationConstants.UpdateCustomerDetails;
                        uiMessage.showInformationMessage(UserMessageConstants.UserSavedSuccessfuly);


                        DataTable resultDt = new DataTable();
                        MeSchedulingBO objMeSchedulingBO = new MeSchedulingBO();
                        objMeSchedulingBO = objSession.MeSchedulingBO;
                        resultDt = objMeSchedulingBO.PropertyInfoDt;

                        foreach (DataColumn col in resultDt.Columns)
                        {
                            col.ReadOnly = false;
                        }

                        if (resultDt.Rows.Count > 0)
                        {
                            resultDt.Rows[0]["Telephone"] = txtCustomerTelephone.Text;
                            resultDt.Rows[0]["Mobile"] = txtCustomerMobile.Text;
                            resultDt.Rows[0]["Email"] = txtCustomerEmail.Text;
                        }
                        resultDt.AcceptChanges();
                        objSession.MeSchedulingBO.PropertyInfoDt = resultDt;
                    }
                    else
                    {
                        dynamic message = string.Empty;
                        foreach (ValidationResult result in results)
                        {
                            message += result.Message;
                            break; 
                        }
                        uiMessage.showErrorMessage(message);
                    }



                }
                else
                {
                    txtCustomerTelephone.Visible = false;
                    lblCustomerTelephone.Visible = true;

                    txtCustomerMobile.Visible = false;
                    lblCustomerMobile.Visible = true;

                    txtCustomerEmail.Visible = false;
                    lblCustomerEmail.Visible = true;

                    btnUpdateCustomerDetails.Text = ApplicationConstants.UpdateCustomerDetails;

                }

            }

        }

        #endregion

        #region "Update notes"
        /// <summary>
        /// Update notes
        /// </summary>
        /// <remarks></remarks>

        public void updateNotes()
        {
            NotesBO objNotesBO = new NotesBO();
            objNotesBO.CustomerNotes = txtCustAppointmentNotes.Text;
            objNotesBO.AppointmentNotes = txtJobSheetNotes.Text;
            Validator<NotesBO> objValidator = valFactory.CreateValidator<NotesBO>();
            ValidationResults results = objValidator.Validate(objNotesBO);

            if (results.IsValid)
            {
                SchedulingBL objSchedulingBL = new SchedulingBL(new SchedulingRepo());
                int isSaved = 0;
                int currentIndex = Convert.ToInt32(ViewState[ViewStateConstants.CurrentIndex]);
                DataRow drAppointment = this.getCurrentIndexRowFromDataset(currentIndex);
                int appointmentId = Convert.ToInt32(drAppointment[ApplicationConstants.AppointmentIdColumn]);
                string customerNotes = txtCustAppointmentNotes.Text;
                string appointmentNotes = txtJobSheetNotes.Text;
                isSaved = objSchedulingBL.updateMeAppointmentsNotes(customerNotes, appointmentNotes, appointmentId);

                if (isSaved == ApplicationConstants.Saved)
                {
                    updateNotesInDataset(currentIndex);
                    uiMessage.showInformationMessage(UserMessageConstants.NotesUpdatedSuccessfully);
                }
                else
                {
                    uiMessage.showErrorMessage(UserMessageConstants.ProblemUpdatingNotes);
                }

            }
            else
            {
                uiMessage.showErrorMessage(UserMessageConstants.MaxLengthExceed);
            }


        }

        #endregion

        #region "Update notes in dataset"
        /// <summary>
        /// Update notes in dataset
        /// </summary>
        /// <remarks></remarks>

        public void updateNotesInDataset(int currentIndex)
        {
            MeSchedulingBO objMeSchedulingBO = new MeSchedulingBO();
            objMeSchedulingBO = objSession.MeSchedulingBO;
            DataTable dt = new DataTable();
            dt = objMeSchedulingBO.ArrangedTradesAppointmentsDt.Copy();

            foreach (DataColumn col in dt.Columns)
            {
                col.ReadOnly = false;
            }


            foreach (DataRow row in dt.Rows)
            {
                if (Convert.ToInt32(row["Row"]) == currentIndex)
                {
                    row[ApplicationConstants.CustomerNotesColumn] = txtCustAppointmentNotes.Text;
                    row[ApplicationConstants.JobsheetNotesColumn] = txtJobSheetNotes.Text;
                }
            }
            dt.AcceptChanges();
            objSession.MeSchedulingBO.ArrangedTradesAppointmentsDt = dt;
        }

        #endregion

        #endregion

    }
}