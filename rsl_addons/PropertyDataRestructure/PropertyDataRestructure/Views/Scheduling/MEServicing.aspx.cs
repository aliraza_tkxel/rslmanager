﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PropertyDataRestructure.Base;
using PDR_Utilities.Constants;
using PDR_Utilities.Helpers;
using PDR_BusinessObject.MeSearch;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_BusinessLogic.Scheduling;
using PDR_DataAccess.Scheduling;
using System.Data;

namespace PropertyDataRestructure.Views.Scheduling
{
    public partial class MEServicing : PageBase
    {

        #region Properties
        String msatType = String.Empty;
        String msatTypeValue = String.Empty;
        #endregion

        #region Events

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                uiMessage.hideMessage();
                getQueryString();

                if (ValidateQueryString())
                {

                    if (!IsPostBack)
                    {
                        populateAttributeDropDown();
                        if (Request.QueryString[PathConstants.ListingTab] == ApplicationConstants.AppointmentArrangedTab)
                        {
                            populateAppointmentArrangeList();
                        }
                        else
                        {
                            populateAppointmentToBeArrangeList();
                        }

                    }
                }

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessage.isError == true)
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region Link Button Appointment to be Arranged
        protected void lnkBtnAptbaTab_Click(object sender, EventArgs e)
        {
            populateAppointmentToBeArrangeList();
        }
        #endregion

        #region 56 Days Due date check changed
        protected void ckBoxDueWithIn56Days_CheckedChanged(object sender, EventArgs e)
        {
            if (MainView.ActiveViewIndex == 0)
            {
                populateAppointmentToBeArrangeList();
            }
            else if (MainView.ActiveViewIndex == 1)
            {
                populateAppointmentArrangeList();
            }

        }
        #endregion

        #region Link Button Appointment Arranged
        protected void lnkBtnAptTab_Click(object sender, EventArgs e)
        {
            populateAppointmentArrangeList();
        }
        #endregion

        #region Search Button Clicked
        protected void imgSearchbtn_Click(object sender, EventArgs e)
        {
            searchResults();
        }
        #endregion

        #endregion

        #region Function

        #region Populate Appointment To Be Arrange List
        public void populateAppointmentToBeArrangeList()
        {
            lnkBtnAptbaTab.CssClass = ApplicationConstants.TabClickedCssClass;
            lnkBtnAptTab.CssClass = ApplicationConstants.TabInitialCssClass;
            MainView.ActiveViewIndex = 0;

            MeSearchBO objMeSearchBO = new MeSearchBO();
            objMeSearchBO.SearchText = txtSearchBox.Text;
            objMeSearchBO.Check56Days = ckBoxDueWithIn56Days.Checked;
            objMeSearchBO.MsatType = msatTypeValue;
            objMeSearchBO.ItemName = ddlAttributes.SelectedValue; 
            if (Request.QueryString[PathConstants.SchemeId] != null)
            {
                objMeSearchBO.SchemeId = Convert.ToInt32(Request.QueryString[PathConstants.SchemeId]);
            }

            if (Request.QueryString[PathConstants.BlockId] != null)
            {
                objMeSearchBO.BlockId = Convert.ToInt32(Request.QueryString[PathConstants.BlockId]);
            }

            if (Request.QueryString[PathConstants.AttributeTypeId] != null)
            {
                objMeSearchBO.AttributeTypeId = Convert.ToInt32(Request.QueryString[PathConstants.AttributeTypeId]);
            }

            objSession.MeSearchBO = null;
            objSession.MeSearchBO = objMeSearchBO;
            ucAppointToBeArranged.loadData();
        }
        #endregion

        #region Populate Appointment Arrange List
        public void populateAppointmentArrangeList()
        {
            lnkBtnAptbaTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnAptTab.CssClass = ApplicationConstants.TabClickedCssClass;
            MainView.ActiveViewIndex = 1;

            MeSearchBO objMeSearchBO = new MeSearchBO();
            objMeSearchBO.SearchText = txtSearchBox.Text;
            objMeSearchBO.Check56Days = ckBoxDueWithIn56Days.Checked;
            objMeSearchBO.MsatType = msatTypeValue;
            objMeSearchBO.ItemName = ddlAttributes.SelectedValue; 
            if (Request.QueryString[PathConstants.SchemeId] != null)
            {
                objMeSearchBO.SchemeId = Convert.ToInt32(Request.QueryString[PathConstants.SchemeId]);
            }

            if (Request.QueryString[PathConstants.BlockId] != null)
            {
                objMeSearchBO.BlockId = Convert.ToInt32(Request.QueryString[PathConstants.BlockId]);
            }

            if (Request.QueryString[PathConstants.AttributeTypeId] != null)
            {
                objMeSearchBO.AttributeTypeId = Convert.ToInt32(Request.QueryString[PathConstants.AttributeTypeId]);
            }

            objSession.MeSearchBO = null;
            objSession.MeSearchBO = objMeSearchBO;
            ucAppointmentArranged.loadData();
        }
        #endregion

        #region Get Query String
        private void getQueryString()
        {

            if (Request.QueryString[PathConstants.MsatType] != null)
            {
                msatType = Request.QueryString[PathConstants.MsatType];
            }

        }
        #endregion

        #region Validate Query String
        private Boolean ValidateQueryString()
        {
            Boolean isValid = true;

            if (msatType.Equals(PathConstants.CyclicMaintenance))
            {
                msatTypeValue = ApplicationConstants.CyclicMaintenance;
                lblHeading.Text = ApplicationConstants.CyclicMaintenance;
            }
            else if (msatType.Equals(PathConstants.MEServicing))
            {
                msatTypeValue = ApplicationConstants.MEServicing;
                lblHeading.Text = ApplicationConstants.MEServicing;
            }
            else
            {
                uiMessage.showErrorMessage(UserMessageConstants.InvalidQueryString);
                disableControls();
                isValid = false;
            }

            return isValid;

        }
        #endregion

        #region Disable controls
        private void disableControls()
        {
            txtSearchBox.Enabled = true;
            lnkBtnAptbaTab.Enabled = false;
            lnkBtnAptTab.Enabled = false;
            ckBoxDueWithIn56Days.Enabled = false;
            imgSearchbtn.Enabled = false;
        }
        #endregion

        #region Search Results
        public void searchResults()
        {

            if (MainView.ActiveViewIndex == 0)
            {
                populateAppointmentToBeArrangeList();
            }
            else
            {
                populateAppointmentArrangeList();
            }

        }
        #endregion

        #region populate Attribute DropDown
        public void populateAttributeDropDown()
        {
            SchedulingBL objSchedulingBl = new SchedulingBL(new SchedulingRepo());
            DataSet resultDs = new DataSet();
            objSchedulingBl.getAllAttributes(ref resultDs);
            ddlAttributes.DataSource = resultDs;
            ddlAttributes.DataValueField = "ItemName";
            ddlAttributes.DataTextField = "ItemName";
            ddlAttributes.DataBind();
            ddlAttributes.Items.Insert(0, new ListItem("Select Attribute", "-1"));   

        }

        #endregion
        #endregion
    }
}