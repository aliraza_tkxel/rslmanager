﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ScheduleMEServicing.aspx.cs"
    MasterPageFile="~/MasterPage/Pdr.Master" Inherits="PropertyDataRestructure.Views.Scheduling.ScheduleMEServicing" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Styles/scheduling.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Site.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/default.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="updateProgressReportsArea" runat="server" DisplayAfter="10">
        <ProgressTemplate>
            <div class="loading-image">
                <img alt="Please Wait" src="../../Images/menu/progress.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="updPanelMiscWork" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="headingTitle">
                &nbsp &nbsp<b>Scheme: &nbsp<asp:Label ID="lblHeaderScheme" runat="server"></asp:Label>
                    &nbsp Block: &nbsp<asp:Label ID="lblHeaderBlock" runat="server"></asp:Label></b>
            </div>
            <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" />
            <div class="mainContainer" style="width: 98%;">
                <div class="leftContainer" id="leftContainer" runat="server">
                    <asp:Panel runat="server" ID="pnlAddMiscWorks">
                        <table style="width: 100%;" cellspacing="7" cellpadding="7">
                            <tr>
                                <td valign="top">
                                    <b>Works required:</b>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtWorksRequired" runat="server" TextMode="MultiLine" Height="100px"
                                        Width="100%" MaxLength="1000"></asp:TextBox>
                                    <asp:HiddenField ID="hdnJournalId" runat="server" />
                                    <asp:Button ID="btnHidden" OnClick="btnHidden_Click" runat="server" Text="" Style="display: none;" />
                                </td>
                            </tr>
                            <tr>
                                <td valign="top">
                                    <b>Appointment notes:</b>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtAppointmentNotes" runat="server" TextMode="MultiLine" Height="150px"
                                        Width="100%" MaxLength="1000"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b>Operative trade:</b>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlTrade" style="padding:5px;" Width="102%" runat="server">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b>Duration:</b>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlDuration" Width="147" style="padding:5px;" runat="server">
                                    </asp:DropDownList>
                                    <asp:Button ID="btnAdd" Text="Add trade" runat="server" OnClick="btnAdd_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 40%;" valign="top">
                                    &nbsp;
                                </td>
                                <td align="left" valign="top">
                                    <asp:UpdatePanel ID="updOperativesList" runat="server">
                                        <ContentTemplate>
                                            <asp:GridView ID="grdTradeOperative" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                                BorderStyle="Solid" CellPadding="10" CellSpacing="10" BorderWidth="1px" GridLines="None" BorderColor="#BBBBBB"  OnRowDeleting="grdTradeOperative_RowDeleting"
                                                Font-Bold="False" CssClass="component-trade-list">
                                                <Columns>
                                                    <asp:BoundField DataField="TradeName" HeaderText="Trade" ShowHeader="False" />
                                                    <asp:BoundField DataField="DurationString" HeaderText="Duration" ShowHeader="False" />
                                                    <asp:TemplateField ItemStyle-BorderStyle="Solid">
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="imgDelete" runat="server" CommandName="Delete"><img src="../../Images/cross2.png" alt="Delete Fault"  style="border:none; width:16px; " />  </asp:LinkButton>
                                                        </ItemTemplate>
                                                        <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                                        <ItemStyle Width="10px" BorderStyle="None" HorizontalAlign="Left" />
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <b>Start date:</b>
                                </td>
                                <td>
                                    <ajaxToolkit:CalendarExtender ID="calDate" runat="server" DaysModeTitleFormat="dd/MM/yyyy"
                                        PopupButtonID="imgCalDate" PopupPosition="Right" TargetControlID="txtDate" TodaysDateFormat="dd/MM/yyyy"
                                        Format="dd/MM/yyyy">
                                    </ajaxToolkit:CalendarExtender>
                                    <asp:TextBox ID="txtDate" Width="100%" runat="server" style="padding:5px;"></asp:TextBox>
                                    <ajaxToolkit:TextBoxWatermarkExtender ID="txtWaterCalDate" runat="server" TargetControlID="txtDate"
                                        WatermarkText="(Optional)" WatermarkCssClass="searchTextDefault">
                                    </ajaxToolkit:TextBoxWatermarkExtender>
                                    <asp:TextBox ID="txtHiddenDate" runat="server" Text="" Style="display: none;" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <asp:Button ID="btnBack" OnClick="btnBack_Click" runat="server" Text="Back to " />
                                </td>
                                <td style="text-align: right;">                                 
                                    <asp:Button ID="btnFindOperative" OnClick="btnFindMeOperative_Click" runat="server" Text="Find me an operative" />
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </div>
                <asp:Panel ID="pnlAvailableOperatives" runat="server" CssClass="rightContainer" Visible="false">
                </asp:Panel>
                <asp:Panel ID="pnlRefreshButton" runat="server" Visible="false">
                </asp:Panel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
