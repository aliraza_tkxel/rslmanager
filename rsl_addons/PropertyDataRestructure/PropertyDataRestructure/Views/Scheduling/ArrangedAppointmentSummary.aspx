﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ArrangedAppointmentSummary.aspx.cs"
    MasterPageFile="~/MasterPage/Pdr.Master" Inherits="PropertyDataRestructure.Views.Scheduling.ArrangedAppointmentSummary" %>

<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Styles/scheduling.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/default.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Site.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="updateProgressReportsArea" runat="server" DisplayAfter="10">
        <ProgressTemplate>
            <div class="loading-image">
                <img alt="Please Wait" src="../../Images/menu/progress.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="updPanelAppointment" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="600px" />
            <div class="headingTitle" style="width: 98%">
                <b>Schedule</b> 
                <asp:Label runat="server" ID="lblHeading" Font-Bold="true" style="margin-left:5px;" Text=""></asp:Label>
            </div>
            <div class="mainContainer">
                <div style="text-align: right;">
                    &nbsp;<asp:Button ID="btnPrevious" OnClick="btnPrevious_Click" runat="server" Text="&lt; Previous"
                        Width="100px" />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="btnNext" runat="server" OnClick="btnNext_Click" Text="Next >" Width="100px" /></div>
                <br />
                <asp:Panel ID="pnlHeader" Width="100%" runat="server">
                    <asp:Label ID="Label1" runat="server" CssClass="leftControl" Font-Bold="true" Text="    Appointment Summary"></asp:Label>
                    <asp:Label ID="lblTotalSheets" runat="server" CssClass="rightControl" Font-Bold="true"
                        Text=""></asp:Label>
                    <asp:Label ID="Label21" runat="server" CssClass="rightControl" Font-Bold="true" Text="of"></asp:Label>
                    <asp:Label ID="lblSheetNumber" runat="server" CssClass="rightControl" Font-Bold="true"
                        Text=""></asp:Label>
                    <asp:Label ID="Label33" runat="server" CssClass="rightControl" Font-Bold="true" Text="Job Sheet"></asp:Label>
                </asp:Panel>
                <br />
                <hr />
                <br />
                <div style="padding: 10px; border: 1px solid;">
                    <table>
                        <tr>
                            <td style="width: 20%;">
                                JSN: &nbsp &nbsp
                                <asp:Label ID="lblJsnHeader" runat="server" Text=""></asp:Label>
                            </td>
                            <td style="width: 66%;">
                                <asp:Label runat="server" ID="lblMsatType" Text=""></asp:Label>
                                Attribute:
                                <asp:Label ID="lblItemName" runat="server" Text=""></asp:Label>
                            </td>
                            <td style="width: 15%;">
                                Trade: &nbsp &nbsp
                                <asp:Label ID="lblTrade" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
                <br />
                <br />
                <asp:Panel ID="pnlAppointmentDetail" runat="server">
                    <div class="leftDiv">
                        <table style="width: 100%;" cellspacing="10" cellpadding="5">
                            <%--<tr>
                                <td class="labelTd">
                                    <asp:Label ID="Label22" runat="server" Text="JSN:"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblJsn" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>--%>
                            <tr>
                                <td class="labelTd">
                                    <asp:Label ID="Label2" runat="server" Text="Operative:"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblOperative" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="labelTd">
                                    <asp:Label ID="Label3" runat="server" Text="Duration:"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblDuration" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="labelTd">
                                    <asp:Label ID="Label4" runat="server" Text="Duration Total:"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblDurationTotal" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="leftDiv">
                        <table style="width: 100%;" cellspacing="10" cellpadding="5">
                            <tr>
                                <td class="labelTd">
                                    <asp:Label ID="Label23" runat="server" Text="Status:"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="labelTd">
                                    <asp:Label ID="Label6" runat="server" Text="Start Date:"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblStartDate" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="labelTd">
                                    <asp:Label ID="Label7" runat="server" Text="End Date:"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblEndDate" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
                <div style="clear: both">
                </div>
                <hr />
                <div style="clear: both">
                </div>
                <asp:Panel ID="pnlPropertyDetail" runat="server">
                    <div class="leftDiv">
                        <table style="width: 100%;" cellspacing="10" cellpadding="5">
                            <tr>
                                <td class="labelTd">
                                    <asp:Label ID="Label8" runat="server" Font-Bold="true" Text="Scheme:"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblScheme" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="labelTd">
                                    <asp:Label ID="Label24" runat="server" Font-Bold="true" Text="Block:"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblBlock" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="labelTd">
                                    <asp:Label ID="Label9" runat="server" Font-Bold="true" Text="Address:"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblAddress" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="labelTd">
                                    <asp:Label ID="Label10" runat="server" Text=""></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblTowncity" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="labelTd">
                                    <asp:Label ID="Label11" runat="server" Text=""></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblCounty" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="labelTd">
                                    <asp:Label ID="Label12" runat="server" Text=""></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblPostcode" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="leftDiv">
                        <table style="width: 100%;" cellspacing="10" cellpadding="5">
                            <tr>
                                <td class="labelTd">
                                    <asp:Label ID="Label13" runat="server" Text="Customer:"></asp:Label>
                                </td>
                                <td>
                                    <asp:HiddenField ID="hdnCustomerId" runat="server" />
                                    <asp:Label ID="lblCustomerName" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="labelTd">
                                    <asp:Label ID="Label14" runat="server" Text="Telephone:"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblCustomerTelephone" runat="server"></asp:Label>
                                    <asp:TextBox ID="txtCustomerTelephone" runat="server" Visible="False"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="labelTd">
                                    <asp:Label ID="Label15" runat="server" Text="Mobile:"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblCustomerMobile" runat="server"></asp:Label>
                                    <asp:TextBox ID="txtCustomerMobile" runat="server" Visible="False"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="labelTd">
                                    <asp:Label ID="Label16" runat="server" Text="Email:"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblCustomerEmail" runat="server"></asp:Label>
                                    <asp:TextBox ID="txtCustomerEmail" runat="server" Visible="False"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="labelTd">
                                    <asp:Label ID="Label17" runat="server" Text=""></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="Label18" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="clear: both">
                    </div>
                    <div style="text-align: right;">
                        <asp:Button ID="btnUpdateCustomerDetails" UseSubmitBehavior="false" OnClick="btnUpdateCustomerDetails_Click"
                            runat="server" Text="Update customer details" />
                    </div>
                </asp:Panel>
                <div style="clear: both">
                </div>
                <hr />
                <div style="clear: both">
                </div>
                <asp:Panel ID="pnlNotes" runat="server">
                    <div class="leftDiv">
                        <table style="width: 100%;" cellspacing="10" cellpadding="5">
                            <tr>
                                <td>
                                    <asp:Label ID="Label19" Font-Bold="true" runat="server" Text="Customer Appointment Notes:"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    If the customer has any preferred contact time,<br />
                                    or ways in which we could contact them,<br />
                                    please enter them in the box below:
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtCustAppointmentNotes" MaxLength="1000" runat="server" TextMode="MultiLine"
                                        Height="150px" Width="100%"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="leftDiv">
                        <table style="width: 100%;" cellspacing="10" cellpadding="5">
                            <tr>
                                <td>
                                    <asp:Label ID="Label20" Font-Bold="true" runat="server" Text="Job Sheet Notes:"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Please enter information specific to the
                                    <br />
                                    <asp:Label runat="server" ID="lblWorkType" Text=""></asp:Label> works in the box below:
                                    <br />
                                    &nbsp
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtJobSheetNotes" MaxLength="1000" runat="server" TextMode="MultiLine"
                                        Height="150px" Width="100%"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
                <div style="clear: both">
                </div>
                <div style="text-align: right;">
                    <asp:Button ID="btnUpdateNotes" runat="server" UseSubmitBehavior="false" OnClick="btnUpdateNotes_click" Text="Update Notes" />
                </div>
                <br />
                <div style="text-align: right; width: 100%;">
                    <asp:Button ID="btnBack" OnClick="btnBack_Click"  runat="server" Text="< Back" />
                    &nbsp &nbsp
                    <asp:Button ID="btnCancelAppointment" UseSubmitBehavior="false" OnClick="btnCancelAppointment_click" runat="server"
                        Text="Cancel Appointment" />
                </div>
            </div>
            <!-----------------------------------------------------Cancel Fault Popup-------------------------------------------------->
            <asp:Panel ID="pnlCancelAppointment" CssClass="left" runat="server" BackColor="White"
                Width="380px" Style="padding: 15px 5px; top: 32px; border: 1px solid black;">
                <div style="width: 100%; font-weight: bold; padding-left: 10px;">
                    <asp:Label ID="lblCancelledJsn" runat="server"> </asp:Label>
                    <b>: Cancel Appointment </b>
                </div>
                <div style="clear: both; height: 1px;">
                </div>
                <hr />
                <asp:ImageButton ID="imgBtnCloseCancelAppointments" runat="server" Style="position: absolute;
                    top: -12px; right: -12px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
                <uim:UIMessage ID="uiMessagePopup" runat="Server" Visible="false" width="400px" />
                <div style="margin-left: 10px; margin-right: 10px;">
                    <asp:Panel ID="pnlAssociatedAppointments" runat="server" Visible="false">
                        <p>
                            There are
                            <asp:Label ID="lblAssociatedAppointmentsCount" runat="server" Text="Label"></asp:Label>
                            JSNs associated with this appointment.
                            <br />
                            By cancelling this appointment, the associated JSNs appointments will also be cancelled.</p>
                        <br />
                        <div style="border: 1px solid black; padding: 5px; text-align: center;">
                            <asp:GridView ID="grdAssociatedAppointments" runat="server" AutoGenerateColumns="False"
                                ShowHeader="false" Width="90%" GridLines="None" CellPadding="3" CellSpacing="5">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("JSN") %>'></asp:Label>
                                            <asp:Label runat="server" Text=': '></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="26%" />
                                        <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="AppointmentDateFormat1" ItemStyle-Width="40%" />
                                    <asp:BoundField ItemStyle-Width="29%" DataField="OperativeShortName" />
                                </Columns>
                                <EmptyDataTemplate>
                                    No Records Found</EmptyDataTemplate>
                            </asp:GridView>
                        </div>
                    </asp:Panel>
                    <br />
                    <asp:Panel ID="pnlDescription" runat="server">
                        <asp:Label ID="lblDescription" Font-Bold="true" runat="server" Text="Reason:"></asp:Label>
                    </asp:Panel>
                    <br />
                    <asp:TextBox ID="txtCancelReason" runat="server" Height="130px" Width="350px" TextMode="MultiLine"></asp:TextBox>
                    <br />
                    <br />
                    <asp:Panel ID="pnlConfirmButton" runat="server" HorizontalAlign="Right">
                        <asp:Button ID="btnBackPopup" OnClick="btnBackPopup_click" runat="server" Text="< Back" />
                        <asp:Button ID="btnConfirmCancel" OnClick = "btnConfirmCancel_click" runat="server" Text="Confirm" />
                    </asp:Panel>
                    <asp:Panel ID="pnlCustomerMessage" runat="server" HorizontalAlign="Center" Visible="False">
                        <asp:Label ID="lblCancelAppointmentText" runat="server" Text="The Appointment(s) has been cancelled."></asp:Label><br />
                        <asp:LinkButton ID="lnkBtnClickHere" OnClick="lnkBtnClickHere_click" runat="server"
                            Visible="True">Click here</asp:LinkButton>
                        <br />
                        &nbsp;<asp:Label ID="lblCustomerText" runat="server" Text="to return to the scheduling."></asp:Label>
                    </asp:Panel>
                </div>
            </asp:Panel>
            <asp:ModalPopupExtender ID="mdlPopUpCancelAppointment" runat="server" DynamicServicePath=""
                Enabled="True" TargetControlID="lblDispCancelAppointmentPopup" PopupControlID="pnlCancelAppointment"
                DropShadow="true" CancelControlID="imgBtnCloseCancelAppointments" BackgroundCssClass="modalBackground"
                BehaviorID="mdlPopUpCancelAppointment">
            </asp:ModalPopupExtender>
            <asp:Label ID="lblDispCancelAppointmentPopup" runat="server"></asp:Label>
            <!-----------------------------------------------------End Add Add Appointment for Fault------------------------------------------>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
