﻿<%@ Page Language="C#" AutoEventWireup="true" Culture="en-GB" CodeBehind="MEServicing.aspx.cs"
    MasterPageFile="~/MasterPage/Pdr.Master" Inherits="PropertyDataRestructure.Views.Scheduling.MEServicing" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register TagPrefix="ucAppointmentArranged" TagName="AppointmentArranged" Src="~/UserControls/Scheduling/AppointmentArranged.ascx" %>
<%@ Register TagPrefix="ucAppointmentToBeArranged" TagName="AppointmentToBeArranged"
    Src="~/UserControls/Scheduling/AppointmentToBeArranged.ascx" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="updateProgressReportsArea" runat="server" DisplayAfter="10">
        <ProgressTemplate>
            <div class="loading-image">
                <img alt="Please Wait" src="../../Images/menu/progress.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="updPanelMEServicing" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="400px" />
            <p style="background-color: Black; height: 22px; text-align: justify; font-family: Tahoma;
                font-weight: bold; margin: 0 0 6px; font-size: 15px; padding: 8px;">
                <font color="white">
                    <asp:Label runat="server" ID="lblHeading" Text=""></asp:Label></font>
            </p>
            <div style="border: 1px solid black; height: 672px; padding: 5px;">
                <table style="width: 100%;">
                    <tr>
                        <td colspan="100%" style="padding-right: 0px !important; width: 100%;">
                            <div style="width: 100%; padding: 0; margin: 0;">
                                <asp:LinkButton ID="lnkBtnAptbaTab" OnClick="lnkBtnAptbaTab_Click" CssClass="TabInitial"
                                    runat="server" BorderStyle="Solid" BorderColor="Black">Appointment to be arranged: </asp:LinkButton>
                                <asp:LinkButton ID="lnkBtnAptTab" OnClick="lnkBtnAptTab_Click" CssClass="TabInitial"
                                    runat="server" BorderStyle="Solid" BorderColor="Black">Appointment arranged: </asp:LinkButton>
                                <div style="float: left; margin-top: 10px;">
                                    <asp:CheckBox ID="ckBoxDueWithIn56Days" runat="server" OnCheckedChanged="ckBoxDueWithIn56Days_CheckedChanged"
                                        AutoPostBack="true" Text="Due within 56 Days" Style="text-align: center" Checked="false" />
                                </div>
                                <div style="float: right; margin-top: 10px;">
                                    <asp:DropDownList runat="server" ID="ddlAttributes" Width="250px" AutoPostBack="true"
                                        OnSelectedIndexChanged="imgSearchbtn_Click" />
                                    <asp:RegularExpressionValidator ID="revSearchBox" runat="server" ControlToValidate="txtSearchBox"
                                        EnableClientScript="True" ErrorMessage="Special characters are not allowed" CssClass="Required"
                                        Display="Dynamic" ValidationExpression="^[A-Za-z0-9- ]+$" />
                                    <asp:TextBox ID="txtSearchBox" runat="server" AutoPostBack="false"></asp:TextBox>
                                    <ajax:TextBoxWatermarkExtender ID="txtSearchBoxWatermarkExtender" runat="server"
                                        TargetControlID="txtSearchBox" WatermarkText="Search">
                                    </ajax:TextBoxWatermarkExtender>
                                    <asp:ImageButton ID="imgSearchbtn" runat="server" ImageAlign="AbsMiddle" ImageUrl="~/Images/glass22x22.png"
                                        OnClick="imgSearchbtn_Click" BorderWidth="0px" />
                                </div>
                            </div>
                            <div style="border-bottom: 1px solid black; height: 0px; clear: both; margin-left: 2px;
                                width: 99.7%;">
                            </div>
                            <div style="clear: both; margin-bottom: 5px;">
                            </div>
                        </td>
                    </tr>
                </table>
                <asp:MultiView ID="MainView" runat="server" ActiveViewIndex="0">
                    <asp:View ID="View1" runat="server">
                        <ucAppointmentToBeArranged:AppointmentToBeArranged ID="ucAppointToBeArranged" runat="server"
                            MaxValue="10" MinValue="1" />
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <ucAppointmentArranged:AppointmentArranged ID="ucAppointmentArranged" runat="server"
                            MaxValue="10" MinValue="1" />
                    </asp:View>
                </asp:MultiView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
