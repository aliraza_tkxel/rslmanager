﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PropertyDataRestructure.Base;
using PDR_Utilities.Constants;
using PDR_Utilities.Helpers;
using PDR_DataAccess.Scheduling;
using PDR_BusinessLogic.Scheduling;
using System.Data;
using PDR_BusinessObject;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using PDR_BusinessObject.TradeAppointment;
using PDR_BusinessObject.MeAppointment;
using PDR_BusinessObject.MeOperativeSearch;
using PDR_BusinessObject.OperativeList;
using System.Web.UI.HtmlControls;

namespace PropertyDataRestructure.Views.Scheduling
{
    public partial class ScheduleMEServicing : PageBase
    {
        #region Properties
        public static TradeDurationDtBO tradeDurationDtBO;
        private string appointmentInfoSeparater = ":::";
        #endregion

        #region "Events"

        #region "Page Load Event"
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>

        protected void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                uiMessage.hideMessage();
                if (!IsPostBack)
                {
                    prepopulateValues();
                }
                else if (uiMessage.isError == false)
                {
                    //re bind the previously displayed data tables with grid, events with buttons 
                    //& redraw the controls for confirmed trade and appointment list (this is requirement of dynamic controls on post back)
                    reDrawConfirmedTradeAppointmentGrid();
                    //re bind the previously displayed data tables with grid, events with buttons 
                    //& redraw the controls for temporary trade and appointment list (this is requirement of dynamic controls on post back)                
                    reDrawTempTradeAppointmentGrid();

                }

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }

        #endregion

        #region "Button Back Click Event"
        /// <summary>
        /// Button Back Click Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                navigateToScheduleMeServicing();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessage.isError == true)
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #region "Find me operative button Event"
        /// <summary>
        /// Find me operative button Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>

        protected void btnFindMeOperative_Click(object sender, EventArgs e)
        {
            try
            {

                objSession.MeAppointmentBO = null;
                objSession.ConfirmMeAppointmetBlockList = null;
                pnlAvailableOperatives.Controls.Clear();
                findOperative();
                if (uiMessage.isError == false)
                {
                    disableControls();
                }

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessage.isError == true)
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }

        #endregion

        #region "btn Select Operative Click"
        protected void btnSelectOperative_Click(object sender, EventArgs e)
        {
            try
            {
                //This a sample string that will be received on this event
                //Sample ---- operative Id:::Operative Name:::Start Date:::End Date:::Trade Id:::Trade Name:::Trade Duration:::Component Trade Id:::Component Name:::BlockId
                //Values ---- 468:::Jade Burrell:::25/02/2013:::25/02/2013:::1:::Plumber:::2:::4:::Bathroom:::1
                string parameters = string.Empty;
                Button btnSelectOperative = (Button)sender;
                parameters = btnSelectOperative.CommandArgument;
                //save the selected appointment information in session
                this.saveSelectedAppointmentInfo(parameters);
                this.navigatetoMeJobsheet();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region "btn Refresh List Click"
        /// <summary>
        /// This function handles the refresh list button event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void btnRefreshList_Click(object sender, EventArgs e)
        {

            try
            {
                Button btnRefresh = (Button)sender;
                int selectedTradeAptBlockId = int.Parse(btnRefresh.CommandArgument);

                //this function 'll get the already temporary trade and appointment list then 
                //it 'll add five more operatives in the list of selected (trade/operative) block
                DateTime startDate = GeneralHelper.convertDateToUkDateFormat(DateTime.Now.ToString("dd/MM/yyyy"));
                if (!string.IsNullOrEmpty(txtDate.Text))
                {
                    startDate = GeneralHelper.convertDateToUkDateFormat(txtDate.Text);
                }
                this.refreshTheOpertivesList(selectedTradeAptBlockId, startDate);

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
                if (ApplicationConstants.DistanceError == false)
                {
                    uiMessage.showErrorMessage(UserMessageConstants.DistanceError);
                }
            }
        }
        #endregion

        #region "Hidden button event to populate dropdown list"
        /// <summary>
        /// Hidden button event to populate dropdown list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void btnHidden_Click(object sender, EventArgs e)
        {
            try
            {
                objSession.MeAppointmentBO = null;
                objSession.ConfirmMeAppointmetBlockList = null;
                objSession.TradesDataTable = null;
                objSession.TempMeAppointmentBOList = null;
                pnlAvailableOperatives.Controls.Clear();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessage.isError == true)
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        #endregion

        #region "btnAdd click event"

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                addRequiredOperatives();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessage.isError == true)
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region "grdTradeOperative row deleting event"
        /// <summary>
        /// grid row deleting event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void grdTradeOperative_RowDeleting(object sender, System.Web.UI.WebControls.GridViewDeleteEventArgs e)
        {

            try
            {

                if (tradeDurationDtBO.dt != null && tradeDurationDtBO.dt.Rows.Count > 0)
                {

                    tradeDurationDtBO.dt.Rows.RemoveAt(e.RowIndex);
                    tradeDurationDtBO.dt.AcceptChanges();

                    int order = 1;
                    foreach (DataRow dtr in tradeDurationDtBO.dt.Rows)
                    {
                        dtr["SortOrder"] = order;
                        order = order + 1;
                    }
                    tradeDurationDtBO.dt.AcceptChanges();
                    objSession.TradesDataTable = tradeDurationDtBO.dt;
                    grdTradeOperative.DataSource = objSession.TradesDataTable;
                    grdTradeOperative.DataBind();
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessage.isError == true)
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #endregion

        #region "Functions"


        #region "Navigate to ME Servicing"
        /// <summary>
        /// Navigate to ME Servicing
        /// </summary>
        /// <remarks></remarks>
        public void navigateToScheduleMeServicing()
        {
            Response.Redirect(PathConstants.MeServicingPath + objSession.SelectedAttribute.MsatQueryStringPath);
        }

        #endregion

        #region "Pre populate values"
        /// <summary>
        /// Pre populate values
        /// </summary>
        /// <remarks></remarks>
        public void prepopulateValues()
        {

            String src = Request.QueryString[PathConstants.Source];
            tradeDurationDtBO = new TradeDurationDtBO();

            lblHeaderBlock.Text = objSession.SelectedAttribute.BlockName;
            lblHeaderScheme.Text = objSession.SelectedAttribute.SchemeName;
            btnBack.Text = btnBack.Text + objSession.SelectedAttribute.MsatType;

            populateAllDropdownList();

            if (src == PathConstants.ToBeArrangedJobsheet)
            {              
                MeAppointmentBO objMeAppointmentBO = objSession.MeAppointmentBO;
                hdnJournalId.Value = Convert.ToString(objMeAppointmentBO.JournalId);
                txtAppointmentNotes.Text = objMeAppointmentBO.JournalNotes;
                txtWorksRequired.Text = objMeAppointmentBO.WorksRequired;
                txtDate.Text = objMeAppointmentBO.UserSelectedStartDate;

                DataTable dt = new DataTable();
                dt = objSession.TradesDataTable;
                grdTradeOperative.DataSource = dt;
                grdTradeOperative.DataBind();
               
                reDrawConfirmedTradeAppointmentGrid();
                redrawTempTradesAppointmentGridPartialScheduling();

                pnlAvailableOperatives.Visible = true;
                disableControls();
            }
            else
            {
                pnlAvailableOperatives.Visible = false;
                pnlAvailableOperatives.Controls.Clear();
                btnAdd.Enabled = true;
                btnFindOperative.Enabled = true;
                objSession.MeAppointmentBO = null;
                objSession.TradesDataTable = null;
            }

        }
        #endregion

        #region "Redraw Temp trades appointment grid after partial scheduling"
        /// <summary>
        /// Redraw Temp trades appointment grid partial scheduling
        /// </summary>
        /// <remarks></remarks>
        public void redrawTempTradesAppointmentGridPartialScheduling()
        {
            List<ConfirmMeAppointmentBO> confirmMeAptBoList = new List<ConfirmMeAppointmentBO>();
            confirmMeAptBoList = objSession.ConfirmMeAppointmetBlockList;
            if (objSession.TradesDataTable.Rows.Count != confirmMeAptBoList.Count())
            {
                DataSet resultDataset = new DataSet();
                resultDataset = this.getAvailableOperatives();
                TempAppointmentDtBO tempAptDtBo = new TempAppointmentDtBO();        
                this.populateAvailableOperativesList(resultDataset, tempAptDtBo);
            }
        }
        #endregion

        #region "Navigate to ME Job sheet"
        /// <summary>
        /// Navigate to ME Job sheet
        /// </summary>
        /// <remarks></remarks>
        public void navigatetoMeJobsheet()
        {
            Response.Redirect(PathConstants.ToBeArrangedAppointmentSummary + objSession.SelectedAttribute.MsatQueryStringPath);
        }
        #endregion

        #region "Add Me Works Functions"

        #region "Populate All drop down list"
        public void populateAllDropdownList()
        {
            populateTradeDropdownList();
            populateDurationDropdownList();
        }
        #endregion

        #region "Setup control properties"

        public void setupControlProperties()
        {
            //txtDate.Attributes.Add("readonly", "readonly")
        }

        #endregion

        #region "Find Operative"
        /// <summary>
        /// Find Operative
        /// </summary>
        /// <remarks></remarks>

        public void findOperative()
        {
            MeOperativeSearchBO objMeOperativeSearchBO = new MeOperativeSearchBO();
            objMeOperativeSearchBO = this.getMeOperativeSearchBO();
            Validator<MeOperativeSearchBO> objValidator = valFactory.CreateValidator<MeOperativeSearchBO>();
            ValidationResults results = objValidator.Validate(objMeOperativeSearchBO);

            string message = string.Empty;
            bool isValid = true;

            if (!results.IsValid)
            {
                foreach (ValidationResult result in results)
                {
                    message += result.Message;
                    break; // TODO: might not be correct. Was : Exit For
                }
                uiMessage.showErrorMessage(message);
                uiMessage.isError = true;

                isValid = false;


            }
            else if (!string.IsNullOrEmpty(txtDate.Text))
            {
                if (DateTime.ParseExact(txtDate.Text, "dd/MM/yyyy", null) < DateTime.Today)
                {
                    message += UserMessageConstants.SelectFutureDate + "<br />";
                    uiMessage.showErrorMessage(message);
                    uiMessage.isError = true;
                    isValid = false;
                }

            }
            else if (txtWorksRequired.Text.Trim().Length == 0)
            {
                message += UserMessageConstants.InvalidWorksRequired + "<br />";
                uiMessage.showErrorMessage(message);
                uiMessage.isError = true;
                isValid = false;
            }
            else if (txtWorksRequired.Text.Trim().Length > 1000)
            {
                message += UserMessageConstants.MaxLengthExceedWorksRequired + "<br />";
                uiMessage.showErrorMessage(message);
                uiMessage.isError = true;
                isValid = false;
            }

            if (isValid)
            {
                if (objSession.TradesDataTable.Rows.Count > 0)
                {
                    DataSet resultDataset = new DataSet();
                    MeAppointmentBO objMeAppointmentBO = new MeAppointmentBO();
                    string tempTradeIds = string.Empty;
                    foreach (DataRow dtr in tradeDurationDtBO.dt.Rows)
                    {
                        //make a comma separated string
                        tempTradeIds += dtr["TradeId"] + ",";
                    }
                    //remove the last comma
                    tempTradeIds = tempTradeIds.Remove(tempTradeIds.Length - 1, 1);
                    objMeAppointmentBO.TradeId = tempTradeIds;
                    objMeAppointmentBO.Duration = objMeOperativeSearchBO.Duration;
                    objMeAppointmentBO.StartDate = objMeOperativeSearchBO.StartDate;
                    this.setAppointmentInSession(objMeAppointmentBO);
                    resultDataset = this.getAvailableOperatives();
                    TempAppointmentDtBO tempAptDtBo = new TempAppointmentDtBO();
                    this.populateAvailableOperativesList(resultDataset, tempAptDtBo);
                }
                else
                {
                    pnlAvailableOperatives.Visible = false;
                    pnlAvailableOperatives.Controls.Clear();
                    uiMessage.isError = true;
                    uiMessage.messageText = UserMessageConstants.MeTradDuration;
                }
            }
            else
            {
                pnlAvailableOperatives.Visible = false;
                pnlAvailableOperatives.Controls.Clear();
            }

        }

        #endregion

        #region "Get Me Operative Search BO"
        /// <summary>
        /// Get Me Operative Search BO
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public MeOperativeSearchBO getMeOperativeSearchBO()
        {

            MeOperativeSearchBO objMeOperativeSearchBO = new MeOperativeSearchBO();
            objMeOperativeSearchBO.Worksrequired = txtWorksRequired.Text;
            objMeOperativeSearchBO.OperativeTradeId = Convert.ToInt32(ddlTrade.SelectedValue);
            objMeOperativeSearchBO.Duration = Convert.ToDouble(ddlDuration.SelectedValue);
            if (!string.IsNullOrEmpty(txtDate.Text))
            {
                objMeOperativeSearchBO.StartDate = GeneralHelper.convertDateToUkDateFormat(txtDate.Text);
            }
            else
            {
                objMeOperativeSearchBO.StartDate = GeneralHelper.convertDateToUkDateFormat(DateTime.Now.ToString("dd/MM/yyyy"));
            }

            return objMeOperativeSearchBO;
        }

        #endregion

        #endregion

        #region "Available Operatives Functions"

        #region "get Available Operatives"
        /// <summary>
        /// this function 'll get the available operatives based on following: 
        /// trade ids of component, property id, start date 
        /// </summary>    
        /// <returns></returns>
        /// <remarks></remarks>
        private DataSet getAvailableOperatives()
        {
            SchedulingBL schedulingBl = new SchedulingBL(new SchedulingRepo());
            DataSet resultDataSet = new DataSet();
            MeAppointmentBO objMeAppointmentBO = new MeAppointmentBO();
            objMeAppointmentBO = objSession.MeAppointmentBO;
            SelectedAttributeBO objSelectedAttributeBO = new SelectedAttributeBO();
            objSelectedAttributeBO = objSession.SelectedAttribute;

            //this function 'll get the available operatives based on following: trade ids of component, property id, start date 
            schedulingBl.getAvailableOperatives(ref resultDataSet, objMeAppointmentBO.TradeId.ToString(), objSelectedAttributeBO.MsatType, objMeAppointmentBO.StartDate);
            if (resultDataSet.Tables[0].Rows.Count == 0)
            {
                throw new Exception(UserMessageConstants.OperativesNotFound);
            }
            else
            {
                objSession.AvailableOperativesDs = resultDataSet;
            }
            return resultDataSet;
        }
        #endregion

        #region "Populate available operative"
        /// <summary>
        /// Populate available operative
        /// </summary>
        /// <remarks></remarks>

        public void populateAvailableOperativesList(DataSet availableOperativesDs, TempAppointmentDtBO tempAptDtBo)
        {
            SchedulingBL schedulingBl = new SchedulingBL(new SchedulingRepo());
            MeAppointmentBO objMeAppointmentBO = new MeAppointmentBO();
            objMeAppointmentBO = objSession.MeAppointmentBO;

            List<ConfirmMeAppointmentBO> confirmMeAptBoList = new List<ConfirmMeAppointmentBO>();
            confirmMeAptBoList = objSession.ConfirmMeAppointmetBlockList;

            DataTable tempTradeDt = new DataTable();
            tempTradeDt = objSession.TradesDataTable.Copy();
            foreach (ConfirmMeAppointmentBO item in confirmMeAptBoList)
            {
                DataRow dr = tempTradeDt.Select(String.Format("TRIM(TradeId) = '{0}'", item.TradeId)).First();
                if (dr != null)
                {
                    tempTradeDt.Rows.Remove(dr);
                }
            }
            tempTradeDt.AcceptChanges();


            List<TempMeAppointmentBO> tempMeAptBoList = new List<TempMeAppointmentBO>();
            //this function 'll process the component trades and 'll create the appointments against them
            tempMeAptBoList = schedulingBl.getAvailableMeAppointments(availableOperativesDs.Tables[ApplicationConstants.AvailableOperatives]
                , availableOperativesDs.Tables[ApplicationConstants.OperativesLeaves]
                , availableOperativesDs.Tables[ApplicationConstants.OperativesAppointments]
                , ref tempAptDtBo
                , objMeAppointmentBO.StartDate, tempTradeDt);


            pnlAvailableOperatives.Visible = true;
            pnlRefreshButton.Visible = true;
            //save list in session 
            objSession.TempMeAppointmentBOList = tempMeAptBoList;
            this.bindTempTradesAppointment(tempMeAptBoList);
        }

        #endregion

        #region "bind Temp Trade Appointments"
        /// <summary>
        /// This function 'll bind the data with temporary trade and appointments block
        /// </summary>
        /// <param name="tempMeAptBoList"></param>
        /// <remarks></remarks>
        private void bindTempTradesAppointment(List<TempMeAppointmentBO> tempMeAptBoList)
        {
            if (tempMeAptBoList == null)
            {
                // Me.unHideBackButton()
            }
            else
            {
                //panel control 
                Panel panelContainer = default(Panel);
                // We are considering trade and its respective operative list as block so 
                //these variables 'll be used to recognize each block when some button/check box 'll be clicked e.g refresh list button
                int uniqueControlId = 0;
                string uniqueControlIdString = uniqueControlId.ToString();

                //bind this above data with grid view             
                foreach (TempMeAppointmentBO item in tempMeAptBoList)
                {
                    //create container for every block of temporary Trades and appointment 
                    panelContainer = this.createContainer(uniqueControlIdString);
                    //Create object of Trade grid
                    HtmlGenericControl lblDuration = new HtmlGenericControl("div");
                    lblDuration.InnerText = "Duration: " + item.DurationString;
                    lblDuration.Attributes.Add("class", "tempTrades");

                    HtmlGenericControl lblTrade = new HtmlGenericControl("div");
                    lblTrade.InnerText = "Trade: " + item.Trade;
                    lblTrade.Attributes.Add("class", "tempTrades");


                    panelContainer.Controls.Add(lblDuration);
                    panelContainer.Controls.Add(lblTrade);

                    //Create object of appointment grid
                    GridView tempAptGrid = new GridView();
                    //Add the GridView control into the div control/panel
                    tempAptGrid = this.createTempAppointmentGrids(tempAptGrid, uniqueControlIdString, item);
                    panelContainer.Controls.Add(tempAptGrid);

                    //Add this panel container to div page container
                    pnlAvailableOperatives.Controls.Add(panelContainer);

                    //this variable 'll be used to recognize each control
                    uniqueControlId = uniqueControlId + 1;
                    uniqueControlIdString = uniqueControlId.ToString();
                }
            }
        }
        #endregion

        #region "add More Operatives In List"
        /// <summary>
        /// This function 'll add more operatives available appointment slots in the existing list
        /// </summary>
        /// <remarks></remarks>
        private void addMoreOperativesInList()
        {
            DataSet resultDataset = new DataSet();
            resultDataset = objSession.AvailableOperativesDs;
            TempAppointmentDtBO tempAptDtBo = new TempAppointmentDtBO();
            tempAptDtBo = objSession.MeAvailableOperativesAppointments;
            this.populateAvailableOperativesList(resultDataset, tempAptDtBo);
        }
        #endregion

        #region "create Container"
        /// <summary>
        /// This function creates the html container for every block of trade and appointment 
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        private Panel createContainer(string uniqueBlockId)
        {
            Panel divContainer = new Panel();
            divContainer.ID = "divContainer" + uniqueBlockId;
            divContainer.CssClass = "trade-appointment-block";
            return divContainer;
        }
        #endregion

        #region "Set Appointment in Session"
        /// <summary>
        /// Set Appointment in Session
        /// </summary>
        /// <remarks></remarks>
        public void setAppointmentInSession(MeAppointmentBO objMeAppointmentBO)
        {
            objSession.MeAppointmentBO = objMeAppointmentBO;
        }

        #endregion

        #region "Display Msg In Empty Grid"
        private void displayMsgInEmptyGrid(ref GridView grid, ref TempMeAppointmentBO tempTradeAptBo, string msg)
        {
            //create the new empty row in data table to display the message
            tempTradeAptBo.tempAppointmentDtBo.dt.Rows.Add(tempTradeAptBo.tempAppointmentDtBo.dt.NewRow());
            grid.DataSource = tempTradeAptBo.tempAppointmentDtBo.dt;
            grid.DataBind();

            //clear the empty row so it should not create problem in re-drawing the controls 
            //after post back
            tempTradeAptBo.tempAppointmentDtBo.dt.Clear();
            //create label to display message
            HtmlGenericControl lblmsg = new HtmlGenericControl("div");
            lblmsg.InnerText = msg;
            lblmsg.Attributes.Add("class", "lblmessage");
            //merge the grid cells so that we can display the message with header
            int columncount = grid.Rows[0].Cells.Count;
            grid.Rows[0].Cells.Clear();
            grid.Rows[0].Cells.Add(new TableCell());
            grid.Rows[0].Cells[0].ColumnSpan = columncount;
            grid.Rows[0].Cells[0].Controls.Add(lblmsg);
        }
        #endregion

        #region "create Temp Appointment Grids"
        /// <summary>
        /// This function 'll create the temporary appointment grid on the basis of unique block id and on data of TempTradeAppointmentBO
        /// </summary>
        /// <param name="uniqueBlockId"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        private GridView createTempAppointmentGrids(GridView tempAptGrid, string uniqueBlockId, TempMeAppointmentBO item)
        {
            bool isEmptyGrid = false;

            tempAptGrid.ID = ApplicationConstants.TempAppointmentGridControlId + uniqueBlockId.ToString();
            tempAptGrid.ShowFooter = true;
            tempAptGrid.EnableViewState = true;
            tempAptGrid.CssClass = "grid_appointment available-apptmts-optable";
            tempAptGrid.HeaderStyle.CssClass = "available-apptmts-Headerrow";
            //Check if , are there any appointments or not .If not then display message
            if (item.tempAppointmentDtBo.dt.Rows.Count == 0)
            {
                this.displayMsgInEmptyGrid(ref tempAptGrid, ref item, UserMessageConstants.NoOperativesExistWithInTime);
                isEmptyGrid = true;
            }
            else
            {
                tempAptGrid.DataSource = item.tempAppointmentDtBo.dt;
                tempAptGrid.DataBind();
                //add row span on footer and add back button, refresh list button and view calendar button
                tempAptGrid.FooterRow.Cells[0].Controls.Add(createAppointmentFooterControls(uniqueBlockId));
            }

            //after adding the buttons add col span to footer so that buttons should appear according to design
            if (tempAptGrid.Rows.Count > 0)
            {
                int m = tempAptGrid.FooterRow.Cells.Count;
                for (int i = m - 1; i >= 1; i += -1)
                {
                    tempAptGrid.FooterRow.Cells.RemoveAt(i);
                }
                tempAptGrid.FooterRow.Cells[0].ColumnSpan = 4;
                //4 is the number of visible columns to span.
            }

            //create header of appointment grid and add start date calendar in it
            TableCell tradAptGridHeaderCell = tempAptGrid.HeaderRow.Cells[3];
            //  tradAptGridHeaderCell.Controls.Add(Me.createAppointmentHeaderControls(uniqueBlockId.ToString(), item.StartSelectedDate))

            //declare the variables that 'll store the information required to bind with select button in grid                
            string operativeId = string.Empty;
            string operativeName = string.Empty;
            string startDateString = string.Empty;
            string endDateString = string.Empty;
            int dtCounter = 0;
            string completeAppointmentInfo = string.Empty;

            //remove the header of unwanted columns
            tempAptGrid.HeaderRow.Cells[2].Visible = false;
            tempAptGrid.HeaderRow.Cells[4].Visible = false;
            tempAptGrid.HeaderRow.Cells[5].Visible = false;
            tempAptGrid.HeaderRow.Cells[6].Visible = false;
            tempAptGrid.HeaderRow.Cells[7].Visible = false;
            tempAptGrid.HeaderRow.Cells[8].Visible = false;
            //if grid is not empty then remove the cells and also create select button and set command argument with that
            if (isEmptyGrid == false)
            {
                //run the loop on grid view rows to remove the cells from each row 
                //and also to add the select button in each row
                foreach (GridViewRow gvr in tempAptGrid.Rows)
                {
                    gvr.Cells[2].Visible = false;
                    gvr.Cells[4].Visible = false;
                    gvr.Cells[5].Visible = false;
                    gvr.Cells[6].Visible = false;
                    gvr.Cells[7].Visible = false;
                    gvr.Cells[8].Visible = false;
                    //get the information that should be set with select button                                             
                    operativeId = Convert.ToString(item.tempAppointmentDtBo.dt.Rows[dtCounter][TempAppointmentDtBO.operativeIdColName]);
                    operativeName = Convert.ToString(item.tempAppointmentDtBo.dt.Rows[dtCounter][TempAppointmentDtBO.operativeColName]);
                    startDateString = Convert.ToString(item.tempAppointmentDtBo.dt.Rows[dtCounter][TempAppointmentDtBO.startDateStringColName]);
                    endDateString = Convert.ToString(item.tempAppointmentDtBo.dt.Rows[dtCounter][TempAppointmentDtBO.endDateStringColName]);
                    dtCounter += 1;

                    //save this operative and time slot information in a string, after that it 'll be bind with select button
                    completeAppointmentInfo = operativeId.ToString() + this.appointmentInfoSeparater + operativeName + this.appointmentInfoSeparater + startDateString + this.appointmentInfoSeparater + endDateString + this.appointmentInfoSeparater + item.TradeId.ToString() + this.appointmentInfoSeparater + item.Trade + this.appointmentInfoSeparater + item.Duration + this.appointmentInfoSeparater + uniqueBlockId;

                    //Add the select button 
                    Button btnSelectOperative = new Button();
                    btnSelectOperative.ID = "btnSelectOperative" + uniqueBlockId.ToString();
                    btnSelectOperative.Text = "Select";
                    btnSelectOperative.CssClass = "row-button";
                    btnSelectOperative.CommandArgument = completeAppointmentInfo;
                    btnSelectOperative.Click += btnSelectOperative_Click;
                    gvr.Cells[3].Controls.Add(btnSelectOperative);
                }
            }
            return tempAptGrid;
        }
        #endregion

        #region "refresh The Operatives List"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="selectedTradeAptBlockId"></param>    
        /// <param name="startAgain">This variable 'll identify either appointment slots should be recreated from the selected start date or not</param>
        /// <remarks></remarks>
        private void refreshTheOpertivesList(int selectedTradeAptBlockId, System.DateTime startDate, bool startAgain = false)
        {
            List<TempMeAppointmentBO> tempMeAptBoList = new List<TempMeAppointmentBO>();
            tempMeAptBoList = objSession.TempMeAppointmentBOList;
            //get the specific object from the list of trade& appointments
            TempMeAppointmentBO tempMeAptBo = new TempMeAppointmentBO();
            tempMeAptBo = tempMeAptBoList[selectedTradeAptBlockId];
            //This function 'll do multiple tasks. It filters operatives and creates appointment slots based on leaves, appointments and due date
            SchedulingBL schedulingBl = new SchedulingBL(new SchedulingRepo());
            //get the available operatives, their leaves, appointments and selected start date
            DataSet availableOperativesDs = new DataSet();
            availableOperativesDs = objSession.AvailableOperativesDs;
            schedulingBl.addMoreTempMeAppointments(availableOperativesDs.Tables[ApplicationConstants.AvailableOperatives]
                , availableOperativesDs.Tables[ApplicationConstants.OperativesLeaves]
                , availableOperativesDs.Tables[ApplicationConstants.OperativesAppointments]
                , ref tempMeAptBo, startDate, startAgain);
            //save in temporary trade appointment list
            tempMeAptBoList[selectedTradeAptBlockId] = tempMeAptBo;

            //'get the instance of grid view control
            GridView grdTempAppointments = (GridView)pnlAvailableOperatives.FindControl("grdTempAppointments" + selectedTradeAptBlockId.ToString());
            this.createTempAppointmentGrids(grdTempAppointments, Convert.ToString(selectedTradeAptBlockId), tempMeAptBo);
        }
        #endregion

        #region "create Appointment Footer Controls"
        /// <summary>
        /// This function 'll create the footer buttons of appointment grid
        /// </summary>
        /// <param name="uniqueBlockId"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        private Panel createAppointmentFooterControls(string uniqueBlockId)
        {
            Panel buttonsContainer = new Panel();

            Button btnRefreshList = new Button();


            //set the refresh list button 
            btnRefreshList.Text = "Refresh List";
            btnRefreshList.CommandArgument = uniqueBlockId;
            btnRefreshList.Click += btnRefreshList_Click;


            //set the css for div/panel that 'll contain all buttons
            buttonsContainer.CssClass = "trade-appointment-buttons";
            //add all buttons to container 
            buttonsContainer.Controls.Add(btnRefreshList);
            return buttonsContainer;
        }
        #endregion

        #region "reDraw Temp Trade Appointment Grid"
        /// <summary>
        /// re bind the previously displayed datatables with grid, events with buttons then
        /// redraw the controls for temporary trade and appointment list
        /// </summary>
        /// <remarks></remarks>
        private void reDrawTempTradeAppointmentGrid()
        {
            //re bind the previously displayed data tables with grid, events with buttons 
            //& redraw the controls for temporary trade and appointment list
            List<TempMeAppointmentBO> tempTradeAptList = new List<TempMeAppointmentBO>();
            tempTradeAptList = objSession.TempMeAppointmentBOList;
            this.bindTempTradesAppointment(tempTradeAptList);
        }
        #endregion

        #region "clear Temp And Cofirm Block From Session"
        /// <summary>
        /// we are saving trades and appointmnet in session. In order to make sure that user should not get the old data 
        /// we are removing this from session (if old data exists)
        /// </summary>
        /// <remarks></remarks>
        private void clearTempAndCofirmBlockFromSession()
        {
            //removing the temporary appointments from session
            objSession.TempMeAppointmentBOList = null;

        }
        #endregion

        #region "save Selected Appointment Info"
        /// <summary>
        /// This function 'll save the selected appointment information in session
        /// </summary>
        /// <param name="parameters"></param>
        /// <remarks></remarks>

        private void saveSelectedAppointmentInfo(string parameters)
        {
            MeAppointmentBO objMeAppointmentBO = new MeAppointmentBO();
            string[] separater = { this.appointmentInfoSeparater };
            string[] appointmentParameters = parameters.Split(separater, StringSplitOptions.RemoveEmptyEntries);
            objMeAppointmentBO.OperativeId = int.Parse(appointmentParameters[0]);
            objMeAppointmentBO.OperativeName = appointmentParameters[1];
            objMeAppointmentBO.StartDate = Convert.ToDateTime(appointmentParameters[2]);
            objMeAppointmentBO.EndDate = Convert.ToDateTime(appointmentParameters[3]);
            objMeAppointmentBO.TradeId = appointmentParameters[4];
            objMeAppointmentBO.TradeName = appointmentParameters[5];
            objMeAppointmentBO.Duration = Convert.ToDouble(appointmentParameters[6]);
            objMeAppointmentBO.JournalId = objSession.SelectedAttribute.JournalId;
            objMeAppointmentBO.AttributeName = objSession.SelectedAttribute.AttributeName;
            objMeAppointmentBO.JournalNotes = txtAppointmentNotes.Text;
            objMeAppointmentBO.WorksRequired = txtWorksRequired.Text;
            objMeAppointmentBO.AppointmentNotes = txtWorksRequired.Text;
            objMeAppointmentBO.CustomerNotes = txtAppointmentNotes.Text;
            objMeAppointmentBO.MsatType = objSession.SelectedAttribute.MsatType;
            objMeAppointmentBO.UserSelectedStartDate = txtDate.Text;
            objMeAppointmentBO.Index = Convert.ToInt32(appointmentParameters[7]) + 1;
            this.setAppointmentInSession(objMeAppointmentBO);
        }
        #endregion

        #region "reDraw Confirmed Trade Appointment Grid"
        /// <summary>
        /// re bind the previously displayed data tables with grid, events with buttons then redraw the controls of confirmed trades and appointment list''' 
        /// </summary>
        /// <remarks></remarks>
        private void reDrawConfirmedTradeAppointmentGrid()
        {
            //re bind the previously displayed data tables with grid, events with buttons 
            //& redraw the controls for confirmed trades and appointment list
            List<ConfirmMeAppointmentBO> confirmMeAptBoList = new List<ConfirmMeAppointmentBO>();
            confirmMeAptBoList = objSession.ConfirmMeAppointmetBlockList;
            this.bindConfirmMeAppointments(confirmMeAptBoList);
        }
        #endregion

        #region "bind Confirm Trades Appointments"
        private void bindConfirmMeAppointments(List<ConfirmMeAppointmentBO> confirmMeAptBoList)
        {

            if (confirmMeAptBoList.Count() > 0)
            {
                int counter = 0;
                string uniqueBlockId = string.Empty;
                //panel control 
                Panel panelContainer = default(Panel);
                //bind this above data with grid view             
                foreach (ConfirmMeAppointmentBO item in confirmMeAptBoList)
                {
                    uniqueBlockId = ApplicationConstants.ConfirmedAppointementGridsIdConstant + counter.ToString();
                    //create container for every block of confirmed trade and appointment 
                    panelContainer = this.createContainer(uniqueBlockId);
                    //Create object of Trade grid
                    HtmlGenericControl lblDuration = new HtmlGenericControl("div");
                    lblDuration.InnerText = "Duration: " + item.DurationString;
                    lblDuration.Attributes.Add("class", "tempTrades");

                    HtmlGenericControl lblTrade = new HtmlGenericControl("div");
                    lblTrade.InnerText = "Trade: " + item.Trade;
                    lblTrade.Attributes.Add("class", "tempTrades");


                    panelContainer.Controls.Add(lblDuration);
                    panelContainer.Controls.Add(lblTrade);
                    //Add the GridView control object dynamically into the parent control
                    //Dim confirmedTradeGrid As New GridView()
                    //panelContainer.Controls.Add(createConfirmedTradeGrids(confirmedTradeGrid, uniqueBlockId, item))

                    //Add the GridView control object dynamically into the div control
                    //Create object of appointment grid
                    GridView confirmedAptGrid = new GridView();
                    panelContainer.Controls.Add(createConfirmedAppointmentGrids(confirmedAptGrid, uniqueBlockId, item));
                    //Add this panel container to div page container
                    pnlAvailableOperatives.Controls.Add(panelContainer);
                    counter = counter + 1;
                }
            }

        }
        #endregion

        #region "Create Confirmed Appointment Grids"
        /// <summary>
        /// This function creates the grid control containing confirmed appointments
        /// </summary>
        /// <param name="confirmedAptGrid"></param>
        /// <param name="uniqueBlockId"></param>
        /// <param name="confirmMeAptBo"></param>
        /// ''' <param name="hideRearrnageButton"> 'this value 'll always be false excetp when all appointments 'll have "Arranged" status</param>
        /// <returns></returns>
        /// <remarks></remarks>
        private GridView createConfirmedAppointmentGrids(GridView confirmedAptGrid, string uniqueBlockId, ConfirmMeAppointmentBO confirmMeAptBo, bool hideRearrnageButton = false)
        {

            confirmedAptGrid.ID = ApplicationConstants.ConfirmAppointmentGridControlId + uniqueBlockId;
            confirmedAptGrid.EnableViewState = true;
            confirmedAptGrid.CssClass = "grid_appointment available-apptmts-optable";
            confirmedAptGrid.HeaderStyle.CssClass = "available-apptmts-frow";
            confirmedAptGrid.DataSource = confirmMeAptBo.tempAppointmentDtBo.dt;
            confirmedAptGrid.DataBind();
            //hide the unwanted columns from the grid
            confirmedAptGrid.HeaderRow.Cells[3].Visible = false;
            confirmedAptGrid.HeaderRow.Cells[4].Visible = false;
            confirmedAptGrid.HeaderRow.Cells[5].Visible = false;
            confirmedAptGrid.HeaderRow.Cells[6].Visible = false;
            confirmedAptGrid.HeaderRow.Cells[7].Visible = false;
            confirmedAptGrid.HeaderRow.Cells[8].Visible = false;
            GridViewRow gvr = confirmedAptGrid.Rows[0];
            gvr.Cells[3].Visible = false;
            gvr.Cells[4].Visible = false;
            gvr.Cells[5].Visible = false;
            gvr.Cells[6].Visible = false;
            gvr.Cells[7].Visible = false;
            gvr.Cells[8].Visible = false;
            return confirmedAptGrid;
        }
        #endregion

        #region "Create Confirmed Trade Grids"
        /// <summary>
        /// This function 'll create the grid control containing component and trade information of of confirmed appointments
        /// </summary>
        /// <param name="confirmedTradeGrid"></param>
        /// <param name="uniqueControlId"></param>
        /// <param name="confirmTradeAptBo"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        private object createConfirmedTradeGrids(GridView confirmedTradeGrid, string uniqueControlId, ConfirmMeAppointmentBO confirmTradeAptBo)
        {
            //Create object of trade grid
            confirmedTradeGrid.ID = ApplicationConstants.ConfirmTradeGridControlId + uniqueControlId;
            confirmedTradeGrid.EnableViewState = true;
            confirmedTradeGrid.CssClass = "grid_temp_trade";
            confirmedTradeGrid.HeaderStyle.CssClass = "available-apptmts-frow";
            confirmedTradeGrid.DataSource = confirmTradeAptBo.tempAppointmentDtBo.dt;
            confirmedTradeGrid.DataBind();

            return confirmedTradeGrid;
        }
        #endregion

        #endregion

        #region "Add Trade to Trade Grid "
        /// <summary>
        /// Add Trade to Trade Grid
        /// </summary>
        /// <remarks></remarks>
        private void addRequiredOperatives()
        {
            DataTable dt = new DataTable();

            OperativeListBO objOperativeListBO = new OperativeListBO();
            objOperativeListBO.TradeId = Convert.ToInt32(ddlTrade.SelectedValue);
            objOperativeListBO.Duration = Convert.ToDecimal(ddlDuration.SelectedValue);
            objOperativeListBO.SOrder = grdTradeOperative.Rows.Count + 1;

            Validator<OperativeListBO> objValidator = valFactory.CreateValidator<OperativeListBO>();
            ValidationResults results = objValidator.Validate(objOperativeListBO);

            if (results.IsValid)
            {
                if (objSession.TradesDataTable.Rows.Count > 0)
                {
                    dt = objSession.TradesDataTable;
                }
                else
                {
                    tradeDurationDtBO = new TradeDurationDtBO();
                    dt = tradeDurationDtBO.dt;
                }

                //Create a new row

                if (!string.IsNullOrEmpty(ddlTrade.SelectedValue) & Convert.ToInt32(ddlTrade.SelectedValue) != -1)
                {
                    tradeDurationDtBO.tradeId = ddlTrade.SelectedValue;
                    tradeDurationDtBO.tradeName = ddlTrade.SelectedItem.Text;
                    tradeDurationDtBO.duration = ddlDuration.SelectedValue;
                    tradeDurationDtBO.durationString = ddlDuration.SelectedItem.Text;
                    tradeDurationDtBO.sOrder = Convert.ToString(dt.Rows.Count + 1);
                }

                var operativeResult = (from ps in dt.AsEnumerable()
                                       where ps["TradeId"].ToString() == ddlTrade.SelectedValue
                                       select ps);

                //Start - Check if Trade is Already in the Trades Table/Grid If yes Show Message else Add
                if (operativeResult.Count() > 0)
                {
                    string ErrorMesg = UserMessageConstants.TradeAlreadyAdded;
                    //"The Trade has already been added."
                    uiMessage.showErrorMessage(ErrorMesg);
                }
                else
                {
                    uiMessage.hideMessage();
                    // dt.Rows.Add(dr)
                    tradeDurationDtBO.addNewDataRow();

                    objSession.TradesDataTable = dt;
                    grdTradeOperative.DataSource = dt;
                    grdTradeOperative.DataBind();
                    //End - Check if Trade is already in Trades from database.
                }
            }
            else
            {
                dynamic message = string.Empty;
                foreach (ValidationResult result in results)
                {
                    message += result.Message;

                }
                uiMessage.isError = true;
                uiMessage.messageText = message;
            }
        }

        #endregion

        #region Populate Trade Dropdown List
        /// <summary>
        /// Populate Trade Dropdown List
        /// </summary>
        /// <remarks></remarks>
        public void populateTradeDropdownList()
        {
            SchedulingBL objSchedulingBL = new SchedulingBL(new SchedulingRepo());
            DataSet resultDataSet = new DataSet();
            objSchedulingBL.getAllTrades(ref resultDataSet);

            ddlTrade.DataSource = resultDataSet;
            ddlTrade.DataValueField = "id";
            ddlTrade.DataTextField = "val";
            ddlTrade.DataBind();

            ListItem newListItem = default(ListItem);
            newListItem = new ListItem("Please select", "-1");
            ddlTrade.Items.Insert(0, newListItem);

        }

        #endregion

        #region Populate Duration Dropdown List
        /// <summary>
        /// Populate Duration Dropdown List
        /// </summary>
        /// <remarks></remarks>
        public void populateDurationDropdownList()
        {
            ListItem newListItem = default(ListItem);
            ddlDuration.Items.Clear();


            for (int i = 1; i <= 40; i++)
            {
                if (i == 1)
                {
                    newListItem = new ListItem(i.ToString() + " hour", i.ToString());
                }
                else
                {
                    newListItem = new ListItem(i.ToString() + " hours", i.ToString());
                }

                ddlDuration.Items.Add(newListItem);
            }

            newListItem = new ListItem("Please select", "-1");
            ddlDuration.Items.Insert(0, newListItem);

        }

        #endregion

        #region Validate Query String
        private Boolean validateQueryString()
        {
            String msatType = Request.QueryString[PathConstants.MsatType];

            if (msatType == null || !(msatType.Equals(PathConstants.CyclicMaintenance) || msatType.Equals(PathConstants.MEServicing)))
            {
                uiMessage.showErrorMessage(UserMessageConstants.InvalidQueryString);
                disableControls();
                return false;
            }

            return true;

        }
        #endregion

        #region Disable controls
        private void disableControls()
        {
            btnAdd.Enabled = false;
            btnFindOperative.Enabled = false;
            txtDate.Attributes.Add("readonly", "readonly");
            txtAppointmentNotes.Attributes.Add("readonly", "readonly");
            txtWorksRequired.Attributes.Add("readonly", "readonly");
            ddlDuration.Enabled = false;
            ddlTrade.Enabled = false;
            txtWaterCalDate.TargetControlID = txtHiddenDate.ID.ToString();

            foreach (GridViewRow grdRow in grdTradeOperative.Rows)
            {
                LinkButton linkBtnDelete = (LinkButton)grdRow.FindControl("imgDelete");
                linkBtnDelete.Enabled = false;
            }

        }
        #endregion

        #endregion

    }
}