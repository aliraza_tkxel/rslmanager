﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage/Pdr.Master"
CodeBehind="ToBeArrangedAppointmentSummary.aspx.cs" 
Inherits="PropertyDataRestructure.Views.Scheduling.ToBeArrangedAppointmentSummary" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Styles/Site.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/scheduling.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/default.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="updateProgressReportsArea" runat="server" DisplayAfter="10">
        <ProgressTemplate>
            <div class="loading-image">
                <img alt="Please Wait" src="../../Images/menu/progress.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="updPanelMisJobSheet" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="headingTitle" style="width: 98%">
                <b>Schedule</b> 
                <asp:Label runat="server" ID="lblHeading" Font-Bold="true" style="margin-left:5px;"  Text=""></asp:Label>
            </div>
            <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="400px" />
            <div class="mainContainer">
                <asp:Panel ID="pnlHeader" Width="100%" runat="server">
                    <asp:Label runat="server" CssClass="leftControl" Font-Bold="true" Text="    Appointment Summary"></asp:Label>
                    <asp:Label ID="lblJobSheetCounter" runat="server" CssClass="rightControl" Font-Bold="true"
                        Text="Job Sheet 1 of 1"></asp:Label>
                </asp:Panel>
                <br />
                <hr />
                <br />
                <div style="padding: 10px; border: 1px solid;">
                    <table>
                        <tr>
                            <td style="width: 20%;">
                                JSN: &nbsp &nbsp
                                <asp:Label ID="lblJsn" runat="server" Text=""></asp:Label>
                                <asp:HiddenField ID="hid_isAppointmentCreated" runat="server" Value="0"></asp:HiddenField>
                            </td>
                            <td style="width: 66%;">
                                <asp:Label ID="lblAppointmentType" runat="server" Text=""></asp:Label> 
                            </td>
                            <td style="width: 15%;">
                                Trade: &nbsp &nbsp
                                <asp:Label ID="lblTrade" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
                <br />
                <br />
                <asp:Panel ID="pnlAppointmentDetail" runat="server">
                    <div class="leftDiv">
                        <table style="width: 100%;" cellspacing="10" cellpadding="5">
                            <tr>
                                <td class="labelTd">
                                    <asp:Label runat="server" Text="Operative:"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblOperative" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="labelTd">
                                    <asp:Label runat="server" Text="Duration:"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblDuration" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="labelTd">
                                    <asp:Label runat="server" Text="Duration Total:"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblDurationTotal" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                          
                        </table>
                    </div>
                    <div class="leftDiv">
                        <table style="width: 100%;" cellspacing="10" cellpadding="5">
                            <tr>
                                <td class="labelTd">
                                    <asp:Label runat="server" Text="Start Date:"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblStartDate" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="labelTd">
                                    <asp:Label runat="server" Text="End Date:"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblEndDate" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
                <div style="clear: both">
                </div>
                <hr />
                <div style="clear: both">
                </div>
                <asp:Panel ID="pnlPropertyDetail" runat="server">
                    <div class="leftDiv">
                        <table style="width: 100%;" cellspacing="10" cellpadding="5">
                            <tr>
                                <td class="labelTd">
                                    <asp:Label runat="server" Font-Bold="true" Text="Scheme:"></asp:Label>                                    
                                </td>
                                <td>
                                    <asp:Label ID="lblScheme" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                             <tr>
                                <td class="labelTd">
                                    <asp:Label runat="server" Font-Bold="true" Text="Block:"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblBlock" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="labelTd">
                                    <asp:Label runat="server" Font-Bold="true" Text="Address:"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblAddress" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="labelTd">
                                    <asp:Label runat="server" Text=""></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblTowncity" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="labelTd">
                                    <asp:Label runat="server" Text=""></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblCounty" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="labelTd">
                                    <asp:Label runat="server" Text=""></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblPostcode" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="leftDiv">
                        <table style="width: 100%;" cellspacing="10" cellpadding="5">
                            <tr>
                                <td class="labelTd">
                                    <asp:Label runat="server" Text="Customer:"></asp:Label>
                                </td>
                                <td>
                                    <asp:HiddenField ID="hdnCustomerId" runat="server" />
                                    <asp:Label ID="lblCustomerName" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="labelTd">
                                    <asp:Label runat="server" Text="Telephone:"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblCustomerTelephone" runat="server"></asp:Label>
                                    <asp:TextBox ID="txtCustomerTelephone" runat="server" Visible="False"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="labelTd">
                                    <asp:Label runat="server" Text="Mobile:"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblCustomerMobile" runat="server"></asp:Label>
                                    <asp:TextBox ID="txtCustomerMobile" runat="server" Visible="False"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="labelTd">
                                    <asp:Label runat="server" Text="Email:"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblCustomerEmail" runat="server"></asp:Label>
                                    <asp:TextBox ID="txtCustomerEmail" runat="server" Visible="False"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="labelTd">
                                    <asp:Label runat="server" Text=""></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="Label4" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="clear: both">
                    </div>
                    <div style="text-align: right;">
                        <asp:Button ID="btnUpdateCustomerDetails" OnClick="btnUpdateCustomerDetails_Click" runat="server" Text="Update customer details" />
                    </div>
                </asp:Panel>
                <div style="clear: both">
                </div>
                <hr />
                <div style="clear: both">
                </div>
                <asp:Panel ID="pnlNotes" runat="server">
                    <div class="leftDiv">
                        <table style="width: 100%;" cellspacing="10" cellpadding="5">
                            <tr>
                                <td>
                                    <asp:Label Font-Bold="true" runat="server" Text="Customer Appointment Notes:"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    If the customer has any preferred contact time,<br />
                                    or ways in which we could contact them,<br />
                                    please enter them in the box below:
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtCustAppointmentNotes" MaxLength="1000" runat="server" TextMode="MultiLine"
                                        Height="150px" Width="100%"></asp:TextBox>                                   
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="leftDiv">
                        <table style="width: 100%;" cellspacing="10" cellpadding="5">
                            <tr>
                                <td>
                                    <asp:Label Font-Bold="true" runat="server" Text="Job Sheet Notes:"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Please enter information specific to the
                                    <br />
                                    <asp:Label runat="server" ID="lblWorkType" Text=""></asp:Label> works in the box below:
                                    <br />
                                    &nbsp
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtJobSheetNotes" MaxLength="1000" runat="server" TextMode="MultiLine"
                                        Height="150px" Width="100%"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
                <div style="clear: both">
                </div>
                <br />
                <div style="text-align: right; width: 100%;">
                    <asp:Button ID="btnBack" OnClick = "btnBack_Click" runat="server" Text="< Back" />
                    &nbsp &nbsp
                    <asp:Button ID="btnScheduleAppointment" runat="server" OnClick="btnScheduleAppointment_Click" Text="Schedule Appointment" />
                    &nbsp &nbsp
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
