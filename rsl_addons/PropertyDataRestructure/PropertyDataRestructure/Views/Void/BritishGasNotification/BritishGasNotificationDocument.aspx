﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BritishGasNotificationDocument.aspx.cs"
    Inherits="PropertyDataRestructure.Views.Void.BritishGasNotification.BritishGasNotificationDocument" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>British Gas Void Notification</title>
    <style type="text/css">
        body
        {
            margin: 0px;
            font-family: Arial;
            margin:5px;
        }
        
        .style1
        {
            width: 30%;
        }
        .style2
        {
            width: 30%;
        }
        .style3
        {
            width: 30%;
        }
        .dataValue
        {
            height: 30px;
            border: 1px Solid #b1b1b1;
            vertical-align: middle;
            padding: 10px 0px 0px 5px;
        }
        
        .dataValue-Large
        {
            height: 70px;
            border: 1px Solid #b1b1b1;
            padding: 10px 0px 0px 5px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager runat="server" />
    <div>
        <asp:UpdatePanel runat="server" ID="updPanelNotification">
            <ContentTemplate>
                <table id="header-Table" width="100%">
                <tr>
                <td style=" width:10%;"> <img alt="BHG Logo" src="../../../Images/menu/broadland.png" /></td>
                <td style=" width:60%; text-align:center;"><h3>British Gas Void Notification Form</h3></td>
                <td style=" width:20%;"><img alt="British Gas Logo" src="../../../Images/britishgas.png" /> </td>
                </tr>
                </table>
                <asp:Panel runat="server" ID="pnlStage1">
                    <h4 style="text-align: center">
                        Stage 1: Void Notification Information</h4>
                    <table id="tblStage1" width="100%">
                        <tr>
                            <td class="style1">
                                Property Address
                            </td>
                            <td>
                                <div class="dataValue">
                                    <asp:Label Text="" ID="lblProertyAddress" runat="server" /></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="style1">
                                Property Post Code
                            </td>
                            <td>
                                <div class="dataValue">
                                    <asp:Label Text="" ID="lblPropertyPostCode" runat="server" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="style1">
                                Current Tenant Name (s)
                            </td>
                            <td>
                                <div class="dataValue">
                                    <asp:Label Text="" ID="lblCurrentTenantName" runat="server" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="style1">
                                Vacating Tenant Forwarding Address
                            </td>
                            <td>
                                <div class="dataValue">
                                    <asp:Label Text="" ID="lblTenantForwardingAddress" runat="server" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="style1">
                                Date Occupancy Ceases
                            </td>
                            <td>
                                <div class="dataValue">
                                    <asp:Label Text="" ID="lblDateOccupancyCeases" runat="server" /></div>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlStage2">
                    <h4 style="text-align: center">
                        Stage 2: Void Inspection Information Gas</h4>
                    <table id="tblStage2" width="100%">
                        <tr>
                            <td class="style3">
                                Meter Type/Reading
                            </td>
                            <td>
                                <div class="dataValue">
                                    <asp:Label Text="" ID="lblMeterType" runat="server" /></div>
                            </td>
                            <td>
                                <div class="dataValue">
                                    <asp:Label Text="" ID="lblMeterReading" runat="server" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="style3">
                                Electric Meter Type/Reading/s
                            </td>
                            <td>
                                <div class="dataValue">
                                    <asp:Label Text="" ID="lblElectricMeterType" runat="server" />
                                </div>
                            </td>
                            <td>
                                <div class="dataValue">
                                    <asp:Label Text="" ID="lblElectricMeterReading" runat="server" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="style3">
                                Gas Debt Amount
                            </td>
                            <td colspan="2">
                                <div class="dataValue">
                                    <asp:Label Text="" ID="lblGasDebtAmount" runat="server" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="style3">
                                Electricity Debt Amount
                            </td>
                             <td colspan="2">
                                <div class="dataValue">
                                    <asp:Label Text="" ID="lblElectricityDebtAmount" runat="server" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlStage3">
                    <h4 style="text-align: center">
                        Stage 3: New Tenant Information New</h4>
                    <table id="tblStage3" width="100%">
                        <tr>
                            <td class="style2">
                                Tenant Name (s)
                            </td>
                            <td>
                                <div class="dataValue">
                                    <asp:Label Text="" ID="lblTenantName1" runat="server" />
                                </div>
                            </td>
                            <td>
                                <div class="dataValue">
                                    <asp:Label Text="" ID="lblTenantName2" runat="server" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="style2">
                                Date Occupancy Begin
                            </td>
                             <td colspan="2">
                                <div class="dataValue">
                                    <asp:Label Text="" ID="lblDateOccupancy" runat="server" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="style2">
                                Previous Address
                            </td>
                            <td colspan="2">
                                <div class="dataValue">
                                    <asp:Label Text="" ID="lblPreviousAddress" runat="server" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="style2">
                                Gas Meter Reading
                            </td>
                            <td colspan="2">
                                <div class="dataValue">
                                    <asp:Label Text="" ID="lblGasMeterReading" runat="server" />
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="style2">
                                Electric Meter Reading/s
                            </td>
                             <td colspan="2">
                                <div class="dataValue">
                                    <asp:Label Text="" ID="lblNewElectricMeterReading" runat="server" /></div>
                            </td>
                        </tr>
                        <tr>
                            <td class="style2">
                                Any other information<br></br> (including D.O.B and<br></br> Telephone Number)<br></br>
                            </td>
                             <td colspan="2">
                                <div class="dataValue-Large">
                                    <asp:Label Text="" ID="lblDob" runat="server" /></div>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <h4 style="text-align: center">
                    Contact Helen Anderson on: 0800 975 6150 or Please email details<br />
                    to:mtt.south@britishgas.co.uk</h4>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
