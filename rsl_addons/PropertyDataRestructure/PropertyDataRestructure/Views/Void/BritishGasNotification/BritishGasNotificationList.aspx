﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Pdr.Master" AutoEventWireup="true"
    CodeBehind="BritishGasNotificationList.aspx.cs" Inherits="PropertyDataRestructure.Views.Void.BritishGasNotificationList" %>

<%@ Register TagPrefix="ucCompleteTab" TagName="BritishGasNotificationCompleteTab"
    Src="~/UserControls/Void/BritishGasNotification/BritishGasNotificationCompleteTab.ascx" %>
<%@ Register TagPrefix="ucInProgressTab" TagName="BritishGasNotificationInProgressTab"
    Src="~/UserControls/Void/BritishGasNotification/BritishGasNotificationInProgressTab.ascx" %>

<%@ Register TagPrefix="ucLoggedTab" TagName="BritishGasNotificationLoggedTab"
    Src="~/UserControls/Void/BritishGasNotification/BritishGasNotificationLoggedTab.ascx" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        //setup before functions
        var typingTimer;                //timer identifier
        var doneTypingInterval = 1000;  //time in ms, 5 second for example

        function TypingInterval() {

            clearTimeout(typingTimer);

            typingTimer = setTimeout(searchTextChanged, doneTypingInterval);

        }

        function searchTextChanged() {
            __doPostBack('<%= txtSearchBox.ClientID %>', '');
        }
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <asp:UpdateProgress ID="updateProgressReportsArea" runat="server" DisplayAfter="10">
        <ProgressTemplate>
            <div class="loading-image">
                <img alt="Please Wait" src="../../../Images/menu/progress.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="updPanelNotificationList" runat="server">
        <ContentTemplate>
            <div style="margin-left:-15px;"><uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="400px" /></div>
            <p style="background-color: Black; height: 22px; text-align: justify; font-family: Tahoma;
                font-weight: bold; margin: 0 0 6px; font-size: 15px; padding: 8px;">
                <font color="white">
                    <asp:Label runat="server" ID="lblHeading" Text="British Gas Notification"></asp:Label></font>
            </p>
            <div style="border: 1px solid black; height: 672px; padding: 5px;">
                <table style="width: 100%;">
                    <tr>
                        <td colspan="100%" style="padding-right: 0px !important; width: 100%;">
                            <div style="width: 100%; padding: 0; margin: 0; z-index: 1;">
                                <asp:LinkButton ID="lnkBtnInProgressTab" OnClick="lnkBtnInProgressTab_Click" CssClass="TabClicked"
                                    runat="server" BorderStyle="Solid" BorderColor="Black">In Progress: </asp:LinkButton>
                                <asp:LinkButton ID="lnkBtnCompleteTab" OnClick="lnkBtnCompleteTab_Click" CssClass="TabInitial"
                                    runat="server" BorderStyle="Solid" BorderColor="Black">Complete: </asp:LinkButton>

                                     <asp:LinkButton ID="lnkBtnLoggedTab" OnClick="lnkBtnLoggedTab_Click" CssClass="TabInitial"
                                    runat="server" BorderStyle="Solid" BorderColor="Black">Logged: </asp:LinkButton>
                                <div style="float: right; margin-top: 10px;">
                                   
                                    <asp:TextBox ID="txtSearchBox" runat="server" AutoPostBack="false" 
                                        onkeyup="TypingInterval();" ontextchanged="txtSearchBox_TextChanged"></asp:TextBox>
                                    <ajax:TextBoxWatermarkExtender ID="txtSearchBoxWatermarkExtender" runat="server"
                                        TargetControlID="txtSearchBox" WatermarkCssClass="searchText" WatermarkText="Quick find ...">
                                    </ajax:TextBoxWatermarkExtender>
                                </div>
                            </div>
                            <div style="border-bottom: 1px solid black; height: 0px; clear: both; margin-left: 2px;
                                width: 99.7%;">
                            </div>
                            <div style="clear: both; margin-bottom: 5px;">
                            </div>
                        </td>
                    </tr>
                </table>
                <asp:MultiView ID="MainView" runat="server" ActiveViewIndex="0">
                    <asp:View ID="View1" runat="server">
                        <ucInProgressTab:BritishGasNotificationInProgressTab ID="ucInProgressTab" runat="server" />
                    </asp:View>
                    <asp:View ID="View2" runat="server"  >
                        <ucCompleteTab:BritishGasNotificationCompleteTab ID="ucCompleteTab" runat="server">
                        

                        

                        


                        

                        </ucCompleteTab:BritishGasNotificationCompleteTab>
                    </asp:View>

                     <asp:View ID="View3" runat="server" >
                        <ucLoggedTab:BritishGasNotificationLoggedTab ID="ucLoggedTab" runat="server">
                         

                         

                         


                         

                        </ucLoggedTab:BritishGasNotificationLoggedTab>
                    </asp:View>

                </asp:MultiView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
