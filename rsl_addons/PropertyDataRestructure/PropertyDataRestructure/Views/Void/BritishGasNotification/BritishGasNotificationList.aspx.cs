﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_Utilities.Constants;
using PropertyDataRestructure.Base;
using PDR_Utilities.Enums;
using PDR_BusinessObject.BritishGas;
using PDR_BusinessObject.PageSort;

namespace PropertyDataRestructure.Views.Void
{
    public partial class BritishGasNotificationList : PageBase
    {
        #region Events
        #region Page load event
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    uiMessage.hideMessage();
                    //getQueryString();

                    if (!IsPostBack)
                    {

                        if (Request.QueryString[PathConstants.ListingTab] == ApplicationConstants.StatusInProgress || Request.QueryString.Count == 0)
                        {
                            populateInProgressNotificationList();
                        }
                        else if (Request.QueryString[PathConstants.ListingTab] == ApplicationConstants.StatusComplete)
                        {
                            populateCompleteNotificationList();
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region Link Button Inspections to be Arranged
        /// <summary>
        ///  Link Button Inspections to be Arranged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkBtnInProgressTab_Click(object sender, EventArgs e)
        {
            try
            {
                lnkBtnInProgressTab.CssClass = ApplicationConstants.TabClickedCssClass;
                lnkBtnCompleteTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnLoggedTab.CssClass = ApplicationConstants.TabInitialCssClass;
                ucInProgressTab.loadData();
                MainView.ActiveViewIndex = 0;
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }


        protected void lnkBtnLoggedTab_Click(object sender, EventArgs e)
        {
            try
            {
                lnkBtnLoggedTab.CssClass = ApplicationConstants.TabClickedCssClass;
                lnkBtnCompleteTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnInProgressTab.CssClass = ApplicationConstants.TabInitialCssClass ;
                ucLoggedTab.loadData();
                MainView.ActiveViewIndex = 2;
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }




        #endregion

        #region Link Button Inspections Arranged
        /// <summary>
        /// Link Button Inspections Arranged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkBtnCompleteTab_Click(object sender, EventArgs e)
        {
            try
            {
                lnkBtnInProgressTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnLoggedTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnCompleteTab.CssClass = ApplicationConstants.TabClickedCssClass;
                MainView.ActiveViewIndex = 1;
                ucCompleteTab.loadData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        #endregion

        #region txtSearchBox TextChanged
        protected void txtSearchBox_TextChanged(object sender, EventArgs e)
        {
            try
            {

                searchResults();

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #endregion

        #region Functions
        /// <summary>
        /// This function would call the user controls function to populate the in progress list items of british gas notification
        /// </summary>
        #region Populate In Progress Notification List
        private void populateInProgressNotificationList()
        {
            BritishGasSearchBo searchBo = new BritishGasSearchBo();
            searchBo.searchText = txtSearchBox.Text.Trim();
            searchBo.notificationStatus = Enums.BritishGasNotificationStatus.InProgress.ToString();
            objSession.BritishGasSearchBo = searchBo;
            ucInProgressTab.loadData();
        }
        #endregion

        #region Populate Complete Notification List
        /// <summary>
        /// This function would call the user controls function to populate the complete list items of british gas notification
        /// </summary>
        private void populateCompleteNotificationList()
        {
            BritishGasSearchBo searchBo = new BritishGasSearchBo();
            searchBo.searchText = txtSearchBox.Text.Trim();
            searchBo.notificationStatus = Enums.BritishGasNotificationStatus.InProgress.ToString();
            objSession.BritishGasSearchBo = searchBo;
            ucCompleteTab.loadData();
        }
        #endregion


        #region Populate Logged Notification List
        /// <summary>
        /// This function would call the user controls function to populate the complete list items of british gas notification
        /// </summary>
        private void populateLoggedNotificationList()
        {
            BritishGasSearchBo searchBo = new BritishGasSearchBo();
            searchBo.searchText = txtSearchBox.Text.Trim();
            searchBo.notificationStatus = Enums.BritishGasNotificationStatus.Logged.ToString();
            objSession.BritishGasSearchBo = searchBo;
            ucLoggedTab.loadData();
        }
        #endregion

        #region Search Results
        public void searchResults()
        {
            objSession.SearchText = txtSearchBox.Text.Trim();
            pageSortViewState = null;

            if (MainView.ActiveViewIndex == 0)
            {

                populateInProgressNotificationList();
            }
            else if (MainView.ActiveViewIndex == 1)
            {
                populateCompleteNotificationList();
            }
            else if (MainView.ActiveViewIndex == 2)
            {
                populateLoggedNotificationList();    
            }

        }
        #endregion
        #endregion
    }
}