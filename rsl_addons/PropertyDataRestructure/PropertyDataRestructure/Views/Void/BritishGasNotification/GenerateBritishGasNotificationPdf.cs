﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EO.Pdf;
using System.IO;
using System.Net.Mail;
using System.Data;
using PropertyDataRestructure.Base;
using System.Text;
using PDR_BusinessLogic.BritishGas;
using PDR_DataAccess.BritishGas;
using System.Net.Mime;

namespace PropertyDataRestructure.Views.Void.BritishGasNotification
{
    public class GenerateBritishGasNotificationPdf
    {
        public int britishGasId = 0;
        public int britishStageId = 0;
        public string documentTemplateUrl = string.Empty;
        public  string notificationDocumentPath = string.Empty;
        public string notificationRootPath = string.Empty;
        public string notificationDoucmentName = string.Empty;
        public byte[] notificationInByteForm = null;
        public void processNotificationDocument()
        {
            this.documentTemplateUrl = this.ToAbsoluteUrl("~/Views/Void/BritishGasNotification/BritishGasNotificationDocument.aspx?nid=" + this.britishGasId + "&sid=" + this.britishStageId.ToString());
            this.setFilePath();
            this.makeEoPdf();
            this.readNotificationDocument();
            //this.downloadNotificationDocument(notificationDoucmentName, notificationDocumentPath);
            this.sendHtmlFormattedEmailWithAttachment();
        }
        public void makeEoPdf()
        {
            //Convert the Url to PDF
            EO.Pdf.Runtime.AddLicense("HOG4cai1wuCvdabw+g7kp+rp2g+9RoGkscufdePt9BDtrNzpz+eupeDn9hny" +
                "ntzCnrWfWZekzQzrpeb7z7iJWZekscufWZfA8g/jWev9ARC8W7zTv/vjn5mk" +
                "BxDxrODz/+ihbaW0s8uud4SOscufWbOz8hfrqO7CnrWfWZekzRrxndz22hnl" +
                "qJfo8h/kdpm5wN23aKm0wt6hWe3pAx7oqOXBs9+hWabCnrWfWZekzR7ooOXl" +
                "BSDxnrXo4xSwsc/Z6hLla8z83/LRe+fN59rmdrTAwB7ooOXlBSDxnrWRm+eu" +
                "peDn9hnynrWRm3Xj7fQQ7azcwp61n1mXpM0X6Jzc8gQQyJ21ucM=");

            //HtmlToPdf.Options.OutputArea = new RectangleF(0f, 1f, 8f, 20f);
            HtmlToPdf.Options.PageSize = PdfPageSizes.A4;
            //HtmlToPdf.Options.AutoFitY = HtmlToPdfAutoFitMode.ScaleToFit
            HtmlToPdf.ConvertUrl(this.documentTemplateUrl, notificationDocumentPath);

        }
        public string ToAbsoluteUrl(string relativeUrl)
        {
            if (string.IsNullOrEmpty(relativeUrl))
            {
                return relativeUrl;
            }

            if (HttpContext.Current == null)
            {
                return relativeUrl;
            }

            if (relativeUrl.StartsWith("/"))
            {
                relativeUrl = relativeUrl.Insert(0, "~");
            }
            if (!relativeUrl.StartsWith("~/"))
            {
                relativeUrl = relativeUrl.Insert(0, "~/");
            }

            dynamic url = HttpContext.Current.Request.Url;
            dynamic port = url.Port != 80 ? (":" + url.Port.ToString()) : String.Empty;

            return String.Format("{0}://{1}{2}{3}", url.Scheme, url.Host, port, VirtualPathUtility.ToAbsolute(relativeUrl));
        }
        private void readNotificationDocument()
        {
            using (FileStream stream = new FileStream(notificationDocumentPath, FileMode.Open, FileAccess.Read))
            {
                using (BinaryReader reader = new BinaryReader(stream))
                {
                    notificationInByteForm = reader.ReadBytes(Convert.ToInt32(stream.Length));
                }
            }
        }

        public void setFilePath()
        {
            //gernate unique String  
            string uniqueString = DateTime.Now.ToString("ddMMyyyyhhmmss");
            notificationRootPath = HttpContext.Current.Server.MapPath("~/PDF/");
            notificationDoucmentName = "BritishGasVoidNotification" + "_" + uniqueString + ".pdf";
            notificationDocumentPath = notificationRootPath + notificationDoucmentName;

            if ((Directory.Exists(notificationRootPath) == false))
            {
                Directory.CreateDirectory(notificationRootPath);
            }
        }


        public void downloadNotificationDocument(string fileName, string completeFilePath)
        {
            HttpContext.Current.Response.AppendHeader("content-disposition", "attachment; filename=" + fileName);
            HttpContext.Current.Response.ContentType = "application/pdf";
            HttpContext.Current.Response.WriteFile(completeFilePath);
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.End();
        }


        public void sendHtmlFormattedEmailWithAttachment()
        {
            DataSet resultDs = new DataSet();
            BritishGasBL britishGasBl = new BritishGasBL(new BritishGasRepo());
            resultDs =  britishGasBl.getVoidPDFDocumentInfo(britishGasId); ;



            string recepientEmail = resultDs.Tables[0].Rows[0]["BritishGasEmail"].ToString();
            string body = string.Empty  ;
            MailMessage mailMessage = new MailMessage();
            mailMessage.Subject = "Stage" + resultDs.Tables[0].Rows[0]["StageId"].ToString() + " British Gas Void Notification Form";
           body = this.populateNotificationEmailBody(resultDs.Tables[0]);
            mailMessage.IsBodyHtml = true;
            mailMessage.To.Add(new MailAddress(recepientEmail));
            // Attach logo images with email
            //==========================================='
            LinkedResource logo50Years = new LinkedResource(HttpContext.Current.Server.MapPath("~/Images/50_Years.gif"));
            logo50Years.ContentId = "logo50Years_Id";

            body = body.Replace("{Logo_50_years}", string.Format("cid:{0}", logo50Years.ContentId));

            LinkedResource logoBroadLandRepairs = new LinkedResource(HttpContext.Current.Server.MapPath("~/Images/Broadland-Housing-Association.gif"));
            logoBroadLandRepairs.ContentId = "logoBroadLandRepairs_Id";

            body = body.Replace("{Logo_Broadland-Housing-Association}", string.Format("cid:{0}", logoBroadLandRepairs.ContentId));
            ContentType mimeType = new ContentType("text/html");

            AlternateView alternatevw = AlternateView.CreateAlternateViewFromString(body.ToString(), mimeType);
            alternatevw.LinkedResources.Add(logo50Years);
            alternatevw.LinkedResources.Add(logoBroadLandRepairs);
            mailMessage.AlternateViews.Add(alternatevw);
            mailMessage.Body = body;
            Stream stream = new MemoryStream(notificationInByteForm);
            Attachment iAttachement = new Attachment(stream, notificationDoucmentName, "application/pdf");
            mailMessage.Attachments.Add(iAttachement);
            //The SmtpClient gets configuration from Web.Config
            SmtpClient smtp = new SmtpClient();

            smtp.Send(mailMessage);
            iAttachement.Dispose();
        }


        #region "Populate Email body for cancel appointment"
        /// <summary>
        /// Populate Email body
        /// </summary>
        /// <param name="objMeAppointmentBO"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public string populateNotificationEmailBody(DataTable resultDt)
        {
            StringBuilder body = new StringBuilder();
            StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/Email/BritishNotificationDocument.html"));
            body.Append(reader.ReadToEnd().ToString());
            body.Replace("{Stage}", britishStageId.ToString());
            body.Replace("{Address}", resultDt.Rows[0]["Address"].ToString());
            body.Replace("{TownCity}", resultDt.Rows[0]["TOWNCITY"].ToString());
            body.Replace("{County}", resultDt.Rows[0]["COUNTY"].ToString());
            body.Replace("{PostCode}", resultDt.Rows[0]["POSTCODE"].ToString());
            
            return body.ToString();
        }
        #endregion
    }
}