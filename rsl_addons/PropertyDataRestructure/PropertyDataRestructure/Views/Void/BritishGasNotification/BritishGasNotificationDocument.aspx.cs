﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PropertyDataRestructure.Base;

using System.Data;
using PDR_BusinessLogic.BritishGas;
using PDR_DataAccess.BritishGas;

namespace PropertyDataRestructure.Views.Void.BritishGasNotification
{
    public partial class BritishGasNotificationDocument : System.Web.UI.Page
    {
        public int britishGasId = 0;
        public int britishStageId = 0;
        #region Events

        #region Page Load Event
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["sid"] != null && Request.QueryString["nid"] != null)
            {
                this.britishGasId = Convert.ToInt32(Request.QueryString["nid"]);
                this.britishStageId = Convert.ToInt32(Request.QueryString["sid"]);
                this.loadData();
            }
        }
        #endregion
        #endregion
        #region Functions


        #region load Data
        public void loadData()
        {
           
            BritishGasBL britishGasBl = new BritishGasBL(new BritishGasRepo());
            DataSet resultDs = new DataSet();
            resultDs = britishGasBl.getVoidPDFDocumentInfo(britishGasId);
            
            this.populateControls(resultDs.Tables[0]);
        }
        #endregion

        #region populate controls
        /// <summary>
        /// populate the controls with values in bo
        /// </summary>
        /// <param name="britishGasBo"></param>
        private void populateControls(DataTable resultDs)
        {

            lblProertyAddress.Text = resultDs.Rows[0]["Address"].ToString();
            lblPropertyPostCode.Text = resultDs.Rows[0]["POSTCODE"].ToString();
            lblCurrentTenantName.Text = resultDs.Rows[0]["Customer"].ToString();
            lblTenantForwardingAddress.Text = resultDs.Rows[0]["TenantAddress"].ToString();
            lblDateOccupancyCeases.Text = resultDs.Rows[0]["DateOccupancyCease"].ToString();

            lblMeterType.Text = resultDs.Rows[0]["GasMeterType"].ToString();
            lblMeterReading.Text = resultDs.Rows[0]["GasMeterReading"].ToString();
            lblElectricMeterType.Text = resultDs.Rows[0]["ElectricMeterType"].ToString();
            lblElectricMeterReading.Text = resultDs.Rows[0]["ElectricMeterReading"].ToString();
            lblGasDebtAmount.Text = resultDs.Rows[0]["GasDebtAmount"].ToString();
            lblElectricityDebtAmount.Text = resultDs.Rows[0]["ElectricDebtAmount"].ToString();


            lblTenantName1.Text = resultDs.Rows[0]["NewTenantName"].ToString();
            //lblTenantName2.Text = resultDs.Rows[0]["NewTenantName"].ToString();
            lblDateOccupancy.Text = resultDs.Rows[0]["DateOccupancy"].ToString();
            lblPreviousAddress.Text = resultDs.Rows[0]["PreviousAddress"].ToString();
            lblGasMeterReading.Text = resultDs.Rows[0]["NewGasMeterReading"].ToString();
            lblNewElectricMeterReading.Text = resultDs.Rows[0]["NewElectricMeterReading"].ToString();
            lblDob.Text = resultDs.Rows[0]["OtherInfo"].ToString();
            pnlStage2.Visible = false;
            pnlStage3.Visible = false;
            if (this.britishStageId == 2)
            {
                pnlStage2.Visible = true;
                pnlStage3.Visible = false;
            }
            else if (this.britishStageId == 3)
            {
                pnlStage2.Visible = true;
                pnlStage3.Visible = true;
            }

        }
        #endregion
        #endregion
    }
}