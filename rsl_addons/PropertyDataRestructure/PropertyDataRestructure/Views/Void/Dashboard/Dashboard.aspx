﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Pdr.Master" AutoEventWireup="true"
    CodeBehind="Dashboard.aspx.cs" Inherits="PropertyDataRestructure.Views.Void.Dashboard.Dashboard" %>

<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false&client=gme-broadlandhousing&v=3.17"></script>
    <script src="../../../Scripts/jquery-v1.9.1.js" type="text/javascript"></script>
    <link href="../../../Styles/Dashboard.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript">

        $(window).load(function () {
            loadDefaultMap(52.630886, 1.297355, -1);
            loadCounts("getVoidInspectionToBeArranged", '#loadingVoidToBeArranged', '#lblVoidToBeArranged')
            loadCounts("getVoidInspectionArranged", '#loadingVoidArranged', '#lblVoidArranged')
            loadCounts("getVoidWorksToBeArranged", '#loadingWorksArranged', '#lblWorksArranged')
            loadCounts("getGasElectricChecksToBeArranged", '#loadingChecks', '#lblChecks')
            loadCounts("getPostVoidInspectionArranged", '#loadingPostArranged', '#lblPostArranged')
            loadCounts("getPostVoidInspectionToBeArranged", '#loadingPostVoid', '#lblPostVoid')
            loadCounts("getReletAlertCount", '#loadingRelets', '#lblRelets')
            loadCounts("getCountNoEntry", '#loadingNoEntry', '#lblNoEntries')
            loadCounts("getPendingTermination", '#loadingPending', '#lblPending')
        });
        function loadCounts(url, loading, label) {

            var postData = JSON.stringify({
            });
            $.ajax({
                type: "POST",
                url: "Dashboard.aspx/" + url,
                data: postData,
                contentType: "application/json",
                dataType: "json",
                beforeSend: function (xhr) {
                    $(loading).show();
                },
                complete: function () {
                    $(loading).hide();
                },

                success: function (result) {
                    $(label).text(result.d);
                }
            });
        }

        function loadDefaultMap(lat, lng, index) {
            var mapOptions = {
                center: new google.maps.LatLng(lat, lng),
                zoom: 8,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            var infoWindow = new google.maps.InfoWindow();
            var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);
        }

        function populateMarker(url, label, heading) {


            $('#<%= lblMapHeading.ClientID %>').text(heading);
            var postData = JSON.stringify({
                pageSize: $(label).text()
            });

            $.ajax({
                type: "POST",
                url: "Dashboard.aspx/" + url,
                data: postData,
                contentType: "application/json",
                dataType: "json",
                success: function (data) {
                    var result = data.d;
                    var items = [];

                    $.each(result, function (i, item) {

                        if (result.length == 1) {
                            items.push('[{"Address": "' + item[1] + '","Postcode": "' + item[4] + '","TownCity": "' + item[2] + '","County": "' + item[3] + '"}]')
                        }
                        else if (i == 0 && result.length > 1) {
                            items.push('[{"Address": "' + item[1] + '","Postcode": "' + item[4] + '","TownCity": "' + item[2] + '","County": "' + item[3] + '"}')
                        }
                        else if (i == result.length - 1 && result.length > 1) {
                            items.push('{"Address": "' + item[1] + '","Postcode": "' + item[4] + '","TownCity": "' + item[2] + '","County": "' + item[3] + '"}]')
                        }

                        else {
                            items.push('{"Address": "' + item[1] + '","Postcode": "' + item[4] + '","TownCity": "' + item[2] + '","County": "' + item[3] + '"}')
                        }
                    });

                    addMarkers(items);
                    return true;
                }
            });
        }


        function addMarkers(markersArray) {

            var mapOptions = {
                center: new google.maps.LatLng(52.630886, 1.297355),
                zoom: 7,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map(document.getElementById("dvMap"), mapOptions);

            var markers;
            if (markersArray.length > 0) {
                markers = JSON.parse(markersArray);

                for (j = 0; j < markers.length; j++) {
                    sleep(100);
                    getGeoLocationAndSetMarker(markers[j], map);

                }

            } else {
                loadDefaultMap(52.630886, 1.297355, -1);
            }
        }
        function infoCallback(infoWindow, marker, map) {
            return function () {
                infoWindow.open(map, marker);
            };
        };
        function sleep(milliseconds) {
            var start = new Date().getTime();
            for (var i = 0; i < 1e7; i++) {
                if ((new Date().getTime() - start) > milliseconds) {
                    break;
                }
            }
        }

        function getGeoLocationAndSetMarker(data, map) {
            var adrs = "";
            adrs = data.Address + ", " + data.TownCity + ", " + data.County + ", " + data.Postcode
            var bounds = new google.maps.LatLngBounds();

            var geocoder = new google.maps.Geocoder();
            geocoder.geocode({ 'address': adrs }, function (results, status) {
                // alert(status);
                if (status == google.maps.GeocoderStatus.OK) {
                    var lat = results[0].geometry.location.lat();
                    var lng = results[0].geometry.location.lng();
                    var myLatlng = new google.maps.LatLng(lat, lng);

                    if (myLatlng != '') {

                        setupMarker(map, myLatlng, data);
                    }
                }
            });

        }

        //point Maeker with respect to address of property 
        function setupMarker(map, location, data) {

            var infoWindow = new google.maps.InfoWindow();
            var icon = '';
            var marker;
            marker = new google.maps.Marker({
                position: location,
                map: map,
                title: data.Address + " " + data.TownCity + " " + data.County,
                animation: google.maps.Animation.DROP
            });

            (function (marker, data) {
                google.maps.event.addListener(marker, "click", function (e) {
                    infoWindow.setContent("<b>" + data.Address + ", " + data.TownCity + ", " + data.County + "<b><br>" + data.Postcode);
                    infoWindow.open(map, marker);
                });
            })(marker, data);



        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="updateProgressReportsArea" runat="server" DisplayAfter="10">
        <ProgressTemplate>
            <div class="loading-image">
                <img alt="Please Wait" src="../../../Images/menu/progress.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:Panel ID="pnlDashboardOuter" runat="server">
        <div class="wrapper">
            <div class="r_main_div">
                <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="400px" />
                <asp:UpdatePanel ID="updPanelCounter" runat="server">
                    <ContentTemplate>
                        <div class="left_part" style="margin-top:0;">
                            <div class="outer-boxes-CallToAction">
                                <div class="cover-box">
                                    <div class="box">
                                        <div class="text">
                                            Pending
                                            <br />
                                            Termination:
                                        </div>
                                        <div class="number">
                                            <label id="lblPending" style='cursor: pointer;' onclick="populateMarker('getPendingTerminationList','#lblPending','Pending Termination:')">
                                                &nbsp;</label>
                                        </div>
                                        <div class="r-arrow">
                                            <a href="../../../Views/Reports/VoidReportArea.aspx?rpt=pt">
                                                <img src="../../../Images/Dashboard/r-arrow.png" alt="" border="0" />
                                            </a>
                                        </div>
                                        <div class="number-loading-Void" id="loadingPending">
                                            <img alt="Please Wait" src="../../../Images/ajax-loader.gif" class="Loding-Image-VoidDashboard" />
                                        </div>
                                    </div>
                                    <div class="box">
                                        <div class="text">
                                            No
                                            <br />
                                            Entries:
                                        </div>
                                        <div class="number">
                                            <label id="lblNoEntries" style='cursor: pointer;' onclick="populateMarker('getNoEntryList','#lblNoEntries','No Entries:')">
                                                &nbsp;</label>
                                        </div>
                                        <div class="r-arrow">
                                            <a href="../../../Views/Reports/VoidReportArea.aspx?rpt=ne">
                                                <img src="../../../Images/Dashboard/r-arrow.png" alt="" border="0" /></a>
                                        </div>
                                        <div class="number-loading-Void" id="loadingNoEntry">
                                            <img alt="Please Wait" src="../../../Images/ajax-loader.gif" class="Loding-Image-VoidDashboard" />
                                        </div>
                                    </div>
                                    <div class="box-red">
                                        <div class="text">
                                            Relets due in the<br />
                                            next 7 days:
                                        </div>
                                        <div class="number-faults">
                                            <label id="lblRelets" style='cursor: pointer;' onclick="populateMarker('getReletAlertList','#lblRelets','Relets due in the next 7 days:')">
                                                &nbsp;
                                            </label>
                                        </div>
                                        <div class="r-arrow">
                                            <a href="../../../Views/Reports/VoidReportArea.aspx?rpt=rd">
                                                <img src="../../../Images/Dashboard/r-arrow.png" alt="" border="0" />
                                            </a>
                                        </div>
                                        <div class="number-loading-Void" id="loadingRelets">
                                            <img alt="Please Wait" src="../../../Images/ajax-loader.gif" class="Loding-Image-VoidDashboard" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="outer-boxes-CallToAction">
                                <div class="cover-box">
                                    <div class="box">
                                        <div class="text">
                                            Void Inspections
                                            <br />
                                            to be arranged:
                                        </div>
                                        <div class="number">
                                            <label id="lblVoidToBeArranged" style='cursor: pointer;' onclick="populateMarker('getVoidInspectionToBeArrangedList','#lblVoidToBeArranged',' Void Inspections to be arranged:')">
                                                &nbsp;</label>
                                        </div>
                                        <div class="r-arrow">
                                            <a href="../../../Views/Void/VoidInspection/VoidInspections.aspx">
                                                <img src="../../../Images/Dashboard/r-arrow.png" alt="" border="0" />
                                            </a>
                                        </div>
                                        <div class="number-loading-Void" id="loadingVoidToBeArranged">
                                            <img alt="Please Wait" src="../../../Images/ajax-loader.gif" class="Loding-Image-VoidDashboard" />
                                        </div>
                                    </div>
                                    <div class="box">
                                        <div class="text">
                                            Void inspections
                                            <br />
                                            arranged:
                                        </div>
                                        <div class="number">
                                            <label id="lblVoidArranged" style='cursor: pointer;' onclick="populateMarker('getVoidInspectionArrangedList','#lblVoidArranged',' Void Inspections arranged:')">
                                                &nbsp;</label>
                                        </div>
                                        <div class="r-arrow">
                                            <a href="../../../Views/Void/VoidInspection/VoidInspections.aspx?tab=aptArranged">
                                                <img src="../../../Images/Dashboard/r-arrow.png" alt="" border="0" />
                                            </a>
                                        </div>
                                        <div class="number-loading-Void" id="loadingVoidArranged">
                                            <img alt="Please Wait" src="../../../Images/ajax-loader.gif" class="Loding-Image-VoidDashboard" />
                                        </div>
                                    </div>
                                    <div class="box">
                                        <div class="text">
                                            Void Works
                                            <br />
                                            to be arranged:
                                        </div>
                                        <div class="number">
                                            <label id="lblWorksArranged" style='cursor: pointer;' onclick="populateMarker('getVoidWorksToBeArrangedList','#lblWorksArranged',' Void Works to be arranged:')">
                                                &nbsp;
                                            </label>
                                        </div>
                                        <div class="r-arrow">
                                            <a href="../../../Views/Void/VoidWorks/VoidAppointments.aspx">
                                                <img src="../../../Images/Dashboard/r-arrow.png" alt="" border="0" />
                                            </a>
                                        </div>
                                        <div class="number-loading-Void" id="loadingWorksArranged">
                                            <img alt="Please Wait" src="../../../Images/ajax-loader.gif" class="Loding-Image-VoidDashboard" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="outer-boxes-CallToAction">
                                <div class="cover-box">
                                    <div class="box">
                                        <div class="text">
                                            Post Void Inspections
                                            <br />
                                            to be arranged:
                                        </div>
                                        <div class="number">
                                            <label id="lblPostVoid" style='cursor: pointer;' onclick="populateMarker('getPostVoidInspectionToBeArrangedList','#lblPostVoid','Post Void Inspections to be arranged:')">
                                                &nbsp;</label>
                                        </div>
                                        <div class="r-arrow">
                                            <a href="../../../Views/Void/VoidInspection/PostVoidInspections.aspx">
                                                <img src="../../../Images/Dashboard/r-arrow.png" alt="" border="0" />
                                            </a>
                                        </div>
                                        <div class="number-loading-Void" id="loadingPostVoid">
                                            <img alt="Please Wait" src="../../../Images/ajax-loader.gif" class="Loding-Image-VoidDashboard" />
                                        </div>
                                    </div>
                                    <div class="box">
                                        <div class="text">
                                            Post Void Inspections
                                            <br />
                                            arranged
                                        </div>
                                        <div class="number">
                                            <label id="lblPostArranged" style='cursor: pointer;' onclick="populateMarker('getPostVoidInspectionArrangedList','#lblPostArranged','Post Void Inspections arranged :')">
                                                &nbsp;</label>
                                        </div>
                                        <div class="r-arrow">
                                            <a href='../../../Views/Void/VoidInspection/PostVoidInspections.aspx?tab=aptArranged'>
                                                <img src="../../../Images/Dashboard/r-arrow.png" alt="" border="0" />
                                            </a>
                                        </div>
                                        <div class="number-loading-Void" id="loadingPostArranged">
                                            <img alt="Please Wait" src="../../../Images/ajax-loader.gif" class="Loding-Image-VoidDashboard" />
                                        </div>
                                    </div>
                                    <div class="box">
                                        <div class="text">
                                            Gas/Electric Checks
                                            <br />
                                            to be arranged:
                                        </div>
                                        <div class="number">
                                            <label id="lblChecks" style='cursor: pointer;' onclick="populateMarker('getGasElectricChecksToBeArrangedList','#lblChecks',' Gas electric checks to be arranged:')">
                                                &nbsp;</label>
                                        </div>
                                        <div class="r-arrow">
                                            <a href="../../../Views/Void/GasElectricCheck/GasElectricChecks.aspx">
                                                <img src="../../../Images/Dashboard/r-arrow.png" alt="" border="0" />
                                            </a>
                                        </div>
                                        <div class="number-loading-Void" id="loadingChecks">
                                            <img alt="Please Wait" src="../../../Images/ajax-loader.gif" class="Loding-Image-VoidDashboard" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <div class="right_Panel" style="width: 50%">
                    <div class="box-left-b" style="margin-top:0; overflow:hidden">

                    
                        <asp:UpdatePanel ID="updPanelOperatives" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <div class="boxborder-s-right" style="padding-bottom:12px;">
                                        <asp:Label ID="lblMapHeading" runat="server" Text="Pending Terminations: " Font-Bold="true"></asp:Label>
                                    </div>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        <div id="dvMap" style="width: 100%; height: 400px; float: left; margin-bottom:2px;"></div>
                        <asp:UpdatePanel ID="updPanelListControl" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>
                                <div style="overflow: auto; float: left; width: 100%;">
                                    <asp:GridView ID="grdPendingTerminationList" runat="server" CssClass="dashboard webgrid table table-responsive" AutoGenerateColumns="False"
                                        ShowHeaderWhenEmpty="true" BorderWidth="0px" BorderStyle="Solid" Width="100%"
                                        AllowSorting="false" AllowPaging="False" GridLines="None" 
                                        CellSpacing="10" HorizontalAlign="Left" ClientIDMode="AutoID" CellPadding="2"
                                        BorderColor="#BBBBBB" EmptyDataText="No Record Found.">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Address:" SortExpression="Address" ConvertEmptyStringToNull="False" >
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAddress" runat="server" Text='<%#Eval("Address") %>'></asp:Label>
                                                </ItemTemplate>
                                                <FooterStyle VerticalAlign="Middle"></FooterStyle>
                                                <HeaderStyle Font-Bold="True" ForeColor="White" HorizontalAlign="left" />
                                                <ItemStyle Width="300px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Termination:" SortExpression="Postcode" ConvertEmptyStringToNull="False" >
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTerminationDate" runat="server" Text='<%#Eval("Termination")%>'></asp:Label>
                                                </ItemTemplate>
                                                <FooterStyle VerticalAlign="Middle"></FooterStyle>
                                                <HeaderStyle Font-Bold="True" ForeColor="White" HorizontalAlign="left" />
                                                <ItemStyle Width="60px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Relet:" SortExpression="Status" ConvertEmptyStringToNull="False">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblReletDate" runat="server" Text='<%#Eval("Relet")%>'></asp:Label>
                                                </ItemTemplate>
                                                <FooterStyle VerticalAlign="Middle"></FooterStyle>
                                                <HeaderStyle Font-Bold="True" ForeColor="White" HorizontalAlign="left" />
                                                <ItemStyle Width="80px" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <EditRowStyle Wrap="True" />
                                        <RowStyle BorderStyle="None" />
                                    </asp:GridView>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
            </div>
        </div>
    </asp:Panel>
</asp:Content>
