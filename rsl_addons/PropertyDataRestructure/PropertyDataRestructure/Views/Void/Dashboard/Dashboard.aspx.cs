﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using PDR_BusinessObject.PageSort;
using PDR_BusinessLogic.VoidInspections;
using PDR_DataAccess.VoidInspections;
using PDR_BusinessObject.CommonSearch;
using System.Web.Services;
using PropertyDataRestructure.Base;
using PropertyDataRestructure.Interface;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace PropertyDataRestructure.Views.Void.Dashboard
{
    public partial class Dashboard : PageBase, IListingPage
    {
        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                uiMessage.hideMessage();
                if (!IsPostBack)
                {
                    loadData();
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region Functions
        #region get Void Inspection To Be Arranged Count
        /// <summary>
        ///  get Void Inspection To Be Arranged Count
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        [System.Web.Services.WebMethod()]
        public static string getVoidInspectionToBeArranged()
        {
            int count = 0;
            PageSortBO objPageSortBo = new PageSortBO("DESC", "ADDRESS", 1, 30);


            VoidInspectionsBL objVoidInspectionsBl = new VoidInspectionsBL(new VoidInspectionsRepo());
            DataSet resultDataSet = new DataSet();
            count = objVoidInspectionsBl.getInspectionToBeArrangedList(ref resultDataSet, objPageSortBo, string.Empty, 0, true);

            return count.ToString();
        }
        #endregion

        #region get Void Inspection  Arranged Count
        /// <summary>
        ///  get Void Inspection Arranged Count
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        [System.Web.Services.WebMethod()]
        public static string getVoidInspectionArranged()
        {
            int count = 0;
            PageSortBO objPageSortBo = new PageSortBO("DESC", "ADDRESS", 1, 30);


            VoidInspectionsBL objVoidInspectionsBl = new VoidInspectionsBL(new VoidInspectionsRepo());
            DataSet resultDataSet = new DataSet();
            count = objVoidInspectionsBl.getInspectionArrangedList(ref resultDataSet, objPageSortBo, string.Empty,0, true);

            return count.ToString();
        }
        #endregion


        #region get VoidWorks to be Arranged Count
        /// <summary>
        ///  get Void works to be Arranged Count
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        [System.Web.Services.WebMethod()]
        public static string getVoidWorksToBeArranged()
        {
            int count = 0;
            PageSortBO objPageSortBo = new PageSortBO("DESC", "ADDRESS", 1, 30);


            VoidInspectionsBL objVoidInspectionsBl = new VoidInspectionsBL(new VoidInspectionsRepo());
            DataSet resultDataSet = new DataSet();
            count = objVoidInspectionsBl.getVoidWorksToBeArranged(ref resultDataSet, objPageSortBo, string.Empty,0 ,true);

            return count.ToString();
        }
        #endregion

        #region get GasElectricChecks to be Arranged Count
        /// <summary>
        ///  get GasElectricChecks to be Arranged Count
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        [System.Web.Services.WebMethod()]
        public static string getGasElectricChecksToBeArranged()
        {
            int count = 0;
            PageSortBO objPageSortBo = new PageSortBO("DESC", "ADDRESS", 1, 30);
            VoidInspectionsBL objVoidInspectionsBl = new VoidInspectionsBL(new VoidInspectionsRepo());
            CommonSearchBO objCommonSearchBo = new CommonSearchBO();
            DataSet resultDataSet = new DataSet();
            count = objVoidInspectionsBl.getGasElectricChecksToBeArranged(ref resultDataSet, objPageSortBo, objCommonSearchBo,0, true);

            return count.ToString();
        }
        #endregion

        #region get Post Void Inspection To Be Arranged Count
        /// <summary>
        ///  get Void Inspection To Be Arranged Count
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        [System.Web.Services.WebMethod()]
        public static string getPostVoidInspectionToBeArranged()
        {
            int count = 0;
            PageSortBO objPageSortBo = new PageSortBO("DESC", "ADDRESS", 1, 30);


            VoidInspectionsBL objVoidInspectionsBl = new VoidInspectionsBL(new VoidInspectionsRepo());
            DataSet resultDataSet = new DataSet();
            count = objVoidInspectionsBl.getPostVoidInspectionToBeArrangedList(ref resultDataSet, objPageSortBo, string.Empty,0, true);

            return count.ToString();
        }
        #endregion

        #region get Post Void Inspection  Arranged Count
        /// <summary>
        ///  get Void Inspection Arranged Count
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        [System.Web.Services.WebMethod()]
        public static string getPostVoidInspectionArranged()
        {
            int count = 0;
            PageSortBO objPageSortBo = new PageSortBO("DESC", "ADDRESS", 1, 30);
            VoidInspectionsBL objVoidInspectionsBl = new VoidInspectionsBL(new VoidInspectionsRepo());
            DataSet resultDataSet = new DataSet();
            count = objVoidInspectionsBl.getPostVoidInspectionArrangedList(ref resultDataSet, objPageSortBo, string.Empty,0, true);

            return count.ToString();
        }
        #endregion

        #region get  Count No entry
        /// <summary>
        /// get  Count No entry
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        [System.Web.Services.WebMethod()]
        public static string getCountNoEntry()
        {
            int count = 0;
            DataSet resultDataSet = new DataSet();
            VoidInspectionsBL objVoidInspectionsBl = new VoidInspectionsBL(new VoidInspectionsRepo());
            PageSortBO objPageSortBo = new PageSortBO("DESC", "JournalId", 1, 30);
            count = objVoidInspectionsBl.getNoEntryCountAndList(ref resultDataSet,objPageSortBo, string.Empty, true);
            
            return count.ToString();
        }
        #endregion

        #region get Relet Alert Count
        /// <summary>
        /// get Relet Alert Count
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        [System.Web.Services.WebMethod()]
        public static string getReletAlertCount()
        {
            int count = 0;
            DataSet resultDataSet = new DataSet();
            VoidInspectionsBL objVoidInspectionsBl = new VoidInspectionsBL(new VoidInspectionsRepo());
            PageSortBO objPageSortBo = new PageSortBO("DESC", "Ref", 1, 30);
            count = objVoidInspectionsBl.getReletAlertCount(ref resultDataSet, objPageSortBo, string.Empty, true);

            return count.ToString();
        }
        #endregion

        #region get Pending Termination
        /// <summary>
        /// get Pending Termination
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        [WebMethod()]
        public static string getPendingTermination()
        {
            int count = 0;
            DataSet resultDataSet = new DataSet();
            VoidInspectionsBL objVoidInspectionsBl = new VoidInspectionsBL(new VoidInspectionsRepo());
     
            PageSortBO objPageSortBo = new PageSortBO("DESC", "Ref", 1, 30);
            count = objVoidInspectionsBl.getPendingTermination(ref resultDataSet, objPageSortBo, string.Empty, true);

            return count.ToString();
        }
        #endregion

        #region get Pending Termination for map
        /// <summary>
        /// get Pending Termination
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        [WebMethod()]
        public static List<object[]> getPendingTerminationList(string pageSize)
        {

            DataSet resultDataSet = new DataSet();
            List<object[]> lstOutput = new List<object[]>();
            VoidInspectionsBL objVoidInspectionsBl = new VoidInspectionsBL(new VoidInspectionsRepo());
            PageSortBO objPageSortBo = new PageSortBO("DESC", "Ref", 1, Convert.ToInt32(pageSize));
            objVoidInspectionsBl.getPendingTermination(ref resultDataSet, objPageSortBo, string.Empty);

            if (resultDataSet.Tables[0].Rows.Count > 0)
            {

                int rows = 0;
                rows = resultDataSet.Tables[0].Rows.Count - 1;
                for (int intRow = 0; intRow <= rows; intRow++)
                {
                    lstOutput.Add(resultDataSet.Tables[0].Rows[intRow].ItemArray);
                }
            }
            return lstOutput;
        }
        #endregion

        #region get Void Inspection To Be Arranged List For map
        /// <summary>
        /// get Void Inspection To Be Arranged List For map
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        [WebMethod()]
        public static List<object[]> getVoidInspectionToBeArrangedList(string pageSize)
        {
            int count = 0;
            DataSet resultDataSet = new DataSet();
            List<object[]> lstOutput = new List<object[]>();
            VoidInspectionsBL objVoidInspectionsBl = new VoidInspectionsBL(new VoidInspectionsRepo());
            PageSortBO objPageSortBo = new PageSortBO("DESC", "ADDRESS", 1, Convert.ToInt32(pageSize));

            count = objVoidInspectionsBl.getInspectionToBeArrangedList(ref resultDataSet, objPageSortBo, string.Empty, 0);

            if (resultDataSet.Tables[0].Rows.Count > 0)
            {

                int rows = 0;
                rows = resultDataSet.Tables[0].Rows.Count - 1;
                for (int intRow = 0; intRow <= rows; intRow++)
                {
                    lstOutput.Add(resultDataSet.Tables[0].Rows[intRow].ItemArray);
                }
            }
            return lstOutput;
        }
        #endregion

        #region get Void Inspection   Arranged List For map
        /// <summary>
        /// get Void Inspection  Arranged List For map
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        [WebMethod()]
        public static List<object[]> getVoidInspectionArrangedList(string pageSize)
        {
            int count = 0;
            DataSet resultDataSet = new DataSet();
            List<object[]> lstOutput = new List<object[]>();
            VoidInspectionsBL objVoidInspectionsBl = new VoidInspectionsBL(new VoidInspectionsRepo());
            PageSortBO objPageSortBo = new PageSortBO("DESC", "ADDRESS", 1, Convert.ToInt32(pageSize));

            count = objVoidInspectionsBl.getInspectionArrangedList(ref resultDataSet, objPageSortBo, string.Empty,-1);

            if (resultDataSet.Tables[0].Rows.Count > 0)
            {

                int rows = 0;
                rows = resultDataSet.Tables[0].Rows.Count - 1;
                for (int intRow = 0; intRow <= rows; intRow++)
                {
                    lstOutput.Add(resultDataSet.Tables[0].Rows[intRow].ItemArray);
                }
            }
            return lstOutput;
        }
        #endregion

        #region get Void Works to be  Arranged List For map
        /// <summary>
        /// get Void Works to be  Arranged List For map
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        [WebMethod()]
        public static List<object[]> getVoidWorksToBeArrangedList(string pageSize)
        {
            int count = 0;
            DataSet resultDataSet = new DataSet();
            List<object[]> lstOutput = new List<object[]>();
            VoidInspectionsBL objVoidInspectionsBl = new VoidInspectionsBL(new VoidInspectionsRepo());
            PageSortBO objPageSortBo = new PageSortBO("DESC", "ADDRESS", 1, Convert.ToInt32(pageSize));

            count = objVoidInspectionsBl.getVoidWorksToBeArranged(ref resultDataSet, objPageSortBo, string.Empty,0);

            if (resultDataSet.Tables[0].Rows.Count > 0)
            {

                int rows = 0;
                rows = resultDataSet.Tables[0].Rows.Count - 1;
                for (int intRow = 0; intRow <= rows; intRow++)
                {
                    lstOutput.Add(resultDataSet.Tables[0].Rows[intRow].ItemArray);
                }
            }
            return lstOutput;
        }
        #endregion

        #region get GasElectricChecks to be  Arranged List For map
        /// <summary>
        /// get GasElectricChecks to be  Arranged List For map
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        [WebMethod()]
        public static List<object[]> getGasElectricChecksToBeArrangedList(string pageSize)
        {
            int count = 0;
            DataSet resultDataSet = new DataSet();
            List<object[]> lstOutput = new List<object[]>();
            VoidInspectionsBL objVoidInspectionsBl = new VoidInspectionsBL(new VoidInspectionsRepo());
            PageSortBO objPageSortBo = new PageSortBO("DESC", "ADDRESS", 1, Convert.ToInt32(pageSize));
            CommonSearchBO objCommonSearchBo = new CommonSearchBO();
            count = objVoidInspectionsBl.getGasElectricChecksToBeArranged(ref resultDataSet, objPageSortBo, objCommonSearchBo,0);

            if (resultDataSet.Tables[0].Rows.Count > 0)
            {

                int rows = 0;
                rows = resultDataSet.Tables[0].Rows.Count - 1;
                for (int intRow = 0; intRow <= rows; intRow++)
                {
                    lstOutput.Add(resultDataSet.Tables[0].Rows[intRow].ItemArray);
                }
            }
            return lstOutput;
        }
        #endregion

        #region get Post Void Inspection To Be Arranged List For map
        /// <summary>
        /// get Post Void Inspection To Be Arranged List For map
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        [WebMethod()]
        public static List<object[]> getPostVoidInspectionToBeArrangedList(string pageSize)
        {
            int count = 0;
            DataSet resultDataSet = new DataSet();
            List<object[]> lstOutput = new List<object[]>();
            VoidInspectionsBL objVoidInspectionsBl = new VoidInspectionsBL(new VoidInspectionsRepo());
            PageSortBO objPageSortBo = new PageSortBO("DESC", "ADDRESS", 1, Convert.ToInt32(pageSize));

            count = objVoidInspectionsBl.getPostVoidInspectionToBeArrangedList(ref resultDataSet, objPageSortBo, string.Empty,0);

            if (resultDataSet.Tables[0].Rows.Count > 0)
            {

                int rows = 0;
                rows = resultDataSet.Tables[0].Rows.Count - 1;
                for (int intRow = 0; intRow <= rows; intRow++)
                {
                    lstOutput.Add(resultDataSet.Tables[0].Rows[intRow].ItemArray);
                }
            }
            return lstOutput;
        }
        #endregion

        #region get Post Void Inspection  Arranged List For map
        /// <summary>
        /// get Post Void Inspection Arranged List For map
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        [WebMethod()]
        public static List<object[]> getPostVoidInspectionArrangedList(string pageSize)
        {
            int count = 0;
            DataSet resultDataSet = new DataSet();
            List<object[]> lstOutput = new List<object[]>();
            VoidInspectionsBL objVoidInspectionsBl = new VoidInspectionsBL(new VoidInspectionsRepo());
            PageSortBO objPageSortBo = new PageSortBO("DESC", "ADDRESS", 1, Convert.ToInt32(pageSize));

            count = objVoidInspectionsBl.getPostVoidInspectionArrangedList(ref resultDataSet, objPageSortBo, string.Empty,0);

            if (resultDataSet.Tables[0].Rows.Count > 0)
            {

                int rows = 0;
                rows = resultDataSet.Tables[0].Rows.Count - 1;
                for (int intRow = 0; intRow <= rows; intRow++)
                {
                    lstOutput.Add(resultDataSet.Tables[0].Rows[intRow].ItemArray);
                }
            }
            return lstOutput;
        }
        #endregion

        #region get Relet Alert List For map
        /// <summary>
        /// get Relet Alert List For map
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        [WebMethod()]
        public static List<object[]> getReletAlertList(string pageSize)
        {

            DataSet resultDataSet = new DataSet();
            List<object[]> lstOutput = new List<object[]>();
            VoidInspectionsBL objVoidInspectionsBl = new VoidInspectionsBL(new VoidInspectionsRepo());
            PageSortBO objPageSortBo = new PageSortBO("DESC", "Ref", 1, 25);
            objPageSortBo.PageSize = Convert.ToInt32(pageSize);
            objVoidInspectionsBl.getReletAlertCount(ref resultDataSet, objPageSortBo, string.Empty);

            if (resultDataSet.Tables[0].Rows.Count > 0)
            {

                int rows = 0;
                rows = resultDataSet.Tables[0].Rows.Count - 1;
                for (int intRow = 0; intRow <= rows; intRow++)
                {
                    lstOutput.Add(resultDataSet.Tables[0].Rows[intRow].ItemArray);
                }
            }
            return lstOutput;
        }
        #endregion

        #region get No Entry List For map
        /// <summary>
        /// get No Entry List For map
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        [WebMethod()]
        public static List<object[]> getNoEntryList(string pageSize)
        {

            DataSet resultDataSet = new DataSet();
            List<object[]> lstOutput = new List<object[]>();
            VoidInspectionsBL objVoidInspectionsBl = new VoidInspectionsBL(new VoidInspectionsRepo());
            PageSortBO objPageSortBo = new PageSortBO("DESC", "JournalId", 1, 25);
            objPageSortBo.PageSize = Convert.ToInt32(pageSize);
            objVoidInspectionsBl.getNoEntryCountAndList(ref resultDataSet,objPageSortBo, string.Empty);

            if (resultDataSet.Tables[0].Rows.Count > 0)
            {

                int rows = 0;
                rows = resultDataSet.Tables[0].Rows.Count - 1;
                for (int intRow = 0; intRow <= rows; intRow++)
                {
                    lstOutput.Add(resultDataSet.Tables[0].Rows[intRow].ItemArray);
                }
            }
            return lstOutput;
        }
        #endregion
        #endregion

        #region IListingPage Implementation

        public void loadData()
        {
            DataSet resultDataSet = new DataSet();
            List<object[]> lstOutput = new List<object[]>();
            VoidInspectionsBL objVoidInspectionsBl = new VoidInspectionsBL(new VoidInspectionsRepo());
            PageSortBO objPageSortBo = new PageSortBO("DESC", "Ref", 1, 30);
            objVoidInspectionsBl.getPendingTermination(ref resultDataSet, objPageSortBo, string.Empty);            
            grdPendingTerminationList.DataSource = resultDataSet;
            grdPendingTerminationList.DataBind();
        }

        public void searchData()
        {
            throw new NotImplementedException();
        }

        public void populateData()
        {
            throw new NotImplementedException();
        }

        public void printData()
        {
            throw new NotImplementedException();
        }

        public void exportGridToExcel()
        {
            throw new NotImplementedException();
        }

        public void exportToPdf()
        {
            throw new NotImplementedException();
        }

        public void downloadData()
        {
            throw new NotImplementedException();
        }

        public void applyFilters()
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}