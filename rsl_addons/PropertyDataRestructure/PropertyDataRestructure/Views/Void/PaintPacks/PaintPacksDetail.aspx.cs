﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PropertyDataRestructure.Base;
using PropertyDataRestructure.Interface;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_Utilities.Constants;
using System.Data;
using PDR_BusinessLogic.PaintPacks;
using PDR_DataAccess.PaintPacks;
using System.Threading;
using PDR_BusinessObject.PaintPacks;

namespace PropertyDataRestructure.Views.Void.PaintPacks
{
    public partial class PaintPacksDetail : PageBase, IListingPage, IAddEditPage
    {
        #region Page load event
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                uiMessage.hideMessage();
                if (!IsPostBack && Request.QueryString[PathConstants.PId] != null)
                {
                    this.loadData();
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region grdPaintPacksList RowEditing Event
        protected void grdPaintPacksList_RowEditing(object sender, GridViewEditEventArgs e)
        {
            try
            {
                grdPaintPacksList.EditIndex = e.NewEditIndex;
                DataSet resultDataSet = new DataSet();

                resultDataSet = objSession.PaintDetailDataSet;

                grdPaintPacksList.DataSource = resultDataSet.Tables[ApplicationConstants.PaintPackDetail];
                grdPaintPacksList.DataBind();

            }
            catch (ThreadAbortException ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region grdPaintPacksList RowCancelingEdit Event
        protected void grdPaintPacksList_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            try
            {
                grdPaintPacksList.EditIndex = -1;
                DataSet resultDataSet = new DataSet();

                resultDataSet = objSession.PaintDetailDataSet;

                grdPaintPacksList.DataSource = resultDataSet.Tables[ApplicationConstants.PaintPackDetail];
                grdPaintPacksList.DataBind();
            }
            catch (ThreadAbortException ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region btnSaveLater Click event
        /// <summary>
        /// btnSaveLater Click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSaveLater_Click(object sender, EventArgs e)
        {
            try
            {
                if (validateData())
                {
                    this.saveData();

                }
            }
            catch (ThreadAbortException ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region btnAmend Click Event Handler
        /// <summary>
        /// btnUpdate Click Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void btnAmend_Click(Object sender, EventArgs e)
        {
            try
            {
                Button btnUpdate = (Button)sender;
                string paintPackId = btnUpdate.CommandArgument.ToString();
                GridViewRow row = (GridViewRow)btnUpdate.NamingContainer;
                PaintPacksBO objPaintPacksBo = new PaintPacksBO();
                objPaintPacksBo = fillPaintPacksObject(row);
                objPaintPacksBo.PaintPackDetailId = Convert.ToInt32(paintPackId);
                PaintPacksBL objPaintPacksBl = new PaintPacksBL(new PaintPacksRepo());
                objPaintPacksBl.amendPaintPacks(objPaintPacksBo);
                grdPaintPacksList.EditIndex = -1;
                this.loadData();
            }
            catch (ThreadAbortException ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region btnBack Click
        /// <summary>
        /// btnBack Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(PathConstants.PaintPackList);
            }
            catch (ThreadAbortException ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region IListingPage Implementation
        public void loadData()
        {
            uiMessage.hideMessage();

            int pId = Convert.ToInt32(Request.QueryString[PathConstants.PId]);

            PaintPacksBL objPaintPacksBl = new PaintPacksBL(new PaintPacksRepo());
            DataSet resultDataSet = new DataSet();
            resultDataSet = objPaintPacksBl.getPaintPacksDetail(pId);
            objSession.PaintDetailDataSet = resultDataSet;

            grdPaintPacksList.DataSource = resultDataSet.Tables[ApplicationConstants.PaintPackDetail];
            grdPaintPacksList.DataBind();
            this.populateDropDowns();
            this.populateData();

        }

        public void searchData()
        {
            throw new NotImplementedException();
        }

        public void populateData()
        {
            DataSet resultDataSet = new DataSet();
            resultDataSet = objSession.PaintDetailDataSet;
            if (resultDataSet.Tables[ApplicationConstants.PaintPackHeader].Rows.Count > 0)
            {
                DataRow row = resultDataSet.Tables[ApplicationConstants.PaintPackHeader].Rows[0];
                lblAddress.Text = row["Address"].ToString();
                lblTermination.Text = row["Termination"].ToString();
                lblRelet.Text = row["Relet"].ToString();
                lblVacatingTenant.Text = row["VacatingTenant"].ToString();
                lblVacatingTenantTel.Text = row["VacatingTelephone"].ToString();
                lblNewTenant.Text = row["NewTenant"] != null && row["NewTenant"].ToString() != string.Empty ? row["NewTenant"].ToString() : "N/A";
                lblNewTenantTel.Text = row["NewTelephone"].ToString();
                if(row["StatusId"].ToString()!= "-1")
                ddlStatus.SelectedValue = row["StatusId"].ToString();
                if (row["Supplier"].ToString() != "-1")
                ddlSupplier.SelectedValue = row["Supplier"].ToString();

                txtDeliveryDue.Text = row["DeliveryDueDate"].ToString();
            }
            else
            {
                uiMessage.showErrorMessage(UserMessageConstants.ProblemLoadingData);
            }
        }
        public void printData()
        {
            throw new NotImplementedException();
        }

        public void exportGridToExcel()
        {
            throw new NotImplementedException();
        }

        public void exportToPdf()
        {
            throw new NotImplementedException();
        }

        public void downloadData()
        {
            throw new NotImplementedException();
        }

        public void applyFilters()
        {
            throw new NotImplementedException();
        }
        #endregion

        #region IAddEditPage Implementation
        public void populateDropDown(DropDownList ddl)
        {
            throw new NotImplementedException();
        }

        public void populateDropDowns()
        {
            DataSet resultDataSet = new DataSet();
            resultDataSet = objSession.PaintDetailDataSet;
            ddlSupplier.DataSource = resultDataSet.Tables[ApplicationConstants.PaintPackSupplier];
            ddlSupplier.DataTextField = "Description";
            ddlSupplier.DataValueField = "Id";
            ddlSupplier.DataBind();

            ddlStatus.DataSource = resultDataSet.Tables[ApplicationConstants.PaintStatus];
            ddlStatus.DataTextField = "Title";
            ddlStatus.DataValueField = "StatusId";
            ddlStatus.DataBind();

            ListItem newListItem = default(ListItem);
            newListItem = new ListItem("Please select", "-1");
            ddlSupplier.Items.Insert(0, newListItem);
            ddlStatus.Items.Insert(0, newListItem);
        }

        public void saveData()
        {

            int pId = Convert.ToInt32(Request.QueryString[PathConstants.PId]);
            PaintPacksBO objPaintPacksBo = new PaintPacksBO();
            PaintPacksBL objPaintPacksBl = new PaintPacksBL(new PaintPacksRepo());
            objPaintPacksBo.PaintPackId = pId;
            objPaintPacksBo.Supplier = Convert.ToInt32(ddlSupplier.SelectedValue);
            objPaintPacksBo.DeliveryDue = Convert.ToDateTime(txtDeliveryDue.Text.Trim());
            objPaintPacksBo.Status = Convert.ToInt32(ddlStatus.SelectedValue);
            objPaintPacksBl.updatePaintPacks(objPaintPacksBo);
            uiMessage.showInformationMessage(UserMessageConstants.PaintPacksSaveSuccessfuly);

        }

        public bool validateData()
        {
            if (Convert.ToInt32(ddlSupplier.SelectedItem.Value) == -1)
            {
                uiMessage.showErrorMessage(UserMessageConstants.SelectSupplier);

                return false;
            }
            if (Convert.ToInt32(ddlStatus.SelectedItem.Value) == -1)
            {
                uiMessage.showErrorMessage(UserMessageConstants.SelectActionStatus);

                return false;
            }

            if (txtDeliveryDue.Text == string.Empty)
            {
                uiMessage.showErrorMessage(UserMessageConstants.NotValidDeliveryDateFormat);

                return false;
            }
            return true;
        }

        public void resetControls()
        {
            throw new NotImplementedException();
        }
        #endregion


        #region fill PaintPacks Object to save data
        public PaintPacksBO fillPaintPacksObject(GridViewRow row)
        {
            PaintPacksBO objPaintPacksBO = new PaintPacksBO();


            TextBox txtPaintRef = (TextBox)row.FindControl("txtPaintRef");
            TextBox txtPaintColor = (TextBox)row.FindControl("txtPaintColor");
            TextBox txtPaintName = (TextBox)row.FindControl("txtPaintName");
            objPaintPacksBO.PaintRef = txtPaintRef.Text.Trim();
            objPaintPacksBO.PaintColor = txtPaintColor.Text.Trim();
            objPaintPacksBO.PaintName = txtPaintName.Text.Trim();


            return objPaintPacksBO;
        }
        #endregion

        protected void btnSaveOrder_Click(object sender, EventArgs e)
        {
            try
            {
                if (validateData())
                {
                    this.saveData();
                    this.ucAssignToContractor.populateControl(Convert.ToInt32(ddlSupplier.SelectedValue), ddlSupplier.SelectedItem.Text);
                    mdlPopupAssignToContractor.Show();

                }
            }
            catch (ThreadAbortException ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }



    }
}