﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Pdr.Master" AutoEventWireup="true"
    CodeBehind="PaintPacksDetail.aspx.cs" Inherits="PropertyDataRestructure.Views.Void.PaintPacks.PaintPacksDetail" %>

<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<%@ Register TagName="AssignToContractor" TagPrefix="ucAssignToContractor" Src="~/UserControls/Void/PaintPacks/PaintPacksAssignToContractor.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../../Styles/scheduling.css" rel="stylesheet" type="text/css" />
    <link href="../../../Styles/default.css" rel="stylesheet" type="text/css" />
    <link href="../../../Styles/Site.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
       <asp:UpdateProgress ID="updateProgressReportsArea" runat="server" DisplayAfter="10">
        <ProgressTemplate>
            <div class="loading-image">
                <img alt="Please Wait" src="../../../Images/menu/progress.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel runat="server" ID="updPanelPaintPacks">
        <ContentTemplate>
            <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="600px" />
            <p style="background-color: Black; height: 22px; text-align: justify; font-family: Tahoma;
                font-weight: bold; margin: 0 0 6px; font-size: 15px; padding: 8px;">
                <font color="white">
                    <asp:Label runat="server" ID="lblHeading" Text="Paint Packs"></asp:Label></font>
            </p>
            <div class="mainContainer">
                <div style="margin-bottom: 5px; padding: 10px; border: 1px solid;">
                    <table width="90%">
                        <tr>
                            <td style="width: 50%;">
                                <b>Address:</b>
                            </td>
                            <td style="width: 25%;">
                                <b>Termination:</b>
                            </td>
                            <td style="width: 25%;">
                                <b>Relet:</b>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 50%;">
                                <asp:Label Text="" ID="lblAddress" runat="server" />
                            </td>
                            <td style="width: 25%;">
                                <asp:Label Text="" ID="lblTermination" runat="server" />
                            </td>
                            <td style="width: 25%;">
                                <asp:Label Text="" ID="lblRelet" runat="server" />
                            </td>
                        </tr>
                    </table>
                </div>
                <table width="90%">
                    <tr>
                        <td style="width: 20%;">
                            <b>Vacating Tenant :</b>
                        </td>
                        <td style="width: 25%;">
                            <asp:Label Text="" ID="lblVacatingTenant" runat="server" />
                        </td>
                        <td style="width: 20%;">
                            <b>New Tenant:</b>
                        </td>
                        <td style="width: 25%;">
                            <asp:Label Text="" ID="lblNewTenant" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 20%;">
                            <b>Vacating Tenant Tel:</b>
                        </td>
                        <td style="width: 25%;">
                            <asp:Label Text="" ID="lblVacatingTenantTel" runat="server" />
                        </td>
                        <td style="width: 20%;">
                            <b>New Tenant Tel:</b>
                        </td>
                        <td style="width: 25%;">
                            <asp:Label Text="" ID="lblNewTenantTel" runat="server" />
                        </td>
                    </tr>
                </table>
                <hr />
                <div style="height: 280px; overflow: auto; border-bottom: 1px solid Black;">
                    <b>Paint Packs:</b>
                    <cc1:PagingGridView ID="grdPaintPacksList" runat="server" AllowPaging="false" AllowSorting="true"
                        AutoGenerateColumns="False" BorderStyle="Solid" BorderWidth="0px" CellPadding="4"
                        GridLines="None" PageSize="10" Width="100%" PagerSettings-Visible="false" HeaderStyle-Font-Underline="false"
                        ShowHeaderWhenEmpty="true" EmptyDataText="No Record Found" OnRowEditing="grdPaintPacksList_RowEditing"
                        OnRowCancelingEdit="grdPaintPacksList_RowCancelingEdit">
                        <Columns>
                            <asp:TemplateField HeaderText="Room:" SortExpression="Room">
                                <ItemTemplate>
                                    <asp:Label ID="lblRoom" runat="server" Text='<%# Bind("Room") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle Width="20%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Paint Ref:" SortExpression="PaintRef">
                                <ItemTemplate>
                                    <asp:Label ID="lblPaintRef" runat="server" Text='<%# Bind("PaintRef") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox runat="server" ID="txtPaintRef" Text='<%# Bind("PaintRef") %>' />
                                </EditItemTemplate>
                                <ItemStyle Width="15%" HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Paint Colour:" SortExpression="PaintColour">
                                <ItemTemplate>
                                    <asp:Label ID="lblPaintColour" runat="server" Text='<%# Bind("PaintColor") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox runat="server" ID="txtPaintColor" Text='<%# Bind("PaintColor") %>' />
                                </EditItemTemplate>
                                <ItemStyle Width="20%" HorizontalAlign="Left" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Paint Name:" SortExpression="PaintName">
                                <ItemTemplate>
                                    <asp:Label ID="lblPaintName" runat="server" Text='<%# Bind("PaintName") %>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox runat="server" ID="txtPaintName" Text='<%# Bind("PaintName") %>' />
                                </EditItemTemplate>
                                <ItemStyle HorizontalAlign="Left" Width="20%" />
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:Button ID="btnSelect" runat="server" CausesValidation="false" Text="Amend" CommandName="Edit"
                                        CommandArgument='<%# Eval("PaintPackDetailId") %>' />
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:Button ID="btnAmend" runat="server" CausesValidation="false" Text="Save" CommandName="Update"
                                        CommandArgument='<%# Eval("PaintPackDetailId") %>' OnClick="btnAmend_Click" />
                                    <asp:Button ID="btnCancel" runat="server" CausesValidation="false" Text="Cancel"
                                        CommandName="Cancel" CommandArgument='<%# Eval("PaintPackDetailId") %>' />
                                </EditItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <HeaderStyle BackColor="#ffffff" CssClass="table-head" Font-Bold="True" ForeColor="black"
                            HorizontalAlign="Left" Font-Underline="false" />
                        <AlternatingRowStyle BackColor="#E8E9EA" Wrap="True" />
                    </cc1:PagingGridView>
                </div>
                <table width="100%">
                    <tr>
                        <td style="width: 20%; padding-left: 5px;">
                            <b>Supplier:<span class="Required">*</span></b>
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlSupplier" Width="250">
                                <asp:ListItem Text="Please Select" />
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator1" ControlToValidate="ddlSupplier"
                                InitialValue="-1" ErrorMessage="*" Display="Dynamic" ValidationGroup="later"
                                CssClass="Required" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 20%; padding-left: 5px;">
                            <b>Delivery Due:<span class="Required">*</span></b>
                        </td>
                        <td>
                            <asp:TextBox runat="server" ID="txtDeliveryDue" Style="width: 180px !important;" />
                            <ajax:CalendarExtender ID="clndrbuildDate" runat="server" Format="dd/MM/yyyy" PopupButtonID="imgCalDate"
                                PopupPosition="Right" TargetControlID="txtDeliveryDue" TodaysDateFormat="dd/MM/yyyy">
                            </ajax:CalendarExtender>
                            <asp:ImageButton ID="imgCalDate" runat="server" ImageUrl="~/Images/calendar.png"
                                Style="vertical-align: bottom; border: none;" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 20%; padding-left: 5px;">
                            <b>Status:<span class="Required">*</span></b>
                        </td>
                        <td>
                            <asp:DropDownList runat="server" ID="ddlStatus" Width="250">
                                <asp:ListItem Text="Please Select" />
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator2" ControlToValidate="ddlStatus"
                                InitialValue="-1" ErrorMessage="*" Display="Dynamic" ValidationGroup="later"
                                CssClass="Required" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div style="float: right; width: 100%; margin-right: 18px;">
                                <asp:Button ID="btnSaveOrder" runat="server" CssClass="margin_right20" Text="Save & Order"
                                    ValidationGroup="save" onclick="btnSaveOrder_Click" />
                                <asp:Button ID="btnSaveLater" Text="Save for Later" runat="server" CssClass="margin_right20"
                                    ValidationGroup="later" OnClick="btnSaveLater_Click" />
                                
                                <asp:Button Text="< Back to Paint Packs" ID="btnBack" runat="server" CssClass="margin_right20" OnClick="btnBack_Click" />
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    
       <asp:Button ID="btnHiddenAssigntoContractor" runat="server" Text="" Style="display: none;" />
        <ajax:ModalPopupExtender ID="mdlPopupAssignToContractor" runat="server" TargetControlID="btnHiddenAssigntoContractor"
            PopupControlID="pnlAssignToContractor" Enabled="true" DropShadow="true" BackgroundCssClass="modalBackground" CancelControlID="imgBtnClosePopup">
        </ajax:ModalPopupExtender>
        <asp:Panel ID="pnlAssignToContractor" runat="server" Style="min-width: 600px; max-width: 700px;">
            <asp:ImageButton ID="imgBtnClosePopup" 
                runat="server" Style="position: absolute; top: -12px; right: -12px;" ImageAlign="Top"
                ImageUrl="~/Images/cross2.png" BorderWidth="0" />
            <div style="width: 100%; height: 630px; overflow: auto;">
                <ucAssignToContractor:AssignToContractor ID="ucAssignToContractor" runat="server" />
            </div>
        </asp:Panel>
</asp:Content>
