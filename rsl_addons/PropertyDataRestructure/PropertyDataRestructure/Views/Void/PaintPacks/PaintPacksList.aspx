﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Pdr.Master" AutoEventWireup="true"
    CodeBehind="PaintPacksList.aspx.cs" Inherits="PropertyDataRestructure.Views.Void.PaintPacks.PaintPacksList" %>

<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="updateProgressReportsArea" runat="server" DisplayAfter="10">
        <ProgressTemplate>
            <div class="loading-image">
                <img alt="Please Wait" src="../../../Images/menu/progress.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel runat="server" ID="updPanelPaintPacksList">
        <ContentTemplate>
            <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="400px" />
            <p style="background-color: Black; height: 22px; text-align: justify; font-family: Tahoma;
                font-weight: bold; margin: 0 0 6px; font-size: 15px; padding: 8px;">
                <font color="white">
                    <asp:Label runat="server" ID="lblHeading" Text="Paint Packs"></asp:Label></font>
            </p>
            <div style="border: 1px solid black;">
                <div style="height: 580px; overflow: auto;">
                    <cc1:PagingGridView ID="grdPaintPacksList" runat="server" AllowPaging="false" AllowSorting="true"
                        AutoGenerateColumns="False" BorderStyle="Solid" BorderWidth="0px" CellPadding="4" OnSorting="grdPaintPacksList_Sorting"
                        GridLines="None" PageSize="10" Width="100%" PagerSettings-Visible="false" HeaderStyle-Font-Underline="false"
                        ShowHeaderWhenEmpty="true" EmptyDataText="No Record Found">
                        <Columns>
                            <asp:TemplateField HeaderText="Address:" SortExpression="Address">
                                <ItemTemplate>
                                    <asp:Label ID="lblAddress" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle Width="25%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="New Tenant Name" SortExpression="NewTenant">
                                <ItemTemplate>
                                    <asp:Label ID="lblNewTenant" runat="server" Text='<%# Bind("Tenant") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="20%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Telephone:" SortExpression="Telephone">
                                <ItemTemplate>
                                    <asp:Label ID="lblTelephone" runat="server" Text='<%# Bind("Telephone") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="20%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Paint Packs:" SortExpression="PaintPacks">
                                <ItemTemplate>
                                    <asp:Label ID="lblPaintPacks" runat="server" Text='<%# Bind("PaintPacks") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="10%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status:" SortExpression="Status">
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" Width="10%" />
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:Button ID="btnUpdate" runat="server" CausesValidation="false" Text="Update"
                                        CommandName="Select" CommandArgument='<%# Eval("PaintPackId") %>' OnClick="btnUpdate_Click" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <HeaderStyle BackColor="#ffffff" CssClass="table-head" Font-Bold="True" ForeColor="black"
                            HorizontalAlign="Left" Font-Underline="false" />
                        <AlternatingRowStyle BackColor="#E8E9EA" Wrap="True" />
                    </cc1:PagingGridView>
                </div>
                <asp:Panel runat="server" ID="pnlPagination" Visible="true" Style="border-top: 1px solid  #000;
                    vertical-align: middle; padding: 10px 0">
                    <table style="width: 57%; margin: 0 auto">
                        <tbody>
                            <tr>
                                <td>
                                    <asp:LinkButton ID="lnkbtnPagerFirst" Text="" runat="server" CommandName="Page" CommandArgument="First"
                                        OnClick="lnkbtnPager_Click" Style="text-decoration: none;">&lt;&lt;First</asp:LinkButton>
                                    &nbsp;
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkbtnPagerPrev" Text="" runat="server" CommandName="Page" CommandArgument="Prev"
                                        OnClick="lnkbtnPager_Click" Style="text-decoration: none;">&lt;Prev</asp:LinkButton>
                                    &nbsp;
                                </td>
                                <td>
                                    Page:&nbsp;
                                    <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="currentPage" />
                                    &nbsp;of&nbsp;
                                    <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />. Records:
                                    <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                                    &nbsp;to&nbsp;
                                    <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                                    &nbsp;of&nbsp;
                                    <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page"
                                        CommandArgument="Next" OnClick="lnkbtnPager_Click" Style="text-decoration: none;" />
                                    &nbsp;
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page"
                                        CommandArgument="Last" OnClick="lnkbtnPager_Click" Style="text-decoration: none;" />
                                    &nbsp;
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </asp:Panel>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
