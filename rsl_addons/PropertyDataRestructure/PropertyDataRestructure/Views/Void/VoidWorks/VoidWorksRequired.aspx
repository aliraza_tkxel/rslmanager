﻿<%@ Page Title="Void Required Works" Language="C#" MasterPageFile="~/MasterPage/Pdr.Master" Culture="en-GB"
    AutoEventWireup="true" CodeBehind="VoidWorksRequired.aspx.cs" Inherits="PropertyDataRestructure.UserControls.Void.VoidWorksRequired"
    ValidateRequest="false" EnableEventValidation="false" %>

<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc2" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<%@ Register TagName="AssignToContractor" TagPrefix="ucAssignToContractor" Src="~/UserControls/Void/VoidWorks/VoidWorksAssignToContractor.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        #tblVoidWorks td
        {
            padding-bottom: 5px;
        }
         input[disabled="disabled"][type="button"]
        {
            color:Gray;
           
        }
        input[disabled="disabled"][type="submit"]
        {
            color: Gray;
        }
    </style>
    <script type="text/javascript" language="javascript">

        /////// Selection Work /////// 

        function checkAllWorks(Checkbox) {

            var GridVwHeaderChckbox = document.getElementById("<%=grdVoidWorksRequired.ClientID %>");

            for (i = 1; i < GridVwHeaderChckbox.rows.length; i++) {

                if (GridVwHeaderChckbox.rows[i].cells[0].getElementsByTagName("INPUT")[0].value == "True") {
                    GridVwHeaderChckbox.rows[i].cells[0].getElementsByTagName("INPUT")[2].checked = Checkbox.checked;
                }
            }

        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="updateProgressReportsArea" runat="server" DisplayAfter="10">
        <ProgressTemplate>
            <div class="loading-image">
                <img alt="Please Wait" src="../../../Images/menu/progress.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="updPanelVoidWorksRequired" runat="server">
        <ContentTemplate>
            <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="400px" />
            <p style="background-color: Black; height: 22px; text-align: justify; font-family: Tahoma;
                font-weight: bold; margin: 0 0 6px; font-size: 15px; padding: 8px;">
                <font color="white">
                    <asp:Label runat="server" ID="lblHeading" Text="Void Works"></asp:Label>
                    <asp:Label Text="" ID="lblAddress" runat="server" />
                </font>
            </p>
            <table style="width: 100%; margin-left: 2px;">
                <tr>
                    <td style="width: 200px; padding-right: 0px !important;">
                        Termination:
                        <asp:Label Text="" runat="server" ID="lblTermination" Font-Bold="true" />
                    </td>
                    <td style="width: 200px; padding-right: 0px !important;">
                        Recorded by:
                        <asp:Label Text="" runat="server" ID="lblRecordedBy" Font-Bold="true" />
                    </td>
                    <td style="width: 300px; padding-right: 0px !important; text-align: right;">
                        <div style="float: right; margin-top: 10px;">
                            <asp:Button ID="btnBack" runat="server" CssClass="margin_right" Text="< Back" OnClick="btnBack_Click" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="width: 200px; padding-right: 0px !important;">
                        Relet:
                        <asp:Label Text="" runat="server" ID="lblRelet" Font-Bold="true" />
                    </td>
                    <td style="width: 200px; padding-right: 0px !important;">
                        Recorded :
                        <asp:Label Text="" runat="server" ID="lblRecordedOn" Font-Bold="true" />
                    </td>
                    <td style="width: 300px; padding-right: 0px !important; text-align: right;">
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
            <div style="width: 100%; z-index: 1;">
                <asp:LinkButton ID="lnkBtnAptbaTab" CssClass="TabClickedVoid" runat="server" BorderStyle="Solid"
                    BorderColor="Black" Enabled="false">Works Required: </asp:LinkButton>
                <div style="float: right; margin-top: 10px;">
                    <asp:Button ID="btnSchedule" runat="server" CssClass="margin_right" Text="Schedule Selected Work"
                        OnClick="btnSchedule_Click" />
                    <asp:Button ID="btnAssignToContractor" runat="server" CssClass="margin_right20" 
                        Text="Assign Selected Work to Contractor" 
                        onclick="btnAssignToContractor_Click" />
                </div>
            </div>
            <div style="border: 1px solid black; height: 625px; clear: both; margin-left: 2px; overflow: auto; width: 99.7%;">
                <div style="min-height: 600px; clear: both; margin-left: 2px; width: 99.8%; overflow: auto;">
                    <div style="min-height: 580px; clear: both;">
                        <cc2:PagingGridView ID="grdVoidWorksRequired" runat="server" AllowPaging="false"
                            OnRowDataBound="grdVoidWorksRequired_RowDataBound" ShowFooter="true" AllowSorting="true"
                            AutoGenerateColumns="False" BorderStyle="Solid" BorderWidth="0px" CellPadding="4"
                            GridLines="None" PageSize="10" Width="100%" PagerSettings-Visible="false" HeaderStyle-Font-Underline="false"
                            ShowHeaderWhenEmpty="true" EmptyDataText="No Record Found">
                            <Columns>
                                <asp:TemplateField ItemStyle-Width="40px">
                                    <HeaderTemplate>
                                        <asp:CheckBox ID="chkSelectAll" runat="server" onclick="checkAllWorks(this);" />
                                    </HeaderTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:HiddenField ID="hdnScheduled" runat="server" Value='<%# checkScheduledItem(Eval("Checked").ToString()) %>' />
                                        <asp:HiddenField ID="hdnRequiredWorksId" runat="server" Value='<%# Bind("RequiredWorksId") %>' />
                                        <asp:HiddenField ID="hdnWorkType" runat="server" Value='<%# Bind("WorkType") %>' />
                                        <asp:CheckBox ID="chkScheduledItem" runat="server" onclick="" Visible='<%# checkScheduledItem(Eval("Checked").ToString()) %>'>
                                        </asp:CheckBox>
                                    </ItemTemplate>
                                    <ItemStyle Width="5%" HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="JSV:" SortExpression="JSV" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:Label ID="lblJsv" runat="server" Text='<%# Bind("Ref") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="5%" />
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Location:" SortExpression="Location">
                                    <ItemTemplate>
                                        <asp:Label ID="lblLocation" runat="server" Text='<%# Bind("Location") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="15%" />
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Detail:" SortExpression="Detail">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPOSTCODE" runat="server" Text='<%# Bind("WorkDescription") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="15%" />
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Appointment(Operative):" SortExpression="Appointment">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAppointment" runat="server" Text='<%# Bind("Appointment") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="10%" HorizontalAlign="Center" />
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Contractor:" SortExpression="Contractor">
                                    <ItemTemplate>
                                        <asp:Label ID="lblContractor" runat="server" Text='<%# Bind("Contractor") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="10%" HorizontalAlign="Left" />
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Left" />
                                    <FooterTemplate>
                                        <asp:Label ID="lblTotalTenantNeglect" Text="Total Tenant Neglect:" runat="server" />
                                    </FooterTemplate>
                                    <FooterStyle Width="12%" HorizontalAlign="Left" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Neglect:" SortExpression="Neglect">
                                    <ItemTemplate>
                                        <asp:Label ID="lblNeglect" runat="server" Text='<%# Bind("Neglect") %>'></asp:Label>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <div style="padding: 0 0 5px 0">
                                            <asp:Label ID="lblTotalNeglet" runat="server" Font-Bold="true" /></div>
                                    </FooterTemplate>
                                    <ItemStyle Width="10%" HorizontalAlign="Right" />
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Right" />
                                    <FooterStyle HorizontalAlign="Right" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Tenant to Complete:">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkTenantToComplete" runat="server" AutoPostBack="true" Checked='<%# Bind("IsTenantWorks") %>'
                                            OnCheckedChanged="chkTenantToComplete_Click"></asp:CheckBox>
                                    </ItemTemplate>
                                    <ItemStyle Width="5%" HorizontalAlign="Center" />
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgBtnEdit" runat="server" BorderStyle="None" Height="16" Width="16"
                                            BorderWidth="0px" ImageUrl='<%# "~/Images/editv.png" %>' OnClick="imgBtnEditWork_Click"
                                            CommandArgument='<%# Eval("RequiredWorksId") %>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="10%" HorizontalAlign="Center" />
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                </asp:TemplateField>
                            </Columns>
                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                            <HeaderStyle BackColor="#ffffff" CssClass="table-head" Font-Bold="True" ForeColor="black"
                                HorizontalAlign="Left" Font-Underline="false" />
                            <AlternatingRowStyle BackColor="#E8E9EA" Wrap="True" />
                            <FooterStyle CssClass="pdr-gridFooter" />
                        </cc2:PagingGridView>
                    </div>
                    <asp:Panel runat="server" ID="pnlPagination" Visible="true" Style="border-top: 1px solid  #000;
                        vertical-align: middle; padding: 10px 0">
                        <table style="width: 57%; margin: 0 auto">
                            <tbody>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkbtnPagerFirst" Text="" runat="server" CommandName="Page" CommandArgument="First"
                                            OnClick="lnkbtnPager_Click" Style="text-decoration: none;">&lt;&lt;First</asp:LinkButton>
                                        &nbsp;
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lnkbtnPagerPrev" Text="" runat="server" CommandName="Page" CommandArgument="Prev"
                                            OnClick="lnkbtnPager_Click" Style="text-decoration: none;">&lt;Prev</asp:LinkButton>
                                        &nbsp;
                                    </td>
                                    <td>
                                        Page:&nbsp;
                                        <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" CssClass="currentPage" />
                                        &nbsp;of&nbsp;
                                        <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />. Records:
                                        <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                                        &nbsp;to&nbsp;
                                        <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                                        &nbsp;of&nbsp;
                                        <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lnkbtnPagerNext" Text="Next>" runat="server" CommandName="Page"
                                            CommandArgument="Next" OnClick="lnkbtnPager_Click" Style="text-decoration: none;" />
                                        &nbsp;
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lnkbtnPagerLast" Text="Last>>" runat="server" CommandName="Page"
                                            CommandArgument="Last" OnClick="lnkbtnPager_Click" Style="text-decoration: none;" />
                                        &nbsp;
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </asp:Panel>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <ajax:ModalPopupExtender ID="mdlpopupEditWorks" runat="server" PopupControlID="pnlEditVoidworks"
        TargetControlID="lblHiddenEntry" CancelControlID="btnClose" BackgroundCssClass="modalBackground">
    </ajax:ModalPopupExtender>
    <asp:Label Text="" ID="lblHiddenEntry" runat="server" />
    <asp:Panel ID="pnlEditVoidworks" runat="server" CssClass="modalPopupSchedular" Style="width: 380px;
        border-color: black; border-bottom-style: solid; border-width: 1px;">
        <asp:Panel runat="server" ID="pnlAddInspection">
            <uim:UIMessage ID="uiMessage1" runat="Server" Visible="false" width="350px" />
            <div style="height: auto; clear: both; padding: 10px;">
                <asp:UpdatePanel runat="server" ID="updPnlEditVoidWork">
                    <ContentTemplate>
                        <table id="tblVoidWorks" style="width: 400px; text-align: left; margin-right: 10px;">
                            <tr>
                                <td colspan="2">
                                    <b>Void Works</b>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <hr class="InspectionArrangeHr" style='width: 390px !important;' />
                                    <div style="margin-left: -20px!important;">
                                        <uim:UIMessage ID="uiMessagePopUp" runat="Server" Visible="false" width="350px" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    JSV:
                                </td>
                                <td>
                                    <asp:Label Text="" runat="server" ID="lblJsv" />
                                    <asp:HiddenField ID="hdnRequiredWork" runat="server" />
                                    <asp:HiddenField ID="hdnJournalId" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <br />
                                    Location:<span class="Required">*</span>
                                </td>
                                <td>
                                    <br />
                                    <asp:DropDownList ID="ddlLocation" runat="server" Width="235px">
                                        <asp:ListItem Selected="True" Text="Please Select" Value="0"></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator runat="server" ID="RequiredFieldValidator8" ControlToValidate="ddlLocation"
                                        InitialValue="-1" ErrorMessage="*" Display="Dynamic" ValidationGroup="save" CssClass="Required" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Detail:
                                </td>
                                <td>
                                    <asp:TextBox ID="txtDetail" runat="server" TextMode="MultiLine" Height="100" Width="230px" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Appointment:
                                </td>
                                <td>
                                    <asp:Label Text="01/01/2015 09:00" runat="server" ID="lblAppointment" />
                                    <asp:Button Text="Rearrange" ID="btnRearrange" runat="server" Style='margin-right: 30px;
                                        float: right;' OnClick="btnRearrange_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Operative:
                                </td>
                                <td>
                                    <asp:Label Text="" runat="server" ID="lblOperative" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Contractor:
                                </td>
                                <td>
                                    <asp:Label Text="" ID="lblContractor" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Tenant Neglect:
                                </td>
                                <td>
                                    <asp:Label Text="" ID="lblNeglect" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Tenant To Complete:
                                </td>
                                <td>
                                    <asp:CheckBox Text="Yes" runat="server" ID="chkTenantToComplete" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div style="float: right; width: 100%; margin-right: 18px;">
                                        <asp:Button ID="btnCancelAppointment" runat="server" CssClass="margin_right20" Text="Cancel Appointment"
                                            OnClick="btnCancelAppointment_Click" />
                                        <asp:Button ID="btnCancelWork" runat="server" CssClass="margin_right" Text="Cancel Works"
                                            OnClick="btnCancelWork_Click" />
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div style="float: right; width: 80%; margin-right: 18px; margin-top: 5px;">
                                        <asp:Button ID="btnSave" runat="server" CssClass="margin_right20" Text="Save" OnClick="btnSave_Click"
                                            ValidationGroup="save" />
                                        <asp:Button ID="btnClose" runat="server" CssClass="margin_right" Text="Close" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="btnClose" />
                    </Triggers>
                </asp:UpdatePanel>
            </div>
        </asp:Panel>
    </asp:Panel>
    <ajax:ModalPopupExtender ID="mdlpopupAppointmentCancelled" runat="server" PopupControlID="pnlAppointmentCancelled"
        TargetControlID="lblHiddenAppointmentCancelled" CancelControlID="btnCancel" BackgroundCssClass="modalBackground">
    </ajax:ModalPopupExtender>
    <asp:Label Text="" ID="lblHiddenAppointmentCancelled" runat="server" />
    <asp:Panel ID="pnlAppointmentCancelled" runat="server" CssClass="modalPopupSchedular"
        Style="width: 380px; border-color: black; border-bottom-style: solid; border-width: 1px;">
        <div style="height: auto; clear: both;">
            <table id="Table1" style="width: 400px; text-align: left; margin-right: 10px;">
                <tr>
                    <td>
                        <b>Confirmation Alert!</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <hr class="InspectionArrangeHr" />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:UpdatePanel runat="server" ID="updPnlSuccessMessage" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Label Text="" ID="lblConfirmationMessage" runat="server" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td style="padding-left: 38px;">
                        <asp:Button Text="Continue" ID="btnContinue" runat="server" CssClass="margin_right20"
                            OnClick="btnContinue_Click" />
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="margin_right20" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>


       <asp:Button ID="btnHiddenAssigntoContractor" runat="server" Text="" Style="display: none;" />
        <ajax:ModalPopupExtender ID="mdlPopupAssignToContractor" runat="server" TargetControlID="btnHiddenAssigntoContractor"
            PopupControlID="pnlAssignToContractor" Enabled="true" DropShadow="False" BackgroundCssClass="modalBackground" CancelControlID="imgBtnClosePopup">
        </ajax:ModalPopupExtender>
        <asp:Panel ID="pnlAssignToContractor" runat="server" Style="min-width: 600px; max-width: 700px;">
            <asp:ImageButton ID="imgBtnClosePopup" 
                runat="server" Style="position: absolute; top: -12px; right: -12px;" ImageAlign="Top"
                ImageUrl="~/Images/cross2.png" BorderWidth="0" />
            <div style="width: 100%; height: 630px; overflow: auto;">
                <ucAssignToContractor:AssignToContractor ID="ucAssignToContractor" runat="server" />
            </div>
        </asp:Panel>
</asp:Content>
