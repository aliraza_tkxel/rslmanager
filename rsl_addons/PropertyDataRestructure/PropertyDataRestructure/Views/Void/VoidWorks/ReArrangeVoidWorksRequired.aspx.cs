﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_BusinessObject.PageSort;
using PropertyDataRestructure.Base;
using PropertyDataRestructure.Interface;
using PDR_Utilities.Helpers;
using PDR_BusinessLogic.VoidInspections;
using PDR_DataAccess.VoidInspections;
using System.Data;
using PDR_Utilities.Constants;
using System.Threading;
using PDR_BusinessObject.RequiredWorks;
using PDR_BusinessLogic.Scheduling;
using PDR_DataAccess.Scheduling;
using PDR_Utilities.Managers;

namespace PropertyDataRestructure.UserControls.Void
{
    public partial class ReArrangeVoidWorksRequired : PageBase, IListingPage, IAddEditPage
    {
        #region Properties

        PageSortBO objPageSortBo = new PageSortBO("DESC", "RequiredWorksId", 1, 30);
        RequiredWorksBO objRequiredWorksBo = new RequiredWorksBO();
        public decimal totalNeglet = 0;


        #endregion

        #region Events
        #region Page load event
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                uiMessage.hideMessage();
                this.ucAssignToContractor.delegateRefreshParentGrid = new Action(loadData);
                if (!IsPostBack)
                {

                    this.loadData();
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region Pager Event Handler
        /// <summary>
        /// Pager event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkbtnPager_Click(object sender, EventArgs e)
        {
            try
            {
                PageSortBO objPageSortBo = new PageSortBO("DESC", "RequiredWorksId", 1, 30);
                pageSortViewState = GridHelper.processGridPager(sender, objPageSortBo, pageSortViewState);
                loadData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region "grdVoidWorksRequired Sorting"
        protected void grdVoidWorksRequired_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                objPageSortBo = pageSortViewState;

                objPageSortBo.SortExpression = e.SortExpression;
                objPageSortBo.PageNumber = 1;
                grdVoidWorksRequired.PageIndex = 0;
                objPageSortBo.setSortDirection();

                pageSortViewState = objPageSortBo;
                loadData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #region grdVoidWorksRequired RowDataBound event
        /// <summary>
        /// grdVoidWorksRequired RowDataBound event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdVoidWorksRequired_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    string negletValue = ((Label)e.Row.FindControl("lblNeglect")).Text;
                    decimal totalvalue = Convert.ToDecimal(negletValue);
                    ((Label)e.Row.FindControl("lblNeglect")).Text = "£" + totalvalue.ToString("N");

                    //if (!((CheckBox)e.Row.FindControl("chkTenantToComplete")).Checked)
                    //    totalNeglet += totalvalue;

                    //string workType = ((HiddenField)e.Row.FindControl("hdnWorkType")).Value;
                    //ImageButton imgEditButton = (ImageButton)e.Row.FindControl("imgBtnEdit");
                    //if (workType.Equals(ApplicationConstants.SaniClean)
                    //    || workType.Equals(ApplicationConstants.StandardVoidWorks))
                    //{
                    //    imgEditButton.Visible = false;
                    //}

                }
                //if (e.Row.RowType == DataControlRowType.Footer)
                //{
                //    Label lblTotalNeglet = (Label)e.Row.FindControl("lblTotalNeglet");
                //    lblTotalNeglet.Text = "£" + totalNeglet.ToString("N");
                //}
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region Button Rearrange Event Handler
        /// <summary>
        /// Button Schedule Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void BtnRearrange_Click(Object sender, EventArgs e)
        {
            try
            {
                Button btnSchedule = (Button)sender;
                int journalId = Convert.ToInt32(btnSchedule.CommandArgument);
                navigateToReScheduleWorksRequired(journalId);
            }
            catch (ThreadAbortException Ex)
            {

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region Navigate to ReSchedule Required Works page
        /// <summary>
        /// Navigate to ReSchedule Required Works page
        /// </summary>
        /// <remarks></remarks>
        public void navigateToReScheduleWorksRequired(int jid)
        {
            objSession.RequiredWorksDs = null;
            int journalId = Convert.ToInt32(Request.QueryString[PathConstants.JId]);
            Response.Redirect(PathConstants.ReScheduleRequiredWorks + "?tab=aptArranged&src=rsl&pdrjid=" + journalId + "&jid=" + jid.ToString());
        }
        #endregion

        #region chkTenantToComplete Event Handler
        /// <summary>
        /// chkTenantToComplete Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void chkTenantToComplete_Click(Object sender, EventArgs e)
        {
            try
            {
                CheckBox chkTenantToComplete = (CheckBox)sender;
                GridViewRow row = (GridViewRow)chkTenantToComplete.NamingContainer;
                HiddenField hdnRequiredWorksId = (HiddenField)row.FindControl("hdnRequiredWorksId");
                int wId = Convert.ToInt32(hdnRequiredWorksId.Value);
                VoidInspectionsBL objVoidInspectionsBl = new VoidInspectionsBL(new VoidInspectionsRepo());
                string isSaved = string.Empty;
                isSaved = objVoidInspectionsBl.updateTenantToComplete(wId, chkTenantToComplete.Checked);

                if (isSaved.Equals(ApplicationConstants.NotSaved))
                {
                    uiMessage.showErrorMessage(UserMessageConstants.TenantToCompleteFailed);
                }
                else
                {
                    decimal negletValue = 0;
                    Label lblNeglect = ((Label)row.FindControl("lblNeglect"));
                    negletValue = Convert.ToDecimal(lblNeglect.Text.Substring(1));
                    isSaved = "1";
                    if (negletValue > 0)
                        isSaved = objVoidInspectionsBl.addTenantRecharges(wId, chkTenantToComplete.Checked, negletValue);
                    if (isSaved.Equals(ApplicationConstants.NotSaved))
                    {
                        uiMessage.showErrorMessage(UserMessageConstants.RequiredWorksUpdatedFailed);
                    }
                    else
                    {
                        uiMessage.showInformationMessage(UserMessageConstants.TenantToCompleteSuccess);
                    }
                }
                loadData();

            }
            catch (ThreadAbortException ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region Button imgBtnEditWork Event Handler
        /// <summary>
        /// Button imgBtnEditWork Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void imgBtnEditWork_Click(Object sender, EventArgs e)
        {
            try
            {
                ImageButton btnSchedule = (ImageButton)sender;
                int requiredWorksId = Convert.ToInt32(btnSchedule.CommandArgument);
                this.populateDropDowns();
                this.populateRequiredWorksPopUp(requiredWorksId);


            }
            catch (ThreadAbortException ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
                mdlpopupEditWorks.Show();
            }
        }
        #endregion

        #region btnSave Click event
        /// <summary>
        /// btnSave Click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                saveData();
                loadData();
                mdlpopupEditWorks.Show();

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region btnCancelWork Click event
        /// <summary>
        /// btnCancelWork Click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancelWork_Click(object sender, EventArgs e)
        {
            try
            {
                populateRequiredWorksPopUp(Convert.ToInt32(hdnRequiredWork.Value));
                objRequiredWorksBo.IsCanceled = true;
                saveData();
                loadData();
                uiMessagePopUp.showInformationMessage(UserMessageConstants.RequiredWorksCanceledSuccessfuly);
                mdlpopupEditWorks.Show();
                btnCancelWork.Enabled = false;
                btnSave.Enabled = false;
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region btnBack Click
        /// <summary>
        /// btnBack Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(PathConstants.VoidAppointments);
            }
            catch (ThreadAbortException ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region btnSchedule Click
        /// <summary>
        /// btnSchedule Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSchedule_Click(object sender, EventArgs e)
        {

            try
            {
                List<RequiredWorksBO> checkedTempWorksList = new List<RequiredWorksBO>();

                if ((!checkAnySelected(ref checkedTempWorksList)))
                {
                    uiMessage.showErrorMessage(UserMessageConstants.SelectRequiredWorks);
                }
                else
                {
                    objSession.VoidAppointmentBO = null;
                    objSession.RequiredWorksBOList = checkedTempWorksList;
                    this.navigateToScheduleRequiredWorks();
                }
            }
            catch (ThreadAbortException ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region btnRearrange click evet
        /// <summary>
        /// btnRearrange click evet
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRearrange_Click(object sender, EventArgs e)
        {
            try
            {
                List<RequiredWorksBO> checkedTempWorksList = new List<RequiredWorksBO>();

                if (fillRequiredWorksBoListForRearrange(ref checkedTempWorksList))
                {
                    objSession.VoidAppointmentBO = null;
                    objSession.RequiredWorksBOList = checkedTempWorksList;
                    this.navigateToReScheduleWorksRequired();
                }

            }
            catch (ThreadAbortException ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #region btnCancelAppointment click event
        /// <summary>
        /// btnCancelAppointment click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnCancelAppointment_Click(object sender, EventArgs e)
        {
            try
            {
                int journalId = Convert.ToInt32(hdnJournalId.Value);
                DataSet resultDataSet = objSession.RequiredWorksDs;

                DataRow[] result = resultDataSet.Tables[ApplicationConstants.RequiredWorkDt].Select("JOURNALID=" + journalId.ToString());
                if (result.Count() > 1)
                {
                    lblConfirmationMessage.Text = UserMessageConstants.MultipleWorkAppointmentCancelMessage;
                }
                else
                {
                    lblConfirmationMessage.Text = UserMessageConstants.AppointmentCancelMessage;
                }
                updPnlSuccessMessage.Update();
                mdlpopupAppointmentCancelled.Show();
                mdlpopupEditWorks.Show();
            }
            catch (ThreadAbortException ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region btnContinue click event
        protected void btnContinue_Click(object sender, EventArgs e)
        {
            try
            {
                VoidSchedulingBL objSchedulingBL = new VoidSchedulingBL(new SchedulingRepo());
                int journalId = Convert.ToInt32(hdnJournalId.Value);
                int isCancelled = objSchedulingBL.cancelVoidAppointment(journalId);

                if (isCancelled == ApplicationConstants.Cancelled)
                {
                    uiMessagePopUp.showInformationMessage(UserMessageConstants.AppointmentCancelledSuccessfully);
                    btnCancelWork.Enabled = false;
                    btnCancelAppointment.Enabled = false;
                    btnSave.Enabled = false;
                    btnRearrange.Enabled = false;
                    loadData();
                    mdlpopupEditWorks.Show();

                }
                else
                {
                    uiMessagePopUp.showErrorMessage(UserMessageConstants.AppointmentCancelledFailed);
                }

            }
            catch (ThreadAbortException ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region btnAssignToContractor Click Event
        /// <summary>
        /// btnAssignToContractor Click Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAssignToContractor_Click(object sender, EventArgs e)
        {
            try
            {
                List<RequiredWorksBO> checkedTempWorksList = new List<RequiredWorksBO>();

                if ((!checkAnySelected(ref checkedTempWorksList)))
                {
                    uiMessage.showErrorMessage(UserMessageConstants.SelectRequiredWorks);
                }
                else
                {
                    objSession.VoidAppointmentBO = null;
                    objSession.RequiredWorksBOList = checkedTempWorksList;
                    ucAssignToContractor.populateControl();
                    mdlPopupAssignToContractor.Show();
                }
            }
            catch (ThreadAbortException ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }

        #endregion

        #endregion


        #region IListingPage Implementation
        public void loadData()
        {
            uiMessage.hideMessage();

            int journalId = Convert.ToInt32(Request.QueryString[PathConstants.JId]);

            PageSortBO objPageSortBo;
            if (pageSortViewState == null)
            {
                objPageSortBo = new PageSortBO("DESC", "RequiredWorksId", 1, 30);
                pageSortViewState = objPageSortBo;
            }
            else
            {
                objPageSortBo = pageSortViewState;
            }

            int totalCount = 0;
            VoidInspectionsBL objVoidInspectionsBl = new VoidInspectionsBL(new VoidInspectionsRepo());
            DataSet resultDataSet = new DataSet();
            totalCount = objVoidInspectionsBl.getArrangedVoidWorksRequired(ref resultDataSet, objPageSortBo, journalId);
            objSession.RequiredWorksDs = resultDataSet;
            grdVoidWorksRequired.DataSource = resultDataSet.Tables[ApplicationConstants.RequiredWorkDt];
            grdVoidWorksRequired.DataBind();
            objPageSortBo.TotalRecords = totalCount;
            objPageSortBo.TotalPages = Convert.ToInt32(Math.Ceiling((Double)totalCount / objPageSortBo.PageSize));

            pageSortViewState = objPageSortBo;
            GridHelper.setGridViewPager(ref pnlPagination, objPageSortBo);
            this.populateData();
        }

        public void searchData()
        {
            throw new NotImplementedException();
        }

        public void populateData()
        {
            DataSet resultDataSet = objSession.RequiredWorksDs;
            if (resultDataSet.Tables[ApplicationConstants.RecordedWorkDt].Rows.Count > 0)
            {
                lblAddress.Text = "> " + resultDataSet.Tables[ApplicationConstants.RecordedWorkDt].Rows[0]["Address"];
                lblTermination.Text = resultDataSet.Tables[ApplicationConstants.RecordedWorkDt].Rows[0]["Termination"].ToString();
                lblRecordedBy.Text = resultDataSet.Tables[ApplicationConstants.RecordedWorkDt].Rows[0]["Recorded"].ToString();
                lblRecordedOn.Text = resultDataSet.Tables[ApplicationConstants.RecordedWorkDt].Rows[0]["CreatedDate"].ToString();
                lblRelet.Text = resultDataSet.Tables[ApplicationConstants.RecordedWorkDt].Rows[0]["ReletDate"].ToString();
                objSession.PatchId = Convert.ToInt32(resultDataSet.Tables[ApplicationConstants.RecordedWorkDt].Rows[0]["PatchId"]);

            }
        }

        public void printData()
        {
            throw new NotImplementedException();
        }

        public void exportGridToExcel()
        {
            throw new NotImplementedException();
        }

        public void exportToPdf()
        {
            throw new NotImplementedException();
        }

        public void downloadData()
        {
            throw new NotImplementedException();
        }

        public void applyFilters()
        {
            throw new NotImplementedException();
        }
        #endregion


        #region check Scheduled Item
        public bool checkScheduledItem(string val)
        {
            bool result = false;
            if (val == "1")
                result = true;
            return result;
        }
        #endregion

        #region populateRequiredWorksPopUp
        private void populateRequiredWorksPopUp(int requiredWorksId)
        {
            uiMessagePopUp.hideMessage();
            btnSave.Enabled = false;
            btnCancelWork.Enabled = false;
            DataSet resultDataSet = objSession.RequiredWorksDs;
            if (resultDataSet.Tables[ApplicationConstants.RequiredWorkDt].Rows.Count > 0)
            {
                var requiredWorkDt = resultDataSet.Tables[ApplicationConstants.RequiredWorkDt].AsEnumerable();
                var requiredWorkResult = (from app in requiredWorkDt
                                          where app["RequiredWorksId"].ToString() == requiredWorksId.ToString()
                                          select app);
                if (requiredWorkResult.Count() > 0)
                {
                    hdnRequiredWork.Value = requiredWorkResult.First()["RequiredWorksId"].ToString();
                    hdnJournalId.Value = requiredWorkResult.First()["JOURNALID"].ToString();
                    ddlLocation.SelectedValue = requiredWorkResult.First()["LocationId"].ToString();
                    lblJsv.Text = requiredWorkResult.First()["Ref"].ToString();
                    txtDetail.Text = requiredWorkResult.First()["WorkDescription"].ToString();
                    lblAppointment.Text = requiredWorkResult.First()["AppointmentStartDateTime"].ToString();
                    lblOperative.Text = requiredWorkResult.First()["Operative"].ToString();
                    lblContractor.Text = requiredWorkResult.First()["Contractor"].ToString();
                    lblNeglect.Text = GeneralHelper.currencyFormat(requiredWorkResult.First()["Neglect"].ToString());
                    chkTenantToComplete.Checked = Convert.ToBoolean(requiredWorkResult.First()["IsTenantWorks"].ToString());
                    string workStatus = requiredWorkResult.First()["WorkStatus"].ToString().Trim();
                    if (workStatus == ApplicationConstants.StatusArranged || workStatus == ApplicationConstants.AssignToContractorStatus)
                    {
                        btnCancelAppointment.Enabled = true;
                        btnRearrange.Enabled = true;
                    }
                    else
                    {
                        btnRearrange.Enabled = false;
                        btnCancelAppointment.Enabled = false;
                    }
                    if (workStatus == ApplicationConstants.StatusToBeArranged)
                    {
                        btnSave.Enabled = true;
                        btnCancelWork.Enabled = true;
                    }

                }

            }


        }
        #endregion

        #region IAddEditPage Implementation
        public void saveData()
        {
            objRequiredWorksBo.RequiredWorksId = Convert.ToInt32(hdnRequiredWork.Value);
            objRequiredWorksBo.LocationId = Convert.ToInt32(ddlLocation.SelectedValue);
            objRequiredWorksBo.WorkDescription = txtDetail.Text.Trim();
            objRequiredWorksBo.IsTenantWorks = chkTenantToComplete.Checked;
            objRequiredWorksBo.UpdatedBy = objSession.EmployeeId;
            VoidInspectionsBL objVoidInspectionsBl = new VoidInspectionsBL(new VoidInspectionsRepo());

            string isSaved = string.Empty;
            isSaved = objVoidInspectionsBl.updateRequiredWorks(objRequiredWorksBo);
            if (isSaved.Equals(ApplicationConstants.NotSaved))
            {
                uiMessage.showErrorMessage(UserMessageConstants.RequiredWorksUpdatedFailed);
            }
            else
            {
                decimal negletValue = 0;
                negletValue = Convert.ToDecimal(lblNeglect.Text.Substring(1));
                isSaved = "1";
                if (negletValue > 0)
                    isSaved = objVoidInspectionsBl.addTenantRecharges(objRequiredWorksBo.RequiredWorksId, objRequiredWorksBo.IsTenantWorks, negletValue);
                if (isSaved.Equals(ApplicationConstants.NotSaved))
                {
                    uiMessage.showErrorMessage(UserMessageConstants.RequiredWorksUpdatedFailed);
                }
                else
                {
                    uiMessagePopUp.showInformationMessage(UserMessageConstants.RequiredWorksUpdatedSuccessfuly);
                }
            }
        }

        public bool validateData()
        {
            throw new NotImplementedException();
        }

        public void resetControls()
        {
            throw new NotImplementedException();
        }

        public void populateDropDown(DropDownList ddl)
        {
            throw new NotImplementedException();
        }

        public void populateDropDowns()
        {
            VoidInspectionsBL objInspectionsBl = new VoidInspectionsBL(new VoidInspectionsRepo());
            DataSet resultDataSet = new DataSet();
            objInspectionsBl.getVoidAreaList(ref resultDataSet);

            ddlLocation.DataSource = resultDataSet;
            ddlLocation.DataValueField = "AreaId";
            ddlLocation.DataTextField = "AreaName";
            ddlLocation.DataBind();
            ListItem newListItem = default(ListItem);
            newListItem = new ListItem("Please select", "-1");
            ddlLocation.Items.Insert(0, newListItem);

        }
        #endregion

        #region check Any checkbox Selected
        public bool checkAnySelected(ref List<RequiredWorksBO> checkedTempWorksList)
        {
            CheckBox chkScheduledItem;
            HiddenField hdnScheduled;
            HiddenField hdnRequiredWorksId;
            DataSet resultDataSet = objSession.RequiredWorksDs;
            foreach (GridViewRow basketGridRows in grdVoidWorksRequired.Rows)
            {
                hdnScheduled = (HiddenField)basketGridRows.FindControl("hdnScheduled");
                if (Convert.ToBoolean(hdnScheduled.Value) == true)
                {
                    RequiredWorksBO objRequiredWorksBo = new RequiredWorksBO();
                    chkScheduledItem = (CheckBox)basketGridRows.FindControl("chkScheduledItem");
                    if (chkScheduledItem.Checked)
                    {
                        hdnRequiredWorksId = (HiddenField)basketGridRows.FindControl("hdnRequiredWorksId");
                        var requiredWorkDt = resultDataSet.Tables[ApplicationConstants.RequiredWorkDt].AsEnumerable();
                        var requiredWorkResult = from app in requiredWorkDt
                                                 where app["RequiredWorksId"].ToString() == hdnRequiredWorksId.Value.ToString()
                                                 select app;
                        if (requiredWorkResult.Count() > 0)
                        {
                            objRequiredWorksBo.RequiredWorksId = Convert.ToInt32(requiredWorkResult.First()["RequiredWorksId"]);
                            objRequiredWorksBo.WorkDescription = requiredWorkResult.First()["WorkDescription"].ToString();
                            objRequiredWorksBo.Location = requiredWorkResult.First()["Location"].ToString();
                            objRequiredWorksBo.Neglect = Convert.ToDecimal(requiredWorkResult.First()["Neglect"]);
                            objRequiredWorksBo.InspectionJournalId = Convert.ToInt32(Request.QueryString[PathConstants.JId]);
                            objRequiredWorksBo.TerminationDate = Convert.ToDateTime(resultDataSet.Tables[ApplicationConstants.RecordedWorkDt].Rows[0]["Termination"]);
                            objRequiredWorksBo.ReletDate = Convert.ToDateTime(resultDataSet.Tables[ApplicationConstants.RecordedWorkDt].Rows[0]["ReletDate"]);
                            objRequiredWorksBo.Ref = resultDataSet.Tables[ApplicationConstants.RecordedWorkDt].Rows[0]["Ref"].ToString();
                            objRequiredWorksBo.Address = resultDataSet.Tables[ApplicationConstants.RecordedWorkDt].Rows[0]["Address"].ToString();
                            objRequiredWorksBo.AppointmentNotes = requiredWorkResult.First()["AppointmentNotes"].ToString();
                            objRequiredWorksBo.JobSheetNotes = requiredWorkResult.First()["JobSheetNotes"].ToString();
                            objRequiredWorksBo.WorkType = requiredWorkResult.First()["WorkType"].ToString();
                            objRequiredWorksBo.Duration = (objRequiredWorksBo.WorkType.Equals(ApplicationConstants.SaniClean) || objRequiredWorksBo.WorkType.Equals(ApplicationConstants.StandardVoidWorks)
                                ? ApplicationConstants.DefaultSaniCleanDuration : Decimal.Parse(requiredWorkResult.First()["Duration"].ToString()));
                            checkedTempWorksList.Add(objRequiredWorksBo);
                        }
                    }
                }
            }
            if (checkedTempWorksList.Count > 0)
            {
                return true;
            }
            return false;
        }
        #endregion

        #region "Navigate to Schedule Required Works page"
        /// <summary>
        /// Navigate to ME Job sheet
        /// </summary>
        /// <remarks></remarks>
        public void navigateToScheduleRequiredWorks()
        {
            Response.Redirect(PathConstants.ScheduleRequiredWorks);
        }
        #endregion

        #region "Navigate to ReSchedule Required Works page"
        /// <summary>
        /// Navigate to ME Job sheet
        /// </summary>
        /// <remarks></remarks>
        public void navigateToReScheduleWorksRequired()
        {
            Response.Redirect(PathConstants.ReScheduleRequiredWorks);
        }
        #endregion

        #region fill Required  Works  List For Rearrange
        public bool fillRequiredWorksBoListForRearrange(ref List<RequiredWorksBO> checkedWorksList)
        {

            int journalId = Convert.ToInt32(hdnJournalId.Value);


            DataSet resultDataSet = objSession.RequiredWorksDs;

            DataRow[] result = resultDataSet.Tables[ApplicationConstants.RequiredWorkDt].Select("JOURNALID=" + journalId.ToString());

            foreach (DataRow row in result)
            {
                RequiredWorksBO objRequiredWorksBo = new RequiredWorksBO();
                objRequiredWorksBo.RequiredWorksId = Convert.ToInt32(row["RequiredWorksId"]);
                objRequiredWorksBo.WorkDescription = row["WorkDescription"].ToString();
                objRequiredWorksBo.Location = row["Location"].ToString();
                objRequiredWorksBo.Neglect = Convert.ToDecimal(row["Neglect"]);
                objRequiredWorksBo.InspectionJournalId = Convert.ToInt32(row["InspectionJournalId"]);
                objRequiredWorksBo.JournalId = Convert.ToInt32(row["JOURNALID"]);
                objRequiredWorksBo.TerminationDate = Convert.ToDateTime(row["Termination"]);
                objRequiredWorksBo.ReletDate = Convert.ToDateTime(row["ReletDate"]);
                objRequiredWorksBo.Ref = row["Ref"].ToString();
                objRequiredWorksBo.Address = row["Address"].ToString();
                objRequiredWorksBo.AppointmentId = Convert.ToInt32(row["AppointmentId"]);
                objRequiredWorksBo.AppointmentNotes = row["AppointmentNotes"].ToString();
                objRequiredWorksBo.JobSheetNotes = row["JobSheetNotes"].ToString();
                objSession.PatchId = Convert.ToInt32(row["PatchId"]);
                checkedWorksList.Add(objRequiredWorksBo);
            }

            if (checkedWorksList.Count > 0)
            {
                return true;
            }
            return false;
        }
        #endregion








    }
}