﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_Utilities.Constants;
using PropertyDataRestructure.Base;
using System.Data;
using PDR_BusinessLogic.VoidInspections;
using PDR_DataAccess.VoidInspections;
using System.Threading;

namespace PropertyDataRestructure.Views.Void
{
    public partial class VoidAppointments : PageBase
    {
        VoidInspectionsBL objVoidInspectionsBl = new VoidInspectionsBL(new VoidInspectionsRepo());
        #region Events
        #region Page load event
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                uiMessage.hideMessage();
                if (!IsPostBack)
                {
                    objSession.SearchText = null;
                    BindPatchFilterValues();
                    objSession.Patch = 0;
                    if (Request.QueryString[PathConstants.ListingTab] == PathConstants.ArrangedTab)
                    {
                        this.showVoidWorksArranged();
                    }
                    else
                    {                       
                        ucVoidWorksToBeArrangedTab.loadData();
                        if (Request.QueryString[PathConstants.Source] == PathConstants.Rsl && Request.QueryString[PathConstants.JId] != null)
                        {
                            if (Request.QueryString[PathConstants.CustomerId] != null)
                            {
                                objSession.CustomerId = Convert.ToInt32(Request.QueryString[PathConstants.CustomerId]);
                            }
                            populateAlertDataForVoidWorks(int.Parse(Request.QueryString[PathConstants.JId]));
                            mdlpopupScheduler.Show();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region Link Button Inspections to be Arranged
        /// <summary>
        ///  Link Button Inspections to be Arranged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkBtnAptbaTab_Click(object sender, EventArgs e)
        {
            try
            {
                lnkBtnAptbaTab.CssClass = ApplicationConstants.TabClickedCssClass;
                lnkBtnAptTab.CssClass = ApplicationConstants.TabInitialCssClass;
                MainView.ActiveViewIndex = 0;
                ucVoidWorksToBeArrangedTab.loadData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        #endregion

        #region
        protected void populateAlertDataForVoidWorks(int inspectionJournalId)
        {
            OperativeText.InnerHtml = "";
            DataSet resultDataSet = new DataSet();
            objVoidInspectionsBl.getVoidWorksbyinspectionjournalId(ref resultDataSet, inspectionJournalId);
            if (resultDataSet.Tables[0] != null && resultDataSet.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < resultDataSet.Tables[0].Rows.Count; i++)
                {
                    var status = resultDataSet.Tables[0].Rows[i][ApplicationConstants.StatusColumn].ToString().Trim();
                    if (status == ApplicationConstants.StatusArranged)
                    {
                        DataSet operativeResultSet = new DataSet();
                        objVoidInspectionsBl.getOperativeDataForAlert(ref operativeResultSet, int.Parse(resultDataSet.Tables[0].Rows[i][ApplicationConstants.WorksJournalIdColumn].ToString()));
                        if (operativeResultSet.Tables[0] != null && operativeResultSet.Tables[0].Rows.Count > 0)
                        {
                            var data = "Operative : " + operativeResultSet.Tables[0].Rows[0][ApplicationConstants.OperativeNameColumn].ToString() + "<br/>";
                            data = data + "Mobile : " + operativeResultSet.Tables[0].Rows[0][ApplicationConstants.MobileColumn].ToString() + "<br/>";
                            data = data + "DD Number : " + operativeResultSet.Tables[0].Rows[0][ApplicationConstants.DDNumberColumn].ToString() + "<br/><br/>";
                            OperativeText.InnerHtml = OperativeText.InnerHtml + data;
                        }
                    }
                }
            }
        }
        #endregion

        #region Link Button Inspections Arranged
        /// <summary>
        /// Link Button Inspections Arranged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkBtnAptTab_Click(object sender, EventArgs e)
        {
            try
            {
                showVoidWorksArranged();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        #endregion

        #region txtSearchBox TextChanged
        protected void txtSearchBox_TextChanged(object sender, EventArgs e)
        {
            try
            {

                searchResults();

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion
        #endregion

        #region Functions
        #region Search Results
        public void searchResults()
        {
            objSession.SearchText = txtSearchBox.Text.Trim();
            if (MainView.ActiveViewIndex == 0)
            {

                ucVoidWorksToBeArrangedTab.loadData();
            }
            else
            {
                ucVoidWorksArrangedTab.loadData();
            }

        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {

                Response.Redirect(PathConstants.CustomerModule + objSession.CustomerId.ToString());
            }
            catch (ThreadAbortException ex)
            {

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);

                }
            }
        }

        #region ReArrange Void Works Now
        protected void btnNow_Click(object sender, EventArgs e)
        {
            try
            {
                uiMessage.hideMessage();
                mdlpopupScheduler.Hide();

                int journalId = Convert.ToInt32(Request.QueryString[PathConstants.JId]);

                string url = PathConstants.RearrangeVoidWorksRequiredPath + journalId.ToString();
                string s = "window.open('" + ResolveClientUrl(url) + "', '_blank');";
                ScriptManager.RegisterClientScriptBlock(this.Page, typeof(Page), Guid.NewGuid().ToString(), s, true);
                //Response.Redirect(PathConstants.CustomerModule + objSession.CustomerId.ToString());
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion


        #endregion

        #region Show Void Works Arranged Tab
        private void showVoidWorksArranged()
        {
            lnkBtnAptbaTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnAptTab.CssClass = ApplicationConstants.TabClickedCssClass;
            MainView.ActiveViewIndex = 1;
            ucVoidWorksArrangedTab.loadData();
        }
        #endregion

        #region Bind Patch filter values
        public void BindPatchFilterValues()
        {
            DataSet resultDataSet = new DataSet();
           
            objVoidInspectionsBl.getPatchData(ref resultDataSet);

            if (resultDataSet.Tables[0].Rows.Count > 0)
            {
                ddlPatchFilter.DataSource = resultDataSet.Tables[0];
                ddlPatchFilter.DataTextField = "LOCATION";
                ddlPatchFilter.DataValueField = "PATCHID";
                ddlPatchFilter.DataBind();
                ddlPatchFilter.Items.Insert(0, new ListItem("All", "0"));
            }



        }
        #endregion

        #region Filter result on patch
        protected void getPatchBasedResults_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            objSession.Patch = int.Parse(ddlPatchFilter.SelectedItem.Value);
            if (MainView.ActiveViewIndex == 0)
            {
                ucVoidWorksToBeArrangedTab.FilterResultOnPatch();
            }
            else
            {
                ucVoidWorksArrangedTab.FilterResultOnPatch();
            }
        }

        #endregion
        #endregion
    }
}