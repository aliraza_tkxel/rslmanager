﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PDR_BusinessObject.RequiredWorks;
using PropertyDataRestructure.Base;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_Utilities.Constants;
using System.Threading;
using PDR_BusinessLogic.Scheduling;
using PDR_DataAccess.Scheduling;
using System.Data;
using PDR_BusinessObject.TradeAppointment;
using PDR_Utilities.Helpers;
using PDR_BusinessObject.VoidAppointment;
using PDR_BusinessObject.PageSort;
using PDR_BusinessLogic.VoidInspections;
using PDR_DataAccess.VoidInspections;

namespace PropertyDataRestructure.Views.Void.VoidWorks
{
    public partial class RearrangeWorksRequired : PageBase
    {



        #region Events
        #region Page load event
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                uiMessage.hideMessage();
                if (!IsPostBack)
                {
                    int journalId = Convert.ToInt32(Request.QueryString[PathConstants.JId]);
                    if (journalId > 0)
                    {

                        this.getRequiredWorksForRearrange(journalId);

                        btnBack.Visible = false;
                        btnNoEntry.Visible = true;
                        if (Request.QueryString[PathConstants.ListingTab] == PathConstants.ArrangedTab && Request.QueryString[PathConstants.Source] == PathConstants.Rsl)
                        {
                            btnNoEntry.Text = "< Back to Void Works Required";
                        }
                        else if (Request.QueryString[PathConstants.ListingTab] == PathConstants.ArrangedTab)
                        {
                            btnNoEntry.Text = "< Back to Void Works Arranged";
                        }
                    }
                    else
                    {
                        populateSelectedRequiredWorks();
                        btnBack.Visible = true;
                        btnNoEntry.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region Button imgBtnEdit  Event Handler
        /// <summary>
        /// Button imgBtnEdit Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void imgBtnEdit_Click(Object sender, EventArgs e)
        {
            try
            {
                ImageButton btnEdit = (ImageButton)sender;
                int requiredWorksId = Convert.ToInt32(btnEdit.CommandArgument);


                GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
                Panel pnlDuration = (Panel)row.FindControl(ApplicationConstants.DurationPanelControlId);

                ImageButton imgBtnEdit = (ImageButton)pnlDuration.FindControl(ApplicationConstants.EditImageButtonControlId);
                Label lblDuration = (Label)pnlDuration.FindControl(ApplicationConstants.DurationLabelControlId);
                DropDownList ddlDuration = (DropDownList)pnlDuration.FindControl(ApplicationConstants.DurationDropdownControlId);
                ImageButton imgBtnDone = (ImageButton)pnlDuration.FindControl(ApplicationConstants.DoneImageButtonControlId);
                HiddenField hdnDuration = (HiddenField)pnlDuration.FindControl(ApplicationConstants.DurationHiddenControlId);

                imgBtnEdit.Visible = false;
                lblDuration.Visible = false;
                ddlDuration.Visible = true;
                imgBtnDone.Visible = true;
                ddlDuration.SelectedValue = hdnDuration.Value;

            }
            catch (ThreadAbortException ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region grdRequiredWorksDetail Row Data Bound event
        /// <summary>
        /// grdRequiredWorksDetail Row Data Bound event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdRequiredWorksDetail_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {

                    Label lblDuration = ((Label)e.Row.FindControl(ApplicationConstants.DurationLabelControlId));
                    lblDuration.Text = (Convert.ToDecimal(lblDuration.Text) > 1 ? Convert.ToString(lblDuration.Text) + " hours" : Convert.ToString(lblDuration.Text) + " hour");
                    lblDuration.Visible = true;
                    DropDownList ddlDuration = (DropDownList)e.Row.FindControl(ApplicationConstants.DurationDropdownControlId);
                    populateDurationDropdownList(ref ddlDuration);
                    ddlDuration.Visible = false;

                    string negletValue = ((Label)e.Row.FindControl("lblNeglect")).Text;
                    decimal totalvalue = Convert.ToDecimal(negletValue);
                    ((Label)e.Row.FindControl("lblNeglect")).Text = "£" + totalvalue.ToString("N");
                }

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region grdOperative Row Data Bound event
        /// <summary>
        /// grdOperative Row Data Bound event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdOperative_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    TextBox txtStartDate = e.Row.FindControl("txtStartDate") as TextBox;
                    List<RequiredWorksBO> checkedTempWorksList = new List<RequiredWorksBO>();
                    checkedTempWorksList = objSession.RequiredWorksBOList;
                    if (checkedTempWorksList[0].StartDate != null)
                    {
                        txtStartDate.Text = checkedTempWorksList[0].StartDate.ToString("dd/MM/yyyy");
                    }
                }

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region Button imgBtnDoneWork Event Handler
        /// <summary>
        /// Button imgBtnDoneWork Event Handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void imgBtnDone_Click(Object sender, EventArgs e)
        {
            try
            {
                ImageButton btnEdit = (ImageButton)sender;
                int requiredWorksId = Convert.ToInt32(btnEdit.CommandArgument);

                GridViewRow row = (GridViewRow)btnEdit.NamingContainer;
                Panel pnlDuration = (Panel)row.FindControl(ApplicationConstants.DurationPanelControlId);
                //find the control to edit duration and save in session using list of RequiredWorksBO
                ImageButton imgBtnEdit = (ImageButton)pnlDuration.FindControl(ApplicationConstants.EditImageButtonControlId);
                Label lblDuration = (Label)pnlDuration.FindControl(ApplicationConstants.DurationLabelControlId);
                DropDownList ddlDuration = (DropDownList)pnlDuration.FindControl(ApplicationConstants.DurationDropdownControlId);
                ImageButton imgBtnDone = (ImageButton)pnlDuration.FindControl(ApplicationConstants.DoneImageButtonControlId);
                HiddenField hdnDuration = (HiddenField)pnlDuration.FindControl(ApplicationConstants.DurationHiddenControlId);
                hdnDuration.Value = ddlDuration.SelectedValue;
                lblDuration.Text = (Convert.ToDecimal(ddlDuration.SelectedValue) > 1 ? Convert.ToString(ddlDuration.SelectedValue) + " hours" : Convert.ToString((ddlDuration.SelectedValue) + " hour"));
                //get list of RequiredWorksBO from session
                List<RequiredWorksBO> checkedTempWorksList = new List<RequiredWorksBO>();
                checkedTempWorksList = objSession.RequiredWorksBOList;
                // update duration and start datetime 
                checkedTempWorksList.Where(w => w.RequiredWorksId == requiredWorksId).ToList().ForEach(i => i.Duration = Convert.ToDecimal(ddlDuration.SelectedValue));
                checkedTempWorksList.Select(w => { w.StartDate = DateTime.Now; return w; }).ToList();
                objSession.RequiredWorksBOList = checkedTempWorksList;

                //Update Duration
                DataRow[] worRows = objSession.RequiredWorksDs.Tables[ApplicationConstants.RequiredWorkDt].Select(String.Format("RequiredWorksId = {0}", requiredWorksId));
                if (worRows.Count()>0)
                {
                    worRows[0]["Duration"] = Convert.ToDecimal(ddlDuration.SelectedValue);
                }
                

                //show hide controls according to requirement
                imgBtnEdit.Visible = true;
                lblDuration.Visible = true;
                ddlDuration.Visible = false;
                imgBtnDone.Visible = false;
                reloadCurrentWebPage();

            }
            catch (ThreadAbortException ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region btn refresh click event
        /// <summary>
        /// btn refresh click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnRefresh_Click(object sender, EventArgs e)
        {
            try
            {
                this.addMoreTempAppointments();

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }

            }
        }
        #endregion

        #region btn Go click event
        /// <summary>
        /// btn Go click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnGo_Click(object sender, EventArgs e)
        {
            try
            {
                List<RequiredWorksBO> checkedTempWorksList = new List<RequiredWorksBO>();
                checkedTempWorksList = objSession.RequiredWorksBOList;
                TextBox txtStartDate = (TextBox)grdOperative.HeaderRow.FindControl("txtStartDate");
                if (txtStartDate.Text != string.Empty)
                {
                    DateTime startDate = DateTime.ParseExact(txtStartDate.Text, "dd/MM/yyyy", null);
                    if (DateTime.ParseExact(txtStartDate.Text, "dd/MM/yyyy", null) < DateTime.Today.Date)
                    {

                        uiMessage.showErrorMessage(UserMessageConstants.SelectFutureDate + "<br />");
                        uiMessage.isError = true;

                    }
                    else
                    {
                        checkedTempWorksList.Select(w => { w.StartDate = startDate; return w; }).ToList();
                        objSession.RequiredWorksBOList = checkedTempWorksList;
                        DataSet resultDataset = new DataSet();
                        resultDataset = this.getAvailableOperatives(checkedTempWorksList[0].StartDate);
                        TempAppointmentDtBO tempAptDtBo = new TempAppointmentDtBO();
                        this.populateAvailableOperativesList(resultDataset, tempAptDtBo);
                    }
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }

            }
        }
        #endregion

        #region "btn Select Operative Click"
        protected void btnSelectOperative_Click(object sender, EventArgs e)
        {
            try
            {
                //This a sample string that will be received on this event
                //Sample ---- operative Id:::Operative Name:::Start Date:::End Date:::Trade Id:::Trade Name:::Trade Duration:::Component Trade Id:::Component Name:::BlockId
                //Values ---- 468:::Jade Burrell:::25/02/2013:::25/02/2013:::1:::Plumber:::2:::4:::Bathroom:::1
                string parameters = string.Empty;
                Button btnSelectOperative = (Button)sender;
                parameters = btnSelectOperative.CommandArgument;
                //save the selected appointment information in session
                this.saveSelectedAppointmentInfo(parameters);
                this.navigatetoVoidJobsheet();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region btnBack click event
        /// <summary>
        /// btnBack click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnBack_Click(object sender, EventArgs e)
        {
            try
            {
                List<RequiredWorksBO> checkedTempWorksList = new List<RequiredWorksBO>();
                checkedTempWorksList = objSession.RequiredWorksBOList;
                this.navigatetoVoidWorksRequired(checkedTempWorksList[0].InspectionJournalId);
            }
            catch (ThreadAbortException ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region btnNoEntry click event
        /// <summary>
        /// btnBack click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNoEntry_Click(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString[PathConstants.ListingTab] == PathConstants.ArrangedTab && Request.QueryString[PathConstants.Source] == PathConstants.Rsl)
                {
                    objSession.RequiredWorksBOList = null;
                    int journalId = Convert.ToInt32(Request.QueryString[PathConstants.PdrJId]);
                    Response.Redirect(PathConstants.RearrangeVoidWorksRequiredPath + journalId.ToString());
                }
                else if (Request.QueryString[PathConstants.ListingTab] == PathConstants.ArrangedTab)
                {
                    objSession.RequiredWorksBOList = null;
                    Response.Redirect(PathConstants.VoidAppointments + "?tab=aptArranged");
                }
                else
                {
                    objSession.RequiredWorksBOList = null;
                    Response.Redirect(PathConstants.NoEntryReportPath);

                }
            }
            catch (ThreadAbortException ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion
        #endregion


        #region Functions
        #region populate Selected Required Works
        private void populateSelectedRequiredWorks()
        {
            //get RequiredWorksBO list from session which store in VoidWorksRequired.aspx.cs page
            List<RequiredWorksBO> checkedTempWorksList = new List<RequiredWorksBO>();
            checkedTempWorksList = objSession.RequiredWorksBOList;
            if (checkedTempWorksList.Count > 0)
            {
                grdRequiredWorksDetail.DataSource = checkedTempWorksList;
                grdRequiredWorksDetail.DataBind();


                VoidAppointmentBO objVoidAppointmentBo = objSession.VoidAppointmentBO;
                if (objVoidAppointmentBo.AppointmentId == 0)
                {
                    //get Available Operatives and populate appointment slots                
                    DataSet resultDataset = new DataSet();
                    resultDataset = this.getAvailableOperatives(checkedTempWorksList[0].StartDate);
                    TempAppointmentDtBO tempAptDtBo = new TempAppointmentDtBO();
                    this.populateAvailableOperativesList(resultDataset, tempAptDtBo);
                    btnRefresh.Enabled = true;
                }
                else
                {
                    btnRefresh.Enabled = false;
                    uiMessage.showWarningMessage(UserMessageConstants.AppointmentAlreadyScheduled);
                }
            }
        }
        #endregion

        #region "Populate duration dropdown"
        /// <summary>
        /// Populate duration dropdown
        /// </summary>
        /// <remarks></remarks>
        public void populateDurationDropdownList(ref DropDownList ddl)
        {
            ListItem newListItem = default(ListItem);
            ddl.Items.Clear();
            for (double i = 1; i <= 100; i++)
            {
                double time = i / 2;
                if ((time <= 1))
                {
                    newListItem = new ListItem(Convert.ToString(time) + " hour", time.ToString());
                }
                else
                {
                    newListItem = new ListItem(Convert.ToString(time) + " hours", time.ToString());
                }

                ddl.Items.Add(newListItem);
            }

        }

        #endregion

        #region "get Available Operatives"
        /// <summary>
        /// this function 'll get the available operatives based on following: 
        /// trade ids of component, property id, start date 
        /// </summary>    
        /// <returns></returns>
        /// <remarks></remarks>
        private DataSet getAvailableOperatives(DateTime startDate)
        {
            SchedulingBL schedulingBl = new SchedulingBL(new SchedulingRepo());
            DataSet resultDataSet = new DataSet();


            //this function 'll get the available operatives based on following: trade ids will be null, start date 
            schedulingBl.getAvailableOperatives(ref resultDataSet, string.Empty, ApplicationConstants.VoidMsatType, startDate);
            if (resultDataSet.Tables[0].Rows.Count == 0)
            {
                throw new Exception(UserMessageConstants.OperativesNotFound);
            }
            else
            {
                objSession.AvailableOperativesDs = resultDataSet;
            }
            return resultDataSet;
        }
        #endregion

        #region "Populate available operative"
        /// <summary>
        /// Populate available operative
        /// </summary>
        /// <remarks></remarks>

        public void populateAvailableOperativesList(DataSet availableOperativesDs, TempAppointmentDtBO tempAptDtBo)
        {

            SchedulingBL schedulingBl = new SchedulingBL(new SchedulingRepo());

            List<RequiredWorksBO> checkedTempWorksList = new List<RequiredWorksBO>();
            checkedTempWorksList = objSession.RequiredWorksBOList;
            double totalDuration = checkedTempWorksList.Sum(x => Convert.ToDouble(x.Duration));

            //this function 'll process the duration and 'll create the appointments slots against them
            schedulingBl.createVoidsTempAppointmentSlots(availableOperativesDs.Tables[ApplicationConstants.AvailableOperatives], availableOperativesDs.Tables[ApplicationConstants.OperativesLeaves]
                , availableOperativesDs.Tables[ApplicationConstants.OperativesAppointments], ref tempAptDtBo
                    , checkedTempWorksList[0].StartDate, totalDuration, GeneralHelper.getAppointmentCreationLimitForSingleRequest());

            objSession.AvailableOperativesTimeSlots = tempAptDtBo;

            grdOperative.DataSource = tempAptDtBo.dt;
            grdOperative.DataBind();

        }

        #endregion

        #region add More Temp Appointments slots
        public void addMoreTempAppointments()
        {
            int previousOperativeId = 0;
            SchedulingBL schedulingBl = new SchedulingBL(new SchedulingRepo());
            TempAppointmentDtBO tempAptDtBo = objSession.AvailableOperativesTimeSlots;
            DataSet availableOperativesDs = objSession.AvailableOperativesDs;
            List<RequiredWorksBO> checkedTempWorksList = new List<RequiredWorksBO>();
            checkedTempWorksList = objSession.RequiredWorksBOList;
            int totalDuration = checkedTempWorksList.Sum(x => Convert.ToInt32(x.Duration));

            //fetch the previous operative id so the operatives name should appear in order                
            var appointments = tempAptDtBo.dt.AsEnumerable();
            var appResult = (from app in appointments
                             where Convert.ToBoolean(app[TempAppointmentDtBO.lastAddedColName]) == true
                             select app);
            if (appResult.Count() > 0)
            {
                previousOperativeId = Convert.ToInt32(appResult.LastOrDefault()[TempAppointmentDtBO.operativeIdColName]);
                //This function 'll create the appointment slots based on operatives, appointments, leaves and start date
                //as we have previous operative id , so it 'll continue creating the appointment slots after existing slots
                schedulingBl.createVoidsTempAppointmentSlots(availableOperativesDs.Tables[ApplicationConstants.AvailableOperatives], availableOperativesDs.Tables[ApplicationConstants.OperativesLeaves]
                , availableOperativesDs.Tables[ApplicationConstants.OperativesAppointments], ref tempAptDtBo, checkedTempWorksList[0].StartDate, totalDuration,
                GeneralHelper.getAppointmentCreationLimitForSingleRequest(), previousOperativeId);
            }

            objSession.AvailableOperativesTimeSlots = tempAptDtBo;
            grdOperative.DataSource = tempAptDtBo.dt;
            grdOperative.DataBind();
        }

        #endregion

        #region "Reload the Current Web Page"
        /// <summary>
        /// This function 'll reload the current page. This is being used when user rearrange the scheduled appointment
        /// </summary>
        /// <remarks></remarks>
        private void reloadCurrentWebPage()
        {
            Response.Redirect(Request.RawUrl);
        }
        #endregion

        #region "save Selected Appointment Info"
        /// <summary>
        /// This function 'll save the selected appointment information in session
        /// </summary>
        /// <param name="parameters"></param>
        /// <remarks></remarks>

        private void saveSelectedAppointmentInfo(string parameters)
        {
            VoidAppointmentBO objVoidAppointmentBo = new VoidAppointmentBO();
            string[] separater = { "|" };
            string[] appointmentParameters = parameters.Split(separater, StringSplitOptions.RemoveEmptyEntries);
            objVoidAppointmentBo.OperativeId = int.Parse(appointmentParameters[0]);
            objVoidAppointmentBo.OperativeName = appointmentParameters[1];
            objVoidAppointmentBo.AppointmentStartDate = Convert.ToDateTime(appointmentParameters[2]);
            objVoidAppointmentBo.AppointmentEndDate = Convert.ToDateTime(appointmentParameters[3]);
            objVoidAppointmentBo.UserId = objSession.EmployeeId;
            objVoidAppointmentBo.StartTime = Convert.ToDateTime(objVoidAppointmentBo.AppointmentStartDate).ToString("HH:mm");
            objVoidAppointmentBo.EndTime = Convert.ToDateTime(objVoidAppointmentBo.AppointmentEndDate).ToString("HH:mm");

            objSession.VoidAppointmentBO = objVoidAppointmentBo;

        }
        #endregion

        #region "Navigate to ME Job sheet"
        /// <summary>
        /// Navigate to ME Job sheet
        /// </summary>
        /// <remarks></remarks>
        public void navigatetoVoidJobsheet()
        {
            if (Request.QueryString[PathConstants.Source] == PathConstants.Rsl)
            {
                Response.Redirect(PathConstants.JSVJobSheetSummary + "?src=rearrange&path=rsl");
            }
            else
            {
                Response.Redirect(PathConstants.JSVJobSheetSummary + "?src=rearrange");
            }
            
        }
        #endregion


        #region "Navigate to void required works page"
        /// <summary>
        /// Navigate to ME Job sheet
        /// </summary>
        /// <remarks></remarks>
        public void navigatetoVoidWorksRequired(int jid)
        {
            Response.Redirect(PathConstants.VoidWorksRequired + jid.ToString());
        }
        #endregion


        private void getRequiredWorksForRearrange(int journalId)
        {

            PageSortBO objPageSortBo;
            objPageSortBo = new PageSortBO("DESC", "RequiredWorksId", 1, 30);

            int totalCount = 0;
            VoidInspectionsBL objVoidInspectionsBl = new VoidInspectionsBL(new VoidInspectionsRepo());

            if (objSession.RequiredWorksDs == null)
            {
                DataSet resultDataSet = new DataSet();
                if (Request.QueryString[PathConstants.ListingTab] == PathConstants.ArrangedTab && Request.QueryString[PathConstants.Source] == PathConstants.Rsl)
                {
                    totalCount = objVoidInspectionsBl.getArrangedVoidWorksRequired(ref resultDataSet, objPageSortBo, journalId, true);
                }
                else
                {
                    totalCount = objVoidInspectionsBl.getVoidWorksRequired(ref resultDataSet, objPageSortBo, journalId, true);
                }
                objSession.RequiredWorksDs = resultDataSet;
            }

            List<RequiredWorksBO> checkedTempWorksList = new List<RequiredWorksBO>();

            if (fillRequiredWorksBoListForRearrange(ref checkedTempWorksList, journalId))
            {
                objSession.VoidAppointmentBO = null;
                objSession.RequiredWorksBOList = checkedTempWorksList;
                populateSelectedRequiredWorks();
            }
            else
            {
                uiMessage.showErrorMessage(UserMessageConstants.InvalidJournalId);
            }
        }
        #endregion
        #region fill Required  Works  List For Rearrange
        public bool fillRequiredWorksBoListForRearrange(ref List<RequiredWorksBO> checkedWorksList, int journalId)
        {

            DataSet resultDataSet = objSession.RequiredWorksDs;
            DataRow[] result = resultDataSet.Tables[ApplicationConstants.RequiredWorkDt].Select();

            foreach (DataRow row in result)
            {
                RequiredWorksBO objRequiredWorksBo = new RequiredWorksBO();
                objRequiredWorksBo.RequiredWorksId = Convert.ToInt32(row["RequiredWorksId"]);
                objRequiredWorksBo.WorkDescription = row["WorkDescription"].ToString();
                objRequiredWorksBo.Location = row["Location"].ToString();
                objRequiredWorksBo.Neglect = Convert.ToDecimal(row["Neglect"]);
                objRequiredWorksBo.InspectionJournalId = Convert.ToInt32(row["InspectionJournalId"]);
                objRequiredWorksBo.JournalId = Convert.ToInt32(row["JOURNALID"]);
                objRequiredWorksBo.TerminationDate = Convert.ToDateTime(row["Termination"]);
                objRequiredWorksBo.ReletDate = Convert.ToDateTime(row["ReletDate"]);
                objRequiredWorksBo.Ref = row["Ref"].ToString();
                objRequiredWorksBo.Address = row["Address"].ToString();
                objRequiredWorksBo.AppointmentId = Convert.ToInt32(row["AppointmentId"]);
                objRequiredWorksBo.AppointmentNotes = row["AppointmentNotes"].ToString();
                objRequiredWorksBo.JobSheetNotes = row["JobSheetNotes"].ToString();
                objRequiredWorksBo.Duration = Convert.ToDecimal(row["Duration"]);
                checkedWorksList.Add(objRequiredWorksBo);
            }

            if (checkedWorksList.Count > 0)
            {
                return true;
            }
            return false;
        }
        #endregion
    }
}