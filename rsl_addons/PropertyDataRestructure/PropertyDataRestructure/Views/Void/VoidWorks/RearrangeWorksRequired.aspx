﻿<%@ Page Title="Scheduling Required Works" Language="C#" MasterPageFile="~/MasterPage/Pdr.Master"
    AutoEventWireup="true" CodeBehind="RearrangeWorksRequired.aspx.cs" Inherits="PropertyDataRestructure.Views.Void.VoidWorks.RearrangeWorksRequired" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="updateProgressReportsArea" runat="server" DisplayAfter="10">
        <ProgressTemplate>
            <div class="loading-image">
                <img alt="Please Wait" src="../../../Images/menu/progress.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="updPanelVoidWorksRequired" runat="server">
        <ContentTemplate>
            <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="500px" />
            <p style="height: 22px; text-align: justify; font-family: Tahoma; font-weight: bold;
                margin: 0 0 6px; font-size: 15px; padding: 8px;">
                <font color="black">
                    <asp:Label runat="server" ID="lblRearrangeVoidWorks" Text="Rearrange Void Works"></asp:Label>
                </font>
            </p>
            <div style='margin-bottom: 5px; border-bottom: 1px solid black; width: 100%;'>
            </div>
            <div style="border: 1px solid black; height: auto; clear: both; margin-left: 2px;
                width: 98.7%; padding: 5px;">
                <asp:GridView ID="grdRequiredWorksDetail" runat="server" AutoGenerateColumns="False"
                    CellPadding="4" ForeColor="Black" GridLines="None" ShowHeader="True" Width="100%"
                    OnRowDataBound="grdRequiredWorksDetail_RowDataBound">
                    <Columns>
                        <asp:TemplateField HeaderText="Termination:" ShowHeader="True">
                            <ItemTemplate>
                                <asp:Label ID="lbTermination" runat="server" Text='<%# Eval("TerminationDate", "{0:dd/MM/yyyy}") %>'></asp:Label>
                            </ItemTemplate>
                             <ItemStyle Width="8%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Relet:" ShowHeader="False">
                            <ItemTemplate>
                                <asp:Label ID="lblRelet" runat="server" Text='<%# Eval("ReletDate", "{0:dd/MM/yyyy}") %>'></asp:Label>
                            </ItemTemplate>
                             <ItemStyle Width="8%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Location:" ShowHeader="False">
                            <ItemTemplate>
                                <asp:Label ID="lbLocation" runat="server" Text='<%# Eval("Location") %>'></asp:Label>
                            </ItemTemplate>
                             <ItemStyle Width="20%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Details:" ShowHeader="False">
                            <ItemTemplate>
                                <asp:Label ID="lblWorkDescription" runat="server" Text='<%# Eval("WorkDescription") %>'></asp:Label>
                            </ItemTemplate>
                             <ItemStyle Width="30%" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Duration:" ShowHeader="False">
                            <ItemTemplate>
                                <asp:Panel runat="server" ID="pnlDuration">
                                    <asp:Label ID="lblDuration" runat="server" Text='<%# Eval("Duration") %>'></asp:Label>
                                    <asp:ImageButton ID="imgBtnEdit" runat="server" BorderStyle="None" Height="16" Width="16"
                                        BorderWidth="0px" ImageUrl='<%# "~/Images/editv.png" %>' OnClick="imgBtnEdit_Click"
                                        CommandArgument='<%# Eval("RequiredWorksId") %>' />
                                    <asp:HiddenField ID="hdnDuration" runat="server" Value='<%# Eval("Duration") %>' />
                                    <asp:DropDownList ID="ddlDuration" runat="server" Width="100px" Align="center">
                                    </asp:DropDownList>
                                    <asp:ImageButton ID="imgBtnDone" runat="server" Visible="false" BorderStyle="None"
                                        Height="16" Width="16" BorderWidth="0px" ImageUrl='<%# "~/Images/done.png" %>'
                                        OnClick="imgBtnDone_Click" CommandArgument='<%# Eval("RequiredWorksId")%>' />
                                </asp:Panel>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Tenant Neglect:" ShowHeader="False">
                            <ItemTemplate>
                                <asp:Label ID="lblNeglect" runat="server" Text='<%# Eval("Neglect") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle HorizontalAlign="Left" />
                </asp:GridView>
                <div style='margin-top: 8px; border-bottom: 1px solid black; width: 100%;'>
                </div>
                <div style='border-bottom: 1px solid #B8C6D3; max-height: 400px; overflow: auto;
                    padding: 5px;'>
                    <asp:GridView ID="grdOperative" runat="server" ShowHeaderWhenEmpty="True" AutoGenerateColumns="False"
                        OnRowDataBound="grdOperative_RowDataBound" GridLines="None" CssClass="gridfaults"
                        EmptyDataText="No Operative data exist!" Width="100%">
                        <Columns>
                            <asp:TemplateField HeaderText="Operative:" ShowHeader="True">
                                <ItemTemplate>
                                    <asp:Label ID="lblOperative" runat="server" Text='<%# Eval("Operative:") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" Width="15%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Start Date:" ShowHeader="True">
                                <ItemTemplate>
                                    <asp:Label ID="lblStartDate" runat="server" Text='<%# Eval("Start date:") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" Width="30%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="End Date:" ShowHeader="True">
                                <ItemTemplate>
                                    <asp:Label ID="lblEndDate" runat="server" Text='<%# Eval("End Date:") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" Width="30%" />
                            </asp:TemplateField>
                            <asp:TemplateField ShowHeader="False">
                                <ItemTemplate>
                                    <asp:Button ID="btnSelect" runat="server" CausesValidation="false" Text="Select"
                                        OnClick="btnSelectOperative_Click" CommandName="Select" CommandArgument='<%# String.Format("{0} | {1} |{2} | {3}", Eval("OperativeId"), Eval("operative:"), Eval("Start Date:"), Eval("End Date:")) %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" Width="25%" />
                                <HeaderTemplate>
                                    <asp:Label ID="lblStart" runat="server" Text='Start Date:'></asp:Label>
                                    <asp:TextBox runat="server" ID="txtStartDate" Width="100" />
                                    <ajax:CalendarExtender ID="clndrbuildDate" runat="server" Format="dd/MM/yyyy" TargetControlID="txtStartDate"
                                        TodaysDateFormat="dd/MM/yyyy">
                                    </ajax:CalendarExtender>
                                    <asp:Button Text="Go" ID="btnGo" runat="server" OnClick="btnGo_Click" />
                                </HeaderTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:GridView>
                </div>
                <div style="float: right; margin-top: 6px; margin-right: 6px; margin-top: 2px;">
                    <asp:Button Text="< Back to Scheduling" ID="btnBack" runat="server" CssClass="padButton"
                        OnClick="btnBack_Click" />
                         <asp:Button Text="< Back to NoEntry Report" ID="btnNoEntry" runat="server" CssClass="padButton"
                        OnClick="btnNoEntry_Click" />
                    <asp:Button Text="Refresh List" ID="btnRefresh" runat="server" CssClass="padButton"
                        OnClick="btnRefresh_Click" />
                </div>
                <div class="clear" />
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
