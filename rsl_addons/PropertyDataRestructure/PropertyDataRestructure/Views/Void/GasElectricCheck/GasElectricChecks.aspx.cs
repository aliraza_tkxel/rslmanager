﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_Utilities.Constants;
using PropertyDataRestructure.Base;
using PDR_BusinessObject.CommonSearch;
using PDR_BusinessObject.PageSort;
using System.Data;
using PDR_BusinessLogic.VoidInspections;
using PDR_DataAccess.VoidInspections;

namespace PropertyDataRestructure.Views.Void
{
    public partial class GasElectricChecks : PageBase
    {
       
        #region Events
        #region Page load event
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                uiMessage.hideMessage();
                if (!IsPostBack)
                {
                    objSession.Patch = 0;
                    BindPatchFilterValues();
                    ucChecksToBeArrangedTab.loadData();
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region Link Button Inspections to be Arranged
        /// <summary>
        ///  Link Button Inspections to be Arranged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkBtnAptbaTab_Click(object sender, EventArgs e)
        {
            try
            {
                lnkBtnAptbaTab.CssClass = ApplicationConstants.TabClickedCssClass;
                lnkBtnAptTab.CssClass = ApplicationConstants.TabInitialCssClass;
                MainView.ActiveViewIndex = 0;
                 ucChecksToBeArrangedTab.loadData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        #endregion

        #region Link Button Inspections Arranged
        /// <summary>
        /// Link Button Inspections Arranged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkBtnAptTab_Click(object sender, EventArgs e)
        {
            try
            {
                lnkBtnAptbaTab.CssClass = ApplicationConstants.TabInitialCssClass;
                lnkBtnAptTab.CssClass = ApplicationConstants.TabClickedCssClass;
                MainView.ActiveViewIndex = 1;
                ucChecksArrangedTab.loadData();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        #endregion

        #region txtSearchBox TextChanged
        protected void txtSearchBox_TextChanged(object sender, EventArgs e)
        {
            try
            {

                searchResults();

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion
        #endregion

        #region Functions
        #region Search Results
        public void searchResults()
        {
            CommonSearchBO objCommonSearchBo = new CommonSearchBO();
            objCommonSearchBo.SearchText = txtSearchBox.Text.Trim();
            objCommonSearchBo.ChecksRequiredType = Convert.ToInt32(ddlType.SelectedValue);
            objSession.Patch = int.Parse(ddlPatchFilter.SelectedItem.Value);
            objSession.CommonSearchBO = objCommonSearchBo;
           
            if (MainView.ActiveViewIndex == 0)
            {

                ucChecksToBeArrangedTab.searchData();
            }
            else
            {
                ucChecksArrangedTab.searchData();
            }

        }
        #endregion
        #endregion

        #region Bind Patch filter values
        public void BindPatchFilterValues()
        {
            VoidInspectionsBL objVoidInspectionsBl = new VoidInspectionsBL(new VoidInspectionsRepo());
            DataSet resultDataSet = new DataSet();
            objVoidInspectionsBl.getPatchData(ref resultDataSet);

            if (resultDataSet.Tables[0].Rows.Count > 0)
            {
                ddlPatchFilter.DataSource = resultDataSet.Tables[0];
                ddlPatchFilter.DataTextField = "LOCATION";
                ddlPatchFilter.DataValueField = "PATCHID";
                ddlPatchFilter.DataBind();
                ddlPatchFilter.Items.Insert(0, new ListItem("All", "0"));
            }



        }
        #endregion

        #region Filter result on patch
        protected void getPatchBasedResults_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            objSession.Patch = int.Parse(ddlPatchFilter.SelectedItem.Value);
            if (MainView.ActiveViewIndex == 0)
            {

                ucChecksToBeArrangedTab.FilterResultOnPatch();
            }
            else
            {

                ucChecksArrangedTab.FilterResultOnPatch();
            }
        }

        #endregion


    }
}