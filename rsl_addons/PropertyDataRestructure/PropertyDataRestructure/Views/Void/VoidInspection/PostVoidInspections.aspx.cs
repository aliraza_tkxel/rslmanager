﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PDR_Utilities.Constants;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PropertyDataRestructure.UserControls.Void;
using PropertyDataRestructure.Base;
using PDR_BusinessObject.PageSort;
using System.Data;
using PDR_BusinessLogic.VoidInspections;
using PDR_DataAccess.VoidInspections;

namespace PropertyDataRestructure.Views.Void
{
    public partial class PostVoidInspections : PageBase
    {
        #region Events
        #region Page load event
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                uiMessage.hideMessage();
                if (!IsPostBack)
                {
                   
                    BindPatchFilterValues();
                    objSession.Patch = 0;
                    if (Request.QueryString[PathConstants.ListingTab] == PathConstants.ArrangedTab)
                    {
                        this.showInspectionArranged();
                    }
                    else
                    {
                        this.showInspectionToBeArranged();
                    }
                  
                   
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region Link Button Inspections to be Arranged
        /// <summary>
        ///  Link Button Inspections to be Arranged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkBtnAptbaTab_Click(object sender, EventArgs e)
        {
            try
            {
                this.showInspectionToBeArranged();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        #endregion

        #region Link Button Inspections Arranged
        /// <summary>
        /// Link Button Inspections Arranged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkBtnAptTab_Click(object sender, EventArgs e)
        {
            try
            {
                this.showInspectionArranged();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        #endregion

        


        #region txtSearchBox TextChanged
        protected void txtSearchBox_TextChanged(object sender, EventArgs e)
        {
            try
            {

                searchResults();

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion
        #endregion

        #region Functions
        #region Search Results
        public void searchResults()
        {
            objSession.Patch = int.Parse(ddlPatchFilter.SelectedItem.Value);
            objSession.SearchText = txtSearchBox.Text.Trim();            
            if (MainView.ActiveViewIndex == 0)
            {

                ucInspectionToBeArranged.searchData();
            }
            else
            {
                ucInspectionArranged.searchData();
            }

        }
        #endregion

        #region Show Inspection Arranged Tab
        private void showInspectionArranged()
        {
            lnkBtnAptbaTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnAptTab.CssClass = ApplicationConstants.TabClickedCssClass;
            MainView.ActiveViewIndex = 1;
            ucInspectionArranged.loadData();
        }
        #endregion

        #region show Inspection To Be Arranged Tab
        private void showInspectionToBeArranged()
        {
            lnkBtnAptbaTab.CssClass = ApplicationConstants.TabClickedCssClass;
            lnkBtnAptTab.CssClass = ApplicationConstants.TabInitialCssClass;
            MainView.ActiveViewIndex = 0;
            ucInspectionToBeArranged.loadData();
        }
        #endregion
        #region Bind Patch filter values
        public void BindPatchFilterValues()
        {
            VoidInspectionsBL objVoidInspectionsBl = new VoidInspectionsBL(new VoidInspectionsRepo());
            DataSet resultDataSet = new DataSet();

            objVoidInspectionsBl.getPatchData(ref resultDataSet);

            if (resultDataSet.Tables[0].Rows.Count > 0)
            {
                ddlPatchFilter.DataSource = resultDataSet.Tables[0];
                ddlPatchFilter.DataTextField = "LOCATION";
                ddlPatchFilter.DataValueField = "PATCHID";
                ddlPatchFilter.DataBind();
                ddlPatchFilter.Items.Insert(0, new ListItem("All", "0"));
            }



        }
        #endregion
        #region Filter result on patch
        protected void getPatchBasedResults_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            objSession.Patch = int.Parse(ddlPatchFilter.SelectedItem.Value);
            if (MainView.ActiveViewIndex == 0)
            {
                ucInspectionToBeArranged.FilterResultOnPatch();
            }
            else
            {
                ucInspectionArranged.FilterResultOnPatch();
            }
        }

        #endregion
        #endregion
    }
}