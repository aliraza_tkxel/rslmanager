﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/Pdr.Master" AutoEventWireup="true"
    CodeBehind="VoidInspections.aspx.cs" Inherits="PropertyDataRestructure.Views.Void.VoidInspections" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<%@ Register TagName="InspectionToBeArranged" TagPrefix="ucInspectionToBeArranged"
    Src="~/UserControls/Void/VoidInspection/VoidInspectionToBeArrangedTab.ascx" %>
<%@ Register TagName="InspectionArranged" TagPrefix="ucInspectionArranged" Src="~/UserControls/Void/VoidInspection/VoidInspectionArrangedTab.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">

        //setup before functions
        var typingTimer;                //timer identifier
        var doneTypingInterval = 1000;  //time in ms, 5 second for example

        function TypingInterval() {

            clearTimeout(typingTimer);

            typingTimer = setTimeout(searchTextChanged, doneTypingInterval);

        }

        function searchTextChanged() {
            __doPostBack('<%= txtSearchBox.ClientID %>', '');
        }
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="updateProgressReportsArea" runat="server" DisplayAfter="10">
        <ProgressTemplate>
            <div class="loading-image">
                <img alt="Please Wait" src="../../../Images/menu/progress.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="updPanelVoidInspections" runat="server">
        <ContentTemplate>
            <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="400px" />
            <p style="background-color: Black; height: 22px; text-align: justify; font-family: Tahoma;
                font-weight: bold; margin: 0 0 6px; font-size: 15px; padding: 8px;">
                <font color="white">
                    <asp:Label runat="server" ID="lblHeading" Text="Void Inspections"></asp:Label></font>
            </p>
            <div style="border: 1px solid black; height: 672px; padding: 5px;">
                <table style="width: 100%;">
                    <tr>
                    
                   
                        <td colspan="100%" style="padding-right: 0px !important; width: 100%;">
                            <div style="width: 100%; padding: 0; margin: 0; z-index: 1;">
                                <asp:LinkButton ID="lnkBtnAptbaTab" OnClick="lnkBtnAptbaTab_Click" CssClass="TabClicked"
                                    runat="server" BorderStyle="Solid" BorderColor="Black">Inspections to be arranged: </asp:LinkButton>
                                <asp:LinkButton ID="lnkBtnAptTab" OnClick="lnkBtnAptTab_Click" CssClass="TabInitial"
                                    runat="server" BorderStyle="Solid" BorderColor="Black">Inspections arranged: </asp:LinkButton>
                          
                      
                 
                      
                    
                    
                                <div style="float: right; margin-top: 10px;">

                                <asp:DropDownList ID="ddlPatchFilter" runat="server" AutoPostBack="True"
                               onselectedindexchanged="getPatchBasedResults_OnSelectedIndexChanged"> </asp:DropDownList> 

                                    <asp:RegularExpressionValidator ID="revSearchBox" runat="server" ControlToValidate="txtSearchBox"
                                        EnableClientScript="True" ErrorMessage="Special characters are not allowed" CssClass="Required"
                                        Display="Dynamic" ValidationExpression="^[A-Za-z0-9- ]+$" />
                                    <asp:TextBox ID="txtSearchBox" runat="server" AutoPostBack="false" onkeyup="TypingInterval();"
                                        OnTextChanged="txtSearchBox_TextChanged"></asp:TextBox>
                                    <ajax:TextBoxWatermarkExtender ID="txtSearchBoxWatermarkExtender" runat="server"
                                        TargetControlID="txtSearchBox" WatermarkCssClass="searchText" WatermarkText="Quick find ...">
                                    </ajax:TextBoxWatermarkExtender>
                                </div>
                               
                  
                            </div>
                            <div style="border-bottom: 1px solid black; height: 0px; clear: both; margin-left: 2px;
                                width: 99.7%;">
                            </div>
                            <div style="clear: both; margin-bottom: 5px;">
                            </div>
                      </td>
                    </tr>
                </table>
                <asp:MultiView ID="MainView" runat="server" ActiveViewIndex="0">
                    <asp:View ID="View1" runat="server">
                        <ucInspectionToBeArranged:InspectionToBeArranged ID="ucInspectionToBeArranged" runat="server"
                            maxvalue="10" minvalue="1" />
                    </asp:View>
                    <asp:View ID="View2" runat="server">
                        <ucInspectionArranged:InspectionArranged ID="ucInspectionArranged" runat="server"
                            maxvalue="10" minvalue="1" />
                    </asp:View>
                </asp:MultiView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <ajax:ModalPopupExtender ID="mdlpopupScheduler" runat="server" PopupControlID="pnlVoidInspection"
        TargetControlID="lblHiddenEntry"  BackgroundCssClass="modalBackground">
    </ajax:ModalPopupExtender>
    <asp:Label Text="" ID="lblHiddenEntry" runat="server" />
    <asp:Panel ID="pnlVoidInspection" runat="server" CssClass="modalPopupSchedular" Style="width: 400px;
        border-color: black; border-bottom-style: solid; border-width: 1px;">
        <asp:UpdatePanel ID="updPopupReportFault" runat="server">
            <ContentTemplate>
                <div style="height: auto; clear: both;">
                    <table id="tblPausedFault" style="width: 420px; text-align: left; margin-right: 10px;">
                        <tr>
                            <td>
                                <b>Arrange 1st Inspection?</b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <hr class="InspectionArrangeHr" style='width: 432px;' />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Would you like to arrange a 1st Inspection?
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                            <div style="float: left ; width: 100%; margin-right: 18px;">
                                <asp:Button ID="btnCancel" runat="server" CssClass="margin_right20" 
                                    Text="Arrange 1st Inspection Later" style='float:left;' 
                                    onclick="btnCancel_Click" />
                                <asp:Button ID="btnNow" runat="server" CssClass="margin_right20" Text="Arrange 1st Inspection Now"
                                    OnClick="btnNow_Click"  style='float:left;' />
                                    </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>
