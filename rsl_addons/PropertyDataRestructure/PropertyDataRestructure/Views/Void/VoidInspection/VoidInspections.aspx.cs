﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PDR_Utilities.Constants;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PropertyDataRestructure.UserControls.Void;
using PropertyDataRestructure.Base;
using PDR_BusinessObject.PageSort;
using PDR_BusinessLogic.VoidInspections;
using PDR_DataAccess.VoidInspections;
using System.Data;
using PDR_BusinessObject.BritishGas;
using System.Threading;
using System.Linq;

namespace PropertyDataRestructure.Views.Void
{
    public partial class VoidInspections : PageBase
    {
        VoidInspectionsBL voidInspectionBl = new VoidInspectionsBL(new VoidInspectionsRepo());
        #region Events
        #region Page load event
        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                
                uiMessage.hideMessage();
                if (!IsPostBack)
                {
                    objSession.Patch = 0;
                    BindPatchFilterValues();
                    if (Request.QueryString[PathConstants.ListingTab] == PathConstants.ArrangedTab)
                    {
                        this.showInspectionArranged();
                        if (Request.QueryString[PathConstants.Source] == PathConstants.Rsl && Request.QueryString[PathConstants.JId] != null)
                        {
                            var journalId = 0;
                            if (Request.QueryString[PathConstants.CustomerId] != null)
                            {
                                objSession.CustomerId = Convert.ToInt32(Request.QueryString[PathConstants.CustomerId]);
                            }
                            if (Request.QueryString[PathConstants.JId] != null)
                            {
                                journalId = Convert.ToInt32(Request.QueryString[PathConstants.JId]);

                                VoidInspectionsBL objVoidInspectionsBl = new VoidInspectionsBL(new VoidInspectionsRepo());
                                DataSet resultDataSet = new DataSet();
                                objVoidInspectionsBl.getInspectionArrangedByJournalId(ref resultDataSet, journalId);
                                var notes = string.Empty;
                                var appointmentDateTime = string.Empty;
                                var operativeName = string.Empty;
                                bool Legionella = false;
                                foreach (var row in resultDataSet.Tables[0].Rows)
                                {
                                    if ((int)((DataRow)row)["JournalId"] == journalId)
                                    { 
                                        notes = (string)((DataRow)row)["APPOINTMENTNOTES"];
                                        appointmentDateTime = (string)((DataRow)row)["Appointment"];
                                        operativeName = (string)((DataRow)row)["OperativeName"];
                                        if (((DataRow)row)["Legionella"] != null)
                                            Legionella = (bool)((DataRow)row)["Legionella"];
                                    }
                                }
                                objSession.InspectionArrangedDataSet = null;
                                ucInspectionArranged.populateAndShowInspectionPopup(journalId, notes,Legionella, appointmentDateTime, operativeName, PathConstants.Rsl);
                            }

                        }
                    }
                    else
                    {
                        this.showInspectionToBeArranged();
                        if (Request.QueryString[PathConstants.Source] == PathConstants.Rsl && Request.QueryString[PathConstants.JId] != null)
                        {
                            if (Request.QueryString[PathConstants.CustomerId ] != null)
                            {
                                objSession.CustomerId = Convert.ToInt32(Request.QueryString[PathConstants.CustomerId]);   
                            }
                            mdlpopupScheduler.Show();

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region Link Button Inspections to be Arranged
        /// <summary>
        ///  Link Button Inspections to be Arranged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkBtnAptbaTab_Click(object sender, EventArgs e)
        {
            try
            {
                this.showInspectionToBeArranged();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        #endregion

        #region Link Button Inspections Arranged
        /// <summary>
        /// Link Button Inspections Arranged
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void lnkBtnAptTab_Click(object sender, EventArgs e)
        {
            try
            {
                this.showInspectionArranged();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        #endregion

        #region Arrange 1st Inspection Now
        protected void btnNow_Click(object sender, EventArgs e)
        {
            try
            {
                uiMessage.hideMessage();
                mdlpopupScheduler.Hide();
                AjaxControlToolkit.ModalPopupExtender mdlpopupInspection = (AjaxControlToolkit.ModalPopupExtender)ucInspectionToBeArranged.FindControl("mdlpopupInspection");
                ArrangeInspectionPopUp ucArrangeInspectionPopUp = (ArrangeInspectionPopUp)ucInspectionToBeArranged.FindControl("ucArrangeInspectionPopUp");
                ucArrangeInspectionPopUp.populatePreValues(Convert.ToInt32(Request.QueryString[PathConstants.JId]));
                
                DataSet resultDs = new DataSet();
                resultDs = voidInspectionBl.getPropertyInfoByJournalId(Convert.ToInt32(Request.QueryString[PathConstants.JId]));
                if (resultDs.Tables[0] != null && resultDs.Tables[0].Rows.Count > 0)
                {
                    BritishGasNotifcationBO britishGasBo = new BritishGasNotifcationBO();
                    britishGasBo.TenancyId = Convert.ToInt32(resultDs.Tables[0].Rows[0]["TenancyId"]);
                    britishGasBo.PropertyId = Convert.ToString(resultDs.Tables[0].Rows[0]["PropertyId"]);
                    britishGasBo.CustomerId = Convert.ToInt32(resultDs.Tables[0].Rows[0]["CustomerId"]);
                    objSession.BritishGasNotificationBo = britishGasBo;
                }
                mdlpopupInspection.Show();

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion


        #region txtSearchBox TextChanged
        protected void txtSearchBox_TextChanged(object sender, EventArgs e)
        {
            try
            {

                searchResults();

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion
        #endregion

        #region Functions
        #region Search Results
        public void searchResults()
        {
            objSession.SearchText = txtSearchBox.Text.Trim();
            if (MainView.ActiveViewIndex == 0)
            {

                ucInspectionToBeArranged.searchData();
            }
            else
            {
                ucInspectionArranged.searchData();
            }

        }
        #endregion

        #region Show Inspection Arranged Tab
        private void showInspectionArranged()
        {
            lnkBtnAptbaTab.CssClass = ApplicationConstants.TabInitialCssClass;
            lnkBtnAptTab.CssClass = ApplicationConstants.TabClickedCssClass;
            MainView.ActiveViewIndex = 1;
            ucInspectionArranged.loadData();
        }
        #endregion

        #region show Inspection To Be Arranged Tab
        private void showInspectionToBeArranged()
        {
            lnkBtnAptbaTab.CssClass = ApplicationConstants.TabClickedCssClass;
            lnkBtnAptTab.CssClass = ApplicationConstants.TabInitialCssClass;
            MainView.ActiveViewIndex = 0;
            ucInspectionToBeArranged.loadData();
        }
        #endregion

        #region Bind Patch filter values
        public void BindPatchFilterValues()
        {
            DataSet resultDataSet = new DataSet();
            voidInspectionBl.getPatchData(ref resultDataSet);
            
            if (resultDataSet.Tables[0].Rows.Count > 0)
            {
                ddlPatchFilter.DataSource = resultDataSet.Tables[0];
                ddlPatchFilter.DataTextField = "LOCATION";
                ddlPatchFilter.DataValueField = "PATCHID";
                ddlPatchFilter.DataBind();
                ddlPatchFilter.Items.Insert(0, new ListItem("All","0")); 
            }
          
             
            
        }
        #endregion

        #region Filter result on patch
        protected void getPatchBasedResults_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            objSession.Patch = int.Parse(ddlPatchFilter.SelectedItem.Value);
            if (MainView.ActiveViewIndex == 0)
            {
                ucInspectionToBeArranged.FilterResultOnPatch();
            }
            else
            {
                ucInspectionArranged.FilterResultOnPatch();
            }
        }

        #endregion

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            try
            {
                
                Response.Redirect(PathConstants.CustomerModule + objSession.CustomerId.ToString());
            }
            catch (ThreadAbortException ex)
            {
                 
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                    
                }
            }
        }
        #endregion
    }
}