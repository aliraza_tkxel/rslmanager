﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PDR_Utilities.Helpers;
using PDR_Utilities.Constants;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_BusinessLogic;
using PropertyDataRestructure.Base;
using PDR_BusinessObject;

using PDR_BusinessObject.Notes;
using PDR_DataAccess.Scheduling;
using PDR_BusinessLogic.Scheduling;
using System.Globalization;
using System.Data;
using PDR_DataAccess.Customer;
using PDR_BusinessLogic.CustomerBL;
using PDR_BusinessObject.Customer;
using Microsoft.Practices.EnterpriseLibrary.Validation.Validators;
using Microsoft.Practices.EnterpriseLibrary.Validation;
using PDR_BusinessObject.VoidAppointment;
using PDR_BusinessObject.RequiredWorks;
using System.Web.UI.HtmlControls;
using PropertyDataRestructure.Interface;
using System.Text;
using System.IO;
using System.Text.RegularExpressions;
using PDR_BusinessObject.ICalFile;
using PDR_BusinessLogic.VoidInspections;
using PDR_DataAccess.VoidInspections;

namespace PropertyDataRestructure.Views.Scheduling
{
    public partial class JSVJobSheetSummary : PageBase, IAddEditPage
    {

        #region "Events"

        #region "Page Load Event"
        /// <summary>
        /// Page Load Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>

        protected void Page_Load(object sender, System.EventArgs e)
        {
            try
            {
                uiMessage.hideMessage();

                if (!IsPostBack)
                {
                    prePopulatedValues();
                    if (Request.QueryString[PathConstants.Path] == PathConstants.Rsl)
                    {
                        btnBackRearrange.Visible = true;
                        btnBack.Visible = false;
                    }
                    else
                    {
                        btnBack.Visible = true;
                        btnBackRearrange.Visible = false;
                    }
                }

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessage.isError == true)
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }

        #endregion

        #region Button Update Customer Detail Click Event
        /// <summary>
        /// Button Update Customer Detail Click Event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void btnUpdateCustomerDetails_Click(object sender, EventArgs e)
        {
            try
            {
                updateAddress();
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessage.isError == true)
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        #endregion

        #region btnScheduleAppointment Click event
        /// <summary>
        /// btnScheduleAppointment Click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnScheduleAppointment_Click(object sender, EventArgs e)
        {

            try
            {
                saveData();

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }

        }
        #endregion

        #region btnBack click event
        /// <summary>
        /// btnBack click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnBack_Click(object sender, EventArgs e)
        {
            if (Request.QueryString[PathConstants.Source] == PathConstants.Rearrange)
            {
                this.navigateToReScheduleRequiredWorks();
            }
            else
            {
                this.navigateToScheduleRequiredWorks();
            }
        }
        #endregion
        #endregion
        #region IAddEditPage Implementation
        public void saveData()
        {
            VoidAppointmentBO objVoidAppointmentBo = objSession.VoidAppointmentBO;
            List<RequiredWorksBO> checkedTempWorksList = new List<RequiredWorksBO>();
            checkedTempWorksList = objSession.RequiredWorksBOList;
            double totalDuration = checkedTempWorksList.Sum(x => Convert.ToDouble(x.Duration));
            objVoidAppointmentBo.Duration = totalDuration;
            objVoidAppointmentBo.InspectionJournalId = checkedTempWorksList[0].InspectionJournalId;
            objVoidAppointmentBo.AppointmentNotes = txtCustAppointmentNotes.Text.Trim();
            objVoidAppointmentBo.JobSheetNotes = txtJobSheetNotes.Text.Trim();
            // objVoidAppointmentBo.RequiredWorkIds =  checkedTempWorksList.Select(s => s.RequiredWorksId.ToString());
            var cats = checkedTempWorksList.Select(i => new { i.RequiredWorksId, i.Duration }).ToList();
            DataTable voidRequiredWorkDurationDt = GeneralHelper.convertToDataTable(cats);

            VoidSchedulingBL objVoidSchedulingBl = new VoidSchedulingBL(new SchedulingRepo());
            string isSaved = string.Empty;
            int appointmentId = Convert.ToInt32(ApplicationConstants.DefaultValue);
            int journalId = Convert.ToInt32(ApplicationConstants.DefaultValue);
            if (Request.QueryString[PathConstants.Source] == PathConstants.Rearrange)
            {
                objVoidAppointmentBo.AppointmentId = checkedTempWorksList[0].AppointmentId;
                objVoidAppointmentBo.JournalId = checkedTempWorksList[0].JournalId;
                if (Request.QueryString[PathConstants.Path] == PathConstants.Rsl)
                {
                    sendEmailForAppointmentRearrange(objVoidAppointmentBo.JournalId);
                }
                isSaved = objVoidSchedulingBl.reScheduleWorksRequired(objVoidAppointmentBo);

            }
            else
            {
                isSaved = objVoidSchedulingBl.scheduleVoidRequiredWorks(objVoidAppointmentBo, ref appointmentId, ref journalId, voidRequiredWorkDurationDt);
                objVoidAppointmentBo.JournalId = journalId;
            }

            if (isSaved.Equals(ApplicationConstants.NotSaved) || appointmentId == 0)
            {
                uiMessage.showErrorMessage(UserMessageConstants.SaveAppointmentFailed);
            }
            else
            {
                uiMessage.showInformationMessage(UserMessageConstants.SaveAppointmentSuccessfully);
                hid_isAppointmentCreated.Value = Convert.ToString(1);
                objVoidAppointmentBo.AppointmentId = appointmentId;
                lblSuccessMessage.Text = UserMessageConstants.AppointmentScheduledSuccessfully;
                if (Request.QueryString[PathConstants.Source] == PathConstants.Rearrange)
                {
                    lblSuccessMessage.Text = UserMessageConstants.AppointmentReScheduledSuccessfully;
                }
                
                updPnlSuccessMessage.Update();
                mdlpopupAppointmentConfirmed.Show();
                prePopulatedValues();
                // Send ICal file via email
                sendICalFileViaEmail(objVoidAppointmentBo);
            }

        }

        public bool validateData()
        {
            throw new NotImplementedException();
        }

        public void resetControls()
        {
            throw new NotImplementedException();
        }

        public void loadData()
        {
            throw new NotImplementedException();
        }

        public void populateDropDown(DropDownList ddl)
        {
            throw new NotImplementedException();
        }

        public void populateDropDowns()
        {
            throw new NotImplementedException();

        }


        #endregion
        #region "Function"
        #region "Pre Populate Values"
        /// <summary>
        /// Pre Populate Values
        /// </summary>
        /// <remarks></remarks>
        public void prePopulatedValues()
        {
            VoidAppointmentBO objVoidAppointmentBo = objSession.VoidAppointmentBO;
            
            List<RequiredWorksBO> checkedTempWorksList = new List<RequiredWorksBO>();
            checkedTempWorksList = objSession.RequiredWorksBOList;
            if (objVoidAppointmentBo == null || checkedTempWorksList == null)
            {
                uiMessage.showErrorMessage(UserMessageConstants.ProblemLoadingData);
                btnScheduleAppointment.Enabled = false;
                btnUpdateCustomerDetails.Enabled = false;
            }
            else
            {
                this.populateAppointmentInfo();
                this.populatePropertyInfo(checkedTempWorksList[0].InspectionJournalId);

                this.checkAppointmentCreated();
            }

        }

        #endregion
        #region "Populate Appointment Info"
        /// <summary>
        /// Populate Appointment Info
        /// </summary>
        /// <remarks></remarks>
        public void populateAppointmentInfo()
        {
            VoidAppointmentBO objVoidAppointmentBo = objSession.VoidAppointmentBO;
            List<RequiredWorksBO> checkedTempWorksList = new List<RequiredWorksBO>();
            checkedTempWorksList = objSession.RequiredWorksBOList;

            foreach (RequiredWorksBO requiredBo in checkedTempWorksList)
            {
                HtmlTableRow newRowWork = new HtmlTableRow();
                HtmlTableCell newCellJSVLabel = new HtmlTableCell();
                if (requiredBo.Ref != null)
                    newCellJSVLabel.InnerText = "JSV:" + requiredBo.Ref.Substring(requiredBo.Ref.Length - 4, 4);
                HtmlTableCell newCellLocationLabel = new HtmlTableCell();
                if (requiredBo.Location != null)
                    newCellLocationLabel.InnerText = requiredBo.Location + ": " + requiredBo.WorkDescription.ToString();
                newCellJSVLabel.Width = "20%";
                newCellJSVLabel.Style.Add("font-weight", "bold");
                newCellLocationLabel.Width = "80%";
                newCellLocationLabel.Style.Add("font-weight", "bold");
                newRowWork.Cells.Add(newCellJSVLabel);
                newRowWork.Cells.Add(newCellLocationLabel);
                tblRequiredWork.Rows.Add(newRowWork);
            }
            double totalDuration = checkedTempWorksList.Sum(x => Convert.ToDouble(x.Duration));

            lblOperative.Text = objVoidAppointmentBo.OperativeName;
            lblDuration.Text = GeneralHelper.appendHourLabel(totalDuration);

            DateTime startDateTime = Convert.ToDateTime(objVoidAppointmentBo.AppointmentStartDate);
            DateTime endDateTime = Convert.ToDateTime(objVoidAppointmentBo.AppointmentEndDate);
            lblStartDate.Text = Convert.ToDateTime(objVoidAppointmentBo.AppointmentStartDate).ToString("HH:mm") + " " + GeneralHelper.getDateWithWeekdayFormat(startDateTime);
            lblEndDate.Text = Convert.ToDateTime(objVoidAppointmentBo.AppointmentEndDate).ToString("HH:mm") + " " + GeneralHelper.getDateWithWeekdayFormat(endDateTime);
            txtCustAppointmentNotes.Text = checkedTempWorksList[0].AppointmentNotes.Trim();
            txtJobSheetNotes.Text = checkedTempWorksList[0].JobSheetNotes.Trim();
        }

        #endregion

        #region "Populate Property Info"
        /// <summary>
        /// Populate Property Info
        /// </summary>
        /// <remarks></remarks>

        public void populatePropertyInfo(int journalId)
        {

            SchedulingBL objSchedulingBL = new SchedulingBL(new SchedulingRepo());

            if (journalId == ApplicationConstants.NoneValue)
            {
                uiMessage.showErrorMessage(UserMessageConstants.ProblemLoadingData);
                btnScheduleAppointment.Enabled = false;
                btnUpdateCustomerDetails.Enabled = false;
            }
            else
            {
                DataSet resultDataset = new DataSet();
                objSchedulingBL.getPropertyDetailByJournalId(ref resultDataset, journalId,true);
                DataTable resultDt = resultDataset.Tables[0];

                if (resultDt.Rows.Count == 0)
                {
                    uiMessage.showErrorMessage(UserMessageConstants.InvalidJournalId);
                    btnScheduleAppointment.Enabled = false;
                    btnUpdateCustomerDetails.Enabled = false;
                }
                else
                {
                    //Property Information
                    lblScheme.Text = Convert.ToString(resultDt.Rows[0]["SchemeName"]);

                    lblAddress.Text = Convert.ToString(resultDt.Rows[0]["HOUSENUMBER"] + " " + resultDt.Rows[0]["ADDRESS1"] + " " + resultDt.Rows[0]["ADDRESS2"]);
                    lblTowncity.Text = Convert.ToString(resultDt.Rows[0]["TOWNCITY"]);
                    lblCounty.Text = Convert.ToString(resultDt.Rows[0]["COUNTY"]);
                    lblPostcode.Text = Convert.ToString(resultDt.Rows[0]["POSTCODE"]);

                    //Customer Information
                    hdnCustomerId.Value = Convert.ToString(resultDt.Rows[0]["CustomerId"]);
                    lblTenantName.Text = Convert.ToString(resultDt.Rows[0]["TenantName"]);
                    lblCustomerTelephone.Text = Convert.ToString(resultDt.Rows[0]["Telephone"]);
                    lblCustomerMobile.Text = Convert.ToString(resultDt.Rows[0]["Mobile"]);
                    lblCustomerEmail.Text = Convert.ToString(resultDt.Rows[0]["Email"]);
                }
            }
        }

        #endregion

        #region "Check appointment created."
        /// <summary>
        /// Check appointment created.
        /// </summary>
        /// <remarks></remarks>

        public void checkAppointmentCreated()
        {
            if (!hid_isAppointmentCreated.Value.Equals("0"))
            {
                btnScheduleAppointment.Enabled = false;
                btnUpdateCustomerDetails.Enabled = false;
                txtCustAppointmentNotes.Attributes.Add("readonly", "readonly");
                txtJobSheetNotes.Attributes.Add("readonly", "readonly");
            }

        }

        #endregion

        #region "Update Customer Address"
        /// <summary>
        /// Update Customer Address
        /// </summary>
        /// <remarks></remarks>

        public void updateAddress()
        {
            //Update Enable

            if (btnUpdateCustomerDetails.Text == ApplicationConstants.UpdateCustomerDetails)
            {
                txtCustomerTelephone.Text = lblCustomerTelephone.Text;
                lblCustomerTelephone.Visible = false;
                txtCustomerTelephone.Visible = true;

                txtCustomerMobile.Text = lblCustomerMobile.Text;
                lblCustomerMobile.Visible = false;
                txtCustomerMobile.Visible = true;

                txtCustomerEmail.Text = lblCustomerEmail.Text;
                lblCustomerEmail.Visible = false;
                txtCustomerEmail.Visible = true;

                btnUpdateCustomerDetails.Text = ApplicationConstants.SaveChanges;

            }
            else
            {
                //Save Changes

                if (lblCustomerTelephone.Text != txtCustomerTelephone.Text | lblCustomerMobile.Text != txtCustomerMobile.Text | lblCustomerEmail.Text != txtCustomerEmail.Text)
                {
                    //If Not isError Then
                    CustomerBL objCustomerBL = new CustomerBL(new CustomerRepo());
                    CustomerBO objCustomerBO = new CustomerBO();

                    objCustomerBO.CustomerId = Convert.ToInt32(hdnCustomerId.Value);
                    objCustomerBO.Telephone = txtCustomerTelephone.Text;
                    objCustomerBO.Mobile = txtCustomerMobile.Text;
                    objCustomerBO.Email = txtCustomerEmail.Text;

                    Validator<CustomerBO> objValidator = valFactory.CreateValidator<CustomerBO>();
                    ValidationResults results = objValidator.Validate(objCustomerBO);


                    if (results.IsValid)
                    {
                        objCustomerBL.updateAddress(objCustomerBO);
                        lblCustomerTelephone.Text = txtCustomerTelephone.Text;
                        lblCustomerMobile.Text = txtCustomerMobile.Text;
                        lblCustomerEmail.Text = txtCustomerEmail.Text;
                        txtCustomerTelephone.Visible = false;
                        lblCustomerTelephone.Visible = true;
                        txtCustomerMobile.Visible = false;
                        lblCustomerMobile.Visible = true;
                        txtCustomerEmail.Visible = false;
                        lblCustomerEmail.Visible = true;
                        btnUpdateCustomerDetails.Text = ApplicationConstants.UpdateCustomerDetails;
                        uiMessage.showInformationMessage(UserMessageConstants.UserSavedSuccessfuly);
                    }
                    else
                    {
                        dynamic message = string.Empty;
                        foreach (ValidationResult result in results)
                        {
                            message += result.Message;
                            break;
                        }
                        uiMessage.showErrorMessage(message);
                    }



                }
                else
                {
                    txtCustomerTelephone.Visible = false;
                    lblCustomerTelephone.Visible = true;

                    txtCustomerMobile.Visible = false;
                    lblCustomerMobile.Visible = true;

                    txtCustomerEmail.Visible = false;
                    lblCustomerEmail.Visible = true;

                    btnUpdateCustomerDetails.Text = ApplicationConstants.UpdateCustomerDetails;

                }

            }
            this.populateAppointmentInfo();

        }

        #endregion

        #region "Navigate to Rearrange Required Works page"
        /// <summary>
        /// Navigate to ME Job sheet
        /// </summary>
        /// <remarks></remarks>
        public void navigateToReScheduleRequiredWorks()
        {
            Response.Redirect(PathConstants.ReScheduleRequiredWorks);
        }
        #endregion

        #region "Navigate to Schedule Required Works page"
        /// <summary>
        /// Navigate to ME Job sheet
        /// </summary>
        /// <remarks></remarks>
        public void navigateToScheduleRequiredWorks()
        {
            Response.Redirect(PathConstants.ScheduleRequiredWorks);
        }
        #endregion

        #endregion

        #region "Email Functionality"

        #region "Send iCal File via Email"
        /// <summary>
        /// Send iCal File via Email
        /// </summary>
        /// <remarks></remarks>

        private void sendICalFileViaEmail(VoidAppointmentBO objVoidAppointmentBO)
        {

            DataSet resultDataset = new DataSet();
            SchedulingBL objSchedulingBL = new SchedulingBL(new SchedulingRepo());
            objSchedulingBL.getOperativeInformation(ref resultDataset, objVoidAppointmentBO.OperativeId);

            string operativeName = string.Empty;
            string operativeEmail = string.Empty;
            operativeName = resultDataset.Tables[0].Rows[0]["Name"].ToString();
            if (resultDataset.Tables[0].Rows[0]["Email"].Equals(DBNull.Value))
            {
                operativeEmail = string.Empty;
            }
            else
            {
                operativeEmail = resultDataset.Tables[0].Rows[0]["Email"].ToString();
            }

            string body = this.populateBody(objVoidAppointmentBO);
            string subject = "Void Works Appointment Arranged";
            string recepientEmail = operativeEmail;

            try
            {

                if (ValidationHelper.isEmail(recepientEmail))
                {
                    populateiCalFile(operativeName, operativeEmail, objVoidAppointmentBO);
                    EmailHelper.sendHtmlFormattedEmailWithAttachment(operativeName, recepientEmail, subject, body, Server.MapPath(PathConstants.ICalFilePath));
                }
                else
                {
                    uiMessage.showErrorMessage(UserMessageConstants.AppointmentArrangedSavedEmailError + " " + UserMessageConstants.InvalidEmail);
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.showErrorMessage(UserMessageConstants.AppointmentArrangedSavedEmailError + ex.Message);


                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }

            }

        }
        #endregion

        #region "Send Email for appointment ReArrange"
        /// <summary>
        /// Send Email for appointment ReArrange
        /// </summary>
        /// <remarks></remarks>

        private void sendEmailForAppointmentRearrange(int journalId)
        {

            DataSet resultDataset = new DataSet();
            VoidInspectionsBL objVoidInspectionsBl = new VoidInspectionsBL(new VoidInspectionsRepo());

            objVoidInspectionsBl.getOperativeDataForAlert(ref resultDataset, journalId);

            string operativeName = string.Empty;
            string operativeEmail = string.Empty;
            operativeName = resultDataset.Tables[0].Rows[0][ApplicationConstants.OperativeNameColumn].ToString();
            if (resultDataset.Tables[0].Rows[0][ApplicationConstants.EmailColumn].Equals(DBNull.Value))
            {
                operativeEmail = string.Empty;
            }
            else
            {
                operativeEmail = resultDataset.Tables[0].Rows[0][ApplicationConstants.EmailColumn].ToString();
            }

            string body = this.populateBodyForRearrangeOnTerminationChange(operativeName);
            string subject = "Void Works Appointment Amended";
            string recepientEmail = operativeEmail;

            try
            {

                if (ValidationHelper.isEmail(recepientEmail))
                {
                    EmailHelper.sendHtmlFormattedEmail(operativeName, recepientEmail, subject, body);
                }
                else
                {
                    uiMessage.showErrorMessage(UserMessageConstants.AppointmentArrangedSavedEmailError + " " + UserMessageConstants.InvalidEmail);
                }
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.showErrorMessage(UserMessageConstants.AppointmentArrangedSavedEmailError + ex.Message);


                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }

            }

        }
        #endregion

        #region "Populate Email body"
        /// <summary>
        /// Populate Email body
        /// </summary>
        /// <param name="objVoidAppointmentBO"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        private string populateBody(VoidAppointmentBO objVoidAppointmentBO)
        {
            StringBuilder body = new StringBuilder();
            StreamReader reader = new StreamReader(Server.MapPath("~/Email/AppointmentArranged.html"));
            body.Append(reader.ReadToEnd().ToString());
            body.Replace("{JSN}", "JSV" + objVoidAppointmentBO.JournalId.ToString().PadLeft(6, '0'));
            body.Replace("{AppointmentTime}", objVoidAppointmentBO.StartTime + " - " + objVoidAppointmentBO.EndTime);
            body.Replace("{AppointmentDate}", objVoidAppointmentBO.AppointmentStartDate.ToString("dd'/'MM'/'yyyy"));
            body.Replace("{Address}", lblAddress.Text);
            body.Replace("{TownCity}", lblTowncity.Text);
            body.Replace("{County}", lblCounty.Text);
            body.Replace("{PostCode}", lblPostcode.Text);
            body.Replace("{Scheme}", lblScheme.Text);
            return body.ToString();
        }
        #endregion

        #region "Populate Email body for Rearrange after termination change"
        /// <summary>
        /// "Populate Email body for Rearrange after termination change"
        /// </summary>
        /// <param name="objVoidAppointmentBO"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        private string populateBodyForRearrangeOnTerminationChange(string OperativeName)
        {
            StringBuilder body = new StringBuilder();
            StreamReader reader = new StreamReader(Server.MapPath("~/Email/RearrangeAppOnTerminationChange.html"));
            body.Append(reader.ReadToEnd().ToString());
            body.Replace("{OperativeName}", OperativeName);
            return body.ToString();
        }
        #endregion

        

        #region "Populate iCal File"
        /// <summary>
        /// 
        /// </summary>
        /// <remarks></remarks>

        public void populateiCalFile(string operativeName, string operativeEmail, VoidAppointmentBO objVoidAppointmentBO)
        {
            ICalFileBO objICalFileBo = new ICalFileBO();
            objICalFileBo.OperativeName = operativeName;
            objICalFileBo.OperativeEmail = operativeEmail;
            objICalFileBo.AppointmentStartDate = objVoidAppointmentBO.AppointmentStartDate;
            objICalFileBo.AppointmentEndDate = objVoidAppointmentBO.AppointmentEndDate;
            objICalFileBo.StartTime = objVoidAppointmentBO.StartTime;
            objICalFileBo.EndTime = objVoidAppointmentBO.EndTime;
            objICalFileBo.Summary = "Void Works Appointment";
            objICalFileBo.JS = "JSV" + objVoidAppointmentBO.JournalId.ToString().PadLeft(6, '0') ;
            GeneralHelper.populateiCalFile(objICalFileBo);
        }

        #endregion

        #endregion

    }
}