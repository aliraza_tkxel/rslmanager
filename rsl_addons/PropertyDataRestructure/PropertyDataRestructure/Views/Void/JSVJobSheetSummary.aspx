﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="JSVJobSheetSummary.aspx.cs"
    MasterPageFile="~/MasterPage/Pdr.Master" Inherits="PropertyDataRestructure.Views.Scheduling.JSVJobSheetSummary" %>

<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajax" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../../Styles/scheduling.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/default.css" rel="stylesheet" type="text/css" />
    <link href="../../Styles/Site.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
      <asp:UpdateProgress ID="updateProgressReportsArea" runat="server" DisplayAfter="10">
        <ProgressTemplate>
            <div class="loading-image">
                <img alt="Please Wait" src="../../Images/menu/progress.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="updPanelAppointment" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="600px" />
            <div class="headingTitle" style="width: 98%">
                <b>Schedule Void Works</b>
            </div>
            <div class="mainContainer">
                <br />
                <asp:Panel ID="pnlHeader" Width="100%" runat="server">
                    <asp:Label ID="Label1" runat="server" CssClass="leftControl" Font-Bold="true" Text="    Appointment Summary"></asp:Label>
                    <asp:Label ID="lblTotalSheets" runat="server" CssClass="rightControl" Font-Bold="true"
                        Text="1"></asp:Label>
                    <asp:Label ID="Label21" runat="server" CssClass="rightControl" Font-Bold="true" Text="of"></asp:Label>
                    <asp:Label ID="lblSheetNumber" runat="server" CssClass="rightControl" Font-Bold="true"
                        Text="1"></asp:Label>
                    <asp:Label ID="Label33" runat="server" CssClass="rightControl" Font-Bold="true" Text="Job Sheet"></asp:Label>
                </asp:Panel>
                <br />
                <hr />
                <br />
                <div style="padding: 10px; border: 1px solid;">
                    <table runat="server" id="tblRequiredWork" width="90%">
                    </table>
                </div>
                <br />
                <br />
                <asp:Panel ID="pnlAppointmentDetail" runat="server">
                    <div class="leftDiv">
                        <table style="width: 100%;" cellspacing="10" cellpadding="5">
                            <asp:HiddenField ID="hid_isAppointmentCreated" runat="server" Value="0"></asp:HiddenField>
                            <tr>
                                <td class="labelTd">
                                    <asp:Label ID="Label2" runat="server" Text="Operative:" Font-Bold="true"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblOperative" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="labelTd">
                                    <asp:Label ID="Label3" runat="server" Text="Duration:" Font-Bold="true"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblDuration" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="leftDiv">
                        <table style="width: 100%;" cellspacing="10" cellpadding="5">
                            <tr>
                                <td class="labelTd">
                                    <asp:Label ID="Label6" runat="server" Text="Start:" Font-Bold="true"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblStartDate" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="labelTd">
                                    <asp:Label ID="Label7" runat="server" Text="End:" Font-Bold="true"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblEndDate" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
                <div style="clear: both">
                </div>
                <hr />
                <div style="clear: both">
                </div>
                <asp:Panel ID="pnlPropertyDetail" runat="server">
                    <div class="leftDiv">
                        <table style="width: 100%;" cellspacing="10" cellpadding="5">
                            <tr>
                                <td class="labelTd">
                                    <asp:Label ID="Label8" runat="server" Font-Bold="true" Text="Scheme:"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblScheme" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="labelTd">
                                    <asp:Label ID="Label9" runat="server" Font-Bold="true" Text="Address:"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblAddress" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="labelTd">
                                    &nbsp;
                                </td>
                                <td>
                                    <asp:Label ID="lblTowncity" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="labelTd">
                                    &nbsp;
                                </td>
                                <td>
                                    <asp:Label ID="lblCounty" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="labelTd">
                                    &nbsp;
                                </td>
                                <td>
                                    <asp:Label ID="lblPostcode" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="leftDiv">
                        <table style="width: 100%;" cellspacing="10" cellpadding="5">
                            <tr>
                                <td class="labelTd">
                                    <asp:Label ID="Label13" runat="server" Text="Tenant:" Font-Bold="true"></asp:Label>
                                </td>
                                <td>
                                    <asp:HiddenField ID="hdnCustomerId" runat="server" />
                                    <asp:Label ID="lblTenantName" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="labelTd">
                                    <asp:Label ID="Label14" runat="server" Text="Telephone:" Font-Bold="true"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblCustomerTelephone" runat="server"></asp:Label>
                                    <asp:TextBox ID="txtCustomerTelephone" runat="server" Visible="False"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="labelTd">
                                    <asp:Label ID="Label15" runat="server" Text="Mobile:" Font-Bold="true"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblCustomerMobile" runat="server"></asp:Label>
                                    <asp:TextBox ID="txtCustomerMobile" runat="server" Visible="False"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="labelTd">
                                    <asp:Label ID="Label16" runat="server" Text="Email:" Font-Bold="true"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblCustomerEmail" runat="server"></asp:Label>
                                    <asp:TextBox ID="txtCustomerEmail" runat="server" Visible="False"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="clear: both">
                    </div>
                    <div style="text-align: right;">
                        <asp:Button ID="btnUpdateCustomerDetails" UseSubmitBehavior="false" OnClick="btnUpdateCustomerDetails_Click"
                            runat="server" Text="Update customer details" />
                    </div>
                </asp:Panel>
                <div style="clear: both">
                </div>
                <hr />
                <div style="clear: both">
                </div>
                <asp:Panel ID="pnlNotes" runat="server">
                    <div class="leftDiv">
                        <table style="width: 100%;" cellspacing="10" cellpadding="5">
                            <tr>
                                <td>
                                    <asp:Label ID="Label19" Font-Bold="true" runat="server" Text="Customer Appointment Notes:"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    If the customer has any preferred contact time,<br />
                                    or ways in which we could contact them,<br />
                                    please enter them in the box below:
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtCustAppointmentNotes" MaxLength="1000" runat="server" TextMode="MultiLine"
                                        Height="150px" Width="100%"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="leftDiv">
                        <table style="width: 100%;" cellspacing="10" cellpadding="5">
                            <tr>
                                <td>
                                    <asp:Label ID="Label20" Font-Bold="true" runat="server" Text="Job Sheet Notes:"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Please enter information specific to the
                                    <br />
                                    Planned works in the box below:
                                    <br />
                                    &nbsp
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtJobSheetNotes" MaxLength="1000" runat="server" TextMode="MultiLine"
                                        Height="150px" Width="100%"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
                <div style="clear: both">
                </div>
                <hr />
                <br />
                <div style="text-align: right; width: 100%;">
                    <asp:Button ID="btnBack" runat="server" Text="< Back to Scheduling" OnClick="btnBack_Click" />
                    <asp:Button ID="btnBackRearrange" runat="server" Text="< Back to Scheduling" OnClientClick="JavaScript:window.history.back(1);return false;" />
                    &nbsp &nbsp
                    <asp:Button ID="btnScheduleAppointment" UseSubmitBehavior="false" runat="server"
                        Text="Schedule Appointment" OnClick="btnScheduleAppointment_Click" />
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <ajax:ModalPopupExtender ID="mdlpopupAppointmentConfirmed" runat="server" PopupControlID="pnlAppointmentConfirmed"
        TargetControlID="lblHiddenEntry" CancelControlID="btnCancel" BackgroundCssClass="modalBackground">
    </ajax:ModalPopupExtender>
    <asp:Label Text="" ID="lblHiddenEntry" runat="server" />
    <asp:Panel ID="pnlAppointmentConfirmed" runat="server" CssClass="modalPopupSchedular"
        Style="width: 380px; border-color: black; border-bottom-style: solid; border-width: 1px;">
        <div style="height: auto; clear: both;">
            <table id="Table1" style="width: 400px; text-align: left; margin-right: 10px;">
                <tr>
                    <td>
                        <b>Appointment Confirmed</b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <hr class="InspectionArrangeHr" />
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:UpdatePanel runat="server" ID="updPnlSuccessMessage" UpdateMode="Conditional">
                            <ContentTemplate>
                             <asp:Label Text="" ID="lblSuccessMessage" runat="server" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                       
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td style="padding-left: 38px;">
                        <asp:Button Text="Close" runat="server" ID="btnCancel" CssClass="margin_right20" />
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
</asp:Content>
