﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PDR_BusinessLogic.Reports.ReportsRdlcBL;
using PDR_BusinessObject.Reports;
using Microsoft.Reporting.WebForms;
using System.Data;
using System.Threading;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace PropertyDataRestructure.Views.RdlcReports
{
    public partial class RentLetter : System.Web.UI.Page
    {
        private ReportsRdlcBL ReportsRdlcBLObj = new ReportsRdlcBL();
        private int action = 0;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["tenancyIds"] != null && Request.QueryString["tenancyIds"] != "" && Request.QueryString["PrintAction"] != null && Request.QueryString["PrintAction"] != "")
                {
                    if (!IsPostBack)
                    {

                        string tenancyIds = Request.QueryString["tenancyIds"];
                        string PrintAction = Request.QueryString["PrintAction"];

                        if (PrintAction == "FirstPrint")
                            action = 1;

                        string dataSourceName = "MultiPrint";
                        List<TenancyIds> resultDataSet = ReportsRdlcBLObj.populateRentLetterReport(tenancyIds);

                        ReportDataSource repds = new ReportDataSource();
                        repds.Name = dataSourceName;
                        repds.Value = resultDataSet;
                        this.rptMultiPrint.LocalReport.DataSources.Add(repds);

                        this.rptMultiPrint.ProcessingMode = ProcessingMode.Local;
                        this.rptMultiPrint.LocalReport.SubreportProcessing += new SubreportProcessingEventHandler(SubreportProcessingEventHandler);
                        this.rptMultiPrint.LocalReport.Refresh();
                    }
                }
            }
            catch (ThreadAbortException ex)
            {
                //Response.Redirect throw this exception to end processing of the current page. We have to catch this exception and destroy it.
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessage.isError == true)
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }

        void SubreportProcessingEventHandler(object sender, SubreportProcessingEventArgs e)
        {

            string Ten_ID = e.Parameters[0].Values[0].ToString();
            List<C__RentLetter_MultiPrint_BO> resultDataSet = ReportsRdlcBLObj.getRentLetterMultiPrint(Ten_ID, action);
            string dataSourceName = e.DataSourceNames[0];
            ReportDataSource repds = new ReportDataSource();
            repds.Name = dataSourceName;
            repds.Value = resultDataSet;
            e.DataSources.Add(repds);

        }
    }
}