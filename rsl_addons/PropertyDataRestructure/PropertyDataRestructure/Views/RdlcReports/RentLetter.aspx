﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RentLetter.aspx.cs" Inherits="PropertyDataRestructure.Views.RdlcReports.RentLetter" %>

<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        body:nth-of-type(1) img[src*="Blank.gif"]
        {
            display: none;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div>
        <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="400px" />
        <rsweb:ReportViewer ID="rptMultiPrint" runat="server" Font-Names="Verdana" Font-Size="8pt"
            InteractiveDeviceInfos="(Collection)" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt"
            Width="100%" Height="600px">
            <LocalReport ReportPath="Views\RdlcReports\MultiPrintReport.rdlc">
            </LocalReport>
        </rsweb:ReportViewer>
    </div>
    </form>
</body>
</html>
