﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PDR_DataAccess.SchemeBlock;
using PDR_BusinessLogic.SchemeBlock;
using PDR_Utilities.Constants;
using System.IO;
using System.Configuration;
using System.Threading;
using PropertyDataRestructure.Base;
using System.Data;
using PropertyDataRestructure.UserControls.Common;
using PDR_Utilities.Helpers;

namespace PropertyDataRestructure.Views.Common
{
    public partial class Download : System.Web.UI.Page
    {
        public int documentId = 0;
        public string documentName = String.Empty;
        public string documentPath = String.Empty;
        public string documentExt= String.Empty;
        public string type = String.Empty;
        public string typeId = null;

        public UIMessage uiMessage = new UIMessage();
        
        
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (Request.QueryString["documentId"] == null || Request.QueryString["documentId"].ToString().Equals(string.Empty))
            {
                documentId = -1;
            }
            else
            {
                documentId = int.Parse(Request.QueryString["documentId"].ToString());
            }
                
            type = Request.QueryString["type"].ToString();

            if (type.Equals("CP12Document"))
            {
                downloadCP12Document(documentId);
            }
            else
            {
                DocumentsBL objDocumentTypeBL = new DocumentsBL(new DocumentsRepo());
                objDocumentTypeBL.getDocumentToDownload(ref documentId, ref type, ref documentName, ref documentPath, ref documentExt, ref typeId);

                documentPath = getFileDirectoryPath(); // for all i.e. Development/Schem/Property
                downloadFile();
            }

            
                
        }

        public void downloadFile()
        {
            
            try
            {
                try
                {
                    this.documentExt = this.documentName.Substring(this.documentName.LastIndexOf(".") + 1);
                    
                }
                catch
                {
                }

                if ((Directory.Exists(this.documentPath) == false))
                {
                    throw new Exception("Directory doesn't exists.");
                }
                else
                {
                    if (File.Exists(this.documentPath + this.documentName) == false)
                    {
                        throw new Exception("File doesn't exists.");
                    }
                }

                Response.Clear();
                switch (documentExt)
                {
                    case "pdf":
                        this.type = "application/pdf";
                        break; // TODO: might not be correct. Was : Exit Select
                    case "doc":
                        this.type = "application/vnd.ms-word";
                        break; // TODO: might not be correct. Was : Exit Select

                    case "docx":
                        this.type = "application/vnd.ms-word";
                        break; // TODO: might not be correct. Was : Exit Select

                    case "xls":
                        this.type = "application/vnd.ms-excel";
                        break; // TODO: might not be correct. Was : Exit Select

                    case "xlsx":
                        this.type = "application/vnd.ms-excel";
                        break; // TODO: might not be correct. Was : Exit Select

                    case "png":
                        this.type = "application/image/png";
                        break; // TODO: might not be correct. Was : Exit Select

                    case "jpg":
                        this.type = "application/image/jpg";
                        break; // TODO: might not be correct. Was : Exit Select

                }
                Response.AppendHeader("content-disposition", "attachment; filename=\"" + this.documentName + "\"");
                Response.ContentType = this.type;
                Response.WriteFile(this.documentPath + this.documentName);
                Response.Flush();
                Response.End();
            }
            catch (ConfigurationErrorsException ex)
            {
                uiMessage.isError= true;
                uiMessage.messageText = UserMessageConstants.DocumentUploadPathKeyDoesNotExist;
            }
            catch (DirectoryNotFoundException ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;
            }
            catch (FileNotFoundException ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;
            }
            catch (ThreadAbortException ex)
            {
                uiMessage.messageText = ex.Message;
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;
            }
            finally
            {
                if (uiMessage.isError == true)
                {
                    //uiMessage.showErrorMessage(lblMessage, pnlMessage, uiMessage.messageText, true);
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }

            }
        }

        public string getFileDirectoryPath()
        {
            //getQueryString();

            var fileDirectoryPath = string.Empty;
            if (type == "Development")
            {
                
                fileDirectoryPath = Server.MapPath(ResolveClientUrl(ConfigHelper.GetDevelopmentDocUploadPath()));
            }
            else if (type == "Scheme")
            {

                fileDirectoryPath = Server.MapPath(ResolveClientUrl(ConfigHelper.GetSchemeDocUploadPath()) + typeId.ToString() + "/Documents/");
            }
            else if (type == "Property")// Will have to figure out this thing
            {

                fileDirectoryPath = Server.MapPath(ResolveClientUrl(ConfigHelper.GetPropertyDocUploadPath()) + typeId.ToString() + "/Documents/");
            }
            
            return fileDirectoryPath;
        }



        private void getQueryString()
        {

            if ((Request.QueryString[ApplicationConstants.DocumentId] != null))
            {
                typeId = Request.QueryString[ApplicationConstants.Id];
            }

            if ((Request.QueryString[ApplicationConstants.Type] != null))
            {

                type = Request.QueryString[ApplicationConstants.Type];
            }
        }


        #region "Download CP12Document"

        public void downloadCP12Document(int documentId)
        {

            try
            {
                int LGSRID = documentId;
                DocumentsBL objDocumentTypeBL = new DocumentsBL(new DocumentsRepo());
                DataSet dataSet = new DataSet();
                objDocumentTypeBL.getCP12DocumentByLGSRID(ref dataSet, ref LGSRID);

                if (dataSet.Tables.Count > 0 && dataSet.Tables[0].Rows.Count > 0)
                {
                    var _with1 = dataSet.Tables[0].Rows[0];
                    string propertyID = dataSet.Tables[0].Rows[0][0].ToString();

                    if (! dataSet.Tables[0].Rows[0][3].Equals(DBNull.Value))
                    {
                        Byte[] CP12DOCUMENT = (Byte[])dataSet.Tables[0].Rows[0][3];

                         //Send the file to the browser
                        Response.AddHeader("Content-type", "application/pdf");
                        Response.AddHeader("Content-Disposition", "attachment; filename=CP12Document_" + propertyID + ".pdf");

                        Response.BinaryWrite(CP12DOCUMENT);
                    }
                    else
                    {

                        ExceptionHelper.IsError= true;
                        ExceptionHelper.ExcpetionMessage = UserMessageConstants.Cp12DocumentNotAvailable;
                    }

                }
                else
                {

                    ExceptionHelper.IsError = true;
                    ExceptionHelper.ExcpetionMessage = UserMessageConstants.Cp12DocumentNotAvailable;
                }
            }
            catch (ThreadAbortException ex)
            {
                ExceptionHelper.IsError = true;
                ExceptionHelper.setMessage(ref lblMessage, ref pnlMessage, ex.InnerException.ToString(), true);
            }
            catch (Exception ex)
            {
                ExceptionHelper.IsError = true;
                ExceptionHelper.setMessage(ref lblMessage,ref pnlMessage,"exception text",true);
            }
            finally
            {
                if (ExceptionHelper.IsError == true)
                {
                    ExceptionHelper.setMessage(ref lblMessage,ref pnlMessage, ExceptionHelper.ExcpetionMessage, true);
                }
                else
                {
                    //Flush the response to avoid blank window if there is no error.
                    Response.Flush();
                    Response.End();
                }

            }
        }

        #endregion


    }
}