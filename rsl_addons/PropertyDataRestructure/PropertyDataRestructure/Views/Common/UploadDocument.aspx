﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UploadDocument.aspx.cs"
    MasterPageFile="~/MasterPage/Blank.Master" Inherits="PropertyDataRestructure.Views.Common.UploadDocument" %>

<%@ Register TagPrefix="uim" TagName="UIMessage" Src="~/UserControls/Common/UIMessage.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script type="text/javascript">
        function documentUploadClick() {
            document.getElementById('<%= btnOk.ClientID %>').click();
        }  
    </script>
    <style type="text/css">
        .hidden
        {
            display: none;
        }
        
        .loading-image
        {
            background-color: #fff;
            width: 100%;
            height: 100%;
            position: fixed;
            top: 0px;
            right: 0px;
            z-index: 1000000;
            text-align: center;
            opacity: 0.7;
            margin-right: -30px;
        }
        
        .loading-image img
        {
            margin-top: 255px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="updPanelUploadDocument" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:Panel ID="pnlUploadDocument" runat="server">
                <uim:UIMessage ID="uiMessage" runat="Server" Visible="false" width="500px" />
                <table style="width: 100%; border: 1px solid black; height: 135px; text-align: left;
                    color: #000000; font-family: Tahoma; font-size: 12px; font-weight: bold;">
                    <tr style="background-color: Black; color: White; height: 30px;" class="pnlHeading">
                        <td>
                            &nbsp;&nbsp;Select file to upload</b>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;&nbsp;
                            <asp:FileUpload ID="FileUpload1" runat="server" onchange="documentUploadClick()" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btnOk" runat="server" BorderStyle="None" CausesValidation="False"
                                Height="0px" OnClick="btnOk_Click1" Text="OK" Width="0px" CssClass="hidden" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btnOk" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="updateProgressUploadDoc" runat="server" AssociatedUpdatePanelID="updPanelUploadDocument"
        DisplayAfter="10">
        <ProgressTemplate>
            <div class="loading-image">
                <img alt="Please Wait" src="../../Images/menu/progress.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
