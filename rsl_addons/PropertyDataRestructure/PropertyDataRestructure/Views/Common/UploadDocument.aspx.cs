﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_Utilities.Helpers;
using PDR_Utilities.Managers;
using PDR_Utilities.Constants;
using System.IO;
using System.Drawing;
using PropertyDataRestructure.Base;


namespace PropertyDataRestructure.Views.Common
{
    public partial class UploadDocument : PageBase
    {
        protected SessionManager objSession = new SessionManager();

        #region "Page Load"
        protected void Page_Load(object sender, System.EventArgs e)
        {
            uiMessage.hideMessage();
            this.Page.Form.Enctype = "multipart/form-data";
        }
        #endregion

        #region "get Document Type"
        private string getDocumentType()
        {
            if (Request.QueryString["type"] != null)
            {
                return Request.QueryString["type"];
            }
            else
            {
                uiMessage.showErrorMessage(UserMessageConstants.InvalidDocumentType);
                return null;
            }

        }
        #endregion

        #region "upload File"

        public void uploadFile(string docType)
        {

            if (FileUpload1.HasFile)
            {
                string filePath = string.Empty;
                string fileName = string.Empty;

                if (docType == "document")
                {
                    filePath = ConfigHelper.GetDevelopmentDocUploadPath();
                }
                else if (docType == "photo")
                {
                    filePath = Server.MapPath("../../Photographs/Images/");

                }
                else if (docType == "signature")
                {
                    filePath = Server.MapPath("../../../EmployeeSignature/");
                }

                if (Directory.Exists(filePath) == false)
                {
                    Directory.CreateDirectory(filePath);
                }

                fileName = Path.GetFileName(FileUpload1.FileName);
                fileName = getUniqueFileName();

                if (docType == "document")
                {
                    fileName = ApplicationConstants.DocPrefix + fileName;
                }

                string filePathName = filePath + fileName;
                FileUpload1.SaveAs(filePathName);

                if (docType == "document")
                {                    
                    objSession.DocumentUploadName = fileName;
                    if (Request.QueryString["popType"] != null)
                    {
                        Response.Write("<script type='text/javascript'>window.opener.fireAddDocUploadCkBoxEvent();window.close();</script>");
                    }
                    else
                    {
                        Response.Write("<script type='text/javascript'>window.opener.fireDocUploadCkBoxEvent();window.close();</script>");
                    }
                }
                else if (docType == "photo")
                {
                    generateThumbnail(fileName);
                    objSession.PhotoUploadName = fileName;

                    if (Request.QueryString["photoType"] != null)
                    {
                        Response.Write("<script type='text/javascript'>window.opener.fireDefectPhotoUploadCkBoxEvent();window.close();</script>");
                    }
                    else
                    {
                        Response.Write("<script type='text/javascript'>window.opener.firePhotoUploadCkBoxEvent();window.close();</script>");
                    }

                }
                else if (docType == "signature")
                {
                    objSession.SignatureUploadName = fileName;
                    Response.Write("<script type='text/javascript'>window.opener.fireSignatureNameEvent('" + fileName + "');window.close();</script>");
                }

            }
            else
            {
                uiMessage.showErrorMessage(UserMessageConstants.SelectDocument);                
            }

        }
        #endregion

        #region "get Unique File Name"
        private string getUniqueFileName()
        {

            string fileName = Path.GetFileNameWithoutExtension(FileUpload1.FileName);
            dynamic ext = Path.GetExtension(FileUpload1.FileName);
            string uniqueString = DateTime.Now.ToString().GetHashCode().ToString("x");

            if (fileName.Length > 35)
            {
                fileName = fileName.Substring(0, 35);
            }

            fileName = fileName + uniqueString;

            return fileName + ext;

        }
        #endregion

        #region "validate File"
        public bool validateFile()
        {
            dynamic isSuccess = true;
            string fileExt = Path.GetExtension(FileUpload1.FileName);
            int size = ConfigHelper.getFileSize();
            if (!fileExt.Equals(".pdf") && !fileExt.Equals(".doc") && !fileExt.Equals(".docx") && !fileExt.Equals(".xls") && !fileExt.Equals(".xlsx") && !fileExt.Equals(".png") && !fileExt.Equals(".jpg"))
            {
                isSuccess = false;
            }
            return isSuccess;
        }
        #endregion

        #region "btn Ok Click"

        protected void btnOk_Click1(object sender, EventArgs e)
        {
            try
            {
                string docType = string.Empty;
                docType = this.getDocumentType();
                if (docType != null)
                {
                    this.uploadFile(docType);
                }

            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if ((uiMessage.isExceptionLogged == false))
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if ((uiMessage.isError == true))
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #region "Generate Thumbnail"
        protected void generateThumbnail(string FileName)
        {
            //Create a new Bitmap Image loading from location of origional file
            string FilePath = string.Empty;
            FilePath = Server.MapPath("../../Photographs/Images/");
            Bitmap bm = (Bitmap)System.Drawing.Image.FromFile(FilePath + FileName); 
            string strFileName = FileName;

            //Declare Thumbnails Height and Width
            int newWidth = 100;
            int newHeight = 100;
            //Create the new image as a blank bitmap
            Bitmap resized = new Bitmap(newWidth, newHeight);
            //Create a new graphics object with the contents of the origional image
            Graphics g = Graphics.FromImage(resized);
            //Resize graphics object to fit onto the resized image
            g.DrawImage(bm, new Rectangle(0, 0, resized.Width, resized.Height), 0, 0, bm.Width, bm.Height, GraphicsUnit.Pixel);
            //Get rid of the evidence
            g.Dispose();
            FilePath = Server.MapPath("../../Photographs/Thumbs/");
            if ((Directory.Exists(FilePath) == false))
            {
                Directory.CreateDirectory(FilePath);
            }
            //Create new path and filename for the resized image
            string newStrFileName = FilePath + strFileName;
            //Save the new image to the same folder as the origional
            resized.Save(newStrFileName);
            resized.Dispose();
            bm.Dispose();
        }
        public UploadDocument()
        {
            Load += Page_Load;
        }
        #endregion

    }

}