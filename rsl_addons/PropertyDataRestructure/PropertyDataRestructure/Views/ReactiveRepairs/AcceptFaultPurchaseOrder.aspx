﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage/ReactiveRepair.Master" AutoEventWireup="true"
    CodeBehind="AcceptFaultPurchaseOrder.aspx.cs" Inherits="PropertyDataRestructure.Views.ReactiveRepairs.AcceptFaultPurchaseOrder" %>

<%@ Register TagPrefix="uc1" TagName="PurchaseOrder" Src="~/UserControls/ReactiveRepairs/PurchaseOrder.ascx" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdateProgress ID="updateProgressReportsArea" runat="server" DisplayAfter="10">
        <ProgressTemplate>
            <div class="loading-image">
                <img alt="Please Wait" src="../../../../Images/menu/progress.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <asp:UpdatePanel ID="updPanelPurchaseOrder" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <uc1:PurchaseOrder ID="ucPurchaseOrder" runat="server" Visible="true"></uc1:PurchaseOrder>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
