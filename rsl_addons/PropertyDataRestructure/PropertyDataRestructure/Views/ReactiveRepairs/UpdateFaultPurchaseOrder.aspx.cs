﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using PropertyDataRestructure.Interface;
using PDR_BusinessLogic.PurchaseOrder;
using PDR_DataAccess.PurchaseOrder;
using PDR_BusinessLogic.UserAccessBL;
using PDR_DataAccess.UserAccess;
using PDR_Utilities.Managers;
using PDR_Utilities.Constants;
namespace PropertyDataRestructure.Views.ReactiveRepairs
{
    public partial class UpdateFaultPurchaseOrder : System.Web.UI.Page
    {
        private int employeeId;
        private int orderID;

        #region Page load event
        protected void Page_Load(object sender, EventArgs e)
        {
            employeeId = int.Parse(Request.QueryString["uid"]);
            orderID = int.Parse(Request.QueryString["oid"]);
            if (!IsPostBack)
            {
                if (checkStatus(orderID) == ApplicationConstants.AcceptedByContractor)
                {
                    populateData();
                }
                else
                {
                    Response.Redirect("PageExpireMessage.aspx");
                }
            }
        }
        #endregion

        #region populate data
        public void populateData()
        {
            UserAccessBL objUserAccessBL = new UserAccessBL(new UserAccessRepo());
            SessionManager objSessionManager = new SessionManager();
            DataSet resultDs = new DataSet();
            resultDs = objUserAccessBL.getEmployeeById(employeeId);
            objSessionManager.UserFullName = resultDs.Tables[0].Rows[0]["FullName"].ToString();
            objSessionManager.EmployeeId = Convert.ToInt32(resultDs.Tables[0].Rows[0]["EmployeeId"]);
            objSessionManager.UserType = Convert.ToString(resultDs.Tables[0].Rows[0]["UserType"]);

            ucUpdatePurchaseOrder.Visible = true;
            ucUpdatePurchaseOrder.populateData(orderID,employeeId);//orderID, employeeId);
        }
        #endregion

        public string checkStatus(int Order_Id)
        {
            PurchaseOrderBL objPurchaseOrderBL = new PurchaseOrderBL(new PurchaseOrderRepo());
            return objPurchaseOrderBL.GetPoStatusForSessionCreation(Order_Id);
        }



        #region Search Property
   [System.Web.Services.WebMethod()]
        public static List<string> getRepairSearchResult(string prefixText, int count)
        {
            PurchaseOrderBL objPurchaseOrderBL = new PurchaseOrderBL(new PurchaseOrderRepo());
            DataSet resultDataset = new DataSet();
            objPurchaseOrderBL.getRepairSearchResult(ref resultDataset, ref prefixText);
            List<string> result = new List<string>();
            foreach (DataRow row in resultDataset.Tables[0].Rows)
            {
               string item = AjaxControlToolkit.AutoCompleteExtender.CreateAutoCompleteItem(row["Description"].ToString(), row["FaultRepairListID"].ToString());
               result.Add(item);
            }
            return result;
        }

    #endregion

    }
}