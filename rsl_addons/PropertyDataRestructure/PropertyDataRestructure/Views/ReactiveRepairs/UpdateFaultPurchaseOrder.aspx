﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage/ReactiveRepair.Master" CodeBehind="UpdateFaultPurchaseOrder.aspx.cs" Inherits="PropertyDataRestructure.Views.ReactiveRepairs.UpdateFaultPurchaseOrder" %>

 <%@ Register TagPrefix="uc1" TagName="UpdatePurchaseOrder" Src="~/UserControls/ReactiveRepairs/UpdatePurchaseOrder.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
<script src="../../Scripts/jquery-v1.9.1.js" type="text/javascript"></script>
 
   
        
            <script type="text/javascript">
                function ClientItemSelected(sender, e) {
                    $get("<%=hdnSelectedRepairId.ClientID %>").value = e.get_value();
                }
         </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<asp:UpdatePanel ID="updPanelUpdatePurchaseOrder" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
     
        <uc1:UpdatePurchaseOrder ID="ucUpdatePurchaseOrder" runat="server"  Visible="true"></uc1:UpdatePurchaseOrder>       
         </ContentTemplate>
   </asp:UpdatePanel>
     <div style="display: none;">
                    <asp:HiddenField ID="hdnSelectedRepairId" runat="server" />
                </div>
</asp:Content>
