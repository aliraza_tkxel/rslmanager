﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PDR_BusinessLogic.UserAccessBL;
using PDR_DataAccess.UserAccess;
using PDR_Utilities.Managers;
using PDR_BusinessLogic.PurchaseOrder;
using PDR_DataAccess.PurchaseOrder;
using PDR_Utilities.Constants;
using PropertyDataRestructure.Base;

namespace PropertyDataRestructure.Views.ReactiveRepairs
{
    public partial class AcceptFaultPurchaseOrder : System.Web.UI.Page//PageBase
    {
        private int employeeId;
        private int orderID;
        protected void Page_Load(object sender, EventArgs e)
        {
            employeeId = int.Parse(Request.QueryString["uid"]);
            orderID = int.Parse(Request.QueryString["oid"]);

     

            if (!IsPostBack)
            {
                if (checkStatus(orderID) == ApplicationConstants.AssignedToContractor)
                {
                    populateData();
                }
                else
                {
                    Response.Redirect("PageExpireMessage.aspx");
                }
                
            }
        }
        public void populateData()
        {
            UserAccessBL objUserAccessBL = new UserAccessBL(new UserAccessRepo());
            SessionManager objSessionManager = new SessionManager();
            DataSet resultDs = new DataSet();
            resultDs = objUserAccessBL.getEmployeeById(employeeId);
            objSessionManager.UserFullName = resultDs.Tables[0].Rows[0]["FullName"].ToString();
            objSessionManager.EmployeeId = Convert.ToInt32(resultDs.Tables[0].Rows[0]["EmployeeId"]);
            objSessionManager.UserType = Convert.ToString(resultDs.Tables[0].Rows[0]["UserType"]);

           ucPurchaseOrder.Visible = true;
           ucPurchaseOrder.populateData(orderID,employeeId);
        }
        public string checkStatus(int Order_Id)
        {
            PurchaseOrderBL objPurchaseOrderBL = new PurchaseOrderBL(new PurchaseOrderRepo());
            return objPurchaseOrderBL.GetPoStatusForSessionCreation(Order_Id);
        }
    }
}