﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using PDR_Utilities.Managers;
using PDR_Utilities.Constants;
using PDR_BusinessLogic.UserAccessBL;
using PDR_DataAccess.UserAccess;
using PDR_Utilities.Helpers;

namespace PropertyDataRestructure
{
    public partial class SchemeRecordMaster : System.Web.UI.MasterPage
    {

        public string messageClass = "topmessagesuccess";
        protected SessionManager session = new SessionManager();

        #region "Events"

        #region "Page Load"
        /// <summary>
        /// Page Load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void Page_Load(object sender, System.EventArgs e)
        {
            // highLightMenuItems()

            if (!Page.IsPostBack)
            {
               populateMenus();     
            }

        }

        #endregion

        #region "Repeater Row Data"
        /// <summary>
        /// Repeater Row Data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>

        protected void rptPages_RowDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {

            SessionManager objSession = new SessionManager();

            if ((e.Item.ItemType == ListItemType.Item) | (e.Item.ItemType == ListItemType.AlternatingItem))
            {
                HiddenField hdnPageId = (HiddenField)e.Item.FindControl("hdnPageId");
                HiddenField hdnAccessLevel = (HiddenField)e.Item.FindControl("hdnAccessLevel");
                Panel pnlPageMenu = (Panel)e.Item.FindControl("pnlPageMenu");
                int pageId = Convert.ToInt32(hdnPageId.Value);
                int accessLevel = Convert.ToInt32(hdnAccessLevel.Value);
                Menu menuPage = (Menu)e.Item.FindControl("menuPage");
                DataSet accessDataSet = session.PageAccessDataSet;
                DataTable pageDt = accessDataSet.Tables[ApplicationConstants.AccessGrantedPagesDt];
                DataTable resultPageDt = pageDt.Clone();
                int parentId = 0;


                if (accessLevel == 1)
                {
                    var query = (from dataRow in pageDt.AsEnumerable()
                                 where
                                     dataRow.Field<int>(ApplicationConstants.GrantPageAccessLevelCol) == 1
                                 orderby dataRow.Field<string>(ApplicationConstants.GrantOrderTextCol)
                                 ascending
                                 select dataRow);

                    if (query.Count() > 0)
                    {
                        resultPageDt = query.CopyToDataTable();
                    }


                }
                else
                {

                    string parentExpression = ApplicationConstants.GrantPageIdCol + " = " + Convert.ToString(pageId);
                    DataRow[] parentRow = pageDt.Select(parentExpression);
                    string abc = parentRow[0].ToString();
                    if (parentRow.Count() > 0)
                    {
                        parentId = Convert.ToInt32(parentRow[0][ApplicationConstants.GrantPageParentPageCol]);

                        var query = (from dataRow in pageDt.AsEnumerable() where dataRow.Field<int?>(ApplicationConstants.GrantPageParentPageCol) == parentId orderby dataRow.Field<string>(ApplicationConstants.GrantOrderTextCol) ascending select dataRow);

                        if (query.Count() > 0)
                        {
                            resultPageDt = query.CopyToDataTable();
                        }

                    }

                }

                menuPage.Items.Clear();

                foreach (DataRow row in resultPageDt.Rows)
                {

                    //Only the manager can view the Admin Calendar

                    if (row[ApplicationConstants.GrantPagePageNameCol].Equals(ApplicationConstants.AdminSchedulingPage))
                    {
                        if (!(objSession.UserType.Equals(ApplicationConstants.ManagerUserType)))
                        {
                            continue;
                        }
                    }

                    MenuItem item = new MenuItem();
                    item.Text = row[ApplicationConstants.GrantPagePageNameCol].ToString();
                    item.Value = row[ApplicationConstants.GrantPageIdCol].ToString();
                    item.NavigateUrl = row[ApplicationConstants.GrantPageCoreUrlCol].ToString();

                    if (Convert.ToInt32(row[ApplicationConstants.GrantPageIdCol]) == pageId)
                    {
                        item.Selected = true;
                    }

                    if ((string.IsNullOrEmpty(item.NavigateUrl) | string.IsNullOrWhiteSpace(item.NavigateUrl))
                        && !checkChildExist(Convert.ToInt32(item.Value), pageDt))
                    {
                        item.Enabled = false;
                    }

                    menuPage.Items.Add(item);
                }
                menuPage.DataBind();

                if (menuPage.Items.Count == 0 & accessLevel == 1)
                {
                    pnlPageMenu.Visible = false;
                }


            }

        }
        #endregion

        #region "Page Item clicked"
        /// <summary>
        /// Menu Item clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>

        public void PageItem_MenuItemClick(object sender, MenuEventArgs e)
        {
            //Get page id of the selected item
            //Check navigate url is null or empty
            //if empty get its child and select first child and redirect to it.
            //if not empty then navigate to url

            string navigateUrl = e.Item.NavigateUrl;
            int pageId = Convert.ToInt32(e.Item.Value);
            DataSet resultDataset = session.PageAccessDataSet;
            DataTable pageDt = resultDataset.Tables[ApplicationConstants.AccessGrantedPagesDt];


            if (string.IsNullOrEmpty(navigateUrl) | string.IsNullOrWhiteSpace(navigateUrl))
            {
                DataRow row = (from dataRow in pageDt.AsEnumerable()
                               where dataRow.Field<int?>(ApplicationConstants.GrantPageParentPageCol) == pageId
                               orderby dataRow.Field<string>(ApplicationConstants.GrantOrderTextCol)
                                   ascending
                               select dataRow).FirstOrDefault();
                navigateUrl = Convert.ToString(row[ApplicationConstants.GrantPageCoreUrlCol]);
            }

            Response.Redirect(navigateUrl);

        }

        #endregion

        #endregion

        #region "Functions"

        #region "Remove Session Values"
        /// <summary>
        /// Remove Session Values
        /// </summary>
        /// <remarks></remarks>
        private void removeSessionValues()
        {
            Session.RemoveAll();
        }
        #endregion

        #region "Check child nodes exist"
        /// <summary>
        /// Check child nodes exist
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public bool checkChildExist(int parentId, DataTable pageDt)
        {
            var query = (from dataRow in pageDt.AsEnumerable()
                         where dataRow.Field<int?>(ApplicationConstants.GrantPageParentPageCol) == parentId
                         orderby dataRow.Field<string>(ApplicationConstants.GrantOrderTextCol)
                         ascending
                         select dataRow);
            return (query.Count() > 0 ? true : false);
        }

        #endregion

        #region "Populate Menus"
        /// <summary>
        /// Populate Menus
        /// </summary>
        /// <remarks></remarks>
        public void populateMenus()
        {
            UserAccessBL objUserAccessBL = new UserAccessBL(new UserAccessRepo());
            int userId = session.EmployeeId;

            string menu = getPageMenu();
            DataSet resultDataset = new DataSet();
            objUserAccessBL.getPropertyPageList(ref resultDataset, userId, ApplicationConstants.ServicingMenu);
            session.PageAccessDataSet = resultDataset;

            DataTable accessGrantedModulesDt = resultDataset.Tables[ApplicationConstants.AccessGrantedModulesDt];
            DataTable accessGrantedMenusDt = resultDataset.Tables[ApplicationConstants.AccessGrantedMenusDt];
            DataTable accessGrantedPagesDt = resultDataset.Tables[ApplicationConstants.AccessGrantedPagesDt];
            DataTable randomPageDt = resultDataset.Tables[ApplicationConstants.RandomPageDt];

            populateRSLModulesList(accessGrantedModulesDt);
            populateHeaderMenus(accessGrantedMenusDt, menu);
            //populatePages(accessGrantedPagesDt, randomPageDt);

        }

        #endregion

        #region "Populate Modules"
        /// <summary>
        /// Populate Modules
        /// </summary>
        /// <remarks></remarks>

        public void populateRSLModulesList(DataTable accessGrantedModulesDt)
        {
            rptRSLMenu.DataSource = accessGrantedModulesDt;
            rptRSLMenu.DataBind();

        }

        #endregion

        #region "Populate Menu"
        /// <summary>
        /// Populate header menu
        /// </summary>
        /// <remarks></remarks>
        public void populateHeaderMenus(DataTable accessGrantedMenusDt, string menu)
        {
            menuHeader.Items.Clear();
            foreach (DataRow parentItem in accessGrantedMenusDt.Rows)
            {
                MenuItem item = new MenuItem();
                item.Text = Convert.ToString(parentItem[ApplicationConstants.MenuCol]);
                item.Value = parentItem[ApplicationConstants.MenuIdCol].ToString();
                item.SeparatorImageUrl = PathConstants.SeparatorImagePath;
                item.NavigateUrl = parentItem[ApplicationConstants.UrlCol].ToString();

                if (parentItem[ApplicationConstants.MenuCol].Equals(menu))
                {
                    item.Selected = true;
                }


                if (string.IsNullOrEmpty(item.NavigateUrl) | string.IsNullOrWhiteSpace(item.NavigateUrl))
                {
                    item.Enabled = false;
                }


                menuHeader.Items.Add(item);
            }
            menuHeader.DataBind();

        }

        #endregion

        #region "Populate Pages"
        /// <summary>
        /// Populate Pages
        /// </summary>
        /// <remarks></remarks>

        public void populatePages(DataTable accessGrantedPagesDt, DataTable randomPageDt)
        {
            int pageId = 0;
            UserAccessBL objMasterPageBL = new UserAccessBL(new UserAccessRepo());
            bool pageFound = false;
            DataTable pageLevelDt = new DataTable();
            pageLevelDt.Columns.Add(ApplicationConstants.GrantPageIdCol, typeof(Int32));
            pageLevelDt.Columns.Add(ApplicationConstants.GrantPageAccessLevelCol, typeof(Int32));

            if (objMasterPageBL.checkPageExist(accessGrantedPagesDt, ref pageId))
            {
                pageFound = true;
            }
            else if (objMasterPageBL.checkPageExist(randomPageDt, ref pageId))
            {
                string parentPageExpression = ApplicationConstants.RandomPageIdCol + " = " + Convert.ToString(pageId);
                DataRow[] parentPageRows = randomPageDt.Select(parentPageExpression);
                if ((parentPageRows[0][ApplicationConstants.RandomParentPageCol] == null) || (parentPageRows[0][ApplicationConstants.RandomParentPageCol] == System.DBNull.Value))
                {
                    pageFound = false;
                }
                else
                {
                    pageId = Convert.ToInt32(parentPageRows[0][ApplicationConstants.RandomParentPageCol]);
                    pageFound = true;
                }

            }


            if (pageFound)
            {
                // Getting Child Page and its level so that child level pages also visible
                string childPageExpression = ApplicationConstants.GrantPageParentPageCol + " = " + Convert.ToString(pageId);
                string sortOrder = ApplicationConstants.GrantOrderTextCol + " ASC";
                DataRow[] childPageRows = accessGrantedPagesDt.Select(childPageExpression, sortOrder);


                if (childPageRows.Count() > 0)
                {
                    int childAccessLevel = Convert.ToInt32(childPageRows[0][ApplicationConstants.GrantPageAccessLevelCol]);
                    int childPageId = Convert.ToInt32(childPageRows[0][ApplicationConstants.GrantPageIdCol]);
                    DataRow dr = pageLevelDt.NewRow();
                    dr[ApplicationConstants.GrantPageIdCol] = childPageId;
                    dr[ApplicationConstants.GrantPageAccessLevelCol] = childAccessLevel;
                    pageLevelDt.Rows.Add(dr);
                    pageLevelDt.AcceptChanges();

                }

                // Getting all parent pages and their levels if exist
                bool allPageLevelsRetrieved = false;

                while (!allPageLevelsRetrieved)
                {
                    string pageExpression = ApplicationConstants.GrantPageIdCol + " = " + Convert.ToString(pageId);
                    DataRow[] pageRows = accessGrantedPagesDt.Select(pageExpression);

                    if (pageRows.Count() > 0)
                    {
                        int accessLevel = Convert.ToInt32(pageRows[0][ApplicationConstants.GrantPageAccessLevelCol]);

                        DataRow dr = pageLevelDt.NewRow();
                        dr[ApplicationConstants.GrantPageIdCol] = pageId;
                        dr[ApplicationConstants.GrantPageAccessLevelCol] = accessLevel;
                        pageLevelDt.Rows.Add(dr);
                        pageLevelDt.AcceptChanges();

                        if (pageRows[0][ApplicationConstants.GrantPageParentPageCol] == System.DBNull.Value
                            || (pageRows[0][ApplicationConstants.GrantPageParentPageCol] == null))
                        {
                            allPageLevelsRetrieved = true;
                        }
                        else
                        {
                            pageId = Convert.ToInt32(pageRows[0][ApplicationConstants.GrantPageParentPageCol]);
                        }
                    }
                }

                var query = (from dataRow in pageLevelDt.AsEnumerable() orderby dataRow.Field<int>(ApplicationConstants.GrantPageAccessLevelCol) ascending select dataRow);
                DataTable resultPageDt = pageLevelDt.Clone();
                resultPageDt = query.CopyToDataTable();

            }
            else
            {
                DataRow dr = pageLevelDt.NewRow();
                dr[ApplicationConstants.GrantPageIdCol] = ApplicationConstants.NoPageSelected;
                dr[ApplicationConstants.GrantPageAccessLevelCol] = ApplicationConstants.PageFirstLevel;
                pageLevelDt.Rows.Add(dr);
                pageLevelDt.AcceptChanges();

            }


        }

        #endregion

        #region "Get Page Menu"
        /// <summary>
        /// Get Page Menu
        /// </summary>
        /// <remarks></remarks>

        public string getPageMenu()
        {
            string menu = string.Empty;
            if (Request.QueryString[PathConstants.MsatType] != null)
            {
                if (Request.QueryString[PathConstants.MsatType] == PathConstants.CyclicMaintenance)
                {
                    menu = PathConstants.MaintenanceMenu;
                }
                else if (Request.QueryString[PathConstants.MsatType] == PathConstants.MEServicing)
                {
                    menu = PathConstants.ServicingMenu;
                }
            }
            else if (Request.QueryString[PathConstants.Menu] != null)
            {
                menu = Request.QueryString[PathConstants.Menu];
            }
            else
            {
                string pageFileName = HttpContext.Current.Request.Url.AbsolutePath.Substring(HttpContext.Current.Request.Url.AbsolutePath.LastIndexOf("/") + 1);
                menu = GeneralHelper.getPageMenu(pageFileName);
            }

            return menu;

        }

        #endregion

        #endregion

    }
}
