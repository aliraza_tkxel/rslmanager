﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using PDR_BusinessObject.PageSort;

namespace PropertyDataRestructure.Interface
{
    interface IListingPage
    {
        void loadData();
        void searchData();
        void populateData();       
        void printData();
        void exportGridToExcel();
        void exportToPdf();
        void downloadData();
        void applyFilters();


    }
}