﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace PropertyDataRestructure.Interface
{
    public interface IAddEditPage
    {
        void populateDropDown(DropDownList ddl);
        void populateDropDowns();
        void loadData();
        void saveData();
        bool validateData();
        void resetControls();                
    }
}