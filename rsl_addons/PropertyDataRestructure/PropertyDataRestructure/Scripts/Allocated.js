﻿
$(document).ready(function () {

    populateAllocated();
});

function populateAllocated() {
    //var rows_selected = [];
    //console.log('xxx');

    var table = $('#dtAllocated').DataTable({
        "destroy": true,
        "dom": "Bfrtip",
        "sPaginationType": "full_numbers",
        "iDisplayLength": 20,
        "processing": true,
        "serverSide": true,
        "Sort": true,

        "ajax":
                {
                    "url": "CyclicalServices.aspx/PopulateServicesAllocated",
                    "contentType": "application/json",
                    "type": "GET",
                    "dataType": "JSON",
                    "cache": false,
                    "data": function (d) {
                        return d;
                    },
                    "dataSrc": function (json) {
                        json.draw = json.d.draw;
                        json.recordsTotal = json.d.recordsTotal;
                        json.recordsFiltered = json.d.recordsFiltered;
                        json.data = json.d.data;
                        var return_data = json;
                        // console.log(json.draw);
                        return return_data.data;
                    }
                },
        "columns": [{ "title": "Service", "data": "ServiceItemId", "sClass": "hidden" },
                            { "title": "PO Ref", "data": "PORef", 'width': '1%' },
                            { "title": "Scheme", "data": "SchemeName", "sClass": "tdScheme" },
                            { "title": "Block", "data": "BlockName", "sClass": "tdScheme" },
                            { "title": "PostCode", "data": "PostCode" },
                            { "title": "Cycle Commencement", "data": "Commencement" },
                            { "title": "Contractor", "data": "Contractor" },
                            { "title": "Service Required", "data": "ServiceRequired" },
                             { "title": "Status", "data": "Status" },
                               { "title": "Contract Start/End", "data": "ContractStart" },
                              {}
                        ],
        'columnDefs': [{
            'targets': 10,
            'searchable': false,
            'orderable': false,
            'width': '1%',
            'className': 'dt-body-center',
            'render': function () {
                return '<img src="../../Images/aero.png" class="hoverImage"/>'
            }
        }]


    });

    $('#dtAllocated tbody').on('click', '.hoverImage', function () {
        var row = this.parentElement.parentElement;
        var data = table.row(row).data();
        var $row = $(row);
        var orderId = $row.find("td:nth-child(2)");
        if (orderId.text() != "" && orderId.text() != "-") {
            var employeeId = $('#hdnSession').val();
            url = "AcceptCyclicalPurchaseOrder.aspx?oid=" + orderId.text() + "&uid=" + employeeId + "&cancel=yes";
            console.log(url);
            var $iframe = $('#acceptiFrame');
            if ($iframe.length) {
                $iframe.attr('src', url);
            }
            $('#AcceptPopUp').modal('show');
        }
        else {
            showAlert("Invalid PO Ref!");
        }
    })

    $('#btnClose').click(function () {
        $('#AcceptPopUp').modal('hide');
    });
}

function showAlert(message) {
    dialog.alert({
        title: "Alert",
        message: message,
        button: "OK",
        animation: "fade"
    });
    return false;
}