﻿
$(document).ready(function () {

    PopulateUnassignedServices();
});

function PopulateUnassignedServices() {
    //var rows_selected = [];
    //console.log('xxx');
    

    var table = $('#dtCancelled').DataTable({
        "destroy": true,
        "dom": "Bfrtip",
        "sPaginationType": "full_numbers",
        "iDisplayLength": 20,
        "processing": true,
        "serverSide": true,
        "Sort": true,
        "ajax":
                {
                    "url": "CancelledOrderReport.aspx/PopulateCancelledOrder",
                    "contentType": "application/json",
                    "type": "GET",
                    "dataType": "JSON",
                    "cache": false,
                    "data": function (d) {
                        return d;
                    },
                    "dataSrc": function (json) {
                        json.draw = json.d.draw;
                        json.recordsTotal = json.d.recordsTotal;
                        json.recordsFiltered = json.d.recordsFiltered;
                        json.data = json.d.data;
                        var return_data = json;
                        // console.log(json.draw);
                        return return_data.data;
                    }
                },
                "columns": [{ "title": "OrderId", "data": "OrderId", "sClass": "hidden" },
                            { "title": "Date", "data": "CancelDate" },
                             { "title": "PO Number", "data": "PONumber" },
                            { "title": "Cancelled By", "data": "CancelledBy" },
                            { "title": "Reason", "data": "Reason", "sClass": "tdScheme"  },
                            { "title": "Scheme", "data": "Scheme" },
                            { "title": "Block", "data": "Block" },
                            { "title": "Attribute", "data": "Attribute", "sClass": "tdScheme" },
                            { "title": "Contractor", "data": "Contractor", "sClass": "tdScheme" },
                            { }
                        ],
                'columnDefs': [{
                    'targets': 9,
                    'searchable': false,
                    'orderable': false,
                    'width': '1%',
                    'className': 'dt-body-center',
                    'render': function () {
                        return '<img src="../../Images/aero.png" class="hoverImage"/>'
                    }
                }]
    });
    $('#dtCancelled tbody').on('click', '.hoverImage', function () {
        console.log("123")
        var row = this.parentElement.parentElement;
        var data = table.row(row).data();
        var $row = $(row);
        var orderId = $row.find("td:nth-child(1)");
        if (orderId.text() != "" && orderId.text() != "-") {

            var employeeId = $('#hdnSession').val();
            url = "../CyclicalServices/AcceptCyclicalPurchaseOrder.aspx?IsReadOnly=yes&oid=" + orderId.text() + "&uid=" + employeeId;
            console.log(url);
            var $iframe = $('#acceptiFrame');
            if ($iframe.length) {
                $iframe.attr('src', url);
            }
            $('#AcceptPopUp').modal('show');
        }
        else {
            showAlert("Invalid PO Ref!");
        }
    });

    $('#btnClose').click(function () {
        $('#AcceptPopUp').modal('hide');
    });
}

function showAlert(message) {
    dialog.alert({
        title: "Alert",
        message: message,
        button: "Ok",
        animation: "fade"
    });
    return false;
}