﻿
$(document).ready(function () {

    PopulateUnassignedServices();
});

function PopulateUnassignedServices() {
    //var rows_selected = [];
    //console.log('xxx');
    var schemeId = $('#hdnschemeId').val();
    var blockId = $('#hdnblockId').val();

    var table = $('#dtCyclicalTab').DataTable({
        "destroy": true,
        "dom": "Bfrtip",
        "sPaginationType": "full_numbers",
        "iDisplayLength": 20,
        "processing": true,
        "serverSide": true,
        "Sort": true,
        "ajax":
                {
                    "url": "UnassignedCyclicalReport.aspx/PopulateUnassignedServices?schemeId=" + schemeId,
                    "contentType": "application/json",
                    "type": "GET",
                    "dataType": "JSON",
                    "cache": false,
                    "data": function (d) {
                        return d;
                    },
                    "dataSrc": function (json) {
                        json.draw = json.d.draw;
                        json.recordsTotal = json.d.recordsTotal;
                        json.recordsFiltered = json.d.recordsFiltered;
                        json.data = json.d.data;
                        var return_data = json;
                        // console.log(json.draw);
                        return return_data.data;
                    }
                },
        "columns": [{ "title": "Service", "data": "ServiceItemId", "sClass": "hidden" },
                            { "title": "Scheme", "data": "SchemeName", "sClass": "tdScheme" },
                            { "title": "Block", "data": "BlockName", "sClass": "tdScheme" },
                            { "title": "Attribute", "data": "ServiceRequired", "sClass": "tdScheme" },
                            { "title": "Cycle (Every)", "data": "Cycle" },
                            { "title": "Commencement", "data": "Commencement" },
                            { "title": "Recent Completion", "data": "RecentCompletion" },
                            { "title": "Cycle Remaining", "data": "RemainingCycle" },
                            { "title": "Next Cycle", "data": "NextCycle" },
                            { "title": "Contractor", "data": "Contractor", "sClass": "tdScheme" },

                        ]
    });


}

function showAlert(message) {
    dialog.alert({
        title: "Alert",
        message: message,
        button: "Ok",
        animation: "fade"
    });
    return false;
}