﻿
$(document).ready(function () {

    PopulateCyclicalTab();
});

function PopulateCyclicalTab() {
    //var rows_selected = [];
    //console.log('xxx');
    var schemeId = $('#hdnschemeId').val();
    var blockId = $('#hdnblockId').val();

    var table = $('#dtCyclicalTab').DataTable({
        "destroy": true,
        "dom": "Bfrtip",
        "sPaginationType": "full_numbers",
        "iDisplayLength": 20,
        "processing": true,
        "serverSide": true,
        "Sort": true,
        "ajax":
                {
                    "url": "CyclicalServices.aspx/PopulateCyclicalTab?schemeId=" + schemeId + '&blockId=' + blockId,
                    "contentType": "application/json",
                    "type": "GET",
                    "dataType": "JSON",
                    "cache": false,
                    "data": function (d) {
                        return d;
                    },
                    "dataSrc": function (json) {
                        json.draw = json.d.draw;
                        json.recordsTotal = json.d.recordsTotal;
                        json.recordsFiltered = json.d.recordsFiltered;
                        json.data = json.d.data;
                        var return_data = json;
                        // console.log(json.draw);
                        return return_data.data;
                    }
                },
        "columns": [{ "title": "Service", "data": "ServiceItemId", "sClass": "hidden" },
                            { "title": "Block", "data": "BlockName" },
                            { "title": "Attribute", "data": "ServiceRequired", "sClass": "tdScheme" },
                            { "title": "Cycle (Every)", "data": "Cycle" },
                            { "title": "Cycle Commencement", "data": "Commencement" },
                            { "title": "Recent Completion", "data": "RecentCompletion" },
                            { "title": "Cycle Remaining", "data": "RemainingCycle" },
                            { "title": "Next Cycle", "data": "NextCycle" },
                            { "title": "Contractor", "data": "Contractor", "sClass": "tdScheme" },
                            { "title": "PO Ref", "data": "PORef" },
                             {}
                        ],
        'columnDefs': [{
            'targets': 10,
            'searchable': false,
            'orderable': false,
            'width': '1%',
            'className': 'dt-body-center',
            'render': function () {
                return '<img src="../../Images/aero.png" class="hoverImage"/>'
            }
        }]
    });

    $('#dtCyclicalTab tbody').on('click', '.hoverImage', function () {
        var row = this.parentElement.parentElement;
        var data = table.row(row).data();
        var $row = $(row);
        var orderId = $row.find("td:nth-child(10)");
        if (orderId.text() != "" && orderId.text() != "-") {
            //        var thisPosition = $row.find("td:nth-child(2)");
            //        var thisPositionText = thisPosition.text();
            //        console.log(orderId.text());
            //        console.log(thisPositionText);
            var employeeId = $('#hdnSession').val();
            url = "AcceptCyclicalPurchaseOrder.aspx?IsReadOnly=yes&oid=" + orderId.text() + "&uid=" + employeeId;
            console.log(url);
            var $iframe = $('#acceptiFrame');
            if ($iframe.length) {
                $iframe.attr('src', url);
            }
            $('#AcceptPopUp').modal('show');
        }
        else {
            showAlert("Invalid PO Ref!");
        }
    })

    $('#btnClose').click(function () {
        $('#AcceptPopUp').modal('hide');
    });
}

function showAlert(message) {
    dialog.alert({
        title: "Alert",
        message: message,
        button: "OK",
        animation: "fade"
    });
    return false;
}