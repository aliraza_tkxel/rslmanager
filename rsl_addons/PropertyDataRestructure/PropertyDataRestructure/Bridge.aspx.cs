﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using PDR_BusinessLogic.UserAccessBL;
using PDR_DataAccess.UserAccess;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PDR_Utilities.Constants;
using PDR_Utilities.Managers;
using PDR_Utilities.Helpers;
using System.Collections.Specialized;
using System.Threading;

namespace PropertyDataRestructure
{
    public partial class Bridge : MSDN.SessionPage
    {
        #region Properties
        int classicUserId = 0;
        SessionManager objSession = new SessionManager();
        #endregion

        #region "Events"

        #region "Page Load"
        /// <summary>
        /// Event fires on page load.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <remarks></remarks>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                this.loadUser();
            }
            catch (ThreadAbortException ex)
            {
                //Response.Redirect throw this exception to end processing of the current page. We have to catch this exception and destroy it.
            }
            catch (Exception ex)
            {
                uiMessage.isError = true;
                uiMessage.messageText = ex.Message;

                if (uiMessage.isExceptionLogged == false)
                {
                    ExceptionPolicy.HandleException(ex, "Exception Policy");
                }
            }
            finally
            {
                if (uiMessage.isError == true)
                {
                    uiMessage.showErrorMessage(uiMessage.messageText);
                }
            }
        }
        #endregion

        #endregion

        #region "Functions"

        #region "Load User"
        /// <summary>
        /// Load User
        /// </summary>
        /// <remarks></remarks>

        private void loadUser()
        {
            UserAccessBL objUserAccessBL = new UserAccessBL(new UserAccessRepo());
            DataSet resultDataSet = new DataSet();
            this.checkClassicAspSession();

            resultDataSet = objUserAccessBL.getEmployeeById(classicUserId);

            if (resultDataSet.Tables[0].Rows.Count == 0)
            {
                uiMessage.showErrorMessage(UserMessageConstants.UserDoesNotExist);
            }
            else
            {
                bool isActive = Convert.ToBoolean(resultDataSet.Tables[0].Rows[0]["IsActive"]);

                if (isActive == false)
                {
                    uiMessage.showErrorMessage(UserMessageConstants.UsersAccountDeactivated);
                }
                else
                {
                    setUserSession(ref resultDataSet);

                    if (Request.QueryString[PathConstants.Page] != null)
                    {
                        string url = GeneralHelper.getPageUrl(Request.QueryString[PathConstants.Page]);

                        NameValueCollection pageQueryString = HttpUtility.ParseQueryString(HttpContext.Current.Request.QueryString.ToString());
                        
                        pageQueryString.Remove("pg");

                        if (url.Contains("?") && pageQueryString.Count > 0)
                        {
                            url = url + "&" + pageQueryString;
                        }
                        else if (pageQueryString.Count > 0)
                        {
                            url = url + "?" + pageQueryString;
                        }
                        

                        //if (!string.IsNullOrEmpty(pageQueryString.ToString()))
                        //{
                        //    url = url + "?" + pageQueryString;
                        //}
                        //if (Request.QueryString[PathConstants.Menu] != null)
                        //{
                        //    url = url + "?" + PathConstants.Menu + "=" + Request.QueryString[PathConstants.Menu];
                        //}


                        Response.Redirect(url);
                    }
                    else
                    {
                        redirectToAccessGrantedPage();
                    }
                }

            }
        }
        #endregion

        #region "Redirect to access granted page"
        /// <summary>
        /// Redirect to access granted page
        /// </summary>
        /// <remarks></remarks>
        public void redirectToAccessGrantedPage()
        {
            UserAccessBL objUserAccessBL = new UserAccessBL(new UserAccessRepo());
            DataTable resultDatatable = null;
            int userId = objSession.EmployeeId;
            string menu = PathConstants.AdminMenu;

            if (Request.QueryString[PathConstants.Menu] != null)
            {
                menu = Request.QueryString[PathConstants.Menu];
            }

            DataSet resultDataset = new DataSet();
            objUserAccessBL.getPropertyPageList(ref resultDataset, userId, menu);

            DataTable accessGrantedPagesDt = resultDataset.Tables[ApplicationConstants.AccessGrantedPagesDt];
            resultDatatable = accessGrantedPagesDt.Clone();
            string navigateUrl = string.Empty;

            var query = (from dataRow in accessGrantedPagesDt.AsEnumerable()
                         orderby dataRow.Field<int>(ApplicationConstants.GrantPageAccessLevelCol), dataRow.Field<int>(ApplicationConstants.GrantPageIdCol) ascending
                         where dataRow.Field<string>(ApplicationConstants.GrantPageCoreUrlCol).Trim() != string.Empty
                         select dataRow);

            if (query.Count() > 0)
            {
                resultDatatable = query.CopyToDataTable();
                navigateUrl = Convert.ToString(resultDatatable.Rows[0][ApplicationConstants.GrantPageCoreUrlCol]);
            }
            else
            {
                navigateUrl = PathConstants.AccessDeniedPath;
            }

            Response.Redirect(navigateUrl, true);

        }

        #endregion

        #region "Set User Session"
        /// <summary>
        /// Set User Session.
        /// </summary>
        /// <param name="resultDataSet"></param>
        /// <remarks></remarks>

        private void setUserSession(ref DataSet resultDataSet)
        {
            objSession.UserFullName = resultDataSet.Tables[0].Rows[0]["FullName"].ToString();
            objSession.EmployeeId = Convert.ToInt32(resultDataSet.Tables[0].Rows[0]["EmployeeId"]);
            objSession.UserType = Convert.ToString(resultDataSet.Tables[0].Rows[0]["UserType"]);
        }
        #endregion

        #region "Check Classic Asp Session"
        /// <summary>
        /// Check Classic Asp Session
        /// </summary>
        /// <remarks></remarks>

        private void checkClassicAspSession()
        {
            if (ASPSession["USERID"] != null)
            {
                classicUserId = int.Parse(ASPSession["USERID"].ToString());
            }
            else
            {
                this.redirectToLoginPage();
            }

        }
        #endregion

        #region "Redirect To Login Page"
        /// <summary>
        /// Redirect To Login Page
        /// </summary>
        /// <remarks></remarks>
        private void redirectToLoginPage()
        {
            Response.Redirect(PathConstants.LoginPath, true);
        }
        #endregion

        #region "Bridge"
        public Bridge()
        {
            Load += Page_Load;
        }
        #endregion

        #endregion

    }

}