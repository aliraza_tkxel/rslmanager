﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_Utilities.Enums
{
    /// <summary>
    /// The purpose of the enum class is to create the enums. Some variables can be assigned only lmited set of values e.g status can be inprogress or complete
    /// Enums will help us to stay defined & comprhensive.
    /// </summary>
    public class Enums
    {
        public enum BritishGasNotificationStatus
        {
            InProgress,
            Complete,
            Logged
        };

        

        public enum AppointmentParameter
        {
            OperativeId = 0,
            Operative = 1,
            AppointmentStartDateTime = 2,
            AppointmentEndDateTime = 3
        }


        public enum BritishGasNotificationStages
        {
            Stage1 = 1,
            Stage2 = 2,
            Stage3 = 3
        }

        public enum VoidAppointmentTypes
        {
            VoidInspection,
            PostVoidInspection,
            VoidWorks,
            VoidGasCheck,
            VoidElectricCheck
        }
    }
}
