﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using PDR_BusinessObject;
using PDR_Utilities.Helpers;
using PDR_BusinessObject.PageSort;
using System.Globalization;
using System.Data;
using System.Reflection;
using PDR_Utilities.Constants;
using System.IO;
using System.Web;
using System.Net;
using PDR_BusinessObject.ICalFile;
using System.Configuration;

namespace PDR_Utilities.Helpers
{
    public class GeneralHelper
    {

        #region "Set Grid View Pager"

        public static void setGridViewPager(ref Panel pnlPagination, PageSortBO objPageSortBO)
        {
            pnlPagination.Visible = false;

            if (objPageSortBO.TotalRecords >= 1)
            {
                pnlPagination.Visible = true;

                int pageNumber = objPageSortBO.PageNumber;
                int pageSize = objPageSortBO.PageSize;
                int TotalRecords = objPageSortBO.TotalRecords;
                int totalPages = objPageSortBO.TotalPages;

                LinkButton lnkbtnPagerFirst = (LinkButton)pnlPagination.FindControl("lnkbtnPagerFirst");
                LinkButton lnkbtnPagerPrev = (LinkButton)pnlPagination.FindControl("lnkbtnPagerPrev");
                LinkButton lnkbtnPagerNext = (LinkButton)pnlPagination.FindControl("lnkbtnPagerNext");
                LinkButton lnkbtnPagerLast = (LinkButton)pnlPagination.FindControl("lnkbtnPagerLast");

                lnkbtnPagerFirst.Enabled = (pageNumber != 1);
                lnkbtnPagerFirst.Font.Underline = lnkbtnPagerFirst.Enabled;

                lnkbtnPagerPrev.Enabled = lnkbtnPagerFirst.Enabled;
                lnkbtnPagerPrev.Font.Underline = lnkbtnPagerPrev.Enabled;

                lnkbtnPagerNext.Enabled = (pageNumber != totalPages);
                lnkbtnPagerNext.Font.Underline = lnkbtnPagerNext.Enabled;

                lnkbtnPagerLast.Enabled = lnkbtnPagerNext.Enabled;
                lnkbtnPagerLast.Font.Underline = lnkbtnPagerLast.Enabled;

                Label lblPagerCurrentPage = (Label)pnlPagination.FindControl("lblPagerCurrentPage");
                lblPagerCurrentPage.Text = pageNumber.ToString();

                Label lblPagerTotalPages = (Label)pnlPagination.FindControl("lblPagerTotalPages");
                lblPagerTotalPages.Text = totalPages.ToString();

                Label lblPagerRecordStart = (Label)pnlPagination.FindControl("lblPagerRecordStart");
                int pagerRecordStart = ((pageNumber - 1) * pageSize) + 1;
                lblPagerRecordStart.Text = pagerRecordStart.ToString();

                Label lblPagerRecordEnd = (Label)pnlPagination.FindControl("lblPagerRecordEnd");
                int pagerRecordEnd = pagerRecordStart + pageSize - 1;
                lblPagerRecordEnd.Text = (pagerRecordEnd < TotalRecords ? pagerRecordEnd : TotalRecords).ToString();

                Label lblPagerRecordTotal = (Label)pnlPagination.FindControl("lblPagerRecordTotal");
                lblPagerRecordTotal.Text = TotalRecords.ToString();

                //Set Range validation error Message and Maximum Value (Minimum value is 1).
                RangeValidator rangevalidatorPageNumber = (RangeValidator)pnlPagination.FindControl("rangevalidatorPageNumber");
                if (rangevalidatorPageNumber != null)
                {
                    rangevalidatorPageNumber.ErrorMessage = "Enter a page between 1 and " + totalPages.ToString();
                    rangevalidatorPageNumber.MaximumValue = totalPages.ToString();
                }
            }
        }

        #endregion

        #region "Convert list to datatable"
        /// <summary>
        /// Convert Generic list to data table
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static DataTable convertToDataTable<T>(List<T> list)
        {
            DataTable dt = new DataTable();
            foreach (PropertyInfo info in typeof(T).GetProperties())
            {
                dt.Columns.Add(new DataColumn(info.Name, Nullable.GetUnderlyingType(info.PropertyType) ?? info.PropertyType));
            }
            if (list != null)
            {
                foreach (T tt in list)
                {
                    DataRow row = dt.NewRow();
                    foreach (PropertyInfo info in typeof(T).GetProperties())
                    {
                        row[info.Name] = info.GetValue(tt, null) ?? DBNull.Value;
                    }
                    dt.Rows.Add(row);
                }
            }
            return dt;
        }

        #endregion
        #region Convert Datatable to List
        public static List<T> convertDatatableToList<T>(DataTable dt)
        {
            // Example 1:
            // Get private fields + non properties
            //var fields = typeof(T).GetFields(BindingFlags.NonPublic | BindingFlags.Instance);

            // Example 2: Your case
            // Get all public fields
            var fields = typeof(T).GetFields();

            List<T> lst = new List<T>();

            foreach (DataRow dr in dt.Rows)
            {
                // Create the object of T
                var ob = Activator.CreateInstance<T>();

                foreach (var fieldInfo in fields)
                {
                    foreach (DataColumn dc in dt.Columns)
                    {
                        // Matching the columns with fields
                        if (fieldInfo.Name == dc.ColumnName)
                        {
                            // Get the value from the datatable cell
                            object value = dr[dc.ColumnName];

                            // Set the value into the object
                            fieldInfo.SetValue(ob, value);
                            break;
                        }
                    }
                }

                lst.Add(ob);
            }

            return lst;
        }
        #endregion
        #region "Set currency Format"
        public static string currencyFormat(string value)
        {
            if (value != "")
            {
                string result = "£" + Convert.ToDecimal(value).ToString("#,##0.00");
                return result;
            }
            else
            {
                return "-";
            }
        }
        #endregion

        #region "Convert Date Time to UK Date format."
        /// <summary>
        /// This function 'll return the date in the following format in string (dd/mm/yyyy)
        /// </summary>
        /// <param name="aDate"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static DateTime convertDateToUkDateFormat(string aDate)
        {
            string format = "dd/MM/yyyy";
            return DateTime.ParseExact(aDate, format, CultureInfo.CreateSpecificCulture("en-UK"));
        }
        #endregion

        #region "get Appointment Creation Limit For Single Request"
        /// <summary>
        /// This function 'll return the appointment creation limit for single request
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public static int getAppointmentCreationLimitForSingleRequest()
        {
            return 5;
        }

        #endregion
        #region "get Appointment Creation Limit For Void appointment"
        /// <summary>
        /// This function 'll return the appointment creation limit for Void appointment
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public static int getAppointmentCreationLimitForVoidAppointment()
        {
            return 8;
        }

        #endregion

        #region "populate Official Hours With Fifteen Min Duration"
        /// <summary>
        /// this function 'll populate the drop down with official hours with fifteen minute Duration
        /// </summary>        
        /// <remarks></remarks>
        public static void populateOfficialHoursWithFifteenMinDuration(ref DropDownList ddl)
        {
            double startHour = ConfigHelper.getOfficialDayStartHour();
            double endHour = ConfigHelper.getOfficialDayEndHour();

            for (double i = startHour; i <= endHour; i++)
            {
                ddl.Items.Add(new ListItem(i.ToString() + ":00", i.ToString() + ":00"));
                if (i != endHour)
                {
                    ddl.Items.Add(new ListItem(i.ToString() + ":15", i.ToString() + ":15"));
                    ddl.Items.Add(new ListItem(i.ToString() + ":30", i.ToString() + ":30"));
                    ddl.Items.Add(new ListItem(i.ToString() + ":45", i.ToString() + ":45"));
                }

            }
        }
        #endregion

        #region "Get Date Time From Date And Number"
        /// <summary>
        /// This function 'll append the date with number to get date time and return it
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public static System.DateTime getDateTimeFromDateAndNumber(System.DateTime aDate, double aHour)
        {
            return Convert.ToDateTime(aDate.Date.ToLongDateString() + " " + aHour.ToString() + ":00");
        }
        #endregion

        #region "Convert Date In Seconds"
        public static double convertDateInSec(DateTime timeToConvert)
        {

            DateTime calendarStartDate = new DateTime(1970, 1, 1);
            TimeSpan ts = timeToConvert - calendarStartDate;
            return ts.TotalSeconds;
        }
        #endregion

        #region "Convert Date In Minute"
        public static double convertDateInMin(System.DateTime timeToConvert)
        {
            DateTime calendarStartDate = new DateTime(1970, 1, 1);
            TimeSpan ts = timeToConvert - calendarStartDate;
            return ts.TotalMinutes;
        }
        #endregion

        #region "Convert Date to Us Culture"
        /// <summary>
        /// This fucntion 'll convert the date in following format e.g Mon 12th Nov 2012
        /// </summary>
        /// <param name="dateToConvert"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static string convertDateToUsCulture(System.DateTime dateToConvert)
        {
            return Convert.ToDateTime(dateToConvert).ToString("dddd d MMMM yyyy", CultureInfo.CreateSpecificCulture("en-US"));
        }

        #endregion

        #region "Convert Time To 24 Hrs Format"
        /// <summary>
        /// This funciton will convert the datetime in 24 hours format
        /// </summary>
        /// <param name="timeToConvert"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static string ConvertTimeTo24hrsFormat(System.DateTime timeToConvert)
        {
            return timeToConvert.ToString("HH:mm");
        }
        #endregion

        #region "Convert UTF8 String to ASCII String"

        public static string UTF8toASCII(string text)
        {
            System.Text.Encoding utf8 = System.Text.Encoding.UTF8;
            Byte[] encodedBytes = utf8.GetBytes(text);
            Byte[] convertedBytes = Encoding.Convert(Encoding.UTF8, Encoding.ASCII, encodedBytes);
            System.Text.Encoding ascii = System.Text.Encoding.ASCII;

            return ascii.GetString(convertedBytes);
        }

        #endregion

        #region "Get 12 Hour Time Format From Date"
        /// <summary>
        /// This funciton 'll extract the time(in 12 hours Format) from date and return that
        /// </summary>
        /// <param name="aDate">date with time in 12 hours format</param>
        /// <returns>time in 12 hours format</returns>
        /// <remarks></remarks>
        public static System.DateTime get12HourTimeFormatFromDate(System.DateTime aDate)
        {
            return Convert.ToDateTime(aDate.ToString("HH") + ":" + aDate.ToString("mm"));
        }
        #endregion

        #region "Get 24 Hour Time Format From Date"
        /// <summary>
        /// This funciton 'll extract the time(in 24 hours Format) from date and return that
        /// </summary>
        /// <param name="aDate">date with time in 24 hours format</param>
        /// <returns>time in 12 hours format</returns>
        /// <remarks></remarks>
        public static object get24HourTimeFormatFromDate(System.DateTime aDate)
        {
            return aDate.ToString("HH:mm");
        }
        #endregion

        #region "Get Date with weekday format"
        /// <summary>
        /// Get Date with weekday format e.g "Tuesday 13th December 2013"
        /// </summary>
        /// <param name="aDate">date time</param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static object getDateWithWeekdayFormat(DateTime aDate)
        {
            string startDay = aDate.DayOfWeek.ToString();
            string startDate = GeneralHelper.getOrdinal(Convert.ToInt32(aDate.ToString("dd")));
            string startMonth = aDate.ToString("MMMMMMMMMMMMM");
            string startYear = aDate.ToString("yyyy");
            return startDay + " " + startDate + " " + startMonth + " " + startYear;
        }
        #endregion

        #region "Get Ordinal"
        /// <summary>
        /// Get Ordinal
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static string getOrdinal(int number)
        {
            string suffix = String.Empty;

            int ones = number % 10;
            int tens = Convert.ToInt32(Math.Floor(number / 10m)) % 10;

            if (tens == 1)
            {
                suffix = "th";
            }
            else
            {
                switch (ones)
                {
                    case 1:
                        suffix = "st";
                        break; // TODO: might not be correct. Was : Exit Select

                    case 2:
                        suffix = "nd";
                        break; // TODO: might not be correct. Was : Exit Select
                    case 3:
                        suffix = "rd";
                        break; // TODO: might not be correct. Was : Exit Select
                    default:

                        suffix = "th";
                        break; // TODO: might not be correct. Was : Exit Select
                }
            }
            return String.Format("{0}{1}", number, suffix);
        }

        #endregion

        #region "Get Hours From Time"
        /// <summary>
        /// This funciton 'll extract the hours from time and return that
        /// </summary>
        /// <param name="aTime">time </param>
        /// <returns>number as double</returns>
        /// <remarks></remarks>
        public static double getHoursFromTime(DateTime aTime)
        {
            return Convert.ToDouble(aTime.ToString("HH")) + (Convert.ToDouble(aTime.ToString("mm")) / 60);
        }

        #endregion

        #region "Combine Date and Time"
        /// <summary>
        /// this function 'll append the time with date and returns date and time
        /// </summary>
        /// <param name="aDate"></param>
        /// <param name="aTime"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static DateTime combineDateAndTime(DateTime aDate, string aTime)
        {
            return Convert.ToDateTime(aDate.ToLongDateString() + " " + aTime);
        }
        #endregion

        #region " Unix time stamp is seconds past epoch"
        public static DateTime unixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix time stamp is seconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp);
            return dtDateTime;
        }
        #endregion

        #region "get Hours And Minute String"
        /// <summary>
        /// This function 'll convert the date into following format : (HH:mm)
        /// </summary>
        /// <param name="aDateTime"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static string getHrsAndMinString(DateTime aDateTime)
        {
            return aDateTime.ToString("HH:mm");
        }
        #endregion

        #region "convert Date Time In to Date Time String"
        /// <summary>
        /// This function 'll return the date in the following format in string (HH:mm dddd d MMMM yyyy)
        /// </summary>
        /// <param name="aDate"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static string convertDateToCustomString(System.DateTime aDate)
        {
            return (aDate.ToString("HH:mm dddd d MMMM yyyy", CultureInfo.CreateSpecificCulture("en-US")));
        }
        #endregion

        #region "get ME Appointment Creation Limit For Single Request"
        /// <summary>
        /// This function 'll return the ME appointment creation limit for single request
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public static int getMeAppointmentCreationLimitForSingleRequest()
        {
            return 20;
        }

        #endregion

        #region "append Day Label"
        /// <summary>
        /// This function 'll append day or days with number
        /// </summary>
        /// <param name="aNumber"></param>
        /// <remarks></remarks>
        public static string appendDayLabel(double aNumber)
        {
            if (aNumber > 1)
            {
                return aNumber.ToString() + " days";
            }
            else
            {
                return aNumber.ToString() + " day";
            }

        }
        #endregion

        #region "append Hour Label"
        /// <summary>
        /// This function 'll append day or days with number
        /// </summary>
        /// <param name="aNumber"></param>
        /// <remarks></remarks>
        public static string appendHourLabel(double aNumber)
        {
            if (aNumber > 1)
            {
                return aNumber.ToString() + " hours";
            }
            else
            {
                return aNumber.ToString() + " hour";
            }

        }
        #endregion

        #region "Is Email"
        public static bool isEmail(string value)
        {
            Regex rgx = new Regex(RegularExpConstants.emailExp);
            if (rgx.IsMatch(value))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region "Get page url"
        /// <summary>
        /// Get page url 
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public static string getPageUrl(string page)
        {

            Dictionary<string, string> pageDict = new Dictionary<string, string>();
            pageDict.Add(PathConstants.DevelopmentList, PathConstants.DevelopmentListPath);
            pageDict.Add(PathConstants.BlockListQr, PathConstants.BlockListPath);
            pageDict.Add(PathConstants.Properties, PathConstants.PropertiesPath);
            pageDict.Add(PathConstants.SchemeList, PathConstants.SchemeListPath);
            pageDict.Add(PathConstants.PropertyTerrier, PathConstants.PropertyTerrierPath);
            pageDict.Add(PathConstants.RentAnalysis, PathConstants.RentAnalysisPath);
            pageDict.Add(PathConstants.StockAnalysis, PathConstants.StockAnalysisPath);
            pageDict.Add(PathConstants.VoidAnalysis, PathConstants.VoidAnalysisPath);
            pageDict.Add(PathConstants.SchemeDashboard, PathConstants.SchemeDashboardPath);
            pageDict.Add(PathConstants.SchemeRecord, PathConstants.SchemeRecordPath);
            pageDict.Add(PathConstants.MeServicing, PathConstants.MAndeServicingPath);
            pageDict.Add(PathConstants.AccessDenied, PathConstants.AccessDeniedPath);
            pageDict.Add(PathConstants.VoidMenu, PathConstants.VoidInspectionPath);
            pageDict.Add(PathConstants.VoidWorks, PathConstants.VoidWorksPath);
            pageDict.Add(PathConstants.NotificationsMenu, PathConstants.BritishGasNotificationPath);
            pageDict.Add(PathConstants.AvailablePropertiesMenu, PathConstants.AvailablePropertiesPath);
            pageDict.Add(PathConstants.ServiceCharge, PathConstants.ServiceChargeReportPath);
            pageDict.Add(PathConstants.ProvisioningReport, PathConstants.ProvisioningReportPath);

            pageDict.Add(PathConstants.RentAccountSummary, PathConstants.RentAccountSummaryPath);
            //pageDict.Add(PathConstants.UnassignedCyclicalReport, PathConstants.ProvisioningReportPath);

            string url = string.Empty;
            if (pageDict.ContainsKey(page))
            {
                url = pageDict[page];
            }

            return url;
        }


        #endregion

        #region "Get Page Menu"
        /// <summary>
        /// "Get Page Menu
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public static string getPageMenu(string page)
        {

            Dictionary<string, string> pageDict = new Dictionary<string, string>();
            pageDict.Add(PathConstants.BlockDashboardPage, PathConstants.AdminMenu);
            pageDict.Add(PathConstants.BlockListPage, PathConstants.AdminMenu);
            pageDict.Add(PathConstants.BlockMainPage, PathConstants.AdminMenu);
            pageDict.Add(PathConstants.AddNewDevelopmentPage, PathConstants.AdminMenu);
            pageDict.Add(PathConstants.DevelopmentListPage, PathConstants.AdminMenu);
            pageDict.Add(PathConstants.SchemeDashBoardPage, PathConstants.AdminMenu);
            pageDict.Add(PathConstants.SchemeRecordPage, PathConstants.PropertyModuleMenu);
            pageDict.Add(PathConstants.SchemeListPage, PathConstants.AdminMenu);
            pageDict.Add(PathConstants.ReportAreaPage, PathConstants.ReportsMenu);
            pageDict.Add(PathConstants.ComplianceDocumentsPage, PathConstants.ReportsMenu);
            pageDict.Add(PathConstants.MaintenanceDashboardPage, PathConstants.MaintenanceMenu);
            pageDict.Add(PathConstants.ArrangedAppointmentSummaryPage, PathConstants.MaintenanceMenu);
            pageDict.Add(PathConstants.MEServicingPage, PathConstants.MaintenanceMenu);
            pageDict.Add(PathConstants.ScheduleMEServicingPage, PathConstants.MaintenanceMenu);
            pageDict.Add(PathConstants.ToBeArrangedAppointmentSummaryPage, PathConstants.MaintenanceMenu);
            pageDict.Add(PathConstants.CyclicalServices, PathConstants.MaintenanceMenu);
            pageDict.Add(PathConstants.UnassignedCyclicalReport, PathConstants.MaintenanceMenu);
            pageDict.Add(PathConstants.CancelledOrderReport, PathConstants.MaintenanceMenu);
            pageDict.Add(PathConstants.VoidDashboard, PathConstants.VoidMenu);
            pageDict.Add(PathConstants.VoidInspections, PathConstants.VoidMenu);
            pageDict.Add(PathConstants.PostVoidInspections, PathConstants.VoidMenu);
            pageDict.Add(PathConstants.VoidAppointmentsPage, PathConstants.VoidMenu);
            pageDict.Add(PathConstants.PaintPacksList, PathConstants.VoidMenu);
            pageDict.Add(PathConstants.CP12StatusReport, PathConstants.VoidMenu);
            pageDict.Add(PathConstants.GasElectricChecksPage, PathConstants.VoidMenu);
            pageDict.Add(PathConstants.VoidReportArea, PathConstants.VoidMenu);
            pageDict.Add(PathConstants.BritishGasNotificationList, PathConstants.VoidMenu);
            pageDict.Add(PathConstants.ProvisioningReportPage, PathConstants.ReportsMenu);

            pageDict.Add(PathConstants.VoidWorksRequiredPage, PathConstants.VoidMenu);
            pageDict.Add(PathConstants.VoidWorksReArrangedPage, PathConstants.VoidMenu);
            pageDict.Add(PathConstants.SchedulingRequiredWorksPage, PathConstants.VoidMenu);
            pageDict.Add(PathConstants.RearrangeWorksRequiredPage, PathConstants.VoidMenu);
            pageDict.Add(PathConstants.JSVJobSheetSummaryPage, PathConstants.VoidMenu);
            pageDict.Add(PathConstants.PaintPacksDetailPage, PathConstants.VoidMenu);
            pageDict.Add(PathConstants.ServiceChargePage, PathConstants.ReportsMenu);

            pageDict.Add(PathConstants.UploadDocument, PathConstants.AdminMenu);

            string menuName = string.Empty;
            if (pageDict.ContainsKey(page))
            {
                menuName = pageDict[page];
            }

            return menuName;

        }

        #endregion

        #region "pushNotification"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="documentTitle"></param>
        /// <param name="expirydate"></param>
        /// <param name="operatorId"></param>
        /// <returns></returns>
        public static bool pushNotificationPlanned(string documentTitle, DateTime expirydate, int operatorId)
        {
            try
            {
                string appointmentTypeDesc = String.Empty;
                string appointmentMessage = String.Empty;

                appointmentMessage = "The document " + documentTitle + " will be expired on " + expirydate;
                // Server.UrlEncode(appointmentMessage)
                string URL = string.Format("{0}?appointmentMessage={1}&operatorId={2}", GeneralHelper.getPushNotificationAddress(HttpContext.Current.Request.Url.AbsoluteUri), appointmentMessage, operatorId);
                WebRequest request = WebRequest.Create(URL);
                request.Credentials = CredentialCache.DefaultCredentials;
                WebResponse response = request.GetResponse();
                Stream dataStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(dataStream);
                string responseFromServer = reader.ReadToEnd();
                reader.Close();
                response.Close();
                if (responseFromServer == "true")
                {
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion

        #region "get PushNotification Address"
        /// <summary>
        /// 
        /// </summary>
        /// <param name="absoluteUri"></param>
        /// <returns></returns>
        public static string getPushNotificationAddress(string absoluteUri)
        {
            string servername = absoluteUri.Substring((absoluteUri.IndexOf("//") + 2), absoluteUri.Substring((absoluteUri.IndexOf("//") + 2), absoluteUri.Substring((absoluteUri.IndexOf("//") + 2)).IndexOf("/")).Length);
            return ("https://"
                        + (servername + "/StockConditionOfflineAPI/push/NotificationApplianceAndPlanned"));
        }
        #endregion

        #region Get Singular or Plural Post Fix for Numeric Values

        /// <summary>
        /// Add a singular or plural post fix to the end of a numeric value
        /// </summary>
        /// <param name="numericValue"></param>
        /// <param name="singularPostfix">singular postfix for values 1 or less.</param>
        /// <param name="pluralPostFix">plural postfix for valus more than 1.
        /// If it is not provides, an s is added at the end of singularpostfix</param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static string getSingularOrPluralPostFixForNumericValues(double numericValue, string singularPostfix, string pluralPostFix = null, string nullString = "N/A")
        {
            string returnString = nullString;
            if (!ReferenceEquals(numericValue, null))
            {
                if (numericValue <= 1)
                {
                    returnString = string.Format("{0} {1}", numericValue, singularPostfix);
                }
                else
                {
                    returnString = string.Format("{0} {1}", numericValue, pluralPostFix != null ? pluralPostFix : singularPostfix + "s");
                }
            }
            return returnString;
        }

        #endregion

        #region Get Yes No String From Inetger value (For UI Display)

        public static string getYesNoStringFromValue(object inputValue)
        {
            if (!(Convert.IsDBNull(inputValue) || ReferenceEquals(inputValue, null)))
            {
                if(inputValue != null)
                    return Convert.ToBoolean(inputValue) ? "Yes" : "No";
            }
            return "No";
        }

        #endregion

        #region Change server controls visibility

        public static void changeServerControlsVisibility(bool visible, params WebControl[] serverControls)
        {
            foreach (var control in serverControls)
            {
                control.Visible = visible;
            }
        }

        #endregion

        #region get Image Upload Path
        public static string getImageUploadPath()
        {
            return ConfigurationManager.AppSettings["ImageUploadPath"];
        }
        #endregion

        #region Get Formated Date or Sting N/A if null

        public static string getFormatedDateDefaultString(object inputDate, string formatString = "dd/MM/yyyy", string defaultOutput = "N/A")
        {
            var outputString = defaultOutput;
            DateTime parsedInputDate = default(DateTime);
            if (!(Convert.IsDBNull(inputDate) || ReferenceEquals(inputDate, null)) && DateTime.TryParse(System.Convert.ToString(inputDate), out parsedInputDate))
            {
                outputString = string.Format("{0:" + formatString + "}", parsedInputDate);
            }
            return outputString;
        }

        #endregion

        #region Set Parse Nullable Boolean Value from String

        public static void setParseNullableBooleanValueFromStringOrDefaultToNull(ref Nullable<bool> nullableBoolean, string valueToParse)
        {
            bool tempBoolean = false;
            if (bool.TryParse(valueToParse, out tempBoolean))
            {
                nullableBoolean = tempBoolean;
            }
        }

        #endregion

        #region Populate Drop Down with duration hours

        public static void PopulateDropDownWithDurationHours(DropDownList dropDownToPopulate, decimal startDuration = 1, decimal endDuraton = 23.5M, decimal increment = 1, string singularPostFix = "hour", string pluralPostFix = null)
        {
            dropDownToPopulate.Items.Clear();

            for (decimal duration = startDuration; duration <= endDuraton; duration += increment)
            {
                dropDownToPopulate.Items.Add(new ListItem(getSingularOrPluralPostFixForNumericValues(Convert.ToDouble(duration), singularPostFix, pluralPostFix), duration.ToString()));
            }
        }

        #endregion

        #region "Populate iCal File"
        /// <summary>
        /// 
        /// </summary>
        /// <remarks></remarks>

        public static void populateiCalFile(ICalFileBO objICalFileBo)
        {

            DateTime startTime = DateTime.Parse(objICalFileBo.AppointmentStartDate.ToString("dd/MM/yyyy") + " " + objICalFileBo.StartTime);
            DateTime endTime = DateTime.Parse(objICalFileBo.AppointmentEndDate.ToString("dd/MM/yyyy") + " " + objICalFileBo.EndTime);

            StringBuilder iCalString = new StringBuilder();
            iCalString.AppendLine("BEGIN:VCALENDAR");
            iCalString.AppendLine("PRODID:-//Apple Inc.//iCal 5.0.2//EN");
            iCalString.AppendLine("VERSION:2.0");
            iCalString.AppendLine("CALSCALE:GREGORIAN");
            iCalString.AppendLine("METHOD:PUBLISH");
            iCalString.AppendLine("X-WR-CALNAME:BHGcalendar");
            iCalString.AppendLine("X-WR-TIMEZONE:" + TimeZone.CurrentTimeZone.ToString());
            iCalString.AppendLine("X-WR-CALDESC:BHG Calendar");
            iCalString.AppendLine("BEGIN:VEVENT");
            iCalString.AppendLine("ATTENDEEROLE=REQ-PARTICIPANTCN=" + objICalFileBo.OperativeName + ":MAILTO:" + objICalFileBo.OperativeEmail);
            iCalString.AppendLine("DTSTART:" + startTime.ToUniversalTime().ToString("yyyyMMddTHHmmssZ"));
            iCalString.AppendLine("DTEND:" + endTime.ToUniversalTime().ToString("yyyyMMddTHHmmssZ"));
            iCalString.AppendLine("DTSTAMP:20130127T040705Z");
            iCalString.AppendLine("UID:" + Convert.ToString(DateTime.Now.Ticks));
            iCalString.AppendLine("CREATED:" + DateTime.Now.ToUniversalTime().ToString("yyyyMMddTHHmmssZ"));
            iCalString.AppendLine("SUMMARY:" + objICalFileBo.Summary);
            iCalString.AppendLine("DESCRIPTION:" + objICalFileBo.Summary + "has been arranged with ref " + objICalFileBo.JS + ". ");
            iCalString.AppendLine("LAST-MODIFIED:" + DateTime.Now.ToUniversalTime().ToString("yyyyMMddTHHmmssZ"));
            iCalString.AppendLine("SEQUENCE:0");
            iCalString.AppendLine("STATUS:CONFIRMED");
            iCalString.AppendLine("TRANSP:TRANSPARENT");
            iCalString.AppendLine("END:VEVENT");
            iCalString.AppendLine("END:VCALENDAR");

            TextWriter iCalTextWriter = new StreamWriter(HttpContext.Current.Server.MapPath(PathConstants.ICalFilePath));
            iCalTextWriter.WriteLine(iCalString);
            iCalTextWriter.Close();
        }

        #endregion

        #region "Skip Weekend"
        /// <summary>
        /// This function 'll skip the weekend (saturday or sunday)
        /// </summary>
        /// <param name="aptStartDate"></param>
        /// <param name="aptStartHour"></param>
        /// <remarks></remarks>
        public static bool isWeekend(DateTime aptStartDate)
        {
            bool isValid = true;
            if (aptStartDate.DayOfWeek == DayOfWeek.Saturday || aptStartDate.DayOfWeek == DayOfWeek.Sunday)
            {
                isValid = false;
            }
            return isValid;
        }
        #endregion

        #region Populate Hours DropDownList
        public static void populateHoursDropDowns(ref DropDownList ddl)
        {
            ddl.Items.Clear();

            ListItem newListItem = default(ListItem);
            foreach (string item in ApplicationConstants.appointmentTimeHours)
            {
                newListItem = new ListItem(item, item);
                ddl.Items.Add(newListItem);

            }
            newListItem = new ListItem("Please select", "-1");
            ddl.Items.Insert(0, newListItem);


        }
        #endregion

        #region Populate Minutes DropDownList
        public static void populateMinDropDowns(ref DropDownList ddl)
        {
            ddl.Items.Clear();

            ListItem newListItemMin = default(ListItem);
            foreach (string item in ApplicationConstants.appointmentTimeMinutes)
            {
                newListItemMin = new ListItem(item, item);
                ddl.Items.Add(newListItemMin);

            }



        }
        #endregion
    }
}
