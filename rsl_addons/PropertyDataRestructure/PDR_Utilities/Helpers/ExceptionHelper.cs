﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.Drawing;

namespace PDR_Utilities.Helpers
{
    public class ExceptionHelper
    {
        public static bool IsExceptionLogged = false;
        public static bool IsError = false;
        public static string ExcpetionMessage;


        public static void setMessage(ref Label lblMessage, ref Panel pnlMessage, string str, bool isError)
        {
            if ((isError == true))
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = str;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }
            pnlMessage.Visible = true;
        }

        public static void resetMessage(ref Label lblMessage, ref Panel pnlMessage)
        {
            lblMessage.Text = String.Empty;
            pnlMessage.Visible = false;
            IsError = false;
        }

    }
}
