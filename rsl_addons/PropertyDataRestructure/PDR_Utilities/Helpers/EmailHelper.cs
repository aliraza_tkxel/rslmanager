﻿// -----------------------------------------------------------------------
// <copyright file="EmailHelper.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace PDR_Utilities.Helpers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Net.Mail;

    /// <summary>
    /// TODO: Update summary.
    /// </summary>
    public class EmailHelper
    {


        public static void sendHtmlFormattedEmail(string recepientName, string recepientEmail, string subject, string body)
        {
            MailMessage mailMessage = new MailMessage();
            mailMessage.Subject = subject;
            mailMessage.Body = body;
            mailMessage.IsBodyHtml = true;
            mailMessage.To.Add(new MailAddress(recepientEmail, recepientName));

            //The SmtpClient gets configuration from Web.Config
            SmtpClient smtp = new SmtpClient();

            smtp.Send(mailMessage);

        }


        public static void sendHtmlFormattedEmailWithAttachment(string recepientName, string recepientEmail, string subject, string body, string filePath)
        {
            MailMessage mailMessage = new MailMessage();
            mailMessage.Subject = subject;
            mailMessage.Body = body;
            mailMessage.IsBodyHtml = true;
            mailMessage.To.Add(new MailAddress(recepientEmail, recepientName));


            Attachment iCalAttachement = new Attachment(filePath);
            mailMessage.Attachments.Add(iCalAttachement);
            //The SmtpClient gets configuration from Web.Config
            SmtpClient smtp = new SmtpClient();

            smtp.Send(mailMessage);
            iCalAttachement.Dispose();
        }

        #region "sendEmail - Simple send email function using mailMessage"

        /// <summary>
        /// A simple send Email Function the mail message settings are handled on the caller function side.
        /// </summary>
        /// <param name="mailMessage"></param>
        /// <remarks></remarks>

        public static void sendEmail(ref MailMessage mailMessage)
        {
            //The SmtpClient gets configuration from Web.Config
            SmtpClient smtp = new SmtpClient();

            smtp.Send(mailMessage);
        }

        #endregion

    }

}
