﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using PDR_Utilities.Constants;
using System.Globalization;

namespace PDR_Utilities.Helpers
{
    public class ValidationHelper
    {
        public static bool emptyString(string value)
        {
            if (value.Equals(string.Empty))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool lengthString(string value, int length)
        {
            if (value.Length > length)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool isNumber(string value)
        {
            Regex rgx = new Regex(RegularExpConstants.numbersExp);
            if (!rgx.IsMatch(value))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public static bool isAmountTwoDec(string value)
        {
            Regex rgx = new Regex(RegularExpConstants.twoDecAmount);
            if (!rgx.IsMatch(value))
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        
        public static bool isEmail(string value)
        {
            Regex rgx = new Regex(RegularExpConstants.emailExp);
            if (rgx.IsMatch(value))
            {
                return true;
            }
            else
            {
                return false;
            }
        }        

        public static bool isMobile(string value)
        {
            Regex rgx = new Regex(RegularExpConstants.mobileExp);
            if (rgx.IsMatch(value))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #region "validate date time"
        public static bool validateDateFormate(string dateString)
        {
            System.DateTime dateTime = DateTime.Now  ;
            bool valid = DateTime.TryParseExact(dateString, "dd/MM/yyyy", CultureInfo.CurrentCulture, DateTimeStyles.None,out dateTime);
            return valid;

        }


        #endregion

    }
}
