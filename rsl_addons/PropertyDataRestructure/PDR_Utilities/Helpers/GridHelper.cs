﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using PDR_BusinessObject.PageSort;
using PDR_Utilities.Constants;

namespace PDR_Utilities.Helpers
{
    public class GridHelper
    {
        /// <summary>
        /// This function would sort the grid
        /// </summary>
        /// <param name="grd">Grid on which you want to apply sorting</param>
        /// <param name="e">Grid View Sort Event Argument</param>
        /// <param name="objPageSortBo">Page sort bo that with new settings that should be saved in view state</param>
        /// <param name="pageSortViewState">Page sort bo that is already saved in view state</param>
        public static PageSortBO sortGrid(GridView grd, GridViewSortEventArgs e, PageSortBO objPageSortBo, PageSortBO pageSortViewState)
        {
            objPageSortBo = pageSortViewState;
            objPageSortBo.SortExpression = e.SortExpression;
            objPageSortBo.PageNumber = 1;
            grd.PageIndex = 0;
            objPageSortBo.setSortDirection();
            pageSortViewState = objPageSortBo;
            return pageSortViewState;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <param name="objPageSortBo">Page sort bo that with new settings that should be saved in view state</param>
        /// <param name="txtPageNumber"></param>
        /// <param name="pageSortViewState">Page sort bo that is already saved in view state</param>
        /// <returns></returns>
        public static PageSortBO changeGridPageNumber(object sender, EventArgs e, PageSortBO objPageSortBo, TextBox txtPageNumber, PageSortBO pageSortViewState)
        {

            Button btnGo = new Button();
            btnGo = (Button)sender;

            objPageSortBo = pageSortViewState;
            int pageNumber = 1;
            pageNumber = Convert.ToInt32(txtPageNumber.Text);
            txtPageNumber.Text = String.Empty;
            objPageSortBo = pageSortViewState;
            if (((pageNumber >= 1)
                        && (pageNumber <= objPageSortBo.TotalPages)))
            {
                objPageSortBo = pageSortViewState;
                objPageSortBo.PageNumber = pageNumber;
                pageSortViewState = objPageSortBo;
                return pageSortViewState;
            }
            else
            {
                throw new Exception("");
            }

        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="objPageSortBo">Page sort bo that with new settings that should be saved in view state</param>
        /// <param name="pageSortViewState">Page sort bo that is already saved in view state</param>
        public static PageSortBO processGridPager(object sender, PageSortBO objPageSortBo, PageSortBO pageSortViewState)
        {
            LinkButton pagebuttton = new LinkButton();
            pagebuttton = (LinkButton)sender;
            objPageSortBo = pageSortViewState;
            if (((pagebuttton.CommandName == "Page")
                        && (pagebuttton.CommandArgument == "First")))
            {
                objPageSortBo.PageNumber = 1;
            }
            else if (((pagebuttton.CommandName == "Page")
                        && (pagebuttton.CommandArgument == "Last")))
            {
                objPageSortBo.PageNumber = objPageSortBo.TotalPages;
            }
            else if (((pagebuttton.CommandName == "Page")
                        && (pagebuttton.CommandArgument == "Next")))
            {
                objPageSortBo.PageNumber++;
            }
            else if (((pagebuttton.CommandName == "Page")
                        && (pagebuttton.CommandArgument == "Prev")))
            {
                objPageSortBo.PageNumber--;
            }
            pageSortViewState = objPageSortBo;
            return pageSortViewState;
        }

        #region "Set Grid View Pager"
        /// <summary>
        /// This function will help to pager increment
        /// </summary>
        /// <param name="pnlPagination"></param>
        /// <param name="objPageSortBO"></param>
        public static void setGridViewPager(ref Panel pnlPagination, PageSortBO objPageSortBO)
        {
            pnlPagination.Visible = false;

            if (objPageSortBO.TotalRecords >= 1)
            {
                pnlPagination.Visible = true;

                int pageNumber = objPageSortBO.PageNumber;
                int pageSize = objPageSortBO.PageSize;
                int TotalRecords = objPageSortBO.TotalRecords;
                int totalPages = objPageSortBO.TotalPages;

                LinkButton lnkbtnPagerFirst = (LinkButton)pnlPagination.FindControl("lnkbtnPagerFirst");
                LinkButton lnkbtnPagerPrev = (LinkButton)pnlPagination.FindControl("lnkbtnPagerPrev");
                LinkButton lnkbtnPagerNext = (LinkButton)pnlPagination.FindControl("lnkbtnPagerNext");
                LinkButton lnkbtnPagerLast = (LinkButton)pnlPagination.FindControl("lnkbtnPagerLast");

                if (lnkbtnPagerFirst != null)
                {
                    lnkbtnPagerFirst.Enabled = (pageNumber != 1);
                    lnkbtnPagerFirst.Font.Underline = lnkbtnPagerFirst.Enabled;
                }
                if (lnkbtnPagerPrev != null)
                {
                    lnkbtnPagerPrev.Enabled = (pageNumber != 1);
                    lnkbtnPagerPrev.Font.Underline = lnkbtnPagerPrev.Enabled;
                }
                if (lnkbtnPagerNext != null)
                {
                    lnkbtnPagerNext.Enabled = (pageNumber != totalPages);
                    lnkbtnPagerNext.Font.Underline = lnkbtnPagerNext.Enabled;
                }
                if (lnkbtnPagerLast != null)
                {
                    lnkbtnPagerLast.Enabled = (pageNumber != totalPages);
                    lnkbtnPagerLast.Font.Underline = lnkbtnPagerLast.Enabled;
                }
                Label lblPagerCurrentPage = (Label)pnlPagination.FindControl("lblPagerCurrentPage");
                if (lblPagerCurrentPage != null)
                {
                    lblPagerCurrentPage.Text = pageNumber.ToString();
                }

                Label lblPagerTotalPages = (Label)pnlPagination.FindControl("lblPagerTotalPages");
                if (lblPagerTotalPages != null)
                {  
                    lblPagerTotalPages.Text = totalPages.ToString();
                }
                Label lblPagerRecordStart = (Label)pnlPagination.FindControl("lblPagerRecordStart");
                int pagerRecordStart = ((pageNumber - 1) * pageSize) + 1;
                if (lblPagerRecordStart != null)
                {
                    lblPagerRecordStart.Text = pagerRecordStart.ToString();
                }

                Label lblPagerRecordEnd = (Label)pnlPagination.FindControl("lblPagerRecordEnd");
                int pagerRecordEnd = pagerRecordStart + pageSize - 1;
                if (lblPagerRecordEnd != null)
                {
                    lblPagerRecordEnd.Text = (pagerRecordEnd < TotalRecords ? pagerRecordEnd : TotalRecords).ToString();
                }

                Label lblPagerRecordTotal = (Label)pnlPagination.FindControl("lblPagerRecordTotal");
                if (lblPagerRecordTotal != null)
                {
                    lblPagerRecordTotal.Text = TotalRecords.ToString();
                }

                RangeValidator rangevalidatorPageNumber = (RangeValidator)pnlPagination.FindControl("rangevalidatorPageNumber");
                if (rangevalidatorPageNumber != null)
                {
                    rangevalidatorPageNumber.ErrorMessage = "Enter a page between 1 and " + totalPages.ToString();
                    rangevalidatorPageNumber.MaximumValue = totalPages.ToString();
                }

            }
        }

        #endregion
    }
}
