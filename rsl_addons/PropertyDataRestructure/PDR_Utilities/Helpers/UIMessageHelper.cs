﻿using System.Web.UI.WebControls;
using System.Drawing;

namespace AS_Utilities
{
    public class UIMessageHelper
    {
        public bool IsExceptionLogged = false;
        public bool IsError = false;
        public string Message;
        public void setMessage(ref Label lblMessage, ref Panel pnlMessage, string message, bool isError)
        {
            this.Message = message;
            if (isError == true)
            {
                lblMessage.Text = message;
                lblMessage.ForeColor = Color.Red;
                lblMessage.Font.Bold = true;
            }
            else
            {
                lblMessage.Text = message;
                lblMessage.ForeColor = Color.Green;
                lblMessage.Font.Bold = true;
            }

            pnlMessage.Visible = true;
        }

        public void resetMessage(ref Label lblMessage, ref Panel pnlMessage)
        {
            lblMessage.Text = string.Empty;
            pnlMessage.Visible = false;
            this.IsError = false;
            this.Message = string.Empty;
        }
    }
}