﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using System.IO;
using System.Web.UI;
using System.Web;
using System.Web.UI.HtmlControls;
using System.Data;
using ClosedXML.Excel;

namespace PDR_Utilities.Helpers
{
    public class ExportDownloadHelper
    {
        public static  void exportGridToExcel(GridView grd, string fileName, string style = "")
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.Buffer = true;
            HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + fileName + ".xls");
            HttpContext.Current.Response.Charset = "";
            HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
            HttpContext.Current.Response.ContentEncoding = System.Text.Encoding.UTF8;
            HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            StringWriter sw = new StringWriter();
            HtmlTextWriter hw = new HtmlTextWriter(sw);
            HtmlForm htmfrm = new HtmlForm();           
            grd.Parent.Controls.Add(htmfrm);
            htmfrm.Attributes["runat"] = "server";
            htmfrm.Controls.Add(grd);
            htmfrm.RenderControl(hw);
            //grd.RenderControl(hw);
            HttpContext.Current.Response.Write(style);
            HttpContext.Current.Response.Output.Write(sw.ToString());
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.End();

        }

        public static void exportGridToExcel( string fileName,DataTable dt)
        {
          
            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dt);

                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.Buffer = true;
                HttpContext.Current.Response.Charset = "";
                HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + fileName + ".xlsx");
                using (MemoryStream MyMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(HttpContext.Current.Response.OutputStream);
                    HttpContext.Current.Response.Flush();
                    HttpContext.Current.Response.End();
                }
            }
        }

        public static void exportCP12StatusReportToExcel(string fileName, DataTable dt)
        {

            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(dt);

                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.Buffer = true;
                HttpContext.Current.Response.Charset = "";
                HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + fileName + ".xlsx");
                using (MemoryStream MyMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(HttpContext.Current.Response.OutputStream);
                    HttpContext.Current.Response.Flush();
                    HttpContext.Current.Response.End(); 
                }
            }
        }

        public static void exportGridToExcelTerrierReport(string fileName, DataTable dt)
        {
            XLWorkbook wb = new XLWorkbook();
                
                var ws = wb.Worksheets.Add(dt);
                
                ws.Row(1).InsertRowsAbove(1); //Empty Row
                
                ws.Row(1).Cell(1).Value = "Asset Register as at " + DateTime.Now.Day + "." + DateTime.Now.Month + "." + DateTime.Now.Year;
                doBold(ref wb, 1, 1);
                
                ws.Row(1).Cell(3).Value = "Totals";
                doBold(ref wb, 1, 3);

                ws.Row(1).Cell(4).Value = " £" + ExportDownloadHelper.computeColumnSum(ref dt, "RENT") + " ";
                doBold(ref wb, 1, 4);

                ws.Row(1).Cell(5).Value = " £" + ExportDownloadHelper.computeColumnSum(ref dt, "Service Charge") + " ";
                doBold(ref wb, 1, 5);

                ws.Row(1).Cell(6).Value = " £" + ExportDownloadHelper.computeColumnSum(ref dt, "EUV-SH") + " ";
                doBold(ref wb, 1, 6);

                ws.Row(1).Cell(7).Value = " £" + ExportDownloadHelper.computeColumnSum(ref dt, "MV-T") + " ";
                doBold(ref wb, 1, 7);


                //Add Headers
                ws.Row(1).InsertRowsAbove(1);

                ws.Row(1).Cell(1).Value = "Broadland Housing Group";
                doBold(ref wb, 1, 1);

                ws.Row(1).Cell(4).Value = "Monthly Rent";
                doBold(ref wb, 1, 4);
                setWhiteFontColor(ref wb, 1, 4);

                ws.Row(1).Cell(5).Value = "Monthly Service Charge";
                setWhiteFontColor(ref wb, 1, 5);
                doBold(ref wb, 1, 5);

                ws.Row(1).Cell(6).Value = "EUV-SH";
                setWhiteFontColor(ref wb, 1, 6);
                doBold(ref wb, 1, 6);

                ws.Row(1).Cell(7).Value = "MV-T";
                setWhiteFontColor(ref wb, 1, 7);
                doBold(ref wb, 1, 7);

                //Set Background Colors
                ws.Range("C1:G1").Style.Fill.BackgroundColor = XLColor.OrangeRed;
                ws.Range("A3:AB3").Style.Fill.BackgroundColor = XLColor.White;

                ws.Range("A3:AB3").Style.Font.Bold = true;
                ws.Range("A3:AB3").Style.Font.SetFontColor(XLColor.FromArgb(32, 32, 32));

                //Set Cell Merge & Alignment
                ws.Range("A1:B1").Merge();//Broadland Housing Group
                ws.Range("A2:B2").Merge();//Asset Register as at 12.05.2016

                ws.Column(1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Right;
                ws.Row(1).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                ws.Row(2).Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
               
                // freeze columns and rows
                ws.SheetView.Freeze(3, 2);
                
                //Set columns width
                ws.Column(1).Width = 12.57;//A
                ws.Column(2).Width = 29.29;//B
                ws.Column(3).Width = 11.29;//C
                ws.Column(4).Width = 24.43;//D
                ws.Column(5).Width = 21.14;//E
                ws.Column(6).Width = 18.00;//F
                ws.Column(7).Width = 28.29;//G
                ws.Column(8).Width = 24.43;//H
                ws.Column(9).Width = 16.86;//I
                ws.Column(10).Width = 11.29;//J
                ws.Column(11).Width = 24.00;//K
                ws.Column(12).Width = 14.14;//L
                ws.Column(13).Width = 7.29;//M
                ws.Column(14).Width = 8.29;//N
                ws.Column(15).Width = 13.43;//O
                ws.Column(16).Width = 6.86;//P
                ws.Column(17).Width = 6.29;//Q
                ws.Column(18).Width = 10.43;//R
                ws.Column(19).Width = 6.14;//S
                ws.Column(20).Width = 13.57;//T
                ws.Column(21).Width = 34.00;//U
                ws.Column(22).Width = 15.86;//V
                ws.Column(23).Width = 15.86;//W
                ws.Column(24).Width = 26.86;//X
                ws.Column(25).Width = 15.86;//Y
                //removeFilters(ref wb);
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.Buffer = true;
                HttpContext.Current.Response.Charset = "";
                HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + fileName + ".xlsx");
                using (MemoryStream MyMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(HttpContext.Current.Response.OutputStream);
                    HttpContext.Current.Response.Flush();
                    HttpContext.Current.Response.End();
                }
            
        }

        public static string computeColumnSum(ref DataTable dt, string columnName)
        {
            double sum = 0;
            foreach (DataRow row in dt.Rows)
            {
                if (row[columnName].ToString() != null && row[columnName].ToString() != "" && row[columnName].ToString() != "NULL")
                {
                    try
                    {
                        sum += double.Parse(row[columnName].ToString().Remove(0, 1).Trim());
                    }
                    catch(Exception e)
                    {
                    }
                }
            }

            return sum.ToString("#,##0.00");
        }

        public static void setWhiteFontColor(ref XLWorkbook wb, int row, int cell)
        {
            wb.Worksheets.FirstOrDefault().Row(row).Cell(cell).Style.Font.FontColor = XLColor.White;
        }

        public static void doBold(ref XLWorkbook wb, int row, int cell)
        {
            wb.Worksheets.FirstOrDefault().Row(row).Cell(cell).Style.Font.Bold = true;
        }

        public static void removeFilters(ref XLWorkbook wb)
        {
            try
            {
                wb.Worksheets.FirstOrDefault().Tables.FirstOrDefault().ShowAutoFilter = false;
                //wb.Worksheets.FirstOrDefault().AutoFilter.Clear();
            }
            catch(Exception e)
            {
            }
            
        }

        public static void ExportToExcel(IEnumerable<dynamic> data, string sheetName)
        {
            XLWorkbook wb = new XLWorkbook();
            var ws = wb.Worksheets.Add(sheetName);
            ws.Cell(2, 1).InsertTable(data);
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
            HttpContext.Current.Response.AddHeader("content-disposition", String.Format(@"attachment;filename={0}.xls", sheetName.Replace(" ", "_")));

            using (MemoryStream memoryStream = new MemoryStream())
            {
                wb.SaveAs(memoryStream);
                memoryStream.WriteTo(HttpContext.Current.Response.OutputStream);
                memoryStream.Close();
            }

            HttpContext.Current.Response.End();
        }
        public static void exportFileToBrowser(string mimeType, string actualFilepath)
        {
            exportFileToBrowser(mimeType, actualFilepath, Path.GetFileName(actualFilepath));
        }

        public static void exportFileToBrowser(string mimeType, string actualFilepath, string displayedFilename)
        {

            HttpContext.Current.Response.ContentType = mimeType;
            string str = FileHelper.windowsSafeFileName(displayedFilename);
            HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("Attachment; Filename=\"{0}\"", str));
            HttpContext.Current.Response.TransmitFile(actualFilepath);
            HttpContext.Current.Response.End();

        }
    }
}
