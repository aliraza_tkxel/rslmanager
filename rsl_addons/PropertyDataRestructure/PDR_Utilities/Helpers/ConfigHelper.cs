﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using PDR_Utilities.Constants;
using System.Web;

namespace PDR_Utilities.Helpers
{
    public class ConfigHelper
    {
        public static bool IsLoggingEnabled
        {
            get
            {
                bool returnVal = true;
                var configKey = ConfigurationManager.AppSettings[ConfigurationKeys.LogErrorsAndMessages];
                if (configKey != null && bool.TryParse(configKey, out returnVal))
                    return returnVal;

                return returnVal;
            }
        }

        public static string GetDocUploadPath()
        {
            string returnVal = string.Empty ;
            var configKey = ConfigurationManager.AppSettings[ConfigurationKeys.DocumentUploadPath];
            if (configKey != null)
                return configKey;

            return returnVal;
        }
        
        #region "get Development Doc Upload & Download Path"
        public static string GetDevelopmentDocUploadPath()
        {
            string returnVal = string.Empty;
            var configKey = ConfigurationManager.AppSettings[ConfigurationKeys.PdrDocumentUploadPath];
            if (configKey != null)
                return configKey + "Developments/";

            return returnVal;
        }

        public static string GetDevelopmentDocDownloadPath()
        {
            string returnVal = string.Empty;
            var configKey = ConfigurationManager.AppSettings[ConfigurationKeys.PdrDevDocsDownloadPath];
            if (configKey != null)
                return configKey + "Developments/";

            return returnVal;
        }

        #endregion

        #region "get Scheme Doc Upload Path"
        public static string GetSchemeDocUploadPath()
        {
            string returnVal = string.Empty;
            var configKey = ConfigurationManager.AppSettings[ConfigurationKeys.PdrDocumentUploadPath];
            if (configKey != null)
                return configKey + "Schemes/";

            return returnVal;
        }

        public static string GetSchemeDocDownloadPath()
        {
            string returnVal = string.Empty;
            var configKey = ConfigurationManager.AppSettings[ConfigurationKeys.PdrSchemeDocsDownloadPath];
            if (configKey != null)
                return configKey + "Schemes/";

            return returnVal;
        }


        public static string GetPropertyDocUploadPath()
        {
            string returnVal = string.Empty;
            var configKey = ConfigurationManager.AppSettings[ConfigurationKeys.PropertyDocumentUploadPath];
            if (configKey != null)
                return configKey + "";

            return returnVal;
        }

        #endregion

        #region "get Block Doc Upload Path"
        public static string GetBlockDocUploadPath()
        {
            string returnVal = string.Empty;
            var configKey = ConfigurationManager.AppSettings[ConfigurationKeys.PdrDocumentUploadPath];
            if (configKey != null)
                return configKey + "Blocks/";

            return returnVal;
        }
        #endregion

        #region "get Server Doc Upload Path"
        public static string getServerDocUploadPath()
        {
            string servername = HttpContext.Current.Request.Url.Host;
            return "https://" + servername + "/PropertyImages/";
        }
        #endregion

        #region "Get Official Day Start Hour"
        public static double getOfficialDayStartHour()
        {           
            return Convert.ToDouble(ConfigurationManager.AppSettings[ConfigurationKeys.OfficialDayStartHour]);
        }
        #endregion

        #region "Get Official Day End Hour"
        public static double getOfficialDayEndHour()
        {
            return Convert.ToDouble(ConfigurationManager.AppSettings[ConfigurationKeys.OfficialDayEndHour]);
        }
        #endregion

        #region "Get Maximum Look Ahead"

        public static double getMaximunLookAhead()
        {
            return Convert.ToDouble(ConfigurationManager.AppSettings[ConfigurationKeys.MaximunLookAhead]);
        }

        #endregion

        #region "get Appointment Duration Hours"
        /// <summary>
        /// 'Appointment Duration represents total hours to complete the appointment e.g 1 hour
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public static double getAppointmentDurationHours()
        {
            return Convert.ToDouble(ConfigurationManager.AppSettings[ConfigurationKeys.AppointmentDurationHours]);
        }
        #endregion

        #region "Get File size"

        public static int getFileSize()
        {
            return Convert.ToInt32(ConfigurationManager.AppSettings[ConfigurationKeys.FileSize]);
        }

        #endregion

        public static string GetInvoiceUploadPath()
        {
            string returnVal = string.Empty;
            var configKey = ConfigurationManager.AppSettings[ConfigurationKeys.InvoiceImages];
            if (configKey != null)
                return configKey + "Invoice_Images/";

            return returnVal;
        }
    }   
}
