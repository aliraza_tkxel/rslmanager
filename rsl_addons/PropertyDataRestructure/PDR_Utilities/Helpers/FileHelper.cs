﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using PDR_Utilities.Constants;

namespace PDR_Utilities.Helpers
{
    public class FileHelper
    {


        public static string windowsSafeFileName(string fileName)
        {
            return FileHelper.windowsSafeFileName(fileName, '\u005F');
        }

        public static string windowsSafeFileName(string fileName, char replacementChar)
        {
            char[] chrArray = new char[] { '\'', '\\', '/', ':', '*', '?', '\"', '<', '>', '|',' ' };
            char[] chrArray1 = chrArray;
            if (fileName.IndexOfAny(chrArray1) >= 0)
            {
                for (int i = 0; i < (int)chrArray1.Length; i++)
                {
                    fileName = fileName.Replace(chrArray1[i], replacementChar);
                }
            }
            return fileName;
        }

        #region "get Unique File Name"
        public static string getUniqueFileName(string fileName)
        {
            dynamic ext = Path.GetExtension(fileName);
            string fileNameWithoutExtention = Path.GetFileNameWithoutExtension(fileName);
            fileNameWithoutExtention = windowsSafeFileName(fileNameWithoutExtention, '-'); 
            string uniqueString = DateTime.Now.ToString().GetHashCode().ToString("x");

            if (fileName.Length > 35)
            {
                fileName = fileName.Substring(0, 35);
            }

            fileName = fileNameWithoutExtention + uniqueString + ext;

             return fileName;
        }
        #endregion

        #region "Get File Type"
        public static string getFileType(string fileName)
        {
            string ext = Path.GetExtension(fileName);
            string fileType = string.Empty;
            if (ext.Equals(".pdf") ){
                fileType = ApplicationConstants.PdfFiletype;
            }
            else if(ext.Equals(".doc") || ext.Equals(".docx")){
                fileType = ApplicationConstants.DocFiletype;
            }
            else if (ext.Equals(".xls") || ext.Equals(".xlsx"))
            {
                fileType = ApplicationConstants.ExcelFiletype;
            }
            else {
                fileType = ApplicationConstants.ImageFiletype;
            }

            return fileName;
        }
        #endregion

    }
}
