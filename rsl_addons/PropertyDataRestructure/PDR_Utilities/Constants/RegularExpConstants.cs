﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_Utilities.Constants
{
    public class RegularExpConstants
    {
        public const string AmountExp = "^\\d+(\\.\\d+)?$";
        public const string numbersExp = "^[0-9]+$";
        public const string emailExp = "^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})$";
        public const string stringLengthExp = "^[\\s\\S]{0,30}$";
        public const string mobileExp = "^[0-9]+$";
        public const string twoDecAmount = "^\\d+.\\d{0,2}$";
    }
}
