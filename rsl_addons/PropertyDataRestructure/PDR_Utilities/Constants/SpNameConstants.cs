﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_Utilities.Constants
{
    public class SpNameConstants
    {
        #region "Master Page"
        public const string PurchaseOrderById = "PDR_PurchaseOrderContractorDetails";
        public const string UpdatePurchaseOrderById = "PDR_UpdatePurchaseOrderContractorDetails";
        public const string GetFaultRepairList = "FL_GETFAULTREPAIRLIST";
        public const string SaveCompletedWork = "PDR_SaveCompletionRecord";
        public const string GetEmployeeById = "FL_GetEmployeeById";
        public const string GetPropertyMenuList = "P_GetPropertyMenuList";
        public const string GetPropertyMenuPages = "P_GetPropertyMenuPages";
        #endregion

        #region "Development"
        public const string GetDevelopmentList = "PDR_GetDevelopmentList";
        public const string GetDevelpomentPhases = "PDR_GetDevelpomentPhases";
        public const string GetDevelopmentScheme = "PDR_GetDevelopmentScheme";
        public const string GetDevelopmentBlocks = "PDR_GetDevelopmentBlocks";
        public const string SavePhase = "PDR_SAVEPHASE";
        public const string GetPhaseInfo = "PDR_GetPhase";
        public const string GetPatch = "PDR_GetPatch";
        public const string GetCompany = "PDR_GetCompany";
        public const string GetDevelopmentType = "PDR_GetDevelopmentTypes";
        public const string GetDevelopmentStatus = "PDR_GetDevelopmentStatus";
        public const string SaveDevelopmentPhase = "PDR_UpdatePhase";
        public const string SaveDevelopmentDocument = "PDR_SaveDevelopmentDocument";
        public const string SaveDevelopment = "PDR_SaveDevelopment";
        public const string GetDevelopment = "PDR_GetDevelopment";
        public const string GetFundingSource = "PDR_GetFuncdingSource";
        public const string FetchExpireDocument = "PDR_GetExpiredDocuments";
        public const string CheckNotificationSent = "PDR_CheckNotificationSent";
        public const string LogPushNotifications = "PDR_LogSendPushNotification";
        public const string UpdateDocument = "PDR_UpdateDocument";
        public const string GetDevelopmentDetail = "PDR_GetDevelopmentDetail";
        #endregion

        #region"Documents"
        public const string SaveDocument = "PDR_SAVEDOCUMENTS";
        public const string GetAllDocuments = "PDR_GetAllDOCUMENTS";
        public const string DownloadComplianceDocument = "PDR_DownloadComplianceDocument";
        public const string GetDevelopmentsDocuments = "PDR_GetDevelopmentsDocuments";
        public const string GetEpcCategoryTypes = "P_GET_EPC_CATEGORY_TYPES";
        public const string GetDevelopmentDocumentInformationById = "PDR_GetDevelopmentDocumentInformationById";
        public const string DeleteDevelopmentDocumentsByDocumentId = "PDR_DeleteDevelopmentDocumentsByDocumentId";
        #endregion

        #region "Block"
        public const string GetBlockList = "PDR_GetBlockList";
        public const string GetBlockTempaltes = "PDR_GetBlockTemplates";
        public const string GetPropertyOwnerShip = "PDR_GetPropertyOwnerShip";
        public const string GetBlockTemplateData = "PDR_GetBlockTemplateData";
        public const string GetBlockRestrictions = "PDR_GetRestrictions";
        public const string GetLandRegistrationOptions = "PDR_GetLandRegistrationOptions"; 
        public const string SaveBlock = "PDR_SaveBlock";
        public const string GetWarrantyList = "PDR_GetWarrantyList";
        public const string GetNHBCWarrantyList = "PDR_GetNHBCWarrantyList";
        public const string GetWarrantyTypes = "PDR_GetWarrantyTypes";
        public const string GetForDropDownValues = "PDR_GetAreaItem";
        public const string GetCategoryDropDownValues = "PDR_GetSchemeBlockLocations";
        public const string GetAreaDropDownValuesByCategory = "PDR_GetSchemeBlockAreaByLocationId";
        public const string GetItemDropDownValuesByArea = "PDR_GetSchemeBlockItemByAreaId";
        public const string GetContractors = "PDR_GetContractor";
        public const string SaveWarranty = "PDR_SaveWarranty";
        public const string GetWarrantyData = "PDR_GetWarrantyData";
        public const string DeleteWarranty = "PDR_DeleteWarranty";
        public const string GetPropertiesList = "PDR_GetPropertiesList";
        public const string RemoveProperties = "PDR_RemoveSelectedProperty";
        public const string GetBlockListBySchemeID = "PDR_GetBlockListBySchemeID";
        public const string GetAsbestosAndRiskInformation = "AS_GetAsbestosAndRiskInformation";
        public const string GetPropertiesByDevPhaseBlock = "PDR_GetPropertiesByDevPhaseBlock";
        public const string SaveRestrictions = "PDR_SaveRestrictions";
        #endregion

        #region "Scheme"
        public const string GetSchemeRestrictions = "PDR_GetRestrictions";   
        public const string GetFaultsAndDefectsForSchemeBlock = "AS_GetFaultsAndDefectsForSchemeBlock";
        public const string GetSchemeList = "PDR_GetSCHEMELIST";
        public const string PopulateSchemeDropDown = "PDR_GetDevelopmentScheme";
        public const string GetSchemeFaultsAndDefects = "PDR_GetFaultsAndDefects";
        public const string GetSchemePlannedAppointment = "PDR_GetSchemePlannedAppointment";
        public const string GetBlocksBySchemeID = "PDR_GetBlocksBySchemeID";
        public const string GetBlockDropDownBySchemeId = "PDR_GetBlocks";
        public const string GetSchemeASBCount = "PDR_GetSchemeASB";
        public const string GetSchemeRepairsCount = "PDR_GetSchemeRepairsCount";
        public const string GetSchemeDetailBySchemeId = "PDR_GetSchemeDetailBySchemeId";
        public const string GetBlocksPropertiesByPhaseId = "PDR_GetBlocksPropertiesByPhaseId";
        public const string SaveScheme = "PDR_SAVESCHEME";
        public const string UpdateSchemeBlocks = "PDR_UpdateSchemeBlocks";
        public const string UpdateSchemeProperties = "PDR_UpdateSchemeProperty";
        public const string GetSchemeData = "PDR_GetSchemeData";
        public const string GetBlocksByBlockID = "PDR_GetBlocksByBlockID";
        public const string GetBlockNameBySchemeId = "PDR_GetBlockNameBySchemeId";
        public const string GetSchemeByBlockId = "PDR_GetSchemeByBlockId";
        #endregion

        #region Reports
        public const string GetProfessionalAnalysisReport = "P_PORTFOLIOANALYSIS_REPORT";
        public const string GetProfessionalAnalysisReportSingleAssetType = "P_PORTFOLIOANALYSIS_REPORT_STOCK_SINGLEASSETTYPE";
        public const string GetProfessionalAnalysisReportWithOccupancy = "P_PORTFOLIOANALYSIS_REPORT_STOCK_SINGLEASSETTYPE_NUMB_BEDROOMS_WITH_OCCUPANCY";
        public const string GetProfessionalAnalysisReportDetail = "PDR_PORTFOLIOANALYSIS_REPORT_DETAIL";
        public const string GetAnalysisReportDetailPrint = "P_PORTFOLIOANALYSIS_REPORT_DETAIL";
        public const string GetDevelopmentByPatchIdAndLAId = "PDR_GetDevelopmentByPatchIdAndLAId";
        public const string GetLocalAuthority = "PDR_GetLocalAuthority";
        public const string GetVoidAnalysisReport = "P_PORTFOLIOANALYSIS_REPORT_VOID_ANALYSIS";
        public const string GetCP12StatusReport = "V_GetCP12StatusReport";
        public const string GetVoidAnalysisReportDetail = "PDR_PORTFOLIOANALYSIS_REPORT_VOID_ANALYSIS_DETAIL";
        public const string GetVoidAnalysisReportDetailForPrint = "P_PORTFOLIOANALYSIS_REPORT_VOID_ANALYSIS_DETAIL";
        public const string GetTerriorReportData = "PDR_GetTerriorReportData";
        public const string GetTerriorReportDataForExcel = "PDR_TerriorReportExportToExcel";
        public const string GetProfessionalAnalysisReportWithOutOccupancy = "P_PORTFOLIOANALYSIS_REPORT_STOCK_SINGLEASSETTYPE_NUMB_BEDROOMS";
        public const string GetFiscalYears = "F_GET_FISCALYEAR";
        public const string GetCustomerAccountRecharges = "F_GET_CUSTOMER_ACCOUNT_RECHARGES";
        public const string GetCustomerLastPayment = "AM_SP_GetCustomerLastPayment";
        public const string GetCustomerInformation = "PDR_GetCustomerInformation";
        public const string GetCustomerAccountDesc = "FLS_GET_CUSTOMER_ACCOUNT_DESC";
        public const string GetIncomeRecoveryOfficer = "FLS_GET_INCOME_RECOVERY_OFFICER";
        public const string GetRentLetterMultiPrint = "C__RentLetter_MultiPrint";
        public const string SaveExcludedProperties = "PDR_SaveExcludedProperties";
        public const string GetServiceChargeSchemeAssociatedBlocks = "PDR_GetServiceChargeSchemeAssociatedBlocks";
        public const string GetIncExcPropertiesOfServiceCharge = "PDR_GetIncExcPropertiesOfServiceCharge";
        public const string GetPurchaseItemExProperties = "PDR_GetPurchaseItemExProperties";
        public const string GetServiceChargeSelectedProperties = "PDR_GetServiceChargeSelectedProperties";
        public const string GetGeneralJournalExProperties = "PDR_GetGeneralJournalExProperties";

        #endregion

        public const string  PdrGetCategoriesList = "PDR_GetCategories";
        #region SchemeBlock

        #region Documents
        public const string PdrGetDocumentTypes = "PDR_GetDocumentTypes";
        public const string GetDocumentTypes = "P_GET_DOCUMENT_TYPES";
        public const string GetDocumentSubtypes = "P_GET_DOCUMENT_SUBTYPES";
        public const string GetPropertyDocumentsInfo = "AS_GetPropertyDocumentsInformation";
        public const string SavePropertyDocumentInformation = "AS_SavePropertyDocumentInformation";
        public const string DeletePropertyDocumentsByDocumentId = "PDR_DeletePropertyDocumentsByDocumentId";
        public const string GetPropertyDocumentInformationById = "AS_GetPropertyDocumentInformationById";
        public const string GetCP12DocumentByLGSRID = "AS_GetCP12DocumentByLGSRID";
        #endregion

        #region "Health&Safety Tab"
        public const string getAsbestosAndRiskInformation = "AS_GetAsbestosAndRiskInformation";
        public const string getHealthAndSafetyTabInformation = "PDR_GetHealthAndSafetyTabInformation";
        public const string saveAsbestosInformation = "PDR_SaveAsbestosInformation";
        public const string saveRefurbishmentInformaiton = "PDR_INSERT_AND_RETRIVE_REFURBISHMENT_HISTORY";
        public const string getRefurbishmentInformaiton = "PDR__GetRefurbishmentInformaiton";
        public const string getAsbestosRiskAndOtherInfo = "AS_getAsbestosRiskAndOtherInfo";
        public const string amendAsbestosInformation = "AS_AmendAsbestosInformation";
        public const string GetEmployeeEmailForAsbestosEmailNotifications = "AS_GetEmployeeEmailForAsbestosEmailNotifications";

        #endregion

        #region Attributes
        public const string GetLocations = "PDR_GetLocations";
        public const string GetAreasByLocationId = "PDR_AreaByLocationId";
        public const string GetItemsByAreaId = "PDR_ItemByAreaId";
        public const string GetSubItemsByItemId = "PDR_SubItemByItemId";
        public const string GetItemByItemId = "AS_ItemByItemId";
        public const string GetBoilersList = "PDR_GetBoilersList";
        public const string GetMultiAttributeListByItemId = "PDR_GetMultiAttributeListByItemId";
        public const string GetItemNotes = "PDR_getItemNotes";
        public const string SaveItemNotes = "AS_SaveItemNotes";
        public const string UpdateItemNotes = "AS_UpdateItemNotes";
        public const string DeleteItemNotes = "AS_DeleteItemNotes";
        public const string GetItemNotesTrail = "AS_GetItemNotesTrail";

        public const string GetPropertyImages = "AS_getPropertyImages";
        public const string SavePhotograph = "AS_SavePhotograph";

        public const string GetItemDetail = "PDR_GetItemDetail";
        public const string GetCycleType = "PDR_GetCycleType";
        public const string AmendAttributeItemDetail = "PDR_AmendAttributeItemDetail";
        public const string SaveServiceChargeProperties = "PDR_PopulateServiceChargePropertyList";


        public const string GetMake = "AS_GetMake";
        public const string GetModelsList = "AS_GetModelsList";
        public const string GetApplianceType = "AS_GetApplianceType";
        public const string SavePropertyAppliance = "AS_SavePropertyAppliance";
        public const string GetApplianceLocations = "AS_GetApplianceLocations";
        public const string GetServiceChargeExProperties = "PDR_GetServiceChargeExProperties";
        public const string GetServiceChargeAssociatedSchemesBlocks = "PDR_GetServiceChargeAssociatedSchemesBlocks";
        public const string PropertyAppliances = "AS_PropertyAppliances";

        public const string GetServiceChargeReport = "PDR_GetServiceChargeReportData";
        public const string GetServiceChargeReportPO = "PDR_GetServiceChargeReportPO";
        public const string GetServiceChargeReportDC = "PDR_GetServiceChargeReportDC";
        public const string SavedetectorsForSchemeBlock = "PDR_savedetectorsForSchemeBlock";

        public const string GetDetectorBySchemeBlockId = "PDR_GetDetectorBySchemeBlockId";
        public const string GetSchemeBlockDefectsById = "PDR_getSchemeBlockDefectsById";
        public const string GetDefectManagementLoopkupsData = "DF_GetSchemeBlockDefectManagementLoopkupsData";
        public const string saveDefect = "PDR_SaveDefectForSchemeBlock";
        public const string getSchemeBlockDefectDetails = "PDR_getSchemeBlockDefectDetails";
        public const string getSchemeBlockAppliances = "PDR_GetSchemeBlockAppliances";
        public const string getPropertyCategory = "AS_GetDefectCategory";
        public const string getSchemeBlockDefectImages = "PDR_getSchemeBlockDefectImages";
        public const string GetSchemeBlockSummaryImages = "P_GetSchemeBlockImages";
        public const string getPropertyImages = "AS_getPropertyImages";
        public const string savePhotograph = "PDR_SaveSchemeBlockPhotograph";
        #endregion

        //::ST - Stored Procedure Names for Provisions
        #region Provisions
        public const string GetProvisions = "PDR_GetProvisions";
        public const string GetProvisionItemDetail  = "PDR_GetProvisionDetailByProvisionId";
        public const string AmendProvisionItemDetail = "PDR_AmendProvisionItemDetail";
        public const string GetProvisionNotes = "PDR_GetProvisionNotes";
        public const string SaveProvisionNotes = "PDR_SaveProvisionNotes";
        public const string GetProvisionImages = "PDR_GetProvisionImages";
        public const string SaveProvisionPhotograph = "PDR_SaveProvisionPhotograph";
        public const string GetProvisionItemHistory = "PDR_GetProvisionItemHistoryProvisionId";
        public const string GetProvisionsListReport = "PDR_GetProvisionsList";
        public const string GetProvisionsCategories = "PDR_GetProvisionCategories";
        public const string SaveProvisionChargeProperties = "PDR_PopulateProvisionChargePropertyList";
        public const string GetProvisionChargeSelectedProperties = "PDR_GetProvisionChargeSelectedProperties";
        public const string GetProvisionChargeExProperties = "PDR_GetProvisionChargeExProperties";
        public const string GetProvisionsByCategory = "PDR_GetProvisionsByCategory";
        public const string GetProvisionByProvisionId = "PDR_GetProvisionByProvisionId";
        #endregion

        #endregion

        #region M & E Scheduling
        public const string GetAppointmentsArranged = "PDR_GetAppointmentsArranged";
        public const string GetAppointmentsToBeArranged = "PDR_GetAppointmentsToBeArranged";
        public const string GetAllTrades = "FL_GetTradeLookUpAll";
        public const string GetAvailableOperatives = "PDR_GetAvailableOperatives";
        public const string ScheduleMeWorkAppointment = "PDR_ScheduleMeWorkAppointment";
        public const string GetPropertyInfoByJournalId = "PDR_GetPropertyInfoByJournalId";
        public const string GetArrangedAppointmentsDetailByJournalId = "PDR_GetArrangedAppointmentsDetailByJournalId";
        public const string UpdateMeAppointmentNotes = "PDR_UpdateMeAppointmentNotes";
        public const string CancelArrangedAppointment = "PDR_CancelArrangedAppointment";
        public const string GetOperativeInformation = "PLANNED_GetOperativeInfo";
        public const string GetAttributeList = "PDR_GetAttributeList";
        #endregion

        #region "Customer"
        public const string AmendCustomerAddress = "FL_AmendCustomerAddress";
        #endregion

        #region "Assign To contractor"

        #region "Drop Down Values"
        public const string GetCostCentreDropDownValues = "PLANNED_GetCostCentreDropDownValues";
        public const string GetBudgetHeadDropDownValuesByCostCentreId = "PLANNED_GetBudgetHeadDropDownValuesByCostCentreId";
        public const string GetExpenditureDropDownValuesByBudgetHeadId = "PLANNED_GetUserExpenditureDropDownValuesAndLimitsByHeadId";
        public const string GetContactDropDownValuesbyContractorId = "PLANNED_GetContactDropDownValuesbyContractorId";
        public const string GetMeContractorDropDownValues = "PDR_GetMeContractorDropDownValues";
        public const string GetBlocksByScheme = "PDR_GetBlocksByScheme";
        public const string GetPropertyByBlock = "PDR_GetPropertyByBlock";        
        public const string GetVatDropDownValues = "PLANNED_GetVatDropDownValues";
        public const string GetAllContractorsDropDownValues = "PDR_GetAllContractors";
        public const string GetAreaDropDownValues = "PDR_GetArea";
        public const string GetAttributesSubItemsDropDownValuesByAreaId = "PDR_GetAttributesSubItemsByAreaID";
        public const string GetContactEmailDetail = "DF_GetContactEmailDetail";
       
        #endregion

        #region "Save Assign to Contractor"
        public const string GetAssignToContractorDetailByJournalId = "PDR_GetAssignToContractorDetailByJournalId";
        public const string AssignWorkToContractor = "PDR_AssignWorkToContractor";
        public const string AssignWorkToContractorForSchemePO = "PDR_SchemeBlockAssignWorkToContractor";
        public const string GetDetailforEmailToContractor = "PDR_GetDetailforEmailToContractor";
        public const string GetSBDetailforEmailToContractor = "PDR_GetSBDetailforEmailToContractor";
        public const string GetContractDetailByContractorId = "PDR_GetContractDetailByContractorId";
        public const string GetContractDetailByContractorIdForAssignToContractor = "PDR_GetContractDetailByContractorIdForAssignToContractor";
        public const string GetBudgetHolderByOrderId = "FL_GetBudgetHolderByOrderId";

        public const string GetSBDetailforProvisionEmailToContractor = "PDR_GetSBDetailforProvisionEmailToContractor";
        #endregion

        #region  PO Actions
        public const string ChangeWorkStatusByContractor = "PDR_ChangeWorkStatusByContractor";
        public const string SaveNoEntryData = "PDR_SaveNoEntryData";
        public const string SaveCancelledData = "PDR_SaveCancelledData";
        public const string RejectedByContractorReport = "FL_GetSubcontractorList";
        public const string GetSchedulerEmailId = "PDR_GetSchedulerEmailId";
        public const string GetPoStatusForSessionCreation = "PDR_GetPoStatusForSessionCreating";
        #endregion

        #region Get Sub Items Details
        public const string GetItemMSATDetailByItemId = "PDR_GetItemMSATDetailByItemId";
        #endregion

        #endregion

        #region "Void Inspection"
        public const string GetVoidInspectionsToBeArranged = "V_GetVoidInspectionsToBeArranged";
        public const string GetAllVoidOperatives = "V_GetVoidOperatives";
        public const string GetOperativesLeavesAndAppointment = "V_GetOperativesLeavesAndAppointment";
        public const string ScheduleVoidInspection = "V_ScheduleVoidInspection";
        public const string GetVoidInspectionArranged = "V_GetVoidInspectionArranged";
        public const string GetVoidInspectionByJournalId = "V_GetVoidInspectionByJournalId";
        public const string GetVoidWorksToBeArranged = "V_GetVoidWorksToBeArranged";
        public const string GetVoidWorksArranged = "V_GetVoidWorksArranged";
        public const string GetVoidWorksRequired = "V_GetVoidWorksRequired";
        public const string UpdateTenantToComplete = "V_UpdateTenantToComplete";
        public const string AddTenantRecharges = "V_AddTenantRecharges";
        public const string GetVoidRoomList = "V_GetVoidRoomList";
        public const string GetVoidAreaList = "V_GetVoidAreaList";
        public const string UpdateRequiredWorks = "V_UpdateRequiredWorks";
        public const string GetJointTenantsInfoByTenancyID = "AS_GetJointTenantsInfoByTenancyID";
        public const string GetChecksToBeArranged = "V_GetGasElectricChecksToBeArranged";
        public const string ScheduleVoidRequiredWorks = "V_ScheduleVoidRequiredWorks";
        public const string ReScheduleWorksRequired = "V_ReScheduleWorksRequired";
        public const string CancelVoidAppointment = "V_CancelVoidAppointment";
        public const string GetChecksArranged = "V_GetGasElectricChecksArranged";
        public static string GetPaintPacksList = "V_GetPaintPacksList";
        public const string GetPaintPacksDetail = "V_GetPaintPacksDetail";
        public const string AmendPaintPacks = "V_AmendPaintPacks";
        public const string UpdatePaintPacks = "V_UpdatePaintPacks";
        public const string ReScheduleVoidInspection = "V_ReScheduleVoidInspection";
        public const string ScheduleGasElectricChecks = "V_ScheduleGasElectricChecks";
        public const string GetPostVoidInspectionsToBeArranged = "V_GetPostVoidInspectionsToBeArranged";
        public const string GetPostVoidInspectionArranged = "V_GetPostVoidInspectionArranged";
        public const string GetPendingTermination = "V_GetPendingTermination";
        public const string GetReletAlertCount = "V_GetReletAlertCount";
        public const string GetNoEntryCountAndList = "V_GetNoEntryList";
        public const string GetAvailableProperties = "V_GetAvailableProperties";
        public const string CancelGasElectricAppointment = "V_CancelGasElectricAppointment";
        public const string GetAppointmentDetailByJournalId = "V_GetAppointmentDetailByJournalId";
        public const string GetPropertyAndTenantInfoByJournalId = "V_GetPropertyAndTenantInfoByJournalId";
        public const string GetReactiveRepairContractors = "V_GetReactiveRepairContractors";
        public const string RequiredWorksAssignWorkToContractor = "V_RequiredWorksAssignWorkToContractor";
        public const string VoidGetContractDetailByContractorId = "V_GetContractDetailByContractorId";
        public const string PaintPackAssignWorkToContractor = "V_PaintPackAssignWorkToContractor";
        public const string VoidGetPropertyInfoByJournalId = "V_GetPropertyInfoByJournalId";
        public const string GetOperativeWorkingHours = "FL_GetOperativeWorkingHours";
        public const string GetArrangedVoidWorks = "V_GetArrangedVoidWorksRequired";
        public const string GetVoidWorksByInspectionJournalId = "V_GetVWAppointmentByInspectionJournalId";
        public const string GetOperativeDataForAlert = "V_GetOperativeDataForAlert";
        public const string GetSupplierDataForAlert = "V_getSupplierDataForAlert";
        
        #endregion

        #region British Gas Notification
        public const string GetBritishGasNotificationList = "V_GetBritishGasNotificationList";
        public const string GetBritishGasStage1Data = "V_GetBritishGasStage1Data";
        public const string GetBritishGasStage2AndStage3Data = "V_GetBritishGasStage2AndStage3Data";
        public const string GetMeterTypes = "V_GetMeterTypes";  
        public const string SaveBritishGasData = "V_SaveBritishGasData";
        public const string UpdateBritishGasData = "V_UpdateBritishGasStage1Data";
        public const string UpdateBritishGasStage2Data = "V_UpdateBritishGasStage2Data";
        public const string UpdateBritishGasStage3Data = "V_UpdateBritishGasStage3Data";
        public const string GetVoidPDFDocumentInfo = "V_GetVoidPDFDocumentInfo";
        #endregion

        #region Cyclic Maintenance
        public const string GetAttributeTypeForAssignToContractor = "PDR_GetAttributeTypeForAssignToContractor";
        public const string GetAllSchemes = "DF_GetAllSchemes";
        public const string GetMaintenanceStatuses = "PDR_GetMaintenanceStatuses";
        public const string GetMaintenanceArrangedAppointmentsCount = "PDR_GetMaintenanceArrangedAppointmentsCount";
        public const string GetMaintenanceAppointmentToBeArrangedCount = "PDR_GetMaintenanceAppointmentToBeArrangedCount";
        public const string GetMaintenanceNoEntryCount = "PDR_GetMaintenanceNoEntryCount";
        public const string GetMaintenanceOverdueCount = "PDR_GetMaintenanceOverdueCount";
        public const string GetBlockListByScheme = "PDR_GetBlockListByScheme";
        public const string GetPropertiesByScheme = "PDR_GetPropertiesByScheme";

        public const string GetServicesToBeAllocated = "CM_GetServicesToBeAllocated";
        public const string GetServicesAllocated = "CM_GetServicesAllocated";
        public const string GetServiceItemDetails = "CM_GetServiceItemDetails";
        public const string UdateCycleValue = "CM_UdateCycleValue";
        public const string AssignCyclicalServicesToContractor = "CM_AssignCyclicalServicesToContractor";
        public const string CMGetDetailforEmailToContractor = "CM_GetDetailforEmailToContractor";
        public const string CMGetPoStatusForSessionCreating = "CM_GetPoStatusForSessionCreating";
        public const string CMPurchaseOrderContractorDetails = "CM_PurchaseOrderContractorDetails";
        public const string AcceptCyclicalPurchaseOrder = "CM_AcceptCyclicalPurchaseOrder";

        public const string UpdateWorksCompleted = "CM_UpdateWorksCompleted";
        public const string UpdateInvoiceUpload = "CM_UpdateInvoiceUpload";
        public const string GetCyclicalTab = "CM_GetCyclicalTab";
        public const string GetUnassignedCyclicalServices = "CM_GetUnassignedCyclicalServices";
        public const string GetContractorDropDownValues = "CM_GetContractorDropDownValues";
        public const string CancelCyclicalWork = "CM_CancelCyclicalWork";
        public const string GetCancelledOrder = "CM_GetCancelledOrder";

        #endregion

        #region Documents
        public const string GetAllDevelopmentDocuments = "PDR_GetAllDevelopmentDocuments";
        public const string GetAllSchemeDocuments = "PDR_GetAllSchemeDocuments";
        public const string GetAllPropertyDocuments = "PDR_GetAllPropertyDocuments";
        public const string GetDocumentsEmployees = "PDR_GetDocumentsEmployees";
        #endregion
    }
}
