﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_Utilities.Constants
{
    public class ViewStateConstants
    {
        public const string PageSortBo = "PageSortBo";
        public const string TotalCount = "TotalCount";
        public const string ResultDataSet = "ResultDataSet";
        public const string CompleteDataSet = "CompleteDataSet";
        public const string VirtualItemCount = "VirtualItemCount";
        public const string Search = "Search";
        public const string SearchText = "SearchText";
        public const string Check56Days = "Check56Days";
        public const string MsatType = "MsatType";
        public const string BlockId = "BlockId";
        public const string SchemeId = "SchemeId";
        public const string RequestType = "RequestType";
        public const string DevelopmentDocuments = "DevelopmentDocuments";
        public const string DefectCategoryId = "DefectCategoryId";
        public const string DefectsDataTable = "DefectsDataTable";
        public const string ApplianceDefectBO = "ApplianceDefectBO";

        public const string DevelopmenId = "DevelopmenId";
        public const string PhaseIdList = "PhaseIdList";
        public const string DocIdList = "DocIdList";
        public const string DevelopmentFundingDt = "DevelopmentFundingDt";

        #region "Scheduling"
        public const string JournalId = "journalId";
        public const string AppointmentId = "appointmentId";
        public const string PropertyId = "PropertyId";
        public const string CurrentIndex = "CurrentIndex";
        public const string TotalJobsheets = "TotalJobsheets";
        public const string IsPhotoGraphExist = "IsPhotoGraphExist";
        #endregion

        #region "Assign to Contractor"
        public const string ServiceRequiredDT = "ServiceRequiredDT";
        public const string AssignToContractorWorkType = "AssignToContractorWorkType";
        public const string IsSaved = "IsSaved";
        #endregion

        #region "Property Summary Tab"

        public const string FaultsResultDataSet = "FaultsResultDataSet";

        #endregion

    }
}
