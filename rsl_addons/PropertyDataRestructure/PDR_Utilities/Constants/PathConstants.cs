﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_Utilities.Constants
{
    public class PathConstants
    {
        #region "URL Constants"
        public const string LoginPath = "~/../BHAIntranet/Login.aspx";
        public const string BridgePath = "~/Bridge.aspx";
        public const string PropertyRecordPath = "~/../RSLApplianceServicing/Bridge.aspx?pid=";
        public const string SeparatorImagePath = "~/Images/menu/sep.jpg";
        public const string BlockList = "~/Views/Pdr/Block/BlockList.aspx";
        public const string BlockMain = "~/Views/Pdr/Block/BlockMain.aspx";
        public const string SchemeDashBoard = "~/Views/Pdr/Scheme/Dashboard/SchemeDashBoard.aspx";
        public const string BRSFaultLocator = "~/../BRSFaultLocator/Bridge.aspx";
        public const string ToBeArrangedAppointmentSummary = "~/Views/Scheduling/ToBeArrangedAppointmentSummary.aspx";
        public const string ScheduleMeServicing = "~/Views/Scheduling/ScheduleMEServicing.aspx";
        public const string MeServicingPath = "~/Views/Scheduling/MEServicing.aspx";
        public const string CyclicalServicingPath = "~/Views/CyclicalServices/CyclicalServices.aspx";
        public const string ArrangedAppointmentSummaryPath = "~/Views/Scheduling/ArrangedAppointmentSummary.aspx";
        public const string BackToScheduleMeServicingPath = "~/Views/Scheduling/ScheduleMEServicing.aspx";
        public const string AccessDeniedPath = "~/Views/Common/AccessDenied.aspx";

        public const string DevelopmentListPath = "~/Views/Pdr/Development/DevelopmentList.aspx";
        public const string BlockListPath = "~/Views/Pdr/Block/BlockList.aspx";
        public const string PropertiesPath = "~/Views/Property/Properties.aspx";
        public const string SchemeListPath = "~/Views/Pdr/Scheme/SchemeList.aspx";
        public const string PropertyTerrierPath = "~/Views/Reports/ReportArea.aspx?rpt=terrier";
        public const string RentAnalysisPath = "~/Views/Reports/ReportArea.aspx?rpt=rent";
        public const string RentAccountSummaryPath = "~/Views/Reports/RentAccountSummary.aspx";
        public const string StockAnalysisPath = "~/Views/Reports/ReportArea.aspx?rpt=stock";
        public const string VoidAnalysisPath = "~/Views/Reports/ReportArea.aspx?rpt=void";
        public const string SchemeDashboardPath = "~/Views/Pdr/Scheme/Dashboard/SchemeDashBoard.aspx";
        public const string SchemeRecordPath = "~/Views/Pdr/Scheme/Dashboard/SchemeRecord.aspx";
        public const string MAndeServicingPath = "~/Views/Scheduling/MEServicing.aspx?msat=mes";
        public const string ICalFilePath = "~/Attachment/AppointmentArrange.ics";
        public const string PropertiesListPath = "~/../RSLApplianceServicing/Bridge.aspx?pg=properties&id=";
        public const string VoidInspectionPath = "~/Views/Void/VoidInspection/VoidInspections.aspx?src=rsl";
        public const string VoidWorksPath = "~/Views/Void/VoidWorks/VoidAppointments.aspx?src=rsl";
        public const string RearrangeVoidWorksRequiredPath = "~/Views/Void/VoidWorks/ReArrangeVoidWorksRequired.aspx?jid=";
        public const string VoidWorksRequired = "~/Views/Void/VoidWorks/VoidWorksRequired.aspx?jid=";
        public const string ScheduleRequiredWorks = "~/Views/Void/VoidWorks/SchedulingRequiredWorks.aspx";
        public const string JSVJobSheetSummary = "~/Views/Void/JSVJobSheetSummary.aspx";
        public const string VoidAppointments = "~/Views/Void/VoidWorks/VoidAppointments.aspx";
        public const string ReScheduleRequiredWorks = "~/Views/Void/VoidWorks/RearrangeWorksRequired.aspx";
        public const string PaintPacksDetail = "~/Views/Void/PaintPacks/PaintPacksDetail.aspx?pid=";
        public const string SendStage1NotificationUserControl = "~/UserControls/Void/BritishGasNotification/SendNotificationStage1PopUp.ascx";

        public const string BritishGasNotificationPath = "~/Views/Void/BritishGasNotification/BritishGasNotificationList.aspx";
        public const string AvailablePropertiesPath = "~/Views/Reports/VoidReportArea.aspx?rpt=ap";
        public const string NoEntryReportPath = "~/Views/Reports/VoidReportArea.aspx?rpt=ne";
        public const string PaintPackList = "~/Views/Void/PaintPacks/PaintPacksList.aspx";
        public const string ServiceChargeReportPath = "~/Views/Reports/ServiceChargeReport.aspx";
        public const string ProvisioningReportPath = "~/Views/Reports/ProvisioningReport.aspx";
        public const string CustomerModule = "~/../Customer/CRM.asp?CustomerID=";


        #endregion

        #region "Property Image Path"
        public const String PDRDocumentPath = "../../../Documents/";
        #endregion

        #region "Query String Constants"
        public const string ToBeArrangedJobsheetSrc = Source + "=" + ToBeArrangedJobsheet;
        public const string MsatType = "msat";
        public const string DevelopmentId = "did";
        public const string MEServicing = "mes";
        public const string CyclicMaintenance = "cm";
        public const string AppointmentType = "apttype";
        public const string AppointmentToBeArranged = "atba";
        public const string SchemeId = "sid";
        public const string BlockId = "bid";
        public const string AttributeTypeId = "atid";
        public const string AppointmentArranged = "apttype";
        public const string Source = "src";
        public const string Path = "path";
        public const string ToBeArrangedJobsheet = "tbajobsheet";
        public const string ListingTab = "tab";
        public const string Page = "pg";
        public const string Menu = "mn";
        public const string Rsl = "rsl";
        public const string DevelopmentList = "developmentlist";
        public const string BlockListQr = "blocklist";
        public const string Properties = "properties";
        public const string SchemeList = "schemelist";
        public const string PropertyTerrier = "propertyterrier";
        public const string RentAnalysis = "rentanalysis";
        public const string StockAnalysis = "stockanalysis";
        public const string VoidAnalysis = "voidanalysis";
        public const string SchemeDashboard = "schemedashboard";
        public const string SchemeRecord = "schemerecord";
        public const string MeServicing = "meservicing";
        public const string AccessDenied = "accessdenied";
        public const string JId = "jid";
        public const string PdrJId = "pdrjid";
        public const string MaintenanceMenu = "Maintenance";
        public const string AdminMenu = "Admin";
        public const string ReportsMenu = "Reports";
        public const string ServicingMenu = "Servicing";
        public const string VoidMenu = "Voids";
        public const string VoidWorks = "VoidWorks";
        public const string NotificationsMenu = "Notifications";
        public const string AvailablePropertiesMenu = "AvailableProperties";
        public const string PId = "pid";
        public const string BlockDashboardPage = "BlockDashboard.aspx";
        public const string BlockListPage = "BlockList.aspx";
        public const string BlockMainPage = "BlockMain.aspx";
        public const string AddNewDevelopmentPage = "AddNewDevelopment.aspx";
        public const string DevelopmentListPage = "DevelopmentList.aspx";
        public const string SchemeDashBoardPage = "SchemeDashBoard.aspx";
        public const string SchemeListPage = "SchemeList.aspx";
        public const string ReportAreaPage = "ReportArea.aspx";
        public const string ComplianceDocumentsPage = "ComplianceDocuments.aspx";
        public const string MaintenanceDashboardPage = "MaintenanceDashboard.aspx";
        public const string ArrangedAppointmentSummaryPage = "ArrangedAppointmentSummary.aspx";
        public const string MEServicingPage = "MEServicing.aspx";
        public const string ScheduleMEServicingPage = "ScheduleMEServicing.aspx";
        public const string ToBeArrangedAppointmentSummaryPage = "ToBeArrangedAppointmentSummary.aspx";
        public const string VoidInspection = "VoidInspection";
        public const string Rearrange = "rearrange";
        public const string ServiceCharge = "servicecharge";
        public const string ProvisioningReport = "provisioning";
        public const string CustomerId = "cid";
        public const string PropertyModuleMenu = "Search";
        public const string RentAccountSummary = "RentSummary";
        public const string TenancyId = "tenancyId";
        public const string RentAccountCustomerId = "customerid";
       
        //Void Menus

        public const string VoidDashboard = "Dashboard.aspx";
        public const string VoidInspections = "VoidInspections.aspx";
        public const string PostVoidInspections = "PostVoidInspections.aspx";
        public const string VoidAppointmentsPage = "VoidAppointments.aspx";
        public const string PaintPacksList = "PaintPacksList.aspx";
        public const string CP12StatusReport = "CP12StatusReport.aspx";
        public const string GasElectricChecksPage = "GasElectricChecks.aspx";
        public const string VoidReportArea = "VoidReportArea.aspx";
        public const string BritishGasNotificationList = "BritishGasNotificationList.aspx";
        public const string ArrangedTab = "aptArranged";
        public const string VoidWorksRequiredPage = "VoidWorksRequired.aspx";
        public const string VoidWorksReArrangedPage = "ReArrangeVoidWorksRequired.aspx";
        public const string SchedulingRequiredWorksPage = "SchedulingRequiredWorks.aspx";
        public const string RearrangeWorksRequiredPage = "RearrangeWorksRequired.aspx";
        public const string JSVJobSheetSummaryPage = "JSVJobSheetSummary.aspx";
        public const string PaintPacksDetailPage = "PaintPacksDetail.aspx";
        public const string ProvisioningReportPage = "ProvisioningReport.aspx";
        public const string ServiceChargePage = "ServiceChargeReport.aspx";
        public const string SchemeRecordPage = "SchemeRecord.aspx";
        public const string CyclicalServices = "CyclicalServices.aspx";
        public const string UnassignedCyclicalReport = "UnassignedCyclicalReport.aspx";
        public const string CancelledOrderReport = "CancelledOrderReport.aspx";

        public const string UploadDocument = "UploadDocument.aspx";

        #endregion

    }

}
