﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_Utilities.Constants
{
    public class ConfigurationKeys
    {
        public const string LogErrorsAndMessages = "LogErrors";
        public const string DocumentUploadPath = "DocumentsUploadPath";
        public const string OfficialDayStartHour = "OfficialDayStartHour";
        public const string OfficialDayEndHour = "OfficialDayEndHour";
        public const string MaximunLookAhead = "MaximunLookAhead";
        public const string AppointmentDurationHours = "AppointmentDurationHours";
        public const string FileSize = "FileSize";
        public const string PdrDocumentUploadPath = "PDRDocumentsPath";
        public const string PdrDevDocsDownloadPath = "PDRDevDocsDownloadPath";
        public const string PdrSchemeDocsDownloadPath = "PDRSchemeDocsDownloadPath";
        public const string PropertyDocumentUploadPath = "ImageUploadPath";
        public const string InvoiceImages = "InvoiceImages";
    }
}
