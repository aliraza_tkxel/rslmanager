﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_Utilities.Constants
{
    public class UserMessageConstants
    {
        #region Development, Block , Scheme
        public const string DateInvalid = "Enter valid To and/or From Dates.";       
        public const string SelectFundingSource = "Please select funding source.";       
        public const string EnterGrantAmount = "Please enter grant amount.";
        public const string OnlyDigitsAllowed = "Only digits allowed for grant amount.";
        public const string InvalidDevelopmentId = "Invalid development id.";
        public const string RecordNotFound = "Record not found.";
        public const string ProblemLoadingData = "Problem loading data.";
        public const string SchemeNameNotFound = "Please enter scheme name.";
        public const string SchemeCodeNotFound = "Please enter scheme code.";
        public const string PatchNotFound = "Please enter patch.";
        public const string LocalAuthority = "Please enter local authority.";
        public const string PhaseNameNotFound = "Pleases enter phase name";
        public const string PhaseNotFound = "Please select phase. It is not possible to create a scheme without a phase.";
        public const string DevelopmentNotFound = "Please select development.";
        public const string EnterValidTitleandDate = "Please enter title and date.";
        public const string EnterValidDocumentDate = "Please enter Document date.";
        public const string DocumentDateFormateError = "Document Date format is not valid.";
        public const string SelectValidDocuentType = "Please select valid Title";
        public const string SelectValidDocumentTitle = "Please select title";
        public const string TitleNotFound = "Please enter document title.";
        public const string ExpireDateNotFound = "Please enter expiry date.";
        public const string DevelopmentNameNotFound = "Please enter development name.";
        public const string DevelopmentSaveSuccessfuly = "Development saved successfully.";
        public const string DuplicateDevelopmentError = "Development already exists with same name, Please enter different name";
        public const string DuplicateBlockError = "Block already exists with same name, Please enter different name";
        public const string DuplicateSchemeError = "Scheme already exists with same name, Please enter different name";
        public const string SchemeNotSaved = "Scheme could not saved.";
        public const string Address1NotFound = "Please enter Address1.";
        public const string TownCityNotFound = "Please enter Town/City.";
        public const string PhaseSavedSuccessfuly = "Phase saved successfully.";
        public const string ExpiryDateShouldbeGreaterThanToday = "Expiry date should be greater than today";
        public const string ErrorDataBaseConnection = "Error while connecting with database.";
        public const string NoRecordFound = "No record found.";
        public const string SpecialCharactersNotAllowed = "Special characters are not allowed";
        public const string InvalidUrl = "Url is invalid.";
        public const string UserDoesNotExist = "You don\'t have access to appliance servicing. Please contant administrator.";
        public const string UsersAccountDeactivated = "Your access to appliance servicing has been de-activiated. Please contant administrator.";
        public const string LoginRslManager = "Please use the login from rsl manager.";
        public const string ErrorDocumentUpload = "Error occurred while uploading the document.Please try again.";
        public const string GasEngNotFound = "Gas engineer not found.";
        public const string IncorrectDateInterval = "From date should not be later than to date";
        public const string InvalidJobSheetNumber = "Invalid Job Sheet Number";
        public const string InvalidMobile = "Invalid Mobile number.";
        public const string AppointmentSavedSMSError = "Appointment saved but unable to send SMS due to : ";
        public const string SelectAPDF = "Upload file must be a pdf";
        public const string SelectAFile = "Please select a file";

        public const string FileSizeError = "File is too large.Max size should be less then 20mb";
        public const string SuccessMessage = "Information has been saved successfully";
        public const string InvalidDate = "Removed Date should be greater than added date";
        public const string InvalidDateGreaterThanCurrent = "Removed date should not be less than current date.";
        public const string InvalidAsbestosLevel = "Abestos Level not available";
        public const string InvalidAsbestosElement = "Asbestos Element does not match with existing elements";
        public const string IvalidRiskId = "Asbestos RiskID is not present";
        public const string RefurbishmentError = "Information is not added as refurbishment date already exists";
        public const string SelectTypeValidationError = "Please select type";
        public const string SelectTitleValidationError = "Please select title";
        public static string SelectCategoryValidationError = "Please select Category";
        public static string SelectDocumentDateValidationError = "Please select Document Date";
        public static string SelectExpiryDateValidationError = "Please select Expiry Date";
        public const string DateValidationError = "Expiry Date should be greater then Document Date";
        public const string InvalidEmail = "Invalid Email address.";
        public const string DirectoryDoesNotExist = "This path {0} does not exist on hard drive. Please create this path on drive.";
        public const string DownloadDirectoryDoesNotExist = "Directory/File does not exist on hard drive. Please create this path on drive.";
        public const string DocumentUploadPathKeyDoesNotExist = "Document upload path key does not exist in web.config. Please insert the document upload path key in " +
        "web.config under app settings";
        public const string FileDoesNotExist = "This file {0} does not exist on hard drive.";
        public const string InvalidDocumentType = "Invlaid document type.";
        public const string InvalidImageType = "Invlaid image type.";
        public const string SelectDocument = "Please select the document";
        public const string FileUploadedSuccessfully = "File is uploaded successfully.";
        public const string DocumentAddedSuccessfully = "Document is added successfully.";
        public const string InvalidPropertyId = "Property Id is invalid.";
        public const string InvalidBlockId = "Block Id is invalid.";
        public const string InvalidSchemeId = "Scheme Id is invalid.";
        public const string FieldRestrictionCheck = "Please add atleast one field to save the restriction record.";
        public const string InvalidPropertyIdQuerypublic = "Invalid Property Id in query public const string.";
        public const string PropertyGasInfoUpdatedSuccessfuly = "Property Gas Info Updated successfully";
        public const string DefectSavedSuccessfuly = "Defect Saved successfully";
        public const string CategoryAddedSuccessfuly = "Category Added successfully";
        public const string SelectAppliance = "Please select value from Appliance List";
        public const string SelectCategory = "Please select value from Category List";
        public const string SelectBoiler = "Please select value from Boilers List";
        public const string SelectDefectDate = "Select a valid date";
        public const string SerialNumber = "Please enter Serial Number";
        public const string PhotographTitle = "Please enter photograph title";
        public const string PropertyIdDoesNotExist = "No property id given in the url";
        public const string SomeServerError = "There is some problem while fetching the data";
        public const string ErrorFetchingCustomerInfo = "There was some issue while fetching the customer infomation";
        public const string NoDataFoundCase = "No Data Found";
        public const string RenewalDateGreaterThanIssueDate = "Renewal Date must be greater than Issue Date";
        public const string notValidDateFormat = "Please enter valid date.";
        public const string itemAddedSuccessfuly = "Item Added successfully.";
        public const string ItemUpdatedSuccessfuly = "Item Updated successfully.";
        public const string notValidNote = "Please enter note.";
        public const string fillMandatory = "Please fill mandatory field.";
        public const string SelectLocation = "Select Location.";
        public const string SelectFuel = "Select Fuel.";
        public const string SelectType = "Select Type.";
        public const string SelectMake = "Select Make.";
        public const string ModelIsEmpty = "Model should not be empty.";
        public const string SelectFlueType = "Select Flue Type.";
        public const string InstalledDateIsEmpty = "Original Install Date should not be empty.";
        public const string ApplianceSavedSuccessfully = "Appliance is saved successfully";
        public const string SelectActionDate = "Please select date.";
        public const string SelectActionType = "Please select type.";
        public const string SelectActionStatus = "Please select status.";
        public const string SelectAction = "Please select action.";
        public const string SelectActionLetter = "Please select letter.";
        public const string SelectActionNotes = "Please enter notes.";
        public const string SelectActionLetterDoc = "Please add letter or upload any document in the list.";
        public const string SaveActvitySuccessfully = "Activity is saved successfully.";
        public const string LetterAlreadyExist = "You have already added this letter.";
        public const string duplicateAppointmentTime = "Another appointment already exist in selected Start/End time.";
        public const string NoLettersFound = "No letter found.";
        public const string NoDocumentsFound = "No document found.";
        public const string SelectLetter = "Please select the letter.";
        public const string StandardLetterCreated = "Standard Letter template successfully created.";
        public const string StandardLetterUpdated = "Standard Letter template successfully updated.";
        public const string StandardLetterDeleted = "Standard Letter template successfully deleted.";
        public const string StandardLetterNotCreated = "Unable to create Standard Letter template.";
        public const string StandardLetterNoStatus = "Please select the status of the Standard Letter template.";
        public const string StandardLetterNoAction = "Please select the action of the Standard Letter template.";
        public const string StandardLetterNoTitle = "Please enter the title of the Standard Letter template.";
        public const string StandardLetterNoCode = "Please enter the code of the Standard Letter template.";
        public const string StandardLetterCodeNotNumeric = "The Letter code shall only be numeric.";
        public const string StandardLetterNoBody = "Please enter the body of the Standard Letter template.";
        public const string SavedLetterNoLetterId = "Error. Could not find letter.";
        public const string SavedLetterNoBody = "Please enter the body of the letter.";
        public const string SavedLetterNoTitle = "Please enter the title of the letter.";
        public const string SavedLetterNoSignOFfLookupCode = "Please select the Sign Off.";
        public const string BlockNameNotFound = "Please enter block name.";
        public const string BlockAddressNotFound = "Please enter block address1.";
        public const string BlockTownCityNotFound = "Please enter block town/city.";
        public const string BlockSavedSuccessfully = "Block saved successfully.";
        public const string RestrictionSavedSuccessfully = "Restriction saved successfully.";
        public const string BlockNotSaved = "Block could not saved.";
        public const string RestrictionNotSaved = "Failed to save restriction.";
        public const string ProvisionSavedSuccessfully = "Provision saved successfully.";
        public const string ProvisionNotSaved = "Provision could not saved.";
        public const string ProvisionUpdatedSuccessfully = "Provision updated successfully.";
        public const string ProvisionNotUpdated = "Provision could not updates.";
        public const string ActionSavedSuccessfuly = "Action saved successfully";
        public const string StatusSavedSuccessfuly = "Status saved successfully";
        public const string SaveRightsSuccessfuly = "Rights saved successfully";
        public const string StatusUpdatedSuccessfuly = "Status updated successfully";
        public const string EditActionSuccessful = "Action edited successfully";
        public const string statusLength = "Status length is too large";
        public const string statusTitle = "Status title is required";
        public const string ActionError = "you can add action only under status";
        public const string UserSavedSuccessfuly = "Customer info saved successfully";
        public const string UserUpdatedSuccessfuly = "User Updated successfully";
        public const string UserDeletedSuccessfuly = "User Deleted successfully";
        public const string UserEditedSuccessful = "User Edited successfully";
        public const string NoUserFind = "No User Find";
        public const string UserGSREnotExists = "GSRE No not exists";
        public const string NoEmployeFound = "No Employe Found";
        public const string chkBoxListProperty = "Please select atleast one inspection type";
        public const string userAlreadyExists = "User already exists";
        public const string UserNameNotValid = "User name is not valid";
        public const string SaveAppointmentSuccessfully = "Appointment is saved successfully.";
        public const string SaveAmendAppointmentSuccessfully = "Appointment is amended successfully.";
        public const string AppointmentTime = "Appointment start time shall always be less than end time.";
        public const string Cp12DocumentNotAvailable = "Cp12 Document is not available";
        public const string InspectionDocumentNotAvailable = "Inspection Document is not available";
        public const string NoOperativesExistWithInTime = "There are currently no operatives available to attend the appliance inspection prior to the certifica" +
        "te expiry date. Either remove the Patch and/or Expiry Date filter and click the\'Refresh button";
        public const string AppointmentSavedEmailError = "Appointment saved but unable to send email due to : ";
        public const string noTenantInformationFound = "No Tenant Information Found.";
        public const string NoTenantFoundToEmail = "No tenant found to send email.";
        public const string UnableToSendEmailToTenant = "Unable to send email to one or more tenants due to wrong Email Address";
        public const string EmailSentSucessfully = "Certificate has been emailed sucessfully.";
        public const string LGSREmailSubject = "Your current LGSR Certificate";
        public const string InvalidStartDate = "Please enter valid Start date.";
        public const string InvalidEndDate = "Please enter valid End date.";
        public const string InvalidStartTime = "Please enter valid Start time.";
        public const string InvalidEndTime = "Please enter valid End time.";
        public const string InvalidStartEndDate = "Start date and End date are not equal.";
        public const string InvalidEndStartTime = "End date/time must be greater than start date/time.";
        public const string SelectFutureDate = "Please select future date/time.";
        public const string SavedSuccessfully = "User info saved Successfully.";
        public const string UserAddedSuccessfully = "User successfully added.";
        public const string FailedToSave = "Unable to save the record. Please try again.";
        public const string InvalidTimeFor = "Invalid time selected for ";
        public const string InvalidFromTime = "Start time must be smaller than End time for ";
        public const string InvalidSelectedTimeFor = "Selected time for ";
        public const string ExtendedHourAlreadyExist = "There is already slot reserved in \'Out of Hours Management\' for the selected date.";
        public const string InvalidSelectedTimeForReason = " is part of the employees \'Out of Hours Management\', please select a different time";
        public const string InvalidSelectedOutOfHours = "Selected time is already reserved as \'Out of Hours Management\', please select a different time.";
        public const string TimeReservedInCoreWorkingHours = "The selected time is part of the employees core working hours, please select a different time.";
        public const string SaveSchemeSuccessfuly = "Scheme Saved successfuly";
        public const string MandatoryField = "All fields are mandatory";
        public const string InvalidQueryString = "Invalid query string.";
        public const string DistanceError = "Unable to get distance..";
        public const string InvalidCheracter = "Special characters are not allowed in search criteria.";
        public const string FileNotExist = "This file does not exist on hard drive.";
        public const string NotValidPATDateFormat = "Please enter valid date for PAT Testing.";
        public const string NotValidMaintenanceDateFormat = "Please enter valid date for Contract Commencment.";
        public const string NotValidMEDateFormat = "Please enter valid date for M&E Servicing.";

        public const string EnterTestingCycle = "Please enter Testing Cycle for PAT Testing.";
        public const string EnterMaintenanceCycle = "Please enter Contract Period as number.";
        public const string EnterCyclicalCycle = "Please enter Cycle as number.";
        public const string EnterServiceCycle = "Please enter Service Cycle for M&E Servicing.";
        public const string ValidDateFormat = "Please enter valid ";
        public const string WeekendDate = "Appointment can not be arranged for weekend.";
        public const string NotValidPurchasedCostFormat = "Please enter valid Installed/Purchased Cost upto 2 decimals.";
        public const string NotValidInstalledDateFormat = "Please enter valid Installation date.";
        public const string NotValidReplacementDueDateFormat = "Please enter valid Replacement Due date.";
        public const string NotValidLastReplacedDateDateFormat = "Please enter valid Last Replacement date.";
        public const string SaveDetectorSuccessfully  = "Detector is saved successfully.";
        public const string FillMandatory = "Please fill mandatory field.";
        #endregion 

        #region M&E Scheduling

        public const string ComponentTradeDoesNotExist = "Trade does not exist against this component. Please add trade against this component.";
        public const string OperativesNotFound = "Operatives are not found against this component trade. Please select the different date or add operatives against these trades";
        public const string SaveAppointmentFailed = "Some error occurred in saving appointments";
        public const string AppointmentConfirmationFailed = "Some error occurred in confirming appointments";
        public const string RearrangeAppointmentFailed = "Some error occurred in Rearrange appointments";
        public const string InvalidPmo = "Invalid PMO number.";
        public const string InvalidJsn = "Invalid Job Sheet Number.";
        public const string SelectedDateIsInPast = "Selected date is in past. This should be today's date or date in future.";
        public const string InvalidJobSheetAndPmoQueryString = "Invalid Job Sheet and PMO in query string.";
        public const string NotesUpdatedSuccessfully = "Notes updated successfully.";
        public const string ProblemUpdatingNotes = "Problem updating notes.";
        public const string EnterReason = "Please enter reason for cancellation.";
        public const string AppointmentCancelledSuccessfully = "Appointment cancelled successfully.";
        public const string AppointmentCancelledFailed = "Appointment cancellation has been failed. Please try again.";
        public const string UnableToRearrangeTheAppointment = "System is unable to re-arrange the appointment. Please refresh the page & try again.";
        public const string UnableToSaveChange = "Unable to save changes.";
        public const string UnableToRemoveTrade = "System is unable to remove the trade. Please refresh the page & try again.";
        public const string UnableToScheduleTheAppointment = "System is unable to schedule the appointment(s). Please refresh the page & try again.";
        public const string AppoinemtsScheduledSuccessfully = "Appointment has been scheduled against all trades";
        public const string MeTradDuration = "Please add at least one trade/duration.";
        public const string TradeAlreadyAdded = "The Operative has already been added.";
        public const string InvalidJournalId = "Invalid Journal Id.";
        public const string MaxLengthExceed = "Please reduce length of notes. Maximum 1000 characters are allowed.";
        public const string AppointmentArrangedSavedEmailError = "Appointment arranged but unable to send email due to : ";
        public const string InvalidWorksRequired = "Please enter works required description.";
        public const string MaxLengthExceedWorksRequired = "Please reduce length of works required. Maximum 1000 characters are allowed.";
        public const string AppointmentCancelledEmailError = "Appointment Cancelled Successfully but unable to send email due to : ";
        #endregion

        #region "Assign to Contractor"
        public const string serviceRequiredCount = "Please add at least one service required.";
        public const string ContractStartEndDateNotAvailable = "Contract start/end date is not available in suppliers scope area.";
        public const string InvalidContractBetweenDate = "Service contract start/end date must be between start/end date available in suppliers scope area.";
        public const string InvalidContractStartEndDate = "Invalid contract start/end date.";
        public const string AssignedToContractor = "Work successfully assigned to Contractor.";
        public const string EmailToContractor = "Email not sent to contractor.";
        public const string AssignedToContractorFailed = "Assign work to contractor has not successful.";
        #endregion

        #region Void Management

        #region British Gas Notification
        public const string RequiredArrangeGasOrElectricCheck = "Please select the option to arrange Gas Or Electric check.";
        public const string RequiredFwAddress1 = "Please enter Address1.";
        public const string RequiredPostCode = "Please select Postcode.";
        public const string RequiredOccupancyCeaseDate = "Please select Occupancy Cease Date.";
        public const string RequireValidOccupancyCeaseDate = "Please enter valid Occupancy Cease Date.";
        public const string ErrorSavingBritishGasNotification = "System is unable to save the record.";
        public const string RequiredGasMeterTypeOrElectricMeterType = "Please select the Gas Meter Type or Electric Meter Type.";
        public const string MeterRedadingShouldBeDigitsOnly = "Meter reading should be digits only.";

        public const string RequiredTenantName = "Please enter new tenant name.";
        public const string RequiredDob = "Please enter new date of birth.";
        public const string RequiredOccupancyDate = "Please enter new occupancy date.";
        public const string RequiredTelephoneOrMobile = "Please enter telephone OR mobile.";

        #endregion

        public const string SelectSupervisor = "Please select Supervisor.";
        public const string RequiredWorksUpdatedSuccessfuly = "Required works updated successfully.";
        public const string RequiredWorksCanceledSuccessfuly = "Required works has been canceled successfully.";
        public const string SelectRequiredWorks = "Please select any works required";
        public const string AppointmentAlreadyScheduled = "An appointment already scheduled for selected works required.";
        public const string DuplicateNHBCError = "NHBC warranty already exists, Please enter different warranty type";
        public const string AppointmentScheduledSuccessfully = "The appointment has been scheduled successfully!";
        public const string AppointmentReScheduledSuccessfully = "The appointment has been Rescheduled successfully!";
        public const string AppointmentCancelMessage = "Are you sure you want to cancel this appointment?";
        public const string MultipleWorkAppointmentCancelMessage = "There are void works also arranged at the same time as the selected works, and the associated void works will also be cancelled.";
        public const string PaintPacksSaveSuccessfuly = "Paint Packs saved successfully.";
        public const string TenantToCompleteFailed = "Some error occurred in updating Tenant To Complete";
        public const string TenantToCompleteSuccess = "Tenant To Complete has been updated successfully.";
        public const string RequiredWorksUpdatedFailed = "Some error occurred in updating Required works.";
        public const string SelectSupplier = "Please select Supplier.";
        public const string NotValidDeliveryDateFormat = "Please enter valid Delivery date.";
        #endregion

        #region PO Management
        public const string AcceptWork = "Work is successfully Accepted by contractor";
        public const string RejectWork = "Work is successfully Rejected by contractor";
        #endregion

        #region work completion popup
        public const string RepairAlreadyAdded = "The Repair has already been added.";
        public const string InvalidNoEntryDate = "Please select date";
        public const string AddAtLeastRepair = "Please Select at least one repair.";
        public const string InvalidCompletedDate  = "Please select Completed date.";
        public const string TimenotgreaterthancurrTime = "Time should not be greater than current time.";
        public const string InvalidNoEntrydate = "Please select valid No Entry Date.";
        public const string EnterFollowOnNotes = "Please Enter follow on notes.";
        public const string ErrorOccueInSavingData = "An error Occur while saving data.";
        public const string dataSavedSuccessfully = "Data Saved Successfully.";
        public const string DateErrorOnNoEntryPopup = "Please Select Date.";
        #endregion

        #region supervisor not found errors
        public const string GasManagerNotFound = "There are currently no supervisors with the trades Gas (Full) or Gas (Part) selected in their employee record. Please notify HR!";
        public const string ElectricManagerNotFound = "There are currently no supervisors with the trades Electric (Full) or Electric (Part) selected in their employee record. Please notify HR!";
        #endregion

        public const string InvalidPageNumber = "Invalid Page Number.";

        #region "Reports"
        public const string SelectIncludedProperty = "Please select property from included property list.";
        public const string SelectExcludedProperty = "Please select property from excluded property list.";
        #endregion

    }
}
