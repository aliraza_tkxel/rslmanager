﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PDR_Utilities.Constants
{

    public class ApplicationConstants
    {

        #region "General"
        /// <summary>
        /// This variable shall be used to as key in a list and that list shall be used to highlight the menu items
        /// </summary>
        /// <remarks></remarks>
        public const string LinkButtonType = "LinkButtonType";
        /// <summary>
        /// This variable shall be used to as key in a list and that list shall be used to highlight the menu items
        /// <remarks></remarks>
        public const string UpdatePanelType = "UpdatePanelType";
        public const string NotAvailable = "N/A";
        public const string DefaultDropDownDataValueField = "id";
        public const string DefaultDropDownDataTextField = "title";
        public const string DefaultValue = "-1";
        public const int NoneValue = -1;
        public const string ManagerUserType = "Manager";

        public const string DocumentWord = "Document";
        public const string LetterWord = "Letter";
        public const string LetterPrefix = "SL_";
        public const string DocPrefix = "Doc_";
        public const string ContactEmailDetailsDt = "ContactEmailDetails";

        #endregion

        #region "Master Layout"

        public const string ConWebBackgroundColor = "#e6e6e6";

        public const string ConWebBackgroundProperty = "background-color";
        public const string MenuCol = "Menu";
        public const string UrlCol = "Url";
        public const string MenuIdCol = "MenuId";
        public const string PageFileNameCol = "PageFileName";
        public const int NoPageSelected = -1;

        public const int PageFirstLevel = 1;
        public const string ServicingMenu = "Servicing";
        public const string ResourcesMenu = "Resources";
        public const string PropertyModuleMenu = "Search";
        public const string RepairsMenu = "Repairs";
        public const string PlannedMenu = "Planned";
        public const string Admin = "Admin";
        public const string MultipleMenus = "";
        public const string AccessGrantedModulesDt = "AccessGrantedModulesDt";
        public const string AccessGrantedMenusDt = "AccessGrantedMenusDt";
        public const string AccessGrantedPagesDt = "AccessGrantedPagesDt";

        public const string RandomPageDt = "RandomPageDt";
        public const string UserDt = "UserDt";

        public const string GrantModulesModuleIdCol = "MODULEID";
        public const string GrantMenuModuleIdCol = "ModuleId";
        public const string GrantMenuMenuIdCol = "MenuId";
        public const string GrantMenuUrlCol = "Url";

        public const string GrantMenuDescriptionCol = "Menu";
        public const string GrantPageParentPageCol = "ParentPage";
        public const string GrantPageAccessLevelCol = "AccessLevel";
        public const string GrantPageQueryStringCol = "PageQueryString";
        public const string GrantPageFileNameCol = "PageFileName";
        public const string GrantPageUrlCol = "PageUrl";
        public const string GrantPageCoreUrlCol = "PageCoreUrl";
        public const string GrantPagePageNameCol = "PageName";
        public const string GrantPageIdCol = "PageId";
        public const string GrantPageMenuIdCol = "MenuId";
        public const string GrantPageModuleIdCol = "ModuleId";

        public const string GrantOrderTextCol = "ORDERTEXT";
        public const string RandomParentPageCol = "ParentPage";
        public const string RandomAccessLevelCol = "AccessLevel";
        public const string RandomQueryStringCol = "PageQueryString";
        public const string RandomFileNameCol = "PageFileName";
        public const string RandomPageNameCol = "PageName";

        public const string RandomPageIdCol = "PageId";
        public const string AdminSchedulingPage = "Admin Calendar";

        #endregion

        #region "Dashboard"

        #endregion

        #region "CSS"
        public const string TabClickedCssClass = "TabClicked";
        public const string TabInitialCssClass = "TabInitial";
        #endregion

        #region "Scheduling"

        public const string IsPhotoGraphExist = "IsPhotoGraphExist";
        public const string IsNotesExist = "IsNotesExist";

        public const  string DropDownDefaultValues  = "-1";
        public const string DropDownAllValue = "-2";
        public const string NHBCWarrantyText = "NHBC Cover";
        public const string MEServicing = "M&E Servicing";
        public const string MaintenanceItem = "Maintenance Item";
        public const string CyclicMaintenance = "Cyclic Maintenance";
        public const string MaintenanceItemWork = "Maintenance Item Work";
        public const string MEServicingWork = "M&E Servicing Work";
        public const int AllSelectedValue = -1;
        public const Boolean DistanceError = true;
        public const string UpdateCustomerDetails = "Update customer details";
        public const string UpdateNotes = "Update Notes";
        public const string SaveChanges = "Save Changes";
        public const string NotSaved = "0";
        public const int Saved = 1;
        public const int Cancelled = 1;
        public const string AvailableOperatives = "AvailableOperatives";
        public const string OperativesLeaves = "OperativesLeaves";
        public const string OperativesAppointments = "OperativesAppointments";
        public const string TempAppointmentGridControlId = "grdTempAppointments";
        public const string ConfirmAppointmentGridControlId = "grdConfirmAppointments";
        public const string RemovedButtonPanelControlId = "pnlRemovedButtons";
        public const string CompleteTradeGridControlId = "grdCompleteTrades";
        public const string ConfirmTradeGridControlId = "grdConfirmTrades";
        public const string RemovedTradeGridControlId = "grdRemovedTrades";
        public const string TempTradeGridControlId = "grdTempTrades";
        public const string AppointmentStartDateControlId = "txtStartDate";
        public const string StartDateLabelControlId = "lblStartDate";
        public const string CalDateControlId = "imgCalDate";
        public const string EditImageButtonControlId = "imgBtnEdit";
        public const string DoneImageButtonControlId = "imgBtnDone";
        public const string DurationLabelControlId = "lblDuration";
        public const string DurationHiddenControlId = "hdnDuration";
        public const string DurationDropdownControlId = "ddlDuration";
        public const string DurationPanelControlId = "pnlDuration";
        public const string CalExtDateControlId = "imgCalExtDate";
        public const string GoControlId = "btnGo";
        public const int NotPending = 0;
        public const int Pending = 1;
        public const string JobsheetCurrentColumn = "Row";
        public const string PmoColumn = "PMO";
        public const string ItemNameColumn = "ItemName";
        public const string TradesColumn = "Trade";
        public const string JsnColumn = "JSN";
        public const string JsnSearchColumn = "JSNSearch";
        public const string StatusColumn = "Status";
        public const string OperativeNameColumn = "OperativeName";
        public const string EmailColumn = "Email";
        public const string WorksJournalIdColumn = "WorksJournalId";
        public const string MobileColumn = "Mobile";
        public const string DDNumberColumn = "WORKDD";
        public const string OperativeColumn = "Operative";
        public const string StartTimeColumn = "StartTime";
        public const string CreationDateCol = "CreationDate";
        public const string EndTimeColumn = "EndTime";
        public const string DurationsColumn = "Duration";
        public const string TotalDurationColumn = "TotalDuration";
        public const string StartDateColumn = "StartDate";
        public const string EndDateColumn = "EndDate";
        public const string CustomerNotesColumn = "CustomerNotes";
        public const string JobsheetNotesColumn = "JobsheetNotes";
        public const string MsatTypeColumn = "MsatType";
        public const string IsMiscAppointmentColumn = "IsMiscAppointment";
        public const string PropertyIdColumn = "PropertyId";
        public const string AppointmentIdColumn = "AppointmentId";
        public const string AppointmentArrangedTab = "aptArranged";
        public const string AppointmentToBeArrangedTab = "aptToBeArranged";
        public const string ConfirmedAppointementGridsIdConstant = "c";
        public const string RemovedAppointementGridsIdConstant = "r";
        public const string CompleteAppointementGridsIdConstant = "comp";
        public const string StatusToBeArranged = "To be Arranged";
        public const string StatusApproved = "Approved";
        public const string StatusPending = "Pending";
        public const string StatusRemovedFromScheduling = "Removed from scheduling";
        public const string StatusArranged = "Arranged";
        public const string StatusInProgress = "inporgress";
        public const string StatusComplete = "complete";
        public const string StatusInterimComplete = "Complete";
        public const string StatusInterimNotStarted = "NotStarted";
        public const string StatusInterimInProgress = "InProgress";
        public const string RiskInfo = "RiskInfo";
        public const string VulnerabilityInfo = "VulnerabilityInfo";
        public const string InterimStatusColumn = "InterimStatus";
        public const string PropertyInfoByJournalId = "PropertyInfoByJournalId";
        public const string MeAppointmentsDetail = "MeAppointmentsDetail";
        public const string PropertyDetail = "PropertyDetail";
        public const string NonSelected = "Non selected";
        public const string RecordedWorkDt = "RecordedWorkDt";
        public const string RequiredWorkDt = "RequiredWorkDt";
        public const string SaniClean = "Sani Clean";
        public const string StandardVoidWorks = "Standard Void Works";
        public const decimal DefaultSaniCleanDuration = 2;
        public const decimal DefaultVoidWorksDuration = 1;
        public const string WeekDayCol = "Name";
        #endregion

        #region "Reports"

        public const string MultipleSchemeTitle = "Multiple Schemes Selected";
        public const string GeneralJournalServiceCharge = "General Journal";
        public const string PurchaseItemServiceCharge = "Purchase Item";

        #endregion

        #region "Drop Down Lists"

        /// <summary>
        /// These two constant are use to add the "Please Select" Drop down item in a drop down list.
        /// </summary>
        /// <remarks>This is a good practice to avoid spelling mistakes etc.</remarks>
        public const int DropDownDefaultValue = -1;

        public const string DropDwonDefaultText = "Please Select";
        public const string ddlDefaultDataTextField = "description";

        public const string ddlDefaultDataValueField = "id";

        public const string noContactFound = "No contact found";

        public const string noAttributeFound = "No attribute found";

        #endregion

        #region  SchemeBlock
        public const string Id = "id";
        public const string DocumentId = "documentId";
        public const string RequestType = "requestType";
        public const string Type = "type";
        public const string Scheme = "scheme";
        public const string Block = "block";
        public const string Source = "src";
        public const string SchemeHeading = "Scheme Set Up:"; 
        #endregion

        #region  Development
        public const string AmendDevelopment = "Amend Development";
        public const string DevelopmentDetailDt = "DevelopmentDetailDt";
        public const string DevelopmentFundingDt = "DevelopmentFundingDt";
        public const string DevelopmentBlocksDt = "DevelopmentBlocksDt";
        public const string DevelopmentDocumentsDt = "DevelopmentDocumentsDt";
        public const string DevelopmentDocumentIdCol = "DocumentId";
        public const string DocumentTitleCol = "Title";
        public const string DocumentExpireCol = "Expire";
        public const string DocumentFileTypeCol = "FileType";
        public const string PdfFiletype = "PDF";
        public const string DocFiletype = "Doc";
        public const string ExcelFiletype = "Excel";
        public const string ImageFiletype = "Image";
        public const string DevelopmentNameCol = "DEVELOPMENTNAME";
        public const string PhaseIdCol = "PHASEID";
        public const string DocumentIdCol = "DocumentId";
        public const string PatchIdCol = "PATCHID";
        public const string Address1Col = "ADDRESS1";
        public const string Address2Col = "ADDRESS2";
        public const string TownCol = "TOWN";
        public const string CountyCol = "COUNTY";
        public const string PostcodeCol = "PostCode";
        public const string ArchitectCol = "Architect";
        public const string ProjectManagerCol = "PROJECTMANAGER";
        public const string DevelopmentTypeCol = "DEVELOPMENTTYPE";
        public const string DevelopmentStatusCol = "STATUS";
        public const string LandValueCol = "LandValue";
        public const string LandValueDateCol = "ValueDate";//changed
        public const string LandPurchasePriceCol = "LandPurchasePrice";
        public const string PurchaseDateCol = "PurchaseDate";
        public const string GrantAmountCol = "GrantAmount";
        public const string BorrowedAmountCol = "BorrowedAmount";
        public const string OutlinePlanningApplicationCol = "OutlinePlanningApplication";
        public const string DetailedPlanningApplicationCol = "DetailedPlanningApplication";
        public const string OutlinePlanningApprovalCol = "OutlinePlanningApproval";
        public const string DetailedPlanningApprovalCol = "DetailedPlanningApproval";
        public const string numberOfUnitCol = "NUMBEROFUNITS";
        public const string FundingSourceCol = "FundingSource";
        public const string DevelopmentFundingIdCol = "DEVELOPMENTFUNDINGID";
        public const string FundingAuthorityIdCol = "FUNDINGAUTHORITYID";
        public const string FundingAuthorityCol = "FUNDINGAUTHORITY";
        public const string DevelopmentGrantAmountCol = "FUNDGRANTAMOUNT";
        public const string LocalAuthorityCol = "LOCALAUTHORITY";
        public const string CompanyIdCol = "COMPANYID";

        #endregion

        #region Provision Datatable
        public const string Provisions = "Provisions";

        #endregion

        #region "attributes Data table"
        public const string Attributes = "PropertyAttributes";
        public const string AttributeValues = "PropertyAttributeValues";
        public const string AttributesImage = "PropertyImage";
        public const string Parameters = "Parameters";
        public const string ParameterValues = "Parametervalues";
        public const string PreInsertedValues = "preinsertedvalues";
        public const string LastReplaced = "lastReplaced";
        public const string ReplacementDue = "replacementDue";
        public const string PlannedComponent = "plannedComponent";
        public const string MST = "MST";
        public const string CyclicServices = "CyclicServices";

        public const bool valueLifeCycle = false;
        public const bool isDdlChangeEvent = false;
        public const int plannedComponentId = 0;
        public const int attributrTypeId = 0;
        public const bool isMapped = false;
        public const int isLifeCycleMapped = 0;
        public const int isAccountingMapped = 0;

        public const bool isWorkSatisfactory = false;
        #endregion

        #region "Prefix"
        public const string Tr = "tr";
        public const string Td1 = "td1_";
        public const string Td2 = "td2_";
        public const string lblDdl = "lblDdl";
        public const string lblChkBox = "lblChkBox";
        public const string lblDate = "lblDate";
        public const string extender = "extender";
        public const string lblRadioBtn = "lblRadioBtn";
        public const string lblTextBox = "lblTextBox";
        public const string textBoxStyle = "dynamicTextBox";
        public const string ddlStyle = "dynamicDdl";
        public const string dynamicRadioButton = "dynamicRadioButton";
        public const string lblCycle = "lblCycle";

        public const string lblInspected = "lblInspected";
        #endregion

        #region "Control ID's"
        public const string ddlParameter = "ddlParameter";
        public const string chkList = "chkList";
        public const string txtDate = "txtDate";
        public const string rdBtn = "rdBtn";
        public const string txt = "txt";
        public const string txtArea = "txtArea";
        public const string lblLifeCycle = "lblLifeCycle";
        public const string conditionSatisfactory = "Satisfactory";
        public const string conditionUnsatisfactory = "Unsatisfactory";
        public const string conditionPotentiallyUnsatisfactory = "Potentially Unsatisfactory";
        public const string hdnLifeCycle = "hdnLifeCycle";
        public const string lastRewired = "lastRewired";
        public const string rewiredDue = "rewiredDue";
        public const string upgradeDue = "upgradeDue";
        public const string ltrlReadOnly = "ltrlReadOnly";

        public const string lastUpdrade = "lastUpdrade";

        #endregion

        #region "Assign Work To Contractor"

        public const string EmailSubject = " has been Assigned";
        public const string EmailSubject_SB = "Purchase Order for BHG Works";
        public const string NoExpenditureSetup = "No Expenditures are Setup";
        public const string AssignToContractorStatus = "Assigned To Contractor";
        public const string CyclicalEmailSubject = " Confirmation of Cyclical Services Order:";
        public const string UrgentAsbestosEmailSubject = "Urgent Asbestos Issue Raised";

        #region "Data table names for email details"
        public const string ContractorWorkDt = "ContractorWork";
        public const string ContractorWorkDetailDt = "ContractorWorkDetail";
        public const string ContractorDetailDt = "ContractorDetailDt";
        public const string ContractorDetailsDt = "ContractorDetails";
        public const string PropertyDetailsDt = "PropertyDetails";
        public const string TenantDetailsDt = "TenantDetails";
        public const string TenantRiskDetailsDt = "TenantRiskDetails";

        public const string TenantVulnerabilityDetailsDt = "TenantVulnerabilityDetails";
        public const string PurchaseOrderDetailsDt = "PurchaseOrderDetails";
        public const string AppointmentCSVDetailsDt = "AppointmentCSVDetails";
        public const string AppointmentSupplierDetailsDt = "AppointmentSupplierDetailsDt";
        public const string PurchaseItemDetailsDt = "PurchaseItemDetailsDt";
        #endregion

        #region "Work Required Data Table column names"

        public const string PdrContractorIdCol = "PDRContractorId";
        public const string ContractorIdCol = "ContractorId";
        public const string ContractorNameCol = "ContractorName";
        public const string ContactIdCol = "ContactId";
        public const string EstimateCol = "Estimate";
        public const string EstimateRefCol = "EstimateRef";
        public const string ContractStartDateCol = "ContractStartDate";
        public const string ContractEndDateCol = "ContractEndDate";

        public const string WorkDetailIdCol = "WorkDetailId";
        public const string ServiceRequiredCol = "ServiceRequired";
        public const string NetCostCol = "NetCost";
        public const string VatTypeCol = "VatType";
        public const string VatCol = "VAT";
        public const string GrossCol = "GROSS";
        public const string PIStatusCol = "PIStatus";
        public const string ExpenditureIdCol = "ExpenditureId";
        public const string BudgetHeadIdCol = "BudgetHeadId";
        public const string CostCenterIdCol = "CostCenterId";        

        public const int MaxStringLegthWordRequired = 4000;
        #endregion

        #endregion

        #region void

        public const string VoidMsatType = "Void";
        public static string[] appointmentTimeHours = { "08", "09", "10", "11", "12", "13", "14", "15", "16", "17" };
        public static string[] appointmentTimeMinutes = { "00", "15", "30", "45" };
        public const string PaintPackHeader = "PaintPackHeader";
        public const string PaintPackDetail = "PaintPackDetail";
        public const string PaintStatus = "PaintStatus";
        public const string PaintPackSupplier = "PaintPackSupplier";
        public const string GasElectricChecksAppointment = "Gas/Electric Checks";
        public const string MdlArrangeInspectionPopup = "mdlpopupInspection";
        public const string MdlAskForGasNotificationPopup = "mdlpopupAskForGasNotification";
        public const string MdlBritishNotificationMessagePopup = "mdlBritishNotificationMessage";
        public const string MdlStage1Notification = "mdlStage1Notification";
        public const string MdlStage2Notification = "mdlStage2Notification";
        public const string MdlStage3Notification = "mdlStage3Notification";


        public const string PendingTermination = "pt";
        public const string ReletDue = "rd";
        public const string NoEntry = "ne";
        public const string AvailableProperties = "ap";
        public const string NoEntryHeading = "No Entry";
        public const string CP12StatusReport = "CP12 Status Report";
        public const string PendingTerminationHeading = "Pending Termination";
        public const string ReletDueHeading = "Relets due in next 7 days";
        public const string AvailablePropertiesHeading = "Available Properties";
        public const string RearrangeButtonText = "Rearrange";
        public const string SaveButtonText = "Save";
        public const string CoreHoursDataTable = "CoreHoursDataTable";
        public const string EmpCoreHoursDataTable = "EmpCoreHoursDataTable";
        public const string OutOfHoursDataTable = "OutOfHoursDataTable";
        public const string OutOfHoursTypeDataTable = "OutOfHoursTypeDataTable";
        #endregion

        #region "Maintenance"

        #region Contractor and PO
        public const string ContractorDetailsForContractorsAcceptPOPage = "contractorDetails";
        public const string PropertyRiskDataTableForContractorsAcceptPOPage = "propertyRiskDataTable";
        public const string VulnerabalityDetailsForContractorsAcceptPOPage = "vulnerabalityDetails";
        public const string AsbestosDetailsForContractorsAcceptPOPage = "asbestosDetails";
        public const string ReportedFaultVatCostForContractorsAcceptPOPage = "reportedFaultVatCost";
        public const string PoStatusForUpdatePoPage = "PoStatusForUpdatePoPage";
        public const string RepairSearchResult = "RepairSearchResult";
        public const string RepairDataTable = "RepairDataTable";
        #endregion
        public const string MaintenanceStatusNoEntries = "1";
        public const string MaintenanceStatusOverdueServices = "2";
        public const string MaintenanceStatusAppointmentsArranged = "3";
        public const string MaintenanceStatusAppointmentsToBeArranged = "4";        
        #region Contractor report
        public const string FaultRepairListID = "FaultRepairListID";
        public const string FaultRepairDescription = "Description";
        public const string EmailDetails = "EmailDetails";

        public const string AssignedToContractor = "Assigned To Contractor";

        public const string AcceptedByContractor = "Accepcted by Contractor";
        public const string Assigned = "Assigned";
        #endregion
        #endregion

        #region Defects
        public const string Appliance = "Appliance:";
        public const string DefaultDefectTrade = "Gas (Full)";
        public const string Boiler = "Boiler:";
        #endregion

    }


}
