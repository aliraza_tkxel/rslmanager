﻿using System;
using System.Collections.Generic;
using System.Data;
using PDR_BusinessObject.AssignToContractor;
using PDR_BusinessObject.BritishGas;
using PDR_BusinessObject.CommonSearch;
using PDR_BusinessObject.Development;
using PDR_BusinessObject.Expenditure;
using PDR_BusinessObject.MeAppointment;
using PDR_BusinessObject.MeSearch;
using PDR_BusinessObject.Reports;
using PDR_BusinessObject.RequiredWorks;
using PDR_BusinessObject.TradeAppointment;
using PDR_BusinessObject.Vat;
using PDR_Utilities.Constants;
using Context = System.Web.HttpContext;
using PDR_BusinessObject.VoidAppointment;
using PDR_BusinessObject;
using PDR_BusinessObject.CompletePurchaseOrder;

namespace PDR_Utilities.Managers
{
    public class SessionManager
    {
        /// <summary>
        /// This key shall be used to save the logged in user's name
        /// </summary>
        private const string _UserFullNameKey = "UserFullName";
        public string UserFullName
        {
            get
            {
                if (Context.Current.Session[_UserFullNameKey] == null)
                {
                    return string.Empty;
                }
                else
                {
                    return Convert.ToString(Context.Current.Session[_UserFullNameKey]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_UserFullNameKey] != null)
                    {
                        Context.Current.Session.Remove(_UserFullNameKey);
                    }
                }
                else
                {
                    Context.Current.Session[_UserFullNameKey] = value;
                }

            }
        }

        /// <summary>
        /// This key shall be used to save the logged in user's name
        /// </summary>
        private const string _UserType = "UserType";
        public string UserType
        {
            get
            {
                if (Context.Current.Session[_UserType] == null)
                {
                    return string.Empty;
                }
                else
                {
                    return Convert.ToString(Context.Current.Session[_UserType]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_UserType] != null)
                    {
                        Context.Current.Session.Remove(_UserType);
                    }
                }
                else
                {
                    Context.Current.Session[_UserType] = value;
                }

            }
        }


        /// <summary>
        /// This key will be used to save the employee Id
        /// </summary>
        private const string _EmployeeId = "UserEmployeeId";
        public int EmployeeId
        {
            get
            {
                if (Context.Current.Session[_EmployeeId] == null)
                {
                    return 0;
                }
                else
                {
                    return Convert.ToInt32(Context.Current.Session[_EmployeeId]);
                }
            }
            set
            {
                if (value == 0)
                {
                    if (Context.Current.Session[_EmployeeId] != null)
                    {
                        Context.Current.Session.Remove(_EmployeeId);
                    }
                }
                else
                {
                    Context.Current.Session[_EmployeeId] = value;
                }
            }
        }

        /// <summary>
        /// This key will be used to save the data access of pages
        /// </summary>
        private const string _PageAccessDataSet = "PageAccessDataSet";
        public DataSet PageAccessDataSet
        {
            get
            {
                if (Context.Current.Session[_PageAccessDataSet] == null)
                {
                    return null;
                }
                else
                {
                    return (DataSet)(Context.Current.Session[_PageAccessDataSet]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_PageAccessDataSet] != null)
                    {
                        Context.Current.Session.Remove(_PageAccessDataSet);
                    }
                }
                else
                {
                    Context.Current.Session[_PageAccessDataSet] = value;
                }
            }
        }

        /// <summary>
        /// This key will be used to save the DocumentIds of Development Documents
        /// </summary>
        private const string _DocIdList = "DocIdList";
        public List<int> DocIdList
        {
            get
            {
                if (Context.Current.Session[_DocIdList] == null)
                {
                    return null;
                }
                else
                {
                    return (List<int>)(Context.Current.Session[_DocIdList]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_DocIdList] != null)
                    {
                        Context.Current.Session.Remove(_DocIdList);
                    }
                }
                else
                {
                    Context.Current.Session[_DocIdList] = value;
                }
            }
        }



        #region This key will be used to save the Reports search to access PopUp for Multiple reports
        /// <summary>
        /// This key will be used to save the Reports search to access PopUp for Multiple reports
        /// </summary>
        private const string _ReportBo = "ReportsBo";
        public ReportsBO ReportsBo
        {
            get
            {
                if (Context.Current.Session[_ReportBo] == null)
                {
                    ReportsBO objReportBo = new ReportsBO();
                    return objReportBo;
                }
                else
                {
                    return (ReportsBO)(Context.Current.Session[_ReportBo]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_ReportBo] != null)
                    {
                        Context.Current.Session.Remove(_ReportBo);
                    }
                }
                else
                {
                    Context.Current.Session[_ReportBo] = value;
                }
            }
        }
        #endregion

        /// <summary>
        /// This key will be used to save the selected tree view node id
        /// </summary>
        private const string _TreeItemId = "TreeItemId";
        public int TreeItemId
        {
            get
            {
                if (Context.Current.Session[_TreeItemId] == null)
                {
                    return 0;
                }
                else
                {
                    return Convert.ToInt32(Context.Current.Session[_TreeItemId]);
                }
            }
            set
            {
                if (value == 0)
                {
                    if (Context.Current.Session[_TreeItemId] != null)
                    {
                        Context.Current.Session.Remove(_TreeItemId);
                    }
                }
                else
                {
                    Context.Current.Session[_TreeItemId] = value;
                }
            }
        }

        /// <summary>
        /// This key will be used to save the selected tree view node id
        /// </summary>
        private const string _TreeItemParentId = "TreeItemParentId";
        public int? TreeItemParentId
        {
            get
            {
                if (Context.Current.Session[_TreeItemParentId] == null)
                {
                    return null;
                }
                else
                {
                    return Convert.ToInt32(Context.Current.Session[_TreeItemParentId]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_TreeItemParentId] != null)
                    {
                        Context.Current.Session.Remove(_TreeItemParentId);
                    }
                }
                else
                {
                    Context.Current.Session[_TreeItemParentId] = value;
                }
            }
        }

        /// <summary>
        /// This key will be used to save the selected tree view node Text
        /// </summary>
        /// 
        private const string _FireToolQuantity = "FireToolQuantity";
        public string FireToolQuantity
        {
            get
            {
                if (Context.Current.Session[_FireToolQuantity] == null)
                {
                    return null;
                }
                else
                {
                    return Context.Current.Session[_FireToolQuantity].ToString();
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_FireToolQuantity] != null)
                    {
                        Context.Current.Session.Remove(_FireToolQuantity);
                    }
                }
                else
                {
                    Context.Current.Session[_FireToolQuantity] = value;
                }
            }
        }



        private const string _TreeItemName = "TreeItemName";
        public string TreeItemName
        {
            get
            {
                if (Context.Current.Session[_TreeItemName] == null)
                {
                    return null;
                }
                else
                {
                    return Context.Current.Session[_TreeItemName].ToString();
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_TreeItemName] != null)
                    {
                        Context.Current.Session.Remove(_TreeItemName);
                    }
                }
                else
                {
                    Context.Current.Session[_TreeItemName] = value;
                }
            }
        }

        /// <summary>
        /// This key will be used to save the selected tree view node Text
        /// </summary>
        private const string _SchemeId = "SchemeId";
        public int SchemeId
        {
            get
            {
                if (Context.Current.Session[_SchemeId] == null)
                {
                    return 0;
                }
                else
                {
                    return Convert.ToInt32(Context.Current.Session[_SchemeId].ToString());
                }
            }
            set
            {
                if (value == 0)
                {
                    if (Context.Current.Session[_SchemeId] != null)
                    {
                        Context.Current.Session.Remove(_SchemeId);
                    }
                }
                else
                {
                    Context.Current.Session[_SchemeId] = value;
                }
            }
        }

        /// <summary>
        /// This key will be used to save the selected Block Id
        /// </summary>
        private const string _BlockId = "BlockId";
        public int BlockId
        {
            get
            {
                if (Context.Current.Session[_BlockId] == null)
                {
                    return 0;
                }
                else
                {
                    return Convert.ToInt32(Context.Current.Session[_BlockId].ToString());
                }
            }
            set
            {
                if (value == 0)
                {
                    if (Context.Current.Session[_BlockId] != null)
                    {
                        Context.Current.Session.Remove(_BlockId);
                    }
                }
                else
                {
                    Context.Current.Session[_BlockId] = value;
                }
            }
        }

        /// <summary>
        /// This key will be used to save the selected Scheme Name
        /// </summary>
        private const string _SchemeName = "SchemeName";
        public string SchemeName
        {
            get
            {
                if (Context.Current.Session[_SchemeName] == null)
                {
                    return null;
                }
                else
                {
                    return Context.Current.Session[_SchemeName].ToString();
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_SchemeName] != null)
                    {
                        Context.Current.Session.Remove(_SchemeName);
                    }
                }
                else
                {
                    Context.Current.Session[_SchemeName] = value;
                }
            }
        }

        /// <summary>
        /// This key will be used to save the selected Block Name
        /// </summary>
        private const string _BlockName = "BlockName";
        public string BlockName
        {
            get
            {
                if (Context.Current.Session[_BlockName] == null)
                {
                    return null;
                }
                else
                {
                    return Context.Current.Session[_BlockName].ToString();
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_BlockName] != null)
                    {
                        Context.Current.Session.Remove(_BlockName);
                    }
                }
                else
                {
                    Context.Current.Session[_BlockName] = value;
                }
            }
        }

        /// <summary>
        /// This key will be used to save the data access of pages
        /// </summary>
        private const string _AttributeResultDataSet = "AttributeResultDataSet";
        public DataSet AttributeResultDataSet
        {
            get
            {
                if (Context.Current.Session[_AttributeResultDataSet] == null)
                {
                    return null;
                }
                else
                {
                    return (DataSet)(Context.Current.Session[_AttributeResultDataSet]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_AttributeResultDataSet] != null)
                    {
                        Context.Current.Session.Remove(_AttributeResultDataSet);
                    }
                }
                else
                {
                    Context.Current.Session[_AttributeResultDataSet] = value;
                }
            }
        }

        /// <summary>
        /// ::ST - This key will be used to save the provision result data set
        /// </summary>
        private const string _ProvisionDataSet = "ProvisionDataSet";
        public DataSet ProvisionDataSet
        {
            get
            {
                if (Context.Current.Session[_ProvisionDataSet] == null)
                {
                    return null;
                }
                else
                {
                    return (DataSet)(Context.Current.Session[_ProvisionDataSet]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_ProvisionDataSet] != null)
                    {
                        Context.Current.Session.Remove(_ProvisionDataSet);
                    }
                }
                else
                {
                    Context.Current.Session[_ProvisionDataSet] = value;
                }
            }
        }

        /// <summary>
        /// ::ST - This key will be used to save the provisioninig List result data set
        /// </summary>
        private const string _ProvisionListDS = "ProvisionListDS";
        public DataSet ProvisionListDS
        {
            get
            {
                if (Context.Current.Session[_ProvisionListDS] == null)
                {
                    return null;
                }
                else
                {
                    return (DataSet)(Context.Current.Session[_ProvisionListDS]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_ProvisionListDS] != null)
                    {
                        Context.Current.Session.Remove(_ProvisionListDS);
                    }
                }
                else
                {
                    Context.Current.Session[_ProvisionListDS] = value;
                }
            }
        }

        /// <summary>
        /// ::ST - This key will be used to save the provisioninig List result data set
        /// </summary>
        private const string _ServiceChargeDS = "ProvisionListDS";
        public DataSet ServiceChargeDS
        {
            get
            {
                if (Context.Current.Session[_ServiceChargeDS] == null)
                {
                    return null;
                }
                else
                {
                    return (DataSet)(Context.Current.Session[_ServiceChargeDS]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_ServiceChargeDS] != null)
                    {
                        Context.Current.Session.Remove(_ServiceChargeDS);
                    }
                }
                else
                {
                    Context.Current.Session[_ServiceChargeDS] = value;
                }
            }
        }

        /// <summary>
        /// ::ST - This key will be used to save the provisioninig category List result data set
        /// </summary>
        private const string _ProvisionCategories = "ProvisionCategoriesListDS";
        public DataSet ProvisionCategories
        {
            get
            {
                if (Context.Current.Session[_ProvisionCategories] == null)
                {
                    return null;
                }
                else
                {
                    return (DataSet)(Context.Current.Session[_ProvisionCategories]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_ProvisionCategories] != null)
                    {
                        Context.Current.Session.Remove(_ProvisionCategories);
                    }
                }
                else
                {
                    Context.Current.Session[_ProvisionCategories] = value;
                }
            }
        }

        /// <summary>
        /// This key will be used to save the selected tree view node Depth
        /// </summary>
        private const string _TVSelectedNodeDepth = "TVSelectedNodeDepth";
        public int TVSelectedNodeDepth
        {
            get
            {
                if (Context.Current.Session[_TVSelectedNodeDepth] == null)
                {
                    return 0;
                }
                else
                {
                    return Convert.ToInt32(Context.Current.Session[_TVSelectedNodeDepth]);
                }
            }
            set
            {
                if (value == 0)
                {
                    if (Context.Current.Session[_TVSelectedNodeDepth] != null)
                    {
                        Context.Current.Session.Remove(_TVSelectedNodeDepth);
                    }
                }
                else
                {
                    Context.Current.Session[_TVSelectedNodeDepth] = value;
                }
            }
        }

        /// <summary>
        /// This key will be used to save the selected tree view node Text
        /// </summary>
        private const string _AmendAttribute = "AmendAttribute";
        public bool AmendAttribute
        {
            get
            {
                if (Context.Current.Session[_AmendAttribute] == null)
                {
                    return false;
                }
                else
                {
                    return Convert.ToBoolean(Context.Current.Session[_AmendAttribute].ToString());
                }
            }
            set
            {
                if (value == false)
                {
                    if (Context.Current.Session[_AmendAttribute] != null)
                    {
                        Context.Current.Session.Remove(_AmendAttribute);
                    }
                }
                else
                {
                    Context.Current.Session[_AmendAttribute] = value;
                }
            }
        }

        /// <summary>
        /// This key will be used to save the selected tree view node Text
        /// </summary>
        private const string _IsWorkSatisfactory = "IsWorkSatisfactory";
        public bool IsWorkSatisfactory
        {
            get
            {
                if (Context.Current.Session[_IsWorkSatisfactory] == null)
                {
                    return false;
                }
                else
                {
                    return Convert.ToBoolean(Context.Current.Session[_IsWorkSatisfactory].ToString());
                }
            }
            set
            {
                if (value == false)
                {
                    if (Context.Current.Session[_IsWorkSatisfactory] != null)
                    {
                        Context.Current.Session.Remove(_IsWorkSatisfactory);
                    }
                }
                else
                {
                    Context.Current.Session[_IsWorkSatisfactory] = value;
                }
            }
        }

        /// <summary>
        /// This key will be used to save the selected tree view node Text
        /// </summary>
        private const string _WorksRequiredRowId = "WorksRequiredRowId";
        public string WorksRequiredRowId
        {
            get
            {
                if (Context.Current.Session[_WorksRequiredRowId] == null)
                {
                    return null;
                }
                else
                {
                    return Context.Current.Session[_WorksRequiredRowId].ToString();
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_WorksRequiredRowId] != null)
                    {
                        Context.Current.Session.Remove(_WorksRequiredRowId);
                    }
                }
                else
                {
                    Context.Current.Session[_WorksRequiredRowId] = value;
                }
            }
        }

        /// <summary>
        /// This key will be used to save the data access of Showlist item
        /// </summary>
        private const string _ApplianceDetailDataSet = "ApplianceDetailDataSet";
        public DataSet ApplianceDetailDataSet
        {
            get
            {
                if (Context.Current.Session[_ApplianceDetailDataSet] == null)
                {
                    return null;
                }
                else
                {
                    return (DataSet)(Context.Current.Session[_ApplianceDetailDataSet]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_ApplianceDetailDataSet] != null)
                    {
                        Context.Current.Session.Remove(_ApplianceDetailDataSet);
                    }
                }
                else
                {
                    Context.Current.Session[_ApplianceDetailDataSet] = value;
                }
            }
        }

        /// <summary>
        /// This key will be used to save the data access of Showlist item
        /// </summary>
        private const string _SchemeDetail = "SchemeDetail";
        public DataSet SchemeDetail
        {
            get
            {
                if (Context.Current.Session[_SchemeDetail] == null)
                {
                    return null;
                }
                else
                {
                    return (DataSet)(Context.Current.Session[_SchemeDetail]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_SchemeDetail] != null)
                    {
                        Context.Current.Session.Remove(_SchemeDetail);
                    }
                }
                else
                {
                    Context.Current.Session[_SchemeDetail] = value;
                }
            }
        }

        #region This key will be used to save ME Appointment BO
        /// <summary>
        /// This key will be used to save ME Appointment BO
        /// </summary>
        private const string _MeAppointmentBO = "MeAppointmentBO";
        public MeAppointmentBO MeAppointmentBO
        {
            get
            {
                if (Context.Current.Session[_MeAppointmentBO] == null)
                {
                    MeAppointmentBO objMeAppointmentBO = new MeAppointmentBO();
                    return objMeAppointmentBO;
                }
                else
                {
                    return (MeAppointmentBO)(Context.Current.Session[_MeAppointmentBO]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_MeAppointmentBO] != null)
                    {
                        Context.Current.Session.Remove(_MeAppointmentBO);
                    }
                }
                else
                {
                    Context.Current.Session[_MeAppointmentBO] = value;
                }
            }
        }
        #endregion

        #region This key will be used to save Trades Datatable
        /// <summary>
        /// This key will be used to save Trades Datatable
        /// </summary>
        private const string _TradesDataTable = "TradesDataTable";
        public DataTable TradesDataTable
        {
            get
            {
                if (Context.Current.Session[_TradesDataTable] == null)
                {
                    DataTable objTradesDataTable = new DataTable();
                    return objTradesDataTable;
                }
                else
                {
                    return (DataTable)(Context.Current.Session[_TradesDataTable]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_TradesDataTable] != null)
                    {
                        Context.Current.Session.Remove(_TradesDataTable);
                    }
                }
                else
                {
                    Context.Current.Session[_TradesDataTable] = value;
                }
            }
        }
        #endregion

        #region This key will be used to save Confirm Me Appointment BlockList
        /// <summary>
        /// This key will be used to save Confirm Me Appointment BlockList
        /// </summary>
        private const string _ConfirmMeAppointmetBlockList = "ConfirmMeAppointmetBlockList";
        public List<ConfirmMeAppointmentBO> ConfirmMeAppointmetBlockList
        {
            get
            {
                if (Context.Current.Session[_ConfirmMeAppointmetBlockList] == null)
                {
                    List<ConfirmMeAppointmentBO> objConfirmMeAppointmentBO = new List<ConfirmMeAppointmentBO>();
                    return objConfirmMeAppointmentBO;
                }
                else
                {
                    return (List<ConfirmMeAppointmentBO>)(Context.Current.Session[_ConfirmMeAppointmetBlockList]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_ConfirmMeAppointmetBlockList] != null)
                    {
                        Context.Current.Session.Remove(_ConfirmMeAppointmetBlockList);
                    }
                }
                else
                {
                    Context.Current.Session[_ConfirmMeAppointmetBlockList] = value;
                }
            }
        }
        #endregion

        #region This key will be used to save Temp Me Appointment BO List
        /// <summary>
        /// This key will be used to save Temp Me Appointment BO List
        /// </summary>
        private const string _TempMeAppointmentBOList = "TempMeAppointmentBOList";
        public List<TempMeAppointmentBO> TempMeAppointmentBOList
        {
            get
            {
                if (Context.Current.Session[_TempMeAppointmentBOList] == null)
                {
                    List<TempMeAppointmentBO> objTempMeAppointmentBO = new List<TempMeAppointmentBO>();
                    return objTempMeAppointmentBO;
                }
                else
                {
                    return (List<TempMeAppointmentBO>)(Context.Current.Session[_TempMeAppointmentBOList]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_TempMeAppointmentBOList] != null)
                    {
                        Context.Current.Session.Remove(_TempMeAppointmentBOList);
                    }
                }
                else
                {
                    Context.Current.Session[_TempMeAppointmentBOList] = value;
                }
            }
        }
        #endregion

        #region This key will be used to save the available operatives Dataset
        /// <summary>
        /// This key will be used to save the available operatives Dataset
        /// </summary>
        private const string _AvailableOperativesDs = "AvailableOperativesDs";
        public DataSet AvailableOperativesDs
        {
            get
            {
                if (Context.Current.Session[_AvailableOperativesDs] == null)
                {
                    return null;
                }
                else
                {
                    return (DataSet)(Context.Current.Session[_AvailableOperativesDs]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_AvailableOperativesDs] != null)
                    {
                        Context.Current.Session.Remove(_AvailableOperativesDs);
                    }
                }
                else
                {
                    Context.Current.Session[_AvailableOperativesDs] = value;
                }
            }
        }
        #endregion

        #region This key will be used to save Me Available Operatives Appointments
        /// <summary>
        /// This key will be used to save Me Available Operatives Appointments
        /// </summary>
        private const string _MeAvailableOperativesAppointments = "MeAvailableOperativesAppointments";
        public TempAppointmentDtBO MeAvailableOperativesAppointments
        {
            get
            {
                if (Context.Current.Session[_SelectedAttribute] == null)
                {
                    TempAppointmentDtBO objTempAppointmentDtBO = new TempAppointmentDtBO();
                    return objTempAppointmentDtBO;
                }
                else
                {
                    return (TempAppointmentDtBO)(Context.Current.Session[_SelectedAttribute]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_SelectedAttribute] != null)
                    {
                        Context.Current.Session.Remove(_SelectedAttribute);
                    }
                }
                else
                {
                    Context.Current.Session[_SelectedAttribute] = value;
                }
            }
        }
        #endregion

        #region This key will be used to save selected attribute from listing of Appointment To Be Arranged Or Appointment Arranged
        /// <summary>
        /// This key will be used to save selected attribute from listing of Appointment To Be Arranged Or Appointment Arranged 
        /// </summary>
        private const string _SelectedAttribute = "SelectedAttribute";
        public SelectedAttributeBO SelectedAttribute
        {
            get
            {
                if (Context.Current.Session[_SelectedAttribute] == null)
                {
                    SelectedAttributeBO objSelectedAttributeBO = new SelectedAttributeBO();
                    return objSelectedAttributeBO;
                }
                else
                {
                    return (SelectedAttributeBO)(Context.Current.Session[_SelectedAttribute]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_SelectedAttribute] != null)
                    {
                        Context.Current.Session.Remove(_SelectedAttribute);
                    }
                }
                else
                {
                    Context.Current.Session[_SelectedAttribute] = value;
                }
            }
        }
        #endregion

        #region This key will be used to save ME Scheduling BO
        /// <summary>
        /// This key will be used to save ME Scheduling BO
        /// </summary>
        private const string _MeSchedulingBO = "MeSchedulingBO";
        public MeSchedulingBO MeSchedulingBO
        {
            get
            {
                if (Context.Current.Session[_MeSchedulingBO] == null)
                {
                    MeSchedulingBO objMeSchedulingBO = new MeSchedulingBO();
                    return objMeSchedulingBO;
                }
                else
                {
                    return (MeSchedulingBO)(Context.Current.Session[_MeSchedulingBO]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_MeSchedulingBO] != null)
                    {
                        Context.Current.Session.Remove(_MeSchedulingBO);
                    }
                }
                else
                {
                    Context.Current.Session[_MeSchedulingBO] = value;
                }
            }
        }
        #endregion

        #region This key will be used to save Expenditure BO List
        /// <summary>
        /// This key will be used to save Expenditure BO List
        /// </summary>
        private const string _ExpenditureBOList = "ExpenditureBOList";
        public List<ExpenditureBO> ExpenditureBOList
        {
            get
            {
                if (Context.Current.Session[_ExpenditureBOList] == null)
                {
                    List<ExpenditureBO> objExpenditureBOList = new List<ExpenditureBO>();
                    return objExpenditureBOList;
                }
                else
                {
                    return (List<ExpenditureBO>)(Context.Current.Session[_ExpenditureBOList]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_ExpenditureBOList] != null)
                    {
                        Context.Current.Session.Remove(_ExpenditureBOList);
                    }
                }
                else
                {
                    Context.Current.Session[_ExpenditureBOList] = value;
                }
            }
        }
        #endregion

        #region This key will be used to save Vat BO List
        /// <summary>
        /// This key will be used to save Vat BO List
        /// </summary>
        private const string _VatBoList = "VatBoList";
        public List<VatBo> VatBOList
        {
            get
            {
                if (Context.Current.Session[_VatBoList] == null)
                {
                    List<VatBo> objVatBOList = new List<VatBo>();
                    return objVatBOList;
                }
                else
                {
                    return (List<VatBo>)(Context.Current.Session[_VatBoList]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_VatBoList] != null)
                    {
                        Context.Current.Session.Remove(_VatBoList);
                    }
                }
                else
                {
                    Context.Current.Session[_VatBoList] = value;
                }
            }
        }
        #endregion


        #region This key will be used to save Assign To Contractor Bo
        /// <summary>
        /// This key will be used to save Assign To Contractor Bo
        /// </summary>
        private const string _AssignToContractorBo = "AssignToContractorBo";
        public AssignToContractorBo AssignToContractorBo
        {
            get
            {
                if (Context.Current.Session[_AssignToContractorBo] == null)
                {
                    AssignToContractorBo objAssignToContractorBo = new AssignToContractorBo();
                    return objAssignToContractorBo;
                }
                else
                {
                    return (AssignToContractorBo)(Context.Current.Session[_AssignToContractorBo]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_AssignToContractorBo] != null)
                    {
                        Context.Current.Session.Remove(_AssignToContractorBo);
                    }
                }
                else
                {
                    Context.Current.Session[_AssignToContractorBo] = value;
                }
            }
        }
        #endregion

        /// <summary>
        /// This key will be used to save the selected tree view node Text
        /// </summary>
        private const string _showHidePopUp = "ShowHidePopUp";
        public string showHidePopUp
        {
            get
            {
                if (Context.Current.Session[_showHidePopUp] == null)
                {
                    return null;
                }
                else
                {
                    return Context.Current.Session[_showHidePopUp].ToString();
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_showHidePopUp] != null)
                    {
                        Context.Current.Session.Remove(_showHidePopUp);
                    }
                }
                else
                {
                    Context.Current.Session[_showHidePopUp] = value;
                }
            }
        }

        #region This key will be used to save the Reports search to access PopUp for Multiple reports
        /// <summary>
        /// This key will be used to save the Reports search to access PopUp for Multiple reports
        /// </summary>
        private const string _StockReportBo = "StockReportsBo";
        public ReportsBO StockReportsBo
        {
            get
            {
                if (Context.Current.Session[_StockReportBo] == null)
                {
                    ReportsBO objReportBo = new ReportsBO();
                    return objReportBo;
                }
                else
                {
                    return (ReportsBO)(Context.Current.Session[_StockReportBo]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_StockReportBo] != null)
                    {
                        Context.Current.Session.Remove(_StockReportBo);
                    }
                }
                else
                {
                    Context.Current.Session[_StockReportBo] = value;
                }
            }
        }
        #endregion

        /// <summary>
        /// This key shall be used to save the Document Upload Name
        /// </summary>
        private const string _DocumentUploadName = "DocumentUploadName";
        public string DocumentUploadName
        {
            get
            {
                if (Context.Current.Session[_DocumentUploadName] == null)
                {
                    return string.Empty;
                }
                else
                {
                    return Convert.ToString(Context.Current.Session[_DocumentUploadName]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_DocumentUploadName] != null)
                    {
                        Context.Current.Session.Remove(_DocumentUploadName);
                    }
                }
                else
                {
                    Context.Current.Session[_DocumentUploadName] = value;
                }

            }
        }

        /// <summary>
        /// This key shall be used to save the Document Upload Name
        /// </summary>
        private const string _PhotoUploadName = "PhotoUploadName";
        public string PhotoUploadName
        {
            get
            {
                if (Context.Current.Session[_PhotoUploadName] == null)
                {
                    return string.Empty;
                }
                else
                {
                    return Convert.ToString(Context.Current.Session[_PhotoUploadName]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_PhotoUploadName] != null)
                    {
                        Context.Current.Session.Remove(_PhotoUploadName);
                    }
                }
                else
                {
                    Context.Current.Session[_PhotoUploadName] = value;
                }

            }
        }

        /// <summary>
        /// This key shall be used to save the Signature Upload Name
        /// </summary>
        private const string _SignatureUploadName = "SignatureUploadName";
        public string SignatureUploadName
        {
            get
            {
                if (Context.Current.Session[_SignatureUploadName] == null)
                {
                    return string.Empty;
                }
                else
                {
                    return Convert.ToString(Context.Current.Session[_SignatureUploadName]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_SignatureUploadName] != null)
                    {
                        Context.Current.Session.Remove(_SignatureUploadName);
                    }
                }
                else
                {
                    Context.Current.Session[_SignatureUploadName] = value;
                }

            }
        }

        private const String _uploadingDocumentBO = "UploadingDocumentBO";
        public DevelopmentDocumentsBO UploadingDocumentBO
        {
            get
            {
                if (Context.Current.Session[_uploadingDocumentBO] == null)
                {
                    DevelopmentDocumentsBO objUploadingDocumentBO = new DevelopmentDocumentsBO();
                    return objUploadingDocumentBO;
                }
                else
                {
                    return (DevelopmentDocumentsBO)(Context.Current.Session[_uploadingDocumentBO]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_uploadingDocumentBO] != null)
                    {
                        Context.Current.Session.Remove(_uploadingDocumentBO);
                    }
                }
                else
                {
                    Context.Current.Session[_uploadingDocumentBO] = value;
                }
            }
        }

        private const String _meSearchBO = "MeSearchBO";
        public MeSearchBO MeSearchBO
        {
            get
            {
                if (Context.Current.Session[_meSearchBO] == null)
                {
                    MeSearchBO objMeSearchBO = new MeSearchBO();
                    return objMeSearchBO;
                }
                else
                {
                    return (MeSearchBO)(Context.Current.Session[_meSearchBO]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_meSearchBO] != null)
                    {
                        Context.Current.Session.Remove(_meSearchBO);
                    }
                }
                else
                {
                    Context.Current.Session[_meSearchBO] = value;
                }
            }
        }

        private const String _fundingSourceDt = "FundingSourceDt";
        public DataTable FundingSourceDt
        {
            get
            {
                if (Context.Current.Session[_fundingSourceDt] == null)
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add(ApplicationConstants.DevelopmentFundingIdCol, typeof(int));
                    dt.Columns.Add(ApplicationConstants.FundingAuthorityIdCol, typeof(int));
                    dt.Columns.Add(ApplicationConstants.FundingAuthorityCol, typeof(string));
                    dt.Columns.Add(ApplicationConstants.DevelopmentGrantAmountCol, typeof(float));
                    return dt;
                }
                else
                {
                    return (DataTable)(Context.Current.Session[_fundingSourceDt]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_fundingSourceDt] != null)
                    {
                        Context.Current.Session.Remove(_fundingSourceDt);
                    }
                }
                else
                {
                    Context.Current.Session[_fundingSourceDt] = value;
                }
            }
        }


        private const string _SearchText = "SearchText";
        public string SearchText
        {
            get
            {
                if (Context.Current.Session[_SearchText] == null)
                {
                    return string.Empty;
                }
                else
                {
                    return Convert.ToString(Context.Current.Session[_SearchText]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_SearchText] != null)
                    {
                        Context.Current.Session.Remove(_SearchText);
                    }
                }
                else
                {
                    Context.Current.Session[_SearchText] = value;
                }

            }
        }

        private const string _PropertyStatus = "PropertyStatus";
        public string PropertyStatus
        {
            get
            {
                if (Context.Current.Session[_PropertyStatus] == null)
                {
                    return string.Empty;
                }
                else
                {
                    return Convert.ToString(Context.Current.Session[_PropertyStatus]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_PropertyStatus] != null)
                    {
                        Context.Current.Session.Remove(_PropertyStatus);
                    }
                }
                else
                {
                    Context.Current.Session[_PropertyStatus] = value;
                }

            }
        }

        private const string _Patch = "";
        public int Patch
        {
            get
            {
                if (Context.Current.Session[_Patch] == null)
                {
                    return 0;
                }
                else
                {
                    return int.Parse(Convert.ToString(Context.Current.Session[_Patch]));
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_Patch] != null)
                    {
                        Context.Current.Session.Remove(_Patch);
                    }
                }
                else
                {
                    Context.Current.Session[_Patch] = value;
                }

            }
        }

        #region This key will be used to save  Available Operatives time slots for Inspection
        /// <summary>
        /// This key will be used to save Available Operatives time slots for Inspection
        /// </summary>
        private const string _AvailableOperativesTimeSlots = "AvailableOperativesTimeSlots";
        public TempAppointmentDtBO AvailableOperativesTimeSlots
        {
            get
            {
                if (Context.Current.Session[_AvailableOperativesTimeSlots] == null)
                {
                    TempAppointmentDtBO objTempAppointmentDtBO = new TempAppointmentDtBO();
                    return objTempAppointmentDtBO;
                }
                else
                {
                    return (TempAppointmentDtBO)(Context.Current.Session[_AvailableOperativesTimeSlots]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_AvailableOperativesTimeSlots] != null)
                    {
                        Context.Current.Session.Remove(_AvailableOperativesTimeSlots);
                    }
                }
                else
                {
                    Context.Current.Session[_AvailableOperativesTimeSlots] = value;
                }
            }
        }
        #endregion

        private const String _britishGasSearchBo = "BritishGasSearchBo";
        public BritishGasSearchBo BritishGasSearchBo
        {
            get
            {
                if (Context.Current.Session[_britishGasSearchBo] == null)
                {
                    BritishGasSearchBo britishGasSearchBo = new BritishGasSearchBo();
                    return britishGasSearchBo;
                }
                else
                {
                    return (BritishGasSearchBo)(Context.Current.Session[_britishGasSearchBo]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_britishGasSearchBo] != null)
                    {
                        Context.Current.Session.Remove(_britishGasSearchBo);
                    }
                }
                else
                {
                    Context.Current.Session[_britishGasSearchBo] = value;
                }
            }
        }

        private const String _britishGasNotificationBo = "BritishGasNotificationBo";
        public BritishGasNotifcationBO BritishGasNotificationBo
        {
            get
            {
                if (Context.Current.Session[_britishGasNotificationBo] == null)
                {
                    BritishGasNotifcationBO britishGasNotificationBo = new BritishGasNotifcationBO();
                    return britishGasNotificationBo;
                }
                else
                {
                    return (BritishGasNotifcationBO)(Context.Current.Session[_britishGasNotificationBo]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_britishGasNotificationBo] != null)
                    {
                        Context.Current.Session.Remove(_britishGasNotificationBo);
                    }
                }
                else
                {
                    Context.Current.Session[_britishGasNotificationBo] = value;
                }
            }
        }

        private const String _RequiredWorksDs = "RequiredWorksDs";
        public DataSet RequiredWorksDs
        {
            get
            {
                if (Context.Current.Session[_RequiredWorksDs] == null)
                {
                    return null;
                }
                else
                {
                    return (DataSet)(Context.Current.Session[_RequiredWorksDs]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_RequiredWorksDs] != null)
                    {
                        Context.Current.Session.Remove(_RequiredWorksDs);
                    }
                }
                else
                {
                    Context.Current.Session[_RequiredWorksDs] = value;
                }
            }
        }


        #region This key will be used to save checked temp required works list
        /// <summary>
        /// This key will be used to save Confirm Me Appointment BlockList
        /// </summary>
        private const string _RequiredWorksBOList = "RequiredWorksBOList";
        public List<RequiredWorksBO> RequiredWorksBOList
        {
            get
            {
                if (Context.Current.Session[_RequiredWorksBOList] == null)
                {
                    List<RequiredWorksBO> objRequiredWorksBoList = new List<RequiredWorksBO>();
                    return objRequiredWorksBoList;
                }
                else
                {
                    return (List<RequiredWorksBO>)(Context.Current.Session[_RequiredWorksBOList]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_RequiredWorksBOList] != null)
                    {
                        Context.Current.Session.Remove(_RequiredWorksBOList);
                    }
                }
                else
                {
                    Context.Current.Session[_RequiredWorksBOList] = value;
                }
            }
        }
        #endregion


        private const String _commonSearchBO = "CommonSearchBO";
        public CommonSearchBO CommonSearchBO
        {
            get
            {
                if (Context.Current.Session[_commonSearchBO] == null)
                {
                    CommonSearchBO objCommonSearchBo = new CommonSearchBO();
                    return objCommonSearchBo;
                }
                else
                {
                    return (CommonSearchBO)(Context.Current.Session[_commonSearchBO]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_commonSearchBO] != null)
                    {
                        Context.Current.Session.Remove(_commonSearchBO);
                    }
                }
                else
                {
                    Context.Current.Session[_commonSearchBO] = value;
                }
            }
        }


        #region This key will be used to save Void Appointment BO
        /// <summary>
        /// This key will be used to save Void Appointment BO
        /// </summary>
        private const string _VoidAppointmentBO = "VoidAppointmentBO";
        public VoidAppointmentBO VoidAppointmentBO
        {
            get
            {
                if (Context.Current.Session[_VoidAppointmentBO] == null)
                {
                    VoidAppointmentBO objAppointmentBO = new VoidAppointmentBO();
                    return objAppointmentBO;
                }
                else
                {
                    return (VoidAppointmentBO)(Context.Current.Session[_VoidAppointmentBO]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_VoidAppointmentBO] != null)
                    {
                        Context.Current.Session.Remove(_VoidAppointmentBO);
                    }
                }
                else
                {
                    Context.Current.Session[_VoidAppointmentBO] = value;
                }
            }
        }
        #endregion

        /// <summary>
        /// This key will be used to save the data access of Showlist item
        /// </summary>
        private const string _PaintDetailDataSet = "PaintDetailDataSet";
        public DataSet PaintDetailDataSet
        {
            get
            {
                if (Context.Current.Session[_PaintDetailDataSet] == null)
                {
                    return null;
                }
                else
                {
                    return (DataSet)(Context.Current.Session[_PaintDetailDataSet]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_PaintDetailDataSet] != null)
                    {
                        Context.Current.Session.Remove(_PaintDetailDataSet);
                    }
                }
                else
                {
                    Context.Current.Session[_PaintDetailDataSet] = value;
                }
            }
        }




        /// <summary>
        /// This key will be used to save the British Gas Pdf DataSet to send email
        /// </summary>
        private const string _BritishGasPdfDataSet = "BritishGasPdfDataSet";
        public DataSet BritishGasPdfDataSet
        {
            get
            {
                if (Context.Current.Session[_BritishGasPdfDataSet] == null)
                {
                    return null;
                }
                else
                {
                    return (DataSet)(Context.Current.Session[_BritishGasPdfDataSet]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_BritishGasPdfDataSet] != null)
                    {
                        Context.Current.Session.Remove(_BritishGasPdfDataSet);
                    }
                }
                else
                {
                    Context.Current.Session[_BritishGasPdfDataSet] = value;
                }
            }
        }
        #region Repairs Data Table, Set and Remove Method/Function(s)

        #region Set Repairs Data Table
        /// <summary>
        /// This function shall be used to save Trades Data Table in session
        /// </summary>
        /// <param name="RepairsDataSet"></param>
        /// <remarks></remarks>
        public static void setRepairsDataTable(ref DataTable RepairsDataSet)
        {
            Context.Current.Session[ApplicationConstants.RepairDataTable] = RepairsDataSet;
        }

        #endregion


        #region Get Repairs Data Table
        /// <summary>
        /// This function shall be used to get Repairs Data Table from session.
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public static DataTable getRepairsDataTable()
        {
            if (((Context.Current.Session[ApplicationConstants.RepairDataTable] == null)))
            {
                DataTable RepairsList = new DataTable();
                return RepairsList;
            }
            else
            {
                return (DataTable)Context.Current.Session[ApplicationConstants.RepairDataTable];
            }
        }

        #endregion


        #region remove Repairs Data Table
        /// <summary>
        /// This function shall be used to remove saved Repairs Data Table from session.
        /// </summary>
        /// <remarks></remarks>
        public static void removeRepairsDataTable()
        {
            if (((Context.Current.Session[ApplicationConstants.RepairDataTable] != null)))
            {
                Context.Current.Session.Remove(ApplicationConstants.RepairDataTable);
            }
        }

        #endregion
        #endregion


        #region Emails details Set and Remove Method/Function(s)

        #region Set Email Details

        public void setEmailDetails(ref CompletePurchaseOrderBO objCompletePurchaseOrderBO)
        {
            Context.Current.Session[ApplicationConstants.EmailDetails] = objCompletePurchaseOrderBO;
        }

        #endregion

        #region Get Email Details
        public CompletePurchaseOrderBO getEmailDetails()
        {
            return (CompletePurchaseOrderBO)Context.Current.Session[ApplicationConstants.EmailDetails];
        }

        #endregion

        #region remove Email Details
        public void removeEmailDetails()
        {
            if (((Context.Current.Session[ApplicationConstants.EmailDetails] != null)))
            {
                Context.Current.Session.Remove(ApplicationConstants.EmailDetails);
            }
        }

        #endregion

        #endregion


        /// <summary>
        /// This key will be used to save the selected tree view node Depth
        /// </summary>
        private const string _PatchId = "PatchId";
        public int PatchId
        {
            get
            {
                if (Context.Current.Session[_PatchId] == null)
                {
                    return 0;
                }
                else
                {
                    return Convert.ToInt32(Context.Current.Session[_PatchId]);
                }
            }
            set
            {
                if (value == 0)
                {
                    if (Context.Current.Session[_PatchId] != null)
                    {
                        Context.Current.Session.Remove(_PatchId);
                    }
                }
                else
                {
                    Context.Current.Session[_PatchId] = value;
                }
            }
        }

        private const String _WorksRequiredForEmailDs = "WorksRequiredForEmailDs";
        public DataSet GetPurchaseOrderDetailDs
        {
            get
            {
                if (Context.Current.Session[_WorksRequiredForEmailDs] == null)
                {
                    return null;
                }
                else
                {
                    return (DataSet)(Context.Current.Session[_WorksRequiredForEmailDs]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_WorksRequiredForEmailDs] != null)
                    {
                        Context.Current.Session.Remove(_WorksRequiredForEmailDs);
                    }
                }
                else
                {
                    Context.Current.Session[_WorksRequiredForEmailDs] = value;
                }
            }
        }

        private const string _OperativeWorkingHoursDataSet = "OperativeWorkingHoursDataSet";
        public DataSet OperativeWorkingHoursDataSet
        {
            get
            {
                if (Context.Current.Session[_OperativeWorkingHoursDataSet] == null)
                {
                    return null;
                }
                else
                {
                    return (DataSet)(Context.Current.Session[_OperativeWorkingHoursDataSet]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_OperativeWorkingHoursDataSet] != null)
                    {
                        Context.Current.Session.Remove(_OperativeWorkingHoursDataSet);
                    }
                }
                else
                {
                    Context.Current.Session[_OperativeWorkingHoursDataSet] = value;
                }
            }
        }



        ///<summary>
        /// Following are filter values for ComplianceDocument Report.
        ///</summary>

        private const string _Type = "Type";
        public int Type
        {
            get
            {
                if (Context.Current.Session[_Type] == null)
                {
                    return 0;
                }
                else
                {
                    return Convert.ToInt32(Context.Current.Session[_Type]);
                }
            }
            set
            {
                if (value == 0)
                {
                    if (Context.Current.Session[_Type] != null)
                    {
                        Context.Current.Session.Remove(_Type);
                    }
                }
                else
                {
                    Context.Current.Session[_Type] = value;
                }
            }
        }

        private const string _Title = "Title";
        public string Title
        {
            get
            {
                if (Context.Current.Session[_Title] == null)
                {
                    return null;
                }
                else
                {
                    return Convert.ToString(Context.Current.Session[_Title]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_Title] != null)
                    {
                        Context.Current.Session.Remove(_Title);
                    }
                }
                else
                {
                    Context.Current.Session[_Title] = value;
                }
            }
        }

        private const string _Added = "Added";
        public string Added
        {
            get
            {
                if (Context.Current.Session[_Added] == null)
                {
                    return null;
                }
                else
                {
                    return (Context.Current.Session[_Added]).ToString();
                }
            }
            set
            {
                if (value.Equals(null))
                {
                    if (Context.Current.Session[_Added] != null)
                    {
                        Context.Current.Session.Remove(_Added);
                    }
                }
                else
                {
                    Context.Current.Session[_Added] = value;
                }
            }
        }

        private const string _By = "By";
        public int By
        {
            get
            {
                if (Context.Current.Session[_By] == null)
                {
                    return 0;
                }
                else
                {
                    return Convert.ToInt32(Context.Current.Session[_By]);
                }
            }
            set
            {
                if (value == 0)
                {
                    if (Context.Current.Session[_By] != null)
                    {
                        Context.Current.Session.Remove(_By);
                    }
                }
                else
                {
                    Context.Current.Session[_By] = value;
                }
            }
        }

        private const string _Document = "Document";
        public string Document
        {
            get
            {
                if (Context.Current.Session[_Document] == null)
                {
                    return null;
                }
                else
                {
                    return Convert.ToString((Context.Current.Session[_Document]));
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_Document] != null)
                    {
                        Context.Current.Session.Remove(_Document);
                    }
                }
                else
                {
                    Context.Current.Session[_Document] = value;
                }
            }
        }

        private const string _Expiry = "Expiry";
        public bool Expiry
        {
            get
            {
                if (Context.Current.Session[_Expiry] == null)
                {
                    return false;
                }
                else
                {
                    return Convert.ToBoolean(Context.Current.Session[_Expiry]);
                }
            }
            set
            {
                if (value == false)
                {
                    if (Context.Current.Session[_Expiry] != null)
                    {
                        Context.Current.Session.Remove(_Expiry);
                    }
                }
                else
                {
                    Context.Current.Session[_Expiry] = value;
                }
            }
        }


        /// <summary>
        /// This key will be used to save the employee Id
        /// </summary>
        private const string _CustomerId = "CustomerId";
        public int CustomerId
        {
            get
            {
                if (Context.Current.Session[_CustomerId] == null)
                {
                    return 0;
                }
                else
                {
                    return Convert.ToInt32(Context.Current.Session[_CustomerId]);
                }
            }
            set
            {
                if (value == 0)
                {
                    if (Context.Current.Session[_CustomerId] != null)
                    {
                        Context.Current.Session.Remove(_CustomerId);
                    }
                }
                else
                {
                    Context.Current.Session[_CustomerId] = value;
                }
            }
        }


        private const string _InspectionJournalId = "InspectionJournalId";
        public int InspectionJournalId
        {
            get
            {
                if (Context.Current.Session[_InspectionJournalId] == null)
                {
                    return 0;
                }
                else
                {
                    return Convert.ToInt32(Context.Current.Session[_InspectionJournalId]);
                }
            }
            set
            {
                if (value == 0)
                {
                    if (Context.Current.Session[_InspectionJournalId] != null)
                    {
                        Context.Current.Session.Remove(_InspectionJournalId);
                    }
                }
                else
                {
                    Context.Current.Session[_InspectionJournalId] = value;
                }
            }
        }

        private const string _PowerSourceDataSet = "PowerSourceDataSet";
        public DataSet PowerSourceDataSet
        {
            get
            {
                if (Context.Current.Session[_PowerSourceDataSet] == null)
                {
                    return null;
                }
                else
                {
                    return (DataSet)(Context.Current.Session[_PowerSourceDataSet]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_PowerSourceDataSet] != null)
                    {
                        Context.Current.Session.Remove(_PowerSourceDataSet);
                    }
                }
                else
                {
                    Context.Current.Session[_PowerSourceDataSet] = value;
                }
            }
        }

        /// <summary>
        /// This key will be used to save the selected tree view node Depth
        /// </summary>
        private const string _OrderId = "OrderId";
        public int OrderId
        {
            get
            {
                if (Context.Current.Session[_OrderId] == null)
                {
                    return 0;
                }
                else
                {
                    return Convert.ToInt32(Context.Current.Session[_OrderId]);
                }
            }
            set
            {
                if (value == 0)
                {
                    if (Context.Current.Session[_OrderId] != null)
                    {
                        Context.Current.Session.Remove(_OrderId);
                    }
                }
                else
                {
                    Context.Current.Session[_OrderId] = value;
                }
            }
        }

        private const string _ContractorDataSet = "ContractorDataSet";
        public DataSet ContractorDataSet
        {
            get
            {
                if (Context.Current.Session[_ContractorDataSet] == null)
                {
                    return null;
                }
                else
                {
                    return (DataSet)(Context.Current.Session[_ContractorDataSet]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_ContractorDataSet] != null)
                    {
                        Context.Current.Session.Remove(_ContractorDataSet);
                    }
                }
                else
                {
                    Context.Current.Session[_ContractorDataSet] = value;
                }
            }
        }

        private const string _InspectionArrangedDataSet = "InspectionArrangedDataSet";
        public DataSet InspectionArrangedDataSet
        {
            get
            {
                if (Context.Current.Session[_InspectionArrangedDataSet] == null)
                {
                    return null;
                }
                else
                {
                    return (DataSet)(Context.Current.Session[_InspectionArrangedDataSet]);
                }
            }
            set
            {
                if (value == null)
                {
                    if (Context.Current.Session[_InspectionArrangedDataSet] != null)
                    {
                        Context.Current.Session.Remove(_InspectionArrangedDataSet);
                    }
                }
                else
                {
                    Context.Current.Session[_InspectionArrangedDataSet] = value;
                }
            }
        }


        private const string _BoilerRoomId = "BoilerRoomId";
        public int BoilerRoomId
        {
            get
            {
                if (Context.Current.Session[_BoilerRoomId] == null)
                {
                    return 0;
                }
                else
                {
                    return Convert.ToInt32(Context.Current.Session[_BoilerRoomId]);
                }
            }
            set
            {
                if (value == 0)
                {
                    if (Context.Current.Session[_BoilerRoomId] != null)
                    {
                        Context.Current.Session.Remove(_BoilerRoomId);
                    }
                }
                else
                {
                    Context.Current.Session[_BoilerRoomId] = value;
                }
            }
        }

        private const string _HeatingMappingId = "HeatingMappingId";
        public int HeatingMappingId
        {
            get
            {
                if (Context.Current.Session[_HeatingMappingId] == null)
                {
                    return 0;
                }
                else
                {
                    return Convert.ToInt32(Context.Current.Session[_HeatingMappingId]);
                }
            }
            set
            {
                if (value == 0)
                {
                    if (Context.Current.Session[_HeatingMappingId] != null)
                    {
                        Context.Current.Session.Remove(_HeatingMappingId);
                    }
                }
                else
                {
                    Context.Current.Session[_HeatingMappingId] = value;
                }
            }
        }


        #region IsPhotoGraphExist, Set and Remove Method/Function(s)

        #region Set Is PhotoGraph Exist
        /// <summary>
        /// This function shall be used to save IsPhotoGraphExist in session
        /// </summary>
        /// <param name="RepairsDataSet"></param>
        /// <remarks></remarks>
        public static void setIsPhotoGraphExist(ref bool isPhotoGraphExist)
        {
            Context.Current.Session[ApplicationConstants.IsPhotoGraphExist] = isPhotoGraphExist;
        }

        #endregion


        #region Get Is PhotoGraph Exist
        /// <summary>
        /// This function shall be used to get IsPhotoGraphExist.
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public static bool getIsPhotoGraphExist()
        {
            if (((Context.Current.Session[ApplicationConstants.IsPhotoGraphExist] == null)))
            {
                return false;
            }
            else
            {
                return (bool)Context.Current.Session[ApplicationConstants.IsPhotoGraphExist];
            }
        }

        #endregion


        #region remove Is PhotoGraph Exist
        /// <summary>
        /// This function shall be used to remove IsPhotoGraphExist.
        /// </summary>
        /// <remarks></remarks>
        public static void removeIsPhotoGraphExist()
        {
            if (((Context.Current.Session[ApplicationConstants.IsPhotoGraphExist] != null)))
            {
                Context.Current.Session.Remove(ApplicationConstants.IsPhotoGraphExist);
            }
        }

        #endregion
        #endregion

        #region IsNotesExist, Set and Remove Method/Function(s)

        #region Set Is Notes Exist
        /// <summary>
        /// This function shall be used to save IsNotesExist in session
        /// </summary>
        /// <param name="RepairsDataSet"></param>
        /// <remarks></remarks>
        public static void setIsNotesExist(ref bool isNotesExist)
        {
            Context.Current.Session[ApplicationConstants.IsNotesExist] = isNotesExist;
        }

        #endregion


        #region Get Is Notes Exist
        /// <summary>
        /// This function shall be used to get IsNotesExist.
        /// </summary>
        /// <returns></returns>
        /// <remarks></remarks>
        public static bool getIsNotesExist()
        {
            if (((Context.Current.Session[ApplicationConstants.IsNotesExist] == null)))
            {
                return false;
            }
            else
            {
                return (bool)Context.Current.Session[ApplicationConstants.IsNotesExist];
            }
        }

        #endregion


        #region remove Is Notes Exist
        /// <summary>
        /// This function shall be used to remove IsPhotoGraphExist.
        /// </summary>
        /// <remarks></remarks>
        public static void removeIsNotesExist()
        {
            if (((Context.Current.Session[ApplicationConstants.IsNotesExist] != null)))
            {
                Context.Current.Session.Remove(ApplicationConstants.IsNotesExist);
            }
        }

        #endregion
        #endregion


        /// <summary>
        /// This key will be used to save the selected tree view node id
        /// </summary>
        private const string _ChildeAttributMappingId = "ChildeAttributMappingId";
        public int ChildeAttributMappingId
        {
            get
            {
                if (Context.Current.Session[_ChildeAttributMappingId] == null)
                {
                    return 0;
                }
                else
                {
                    return Convert.ToInt32(Context.Current.Session[_ChildeAttributMappingId]);
                }
            }
            set
            {
                if (value == 0)
                {
                    if (Context.Current.Session[_ChildeAttributMappingId] != null)
                    {
                        Context.Current.Session.Remove(_ChildeAttributMappingId);
                    }
                }
                else
                {
                    Context.Current.Session[_ChildeAttributMappingId] = value;
                }
            }
        }

        /// <summary>
        /// This key will be used to save that Is Provision Click
        /// </summary>
        private const string _IsProvisionClicked = "IsProvisionClicked";
        public bool IsProvisionClicked
        {
            get
            {
                if (Context.Current.Session[_IsProvisionClicked] == null)
                {
                    return false;
                }
                else
                {
                    return Convert.ToBoolean(Context.Current.Session[_IsProvisionClicked]);
                }
            }
            set
            {
                Context.Current.Session[_IsProvisionClicked] = value;
            }
        }

    }
}
