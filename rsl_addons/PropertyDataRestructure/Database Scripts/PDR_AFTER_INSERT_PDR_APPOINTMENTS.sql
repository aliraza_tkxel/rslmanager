--====================================
-- Author:		<Author,,Ahmed Mehmood>
-- Create date: <Create Date,,6th January,2015>
-- Description:	<Description,,This trigger 'll insert the record in PDR_APPOINTMENTS_HISTORY after insertion in PDR_APPOINTMENTS
--====================================
CREATE TRIGGER PDR_AFTER_INSERT_PDR_APPOINTMENTS  
 ON PDR_APPOINTMENTS   
AFTER INSERT  
AS  
BEGIN  	
	INSERT INTO PDR_APPOINTMENT_HISTORY
            ([APPOINTMENTID]
           ,[TENANCYID]
           ,[JOURNALID]
           ,[JOURNALHISTORYID]
           ,[APPOINTMENTSTARTDATE]
           ,[APPOINTMENTENDDATE]
           ,[APPOINTMENTSTARTTIME]
           ,[APPOINTMENTENDTIME]
           ,[ASSIGNEDTO]
           ,[CREATEDBY]
           ,[LOGGEDDATE]
           ,[APPOINTMENTNOTES]
           ,[CUSTOMERNOTES]
           ,[APPOINTMENTSTATUS]
           ,[APPOINTMENTALERT]
           ,[APPOINTMENTCALENDAR]
           ,[TRADEID]
           ,[DURATION])
           SELECT 
            i.[APPOINTMENTID]
           ,i.[TENANCYID]
           ,i.[JOURNALID] 
           ,i.[JOURNALHISTORYID]
           ,i.[APPOINTMENTSTARTDATE] 
           ,i.[APPOINTMENTENDDATE]
           ,i.[APPOINTMENTSTARTTIME]
           ,i.[APPOINTMENTENDTIME]
           ,i.[ASSIGNEDTO]
           ,i.[CREATEDBY]
           ,i.[LOGGEDDATE]
           ,i.[APPOINTMENTNOTES]
           ,i.[CUSTOMERNOTES]
           ,i.[APPOINTMENTSTATUS]
           ,i.[APPOINTMENTALERT]
           ,i.[APPOINTMENTCALENDAR]
           ,i.[TRADEID]
           ,i.[DURATION]
           FROM INSERTED i                         
 END    
