USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetPropertyDocumentsInformation]    Script Date: 12/23/2015 14:58:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
 --=============================================  

/*
 EXEC AS_GetPropertyDocumentsInformation   
	 'A010000038',null,549,30,1,'DocumentId','DESC'
							History
						--------------
						
Version:	Author:			Create date:	Description:			WebPage:  

1.0			Aqib Javed 		18-Dec-2013		Get Property			PropertyRecord.aspx	-- > Document Tab  		
											Document Information		
											
	
1.1			Raja Aneeq		25/11/2014		Add three new Columns. 
											uploaded by,document date,Expiry date
	
*/											
-- =============================================  
ALTER PROCEDURE [dbo].[AS_GetPropertyDocumentsInformation]   
 @propertyId varchar(50)=null,
 @schemeId int=null,
 @blockId int = null ,  
 @pageSize int = 30,  
 @pageNumber int = 1,  
 @sortColumn varchar(50) = 'DocumentId',  
 @sortOrder varchar (5) = 'DESC',  
 @totalCount int = 0 output  
   
AS  
BEGIN  
  
 DECLARE @SelectClause varchar(MAX),  
        @fromClause   varchar(3000),  
        @whereClause  varchar(3000),           
        @orderClause  varchar(100),   
        @mainSelectQuery varchar(MAX),          
        @rowNumberQuery varchar(MAX),  
        @finalQuery varchar(MAX),  
        -- used to add in conditions in WhereClause based on search criteria provided  
        @searchCriteria varchar(1500),  
          
        --variables for paging  
        @offset int,  
  @limit int  
  --Paging Formula  
  SET @offset = 1 + (@pageNumber - 1) * @pageSize  
  SET @limit = (@offset + @pageSize) - 1   
  
  --========================================================================================           
  -- Begin building SELECT clause  
  -- Insert statements for procedure here  
  
  SET @selectClause = 'SELECT TOP (' + CONVERT(varchar(10), @limit) + ') p.DocumentId,  
        p.DocumentSize,  
        p.DocumentFormat,  
        p.DocumentTypeId,   
        dt.Title AS TypeTitle,  
        p.DocumentSubtypeId,  
        ds.Title AS SubtypeTitle,  
        CONVERT(varchar, p.CreatedDate, 103) AS CreatedDate,
        ISNULL(CONVERT(varchar,p.UploadedBy),''N/A'')AS UploadedBy,
        ISNULL(CONVERT(varchar,p.ExpiryDate,103),''N/A'')AS ExpiryDate,
        ISNULL(CONVERT(varchar,p.DocumentDate,103),''N/A'')AS DocumentDate
        '  
  
  -- End building SELECT clause  
  --========================================================================================          
  
  
  --========================================================================================      
  -- Begin building FROM clause  
  SET @fromClause = CHAR(10) + 'FROM P_Documents p'  
  SET @fromClause = @fromClause + CHAR(10) + 'INNER JOIN dbo.P_Documents_Type dt ON (dt.DocumentTypeId = p.DocumentTypeId)'  
  SET @fromClause = @fromClause + CHAR(10) + 'INNER JOIN dbo.P_Documents_Subtype ds ON (ds.DocumentSubtypeId = p.DocumentSubtypeId)'  
    
  -- End building From clause  
  --========================================================================================  
    
  --========================================================================================  
  -- Begin building SearchCriteria clause  
  -- These conditions will be added into where clause based on search criteria provided  
  
  SET @searchCriteria = ' 1=1  '  
  IF @propertyId != ''
	  BEGIN
	   SET @searchCriteria =@searchCriteria + CHAR(10) +'  AND PropertyId = ''' + @propertyId + ''' '  
	  END
	  
IF @schemeId > 0
	  BEGIN
	   SET @searchCriteria =@searchCriteria + CHAR(10) +'  AND SchemeId = ' + CONVERT(varchar(10), @schemeId) + ' '  
	  END	  
  IF @blockId >0
	  BEGIN
	   SET @searchCriteria =@searchCriteria + CHAR(10) +'  AND BlockId = ' +  CONVERT(varchar(10), @blockId) + ' '  
	  END
  -- End building SearchCriteria clause     
  --========================================================================================  
  
  --========================================================================================  
  -- Begin building WHERE clause  
  
  -- This Where clause contains subquery to exclude already displayed records       
   
  SET @whereClause = CHAR(10) + ' WHERE ' + CHAR(10) + @searchCriteria  
  
  -- End building WHERE clause  
  --========================================================================================  
  
  --========================================================================================      
  -- Begin building OrderBy clause    
  
  -- We are using this condition becuase row_number does not accept tableName.columnName. It only accepts alias  
  
  SET @orderClause = CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder  
  
  -- End building OrderBy clause  
  --========================================================================================  
  
  --========================================================================================  
  -- Begin building the main select Query  
  
  SET @mainSelectQuery = @selectClause + @fromClause + @whereClause + @orderClause  
  
  -- End building the main select Query  
  --========================================================================================                                     
  
  --========================================================================================  
  -- Begin building the row number query  
  
  SET @rowNumberQuery = '  SELECT *, row_number() over (order by ' + CHAR(10) + @sortColumn + CHAR(10) + @sortOrder + CHAR(10) + ') as row   
          FROM (' + CHAR(10) + @mainSelectQuery + CHAR(10) + ')AS Records'  
  
  -- End building the row number query  
  --========================================================================================  
  
  --========================================================================================  
  -- Begin building the final query   
  
  SET @finalQuery = ' SELECT *  
       FROM(' + CHAR(10) + @rowNumberQuery + CHAR(10) + ') AS Result   
       WHERE  
       Result.row between' + CHAR(10) + CONVERT(varchar(10), @offset) + CHAR(10) + 'and' + CHAR(10) + CONVERT(varchar(10), @limit)  
      
    
  -- End building the final query  
  --========================================================================================           
   
  --========================================================================================  
  -- Begin - Execute the Query   
  print(@finalQuery)  
  EXEC (@finalQuery)  
                           
  -- End - Execute the Query   
  --========================================================================================           
    
  --========================================================================================  
  -- Begin building Count Query   
    
  Declare @selectCount nvarchar(2000),   
  @parameterDef NVARCHAR(500)  
  
  SET @parameterDef = '@totalCount int OUTPUT';  
  SET @selectCount = 'SELECT  @totalCount = COUNT(DocumentId) ' + @fromClause + @whereClause  
  
  --print @selectCount  
  EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;  
      
  -- End building the Count Query  
  --========================================================================================  
  
END