-- =============================================    
-- Author:  <Author,Ahmed Mehmood>    
-- Create date: <Create Date,1/13/2015>    
-- Description: <Description,Update/Insert PDR_JOURNAL,PDR_APPOINTMENTS    
--Last modified Date:1/13/2015
---======================================================================    
    
-- =============================================    
CREATE PROCEDURE [dbo].[PDR_ScheduleMeWorkAppointment]    
 -- Add the parameters for the stored procedure here    
@userId int   
,@journalNotes varchar(1000)  
,@customerNotes varchar(1000)    
,@appointmentNotes varchar(1000)    
,@tenancyId int 
,@worksRequired  varchar(4000)   
,@appointmentStartDate date    
,@appointmentEndDate date    
,@startTime varchar(10)    
,@endTime varchar(10)    
,@operativeId int    
,@tradeId int     
,@journalId INT    
,@duration float    
,@isSaved int = 0 out    
,@appointmentIdOut int = -1 out 
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
             
DECLARE     
    
@ArrangedId int    
,@JournalHistoryId int    
,@AppointmentId int    
,@AppointmentHistoryId int    
    
    
BEGIN TRANSACTION;    
BEGIN TRY    

-- =============================================    
-- Get status id of "Arranged"    
-- =============================================     
	SELECT  @ArrangedId = PDR_STATUS.statusid 
	FROM	PDR_STATUS 
	WHERE	PDR_STATUS.title ='Arranged'    

-- ====================================================================================================    
--          UPDATE (PDR_JOURNAL)    
-- ====================================================================================================    

     
 UPDATE [PDR_JOURNAL]
 SET	STATUSID = @ArrangedId
	    ,CREATEDBY = @userId
	    ,CREATIONDATE = GETDATE()
	    ,NOTES = @journalNotes
 WHERE	JOURNALID = @journalId
     
 PRINT 'JOURNALID = '+ CONVERT(VARCHAR,@journalId )    

  
    
-- ====================================================================================================    
--          INSERTION (PLANNED_APPOINTMENTS)    
-- ====================================================================================================    
    
	SELECT	@JournalHistoryId = MAX(JOURNALHISTORYID)    
	FROM	PDR_JOURNAL_HISTORY    
	WHERE	JOURNALID = @journalId    
      
 PRINT 'JOURNALHISTORYID = '+ CONVERT(VARCHAR,@JournalHistoryId)  
    
    IF @tenancyId = -1
    BEGIN
		SET @tenancyId = null
    END
           
    INSERT INTO [PDR_APPOINTMENTS]
           ([TENANCYID]
           ,[JOURNALID]
           ,[JOURNALHISTORYID]
           ,[APPOINTMENTSTARTDATE]
           ,[APPOINTMENTENDDATE]
           ,[APPOINTMENTSTARTTIME]
           ,[APPOINTMENTENDTIME]
           ,[ASSIGNEDTO]
           ,[CREATEDBY]
           ,[LOGGEDDATE]
           ,[APPOINTMENTNOTES]
           ,[CUSTOMERNOTES]
           ,[APPOINTMENTSTATUS]
           ,[APPOINTMENTALERT]
           ,[APPOINTMENTCALENDAR]
           ,[TRADEID]
           ,[DURATION])
     VALUES
           (@tenancyId
           ,@journalId
           ,@JournalHistoryId
           ,@appointmentStartDate
           ,@appointmentEndDate
           ,@startTime
           ,@endTime
           ,@operativeId    
           ,@userId  
           ,GETDATE()
           ,@appointmentNotes
           ,@customerNotes
           ,'NotStarted'    
           ,NULL
           ,NULL
           ,@tradeId
           ,@duration)   
          
      SELECT @AppointmentId = SCOPE_IDENTITY()    
      PRINT 'APPOINTMENTID = '+ CONVERT(VARCHAR,@AppointmentId)            
     
	  SELECT @appointmentIdOut = @AppointmentId
          
END TRY    
BEGIN CATCH     
 IF @@TRANCOUNT > 0    
 BEGIN         
  ROLLBACK TRANSACTION;       
  SET @isSaved = 0            
 END    
 DECLARE @ErrorMessage NVARCHAR(4000);    
 DECLARE @ErrorSeverity INT;    
 DECLARE @ErrorState INT;    
    
 SELECT @ErrorMessage = ERROR_MESSAGE(),    
 @ErrorSeverity = ERROR_SEVERITY(),    
 @ErrorState = ERROR_STATE();    
    
 -- Use RAISERROR inside the CATCH block to return     
 -- error information about the original error that     
 -- caused execution to jump to the CATCH block.    
 RAISERROR (@ErrorMessage, -- Message text.    
    @ErrorSeverity, -- Severity.    
    @ErrorState -- State.    
   );    
END CATCH;    
    
IF @@TRANCOUNT > 0    
 BEGIN      
  COMMIT TRANSACTION;      
  SET @isSaved = 1    
 END    
  
    
END