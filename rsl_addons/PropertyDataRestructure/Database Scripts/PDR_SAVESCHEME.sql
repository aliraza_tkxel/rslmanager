-- Stored Procedure

/* =================================================================================    
    Page Description: SCHEME save page

    Author: Salman Nazir
    Creation Date: Dec-10-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-09-2014      Salman			SAVE SCHEME DATA
    ==============================================================================
    EXEC PDR_SAVESCHEME 'TEST',1,1
  =================================================================================*/
ALTER PROCEDURE [dbo].[PDR_SAVESCHEME]
@SCHEMENAME VARCHAR(50),
@SCHEMECODE VARCHAR(50),
@DEVELOPMENTID INT,
@PHASEID INT,
@existingSchemeId int= null,
@blockIds nvarchar(MAX) = null ,
@propertyIds nvarchar(MAX)= null,
@SCHEMEID varchar(200) OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	BEGIN TRANSACTION
	BEGIN TRY 	

IF @existingSchemeId <= 0 OR @existingSchemeId IS NULL 
	BEGIN
		IF Not Exists(select top 1 * from P_SCHEME Where SCHEMENAME = @SCHEMENAME AND DEVELOPMENTID= @DEVELOPMENTID )
				BEGIN
				INSERT INTO P_SCHEME(SCHEMENAME,SCHEMECODE,DEVELOPMENTID,PHASEID)
				VALUES (@SCHEMENAME,@SCHEMECODE,@DEVELOPMENTID,@PHASEID)

				SELECT @SCHEMEID = SCOPE_IDENTITY()
				END
			ELSE
			   BEGIN
			   SET @SCHEMEID='Exist'
			   END
	END
ELSE
	BEGIN
	Update P_SCHEME SET SCHEMENAME=@SCHEMENAME,SCHEMECODE=@SCHEMECODE,DEVELOPMENTID=@DEVELOPMENTID,PHASEID=@DEVELOPMENTID
	Where SchemeId=@existingSchemeId
	 SET @SCHEMEID=@existingSchemeId
	END	
IF @SCHEMEID !='Exist'
BEGIN	

	IF @blockIds IS NOT NULL AND @blockIds <> ''
		BEGIN
		SELECT * INTO #tempBlocks FROM dbo.SPLIT_STRING(@blockIds,',')


			UPDATE P_BLOCK SET SCHEMEID = NULL WHERE SCHEMEID = @SCHEMEID     
			UPDATE P_BLOCK SET SCHEMEID = @SCHEMEID WHERE BLOCKID IN ( SELECT COLUMN1 from #tempBlocks)
			DROP TABLE #tempBlocks
		END
	IF @propertyIds IS NOT NULL AND @propertyIds <> ''
		BEGIN
			SELECT * INTO #tempProperties FROM dbo.SPLIT_STRING(@propertyIds,',')
			--UPDATE P__PROPERTY SET SCHEMEID =NULL WHERE SCHEMEID =@SCHEMEID
			UPDATE P SET SCHEMEID =@SCHEMEID 
			From P__PROPERTY P
			INNER JOIN #tempProperties T ON P.PROPERTYID =CONVERT(NVARCHAR, T.COLUMN1) COLLATE SQL_Latin1_General_CP1_CI_AS
			--WHERE PROPERTYID IN (SELECT CONVERT(NVARCHAR(100), COLUMN1) from #tempProperties)
			DROP TABLE #tempProperties
		END   
	END	


END TRY 
	BEGIN CATCH 	
		IF @@TRANCOUNT >0	  
		BEGIN	   
			  PRINT 'Some thing went wrong with this transaction'
			SET @SCHEMEID='Failed'
			ROLLBACK TRANSACTION;	
		END 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
		@ErrorSeverity, -- Severity.
		@ErrorState -- State.
		);
	END CATCH  

IF @@TRANCOUNT >0
	BEGIN
		PRINT 'Transaction completed successfully'	
		COMMIT TRANSACTION;

	END 


END
GO