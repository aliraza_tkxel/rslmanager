-- =============================================
-- Author:		<Ahmed Mehmood>
-- Create date: <29/10/2015>
-- Description: Added attributeTypeId to differentiate provision and attributed items in PDR_MSAT
-- =============================================


ALTER TABLE PDR_MSAT
ADD attributeTypeId int


-- UPDATING OLD VALUES
DECLARE @attributeTypeId INT
SELECT  @attributeTypeId = PDR_AttributesType.AttributeTypeId
FROM	PDR_AttributesType
WHERE	PDR_AttributesType.AttributeType = 'Attribute'

UPDATE PDR_MSAT
SET PDR_MSAT.attributeTypeId = @attributeTypeId
WHERE PDR_MSAT.ItemId IS NOT NULL
