USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetDevelopmentList]    Script Date: 22/04/2018 23:09:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,03/12/2014>
-- Description:	<Description,,Get Development List on DevelopmentList.aspx>
-- 
-- =============================================
ALTER PROCEDURE [dbo].[PDR_GetDevelopmentList]
	-- Add the parameters for the stored procedure here
		@searchText VARCHAR(200),
		@searchCompany VARCHAR(2) ,
	--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'DEVELOPMENTNAME', 
		@sortOrder varchar (5) = 'DESC',
		@totalCount int = 0 output	
AS
BEGIN
	DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(2000),
        
        --variables for paging
        @offset int,
		@limit int
		
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		
		
		--=====================Search Criteria===============================
		SET @searchCriteria = ' 1=1 '
		
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND ( DEVELOPMENTNAME  LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR Town LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR S.SchemeName LIKE ''%' + @searchText + '%'') '
		END	

		IF(@searchCompany != '-1' OR @searchCompany != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND D.COMPANYID  = ' + @searchCompany + ' '
		END	
		
		--=======================Select Clause=============================================
		SET @SelectClause = 'Select top ('+convert(varchar(10),@limit)+')
		D.DEVELOPMENTID as DEVELOPMENT,
		ISNULL(DEVELOPMENTNAME,'''') AS DEVELOPMENTNAME,
		ISNULL(Town,''-'') AS TOWN,
		ISNULL(DT.DESCRIPTION,''-'') as DevelopmentType, 
		convert(varchar(10), PurchaseDate, 103) PurchaseDate,
		ISNULL(S.SchemeName,''-'') as Scheme'
		
		
		--============================From Clause============================================
		SET @fromClause = CHAR(10) +' from PDR_DEVELOPMENT D
							LEFT Join P_DEVELOPMENTTYPE DT on DT.DEVELOPMENTTYPEID = D.DEVELOPMENTTYPE
							LEFT JOIN P_SCHEME S on S.DevelopmentId = D.DEVELOPMENTID'
							
		--============================Order Clause==========================================
		IF(@sortColumn = 'DEVELOPMENTNAME')
		BEGIN
			SET @sortColumn = CHAR(10)+ ' DEVELOPMENT' 	
			
		END
		
		IF(@sortColumn = 'Town')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Town' 	
			
		END
		IF(@sortColumn = 'Scheme')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Scheme' 	
			
		END
				
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		--===============================Main Query ====================================
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
		print(@finalQuery)
		EXEC (@finalQuery)
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
											
END
