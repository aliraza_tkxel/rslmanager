USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Ahmed Mehmood>
-- Create date: <Create Date,,29-Oct-2015>
-- Description:	<Description,,Get Overdue count on Maintenance Dashboard>
--DECLARE	@return_value int,
--		@totalCount int

--EXEC	@return_value = [dbo].[PDR_GetMaintenanceOverdueCount]
--		@schemeId = -1,
--		@blockId = -1,
--		@maintenanceType = -1,
--		@pageSize = 30,
--		@pageNumber = 1,
--		@sortColumn = N'Address',
--		@sortOrder = N'desc',
--		@getOnlyCount = 0,
--		@totalCount = @totalCount OUTPUT
-- =============================================
CREATE PROCEDURE [dbo].[PDR_GetMaintenanceOverdueCount]
		@schemeId int =-1,
		@blockId int =-1,
		@maintenanceType int =-1,
	    --Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'Address', 
		@sortOrder varchar (5) = 'DESC',
		@getOnlyCount bit=0,
		@totalCount int = 0 output	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

		
		DECLARE @selectClause varchar(3000),
		@fromClause   varchar(3000),
		@whereClause  varchar(2000),
		@mainQuery varchar(7000),
		@searchCriteria varchar(3000),
		@rowNumberQuery varchar(7000),
		@finalQuery varchar(7000),
		@orderClause  varchar(2000),
		@offset int,
		@limit int,
		@mainSelectQuery varchar(7000)
		
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
        		
		--========================================================================================
		-- Begin building SearchCriteria clause
		-- These conditions will be added into where clause based on search criteria provided
		
		SET @searchCriteria = ' 1=1 '
				
		IF(@schemeId > 0)
		BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND ( PDR_MSAT.SchemeId = ' + CONVERT(VARCHAR,@schemeId) +''			
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR P__PROPERTY.SCHEMEID = ' + CONVERT(VARCHAR,@schemeId) + ')'
		END 
		
		IF(@blockId > 0)
		BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND ( PDR_MSAT.blockId = ' + CONVERT(VARCHAR,@blockId) +''			
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR P__PROPERTY.BLOCKID = ' + CONVERT(VARCHAR,@blockId) + ')'
		END
		
		IF(@maintenanceType <> -1 )
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND PDR_MSAT.attributeTypeId = ' + CONVERT(NVARCHAR(10), @maintenanceType)
		END	
		ELSE
		BEGIN
		    SET @searchCriteria = @searchCriteria + CHAR(10) +' AND PDR_MSAT.attributeTypeId IS NOT NULL '
		END 
				
	
		--These conditions w�ll be used in every case
		
		SET @searchCriteria = @searchCriteria + CHAR(10) + 'AND CURRENT_TIMESTAMP > PDR_MSAT.NextDate
															AND PDR_STATUS.TITLE IN (''To be Arranged'',''Cancelled'')
															AND PDR_MSATType.MSATTypeName = ''Cyclic Maintenance'' AND PDR_MSAT.IsRequired = 1 '
	
		-- End building SearchCriteria clause   
		--========================================================================================
   
   		--======================= SELECT Clause =================================
		SET @selectClause = 'SELECT top ('+convert(varchar(10),@limit)+')
			CASE
			WHEN	PDR_MSAT.PROPERTYID IS not null THEN
					ISNULL(P__PROPERTY.ADDRESS1,'''')+'' ''+ISNULL(P__PROPERTY.ADDRESS2,'''')+'' ''+ISNULL(P__PROPERTY.ADDRESS3,'''') 
			ELSE 
					ISNULL(P_BLOCK.ADDRESS1,'''')+'' ''+ISNULL(P_BLOCK.ADDRESS2,'''')+'' ''+ISNULL(P_BLOCK.ADDRESS3,'''') 
			END as [Address]
			,ISNULL(PDR_STATUS.TITLE,'' '') as [StatusTitle]
			,PDR_MSAT.NextDate as [NextDate]' + CHAR(10)
   
   
		-- Begin building FROM clause
		
		SET @fromClause = CHAR(10) + ' FROM	PDR_JOURNAL
				INNER JOIN PDR_MSAT ON PDR_JOURNAL.MSATId = PDR_MSAT.MSATId
				INNER JOIN PDR_STATUS ON PDR_JOURNAL.STATUSID = PDR_STATUS.STATUSID	
				INNER JOIN	PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId
				LEFT JOIN	P_SCHEME ON PDR_MSAT.SchemeId = P_SCHEME.SCHEMEID 
				LEFT JOIN	P_BLOCK ON PDR_MSAT.BlockId = P_BLOCK.BLOCKID
				LEFT JOIN	P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID
				 '
		
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		--============================ Order Clause ==========================================
				
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		--===============================Main Query ====================================
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
		IF(@getOnlyCount=0)
		BEGIN
			print(@finalQuery)
			EXEC (@finalQuery)
		END
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(DISTINCT PDR_JOURNAL.MSATID) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
	
END
