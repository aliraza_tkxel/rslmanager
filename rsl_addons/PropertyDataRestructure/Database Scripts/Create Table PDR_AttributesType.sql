/*
   Wednesday, October 21, 20153:05:23 PM
   User: tkxel
   Server: 10.0.1.75
   Database: RSLBHALive
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.PDR_AttributesType
	(
	AttributeTypeId int NOT NULL,
	AttributeType nvarchar(50) NULL,
	IsActive bit NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.PDR_AttributesType ADD CONSTRAINT
	PK_PDR_AttributesType PRIMARY KEY CLUSTERED 
	(
	AttributeTypeId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.PDR_AttributesType SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.PDR_AttributesType', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.PDR_AttributesType', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.PDR_AttributesType', 'Object', 'CONTROL') as Contr_Per 