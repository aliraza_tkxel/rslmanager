-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:           Salman Nazir
-- Create date:      03/07/2015
-- Description:      Get Void PDF Document Info
-- History:          
-- =============================================
CREATE PROCEDURE GetVoidPDFDocumentInfo
@stageId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select P.address1+' '+P.address2 as [Address],P.postCode,C.FirstName+' '+C.LastName as Customer ,TenantFwCity+' '+TenantFwAddress1+' '+TenantFwAddress2 as TenantAddress , DateOccupancyCease , 
GasMeterReading,GasMeterReadingDate,ElectricMeterReading,ElectricMeterReadingDate,GasDebtAmount,GasDebtAmountDate,ElectricDebtAmount,ElectricDebtAmountDate,NewTenantName,
NewTenantPreviousAddress1+' '+NewTenantPreviousAddress2+' '+NewTenantPreviousTownCity as PreviousAddress, NewTenantOccupancyDate as DateOccupancy,NewGasMeterReading as NewGasMeterReading,
NewElectricMeterReading, CONVERT(VARCHAR,NewTenantDateOfBirth)+' '+NewTenantTel+' '+NewTenantMobile as OtherInfo
from V_BritishGasVoidNotification B
INNER JOIN P__PROPERTY P on P.PropertyId = B.PropertyId
INNER JOIN C__Customer C on C.CustomerId = B.CustomerId
INNER JOIN C_Tenancy T on T.TenancyId = B.TenancyId
INNER JOIN C_CustomerTenancy CT on CT.customerId = C.CustomerID
Where B.StageId = @stageId
End

GO
