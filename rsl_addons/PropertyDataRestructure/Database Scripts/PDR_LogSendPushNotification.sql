-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,02-Feb-2015>
-- Description:	<Description,,Logged notification send message>
-- EXEC PDR_LogSendPushNotification
-- =============================================
CREATE PROCEDURE PDR_LogSendPushNotification
@documentId int,
@documentTitle varchar(200),
@userId int,
@status varchar(50),
@message varchar(500)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO PDR_DocumentExpiryNotificationLog
	(DocumentId,DocumentTitle,UserId,NotificationDate,Status,Message)
	VALUES
	(@documentId,@documentTitle,@userId,GETDATE(),@status,@message)
	
END
GO
