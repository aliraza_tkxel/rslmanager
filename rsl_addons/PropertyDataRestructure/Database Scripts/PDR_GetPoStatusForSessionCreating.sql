USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetPoStatusForSessionCreating]    Script Date: 12/14/2015 11:59:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Raja Aneeq>
-- Create date: <11/12/2015>
-- Description:	<Get PO status for opening PO link>
-- Web Page:	<Email to schedular>
-- EXEC PDR_GetPoStatusForSessionCreating  265606
-- =============================================
ALTER PROCEDURE [dbo].[PDR_GetPoStatusForSessionCreating]
	@orderId int
AS
BEGIN


 SELECT Description As PoStatus
 FROM FL_FAULT_STATUS WHERE faultstatusid =  (select statusid from   FL_FAULT_LOG 
 WHERE FaultLogID = (select FL.FaultLogId from FL_FAULT_JOURNAL J
 INNER JOIN FL_FAULT_LOG FL ON  J.FaultLogId = FL.FaultLogId
 INNER JOIN  FL_CONTRACTOR_WORK  CW   ON CW.JournalId  =J.JournalId
 
  WHERE CW.PURCHASEORDERID =  @orderId))
	
END
