USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetBlockTemplates]    Script Date: 12/23/2014 15:21:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,04/12/2014>
-- Description:	<Description,,Get Block Templates>
-- EXEC PDR_GetBlockTemplates 
-- =============================================
CREATE PROCEDURE [dbo].[PDR_GetBlockTemplates] 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select BLOCKID,BLOCKNAME from P_BLOCK Where IsTemplate = 1
END
