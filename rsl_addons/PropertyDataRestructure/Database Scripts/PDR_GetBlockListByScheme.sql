/* =================================================================================    
    Page Description:  Get Blocks By SchemeID for dropdown
 
    Author: Ali Raza
    Creation Date:  4-11-2015 

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Nov-04-2015      Ahmed Mehmood           Get Blocks By Scheme for dropdown
    
    Execution Command:
    
    Exec PDR_GetBlockListByScheme  2
  =================================================================================*/
ALTER PROCEDURE PDR_GetBlockListByScheme
	@SchemeId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @mainQuery NVARCHAR(MAX),
	@searchCriteria NVARCHAR(MAX),
	@orderByClause NVARCHAR(MAX)
	
	SET @searchCriteria = '1=1 '
	
	IF(@SchemeId <> -1 )
	    SET @searchCriteria = @searchCriteria + CHAR(10) +' AND P_BLOCK.schemeid = ' + CONVERT(NVARCHAR(10), @schemeId)
    
    SET @orderByClause = 'ORDER BY BLOCKNAME ASC '

    SET @mainQuery = 'SELECT	BLOCKID,BLOCKNAME  FROM	P_BLOCK WHERE ' + @searchCriteria + @orderByClause
    
    EXEC (@mainQuery)
END
GO
