USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_PORTFOLIOANALYSIS_REPORT_VOID_ANALYSIS_DETAIL]    Script Date: 02/13/2015 15:45:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    Page Description:      Get Void Detail Portfolio Analysis detail
 
    Author: Ali Raza
    Creation Date: Dec-22-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-22-2014      Ali Raza           Get Void Detail Portfolio Analysis detail
    
    Execution Command:
    
    Exec PDR_PORTFOLIOANALYSIS_REPORT_VOID_ANALYSIS_DETAIL
  =================================================================================*/
CREATE PROCEDURE [dbo].[PDR_PORTFOLIOANALYSIS_REPORT_VOID_ANALYSIS_DETAIL]
	-- Add the parameters for the stored procedure here
		 @REPORTDATE SMALLDATETIME,  
		 @LOCALAUTHORITY INT = NULL,  
		 @SCHEME INT = NULL,  
		 @POSTCODE NVARCHAR(15) = NULL,  
		 @PATCH INT = NULL,  
		 @STATUS INT = NULL,  
		 @ASSETTYPE INT = NULL,  
		 @PERIOD INT = NULL,
	--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'ASSETTYPEID', 
		@sortOrder varchar (5) = 'DESC',
		@totalCount int = 0 output	
AS
BEGIN
	DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @SelectClauseUnion varchar(3000),
        @fromClauseUnion   varchar(3000),
        @whereClause  varchar(8000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(MAX),        
        @rowNumberQuery varchar(MAX),
        @finalQuery varchar(MAX),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(6000),
        
        --variables for paging
        @offset int,
		@limit int
		
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		
		
		--=====================Search Criteria===============================
		SET @searchCriteria = ' 1=1 '
			--SET @searchCriteria = @searchCriteria + CHAR(10) +'AND (1=1 '	
				
				IF @STATUS IS NOT NULL 
				BEGIN
				SET @searchCriteria = @searchCriteria + CHAR(10) +'AND((S.NEWSTATUS = '+convert(varchar(10),@STATUS)+' OR '+convert(varchar(10),@STATUS)+' IS NULL)'+' OR (S.NEWSTATUS=2 AND S.NEWSUBSTATUS=22))   '
				END
				IF @ASSETTYPE IS NOT NULL 
				BEGIN
				SET @searchCriteria = @searchCriteria + CHAR(10) +'AND(P.ASSETTYPE = '+convert(varchar(10),@ASSETTYPE)+' OR '+convert(varchar(10),@ASSETTYPE)+' IS NULL)'
				END
				IF @LOCALAUTHORITY > 0
				BEGIN
				SET @searchCriteria = @searchCriteria + CHAR(10) +'AND (D.LOCALAUTHORITY = '+convert(varchar(10),@LOCALAUTHORITY)+' OR '+convert(varchar(10),@LOCALAUTHORITY)+' IS NULL)'
				END
				IF @SCHEME > 0 
				BEGIN
				SET @searchCriteria = @searchCriteria + CHAR(10) +'AND	(D.DEVELOPMENTID = '+convert(varchar(10),@SCHEME)+' OR '+convert(varchar(10),@SCHEME)+' IS NULL)'
			    END
			    IF @POSTCODE IS NOT NULL 
				BEGIN
			    SET @searchCriteria = @searchCriteria + CHAR(10) +	'AND (P.POSTCODE LIKE '''+@POSTCODE+''' OR '''+@POSTCODE+''' IS NULL)'
				END
			    IF @PATCH > 0
				BEGIN
				SET @searchCriteria = @searchCriteria + CHAR(10) +'AND (D.PATCHID = '+convert(varchar(10),@PATCH) +' OR '+convert(varchar(10),@PATCH)+' IS NULL)'
				END
				SET @searchCriteria = @searchCriteria + CHAR(10) +'AND (  
   CASE  
    WHEN '+convert(varchar(10),@PERIOD)+'=0 AND (NOT (S.NEWSTATUS = 2 AND S.NEWSUBSTATUS = 22)) AND DATEDIFF(d, S.CTIMESTAMP, ''' + convert(varchar(20),@REPORTDATE) + ''') >= 0 THEN 1  
    WHEN '+convert(varchar(10),@PERIOD)+'=1 AND (S.NEWSTATUS = 2 AND S.NEWSUBSTATUS = 22) THEN 1   
    WHEN '+convert(varchar(10),@PERIOD)+'=2 AND (NOT (S.NEWSTATUS = 2 AND S.NEWSUBSTATUS = 22) AND DATEDIFF(ww, S.CTIMESTAMP, ''' + convert(varchar(20),@REPORTDATE) + ''') < 3) THEN 1  
    WHEN '+convert(varchar(10),@PERIOD)+'=3 AND (NOT (S.NEWSTATUS = 2 AND S.NEWSUBSTATUS = 22) AND DATEDIFF(ww, S.CTIMESTAMP, ''' + convert(varchar(20),@REPORTDATE) + ''') >= 3 AND DATEDIFF(ww, S.CTIMESTAMP, ''' + convert(varchar(20),@REPORTDATE) + ''') < 6)  THEN 1  
    WHEN '+convert(varchar(10),@PERIOD)+'=4 AND (NOT (S.NEWSTATUS = 2 AND S.NEWSUBSTATUS = 22) AND DATEDIFF(ww, S.CTIMESTAMP, ''' + convert(varchar(20),@REPORTDATE) + ''') >= 6 AND DATEDIFF(m, S.CTIMESTAMP, ''' + convert(varchar(20),@REPORTDATE) + ''') < 6)  THEN 1   
    WHEN '+convert(varchar(10),@PERIOD)+'=5 AND (NOT (S.NEWSTATUS = 2 AND S.NEWSUBSTATUS = 22) AND DATEDIFF(m, S.CTIMESTAMP, ''' + convert(varchar(20),@REPORTDATE) + ''') >= 6 AND DATEDIFF(m, S.CTIMESTAMP, ''' + convert(varchar(20),@REPORTDATE) + ''') < 12)  THEN 1   
    WHEN '+convert(varchar(10),@PERIOD)+'=6 AND (NOT (S.NEWSTATUS = 2 AND S.NEWSUBSTATUS = 22) AND DATEDIFF(m, S.CTIMESTAMP, ''' + convert(varchar(20),@REPORTDATE) + ''') >= 12) THEN 1   
    ELSE 0   
   END ) = 1  '			
				

		--=======================Select Clause=============================================
		SET @SelectClause = 'Select top ('+convert(char(10),@limit)+')		 
			 AT.ASSETTYPEID, AT.DESCRIPTION AS DESCRIPTION,  
  SCH.SCHEMENAME as SCHEMENAME ,P.PROPERTYID,Convert(Varchar,S.CTIMESTAMP,103) AS ENDDATE,ISNULL(FH.TOTALRENT,0) AS TOTALRENT ,ISNULL(FH.TOTALRENT,0) * ISNULL(DATEDIFF(m, S.CTIMESTAMP, ''' + convert(varchar(20),@REPORTDATE) + '''),0) AS ACCUMULATEDRENT,DATEDIFF(d, S.CTIMESTAMP, ''' + convert(varchar(20),@REPORTDATE) + ''')   AS NUMBOFDAYSVOID,  
  ISNULL(FH.RENT,0) AS RENT, ISNULL(FH.SERVICES,0) AS SERVICES, ISNULL(FH.INELIGSERV,0) AS INELIGSERV, ISNULL(FH.SUPPORTEDSERVICES,0) AS SUPPORTEDSERVICES, ISNULL(FH.WATERRATES,0) AS WATERRATES, ISNULL(FH.COUNCILTAX,0) AS COUNCILTAX, ISNULL(FH.GARAGE,0)   AS GARAGE,  
  LEFT(REPLACE(ISNULL(P.HOUSENUMBER,'''') + '' '' + ISNULL(P.ADDRESS1,'''') + '', '' + ISNULL(P.ADDRESS2,'''') + '', '' + ISNULL(P.TOWNCITY,''''), '', ,'', '', ''),50) AS ADDRESS   '
		

		--============================From Clause============================================
		SET @fromClause = '
		FROM P__PROPERTY P  
  LEFT JOIN P_ASSETTYPE AT ON P.ASSETTYPE = AT.ASSETTYPEID  
  
  INNER JOIN P_SCHEME SCH ON P.SchemeId = SCH.SchemeId	
  INNER JOIN (SELECT DEVELOPMENTID,DEVELOPMENTNAME,LOCALAUTHORITY,PATCHID FROM PDR_DEVELOPMENT) D ON SCH.DEVELOPMENTID = D.DEVELOPMENTID
  LEFT JOIN P_STATUSCHANGEHISTORY S ON S.PROPERTYID = P.PROPERTYID   
   AND S.CHANGEID = (SELECT MAX(CHANGEID) FROM P_STATUSCHANGEHISTORY WHERE PROPERTYID = P.PROPERTYID AND convert(smalldatetime,convert(char(12),CTIMESTAMP),103) <= ''' + convert(varchar(20),@REPORTDATE) + ''')  
   AND ISNULL(S.NEWSUBSTATUS,0)<>1 -- EXCLUDES UNDERCONSTRUCTION PROPERTIES  
  LEFT JOIN (SELECT FF.PROPERTYID,FF.RENT,FF.SERVICES,FF.INELIGSERV,FF.SUPPORTEDSERVICES,FF.WATERRATES,FF.COUNCILTAX,FF.GARAGE,FF.TOTALRENT  
       FROM P_FINANCIAL_HISTORY FF   
       WHERE FF.PFTIMESTAMP =(  
          SELECT MAX(PFTIMESTAMP)   
          FROM P_FINANCIAL_HISTORY    
          WHERE PROPERTYID = FF.PROPERTYID   
          AND PFTIMESTAMP >= ''' + convert(varchar(20),@REPORTDATE) + '''
          )   
  
  
     UNION   
     SELECT  F.PROPERTYID,F.RENT,F.SERVICES,F.INELIGSERV,F.SUPPORTEDSERVICES,F.WATERRATES,F.COUNCILTAX,F.GARAGE,F.TOTALRENT  
     FROM P_FINANCIAL F  
     LEFT JOIN P_FINANCIAL_HISTORY PFH ON PFH.PROPERTYID=F.PROPERTYID  
       AND  PFH.PFTIMESTAMP = (  
               SELECT MAX(PFTIMESTAMP)   
               FROM P_FINANCIAL_HISTORY    
               WHERE PROPERTYID = PFH.PROPERTYID   
              )   
       AND PFH.PFTIMESTAMP < ''' + convert(varchar(20),@REPORTDATE) + '''  
      WHERE PFH.PFTIMESTAMP IS NOT NULL  
      ) FH ON FH.PROPERTYID = P.PROPERTYID  '
							
			
		--=======================Select Clause=============================================
		SET @SelectClauseUnion = 'SELECT   
  NULL, NULL,''TOTAL''AS SCHEMENAME,NULL,NULL,SUM(ISNULL(FH.TOTALRENT,0)) AS TOTALRENT ,SUM(ISNULL(FH.TOTALRENT,0) * ISNULL(DATEDIFF(m, S.CTIMESTAMP, ''' + convert(varchar(20),@REPORTDATE) + '''),0)) AS ACCUMULATEDRENT,SUM(DATEDIFF(d, S.CTIMESTAMP, ''' + convert(varchar(20),@REPORTDATE) + ''')) AS NUMBOFDAYSVOID,  
  SUM(ISNULL(FH.RENT,0)) AS RENT,SUM(ISNULL(FH.SERVICES,0)) AS SERVICES, SUM(ISNULL(FH.INELIGSERV,0)) AS INELIGSERV, SUM(ISNULL(FH.SUPPORTEDSERVICES,0)) AS SUPPORTEDSERVICES, SUM(ISNULL(FH.WATERRATES,0)) AS WATERRATES, SUM(ISNULL(FH.COUNCILTAX,0))AS COUNCILTAX, SUM(ISNULL(FH.GARAGE,0)) AS GARAGE,NULL  '
		
		
							
		--============================Order Clause==========================================
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		--===============================Main Query ====================================
		Set @mainSelectQuery = @selectClause +@fromClause +CHAR(10)+ @whereClause  +CHAR(10)+ @orderClause +CHAR(10)+  ' UNION ALL ' + CHAR(10)+CHAR(10) + @SelectClauseUnion + CHAR(10)+CHAR(10) + @fromClause + CHAR(10)+ @whereClause 
		
		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+ CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
		print(@finalQuery)
		EXEC (@finalQuery)
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(MAX), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= '
		SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
											
END
