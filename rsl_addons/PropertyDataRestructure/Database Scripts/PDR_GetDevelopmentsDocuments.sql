USE [RSLBHALive]

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    Page Description:     Get Documents for Documents Grid on New Development Page
 
    Author: Ahmed Mehmood
    Creation Date: Feb-26-2015   
    Execution Command:
    
    Exec PDR_GetDevelopmentsDocuments
  =================================================================================*/
CREATE PROCEDURE [dbo].[PDR_GetDevelopmentsDocuments]
@developmentId int = -1
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   		Select distinct DocumentId
		From P_DEVELOPMENTDOCUMENTS
		WHERE DevelopmentId = @developmentId
END

