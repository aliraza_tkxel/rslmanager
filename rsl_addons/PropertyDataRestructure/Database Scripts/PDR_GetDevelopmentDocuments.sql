-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,02/12/2014>
-- Description:	<Description,,Get Development Document on SaveDevelopment.aspx>
-- EXEC PDR_GetDevelopmentDocuments 1
-- =============================================
CREATE PROCEDURE PDR_GetDevelopmentDocuments
@developmentId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select D.DocumentID, Title, Expires, FileType 
	From PDR_Documents D
	INNER JOIN P_DEVELOPMENTDOCUMENTS DC on D.DocumentId = DC.DocumentId
	Where DC.DevelopmentId = @developmentId
END
GO
