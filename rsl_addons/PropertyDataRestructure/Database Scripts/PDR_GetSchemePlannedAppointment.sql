USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetPatch]    Script Date: 12/24/2014 18:37:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    Page Description:     Get Planned appointment for scheme dashboard
 
    Author: Ali Raza
    Creation Date: Dec-18-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-18-2014      Ali Raza           Get Planned appointment for scheme dashboard
    
    Execution Command:
    
    Exec PDR_GetSchemePlannedAppointment 1
  =================================================================================*/
CREATE PROCEDURE [dbo].[PDR_GetSchemePlannedAppointment]
	@SchemeID int
AS
BEGIN
	
 SET NOCOUNT ON;    
 SELECT DISTINCT PLANNED_APPOINTMENTS.APPOINTMENTID ,ISNULL(PLANNED_COMPONENT.COMPONENTNAME,'Miscellaneous works') + ' Replacement' as Component   
  , CONVERT(VARCHAR(11), PLANNED_APPOINTMENTS.APPOINTMENTDATE  , 106) AppointmentDate  
 FROM PLANNED_APPOINTMENTS  
  INNER JOIN PLANNED_JOURNAL ON PLANNED_APPOINTMENTS.JournalId = PLANNED_JOURNAL.JOURNALID  
  INNER JOIN PLANNED_STATUS ON  PLANNED_JOURNAL.STATUSID =  PLANNED_STATUS.STATUSID  
  LEFT JOIN PLANNED_COMPONENT ON  PLANNED_JOURNAL.COMPONENTID = PLANNED_COMPONENT.COMPONENTID  
 WHERE PLANNED_JOURNAL.SchemeID = @SchemeID AND PLANNED_STATUS.TITLE = 'Arranged'    
 ORDER BY  PLANNED_APPOINTMENTS.APPOINTMENTDATE DESC  
END
