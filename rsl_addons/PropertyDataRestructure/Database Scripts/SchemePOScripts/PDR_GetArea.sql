USE [RSLBHALive]
GO

/****** Object:  StoredProcedure [dbo].[PDR_GetArea]    Script Date: 12/30/2015 22:15:03 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/* =================================================================================    
    Page Description: Get Area for Dop down list over scheme/Block Assign to contractor PopUp 
 
    Author: Shaheen
    Creation Date: Dec-24-2015

    
    Execution Command: Exec PDR_GetArea
  =================================================================================*/
CREATE PROCEDURE [dbo].[PDR_GetArea]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

SELECT  AreaID AS [id], PA_LOCATION.LocationName+' > '+PA_AREA.AreaName AS [description] 

FROM PA_AREA 
INNER JOIN PA_LOCATION ON PA_AREA.LocationId = PA_LOCATION.LocationID
WHERE PA_AREA.IsActive = 1 
ORDER BY PA_LOCATION.LocationID  ASC
END

GO


