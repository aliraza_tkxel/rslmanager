USE [RSLBHALive]
GO

/****** Object:  StoredProcedure [dbo].[PDR_GetAllContractors]    Script Date: 12/31/2015 12:26:05 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Shaheen 
-- Create date: 23/12/2015
-- Description:	To get contractors
-- =============================================
CREATE PROCEDURE [dbo].[PDR_GetAllContractors] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT O.ORGID AS [id],
			NAME AS [description]
	FROM S_ORGANISATION O
	INNER JOIN S_SCOPE S ON O.ORGID = S.ORGID
	INNER JOIN S_SCOPETOPATCHANDSCHEME SS ON S.SCOPEID = SS.SCOPEID 
	INNER JOIN S_AREAOFWORK AoW ON S.AREAOFWORK = AoW.AREAOFWORKID 
END

GO


