USE [RSLBHALive]
GO

/****** Object:  StoredProcedure [dbo].[PDR_GetContractDetailByContractorIdForAssignToContractor]    Script Date: 12/30/2015 22:18:29 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[PDR_GetContractDetailByContractorIdForAssignToContractor] 
	-- Add the parameters for the stored procedure here	 
	 @contractorId int
	,@schemeId int
	,@blockId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT S.STARTDATE StartDate, S.RENEWALDATE EndDate
	FROM S_ORGANISATION O
	INNER JOIN S_SCOPE S ON O.ORGID = S.ORGID
	INNER JOIN S_SCOPETOPATCHANDSCHEME SPC ON S.SCOPEID = SPC.SCOPEID
	WHERE O.ORGID = @contractorId
	AND (SPC.SCHEMEID = @schemeId OR SPC.BLOCKID = @blockId ) 
	
END
GO


