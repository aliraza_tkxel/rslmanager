USE [RSLBHALive]
GO

/****** Object:  StoredProcedure [dbo].[PDR_GetAttributesSubItemsByAreaID]    Script Date: 12/30/2015 22:16:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Shaheen
-- Create date: 12/28/2015
-- Description:	Get Concatenated SubItems in Items By AreaId
-- =============================================
CREATE PROCEDURE [dbo].[PDR_GetAttributesSubItemsByAreaID]
( @areaId INT)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
SET NOCOUNT ON;
SELECT   PA_ITEM.ItemID AS [id], PA_AREA.AreaName +' > '+PA_ITEM.ItemName AS [description] 
FROM PA_AREA 
INNER JOIN PA_ITEM ON PA_ITEM.AreaID = PA_AREA.AreaID 
WHERE PA_AREA.IsActive = 1 AND PA_AREA.AREAID=@areaId   AND NOT EXISTS
(Select I.ItemId as [id], P.description +' > '+I.ItemName AS [description]
FROM
(SELECT  PA_AREA.LocationID, PA_ITEM.ItemID AS [id], PA_AREA.AreaName +' > '+PA_ITEM.ItemName AS [description] 
FROM PA_AREA 
INNER JOIN PA_ITEM ON PA_ITEM.AreaID = PA_AREA.AreaID 
WHERE PA_AREA.IsActive = 1  and PA_ITEM.ParentItemId IS NULL AND PA_AREA.AREAID=@areaId) AS P 
INNER JOIN PA_ITEM I On I.ParentItemId =  P.id
)
UNION 
    -- Insert statements for procedure here
Select I.ItemId as [id], P.description +' > '+I.ItemName AS [description]

FROM
(SELECT  PA_AREA.LocationID, PA_ITEM.ItemID AS [id], PA_AREA.AreaName +' > '+PA_ITEM.ItemName AS [description] 
FROM PA_AREA 
INNER JOIN PA_ITEM ON PA_ITEM.AreaID = PA_AREA.AreaID
WHERE PA_AREA.IsActive = 1  and PA_ITEM.ParentItemId IS NULL) AS P 
INNER JOIN PA_ITEM I On I.ParentItemId =  P.id
WHERE I.AreaID = @areaId

END

GO


