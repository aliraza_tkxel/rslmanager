USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_Cp12SaveDocument]    Script Date: 12/14/2015 18:24:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Author,,Name>  
-- Create date: <Create Date,,>  
-- Description: <Description,,>  


-- =============================================  
ALTER PROCEDURE [dbo].[AS_Cp12SaveDocument]  
 @propertyId as varchar(20),  
 @journalId as int,  
 @cp12Doc as image,
 @updatedBy as varchar(20)
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
 --Select CP12Document From P_LGSR where propertyId= @propertyId and journalId= @journalId  
   DECLARE @hisId int
 UPDATE [P_LGSR]  
 SET   
      --,[CP12NUMBER] = <CP12NUMBER, nvarchar(20),>  
      --,[CP12ISSUEDBY] = <CP12ISSUEDBY, int,>        
      [CP12DOCUMENT] = @cp12Doc  
        
 WHERE propertyId= @propertyId and journalId= @journalId  
 
 UPDATE P_LGSR_HISTORY SET CP12DOCUMENT = @cp12Doc ,CP12UPLOADEDBY = @updatedBy
 WHERE LGSRHISTORYID=(SELECT MAX(LGSRHISTORYID) FROM P_LGSR_HISTORY WHERE PROPERTYID=@propertyId)
 
END  