-- Stored Procedure

-- =============================================
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,02/12/2014>
-- Description:	<Description,,Save Development docs on SaveDevelopment.aspx>
-- EXEC PDR_SaveDevelopmentDocument -1,-1
-- =============================================
ALTER PROCEDURE [dbo].[PDR_SaveDevelopmentDocument]
@DevelopmentId int,
@docId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO P_DEVELOPMENTDOCUMENTS (DevelopmentId,DocumentId )
	Values (@developmentId,@docId)
END
GO