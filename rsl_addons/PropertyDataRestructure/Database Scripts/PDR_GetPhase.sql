USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetPhase]    Script Date: 02/03/2015 23:42:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,01/12/2014>
-- Description:	<Description,,Populate Phase dropdown on SAveDevelopment.aspx>
-- EXEC PDR_GetPhase 3
-- =============================================
CREATE PROCEDURE [dbo].[PDR_GetPhase] 
@PhaseId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    IF @PhaseId = -1
		BEGIN
			SELECT * FROM P_PHASE
		END
	ELSE
		BEGIN
			SELECT p.PHASEID,ISNULL(PhaseName,'') as PhaseName,ConstructionStart,AnticipatedCompletion,ActualCompletion,HandoverintoManagement,ISNULL(SCHEMEID,'') as SCHEMEID,ISNULL(SCHEMENAME,'') as SchemeName FROM P_PHASE P 
			LEFT JOIN P_SCHEME S on S.PHASEID = P.PHASEID
			Where P.PHASEID = @PhaseId
		END
END
