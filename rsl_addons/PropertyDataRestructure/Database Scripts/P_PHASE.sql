/*
   Tuesday, February 03, 201511:36:45 PM
   User: sa
   Server: dev-pc4
   Database: RSLBHALive
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_P_PHASE
	(
	PHASEID int NOT NULL IDENTITY (1, 1),
	ConstructionStart smalldatetime NULL,
	AnticipatedCompletion smalldatetime NULL,
	ActualCompletion smalldatetime NULL,
	HandoverintoManagement smalldatetime NULL,
	DEVELOPMENTID int NULL,
	PhaseName varchar(50) NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_P_PHASE SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_P_PHASE ON
GO
IF EXISTS(SELECT * FROM dbo.P_PHASE)
	 EXEC('INSERT INTO dbo.Tmp_P_PHASE (PHASEID, ConstructionStart, AnticipatedCompletion, ActualCompletion, HandoverintoManagement, DEVELOPMENTID, PhaseName)
		SELECT PHASEID, ConstructionStart, AnticipatedCompletion, ActualCompletion, HandoverintoManagement, DEVELOPMENTID, PhaseName FROM dbo.P_PHASE WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_P_PHASE OFF
GO
DROP TABLE dbo.P_PHASE
GO
EXECUTE sp_rename N'dbo.Tmp_P_PHASE', N'P_PHASE', 'OBJECT' 
GO
ALTER TABLE dbo.P_PHASE ADD CONSTRAINT
	PK_P_PHASE PRIMARY KEY CLUSTERED 
	(
	PHASEID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
select Has_Perms_By_Name(N'dbo.P_PHASE', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.P_PHASE', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.P_PHASE', 'Object', 'CONTROL') as Contr_Per 