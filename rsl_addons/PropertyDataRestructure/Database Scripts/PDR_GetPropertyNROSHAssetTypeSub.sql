USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetPropertyNROSHAssetTypeSub]    Script Date: 12/10/2014 18:25:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* =================================================================================    
    Page Description:  Get Property NROSH AssetType2 for dropdown
 
    Author: Ali Raza
    Creation Date: Dec-10-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-10-2014      Ali Raza           Get Property NROSH AssetType2 for dropdown
    
    Execution Command:
    
    Exec PDR_GetPropertyNROSHAssetTypeSub 2
  =================================================================================*/

CREATE PROCEDURE [dbo].[PDR_GetPropertyNROSHAssetTypeSub]
(@MainCategoryId int = -1)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
IF @MainCategoryId > 0 
	BEGIN
		-- Insert statements for procedure here
		SELECT Sid,Description FROM P_ASSETTYPE_NROSH_SUB WHERE MAINCATEGORYID =@MainCategoryId
		
	END
ELSE
	BEGIN
		-- Insert statements for procedure here
		SELECT Sid,Description FROM P_ASSETTYPE_NROSH_SUB ATNS 
		
	END

END
