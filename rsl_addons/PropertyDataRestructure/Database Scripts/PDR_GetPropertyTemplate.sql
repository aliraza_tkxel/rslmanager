USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetPropertyTemplate]    Script Date: 12/10/2014 18:28:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* =================================================================================    
    Page Description:  Get Property Template for dropdown
 
    Author: Ali Raza
    Creation Date: Dec-10-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-10-2014      Ali Raza           Get Property Template for dropdown
    
    Execution Command:
    
    Exec PDR_GetPropertyTemplate
  =================================================================================*/

CREATE PROCEDURE [dbo].[PDR_GetPropertyTemplate]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
Select PROPERTYID,TemplateName from P__PROPERTY where IsTemplate=1
END
