USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[FL_GetFaultStatusLookUpSubContractor]    Script Date: 11/24/2015 18:26:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* ===========================================================================
--	EXEC FL_GetFaultStatusLookUpSubContractor
--  Author:			Aamir Waheed
--  DATE CREATED:	14 March 2013
--  Description:	To Get FL_Fault_Status Look Up List for Job Sheet Summary Sub Contractor - Update
--  Webpage:		View/Reports/ReportArea.aspx
--  Modified by:	Raja Aneeq
--  Description:	Add two new PO status. Accepcted by Contractor,Rejected by Contractor
 '==============================================================================*/
ALTER PROCEDURE [dbo].[FL_GetFaultStatusLookUpSubContractor]	
AS
BEGIN
	
	SET NOCOUNT ON;

    SELECT FaultStatusID as id, Description AS val
	FROM FL_FAULT_STATUS
	WHERE Description IN('Cancelled', 'Complete', 'No Entry', 'Assigned To Contractor','Accepcted by Contractor','Rejected by Contractor')
END
