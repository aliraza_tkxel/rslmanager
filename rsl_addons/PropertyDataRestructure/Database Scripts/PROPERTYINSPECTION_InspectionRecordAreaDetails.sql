USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PROPERTYINSPECTION_InspectionRecordAreaDetails]    Script Date: 01/08/2015 12:32:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Abdullah Saeed
-- Create date: August 4,2014
-- Description:	Area Details
-- Exec PROPERTYINSPECTION_InspectionRecordAreaDetails 'A240110002'
-- =============================================
ALTER PROCEDURE [dbo].[PROPERTYINSPECTION_InspectionRecordAreaDetails]
	-- Add the parameters for the stored procedure here
(
@propertyID varchar(100)
)
	
	
AS
BEGIN
	
	SET NOCOUNT ON;
-- get location
    select LocationID, LocationName from PA_LOCATION
	-- get area
	select AreaID, LocationId, AreaName from PA_AREA Where IsActive=1
	--get item
	select ItemID,AreaID,ItemName from PA_ITEM where  ParentItemId is Null and IsActive =1
	-- get sub item
	select ItemID as SubItemID ,AreaID ,ItemName as SubItemName, ParentItemId  from PA_ITEM 
	where  ParentItemId is not Null and IsActive =1 And ItemName Not like '%Aids & Adaptations%'
	-- get table update or not
	SELECT ItemId , PROPERTYID,ATTRIBUTEID,PA_ITEM_PARAMETER.ITEMPARAMID FROM PA_PROPERTY_ATTRIBUTES  
	INNER JOIN PA_ITEM_PARAMETER ON  PA_PROPERTY_ATTRIBUTES.ITEMPARAMID= PA_ITEM_PARAMETER.ItemParamID     
	WHERE PROPERTYID=  @propertyID
	
--=================================================================      
  --get Parameters      
 select ItemParamID,PA_ITEM.ItemID,PA_ITEM.ItemName ,PA_PARAMETER.ParameterID,ParameterName, ControlType 
  from PA_ITEM_PARAMETER       
 inner JOIN PA_PARAMETER on PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID       
 INNER JOIN PA_ITEM ON PA_ITEM.ItemID = PA_ITEM_PARAMETER.ItemId       
 INNER JOIN PA_AREA on PA_AREA.AreaID = PA_ITEM.AreaID       
 where  PA_ITEM_PARAMETER.isActive = 1 and PA_ITEM.isActive =1 and PA_PARAMETER.isActive =1     
 ORDER BY ParameterSorder asc 
	
	--=================================================================      
  ---get parameter values      
  --=================================================================      
 SELECT ValueID, ParameterID, ValueDetail, Sorder     
 FROM PA_PARAMETER_VALUE where ParameterID IN (     
 select PA_PARAMETER.ParameterID from PA_ITEM_PARAMETER       
 inner JOIN PA_PARAMETER on PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID       
 INNER JOIN PA_ITEM ON PA_ITEM.ItemID = PA_ITEM_PARAMETER.ItemId       
 INNER JOIN PA_AREA on PA_AREA.AreaID = PA_ITEM.AreaID       
 where  PA_ITEM_PARAMETER.isActive = 1 and PA_ITEM.isActive =1 and PA_PARAMETER.isActive =1 ) and PA_PARAMETER_VALUE.IsActive = 1      
 Order BY SOrder --AND PA_AREA.AreaID =@areaId)       
       
 --=================================================================      
 ---get pre inserted values       
 --=================================================================      
SELECT PROPERTYID,ATTRIBUTEID,ITEMPARAMID,PARAMETERVALUE,VALUEID,UPDATEDON,UPDATEDBY ,IsCheckBoxSelected FROM PA_PROPERTY_ATTRIBUTES      
  WHERE PROPERTYID=  @propertyId AND ITEMPARAMID IN  ( select ItemParamID from PA_ITEM_PARAMETER       
 INNER JOIN PA_PARAMETER on PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID       
 INNER JOIN PA_ITEM ON PA_ITEM.ItemID = PA_ITEM_PARAMETER.ItemId       
 INNER JOIN PA_AREA on PA_AREA.AreaID = PA_ITEM.AreaID       
where  PA_ITEM_PARAMETER.isActive = 1 and PA_ITEM.isActive =1 and PA_PARAMETER.isActive =1 ) --AND PA_AREA.AreaID =@areaId)        
       
--	select PropertyID,ITEMPARAMID, ISNULL(PARAMETERVALUE,'NA') as ParameterValue from PA_PROPERTY_ATTRIBUTES where PROPERTYID='E240250009'
	

	
	
	 SELECT [SID],PROPERTYID,ItemId,LastDone,DueDate,PA_PARAMETER.ParameterId,ISNULL(ParameterName,'NA')as  ParameterName from PA_PROPERTY_ITEM_DATES      
   left JOIN PA_PARAMETER On PA_PARAMETER.ParameterID = PA_PROPERTY_ITEM_DATES.ParameterId      
   where PROPERTYID=@propertyId 
   
   --========================================================================================
   --get Insepection Notes
   
   select PA_PROPERTY_ITEM_NOTES.PROPERTYID,PA_PROPERTY_ITEM_NOTES.ItemId,PA_PROPERTY_ITEM_NOTES.Notes,PA_LOCATION.LocationID,PA_AREA.AreaID,PA_LOCATION.LocationName,PA_AREA.AreaName,PA_ITEM.ItemName 
   from PA_PROPERTY_ITEM_NOTES
   INNER JOIN PA_ITEM on PA_ITEM.ItemID=PA_PROPERTY_ITEM_NOTES.ItemId
   INNER JOIN PA_AREA on PA_AREA.AreaID=PA_ITEM.AreaID
   INNER JOIN PA_LOCATION on PA_LOCATION.LocationID=PA_AREA.LocationId
    where PROPERTYID=@propertyID
   
   --==============================================================================================
   
   --get Property Images
   select 
   PA_PROPERTY_ITEM_IMAGES.PROPERTYID,PA_PROPERTY_ITEM_IMAGES.ItemId,PA_PROPERTY_ITEM_IMAGES.ImageName,PA_PROPERTY_ITEM_IMAGES.ImagePath,PA_LOCATION.LocationID,PA_AREA.AreaID,PA_LOCATION.LocationName,PA_AREA.AreaName,PA_ITEM.ItemName
   from PA_PROPERTY_ITEM_IMAGES 
   INNER JOIN PA_ITEM on PA_ITEM.ItemID=PA_PROPERTY_ITEM_IMAGES.ItemId
   INNER JOIN PA_AREA on PA_AREA.AreaID=PA_ITEM.AreaID
   INNER JOIN PA_LOCATION on PA_LOCATION.LocationID=PA_AREA.LocationId
   where PROPERTYID=@propertyID
	
	
	
	
	
END
