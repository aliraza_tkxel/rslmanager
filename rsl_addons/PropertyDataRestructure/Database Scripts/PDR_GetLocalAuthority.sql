USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetLocalAuthority]    Script Date: 10/09/2015 11:28:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    Page Description:     Get LocalAuthority For DropDown
 
    Author: Ali Raza
    Creation Date: Dec-18-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-18-2014      Ali Raza           Get LocalAuthority For DropDown
    v1.1		 Oct-9-2015		Raja Aneeq			Arrange result Alphabetically
    
    Execution Command:
    
    Exec PDR_GetLocalAuthority
  =================================================================================*/
ALTER PROCEDURE [dbo].[PDR_GetLocalAuthority]
	
AS
BEGIN
	
	SET NOCOUNT ON;    
	SELECT LOCALAUTHORITYID,DESCRIPTION From G_LOCALAUTHORITY ORDER BY DESCRIPTION ASC
END
