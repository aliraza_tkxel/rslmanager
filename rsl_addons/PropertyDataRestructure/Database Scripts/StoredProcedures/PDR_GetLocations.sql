
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Exec AS_Locations
-- Author:		Ali Raza
-- Create date: <02/10/2015>
-- Description:	<Get All locations for Attributes Tree>
-- =============================================
CREATE PROCEDURE PDR_GetLocations
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    Select AreaID as LocationID, AreaName as LocationName, 'Area' as Location  from PA_AREA where IsForSchemeBlock = 1 
   UNION ALL
	SELECT LocationID as LocationID,LocationName as LocationName, 'Location'as Location    	From PA_LOCATION
END
GO
