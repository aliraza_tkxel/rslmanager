
/****** Object:  StoredProcedure [dbo].[PDR_AmendProvisionItemDetail]    Script Date: 11/27/2015 13:07:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Stored Procedure

   /* =================================================================================    
    Page Description:    save the detail of Provision Item

    Shaheen Tariq
    Creation Date: Nov-23-2015  
  =================================================================================*/
ALTER PROCEDURE [dbo].[PDR_AmendProvisionItemDetail]   
  -- Add the parameters for the stored procedure here  
  @name					nvarchar(100),
  @manufacturer			nvarchar(100),
  @model				nvarchar(50),
  @serialNumber			nvarchar(100),
  @installedDate		smalldatetime,
  @installedCost		decimal(18,0),
  @replacementDueDate	smalldatetime,
  @lastReplacedDate		smalldatetime,
  @provisionParentId	INT,
  @lifeSpan				INT,
  @conditionRating		INT,
  @cycleTypeId			INT,
  @schemeId				INT=null, 
  @blockId				INT=null,  
  @updatedBy			INT,   
  @existingProvisionId  INT= null,
  @ProvisionId INT output,
  @MSATDetail AS AS_MSATDETAIL readonly   
AS   
  BEGIN   
      -- SET NOCOUNT ON added to prevent extra result sets from          
      -- interfering with SELECT statements.          
      SET nocount ON;   
		BEGIN TRANSACTION
		BEGIN TRY 	
			IF @existingProvisionId <= 0 OR @existingProvisionId IS NULL 
				BEGIN
					-- Insert statements for procedure herer
					INSERT INTO PDR_Provisions(ProvisionName,ProvisionParentId,Manufacturer,Model,SerialNumber,InstalledDate,InstallationCost,LifeSpan,CycleId,ReplacementDue,ConditionRating,IsActive,SchemeId,BlockId,LastReplaced,UpdatedBy )
					VALUES (@name,@provisionParentId,@manufacturer,@model,@serialNumber,@installedDate,@installedCost,@lifeSpan,@cycleTypeId,@replacementDueDate,@conditionRating,1,@schemeId,@blockId,@lastReplacedDate,@updatedBy)

					SELECT @ProvisionId = SCOPE_IDENTITY()
					
					INSERT INTO PDR_Provision_History(ProvisionId, ProvisionName,ProvisionParentId,Manufacturer,Model,SerialNumber,InstalledDate,InstallationCost,LifeSpan,CycleId,ReplacementDue,ConditionRating,IsActive,SchemeId,BlockId,LastReplaced,UpdatedBy )
					VALUES (@ProvisionId, @name,@provisionParentId,@manufacturer,@model,@serialNumber,@installedDate,@installedCost,@lifeSpan,@cycleTypeId,@replacementDueDate,@conditionRating,1,@schemeId,@blockId,@lastReplacedDate,@updatedBy)
				 END
			ELSE
			BEGIN
				 Update PDR_Provisions 
				 Set ProvisionName  =  @name ,
				 ProvisionParentId =@provisionParentId,Manufacturer=@manufacturer,Model =@model,SerialNumber =@serialNumber,InstalledDate =@installedDate,
				 InstallationCost = @installedCost,LifeSpan = @lifeSpan,CycleId = @cycleTypeId,
				 ReplacementDue=@replacementDueDate,ConditionRating=@conditionRating,IsActive=1,SchemeId=@schemeId,BlockId=@blockId,LastReplaced=@lastReplacedDate,UpdatedBy =@updatedBy
     			 WHERE ProvisionId = @existingProvisionId
				 
				 SET @ProvisionId =@existingProvisionId
				 
				 INSERT INTO PDR_Provision_History(ProvisionId, ProvisionName,ProvisionParentId,Manufacturer,Model,SerialNumber,InstalledDate,InstallationCost,LifeSpan,CycleId,ReplacementDue,ConditionRating,IsActive,SchemeId,BlockId,LastReplaced,UpdatedBy )
				 VALUES (@ProvisionId, @name,@provisionParentId,@manufacturer,@model,@serialNumber,@installedDate,@installedCost,@lifeSpan,@cycleTypeId,@replacementDueDate,@conditionRating,1,@schemeId,@blockId,@lastReplacedDate,@updatedBy)
			END
			
			Exec PDR_AmendpMSTAItemDetailForProvisions @ProvisionId,@schemeId,@blockId,@UpdatedBy,@MSATDetail 	   
	END TRY 
	BEGIN CATCH 	
		IF @@TRANCOUNT >0	  
		BEGIN	   
			  PRINT 'Some thing went wrong with this transaction'
			SET @ProvisionId= -1
			ROLLBACK TRANSACTION;	
		END 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
		@ErrorSeverity, -- Severity.
		@ErrorState -- State.
		);
	END CATCH  

	IF @@TRANCOUNT >0
		BEGIN
			PRINT 'Transaction completed successfully'	
			COMMIT TRANSACTION;
		END 
END
      

