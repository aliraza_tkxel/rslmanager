USE [RSLBHALive ]
GO
/****** Object:  StoredProcedure [dbo].[PDR_SaveCompletionRecord]    Script Date: 02/12/2016 19:25:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Raja Aneeq>
-- Create date: <30/10/2015>
-- Description:	<Save work completion record>
-- =============================================
ALTER PROCEDURE [dbo].[PDR_SaveCompletionRecord] 
@FaultLogID int,
@FaultRepairIDList varchar(8000),
@Time VARCHAR(50),
@Date datetime,
@UserID Int,
@FollowOnStatus bit,
@FollowOnNotes nvarchar(1000),
@RESULT  INT = 1  OUTPUT 
AS
BEGIN

DECLARE @statusId int
select @statusId =  faultStatusId from FL_Fault_status where Description ='Complete'


	--BEGIN TRAN
DECLARE @FaultRepairId INT,@Success int	
CREATE TABLE #tempFaultRepairId(FaultRepairId INT )
INSERT INTO #tempFaultRepairId (FaultRepairId)
SELECT COLUMN1
FROM dbo.SPLIT_STRING(@FaultRepairIDList, ',')

DECLARE @ItemToInsertCursor CURSOR
--Initialize cursor
SET @ItemToInsertCursor = CURSOR FAST_FORWARD FOR SELECT
	FaultRepairId
FROM #tempFaultRepairId;
   --Open cursor
   OPEN @ItemToInsertCursor
---fetch row from cursor
   FETCH NEXT FROM @ItemToInsertCursor INTO @FaultRepairId
 
   
    ---Iterate cursor to get record row by row
WHILE @@FETCH_STATUS = 0
BEGIN
Set @Success = 0	
INSERT INTO FL_CO_FAULTLOG_TO_REPAIR (FaultLogID,FaultRepairListID, InspectionDate, InspectionTime, UserId )
VALUES (@FaultLogID, @FaultRepairId, @Date,@Time,@UserID)
	
	
FETCH NEXT FROM @ItemToInsertCursor INTO @FaultRepairId
  
END
--close & deallocate cursor		
CLOSE @ItemToInsertCursor

DEALLOCATE @ItemToInsertCursor
IF (@FollowOnStatus = 1)
BEGIN

INSERT INTO FL_FAULT_FOLLOWON (FaultLogID,RecordedOn,FollowOnNotes,IsFollowOnScheduled) values(@FaultLogID,GETDATE(),@FollowOnNotes,0) 	
END
update FL_FAULT_LOG set statusId = @statusId where faultLogId =@FaultLogID

IF OBJECT_ID('tempdb..#tempFaultRepairId') IS NOT NULL
DROP TABLE #tempFaultRepairId
 Set @Success = 1
	
	Select @RESULT= @Success
	
END




