
/****** Object:  StoredProcedure [dbo].[PDR_GetProvisionsList]    Script Date: 12/05/2015 03:08:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Shaheen
-- Create date: 1st December 2015
-- Description:	Provision List for Provision Report
-- =============================================
ALTER PROCEDURE [dbo].[PDR_GetProvisionsList]
		@filterText VARCHAR(200),
	-- Add the parameters for the stored procedure here
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'PROVISIONID', 
		@sortOrder varchar (5) = 'DESC',
		@totalCount int = 0 output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(2000),
    
		--variables for paging
        @offset int,
		@limit int
		
		
		SET @searchCriteria = ' 1=1 '
		IF(@filterText != '' OR @filterText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND '+  @filterText
		END	
	
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
				
		SET @SelectClause = 'Select top ('+convert(nvarchar(10),@limit)+')
							 ISNULL(S.SCHEMENAME, ''-'') as SCHEMENAME
							,ISNULL(B.BLOCKNAME,''-'') as BLOCKNAME
							,ISNULL(p.ProvisionName,''-'') as PROVISIONNAME
							,ISNULL(CAST (p.InstalledDate as DATE),'' '') as INSTALLEDDATE
							,ISNULL(p.LifeSpan,'' '') as LIFESPAN
							,ISNULL(CAST (p.ReplacementDue as DATE),'' '') as REPLACEMENTDUE
							,p.ProvisionId as PROVISIONID
							'
			
		SET @fromClause = CHAR(10) +' FROM PDR_Provisions P
								   LEFT JOIN P_SCHEME S ON S.SCHEMEID = p.SchemeId 
								   LEFT JOIN P_BLOCK B on B.BLOCKID= p.BlockId'
								   
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder						   
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
		print(@finalQuery)
		EXEC (@finalQuery)
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
END

GO


