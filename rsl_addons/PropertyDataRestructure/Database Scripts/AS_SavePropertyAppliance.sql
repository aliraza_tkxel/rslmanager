USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_SavePropertyAppliance]    Script Date: 11/11/2014 11:45:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,Ali Raza>
-- Create date: <Create Date,,05/11/2012>
-- Description:	<Description,,Save appliance against property >
-- Web Page: PropertyRecord.aspx
-- Modified : <Aamir Waheed, 08/05/2013>
-- =============================================
ALTER PROCEDURE [dbo].[AS_SavePropertyAppliance]
	@propertyId varchar(50)=null,
	@schemeId int=null, 
   @blockId int=null,
	@locationId int,
	@location varchar(200)=null,	
	@typeId int,
	@type varchar(200)=null,	
	@makeId int,
	@make varchar(200)=null,	
	@modelId int,
	@model varchar(200)=null,	
	@itemId int,		
	@item NVARCHAR(200),	
	@quantity NVARCHAR(200),
	@dimensions NVARCHAR(200),
	@serialNumber NVARCHAR(50),
	@purchaseCost float,
	@lifeSpan int,
	@datePurchased DATE,	
	@notes NVARCHAR(500),
	@existingApplianceId int=0,
	@applianceId int=0 out
AS
BEGIN
	      
	--Declare @newModelId int,@newlocationId Int,@newtypeId int,new@makeId int
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;			
Set @modelId = ( Select top 1  ModelID
						From GS_ApplianceModel
					   Where Lower( LTrim( RTrim( Model ) ) ) = Lower( LTrim( RTrim( @model ) ) ) )
    If( IsNull( @modelId, 0 ) = 0  And @model <> '' )
	Begin
		Insert Into GS_ApplianceModel( [Model] ) 
			  Values ( @model)
		Set @modelId = SCOPE_IDENTITY()
	End

Set @locationId = ( Select top 1 LOCATIONID
						From GS_LOCATION
					   Where Lower( LTrim( RTrim( LOCATION ) ) ) = Lower( LTrim( RTrim( @location ) ) ) )
    If( IsNull( @locationId, 0 ) = 0  And @location <> '' )
	Begin
		Insert Into GS_LOCATION( LOCATION ) 
			  Values ( @location)
		Set @locationId = SCOPE_IDENTITY()
	End


	Set @typeId = ( Select top 1  APPLIANCETYPEID
						From GS_APPLIANCE_TYPE
					   Where Lower( LTrim( RTrim( APPLIANCETYPE ) ) ) = Lower( LTrim( RTrim( @type ) ) ) )
    If( IsNull( @typeId, 0 ) = 0 And @type <> '')
	Begin
		INSERT INTO GS_APPLIANCE_TYPE(APPLIANCETYPE, ISACTIVE)VALUES(@type,1)
		Set @typeId = SCOPE_IDENTITY()
	End
   
	Set @makeId = ( Select top 1 MANUFACTURERID
						From GS_MANUFACTURER
					   Where Lower( LTrim( RTrim( MANUFACTURER ) ) ) = Lower( LTrim( RTrim( @make ) ) ) )
    If( IsNull( @makeId, 0 ) = 0 And @make <> '')
	Begin
		INSERT INTO GS_MANUFACTURER(MANUFACTURER)VALUES(@make)
			Set @makeId = SCOPE_IDENTITY()
	End
	if @modelId = 0
		BEGIN
			SET @modelId = NULL
		END
	if @locationId = 0
		BEGIN
			SET @locationId = NULL
		END
	if @typeId = 0
		BEGIN
			SET @typeId = NULL
		END
	if @makeId = 0
		BEGIN
			SET @makeId = NULL
		END
	
	If (@existingApplianceId > 0)
		BEGIN
			Update GS_PROPERTY_APPLIANCE SET MANUFACTURERID=@makeId,[LOCATIONID]=@locationId,[APPLIANCETYPEID]=@typeId,[MODELID]=@modelId,Item=@item,
			ItemId=@itemId,Quantity=@quantity,Dimensions=@dimensions,SerialNumber=@serialNumber,LifeSpan=@lifeSpan,PurchaseCost=@purchaseCost,Datepurchased=@datePurchased
			,NOTES=@notes WHERE  PropertyApplianceId=@existingApplianceId
			SELECT @applianceId = @existingApplianceId
		END
	ELSE
		BEGIN
		print 'Insertion '
		print @modelId
   			INSERT INTO [GS_PROPERTY_APPLIANCE]
				   ([PROPERTYID],[MANUFACTURERID],[LOCATIONID],[APPLIANCETYPEID],[MODELID],Item,ItemId,Quantity,Dimensions,SerialNumber,LifeSpan,PurchaseCost,Datepurchased,NOTES,SchemeId,BlockId)
			 VALUES
				   (@propertyId ,@makeId,@locationId,@typeId,@modelId ,@item,@itemId,@quantity,@dimensions,@serialNumber,@lifeSpan,@purchaseCost,@datePurchased,@notes,@schemeId,@blockId)

		SELECT @applianceId = @@identity
   		END
END
