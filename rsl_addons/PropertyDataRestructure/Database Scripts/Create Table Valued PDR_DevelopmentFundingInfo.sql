USE [RSLBHALive]
GO

/****** Object:  UserDefinedTableType [dbo].[PDR_DevelopmentFundingInfo]    Script Date: 02/28/2015 16:34:57 ******/
CREATE TYPE [dbo].[PDR_DevelopmentFundingInfo] AS TABLE(
	[DEVELOPMENTFUNDINGID] [int] NOT NULL,
	[FUNDINGAUTHORITYID] [int] NOT NULL,
	[FUNDINGAUTHORITY] VARCHAR(100) NOT NULL,
	[GRANTAMOUNT] [float] NOT NULL
)
GO


