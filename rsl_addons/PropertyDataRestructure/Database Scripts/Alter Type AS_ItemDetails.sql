USE [RSLBHALive]
GO
BEGIN TRANSACTION
BEGIN TRY 
/****** Object:  StoredProcedure [dbo].[PDR_AmendAttributeItemDetail]    Script Date: 02/26/2015 17:11:36 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PDR_AmendAttributeItemDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[PDR_AmendAttributeItemDetail]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[AS_AmendPropertyItemDetail]') AND type in (N'P', N'PC'))
DROP PROCEDURE [dbo].[AS_AmendPropertyItemDetail]

IF  EXISTS (SELECT * FROM sys.types st JOIN sys.schemas ss ON st.schema_id = ss.schema_id WHERE st.name = N'AS_ItemDetails' AND ss.name = N'dbo')
DROP TYPE [dbo].[AS_ItemDetails]

CREATE TYPE [dbo].[AS_ItemDetails] AS TABLE(
	[ItemParamId] [int] NULL,
	[ParameterValue] [nvarchar](1000) NULL,
	[ValueId] [int] NULL,
	[IsCheckBoxSelected] [bit] NULL
)
  


END TRY 
	BEGIN CATCH 	
		IF @@TRANCOUNT >0	  
		BEGIN	   
			  PRINT 'Some thing went wrong with this transaction'
			
			ROLLBACK TRANSACTION;	
		END 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
		@ErrorSeverity, -- Severity.
		@ErrorState -- State.
		);
	END CATCH  
	 
IF @@TRANCOUNT >0
	BEGIN
		PRINT 'Transaction completed successfully'	
		COMMIT TRANSACTION;
		
	END 
GO