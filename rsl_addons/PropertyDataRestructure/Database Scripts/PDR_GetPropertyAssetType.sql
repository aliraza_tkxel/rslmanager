USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetPropertyAssetType]    Script Date: 12/10/2014 18:23:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* =================================================================================    
    Page Description:  Get Property Asset type for dropdown
 
    Author: Ali Raza
    Creation Date: Dec-10-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-10-2014      Ali Raza           Get Property Asset type for dropdown
    
    Execution Command:
    
    Exec PDR_GetPropertyAssetType
  =================================================================================*/

CREATE PROCEDURE [dbo].[PDR_GetPropertyAssetType]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT AT.ASSETTYPEID,AT.DESCRIPTION FROM P_ASSETTYPE AT 
	
END
