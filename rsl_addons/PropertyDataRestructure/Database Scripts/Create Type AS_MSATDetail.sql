USE [RSLBHALive]
GO

/****** Object:  UserDefinedTableType [dbo].[AS_ItemDetails]    Script Date: 12/30/2014 17:14:06 ******/
CREATE TYPE [dbo].[AS_MSATDetail] AS TABLE(
	[IsRequired] [bit] NULL,
	[LastDate] [datetime] NULL,
	[Cycle] [int] NULL,
	[CycleTypeId] [int] NULL,
	[NextDate] [datetime] NULL,
	[AnnualApportionment] [float] NULL,
	[MSATTypeId] [int]
)
GO


