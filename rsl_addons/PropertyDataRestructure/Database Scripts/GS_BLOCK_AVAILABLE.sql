USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[GS_SCHEME_AVAILABLE_WO]    Script Date: 02/03/2015 09:22:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ================================================
-- Author:		Ahmed Mehmood
-- Create date: 3/02/2015
-- Description:	This procedure is used to get the 
--				block list for work order creation
-- =================================================
CREATE PROCEDURE [dbo].[GS_BLOCK_AVAILABLE]
@ORGID INT = NULL,
@PATCHID INT=NULL,
@SCHEMEID INT =NULL

--THIS GETS THE PATCH FOR THE WORK ORDER 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

 -- Insert statements for procedure here
SELECT ISNULL(SS.BlockId,0) AS BLOCKID,B.BLOCKNAME
FROM S_SCOPE S 
	INNER JOIN S_SCOPETOPATCHANDSCHEME SS ON S.SCOPEID=SS.SCOPEID 
	INNER JOIN P_BLOCK B ON SS.BlockId = B.BLOCKID
WHERE SS.PATCHID = COALESCE(@PATCHID, SS.PATCHID) AND 
	S.ORGID= COALESCE(@ORGID, S.ORGID)   AND 
	SS.SchemeId = @SCHEMEID AND 
	S.RENEWALDATE > GETDATE() AND
	S.SCOPEID NOT IN (SELECT DISTINCT SCOPEID FROM C_REPAIR WHERE SCOPEID IS NOT NULL) 
END


