USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetPropertyType]    Script Date: 12/10/2014 18:28:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* =================================================================================    
    Page Description:  Get Property Type for dropdown
 
    Author: Ali Raza
    Creation Date: Dec-10-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-10-2014      Ali Raza           Get Property Type for dropdown
    
    Execution Command:
    
    Exec PDR_GetPropertyType
  =================================================================================*/

CREATE PROCEDURE [dbo].[PDR_GetPropertyType]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT PROPERTYTYPEID, DESCRIPTION FROM P_PROPERTYTYPE WHERE PROPERTYTYPEID <> 17
	
END
