
-- =============================================
-- Author:		<Ahmed Mehmood>
-- Create date: <9/4/2015>
-- Description:	<This script updates P_BLOCK with schemeid from  P__PROPERTY >
-- =============================================



UPDATE	P_BLOCK 
SET		P_BLOCK.SCHEMEID = PROPERTY_BLOCK.SCHEMEID
FROM 
(SELECT	DISTINCT BLOCKID, SCHEMEID 
FROM	P__PROPERTY
WHERE	P__PROPERTY.BLOCKID IS NOT NULL) PROPERTY_BLOCK
WHERE P_BLOCK.BLOCKID = PROPERTY_BLOCK.BLOCKID