USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_ChangeWorkStatusByContractor]    Script Date: 12/14/2015 11:57:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Raja Aneeq>
-- Create date: <28/10/2015>
-- Description:	<update PO Status after accepting or rejecting work by contractor >
-- =============================================
ALTER PROCEDURE [dbo].[PDR_ChangeWorkStatusByContractor] 
	@orderId int ,
	@isAccept bit
	AS
BEGIN
	
	SET NOCOUNT ON;
	Declare @StatusId int
	
IF  (@isAccept = 1) 
	BEGIN
		SELECT  @StatusId = FLS.FaultStatusId FROM FL_FAULT_STATUS FLS WHERE Description = 'Accepcted by Contractor'  
	End 
  
ELSE 
  BEGIN
	SELECT  @StatusId = FLS.FaultStatusId FROM FL_FAULT_STATUS FLS WHERE Description = 'Rejected by Contractor'  
  End
  
  UPDATE  FL_FAULT_LOG set  STATUSID = @StatusId 
  WHERE FaultLogID = (select FL.FaultLogId from FL_FAULT_JOURNAL J
 INNER JOIN FL_FAULT_LOG FL ON  J.FaultLogId = FL.FaultLogId
 INNER JOIN  FL_CONTRACTOR_WORK  CW   ON CW.JournalId  =J.JournalId
 
  WHERE CW.PURCHASEORDERID = @orderId)
  
  
  --END IF
END






