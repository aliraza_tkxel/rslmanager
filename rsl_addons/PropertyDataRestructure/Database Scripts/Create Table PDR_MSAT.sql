USE [RSLBHALive]
GO

/****** Object:  Table [dbo].[PDR_MSAT]    Script Date: 01/02/2015 10:23:24 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PDR_MSAT](
	[MSATId] [int] IDENTITY(1,1) NOT NULL,
	[PropertyId] [nvarchar](50) NULL,
	[ItemId] [int] NULL,
	[MSATTypeId] [int] NULL,
	[CycleTypeId] [int] NULL,
	[IsRequired] [bit] NULL,
	[LastDate] [datetime] NULL,
	[Cycle] [int] NULL,
	[NextDate] [datetime] NULL,
	[AnnualApportionment] [float] NULL,
	[IsActive] [bit] NULL,
	[SchemeId] [int] NULL,
	[BlockId] [int] NULL,
 CONSTRAINT [PK_PDR_MSAT] PRIMARY KEY CLUSTERED 
(
	[MSATId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


