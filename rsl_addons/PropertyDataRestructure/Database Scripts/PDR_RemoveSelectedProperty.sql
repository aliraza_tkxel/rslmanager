-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
Go
/* =================================================================================    
    Page Description: Properties Listing on Scheme and Block
 
    Author: Salman Nazir
    Creation Date: Dec-25-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-25-2014      Salman         Remove Selected Property by request type
    
    
    =================================================================================
    EXEC PDR_UpdateSchemeProperty  1, 'A0120354'
  =================================================================================*/
CREATE PROCEDURE PDR_RemoveSelectedProperty
@PropertyRef varchar(200),
@requestType varchar(100)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	--Select * from P__PROPERTY Where PROPERTYID =@PropertyRef
    -- Insert statements for procedure here
    If (@requestType = 'Block')
		BEGIN
			Update P__PROPERTY Set BLOCKID = null Where PROPERTYID = @PropertyRef
		END
	Else
		BEGIN
			Update P__PROPERTY Set SCHEMEID = null Where PROPERTYID = @PropertyRef
		END	
END
GO
