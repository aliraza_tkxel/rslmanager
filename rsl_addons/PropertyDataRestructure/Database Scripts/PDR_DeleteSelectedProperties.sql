-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,05/12/2014>
-- Description:	<Description,,Delete selected properties>
-- EXEC PDR_DeleteSelectedProperties 'A010000018'
-- =============================================
CREATE PROCEDURE PDR_DeleteSelectedProperties
@propertyId varchar(50)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Update P__PROPERTY Set BLOCKID = Null Where PROPERTYID = @propertyId 
END
GO
