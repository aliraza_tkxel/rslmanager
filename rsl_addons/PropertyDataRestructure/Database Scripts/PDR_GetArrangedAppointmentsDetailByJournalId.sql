USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC PDR_GetArrangedAppointmentsDetailByJournalId 
-- @journalId =1
-- Author:		Ahmed Mehmood
-- Create date: <14/1/2015>
-- Description:	<Description,Return appointments associated with journalId >
-- Webpage : ViewArrangedAppointmentSummary.aspx

-- =============================================
CREATE PROCEDURE [dbo].[PDR_GetArrangedAppointmentsDetailByJournalId]
	@journalId int
AS
BEGIN

	
	DECLARE @propertyId varchar(20)

	SELECT	ROW_NUMBER() OVER (ORDER BY PDR_APPOINTMENTS.APPOINTMENTID) AS Row
			,PDR_APPOINTMENTS.JOURNALID  AS JournalId
			,PA_ITEM.ItemName AS ItemName
			,PA_ITEM.ItemID AS ItemId
			,PDR_APPOINTMENTS.TRADEID AS TradeId
			,G_TRADE.Description AS Trade
			,RIGHT('000000'+ CONVERT(VARCHAR,PDR_APPOINTMENTS.APPOINTMENTID),6) AS JSN
			,PDR_STATUS.TITLE AS Status
			,PDR_MSATType.MSATTypeName AS MsatType
			,E__EMPLOYEE.FIRSTNAME+' '+ E__EMPLOYEE.LASTNAME as Operative
			,LEFT(E__EMPLOYEE.FIRSTNAME, 1)+' '+ E__EMPLOYEE.LASTNAME as OperativeShortName
			,CONVERT(char(5),cast(PDR_APPOINTMENTS.APPOINTMENTSTARTTIME as datetime),108) AS StartTime
			,CONVERT(char(5),cast(PDR_APPOINTMENTS.APPOINTMENTENDTIME  as datetime),108) AS EndTime
			,PDR_APPOINTMENTS.DURATION AS Duration
			, (	SELECT SUM(PDR_APPOINTMENTS.DURATION)
				FROM	PDR_APPOINTMENTS
				WHERE	PDR_APPOINTMENTS.JournalId = @journalId )
			 AS TotalDuration				
			,CONVERT(varchar(10), PDR_APPOINTMENTS.APPOINTMENTSTARTDATE, 103) AS StartDate
			,CONVERT(varchar(10),PDR_APPOINTMENTS.APPOINTMENTENDDATE, 103) AS EndDate
			
			,ISNULL(PDR_APPOINTMENTS.CUSTOMERNOTES,'') AS CustomerNotes
			,ISNULL(PDR_APPOINTMENTS.APPOINTMENTNOTES,'') AS JobsheetNotes
			,PDR_MSAT.PROPERTYID  AS PropertyId
			,PDR_APPOINTMENTS.APPOINTMENTID AS AppointmentId
			,PDR_APPOINTMENTS.APPOINTMENTSTATUS as InterimStatus
			,SUBSTRING(DATENAME(weekday,  PDR_APPOINTMENTS.APPOINTMENTSTARTDATE),1,3)+' '+ CONVERT(VARCHAR(11), PDR_APPOINTMENTS.APPOINTMENTSTARTDATE, 106) AppointmentDateFormat1

	FROM	PDR_APPOINTMENTS
			INNER JOIN PDR_JOURNAL ON PDR_APPOINTMENTS.JournalId = PDR_JOURNAL.JOURNALID 
			INNER JOIN PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId
			INNER JOIN PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId
			INNER JOIN PA_ITEM ON PDR_MSAT.ItemId = PA_ITEM.ItemID
			INNER JOIN G_TRADE ON PDR_APPOINTMENTS.TRADEID = G_TRADE.TradeId
			INNER JOIN PDR_STATUS ON PDR_JOURNAL.STATUSID = PDR_STATUS.STATUSID
			INNER JOIN E__EMPLOYEE ON PDR_APPOINTMENTS.ASSIGNEDTO = E__EMPLOYEE.EMPLOYEEID 				
	WHERE	PDR_APPOINTMENTS.JournalId = @journalId
	
	ORDER BY PDR_APPOINTMENTS.APPOINTMENTID Asc
	
 
	-- PROPERTY DETAIL
	EXEC	PDR_GetPropertyInfoByJournalId @journalId
	
END
