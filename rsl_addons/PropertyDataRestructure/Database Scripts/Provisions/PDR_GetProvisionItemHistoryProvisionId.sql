USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetProvisionItemHistoryProvisionId]    Script Date: 01/07/2016 15:12:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    Author: Shaheen
    Date: Nov-26-2015

    Execution Command: Exec [PDR_GetProvisionItemHistoryProvisionId] 1
    Change History:
    
    --   Version      Date             By                      Description
	--   =======     ============    ========           ===========================
	--   v1.0         jan-07-2016     Shaheen Tariq      The most recent provision item would always be displayed in the Disposals tab, however, the details of any previous provision items will only remain within the Disposals tab for the provisions lifespan 
 
  =================================================================================*/
  
ALTER PROCEDURE [dbo].[PDR_GetProvisionItemHistoryProvisionId]
 @provisionId INT=NULL,
  @schemeId INT=NULL,
 @blockId INT=NULL
 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT *      
	FROM PDR_Provision_History
	WHERE PDR_Provision_History.IsActive = 1 AND PDR_Provision_History.ProvisionId = @provisionId AND
	(PDR_Provision_History.SchemeId = @schemeId OR @schemeId IS NULL)AND (PDR_Provision_History.BlockId = @blockId OR @blockId IS NULL) AND
	GETDATE() between InstalledDate and (CASE	WHEN CycleId  = 4 THEN (DATEADD(year,LifeSpan,InstalledDate))
												WHEN CycleId  = 3 THEN (DATEADD(month,LifeSpan,InstalledDate))
												WHEN CycleId  = 2 THEN (DATEADD(week,LifeSpan,InstalledDate)) 
										 ELSE  (DATEADD(day,LifeSpan,InstalledDate)) END)
END
