/****** Object:  StoredProcedure [dbo].[PDR_GetProvisionNotes]    Script Date: 11/26/2015 13:24:35 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Shaheen
-- Create date: 25 Nov 2015
-- Description:	Get provision Item Notes By Id
-- =============================================
CREATE PROCEDURE [dbo].[PDR_GetProvisionNotes]  (    
@schemeId int=null,   
@blockId int=null,     
@provisionId int    
)    
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
  DECLARE @searchCriteria NVARCHAR(2000),@selectClause NVARCHAR(2000),@whereClause NVARCHAR(1000),@MainQuery NVARCHAR(4000)  
  --========================================================================================      
  -- Begin building SearchCriteria clause      
  -- These conditions will be added into where clause based on search criteria provided      
      
    
    SET @searchCriteria = 'And 1=1 '
       
IF @schemeId IS NOT NULL AND @schemeId > 0    
   BEGIN    
    SET @searchCriteria = +'  AND SchemeId = ' + CONVERT(varchar(10), @schemeId) + ' '      
   END       
  IF @blockId IS NOT NULL AND  @blockId >0    
   BEGIN    
    SET @searchCriteria =@searchCriteria + CHAR(10) +'  AND BlockId = ' +  CONVERT(varchar(10), @blockId) + ' '      
   END    
  -- End building SearchCriteria clause         
  --========================================================================================      
      
    -- Insert statements for procedure here    
  
   
 SET @selectClause = ' SELECT [SID],ProvisionId,Notes, CONVERT(varchar,CreatedOn,103 )   CreatedOn,LEFT(e.FIRSTNAME, 1)+ LEFT(e.LASTNAME, 1)  CreatedBy    
 FROM PDR_Provision_Notes  
 INNER JOIN E__EMPLOYEE e ON e.EMPLOYEEID = CreatedBy '      
      
 -- Begin building WHERE clause      
      
  -- This Where clause contains subquery to exclude already displayed records           
       
  SET @whereClause = CHAR(10) + ' WHERE ProvisionId ='+  CONVERT(varchar(10), @provisionId) + CHAR(10) + @searchCriteria      
  PRINT(@searchCriteria) 
  PRINT(@whereClause)   
  -- End building WHERE clause      
    
   -- Begin building the main select Query      
      
  SET @MainQuery = @selectClause  + @whereClause    
      
  -- End building the main select Query      
 PRINT(@MainQuery)  
 EXEC(@MainQuery)  
END 
GO


