
/****** Object:  StoredProcedure [dbo].[PDR_GetProvisionImages]    Script Date: 11/26/2015 13:24:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Shaheen Tariq
-- Create date: 25 Nov 2015
-- Description:	Get Provision Item Images
-- =============================================
CREATE PROCEDURE [dbo].[PDR_GetProvisionImages] (  
@schemeId int=null, 
@blockId int=null,   
@provisionId int  
)  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
  DECLARE @searchCriteria NVARCHAR(2000),@selectClause NVARCHAR(2000),@whereClause NVARCHAR(1000),@MainQuery NVARCHAR(4000)
  --========================================================================================    
  -- Begin building SearchCriteria clause    
  -- These conditions will be added into where clause based on search criteria provided 
IF @schemeId > 0  
   BEGIN  
    SET @searchCriteria = +'  AND SchemeId = ' + CONVERT(varchar(10), @schemeId)    
   END     
  IF @blockId >0  
   BEGIN  
    SET @searchCriteria = +'  AND BlockId = ' +  CONVERT(varchar(10), @blockId)    
   END 
   
  -- End building SearchCriteria clause       
  --========================================================================================    
    
 
 
 SET @selectClause = ' SELECT [SID],ProvisionId,i.ImagePath,ImageName,CONVERT(varchar,CreatedOn,103 ) CreatedOn,LEFT(e.FIRSTNAME, 1)+'' ''+ e.LASTNAME  CreatedBy,i.Title  
 FROM PDR_Provision_Images i 
  INNER JOIN E__EMPLOYEE e ON e.EMPLOYEEID = CreatedBy    '    
    
 -- Begin building WHERE clause    
    
  -- This Where clause contains subquery to exclude already displayed records         
     
  SET @whereClause = CHAR(10) + ' WHERE ProvisionId ='+  CONVERT(varchar(10), @provisionId) + CHAR(10) + @searchCriteria    
    
  -- End building WHERE clause    
  
   -- Begin building the main select Query    
    
  SET @MainQuery = @selectClause  + @whereClause  
    
  -- End building the main select Query    
 PRINT(@MainQuery)
 EXEC(@MainQuery) 
    -- Insert statements for procedure here  
END  
GO


