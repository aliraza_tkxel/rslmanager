/****** Object:  Table [dbo].[PDR_Provision_Notes]    Script Date: 11/26/2015 13:18:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PDR_Provision_Notes](
	[SID] [int] IDENTITY(1,1) NOT NULL,
	[ProvisionId] [int] NULL,
	[Notes] [nvarchar](300) NULL,
	[CreatedOn] [smalldatetime] NULL,
	[CreatedBy] [int] NULL,
	[SchemeId] [int] NULL,
	[BlockId] [int] NULL,
 CONSTRAINT [PK_PDR_Provision_Notes] PRIMARY KEY CLUSTERED 
(
	[SID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[PDR_Provision_Notes]  WITH CHECK ADD  CONSTRAINT [FK_PDR_Provision_Notes_PDR_Provisions] FOREIGN KEY([ProvisionId])
REFERENCES [dbo].[PDR_Provisions] ([ProvisionId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[PDR_Provision_Notes] CHECK CONSTRAINT [FK_PDR_Provision_Notes_PDR_Provisions]
GO


