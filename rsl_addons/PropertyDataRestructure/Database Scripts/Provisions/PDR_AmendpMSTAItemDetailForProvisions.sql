USE [RSLBHALive]
GO

/****** Object:  StoredProcedure [dbo].[PDR_AmendpMSTAItemDetailForProvisions]    Script Date: 01/01/2016 16:17:39 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/* =================================================================================       
  Author: Shaheen
  Creation Date: 24 Nov 2015  
  
  UpdatedBy: Shaheen
  Description: Changed the attribute Type to Provision instead of attribute
=================================================================================*/ 
ALTER PROCEDURE [dbo].[PDR_AmendpMSTAItemDetailForProvisions] 
  -- Add the parameters for the stored procedure here              
  @PovisionId INT , 
  @schemeId   INT = NULL, 
  @blockId    INT = NULL,  
  @UpdatedBy  INT, 
  @MSATDetail AS AS_MSATDETAIL readonly 
AS 
  BEGIN 
      -- SET NOCOUNT ON added to prevent extra result sets from              
      -- interfering with SELECT statements.              
      SET nocount ON; 

                
      --=================================================================================          
      ---Variable to insert or update data in PA_PROPERTY_ATTRIBUTES              
      DECLARE @IsRequired           BIT, 
              @LastDate             DATETIME, 
              @Cycle                INT, 
              @CycleTypeId          INT, 
              @NextDate             DATETIME, 
              @AnnualApportionment  FLOAT, 
              @MSATTypeId           INT, 
              @MSATId               INT, 
              @ToBeArrangedStatusId INT, 
              @CompletedStatus      INT 
      ---Declare Cursor variable              
      DECLARE @ItemToInsertCursor CURSOR 

      SELECT @ToBeArrangedStatusId = pdr_status.statusid 
      FROM   pdr_status 
      WHERE  title = 'To be Arranged' 

      DECLARE @attributeTypeId INT 

      SELECT @attributeTypeId = pdr_attributestype.attributetypeid 
      FROM   pdr_attributestype 
      WHERE  pdr_attributestype.attributetype = 'Provision' 

      SELECT @CompletedStatus = pdr_status.statusid 
      FROM   pdr_status 
      WHERE  title = 'Completed' 

      --Initialize cursor              
      SET @ItemToInsertCursor = CURSOR fast_forward 
      FOR SELECT [isrequired], 
                 [lastdate], 
                 [cycle], 
                 [cycletypeid], 
                 [nextdate], 
                 [annualapportionment], 
                 [msattypeid] 
          FROM   @MSATDetail; 

      --Open cursor              
      OPEN @ItemToInsertCursor 

      ---fetch row from cursor              
      FETCH next FROM @ItemToInsertCursor INTO @IsRequired, @LastDate, @Cycle, 
      @CycleTypeId, @NextDate, @AnnualApportionment, @MSATTypeId 

      ---Iterate cursor to get record row by row              
      WHILE @@FETCH_STATUS = 0 
        BEGIN 
            SET @MSATId = 0 

            SELECT @MSATId = msatid 
            FROM   pdr_msat 
            WHERE  ( provisionId = @PovisionId 
                      OR @PovisionId IS NULL ) 
                   AND ( schemeid = @schemeId 
                          OR @schemeId IS NULL ) 
                   AND ( blockid = @blockId 
                          OR @blockId IS NULL ) 
                   AND msattypeid = @MSATTypeId 

            IF @LastDate = '1900-01-01 00:00:00.000' 
                OR @LastDate IS NULL 
              SET @LastDate = NULL 

            IF @NextDate = '1900-01-01 00:00:00.000' 
                OR @NextDate IS NULL 
              SET @NextDate = NULL 

            IF @MSATId > 0 
              BEGIN 
                  UPDATE pdr_msat 
                  SET    cycletypeid = @CycleTypeId, 
                         isrequired = @IsRequired, 
                         cycle = @Cycle, 
                         lastdate = @LastDate, 
                         nextdate = @NextDate, 
                         annualapportionment = @AnnualApportionment, 
                         isactive = 1 
                  WHERE  msatid = @MSATId 

                  IF @IsRequired = 1 
                    BEGIN 
                        IF EXISTS(SELECT 1 
                                  FROM   [pdr_journal] 
                                  WHERE  msatid = @MSATId) 
                          BEGIN 
                              UPDATE [pdr_journal] 
                              SET    [statusid] = @ToBeArrangedStatusId, 
                                     [creationdate] = Getdate(), 
                                     [createdby] = @UpdatedBy 
                              WHERE  msatid = @MSATId 
                                     AND statusid IN ( @ToBeArrangedStatusId, 
                                                       @CompletedStatus 
                                                     ) 
                          END 
                        ELSE 
                          BEGIN 
                              INSERT INTO [pdr_journal] 
                                          ([msatid], 
                                           [statusid], 
                                           [creationdate], 
                                           [createdby]) 
                              VALUES      (@MSATId, 
                                           @ToBeArrangedStatusId, 
                                           Getdate(), 
                                           @UpdatedBy) 
                          END 
                    END 
              END 
            ELSE 
              BEGIN 
                  INSERT INTO pdr_msat 
                              (isrequired, 
                               ProvisionId, 
                               lastdate, 
                               cycle, 
                               cycletypeid, 
                               nextdate, 
                               annualapportionment, 
                               msattypeid, 
                               isactive, 
                               schemeid, 
                               blockid, 
                               attributetypeid) 
                  VALUES      (@IsRequired, 
                               @PovisionId, 
                               @LastDate, 
                               @Cycle, 
                               @CycleTypeId, 
                               @NextDate, 
                               @AnnualApportionment, 
                               @MSATTypeId, 
                               1, 
                               @schemeId, 
                               @blockId, 
                               @attributeTypeId) 

                  SELECT @MSATId = Scope_identity() 

                  IF @IsRequired = 1 
                    BEGIN 
                        INSERT INTO [pdr_journal] 
                                    ([msatid], 
                                     [statusid], 
                                     [creationdate], 
                                     [createdby]) 
                        VALUES      (@MSATId, 
                                     @ToBeArrangedStatusId, 
                                     Getdate(), 
                                     @UpdatedBy) 
                    END 
              END 

            FETCH next FROM @ItemToInsertCursor INTO @IsRequired, @LastDate, 
            @Cycle, 
            @CycleTypeId, @NextDate, @AnnualApportionment, @MSATTypeId 
        END 

      --close & deallocate cursor                
      CLOSE @ItemToInsertCursor 

      DEALLOCATE @ItemToInsertCursor 
  END


GO


