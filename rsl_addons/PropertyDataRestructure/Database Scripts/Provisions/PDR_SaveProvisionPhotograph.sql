USE [RSLBHALive]
GO

/****** Object:  StoredProcedure [dbo].[PDR_SaveProvisionPhotograph]    Script Date: 12/01/2015 14:55:19 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Shaheen Tariq
-- Create date: 25 Nov 2015
-- Description:	Save Provision Images
-- =============================================
CREATE PROCEDURE [dbo].[PDR_SaveProvisionPhotograph] (    

@provisionId int,    
@title varchar(500),    
@uploadDate smalldatetime,    
@imagePath varchar(1000),    
@imageName varchar(500),    
@createdBy int,  
@schemeId int=null, 
@blockId int=null, 
@isDefaultImage bit = null 
)    
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;        
 DECLARE @ProvisionImageId INT 
 INSERT INTO PDR_Provision_Images    
 ([ProvisionId],[ImagePath],[ImageName],[CreatedOn],[CreatedBy],[Title],SchemeId,BlockId )    
 VALUES    
 (@provisionId,@imagePath ,@imageName,@uploadDate,@createdBy,@title,@schemeId,@blockId)    
   SET @ProvisionImageId=SCOPE_IDENTITY()  
END 

GO


