/****** Object:  StoredProcedure [dbo].[PDR_SaveProvisionNotes]    Script Date: 11/26/2015 13:27:04 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		SHAHEEN
-- Create date: 25Nov2015
-- Description:	Save Notes for Provisions
-- =============================================
CREATE PROCEDURE [dbo].[PDR_SaveProvisionNotes] (
@provisionId varchar(500),
@schemeId int=null, 
@blockId int=null, 
@note varchar(500),
@createdBy int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    -- Insert statements for procedure here
	INSERT INTO PDR_Provision_Notes ([ProvisionId],[Notes],[CreatedOn],[CreatedBy],[SchemeId],[BlockId])
	VALUES(@provisionId,@note ,GETDATE(),@createdBy,@schemeId,@blockId)
END

GO


