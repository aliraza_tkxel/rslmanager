/****** Object:  Table [dbo].[PDR_Provision_Images]    Script Date: 11/26/2015 13:19:18 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PDR_Provision_Images](
	[SID] [int] IDENTITY(1,1) NOT NULL,
	[ProvisionId] [int] NULL,
	[ImagePath] [nvarchar](400) NULL,
	[ImageName] [nvarchar](50) NULL,
	[CreatedOn] [smalldatetime] NULL,
	[CreatedBy] [int] NULL,
	[Title] [nvarchar](500) NULL,
	[ImageIdentifier] [nvarchar](64) NULL,
	[SchemeId] [int] NULL,
	[BlockId] [int] NULL,
	[Isdefault] [bit] NULL,
 CONSTRAINT [PK_PDR_Provision_Images] PRIMARY KEY CLUSTERED 
(
	[SID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[PDR_Provision_Images]  WITH CHECK ADD  CONSTRAINT [FK_PDR_Provision_Images_PDR_Provisions] FOREIGN KEY([ProvisionId])
REFERENCES [dbo].[PDR_Provisions] ([ProvisionId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO

ALTER TABLE [dbo].[PDR_Provision_Images] CHECK CONSTRAINT [FK_PDR_Provision_Images_PDR_Provisions]
GO


