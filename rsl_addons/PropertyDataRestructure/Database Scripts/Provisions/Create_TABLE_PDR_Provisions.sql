/****** Object:  Table [dbo].[PDR_Provisions]    Script Date: 11/26/2015 13:18:20 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[PDR_Provisions](
	[ProvisionId] [int] IDENTITY(1,1) NOT NULL,
	[ProvisionName] [nvarchar](100) NOT NULL,
	[ProvisionParentId] [int] NULL,
	[Manufacturer] [nvarchar](100) NOT NULL,
	[Model] [nvarchar](50) NOT NULL,
	[SerialNumber] [nvarchar](100) NOT NULL,
	[InstalledDate] [smalldatetime] NOT NULL,
	[InstallationCost] [decimal](18, 0) NOT NULL,
	[LifeSpan] [int] NOT NULL,
	[CycleId] [int] NULL,
	[ReplacementDue] [smalldatetime] NOT NULL,
	[ConditionRating] [int] NOT NULL,
	[IsActive] [bit] NOT NULL,
	[SchemeId] [int] NULL,
	[BlockId] [int] NULL,
	[LastReplaced] [smalldatetime] NOT NULL,
	[UpdatedBy] [int] NULL,
 CONSTRAINT [PK_PDR_Provisions] PRIMARY KEY CLUSTERED 
(
	[ProvisionId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[PDR_Provisions]  WITH CHECK ADD  CONSTRAINT [FK_PDR_Provisions_PDR_Provisions] FOREIGN KEY([CycleId])
REFERENCES [dbo].[PDR_CycleType] ([CycleTypeId])
GO

ALTER TABLE [dbo].[PDR_Provisions] CHECK CONSTRAINT [FK_PDR_Provisions_PDR_Provisions]
GO


