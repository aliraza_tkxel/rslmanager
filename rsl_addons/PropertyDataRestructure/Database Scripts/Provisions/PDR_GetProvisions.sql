/****** Object:  StoredProcedure [dbo].[PDR_GetProvisions]    Script Date: 11/26/2015 13:24:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

/* =================================================================================    
    Author: Shaheen
    Date: Nov-18-2015

    Execution Command: Exec [PDR_GetProvisions]
  =================================================================================*/
  
CREATE PROCEDURE [dbo].[PDR_GetProvisions]
 @schemeId INT=NULL,
 @blockId INT=NULL
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

--=================================================================            
--Get All Provisions 
--=================================================================            

	SELECT * 
	FROM PDR_Provisions
	WHERE PDR_Provisions.IsActive = 1 AND (PDR_Provisions.SchemeId = @schemeId OR @schemeId IS NULL)AND (PDR_Provisions.BlockId = @blockId OR @blockId IS NULL)
--=================================================================            
--Get All MSAT related to Provisions
--=================================================================  

SELECT 
PDR_MSAT.MSATId,PDR_MSAT.CycleTypeId, PDR_MSAT.IsRequired, convert(varchar(20),PDR_MSAT.LastDate,103)AS LastDate,
PDR_MSAT.Cycle,convert(varchar(20),PDR_MSAT.NextDate,103)AS NextDate,PDR_MSAT.AnnualApportionment,CycleType, 
PDR_MSAT.MSATTypeId,  MSATTypeName, PDR_MSAT.ProvisionId

FROM PDR_MSAT
INNER JOIN PDR_Provisions ON PDR_Provisions.ProvisionId= PDR_MSAT.ProvisionId
LEFT JOIN PDR_CycleType ON PDR_MSAT.CycleTypeId = PDR_CycleType.CycleTypeId And PDR_CycleType.IsActive = 1
INNER JOIN PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId and PDR_MSATType.IsActive = 1  
WHERE (PDR_MSAT.SchemeId = @schemeId OR @schemeId IS NULL)AND (PDR_MSAT.BlockId = @blockId OR @blockId IS NULL) 
	
END




GO


