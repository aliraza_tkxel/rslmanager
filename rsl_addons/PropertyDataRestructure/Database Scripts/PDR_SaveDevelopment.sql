﻿

-- =============================================
-- Author:           Salman Nazir
-- Create date:      12/02/2014
-- Description:      save development on SaveDevelopment.aspx
-- History:          12/02/2014 SN : save development on SaveDevelopment.aspx
--                   23/07/2015 Ali Raza : Add Insert and update data in F_Head
--                  
-- =============================================
ALTER PROCEDURE [dbo].[PDR_SaveDevelopment]
	@DevelopmentId int
	,@DEVELOPMENTNAME varchar(50)
	,@PATCH int
	,@ADDRESS1 varchar(500)
	,@ADDRESS2 varchar(500)
	,@PostCode varchar(50)
	,@TownCity varchar(50)
	,@COUNTY varchar(50)
	,@Architect varchar(50),@PROJECTMANAGER varchar(50)
	,@CompanyId int
	,@DevelopmentType int
	,@DevelopmentStatus int
	,@LandValue float
	,@ValueDate smalldatetime --new param
	,@PurchaseDate smalldatetime
    ,@GrantAmount float,@BorrowedAmount float,@OutlinePlanningApplication smalldatetime
    ,@DetailedPlanningApplication smalldatetime,@OutlinePlanningApproval smalldatetime
    ,@DetailedPlanningApproval smalldatetime,@NoofUnits int
    ,@localAuthorityId INT = -1 ,@LandPurchasePrice float 
    ,@UserId INT
    ,@developmentFundingDetail AS PDR_DevelopmentFundingInfo READONLY
    ,@UpdatedDevelopmentId varchar(50) Output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
	--===================================
	-- Setting null to default values
	--===================================
	IF(@LandValue = -1)
	BEGIN
		SET @LandValue = null;
	END
	
	IF(@GrantAmount= -1 )
	BEGIN
		SET @GrantAmount = null;
	END
	
	IF(@BorrowedAmount = -1)
	BEGIN
		SET @BorrowedAmount = null;
	END 
	
	IF( @NoofUnits = -1)
	BEGIN
		SET @NoofUnits = null;
	END
	IF (@localAuthorityId = -1)
	BEGIN
		 SET @localAuthorityId = NULL
	END
	
	IF(@LandPurchasePrice = -1)
	BEGIN
		SET @LandPurchasePrice = null;
	END
	
	
	
	SET NOCOUNT ON;
	BEGIN TRANSACTION
	BEGIN TRY 	
	
	Declare @HeadId INT
	
	--=======================================================
	-- DEVELOPMENT INFO
	--=======================================================
	
	IF (@DevelopmentId > 0)
	BEGIN
	
	UPDATE [RSLBHALive].[dbo].[PDR_DEVELOPMENT]
   SET [PATCHID] = @PATCH
      ,[DEVELOPMENTNAME] = @DEVELOPMENTNAME
      ,[DEVELOPMENTTYPE] = @DevelopmentType
      ,[NUMBEROFUNITS] = @NoofUnits
      ,[PROJECTMANAGER] = @PROJECTMANAGER
      ,[ADDRESS1] = @ADDRESS1
      ,[ADDRESS2] = @ADDRESS2
      ,[TOWN] = @TownCity
      ,[COUNTY] = @COUNTY
      ,[Architect] = @Architect    
      ,[LandValue] = @LandValue
      ,[ValueDate] = @ValueDate
      ,[GrantAmount] = @GrantAmount
      ,[BorrowedAmount] = @BorrowedAmount
      ,[OutlinePlanningApplication] = @OutlinePlanningApplication
      ,[DetailedPlanningApplication] = @DetailedPlanningApplication
      ,[OutlinePlanningApproval] = @OutlinePlanningApproval
      ,[DetailedPlanningApproval] = @DetailedPlanningApproval
      ,[PostCode] = @PostCode
      ,[PurchaseDate] = @PurchaseDate
      ,[DEVELOPMENTSTATUS] = @DevelopmentStatus
      ,[LOCALAUTHORITY] = @localAuthorityId
      ,[LandPurchasePrice] = @LandPurchasePrice
	  ,[CompanyId] = @CompanyId
	WHERE DEVELOPMENTID = @DevelopmentId
		
	--UPDATE THE HEAD NAME UNDER THE DEVELOPMENT COST CENTRE AS FOLLOWS
		UPDATE F_HEAD SET DESCRIPTION = D.DEVELOPMENTNAME FROM PDR_DEVELOPMENT D 
		INNER JOIN F_DEVELOPMENT_HEAD_LINK DHL ON DHL.DEVELOPMENTID = D.DEVELOPMENTID 
		INNER JOIN F_HEAD H ON H.HEADID = DHL.HEADID
		WHERE D.DEVELOPMENTID = @DevelopmentId
		
		
		
		SET @UpdatedDevelopmentId = @DevelopmentId
		
		
		
	END
	ELSE
		BEGIN
	
			IF Not Exists(select top 1 * from PDR_DEVELOPMENT Where DEVELOPMENTNAME = @DEVELOPMENTNAME AND ADDRESS1= @Address1 And TOWN = @TownCity)
			BEGIN
    
				-- Insert statements for procedure here
				INSERT INTO PDR_DEVELOPMENT (DEVELOPMENTNAME,PATCHID,POSTCODE,ADDRESS1,ADDRESS2,TOWN,COUNTY,Architect,PROJECTMANAGER,PurchaseDate,LandValue,ValueDate
				,GrantAmount,BorrowedAmount,OutlinePlanningApplication,DetailedPlanningApplication,OutlinePlanningApproval,DetailedPlanningApproval, LOCALAUTHORITY,LandPurchasePrice, Companyid)
				VALUES(@DEVELOPMENTNAME,@PATCH,@PostCode,@ADDRESS1,@ADDRESS2,@TownCity,@COUNTY,@PROJECTMANAGER,@Architect,@PurchaseDate,@LandValue,@ValueDate
				,@GrantAmount,@BorrowedAmount,@OutlinePlanningApplication,@DetailedPlanningApplication,@OutlinePlanningApproval,@DetailedPlanningApproval, @localAuthorityId, @LandPurchasePrice, @CompanyId)
      
				SELECT	@UpdatedDevelopmentId = SCOPE_IDENTITY()
				SET		@DevelopmentId = @UpdatedDevelopmentId
				--Return @Developmentid
				
			--NEXT INSERT A HEAD UNDER THE DEVELOPMENT COST CENTRE	
			INSERT INTO F_HEAD (DESCRIPTION, HEADDATE, USERID, LASTMODIFIED, COSTCENTREID) 
			SELECT @DEVELOPMENTNAME, GETDATE(), @UserId, GETDATE(), (SELECT DEFAULTVALUE FROM RSL_DEFAULTS WHERE DEFAULTNAME = 'DEVELOPMENTID') FROM PDR_DEVELOPMENT WHERE DEVELOPMENTID = @DevelopmentId
			SELECT @HeadId=SCOPE_IDENTITY() 
				
			--NEXT INSERT A DUMMY ROW WITH A VALUE OF 0 INTO F_HEAD_ALLOCATION	
			INSERT INTO F_HEAD_ALLOCATION (HEADID, HEADALLOCATION, MODIFIED, MODIFIEDBY, ACTIVE)
			VALUES (@HeadId, 0, GETDATE(), @UserId, 1)
			
			--FINALLY MAKE THE LINK BETWEEN THE HEAD AND DEVELOPMENT
			INSERT INTO F_DEVELOPMENT_HEAD_LINK (DEVELOPMENTID, HEADID) 
			VALUES (@DevelopmentId,@HeadId)
			
			END
			ELSE
			BEGIN
				SET @UpdatedDevelopmentId = 'Exist'
			END
		
		END
	
	--=======================================================
	-- DEVELOPMENT FUNDING SOURCE
	--=======================================================

DECLARE fundingSourceCursor CURSOR FOR SELECT
	*
FROM @developmentFundingDetail
OPEN fundingSourceCursor

-- Declare Variable to use with cursor
DECLARE
@DEVELOPMENTFUNDINGID int,
@FUNDINGAUTHORITYID int ,
@FUNDINGAUTHORITY VARCHAR(100) ,
@FUNDGRANTAMOUNT float
	
		-- Fetch record for First loop iteration.
		FETCH NEXT FROM fundingSourceCursor INTO @DEVELOPMENTFUNDINGID,@FUNDINGAUTHORITYID,@FUNDINGAUTHORITY,@FUNDGRANTAMOUNT

		WHILE @@FETCH_STATUS = 0 BEGIN
	
		IF (@DEVELOPMENTFUNDINGID = -1 AND @UpdatedDevelopmentId <> 'Exist')
		BEGIN
		
			INSERT INTO [PDR_DEVELOPMENT_FUNDING]
           ([FUNDINGAUTHORITYID]
           ,[GRANTAMOUNT]
           ,[DEVELOPMENTID])
			VALUES
           (@FUNDINGAUTHORITYID
           ,@FUNDGRANTAMOUNT
           ,@DevelopmentId)	
           
		END

-- Fetch record for next loop iteration.
FETCH NEXT FROM fundingSourceCursor INTO @DEVELOPMENTFUNDINGID,@FUNDINGAUTHORITYID,@FUNDINGAUTHORITY,@FUNDGRANTAMOUNT
END

CLOSE fundingSourceCursor
DEALLOCATE fundingSourceCursor
	
END TRY 
	BEGIN CATCH 	
		IF @@TRANCOUNT >0	  
		BEGIN	   
			  PRINT 'Some thing went wrong with this transaction'
			SET @UpdatedDevelopmentId='Failed'
			ROLLBACK TRANSACTION;	
		END 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
		@ErrorSeverity, -- Severity.
		@ErrorState -- State.
		);
	END CATCH  
	 
IF @@TRANCOUNT >0
	BEGIN
		PRINT 'Transaction completed successfully'	
		COMMIT TRANSACTION;	
	END
END