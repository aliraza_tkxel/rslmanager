USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[GS_SCHEME_AVAILABLE_WO]    Script Date: 02/03/2015 09:22:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- ================================================
-- Author:		ROB
-- Create date: 29/04/2008
-- Description:	This procedure is used to get the 
--				scheme list for work order creation
-- Updated: Umair
-- Update Date: 15/05/2008
-- =================================================
ALTER PROCEDURE [dbo].[GS_SCHEME_AVAILABLE_WO]
@ORGID INT = NULL,
@PATCHID INT=NULL

--EXEC GS_SCHEME_AVAILABLE_WO 1270, 18

--THIS GETS THE PATCH FOR THE WORK ORDER 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

 -- Insert statements for procedure here
SELECT ISNULL(PS.SCHEMEID,0) AS SCHEMEID,PS.SCHEMENAME
FROM S_SCOPE S 
	INNER JOIN S_SCOPETOPATCHANDSCHEME SS ON S.SCOPEID=SS.SCOPEID 
	INNER JOIN P_SCHEME PS ON PS.SCHEMEID=SS.SchemeId 
WHERE SS.PATCHID = COALESCE(@PATCHID, SS.PATCHID) AND 
	S.ORGID= COALESCE(@ORGID, S.ORGID)   AND 
	SS.SchemeId IS NOT NULL AND 
	S.RENEWALDATE > GETDATE() AND
	S.SCOPEID NOT IN (SELECT DISTINCT SCOPEID FROM C_REPAIR WHERE SCOPEID IS NOT NULL) 
END


