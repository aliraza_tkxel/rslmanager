USE [RSLBHALive]
GO
/****** Object:  Table [dbo].[PDR_MSATType]    Script Date: 01/29/2015 18:28:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PDR_MSATType](
	[MSATTypeId] [int] NOT NULL,
	[MSATTypeName] [nvarchar](200) NOT NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_PDR_MSATType] PRIMARY KEY CLUSTERED 
(
	[MSATTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[PDR_MSATType] ([MSATTypeId], [MSATTypeName], [IsActive]) VALUES (1, N'PAT Testing', 1)
INSERT [dbo].[PDR_MSATType] ([MSATTypeId], [MSATTypeName], [IsActive]) VALUES (2, N'Cyclic Maintenance', 1)
INSERT [dbo].[PDR_MSATType] ([MSATTypeId], [MSATTypeName], [IsActive]) VALUES (3, N'Service Charge', 1)
INSERT [dbo].[PDR_MSATType] ([MSATTypeId], [MSATTypeName], [IsActive]) VALUES (4, N'M&E Servicing', 1)
