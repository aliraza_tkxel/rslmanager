BEGIN TRANSACTION
BEGIN TRY

IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES 
           WHERE TABLE_NAME = N'PA_AREA')
BEGIN
  PRINT 'Table Exists';
  
IF NOT EXISTS (SELECT * FROM sys.columns WHERE  object_id = OBJECT_ID(N'[dbo].[PA_AREA]' ) AND name = 'IsForSchemeBlock')
BEGIN
	PRINT 'IsForSchemeBlock Column not Added';
	SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON

ALTER TABLE dbo.PA_AREA
	DROP CONSTRAINT FK_PA_AREA_PA_LOCATION

ALTER TABLE dbo.PA_LOCATION SET (LOCK_ESCALATION = TABLE)

select Has_Perms_By_Name(N'dbo.PA_LOCATION', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.PA_LOCATION', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.PA_LOCATION', 'Object', 'CONTROL') as Contr_Per 

ALTER TABLE dbo.PA_AREA
	DROP CONSTRAINT DF__PA_AREA__IsActiv__3911995D

ALTER TABLE dbo.PA_AREA
	DROP CONSTRAINT DF__PA_AREA__IsActiv__1852F16C

CREATE TABLE dbo.Tmp_PA_AREA
	(
	AreaID int NOT NULL IDENTITY (1, 1),
	AreaName varchar(20) NOT NULL,
	LocationId smallint NULL,
	AreaSorder int NULL,
	ShowInApp bit NULL,
	IsActive bit NULL,
	IsForSchemeBlock bit NULL
	)  ON [PRIMARY]

ALTER TABLE dbo.Tmp_PA_AREA SET (LOCK_ESCALATION = TABLE)

ALTER TABLE dbo.Tmp_PA_AREA ADD CONSTRAINT
	DF__PA_AREA__IsActiv__3911995D DEFAULT ((1)) FOR ShowInApp

ALTER TABLE dbo.Tmp_PA_AREA ADD CONSTRAINT
	DF__PA_AREA__IsActiv__1852F16C DEFAULT ((1)) FOR IsActive

SET IDENTITY_INSERT dbo.Tmp_PA_AREA ON

IF EXISTS(SELECT * FROM dbo.PA_AREA)
	 EXEC('INSERT INTO dbo.Tmp_PA_AREA (AreaID, AreaName, LocationId, AreaSorder, ShowInApp, IsActive)
		SELECT AreaID, AreaName, LocationId, AreaSorder, ShowInApp, IsActive FROM dbo.PA_AREA WITH (HOLDLOCK TABLOCKX)')

SET IDENTITY_INSERT dbo.Tmp_PA_AREA OFF

ALTER TABLE dbo.PA_ITEM
	DROP CONSTRAINT FK_PA_ITEM_PA_AREA

DROP TABLE dbo.PA_AREA

EXECUTE sp_rename N'dbo.Tmp_PA_AREA', N'PA_AREA', 'OBJECT' 

ALTER TABLE dbo.PA_AREA ADD CONSTRAINT
	PK_PA_AREA PRIMARY KEY CLUSTERED 
	(
	AreaID
	) WITH( PAD_INDEX = OFF, FILLFACTOR = 100, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

ALTER TABLE dbo.PA_AREA WITH NOCHECK ADD CONSTRAINT
	FK_PA_AREA_PA_LOCATION FOREIGN KEY
	(
	LocationId
	) REFERENCES dbo.PA_LOCATION
	(
	LocationID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
	

select Has_Perms_By_Name(N'dbo.PA_AREA', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.PA_AREA', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.PA_AREA', 'Object', 'CONTROL') as Contr_Per


ALTER TABLE dbo.PA_ITEM WITH NOCHECK ADD CONSTRAINT
	FK_PA_ITEM_PA_AREA FOREIGN KEY
	(
	AreaID
	) REFERENCES dbo.PA_AREA
	(
	AreaID
	) ON UPDATE  CASCADE 
	 ON DELETE  CASCADE 
	

ALTER TABLE dbo.PA_ITEM SET (LOCK_ESCALATION = TABLE)

--COMMIT
select Has_Perms_By_Name(N'dbo.PA_ITEM', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.PA_ITEM', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.PA_ITEM', 'Object', 'CONTROL') as Contr_Per 
      END--if
ELSE
	BEGIN 
		PRINT 'Coloumn already exits'
	END	

	
END --if
IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, 
				@ErrorSeverity, 
				@ErrorState 
			);
Print (@ErrorMessage)
END CATCH

GO
BEGIN TRANSACTION
BEGIN TRY
IF Not EXISTS (Select 1 from PA_AREA where IsForSchemeBlock=1)
	BEGIN  
		Update PA_AREA set IsForSchemeBlock=0
	END
UPDATE PA_AREA set IsActive=0  from PA_AREA
INNER JOIN PA_LOCATION on PA_AREA.LocationId= PA_LOCATION.LocationID
Where LocationName = 'Externals' And AreaName='Communal'
	
PRINT('Deactivate Externals>Communal for both Properties and Scheme/Block')

--Common Areas
IF Not EXISTS (SELECT 1 from PA_AREA WHERE AreaName='Common Areas' and  IsForSchemeBlock=1)
		BEGIN  
			INSERT INTO PA_AREA(AreaName,AreaSorder,ShowInApp,IsActive,IsForSchemeBlock)
					VALUES('Common Areas',0,0,1,1)
			PRINT 'Common Areas Added Successfully!'		
		END		
	ELSE
		BEGIN 
			PRINT 'Common Areas already exits'
		END
	
	
	--For Communal Facilities
IF Not EXISTS (SELECT 1 from PA_AREA WHERE AreaName='Communal Facilities' and  IsForSchemeBlock=1)
	BEGIN  
	INSERT INTO PA_AREA(AreaName,AreaSorder,ShowInApp,IsActive,IsForSchemeBlock)
			VALUES('Communal Facilities',1,0,1,1)
			PRINT 'Communal Facilities Added Successfully!'		
		END		
	ELSE
		BEGIN 
			PRINT 'Communal Facilities already exits'
		END
	
	
	--For Supplied Services
IF Not EXISTS (SELECT 1 from PA_AREA WHERE AreaName='Supplied Services' and  IsForSchemeBlock=1)
		BEGIN  
			INSERT INTO PA_AREA(AreaName,AreaSorder,ShowInApp,IsActive,IsForSchemeBlock)
			VALUES('Supplied Services',2,0,1,1)
			PRINT 'Supplied Services Added Successfully!'	
		END		
	ELSE
		BEGIN 
			PRINT 'Supplied Services already exits'
		END
	
	--For Building Systems
IF Not EXISTS (SELECT 1 from PA_AREA WHERE AreaName='Building Systems' and  IsForSchemeBlock=1)
	BEGIN  
	INSERT INTO PA_AREA(AreaName,AreaSorder,ShowInApp,IsActive,IsForSchemeBlock)
			VALUES('Building Systems',3,0,1,1)
			
			PRINT 'Building Systems Added Successfully!'	
		END		
	ELSE
		BEGIN 
			PRINT 'Building Systems already exits'
		END
	

	--For Sundry
IF Not EXISTS (SELECT 1 from PA_AREA WHERE AreaName='Sundry' and  IsForSchemeBlock=1)
	BEGIN  
	INSERT INTO PA_AREA(AreaName,AreaSorder,ShowInApp,IsActive,IsForSchemeBlock)
			VALUES('Sundry',4,0,1,1)
			
			PRINT 'Sundry Added Successfully!'	
		END		
	ELSE
		BEGIN 
			PRINT 'Sundry already exits'
		END
		
	
	--For Sundry
IF Not EXISTS (SELECT 1 from PA_AREA WHERE AreaName='Appliances' and  IsForSchemeBlock=1)
	BEGIN  
	INSERT INTO PA_AREA(AreaName,AreaSorder,ShowInApp,IsActive,IsForSchemeBlock)
			VALUES('Appliances',5,0,1,1)
			PRINT 'Appliances Added Successfully!'	
		END		
	ELSE
		BEGIN 
			PRINT 'Appliances already exits'
		END
		
--For Lighting
IF Not EXISTS (SELECT 1 from PA_AREA WHERE AreaName='Lighting' and  IsForSchemeBlock=1)
	BEGIN  
	INSERT INTO PA_AREA(AreaName,AreaSorder,ShowInApp,IsActive,IsForSchemeBlock)
			VALUES('Lighting',6,0,1,1)
			PRINT 'Lighting Added Successfully!'	
		END		
	ELSE
		BEGIN 
			PRINT 'Lighting already exits'
		END	
	IF @@TRANCOUNT > 0
	BEGIN     
		COMMIT TRANSACTION;   	
	END
END TRY
BEGIN CATCH
IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   	
	END
	DECLARE @ErrorScriptMessage NVARCHAR(4000);


	SELECT @ErrorScriptMessage = ERROR_MESSAGE()

Print (@ErrorScriptMessage)
END CATCH