-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,16/02/2015>
-- Description:	<Description,,Save and get Refurbishment History>
-- =============================================
CREATE PROCEDURE PDR_INSERT_AND_RETRIVE_REFURBISHMENT_HISTORY
@requestType varchar(50),
@Id int	,
@USER_ID int=NULL,
@REFURBISHMENT_DATE smalldatetime,
@NOTES nvarchar(200)=NULL,
@ErrorMessage VARCHAR(100) = '' output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	DECLARE @OLD_REFURBISHMENT_DATE smalldatetime
	DECLARE @Flag as Bit
	
	SET @Flag = 0
	
		If(@requestType = 'Block')
		BEGIN
	
			SET @OLD_REFURBISHMENT_DATE=(SELECT REFURBISHMENT_DATE FROM P_REFURBISHMENT_HISTORY WHERE BlockId=@Id AND REFURBISHMENT_DATE=@REFURBISHMENT_DATE )
			
			
			If(@Id IS NOT NULL AND @REFURBISHMENT_DATE IS NOT NULL AND @USER_ID IS NOT NULL AND @OLD_REFURBISHMENT_DATE is null)
			BEGIN
				INSERT INTO P_REFURBISHMENT_HISTORY(BLOCKID,REFURBISHMENT_DATE,USERID,NOTES)
				VALUES(@Id,@REFURBISHMENT_DATE,@USER_ID,@NOTES)
				SET @Flag = 1
			END
			
			If (@Flag = 0)
			BEGIN
			SET @ErrorMessage = 'Information is not added as refurbishment date already exists'
			END
	
			SELECT BlockId,REFURBISHMENT_DATE,FIRSTNAME+', '+LASTNAME USERNAME,NOTES FROM P_REFURBISHMENT_HISTORY,E__EMPLOYEE WHERE SchemeId=@Id and USERID=EMPLOYEEID ORDER BY REFURBISHMENT_DATE desc
		END
		If (@requestType = 'Scheme')
		BEGIN
			SET @OLD_REFURBISHMENT_DATE=(SELECT REFURBISHMENT_DATE FROM P_REFURBISHMENT_HISTORY WHERE SchemeId=@Id AND REFURBISHMENT_DATE=@REFURBISHMENT_DATE )
			
			
			If(@Id IS NOT NULL AND @REFURBISHMENT_DATE IS NOT NULL AND @USER_ID IS NOT NULL AND @OLD_REFURBISHMENT_DATE is null)
			BEGIN
				INSERT INTO P_REFURBISHMENT_HISTORY(SCHEMEID,REFURBISHMENT_DATE,USERID,NOTES)
				VALUES(@Id,@REFURBISHMENT_DATE,@USER_ID,@NOTES)
				SET @Flag = 1
			END
			
			If (@Flag = 0)
			BEGIN
			SET @ErrorMessage = 'Information is not added as refurbishment date already exists'
			END
			
			SELECT SCHEMEID,REFURBISHMENT_DATE,FIRSTNAME+', '+LASTNAME USERNAME,NOTES FROM P_REFURBISHMENT_HISTORY,E__EMPLOYEE WHERE SchemeId=@Id and USERID=EMPLOYEEID ORDER BY REFURBISHMENT_DATE desc
			
		END
END
GO
