USE [RSLBHALive]
GO

/****** Object:  Table [dbo].[P_SCHEME]    Script Date: 03/29/2015 13:42:25 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[P_SCHEME](
	[SCHEMEID] [INT] IDENTITY(1,1) NOT NULL,
	[SCHEMENAME] [NVARCHAR](500) NOT NULL,
	[PHASEID] [INT] NULL,
	[DEVELOPMENTID] [INT] NULL,
	[SCHEMECODE] [NVARCHAR](100) NULL,
 CONSTRAINT [PK_P_SCHEME] PRIMARY KEY CLUSTERED 
(
	[SCHEMEID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


