USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC PDR_CancelArrangedAppointment @journalId,@reason,@isCancelled
-- Author:		Ahmed Mehmood
-- Create date: <15/1/2015>
-- Description:	<Description,cancel appointments associated with Journal Id >
-- Webpage : ArrangedAppointmentSummary.aspx

-- =============================================
CREATE PROCEDURE [dbo].[PDR_CancelArrangedAppointment]
	@journalId int
	,@reason varchar(1000)
	,@isCancelled int = 0 out
AS
BEGIN


BEGIN TRANSACTION;
BEGIN TRY

	-- ========================================================
	--	INSERT INTO PDR_CANCELLED_JOBS
	-- ========================================================
	INSERT INTO PDR_CANCELLED_JOBS (AppointmentId,RecordedOn ,Notes)
	SELECT PDR_APPOINTMENTS.APPOINTMENTID as AppointmentId, GETDATE() as RecordedOn,@reason as Notes 
	FROM PDR_APPOINTMENTS
	WHERE PDR_APPOINTMENTS.JournalId = @journalId
	AND PDR_APPOINTMENTS.APPOINTMENTSTATUS <> 'Complete'
	
	
	-- ========================================================
	--	UPDATE PDR_JOURNAL
	-- ========================================================
	
	UPDATE	PDR_JOURNAL
	SET		PDR_JOURNAL.STATUSID = (SELECT PDR_STATUS.STATUSID 
									FROM PDR_STATUS 
									WHERE PDR_STATUS.TITLE= 'Cancelled' )
	WHERE	PDR_JOURNAL.JOURNALID = @journalId


	
	END TRY
BEGIN CATCH 
    IF @@TRANCOUNT > 0
  BEGIN     
   ROLLBACK TRANSACTION;   
 SET @isCancelled = 0
    
    DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    -- Use RAISERROR inside the CATCH block to return 
    -- error information about the original error that 
    -- caused execution to jump to the CATCH block.
    RAISERROR (@ErrorMessage, -- Message text.
         @ErrorSeverity, -- Severity.
         @ErrorState -- State.
         );
  END
END CATCH;

IF @@TRANCOUNT > 0
 BEGIN  
  COMMIT TRANSACTION;  
  SET @isCancelled = 1
 END

END