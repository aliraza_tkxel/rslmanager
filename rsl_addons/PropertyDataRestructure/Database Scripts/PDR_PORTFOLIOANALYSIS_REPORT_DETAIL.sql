-- Stored Procedure

/* =================================================================================    
    Page Description:      Get Property Detail Portfolio Analysis detail

    Author: Ali Raza
    Creation Date: Dec-18-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-18-2014      Ali Raza           Get Property Detail Portfolio Analysis detail

    Execution Command:

    Exec PDR_PORTFOLIOANALYSIS_REPORT_DETAIL
  =================================================================================*/
ALTER PROCEDURE [dbo].[PDR_PORTFOLIOANALYSIS_REPORT_DETAIL]
	-- Add the parameters for the stored procedure here
		@REPORTDATE SMALLDATETIME,
		@LOCALAUTHORITY INT = NULL,
		@SCHEME INT = NULL,
		@POSTCODE NVARCHAR(15) = NULL,
		@PATCH INT = NULL,
		@ASSETTYPE INT = NULL,
		@PROPERTYTYPE INT = NULL,
		@BEDS NVARCHAR(4) = NULL,
		@OCCUPANCY NVARCHAR(4) = NULL,
		@STATUS INT = NULL,
		@SUBSTATUS INT = NULL,
	--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'PROPERTYID', 
		@sortOrder varchar (5) = 'DESC',
		@totalCount int = 0 output	
AS
BEGIN
	DECLARE 

		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @SelectClauseUnion varchar(3000),
        @fromClauseUnion   varchar(3000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(8000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(2000),

        --variables for paging
        @offset int,
		@limit int

		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1


		--=====================Search Criteria===============================
		SET @searchCriteria = ' 1=1 '


			SET @searchCriteria = @searchCriteria + CHAR(10) +'AND (1=1 '	
				IF @ASSETTYPE IS NOT NULL 
				BEGIN
				SET @searchCriteria = @searchCriteria + CHAR(10) +'AND(P.ASSETTYPE = '+convert(varchar(10),@ASSETTYPE)+' OR '+convert(varchar(10),@ASSETTYPE)+' IS NULL)'
				END
				IF @PROPERTYTYPE IS NOT NULL 
				BEGIN
				SET @searchCriteria = @searchCriteria + CHAR(10) +'AND (P.PROPERTYTYPE = '+convert(varchar(10),@PROPERTYTYPE)+' OR '+convert(varchar(10),@PROPERTYTYPE)+' IS NULL)'
				END
				IF @BEDS IS NOT NULL 
				BEGIN
				IF @BEDS= '?b' BEGIN SET @BEDS='' END

				SET @searchCriteria = @searchCriteria + CHAR(10) +'AND (AT.BED= '''+convert(varchar(10),@BEDS)+''' OR '''+convert(varchar(10),@BEDS)+''' IS NULL)'
    			END
				IF @OCCUPANCY IS NOT NULL 
				BEGIN
				IF @OCCUPANCY= '?p' BEGIN SET @OCCUPANCY='' END

    			SET @searchCriteria = @searchCriteria + CHAR(10) +'AND (AT.OCCUPANCY =+ '''+convert(varchar(10),@OCCUPANCY)+''' OR'''+ convert(varchar(10),@OCCUPANCY)+''' IS NULL)'
				END
				IF @LOCALAUTHORITY > 0
				BEGIN
				SET @searchCriteria = @searchCriteria + CHAR(10) +'AND (D.LOCALAUTHORITY = '+convert(varchar(10),@LOCALAUTHORITY)+' OR '+convert(varchar(10),@LOCALAUTHORITY)+' IS NULL)'
				END
				IF @SCHEME > 0 
				BEGIN
				SET @searchCriteria = @searchCriteria + CHAR(10) +'AND	(SCH.SCHEMEID = '+convert(varchar(10),@SCHEME)+' OR '+convert(varchar(10),@SCHEME)+' IS NULL)'
			    END
				IF @POSTCODE IS NOT NULL 
				BEGIN
			    SET @searchCriteria = @searchCriteria + CHAR(10) +	'AND (P.POSTCODE LIKE '''+@POSTCODE+''' OR '''+@POSTCODE+''' IS NULL)'
				END
				IF @PATCH > 0
				BEGIN
				SET @searchCriteria = @searchCriteria + CHAR(10) +'AND (D.PATCHID = '+convert(varchar(10),@PATCH) +' OR D.PATCHID  IS NULL)'
				END
				IF @STATUS > 0
				BEGIN
				SET @searchCriteria = @searchCriteria + CHAR(10) +'AND (S.NEWSTATUS = '+convert(varchar(10),@STATUS)+' OR '+convert(varchar(10),@STATUS)+' IS NULL)'
				END
				IF @SUBSTATUS > 0
				BEGIN
					SET @searchCriteria = @searchCriteria + CHAR(10) +'AND (S.NEWSUBSTATUS = '+convert(varchar(10),@SUBSTATUS)+' OR '+convert(varchar(10),@SUBSTATUS)+' IS NULL)	'			
				END
				SET @searchCriteria = @searchCriteria + CHAR(10) +'AND S.NEWSTATUS IN (1,2,4)'


				SET @searchCriteria = @searchCriteria + CHAR(10) +') '



		--=======================Select Clause=============================================
		SET @SelectClause = 'Select top ('+convert(char(10),@limit)+')		 
			P.PROPERTYID, ISNULL(SCH.SCHEMENAME,''-'') AS SCHEME
			,LEFT(REPLACE(ISNULL(P.HOUSENUMBER,'''') + '' '' + ISNULL(P.ADDRESS1,'''') + '', '' + ISNULL(P.ADDRESS2,'''') + '', '' + ISNULL(P.TOWNCITY,''''), '', ,'', '', ''),50) AS ADDRESS
			,ISNULL(CAST(S.NEWSTATUS AS VARCHAR), ''?'') AS STATUS
			,ISNULL(PS.DESCRIPTION,''?'') AS STATUSDESCRIPTION
			,ISNULL(S.NEWSUBSTATUS,''-'') AS SUBSTATUS
			,ISNULL(PSU.DESCRIPTION,''-'') AS SUBSTATUSDESCRIPTION
			,ISNULL(FH.RENT,0) AS RENT
			,ISNULL(FH.SERVICES,0) AS SERVICES
			,ISNULL(FH.INELIGSERV,0) AS INELIGSERV
			,ISNULL(FH.SUPPORTEDSERVICES,0)  AS SUPPORTEDSERVICES
			,ISNULL(FH.WATERRATES,0) AS WATERRATES
			,ISNULL(FH.COUNCILTAX,0) AS COUNCILTAX
			,ISNULL(FH.GARAGE,0) AS GARAGE
			,ISNULL(FH.TOTALRENT,0) AS TOTALRENT '


		--============================From Clause============================================
		SET @fromClause = 'FROM P__PROPERTY P
		LEFT JOIN (
				Select PP.PROPERTYID,ISNULL(A.PARAMETERVALUE,''?'') AS BED,ISNULL(PA.PARAMETERVALUE,''?'') AS OCCUPANCY
				FROM P__PROPERTY PP 				
					Left outer Join PA_PROPERTY_ATTRIBUTES A ON A.PROPERTYID = PP.PROPERTYID AND A.ITEMPARAMID = (
									 SELECT ItemParamID from PA_ITEM_PARAMETER
									 INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId= PA_PARAMETER.ParameterID
									  INNER JOIN PA_ITEM on PA_ITEM_PARAMETER.ItemId= PA_ITEM.ItemID
									 Where ParameterName=''Quantity'' And ItemName=''Bedrooms'')
									
					Left outer Join PA_PROPERTY_ATTRIBUTES PA ON PA.PROPERTYID = PP.PROPERTYID AND PA.ITEMPARAMID = (
									 SELECT ItemParamID from PA_ITEM_PARAMETER
									 INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId= PA_PARAMETER.ParameterID
									 Where ParameterName=''Max People'')
     		) AT ON AT.PROPERTYID = P.PROPERTYID
     	INNER JOIN P_SCHEME SCH ON P.SchemeId = SCH.SchemeId	
		INNER JOIN (SELECT DEVELOPMENTID,DEVELOPMENTNAME,LOCALAUTHORITY,PATCHID FROM PDR_DEVELOPMENT) D ON SCH.DEVELOPMENTID = D.DEVELOPMENTID

		LEFT JOIN P_STATUSCHANGEHISTORY S ON S.PROPERTYID = P.PROPERTYID 
					AND S.CHANGEID = (SELECT MAX(CHANGEID) FROM P_STATUSCHANGEHISTORY WHERE PROPERTYID = P.PROPERTYID AND convert(smalldatetime,convert(char(12),CTIMESTAMP),103) <= ''' + convert(varchar(20),@REPORTDATE) + ''')
		LEFT JOIN P_STATUS PS ON PS.STATUSID=S.NEWSTATUS
		LEFT JOIN P_SUBSTATUS PSU ON PSU.SUBSTATUSID=S.NEWSUBSTATUS

		LEFT JOIN (
						SELECT F.PROPERTYID,F.RENT,F.SERVICES,F.INELIGSERV,F.SUPPORTEDSERVICES,F.WATERRATES,F.COUNCILTAX,F.GARAGE,F.TOTALRENT,F.DATERENTSET,F.RENTEFFECTIVE,F.PFTIMESTAMP
						FROM P_FINANCIAL F
						WHERE F.DATERENTSET <= ''' + convert(varchar(20),@REPORTDATE) + '''

						UNION
						SELECT FF.PROPERTYID,FF.RENT,FF.SERVICES,FF.INELIGSERV,FF.SUPPORTEDSERVICES,FF.WATERRATES,FF.COUNCILTAX,FF.GARAGE,FF.TOTALRENT,FF.DATERENTSET,FF.RENTEFFECTIVE,FF.PFTIMESTAMP
						FROM P_FINANCIAL_HISTORY FF 
						WHERE FF.SID =(SELECT MAX(SID) FROM P_FINANCIAL_HISTORY  WHERE PROPERTYID = FF.PROPERTYID AND DATERENTSET <= ''' + convert(varchar(20),@REPORTDATE) +  ''') 
						AND FF.PROPERTYID NOT IN (SELECT PROPERTYID FROM P_FINANCIAL  WHERE DATERENTSET <= ''' + convert(varchar(20),@REPORTDATE) +  ''' AND PROPERTYID=FF.PROPERTYID )

			) FH ON FH.PROPERTYID = P.PROPERTYID  '


		--=======================Select Clause=============================================
		SET @SelectClauseUnion = 'Select 
		NULL,NULL,''TOTAL'',NULL,NULL,NULL,NULL
				,SUM(ISNULL(FH.RENT,0)), SUM(ISNULL(FH.SERVICES,0)), SUM(ISNULL(FH.INELIGSERV,0)), SUM(ISNULL(FH.SUPPORTEDSERVICES,0)), SUM(ISNULL(FH.WATERRATES,0)), SUM(ISNULL(FH.COUNCILTAX,0)), SUM(ISNULL(FH.GARAGE,0)), SUM(ISNULL(FH.TOTALRENT,0))'



		--============================Order Clause==========================================
		SET @orderClause =  CHAR(10) + ' Order By P.' + @sortColumn + CHAR(10) + @sortOrder

		--=================================	Where Clause ================================

		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 

		--===============================Main Query ====================================
		Set @mainSelectQuery = @selectClause +@fromClause +CHAR(10)+ @whereClause  +CHAR(10)+ @orderClause +CHAR(10)+  ' UNION ALL ' + CHAR(10)+CHAR(10) + @SelectClauseUnion + CHAR(10)+CHAR(10) + @fromClause + CHAR(10)+ @whereClause 

		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+ CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'

		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				

		--============================ Exec Final Query =================================
		print(@finalQuery)
		EXEC (@finalQuery)

		--========================================================================================
		-- Begin building Count Query 

		Declare @selectCount nvarchar(MAX), 
		@parameterDef NVARCHAR(500)

		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= '
		SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;

		-- End building the Count Query
		--========================================================================================	

END
GO