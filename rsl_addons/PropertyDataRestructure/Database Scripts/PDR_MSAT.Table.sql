USE [RSLBHALive]
GO
/****** Object:  Table [dbo].[PDR_MSAT]    Script Date: 12/30/2014 21:53:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PDR_MSAT](
	[MSATId] [int] IDENTITY(1,1) NOT NULL,
	[PropertyId] [nvarchar](50) NULL,
	[ItemId] [int] NULL,
	[MSATTypeId] [int] NULL,
	[CycleTypeId] [int] NULL,
	[IsRequired] [bit] NULL,
	[LastDate] [datetime] NULL,
	[Cycle] [int] NULL,
	[NextDate] [datetime] NULL,
	[AnnualApportionment] [float] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_PDR_MSAT] PRIMARY KEY CLUSTERED 
(
	[MSATId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[PDR_MSAT] ON
INSERT [dbo].[PDR_MSAT] ([MSATId], [PropertyId], [ItemId], [MSATTypeId], [CycleTypeId], [IsRequired], [LastDate], [Cycle], [NextDate], [AnnualApportionment], [IsActive]) VALUES (1, N'A011000003', 28, 3, NULL, 1, NULL, NULL, NULL, 500, 1)
INSERT [dbo].[PDR_MSAT] ([MSATId], [PropertyId], [ItemId], [MSATTypeId], [CycleTypeId], [IsRequired], [LastDate], [Cycle], [NextDate], [AnnualApportionment], [IsActive]) VALUES (2, N'A011000003', 28, 1, 1, 1, CAST(0x0000A3F400000000 AS DateTime), 32, CAST(0x0000A3F500000000 AS DateTime), NULL, 1)
SET IDENTITY_INSERT [dbo].[PDR_MSAT] OFF
