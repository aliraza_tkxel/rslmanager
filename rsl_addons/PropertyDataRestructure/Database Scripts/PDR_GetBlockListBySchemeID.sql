/* =================================================================================    
    Page Description:  Get Blocks By SchemeID for dropdown
 
    Author: Ali Raza
    Creation Date:  Jan-21-2015 

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Jan-21-2015      Ali Raza           Get Blocks By SchemeID for dropdown
    
    Execution Command:
    
    Exec PDR_GetBlockListBySchemeID  1
  =================================================================================*/
CREATE PROCEDURE PDR_GetBlockListBySchemeID 
	@SchemeId INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select BLOCKID,BLOCKNAME  FROM P_BLOCK  
	Where SCHEMEID = @SchemeId
END
GO
