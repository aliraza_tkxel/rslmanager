USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[FL_CountRejectedFaults]    Script Date: 11/09/2015 14:57:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Raja Aneeq>
-- Create date: <4/11/2015>
-- Description:	Count no. of rejected faults for white board
-- Exec FL_CountRejectedFaults 760
-- =============================================
ALTER PROCEDURE [dbo].[FL_CountRejectedFaults]
	-- Add the parameters for the stored procedure here
	@userId int
AS
BEGIN
DECLARE @StatusId INT
	

	SET NOCOUNT ON;
SELECT @StatusId =  FaultStatusID FROM FL_Fault_status WHERE description ='Rejected by Contractor'
SELECT  COUNT(FL.StatusID) RejectedWork
FROM FL_FAULT_LOG FL
INNER JOIN  FL_FAULT_JOURNAL FJ ON FJ.faultlogid = FL.faultlogId
INNER JOIN 	FL_CONTRACTOR_WORK CW ON CW.journalid = FJ.journalId
INNER JOIN F_PurchaseOrder PO ON PO.orderid = CW.purchaseOrderId
WHERE FL.userID = @userId 
AND FL.StatusID = @StatusId
    
END
