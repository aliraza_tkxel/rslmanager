-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    Page Description: general 
 
    Author: Salman Nazir
    Creation Date: Dec-18-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-18-2014      Salman         Get Scheme for Scheme Drop downs 
  =================================================================================*/
  -- Exec PDR_GetDevelopmentScheme -1
-- =============================================
ALTER PROCEDURE PDR_GetDevelopmentScheme
@developmentId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    if @developmentId = -1
		BEGIN
			SELECT SCHEMEID,SCHEMENAME 
			FROM P_SCHEME 
			ORDER BY CAST(SUBSTRING(SCHEMENAME, 1,CASE	WHEN PATINDEX('%[^0-9]%',SCHEMENAME) > 0 THEN 
												PATINDEX('%[^0-9]%',SCHEMENAME) - 1 
											ELSE LEN(SCHEMENAME) 
											END
											) AS INT) ASC, SCHEMENAME ASC
		END
	ELSE
		BEGIN
			SELECT SCHEMEID,SCHEMENAME 
			FROM P_SCHEME 
			Where DEVELOPMENTID = @developmentId 
			ORDER BY CAST(SUBSTRING(SCHEMENAME, 1,CASE	WHEN PATINDEX('%[^0-9]%',SCHEMENAME) > 0 THEN 
												PATINDEX('%[^0-9]%',SCHEMENAME) - 1 
											ELSE LEN(SCHEMENAME) 
											END
											) AS INT) ASC, SCHEMENAME ASC
		END
		
		
		
		
		
END
GO
