
/* ===========================================================================
--  Author:			Ahmed Mehmood
--  DATE CREATED:	22 January 2015
--  Description:	Insertion scripts for Property Data Restructure related Menus, pages and subpages
--					
 '==============================================================================*/

	BEGIN TRANSACTION
	BEGIN TRY     

DECLARE @PROPERTY_MODULEID INT,@MAINTENANCE_MENUID INT,@ADMIN_MENUID INT,@REPORTS_MENUID INT,@SERVICING_MENUID INT
DECLARE @MAINTENANCE_SCHEDULING_PAGEID INT,@ADMIN_DEVELOPMENT_PAGEID INT
	,@ADMIN_BLOCKS_PAGEID INT, @ADMIN_PROPERTIES_PAGEID INT,@ADMIN_SCHEMES_PAGEID INT,@SERVICING_SCHEDULING_PAGEID INT
	,@SCHEDULING_CYCLIC_PAGEID INT,@MESERVICING_SCHEDULING_PAGEID INT
--======================================================================================================
--												MODULES
--======================================================================================================

SELECT	@PROPERTY_MODULEID = MODULEID
FROM	AC_MODULES
WHERE	DESCRIPTION = 'Property'
	
--======================================================================================================
--												MENUS
--======================================================================================================
	
--	INSERTION MAINTENANCE MENUID

INSERT INTO [AC_MENUS]([DESCRIPTION],[MODULEID],[ACTIVE],[PAGE],[MENUWIDTH],[SUBMENUWIDTH],[ORDERTEXT])
VALUES		('Maintenance',@PROPERTY_MODULEID,1,'~/../PropertyDataRestructure/Bridge.aspx?mn=Maintenance',100,170,'9')
SET			@MAINTENANCE_MENUID = SCOPE_IDENTITY()

--	INSERTION ADMIN MENUID

INSERT INTO [AC_MENUS]([DESCRIPTION],[MODULEID],[ACTIVE],[PAGE],[MENUWIDTH],[SUBMENUWIDTH],[ORDERTEXT])
VALUES		('Admin',@PROPERTY_MODULEID,1,'~/../PropertyDataRestructure/Bridge.aspx?mn=Admin',100,170,'91')
SET			@ADMIN_MENUID = SCOPE_IDENTITY()

--	INSERTION REPORTS MENUID

INSERT INTO [AC_MENUS]([DESCRIPTION],[MODULEID],[ACTIVE],[PAGE],[MENUWIDTH],[SUBMENUWIDTH],[ORDERTEXT])
VALUES		('Reports',@PROPERTY_MODULEID,1,'~/../PropertyDataRestructure/Bridge.aspx?mn=Reports',100,170,'92')
SET			@REPORTS_MENUID = SCOPE_IDENTITY()

-- FETCH SERVICING MENU ID
SELECT	@SERVICING_MENUID = MENUID
FROM	AC_MENUS
WHERE	DESCRIPTION = 'Servicing'

--======================================================================================================
--												PAGES	(LEVEL 1)
--======================================================================================================
	
	
--============
-- Maintenance
--============
INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Dashboard',@MAINTENANCE_MENUID,1,1,'','1',1,NULL,'',1)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Resources',@MAINTENANCE_MENUID,1,1,'','2',1,NULL,'',1)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Scheduling',@MAINTENANCE_MENUID,1,1,'','3',1,NULL,'',1)
SET @MAINTENANCE_SCHEDULING_PAGEID = SCOPE_IDENTITY()

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Calendar',@MAINTENANCE_MENUID,1,1,'','4',1,NULL,'',1)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Reports',@MAINTENANCE_MENUID,1,1,'','5',1,NULL,'',1)
 
--============
-- Admin
--============
INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Developments',@ADMIN_MENUID,1,1,'~/../PropertyDataRestructure/Bridge.aspx?pg=developmentlist','1',1,NULL,'~/../PropertyDataRestructure/Views/Pdr/Development/DevelopmentList.aspx',1)
SET @ADMIN_DEVELOPMENT_PAGEID = SCOPE_IDENTITY()

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Blocks',@ADMIN_MENUID,1,1,'~/../PropertyDataRestructure/Bridge.aspx?pg=blocklist','2',1,NULL,'~/../PropertyDataRestructure/Views/Pdr/Block/BlockList.aspx',1)
SET @ADMIN_BLOCKS_PAGEID = SCOPE_IDENTITY()

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Properties',@ADMIN_MENUID,1,1,'~/../RSLApplianceServicing/Bridge.aspx?pg=properties','3',1,NULL,'~/../RSLApplianceServicing/Views/Property/Properties.aspx',1)
SET @ADMIN_PROPERTIES_PAGEID = SCOPE_IDENTITY()

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Schemes',@ADMIN_MENUID,1,1,'~/../PropertyDataRestructure/Bridge.aspx?pg=schemelist','4',1,NULL,'~/../PropertyDataRestructure/Views/Pdr/Scheme/SchemeList.aspx',1)
SET @ADMIN_SCHEMES_PAGEID =  SCOPE_IDENTITY()

--============
-- Reports
--============
INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Property Terrier',@REPORTS_MENUID,1,1,'~/../PropertyDataRestructure/Bridge.aspx?pg=propertyterrier','1',1,NULL,'~/../PropertyDataRestructure/Views/Reports/ReportArea.aspx?rpt=terrier',1)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Rent Analysis',@REPORTS_MENUID,1,1,'~/../PropertyDataRestructure/Bridge.aspx?pg=rentanalysis','2',1,NULL,'~/../PropertyDataRestructure/Views/Reports/ReportArea.aspx?rpt=rent',1)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Stock Analysis',@REPORTS_MENUID,1,1,'~/../PropertyDataRestructure/Bridge.aspx?pg=stockanalysis','3',1,NULL,'~/../PropertyDataRestructure/Views/Reports/ReportArea.aspx?rpt=stock',1)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Void Analysis',@REPORTS_MENUID,1,1,'~/../PropertyDataRestructure/Bridge.aspx?pg=voidanalysis','4',1,NULL,'~/../PropertyDataRestructure/Views/Reports/ReportArea.aspx?rpt=void',1)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Provisioning',@REPORTS_MENUID,1,1,'','5',1,NULL,'',1)

--=====================================================================================================================
-- UPDATE SERVCIING LEVEL 1 PAGES BECAUSE THEIR NESTED PAGES ARE DISTRIBUTED AMONG PDR AND APPLIANCE SERVICING PROJECTS
--=====================================================================================================================

--===========
-- Dashboard
--===========
UPDATE	AC_PAGES
SET		AC_PAGES.PAGE = '~/../RSLApplianceServicing/Bridge.aspx?pg=dashboard'
		,AC_PAGES.BridgeActualPage = '~/../RSLApplianceServicing/Views/Dashboard/Dashboard.aspx'
FROM	AC_PAGES
		INNER JOIN AC_MENUS ON AC_PAGES.MENUID = AC_MENUS.MENUID
WHERE	AC_PAGES.DESCRIPTION = 'Dashboard'
		AND AC_MENUS.DESCRIPTION = 'Servicing'
		
--===========		
-- Resources
--===========

--Status and Actions
UPDATE	AC_PAGES
SET		AC_PAGES.PAGE = '~/../RSLApplianceServicing/Bridge.aspx?pg=statusactionmain'
		,AC_PAGES.BridgeActualPage = '~/../RSLApplianceServicing/Views/Resources/StatusActionMain.aspx'
FROM	AC_PAGES
		INNER JOIN AC_MENUS ON AC_PAGES.MENUID = AC_MENUS.MENUID
WHERE	AC_PAGES.DESCRIPTION = 'Status and Actions'
		AND AC_MENUS.DESCRIPTION = 'Servicing'
		
--Letters
UPDATE	AC_PAGES
SET		AC_PAGES.PAGE = '~/../RSLApplianceServicing/Bridge.aspx?pg=viewletters'
		,AC_PAGES.BridgeActualPage = '~/../RSLApplianceServicing/Views/Resources/ViewLetters.aspx'
FROM	AC_PAGES
		INNER JOIN AC_MENUS ON AC_PAGES.MENUID = AC_MENUS.MENUID
WHERE	AC_PAGES.DESCRIPTION = 'Letters'
		AND AC_MENUS.DESCRIPTION = 'Servicing' and LINK = 1
		
--==============
-- Admin Calendar
--==============
UPDATE	AC_PAGES
SET		AC_PAGES.PAGE = '~/../RSLApplianceServicing/Bridge.aspx?pg=adminscheduling'
		,AC_PAGES.BridgeActualPage = '~/../RSLApplianceServicing/Views/Scheduling/AdminScheduling.aspx'
FROM	AC_PAGES
		INNER JOIN AC_MENUS ON AC_PAGES.MENUID = AC_MENUS.MENUID
WHERE	AC_PAGES.DESCRIPTION = 'Admin Calendar'
		AND AC_MENUS.DESCRIPTION = 'Servicing'
		
--==============
-- SCHEDULING
--==============

-- GET PAGEID
SELECT	@SERVICING_SCHEDULING_PAGEID = PAGEID
FROM	AC_PAGES
WHERE	PAGE = '~/../RSLApplianceServicing/Views/Scheduling/FuelScheduling.aspx'

-- UPDATE SCHEDULING 
UPDATE	AC_PAGES
SET		PAGE = ''
WHERE	PAGE = '~/../RSLApplianceServicing/Views/Scheduling/FuelScheduling.aspx'

--==============
-- CALENDAR
--==============

UPDATE	AC_PAGES
SET		AC_PAGES.PAGE = '~/../RSLApplianceServicing/Bridge.aspx?pg=weeklycalendar'
		,AC_PAGES.BridgeActualPage = '~/../RSLApplianceServicing/Views/Calendar/WeeklyCalendar.aspx'
FROM	AC_PAGES
		INNER JOIN AC_MENUS ON AC_PAGES.MENUID = AC_MENUS.MENUID
WHERE	AC_PAGES.DESCRIPTION = 'Calendar'
		AND AC_MENUS.DESCRIPTION = 'Servicing' AND LINK =1
		
--==============
-- REPORTS
--==============

-- Certificate Expiry
UPDATE	AC_PAGES
SET		AC_PAGES.PAGE = '~/../RSLApplianceServicing/Bridge.aspx?pg=certificateexpiry'
		,AC_PAGES.BridgeActualPage = '~/../RSLApplianceServicing/Views/Reports/CertificateExpiry/CertificateExpiry.aspx'
FROM	AC_PAGES
		INNER JOIN AC_MENUS ON AC_PAGES.MENUID = AC_MENUS.MENUID
WHERE	AC_PAGES.DESCRIPTION = 'Certificate Expiry'
		AND AC_MENUS.DESCRIPTION = 'Servicing' 

-- Issued Certificates
UPDATE	AC_PAGES
SET		AC_PAGES.PAGE = '~/../RSLApplianceServicing/Bridge.aspx?pg=issuedcertificates'
		,AC_PAGES.BridgeActualPage = '~/../RSLApplianceServicing/Views/Reports/IssuedCertificates/IssuedCertificates.aspx'		
FROM	AC_PAGES
		INNER JOIN AC_MENUS ON AC_PAGES.MENUID = AC_MENUS.MENUID
WHERE	AC_PAGES.DESCRIPTION = 'Issued Certificates'
		AND AC_MENUS.DESCRIPTION = 'Servicing' 
		
-- Print Certificates
UPDATE	AC_PAGES
SET		AC_PAGES.PAGE = '~/../RSLApplianceServicing/Bridge.aspx?pg=printcertificates'
		,AC_PAGES.BridgeActualPage = '~/../RSLApplianceServicing/Views/Reports/PrintCertificates/PrintCertificates.aspx'
FROM	AC_PAGES
		INNER JOIN AC_MENUS ON AC_PAGES.MENUID = AC_MENUS.MENUID
WHERE	AC_PAGES.DESCRIPTION = 'Print Certificates'
		AND AC_MENUS.DESCRIPTION = 'Servicing' 		


--======================================================================================================
--												PAGES	(LEVEL 2)
--======================================================================================================
 	
--=========================
-- Maintenance > Scheduling
--=========================

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Cyclic Maintenance',@MAINTENANCE_MENUID,1,1,'~/../PropertyDataRestructure/Views/Scheduling/MEServicing.aspx?msat=cm','1',2,@MAINTENANCE_SCHEDULING_PAGEID,'',1)
SET		@SCHEDULING_CYCLIC_PAGEID  =  SCOPE_IDENTITY()

--=========================
-- Admin > Developments
--=========================
INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Add New Development',@ADMIN_MENUID,1,0,'~/../PropertyDataRestructure/Views/Pdr/Development/AddNewDevelopment.aspx','1',2,@ADMIN_DEVELOPMENT_PAGEID,'',0)

--=========================
-- Admin > Block
--=========================
INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Block Main',@ADMIN_MENUID,1,0,'~/../PropertyDataRestructure/Views/Pdr/Block/BlockMain.aspx','1',2,@ADMIN_BLOCKS_PAGEID,'',0)

--=========================
-- Admin > Properties
--=========================
INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Property List Detail',@ADMIN_MENUID,1,0,'~/../RSLApplianceServicing/Views/Property/Properties.aspx?src=list','1',2,@ADMIN_PROPERTIES_PAGEID,'',0)

--=========================
-- Admin > Schemes
--=========================
INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Scheme List Detail',@ADMIN_MENUID,1,0,'~/../PropertyDataRestructure/Views/Pdr/Scheme/SchemeList.aspx','1',2,@ADMIN_SCHEMES_PAGEID,'',0)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Scheme Dashboard',@ADMIN_MENUID,1,0,'~/../PropertyDataRestructure/Bridge.aspx?pg=schemedashboard','1',2,@ADMIN_SCHEMES_PAGEID,'~/../PropertyDataRestructure/Views/Pdr/Scheme/Dashboard/SchemeDashBoard.aspx',0)

--=================================
-- Appliance Servicing > Scheduling
--=================================

-- Fuel Servicing
INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Fuel Servicing',@SERVICING_MENUID,1,1,'~/../RSLApplianceServicing/Bridge.aspx?pg=fuelservicing','1',2,@SERVICING_SCHEDULING_PAGEID,'~/../RSLApplianceServicing/Views/Scheduling/FuelScheduling.aspx',1)

-- M&E Servicing
INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('M&E Servicing',@SERVICING_MENUID,1,1,'~/../PropertyDataRestructure/Bridge.aspx?pg=meservicing','2',2,@SERVICING_SCHEDULING_PAGEID,'~/../PropertyDataRestructure/Views/Scheduling/MEServicing.aspx?msat=mes',1)
SET @MESERVICING_SCHEDULING_PAGEID = SCOPE_IDENTITY()

-- PAT Testing
INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('PAT Testing',@SERVICING_MENUID,1,1,'','3',2,@SERVICING_SCHEDULING_PAGEID,'',1)


--======================================================================================================
--												PAGES	(LEVEL 3)
--======================================================================================================

--================================================
-- Maintenance > Scheduling > Cyclic Maintenance
--================================================

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Schedule Cyclic Maintenance',@MAINTENANCE_MENUID,1,0,'~/../PropertyDataRestructure/Views/Scheduling/ScheduleMEServicing.aspx?msat=cm','1',3,@SCHEDULING_CYCLIC_PAGEID,'',0)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('ToBeArranged Cyclic Maintenance Appointment Summary',@MAINTENANCE_MENUID,1,0,'~/../PropertyDataRestructure/Views/Scheduling/ToBeArrangedAppointmentSummary.aspx?msat=cm','2',3,@SCHEDULING_CYCLIC_PAGEID,'',0)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Arranged Cyclic Maintenance Appointment Summary',@MAINTENANCE_MENUID,1,0,'~/../PropertyDataRestructure/Views/Scheduling/ArrangedAppointmentSummary.aspx?msat=cm','3',3,@SCHEDULING_CYCLIC_PAGEID,'',0)


--================================================
-- Admin > Scheduling > M&E Servicing
--================================================

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Schedule M&E Servicing',@SERVICING_MENUID,1,0,'~/../PropertyDataRestructure/Views/Scheduling/ScheduleMEServicing.aspx?msat=mes','1',3,@MESERVICING_SCHEDULING_PAGEID,'',0)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('ToBeArranged M&E Servicing Appointment Summary',@SERVICING_MENUID,1,0,'~/../PropertyDataRestructure/Views/Scheduling/ToBeArrangedAppointmentSummary.aspx?msat=mes','2',3,@MESERVICING_SCHEDULING_PAGEID,'',0)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Arranged M&E Servicing Appointment Summary',@SERVICING_MENUID,1,0,'~/../PropertyDataRestructure/Views/Scheduling/ArrangedAppointmentSummary.aspx?msat=mes','3',3,@MESERVICING_SCHEDULING_PAGEID,'',0)

END TRY 
	BEGIN CATCH 	
		IF @@TRANCOUNT >0	  
		BEGIN	   
			ROLLBACK TRANSACTION;	
		END 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
		@ErrorSeverity, -- Severity.
		@ErrorState -- State.
		);
	END CATCH  
	 
	IF @@TRANCOUNT >0
	BEGIN
		COMMIT TRANSACTION;
	END 
	