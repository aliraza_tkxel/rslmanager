SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ahmed Mehmood
-- Create date: 30/01/2015
-- Description:	To get contractors having at lease one planned contract, as drop down values for assgin work to contractor.
-- =============================================
CREATE PROCEDURE PDR_GetContractDetailByContractorId 
	-- Add the parameters for the stored procedure here	 
	@areaofwork varchar(100)
	,@contractorId int
	,@schemeId int
	,@blockId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT S.STARTDATE StartDate, S.RENEWALDATE EndDate
	FROM S_ORGANISATION O
	INNER JOIN S_SCOPE S ON O.ORGID = S.ORGID
	INNER JOIN S_AREAOFWORK AoW ON S.AREAOFWORK = AoW.AREAOFWORKID
	INNER JOIN S_SCOPETOPATCHANDSCHEME SPC ON S.SCOPEID = SPC.SCOPEID
	WHERE O.ORGID = @contractorId and AoW.DESCRIPTION = @areaofwork
	AND (SPC.SCHEMEID = @schemeId OR SPC.BLOCKID = @blockId ) 
	
END
GO
