USE [RSLBHALive]
GO

/****** Object:  UserDefinedTableType [dbo].[PDR_AssingToContractorWorksRequired]    Script Date: 01/28/2015 19:35:47 ******/
CREATE TYPE [dbo].[PDR_AssingToContractorWorksRequired] AS TABLE(
	
	[WorkDetailId] [int] NOT NULL,
	[ServiceRequired] [nvarchar](4000) NOT NULL,
	[NetCost] [smallmoney] NOT NULL,
	[VatType] [int] NOT NULL,
	[VAT] [smallmoney] NOT NULL,
	[GROSS] [smallmoney] NOT NULL,
	[PIStatus] [int] NOT NULL,
	[ExpenditureId] [int] NOT NULL,
	[CostCenterId] [int] NOT NULL,
	[BudgetHeadId] [int] NOT NULL
	
)
GO

	

