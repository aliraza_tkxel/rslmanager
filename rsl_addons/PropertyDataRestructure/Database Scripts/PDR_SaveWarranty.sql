USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_SaveWarranty]    Script Date: 02/16/2015 15:53:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,05/12/2014>
-- Description:	<Description,,Save Warranty for the Block>
-- =============================================
CREATE PROCEDURE [dbo].[PDR_SaveWarranty] 
	   @ID varchar(100) 
      ,@WARRANTYTYPE int
      ,@AREAITEM int
      ,@CONTRACTOR int 
      ,@EXPIRYDATE smalldatetime
      ,@NOTES varchar(500)
      ,@requestType nvarchar (50)
      ,@WarrantyId int
     
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    IF (@WarrantyId > 0)
		BEGIN 
			Update PDR_WARRANTY Set WARRANTYTYPE= @WARRANTYTYPE,CONTRACTOR= @CONTRACTOR,AREAITEM = @AREAITEM,EXPIRYDATE= @EXPIRYDATE,NOTES = @NOTES WHERE WARRANTYID = @WarrantyId
		END
    ELSE
		BEGIN
			If (@requestType = 'Block')
				BEGIN
			    
					INSERT INTO PDR_WARRANTY(BLOCKID,WARRANTYTYPE,AREAITEM,CONTRACTOR,EXPIRYDATE,NOTES)
					VALUES (@ID,@WARRANTYTYPE,@AREAITEM,@CONTRACTOR,@EXPIRYDATE,@NOTES)
				END
			ELSE IF(@requestType = 'Scheme')
				BEGIN 
					INSERT INTO PDR_WARRANTY(SCHEMEID,WARRANTYTYPE,AREAITEM,CONTRACTOR,EXPIRYDATE,NOTES)
					VALUES (@ID,@WARRANTYTYPE,@AREAITEM,@CONTRACTOR,@EXPIRYDATE,@NOTES)
				END
			ELSE 
				BEGIN 
					INSERT INTO PDR_WARRANTY(PROPERTYID,WARRANTYTYPE,AREAITEM,CONTRACTOR,EXPIRYDATE,NOTES)
					VALUES (@ID,@WARRANTYTYPE,@AREAITEM,@CONTRACTOR,@EXPIRYDATE,@NOTES)
				END
		END		
END
