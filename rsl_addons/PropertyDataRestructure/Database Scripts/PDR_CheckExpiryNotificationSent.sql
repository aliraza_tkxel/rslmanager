-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,30-Jan-2015>
-- Description:	<Description,,Check Document Expiry Notifications >
-- Exec PDR_CheckNotificationSent 1
-- =============================================
CREATE PROCEDURE PDR_CheckNotificationSent
@documentId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select * from PDR_DocumentExpiryNotificationLog WHERE DocumentId = @documentId
END
GO
