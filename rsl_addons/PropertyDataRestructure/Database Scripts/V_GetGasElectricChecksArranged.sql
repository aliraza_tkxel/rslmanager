USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[V_GetGasElectricChecksArranged]    Script Date: 11/11/2015 14:44:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
/* =================================================================================    
    Page Description: Get Gas/Electric Checks To Be Arranged List 
    Author: Ali Raza
    Creation Date: May-26-2015

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         May-26-2015      Ali Raza         Get Gas/Electric Checks To Be Arranged List 
    v2.0         Nov-10-2015      Raja Aneeq        Add a patch filter 
 -- =================================================================================*/
--  DECLARE	@totalCount int
--EXEC	 V_GetGasElectricChecksArranged
--		@searchText = NULL,
--		@patch = -1,
--		@checksRequired=1,
--		@pageSize = 100,
--		@pageNumber = 1,
--		@totalCount = @totalCount OUTPUT
--SELECT	@totalCount as N'@totalCount'
-- =============================================
ALTER PROCEDURE [dbo].[V_GetGasElectricChecksArranged]
-- Add the parameters for the stored procedure here
		@patch int = 0,
		@searchText VARCHAR(200),
		@checksRequired int = 1,
	--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'JournalId', 
		@sortOrder varchar (5) = 'DESC',
		@getOnlyCount bit=0,
		@totalCount int = 0 output	
AS
BEGIN
	DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(2000),
        @filterCriteria varchar(200)='',
        --variables for paging
        @offset int,
		@limit int,
		@checksRequiredType varchar(200),
		@MSATTypeId int,
		@ArrangedStatusId int
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		
		SELECT  @ArrangedStatusId = PDR_STATUS.STATUSID FROM PDR_STATUS WHERE TITLE = 'Arranged'
		--=====================Search Criteria===============================
		SET @searchCriteria = ' 1=1 '
		
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND (ISNULL(P__PROPERTY.HouseNumber, '''')	+ ISNULL('' ''+P__PROPERTY.ADDRESS1, '''') 	+ ISNULL('', ''+P__PROPERTY.ADDRESS2, '''') LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR CONVERT(VARCHAR,Case When C__CUSTOMER.TITLE=6 then '''' Else  ISNULL(G_TITLE.[DESCRIPTION],'''')END)+ CONVERT(VARCHAR, ISNULL(C__CUSTOMER.FIRSTNAME,''''))+ CONVERT(VARCHAR, ISNULL(C__CUSTOMER.LASTNAME,'''')) LIKE ''%' + @searchText + '%'')'
		END	
		
		IF (@checksRequired = 1)
		BEGIN
			
			SET @checksRequiredType='Gas Check'
			SELECT 	@MSATTypeId = MSATTypeId  FROM PDR_MSATType WHERE MSATTypeName = 'Void Gas Check'
		
		END
		ELSE IF (@checksRequired = 2)
		BEGIN
			
			SET @checksRequiredType='Electric Check'
			SELECT 	@MSATTypeId = MSATTypeId  FROM PDR_MSATType WHERE MSATTypeName = 'Void Electric Check'
		END
		
		SET @searchCriteria = @searchCriteria + CHAR(10) +' AND PDR_MSAT.MSATTypeId= '+convert(varchar(10),@MSATTypeId)+' AND PDR_JOURNAL.STATUSID= '+convert(varchar(10),@ArrangedStatusId)+''
		--=================================	Filter Criteria================================
		IF(@patch > 0 )
		BEGIN
		SET @filterCriteria = @filterCriteria + 'AND P__PROPERTY.PATCH = ' + CONVERT (VARCHAR,@patch) 
		END
		--=======================Select Clause=============================================
		SET @SelectClause = 'Select top ('+convert(varchar(10),@limit)+')
							 ''JSV''+CONVERT(VARCHAR, RIGHT(''000000''+ CONVERT(VARCHAR,ISNULL(PDR_JOURNAL.JOURNALID,-1)),4)) AS Ref,PDR_JOURNAL.JOURNALID as JournalId 
		,ISNULL(P__PROPERTY.HouseNumber, '''')	+ ISNULL('' ''+P__PROPERTY.ADDRESS1, '''') 	+ ISNULL('', ''+P__PROPERTY.ADDRESS2, '''') AS Address
		,	ISNULL(P__PROPERTY.POSTCODE, '''') AS Postcode
		, CONVERT(VARCHAR,Case When C__CUSTOMER.TITLE=6 then '''' Else  ISNULL(G_TITLE.[DESCRIPTION],'''')END)+ CONVERT(VARCHAR, ISNULL(+'' ''+C__CUSTOMER.FIRSTNAME,''''))+ CONVERT(VARCHAR, ISNULL(+'' ''+C__CUSTOMER.LASTNAME,'''')) AS Tenant 
		,CONVERT(nvarchar(50),PDR_MSAT.TerminationDate, 103) as Termination	
		,Case When PDR_MSAT.ReletDate is NULL then CONVERT(nvarchar(50),DATEADD(day,7,PDR_MSAT.TerminationDate), 103) Else	CONVERT(nvarchar(50),PDR_MSAT.ReletDate, 103) END as Relet
		,PDR_MSAT.TenancyId	,'''+@checksRequiredType+''' AS Type
		,LEFT(E__EMPLOYEE.Firstname, 1)+''''+LEFT(E__EMPLOYEE.LASTNAME, 1) as Surveyor
		,CONVERT(nvarchar(50),PDR_APPOINTMENTS.APPOINTMENTSTARTDATE, 103)+''</br> ''+CONVERT(VARCHAR(5), PDR_APPOINTMENTS.APPOINTMENTSTARTTIME, 108) 
		as Appointment	,PDR_APPOINTMENTS.APPOINTMENTNOTES as AppointmentNotes 	
		
		
		'
			
		
		
		--============================From Clause============================================
		SET @fromClause = CHAR(10) +'FROM	PDR_JOURNAL
	
		INNER JOIN	PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId
		INNER JOIN	PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId		
		LEFT JOIN	P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID	
		INNER JOIN	PDR_STATUS ON PDR_JOURNAL.STATUSID = PDR_STATUS.STATUSID
		INNER JOIN C_TENANCY ON PDR_MSAT.TenancyId=	C_TENANCY.TENANCYID
		INNER JOIN C__CUSTOMER ON PDR_MSAT.CustomerId = C__CUSTOMER.CUSTOMERID
		INNER JOIN C_CUSTOMERTENANCY on C__CUSTOMER.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID and C_TENANCY.TENANCYID=C_CUSTOMERTENANCY.TENANCYID
		LEFT JOIN G_TITLE ON C__CUSTOMER.TITLE=G_TITLE.TITLEID
		INNER JOIN PDR_APPOINTMENTS ON PDR_JOURNAL.JOURNALID=PDR_APPOINTMENTS.JOURNALID
		INNER JOIN E__EMPLOYEE ON PDR_APPOINTMENTS.ASSIGNEDTO = E__EMPLOYEE.EMPLOYEEID
		
		'
							
		--============================Order Clause==========================================
		IF(@sortColumn = 'Ref')
		BEGIN
			SET @sortColumn = CHAR(10)+ ' PDR_JOURNAL.JOURNALID' 	
			
		END
		
		IF(@sortColumn = 'Address')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Address' 	
			
		END
		IF(@sortColumn = 'Tenant')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Tenant' 	
			
		END
		
		IF(@sortColumn = 'Termination')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Termination' 	
			
		END
		
		IF(@sortColumn = 'Relet')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Relet' 	
			
		END
				
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria +@filterCriteria
		
		--===============================Main Query ====================================
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
		IF(@getOnlyCount=0)
		BEGIN
			print(@finalQuery)
			EXEC (@finalQuery)
		END
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
END
