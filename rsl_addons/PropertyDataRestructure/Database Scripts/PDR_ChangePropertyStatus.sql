USE [RSLBHALive ]
GO
/****** Object:  StoredProcedure [dbo].[PDR_ChangePropertyStatus]    Script Date: 02/08/2016 10:47:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
IF object_id('PDR_ChangePropertyStatus') IS NULL
    EXEC ('create procedure PDR_ChangePropertyStatus as select 1')
GO
-- =============================================
-- Author:		Raja Aneeq
-- Create date: 4/2/2016
-- Description:	change Property Status  to 'Available To Rent' the day after the 'Handover date'
-- Exec PDR_ChangePropertyStatus
-- =============================================


ALTER PROCEDURE  [dbo].[PDR_ChangePropertyStatus]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	
	SET NOCOUNT ON;
	DECLARE @DevelopmentId int
	DECLARE @currentDate smalldatetime
	DECLARE @newStatus int
	DECLARE @oldStatus int
	DECLARE @newSubStatus int
	DECLARE @oldSubStatus int
	SET  @currentDate = GETDATE()
	SELECT @oldStatus = statusId from P_Status where  Description='Unavailable'
	SELECT @newStatus = statusId from P_Status where  Description='Available to rent'
	SELECT @newSubStatus = SubStatusId from P_SubStatus where  Description='N/A'
	 
	DECLARE cur cursor 
	for SELECT  DevelopmentId FROM P_Phase WHERE HandoverintoManagement <= @currentDate
	open cur
	
	
	
	fetch next from cur into @DevelopmentId
	
   UPDATE P__Property set Status =@newStatus, SubStatus = @newSubStatus  
   WHERE DevelopmentId = @DevelopmentId AND Status= @oldStatus
  
   while (@@fetch_status = 0)
   BEGIN
   fetch next from cur into @DevelopmentId
    UPDATE P__Property set Status =@newStatus, SubStatus = @newSubStatus  
   WHERE DevelopmentId = @DevelopmentId AND Status= @oldStatus
   END
   CLOSE cur
   
   DEALLOCATE cur
	
END
