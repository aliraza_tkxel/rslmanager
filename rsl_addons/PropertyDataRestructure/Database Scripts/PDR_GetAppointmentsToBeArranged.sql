USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/* =============================================
-- EXEC PDR_GetAppointmentsToBeArranged
			@check56Days = 0,		
			@searchedText = NULL,
			@msatType = 'M&E Servicing',
			@pageSize = 30,
			@pageNumber = 1,
			@schemeId = -1,
			@blockId = -1,
			@attributeTypeId int = -1,
			@sortColumn = 'LoggedDate',
			@sortOrder = 'ASC',
			@totalCount = 0
		
-- Author:		<Ahmed Mehmood>
-- Create date: <2 Jan 2015>
-- Description:	<This stored procedure returns  attributes that have �Servicing required?� set as �Yes� and have a �Next service
date� in the past or within the next 56 days>
-- Parameters:	
		--@check56Days bit,
		--@searchedText varchar(8000),
		--@msatType NVARCHAR(200),
		--@pageSize int = 30,
		--@pageNumber int = 1,
		--@schemeId int = -1,
		--@blockId int = -1,
		--@pageNumber int = 1,
		--@sortColumn varchar(50) = 'LoggedDate',
		--@sortOrder varchar (5) = 'DESC',
		--@totalCount int=0 output
-- ============================================= */
ALTER PROCEDURE [dbo].[PDR_GetAppointmentsToBeArranged]
( 
		--These parameters will be used for search
		@check56Days bit,				
		@searchedText varchar(5000),		
		@msatType NVARCHAR(200),
		--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@schemeId int = -1,
		@blockId int = -1,
		@attributeTypeId int = -1,
		@sortColumn varchar(50) = 'LoggedDate',
		@sortOrder varchar (5) = 'DESC',
		@totalCount int=0 output
		
)
AS
BEGIN
		DECLARE @SelectClause varchar(5000),
        @fromClause   varchar(1000),
        @whereClause  varchar(1000),	        
        @orderClause  varchar(100),	
        @mainSelectQuery varchar(8000),        
        @rowNumberQuery varchar(8000),
        @finalQuery varchar(8000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(1000),
        
        --variables for paging
        @offset int,
		@limit int
		
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
        		
		--========================================================================================
		-- Begin building SearchCriteria clause
		-- These conditions will be added into where clause based on search criteria provided
		
		SET @searchCriteria = ' 1=1 '
		IF(@searchedText != '' OR @searchedText != NULL)
		BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND ( P_SCHEME.SCHEMENAME LIKE ''%' + @searchedText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR P_PROPERTY_SCHEME.SCHEMENAME LIKE ''%' + @searchedText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR P_BLOCK.BLOCKNAME LIKE ''%' + @searchedText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR P_PROPERTY_BLOCK.BLOCKNAME LIKE ''%' + @searchedText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR P__PROPERTY.POSTCODE LIKE ''%' + @searchedText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR P_BLOCK.POSTCODE LIKE ''%' + @searchedText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR P_PROPERTY_BLOCK.POSTCODE LIKE ''%' + @searchedText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR PA_ITEM.ItemName LIKE ''%' + @searchedText + '%'') '
		END 
				
		IF (@schemeId > 0 AND @blockId > 0)
			BEGIN
					SET @searchCriteria = @searchCriteria + CHAR(10) +' AND (( PDR_MSAT.SchemeId = ' + CONVERT(VARCHAR,@schemeId) +''			
					SET @searchCriteria = @searchCriteria + CHAR(10) +' OR P__PROPERTY.SCHEMEID = ' + CONVERT(VARCHAR,@schemeId) + ')'
					SET @searchCriteria = @searchCriteria + CHAR(10) +' OR ( PDR_MSAT.blockId = ' + CONVERT(VARCHAR,@blockId) +''			
					SET @searchCriteria = @searchCriteria + CHAR(10) +' OR P__PROPERTY.BLOCKID = ' + CONVERT(VARCHAR,@blockId) + '))'
				
			END 
		ELSE
			BEGIN
				
					
				IF(@schemeId > 0)
				BEGIN
					SET @searchCriteria = @searchCriteria + CHAR(10) +' AND ( PDR_MSAT.SchemeId = ' + CONVERT(VARCHAR,@schemeId) +''			
					SET @searchCriteria = @searchCriteria + CHAR(10) +' OR P__PROPERTY.SCHEMEID = ' + CONVERT(VARCHAR,@schemeId) + ')'
				END 
				
				IF(@blockId > 0)
				BEGIN
					SET @searchCriteria = @searchCriteria + CHAR(10) +' AND ( PDR_MSAT.blockId = ' + CONVERT(VARCHAR,@blockId) +''			
					SET @searchCriteria = @searchCriteria + CHAR(10) +' OR P__PROPERTY.BLOCKID = ' + CONVERT(VARCHAR,@blockId) + ')'
				END 
			END	
				
		IF(@check56Days = 1)
		BEGIN			
			SET @searchCriteria = @searchCriteria + CHAR(10) + 'AND PDR_MSAT.NextDate <= DATEADD(day,56,CURRENT_TIMESTAMP) '
		END
		
		IF(@attributeTypeId <> -1 )
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND PDR_MSAT.attributeTypeId = ' + CONVERT(NVARCHAR(10), @attributeTypeId)
		END	
		
		--These conditions w�ll be used in every case
		
		SET @searchCriteria = @searchCriteria + CHAR(10) + 'AND PDR_STATUS.TITLE = ''To be Arranged''
															AND PDR_MSATType.MSATTypeName = ''' +  @msatType
														+	''' AND PDR_MSAT.IsRequired = 1 '
	
		-- End building SearchCriteria clause   
		--========================================================================================
		
		SET NOCOUNT ON;
		--========================================================================================	        
		-- Begin building SELECT clause
		-- Insert statements for procedure here
		
		SET @SelectClause = 'SELECT top ('+convert(varchar(10),@limit)+')
							COALESCE(P_SCHEME.SCHEMENAME, P_PROPERTY_SCHEME.SCHEMENAME,''-'') AS Scheme
							,COALESCE(P_BLOCK.BLOCKNAME  ,P_PROPERTY_BLOCK.BLOCKNAME,''-'') AS Block
							,COALESCE(P__PROPERTY.POSTCODE,P_BLOCK.POSTCODE,''-'') AS Postcode
							,CONVERT(varchar,PDR_MSAT.NextDate,103) AS NextService
							,DATEDIFF(DAY,CURRENT_TIMESTAMP,PDR_MSAT.NextDate) AS Days
							,PDR_STATUS.TITLE AS Status
							,PA_ITEM.ItemName AS Attribute
							,PDR_JOURNAL.CREATIONDATE AS LoggedDate
							,PDR_MSAT.NextDate AS NextServiceSort
							,PDR_JOURNAL.JOURNALID  AS JournalId
							,PDR_MSAT.PropertyId AS PropertyId
							,COALESCE(PDR_MSAT.BlockId,P__PROPERTY.BlockId,-1) AS BlockId
							,COALESCE(PDR_MSAT.SchemeId,P__PROPERTY.SchemeId,-1) AS SchemeId
							,CONVERT(NVARCHAR, PDR_MSAT.Cycle) + '' '' + PDR_CycleType.CycleType AS Lifecycle '
		
		-- End building SELECT clause
		--======================================================================================== 							
		
		
		--========================================================================================    
		-- Begin building FROM clause
		
		SET @fromClause = CHAR(10) + ' FROM	PDR_JOURNAL
		INNER JOIN	PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId
		INNER JOIN	PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId
		INNER JOIN	PDR_CycleType ON PDR_MSAT.CycleTypeId = PDR_CycleType.CycleTypeId
		LEFT JOIN	P_SCHEME ON PDR_MSAT.SchemeId = P_SCHEME.SCHEMEID 
		LEFT JOIN	P_BLOCK ON PDR_MSAT.BlockId = P_BLOCK.BLOCKID
		LEFT JOIN	P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID
		LEFT JOIN P_BLOCK AS P_PROPERTY_BLOCK ON P__PROPERTY.BLOCKID = P_PROPERTY_BLOCK.BLOCKID
		LEFT JOIN P_SCHEME AS P_PROPERTY_SCHEME ON P__PROPERTY.SCHEMEID = P_PROPERTY_SCHEME.SCHEMEID
		INNER JOIN	PA_ITEM ON PDR_MSAT.ItemId = PA_ITEM.ItemID 
		INNER JOIN	PDR_STATUS ON PDR_JOURNAL.STATUSID = PDR_STATUS.STATUSID '
		
		-- End building From clause
		--======================================================================================== 														  
		
		
		
		--========================================================================================    
		-- Begin building OrderBy clause						
		IF(@sortColumn = 'NextService')    
		BEGIN    
			SET @sortColumn = CHAR(10)+ 'NextServiceSort'         
		END 	
		
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		-- End building OrderBy clause
		--========================================================================================
		
		--========================================================================================
		-- Begin building WHERE clause
	    			  				
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria
		
		-- End building WHERE clause
		--========================================================================================
		
		--========================================================================================
		-- Begin building the main select Query
		
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		-- End building the main select Query
		--========================================================================================																																			

		--========================================================================================
		-- Begin building the row number query
		
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		-- End building the row number query
		--========================================================================================	
		
		--========================================================================================
		-- Begin building the final query 
		
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		-- End building the final query
		--========================================================================================									
	
		--========================================================================================
		-- Begin - Execute the Query 
		print(@finalQuery)
		EXEC (@finalQuery)																									
		-- End - Execute the Query 
		--========================================================================================		
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(MAX), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================									
END


