USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Stored Procedure

-- =============================================
-- Author:		<Author,,Ahmed Mehmood>
-- Create date: <Create Date,,29-Oct-2015>
-- Description:	<Description,,NoEntry Counts on Maintenance Dashboard >

--DECLARE	@return_value int,
--		@totalCount int

--EXEC	@return_value = [dbo].[PDR_GetMaintenanceNoEntryCount]
--		@schemeId = -1,
--		@blockId = -1,
--		@maintenanceType = -1,
--		@pageSize = 30,
--		@pageNumber = 1,
--		@sortColumn = N'Address',
--		@sortOrder = N'Desc',
--		@getOnlyCount = 1,
--		@totalCount = @totalCount OUTPUT
-- =============================================
ALTER PROCEDURE [dbo].[PDR_GetMaintenanceNoEntryCount]
		@schemeId int =-1,
		@blockId int =-1,
		@maintenanceType int =-1,
		--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'Address', 
		@sortOrder varchar (5) = 'DESC',
		@getOnlyCount bit=0,
		@totalCount int = 0 output	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

DECLARE	@selectClause varchar(3000),
		@fromClause   varchar(3000),
		@whereClause  varchar(2000),
		@mainQuery varchar(7000),
		@searchCriteria varchar(3000),
		@rowNumberQuery varchar(7000),
		@finalQuery varchar(7000),
		@orderClause  varchar(2000),
		@offset int,
		@limit int,
		@mainSelectQuery varchar(7000)
	
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1

	
		--===================== Search Criteria ===============================
	
		SET @searchCriteria = ' MT.MSATTypeName = ''Cyclic Maintenance'' 
	        AND S.TITLE = ''No Entry'' '
		
		IF(@maintenanceType <> -1 )
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND MS.attributeTypeId = ' + CONVERT(NVARCHAR(10), @maintenanceType)
		END	
		ELSE
		BEGIN
		    SET @searchCriteria = @searchCriteria + CHAR(10) +' AND MS.attributeTypeId IS NOT NULL '
		    
		END
		
		IF(@schemeId <> -1 )
		BEGIN
		    SET @searchCriteria = @searchCriteria + CHAR(10) +' AND MS.schemeid = ' + CONVERT(NVARCHAR(10), @schemeId)
		END
		
		IF(@blockId <> -1 )
		BEGIN
		    SET @searchCriteria = @searchCriteria + CHAR(10) +' AND MS.blockid = ' + CONVERT(NVARCHAR(10), @blockId)
		END
	
		--======================= SELECT Clause =================================
		SET @selectClause = 'SELECT top ('+convert(varchar(10),@limit)+')
			CASE
			WHEN	MS.PROPERTYID IS not null THEN
					ISNULL(P__PROPERTY.ADDRESS1,'''')+'' ''+ISNULL(P__PROPERTY.ADDRESS2,'''')+'' ''+ISNULL(P__PROPERTY.ADDRESS3,'''') 
			ELSE 
					ISNULL(P_BLOCK.ADDRESS1,'''')+'' ''+ISNULL(P_BLOCK.ADDRESS2,'''')+'' ''+ISNULL(P_BLOCK.ADDRESS3,'''') 
			END as [Address]
			,ISNULL(S.TITLE,'' '') as [StatusTitle]
			,MS.NextDate as [NextDate]' + CHAR(10)
		
		
		--============================ FROM Clause ==============================
		SET @fromClause = CHAR(10) +' FROM	PDR_APPOINTMENT_HISTORY A
			INNER JOIN PDR_JOURNAL J on J.JOURNALID = A.JOURNALID
			INNER JOIN PDR_MSAT MS ON MS.MSATId = J.MSATID
			INNER JOIN PDR_MSATType MT ON MS.MSATTypeId = MT.MSATTypeId
			INNER JOIN PDR_STATUS S ON S.STATUSID = J.STATUSID
			LEFT JOIN	P_SCHEME ON MS.SchemeId = P_SCHEME.SCHEMEID 
			LEFT JOIN	P_BLOCK ON MS.BlockId = P_BLOCK.BLOCKID
			LEFT JOIN	P__PROPERTY ON MS.PropertyId = P__PROPERTY.PROPERTYID  '
	
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		--============================ Order Clause ==========================================
				
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		--===============================Main Query ====================================
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
		IF(@getOnlyCount=0)
		BEGIN
			print(@finalQuery)
			EXEC (@finalQuery)
		END
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(DISTINCT J.MSATID) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	

END
