USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetPhase]    Script Date: 12/17/2014 13:26:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    Page Description:   Get Phase By development ID 
 
    Author: Ali Raza
    Creation Date: Dec-17-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-17-2014      Ali Raza           Get Phase By development ID 
    
    Execution Command:
    
    Exec PDR_GetPhaseByDevelopmentId 1
  =================================================================================*/
CREATE PROCEDURE [dbo].[PDR_GetPhaseByDevelopmentId] 
( @developmentId INT)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    
	SELECT PHASEID , PhaseName FROM P_PHASE ph
	Inner Join PDR_DEVELOPMENT dev ON dev.DEVELOPMENTID = ph.DEVELOPMENTID
	where ph.DEVELOPMENTID = @developmentId
END
