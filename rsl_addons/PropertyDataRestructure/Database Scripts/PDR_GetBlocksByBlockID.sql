USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetBlocks]    Script Date: 12/16/2014 21:09:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    Page Description:   Get Blocks By BlockID 
 
    Author: Ali Raza
    Creation Date: Dec-17-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-17-2014      Ali Raza           Get Blocks By BlockID 
    
    Execution Command:
    
    Exec PDR_GetBlocksByBlockID 2
  =================================================================================*/
CREATE PROCEDURE [dbo].[PDR_GetBlocksByBlockID]
( @blockId INT)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select ADDRESS1,ADDRESS2,TOWNCITY,COUNTY,POSTCODE from P_BLOCK
	WHERE BLOCKID =@blockId
END
