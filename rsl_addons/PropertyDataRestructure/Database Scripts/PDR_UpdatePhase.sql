-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    Page Description:  Update Phase after saving Development
 
    Author: Salman Nazir
    Creation Date: Dec-10-2014

    Change History:

    Version      Date                   By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-10-2014      Salman Nazir         Update Phase after saving Development
  =================================================================================*/
CREATE PROCEDURE PDR_UpdatePhase
@phaseId int,
@developmentId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Update P_PHASE Set DEVELOPMENTID = @developmentId Where PHASEID = @phaseId
END
GO
