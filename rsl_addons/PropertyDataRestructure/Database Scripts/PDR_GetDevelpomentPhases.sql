USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetDevelpomentPhases]    Script Date: 12/09/2014 17:13:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================

/* =================================================================================    
    Page Description: general 
 
    Author: Salman Nazir
    Creation Date: Dec-04-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-04-2014      Salman         Get Phases for Phase Drop downs 
  =================================================================================*/
  -- Exec PDR_GetPhases -1
-- =============================================
CREATE PROCEDURE [dbo].[PDR_GetDevelpomentPhases]
@DevelopmentId int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
    if @DevelopmentId = -1
		BEGIN
			Select PHASEID,PhaseName from P_PHASE
		END
	ELSE
		BEGIN
			
			Select B.PHASEID,PhaseName from P_PHASE B
			Where B.DEVELOPMENTID = @DevelopmentId
		END
END
