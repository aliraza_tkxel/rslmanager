USE [RSLBHALive]
GO
/****** Object:  Table [dbo].[PDR_CycleType]    Script Date: 12/30/2014 21:53:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PDR_CycleType](
	[CycleTypeId] [int] IDENTITY(1,1) NOT NULL,
	[CycleType] [nvarchar](200) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_PDR_CycleType] PRIMARY KEY CLUSTERED 
(
	[CycleTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[PDR_CycleType] ON
INSERT [dbo].[PDR_CycleType] ([CycleTypeId], [CycleType], [IsActive]) VALUES (1, N'Day(s)', 1)
INSERT [dbo].[PDR_CycleType] ([CycleTypeId], [CycleType], [IsActive]) VALUES (2, N'Week(s)', 1)
INSERT [dbo].[PDR_CycleType] ([CycleTypeId], [CycleType], [IsActive]) VALUES (3, N'Month(s)', 1)
INSERT [dbo].[PDR_CycleType] ([CycleTypeId], [CycleType], [IsActive]) VALUES (4, N'Year(s)', 1)
SET IDENTITY_INSERT [dbo].[PDR_CycleType] OFF
