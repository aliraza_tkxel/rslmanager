USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetPropertyNROSHAssetTypeMain]    Script Date: 12/10/2014 18:25:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* =================================================================================    
    Page Description:  Get Property NROSH AssetType1 for dropdown
 
    Author: Ali Raza
    Creation Date: Dec-10-2014

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-10-2014      Ali Raza           Get Property NROSH AssetType1 for dropdown
    
    Execution Command:
    
    Exec PDR_GetPropertyNROSHAssetTypeMain
  =================================================================================*/

CREATE PROCEDURE [dbo].[PDR_GetPropertyNROSHAssetTypeMain]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT ATN.Sid,ATN.Description FROM P_ASSETTYPE_NROSH_MAIN ATN 
	
END
