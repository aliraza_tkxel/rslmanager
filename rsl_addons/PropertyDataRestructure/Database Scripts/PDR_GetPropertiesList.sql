-- Stored Procedure

-- =============================================
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,03/12/2014>
-- Description:	<Description,,Get Properties List on Block Details>
-- 
-- =============================================
ALTER PROCEDURE [dbo].[PDR_GetPropertiesList]

		@ID int,
		@requestType varchar(200),
	--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'Ref', 
		@sortOrder varchar (5) = 'DESC',
		@totalCount int = 0 output	
AS
BEGIN
	DECLARE 

		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(2000),

        --variables for paging
        @offset int,
		@limit int

		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1

		--=======================Search Criteria==========================================
		SET  @searchCriteria = ' 1 = 1'
		IF (@requestType = 'Block')
		Begin
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND P.BLOCKID ='+convert(varchar(10),@ID)
		END
		Else
		BEGIN
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND P.SCHEMEID ='+convert(varchar(10),@ID)
		END
		--=======================Select Clause=============================================
		SET @SelectClause = 'Select top ('+convert(varchar(10),@limit)+')
		P.PROPERTYID as Ref,
		(ISNULL(HOUSENUMBER,'' '')+'' ''+ISNULL(ADDRESS1,'' '')+'' ''+ISNULL(ADDRESS2,'' '')) as Address,
		ISNULL(POSTCODE,'' '') as PostCode,
		ISNULL(S.DESCRIPTION,'' '') as Status,
		 ISNULL(PT.DESCRIPTION,'' '') as Type,
		 CASE  
			WHEN PASB.ASBESTOSCOUNT > 0 THEN ''Yes''
			ELSE ''NO'' 
		 END as Asbestos'


		--============================From Clause============================================
		SET @fromClause = CHAR(10) +' from P__PROPERTY P
							 INNER JOIN P_STATUS S on S.STATUSID = P.STATUS
							 INNER JOIN P_PROPERTYTYPE PT on PT.PROPERTYTYPEID = P.PROPERTYTYPE
							 LEFT JOIN (SELECT COUNT(PASB.ASBESTOSID) ASBESTOSCOUNT, PASB.PROPERTYID FROM P_PROPERTY_ASBESTOS_RISKLEVEL PASB 
							 INNER JOIN P_ASBESTOS ASB on ASB.ASBESTOSID = PASB.ASBESTOSID GROUP BY PASB.PROPERTYID) PASB on PASB.PROPERTYID = P.PROPERTYID'

		--============================Order Clause==========================================
		IF(@sortColumn = 'Ref')
		BEGIN
			SET @sortColumn = CHAR(10)+ ' Ref' 	

		END

		IF(@sortColumn = 'Status')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Status' 	

		END
		IF(@sortColumn = 'Type')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Type' 	

		END

		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder

		--=================================	Where Clause ================================

		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 

		--===============================Main Query ====================================
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 

		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'

		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				

		--============================ Exec Final Query =================================
		print(@finalQuery)
		EXEC (@finalQuery)

		--========================================================================================
		-- Begin building Count Query 

		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)

		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause

		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;

		-- End building the Count Query
		--========================================================================================	

END
GO