-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ahmed Mehmood
-- Create date: 27/01/2015
-- Description:	To get contractors having at lease one planned contract, as drop down values for assgin work to contractor.
-- =============================================
CREATE PROCEDURE PDR_GetMeContractorDropDownValues 
	-- Add the parameters for the stored procedure here	 
	@msat varchar(100)
	,@schemeId int
	,@blockId int 
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT O.ORGID AS [id],
			NAME AS [description]
	FROM S_ORGANISATION O
	INNER JOIN S_SCOPE S ON O.ORGID = S.ORGID
	INNER JOIN S_SCOPETOPATCHANDSCHEME SS ON S.SCOPEID = SS.SCOPEID 
	INNER JOIN S_AREAOFWORK AoW ON S.AREAOFWORK = AoW.AREAOFWORKID 
	WHERE AoW.DESCRIPTION = @msat 
			AND (SS.BLOCKID = @blockId OR SS.SCHEMEID = @schemeId)
			AND (S.CREATIONDATE < GETDATE()
				AND S.RENEWALDATE > GETDATE())
	
END
GO