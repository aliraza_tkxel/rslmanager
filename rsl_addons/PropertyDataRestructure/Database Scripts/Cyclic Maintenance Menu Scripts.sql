-- =============================================
-- Author:		<Ahmed Mehmood>
-- Create date: <22/10/2015>
-- Description: Cyclic Maintenance Menu Scripts
-- =============================================

-- Enable Maintenance Module
UPDATE AC_MENUS
SET PAGE = '~/../PropertyDataRestructure/Bridge.aspx?mn=Maintenance'
WHERE DESCRIPTION = 'Maintenance'

-- Add Maintenance Dashboard Page
UPDATE	AC_PAGES
SET		AC_PAGES.PAGE = '~/../PropertyDataRestructure/Views/Maintenance/Dashboard/MaintenanceDashboard.aspx'
FROM	AC_PAGES
		INNER JOIN AC_MENUS ON AC_PAGES.MENUID = AC_MENUS.MENUID
WHERE	AC_MENUS.DESCRIPTION = 'Maintenance'
		AND AC_PAGES.DESCRIPTION = 'Dashboard'