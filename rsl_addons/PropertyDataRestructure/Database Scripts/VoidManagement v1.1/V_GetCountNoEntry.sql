USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[PDR_GetCountNoEntry]    Script Date: 06/17/2015 10:59:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    Page Description: NoEntry Counts on Void Dashboard
    Author: Ali Raza
    Creation Date: June-17-2015

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0        June-17-2015    Ali Raza           NoEntry Counts on Void Dashboard
   Exec Command 
    Exec [dbo].[V_GetCountNoEntry]    
  =================================================================================*/

ALTER PROCEDURE [dbo].[V_GetCountNoEntry]
@getOnlyCount bit=0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Declare @statusId int	
	Select @statusId = STATUSID from PDR_STATUS Where TITLE = 'No Entry'
IF(@getOnlyCount=1)
	BEGIN
	Select COUNT(A.APPOINTMENTID) as total from 
			PDR_APPOINTMENT_HISTORY A
			INNER JOIN PDR_JOURNAL J on J.JOURNALID = A.JOURNALID
			INNER JOIN PDR_MSAT MS on MS.MSATId = J.MSATID
			INNER JOIN PDR_STATUS S on S.STATUSID = J.STATUSID
			
			Where S.STATUSID = @statusId
			AND MS.MSATTypeId IN (Select MSATTypeId from PDR_MSATTYPE where MSATTypeName Like '%Void%')
	END
ELSE
	BEGIN
			select P.PROPERTYID 	,ISNULL(P.HouseNumber, '')	+ ISNULL(' '+P.ADDRESS1, '') 	+ ISNULL(', '+P.ADDRESS2, '') AS Address
			,	P.TOWNCITY,P.COUNTY,	ISNULL(P.POSTCODE, '') AS Postcode 
			from 
				PDR_APPOINTMENT_HISTORY A
				INNER JOIN PDR_JOURNAL J on J.JOURNALID = A.JOURNALID
				INNER JOIN PDR_MSAT MS on MS.MSATId = J.MSATID
				INNER JOIN PDR_STATUS S on S.STATUSID = J.STATUSID
				INNER JOIN P__PROPERTY P ON MS.PropertyId=P.PROPERTYID 
				Where S.STATUSID = @statusId
				AND MS.MSATTypeId IN (Select MSATTypeId from PDR_MSATTYPE where MSATTypeName Like '%Void%')
	END	
	
	
	
	
END
