/* =================================================================================    
    Page Description:  Update Paint pack 
    Author: Ali Raza
    Creation Date: June-11-2015

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0        June-11-2015     Ali Raza		   Update Paint pack 
  =================================================================================*/

CREATE PROCEDURE dbo.V_UpdatePaintPacks
	@paintPackId INT,
	@supplierId INT,
	@statusId INT,
	@deliveryDueDate datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   Update V_PaintPack SET StatusId=@statusId, SupplierId=@supplierId, DeliveryDueDate=@deliveryDueDate,
    ModifiedDate=GETDATE() WHERE PaintPackId=@paintPackId
END
GO
