/*
   Thursday, May 7, 20153:19:25 PM
   User: sa
   Server: DEV-PC4
   Database: RSLBHALive
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.V_RoomList
	(
	RoomId int NOT NULL IDENTITY (1, 1),
	Name varchar(50) NOT NULL,
	Description varchar(100) NULL,
	IsActive bit NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.V_RoomList ADD CONSTRAINT
	PK_V_RoomList PRIMARY KEY CLUSTERED 
	(
	RoomId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.V_RoomList SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.V_RoomList', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.V_RoomList', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.V_RoomList', 'Object', 'CONTROL') as Contr_Per 