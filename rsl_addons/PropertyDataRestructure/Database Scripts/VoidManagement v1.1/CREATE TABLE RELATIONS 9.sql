/*
   Wednesday, May 20, 201511:45:46 AM
   User: sa
   Server: DEV-PC4
   Database: RSLBHALive
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.V_RequiredWorks
	DROP CONSTRAINT FK_V_RequiredWorks_V_AppointmentRecordedData
GO
EXECUTE sp_rename N'dbo.V_AppointmentRecordedData.JournalId', N'Tmp_InspectionJournalId', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.V_AppointmentRecordedData.Tmp_InspectionJournalId', N'InspectionJournalId', 'COLUMN' 
GO
ALTER TABLE dbo.V_AppointmentRecordedData SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.V_AppointmentRecordedData', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.V_AppointmentRecordedData', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.V_AppointmentRecordedData', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
EXECUTE sp_rename N'dbo.V_RequiredWorks.JournalId', N'Tmp_InspectionJournalId_1', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.V_RequiredWorks.Tmp_InspectionJournalId_1', N'InspectionJournalId', 'COLUMN' 
GO
ALTER TABLE dbo.V_RequiredWorks
	DROP COLUMN AppointmentRecordedDataId
GO
ALTER TABLE dbo.V_RequiredWorks SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.V_RequiredWorks', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.V_RequiredWorks', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.V_RequiredWorks', 'Object', 'CONTROL') as Contr_Per 