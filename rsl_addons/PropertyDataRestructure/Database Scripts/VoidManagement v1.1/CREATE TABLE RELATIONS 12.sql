/*
   Wednesday, May 20, 20158:32:05 PM
   User: sa
   Server: DEV-PC4
   Database: RSLBHALive
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.V_PaintPackDetails
	DROP CONSTRAINT FK_V_PaintPackDetails_V_Status
GO
ALTER TABLE dbo.V_BritishGasVoidNotification
	DROP CONSTRAINT FK_V_BritishGasVoidNotification_V_Status
GO
DROP TABLE dbo.V_Status
GO
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.PDR_STATUS SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.PDR_STATUS', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.PDR_STATUS', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.PDR_STATUS', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.V_BritishGasVoidNotification ADD CONSTRAINT
	FK_V_BritishGasVoidNotification_PDR_STATUS FOREIGN KEY
	(
	StatusId
	) REFERENCES dbo.PDR_STATUS
	(
	STATUSID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.V_BritishGasVoidNotification SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.V_BritishGasVoidNotification', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.V_BritishGasVoidNotification', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.V_BritishGasVoidNotification', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.V_PaintPackDetails ADD CONSTRAINT
	FK_V_PaintPackDetails_PDR_STATUS FOREIGN KEY
	(
	StatusId
	) REFERENCES dbo.PDR_STATUS
	(
	STATUSID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.V_PaintPackDetails SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.V_PaintPackDetails', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.V_PaintPackDetails', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.V_PaintPackDetails', 'Object', 'CONTROL') as Contr_Per 