USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[V_ScheduleVoidInspection]    Script Date: 05/28/2015 16:50:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
  Page Description:     Schedule Void Required Works Appointments

  Author: Ali Raza
  Creation Date: May-28-2015

  Change History:

  Version      Date             By                      Description
  =======     ============    ========           ===========================
  v1.0         May-18-2015      Ali Raza           Schedule Void Required Works Appointments
  Execution Command:
  
    
-- ============================================= */   
CREATE PROCEDURE [dbo].[V_ReScheduleWorksRequired]    
 -- Add the parameters for the stored procedure here    
 @userId int 
,@appointmentNotes varchar(1000)  
,@jobSheetNotes varchar(1000)    
,@appointmentStartDate date    
,@appointmentEndDate date    
,@startTime varchar(10)    
,@endTime varchar(10)    
,@operativeId int         
,@journalId INT    
,@duration float 
,@appointmentId int  
,@isSaved int = 0 out    

     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
             
DECLARE 
@ArrangedId int    
,@JournalHistoryId int    
,@AppointmentHistoryId int    

 
BEGIN TRANSACTION;    
BEGIN TRY    

-- =============================================    
-- Get status id of "Arranged"    and MSATTypeId
-- =============================================     
	SELECT  @ArrangedId = PDR_STATUS.statusid 
	FROM	PDR_STATUS 
	WHERE	PDR_STATUS.title ='Arranged'   
	
	
-- ====================================================================================================    
--          Update PDR_JOURNAL    
-- ====================================================================================================    	
Update PDR_JOURNAL SET STATUSID = @ArrangedId ,CREATIONDATE=GETDATE(), CREATEDBY=@userId where JOURNALID = @journalId

-- ====================================================================================================    
--          INSERTION (PDR_APPOINTMENTS)    
-- ====================================================================================================    
    
	SELECT	@JournalHistoryId = MAX(JOURNALHISTORYID)    
	FROM	PDR_JOURNAL_HISTORY    
	WHERE	JOURNALID = @journalId    
      
 PRINT 'JOURNALHISTORYID = '+ CONVERT(VARCHAR,@JournalHistoryId)  
    
-- ====================================================================================================    
--          Update PDR_Appointments    
-- ====================================================================================================  
   UPDATE PDR_APPOINTMENTS SET JOURNALID=@journalId,JOURNALHISTORYID=@JournalHistoryId,APPOINTMENTSTARTDATE=  @appointmentStartDate,APPOINTMENTENDDATE= @appointmentEndDate,
   APPOINTMENTSTARTTIME= @startTime,APPOINTMENTENDTIME= @endTime,ASSIGNEDTO=@operativeId,CREATEDBY= @userId,LOGGEDDATE=GETDATE(),
   APPOINTMENTNOTES= @appointmentNotes,CUSTOMERNOTES=@jobSheetNotes, APPOINTMENTSTATUS='NotStarted',DURATION=@duration
   Where APPOINTMENTID=@appointmentId
    
-- ====================================================================================================    
--          Update V_RequiredWorks    
-- ====================================================================================================      
		 Update V_RequiredWorks SET WorksJournalId=@journalId, IsScheduled=1, StatusId=@ArrangedId WHERE WorksJournalId=@journalId
		
          
END TRY    
BEGIN CATCH     
 IF @@TRANCOUNT > 0    
 BEGIN         
  ROLLBACK TRANSACTION;       
  SET @isSaved = 0            
 END    
 DECLARE @ErrorMessage NVARCHAR(4000);    
 DECLARE @ErrorSeverity INT;    
 DECLARE @ErrorState INT;    
    
 SELECT @ErrorMessage = ERROR_MESSAGE(),    
 @ErrorSeverity = ERROR_SEVERITY(),    
 @ErrorState = ERROR_STATE();    
    
 -- Use RAISERROR inside the CATCH block to return     
 -- error information about the original error that     
 -- caused execution to jump to the CATCH block.    
 RAISERROR (@ErrorMessage, -- Message text.    
    @ErrorSeverity, -- Severity.    
    @ErrorState -- State.    
   );    
END CATCH;    
    
IF @@TRANCOUNT > 0    
 BEGIN      
  COMMIT TRANSACTION;      
  SET @isSaved = 1    
 END    
  
    
END