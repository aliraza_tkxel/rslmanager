
-- =============================================
-- Author:           Ali Raza
-- Create date:      06/07/2015
-- Description:      Get appointmentInfo by Journal Id
-- History:          06/07/2015 Void: Get appointment Info to send email cancel appointment detail to last Operative.
--                   
-- =============================================

CREATE PROCEDURE [dbo].[V_GetAppointmentDetailByJournalId](
	@journalId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	 		
	SELECT 
		'JSV' + CONVERT(VARCHAR, RIGHT('000000' + CONVERT(VARCHAR, ISNULL(
	CASE
		WHEN V.WorksJournalId IS NULL THEN J.JOURNALID
		ELSE V.WorksJournalId
	END, -1)), 4)) AS Ref,
		ISNULL(P.HouseNumber, '') + ISNULL(' ' + P.ADDRESS1, '') + ISNULL(', ' + P.ADDRESS2, '') AS Address,
		ISNULL(P.TOWNCITY,'') as TOWNCITY,
		ISNULL(P.COUNTY,'') as COUNTY,
		ISNULL(P.POSTCODE,'') as POSTCODE,		
		ISNULL(P_SCHEME.SCHEMENAME,'') as Scheme,		
		ISNULL(P_BLOCK.BLOCKNAME,'') as Block,CONVERT(NVARCHAR(50), PDR_MSAT.TerminationDate, 103) AS Termination,
		CONVERT(nvarchar(50),DATEADD(day,7,Case When PDR_MSAT.ReletDate IS NULL Then PDR_MSAT.TerminationDate ELSE PDR_MSAT.ReletDate END), 103)
		 as Relet,		
		E.EMPLOYEEID as EmployeeId, E.FIRSTNAME +' '+ E.LASTNAME as OperativeName , C.WORKEMAIL as Email,CONVERT(NVARCHAR(50), 
		A.APPOINTMENTSTARTDATE, 103)as StartDate,A.APPOINTMENTSTARTTIME as StartTime,A.APPOINTMENTENDTIME AS EndTime
	FROM
		PDR_JOURNAL J
		INNER JOIN PDR_MSAT ON J.MSATID =  PDR_MSAT.MSATId
		INNER JOIN PDR_APPOINTMENTS A ON J.JOURNALID=A.JOURNALID
		INNER JOIN P__PROPERTY P ON PDR_MSAT.PropertyId = P.PROPERTYID
		INNER JOIN E__EMPLOYEE E ON A.ASSIGNEDTO=E.EMPLOYEEID    
		LEFT JOIN E_CONTACT C ON E.EMPLOYEEID = C.EMPLOYEEID 
		LEFT JOIN P_SCHEME ON P.SCHEMEID = P_SCHEME.SCHEMEID 
		LEFT JOIN P_BLOCK ON P.BLOCKID = P_BLOCK.BLOCKID
		LEFT JOIN V_RequiredWorks V ON J.JOURNALID = V.WorksJournalId
	WHERE J.JOURNALID=@journalId	
		
					
END
