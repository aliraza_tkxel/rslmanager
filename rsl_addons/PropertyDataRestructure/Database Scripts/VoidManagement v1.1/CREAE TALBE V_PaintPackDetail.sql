USE [RSLBHALive]
GO

/****** Object:  Table [dbo].[V_PaintPackDetails]    Script Date: 06/23/2015 13:20:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[V_PaintPackDetails](
	[PaintPackDetailId] [int] IDENTITY(1,1) NOT NULL,
	[PaintPackId] [int] NULL,
	[RoomId] [int] NULL,
	[PaintRef] [varchar](50) NULL,
	[PaintColor] [varchar](50) NULL,
	[PaintName] [varchar](50) NULL,
 CONSTRAINT [PK_V_PaintPackDetails] PRIMARY KEY CLUSTERED 
(
	[PaintPackDetailId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[V_PaintPackDetails]  WITH CHECK ADD  CONSTRAINT [FK_V_PaintPackDetails_V_PaintPack] FOREIGN KEY([PaintPackId])
REFERENCES [dbo].[V_PaintPack] ([PaintPackId])
GO

ALTER TABLE [dbo].[V_PaintPackDetails] CHECK CONSTRAINT [FK_V_PaintPackDetails_V_PaintPack]
GO


