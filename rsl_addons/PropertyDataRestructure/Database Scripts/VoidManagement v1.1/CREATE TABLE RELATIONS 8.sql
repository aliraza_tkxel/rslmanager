/*
   Monday, May 18, 20157:49:43 PM
   User: sa
   Server: DEV-PC4
   Database: RSLBHALive
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.P__PROPERTY SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.P__PROPERTY', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.P__PROPERTY', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.P__PROPERTY', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.C__CUSTOMER SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.C__CUSTOMER', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.C__CUSTOMER', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.C__CUSTOMER', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.V_BritishGasVoidNotification ADD CONSTRAINT
	FK_V_BritishGasVoidNotification_P__PROPERTY FOREIGN KEY
	(
	PropertyId
	) REFERENCES dbo.P__PROPERTY
	(
	PROPERTYID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.V_BritishGasVoidNotification ADD CONSTRAINT
	FK_V_BritishGasVoidNotification_C__CUSTOMER FOREIGN KEY
	(
	CustomerId
	) REFERENCES dbo.C__CUSTOMER
	(
	CUSTOMERID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.V_BritishGasVoidNotification SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.V_BritishGasVoidNotification', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.V_BritishGasVoidNotification', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.V_BritishGasVoidNotification', 'Object', 'CONTROL') as Contr_Per 