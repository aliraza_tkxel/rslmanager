-- =============================================
-- Author:           Ali Raza
-- Create date:      29/06/2015
-- Description:       Void Tenancy
-- History:          29/06/2015 Void : Script for Void access rights            
-- =============================================
	BEGIN TRANSACTION
	BEGIN TRY   
	
	DECLARE @PROPERTY_MODULEID INT,@Void_MENUID INT
	DECLARE @Void_SCHEDULING_PAGEID INT,@Void_Reports_PAGEID INT,@Void_Works_PAGEID INT,@Void_PaintPacks_PAGEID INT
--======================================================================================================
--												MODULES
--======================================================================================================

SELECT	@PROPERTY_MODULEID = MODULEID
FROM	AC_MODULES
WHERE	DESCRIPTION = 'Property'
--======================================================================================================
--												MENUS
--======================================================================================================
	SELECT @Void_MENUID=MENUID From AC_MENUS Where DESCRIPTION='Voids'
	Update AC_MENUS SET PAGE='~/../PropertyDataRestructure/Bridge.aspx?mn=Voids' Where DESCRIPTION='Voids'
	
--======================================================================================================
--												PAGES	(LEVEL 1)
--======================================================================================================
	
INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Dashboard',@Void_MENUID,1,1,'~/../PropertyDataRestructure/Views/Void/Dashboard/Dashboard.aspx','1',1,NULL,'',1)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Resources',@Void_MENUID,1,1,'','2',1,NULL,'',1)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Scheduling',@Void_MENUID,1,1,'','3',1,NULL,'',1)
SET @Void_SCHEDULING_PAGEID = SCOPE_IDENTITY()

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Calendar',@Void_MENUID,1,1,'','4',1,NULL,'',1)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Reports',@Void_MENUID,1,1,'','5',1,NULL,'',1)

SET @Void_Reports_PAGEID = SCOPE_IDENTITY()	

--======================================================================================================
--												PAGES	(LEVEL 2)
--======================================================================================================
 	 	
--=========================
-- Voids > Scheduling
--=========================

--Void Inspections
INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Void Inspections',@Void_MENUID,1,1,'~/../PropertyDataRestructure/Views/Void/VoidInspection/VoidInspections.aspx','1',2,@Void_SCHEDULING_PAGEID,'',1)

--Post Void Inspections
INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Post Void Inspections',@Void_MENUID,1,1,'~/../PropertyDataRestructure/Views/Void/VoidInspection/PostVoidInspections.aspx','2',2,@Void_SCHEDULING_PAGEID,'',1)

--Void Works
INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Void Works',@Void_MENUID,1,1,'~/../PropertyDataRestructure/Views/Void/VoidWorks/VoidAppointments.aspx','3',2,@Void_SCHEDULING_PAGEID,'',1)
SET @Void_Works_PAGEID = SCOPE_IDENTITY()	

--Paint Packs
INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Paint Packs',@Void_MENUID,1,1,'~/../PropertyDataRestructure/Views/Void/PaintPacks/PaintPacksList.aspx','4',2,@Void_SCHEDULING_PAGEID,'',1)
SET @Void_PaintPacks_PAGEID = SCOPE_IDENTITY()
	
--Gas/Electric Checks
INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Gas/Electric Checks',@Void_MENUID,1,1,'~/../PropertyDataRestructure/Views/Void/GasElectricCheck/GasElectricChecks.aspx','5',2,@Void_SCHEDULING_PAGEID,'',1)
	
	
--=========================
-- Voids > Reports
--=========================

--Available Properties
INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Available Properties',@Void_MENUID,1,1,'~/../PropertyDataRestructure/Views/Reports/VoidReportArea.aspx?rpt=ap','1',2,@Void_Reports_PAGEID,'',1)

--British Gas Notifications
INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('British Gas Notifications',@Void_MENUID,1,1,'~/../PropertyDataRestructure/Views/Void/BritishGasNotification/BritishGasNotificationList.aspx','2',2,@Void_Reports_PAGEID,'',1)

--No Entry Report
INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('No Entries',@Void_MENUID,1,1,'~/../PropertyDataRestructure/Views/Reports/VoidReportArea.aspx?rpt=ne','3',2,@Void_Reports_PAGEID,'',1)
	
--======================================================================================================
--												PAGES	(LEVEL 3)
--======================================================================================================

--Pending Termination
INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Pending Termination',@Void_MENUID,1,0,'~/../PropertyDataRestructure/Views/Reports/VoidReportArea.aspx?rpt=pt','1',3,@Void_Reports_PAGEID,'',0)

--Relet Due In Next 7 Days
INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Relet Due In Next 7 Days',@Void_MENUID,1,0,'~/../PropertyDataRestructure/Views/Reports/VoidReportArea.aspx?rpt=rd','1',3,@Void_Reports_PAGEID,'',0)

--================================================
-- Void > Scheduling > Required Void Works 
--================================================

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Void Works Required',@Void_MENUID,1,0,'~/../PropertyDataRestructure/Views/Void/VoidWorks/VoidWorksRequired.aspx','1',3,@Void_Works_PAGEID,'',0)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Scheduling Required Works',@Void_MENUID,1,0,'~/../PropertyDataRestructure/Views/Void/VoidWorks/SchedulingRequiredWorks.aspx','2',3,@Void_Works_PAGEID,'',0)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('ReScheduling Required Works',@Void_MENUID,1,0,'~/../PropertyDataRestructure/Views/Void/VoidWorks/RearrangeWorksRequired.aspx','3',3,@Void_Works_PAGEID,'',0)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('JSV Job Sheet Summary',@Void_MENUID,1,0,'~/../PropertyDataRestructure/Views/Void/VoidWorks/JSVJobSheetSummary.aspx','4',3,@Void_Works_PAGEID,'',0)

--================================================
-- Void > Scheduling > Paint Pack List 
--================================================

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Paint Packs Detail',@Void_MENUID,1,0,'~/../PropertyDataRestructure/Views/Void/PaintPacks/PaintPacksDetail.aspx','1',3,@Void_PaintPacks_PAGEID,'',0)
		
END TRY 
	BEGIN CATCH 	
		IF @@TRANCOUNT >0	  
		BEGIN	   
			ROLLBACK TRANSACTION;	
		END 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
		@ErrorSeverity, -- Severity.
		@ErrorState -- State.
		);
	END CATCH  
	 
	IF @@TRANCOUNT >0
	BEGIN
		COMMIT TRANSACTION;
	END 
	

