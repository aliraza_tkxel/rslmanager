/* =================================================================================    
  Page Description:     Save Tenancy Termination for 1st Inspection

  Author: Ali Raza
  Creation Date: May-13-2015

  Change History:

  Version      Date             By                      Description
  =======     ============    ========           ===========================
  v1.0         Dec-30-2014      Ali Raza           Save Tenancy Termination for 1st Inspection
  Execution Command:
  
  Exec void_SaveTenancyTermination
=================================================================================*/
CREATE PROCEDURE V_SaveTenancyTermination
	@PropertyId varchar(100) = NULL,
	@CustomerId int = NULL,
	@TenancyId int = NULL,
	@TerminationDate DateTime,
	@UpdatedBy int,
	@Inspection int=1
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from

SET NOCOUNT ON;

DECLARE @MSATTypeId INT,
		@MSATId INT,
		@ToBeArrangedStatusId INT
---Declare Cursor variable            
DECLARE @ItemToInsertCursor CURSOR

SELECT
	@ToBeArrangedStatusId = PDR_STATUS.STATUSID
FROM PDR_STATUS
WHERE TITLE = 'To be Arranged'

IF (@Inspection = 1)
	BEGIN
SELECT
	@MSATTypeId = MSATTypeId
FROM PDR_MSATType
WHERE MSATTypeName = 'Void Inspection'
END

IF Not EXISTS(SELECT MSATID from PDR_MSAT where PropertyId=@PropertyId AND CustomerId=@CustomerId AND TenancyId=@TenancyId)
 BEGIN
		INSERT INTO PDR_MSAT (PropertyId, MSATTypeId, CustomerId, TenancyId, TerminationDate)
			VALUES (@PropertyId, @MSATTypeId, @CustomerId, @TenancyId, @TerminationDate)

		SELECT
			@MSATId = SCOPE_IDENTITY()

		INSERT INTO [PDR_JOURNAL] ([MSATID], [STATUSID], [CREATIONDATE], [CREATEDBY])
			VALUES (@MSATId, @ToBeArrangedStatusId, GETDATE(), @UpdatedBy)

		return SCOPE_IDENTITY()
	END
END
GO