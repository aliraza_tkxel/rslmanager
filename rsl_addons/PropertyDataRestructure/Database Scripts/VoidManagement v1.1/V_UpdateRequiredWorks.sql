
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE V_UpdateRequiredWorks
	-- Add the parameters for the stored procedure here
	@requiredWorksId int,
	@roomId int,
	@workDescription nvarchar(500),
	@isTenantWorks bit,
	@isCanceled bit,
	@updatedBy int,
	@isSaved int = 0 out 
AS
BEGIN

DECLARE @isBrsWork BIT='False'	
BEGIN TRANSACTION;    
BEGIN TRY  
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	IF(@isTenantWorks = 0)
	BEGIN
	SET @isBrsWork = 'TRUE'
	END
	
IF(@isCanceled = 1)
	BEGIN
	 Update V_RequiredWorks SET RoomId=@roomId,WorkDescription=@workDescription ,IsTenantWorks=@isTenantWorks,
	 IsBrsWorks=@isBrsWork,ModifiedDate=@updatedBy,IsCanceled=@isCanceled,StatusId=(Select STATUSID from PDR_STATUS where TITLE='Cancelled')	
	   WHERE  RequiredWorksId=@requiredWorksId
	END
ELSE
	BEGIN

	   Update V_RequiredWorks SET RoomId=@roomId,WorkDescription=@workDescription ,IsTenantWorks=@isTenantWorks,
	   IsBrsWorks=@isBrsWork,ModifiedDate=@updatedBy
	   WHERE  RequiredWorksId=@requiredWorksId
	END
   
END TRY    
BEGIN CATCH     
 IF @@TRANCOUNT > 0    
 BEGIN         
  ROLLBACK TRANSACTION;       
  SET @isSaved = 0            
 END    
 DECLARE @ErrorMessage NVARCHAR(4000);    
 DECLARE @ErrorSeverity INT;    
 DECLARE @ErrorState INT;    
    
 SELECT @ErrorMessage = ERROR_MESSAGE(),    
 @ErrorSeverity = ERROR_SEVERITY(),    
 @ErrorState = ERROR_STATE();    
    
 -- Use RAISERROR inside the CATCH block to return     
 -- error information about the original error that     
 -- caused execution to jump to the CATCH block.    
 RAISERROR (@ErrorMessage, -- Message text.    
    @ErrorSeverity, -- Severity.    
    @ErrorState -- State.    
   );    
END CATCH;    
    
IF @@TRANCOUNT > 0    
 BEGIN      
  COMMIT TRANSACTION;      
  SET @isSaved = 1    
 END    
  
    
END
