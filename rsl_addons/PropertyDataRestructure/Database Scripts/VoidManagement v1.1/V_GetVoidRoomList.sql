USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[V_GetVoidRoomList]    Script Date: 5/25/2015 1:24:14 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,Ali Raza>
-- Create date: <Create Date,22/05/2015>
-- Description:	<Description,got room list to populate dropdown list>
--	Version     Date			By				Description
--  =======		============    ========		===========================
--  v1.0        May-15-2015     Ali Raza		Get room list to populate dropdown list
-- =============================================
CREATE PROCEDURE [dbo].[V_GetVoidRoomList] 
	
AS
BEGIN
	Select RoomId, Name from V_RoomList WHERE IsActive=1
END
