
/* =================================================================================    
    Page Description: This trigger 'll insert the record in PA_PROPERTY_ATTRIBUTES_HISTORY after Updation 
	in [PA_PROPERTY_ATTRIBUTES]
    Author: Ali Raza
    Creation Date: June-22-2015

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         June-22-2015      Ali Raza         This trigger 'll insert the record in 
													PA_PROPERTY_ATTRIBUTES_HISTORY after Updation
													in [PA_PROPERTY_ATTRIBUTES]
  =================================================================================*/
CREATE TRIGGER PA_AFTER_Update_PA_PROPERTY_ATTRIBUTES
   ON  dbo.[PA_PROPERTY_ATTRIBUTES]
   AFTER UPDATE
AS 
BEGIN
	INSERT INTO PA_PROPERTY_ATTRIBUTES_HISTORY
	(ATTRIBUTEID
	,PROPERTYID
	,ITEMPARAMID
	,PARAMETERVALUE
	,VALUEID
	,UPDATEDON
	,UPDATEDBY
	,IsCheckBoxSelected
	,SchemeId
	,BlockId
	,IsUpdated)
	SELECT
	i.ATTRIBUTEID,
	i.PROPERTYID,
	i.ITEMPARAMID,
	i.PARAMETERVALUE,
	i.VALUEID,
	i.UPDATEDON,
	i.UPDATEDBY,
	i.IsCheckBoxSelected,
	i.SchemeId,
	i.BlockId,
	i.IsUpdated
	From INSERTED i
	

END

