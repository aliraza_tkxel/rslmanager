/*
   Thursday, May 7, 20156:56:55 PM
   User: sa
   Server: DEV-PC4
   Database: RSLBHALive
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.PDR_JOURNAL SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.PDR_JOURNAL', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.PDR_JOURNAL', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.PDR_JOURNAL', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.V_AppointmentRecordedData ADD CONSTRAINT
	FK_V_AppointmentRecordedData_PDR_JOURNAL1 FOREIGN KEY
	(
	GasElectricCheckJournalId
	) REFERENCES dbo.PDR_JOURNAL
	(
	JOURNALID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.V_AppointmentRecordedData SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.V_AppointmentRecordedData', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.V_AppointmentRecordedData', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.V_AppointmentRecordedData', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.V_RequiredWorks ADD CONSTRAINT
	FK_V_RequiredWorks_PDR_JOURNAL1 FOREIGN KEY
	(
	WorksJournalId
	) REFERENCES dbo.PDR_JOURNAL
	(
	JOURNALID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.V_RequiredWorks SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.V_RequiredWorks', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.V_RequiredWorks', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.V_RequiredWorks', 'Object', 'CONTROL') as Contr_Per 