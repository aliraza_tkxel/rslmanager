/*
   Friday, May 8, 20157:31:08 PM
   User: sa
   Server: DEV-PC4
   Database: RSLBHALive
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.V_RequiredWorks SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.V_RequiredWorks', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.V_RequiredWorks', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.V_RequiredWorks', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
CREATE TABLE dbo.V_PauseWorks
	(
	PauseWorksId int NOT NULL IDENTITY (1, 1),
	RequiredWorksId int NULL,
	PausedOn smalldatetime NULL,
	Notes nvarchar(100) NULL,
	Reason nvarchar(50) NULL,
	PausedBy int NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.V_PauseWorks ADD CONSTRAINT
	PK_V_PauseWorks PRIMARY KEY CLUSTERED 
	(
	PauseWorksId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.V_PauseWorks ADD CONSTRAINT
	FK_V_PauseWorks_V_RequiredWorks FOREIGN KEY
	(
	RequiredWorksId
	) REFERENCES dbo.V_RequiredWorks
	(
	RequiredWorksId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.V_PauseWorks SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.V_PauseWorks', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.V_PauseWorks', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.V_PauseWorks', 'Object', 'CONTROL') as Contr_Per 