-- =============================================
/* =================================================================================    
    Page Description: Get Paint Packs List
    Author: Ali Raza
    Creation Date: June-04-2015 

    Change History:

    Version      Date             By                      Description
    =======     ============    ========			===========================
    v1.0        June-04-2015      Ali Raza		   Get Paint Packs List
    
-- Execute Command   
EXEC V_GetPaintPacksDetail 	1   
  =================================================================================*/
ALTER PROCEDURE V_GetPaintPacksDetail 
	(
	@paintPackId int
	)
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;
--=====================================================
--Get Paint Pack Header values
--=====================================================

SELECT
	ISNULL(P__PROPERTY.HouseNumber, '') + ISNULL(' ' + P__PROPERTY.ADDRESS1, '') + ISNULL(', ' + P__PROPERTY.ADDRESS2, '') AS Address,
	ISNULL(BN.NewTenantName,'N/A') AS NewTenant,
	CONVERT(NVARCHAR(50), PDR_MSAT.TerminationDate, 103) AS Termination,
	CONVERT(NVARCHAR(50), DATEADD(DAY, 7, PDR_MSAT.TerminationDate), 103) AS Relet,
	ISNULL(BN.NewTenantTel,'N/A') AS NewTelephone,
	PDR_MSAT.TENANCYID AS TenancyId,
	Vacating.Tenant AS VacatingTenant,
	ISNULL(Vacating.Tele,'N/A') AS VacatingTelephone,
	ISNULL(P.SupplierId,'-1') as Supplier,
	ISNULL(P.StatusId,'-1') as StatusId,
	ISNULL(CONVERT(NVARCHAR(50), P.DeliveryDueDate, 103),'') as DeliveryDueDate
FROM PDR_JOURNAL J
				INNER JOIN V_PaintPack P ON J.JOURNALID = P.InspectionJournalId

				INNER JOIN PDR_MSAT ON J.MSATID = PDR_MSAT.MSATId
				INNER JOIN PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId
				INNER JOIN P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID
				Left JOIN V_BritishGasVoidNotification BN ON P__PROPERTY.PROPERTYID = BN.PropertyId AND PDR_MSAT.TenancyId = BN.TenancyId AND PDR_MSAT.CustomerId=BN.CustomerId
				
				--LEFT JOIN C_TENANCY ON P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID
				--	AND C_TENANCY.ENDDATE IS NULL
				--LEFT JOIN C_CUSTOMERTENANCY ON C_TENANCY.TENANCYID = C_CUSTOMERTENANCY.TENANCYID
				--LEFT JOIN C__CUSTOMER ON C_CUSTOMERTENANCY.CUSTOMERID = C__CUSTOMER.CUSTOMERID
				--LEFT JOIN C_ADDRESS ON C__CUSTOMER.CUSTOMERID = C_ADDRESS.CUSTOMERID AND ISDEFAULT = 1
				--LEFT JOIN G_TITLE ON C__CUSTOMER.TITLE = G_TITLE.TITLEID
				
				INNER JOIN (SELECT CU.CUSTOMERID AS CID, CONVERT(VARCHAR, CASE WHEN CU.TITLE = 6 THEN '' ELSE ISNULL(G_TITLE.[DESCRIPTION], '')END) + CONVERT(VARCHAR, ISNULL(+' ' + CU.FIRSTNAME, '')) + ' ' + CONVERT(VARCHAR, ISNULL(CU.LASTNAME, '')) AS Tenant,
					AD.TEL AS Tele FROM C__CUSTOMER CU
				LEFT JOIN C_ADDRESS AD ON CU.CUSTOMERID = AD.CUSTOMERID AND ISDEFAULT = 1
				LEFT JOIN G_TITLE ON CU.TITLE = G_TITLE.TITLEID) AS Vacating
					ON PDR_MSAT.CustomerId = Vacating.CID

WHERE P.PaintPackId = @paintPackId

--=====================================================
--Get Paint Pack Detail 
--=====================================================

SELECT PD.PaintPackDetailId,ISNULL(PD.PaintRef,'-') AS PaintRef ,ISNULL(PD.PaintColor,'-') as PaintColor ,ISNULL(PD.PaintName,'-') as PaintName
,R.Name As Room from 
V_PaintPackDetails PD
INNER JOIN V_RoomList R ON PD.RoomId = R.RoomId
WHERE PD.PaintPackId = @paintPackId
--=====================================================
--Get Status For Dropdown
--=====================================================
Select StatusId,Title  from V_PaintStatus
--=====================================================
--Get Supplier for Dropdown
--=====================================================
SELECT DISTINCT O.ORGID AS [Id],NAME AS [Description] FROM S_ORGANISATION O ORDER BY NAME ASC
END
GO