/*
   Monday, June 15, 20156:13:20 PM
   User: sa
   Server: DEV-PC4
   Database: RSLBHALive
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.V_PaintPack SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.V_PaintPack', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.V_PaintPack', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.V_PaintPack', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.V_PaintPackDetails ADD CONSTRAINT
	FK_V_PaintPackDetails_V_PaintPack FOREIGN KEY
	(
	PaintPackId
	) REFERENCES dbo.V_PaintPack
	(
	PaintPackId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.V_PaintPackDetails SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.V_PaintPackDetails', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.V_PaintPackDetails', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.V_PaintPackDetails', 'Object', 'CONTROL') as Contr_Per 