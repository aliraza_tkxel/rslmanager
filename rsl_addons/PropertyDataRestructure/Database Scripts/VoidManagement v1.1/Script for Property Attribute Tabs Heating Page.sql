BEGIN TRANSACTION
	BEGIN TRY  
	
	
--  Internals > Services > Heating & Water Supply > Heating
declare @itemid int;
declare @paramid int;
declare @paramSOrder int;
SELECT @itemid = ItemID from PA_ITEM where ItemName = 'Heating'
	
select @paramSOrder=ParameterSorder from PA_PARAMETER where ParameterName ='Meter Location'	

--For Gas Meter Type
insert into [RSLBHALive].[dbo].[PA_PARAMETER] (ParameterName,DataType,ControlType,[isdate],ParameterSorder,IsActive,ShowInApp)
values
('Gas Meter Type','String','Dropdown',0,@paramSOrder+1,1,1)

set @paramid = SCOPE_IDENTITY()

insert into [RSLBHALive].[dbo].[PA_ITEM_PARAMETER] (ItemId,ParameterId,IsActive)
values
(@itemid,@paramid,1)	

insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values(@paramid,'Coriolis',0,1)
insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values(@paramid,'Diaphragm/bellows',1,1)
insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values(@paramid,'Orifice',2,1)
insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values(@paramid,'Rotary',3,1)
insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values(@paramid,'Turbine',4,1)
insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values(@paramid,'Ultrasonic flow',5,1)
	
--For Gas Meter Reading
insert into [RSLBHALive].[dbo].[PA_PARAMETER] (ParameterName,DataType,ControlType,[isdate],ParameterSorder,IsActive,ShowInApp)
values
('Gas Meter Reading','String','TextBox',0,@paramSOrder+2,1,1)

set @paramid = SCOPE_IDENTITY()

insert into [RSLBHALive].[dbo].[PA_ITEM_PARAMETER] (ItemId,ParameterId,IsActive)
values
(@itemid,@paramid,1)	
	
--For Gas Meter Reading Date
insert into [RSLBHALive].[dbo].[PA_PARAMETER] (ParameterName,DataType,ControlType,[isdate],ParameterSorder,IsActive,ShowInApp)
values
('Gas Meter Reading Date','Date','Date',1,@paramSOrder+2,1,1)

set @paramid = SCOPE_IDENTITY()

insert into [RSLBHALive].[dbo].[PA_ITEM_PARAMETER] (ItemId,ParameterId,IsActive)
values
(@itemid,@paramid,1)


--For Electric Meter Type
insert into [RSLBHALive].[dbo].[PA_PARAMETER] (ParameterName,DataType,ControlType,[isdate],ParameterSorder,IsActive,ShowInApp)
values
('Electric Meter Type','String','Dropdown',0,@paramSOrder+3,1,1)

set @paramid = SCOPE_IDENTITY()

insert into [RSLBHALive].[dbo].[PA_ITEM_PARAMETER] (ItemId,ParameterId,IsActive)
values
(@itemid,@paramid,1)	
	
insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values(@paramid,'Standard Meter',0,1)
insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values(@paramid,'Variable Rate-Economy 7',1,1)
insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values(@paramid,'Pre-Payment',2,1)
insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values(@paramid,'Smart Meters',3,1)
insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values(@paramid,'Economy 10',4,1)	
--For Electric Meter Reading
insert into [RSLBHALive].[dbo].[PA_PARAMETER] (ParameterName,DataType,ControlType,[isdate],ParameterSorder,IsActive,ShowInApp)
values
('Electric Meter Reading','String','TextBox',0,@paramSOrder+4,1,1)

set @paramid = SCOPE_IDENTITY()

insert into [RSLBHALive].[dbo].[PA_ITEM_PARAMETER] (ItemId,ParameterId,IsActive)
values
(@itemid,@paramid,1)	
	
--For Electric Meter Reading Date
insert into [RSLBHALive].[dbo].[PA_PARAMETER] (ParameterName,DataType,ControlType,[isdate],ParameterSorder,IsActive,ShowInApp)
values
('Electric Meter Reading Date','Date','Date',1,@paramSOrder+4,1,1)

set @paramid = SCOPE_IDENTITY()

insert into [RSLBHALive].[dbo].[PA_ITEM_PARAMETER] (ItemId,ParameterId,IsActive)
values
(@itemid,@paramid,1)




END TRY 
	BEGIN CATCH 	
		IF @@TRANCOUNT >0	  
		BEGIN	   
			  PRINT 'Some thing went wrong with this transaction'

			ROLLBACK TRANSACTION;	
		END 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
		@ErrorSeverity, -- Severity.
		@ErrorState -- State.
		);
	END CATCH  
	 
	IF @@TRANCOUNT >0
	BEGIN
  PRINT 'Transaction completed successfully'
	
		COMMIT TRANSACTION;
	END 