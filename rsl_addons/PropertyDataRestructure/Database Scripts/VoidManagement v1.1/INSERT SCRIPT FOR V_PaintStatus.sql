USE [RSLBHALive]
GO
/****** Object:  Table [dbo].[V_PaintStatus]    Script Date: 06/23/2015 14:49:44 ******/
INSERT [dbo].[V_PaintStatus] ([StatusId], [Title], [IsActive]) VALUES (1, N'Logged', 1)
INSERT [dbo].[V_PaintStatus] ([StatusId], [Title], [IsActive]) VALUES (2, N'In Progress', 1)
INSERT [dbo].[V_PaintStatus] ([StatusId], [Title], [IsActive]) VALUES (3, N'Ordered', 1)
INSERT [dbo].[V_PaintStatus] ([StatusId], [Title], [IsActive]) VALUES (4, N'Delivered', 1)
INSERT [dbo].[V_PaintStatus] ([StatusId], [Title], [IsActive]) VALUES (5, N'Complete', 1)
