
-- =============================================
--EXEC	V_UpdateTenantToComplete
--		@worksRequiredId = 8,
--		@checkedValue = 
-- Author:		<Author,,Ali Raza>
-- Create date: <Create Date,May 22,2015>
-- Description:	<Description,Update Tenant To Complete>
-- =============================================
ALTER PROCEDURE V_UpdateTenantToComplete(
	@worksRequiredId int,
	@checkedValue bit,
	@isSaved int = 0 out 
	)
	AS
BEGIN
DECLARE @isBrsWork BIT='False'	 
IF(@checkedValue = 0)
	BEGIN
	SET @isBrsWork = 'TRUE'
	END
BEGIN TRANSACTION;    
BEGIN TRY   
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   update V_RequiredWorks set IsTenantWorks=@checkedValue, IsBrsWorks=@isBrsWork where RequiredWorksId=@worksRequiredId
   
END TRY    
BEGIN CATCH     
 IF @@TRANCOUNT > 0    
 BEGIN         
  ROLLBACK TRANSACTION;       
  SET @isSaved = 0            
 END    
 DECLARE @ErrorMessage NVARCHAR(4000);    
 DECLARE @ErrorSeverity INT;    
 DECLARE @ErrorState INT;    
    
 SELECT @ErrorMessage = ERROR_MESSAGE(),    
 @ErrorSeverity = ERROR_SEVERITY(),    
 @ErrorState = ERROR_STATE();    
    
 -- Use RAISERROR inside the CATCH block to return     
 -- error information about the original error that     
 -- caused execution to jump to the CATCH block.    
 RAISERROR (@ErrorMessage, -- Message text.    
    @ErrorSeverity, -- Severity.    
    @ErrorState -- State.    
   );    
END CATCH;    
    
IF @@TRANCOUNT > 0    
 BEGIN      
  COMMIT TRANSACTION;      
  SET @isSaved = 1    
 END    
  
    
END
