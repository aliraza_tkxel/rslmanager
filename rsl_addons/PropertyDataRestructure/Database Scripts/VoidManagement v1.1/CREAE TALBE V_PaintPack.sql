USE [RSLBHALive]
GO

/****** Object:  Table [dbo].[V_PaintPack]    Script Date: 06/23/2015 13:19:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[V_PaintPack](
	[PaintPackId] [int] IDENTITY(1,1) NOT NULL,
	[InspectionJournalId] [int] NULL,
	[StatusId] [int] NULL,
	[SupplierId] [int] NULL,
	[DeliveryDueDate] [datetime] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
 CONSTRAINT [PK_V_PaintPack] PRIMARY KEY CLUSTERED 
(
	[PaintPackId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[V_PaintPack]  WITH CHECK ADD  CONSTRAINT [FK_V_PaintPack_PDR_JOURNAL] FOREIGN KEY([InspectionJournalId])
REFERENCES [dbo].[PDR_JOURNAL] ([JOURNALID])
GO

ALTER TABLE [dbo].[V_PaintPack] CHECK CONSTRAINT [FK_V_PaintPack_PDR_JOURNAL]
GO


