USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[V_GetBritishGasStage1Data]    Script Date: 6/30/2015 1:19:47 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =============================================
 Author:		<Author,Noor Muhamamd>
 Create date: <Create Date,25/05/2015>
 Description:	<Description,got room list to populate dropdown list>
 History:	25/06/2015 Noor : Query Get the record to populate the british gas notification of stage1
            30/09/2014 Name : Correct the query to fetch gas electric check boolean value
-- =============================================*/
ALTER PROCEDURE [dbo].[V_GetBritishGasStage1Data] 
	@propertyId varchar(20)
	,@customerId int
	,@tenancyId int
AS
BEGIN
	SELECT 
	case when  V_BritishGasVoidNotification.IsGasCheck=1 OR  V_BritishGasVoidNotification.IsElectricCheck=1 then 1 Else 0 END   AS IsGasElectricCheck
	,P__PROPERTY.PROPERTYID AS PropertyId
	,C__CUSTOMER.CUSTOMERID AS CustomerId
	,C_TENANCY.TENANCYID  AS TenancyId
	,P__PROPERTY.ADDRESS1 AS Address1
	,P__PROPERTY.ADDRESS2 AS Address2
	,P__PROPERTY.TOWNCITY AS City
	,P__PROPERTY.County As County
	,P__PROPERTY.POSTCODE AS PostCode
	, CONVERT(VARCHAR,Case When C__CUSTOMER.TITLE=6 then '' Else  ISNULL(G_TITLE.[DESCRIPTION],'')END)+ CONVERT(VARCHAR, ISNULL(+' '+C__CUSTOMER.FIRSTNAME,''))+' '+ CONVERT(VARCHAR, ISNULL(C__CUSTOMER.LASTNAME,'')) AS TenantName 
	,V_BritishGasVoidNotification.TenantFwAddress1 AS TenantFwAddress1
	,V_BritishGasVoidNotification.TenantFwAddress2 AS TenantFwAddress2
	,V_BritishGasVoidNotification.TenantFwCity AS TenantFwCity
	,V_BritishGasVoidNotification.TenantFwPostCode AS TenantFwPostCode	
	,Case When V_BritishGasVoidNotification.DateOccupancyCease IS NULL
		THEN  
			(SELECT TOP 1 PDR_MSAT.TerminationDate 
				FROM PDR_MSAT 
				WHERE 
				PDR_MSAT.PROPERTYID =@propertyId
				AND PDR_MSAT.TENANCYID = @tenancyId
				AND PDR_MSAT.CUSTOMERID = @customerId 
				AND TERMINATIONDATE IS NOT NULL) 
		ELSE V_BritishGasVoidNotification.DateOccupancyCease
		END
		AS DateOccupancyCease
	FROM V_BritishGasVoidNotification 
	INNER JOIN	P__PROPERTY ON V_BritishGasVoidNotification.PropertyId = P__PROPERTY.PROPERTYID	
	INNER JOIN C_TENANCY ON V_BritishGasVoidNotification.TenancyId=	C_TENANCY.TENANCYID
	INNER JOIN C__CUSTOMER ON V_BritishGasVoidNotification.CustomerId = C__CUSTOMER.CUSTOMERID
	INNER JOIN C_CUSTOMERTENANCY on C__CUSTOMER.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID and C_TENANCY.TENANCYID=C_CUSTOMERTENANCY.TENANCYID
	LEFT JOIN G_TITLE ON C__CUSTOMER.TITLE=G_TITLE.TITLEID
	WHERE 
	 P__PROPERTY.PROPERTYID =@propertyId
	 AND C_TENANCY.TENANCYID = @tenancyId
	 AND C__CUSTOMER.CUSTOMERID = @customerId

END
