USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[V_SaveBritishGasData]    Script Date: 6/30/2015 1:30:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
  Page Description:     SaveBritishGasData

  Author: Noor Muhammad
  Creation Date: June-16-2015
   History:  16/06/2015 Noor : Query for Save British Gas Data
             30/09/2014 Name : 
  Execution Command:
  
    
-- ============================================= */   
ALTER PROCEDURE [dbo].[V_SaveBritishGasData]    
 -- Add the parameters for the stored procedure here    
 @customerId int 
,@propertyId varchar(20)  
,@tenancyId int    
,@isSaved bit out    
,@britishGasId int out 
,@britishGasStageId int out 
,@britishGasStage varchar(20) out 
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
                 
BEGIN TRANSACTION;    
BEGIN TRY    
	
	DECLARE @notificationStatusId INT
	
	SELECT @britishGasId = BritishGasVoidNotificationId 
	FROM [V_BritishGasVoidNotification]
	WHERE 
	CustomerId = @customerId
	AND PropertyId = @propertyId
	AND TenancyId = @tenancyId
	
	-- =============================================    
	-- Get status id of "Stage1"    
	-- =============================================     
	SELECT  @britishGasStageId = V_Stage.StageId, @britishGasStage = V_Stage.Name
	FROM	V_Stage 
	WHERE	V_Stage.Name ='Stage 1' 
	AND V_Stage.IsActive = 1 
	
	-- =============================================    
	-- Get status id of In Progress   
	-- =============================================    
	SELECT  @notificationStatusId = PDR_STATUS.STATUSID FROM PDR_STATUS WHERE TITLE = 'In Progress'
	
	IF @britishGasId = 0 OR @britishGasId IS NULL 
		BEGIN	    	
			             
			INSERT INTO [V_BritishGasVoidNotification]
				   (
					[CustomerId]
				   ,[PropertyId]
				   ,[TenancyId]
				   ,[StageId]
				   ,[StatusId]
				   ,[CreatedDate]
				   ,[ModifiedDate]
				   ,[isNotificationSent])
			 VALUES
				   (
					 @customerId
					,@propertyId
					,@tenancyId
					,@britishGasStageId
					,@notificationStatusId
					,CURRENT_TIMESTAMP
					,CURRENT_TIMESTAMP
					,0)   
          
			  SELECT @britishGasId = SCOPE_IDENTITY()    
			  PRINT 'British Gas Id = '+ CONVERT(VARCHAR,@britishGasId)            
		END	   
END TRY    
BEGIN CATCH     
 IF @@TRANCOUNT > 0    
 BEGIN         
  ROLLBACK TRANSACTION;       
  SET @isSaved = 0            
 END    
 DECLARE @ErrorMessage NVARCHAR(4000);    
 DECLARE @ErrorSeverity INT;    
 DECLARE @ErrorState INT;    
    
 SELECT @ErrorMessage = ERROR_MESSAGE(),    
 @ErrorSeverity = ERROR_SEVERITY(),    
 @ErrorState = ERROR_STATE();    
    
 -- Use RAISERROR inside the CATCH block to return     
 -- error information about the original error that     
 -- caused execution to jump to the CATCH block.    
 RAISERROR (@ErrorMessage, -- Message text.    
    @ErrorSeverity, -- Severity.    
    @ErrorState -- State.    
   );    
END CATCH;    
    
IF @@TRANCOUNT > 0    
 BEGIN      
  COMMIT TRANSACTION;      
  SET @isSaved = 1    
 END    
  
    
END