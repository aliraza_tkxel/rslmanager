USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[V_GetBritishGasStage2AndStage3Data]    Script Date: 6/26/2015 12:36:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:           Noor Muhammad
-- Create date:      23/06/2015
-- Description:      Get British Gas Stage 2 & Stage 3 Data
-- History:          23/06/2015 NoorM : Query for stage 2 british gas data
--                   03/07/2015 NoorM : Query for stage 3 british gas data
-- =============================================
CREATE PROCEDURE [dbo].[V_GetBritishGasStage2AndStage3Data] 
	@britishGasVoidNotificationId int	
AS
BEGIN
	SELECT 	
	V_BritishGasVoidNotification.BritishGasVoidNotificationId AS BritishGasVoidNotificationId
	,V_BritishGasVoidNotification.GasMeterTypeId AS GasMeterTypeId
	,V_BritishGasVoidNotification.GasMeterReading AS GasMeterReading
	,V_BritishGasVoidNotification.GasMeterReadingDate AS GasMeterReadingDate
	,V_BritishGasVoidNotification.ElectricMeterTypeId AS ElectricMeterTypeId
	,V_BritishGasVoidNotification.ElectricMeterReading AS ElectricMeterReading
	,V_BritishGasVoidNotification.ElectricMeterReadingDate AS ElectricMeterReadingDate
	,V_BritishGasVoidNotification.GasDebtAmount AS GasDebtAmount	
	,V_BritishGasVoidNotification.GasDebtAmountDate AS GasDebtAmountDate
	,V_BritishGasVoidNotification.ElectricDebtAmount AS ElectricDebtAmount	
	,V_BritishGasVoidNotification.ElectricDebtAmountDate AS ElectricDebtAmountDate	
	,V_BritishGasVoidNotification.NewTenantName AS NewTenantName	
	,V_BritishGasVoidNotification.NewTenantDateOfBirth AS NewTenantDateOfBirth
	,V_BritishGasVoidNotification.NewTenantTel AS NewTenantTel
	,V_BritishGasVoidNotification.NewTenantMobile AS NewTenantMobile
	,V_BritishGasVoidNotification.NewTenantOccupancyDate AS NewTenantOccupancyDate
	,V_BritishGasVoidNotification.NewTenantPreviousAddress1 AS NewTenantPreviousAddress1
	,V_BritishGasVoidNotification.NewTenantPreviousAddress2 AS NewTenantPreviousAddress2
	,V_BritishGasVoidNotification.NewTenantPreviousPostCode AS NewTenantPreviousPostCode
	,V_BritishGasVoidNotification.NewTenantPreviousTownCity AS NewTenantPreviousTownCity
	,V_BritishGasVoidNotification.NewGasMeterReading AS NewGasMeterReading
	,V_BritishGasVoidNotification.NewGasMeterReadingDate AS NewGasMeterReadingDate
	,V_BritishGasVoidNotification.NewElectricMeterReading AS NewElectricMeterReading
	,V_BritishGasVoidNotification.NewElectricMeterReadingDate AS NewElectricMeterReadingDate
	
	FROM V_BritishGasVoidNotification 
	
	WHERE 
	 V_BritishGasVoidNotification.BritishGasVoidNotificationId =@britishGasVoidNotificationId
	 

END
