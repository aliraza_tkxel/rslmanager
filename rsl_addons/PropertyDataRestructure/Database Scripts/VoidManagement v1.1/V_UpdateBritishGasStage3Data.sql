USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[V_UpdateBritishGasData]    Script Date: 7/2/2015 11:12:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
  Name:     [V_UpdateBritishGasStage3Data]

  Author: Ali Raza
  Creation Date: August-17-2015
  Description: Update British Gas Data For Stage3
  History:  17/08/2015 Noor : Query for update of british gas  stage 3 data
            
  Execution Command:
  
    
-- ============================================= */   
ALTER PROCEDURE [dbo].[V_UpdateBritishGasStage3Data]    
 -- Add the parameters for the stored procedure here    
 @britishGasId int 
,@newTenantName varchar(50) = null 
,@newTenantTel varchar(30) = null
,@newTenantMobile varchar(30) = null
,@newTenantDateOfBirth  date = null
,@newTenantOccupancyDate date = null
,@newTenantPreviousAddress1 varchar(50) = null
,@newTenantPreviousAddress2 varchar(50) = null
,@newTenantPreviousPostCode varchar(20) = null
,@newTenantPreviousTownCity varchar(50) = null
,@electricMeterReading bigint = null
,@electricMeterReadingDate date = null
,@gasMeterReading bigint = null
,@gasMeterReadingDate date = null
,@userId int = null
,@isSaved bit = 0 out    
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
                 
BEGIN TRANSACTION;    
BEGIN TRY     
                  
   UPDATE [dbo].[V_BritishGasVoidNotification]
   SET 
      
      [NewGasMeterReading] = @gasMeterReading
      ,[NewGasMeterReadingDate] = @gasMeterReadingDate
      ,[NewElectricMeterReading] = @electricMeterReading
      ,[NewElectricMeterReadingDate] = @electricMeterReadingDate
      ,[NewTenantName] = @newTenantName
      ,[NewTenantTel]=@newTenantTel
      ,[NewTenantMobile]=@newTenantMobile
      ,[NewTenantDateOfBirth]=@newTenantDateOfBirth
      ,[NewTenantOccupancyDate]=@newTenantOccupancyDate
      ,[NewTenantPreviousAddress1]=@newTenantPreviousAddress1
      ,[NewTenantPreviousAddress2]=@newTenantPreviousAddress2
      ,[NewTenantPreviousTownCity]=@newTenantPreviousTownCity
      ,[NewTenantPreviousPostCode]=@newTenantPreviousPostCode
      ,[ModifiedDate] = CURRENT_TIMESTAMP
	  ,[UserId] = @userId
	  ,StageId = (SELECT StageId FROM V_Stage WHERE V_Stage.Name = 'Stage 3')
	  ,StatusId=(Select  STATUSID from PDR_STATUS where TITLE='Completed')
	  ,isNotificationSent=1
 WHERE 	   
	   BritishGasVoidNotificationId = @britishGasId   
	
			   	  
END TRY    
BEGIN CATCH     
 IF @@TRANCOUNT > 0    
 BEGIN         
  ROLLBACK TRANSACTION;       
  SET @isSaved = 0            
 END    
 DECLARE @ErrorMessage NVARCHAR(4000);    
 DECLARE @ErrorSeverity INT;    
 DECLARE @ErrorState INT;    
    
 SELECT @ErrorMessage = ERROR_MESSAGE(),    
 @ErrorSeverity = ERROR_SEVERITY(),    
 @ErrorState = ERROR_STATE();    
    
 -- Use RAISERROR inside the CATCH block to return     
 -- error information about the original error that     
 -- caused execution to jump to the CATCH block.    
 RAISERROR (@ErrorMessage, -- Message text.    
    @ErrorSeverity, -- Severity.    
    @ErrorState -- State.    
   );    
END CATCH;    
    
IF @@TRANCOUNT > 0    
 BEGIN      
  COMMIT TRANSACTION;      
  SET @isSaved = 1    
 END    
  
    
END