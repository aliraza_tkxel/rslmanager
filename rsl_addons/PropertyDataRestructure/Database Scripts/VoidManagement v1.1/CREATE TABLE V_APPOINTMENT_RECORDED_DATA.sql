/*
   Thursday, May 7, 20154:23:28 PM
   User: sa
   Server: DEV-PC4
   Database: RSLBHALive
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
EXECUTE sp_rename N'dbo.V_APPOINTMENT_RECORDED_DATA.GasElectircCheckJournalId', N'Tmp_GasElectricCheckJournalId_1', 'COLUMN' 
GO
EXECUTE sp_rename N'dbo.V_APPOINTMENT_RECORDED_DATA.Tmp_GasElectricCheckJournalId_1', N'GasElectricCheckJournalId', 'COLUMN' 
GO
ALTER TABLE dbo.V_APPOINTMENT_RECORDED_DATA SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.V_APPOINTMENT_RECORDED_DATA', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.V_APPOINTMENT_RECORDED_DATA', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.V_APPOINTMENT_RECORDED_DATA', 'Object', 'CONTROL') as Contr_Per 