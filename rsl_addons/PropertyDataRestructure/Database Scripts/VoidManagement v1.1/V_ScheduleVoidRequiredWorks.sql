USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[V_ScheduleVoidRequiredWorks]    Script Date: 06/01/2015 16:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
  Page Description:     Schedule Void Required Works Appointments

  Author: Ali Raza
  Creation Date: May-28-2015

  Change History:

  Version      Date             By                      Description
  =======     ============    ========           ===========================
  v1.0         May-18-2015      Ali Raza           Schedule Void Required Works Appointments
  Execution Command:
  
    
-- ============================================= */   
ALTER PROCEDURE [dbo].[V_ScheduleVoidRequiredWorks]    
 -- Add the parameters for the stored procedure here    
 @userId int 
,@appointmentNotes varchar(1000)  
,@jobSheetNotes varchar(1000)    
,@appointmentStartDate date    
,@appointmentEndDate date    
,@startTime varchar(10)    
,@endTime varchar(10)    
,@operativeId int         
,@inspectionJournalId INT    
,@duration float 
--,@requiredWorkIds varchar(1000) 
,@voidRequiredWorkDuration   as VoidRequiredWorkDuration Readonly
,@isSaved int = 0 out    
,@appointmentIdOut int = -1 out 
,@journalIdOut int = -1 out     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
             
DECLARE 
@ArrangedId int    
,@JournalHistoryId int    
,@AppointmentId int    
,@AppointmentHistoryId int    
,@PropertyId varchar(100) = NULL
,@CustomerId int = NULL
,@TenancyId int = NULL   
,@MSATTypeId INT
,@MSATId INT
,@journalId INT 
,@terminationDate DATETIME
,@reletDate DATETIME  
BEGIN TRANSACTION;    
BEGIN TRY    
-- =============================================    
-- Get PropertyId,TenancyId and customerId by inspectionJournalId    
-- =============================================  

Select @PropertyId=PropertyId,@TenancyId=TenancyId,@CustomerId=CustomerId,@terminationDate=TerminationDate,@reletDate=ReletDate from PDR_MSAT
INNER JOIN PDR_JOURNAL ON PDR_JOURNAL.MSATID= PDR_MSAT.MSATId
Where JOURNALID=@inspectionJournalId
-- =============================================    
-- Get status id of "Arranged"    and MSATTypeId
-- =============================================     
	SELECT  @ArrangedId = PDR_STATUS.statusid 
	FROM	PDR_STATUS 
	WHERE	PDR_STATUS.title ='Arranged'   
	
	SELECT @MSATTypeId = MSATTypeId FROM PDR_MSATType WHERE MSATTypeName = 'Void Works'
	
-- ====================================================================================================    
--          INSERT PDR_MSAT AND  PDR_JOURNAL    
-- ====================================================================================================    	
	
	
INSERT INTO PDR_MSAT (PropertyId, MSATTypeId, CustomerId, TenancyId,TerminationDate,ReletDate)
	VALUES (@PropertyId, @MSATTypeId, @CustomerId, @TenancyId,@terminationDate,@reletDate)

SELECT
	@MSATId = SCOPE_IDENTITY()

INSERT INTO [PDR_JOURNAL] ([MSATID], [STATUSID], [CREATIONDATE], [CREATEDBY])
	VALUES (@MSATId, @ArrangedId, GETDATE(), @userId)

SELECT @journalId = SCOPE_IDENTITY()
	

-- ====================================================================================================    
--          INSERTION (PDR_APPOINTMENTS)    
-- ====================================================================================================    
    
	SELECT	@JournalHistoryId = MAX(JOURNALHISTORYID)    
	FROM	PDR_JOURNAL_HISTORY    
	WHERE	JOURNALID = @journalId    
      
 PRINT 'JOURNALHISTORYID = '+ CONVERT(VARCHAR,@JournalHistoryId)  
    
    
           
    INSERT INTO [PDR_APPOINTMENTS]
           (
            [JOURNALID]
           ,[JOURNALHISTORYID]
           ,[APPOINTMENTSTARTDATE]
           ,[APPOINTMENTENDDATE]
           ,[APPOINTMENTSTARTTIME]
           ,[APPOINTMENTENDTIME]
           ,[ASSIGNEDTO]
           ,[CREATEDBY]
           ,[LOGGEDDATE]
           ,[APPOINTMENTNOTES] 
           ,[CUSTOMERNOTES]          
           ,[APPOINTMENTSTATUS]
           ,[DURATION])
     VALUES
           (
            @journalId
           ,@JournalHistoryId
           ,@appointmentStartDate
           ,@appointmentEndDate
           ,@startTime
           ,@endTime
           ,@operativeId    
           ,@userId  
           ,GETDATE()
           ,@appointmentNotes
           ,@jobSheetNotes           
           ,'NotStarted' 
           ,@duration)   
          
      SELECT @AppointmentId = SCOPE_IDENTITY()    
      PRINT 'APPOINTMENTID = '+ CONVERT(VARCHAR,@AppointmentId)            
     
     IF(@AppointmentId>0)
		 BEGIN
		 
		 CREATE TABLE #TMPDURATION( TMP_SID int, TMP_DURATION float )
	
	INSERT	INTO #TMPDURATION (TMP_SID, TMP_DURATION) 
	SELECT	RequiredWorksId, Duration 
	FROM	@voidRequiredWorkDuration 
	
	
	UPDATE	V_RequiredWorks 
	SET		WorksJournalId=@journalId, IsScheduled=1, StatusId=@ArrangedId,Duration=TMP_DURATION,ModifiedDate=GETDATE()
	FROM	#TMPDURATION
	WHERE	V_RequiredWorks.RequiredWorksId = #TMPDURATION.TMP_SID   


	DROP TABLE #TMPDURATION 
		 
		 
		 --Update V_RequiredWorks SET WorksJournalId=@journalId, IsScheduled=1, StatusId=@ArrangedId WHERE RequiredWorksId IN (SELECT COLUMN1 from dbo.SPLIT_STRING(@requiredWorkIds,','))
		 
		 END
     ELSE
		 BEGIN
		  ROLLBACK TRANSACTION;       
		  SET @isSaved = 0          
		 END
	  SELECT @appointmentIdOut = @AppointmentId
       SELECT @journalIdOut =    @journalId
END TRY    
BEGIN CATCH     
 IF @@TRANCOUNT > 0    
 BEGIN         
  ROLLBACK TRANSACTION;       
  SET @isSaved = 0            
 END    
 DECLARE @ErrorMessage NVARCHAR(4000);    
 DECLARE @ErrorSeverity INT;    
 DECLARE @ErrorState INT;    
    
 SELECT @ErrorMessage = ERROR_MESSAGE(),    
 @ErrorSeverity = ERROR_SEVERITY(),    
 @ErrorState = ERROR_STATE();    
    
 -- Use RAISERROR inside the CATCH block to return     
 -- error information about the original error that     
 -- caused execution to jump to the CATCH block.    
 RAISERROR (@ErrorMessage, -- Message text.    
    @ErrorSeverity, -- Severity.    
    @ErrorState -- State.    
   );    
END CATCH;    
    
IF @@TRANCOUNT > 0    
 BEGIN      
  COMMIT TRANSACTION;      
  SET @isSaved = 1    
 END    
  
    
END