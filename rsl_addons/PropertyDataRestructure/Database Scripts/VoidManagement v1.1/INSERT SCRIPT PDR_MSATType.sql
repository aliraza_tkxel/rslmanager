GO
INSERT [dbo].[PDR_MSATType] ([MSATTypeId], [MSATTypeName], [IsActive]) VALUES (5, N'Void Inspection', 1)
GO
INSERT [dbo].[PDR_MSATType] ([MSATTypeId], [MSATTypeName], [IsActive]) VALUES (6, N'Post Void Inspection', 1)
GO
INSERT [dbo].[PDR_MSATType] ([MSATTypeId], [MSATTypeName], [IsActive]) VALUES (7, N'Void Works', 1)
GO
INSERT [dbo].[PDR_MSATType] ([MSATTypeId], [MSATTypeName], [IsActive]) VALUES (8, N'Void Gas Check', 1)
GO
INSERT [dbo].[PDR_MSATType] ([MSATTypeId], [MSATTypeName], [IsActive]) VALUES (9, N'Void Electric Check', 1)
GO
INSERT [dbo].[PDR_MSATType] ([MSATTypeId], [MSATTypeName], [IsActive]) VALUES (10, N'Void Major Works', 1)
GO
