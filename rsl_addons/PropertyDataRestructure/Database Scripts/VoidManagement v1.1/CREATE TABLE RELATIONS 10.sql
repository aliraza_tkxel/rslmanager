/*
   Wednesday, May 20, 201511:52:59 AM
   User: sa
   Server: DEV-PC4
   Database: RSLBHALive
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.V_RequiredWorks
	DROP CONSTRAINT FK_V_RequiredWorks_PDR_JOURNAL
GO
ALTER TABLE dbo.V_RequiredWorks
	DROP CONSTRAINT FK_V_RequiredWorks_PDR_JOURNAL1
GO
ALTER TABLE dbo.PDR_JOURNAL SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.PDR_JOURNAL', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.PDR_JOURNAL', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.PDR_JOURNAL', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_V_RequiredWorks
	(
	RequiredWorksId int NOT NULL IDENTITY (1, 1),
	InspectionJournalId int NULL,
	IsMajorWorksRequired bit NULL,
	RoomId int NULL,
	OtherLocation varchar(100) NULL,
	WorkDescription ntext NULL,
	TenantNeglectEstimation money NULL,
	ComponentId int NULL,
	ReplacementDue date NULL,
	Condition varchar(100) NULL,
	WorksJournalId int NULL,
	IsTenantWorks bit NULL,
	IsBrsWorks bit NULL,
	StatusId int NULL,
	IsScheduled bit NULL,
	CreatedDate datetime NULL,
	ModifiedDate datetime NULL
	)  ON [PRIMARY]
	 TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_V_RequiredWorks SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_V_RequiredWorks ON
GO
IF EXISTS(SELECT * FROM dbo.V_RequiredWorks)
	 EXEC('INSERT INTO dbo.Tmp_V_RequiredWorks (RequiredWorksId, InspectionJournalId, IsMajorWorksRequired, RoomId, WorkDescription, TenantNeglectEstimation, ComponentId, ReplacementDue, Condition, WorksJournalId, IsTenantWorks, IsBrsWorks, StatusId, IsScheduled, CreatedDate, ModifiedDate)
		SELECT RequiredWorksId, InspectionJournalId, IsMajorWorksRequired, RoomId, WorkDescription, TenantNeglectEstimation, ComponentId, ReplacementDue, Condition, WorksJournalId, IsTenantWorks, IsBrsWorks, StatusId, IsScheduled, CreatedDate, ModifiedDate FROM dbo.V_RequiredWorks WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_V_RequiredWorks OFF
GO
ALTER TABLE dbo.V_PauseWorks
	DROP CONSTRAINT FK_V_PauseWorks_V_RequiredWorks
GO
DROP TABLE dbo.V_RequiredWorks
GO
EXECUTE sp_rename N'dbo.Tmp_V_RequiredWorks', N'V_RequiredWorks', 'OBJECT' 
GO
ALTER TABLE dbo.V_RequiredWorks ADD CONSTRAINT
	PK_V_RequiredWorks PRIMARY KEY CLUSTERED 
	(
	RequiredWorksId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.V_RequiredWorks ADD CONSTRAINT
	FK_V_RequiredWorks_PDR_JOURNAL FOREIGN KEY
	(
	InspectionJournalId
	) REFERENCES dbo.PDR_JOURNAL
	(
	JOURNALID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.V_RequiredWorks ADD CONSTRAINT
	FK_V_RequiredWorks_PDR_JOURNAL1 FOREIGN KEY
	(
	WorksJournalId
	) REFERENCES dbo.PDR_JOURNAL
	(
	JOURNALID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.V_RequiredWorks', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.V_RequiredWorks', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.V_RequiredWorks', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.V_PauseWorks ADD CONSTRAINT
	FK_V_PauseWorks_V_RequiredWorks FOREIGN KEY
	(
	RequiredWorksId
	) REFERENCES dbo.V_RequiredWorks
	(
	RequiredWorksId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.V_PauseWorks SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.V_PauseWorks', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.V_PauseWorks', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.V_PauseWorks', 'Object', 'CONTROL') as Contr_Per 