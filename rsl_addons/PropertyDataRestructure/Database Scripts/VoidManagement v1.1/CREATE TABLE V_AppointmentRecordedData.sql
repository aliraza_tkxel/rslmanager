USE [RSLBHALive]
GO

/****** Object:  Table [dbo].[V_AppointmentRecordedData]    Script Date: 06/15/2015 14:43:07 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[V_AppointmentRecordedData](
	[AppointmentRecordedDataId] [int] IDENTITY(1,1) NOT NULL,
	[InspectionJournalId] [int] NOT NULL,
	[IsWorksRequired] [bit] NOT NULL,
	[IsGasCheckRequired] [bit] NOT NULL,
	[IsElectricCheckRequired] [bit] NOT NULL,
	[IsEpcCheckRequired] [bit] NOT NULL,
	[IsAbestosCheckRequired] [bit] NOT NULL,
	[IsMajorWorksRequired] [bit] NOT NULL,
	[IsPaintPackAssistance] [bit] NOT NULL,
	[ReletDate] [datetime] NULL,
	[ElectricCheckJournalId] [int] NULL,
	[GasCheckJournalId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
 CONSTRAINT [PK_V_APPOINTMENT_RECORDED_DATA] PRIMARY KEY CLUSTERED 
(
	[AppointmentRecordedDataId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[V_AppointmentRecordedData]  WITH CHECK ADD  CONSTRAINT [FK_V_AppointmentRecordedData_PDR_JOURNAL] FOREIGN KEY([InspectionJournalId])
REFERENCES [dbo].[PDR_JOURNAL] ([JOURNALID])
GO

ALTER TABLE [dbo].[V_AppointmentRecordedData] CHECK CONSTRAINT [FK_V_AppointmentRecordedData_PDR_JOURNAL]
GO

ALTER TABLE [dbo].[V_AppointmentRecordedData]  WITH CHECK ADD  CONSTRAINT [FK_V_AppointmentRecordedData_PDR_JOURNAL1] FOREIGN KEY([GasCheckJournalId])
REFERENCES [dbo].[PDR_JOURNAL] ([JOURNALID])
GO

ALTER TABLE [dbo].[V_AppointmentRecordedData] CHECK CONSTRAINT [FK_V_AppointmentRecordedData_PDR_JOURNAL1]
GO


