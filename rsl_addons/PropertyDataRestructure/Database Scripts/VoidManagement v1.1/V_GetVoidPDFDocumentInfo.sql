
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:           Salman Nazir
-- Create date:      03/07/2015
-- Description:      Get Void PDF Document Info
-- History:          
-- =============================================
ALTER PROCEDURE V_GetVoidPDFDocumentInfo
@britishGasId int
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
--SET NOCOUNT ON;

-- Insert statements for procedure here
SELECT
	P.ADDRESS1 + ISNULL(' ' + P.ADDRESS2, '') AS [Address],
	P.POSTCODE,
	C.FIRSTNAME + ' ' + C.LASTNAME AS Customer,
	B.TenantFwAddress1 + ISNULL(' ' + B.TenantFwAddress2, '') + ISNULL(' ' + B.TenantFwCity, '') AS TenantAddress,
	CONVERT(NVARCHAR(50), B.DateOccupancyCease, 103)as DateOccupancyCease,
	B.GasMeterReading,
	CONVERT(NVARCHAR(50), B.GasMeterReadingDate, 103)as GasMeterReadingDate,
	B.ElectricMeterReading,
	CONVERT(NVARCHAR(50), B.ElectricMeterReadingDate, 103)as ElectricMeterReadingDate,
	B.GasDebtAmount,
	CONVERT(NVARCHAR(50), B.GasDebtAmountDate, 103)as GasDebtAmountDate,
	B.ElectricDebtAmount,
	CONVERT(NVARCHAR(50), B.ElectricDebtAmountDate, 103)as ElectricDebtAmountDate,
	B.NewTenantName,
	B.NewTenantPreviousAddress1 + ISNULL(' ' + B.NewTenantPreviousAddress2, '') + ISNULL(' ' + B.NewTenantPreviousTownCity, '') AS PreviousAddress,
	CONVERT(NVARCHAR(50), B.NewTenantOccupancyDate, 103)as   DateOccupancy,
	B.NewGasMeterReading AS NewGasMeterReading,
	B.NewElectricMeterReading,
	CONVERT(VARCHAR, B.NewTenantDateOfBirth) + ISNULL(' ' + B.NewTenantTel, '') + ISNULL(' ' + B.NewTenantMobile, '') AS OtherInfo,
	PV.ValueDetail AS GasMeterType,
	PVE.ValueDetail AS ElectricMeterType,
	B.isNotificationSent,
	B.StageId
FROM V_BritishGasVoidNotification B
INNER JOIN P__PROPERTY P
	ON P.PropertyId = B.PropertyId
INNER JOIN C__Customer C
	ON C.CustomerId = B.CustomerId
INNER JOIN C_Tenancy T
	ON T.TenancyId = B.TenancyId
LEFT JOIN PA_PARAMETER_VALUE PV
	ON B.GasMeterTypeId = PV.ValueID
LEFT JOIN PA_PARAMETER_VALUE PVE
	ON B.ElectricMeterTypeId = PVE.ValueID

WHERE B.BritishGasVoidNotificationId = @britishGasId
END

GO