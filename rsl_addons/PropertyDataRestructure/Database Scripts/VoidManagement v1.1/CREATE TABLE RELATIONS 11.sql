/*
   Wednesday, May 20, 201511:58:51 AM
   User: sa
   Server: DEV-PC4
   Database: RSLBHALive
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.V_PaintPack
	DROP CONSTRAINT FK_V_PaintPack_V_AppointmentRecordedData
GO
ALTER TABLE dbo.V_AppointmentRecordedData SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.V_AppointmentRecordedData', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.V_AppointmentRecordedData', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.V_AppointmentRecordedData', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.V_PaintPack
	DROP COLUMN AppointmentRecordedDataId
GO
ALTER TABLE dbo.V_PaintPack SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.V_PaintPack', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.V_PaintPack', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.V_PaintPack', 'Object', 'CONTROL') as Contr_Per 