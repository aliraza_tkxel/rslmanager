USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[V_GetChecksToBeArranged]    Script Date: 06/03/2015 14:36:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
/* =================================================================================    
    Page Description: Get Pending Termination for  Void Dashboard 
    Author: Ali Raza
    Creation Date: June-18-2015

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         June-18-2015      Ali Raza         Get Pending Termination for  Void Dashboard 
  =================================================================================*/
--  DECLARE	@totalCount int
--EXEC	 [dbo].[V_GetPendingTermination]
--		@searchText = NULL,
--		@checksRequired=1
--		@pageSize = 100,
--		@pageNumber = 1,
--		@totalCount = @totalCount OUTPUT
--SELECT	@totalCount as N'@totalCount'
-- =============================================
ALTER PROCEDURE [dbo].[V_GetPendingTermination]
-- Add the parameters for the stored procedure here
		@searchText VARCHAR(200)='',
		
	--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'Ref', 
		@sortOrder varchar (5) = 'DESC',
		@getOnlyCount bit=0,
		@totalCount int = 0 output	
AS
BEGIN
	DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(5000),
        
        --variables for paging
        @offset int,
		@limit int,
		@checksRequiredType varchar(200),
		@MSATTypeId int,
		@ArrangedStatusId int,
		@ToBeArrangedStatusId INT
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		
		SELECT
			@ToBeArrangedStatusId = PDR_STATUS.STATUSID
		FROM PDR_STATUS
		WHERE TITLE = 'To be Arranged'

		--=====================Search Criteria===============================
		SET @searchCriteria = ' 1=1 AND P.SUBSTATUS=22 AND  ITEMNATUREID = 27 AND CURRENTITEMSTATUSID = 13  '
		
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND (ISNULL(P.HouseNumber, '''')	+ ISNULL('' ''+P.ADDRESS1, '''') 	+ ISNULL('', ''+P.ADDRESS2, '''') LIKE ''%' + @searchText + '%'')'
		END	
		
		
		--=======================Select Clause=============================================
		SET @SelectClause = 'Select DISTINCT top ('+convert(varchar(10),@limit)+')
							 J.CUSTOMERID
	,ISNULL(P.HouseNumber, '''')	+ ISNULL('' ''+P.ADDRESS1, '''') 	+ ISNULL('', ''+P.ADDRESS2, '''') AS Address
		,	P.TOWNCITY,P.COUNTY,ISNULL(P.POSTCODE, '''') AS Postcode, Convert(Varchar(50), T.TERMINATIONDATE,103) as Termination,
		Case When M.ReletDate IS NULL Then CONVERT(nvarchar(50),DATEADD(day,7,T.TERMINATIONDATE), 103)
		ELSE Convert(Varchar(50), M.ReletDate,103)END as Relet,P.PROPERTYID As Ref,J.TENANCYID,ISNULL(ST.DESCRIPTION,''-'') AS Status
,ISNULL(SUB.DESCRIPTION,''-'') as SubStatus,ISNULL(S.SCHEMENAME,''-'') as Scheme,ISNULL(B.BLOCKNAME,''-'') AS Block'
			
		
		
		--============================From Clause============================================
		SET @fromClause = CHAR(10) +'from  P__PROPERTY P
	INNER JOIN C_JOURNAL J ON P.PROPERTYID=J.PROPERTYID	
	INNER JOIN C_TERMINATION T ON J.JOURNALID=T.JOURNALID
	INNER JOIN C__CUSTOMER C ON J.CUSTOMERID = C.CUSTOMERID
	INNER JOIN C_TENANCY CT ON J.TENANCYID=CT.TENANCYID
	INNER JOIN C_CUSTOMERTENANCY on C.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID and CT.TENANCYID=C_CUSTOMERTENANCY.TENANCYID
	LEFT JOIN P_BLOCK B ON P.BLOCKID=B.BLOCKID
	LEFT JOIN P_SCHEME S ON P.SCHEMEID=S.SCHEMEID
	INNER JOIN P_STATUS ST ON P.STATUS = ST.STATUSID
	LEFT JOIN P_SUBSTATUS SUB ON P.SUBSTATUS = SUB.SUBSTATUSID
	INNER JOIN PDR_MSAT M ON P.PropertyId =M.PropertyId AND C.CUSTOMERID= M.CustomerId AND CT.TENANCYID=M.TenancyId
	INNER JOIN PDR_JOURNAL VJ ON M.MSATID= VJ.MSATID AND VJ.STATUSID= '+convert(varchar(10),@ToBeArrangedStatusId)
	
		
							
		--============================Order Clause==========================================
		IF(@sortColumn = 'Ref')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Ref' 	
			
		END
		
		IF(@sortColumn = 'Address')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Address' 	
			
		END
		
		
		IF(@sortColumn = 'Termination')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Termination' 	
			
		END
		
		IF(@sortColumn = 'Relet')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Relet' 	
			
		END
				
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		--===============================Main Query ====================================
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
		IF(@getOnlyCount=0)
		BEGIN
			print(@finalQuery)
			EXEC (@finalQuery)
		END
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(DISTINCT P.PropertyID) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
END
