/*
   Friday, May 8, 20154:02:42 PM
   User: sa
   Server: DEV-PC4
   Database: RSLBHALive
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.PDR_MSATType SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.PDR_MSATType', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.PDR_MSATType', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.PDR_MSATType', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.PDR_APPOINTMENTS SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.PDR_APPOINTMENTS', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.PDR_APPOINTMENTS', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.PDR_APPOINTMENTS', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.V_NoEntry ADD CONSTRAINT
	FK_V_NoEntry_PDR_APPOINTMENTS FOREIGN KEY
	(
	AppointmentId
	) REFERENCES dbo.PDR_APPOINTMENTS
	(
	APPOINTMENTID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.V_NoEntry ADD CONSTRAINT
	FK_V_NoEntry_PDR_MSATType FOREIGN KEY
	(
	MsatTypeId
	) REFERENCES dbo.PDR_MSATType
	(
	MSATTypeId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.V_NoEntry SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.V_NoEntry', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.V_NoEntry', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.V_NoEntry', 'Object', 'CONTROL') as Contr_Per 