USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[V_GetPaintPacksList]    Script Date: 11/12/2015 11:51:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
/* =================================================================================    
    Page Description: Get Paint Packs List
    Author: Ali Raza
    Creation Date: June-04-2015 

    Change History:

    Version      Date             By                      Description
    =======     ============    ========			===========================
    v1.0        June-04-2015      Ali Raza		   Get Paint Packs List
  =================================================================================*/
--  DECLARE	@totalCount int
--EXEC	 [dbo].[V_GetPaintPacksList]

--		@pageSize = 100,
--		@pageNumber = 1,
--		@totalCount = @totalCount OUTPUT
--SELECT	@totalCount as N'@totalCount'
-- =============================================
ALTER PROCEDURE [dbo].[V_GetPaintPacksList]

	--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'JournalId', 
		@sortOrder varchar (5) = 'DESC',
		@totalCount int = 0 output	
AS
BEGIN
	DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(2000),
        
        --variables for paging
        @offset int,
		@limit int,
		@MSATTypeId int,
		@ToBeArrangedStatusId int
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		
		SELECT 	@MSATTypeId = MSATTypeId  FROM PDR_MSATType WHERE MSATTypeName = 'Void Inspection'
		
		--=====================Search Criteria===============================
		SET @searchCriteria = ' PDR_MSAT.MSATTypeId= '+convert(varchar(10),@MSATTypeId)+' '
		
		
		
		--=======================Select Clause=============================================
		SET @SelectClause = 'Select DISTINCT top ('+convert(varchar(10),@limit)+')
							 J.JournalId as JournalId 
		,ISNULL(P__PROPERTY.HouseNumber, '''')	+ ISNULL('' ''+P__PROPERTY.ADDRESS1, '''') 	+ ISNULL('', ''+P__PROPERTY.ADDRESS2, '''') AS Address
		
		,ISNULL(BN.NewTenantName,''N/A'') as Tenant
		,CONVERT(nvarchar(50),PDR_MSAT.TerminationDate, 103) as Termination	
		,CONVERT(nvarchar(50),PDR_MSAT.ReletDate, 103) as Relet		
		,P__PROPERTY.PropertyId as PropertyId
		,ISNULL(BN.NewTenantTel,''N/A'') as Telephone
		,PDR_MSAT.TENANCYID as TenancyId,PS.Title as [Status]
		,PPD.PaintPacks  as PaintPacks,PPD.PaintPackId '
		
		
		--============================From Clause============================================
		SET @fromClause = CHAR(10) +'FROM PDR_JOURNAL J
		INNER JOIN V_PaintPack P ON J.JOURNALID=P.InspectionJournalId
		INNER JOIN V_PaintStatus PS ON P.StatusId=PS.StatusId AND (PS.Title<> ''Complete'' OR P.StatusId IS NULL)
		CROSS APPLY ( Select Count(PaintPackId) as PaintPacks,PaintPackId  from V_PaintPackDetails Where V_PaintPackDetails.PaintPackId=P.PaintPackId
		GROUP BY PaintPackId) AS PPD
		INNER JOIN V_PaintPackDetails ON PPD.PaintPackId = V_PaintPackDetails.PaintPackId 	 		
		INNER JOIN	PDR_MSAT ON J.MSATID = PDR_MSAT.MSATId
		INNER JOIN	PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId		
		INNER JOIN	P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID		
		Left JOIN V_BritishGasVoidNotification BN ON P__PROPERTY.PROPERTYID = BN.PropertyId
		 AND PDR_MSAT.TenancyId = BN.TenancyId AND PDR_MSAT.CustomerId=BN.CustomerId
		'
							
		--============================Order Clause==========================================
		IF(@sortColumn = 'Ref')
		BEGIN
			SET @sortColumn = CHAR(10)+ ' PDR_JOURNAL.JOURNALID' 	
			
		END
		
		IF(@sortColumn = 'Address')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Address' 	
			
		END
		IF(@sortColumn = 'Tenant')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Tenant' 	
			
		END
		
		IF(@sortColumn = 'Telephone')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Telephone' 	
			
		END
		
		
				
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		--===============================Main Query ====================================
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
		print(@finalQuery)
		EXEC (@finalQuery)
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(DISTINCT JournalId) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
END
