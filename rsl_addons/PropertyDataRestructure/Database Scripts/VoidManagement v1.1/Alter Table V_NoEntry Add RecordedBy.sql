/*
   Thursday, June 25, 201512:39:44 PM
   User: sa
   Server: dev-pc4
   Database: RSLBHALive
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.V_NoEntry ADD
	RecordedBy int NULL,
	IsNoEntryScheduled bit
GO
ALTER TABLE dbo.V_NoEntry SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.V_NoEntry', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.V_NoEntry', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.V_NoEntry', 'Object', 'CONTROL') as Contr_Per 