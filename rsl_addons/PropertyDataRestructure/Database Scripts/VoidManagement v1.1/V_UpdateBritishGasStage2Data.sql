USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[V_UpdateBritishGasData]    Script Date: 7/2/2015 11:12:26 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
  Name:     [V_UpdateBritishGasStage2Data]

  Author: Noor Muhammad
  Creation Date: July-01-2015
  Description: Update British Gas Data For Stage2
  History:  01/07/2015 Noor : Query for update of british gas  stage 2 data
            30/09/2014 Name : 
  Execution Command:
  
    
-- ============================================= */   
ALTER PROCEDURE [dbo].[V_UpdateBritishGasStage2Data]    
 -- Add the parameters for the stored procedure here    
 @britishGasId int 
,@electricDebtAmount money = null 
,@electricDebtAmountDate date = null
,@electricMeterReading bigint = null
,@electricMeterReadingDate date = null
,@electricMeterTypeId int = null
,@gasDebtAmount money = null
,@gasDebtAmountDate date = null
,@gasMeterReading bigint = null
,@gasMeterReadingDate date = null
,@gasMeterTypeId int = null
,@britishGasStageId int = null
,@isNotificationSent bit=0
,@userId int = null
,@isSaved bit = 0 out    
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
                 
BEGIN TRANSACTION;    
BEGIN TRY     
              
     IF @britishGasStageId =1
		 BEGIN 
			Set SELECT @britishGasStageId=StageId FROM V_Stage WHERE V_Stage.Name = 'Stage 1'
		 END
     ELSE 
		 BEGIN 
			Set SELECT @britishGasStageId=StageId FROM V_Stage WHERE V_Stage.Name = 'Stage 2'
		 END         
                  
   UPDATE [dbo].[V_BritishGasVoidNotification]
   SET 
       [GasMeterTypeId] = @gasMeterTypeId
      ,[GasMeterReading] = @gasMeterReading
      ,[GasMeterReadingDate] = @gasMeterReadingDate
      ,[ElectricMeterTypeId] = @electricMeterTypeId
      ,[ElectricMeterReading] = @electricMeterReading
      ,[ElectricMeterReadingDate] = @electricMeterReadingDate
      ,[GasDebtAmount] = @gasDebtAmount
      ,[GasDebtAmountDate] = @gasDebtAmountDate
      ,[ElectricDebtAmount] = @electricDebtAmount
      ,[ElectricDebtAmountDate] = @electricDebtAmountDate      
      ,[ModifiedDate] = CURRENT_TIMESTAMP
	  ,[UserId] = @userId
	  ,StageId = @britishGasStageId
	  ,[isNotificationSent] = @isNotificationSent      
 WHERE 	   
	   BritishGasVoidNotificationId = @britishGasId   
	
			   	  
END TRY    
BEGIN CATCH     
 IF @@TRANCOUNT > 0    
 BEGIN         
  ROLLBACK TRANSACTION;       
  SET @isSaved = 0            
 END    
 DECLARE @ErrorMessage NVARCHAR(4000);    
 DECLARE @ErrorSeverity INT;    
 DECLARE @ErrorState INT;    
    
 SELECT @ErrorMessage = ERROR_MESSAGE(),    
 @ErrorSeverity = ERROR_SEVERITY(),    
 @ErrorState = ERROR_STATE();    
    
 -- Use RAISERROR inside the CATCH block to return     
 -- error information about the original error that     
 -- caused execution to jump to the CATCH block.    
 RAISERROR (@ErrorMessage, -- Message text.    
    @ErrorSeverity, -- Severity.    
    @ErrorState -- State.    
   );    
END CATCH;    
    
IF @@TRANCOUNT > 0    
 BEGIN      
  COMMIT TRANSACTION;      
  SET @isSaved = 1    
 END    
  
    
END