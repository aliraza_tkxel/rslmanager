USE [RSLBHALive]
GO
-- =============================================
-- Author:           Ali Raza
-- Create date:      07/09/2015
-- Description:      New Tables for Void Management
-- =============================================
	BEGIN TRANSACTION
	BEGIN TRY 


/****** Object:  Table [dbo].[V_AppointmentRecordedData]    Script Date: 09/07/2015 12:00:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[V_AppointmentRecordedData](
	[AppointmentRecordedDataId] [int] IDENTITY(1,1) NOT NULL,
	[InspectionJournalId] [int] NOT NULL,
	[IsWorksRequired] [bit] NOT NULL,
	[IsGasCheckRequired] [bit] NOT NULL,
	[IsElectricCheckRequired] [bit] NOT NULL,
	[IsEpcCheckRequired] [bit] NOT NULL,
	[IsAbestosCheckRequired] [bit] NOT NULL,
	[IsMajorWorksRequired] [bit] NOT NULL,
	[IsPaintPackAssistance] [bit] NOT NULL,
	[ReletDate] [datetime] NULL,
	[ElectricCheckJournalId] [int] NULL,
	[GasCheckJournalId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[WorkDescription] [varchar](500) NULL,
	[VoidWorksNotes] [varchar](500) NULL,
 CONSTRAINT [PK_V_APPOINTMENT_RECORDED_DATA] PRIMARY KEY CLUSTERED 
(
	[AppointmentRecordedDataId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[V_PaintPack]    Script Date: 09/07/2015 12:00:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[V_PaintPack](
	[PaintPackId] [int] IDENTITY(1,1) NOT NULL,
	[InspectionJournalId] [int] NULL,
	[StatusId] [int] NULL,
	[SupplierId] [int] NULL,
	[DeliveryDueDate] [datetime] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[CreatedBy] [int] NULL,
 CONSTRAINT [PK_V_PaintPack] PRIMARY KEY CLUSTERED 
(
	[PaintPackId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[V_PaintPackDetails]    Script Date: 09/07/2015 12:00:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[V_PaintPackDetails](
	[PaintPackDetailId] [int] IDENTITY(1,1) NOT NULL,
	[PaintPackId] [int] NULL,
	[RoomId] [int] NULL,
	[PaintRef] [varchar](50) NULL,
	[PaintColor] [varchar](50) NULL,
	[PaintName] [varchar](50) NULL,
 CONSTRAINT [PK_V_PaintPackDetails] PRIMARY KEY CLUSTERED 
(
	[PaintPackDetailId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[V_Images]    Script Date: 09/07/2015 12:00:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[V_Images](
	[ImageId] [int] IDENTITY(1,1) NOT NULL,
	[JournalId] [int] NULL,
	[CreatedOn] [smalldatetime] NULL,
	[CreatedBy] [int] NULL,
	[SchemeId] [int] NULL,
	[BlockId] [int] NULL,
	[PropertyId] [varchar](50) NULL,
	[ImageName] [nvarchar](100) NULL,
	[IsAfterWorkImage] [bit] NULL,
	[RequiredWorksId] [int] NULL,
 CONSTRAINT [PK_V_Images] PRIMARY KEY CLUSTERED 
(
	[ImageId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[V_RequiredWorks]    Script Date: 09/07/2015 12:00:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[V_RequiredWorks](
	[RequiredWorksId] [int] IDENTITY(1,1) NOT NULL,
	[InspectionJournalId] [int] NULL,
	[IsMajorWorksRequired] [bit] NULL,
	[RoomId] [int] NULL,
	[OtherLocation] [varchar](100) NULL,
	[WorkDescription] [ntext] NULL,
	[VoidWorksNotes] [ntext] NULL,
	[TenantNeglectEstimation] [money] NULL,
	[ComponentId] [int] NULL,
	[ReplacementDue] [date] NULL,
	[Condition] [varchar](100) NULL,
	[WorksJournalId] [int] NULL,
	[IsTenantWorks] [bit] NULL,
	[IsBrsWorks] [bit] NULL,
	[StatusId] [int] NULL,
	[IsScheduled] [bit] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[IsCanceled] [bit] NULL,
	[Duration] [float] NULL,
	[IsVerified] [bit] NULL,
 CONSTRAINT [PK_V_RequiredWorks] PRIMARY KEY CLUSTERED 
(
	[RequiredWorksId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[V_PauseWorks]    Script Date: 09/07/2015 12:00:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[V_PauseWorks](
	[PauseWorksId] [int] IDENTITY(1,1) NOT NULL,
	[RequiredWorksId] [int] NULL,
	[PausedOn] [smalldatetime] NULL,
	[Notes] [nvarchar](100) NULL,
	[Reason] [nvarchar](50) NULL,
	[PausedBy] [int] NULL,
 CONSTRAINT [PK_V_PauseWorks] PRIMARY KEY CLUSTERED 
(
	[PauseWorksId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[V_VoidWorksRepair]    Script Date: 09/07/2015 12:00:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[V_VoidWorksRepair](
	[VoidWorksRepairId] [int] IDENTITY(1,1) NOT NULL,
	[WorksJournalId] [int] NULL,
	[RepairNotes] [ntext] NULL,
	[RepairId] [int] NULL,
	[FollowOnNotes] [ntext] NULL,
	[IsFollowOnWorksRequired] [bit] NULL,
	[CreatedDate] [smalldatetime] NULL,
	[CreatedBy] [int] NULL,
 CONSTRAINT [PK_V_VoidWorksRepair] PRIMARY KEY CLUSTERED 
(
	[VoidWorksRepairId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[V_Stage]    Script Date: 09/07/2015 12:00:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[V_Stage](
	[StageId] [int] NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_V_Stage] PRIMARY KEY CLUSTERED 
(
	[StageId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[V_RoomList]    Script Date: 09/07/2015 12:00:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[V_RoomList](
	[RoomId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Description] [varchar](100) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_V_RoomList] PRIMARY KEY CLUSTERED 
(
	[RoomId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[V_PaintStatus]    Script Date: 09/07/2015 12:00:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[V_PaintStatus](
	[StatusId] [int] NOT NULL,
	[Title] [nvarchar](200) NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_V_PaintStatus] PRIMARY KEY CLUSTERED 
(
	[StatusId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[V_GeneralSettings]    Script Date: 09/07/2015 12:00:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[V_GeneralSettings](
	[Id] [int] NOT NULL,
	[Key] [text] NULL,
	[Value] [text] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_V_GeneralSettings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[V_NoEntry]    Script Date: 09/07/2015 12:00:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[V_NoEntry](
	[NoEntryId] [int] IDENTITY(1,1) NOT NULL,
	[AppointmentId] [int] NULL,
	[RecordedOn] [datetime] NULL,
	[Notes] [ntext] NULL,
	[MsatTypeId] [int] NULL,
	[RecordedBy] [int] NULL,
	[IsNoEntryScheduled] [bit] NULL,
 CONSTRAINT [PK_V_NoEntry] PRIMARY KEY CLUSTERED 
(
	[NoEntryId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[V_BritishGasVoidNotification]    Script Date: 09/07/2015 12:00:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[V_BritishGasVoidNotification](
	[BritishGasVoidNotificationId] [int] IDENTITY(1,1) NOT NULL,
	[CustomerId] [int] NULL,
	[PropertyId] [nvarchar](20) NULL,
	[TenancyId] [int] NULL,
	[IsGasCheck] [bit] NULL,
	[IsElectricCheck] [bit] NULL,
	[GasCheckJournalId] [int] NULL,
	[ElectricCheckJournalId] [int] NULL,
	[TenantFwAddress1] [varchar](50) NULL,
	[TenantFwAddress2] [varchar](50) NULL,
	[TenantFwCity] [varchar](50) NULL,
	[TenantFwPostCode] [varchar](20) NULL,
	[DateOccupancyCease] [date] NULL,
	[BritishGasEmail] [varchar](100) NULL,
	[GasMeterTypeId] [int] NULL,
	[GasMeterReading] [bigint] NULL,
	[GasMeterReadingDate] [date] NULL,
	[ElectricMeterTypeId] [int] NULL,
	[ElectricMeterReading] [bigint] NULL,
	[ElectricMeterReadingDate] [date] NULL,
	[GasDebtAmount] [money] NULL,
	[GasDebtAmountDate] [date] NULL,
	[ElectricDebtAmount] [money] NULL,
	[ElectricDebtAmountDate] [date] NULL,
	[NewTenantName] [varchar](50) NULL,
	[NewTenantDateOfBirth] [date] NULL,
	[NewTenantTel] [varchar](30) NULL,
	[NewTenantMobile] [varchar](30) NULL,
	[NewTenantOccupancyDate] [date] NULL,
	[NewTenantPreviousAddress1] [varchar](50) NULL,
	[NewTenantPreviousAddress2] [varchar](50) NULL,
	[NewTenantPreviousTownCity] [varchar](50) NULL,
	[NewTenantPreviousPostCode] [varchar](20) NULL,
	[NewGasMeterReading] [bigint] NULL,
	[NewGasMeterReadingDate] [date] NULL,
	[NewElectricMeterReading] [bigint] NULL,
	[NewElectricMeterReadingDate] [date] NULL,
	[StatusId] [int] NULL,
	[StageId] [int] NULL,
	[DocumentName] [varchar](250) NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[UserId] [int] NULL,
	[TenantType] [nvarchar](50) NULL,
	[isNotificationSent] [bit] NULL,
 CONSTRAINT [PK_V_BritishGasVoidNotification] PRIMARY KEY CLUSTERED 
(
	[BritishGasVoidNotificationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Default [DF__V_British__isNot__2542D836]    Script Date: 09/07/2015 12:00:34 ******/
ALTER TABLE [dbo].[V_BritishGasVoidNotification] ADD  DEFAULT ((0)) FOR [isNotificationSent]
GO
/****** Object:  Default [DF__V_Require__IsVer__41DF16E4]    Script Date: 09/07/2015 12:00:34 ******/
ALTER TABLE [dbo].[V_RequiredWorks] ADD  DEFAULT ((0)) FOR [IsVerified]
GO
/****** Object:  ForeignKey [FK_V_AppointmentRecordedData_PDR_JOURNAL]    Script Date: 09/07/2015 12:00:34 ******/
ALTER TABLE [dbo].[V_AppointmentRecordedData]  WITH CHECK ADD  CONSTRAINT [FK_V_AppointmentRecordedData_PDR_JOURNAL] FOREIGN KEY([InspectionJournalId])
REFERENCES [dbo].[PDR_JOURNAL] ([JOURNALID])
GO
ALTER TABLE [dbo].[V_AppointmentRecordedData] CHECK CONSTRAINT [FK_V_AppointmentRecordedData_PDR_JOURNAL]
GO
/****** Object:  ForeignKey [FK_V_AppointmentRecordedData_PDR_JOURNAL1]    Script Date: 09/07/2015 12:00:34 ******/
ALTER TABLE [dbo].[V_AppointmentRecordedData]  WITH CHECK ADD  CONSTRAINT [FK_V_AppointmentRecordedData_PDR_JOURNAL1] FOREIGN KEY([GasCheckJournalId])
REFERENCES [dbo].[PDR_JOURNAL] ([JOURNALID])
GO
ALTER TABLE [dbo].[V_AppointmentRecordedData] CHECK CONSTRAINT [FK_V_AppointmentRecordedData_PDR_JOURNAL1]
GO
/****** Object:  ForeignKey [FK_V_BritishGasVoidNotification_C__CUSTOMER]    Script Date: 09/07/2015 12:00:34 ******/
ALTER TABLE [dbo].[V_BritishGasVoidNotification]  WITH CHECK ADD  CONSTRAINT [FK_V_BritishGasVoidNotification_C__CUSTOMER] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[C__CUSTOMER] ([CUSTOMERID])
GO
ALTER TABLE [dbo].[V_BritishGasVoidNotification] CHECK CONSTRAINT [FK_V_BritishGasVoidNotification_C__CUSTOMER]
GO
/****** Object:  ForeignKey [FK_V_BritishGasVoidNotification_P__PROPERTY]    Script Date: 09/07/2015 12:00:34 ******/
ALTER TABLE [dbo].[V_BritishGasVoidNotification]  WITH CHECK ADD  CONSTRAINT [FK_V_BritishGasVoidNotification_P__PROPERTY] FOREIGN KEY([PropertyId])
REFERENCES [dbo].[P__PROPERTY] ([PROPERTYID])
GO
ALTER TABLE [dbo].[V_BritishGasVoidNotification] CHECK CONSTRAINT [FK_V_BritishGasVoidNotification_P__PROPERTY]
GO
/****** Object:  ForeignKey [FK_V_BritishGasVoidNotification_PDR_STATUS]    Script Date: 09/07/2015 12:00:34 ******/
ALTER TABLE [dbo].[V_BritishGasVoidNotification]  WITH CHECK ADD  CONSTRAINT [FK_V_BritishGasVoidNotification_PDR_STATUS] FOREIGN KEY([StatusId])
REFERENCES [dbo].[PDR_STATUS] ([STATUSID])
GO
ALTER TABLE [dbo].[V_BritishGasVoidNotification] CHECK CONSTRAINT [FK_V_BritishGasVoidNotification_PDR_STATUS]
GO
/****** Object:  ForeignKey [FK_V_BritishGasVoidNotification_V_Stage]    Script Date: 09/07/2015 12:00:34 ******/
ALTER TABLE [dbo].[V_BritishGasVoidNotification]  WITH CHECK ADD  CONSTRAINT [FK_V_BritishGasVoidNotification_V_Stage] FOREIGN KEY([StageId])
REFERENCES [dbo].[V_Stage] ([StageId])
GO
ALTER TABLE [dbo].[V_BritishGasVoidNotification] CHECK CONSTRAINT [FK_V_BritishGasVoidNotification_V_Stage]
GO
/****** Object:  ForeignKey [FK_V_Images_PDR_JOURNAL]    Script Date: 09/07/2015 12:00:34 ******/
ALTER TABLE [dbo].[V_Images]  WITH CHECK ADD  CONSTRAINT [FK_V_Images_PDR_JOURNAL] FOREIGN KEY([JournalId])
REFERENCES [dbo].[PDR_JOURNAL] ([JOURNALID])
GO
ALTER TABLE [dbo].[V_Images] CHECK CONSTRAINT [FK_V_Images_PDR_JOURNAL]
GO
/****** Object:  ForeignKey [FK_V_NoEntry_PDR_APPOINTMENTS]    Script Date: 09/07/2015 12:00:34 ******/
ALTER TABLE [dbo].[V_NoEntry]  WITH CHECK ADD  CONSTRAINT [FK_V_NoEntry_PDR_APPOINTMENTS] FOREIGN KEY([AppointmentId])
REFERENCES [dbo].[PDR_APPOINTMENTS] ([APPOINTMENTID])
GO
ALTER TABLE [dbo].[V_NoEntry] CHECK CONSTRAINT [FK_V_NoEntry_PDR_APPOINTMENTS]
GO
/****** Object:  ForeignKey [FK_V_NoEntry_PDR_MSATType]    Script Date: 09/07/2015 12:00:34 ******/
ALTER TABLE [dbo].[V_NoEntry]  WITH CHECK ADD  CONSTRAINT [FK_V_NoEntry_PDR_MSATType] FOREIGN KEY([MsatTypeId])
REFERENCES [dbo].[PDR_MSATType] ([MSATTypeId])
GO
ALTER TABLE [dbo].[V_NoEntry] CHECK CONSTRAINT [FK_V_NoEntry_PDR_MSATType]
GO
/****** Object:  ForeignKey [FK_V_PaintPack_PDR_JOURNAL]    Script Date: 09/07/2015 12:00:34 ******/
ALTER TABLE [dbo].[V_PaintPack]  WITH CHECK ADD  CONSTRAINT [FK_V_PaintPack_PDR_JOURNAL] FOREIGN KEY([InspectionJournalId])
REFERENCES [dbo].[PDR_JOURNAL] ([JOURNALID])
GO
ALTER TABLE [dbo].[V_PaintPack] CHECK CONSTRAINT [FK_V_PaintPack_PDR_JOURNAL]
GO
/****** Object:  ForeignKey [FK_V_PaintPackDetails_V_PaintPack]    Script Date: 09/07/2015 12:00:34 ******/
ALTER TABLE [dbo].[V_PaintPackDetails]  WITH CHECK ADD  CONSTRAINT [FK_V_PaintPackDetails_V_PaintPack] FOREIGN KEY([PaintPackId])
REFERENCES [dbo].[V_PaintPack] ([PaintPackId])
GO
ALTER TABLE [dbo].[V_PaintPackDetails] CHECK CONSTRAINT [FK_V_PaintPackDetails_V_PaintPack]
GO
/****** Object:  ForeignKey [FK_V_PauseWorks_V_RequiredWorks]    Script Date: 09/07/2015 12:00:34 ******/
ALTER TABLE [dbo].[V_PauseWorks]  WITH CHECK ADD  CONSTRAINT [FK_V_PauseWorks_V_RequiredWorks] FOREIGN KEY([RequiredWorksId])
REFERENCES [dbo].[V_RequiredWorks] ([RequiredWorksId])
GO
ALTER TABLE [dbo].[V_PauseWorks] CHECK CONSTRAINT [FK_V_PauseWorks_V_RequiredWorks]
GO
/****** Object:  ForeignKey [FK_V_RequiredWorks_PDR_JOURNAL]    Script Date: 09/07/2015 12:00:34 ******/
ALTER TABLE [dbo].[V_RequiredWorks]  WITH CHECK ADD  CONSTRAINT [FK_V_RequiredWorks_PDR_JOURNAL] FOREIGN KEY([InspectionJournalId])
REFERENCES [dbo].[PDR_JOURNAL] ([JOURNALID])
GO
ALTER TABLE [dbo].[V_RequiredWorks] CHECK CONSTRAINT [FK_V_RequiredWorks_PDR_JOURNAL]
GO
/****** Object:  ForeignKey [FK_V_RequiredWorks_PDR_JOURNAL1]    Script Date: 09/07/2015 12:00:34 ******/
ALTER TABLE [dbo].[V_RequiredWorks]  WITH CHECK ADD  CONSTRAINT [FK_V_RequiredWorks_PDR_JOURNAL1] FOREIGN KEY([WorksJournalId])
REFERENCES [dbo].[PDR_JOURNAL] ([JOURNALID])
GO
ALTER TABLE [dbo].[V_RequiredWorks] CHECK CONSTRAINT [FK_V_RequiredWorks_PDR_JOURNAL1]
GO

END TRY 
	BEGIN CATCH 	
		IF @@TRANCOUNT >0	  
		BEGIN	   
			ROLLBACK TRANSACTION;	
		END 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
		@ErrorSeverity, -- Severity.
		@ErrorState -- State.
		);
	END CATCH  
	 
	IF @@TRANCOUNT >0
	BEGIN
		COMMIT TRANSACTION;
	END 
	