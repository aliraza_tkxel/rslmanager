Brief explanation for "BHG BRS Maintenance Systems Voids Management_28 day process" Deployment.

Please get the database backup befor deployment

All scripts related to deployment exist in following svn Path
"BHA.rslManager\rsl_addons\PropertyDataRestructure\Database Scripts\VoidManagement v1.1\Script for Deployment\"
Please execute the script in following order

Step 1 Execute DDL object scripts: 
1). Create Void Tables.sql
2). Alter Tables for Void Management.sql 
3). CREATE TYPE [dbo].[VoidRequiredWorkDuration].sql
4). New Stored Procedures.sql
5). ALTER Stored Procedures for Void Management.sql


Step 2 Execute DML object scripts:
1). Insertion Scripts for Void Management.sql
2). Insert_Meters.sql

Step 3 Build the following Projects:

RSL master site
PropertyDataRestructure
BRSFaultLocator
StockConditionOfflineAPI