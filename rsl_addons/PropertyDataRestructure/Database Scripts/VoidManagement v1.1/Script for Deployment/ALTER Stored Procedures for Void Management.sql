-- =============================================
-- Author:           Ali Raza
-- Create date:      07/09/2015
-- Description:      ALTER Stored Procedures for Void Management
-- =============================================

	
-- Stored Procedure FL_GetAppointmentsCalendarInfo

-- =============================================  
-- EXEC FL_GetAppointmentsCalendarInfo @startDate='31/10/2013',@endDate='30/11/2013' 
-- Author:  Aqib Javed  
-- Create date: <29/01/2013>  
-- Description: <Provided information of Scheduled Appointments of Engineers on Weekly basis>  
-- Webpage : SchedulingCalendar.aspx  

-- =============================================  
ALTER PROCEDURE [dbo].[FL_GetAppointmentsCalendarInfo]
    @startDate VARCHAR(25) ,
    @endDate VARCHAR(25) ,
    @scheme INT = -1
AS 
    DECLARE @InsertClause VARCHAR(1000)

    DECLARE @SelectClause VARCHAR(1000)

    DECLARE @FromClause VARCHAR(800)

    DECLARE @WhereClause VARCHAR(800)

    DECLARE @MainQuery VARCHAR(MAX)

    BEGIN


        WITH    CTE_Appointments ( AppointmentDate, OperativeID, [Time], NAME, MOBILE, CustAddress, POSTCODE, FaultStatus, AppointmentID, EndTime, StartTimeInSec, EndTimeInSec, TimeToOrderAppointments, TenancyId, IsCalendarAppointment, AppointmentType ,AppointmentEndDate)
                  AS ( 

	-- ==========================================================================================    
	--	FAULT APPOINTMENTS
	-- ========================================================================================== 

    SELECT DISTINCT
	CONVERT(VARCHAR(20), APPOINTMENTDATE, 103)									AS AppointmentDate
	,FL_CO_APPOINTMENT.OperativeID												AS OperativeID
	,FL_CO_APPOINTMENT.[Time]													AS [Time]
	,'Test Name'																AS Name
	,'Test Mobile'																AS MOBILE
	,Case 
	When FL_FAULT_LOG.PROPERTYID <> '' THEN
	ISNULL(P__PROPERTY.HouseNumber, '') + ' '
	+ ISNULL(P__PROPERTY.ADDRESS1, '') + ', '
	+ ISNULL(P__PROPERTY.TOWNCITY, '') + ', '
	+ ISNULL(P__PROPERTY.POSTCODE, '')
	WHEN FL_FAULT_LOG.BLOCKID > 0 THEN 
	ISNULL(P_BLOCK.ADDRESS1,'')
	+ ISNULL(', '+P_BLOCK.TOWNCITY, '') 
	+ ISNULL(', '+P_BLOCK.POSTCODE, '') 
	When FL_FAULT_LOG.SCHEMEID > 0 THEN
	ISNULL(P_SCHEME.SCHEMENAME,'')
	+ ISNULL(', '+PDR_DEVELOPMENT.TOWN, '') 
	+ ISNULL(', '+PDR_DEVELOPMENT.COUNTY, '')
	+ ISNULL(', '+PDR_DEVELOPMENT.PostCode, '')
	END																			AS CustAddress
	,'Test PostCode'															AS POSTCODE
	,FL_FAULT_STATUS.[Description]												AS FaultStatus
	,FL_CO_APPOINTMENT.AppointmentID											AS AppointmentID
	,FL_CO_APPOINTMENT.EndTime													AS EndTime
	,DATEDIFF(s, '1970-01-01',
	CONVERT(DATETIME, CONVERT(VARCHAR(10), appointmentdate, 103)
	+ ' ' + [Time], 103))														AS StartTimeInSec
	,DATEDIFF(s, '1970-01-01',
	CONVERT(DATETIME, CONVERT(VARCHAR(10), appointmentdate, 103)
	+ ' ' + EndTime, 103))														AS EndTimeInSec
	,CONVERT(TIME, [TIME])														AS TimeToOrderAppointments
	,C_TENANCY.TENANCYID														AS TenancyId
	,FL_CO_APPOINTMENT.isCalendarAppointment									AS IsCalendarAppointment
	,Case 
	When FL_FAULT_LOG.PROPERTYID IS NULL THEN	'SbFault'	
	Else 'Fault'
	End																AS AppointmentType
	,CONVERT(VARCHAR(20), COALESCE(AppointmentEndDate, AppointmentDate), 103)	AS AppointmentEndDate
FROM
	FL_CO_APPOINTMENT
		INNER JOIN FL_FAULT_APPOINTMENT ON FL_CO_APPOINTMENT.AppointmentID = FL_FAULT_APPOINTMENT.AppointmentId
		INNER JOIN FL_FAULT_LOG ON FL_FAULT_APPOINTMENT.FaultLogId = FL_FAULT_LOG.FaultLogID
		INNER JOIN FL_FAULT_STATUS ON FL_FAULT_LOG.StatusID = FL_FAULT_STATUS.FaultStatusID
		Left JOIN P__PROPERTY ON FL_FAULT_LOG.PROPERTYID = P__PROPERTY.PROPERTYID
		Left JOIN C_TENANCY ON P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID
		LEFT JOIN P_SCHEME ON FL_FAULT_LOG.SCHEMEID = P_SCHEME.SCHEMEID  
		LEFT JOIN P_BLOCK ON FL_FAULT_LOG.BlockId = P_BLOCK.BLOCKID  
		LEFT JOIN PDR_DEVELOPMENT ON P_SCHEME.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID
WHERE
	(C_TENANCY.ENDDATE IS NULL
		OR C_TENANCY.ENDDATE > GETDATE()
	)
	AND FL_CO_APPOINTMENT.APPOINTMENTDATE BETWEEN CONVERT(DATE, @startDate, 103)
	AND
	CONVERT(DATE, @endDate, 103)
	AND FL_FAULT_STATUS.[Description] != 'Cancelled'
	AND (P__PROPERTY.SCHEMEID = @scheme
		OR @scheme = -1
		OR @scheme = 0
	) 
	AND(FL_FAULT_LOG.PROPERTYID <> '' OR FL_FAULT_LOG.SCHEMEID  IS NOT NULL OR FL_FAULT_LOG.BlockId > 0)

	UNION ALL 

	-- ==========================================================================================    
	--	PLANNED APPOINTMENTS
	-- ========================================================================================== 

	SELECT DISTINCT
	CONVERT(VARCHAR(20), PLANNED_APPOINTMENTS.APPOINTMENTDATE, 103)		AS AppointmentDate
	,PLANNED_APPOINTMENTS.ASSIGNEDTO									AS OperativeID
	,PLANNED_APPOINTMENTS.APPOINTMENTSTARTTIME							AS Time
	,'Test Name'														AS Name
	,'Test Mobile'														AS MOBILE
	,ISNULL(P__PROPERTY.HouseNumber, '') + ' '
	+ ISNULL(P__PROPERTY.ADDRESS1, '') + ', '
	+ ISNULL(P__PROPERTY.TOWNCITY, '') + ', '
	+ ISNULL(P__PROPERTY.POSTCODE, '')									AS CustAddress
	,'Test PostCode'													AS POSTCODE
	,PLANNED_STATUS.TITLE												AS FaultStatus
	,PLANNED_APPOINTMENTS.AppointmentID									AS AppointmentID
	,PLANNED_APPOINTMENTS.APPOINTMENTENDTIME							AS EndTime
	,DATEDIFF(s, '1970-01-01',
	CONVERT(DATETIME, CONVERT(VARCHAR(10), PLANNED_APPOINTMENTS.APPOINTMENTDATE, 103)
	+ ' '
	+ PLANNED_APPOINTMENTS.APPOINTMENTSTARTTIME, 103))					AS StartTimeInSec
	,DATEDIFF(s, '1970-01-01',
	CONVERT(DATETIME, CONVERT(VARCHAR(10), PLANNED_APPOINTMENTS.APPOINTMENTDATE, 103)
	+ ' '
	+ PLANNED_APPOINTMENTS.APPOINTMENTENDTIME, 103))					AS EndTimeInSec
	,CONVERT(TIME, PLANNED_APPOINTMENTS.APPOINTMENTSTARTTIME)			AS TimeToOrderAppointments
	,C_TENANCY.TENANCYID												AS TenancyId
	,0																	AS IsCalendarAppointment
	,'Planned'															AS AppointmentType
	,CONVERT(VARCHAR(20), PLANNED_APPOINTMENTS.APPOINTMENTENDDATE, 103)	AS AppointmentEndDate
FROM
	PLANNED_APPOINTMENTS
		INNER JOIN PLANNED_JOURNAL ON PLANNED_APPOINTMENTS.JournalId = PLANNED_JOURNAL.JOURNALID
		INNER JOIN PLANNED_STATUS ON PLANNED_JOURNAL.STATUSID = PLANNED_STATUS.STATUSID
		INNER JOIN P__PROPERTY ON PLANNED_JOURNAL.PROPERTYID = P__PROPERTY.PROPERTYID
		INNER JOIN C_TENANCY ON P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID
WHERE
	(C_TENANCY.ENDDATE IS NULL
		OR C_TENANCY.ENDDATE > GETDATE())
	AND (PLANNED_APPOINTMENTS.APPOINTMENTDATE BETWEEN CONVERT(DATE, @startDate, 103) AND CONVERT(DATE, @endDate, 103)
		OR PLANNED_APPOINTMENTS.APPOINTMENTENDDATE BETWEEN CONVERT(DATE, @startDate, 103) AND CONVERT(DATE, @endDate, 103)
		OR (PLANNED_APPOINTMENTS.APPOINTMENTDATE <= CONVERT(DATE, @startDate, 103)
			AND PLANNED_APPOINTMENTS.APPOINTMENTENDDATE >= CONVERT(DATE, @endDate, 103)))
	AND (P__PROPERTY.SCHEMEID = @scheme
		OR @scheme = -1
		OR @scheme = 0
	)
AND APPOINTMENTSTATUS <> 'Cancelled' AND ISPENDING = 0
	UNION ALL 

	-- ==========================================================================================    
	--	PDR APPOINTMENTS
	-- ==========================================================================================   

	---Get PDR appointment e.g M&E Servicing, Maintenance Servicing
	SELECT DISTINCT 
CONVERT(VARCHAR(20), PDR_APPOINTMENTS.APPOINTMENTSTARTDATE, 103)		AS AppointmentDate
	,PDR_APPOINTMENTS.ASSIGNEDTO										AS OperativeID
	,PDR_APPOINTMENTS.APPOINTMENTSTARTTIME								AS [Time]
	,'Test Name'														AS Name
	,'Test Mobile'														AS MOBILE
	,CASE
    WHEN PDR_MSAT.propertyid IS NOT NULL THEN
		ISNULL(P__PROPERTY.HOUSENUMBER, '') + ' '
		+ ISNULL(P__PROPERTY.ADDRESS1, '') 
		+ ISNULL(', '+P__PROPERTY.TOWNCITY, '') 
		+ ISNULL( ' '+P__PROPERTY.POSTCODE, '')
	WHEN PDR_MSAT.SCHEMEID IS NOT NULL THEN
		ISNULL(P_SCHEME.SCHEMENAME , '') 
		+ ISNULL( ' '+P_SCHEME.SCHEMECODE, '')
	WHEN PDR_MSAT.BLOCKID IS NOT NULL THEN
		ISNULL(P_BLOCK.BLOCKNAME, '') + ' '
		+ ISNULL(P_BLOCK.ADDRESS1 , '') 
		+ ISNULL(', '+P_BLOCK.TOWNCITY, '') 
		+ ISNULL( ' '+P_BLOCK.POSTCODE, '')
	END 							AS CustAddress
	,CASE
    WHEN PDR_MSAT.propertyid IS NOT NULL THEN
		ISNULL(P__PROPERTY.POSTCODE, '')
	WHEN PDR_MSAT.SCHEMEID IS NOT NULL THEN
		ISNULL(P_SCHEME.SCHEMECODE,'') 
	WHEN PDR_MSAT.BLOCKID IS NOT NULL THEN
		ISNULL(P_BLOCK.POSTCODE, '')
	END 		AS POSTCODE
	, CASE
    WHEN PDR_STATUS.TITLE='Arranged' THEN 'Appointment Arranged'
    WHEN PDR_STATUS.TITLE='Completed' THEN 'Complete'
     ELSE  PDR_STATUS.TITLE	END											AS FaultStatus
	,PDR_APPOINTMENTS.APPOINTMENTID										AS AppointmentID
	,PDR_APPOINTMENTS.APPOINTMENTENDTIME								AS EndTime
	,DATEDIFF(s, '1970-01-01',
	CONVERT(DATETIME, CONVERT(VARCHAR(10), PDR_APPOINTMENTS.APPOINTMENTSTARTDATE, 103)
	+ ' '
	+ PDR_APPOINTMENTS.APPOINTMENTSTARTTIME, 103))						AS StartTimeInSec
	,DATEDIFF(s, '1970-01-01',
	CONVERT(DATETIME, CONVERT(VARCHAR(10), PDR_APPOINTMENTS.APPOINTMENTSTARTDATE, 103)
	+ ' '
	+ PDR_APPOINTMENTS.APPOINTMENTENDTIME, 103))						AS EndTimeInSec
	,CONVERT(TIME, PDR_APPOINTMENTS.APPOINTMENTSTARTTIME)			    AS TimeToOrderAppointments
	,C_TENANCY.TENANCYID												AS TenancyId
	,0																	AS IsCalendarAppointment
	,PDR_MSATType.MSATTypeName											AS AppointmentType
	,CONVERT(VARCHAR(20), PDR_APPOINTMENTS.APPOINTMENTENDDATE, 103)	AS AppointmentEndDate

   FROM PDR_APPOINTMENTS
   INNER JOIN PDR_JOURNAL ON PDR_APPOINTMENTS.JOURNALID = PDR_JOURNAL.JOURNALID
   INNER JOIN pdr_status ON PDR_JOURNAL.STATUSID = PDR_STATUS.STATUSID AND PDR_STATUS.TITLE <>'Cancelled'
   INNER JOIN PDR_MSAT ON PDR_MSAT.MSATId = PDR_JOURNAL.MSATID
   LEFT JOIN	P_SCHEME ON PDR_MSAT.SchemeId = P_SCHEME.SCHEMEID 
   LEFT JOIN	P_BLOCK ON PDR_MSAT.BlockId = P_BLOCK.BLOCKID
   LEFT JOIN	P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID
   INNER JOIN PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId
   LEFT JOIN p_status ON p__property.[status] = p_status.statusid


   LEFT JOIN c_tenancy ON p__property.propertyid = c_tenancy.propertyid
   AND (dbo.c_tenancy.enddate IS NULL
        OR dbo.c_tenancy.enddate > Getdate())
   LEFT JOIN c_customer_names_grouped CG ON CG.i = dbo.c_tenancy.tenancyid
   AND CG.id IN
     (SELECT Max(id) ID
      FROM c_customer_names_grouped
      GROUP BY i)
   WHERE PDR_STATUS.TITLE != 'Cancelled' AND pdr_appointments.appointmentstartdate BETWEEN CONVERT(DATE, @startDate, 103) AND CONVERT(DATE, @endDate, 103)
     OR pdr_appointments.appointmentenddate BETWEEN CONVERT(DATE, @startDate, 103) AND CONVERT(DATE, @endDate, 103)
     OR (pdr_appointments.appointmentstartdate <= CONVERT(DATE, @startDate, 103)
         AND pdr_appointments.appointmentenddate >= CONVERT(DATE, @endDate, 103))

)
SELECT
	AppointmentDate
	,OperativeID
	,[Time]
	,NAME
	,MOBILE
	,CustAddress
	,POSTCODE
	,FaultStatus
	,AppointmentID
	,EndTime
	,StartTimeInSec
	,EndTimeInSec
	,TimeToOrderAppointments	TenancyId
	,IsCalendarAppointment
	,AppointmentType
	,AppointmentEndDate
FROM
	CTE_Appointments
ORDER BY	APPOINTMENTDATE
			,TimeToOrderAppointments	ASC

--------------------------------------------------------------------------------------------------------------------------  

	-- ==========================================================================================    
	--	FAULT APPOINTMENTS TEMP TABLE
	-- ========================================================================================== 


--Creating Temp Table for JSN against Appointment  
CREATE TABLE #tblAppointmentFaults(AppointmentId INT,
FaultLogId INT,
JobSheetNumber NVARCHAR(50),
FaultDetail NVARCHAR(1000),
AppointmentType NVARCHAR(50))
SET @InsertClause = 'INSERT INTO #tblAppointmentFaults(JobSheetNumber,AppointmentId,FaultLogId,FaultDetail,AppointmentType)'

SET @SelectClause = 'Select distinct FL_FAULT_LOG.JobSheetNumber,  
						FL_FAULT_APPOINTMENT.AppointmentID,  
						FL_FAULT_APPOINTMENT.FaultLogId
						,FL_FAULT.[Description]
						,Case 
						When FL_FAULT_LOG.PROPERTYID IS NULL And FL_FAULT_LOG.SchemeId IS NOT NULL THEN	''SbFault''	
						Else ''Fault'' END as AppointmentType '

SET @FromClause = 'FROM  FL_CO_APPOINTMENT   
						INNER JOIN FL_FAULT_APPOINTMENT ON FL_CO_APPOINTMENT.AppointmentID =FL_FAULT_APPOINTMENT.AppointmentId   
						INNER JOIN FL_FAULT_LOG  ON FL_FAULT_APPOINTMENT.FaultLogId = FL_FAULT_LOG.FaultLogID  
						INNER JOIN FL_FAULT  ON FL_FAULT_LOG.FaultID  = FL_FAULT.FaultID

						'

SET @WhereClause = ' Where FL_CO_APPOINTMENT.APPOINTMENTDATE BETWEEN CONVERT(date,'''
+ @startDate + ''',103) and Convert(date,''' + @endDate+ ''',103)'
SET @MainQuery = @InsertClause + CHAR(10) + @SelectClause + CHAR(10)
+ @FromClause + CHAR(10) + @WhereClause

--print @InsertClause+Char(10)  
--print @SelectClause+Char(10)  
--print @FromClause+Char(10)  
--print @WhereClause         
PRINT @MainQuery
EXEC (@MainQuery)

PRINT 'done fault'

	-- ==========================================================================================    
	--	PLANNED APPOINTMENTS TEMP TABLE
	-- ========================================================================================== 


SET @SelectClause = 'Select distinct ''JSN'' +RIGHT(''00000''+ CONVERT(VARCHAR,PLANNED_APPOINTMENTS.APPOINTMENTID),5) as JobSheetNumber  
				,PLANNED_APPOINTMENTS.AppointmentID
				,PLANNED_APPOINTMENTS.AppointmentID as FaultLogId
				,PLANNED_APPOINTMENTS.APPOINTMENTNOTES as Description
				,''Planned'' as AppointmentType '

SET @FromClause = 'FROM	PLANNED_APPOINTMENTS '

SET @WhereClause = ' Where 
        PLANNED_APPOINTMENTS.APPOINTMENTDATE BETWEEN CONVERT(date,''' + @startDate + ''',103) and Convert(date,''' + @endDate + ''',103)
        OR PLANNED_APPOINTMENTS.APPOINTMENTENDDATE BETWEEN CONVERT(DATE,''' + @startDate + ''', 103) AND CONVERT(DATE,''' + @endDate + ''', 103)
		OR (PLANNED_APPOINTMENTS.APPOINTMENTDATE <= CONVERT(DATE,''' + @startDate + ''', 103) AND PLANNED_APPOINTMENTS.APPOINTMENTENDDATE >= CONVERT(DATE,''' + @endDate + ''', 103))'
SET @MainQuery = @InsertClause + CHAR(10) + @SelectClause + CHAR(10)
+ @FromClause + CHAR(10) + @WhereClause

--print @InsertClause+Char(10)  
--print @SelectClause+Char(10)  
--print @FromClause+Char(10)  
--print @WhereClause         
PRINT @MainQuery
EXEC (@MainQuery)

	-- ==========================================================================================    
	--	PDR APPOINTMENTS TEMP TABLE
	-- ========================================================================================== 

SET @SelectClause = 'Select distinct case when PDR_MSATType.MSATTypeName Like ''%Void%'' then ''JSV'' +RIGHT(''00000''+ CONVERT(VARCHAR,PDR_JOURNAL.JOURNALID),5) 
 ELSE ''JSN'' +RIGHT(''00000''+ CONVERT(VARCHAR,PDR_APPOINTMENTS.APPOINTMENTID),5) END as JobSheetNumber  
				,PDR_APPOINTMENTS.APPOINTMENTID
				,PDR_APPOINTMENTS.APPOINTMENTID as FaultLogId
				,PDR_APPOINTMENTS.APPOINTMENTNOTES as Description
				,PDR_MSATType.MSATTypeName as AppointmentType '

SET @FromClause = 'FROM	PDR_APPOINTMENTS 
 INNER JOIN PDR_JOURNAL ON PDR_APPOINTMENTS.JOURNALID = PDR_JOURNAL.JOURNALID
   INNER JOIN pdr_status ON PDR_JOURNAL.STATUSID = PDR_STATUS.STATUSID AND PDR_STATUS.TITLE <>''Cancelled''
   INNER JOIN PDR_MSAT ON PDR_MSAT.MSATId = PDR_JOURNAL.MSATID
   LEFT JOIN	P_SCHEME ON PDR_MSAT.SchemeId = P_SCHEME.SCHEMEID 
   LEFT JOIN	P_BLOCK ON PDR_MSAT.BlockId = P_BLOCK.BLOCKID
   LEFT JOIN	P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID
   INNER JOIN PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId

'

SET @WhereClause = ' Where 
        PDR_APPOINTMENTS.appointmentstartdate BETWEEN CONVERT(date,''' + @startDate + ''',103) and Convert(date,''' + @endDate + ''',103)
        OR PDR_APPOINTMENTS.appointmentenddate BETWEEN CONVERT(DATE,''' + @startDate + ''', 103) AND CONVERT(DATE,''' + @endDate + ''', 103)
		OR (PDR_APPOINTMENTS.appointmentstartdate <= CONVERT(DATE,''' + @startDate + ''', 103) AND PDR_APPOINTMENTS.appointmentenddate >= CONVERT(DATE,''' + @endDate + ''', 103))'
SET @MainQuery = @InsertClause + CHAR(10) + @SelectClause + CHAR(10)
+ @FromClause + CHAR(10) + @WhereClause

PRINT @MainQuery
EXEC (@MainQuery)

SELECT
	*
FROM
	#tblAppointmentFaults

DROP TABLE #tblAppointmentFaults

END
GO
	
	
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
/*-- =============================================
-- Author:           Aamir Waheed
-- Create date:      11/06/2015
-- Description:      PropertyCalendar.aspx
-- History:          11/06/2015 Aqib :This Stored Proceedure shows the Appointment details on the Calendar Page
--                   13/07/2015 Ali Raza : Added All void appointments union
-- EXEC AS_ScheduledAppointmentsDetail @startDate='31/12/2013',@endDate='04/01/2013'

Execution Command:
----------------------------------------------------

DECLARE	@return_value int

EXEC	@return_value = [dbo].[FL_GetPropertyCalendarDetails]
						@schemeId = -1
						,@patchId = -1
						,@tradeId = -1
						,@operativeId = -1
						,@jsn = ''
						,@tenant = ''
						,@address = ''
						,@startDate = '20150616'
						,@endDate = '20150616'

SELECT	@return_value as N'@return_value'


*================================================================================= */

ALTER PROCEDURE [dbo].[FL_GetPropertyCalendarDetails]	
	@schemeId INT = -1
	,@patchId INT = -1
	,@tradeId INT = -1
	,@operativeId INT = -1
	,@jsn NVARCHAR(50) = ''
	,@tenant NVARCHAR(200) = ''
	,@address NVARCHAR(200) = ''
	,@startDate DATETIME
	,@endDate DATETIME
AS
BEGIN
	DECLARE @currentDateTime AS DATETIME2 = GETDATE()
	DECLARE @today AS DATE = @currentDateTime
	
	/* ===============================================================
	 * Get operatives for selected trades
	 * =============================================================== */
	
	SET
		@startDate = DATEADD(DAY, DATEDIFF(DAY, 0, @startDate), '00:00:00')
	SET
		@endDate = DATEADD(DAY, DATEDIFF(DAY, 0, @endDate), '23:59:59')
	DECLARE @Operatives AS TABLE (EmployeeID INT, Name VARCHAR(200))
		
	INSERT INTO @Operatives (EmployeeID,Name)
	SELECT
		U.EmployeeId					AS EMPLOYEEID
		,E.FIRSTNAME + ' ' + E.LASTNAME	AS Name		
	FROM
		AS_USER AS U
			INNER JOIN E__EMPLOYEE AS E ON U.EmployeeId = E.EMPLOYEEID
			LEFT JOIN E_TRADE AS T ON E.EMPLOYEEID = T.EmpId
					AND
					T.TradeId = @tradeId
	WHERE
		(@tradeId = -1 OR T.TradeId IS NOT NULL)
		AND (@operativeId = -1 OR U.EmployeeId = @operativeId)

	
	/* ===============================================================
	 * Get The list of operatives for calendar.
	 * =============================================================== */
	
	SELECT
		*
	FROM
		@operatives

	/* ===============================================================
	 * Get Leaves Information for selected operatives
	 * =============================================================== */
	SELECT DISTINCT
		J.EMPLOYEEID										AS EMPLOYEEID
		,COALESCE(N.DESCRIPTION, A.REASON, E_ABSENCEREASON.DESCRIPTION, 'N/A')
		+ CASE
			WHEN DATEPART(HOUR, A.STARTDATE) > 0
				AND DATEPART(HOUR, A.RETURNDATE) > 0
				THEN '<br /> ' + CONVERT(NVARCHAR(5), A.STARTDATE, 108) + ' - '
					+ CONVERT(NVARCHAR(5), A.RETURNDATE, 108)
			ELSE ''
		END													AS REASON
		,CONVERT(DATE, A.STARTDATE)							AS StartDate
		,CONVERT(DATE, COALESCE(A.RETURNDATE, GETDATE()))	AS EndDate
		,J.EMPLOYEEID										AS OperativeId
		,A.HolType
		,A.duration
		,CASE
			WHEN (A.STARTDATE <> DATEADD(dd, 0, DATEDIFF(dd, 0, A
				.STARTDATE)))
				THEN REPLACE(REPLACE(LTRIM(RIGHT(CONVERT(NVARCHAR, A.STARTDATE, 0), 8)), 'AM', ' AM'), 'PM', ' PM')
			WHEN HolType = ''
				THEN '08:00 AM'
			WHEN HolType = 'M-M'
				THEN '08:00 AM'
			WHEN HolType = 'M'
				THEN '08:00 AM'
			WHEN HolType = 'A'
				THEN '01:00 PM'
			WHEN HolType = 'F'
				THEN '00:00 AM'
			WHEN HolType = 'F-F'
				THEN '00:00 AM'
			WHEN HolType = 'F-M'
				THEN '00:00 AM'
			WHEN HolType = 'A-F'
				THEN '01:00 PM'
		END													AS StartTime
		,CASE
			WHEN
				(A.RETURNDATE <> DATEADD(dd, 0, DATEDIFF(dd, 0, A
				.RETURNDATE)))
				THEN REPLACE(REPLACE(LTRIM(RIGHT(CONVERT(NVARCHAR, A.RETURNDATE, 0), 8)), 'AM', ' AM'), 'PM', ' PM')
			WHEN HolType = ''
				THEN CONVERT(VARCHAR(20), FLOOR(ISNULL(A.DURATION_HRS, 0) + 8)) + ':00'
			WHEN HolType = 'M-M'
				THEN '12:00 PM'
			WHEN HolType = 'M'
				THEN '12:00 PM'
			WHEN HolType = 'A'
				THEN '05:00 PM'
			WHEN HolType = 'F'
				THEN '11:59 PM'
			WHEN HolType = 'F-F'
				THEN '11:59 PM'
			WHEN HolType = 'F-M'
				THEN '12:00 PM'
			WHEN HolType = 'A-F'
				THEN '11:59 PM'
		END													AS EndTime
		,CASE
			WHEN
				(A.STARTDATE <> DATEADD(dd, 0, DATEDIFF(dd, 0, A.STARTDATE)))
				THEN DATEDIFF(mi, '1970-01-01', A.STARTDATE)
			WHEN HolType = ''
				THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), StartDate, 103) + ' ' + '08:00 AM', 103))
			WHEN HolType = 'M-M'
				THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), StartDate, 103) + ' ' + '08:00 AM', 103))
			WHEN HolType = 'M'
				THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), StartDate, 103) + ' ' + '08:00 AM', 103))
			WHEN HolType = 'A'
				THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), StartDate, 103) + ' ' + '01:00 PM', 103))
			WHEN HolType = 'F'
				THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), StartDate, 103), 103))
			WHEN HolType = 'F-F'
				THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), StartDate, 103), 103))
			WHEN HolType = 'F-M'
				THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), StartDate, 103), 103))
			WHEN HolType = 'A-F'
				THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), StartDate, 103) + ' ' + '01:00 PM', 103))
		END													AS StartTimeInMin
		,CASE
			WHEN
				(A.RETURNDATE <> DATEADD(dd, 0, DATEDIFF(dd, 0, A.RETURNDATE)))
				THEN DATEDIFF(mi, '1970-01-01', A.RETURNDATE)
			WHEN HolType = ''
				THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), StartDate, 103) + ' ' + '12:00 PM', 103))
			WHEN HolType = 'M-M'
				THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '12:00 PM', 103))
			WHEN HolType = 'M'
				THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '12:00 PM', 103))
			WHEN HolType = 'A'
				THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '05:00 PM', 103))
			WHEN HolType = 'F'
				THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '11:59 PM', 103))
			WHEN HolType = 'F-F'
				THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '11:59 PM', 103))
			WHEN HolType = 'F-M'
				THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '12:00 PM', 103))
			WHEN HolType = 'A-F'
				THEN DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), ReturnDate, 103) + ' ' + '11:59 PM', 103))
		END													AS EndTimeInMin

	FROM
		E_JOURNAL J
			-- This join is added to just bring Max History Id of every Journal  
			CROSS APPLY
				(
					SELECT
						MAX(ABSENCEHISTORYID) AS MaxAbsenceHistoryId
					FROM
						E_ABSENCE
					WHERE
						JOURNALID = J.JOURNALID
				) AS MaxAbsence
			INNER JOIN E_ABSENCE A ON MaxAbsence.MaxAbsenceHistoryId = A.ABSENCEHISTORYID
			INNER JOIN E_STATUS ON A.ITEMSTATUSID = E_STATUS.ITEMSTATUSID
			LEFT JOIN E_ABSENCEREASON ON A.REASONID = E_ABSENCEREASON.SID
			INNER JOIN E_NATURE N ON N.ITEMNATUREID = J.ITEMNATUREID
			INNER JOIN @Operatives O ON O.EMPLOYEEID = J.EMPLOYEEID
	WHERE
		(
		-- To filter for planned i.e annual leaves etc. where approval is needed  
		(E_STATUS.ITEMSTATUSID = 5
		AND J.ITEMNATUREID >= 2)
		OR
		-- To filter for sickness leaves. where approval is not needed  
		(J.ITEMNATUREID = 1
		AND E_STATUS.ITEMSTATUSID <> 20)
		)
		AND ((A.STARTDATE >= @startDate
		AND COALESCE(A.RETURNDATE, @currentDateTime) <= @endDate)
		OR (A.STARTDATE <= @endDate
		AND COALESCE(A.RETURNDATE, @currentDateTime) >= @startDate)
		OR (A.STARTDATE <= @startDate
		AND COALESCE(A.RETURNDATE, @currentDateTime) >= @endDate))
	-- Get Bank Holidays 
	UNION ALL
	SELECT DISTINCT
			O.EMPLOYEEID																																		AS EMPLOYEEID
			,'Bank Holiday'																																		AS REASON
			,CAST(CAST(bh.BHDATE AS DATE) AS SMALLDATETIME)																										AS StartDate
			,CAST(CAST(bh.BHDATE AS DATE) AS SMALLDATETIME)																										AS EndDate
			,O.EmployeeId																																		AS OperativeId
			,'F'																																				AS HolType
			,1																																					AS duration
			,'00:00 AM'																																			AS StartTime
			,'11:59 PM'																																			AS EndTime
			,DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), CAST(CAST(bh.BHDATE AS DATE) AS SMALLDATETIME), 103), 103))						AS StartTimeInMin
			,DATEDIFF(mi, '1970-01-01', CONVERT(DATETIME, CONVERT(VARCHAR(10), CAST(CAST(bh.BHDATE AS DATE) AS SMALLDATETIME), 103) + ' ' + '11:59 PM', 103))	AS EndTimeInMin
	FROM
		G_BANKHOLIDAYS bh
			CROSS JOIN @Operatives O
	WHERE
		bh.BHDATE BETWEEN @startDate AND @endDate
		AND bh.BHA = 1
	ORDER BY StartDate
			
	/* ===============================================================
	 * Get Appointments For Selected Operatives and Trades
	 * =============================================================== */

	WITH    CTE_Appointments ( AppointmentDate, OperativeID, [Time], NAME, MOBILE, CustAddress, POSTCODE, FaultStatus, AppointmentID, EndTime, StartTimeInSec, EndTimeInSec, TimeToOrderAppointments, TenancyId, IsCalendarAppointment, AppointmentType ,AppointmentEndDate)
	AS (

	-- ==========================================================================================    
	--	FAULT APPOINTMENTS
	-- ========================================================================================== 

	SELECT DISTINCT
		CONVERT(VARCHAR(20), APPOINTMENTDATE, 103)									AS AppointmentDate
		,FL_CO_APPOINTMENT.OperativeID												AS OperativeID
		,FL_CO_APPOINTMENT.[Time]													AS [Time]
		,'Test Name'																AS Name
		,'Test Mobile'																AS MOBILE
		,CASE
			WHEN FL_FAULT_LOG.PROPERTYID <> ''
				THEN ISNULL(P__PROPERTY.HouseNumber, '') + ' '
					+ ISNULL(P__PROPERTY.ADDRESS1, '') + ', '
					+ ISNULL(P__PROPERTY.TOWNCITY, '') + ', '
					+ ISNULL(P__PROPERTY.POSTCODE, '')
			WHEN FL_FAULT_LOG.BLOCKID > 0
				THEN ISNULL(P_BLOCK.ADDRESS1, '')
					+ ISNULL(', ' + P_BLOCK.TOWNCITY, '')
					+ ISNULL(', ' + P_BLOCK.POSTCODE, '')
			WHEN FL_FAULT_LOG.SCHEMEID > 0
				THEN ISNULL(P_SCHEME.SCHEMENAME, '')
					+ ISNULL(', ' + PDR_DEVELOPMENT.TOWN, '')
					+ ISNULL(', ' + PDR_DEVELOPMENT.COUNTY, '')
					+ ISNULL(', ' + PDR_DEVELOPMENT.PostCode, '')
		END																			AS CustAddress
		,'Test PostCode'															AS POSTCODE
		,FL_FAULT_STATUS.[Description]												AS FaultStatus
		,FL_CO_APPOINTMENT.AppointmentID											AS AppointmentID
		,FL_CO_APPOINTMENT.EndTime													AS EndTime
		,DATEDIFF(s, '1970-01-01',
		CONVERT(DATETIME, CONVERT(VARCHAR(10), appointmentdate, 103)
		+ ' ' + [Time], 103))														AS StartTimeInSec
		,DATEDIFF(s, '1970-01-01',
		CONVERT(DATETIME, CONVERT(VARCHAR(10), appointmentdate, 103)
		+ ' ' + EndTime, 103))														AS EndTimeInSec
		,CONVERT(TIME, [TIME])														AS TimeToOrderAppointments
		,C_TENANCY.TENANCYID														AS TenancyId
		,FL_CO_APPOINTMENT.isCalendarAppointment									AS IsCalendarAppointment
		,CASE
			WHEN FL_FAULT_LOG.PROPERTYID IS NULL
				THEN 'SbFault'
			ELSE 'Fault'
		END																			AS AppointmentType
		,CONVERT(VARCHAR(20), COALESCE(AppointmentEndDate, AppointmentDate), 103)	AS AppointmentEndDate
	FROM
		FL_CO_APPOINTMENT
			INNER JOIN @Operatives AS O ON FL_CO_APPOINTMENT.OperativeID = O.EMPLOYEEID
			INNER JOIN FL_FAULT_APPOINTMENT ON FL_CO_APPOINTMENT.AppointmentID = FL_FAULT_APPOINTMENT.AppointmentId
			INNER JOIN FL_FAULT_LOG ON FL_FAULT_APPOINTMENT.FaultLogId = FL_FAULT_LOG.FaultLogID
			INNER JOIN FL_FAULT_TRADE AS FT ON FL_FAULT_LOG.FaultTradeID = FT.FaultTradeId
					AND
					@tradeId IN (FT.TradeId, 0, -1)
			INNER JOIN FL_FAULT_STATUS ON FL_FAULT_LOG.StatusID = FL_FAULT_STATUS.FaultStatusID
			LEFT JOIN C__CUSTOMER C ON FL_FAULT_LOG.CustomerId = C.CUSTOMERID
			LEFT JOIN P__PROPERTY ON FL_FAULT_LOG.PROPERTYID = P__PROPERTY.PROPERTYID
					AND
					@schemeId IN (P__PROPERTY.SCHEMEID, 0, -1)
					AND
					@patchId IN (P__PROPERTY.PATCH, 0, -1)
			LEFT JOIN C_TENANCY ON P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID AND (C_TENANCY.ENDDATE IS NULL	OR C_TENANCY.ENDDATE >=  @today)
			LEFT JOIN P_SCHEME ON FL_FAULT_LOG.SCHEMEID = P_SCHEME.SCHEMEID
					AND
					@schemeId IN (P_SCHEME.SCHEMEID, 0, -1)
			LEFT JOIN P_BLOCK ON FL_FAULT_LOG.BlockId = P_BLOCK.BLOCKID
			LEFT JOIN PDR_DEVELOPMENT ON P_SCHEME.DEVELOPMENTID = PDR_DEVELOPMENT.DEVELOPMENTID
	WHERE		
		FL_CO_APPOINTMENT.APPOINTMENTDATE BETWEEN CONVERT(DATE, @startDate, 103)
		AND
		CONVERT(DATE, @endDate, 103)
		AND FL_FAULT_STATUS.[Description] != 'Cancelled'		
		AND (FL_FAULT_LOG.PROPERTYID <> '' OR FL_FAULT_LOG.SCHEMEID IS NOT NULL OR FL_FAULT_LOG.BlockId > 0)		
		AND CASE
			WHEN FL_FAULT_LOG.PROPERTYID <> ''
				THEN ISNULL(P__PROPERTY.HouseNumber, '') + ' '
					+ ISNULL(P__PROPERTY.ADDRESS1, '') + ', '
					+ ISNULL(P__PROPERTY.TOWNCITY, '') + ', '
					+ ISNULL(P__PROPERTY.POSTCODE, '')
			WHEN FL_FAULT_LOG.BLOCKID > 0
				THEN ISNULL(P_BLOCK.ADDRESS1, '')
					+ ISNULL(', ' + P_BLOCK.TOWNCITY, '')
					+ ISNULL(', ' + P_BLOCK.POSTCODE, '')
			WHEN FL_FAULT_LOG.SCHEMEID > 0
				THEN ISNULL(P_SCHEME.SCHEMENAME, '')
					+ ISNULL(', ' + PDR_DEVELOPMENT.TOWN, '')
					+ ISNULL(', ' + PDR_DEVELOPMENT.COUNTY, '')
					+ ISNULL(', ' + PDR_DEVELOPMENT.PostCode, '')
			END LIKE '%' + @address + '%'
		AND C.FIRSTNAME + ISNULL(' ' + C.MIDDLENAME,'') + ISNULL(' ' + C.LASTNAME, '') LIKE '%' + @tenant +'%'
		AND FL_FAULT_LOG.JobSheetNumber LIKE '%' + @jsn + '%'

	-- ==========================================================================================    
	--	PLANNED APPOINTMENTS
	-- ========================================================================================== 
	UNION ALL
	SELECT DISTINCT
		CONVERT(VARCHAR(20), PLANNED_APPOINTMENTS.APPOINTMENTDATE, 103)		AS AppointmentDate
		,PLANNED_APPOINTMENTS.ASSIGNEDTO									AS OperativeID
		,PLANNED_APPOINTMENTS.APPOINTMENTSTARTTIME							AS Time
		,'Test Name'														AS Name
		,'Test Mobile'														AS MOBILE
		,ISNULL(P__PROPERTY.HouseNumber, '') + ' '
		+ ISNULL(P__PROPERTY.ADDRESS1, '') + ', '
		+ ISNULL(P__PROPERTY.TOWNCITY, '') + ', '
		+ ISNULL(P__PROPERTY.POSTCODE, '')									AS CustAddress
		,'Test PostCode'													AS POSTCODE
		,PLANNED_STATUS.TITLE												AS FaultStatus
		,PLANNED_APPOINTMENTS.AppointmentID									AS AppointmentID
		,PLANNED_APPOINTMENTS.APPOINTMENTENDTIME							AS EndTime
		,DATEDIFF(s, '1970-01-01',
		CONVERT(DATETIME, CONVERT(VARCHAR(10), PLANNED_APPOINTMENTS.APPOINTMENTDATE, 103)
		+ ' '
		+ PLANNED_APPOINTMENTS.APPOINTMENTSTARTTIME, 103))					AS StartTimeInSec
		,DATEDIFF(s, '1970-01-01',
		CONVERT(DATETIME, CONVERT(VARCHAR(10), PLANNED_APPOINTMENTS.APPOINTMENTDATE, 103)
		+ ' '
		+ PLANNED_APPOINTMENTS.APPOINTMENTENDTIME, 103))					AS EndTimeInSec
		,CONVERT(TIME, PLANNED_APPOINTMENTS.APPOINTMENTSTARTTIME)			AS TimeToOrderAppointments
		,C_TENANCY.TENANCYID												AS TenancyId
		,0																	AS IsCalendarAppointment
		,ISNULL(PAT.Planned_Appointment_Type, 'Planned')					AS AppointmentType
		,CONVERT(VARCHAR(20), PLANNED_APPOINTMENTS.APPOINTMENTENDDATE, 103)	AS AppointmentEndDate
	FROM
		PLANNED_APPOINTMENTS
			INNER JOIN @Operatives AS O ON PLANNED_APPOINTMENTS.ASSIGNEDTO = O.EMPLOYEEID
			LEFT JOIN Planned_Appointment_Type AS PAT ON PLANNED_APPOINTMENTS.Planned_Appointment_TypeId = PAT.Planned_Appointment_TypeId
			INNER JOIN PLANNED_JOURNAL ON PLANNED_APPOINTMENTS.JournalId = PLANNED_JOURNAL.JOURNALID
			INNER JOIN PLANNED_STATUS ON PLANNED_JOURNAL.STATUSID = PLANNED_STATUS.STATUSID
			INNER JOIN P__PROPERTY ON PLANNED_JOURNAL.PROPERTYID = P__PROPERTY.PROPERTYID
					AND
					@schemeId IN (P__PROPERTY.SCHEMEID, 0, -1)
					AND
					@patchId IN (P__PROPERTY.PATCH, 0, -1)
					AND
					 ISNULL(P__PROPERTY.HouseNumber, '')
					 +  ISNULL(' '+ P__PROPERTY.ADDRESS1, '') + 
					 + ISNULL(', ' + P__PROPERTY.TOWNCITY, '')
					 + ISNULL(', ' +P__PROPERTY.POSTCODE, '') LIKE '%' + @address + '%'
			LEFT JOIN PLANNED_COMPONENT_TRADE AS PCT ON PLANNED_APPOINTMENTS.COMPTRADEID = PCT.COMPTRADEID
			LEFT JOIN PLANNED_MISC_TRADE AS PMT ON PLANNED_APPOINTMENTS.APPOINTMENTID = PMT.AppointmentId
			INNER JOIN C_TENANCY ON P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID AND (C_TENANCY.ENDDATE IS NULL
										OR C_TENANCY.ENDDATE >= @today)
			LEFT JOIN C_CUSTOMERTENANCY CT ON C_TENANCY.TENANCYID = CT.TENANCYID
			LEFT JOIN C__CUSTOMER C ON CT.CUSTOMERID = C.CUSTOMERID
	WHERE		
		(PLANNED_APPOINTMENTS.APPOINTMENTDATE BETWEEN @startDate AND @endDate
			OR PLANNED_APPOINTMENTS.APPOINTMENTENDDATE BETWEEN @startDate AND @endDate
			OR (PLANNED_APPOINTMENTS.APPOINTMENTDATE <= @startDate AND PLANNED_APPOINTMENTS.APPOINTMENTENDDATE >= @endDate))
		AND APPOINTMENTSTATUS <> 'Cancelled'
		AND ISPENDING = 0
		AND @tradeId IN (PCT.TRADEID,PMT.TradeId,0,-1)
		AND C.FIRSTNAME + ISNULL(' ' + C.MIDDLENAME,'') + ISNULL(' ' + C.LASTNAME,'') LIKE '%' + @tenant +'%'
		AND (@jsn = '' OR @jsn IS NULL)
	-- ==========================================================================================    
	--	PDR APPOINTMENTS
	-- ==========================================================================================   

	---Get PDR appointment e.g M&E Servicing, Maintenance Servicing
	UNION ALL
	SELECT DISTINCT
		CONVERT(VARCHAR(20), PDR_APPOINTMENTS.APPOINTMENTSTARTDATE, 103)	AS AppointmentDate
		,PDR_APPOINTMENTS.ASSIGNEDTO										AS OperativeID
		,PDR_APPOINTMENTS.APPOINTMENTSTARTTIME								AS [Time]
		,'Test Name'														AS Name
		,'Test Mobile'														AS MOBILE
		,CASE
			WHEN PDR_MSAT.propertyid IS NOT NULL
				THEN ISNULL(P__PROPERTY.HOUSENUMBER, '') + ' '
					+ ISNULL(P__PROPERTY.ADDRESS1, '')
					+ ISNULL(', ' + P__PROPERTY.TOWNCITY, '')
					+ ISNULL(' ' + P__PROPERTY.POSTCODE, '')
			WHEN PDR_MSAT.SCHEMEID IS NOT NULL
				THEN ISNULL(P_SCHEME.SCHEMENAME, '')
					+ ISNULL(' ' + P_SCHEME.SCHEMECODE, '')
			WHEN PDR_MSAT.BLOCKID IS NOT NULL
				THEN ISNULL(P_BLOCK.BLOCKNAME, '') + ' '
					+ ISNULL(P_BLOCK.ADDRESS1, '')
					+ ISNULL(', ' + P_BLOCK.TOWNCITY, '')
					+ ISNULL(' ' + P_BLOCK.POSTCODE, '')
		END																	AS CustAddress
		,CASE
			WHEN PDR_MSAT.propertyid IS NOT NULL
				THEN ISNULL(P__PROPERTY.POSTCODE, '')
			WHEN PDR_MSAT.SCHEMEID IS NOT NULL
				THEN ISNULL(P_SCHEME.SCHEMECODE, '')
			WHEN PDR_MSAT.BLOCKID IS NOT NULL
				THEN ISNULL(P_BLOCK.POSTCODE, '')
		END																	AS POSTCODE
		,CASE
			WHEN PDR_STATUS.TITLE = 'Arranged'
				THEN 'Appointment Arranged'
			WHEN PDR_STATUS.TITLE = 'Completed'
				THEN 'Complete'
			ELSE PDR_STATUS.TITLE
		END																	AS FaultStatus
		,PDR_APPOINTMENTS.APPOINTMENTID										AS AppointmentID
		,PDR_APPOINTMENTS.APPOINTMENTENDTIME								AS EndTime
		,DATEDIFF(s, '1970-01-01',
		CONVERT(DATETIME, CONVERT(VARCHAR(10), PDR_APPOINTMENTS.APPOINTMENTSTARTDATE, 103)
		+ ' '
		+ PDR_APPOINTMENTS.APPOINTMENTSTARTTIME, 103))						AS StartTimeInSec
		,DATEDIFF(s, '1970-01-01',
		CONVERT(DATETIME, CONVERT(VARCHAR(10), PDR_APPOINTMENTS.APPOINTMENTSTARTDATE, 103)
		+ ' '
		+ PDR_APPOINTMENTS.APPOINTMENTENDTIME, 103))						AS EndTimeInSec
		,CONVERT(TIME, PDR_APPOINTMENTS.APPOINTMENTSTARTTIME)				AS TimeToOrderAppointments
		,C_TENANCY.TENANCYID												AS TenancyId
		,0																	AS IsCalendarAppointment
		,PDR_MSATType.MSATTypeName											AS AppointmentType
		,CONVERT(VARCHAR(20), PDR_APPOINTMENTS.APPOINTMENTENDDATE, 103)		AS AppointmentEndDate

	FROM
		PDR_APPOINTMENTS
			INNER JOIN @Operatives AS O ON PDR_APPOINTMENTS.ASSIGNEDTO = O.EMPLOYEEID
			INNER JOIN PDR_JOURNAL ON PDR_APPOINTMENTS.JOURNALID = PDR_JOURNAL.JOURNALID
			INNER JOIN pdr_status ON PDR_JOURNAL.STATUSID = PDR_STATUS.STATUSID
					AND
					PDR_STATUS.TITLE <> 'Cancelled'
			INNER JOIN PDR_MSAT ON PDR_MSAT.MSATId = PDR_JOURNAL.MSATID
			LEFT JOIN P_SCHEME ON PDR_MSAT.SchemeId = P_SCHEME.SCHEMEID
					AND
					@schemeId IN (P_SCHEME.SCHEMEID, 0, -1)
			LEFT JOIN P_BLOCK ON PDR_MSAT.BlockId = P_BLOCK.BLOCKID
			LEFT JOIN P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID
					AND
					@schemeId IN (P__PROPERTY.SCHEMEID, 0, -1)
					AND
					@patchId IN (P__PROPERTY.PATCH, 0, -1)
			INNER JOIN PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId AND MSATTypeName NOT LIKE '%Void%' 
			LEFT JOIN p_status ON p__property.[status] = p_status.statusid


			LEFT JOIN c_tenancy ON p__property.propertyid = c_tenancy.propertyid
					AND
					(dbo.c_tenancy.enddate IS NULL
						OR dbo.c_tenancy.enddate >= @today)
			LEFT JOIN c_customer_names_grouped CG ON CG.i = c_tenancy.tenancyid
					AND
					CG.id IN
					(
						SELECT
							MAX(id) ID
						FROM
							c_customer_names_grouped
						GROUP BY
							i
					)
	WHERE
		PDR_STATUS.TITLE != 'Cancelled'
		AND (pdr_appointments.appointmentstartdate BETWEEN @startDate AND @endDate
			OR pdr_appointments.appointmentenddate BETWEEN @startDate AND @endDate
			OR (pdr_appointments.appointmentstartdate <= @startDate AND pdr_appointments.appointmentenddate >= @endDate))
		AND	CASE
			WHEN PDR_MSAT.propertyid IS NOT NULL
				THEN ISNULL(P__PROPERTY.HOUSENUMBER, '') + ' '
					+ ISNULL(P__PROPERTY.ADDRESS1, '')
					+ ISNULL(', ' + P__PROPERTY.TOWNCITY, '')
					+ ISNULL(' ' + P__PROPERTY.POSTCODE, '')
			WHEN PDR_MSAT.SCHEMEID IS NOT NULL
				THEN ISNULL(P_SCHEME.SCHEMENAME, '')
					+ ISNULL(' ' + P_SCHEME.SCHEMECODE, '')
			WHEN PDR_MSAT.BLOCKID IS NOT NULL
				THEN ISNULL(P_BLOCK.BLOCKNAME, '') + ' '
					+ ISNULL(P_BLOCK.ADDRESS1, '')
					+ ISNULL(', ' + P_BLOCK.TOWNCITY, '')
					+ ISNULL(' ' + P_BLOCK.POSTCODE, '')
			END LIKE '%' + @address + '%'
		AND CG.LIST LIKE '%' + @tenant + '%'
		AND (@jsn = '' OR @jsn IS NULL)
		AND @tradeId IN (0,-1)
	-- ==========================================================================================    
	--	Stock Appointments
	-- ========================================================================================== 
	UNION ALL
	
	SELECT DISTINCT
		CONVERT(VARCHAR(20), PDR_APPOINTMENTS.APPOINTMENTSTARTDATE, 103)	AS AppointmentDate
		,PDR_APPOINTMENTS.ASSIGNEDTO										AS OperativeID
		,PDR_APPOINTMENTS.APPOINTMENTSTARTTIME								AS [Time]
		,'Test Name'														AS Name
		,'Test Mobile'														AS MOBILE
		,ISNULL(P__PROPERTY.HouseNumber, '') + ' '
		+ ISNULL(P__PROPERTY.ADDRESS1, '') + ', '
		+ ISNULL(P__PROPERTY.TOWNCITY, '') + ', '
		+ ISNULL(P__PROPERTY.POSTCODE, '')									AS CustAddress
		,ISNULL(P__PROPERTY.POSTCODE, '')							AS POSTCODE
		,CASE
			WHEN PDR_STATUS.TITLE = 'Arranged'
				THEN 'Appointment Arranged'
			WHEN PDR_STATUS.TITLE = 'Completed'
				THEN 'Complete'
			ELSE PDR_STATUS.TITLE
		END																	AS FaultStatus
		,PDR_APPOINTMENTS.APPOINTMENTID										AS AppointmentID
		,PDR_APPOINTMENTS.APPOINTMENTENDTIME								AS EndTime
		,DATEDIFF(s, '1970-01-01',
		CONVERT(DATETIME, CONVERT(VARCHAR(10), PDR_APPOINTMENTS.APPOINTMENTSTARTDATE, 103)
		+ ' '
		+ PDR_APPOINTMENTS.APPOINTMENTSTARTTIME, 103))						AS StartTimeInSec
		,DATEDIFF(s, '1970-01-01',
		CONVERT(DATETIME, CONVERT(VARCHAR(10), PDR_APPOINTMENTS.APPOINTMENTSTARTDATE, 103)
		+ ' '
		+ PDR_APPOINTMENTS.APPOINTMENTENDTIME, 103))						AS EndTimeInSec
		,CONVERT(TIME, PDR_APPOINTMENTS.APPOINTMENTSTARTTIME)				AS TimeToOrderAppointments
		,C_TENANCY.TENANCYID												AS TenancyId
		,0																	AS IsCalendarAppointment
		,PDR_MSATType.MSATTypeName											AS AppointmentType
		,CONVERT(VARCHAR(20), PDR_APPOINTMENTS.APPOINTMENTENDDATE, 103)		AS AppointmentEndDate

	
	FROM
		PDR_APPOINTMENTS
			INNER JOIN @operatives AS O ON PDR_APPOINTMENTS.ASSIGNEDTO = O.EMPLOYEEID
			INNER JOIN PDR_JOURNAL ON PDR_APPOINTMENTS.JOURNALID = PDR_JOURNAL.JOURNALID
			INNER JOIN pdr_status ON PDR_JOURNAL.STATUSID = PDR_STATUS.STATUSID AND PDR_STATUS.TITLE <> 'Cancelled'
			INNER JOIN PDR_MSAT ON PDR_MSAT.MSATId = PDR_JOURNAL.MSATID			
			INNER JOIN P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID AND @schemeId IN (P__PROPERTY.SCHEMEID, 0, -1) AND @patchId IN (P__PROPERTY.PATCH, 0, -1)
			INNER JOIN PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId AND MSATTypeName LIKE '%Void%' 
			LEFT JOIN p_status ON p__property.[status] = p_status.statusid


			INNER JOIN C_TENANCY ON PDR_MSAT.TenancyId = C_TENANCY.TENANCYID
			LEFT JOIN c_customer_names_grouped CG ON CG.i = c_tenancy.tenancyid
					AND
					CG.id IN
					(
						SELECT
							MAX(id) ID
						FROM
							c_customer_names_grouped
						GROUP BY
							i
					)
	WHERE
		PDR_STATUS.TITLE != 'Cancelled'
		AND (pdr_appointments.appointmentstartdate BETWEEN @startDate AND @endDate
			OR pdr_appointments.appointmentenddate BETWEEN @startDate AND @endDate
			OR (pdr_appointments.appointmentstartdate <= @startDate AND pdr_appointments.appointmentenddate >= @endDate))
		AND	(ISNULL(P__PROPERTY.HOUSENUMBER, '') + ' '
					+ ISNULL(P__PROPERTY.ADDRESS1, '')
					+ ISNULL(', ' + P__PROPERTY.TOWNCITY, '')
					+ ISNULL(' ' + P__PROPERTY.POSTCODE, ''))  LIKE '%' + @address + '%'
		AND CG.LIST LIKE '%' + @tenant + '%'
		
	
	-- ==========================================================================================    
	--	Stock Appointments
	-- ========================================================================================== 	
		UNION ALL
		SELECT DISTINCT
		CONVERT(VARCHAR(20), SA.AppointStartDateTime, 103)					AS AppointmentDate
		,AL.EMPLOYEEID														AS OperativeID
		,CONVERT(VARCHAR(5),SA.AppointStartDateTime,108)					AS [Time]
		,'Test Name'														AS Name
		,'Test Mobile'														AS MOBILE
		,SA.AppointLocation													AS CustAddress
		,P.POSTCODE															AS POSTCODE
		,SA.AppointProgStatus												AS FaultStatus
		,SA.AppointId														AS AppointmentID
		,CONVERT(VARCHAR(5),SA.AppointEndDateTime,108)						AS EndTime
		,DATEDIFF(s, '1970-01-01',SA.AppointStartDateTime)					AS StartTimeInSec
		,DATEDIFF(s, '1970-01-01',SA.AppointEndDateTime)					AS EndTimeInSec
		,CONVERT(TIME, SA.AppointStartDateTime)								AS TimeToOrderAppointments
		,T.TENANCYID														AS TenancyId
		,0																	AS IsCalendarAppointment
		,'Stock'															AS AppointmentType
		,CONVERT(VARCHAR(20), SA.AppointEndDateTime, 103)					AS AppointmentEndDate
		FROM PS_Appointment SA
		INNER JOIN AC_LOGINS AS AL ON AL.LOGIN = SA.SurveyourUserName
		INNER JOIN @Operatives AS O ON AL.EMPLOYEEID = O.EMPLOYEEID
		INNER JOIN PS_Appointment2Survey AS SA2S ON SA.AppointId = SA2S.AppointId
		INNER JOIN PS_Survey AS PS ON PS.SurveyId = SA2S.SurveyId
		INNER JOIN P__PROPERTY AS P ON PS.PROPERTYID = P.PROPERTYID
		LEFT JOIN C_TENANCY AS T ON PS.PROPERTYID = T.PROPERTYID AND (T.ENDDATE IS NULL OR T.ENDDATE >= @Today)
		LEFT JOIN c_customer_names_grouped CG ON CG.i = T.tenancyid
					AND
					CG.id IN
					(
						SELECT
							MAX(id) ID
						FROM
							c_customer_names_grouped
						GROUP BY
							i
					)
		WHERE
			SA.AppointProgStatus != 'Cancelled'
			AND (SA.AppointStartDateTime BETWEEN @startDate AND @endDate
					OR SA.AppointEndDateTime BETWEEN @startDate AND @endDate
					OR (SA.AppointStartDateTime <= @startDate AND SA.AppointEndDateTime >= @endDate)
				)
			AND	ISNULL(P.HouseNumber, '')
					 + ISNULL(' ' + P.ADDRESS1, '')
					 + ISNULL(', ' + P.TOWNCITY, '')
					 + ISNULL(', ' +P.POSTCODE, '') LIKE '%' + @address + '%'
			AND CG.LIST LIKE '%' + @tenant + '%'
			AND (@jsn = '' OR @jsn IS NULL)
	)
	
	
	
	
	
	
	
	
	SELECT
		AppointmentDate
		,OperativeID
		,[Time]
		,NAME
		,MOBILE
		,CustAddress
		,POSTCODE
		,FaultStatus
		,AppointmentID
		,EndTime
		,StartTimeInSec
		,EndTimeInSec
		,TimeToOrderAppointments	TenancyId
		,IsCalendarAppointment
		,AppointmentType
		,AppointmentEndDate
	FROM
		CTE_Appointments
	ORDER BY	APPOINTMENTDATE
				,TimeToOrderAppointments	ASC







	/* ===============================================================
	 * Get operatives working hours core and out of office
	 * =============================================================== */
	-- Get Core Working Hours		
	SELECT DISTINCT
		E_WEEKDAYS.NAME		AS WeekDayName
		,E_WEEKDAYS.DayId	AS DayId
		,ECWH.EMPLOYEEID	AS EmployeeId
		,STARTTIME			AS StartTime
		,ENDTIME			AS EndTime
		,'CoreWorkingHours'	AS HoursType
	FROM
		E_CORE_WORKING_HOURS ECWH
		INNER JOIN E_WEEKDAYS
			ON ECWH.DAYID = E_WEEKDAYS.DAYID
		INNER JOIN @Operatives AS O ON  ECWH.EMPLOYEEID = O.EMPLOYEEID

	WHERE
		LEN(STARTTIME) > 0
		AND LEN(ENDTIME) > 0

	-- Get Out of office hours
	UNION ALL SELECT
		E_WEEKDAYS.NAME		AS WeekDayName
		,E_WEEKDAYS.DayId	AS DayId
		,EOOH.EMPLOYEEID	AS EmployeeId
		,STARTTIME			AS StartTime
		,ENDTIME			AS EndTime
		,'OutOfOfficeHours'	AS HoursType
	FROM
		E_OUT_OF_HOURS EOOH
		INNER JOIN E_WEEKDAYS
			ON DATEPART(WEEKDAY, EOOH.STARTDATE) = E_WEEKDAYS.DAYID
		INNER JOIN @Operatives AS O ON  EOOH.EMPLOYEEID = O.EMPLOYEEID
	WHERE	
		(EOOH.STARTDATE BETWEEN @startDate AND @endDate
				OR EOOH.ENDDATE BETWEEN @startDate AND @endDate
				OR (EOOH.STARTDATE <= @startDate AND EOOH.ENDDATE >= @endDate))


	
END	
	
	/****** Object:  StoredProcedure [dbo].[PDR_GetAvailableOperatives]    Script Date: 12/10/2013 12:58:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =============================================
--EXEC	[dbo].[PDR_GetAvailableOperatives]
--		@tradeIds = N'1,2,3',
--		@msattype= 'M&E Servicing',
--		@startDate = N'' 
-- Author:		Ahmed Muhammad
-- Create date: Create Date,,16 Jan,2015
-- Description:	This stored procedure fetch  the employees 
-- History:		25/08/2015 AR: Added Cancelled and Complete Status
-- ============================================= */
ALTER PROCEDURE [dbo].[PDR_GetAvailableOperatives] 
	-- Add the parameters for the stored procedure here
	@tradeIds as varchar(max)= NULL,
	@msattype as varchar(max),
	@startDate as datetime	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @operativeIdStr NVARCHAR(MAX)
	
	--=================================================================================================================
	------------------------------------------------------ Step 0------------------------------------------------------
	--Create Temporary Table
	
	
    CREATE TABLE #AvailableOperatives(
		EmployeeId int
		,FirstName nvarchar(50)
		,LastName nvarchar(50)
		,FullName nvarchar(100)		
		,PatchId int
		,PatchName nvarchar(20)		
		,Distance nvarchar(10)
		,TradeId INT				
	)
	
	DECLARE @InsertClause varchar(200)
	DECLARE @SelectClause varchar(800)
	DECLARE @FromClause varchar(800)
	DECLARE @WhereClause varchar(max)		
	DECLARE @MainQuery varchar(max)
	--=================================================================================================================
	------------------------------------------------------ Step 1------------------------------------------------------
	--This query fetches the employees which matches with the following criteria
	--Trade of employee is same as trade of component 
	--User type is M&E Servicing or Cyclic Maintenance		
	--Exclude the sick leaves
	
	SET @InsertClause = 'INSERT INTO #AvailableOperatives(EmployeeId,FirstName,LastName,FullName, PatchId, PatchName'
	
	SET @SelectClause = 'SELECT distinct E__EMPLOYEE.employeeid as EmployeeId
	,E__EMPLOYEE.FirstName as FirstName
	,E__EMPLOYEE.LastName as LastName
	,E__EMPLOYEE.FirstName + '' ''+ E__EMPLOYEE.LastName as FullName	
	,E_JOBDETAILS.PATCH as PatchId
	,E_PATCH.Location as PatchName
	' 
	
	SET @FromClause  = 'FROM  E__EMPLOYEE 
	INNER JOIN E_TRADE ON E_TRADE.EmpId = E__EMPLOYEE.EmployeeId
	INNER JOIN (SELECT Distinct EmployeeId,InspectionTypeID FROM AS_USER_INSPECTIONTYPE) AS_USER_INSPECTIONTYPE ON E__EMPLOYEE.EMPLOYEEID=AS_USER_INSPECTIONTYPE.EmployeeId
	INNER JOIN dbo.P_INSPECTIONTYPE  ON AS_USER_INSPECTIONTYPE.InspectionTypeID=P_INSPECTIONTYPE.InspectionTypeID 
	INNER JOIN E_JOBDETAILS ON E__EMPLOYEE.EMPLOYEEID = E_JOBDETAILS.EMPLOYEEID
	LEFT JOIN E_PATCH ON E_JOBDETAILS.PATCH = E_PATCH.PATCHID '
	
	SET @WhereClause  = 'WHERE P_INSPECTIONTYPE.Description  ='''+@msattype+''' 	
	AND E_JOBDETAILS.Active=1'
	
	IF(LEN(@tradeIds) > 0 AND @tradeIds IS NOT NULL)
	BEGIN
		SET @WhereClause = @WhereClause + CHAR(10)+ ' AND E_TRADE.tradeid in ('+@tradeIds+')'
		SET @SelectClause = @SelectClause + CHAR(10)+',E_TRADE.TradeId AS TradeId '
		SET @InsertClause = @InsertClause + CHAR(10)+', TradeId) '
	END
	ELSE
	BEGIN
	SET @InsertClause = @InsertClause + CHAR(10)+') '
	END
	---------------------------------------------------------
	-- Filter to skip out operative(s) of sick leave.
	SET @WhereClause  =  @WhereClause +CHAR(10)+ ' AND E__EMPLOYEE.EmployeeId NOT IN (
																						SELECT 
																							EMPLOYEEID 
																							FROM E_JOURNAL
																							INNER JOIN E_ABSENCE ON E_JOURNAL.JOURNALID = E_ABSENCE.JOURNALID 
																							AND E_ABSENCE.ABSENCEHISTORYID IN (
																																SELECT 
																																	MAX(ABSENCEHISTORYID) 
																																	FROM E_ABSENCE 
																																	GROUP BY JOURNALID
																																)
																							WHERE ITEMNATUREID = 1
																							AND E_ABSENCE.RETURNDATE IS NULL
																							AND E_ABSENCE.ITEMSTATUSID = 1
																						)	'
	---------------------------------------------------------				
	--Combine the query clauses to make the one / main query
	SET @MainQuery = @InsertClause+Char(10)+@SelectClause+Char(10)+@FromClause+Char(10)+@WhereClause 										
	
	
	--print @InsertClause+Char(10)
	print @SelectClause+Char(10)
	print @FromClause+Char(10)
	print @WhereClause 						
	
	EXEC (@MainQuery)		
	
	SELECT Distinct EmployeeId, FirstName, LastName, FullName, PatchId, PatchName, Distance, TradeID  FROM  #AvailableOperatives	
	--=================================================================================================================
	------------------------------------------------------ Step 3------------------------------------------------------						
	--This query selects the leaves of employees i.e the employess which we get in step1
	--M : morning - 08:00 AM - 12:00 PM
	--A : mean after noon - 01:00 PM - 05:00 PM
	--F : Single full day
	--F-F : Multiple full days
	--F-M : Multiple full days with last day  morning - 08:00 AM - 12:00 PM
	--A-F : From First day after noon - 01:00 PM - 05:00 PM with multiple full days
	
	
	SELECT E_ABSENCE.STARTDATE as StartDate
	, E_ABSENCE.RETURNDATE	as EndDate
	, E_JOURNAL.employeeid as OperativeId
	,E_ABSENCE.HolType as HolType
	,E_ABSENCE.duration as Duration
	,CASE 
		WHEN HolType = 'M' THEN '09:00 AM'
		WHEN HolType = 'A' THEN '01:00 PM'
		WHEN HolType = 'F' THEN '00:00 AM'
		WHEN HolType = 'F-F' THEN '00:00 AM'
		WHEN HolType = 'F-M' THEN '00:00 AM'
		WHEN HolType = 'A-F' THEN '01:00 PM'
	END as StartTime
	,CASE 
		WHEN HolType = 'M' THEN '01:00 PM'
		WHEN HolType = 'A' THEN '05:00 PM'
		WHEN HolType = 'F' THEN '11:59 PM'
		WHEN HolType = 'F-F' THEN '11:59 PM'
		WHEN HolType = 'F-M' THEN '01:00 PM'
		WHEN HolType = 'A-F' THEN '11:59 PM'
	END as EndTime
	,CASE 
		WHEN HolType = 'M' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), StartDate,103) + ' ' + '09:00 AM',103)) 
		WHEN HolType = 'A' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), StartDate,103) + ' ' + '01:00 PM',103)) 
		WHEN HolType = 'F' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), StartDate,103),103)) 
		WHEN HolType = 'F-F' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), StartDate,103),103)) 
		WHEN HolType = 'F-M' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), StartDate,103),103)) 
		WHEN HolType = 'A-F' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), StartDate,103) + ' ' + '01:00 PM',103)) 
	END as StartTimeInMin
	,CASE 
		WHEN HolType = 'M' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), ReturnDate,103) + ' ' + '01:00 PM',103)) 
		WHEN HolType = 'A' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), ReturnDate,103) + ' ' + '05:00 PM',103)) 
		WHEN HolType = 'F' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), ReturnDate,103)+ ' ' + '11:59 PM',103)) 
		WHEN HolType = 'F-F' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), ReturnDate,103)+ ' ' + '11:59 PM',103)) 
		WHEN HolType = 'F-M' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), ReturnDate,103) + ' ' + '01:00 PM',103)) 
		WHEN HolType = 'A-F' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), ReturnDate,103)+ ' ' + '11:59 PM',103)) 
	END as EndTimeInMin
	FROM E_JOURNAL 
	INNER JOIN E_ABSENCE ON E_JOURNAL.JOURNALID = E_ABSENCE.JOURNALID AND E_ABSENCE.ABSENCEHISTORYID IN (SELECT MAX(ABSENCEHISTORYID) FROM E_ABSENCE GROUP BY JOURNALID)
	WHERE
	(
		-- To filter for M&E Servicing or Cyclic Maintenance i.e annual leaves etc. where approval is needed
		( 
			E_ABSENCE.ITEMSTATUSID = 5
   AND itemnatureid >= 2
		)
		OR
		-- To filter for sickness leaves. where the operative is now returned to work.
		( 
			ITEMNATUREID = 1
			AND E_ABSENCE.ITEMSTATUSID = 2
			AND E_ABSENCE.RETURNDATE IS NOT NULL 
		)
	)
	AND E_ABSENCE.RETURNDATE >= CONVERT(DATE,@startDate)
	AND E_JOURNAL.employeeid in(SELECT employeeid FROM  #AvailableOperatives)								

	--=================================================================================================================
	------------------------------------------------------ Step 4------------------------------------------------------						
	--This query selects the appointments of employees i.e the employess which we get in step1
	--Fault Appointments
	--Gas Appointments
	--Planned Appointments
	--M&E Servicing or Cyclic Maintenance Appointments
	-----------------------------------------------------------------------------------------------------------------------
	-- Fault Appointments
	SELECT AppointmentDate as AppointmentStartDate
	,AppointmentDate as AppointmentEndDate
	,OperativeId as OperativeId
	,Time as StartTime
	,EndTime as EndTime
	,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), appointmentdate,103) + ' ' + Time,103)) as StartTimeInSec
	,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), appointmentdate,103) + ' ' + EndTime,103)) as EndTimeInSec
	,p__property.postcode as PostCode
	,ISNULL(P__PROPERTY.HouseNumber,'') +' '+ ISNULL(P__PROPERTY.ADDRESS1,'') +' '+ ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'')  AS Address			
	,P__PROPERTY.TownCity as TownCity    
	,P__PROPERTY.County as County
	,'Fault Appointment'  as AppointmentType
	FROM FL_CO_Appointment 
	inner join fl_fault_appointment on FL_co_APPOINTMENT.appointmentid = fl_fault_appointment.appointmentid
	inner join fl_fault_log on fl_fault_appointment.faultlogid = fl_fault_log.faultlogid
	INNER JOIN FL_FAULT_STATUS ON FL_FAULT_STATUS.FaultStatusID = FL_FAULT_LOG.StatusID
	inner join p__property on fl_fault_log.propertyid = p__property.propertyid				
	WHERE 
	1=1
	AND appointmentdate >= CONVERT(DATE,@startDate)
	AND (FL_FAULT_STATUS.Description <> 'Cancelled' AND FL_FAULT_STATUS.Description <> 'Complete')
	AND operativeid in (SELECT employeeid FROM  #AvailableOperatives)	
	-----------------------------------------------------------------------------------------------------------------------
	--Appliance Appointments	
	UNION ALL 	
	SELECT 
		AS_APPOINTMENTS.AppointmentDate as AppointmentStartDate
		,AS_APPOINTMENTS.AppointmentDate as AppointmentEndDate
		,AS_APPOINTMENTS.ASSIGNEDTO as OperativeId
		,APPOINTMENTSTARTTIME as StartTime
		,APPOINTMENTENDTIME as EndTime
		,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), AS_APPOINTMENTS.AppointmentDate,103) + ' ' + APPOINTMENTSTARTTIME,103)) as StartTimeInSec
		,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), AS_APPOINTMENTS.AppointmentDate,103) + ' ' + APPOINTMENTENDTIME,103)) as EndTimeInSec
		,p__property.postcode as PostCode
		,ISNULL(P__PROPERTY.HouseNumber,'') +' '+ ISNULL(P__PROPERTY.ADDRESS1,'') +' '+ ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'')  AS Address
		,P__PROPERTY.TownCity as TownCity
		,P__PROPERTY.County as County
		,'Gas Appointment' as AppointmentType  
	FROM AS_APPOINTMENTS 
		INNER JOIN AS_JOURNAL ON AS_APPOINTMENTS.JournalId=AS_JOURNAL.JOURNALID 
		INNER JOIN AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId
		INNER JOIN P__PROPERTY ON AS_JOURNAL.PROPERTYID=P__PROPERTY.PROPERTYID 
		INNER JOIN P_STATUS  ON P__PROPERTY.[STATUS] =P_STATUS.STATUSID  
		LEFT JOIN C_TENANCY ON P__PROPERTY.PROPERTYID=C_TENANCY.PROPERTYID AND (DBO.C_TENANCY.ENDDATE IS NULL OR DBO.C_TENANCY.ENDDATE > GETDATE())
	WHERE AS_JOURNAL.IsCurrent = 1		
		AND Convert(date,AS_APPOINTMENTS.AppointmentDate,103) >= Convert(date,@startDate,103)
		AND (AS_Status.Title <> 'Cancelled'
		AND APPOINTMENTSTATUS <> 'Complete')		
		AND AS_APPOINTMENTS.ASSIGNEDTO in (SELECT employeeid FROM  #AvailableOperatives)		
	-----------------------------------------------------------------------------------------------------------------------
	--Planned Appointments
	UNION ALL 
	SELECT 
		APPOINTMENTDATE as AppointmentStartDate
		,APPOINTMENTENDDATE as AppointmentEndDate
		,ASSIGNEDTO  as OperativeId
		,APPOINTMENTSTARTTIME as StartTime
		,APPOINTMENTENDTIME as EndTime
		,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), APPOINTMENTDATE,103) + ' ' + APPOINTMENTSTARTTIME ,103)) as StartTimeInSec
		,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), APPOINTMENTENDDATE,103) + ' ' + APPOINTMENTENDTIME,103)) as EndTimeInSec
		,p__property.postcode as PostCode
		,ISNULL(P__PROPERTY.HouseNumber,'') +' '+ ISNULL(P__PROPERTY.ADDRESS1,'') +' '+ ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'')  AS Address			
		,P__PROPERTY.TownCity as TownCity    
		,P__PROPERTY.County as County
		,'Planned Appointment' as AppointmentType
	FROM 
		PLANNED_APPOINTMENTS
		INNER JOIN PLANNED_JOURNAL ON PLANNED_JOURNAL.JOURNALID = PLANNED_APPOINTMENTS.JournalId 
		INNER JOIN P__PROPERTY ON PLANNED_JOURNAL.PROPERTYID = P__PROPERTY.PROPERTYID 
		INNER JOIN PLANNED_STATUS ON PLANNED_STATUS.STATUSID = PLANNED_JOURNAL.STATUSID
	WHERE 
		Convert(date,AppointmentDate,103) >= Convert(date,@startDate,103)	
		AND (PLANNED_STATUS.TITLE <> 'Cancelled'
		AND APPOINTMENTSTATUS NOT IN ('Complete', 'Cancelled'))	
		AND PLANNED_APPOINTMENTS.ASSIGNEDTO in (SELECT employeeid FROM  #AvailableOperatives)
		
	-----------------------------------------------------------------------------------------------------------------------
	--M&E Appointments
	UNION ALL 
	SELECT 
		APPOINTMENTSTARTDATE as AppointmentStartDate
		,APPOINTMENTENDDATE as AppointmentEndDate
		,ASSIGNEDTO  as OperativeId
		,APPOINTMENTSTARTTIME as StartTime
		,APPOINTMENTENDTIME as EndTime
		,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), APPOINTMENTSTARTDATE,103) + ' ' + APPOINTMENTSTARTTIME ,103)) as StartTimeInSec
		,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), APPOINTMENTENDDATE,103) + ' ' + APPOINTMENTENDTIME,103)) as EndTimeInSec
		,P__PROPERTY.postcode as PostCode
		,ISNULL(P__PROPERTY.HouseNumber,'') +' '+ ISNULL(P__PROPERTY.ADDRESS1,'') +' '+ ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'')  AS Address			
		,ISNULL(P__PROPERTY.TownCity,'') as TownCity    
		,ISNULL(P__PROPERTY.County,'') as County
		,PDR_MSATType.MSATTypeName +' Appointment' as AppointmentType
	FROM 
		PDR_APPOINTMENTS
		INNER JOIN PDR_JOURNAL ON PDR_JOURNAL.JOURNALID = PDR_APPOINTMENTS.JournalId 
		INNER JOIN PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId
		INNER JOIN PDR_MSATType on PDR_MSAT.MSATTypeId=PDR_MSATType.MSATTypeId
		LEFT JOIN P__PROPERTY ON PDR_MSAT.PROPERTYID = P__PROPERTY.PROPERTYID 
		INNER JOIN PDR_STATUS ON PDR_STATUS.STATUSID = PDR_JOURNAL.STATUSID
	WHERE 
		Convert(date,APPOINTMENTSTARTDATE,103) >= Convert(date,@startDate,103)
		AND (PDR_STATUS.TITLE <> 'Cancelled'
		AND APPOINTMENTSTATUS NOT IN ('Complete', 'Cancelled'))		
		AND PDR_APPOINTMENTS.ASSIGNEDTO in (SELECT employeeid FROM  #AvailableOperatives)		
				
	DROP TABLE #AvailableOperatives		

END




/****** Object:  StoredProcedure [dbo].[AS_PropertyActivities]    Script Date: 07/06/2015 16:59:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
          
/* =============================================            
 --EXEC AS_PropertyActivities @propertyId ='B060080001'            
 -- ,@actionType = -1            
 -- ,@sortColumn = 'JournalHistoryId'            
 -- ,@sortOrder = 'DESC'           
-- Author:  <Noor Muhammad>            
-- Create date: <10/1/2012>            
-- Description: <This stored procedure returns the all the activities against the property>            
-- Parameters:             
  --@propertyId varchar(50),            
  --@actionType int = -1,               
               
  ---- column name on which sorting is performed            
  --@sortColumn varchar(50) = 'JournalHistoryId',            
  --@sortOrder varchar (5) = 'DESC'            
-- Webpage :PropertyRecord.aspx            
-- Modified By: <April 20, 2103, Aamir Waheed>            
-- Modified By: Abdullah Saeed <August 15,2014>          
-- ============================================= */    
ALTER PROCEDURE [dbo].[AS_PropertyActivities]
(             
  @propertyId varchar(50),            
  @actionType int = -1,                
  -- column name on which sorting is performed            
  @sortColumn varchar(50) = 'CreateDate',            
  @sortOrder varchar (5) = 'DESC'            
              
)            
AS            
BEGIN    
            
            
 DECLARE @PlannedSelectClause NVARCHAR(MAX),            
   @PlannedFromClause NVARCHAR(MAX),            
   @PlannedWhereClause NVARCHAR(MAX),            
   @PlannedOrderClause NVARCHAR(MAX),           
   @PdrSelectClause NVARCHAR(MAX),          
   @PdrFromClause NVARCHAR(MAX),          
   @PdrWhereClause NVARCHAR(MAX),          
   @PdrOrderClause NVARCHAR(MAX),     
             
   @StockConditionSelectClause NVARCHAR(MAX),          
   @StockConditionFromClause NVARCHAR(MAX),          
   @StockConditionWhereClause NVARCHAR(MAX),           
               
   @ConditionSelectClause NVARCHAR(MAX),            
   @ConditionFromClause NVARCHAR(MAX),            
   @ConditionWhereClause NVARCHAR(MAX),            
   @ConditionOrderClause NVARCHAR(MAX),           
               
   @unionClause varchar(100),            
   @mainSelectQuery Nvarchar(MAX),          
             
   @ApplianceSelectClause NVARCHAR(MAX),            
   @ApplianceFromClause NVARCHAR(MAX),            
   @ApplianceWhereClause NVARCHAR(MAX),            
   @ApplianceOrderClause NVARCHAR(MAX) ,                
   @orderClause  varchar(100),            
   @FaultSelectClause NVARCHAR(MAX),          
   @FaultFromClause NVARCHAR(MAX),          
   @FaultWhereClause NVARCHAR(MAX)  ,
   
   @GasElectricSelectClause NVARCHAR(MAX),          
   @GasElectricFromClause NVARCHAR(MAX),          
   @GasElectricWhereClause NVARCHAR(MAX),          
   @GasElectricOrderClause NVARCHAR(MAX),
   
   
   @PaintPackSelectClause NVARCHAR(MAX),          
   @PaintPackFromClause NVARCHAR(MAX),          
   @PaintPackWhereClause NVARCHAR(MAX),          
   @PaintPackOrderClause NVARCHAR(MAX),
      
   @Inspection varchar(100)
  
--===========================================================================================            
--       INSPECTION DESCRIPTION
--===========================================================================================    
  
  SELECT	@Inspection = P.Description FROM P_INSPECTIONTYPE P WHERE P.InspectionTypeID = @actionType
  
--===========================================================================================          
--       CONDITION ACTIVITIES            
--===========================================================================================            
    
SET @ConditionSelectClause = '          
  SELECT [PCW].ConditionWorksHistoryId AS JournalHistoryId          
  ,ISNULL(COALESCE(PC.COMPONENTNAME,[I].ItemName) +'' - ''+ PV.ValueDetail,''N/A'') AS Status          
  , CASE  WHEN [PLA].Title =''Rejected'' THEN  
  [PLA].Title + '' - '' + [PCWR].[RejectionNotes]  
 ELSE  
  ISNULL([PLA].Title,''N/A'')   
 END AS Action  
  ,''Condition Works'' AS InspectionType            
  ,CONVERT(varchar(10), [PCW].CreatedDate , 103) AS CreateDate          
  ,SUBSTRING(E.FIRSTNAME,1,1) + '' '' +E.LASTNAME AS Name           
  ,0 AS IsLetterAttached            
  ,0 AS IsDocumentAttached            
  ,''None'' as Document            
  ,0 as CP12DocumentID            
  ,[PCW].CreatedDate AS CREATIONDATE  
  ,(''JSN'' + CONVERT(VARCHAR,A.APPOINTMENTID) ) AS REF   
  ,Convert( NVarchar, A.APPOINTMENTDATE, 103 ) As AppointmentDate   
  ,(IsNULL( OP.FIRSTNAME,'''') + '' '' +Isnull(OP.LASTNAME,'''' ))  As OPERATIVENAME  
  ,T.Description As  OPERATIVETRADE 
  ,[PCW].ConditionWorksId As JournalId 
  ' + CHAR(10)    
    
SET @ConditionFromClause = ' FROM PLANNED_CONDITIONWORKS_HISTORY  [PCW]           
  INNER JOIN PLANNED_Action [PLA] ON [PCW].ConditionAction = [PLA].ActionId           
  INNER JOIN PA_PROPERTY_ATTRIBUTES [ATT] ON [PCW].AttributeId = [ATT].ATTRIBUTEID           
  INNER JOIN PA_ITEM_PARAMETER [IP] ON IP.ItemParamID = [ATT].ITEMPARAMID            
  INNER JOIN PA_ITEM [I] ON [I].ItemId = [IP].ItemId           
  INNER JOIN PA_PARAMETER_VALUE PV ON [PCW].VALUEID = PV.ValueID              
  
  LEFT JOIN PLANNED_CONDITIONWORKS CW ON CW.ConditionWorksId = PCW.ConditionWorksId  
  LEFT JOIN PLANNED_JOURNAL J ON CW.JournalId = J.JOURNALID  
  LEFT JOIN PLANNED_APPOINTMENTS A ON A.JournalId = J.JOURNALID  
  LEFT JOIN E__EMPLOYEE OP ON OP.EMPLOYEEID = A.ASSIGNEDTO  
  LEFT JOIN PLANNED_MISC_TRADE MT ON MT.AppointmentId = A.APPOINTMENTID  
  LEFT JOIN G_TRADE T ON T.TradeId = MT.TradeId   
  
  LEFT JOIN E__EMPLOYEE E ON [PCW].CreatedBy = E.EMPLOYEEID   
  LEFT JOIN PA_PROPERTY_ITEM_DATES [PID] ON [PID].ItemId = [IP].ItemID AND [PID].PROPERTYID = [ATT].PROPERTYID AND ([PID].ParameterId = [IP].ParameterId OR [PID].ParameterId IS NULL)          
  LEFT JOIN PLANNED_COMPONENT [PC] ON [PC].COMPONENTID = [PCW].COMPONENTID  
  LEFT JOIN PLANNED_CONDITIONWORKS_REJECTED [PCWR] ON [PCWR].ConditionWorksId = [PCW].ConditionWorksId '  
      
+ CHAR(10)    
    
    
SET @ConditionWhereClause = 'WHERE [ATT].PROPERTYID = ''' + @propertyId + ''''    
+ CHAR(10)    
    
  PRINT '--===================================================================='
  --PRINT @ConditionSelectClause + @ConditionFromClause + @ConditionWhereClause
--===========================================================================================            
--       PLANNED MAINTENANCE ACTIVITIES            
--===========================================================================================            
   
SET @PlannedSelectClause = 'SELECT DISTINCT PJH.JOURNALHISTORYID as JournalHistoryId            
  ,ISNULL(S.TITLE, ''N/A'')   
  + CASE WHEN S.TITLE = ''Inspection Arranged'' THEN ISNULL('' ('' + SUBSTRING(OP.FIRSTNAME, 1, 1) + '' '' + OP.LASTNAME  
  + '' '' + CONVERT(NVARCHAR,PIA.APPOINTMENTDATE,103)   
  + '' '' + PIA.APPOINTMENTTIME + '')'','''') ELSE '''' END AS Status            
  ,ISNULL(ACT.Title ,''N/A'' ) AS Action            
  ,''Planned - ''+ COALESCE(C.COMPONENTNAME,AT.Planned_Appointment_Type,''N/A'') AS InspectionType            
  ,Convert(varchar(10),PJH.CREATIONDATE ,103) as CreateDate            
  ,ISNULL(E.FIRSTNAME,'''') +'' ''+ ISNULL(E.lastname,'''')  AS Name            
  ,ISNULL(IsLetterAttached, 0) AS IsLetterAttached            
  ,ISNULL(IsDocumentAttached, 0) AS IsDocumentAttached            
  ,''None'' as Document            
  ,0 as CP12DocumentID            
  ,PJH.CREATIONDATE AS CREATIONDATE  
  ,( ''JSN'' + Convert( NVarchar, A.APPOINTMENTID ) ) AS REF   
  ,Convert( NVarchar, A.APPOINTMENTDATE, 103 ) As AppointmentDate   
  ,ISNULL(SUBSTRING( OP.FIRSTNAME,1,1),'''') +'' ''+ ISNULL(OP.lastname,'''') As OPERATIVENAME  
  ,T.Description As OPERATIVETRADE 
   ,PJH.JOURNALID As JournalId 
  ' + CHAR(10)    
    
SET @PlannedFromClause = ' FROM PLANNED_JOURNAL_HISTORY PJH      
  LEFT JOIN PLANNED_COMPONENT C ON PJH.COMPONENTID = C.COMPONENTID             
  INNER JOIN PLANNED_STATUS S ON  PJH.STATUSID = S.STATUSID             
  LEFT JOIN PLANNED_Action ACT ON  PJH.ACTIONID = ACT.ActionId             
  LEFT JOIN E__EMPLOYEE E ON PJH.CREATEDBY = E.EMPLOYEEID      
  LEFT JOIN PLANNED_APPOINTMENTS_HISTORY AH ON PJH.JOURNALHISTORYID = AH.JOURNALHISTORYID      
  LEFT JOIN PLANNED_APPOINTMENTS A ON A.APPOINTMENTID = AH.APPOINTMENTID      
  LEFT JOIN Planned_Appointment_Type AT ON AT.Planned_Appointment_TypeId = A.Planned_Appointment_TypeId   
  LEFT JOIN PLANNED_INSPECTION_APPOINTMENTS PIA ON PIA.JournalId = PJH.JOURNALID  
  LEFT JOIN E__EMPLOYEE OP ON OP.EMPLOYEEID = PIA.ASSIGNEDTO OR OP.EMPLOYEEID = AH.ASSIGNEDTO    
  LEFT JOIN PLANNED_COMPONENT_TRADE CT ON CT.COMPTRADEID = A.COMPTRADEID  
  LEFT JOIN PLANNED_MISC_TRADE MT ON MT.AppointmentId = A.APPOINTMENTID  
  LEFT JOIN G_TRADE T ON T.TRADEID = CT.TRADEID OR T.TradeId = MT.TradeId '  
+ CHAR(10)    
    
SET @PlannedWhereClause = 'WHERE PJH.PROPERTYID = ''' + @propertyId + ''''    
+ CHAR(10)    
      PRINT '--===================================================================='
  ---PRINT @PlannedSelectClause + @PlannedFromClause + @PlannedWhereClause
  
--===========================================================================================          
--       PDR ACTIVITIES          
--===========================================================================================          

SET @PdrSelectClause = 'SELECT DISTINCT PJH.JOURNALHISTORYID as JournalHistoryId          
  ,ISNULL(S.TITLE, ''N/A'')	 AS Status          
  ,''N/A''  AS Action          
  ,MSATTYPE.MSATTypeName +'' - ''+ ISNULL(PL.LocationName +'' > ''+ PA.AreaName + '' > '' + PT.ItemName,''N/A'') AS InspectionType          
  ,Convert(varchar(10),PJH.CREATIONDATE ,103) as CreateDate          
  ,ISNULL(substring(E.FIRSTNAME,1,1),'''') +'' ''+ ISNULL(E.lastname,'''')  AS Name          
  ,0 AS IsLetterAttached          
  ,0 AS IsDocumentAttached          
  ,''None'' as Document          
  ,0 as CP12DocumentID          
  ,PJH.CREATIONDATE AS CREATIONDATE   
  ,( ''JSN'' + Convert( NVarchar, A.APPOINTMENTID ) ) AS REF   
  ,Convert( NVarchar, A.APPOINTMENTSTARTDATE, 103 ) As AppointmentDate   
  ,ISNULL(SUBSTRING( OP.FIRSTNAME,1,1),'''') +'' ''+ ISNULL(OP.lastname,'''') As OPERATIVENAME  
  ,T.Description As OPERATIVETRADE
  ,PJH.JOURNALID As JournalId 
   ' + CHAR(10)  
  
SET @PdrFromClause = ' FROM PDR_JOURNAL_HISTORY PJH
	INNER JOIN PDR_MSAT MSAT ON PJH.MSATID = MSAT.MSATId
	INNER JOIN PDR_MSATType MSATTYPE ON MSAT.MSATTypeId = MSATTYPE.MSATTypeId 
	INNER JOIN PDR_STATUS S ON  PJH.STATUSID = S.STATUSID  
	INNER JOIN PA_ITEM PT ON MSAT.ItemId = PT.ItemID
	INNER JOIN PA_AREA PA ON  PT.AreaID = PA.AreaID
	INNER JOIN PA_LOCATION PL ON PA.LocationId = PL.LocationID        
	LEFT JOIN E__EMPLOYEE E ON PJH.CREATEDBY = E.EMPLOYEEID
	LEFT JOIN PDR_APPOINTMENT_HISTORY AH ON PJH.JOURNALHISTORYID = AH.JOURNALHISTORYID      
	LEFT JOIN PDR_APPOINTMENTS A ON A.APPOINTMENTID = AH.APPOINTMENTID  
	LEFT JOIN E__EMPLOYEE OP ON OP.EMPLOYEEID = AH.ASSIGNEDTO 
	LEFT JOIN G_TRADE T ON AH.TRADEID = T.TRADEID
	    ' + CHAR(10)  
  
  
SET @PdrWhereClause = 'WHERE MSAT.PROPERTYID = ''' + @propertyId + ''''  

+ CHAR(10)  

IF @actionType != -1
BEGIN
	SET @PdrWhereClause = @PdrWhereClause+ ' AND  MSATTYPE.MSATTypeName = '''+ @Inspection + ''''  
END
    
    PRINT '--===================================================================='
    --PRINT @PdrSelectClause + @PdrFromClause + @PdrWhereClause
--===========================================================================================            
--       APPLIANCES ACTIVITIES            
--===========================================================================================       
    
SET @ApplianceSelectClause = 'SELECT DISTINCT            
      AS_JOURNALHISTORY.JOURNALHISTORYID as JournalHistoryId            
      ,ISNULL(as_status.title, ''N/A'') as Status            
      ,ISNULL(as_action.title, ''N/A'') as Action            
      ,ISNULL(P_INSPECTIONTYPE.description, ''N/A'') AS InspectionType            
      ,Convert(varchar(10),AS_JOURNALHISTORY.creationdate,103) as CreateDate            
      ,ISNULL(e__employee.FIRSTNAME,'''') +'' ''+ ISNULL(e__employee.lastname,'''')  AS Name            
      ,ISNULL(IsLetterAttached, 0) AS IsLetterAttached            
      ,ISNULL(IsDocumentAttached, 0) AS IsDocumentAttached 
      ,CASE WHEN P_LGSR_HISTORY.CP12DOCUMENT IS NOT NULL AND as_status.title = ''CP12 issued''  THEN ''cp12.pdf'' ELSE ''None'' END Document 
      ,CASE WHEN P_LGSR_HISTORY.CP12DOCUMENT IS NULL THEN 0 ELSE P_LGSR_HISTORY.LGSRID END AS CP12DocumentID            
      ,AS_JOURNALHISTORY.CREATIONDATE AS CREATIONDATE  
      , ''JSG'' + convert(NVARCHAR, AS_APPOINTMENTSHISTORY.JSGNUMBER ) AS REF   
      ,( Convert( NVarchar, AS_APPOINTMENTSHISTORY.APPOINTMENTDATE, 103 ) + '' '' + AS_APPOINTMENTSHISTORY.APPOINTMENTSTARTTIME + ''-'' + AS_APPOINTMENTSHISTORY.APPOINTMENTENDTIME ) As AppointmentDate   
      ,( IsNULL(SUBSTRING( OP.FIRSTNAME,1,1), '''') + '' '' + isnull( OP.LASTNAME, '''' ) ) As OPERATIVENAME   
      ,''N/A'' As OPERATIVETRADE 
      ,AS_JOURNALHISTORY.JOURNALID As JournalId   
      ' + CHAR(10)    
    
SET @ApplianceFromClause = 'FROM AS_JOURNALHISTORY
INNER JOIN P_INSPECTIONTYPE ON AS_JOURNALHISTORY.INSPECTIONTYPEID = P_INSPECTIONTYPE.INSPECTIONTYPEID
INNER JOIN as_status ON as_status.statusid = AS_JOURNALHISTORY.statusid
LEFT JOIN AS_Action ON AS_Action.actionid = AS_JOURNALHISTORY.actionid
INNER JOIN e__employee ON e__employee.employeeid = AS_JOURNALHISTORY.createdby
LEFT JOIN (SELECT LGSRHISTORYID AS LGSRID, PROPERTYID,CP12DOCUMENT,JOURNALID
FROM P_LGSR_HISTORY WHERE CP12DOCUMENT IS NOT NULL) AS P_LGSR_HISTORY
	ON P_LGSR_HISTORY.JOURNALID = AS_JOURNALHISTORY.JOURNALID
LEFT JOIN AS_APPOINTMENTSHISTORY ON AS_JOURNALHISTORY.JOURNALHISTORYID = AS_APPOINTMENTSHISTORY.JOURNALHISTORYID
LEFT JOIN E__EMPLOYEE OP ON AS_APPOINTMENTSHISTORY.ASSIGNEDTO = OP.EMPLOYEEID
'    
+ CHAR(10)    
    
    
SET @ApplianceWhereClause = 'WHERE (' + CONVERT(NVARCHAR, @actionType) + '= -1 ' + ' OR as_journalhistory.inspectiontypeid = ' + CONVERT(NVARCHAR, @actionType) + ')            
       AND AS_JOURNALHISTORY.PROPERTYID= ''' + @propertyId + ''''    
+ CHAR(10)    
    
  PRINT '--===================================================================='
    PRINT @ApplianceSelectClause + @ApplianceFromClause + @ApplianceWhereClause
  
    
--===========================================================================================          
--       FAULT APPOINTMENTS          
--===========================================================================================          
    
SET @FaultSelectClause = 'SELECT DISTINCT FL_FAULT_LOG_HISTORY.FaultLogHistoryID as JournalHistoryId            
        ,ISNULL(FL_FAULT_STATUS.[Description],''N/A'' ) AS Status            
        ,''N/A''  AS Action            
        ,''Fault Repair'' AS InspectionType            
        ,Convert(varchar(10),SubmitDate ,103) as CreateDate            
        ,ISNULL(E__EMPLOYEE.FIRSTNAME,'''') +'' '' + ISNULL(E__EMPLOYEE.lastname,'' '')  AS Name            
        ,0 AS IsLetterAttached            
        ,0 AS IsDocumentAttached            
        ,''None'' as Document            
        ,0 as CP12DocumentID            
        ,SubmitDate AS CREATIONDATE   
        ,FL_FAULT_LOG.JobSheetNumber  AS REF   
        ,Convert( NVarchar, FL_CO_APPOINTMENT.AppointmentDate, 103 ) As AppointmentDate   
        ,( ISNULL(SUBSTRING(E__EMPLOYEE.FIRSTNAME,1,1),'''') +'' '' + ISNULL(E__EMPLOYEE.lastname,'' '') ) As OPERATIVENAME  
        ,T.Description As OPERATIVETRADE 
         ,FL_FAULT_LOG_HISTORY.JournalID As JournalId 
        ' + CHAR(10)    
  
SET @FaultFromClause = 'from FL_FAULT_LOG_HISTORY  
      Inner Join FL_FAULT_LOG On FL_FAULT_LOG.FaultLogID = FL_FAULT_LOG_HISTORY.FaultLogID           
      LEFT join FL_FAULT_APPOINTMENT on FL_FAULT_APPOINTMENT.FaultLogId = FL_FAULT_LOG.FaultLogID           
      INNER JOIN FL_FAULT_STATUS on FL_FAULT_LOG_HISTORY.FaultStatusID = FL_FAULT_STATUS.FaultStatusID          
      INNER JOIN E__EMPLOYEE On EMPLOYEEID = UserId     
      LEFT JOIN FL_CO_APPOINTMENT ON FL_CO_APPOINTMENT.AppointmentID = FL_FAULT_APPOINTMENT.AppointmentID  
      LEFT JOIN E__EMPLOYEE OP On OP.EMPLOYEEID = FL_CO_APPOINTMENT.OperativeID  
      LEFT JOIN FL_FAULT_TRADE ON FL_FAULT_TRADE.FaultTradeID = FL_FAULT_LOG.FaultTradeID  
      LEFT JOIN G_TRADE T ON FL_FAULT_TRADE.TradeId = T.TradeId  
      ' + CHAR(10)    
  
SET @FaultWhereClause = 'where  FL_FAULT_STATUS.FaultStatusID <>  13 AND FL_FAULT_LOG.PROPERTYID =''' + @propertyId + ''''    
+ CHAR(10)    
  
    PRINT '--===================================================================='
   -- PRINT @FaultSelectClause + @FaultFromClause + @FaultWhereClause
  
--===========================================================================================            
--       STOCK CONDITION  
--===========================================================================================            
SET @StockConditionSelectClause = 'select           
      0 as JournalHistoryId,          
      ''Inspection Completed'' As Status,          
      ''N/A'' as Action,          
      ''Stock Condition'' As InspectionType,          
      CONVERT(CHAR(10), PA_Property_Inspection_Record.CreatedDate, 103) As ''CreateDate'',          
      ISNULL(e__employee.FIRSTNAME,'''') +'' ''+ ISNULL(e__employee.lastname,'''')  AS Name  ,          
      0 AS IsLetterAttached,           
      CASE WHEN PA_Property_Inspection_Record.InspectionDocument IS NOT NULL Then 1 Else 0 END AS IsDocumentAttached,           
      ''None''as Document,          
      PA_Property_Inspection_Record.InspectionId  as CP12DocumentID,          
      PA_Property_Inspection_Record.CreatedDate as CREATIONDATE   
      ,''N/A'' AS REF   
      ,Convert( char, PA_Property_Inspection_Record.inspectionDate, 103 ) As AppointmentDate   
      ,( ISNULL(SUBSTRING( E__EMPLOYEE.FIRSTNAME,1,1),'''') +'' '' + ISNULL(E__EMPLOYEE.lastname,'''') ) As OPERATIVENAME  
       ,''N/A'' As OPERATIVETRADE 
       ,InspectionId As JournalId       
      ' + CHAR(10)    
    
SET @StockConditionFromClause = ' from PA_Property_Inspection_Record ' + CHAR(10) +    
   'LEFT JOIN E__EMPLOYEE on E__EMPLOYEE.EMPLOYEEID = PA_Property_Inspection_Record.createdBy   
    LEFT JOIN PS_Appointment2Survey ON PS_Appointment2Survey.SurveyId = PA_Property_Inspection_Record.SurveyId  
    LEFT JOIN PS_Appointment ON PS_Appointment2Survey.AppointId = PS_Appointment.AppointId  
    LEFT JOIN E__EMPLOYEE OP ON OP.EMPLOYEEID = PS_Appointment.SurveyourUserName  
   '    
  
SET @StockConditionWhereClause = 'where PA_Property_Inspection_Record.PropertyId=''' + @propertyId + '''' + CHAR(10) 

  PRINT '--===================================================================='
  --PRINT @StockConditionSelectClause + @StockConditionFromClause + @StockConditionWhereClause
 
 
 
--===========================================================================================          
--       Gas Electric ACTIVITIES          
--===========================================================================================          

SET @GasElectricSelectClause = 'SELECT DISTINCT PJH.JOURNALHISTORYID as JournalHistoryId          
  ,ISNULL(S.TITLE, ''N/A'')	 AS Status          
  ,''N/A''  AS Action          
  ,MSATTYPE.MSATTypeName  AS InspectionType          
  ,Convert(varchar(10),PJH.CREATIONDATE ,103) as CreateDate          
  ,ISNULL(substring(E.FIRSTNAME,1,1),'''') +'' ''+ ISNULL(E.lastname,'''')  AS Name          
  ,0 AS IsLetterAttached          
  ,0 AS IsDocumentAttached          
  ,''None'' as Document          
  ,0 as CP12DocumentID          
  ,PJH.CREATIONDATE AS CREATIONDATE   
  ,( ''JSV'' + Convert( NVarchar, PJH.JournalID ) ) AS REF   
  ,Convert( NVarchar, A.APPOINTMENTSTARTDATE, 103 ) As AppointmentDate   
  ,ISNULL(SUBSTRING( OP.FIRSTNAME,1,1),'''') +'' ''+ ISNULL(OP.lastname,'''') As OPERATIVENAME  
  ,T.Description As OPERATIVETRADE
  ,PJH.JOURNALID As JournalId
   ' + CHAR(10)  
  
SET @GasElectricFromClause = ' FROM PDR_JOURNAL_HISTORY PJH
	INNER JOIN PDR_MSAT MSAT ON PJH.MSATID = MSAT.MSATId
	INNER JOIN PDR_MSATType MSATTYPE ON MSAT.MSATTypeId = MSATTYPE.MSATTypeId 
	INNER JOIN PDR_STATUS S ON  PJH.STATUSID = S.STATUSID  	      
	LEFT JOIN E__EMPLOYEE E ON PJH.CREATEDBY = E.EMPLOYEEID
	LEFT JOIN PDR_APPOINTMENT_HISTORY AH ON PJH.JOURNALHISTORYID = AH.JOURNALHISTORYID      
	LEFT JOIN PDR_APPOINTMENTS A ON A.APPOINTMENTID = AH.APPOINTMENTID  
	LEFT JOIN E__EMPLOYEE OP ON OP.EMPLOYEEID = AH.ASSIGNEDTO 
	LEFT JOIN G_TRADE T ON AH.TRADEID = T.TRADEID	
	    ' + CHAR(10)  
  
  
SET @GasElectricWhereClause = 'WHERE MSAT.PROPERTYID = ''' + @propertyId + ''''  + CHAR(10)  
SET @GasElectricWhereClause = @GasElectricWhereClause+ ' AND ( MSATTYPE.MSATTypeName Like ''%Gas Check%'''  
SET @GasElectricWhereClause = @GasElectricWhereClause+ ' OR  MSATTYPE.MSATTypeName Like ''%Electric Check%'')'  
    
    PRINT '--===================================================================='
   -- PRINT @GasElectricSelectClause + @GasElectricFromClause + @GasElectricWhereClause 
 
--===========================================================================================          
--       Paint Pack ACTIVITIES          
--===========================================================================================          

SET @PaintPackSelectClause = 'SELECT  P.PaintPackId as JournalHistoryId          
  ,ISNULL(PS.TITLE, ''N/A'')	 AS Status          
  ,''N/A''  AS Action          
  ,''Void Paint Pack''  AS InspectionType          
  ,Convert(varchar(10),P.CreatedDate ,103) as CreateDate          
  ,ISNULL(substring(E.FIRSTNAME,1,1),'''') +'' ''+ ISNULL(E.lastname,'''')  AS Name          
  ,0 AS IsLetterAttached          
  ,0 AS IsDocumentAttached          
  ,''None'' as Document          
  ,0 as CP12DocumentID          
  ,P.CreatedDate AS CREATIONDATE   
  ,'''' AS REF   
  ,'''' As AppointmentDate   
  ,'''' As OPERATIVENAME  
  ,''''As OPERATIVETRADE
  ,P.InspectionJournalId As JournalId
   ' + CHAR(10)  
  
SET @PaintPackFromClause = ' FROM PDR_JOURNAL J
		INNER JOIN V_PaintPack P ON J.JOURNALID=P.InspectionJournalId
		INNER JOIN V_PaintStatus PS ON P.StatusId=PS.StatusId 
		INNER JOIN	PDR_MSAT ON J.MSATID = PDR_MSAT.MSATId
		INNER JOIN	PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId		
		INNER JOIN	P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID			
		INNER JOIN E__EMPLOYEE E ON E.EMPLOYEEID = P.CreatedBy	
	    ' + CHAR(10)  
  
  
SET @PaintPackWhereClause = 'WHERE PDR_MSAT.PROPERTYID = ''' + @propertyId + ''''  + CHAR(10)  
--==============================================================================================        
  
--Set union Clause          
SET @unionClause = CHAR(10) + CHAR(9) + 'UNION ALL' + CHAR(10) + CHAR(9)  
--========================================================================================            
--Set ORDER Clause				
SET @OrderClause = 'order by ' + @sortColumn + ' ' + @sortOrder + CHAR(10) 
IF @sortColumn = 'CREATIONDATE'
	Begin
		SET	@OrderClause = @OrderClause + ', JournalHistoryId ' + @sortOrder + CHAR(10) 
	END
--======================================================================================== 
   
/* @actionType  
 -1 => All (Default)  
 1 => Appliance Servicing  
 2 => Reactive  
 3 => Stock  
 4 => Void  
 5 => Planned  
 6 => Condition Works 
 7 => M&E Servicing
 8 => Cyclic Maintenance
 9 => PAT Testing 
*/  
  
IF (@actionType = 1)   
 SET @mainSelectQuery = @ApplianceSelectClause + @ApplianceFromClause + @ApplianceWhereClause + @OrderClause   
ELSE IF (@actionType = 2)  
 SET @mainSelectQuery = @FaultSelectClause + @FaultFromClause + @FaultWhereClause + @OrderClause  
ELSE IF (@actionType = 3)  
 SET @mainSelectQuery = @StockConditionSelectClause + @StockConditionFromClause + @StockConditionWhereClause + @OrderClause  
ELSE IF (@actionType = 5)  
 SET @mainSelectQuery = @PlannedSelectClause + @PlannedFromClause + @PlannedWhereClause + @OrderClause  
ELSE IF (@actionType = 6)  
 SET @mainSelectQuery = @ConditionSelectClause + @ConditionFromClause + @ConditionWhereClause + @OrderClause  
ELSE IF (@actionType = 7 or @actionType = 8 or @actionType = 9)  
 SET @mainSelectQuery = @PdrSelectClause + @PdrFromClause + @PdrWhereClause + @OrderClause 
ELSE SET @mainSelectQuery = @PlannedSelectClause + @PlannedFromClause + @PlannedWhereClause  
 + @unionClause  
 + @ApplianceSelectClause + @ApplianceFromClause + @ApplianceWhereClause  
 + @unionClause  
 + @FaultSelectClause + @FaultFromClause + @FaultWhereClause  
 + @unionClause  
 + @StockConditionSelectClause + @StockConditionFromClause + @StockConditionWhereClause  
 + @unionClause  
 + @ConditionSelectClause + @ConditionFromClause + @ConditionWhereClause  
 + @unionClause  
 + @PdrSelectClause + @PdrFromClause + @PdrWhereClause 
 + @unionClause  
 + @GasElectricSelectClause + @GasElectricFromClause + @GasElectricWhereClause 
 + @unionClause  
 + @PaintPackSelectClause + @PaintPackFromClause + @PaintPackWhereClause 
   
 + @OrderClause  
 
   PRINT '====================================================================' 
PRINT @mainSelectQuery  
EXEC (@mainSelectQuery)  
  
END



/****** Object:  StoredProcedure [dbo].[AS_AppointmentsCalendarDetail]    Script Date: 10/24/2014 17:44:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- EXEC AS_AppointmentsCalendarDetail @startDate='2012-12-31',@endDate='2014-01-04'  
-- Author:  Aqib Javed  
-- Create date: <9/21/2012>  
-- Last Modified: <04/01/2013>  
-- Description: <Description,Provided information of Scheduled Appointments of Engineers on Weekly basis>  
-- Webpage : WeeklyCalendar.aspx, MonthlyCalendar.aspx  
  
-- =============================================  
ALTER PROCEDURE [dbo].[AS_AppointmentsCalendarDetail]
    @startDate DATE ,
    @endDate DATE
AS 
    BEGIN  
;
        WITH    CTE_Appointment ( APPOINTMENTSHIFT, APPOINTMENTDATE, NAME, MOBILE, CUSTADDRESS, ASSIGNEDTO, POSTCODE, TITLE, APPOINTMENTSTARTTIME, APPOINTMENTENDTIME, [DESCRIPTION], [TYPE], APPOINTMENTENDDATE )
                  AS (			
                  
                  
-- ==========================================================================================    
-- PDR And Void APPOINTMENTS
-- ==========================================================================================  
                  
                  
                  SELECT   
								DISTINCT                    CASE WHEN DATEPART(HOUR,
                                                   PDR_APPOINTMENTS.APPOINTMENTSTARTTIME) < 12
                                     THEN 'AM'
                                     ELSE 'PM'
                                END AS APPOINTMENTSHIFT ,
                                CONVERT(VARCHAR, APPOINTMENTSTARTDATE, 103) AS APPOINTMENTDATE ,
                                ISNULL(CG.LIST, 'N/A') AS NAME ,
                                '' AS MOBILE 
                                ,CASE
								WHEN PDR_MSAT.propertyid IS NOT NULL THEN
									ISNULL(P__PROPERTY.HouseNumber, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS1, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS2, '') + ' ' + ISNULL(P__PROPERTY.ADDRESS3, '')
								WHEN PDR_MSAT.SCHEMEID IS NOT NULL THEN
									ISNULL(P_SCHEME.SCHEMENAME , '') 		
								ELSE
									ISNULL(P_BLOCK.BLOCKNAME, '') + ' '
									+ ISNULL(P_BLOCK.ADDRESS1 , '') 
									+ ISNULL(', '+P_BLOCK.ADDRESS2, '') 
									+ ISNULL( ' '+P_BLOCK.ADDRESS3, '')
								END AS CUSTADDRESS ,
                                PDR_APPOINTMENTS.ASSIGNEDTO 
                                ,COALESCE(p__property.postcode,P_SCHEME.SCHEMECODE,P_BLOCK.POSTCODE) POSTCODE ,
                                PDR_STATUS.TITLE ,
                                PDR_APPOINTMENTS.APPOINTMENTSTARTTIME ,
                                PDR_APPOINTMENTS.APPOINTMENTENDTIME ,
                                P_STATUS.[DESCRIPTION] ,
                                ISNULL(PDR_MSATTYPE.MSATTypeName,'') AS Type ,
                                CONVERT(VARCHAR, APPOINTMENTENDDATE, 103) AS APPOINTMENTENDDATE
                       FROM     PDR_APPOINTMENTS
                                INNER JOIN PDR_JOURNAL ON PDR_APPOINTMENTS.JOURNALID = PDR_JOURNAL.JOURNALID
                                INNER JOIN	PDR_Status ON PDR_JOURNAL.StatusId = PDR_Status.StatusId AND PDR_STATUS.TITLE Not In ('Cancelled','To be Arranged')
								INNER JOIN PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATID
								INNER JOIN PDR_MSATTYPE ON PDR_MSAT.MSATTYPEID = PDR_MSATTYPE.MSATTYPEID
								LEFT JOIN	P_SCHEME ON PDR_MSAT.SchemeId = P_SCHEME.SCHEMEID 
								LEFT JOIN	P_BLOCK ON PDR_MSAT.BlockId = P_BLOCK.BLOCKID
								LEFT JOIN	P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID
								LEFT JOIN P_STATUS ON P__PROPERTY.[STATUS] = P_STATUS.STATUSID
								LEFT JOIN C_TENANCY ON PDR_MSAT.TenancyId = C_TENANCY.TENANCYID
                                --LEFT JOIN C_TENANCY ON P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID
                                --                       AND ( DBO.C_TENANCY.ENDDATE IS NULL
                                --                             OR DBO.C_TENANCY.ENDDATE > GETDATE()
                                --                           )
                                LEFT JOIN C_CUSTOMER_NAMES_GROUPED CG ON CG.I = DBO.C_TENANCY.TENANCYID
                                                              AND CG.ID IN (
                                                              SELECT
                                                              MAX(ID) ID
                                                              FROM
                                                              C_CUSTOMER_NAMES_GROUPED
                                                              GROUP BY I )
                       WHERE    		PDR_APPOINTMENTS.APPOINTMENTSTARTDATE BETWEEN CONVERT(DATE, @startDate, 103) AND CONVERT(DATE, @endDate, 103)
										OR PDR_APPOINTMENTS.APPOINTMENTENDDATE BETWEEN CONVERT(DATE, @startDate, 103) AND CONVERT(DATE, @endDate, 103)
										OR (PDR_APPOINTMENTS.APPOINTMENTSTARTDATE <= CONVERT(DATE, @startDate, 103) AND PDR_APPOINTMENTS.APPOINTMENTENDDATE >= CONVERT(DATE, @endDate, 103))
       
-- ==========================================================================================    
-- PLANNED APPOINTMENTS
-- ==========================================================================================  
                  
                      UNION ALL
                      
								SELECT   
								DISTINCT                    CASE WHEN DATEPART(HOUR,
                                                   PLANNED_APPOINTMENTS.APPOINTMENTSTARTTIME) < 12
                                     THEN 'AM'
                                     ELSE 'PM'
                                END AS APPOINTMENTSHIFT ,
                                CONVERT(VARCHAR, APPOINTMENTDATE, 103) AS APPOINTMENTDATE ,
                                ISNULL(CG.LIST, 'N/A') AS NAME ,
                                '' AS MOBILE ,
                                ISNULL(P__PROPERTY.HOUSENUMBER, '') + ' '
                                + ISNULL(P__PROPERTY.ADDRESS1, '') + ' '
                                + ISNULL(P__PROPERTY.ADDRESS2, '') + ' '
                                + ISNULL(P__PROPERTY.ADDRESS3, '') AS CUSTADDRESS ,
                                PLANNED_APPOINTMENTS.ASSIGNEDTO ,
                                P__PROPERTY.POSTCODE ,
                                PLANNED_STATUS.TITLE ,
                                PLANNED_APPOINTMENTS.APPOINTMENTSTARTTIME ,
                                PLANNED_APPOINTMENTS.APPOINTMENTENDTIME ,
                                P_STATUS.[DESCRIPTION] ,
                                'Planned' AS Type ,
                                CONVERT(VARCHAR, APPOINTMENTENDDATE, 103) AS APPOINTMENTENDDATE
                       FROM     PLANNED_APPOINTMENTS
                                INNER JOIN PLANNED_JOURNAL ON PLANNED_APPOINTMENTS.JOURNALID = PLANNED_JOURNAL.JOURNALID
                                INNER JOIN PLANNED_STATUS ON PLANNED_JOURNAL.STATUSID = PLANNED_STATUS.STATUSID
                                INNER JOIN P__PROPERTY ON PLANNED_JOURNAL.PROPERTYID = P__PROPERTY.PROPERTYID
                                INNER JOIN P_STATUS ON P__PROPERTY.[STATUS] = P_STATUS.STATUSID
                                INNER JOIN PLANNED_COMPONENT_TRADE pcd ON PLANNED_APPOINTMENTS.COMPTRADEID = pcd.COMPTRADEID
                                LEFT JOIN C_TENANCY ON P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID
                                                       AND ( DBO.C_TENANCY.ENDDATE IS NULL
                                                             OR DBO.C_TENANCY.ENDDATE > GETDATE()
                                                           )
                                LEFT JOIN C_CUSTOMER_NAMES_GROUPED CG ON CG.I = DBO.C_TENANCY.TENANCYID
                                                              AND CG.ID IN (
                                                              SELECT
                                                              MAX(ID) ID
                                                              FROM
                                                              C_CUSTOMER_NAMES_GROUPED
                                                              GROUP BY I )
                       WHERE    		(PLANNED_APPOINTMENTS.APPOINTMENTDATE BETWEEN CONVERT(DATE, @startDate, 103) AND CONVERT(DATE, @endDate, 103)
										OR PLANNED_APPOINTMENTS.APPOINTMENTENDDATE BETWEEN CONVERT(DATE, @startDate, 103) AND CONVERT(DATE, @endDate, 103)
										OR (PLANNED_APPOINTMENTS.APPOINTMENTDATE <= CONVERT(DATE, @startDate, 103) AND PLANNED_APPOINTMENTS.APPOINTMENTENDDATE >= CONVERT(DATE, @endDate, 103)))
										AND APPOINTMENTSTATUS <> 'Cancelled' AND ISPENDING = 0
                     
-- ==========================================================================================    
-- APPLIANCE APPOINTMENTS
-- ==========================================================================================  
                     
                     
                       UNION ALL
                       SELECT DISTINCT
                                AS_APPOINTMENTS.APPOINTMENTSHIFT ,
                                CONVERT(VARCHAR, APPOINTMENTDATE, 103) AS APPOINTMENTDATE ,
                                ISNULL(CG.LIST, 'N/A') AS NAME  
    --,C__CUSTOMER.FIRSTNAME+'  '+C__CUSTOMER.LASTNAME AS Name       
                                ,
                                '' AS MOBILE --C_ADDRESS.MOBILE  
                                ,
                                ISNULL(P__PROPERTY.HOUSENUMBER, '') + ' '
                                + ISNULL(P__PROPERTY.ADDRESS1, '') + ' '
                                + ISNULL(P__PROPERTY.ADDRESS2, '') + ' '
                                + ISNULL(P__PROPERTY.ADDRESS3, '') AS CUSTADDRESS ,
                                AS_APPOINTMENTS.ASSIGNEDTO ,
                                P__PROPERTY.POSTCODE ,
                                AS_STATUS.TITLE ,
                                AS_APPOINTMENTS.APPOINTMENTSTARTTIME ,
                                AS_APPOINTMENTS.APPOINTMENTENDTIME ,
                                P_STATUS.[DESCRIPTION] ,
                                'GAS' AS Type ,
                                CONVERT(VARCHAR, APPOINTMENTDATE, 103) AS APPOINTMENTENDDATE
                       FROM     AS_APPOINTMENTS
                                INNER JOIN AS_JOURNAL ON AS_APPOINTMENTS.JOURNALID = AS_JOURNAL.JOURNALID
                                INNER JOIN AS_STATUS ON AS_JOURNAL.STATUSID = AS_STATUS.STATUSID
                                INNER JOIN P__PROPERTY ON AS_JOURNAL.PROPERTYID = P__PROPERTY.PROPERTYID
                                INNER JOIN P_STATUS ON P__PROPERTY.[STATUS] = P_STATUS.STATUSID
                                LEFT JOIN C_TENANCY ON P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID
                                                       AND ( DBO.C_TENANCY.ENDDATE IS NULL
                                                             OR DBO.C_TENANCY.ENDDATE > GETDATE()
                                                           )
                                LEFT JOIN C_CUSTOMER_NAMES_GROUPED CG ON CG.I = DBO.C_TENANCY.TENANCYID
                                                              AND CG.ID IN (
                                                              SELECT
                                                              MAX(ID) ID
                                                              FROM
                                                              C_CUSTOMER_NAMES_GROUPED
                                                              GROUP BY I )
                       WHERE    AS_JOURNAL.ISCURRENT = 1
                                AND AS_APPOINTMENTS.APPOINTMENTDATE BETWEEN @startDate
                                                              AND
                                                              @endDate
                     )
            SELECT  APPOINTMENTSHIFT ,
                    APPOINTMENTDATE ,
                    NAME ,
                    MOBILE ,
                    CUSTADDRESS ,
                    ASSIGNEDTO ,
                    POSTCODE ,
                    TITLE ,
                    APPOINTMENTSTARTTIME ,
                    APPOINTMENTENDTIME ,
                    [DESCRIPTION] ,
                    [TYPE] ,
                    APPOINTMENTENDDATE
            FROM    CTE_Appointment
            ORDER BY APPOINTMENTDATE ASC ,
                    APPOINTMENTSTARTTIME ASC
	
    END

/****** Object:  StoredProcedure [dbo].[AS_ScheduledAppointmentsDetail]    Script Date: 06/17/2013 15:53:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:           Aqib Javed
-- Create date:      23/09/2012
-- Description:      Performance Summary
-- History:          23/09/2012 Aqib :This Stored Proceedure shows the Appointment details on the Calendar Page
--                   13/07/2015 Ali Raza : Added All void appointments union
-- EXEC AS_ScheduledAppointmentsDetail @startDate='31/12/2013',@endDate='04/01/2013'
-- =============================================
ALTER PROCEDURE [dbo].[AS_ScheduledAppointmentsDetail]
@startDate date,
@endDate date	
AS
BEGIN

	-----------------------------------SELECT Gas / Appliance Servicing Appointments -----------
	--------------------------------------------------------------------------------------------------------
	SELECT DISTINCT 
		AS_APPOINTMENTS.APPOINTMENTSHIFT AS APPOINTMENTSHIFT,
		CONVERT(varchar, APPOINTMENTDATE,103) as AppointmentDate, 
		ISNULL(P__PROPERTY.HouseNumber,'') +' '+ ISNULL(P__PROPERTY.ADDRESS1,'') +' '+ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'') AS CustAddress,
		AS_APPOINTMENTS.ASSIGNEDTO AS ASSIGNEDTO,
		P__PROPERTY.POSTCODE AS PostCode,
		AS_Status.Title AS Title,
		AS_APPOINTMENTS.APPOINTMENTSTARTTIME AS APPOINTMENTSTARTTIME,
		AS_APPOINTMENTS.APPOINTMENTENDTIME AS APPOINTMENTENDTIME,
		AS_APPOINTMENTS.APPOINTMENTID AS APPOINTMENTID,
		datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), CONVERT(varchar, APPOINTMENTDATE,103),103) + ' ' + APPOINTMENTSTARTTIME,103)) as StartTimeInSec,
		datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), CONVERT(varchar, APPOINTMENTDATE,103),103) + ' ' + APPOINTMENTENDTIME,103)) as EndTimeInSec
		,CONVERT(varchar, APPOINTMENTDATE,103) as ValidateAppointmentDate
		,'Gas' as AppointmentType,
		C_TENANCY.TENANCYID AS TenancyId,
		P_STATUS.[DESCRIPTION] AS PropertyStatus


		FROM AS_APPOINTMENTS 
		INNER JOIN AS_JOURNAL ON AS_APPOINTMENTS.JournalId=AS_JOURNAL.JOURNALID 
		INNER JOIN AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId
		INNER JOIN P__PROPERTY ON AS_JOURNAL.PROPERTYID=P__PROPERTY.PROPERTYID 
		INNER JOIN P_STATUS  ON P__PROPERTY.[STATUS] =P_STATUS.STATUSID  
		LEFT JOIN C_TENANCY ON P__PROPERTY.PROPERTYID=C_TENANCY.PROPERTYID AND (DBO.C_TENANCY.ENDDATE IS NULL OR DBO.C_TENANCY.ENDDATE > GETDATE())
		WHERE AS_JOURNAL.IsCurrent = 1
		AND AS_APPOINTMENTS.APPOINTMENTDATE BETWEEN @startDate and @endDate
		-- Added Condition to filter only the appointment of Active gas operatives
		AND AS_APPOINTMENTS.ASSIGNEDTO IN ( SELECT DISTINCT EmployeeId FROM AS_USER WHERE UserTypeID = 3 AND IsActive  = 1 )
	
	-----------------------------------UNION SELECT Reactive Repair Appointments ---------------------
	---------------------------------------------------------------------------------------------------------
	
	UNION ALL
    
	SELECT DISTINCT substring(Time,6,len(Time)) as APPOINTMENTSHIFT
		,CONVERT(varchar(20), APPOINTMENTDATE,103) as AppointmentDate
		,ISNULL(P__PROPERTY.HouseNumber,'') +' '+ ISNULL(P__PROPERTY.ADDRESS1,'') 
				+' '+ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'') AS CustAddress
		,FL_CO_APPOINTMENT.OperativeID as ASSIGNEDTO
		,P__PROPERTY.POSTCODE
		,FL_FAULT_STATUS.[Description] as Title
		,FL_CO_APPOINTMENT.[Time] as APPOINTMENTSTARTTIME
		,FL_CO_APPOINTMENT.EndTime as APPOINTMENTENDTIME
		,FL_CO_APPOINTMENT.AppointmentID
		,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), appointmentdate,103) + ' ' + [Time],103)) as StartTimeInSec
		,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), appointmentdate,103) + ' ' + EndTime,103)) as EndTimeInSec
		,CONVERT(varchar(20), APPOINTMENTDATE,103) as ValidateAppointmentDate
		,'Reactive Repair' as AppointmentType	
		,C_TENANCY.TENANCYID
		,P_STATUS.[DESCRIPTION] 
		from FL_CO_APPOINTMENT 
		INNER JOIN FL_FAULT_APPOINTMENT ON FL_CO_APPOINTMENT.AppointmentID =FL_FAULT_APPOINTMENT.AppointmentId 
		INNER JOIN	FL_FAULT_LOG  ON FL_FAULT_APPOINTMENT.FaultLogId = FL_FAULT_LOG.FaultLogID 
		INNER JOIN	FL_FAULT_STATUS ON FL_FAULT_LOG.StatusID  = FL_FAULT_STATUS.FaultStatusID 
		INNER JOIN P__PROPERTY ON FL_FAULT_LOG.PROPERTYID=P__PROPERTY.PROPERTYID
		INNER JOIN P_STATUS  ON P__PROPERTY.[STATUS] =P_STATUS.STATUSID  
		LEFT JOIN C_TENANCY ON P__PROPERTY.PROPERTYID=C_TENANCY.PROPERTYID AND (DBO.C_TENANCY.ENDDATE IS NULL OR DBO.C_TENANCY.ENDDATE > GETDATE())
		
	WHERE 

		FL_CO_APPOINTMENT.APPOINTMENTDATE BETWEEN CONVERT(date, @startDate,103) and CONVERT(date, @endDate,103)
		AND FL_FAULT_STATUS.[Description]!='Cancelled'
		-- Added Condition to filter only the appointment of Active gas operatives
		AND FL_CO_APPOINTMENT.OPERATIVEID IN (SELECT DISTINCT EmployeeId FROM AS_USER WHERE UserTypeID = 3 AND IsActive  = 1)
		
	
	-----------------------------------UNION SELECT Stock ---------------------
	---------------------------------------------------------------------------------------------------------
	
	UNION ALL
	
	SELECT 
		RIGHT(Convert(VARCHAR(20), PS_Appointment.AppointStartDateTime,100),2) as APPOINTMENTSHIFT
		,PS_Appointment.AppointStartDateTime as AppointmentDate		
		,ISNULL(P__PROPERTY.HouseNumber,'') +' '+ ISNULL(P__PROPERTY.ADDRESS1,'') +' '+ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'') AS CustAddress
		,PS_Appointment.CreatedBy as ASSIGNEDTO
		,P__PROPERTY.POSTCODE
		,'' as Title
		,RIGHT(Convert(VARCHAR(20), PS_Appointment.AppointStartDateTime,100),8) as APPOINTMENTSTARTTIME
		,RIGHT(Convert(VARCHAR(20), PS_Appointment.AppointEndDateTime,100),8) as APPOINTMENTENDTIME		
		,PS_Appointment.AppointId as APPOINTMENTID
		,datediff(s, '1970-01-01', convert(datetime,AppointStartDateTime,103)) as StartTimeInSec
		,datediff(s, '1970-01-01', convert(datetime,AppointStartDateTime,103)) as EndTimeInSec	
		,CONVERT(varchar(20), AppointStartDateTime,103) as ValidateAppointmentDate	
		,PS_Appointment.AppointType as AppointmentType
		,C_TENANCY.TENANCYID  	
		,P_STATUS.[DESCRIPTION]  
	FROM PS_Appointment 
		INNER JOIN PS_Property2Appointment ON PS_Appointment.AppointId = PS_Property2Appointment.AppointId		
		INNER JOIN P__PROPERTY ON PS_Property2Appointment.PROPERTYID = P__PROPERTY.PROPERTYID
		INNER JOIN P_STATUS  ON P__PROPERTY.[STATUS] =P_STATUS.STATUSID  
		LEFT JOIN C_TENANCY ON P__PROPERTY.PROPERTYID=C_TENANCY.PROPERTYID AND (DBO.C_TENANCY.ENDDATE IS NULL OR DBO.C_TENANCY.ENDDATE > GETDATE())
	
	
	WHERE 
		(PS_Appointment.AppointType = 'Stock')	
		AND convert(date, PS_Appointment.AppointStartDateTime,103) BETWEEN CONVERT(date, @startDate,103) and CONVERT(date, @endDate,103)
		-- Added Condition to filter only the appointment of Active gas operatives
		AND convert(date, PS_Appointment.AppointStartDateTime,103) BETWEEN CONVERT(date, @startDate,103) and CONVERT(date, @endDate,103)
		AND PS_Appointment.SurveyourUserName IN (	SELECT DISTINCT PS_Appointment.SurveyourUserName 
													FROM PS_Appointment 
														INNER JOIN AC_LOGINS ON PS_Appointment.SurveyourUserName = AC_LOGINS.LOGIN 
														INNER JOIN AS_USER ON AC_LOGINS.EMPLOYEEID = AS_USER.EMPLOYEEID 
													WHERE AS_USER.UserTypeID = 3 AND AS_USER.IsActive  = 1 )
													
		
		
		-----------------------------------UNION SELECT  Void Appointments ---------------------
	---------------------------------------------------------------------------------------------------------
	
	UNION ALL	
		
		SELECT DISTINCT 
		RIGHT(Convert(VARCHAR(20), convert(datetime,convert(varchar(10), CONVERT(varchar, PDR_APPOINTMENTS.APPOINTMENTSTARTDATE,103),103) + ' ' + APPOINTMENTSTARTTIME,103),100),2) as APPOINTMENTSHIFT,
		CONVERT(varchar, PDR_APPOINTMENTS.APPOINTMENTSTARTDATE,103) as AppointmentDate, 
		ISNULL(P__PROPERTY.HouseNumber,'') +' '+ ISNULL(P__PROPERTY.ADDRESS1,'') +' '+ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'') AS CustAddress,
		PDR_APPOINTMENTS.ASSIGNEDTO AS ASSIGNEDTO,
		P__PROPERTY.POSTCODE AS PostCode,
		PDR_Status.TITLE AS Title,
		PDR_APPOINTMENTS.APPOINTMENTSTARTTIME AS APPOINTMENTSTARTTIME,
		PDR_APPOINTMENTS.APPOINTMENTENDTIME AS APPOINTMENTENDTIME,
		PDR_APPOINTMENTS.APPOINTMENTID AS APPOINTMENTID,
		datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), CONVERT(varchar, PDR_APPOINTMENTS.APPOINTMENTSTARTDATE,103),103) + ' ' + APPOINTMENTSTARTTIME,103)) as StartTimeInSec,
		datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), CONVERT(varchar, PDR_APPOINTMENTS.APPOINTMENTENDDATE,103),103) + ' ' + APPOINTMENTENDTIME,103)) as EndTimeInSec
		,CONVERT(varchar, APPOINTMENTSTARTDATE,103) as ValidateAppointmentDate
		,PDR_MSATTYPE.MSATTypeName as AppointmentType,
		C_TENANCY.TENANCYID AS TenancyId,
		P_STATUS.[DESCRIPTION] AS PropertyStatus

		
		       FROM     PDR_APPOINTMENTS
                                INNER JOIN PDR_JOURNAL ON PDR_APPOINTMENTS.JOURNALID = PDR_JOURNAL.JOURNALID
                                INNER JOIN	PDR_Status ON PDR_JOURNAL.StatusId = PDR_Status.StatusId AND PDR_STATUS.TITLE Not In ('Cancelled','To be Arranged')
								INNER JOIN PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATID
								INNER JOIN PDR_MSATTYPE ON PDR_MSAT.MSATTYPEID = PDR_MSATTYPE.MSATTYPEID
								LEFT JOIN	P_SCHEME ON PDR_MSAT.SchemeId = P_SCHEME.SCHEMEID 
								LEFT JOIN	P_BLOCK ON PDR_MSAT.BlockId = P_BLOCK.BLOCKID
								LEFT JOIN	P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID
								LEFT JOIN P_STATUS ON P__PROPERTY.[STATUS] = P_STATUS.STATUSID
								INNER JOIN C_TENANCY ON PDR_MSAT.TenancyId = C_TENANCY.TENANCYID
                                --LEFT JOIN C_TENANCY ON P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID
                                --                       AND ( DBO.C_TENANCY.ENDDATE IS NULL
                                --                             OR DBO.C_TENANCY.ENDDATE > GETDATE()
                                --                           )
                                --LEFT JOIN C_CUSTOMER_NAMES_GROUPED CG ON CG.I = DBO.C_TENANCY.TENANCYID
                                --                              AND CG.ID IN (
                                --                              SELECT
                                --                              MAX(ID) ID
                                --                              FROM
                                --                              C_CUSTOMER_NAMES_GROUPED
                                --                              GROUP BY I )
                       WHERE    		PDR_APPOINTMENTS.APPOINTMENTSTARTDATE BETWEEN CONVERT(DATE, @startDate, 103) AND CONVERT(DATE, @endDate, 103)
										OR PDR_APPOINTMENTS.APPOINTMENTENDDATE BETWEEN CONVERT(DATE, @startDate, 103) AND CONVERT(DATE, @endDate, 103)
										OR (PDR_APPOINTMENTS.APPOINTMENTSTARTDATE <= CONVERT(DATE, @startDate, 103) AND PDR_APPOINTMENTS.APPOINTMENTENDDATE >= CONVERT(DATE, @endDate, 103))
       
		
		
		-- Order By Caluse for all record from UNION(S) ALL
		ORDER BY StartTimeInSec ASC
	
	
END


