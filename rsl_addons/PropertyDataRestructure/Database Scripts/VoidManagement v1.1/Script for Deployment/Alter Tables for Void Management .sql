-- =============================================
-- Author:           Ali Raza
-- Create date:      07/09/2015
-- Description:      ALTER Table for Void Management
-- =============================================
	BEGIN TRANSACTION
	BEGIN TRY 
	 
		---=====================================
		--- Alter Table PDR_MSAT
		---=====================================
		ALTER TABLE dbo.PDR_MSAT ADD
			CustomerId int NULL,
			TenancyId int NULL,
			TerminationDate datetime NULL,
			ReletDate datetime NULL
			
		---=====================================
		--- Alter Table PDR_CONTRACTOR_WORK
		---=====================================	
		ALTER TABLE [PDR_CONTRACTOR_WORK] ALTER COLUMN [JournalId] INTEGER  NULL
		ALTER Table PDR_CONTRACTOR_WORK ADD PaintPackId INT DEFAULT NULL	

		---=====================================
		--- Alter Table PA_PROPERTY_ITEM_DATES
		---=====================================	
		ALTER TABLE PA_PROPERTY_ITEM_DATES ADD isVoidAppointmentAssociated bit default null 


END TRY 
	BEGIN CATCH 	
		IF @@TRANCOUNT >0	  
		BEGIN	   
			ROLLBACK TRANSACTION;	
		END 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
		@ErrorSeverity, -- Severity.
		@ErrorState -- State.
		);
	END CATCH  
	 
	IF @@TRANCOUNT >0
	BEGIN
		COMMIT TRANSACTION;
	END 
	