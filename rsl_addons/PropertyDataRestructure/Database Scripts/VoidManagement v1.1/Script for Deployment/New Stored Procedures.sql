

USE [RSLBHALive]
GO

-- =============================================
-- Author:           Ali Raza
-- Create date:      07/09/2015
-- Description:      New Stored Procedures for Void Management
-- =============================================
	
/****** Object:  StoredProcedure [dbo].[V_GetBritishGasStage2AndStage3Data]    Script Date: 09/07/2015 15:17:02 ******/
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:           Noor Muhammad
-- Create date:      23/06/2015
-- Description:      Get British Gas Stage 2 & Stage 3 Data
-- History:          23/06/2015 NoorM : Query for stage 2 british gas data
--                   03/07/2015 NoorM : Query for stage 3 british gas data
-- =============================================
CREATE PROCEDURE [dbo].[V_GetBritishGasStage2AndStage3Data] 
	@britishGasVoidNotificationId int	
AS
BEGIN
	SELECT 	
	V_BritishGasVoidNotification.BritishGasVoidNotificationId AS BritishGasVoidNotificationId
	,V_BritishGasVoidNotification.GasMeterTypeId AS GasMeterTypeId
	,V_BritishGasVoidNotification.GasMeterReading AS GasMeterReading
	,V_BritishGasVoidNotification.GasMeterReadingDate AS GasMeterReadingDate
	,V_BritishGasVoidNotification.ElectricMeterTypeId AS ElectricMeterTypeId
	,V_BritishGasVoidNotification.ElectricMeterReading AS ElectricMeterReading
	,V_BritishGasVoidNotification.ElectricMeterReadingDate AS ElectricMeterReadingDate
	,V_BritishGasVoidNotification.GasDebtAmount AS GasDebtAmount	
	,V_BritishGasVoidNotification.GasDebtAmountDate AS GasDebtAmountDate
	,V_BritishGasVoidNotification.ElectricDebtAmount AS ElectricDebtAmount	
	,V_BritishGasVoidNotification.ElectricDebtAmountDate AS ElectricDebtAmountDate	
	,V_BritishGasVoidNotification.NewTenantName AS NewTenantName	
	,V_BritishGasVoidNotification.NewTenantDateOfBirth AS NewTenantDateOfBirth
	,V_BritishGasVoidNotification.NewTenantTel AS NewTenantTel
	,V_BritishGasVoidNotification.NewTenantMobile AS NewTenantMobile
	,V_BritishGasVoidNotification.NewTenantOccupancyDate  AS NewTenantOccupancyDate
	,V_BritishGasVoidNotification.NewTenantPreviousAddress1 AS NewTenantPreviousAddress1
	,V_BritishGasVoidNotification.NewTenantPreviousAddress2 AS NewTenantPreviousAddress2
	,V_BritishGasVoidNotification.NewTenantPreviousPostCode AS NewTenantPreviousPostCode
	,V_BritishGasVoidNotification.NewTenantPreviousTownCity AS NewTenantPreviousTownCity
	,V_BritishGasVoidNotification.NewGasMeterReading AS NewGasMeterReading
	,V_BritishGasVoidNotification.NewGasMeterReadingDate AS NewGasMeterReadingDate
	,V_BritishGasVoidNotification.NewElectricMeterReading AS NewElectricMeterReading
	,V_BritishGasVoidNotification.NewElectricMeterReadingDate AS NewElectricMeterReadingDate
	
	FROM V_BritishGasVoidNotification 
	
	WHERE 
	 V_BritishGasVoidNotification.BritishGasVoidNotificationId =@britishGasVoidNotificationId
	 

END
GO
/****** Object:  StoredProcedure [dbo].[V_AmendPaintPacks]    Script Date: 09/07/2015 15:16:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:           Ali Raza
-- Create date:      10/06/2015
-- Description:      Amend Paint packs details 
-- History:          10/06/2015 AR : Amend Paint packs details                   
-- =============================================

CREATE PROCEDURE [dbo].[V_AmendPaintPacks] 
	-- Add the parameters for the stored procedure here
	(@paintPackDetailId INT,
	@paintRef nvarchar(50),
	@paintColor nvarchar(50),
	@paintName nvarchar(50)
	)
AS
BEGIN
	
	Update V_PaintPackDetails SET PaintRef=@paintRef,PaintColor=@paintColor,PaintName=@paintName
	
	Where PaintPackDetailId=@paintPackDetailId
END
GO
/****** Object:  StoredProcedure [dbo].[V_UpdateTenantToComplete]    Script Date: 09/07/2015 15:17:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:           Ali Raza
-- Create date:      10/06/2015
-- Description:      Update Tenant To Complete
-- History:          10/06/2015 AR : Update Tenant To Complete                 
-- =============================================
--EXEC	V_UpdateTenantToComplete
--		@worksRequiredId = 8,
--		@checkedValue = 0
CREATE PROCEDURE [dbo].[V_UpdateTenantToComplete](
	@worksRequiredId int,
	@checkedValue bit,
	@isSaved int = 0 out 
	)
	AS
BEGIN
DECLARE @isBrsWork BIT='False'	 
IF(@checkedValue = 0)
	BEGIN
	SET @isBrsWork = 'TRUE'
	END
BEGIN TRANSACTION;    
BEGIN TRY   
	
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   update V_RequiredWorks set IsTenantWorks=@checkedValue, IsBrsWorks=@isBrsWork where RequiredWorksId=@worksRequiredId
   
END TRY    
BEGIN CATCH     
 IF @@TRANCOUNT > 0    
 BEGIN         
  ROLLBACK TRANSACTION;       
  SET @isSaved = 0            
 END    
 DECLARE @ErrorMessage NVARCHAR(4000);    
 DECLARE @ErrorSeverity INT;    
 DECLARE @ErrorState INT;    
    
 SELECT @ErrorMessage = ERROR_MESSAGE(),    
 @ErrorSeverity = ERROR_SEVERITY(),    
 @ErrorState = ERROR_STATE();    
    
 -- Use RAISERROR inside the CATCH block to return     
 -- error information about the original error that     
 -- caused execution to jump to the CATCH block.    
 RAISERROR (@ErrorMessage, -- Message text.    
    @ErrorSeverity, -- Severity.    
    @ErrorState -- State.    
   );    
END CATCH;    
    
IF @@TRANCOUNT > 0    
 BEGIN      
  COMMIT TRANSACTION;      
  SET @isSaved = 1    
 END    
  
    
END
GO
/****** Object:  StoredProcedure [dbo].[V_UpdatePaintPacks]    Script Date: 09/07/2015 15:17:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:           Ali Raza
-- Create date:      10/06/2015
-- Description:      Update Update Paint pack
-- History:          11/06/2015 AR : Update Paint pack                 
-- =============================================
CREATE PROCEDURE [dbo].[V_UpdatePaintPacks]
	@paintPackId INT,
	@supplierId INT,
	@statusId INT,
	@deliveryDueDate datetime
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   Update V_PaintPack SET StatusId=@statusId, SupplierId=@supplierId, DeliveryDueDate=@deliveryDueDate,
    ModifiedDate=GETDATE() WHERE PaintPackId=@paintPackId
END
GO
/****** Object:  StoredProcedure [dbo].[V_UpdateBritishGasStage2Data]    Script Date: 09/07/2015 15:17:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
  Name:     [V_UpdateBritishGasStage2Data]

  Author: Noor Muhammad
  Creation Date: July-01-2015
  Description: Update British Gas Data For Stage2
  History:  01/07/2015 Noor : Query for update of british gas  stage 2 data
            30/09/2014 Name : 
  Execution Command:
  
    
-- ============================================= */   
CREATE PROCEDURE [dbo].[V_UpdateBritishGasStage2Data]    
 -- Add the parameters for the stored procedure here    
 @britishGasId int 
,@electricDebtAmount money = null 
,@electricDebtAmountDate date = null
,@electricMeterReading bigint = null
,@electricMeterReadingDate date = null
,@electricMeterTypeId int = null
,@gasDebtAmount money = null
,@gasDebtAmountDate date = null
,@gasMeterReading bigint = null
,@gasMeterReadingDate date = null
,@gasMeterTypeId int = null
,@britishGasStageId int = null
,@isNotificationSent bit=0
,@userId int = null
,@isSaved bit = 0 out    
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
                 
BEGIN TRANSACTION;    
BEGIN TRY     
              
     IF @britishGasStageId =1
		 BEGIN 
			Set SELECT @britishGasStageId=StageId FROM V_Stage WHERE V_Stage.Name = 'Stage 1'
		 END
     ELSE 
		 BEGIN 
			Set SELECT @britishGasStageId=StageId FROM V_Stage WHERE V_Stage.Name = 'Stage 2'
		 END         
                  
   UPDATE [dbo].[V_BritishGasVoidNotification]
   SET 
       [GasMeterTypeId] = @gasMeterTypeId
      ,[GasMeterReading] = @gasMeterReading
      ,[GasMeterReadingDate] = @gasMeterReadingDate
      ,[ElectricMeterTypeId] = @electricMeterTypeId
      ,[ElectricMeterReading] = @electricMeterReading
      ,[ElectricMeterReadingDate] = @electricMeterReadingDate
      ,[GasDebtAmount] = @gasDebtAmount
      ,[GasDebtAmountDate] = @gasDebtAmountDate
      ,[ElectricDebtAmount] = @electricDebtAmount
      ,[ElectricDebtAmountDate] = @electricDebtAmountDate      
      ,[ModifiedDate] = CURRENT_TIMESTAMP
	  ,[UserId] = @userId
	  ,StageId = @britishGasStageId
	  ,[isNotificationSent] = @isNotificationSent      
 WHERE 	   
	   BritishGasVoidNotificationId = @britishGasId   
	
			   	  
END TRY    
BEGIN CATCH     
 IF @@TRANCOUNT > 0    
 BEGIN         
  ROLLBACK TRANSACTION;       
  SET @isSaved = 0            
 END    
 DECLARE @ErrorMessage NVARCHAR(4000);    
 DECLARE @ErrorSeverity INT;    
 DECLARE @ErrorState INT;    
    
 SELECT @ErrorMessage = ERROR_MESSAGE(),    
 @ErrorSeverity = ERROR_SEVERITY(),    
 @ErrorState = ERROR_STATE();    
    
 -- Use RAISERROR inside the CATCH block to return     
 -- error information about the original error that     
 -- caused execution to jump to the CATCH block.    
 RAISERROR (@ErrorMessage, -- Message text.    
    @ErrorSeverity, -- Severity.    
    @ErrorState -- State.    
   );    
END CATCH;    
    
IF @@TRANCOUNT > 0    
 BEGIN      
  COMMIT TRANSACTION;      
  SET @isSaved = 1    
 END    
  
    
END
GO
/****** Object:  StoredProcedure [dbo].[V_GetAvailableProperties]    Script Date: 09/07/2015 15:17:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:           Ali Raza
-- Create date:      28/06/2015
-- Description:      Get available Properties for Report 
-- History:          11/06/2015 AR : Get available Properties for Report                 
-- =============================================
--  DECLARE	@totalCount int
--EXEC	 [dbo].[V_GetAvailableProperties]
--		@searchText = NULL,
--		@checksRequired=1
--		@pageSize = 100,
--		@pageNumber = 1,
--		@totalCount = @totalCount OUTPUT
--SELECT	@totalCount as N'@totalCount'
-- =============================================
CREATE PROCEDURE [dbo].[V_GetAvailableProperties]
-- Add the parameters for the stored procedure here
		@searchText VARCHAR(200)='',
		
	--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'Ref', 
		@sortOrder varchar (5) = 'DESC',
		@getOnlyCount bit=0,
		@totalCount int = 0 output	
AS
BEGIN
	DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(5000),
        
        --variables for paging
        @offset int,
		@limit int,
		@checksRequiredType varchar(200),
		@MSATTypeId int,
		@ArrangedStatusId int
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		

		--=====================Search Criteria===============================
		SET @searchCriteria = ' 1=1 AND P.STATUS = 1 AND (P.SUBSTATUS <> 21 OR P.SUBSTATUS IS NULL)'
		
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND (ISNULL(P.HouseNumber, '''')	+ ISNULL('' ''+P.ADDRESS1, '''') 	+ ISNULL('', ''+P.ADDRESS2, '''') LIKE ''%' + @searchText + '%'')'
		END	
		
		
		--=======================Select Clause=============================================
		SET @SelectClause = 'Select top ('+convert(varchar(10),@limit)+')
							  P.PROPERTYID As Ref,ISNULL(P.HouseNumber, '''')	+ ISNULL('' ''+P.ADDRESS1, '''') 	+ ISNULL('', ''+P.ADDRESS2, '''') AS Address, P.TOWNCITY, P.POSTCODE  
,ISNULL(S.SCHEMENAME,''-'') as Scheme,ISNULL(B.BLOCKNAME,''-'') AS Block,ISNULL(Convert(Varchar(50), T.TerminationDate,103),''-'') as Termination,ISNULL(Convert(Varchar(50),T.ReletDate,103),''-'') as Relet,ISNULL(ST.DESCRIPTION,''-'') AS Status
,ISNULL(SUB.DESCRIPTION,''-'') as SubStatus'
			
		
		
		--============================From Clause============================================
		SET @fromClause = CHAR(10) +'FROM P__PROPERTY P  

LEFT JOIN P_BLOCK B ON P.BLOCKID=B.BLOCKID
LEFT JOIN P_SCHEME S ON P.SCHEMEID=S.SCHEMEID
INNER JOIN P_STATUS ST ON P.STATUS = ST.STATUSID
LEFT JOIN P_SUBSTATUS SUB ON P.SUBSTATUS = SUB.SUBSTATUSID
LEFT JOIN (SELECT P.PROPERTYID,Convert(Varchar(50), T.TERMINATIONDATE,103) as TerminationDate,
		Case When Rec.ReletDate IS NULL Then CONVERT(nvarchar(50),DATEADD(day,7,T.TERMINATIONDATE), 103)
		ELSE Convert(Varchar(50), Rec.ReletDate,103)END as ReletDate
		FROM P__PROPERTY P
		  INNER JOIN C_JOURNAL J ON P.PROPERTYID=J.PROPERTYID	
	INNER JOIN C_TERMINATION T ON J.JOURNALID=T.JOURNALID
	INNER JOIN C__CUSTOMER C ON J.CUSTOMERID = C.CUSTOMERID
	INNER JOIN C_TENANCY CT ON J.TENANCYID=CT.TENANCYID
	INNER JOIN C_CUSTOMERTENANCY on C.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID and CT.TENANCYID=C_CUSTOMERTENANCY.TENANCYID
	INNER JOIN P_STATUS S ON P.STATUS = S.STATUSID
	INNER JOIN P_SUBSTATUS SUB ON P.SUBSTATUS = SUB.SUBSTATUSID
	Left Join (select PDR_MSAT.PropertyId,PDR_MSAT.ReletDate from PDR_MSAT
	INNER JOIN PDR_Journal ON PDR_MSAT.MSATID= PDR_Journal.MSATID
	INNER JOIN V_AppointmentRecordedData ON PDR_JOURNAL.JOURNALID = V_AppointmentRecordedData.InspectionJournalId) As Rec on P.PropertyId = Rec.PropertyId  
	WHERE P.SUBSTATUS=22 AND  ITEMNATUREID = 27 AND CURRENTITEMSTATUSID = 13 
 ) As  T ON P.PROPERTYID=T.PROPERTYID

		'
							
		--============================Order Clause==========================================
		IF(@sortColumn = 'Ref')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Ref' 	
			
		END
		
		IF(@sortColumn = 'Address')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Address' 	
			
		END
		
		
		IF(@sortColumn = 'Termination')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Termination' 	
			
		END
		
		IF(@sortColumn = 'Relet')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Relet' 	
			
		END
				
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		--===============================Main Query ====================================
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
		IF(@getOnlyCount=0)
		BEGIN
			print(@finalQuery)
			EXEC (@finalQuery)
		END
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
END
GO
/****** Object:  StoredProcedure [dbo].[V_GetNoEntryList]    Script Date: 09/07/2015 15:17:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:           Ali Raza
-- Create date:      25/06/2015
-- Description:      Get No Entry List 
-- History:          25/06/2015 AR : Get No Entry List                  
-- =============================================
--  DECLARE	@totalCount int
--EXEC	 [dbo].[V_GetNoEntryList]
--		@searchText = NULL,
--		@pageSize = 100,
--		@pageNumber = 1,
--		@totalCount = @totalCount OUTPUT
--SELECT	@totalCount as N'@totalCount'
-- =============================================
CREATE PROCEDURE [dbo].[V_GetNoEntryList]
-- Add the parameters for the stored procedure here
		@searchText VARCHAR(200)='',
	--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'JournalId', 
		@sortOrder varchar (5) = 'DESC',
		@getOnlyCount bit=0,
		@totalCount int = 0 output	
AS
BEGIN
	DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(2000),
        
        --variables for paging
        @offset int,
		@limit int,
		@MSATTypeId int,
		@ToBeArrangedStatusId int
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		
		
		--=====================Search Criteria===============================
		SET @searchCriteria = ' N.IsNoEntryScheduled=0 '
		
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND (ISNULL(P.HouseNumber, '''')	+ ISNULL('' ''+P.ADDRESS1, '''') 	+ ISNULL('', ''+P.ADDRESS2, '''') LIKE ''%' + @searchText + '%'')'
		
		END	
		
		--=======================Select Clause=============================================
		SET @SelectClause = 'Select top ('+convert(varchar(10),@limit)+')
								''JSV'' + CONVERT(VARCHAR, RIGHT(''000000'' + CONVERT(VARCHAR, ISNULL(
	CASE
		WHEN V.WorksJournalId IS NULL THEN J.JOURNALID
		ELSE V.WorksJournalId
	END, -1)), 4)) AS Ref,
	ISNULL(P.HouseNumber, '''') + ISNULL('' '' + P.ADDRESS1, '''') + ISNULL('', '' + P.ADDRESS2, '''') AS Address,
	P.TOWNCITY,
	P.COUNTY,
	ISNULL(P.POSTCODE, '''') AS Postcode,
	J.JOURNALID AS JournalId,
	CONVERT(NVARCHAR(50), M.TerminationDate, 103) AS Termination,
	CASE when M.ReletDate IS NULL THEN CONVERT(nvarchar(50),DATEADD(day,7,M.TerminationDate), 103)
	
	 ELSE CONVERT(NVARCHAR(50), M.ReletDate, 103) END AS Relet,
	CONVERT(NVARCHAR(50), N.RecordedOn, 103) AS RecordedOn,
	LEFT(E.Firstname, 1) + '''' + LEFT(E.LASTNAME, 1) AS RecordedBy,
	MT.MSATTypeName AS AppointmentType,
	A.APPOINTMENTID AS AppointmentId
			
			'
		
		
		--============================From Clause============================================
		SET @fromClause = CHAR(10) +' FROM V_NoEntry N
INNER JOIN PDR_APPOINTMENTS A
	ON N.AppointmentId = A.APPOINTMENTID
INNER JOIN PDR_JOURNAL J
	ON A.JOURNALID = J.JOURNALID
INNER JOIN PDR_MSAT M
	ON J.MSATID = M.MSATId
INNER JOIN PDR_MSATType MT
	ON M.MSATTypeId = MT.MSATTypeId
INNER JOIN PDR_STATUS S
	ON J.STATUSID = S.STATUSID
INNER JOIN P__PROPERTY P
	ON M.PropertyId = P.PROPERTYID
INNER JOIN E__EMPLOYEE E
	ON A.ASSIGNEDTO = E.EMPLOYEEID
LEFT JOIN V_RequiredWorks V
	ON J.JOURNALID = V.WorksJournalId
	AND V.IsCanceled = 0
		'
							
		--============================Order Clause==========================================
		IF(@sortColumn = 'Ref')
		BEGIN
			SET @sortColumn = CHAR(10)+ ' J.JOURNALID' 	
			
		END
		
		IF(@sortColumn = 'Address')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Address' 	
			
		END
		
		
		IF(@sortColumn = 'Termination')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Termination' 	
			
		END
		
		IF(@sortColumn = 'Relet')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Relet' 	
			
		END
				
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		--===============================Main Query ====================================
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
		IF(@getOnlyCount=0)
		BEGIN
			print(@finalQuery)
			EXEC (@finalQuery)
		END
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
END
GO
/****** Object:  StoredProcedure [dbo].[V_GetVoidWorksToBeArranged]    Script Date: 09/07/2015 15:17:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:           Ali Raza
-- Create date:      25/07/2015
-- Description:      Get Void Works To Be Arranged List  
-- History:          25/07/2015 AR : Get Void Works To Be Arranged List                 
-- =============================================


--  DECLARE	@totalCount int
--EXEC	 [dbo].[V_GetVoidWorksToBeArranged]
--		@searchText = NULL,
--		@pageSize = 100,
--		@pageNumber = 1,
--		@totalCount = @totalCount OUTPUT
--SELECT	@totalCount as N'@totalCount'
-- =============================================
CREATE PROCEDURE [dbo].[V_GetVoidWorksToBeArranged]
-- Add the parameters for the stored procedure here
		@searchText VARCHAR(200)='',
	--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'JournalId', 
		@sortOrder varchar (5) = 'DESC',
		@getOnlyCount bit=0,
		@totalCount int = 0 output	
AS
BEGIN
	DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(2000),
        
        --variables for paging
        @offset int,
		@limit int,
		@MSATTypeId int,
		@ToBeArrangedStatusId int
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		
		
		--=====================Search Criteria===============================
		SET @searchCriteria = 'V_Works.Works > 0 AND (V_Schedule.Arranged < V_Works.Works  OR V_Schedule.Arranged is NULL)And (V_AppointmentRecordedData.IsWorksRequired=1)'
		
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND (ISNULL(P__PROPERTY.HouseNumber, '''')	+ ISNULL('' ''+P__PROPERTY.ADDRESS1, '''') 	+ ISNULL('', ''+P__PROPERTY.ADDRESS2, '''') LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR P_BLOCK.BLOCKNAME LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR P_SCHEME.SCHEMENAME LIKE ''%' + @searchText + '%'')'
		END	
		
		--=======================Select Clause=============================================
		SET @SelectClause = 'Select top ('+convert(varchar(10),@limit)+')
							''JSV''+CONVERT(VARCHAR, RIGHT(''000000''+ CONVERT(VARCHAR,ISNULL(PDR_JOURNAL.JOURNALID,-1)),4)) AS Ref,
								ISNULL(P__PROPERTY.HouseNumber, '''')	+ ISNULL('' ''+P__PROPERTY.ADDRESS1, '''') 	+ ISNULL('', ''+P__PROPERTY.ADDRESS2, '''') AS Address 
			,	P__PROPERTY.TOWNCITY,P__PROPERTY.COUNTY,	ISNULL(P__PROPERTY.POSTCODE, '''') AS Postcode,
		ISNULL(P_SCHEME.SCHEMENAME,''-'') as Scheme,ISNULL(P_BLOCK.BLOCKNAME,''-'') AS Block,
	PDR_JOURNAL.JOURNALID 
		,CONVERT(nvarchar(50),PDR_MSAT.TerminationDate, 103) as Termination	
		,ISNULL(V_Works.Works,0) AS Works ,ISNULL(Arranged ,0)  as Arranged,C_TENANCY.TENANCYID as TenancyId 	'
		
		
		--============================From Clause============================================
		SET @fromClause = CHAR(10) +'FROM V_AppointmentRecordedData
		INNER JOIN PDR_JOURNAL ON PDR_JOURNAL.JOURNALID= V_AppointmentRecordedData.InspectionJournalId
		LEFT JOIN (Select COUNT(RequiredWorksId)as Works,InspectionJournalId from V_RequiredWorks WHERE (IsCanceled IS NULL OR IsCanceled=0) AND RoomId > 0 GROUP BY InspectionJournalId) AS V_Works ON 
		V_Works.InspectionJournalId=PDR_JOURNAL.JOURNALID
		LEFT JOIN (Select COUNT(RequiredWorksId)as Arranged,InspectionJournalId from V_RequiredWorks WHERE IsScheduled=1 AND RoomId > 0  AND (IsCanceled IS NULL OR IsCanceled=0) GROUP BY InspectionJournalId) AS V_Schedule
		ON V_Schedule.InspectionJournalId = PDR_JOURNAL.JOURNALID
		INNER JOIN	PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId
		INNER JOIN	PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId		
		LEFT JOIN	P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID	
		LEFT JOIN P_SCHEME on P_SCHEME.SCHEMEID = P__PROPERTY.SchemeId 
		LEFT JOIN P_BLOCK on  P_BLOCK.BLOCKID	=P__PROPERTY.BLOCKID 
		INNER JOIN C_TENANCY ON PDR_MSAT.TenancyId=	C_TENANCY.TENANCYID
		INNER JOIN C__CUSTOMER ON PDR_MSAT.CustomerId = C__CUSTOMER.CUSTOMERID
		INNER JOIN C_CUSTOMERTENANCY on C__CUSTOMER.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID and C_TENANCY.TENANCYID=C_CUSTOMERTENANCY.TENANCYID
		'
							
		--============================Order Clause==========================================
		IF(@sortColumn = 'Ref')
		BEGIN
			SET @sortColumn = CHAR(10)+ ' PDR_JOURNAL.JOURNALID' 	
			
		END
		
		IF(@sortColumn = 'Address')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Address' 	
			
		END
		IF(@sortColumn = 'Scheme')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Scheme' 	
			
		END
		
		IF(@sortColumn = 'Termination')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Termination' 	
			
		END
		
		IF(@sortColumn = 'Block')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Block' 	
			
		END
		IF(@sortColumn = 'Works')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Works' 	
			
		END
		IF(@sortColumn = 'Arranged')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Arranged' 	
			
		END
				
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		--===============================Main Query ====================================
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
	IF(@getOnlyCount=0)
		BEGIN
			print(@finalQuery)
			EXEC (@finalQuery)
		END
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
END
GO
/****** Object:  StoredProcedure [dbo].[V_GetReletAlertCount]    Script Date: 09/07/2015 15:17:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:           Ali Raza
-- Create date:      28/06/2015
-- Description:      Get Relets due in next 7 days Properties for Report 
-- History:          28/06/2015 AR : Get Relets due in next 7 days Properties for Report                  
-- =============================================
--  DECLARE	@totalCount int
--EXEC	 [dbo].[V_GetReletAlertCount]
--		@searchText = NULL,
--		@checksRequired=1
--		@pageSize = 100,
--		@pageNumber = 1,
--		@totalCount = @totalCount OUTPUT
--SELECT	@totalCount as N'@totalCount'
-- =============================================
CREATE PROCEDURE [dbo].[V_GetReletAlertCount]
-- Add the parameters for the stored procedure here
		@searchText VARCHAR(200)='',
		
	--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'Ref', 
		@sortOrder varchar (5) = 'DESC',
		@getOnlyCount bit=0,
		@totalCount int = 0 output	
AS
BEGIN
	DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(5000),
        
        --variables for paging
        @offset int,
		@limit int,
		@checksRequiredType varchar(200),
		@MSATTypeId int,
		@ArrangedStatusId int
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		

		--=====================Search Criteria===============================
		SET @searchCriteria = ' 1=1 AND (CONVERT(DATETIME,M.ReletDate, 103) > CONVERT(DATETIME,GETDATE(), 103) AND CONVERT(DATETIME,M.ReletDate, 103)<=CONVERT(DATETIME, DATEADD(day,7,GETDATE()), 103))'
		
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND (ISNULL(P.HouseNumber, '''')	+ ISNULL('' ''+P.ADDRESS1, '''') 	+ ISNULL('', ''+P.ADDRESS2, '''') LIKE ''%' + @searchText + '%'')'
		END	
		
		
		--=======================Select Clause=============================================
		SET @SelectClause = 'Select DISTINCT top ('+convert(varchar(10),@limit)+')
							  P.PROPERTYID As Ref 	,ISNULL(P.HouseNumber, '''')	+ ISNULL('' ''+P.ADDRESS1, '''') 	+ ISNULL('', ''+P.ADDRESS2, '''') AS Address
		,	P.TOWNCITY,P.COUNTY,	ISNULL(P.POSTCODE, '''') AS Postcode ,ISNULL(S.SCHEMENAME,''-'') as Scheme,ISNULL(B.BLOCKNAME,''-'') AS Block,
		ISNULL(Convert(Varchar(50), M.TerminationDate,103),''-'') as Termination,ISNULL(Convert(Varchar(50),M.ReletDate,103),''-'') as Relet,ISNULL(ST.DESCRIPTION,''-'') AS Status
,ISNULL(SUB.DESCRIPTION,''-'') as SubStatus'
			
		
		
		--============================From Clause============================================
		SET @fromClause = CHAR(10) +'From P__PROPERTY P
	INNER JOIN PDR_MSAT M ON P.PROPERTYID=M.PropertyId
	INNER JOIN PDR_JOURNAL J ON M.MSATId=J.MSATID
	LEFT JOIN P_BLOCK B ON P.BLOCKID=B.BLOCKID
	LEFT JOIN P_SCHEME S ON P.SCHEMEID=S.SCHEMEID
	INNER JOIN P_STATUS ST ON P.STATUS = ST.STATUSID
	LEFT JOIN P_SUBSTATUS SUB ON P.SUBSTATUS = SUB.SUBSTATUSID

		'
							
		--============================Order Clause==========================================
		IF(@sortColumn = 'Ref')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Ref' 	
			
		END
		
		IF(@sortColumn = 'Address')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Address' 	
			
		END
		
		
		IF(@sortColumn = 'Termination')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Termination' 	
			
		END
		
		IF(@sortColumn = 'Relet')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Relet' 	
			
		END
				
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		--===============================Main Query ====================================
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
		IF(@getOnlyCount=0)
		BEGIN
			print(@finalQuery)
			EXEC (@finalQuery)
		END
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(DISTINCT P.PROPERTYID) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
END
GO
/****** Object:  StoredProcedure [dbo].[V_UpdateRequiredWorks]    Script Date: 09/07/2015 15:17:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:           Ali Raza
-- Create date:      28/06/2015
-- Description:      Update Required Works
-- History:          28/06/2015 AR : Update Required Works using edit work popup               
-- =============================================
CREATE PROCEDURE [dbo].[V_UpdateRequiredWorks]
	-- Add the parameters for the stored procedure here
	@requiredWorksId int,
	@roomId int,
	@workDescription nvarchar(500),
	@isTenantWorks bit,
	@isCanceled bit,
	@updatedBy int,
	@isSaved int = 0 out 
AS
BEGIN

DECLARE @isBrsWork BIT='False'	
BEGIN TRANSACTION;    
BEGIN TRY  
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


	IF(@isTenantWorks = 0)
	BEGIN
	SET @isBrsWork = 'TRUE'
	END
	
IF(@isCanceled = 1)
	BEGIN
	 Update V_RequiredWorks SET RoomId=@roomId,WorkDescription=@workDescription ,IsTenantWorks=@isTenantWorks,
	 IsBrsWorks=@isBrsWork,ModifiedDate=@updatedBy,IsCanceled=@isCanceled,StatusId=(Select STATUSID from PDR_STATUS where TITLE='Cancelled')	
	   WHERE  RequiredWorksId=@requiredWorksId
	END
ELSE
	BEGIN

	   Update V_RequiredWorks SET RoomId=@roomId,WorkDescription=@workDescription ,IsTenantWorks=@isTenantWorks,
	   IsBrsWorks=@isBrsWork,ModifiedDate=@updatedBy
	   WHERE  RequiredWorksId=@requiredWorksId
	END
   
END TRY    
BEGIN CATCH     
 IF @@TRANCOUNT > 0    
 BEGIN         
  ROLLBACK TRANSACTION;       
  SET @isSaved = 0            
 END    
 DECLARE @ErrorMessage NVARCHAR(4000);    
 DECLARE @ErrorSeverity INT;    
 DECLARE @ErrorState INT;    
    
 SELECT @ErrorMessage = ERROR_MESSAGE(),    
 @ErrorSeverity = ERROR_SEVERITY(),    
 @ErrorState = ERROR_STATE();    
    
 -- Use RAISERROR inside the CATCH block to return     
 -- error information about the original error that     
 -- caused execution to jump to the CATCH block.    
 RAISERROR (@ErrorMessage, -- Message text.    
    @ErrorSeverity, -- Severity.    
    @ErrorState -- State.    
   );    
END CATCH;    
    
IF @@TRANCOUNT > 0    
 BEGIN      
  COMMIT TRANSACTION;      
  SET @isSaved = 1    
 END    
  
    
END
GO
/****** Object:  StoredProcedure [dbo].[V_UpdateBritishGasStage3Data]    Script Date: 09/07/2015 15:17:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
  Name:     [V_UpdateBritishGasStage3Data]

  Author: Ali Raza
  Creation Date: August-17-2015
  Description: Update British Gas Data For Stage3
  History:  17/08/2015 Noor : Query for update of british gas  stage 3 data
            
  Execution Command:
  
    
-- ============================================= */   
CREATE PROCEDURE [dbo].[V_UpdateBritishGasStage3Data]    
 -- Add the parameters for the stored procedure here    
 @britishGasId int 
,@newTenantName varchar(50) = null 
,@newTenantTel varchar(30) = null
,@newTenantMobile varchar(30) = null
,@newTenantDateOfBirth  date = null
,@newTenantOccupancyDate date = null
,@newTenantPreviousAddress1 varchar(50) = null
,@newTenantPreviousAddress2 varchar(50) = null
,@newTenantPreviousPostCode varchar(20) = null
,@newTenantPreviousTownCity varchar(50) = null
,@electricMeterReading bigint = null
,@electricMeterReadingDate date = null
,@gasMeterReading bigint = null
,@gasMeterReadingDate date = null
,@userId int = null
,@isSaved bit = 0 out    
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
                 
BEGIN TRANSACTION;    
BEGIN TRY     
                  
   UPDATE [dbo].[V_BritishGasVoidNotification]
   SET 
      
      [NewGasMeterReading] = @gasMeterReading
      ,[NewGasMeterReadingDate] = @gasMeterReadingDate
      ,[NewElectricMeterReading] = @electricMeterReading
      ,[NewElectricMeterReadingDate] = @electricMeterReadingDate
      ,[NewTenantName] = @newTenantName
      ,[NewTenantTel]=@newTenantTel
      ,[NewTenantMobile]=@newTenantMobile
      ,[NewTenantDateOfBirth]=@newTenantDateOfBirth
      ,[NewTenantOccupancyDate]=@newTenantOccupancyDate
      ,[NewTenantPreviousAddress1]=@newTenantPreviousAddress1
      ,[NewTenantPreviousAddress2]=@newTenantPreviousAddress2
      ,[NewTenantPreviousTownCity]=@newTenantPreviousTownCity
      ,[NewTenantPreviousPostCode]=@newTenantPreviousPostCode
      ,[ModifiedDate] = CURRENT_TIMESTAMP
	  ,[UserId] = @userId
	  ,StageId = (SELECT StageId FROM V_Stage WHERE V_Stage.Name = 'Stage 3')
	  ,StatusId=(Select  STATUSID from PDR_STATUS where TITLE='Completed')
	  ,isNotificationSent=1
 WHERE 	   
	   BritishGasVoidNotificationId = @britishGasId   
	
			   	  
END TRY    
BEGIN CATCH     
 IF @@TRANCOUNT > 0    
 BEGIN         
  ROLLBACK TRANSACTION;       
  SET @isSaved = 0            
 END    
 DECLARE @ErrorMessage NVARCHAR(4000);    
 DECLARE @ErrorSeverity INT;    
 DECLARE @ErrorState INT;    
    
 SELECT @ErrorMessage = ERROR_MESSAGE(),    
 @ErrorSeverity = ERROR_SEVERITY(),    
 @ErrorState = ERROR_STATE();    
    
 -- Use RAISERROR inside the CATCH block to return     
 -- error information about the original error that     
 -- caused execution to jump to the CATCH block.    
 RAISERROR (@ErrorMessage, -- Message text.    
    @ErrorSeverity, -- Severity.    
    @ErrorState -- State.    
   );    
END CATCH;    
    
IF @@TRANCOUNT > 0    
 BEGIN      
  COMMIT TRANSACTION;      
  SET @isSaved = 1    
 END    
  
    
END
GO
/****** Object:  StoredProcedure [dbo].[V_ScheduleVoidInspection]    Script Date: 09/07/2015 15:17:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:           Ali Raza
-- Create date:      28/06/2015
-- Description:      Update/Insert PDR_JOURNAL,PDR_APPOINTMENTS
-- History:          28/06/2015 AR : Update/Insert PDR_JOURNAL,PDR_APPOINTMENTS              
-- =============================================



CREATE PROCEDURE [dbo].[V_ScheduleVoidInspection]    
 -- Add the parameters for the stored procedure here    
 @userId int 
,@appointmentNotes varchar(1000)     
,@appointmentStartDate date    
,@appointmentEndDate date    
,@startTime varchar(10)    
,@endTime varchar(10)    
,@operativeId int         
,@journalId INT    
,@duration float    
,@isSaved int = 0 out    
,@appointmentIdOut int = -1 out 
     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
             
DECLARE     
    
@ArrangedId int    
,@JournalHistoryId int    
,@AppointmentId int    
,@AppointmentHistoryId int    
    
    
BEGIN TRANSACTION;    
BEGIN TRY    

-- =============================================    
-- Get status id of "Arranged"    
-- =============================================     
	SELECT  @ArrangedId = PDR_STATUS.statusid 
	FROM	PDR_STATUS 
	WHERE	PDR_STATUS.title ='Arranged'    
-- ====================================================================================================    
--          UPDATE (PDR_JOURNAL)    
-- ====================================================================================================    

     
 UPDATE [PDR_JOURNAL]
 SET	STATUSID = @ArrangedId
	    ,CREATEDBY = @userId
	    ,CREATIONDATE = GETDATE()	    
 WHERE	JOURNALID = @journalId
     
 PRINT 'JOURNALID = '+ CONVERT(VARCHAR,@journalId )    
-- ====================================================================================================    
--          INSERTION (PDR_APPOINTMENTS)    
-- ====================================================================================================    
    
	SELECT	@JournalHistoryId = MAX(JOURNALHISTORYID)    
	FROM	PDR_JOURNAL_HISTORY    
	WHERE	JOURNALID = @journalId    
      
 PRINT 'JOURNALHISTORYID = '+ CONVERT(VARCHAR,@JournalHistoryId)  
    
    
           
    INSERT INTO [PDR_APPOINTMENTS]
           (
            [JOURNALID]
           ,[JOURNALHISTORYID]
           ,[APPOINTMENTSTARTDATE]
           ,[APPOINTMENTENDDATE]
           ,[APPOINTMENTSTARTTIME]
           ,[APPOINTMENTENDTIME]
           ,[ASSIGNEDTO]
           ,[CREATEDBY]
           ,[LOGGEDDATE]
           ,[APPOINTMENTNOTES]           
           ,[APPOINTMENTSTATUS]
           ,[DURATION])
     VALUES
           (
            @journalId
           ,@JournalHistoryId
           ,@appointmentStartDate
           ,@appointmentEndDate
           ,@startTime
           ,@endTime
           ,@operativeId    
           ,@userId  
           ,GETDATE()
           ,@appointmentNotes           
           ,'NotStarted' 
           ,@duration)   
          
      SELECT @AppointmentId = SCOPE_IDENTITY()    
      PRINT 'APPOINTMENTID = '+ CONVERT(VARCHAR,@AppointmentId)            
     
	  SELECT @appointmentIdOut = @AppointmentId
          
END TRY    
BEGIN CATCH     
 IF @@TRANCOUNT > 0    
 BEGIN         
  ROLLBACK TRANSACTION;       
  SET @isSaved = 0            
 END    
 DECLARE @ErrorMessage NVARCHAR(4000);    
 DECLARE @ErrorSeverity INT;    
 DECLARE @ErrorState INT;    
    
 SELECT @ErrorMessage = ERROR_MESSAGE(),    
 @ErrorSeverity = ERROR_SEVERITY(),    
 @ErrorState = ERROR_STATE();    
    
 -- Use RAISERROR inside the CATCH block to return     
 -- error information about the original error that     
 -- caused execution to jump to the CATCH block.    
 RAISERROR (@ErrorMessage, -- Message text.    
    @ErrorSeverity, -- Severity.    
    @ErrorState -- State.    
   );    
END CATCH;    
    
IF @@TRANCOUNT > 0    
 BEGIN      
  COMMIT TRANSACTION;      
  SET @isSaved = 1    
 END    
  
    
END
GO
/****** Object:  StoredProcedure [dbo].[V_ScheduleGasElectricChecks]    Script Date: 09/07/2015 15:17:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:           Ali Raza
-- Create date:      28/06/2015
-- Description:      Schedule Gas Electric Checks
-- History:          28/06/2015 AR : Schedule Gas Electric Checks          
-- =============================================

CREATE PROCEDURE [dbo].[V_ScheduleGasElectricChecks]    
 -- Add the parameters for the stored procedure here    
 @userId int 
,@appointmentNotes varchar(1000)  
,@appointmentStartDate date    
,@appointmentEndDate date    
,@startTime varchar(10)    
,@endTime varchar(10)    
,@operativeId int         
,@journalId INT    
,@duration float  
,@isSaved int = 0 out    
,@appointmentIdOut int = -1 out 
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
             
DECLARE     
    
@ArrangedId int    
,@JournalHistoryId int    
,@AppointmentId int    
,@AppointmentHistoryId int    
    
    
BEGIN TRANSACTION;    
BEGIN TRY    

-- =============================================    
-- Get status id of "Arranged"    
-- =============================================     
	SELECT  @ArrangedId = PDR_STATUS.statusid 
	FROM	PDR_STATUS 
	WHERE	PDR_STATUS.title ='Arranged'    
-- ====================================================================================================    
--          UPDATE (PDR_JOURNAL)    
-- ====================================================================================================    

     
 UPDATE [PDR_JOURNAL]
 SET	STATUSID = @ArrangedId
	    ,CREATEDBY = @userId
	    ,CREATIONDATE = GETDATE()	    
 WHERE	JOURNALID = @journalId
     
 PRINT 'JOURNALID = '+ CONVERT(VARCHAR,@journalId )    
-- ====================================================================================================    
--          INSERTION (PDR_APPOINTMENTS)    
-- ====================================================================================================    
    
	SELECT	@JournalHistoryId = MAX(JOURNALHISTORYID)    
	FROM	PDR_JOURNAL_HISTORY    
	WHERE	JOURNALID = @journalId    
      
 PRINT 'JOURNALHISTORYID = '+ CONVERT(VARCHAR,@JournalHistoryId)  
    
    
           
    INSERT INTO [PDR_APPOINTMENTS]
           (
            [JOURNALID]
           ,[JOURNALHISTORYID]
           ,[APPOINTMENTSTARTDATE]
           ,[APPOINTMENTENDDATE]
           ,[APPOINTMENTSTARTTIME]
           ,[APPOINTMENTENDTIME]
           ,[ASSIGNEDTO]
           ,[CREATEDBY]
           ,[LOGGEDDATE]
           ,[APPOINTMENTNOTES]           
           ,[APPOINTMENTSTATUS]
           ,[DURATION])
     VALUES
           (
            @journalId
           ,@JournalHistoryId
           ,@appointmentStartDate
           ,@appointmentEndDate
           ,@startTime
           ,@endTime
           ,@operativeId    
           ,@userId  
           ,GETDATE()
           ,@appointmentNotes           
           ,'NotStarted' 
           ,@duration)   
          
      SELECT @AppointmentId = SCOPE_IDENTITY()    
      PRINT 'APPOINTMENTID = '+ CONVERT(VARCHAR,@AppointmentId)            
     
	  SELECT @appointmentIdOut = @AppointmentId
END TRY    
BEGIN CATCH     
 IF @@TRANCOUNT > 0    
 BEGIN         
  ROLLBACK TRANSACTION;       
  SET @isSaved = 0            
 END    
 DECLARE @ErrorMessage NVARCHAR(4000);    
 DECLARE @ErrorSeverity INT;    
 DECLARE @ErrorState INT;    
    
 SELECT @ErrorMessage = ERROR_MESSAGE(),    
 @ErrorSeverity = ERROR_SEVERITY(),    
 @ErrorState = ERROR_STATE();    
    
 -- Use RAISERROR inside the CATCH block to return     
 -- error information about the original error that     
 -- caused execution to jump to the CATCH block.    
 RAISERROR (@ErrorMessage, -- Message text.    
    @ErrorSeverity, -- Severity.    
    @ErrorState -- State.    
   );    
END CATCH;    
    
IF @@TRANCOUNT > 0    
 BEGIN      
  COMMIT TRANSACTION;      
  SET @isSaved = 1    
 END    
  
    
END
GO
/****** Object:  StoredProcedure [dbo].[V_SaveBritishGasData]    Script Date: 09/07/2015 15:17:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



/* =================================================================================    
  Page Description:     SaveBritishGasData

  Author: Noor Muhammad
  Creation Date: June-16-2015
   History:  16/06/2015 Noor : Query for Save British Gas Data
             30/09/2014 Name : 
  Execution Command:
  
    
-- ============================================= */   
CREATE PROCEDURE [dbo].[V_SaveBritishGasData]    
 -- Add the parameters for the stored procedure here    
 @customerId int 
,@propertyId varchar(20)  
,@tenancyId int    
,@isSaved bit out    
,@britishGasId int out 
,@britishGasStageId int out 
,@britishGasStage varchar(20) out 
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
                 
BEGIN TRANSACTION;    
BEGIN TRY    
	
	DECLARE @notificationStatusId INT
	
	SELECT @britishGasId = BritishGasVoidNotificationId 
	FROM [V_BritishGasVoidNotification]
	WHERE 
	CustomerId = @customerId
	AND PropertyId = @propertyId
	AND TenancyId = @tenancyId
	
	-- =============================================    
	-- Get status id of "Stage1"    
	-- =============================================     
	SELECT  @britishGasStageId = V_Stage.StageId, @britishGasStage = V_Stage.Name
	FROM	V_Stage 
	WHERE	V_Stage.Name ='Stage 1' 
	AND V_Stage.IsActive = 1 
	
	-- =============================================    
	-- Get status id of In Progress   
	-- =============================================    
	SELECT  @notificationStatusId = PDR_STATUS.STATUSID FROM PDR_STATUS WHERE TITLE = 'In Progress'
	
	IF @britishGasId = 0 OR @britishGasId IS NULL 
		BEGIN	    	
			             
			INSERT INTO [V_BritishGasVoidNotification]
				   (
					[CustomerId]
				   ,[PropertyId]
				   ,[TenancyId]
				   ,[StageId]
				   ,[StatusId]
				   ,[CreatedDate]
				   ,[ModifiedDate]
				   ,[isNotificationSent])
			 VALUES
				   (
					 @customerId
					,@propertyId
					,@tenancyId
					,@britishGasStageId
					,@notificationStatusId
					,CURRENT_TIMESTAMP
					,CURRENT_TIMESTAMP
					,0)   
          
			  SELECT @britishGasId = SCOPE_IDENTITY()    
			  PRINT 'British Gas Id = '+ CONVERT(VARCHAR,@britishGasId)            
		END	   
END TRY    
BEGIN CATCH     
 IF @@TRANCOUNT > 0    
 BEGIN         
  ROLLBACK TRANSACTION;       
  SET @isSaved = 0            
 END    
 DECLARE @ErrorMessage NVARCHAR(4000);    
 DECLARE @ErrorSeverity INT;    
 DECLARE @ErrorState INT;    
    
 SELECT @ErrorMessage = ERROR_MESSAGE(),    
 @ErrorSeverity = ERROR_SEVERITY(),    
 @ErrorState = ERROR_STATE();    
    
 -- Use RAISERROR inside the CATCH block to return     
 -- error information about the original error that     
 -- caused execution to jump to the CATCH block.    
 RAISERROR (@ErrorMessage, -- Message text.    
    @ErrorSeverity, -- Severity.    
    @ErrorState -- State.    
   );    
END CATCH;    
    
IF @@TRANCOUNT > 0    
 BEGIN      
  COMMIT TRANSACTION;      
  SET @isSaved = 1    
 END    
  
    
END
GO
/****** Object:  StoredProcedure [dbo].[V_ReScheduleWorksRequired]    Script Date: 09/07/2015 15:17:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:           Ali Raza
-- Create date:      28/07/2015
-- Description:      Schedule Void Required Works Appointments
-- History:          28/07/2015 AR : Schedule Void Required Works Appointments          
-- =============================================
   
CREATE PROCEDURE [dbo].[V_ReScheduleWorksRequired]    
 -- Add the parameters for the stored procedure here    
 @userId int 
,@appointmentNotes varchar(1000)  
,@jobSheetNotes varchar(1000)    
,@appointmentStartDate date    
,@appointmentEndDate date    
,@startTime varchar(10)    
,@endTime varchar(10)    
,@operativeId int         
,@journalId INT    
,@duration float 
,@appointmentId int  
,@isSaved int = 0 out    

     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
             
DECLARE 
@ArrangedId int    
,@JournalHistoryId int    
,@AppointmentHistoryId int    

 
BEGIN TRANSACTION;    
BEGIN TRY    

-- =============================================    
-- Get status id of "Arranged"    and MSATTypeId
-- =============================================     
	SELECT  @ArrangedId = PDR_STATUS.statusid 
	FROM	PDR_STATUS 
	WHERE	PDR_STATUS.title ='Arranged'   
	
	
-- ====================================================================================================    
--          Update PDR_JOURNAL    
-- ====================================================================================================    	
Update PDR_JOURNAL SET STATUSID = @ArrangedId ,CREATIONDATE=GETDATE(), CREATEDBY=@userId where JOURNALID = @journalId

-- ====================================================================================================    
--          INSERTION (PDR_APPOINTMENTS)    
-- ====================================================================================================    
    
	SELECT	@JournalHistoryId = MAX(JOURNALHISTORYID)    
	FROM	PDR_JOURNAL_HISTORY    
	WHERE	JOURNALID = @journalId    
      
 PRINT 'JOURNALHISTORYID = '+ CONVERT(VARCHAR,@JournalHistoryId)  
    
-- ====================================================================================================    
--          Update PDR_Appointments    
-- ====================================================================================================  
   UPDATE PDR_APPOINTMENTS SET JOURNALID=@journalId,JOURNALHISTORYID=@JournalHistoryId,APPOINTMENTSTARTDATE=  @appointmentStartDate,APPOINTMENTENDDATE= @appointmentEndDate,
   APPOINTMENTSTARTTIME= @startTime,APPOINTMENTENDTIME= @endTime,ASSIGNEDTO=@operativeId,CREATEDBY= @userId,LOGGEDDATE=GETDATE(),
   APPOINTMENTNOTES= @appointmentNotes,CUSTOMERNOTES=@jobSheetNotes, APPOINTMENTSTATUS='NotStarted',DURATION=@duration
   Where APPOINTMENTID=@appointmentId
    
-- ====================================================================================================    
--          Update V_RequiredWorks    
-- ====================================================================================================      
		 Update V_RequiredWorks SET WorksJournalId=@journalId, IsScheduled=1, StatusId=@ArrangedId,VoidWorksNotes=@jobSheetNotes WHERE WorksJournalId=@journalId
		
		
-- ====================================================================================================    
--          Update V_NoEntry    
-- ====================================================================================================  		
  UPDATE V_NoEntry Set IsNoEntryScheduled=1   Where APPOINTMENTID=@appointmentId
        
END TRY    
BEGIN CATCH     
 IF @@TRANCOUNT > 0    
 BEGIN         
  ROLLBACK TRANSACTION;       
  SET @isSaved = 0            
 END    
 DECLARE @ErrorMessage NVARCHAR(4000);    
 DECLARE @ErrorSeverity INT;    
 DECLARE @ErrorState INT;    
    
 SELECT @ErrorMessage = ERROR_MESSAGE(),    
 @ErrorSeverity = ERROR_SEVERITY(),    
 @ErrorState = ERROR_STATE();    
    
 -- Use RAISERROR inside the CATCH block to return     
 -- error information about the original error that     
 -- caused execution to jump to the CATCH block.    
 RAISERROR (@ErrorMessage, -- Message text.    
    @ErrorSeverity, -- Severity.    
    @ErrorState -- State.    
   );    
END CATCH;    
    
IF @@TRANCOUNT > 0    
 BEGIN      
  COMMIT TRANSACTION;      
  SET @isSaved = 1    
 END    
  
    
END
GO
/****** Object:  StoredProcedure [dbo].[V_ReScheduleVoidInspection]    Script Date: 09/07/2015 15:17:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:           Ali Raza
-- Create date:      28/07/2015
-- Description:      Rescheduele Void Inspection Appointments.
-- History:          28/07/2015 AR : Rescheduele Void Inspection Appointments.          
-- =============================================

  
CREATE PROCEDURE [dbo].[V_ReScheduleVoidInspection]    
 -- Add the parameters for the stored procedure here    
 @userId int 
,@appointmentNotes varchar(1000)     
,@appointmentStartDate date    
,@appointmentEndDate date    
,@startTime varchar(10)    
,@endTime varchar(10)    
,@operativeId int         
,@journalId INT     
,@isSaved int = 0 out    

     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
             
DECLARE 
@ArrangedId int    
,@JournalHistoryId int    
,@AppointmentHistoryId int    
,@appointmentId int
 
BEGIN TRANSACTION;    
BEGIN TRY    

-- =============================================    
-- Get status id of "Arranged"
-- =============================================     
	SELECT  @ArrangedId = PDR_STATUS.statusid 
	FROM	PDR_STATUS 
	WHERE	PDR_STATUS.title ='Arranged'   
	
	
-- ====================================================================================================    
--          Update PDR_JOURNAL    
-- ====================================================================================================    	
Update PDR_JOURNAL SET STATUSID = @ArrangedId ,CREATIONDATE=GETDATE(), CREATEDBY=@userId where JOURNALID = @journalId

-- ====================================================================================================    
--          Get Latest JournalHistoryId 
-- ====================================================================================================    
    
	SELECT	@JournalHistoryId = MAX(JOURNALHISTORYID)    
	FROM	PDR_JOURNAL_HISTORY    
	WHERE	JOURNALID = @journalId    
      
 PRINT 'JOURNALHISTORYID = '+ CONVERT(VARCHAR,@JournalHistoryId)  
    
-- ====================================================================================================    
--          Update PDR_Appointments    
-- ====================================================================================================  
   UPDATE PDR_APPOINTMENTS SET JOURNALHISTORYID=@JournalHistoryId,APPOINTMENTSTARTDATE=  @appointmentStartDate,APPOINTMENTENDDATE= @appointmentEndDate,
   APPOINTMENTSTARTTIME= @startTime,APPOINTMENTENDTIME= @endTime,ASSIGNEDTO=@operativeId,CREATEDBY= @userId,LOGGEDDATE=GETDATE(),
   APPOINTMENTNOTES= @appointmentNotes, APPOINTMENTSTATUS='NotStarted'
   Where JOURNALID =@journalId

Delete from V_AppointmentRecordedData where InspectionJournalId=@journalId
DELETE from V_RequiredWorks WHERE InspectionJournalId=@journalId

SELECT @appointmentId=APPOINTMENTID from PDR_APPOINTMENTS WHERE JOURNALID =@journalId 
       
  IF EXISTS(SELECT NoEntryId from V_NoEntry WHERE AppointmentId=@appointmentId)
	BEGIN
		UPDATE V_NoEntry SET IsNoEntryScheduled = 'TRUE' WHERE AppointmentId=@appointmentId
	END     
       
END TRY    
BEGIN CATCH     
 IF @@TRANCOUNT > 0    
 BEGIN         
  ROLLBACK TRANSACTION;       
  SET @isSaved = 0            
 END    
 DECLARE @ErrorMessage NVARCHAR(4000);    
 DECLARE @ErrorSeverity INT;    
 DECLARE @ErrorState INT;    
    
 SELECT @ErrorMessage = ERROR_MESSAGE(),    
 @ErrorSeverity = ERROR_SEVERITY(),    
 @ErrorState = ERROR_STATE();    
    
 -- Use RAISERROR inside the CATCH block to return     
 -- error information about the original error that     
 -- caused execution to jump to the CATCH block.    
 RAISERROR (@ErrorMessage, -- Message text.    
    @ErrorSeverity, -- Severity.    
    @ErrorState -- State.    
   );    
END CATCH;    
    
IF @@TRANCOUNT > 0    
 BEGIN      
  COMMIT TRANSACTION;      
  SET @isSaved = 1    
 END    
  
    
END
GO
/****** Object:  StoredProcedure [dbo].[V_CancelVoidAppointment]    Script Date: 09/07/2015 15:17:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:           Ali Raza
-- Create date:      28/07/2015
-- Description:		 Cancel Void Appointment 
-- History:          28/07/2015 AR : Cancel Void Appointment         
-- =============================================

Create PROCEDURE [dbo].[V_CancelVoidAppointment]
	@journalId int	
	,@isCancelled int = 0 out
AS
BEGIN


BEGIN TRANSACTION;
BEGIN TRY

	-- ========================================================
	--	INSERT INTO PDR_CANCELLED_JOBS
	-- ========================================================
	INSERT INTO PDR_CANCELLED_JOBS (AppointmentId,RecordedOn)
	SELECT PDR_APPOINTMENTS.APPOINTMENTID as AppointmentId, GETDATE() as RecordedOn
	FROM PDR_APPOINTMENTS
	WHERE PDR_APPOINTMENTS.JournalId = @journalId
	AND PDR_APPOINTMENTS.APPOINTMENTSTATUS <> 'Complete'
	
	
	-- ========================================================
	--	UPDATE PDR_JOURNAL
	-- ========================================================
	
	UPDATE	PDR_JOURNAL
	SET		PDR_JOURNAL.STATUSID = (SELECT PDR_STATUS.STATUSID 
									FROM PDR_STATUS 
									WHERE PDR_STATUS.TITLE= 'Cancelled' )
	WHERE	PDR_JOURNAL.JOURNALID = @journalId

	-- ========================================================
	--	UPDATE V_RequiredWorks
	-- ========================================================
	
	Update V_RequiredWorks SET WorksJournalId=NULL, IsScheduled=0, 
	StatusId=(SELECT PDR_STATUS.STATUSID FROM PDR_STATUS WHERE PDR_STATUS.TITLE= 'To be Arranged'), ModifiedDate=GETDATE()
	WHERE	V_RequiredWorks.WorksJournalId = @journalId

	
	END TRY
BEGIN CATCH 
    IF @@TRANCOUNT > 0
  BEGIN     
   ROLLBACK TRANSACTION;   
 SET @isCancelled = 0
    
    DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    -- Use RAISERROR inside the CATCH block to return 
    -- error information about the original error that 
    -- caused execution to jump to the CATCH block.
    RAISERROR (@ErrorMessage, -- Message text.
         @ErrorSeverity, -- Severity.
         @ErrorState -- State.
         );
  END
END CATCH;

IF @@TRANCOUNT > 0
 BEGIN  
  COMMIT TRANSACTION;  
  SET @isCancelled = 1
 END

END
GO
/****** Object:  StoredProcedure [dbo].[V_UpdateBritishGasStage1Data]    Script Date: 09/07/2015 15:17:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
  Name:     [V_UpdateBritishGasStage1Data]

  Author: Noor Muhammad
  Creation Date: June-16-2015
  Description: Get British Gas Stage 1 Data
  History:  16/06/2015 Noor : Query for update of british gas data
            30/09/2014 Name : Correct the query to insert 2 records in pdr_journal
  Execution Command:
  
    
-- ============================================= */   
CREATE PROCEDURE [dbo].[V_UpdateBritishGasStage1Data]    
 -- Add the parameters for the stored procedure here    
 @britishGasId int 
,@customerId int 
,@propertyId varchar(20)  
,@tenancyId int		
,@isGasElectricCheck bit = null
,@occupancyCeaseDate date = null
,@tenantFwAddress1 varchar(50) = null
,@tenantFwAddress2 varchar(50) = null
,@tenantFwCity varchar(50) = null
,@tenantFwPostCode varchar(30) = null
,@britishGasEmail varchar(100) = null
,@isNotificationSent bit=0
,@userId int = null
,@isSaved bit = 0 out    
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
                 
BEGIN TRANSACTION;    
BEGIN TRY     
                  
   UPDATE [dbo].[V_BritishGasVoidNotification]
   SET [CustomerId] = @customerId
      ,[PropertyId] = @propertyId
      ,[TenancyId] =  @tenancyId      
	  ,[IsGasCheck] = @isGasElectricCheck
	  ,[IsElectricCheck] = @isGasElectricCheck      
      ,[TenantFwAddress1] = @tenantFwAddress1
      ,[TenantFwAddress2] = @tenantFwAddress2
      ,[TenantFwCity] = @tenantFwCity
      ,[TenantFwPostCode] = @tenantFwPostCode
      ,[DateOccupancyCease] = @occupancyCeaseDate
      ,[BritishGasEmail] = @britishGasEmail
      ,[StageId] = (SELECT StageId FROM V_Stage WHERE V_Stage.Name = 'Stage 1')  
      ,[isNotificationSent] = @isNotificationSent      
      ,[ModifiedDate] = CURRENT_TIMESTAMP
	  ,[UserId] = @userId
 WHERE 
	   PropertyId = @propertyId
	   AND CustomerId = @customerId
	   AND TenancyId = @tenancyId	
	   AND BritishGasVoidNotificationId = @britishGasId   
	
-------------------------------------------------------------------------------------------------------------------------------------
--INSERT RECORD FOR GAS ELECTRIC CHECK APPOINTMENT IN JOURNAL (PDR_JOURNAL) ONLY IF ITS STAGE 1 NOTIFICATION IS GOING TO BE RECORDED
-------------------------------------------------------------------------------------------------------------------------------------	       
	---FIRST OF ALL CHECK EITHER RECORD IS ALREADY INSERTED IN PDR_JOURNAL FOR GAS APPOINTMENT / ELECTRIC APPOINTMENT OR NOT 
	---IF YES THEN WE WOULD NOT INSERT NEW RECORD IN PDR_JOURNAL FOR GAS OR ELECTRIC APPOINTMENT
	DECLARE @gasCheckJournalId int, @electricCheckJournalId int,@terminationDate DATETIME,@reletDate DATETIME

	SELECT @gasCheckJournalId = GasCheckJournalId, @electricCheckJournalId = ElectricCheckJournalId 
	FROM V_BritishGasVoidNotification
	WHERE BritishGasVoidNotificationId = @britishGasId
	Select @terminationDate=TerminationDate,@reletDate=ReletDate from PDR_MSAT INNER JOIN PDR_MSATType ON PDR_MSAT.MSATTypeId= PDR_MSATType.MSATTypeId	
	  WHERE 
	   PropertyId = @propertyId
	   AND CustomerId = @customerId
	   AND TenancyId = @tenancyId	
		AND MSATTypeName = 'Void Inspection'	
	
	IF @gasCheckJournalId IS NULL AND @electricCheckJournalId IS NULL AND @isGasElectricCheck = 1 
	BEGIN
		DECLARE @electricCheckTypeId int, @gasCheckTypeId int, @msatId int, @toBeArrangedStatusId int, @journalId int

		--GET THE VOID ELECTRIC CHECK / GAS ELECTRIC CHECK STATUS ID
		IF @isGasElectricCheck = 1 
			BEGIN 			
				SELECT @gasCheckTypeId = MSATTypeId
				FROM PDR_MSATType
				WHERE MSATTypeName = 'Void Gas Check'

				SELECT @electricCheckTypeId = MSATTypeId
				FROM PDR_MSATType
				WHERE MSATTypeName = 'Void Electric Check'			
			END	
	
		---GET To Be Arranged Status Id
		SELECT @toBeArrangedStatusId = PDR_STATUS.STATUSID
		FROM PDR_STATUS
		WHERE TITLE = 'To be Arranged'
	
		------------------------------FOR GAS APPOINTMENT TO BE ARRANGED ENTRY----------------------------------
		--------------------------------------------------------------------------------------------------------
		---INSERT RECORD IN PDR_MSAT (FOR GAS)
		INSERT INTO PDR_MSAT (PropertyId, MSATTypeId, CustomerId, TenancyId,IsActive,TerminationDate,ReletDate)
		VALUES( @propertyId, @gasCheckTypeId, @customerId, @tenancyId,1,@terminationDate,@reletDate)
	
		--Get The Recent MsatId
		SET
		@msatId = SCOPE_IDENTITY()

		--INSERT RECORD IN PDR_JOURNAL
		INSERT INTO [PDR_JOURNAL] ([MSATID], [STATUSID], [CREATIONDATE], [CREATEDBY])
		VALUES (@msatId, @toBeArrangedStatusId, GETDATE(), @userId)

		SET @gasCheckJournalId =  SCOPE_IDENTITY()

		------------------------------FOR ELECTRIC APPOINTMENT TO BE ARRANGED ENTRY----------------------------------
		-------------------------------------------------------------------------------------------------------------
		---INSERT RECORD IN PDR_MSAT (FOR ELECTRIC)
		INSERT INTO PDR_MSAT (PropertyId, MSATTypeId, CustomerId, TenancyId,IsActive,TerminationDate,ReletDate)
		VALUES( @propertyId, @electricCheckTypeId, @customerId, @tenancyId,1,@terminationDate,@reletDate)
	
		--Get The Recent MsatId
		SET
		@msatId = SCOPE_IDENTITY()

		--INSERT RECORD IN PDR_JOURNAL
		INSERT INTO [PDR_JOURNAL] ([MSATID], [STATUSID], [CREATIONDATE], [CREATEDBY])
		VALUES (@msatId, @toBeArrangedStatusId, GETDATE(), @userId)

		SET @electricCheckJournalId =  SCOPE_IDENTITY()

		--------------------------UPDATE GAS CHECK JOURNAL ID & ELECTRIC CHECK JOURNAL ID IN BRITISH GAS VOID NOTIFICATION-----------------
		-----------------------------------------------------------------------------------------------------------------------------------
		--Update British Gas Void Notification Table With Journal Id		
		UPDATE [dbo].[V_BritishGasVoidNotification]
		SET
		 GasCheckJournalId = @gasCheckJournalId 
		,ElectricCheckJournalId = @electricCheckJournalId	
		WHERE BritishGasVoidNotificationId = @britishGasId
	END
	
			   	  
END TRY    
BEGIN CATCH     
 IF @@TRANCOUNT > 0    
 BEGIN         
  ROLLBACK TRANSACTION;       
  SET @isSaved = 0            
 END    
 DECLARE @ErrorMessage NVARCHAR(4000);    
 DECLARE @ErrorSeverity INT;    
 DECLARE @ErrorState INT;    
    
 SELECT @ErrorMessage = ERROR_MESSAGE(),    
 @ErrorSeverity = ERROR_SEVERITY(),    
 @ErrorState = ERROR_STATE();    
    
 -- Use RAISERROR inside the CATCH block to return     
 -- error information about the original error that     
 -- caused execution to jump to the CATCH block.    
 RAISERROR (@ErrorMessage, -- Message text.    
    @ErrorSeverity, -- Severity.    
    @ErrorState -- State.    
   );    
END CATCH;    
    
IF @@TRANCOUNT > 0    
 BEGIN      
  COMMIT TRANSACTION;      
  SET @isSaved = 1    
 END    
  
    
END
GO
/****** Object:  StoredProcedure [dbo].[V_UpdateBritishGasData]    Script Date: 09/07/2015 15:17:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
  Name:     [V_UpdateBritishGasData]

  Author: Noor Muhammad
  Creation Date: June-16-2015
  Description: Get British Gas Stage 2 Data
  History:  16/06/2015 Noor : Query for update of british gas data
            30/09/2014 Name : Correct the query to insert 2 records in pdr_journal
  Execution Command:
  
    
-- ============================================= */   
CREATE PROCEDURE [dbo].[V_UpdateBritishGasData]    
 -- Add the parameters for the stored procedure here    
 @britishGasId int 
,@customerId int 
,@propertyId varchar(20)  
,@tenancyId int		
,@currentTenantName varchar(100) = null
,@documentName varchar(250) = null
,@electricDebtAmount money = null 
,@electricDebtAmountDate date = null
,@electricMeterReading bigint = null
,@electricMeterReadingDate date = null
,@electricMeterTypeId int = null
,@gasDebtAmount money = null
,@gasDebtAmountDate date = null
,@gasMeterReading bigint = null
,@gasMeterReadingDate date = null
,@gasMeterTypeId int = null
,@isGasElectricCheck bit = null
,@newElectricMeterReading bigint = null
,@newElectricMeterReadingDate date = null
,@newGasMeterReading bigint = null
,@newGasMeterReadingDate date = null
,@newTenantDateOfBirth date = null
,@newTenantMobile varchar(30) = null
,@newTenantName varchar(100) = null
,@newTenantOccupancyDate date = null
,@newTenantPreviousAddress1 varchar(50) = null
,@newTenantPreviousAddress2 varchar(50) = null
,@newTenantPreviousPostCode varchar (20) = null
,@newTenantPreviousTownCity varchar(50) = null
,@newTenantTel varchar(30) = null
,@occupancyCeaseDate date = null
,@propertyAddress1 varchar(50) = null
,@propertyAddress2 varchar(50) = null
,@propertyCity varchar(50) = null
,@propertyCounty varchar(50) = null
,@propertyPostCode varchar (20) = null
,@tenantFwAddress1 varchar(50) = null
,@tenantFwAddress2 varchar(50) = null
,@tenantFwCity varchar(50) = null
,@tenantFwPostCode varchar(30) = null
,@britishGasEmail varchar(100) = null
,@britishGasStageId int = null
,@isNotificationSent bit=0
,@userId int = null
,@isSaved bit = 0 out    
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
                 
BEGIN TRANSACTION;    
BEGIN TRY     
                  
   UPDATE [dbo].[V_BritishGasVoidNotification]
   SET [CustomerId] = @customerId
      ,[PropertyId] = @propertyId
      ,[TenancyId] =  @tenancyId      
	  ,[IsGasCheck] = @isGasElectricCheck 
	  ,[IsElectricCheck]=@isGasElectricCheck
      ,[TenantFwAddress1] = @tenantFwAddress1
      ,[TenantFwAddress2] = @tenantFwAddress2
      ,[TenantFwCity] = @tenantFwCity
      ,[TenantFwPostCode] = @tenantFwPostCode
      ,[DateOccupancyCease] = @occupancyCeaseDate
      ,[BritishGasEmail] = @britishGasEmail
      ,[GasMeterTypeId] = @gasMeterTypeId
      ,[GasMeterReading] = @gasMeterReading
      ,[GasMeterReadingDate] = @gasMeterReadingDate
      ,[ElectricMeterTypeId] = @electricMeterTypeId
      ,[ElectricMeterReading] = @electricMeterReading
      ,[ElectricMeterReadingDate] = @electricMeterReadingDate
      ,[GasDebtAmount] = @gasDebtAmount
      ,[GasDebtAmountDate] = @gasDebtAmountDate
      ,[ElectricDebtAmount] = @electricDebtAmount
      ,[ElectricDebtAmountDate] = @electricDebtAmountDate
      ,[NewTenantName] = @newTenantName
      ,[NewTenantDateOfBirth] = @newTenantDateOfBirth
      ,[NewTenantTel] = @newTenantTel
      ,[NewTenantMobile] = @newTenantMobile
      ,[NewTenantOccupancyDate] = @newTenantOccupancyDate
      ,[NewTenantPreviousAddress1] = @newTenantPreviousAddress1
      ,[NewTenantPreviousAddress2] = @newTenantPreviousAddress2
      ,[NewTenantPreviousTownCity] = @newTenantPreviousTownCity
      ,[NewTenantPreviousPostCode] = @newTenantPreviousPostCode
      ,[NewGasMeterReading] = @newGasMeterReading
      ,[NewGasMeterReadingDate] = @newGasMeterReadingDate
      ,[NewElectricMeterReading] = @newElectricMeterReading
      ,[NewElectricMeterReadingDate] = @newElectricMeterReadingDate   
      ,[StageId] = @britishGasStageId
      ,[DocumentName] = @documentName   
      ,isNotificationSent = @isNotificationSent 
      ,[ModifiedDate] = CURRENT_TIMESTAMP
	  ,[UserId] = @userId
 WHERE 
	   PropertyId = @propertyId
	   AND CustomerId = @customerId
	   AND TenancyId = @tenancyId	
	   AND BritishGasVoidNotificationId = @britishGasId   
	
-------------------------------------------------------------------------------------------------------------------------------------
--INSERT RECORD FOR GAS ELECTRIC CHECK APPOINTMENT IN JOURNAL (PDR_JOURNAL) ONLY IF ITS STAGE 1 NOTIFICATION IS GOING TO BE RECORDED
-------------------------------------------------------------------------------------------------------------------------------------	       
	---FIRST OF ALL CHECK EITHER RECORD IS ALREADY INSERTED IN PDR_JOURNAL FOR GAS APPOINTMENT / ELECTRIC APPOINTMENT OR NOT 
	---IF YES THEN WE WOULD NOT INSERT NEW RECORD IN PDR_JOURNAL FOR GAS OR ELECTRIC APPOINTMENT
		
		DECLARE @gasCheckJournalId int, @electricCheckJournalId int

		SELECT @gasCheckJournalId = GasCheckJournalId, @electricCheckJournalId = ElectricCheckJournalId 
		FROM V_BritishGasVoidNotification
		WHERE BritishGasVoidNotificationId = @britishGasId

		IF @gasCheckJournalId IS NULL AND @electricCheckJournalId IS NULL AND @isGasElectricCheck = 1 
		BEGIN
			DECLARE @electricCheckTypeId int, @gasCheckTypeId int, @msatId int, @toBeArrangedStatusId int, @journalId int

			--GET THE VOID ELECTRIC CHECK / GAS ELECTRIC CHECK STATUS ID
			IF @isGasElectricCheck = 1 
				BEGIN 			
					SELECT @gasCheckTypeId = MSATTypeId
					FROM PDR_MSATType
					WHERE MSATTypeName = 'Void Gas Check'

					SELECT @electricCheckTypeId = MSATTypeId
					FROM PDR_MSATType
					WHERE MSATTypeName = 'Void Electric Check'			
				END	
		
			---GET To Be Arranged Status Id
			SELECT @toBeArrangedStatusId = PDR_STATUS.STATUSID
			FROM PDR_STATUS
			WHERE TITLE = 'To be Arranged'
		
			------------------------------FOR GAS APPOINTMENT TO BE ARRANGED ENTRY----------------------------------
			--------------------------------------------------------------------------------------------------------
			---INSERT RECORD IN PDR_MSAT (FOR GAS)
			INSERT INTO PDR_MSAT (PropertyId, MSATTypeId, CustomerId, TenancyId)
			VALUES( @propertyId, @gasCheckTypeId, @customerId, @tenancyId)
		
			--Get The Recent MsatId
			SET
			@msatId = SCOPE_IDENTITY()

			--INSERT RECORD IN PDR_JOURNAL
			INSERT INTO [PDR_JOURNAL] ([MSATID], [STATUSID], [CREATIONDATE], [CREATEDBY])
			VALUES (@msatId, @toBeArrangedStatusId, GETDATE(), @userId)

			SET @gasCheckJournalId =  SCOPE_IDENTITY()

			------------------------------FOR ELECTRIC APPOINTMENT TO BE ARRANGED ENTRY----------------------------------
			-------------------------------------------------------------------------------------------------------------
			---INSERT RECORD IN PDR_MSAT (FOR ELECTRIC)
			INSERT INTO PDR_MSAT (PropertyId, MSATTypeId, CustomerId, TenancyId)
			VALUES( @propertyId, @electricCheckTypeId, @customerId, @tenancyId)
		
			--Get The Recent MsatId
			SET
			@msatId = SCOPE_IDENTITY()

			--INSERT RECORD IN PDR_JOURNAL
			INSERT INTO [PDR_JOURNAL] ([MSATID], [STATUSID], [CREATIONDATE], [CREATEDBY])
			VALUES (@msatId, @toBeArrangedStatusId, GETDATE(), @userId)

			SET @electricCheckJournalId =  SCOPE_IDENTITY()

			--------------------------UPDATE GAS CHECK JOURNAL ID & ELECTRIC CHECK JOURNAL ID IN BRITISH GAS VOID NOTIFICATION-----------------
			-----------------------------------------------------------------------------------------------------------------------------------
			--Update British Gas Void Notification Table With Journal Id		
			UPDATE [dbo].[V_BritishGasVoidNotification]
			SET
			 GasCheckJournalId = @gasCheckJournalId 
			,ElectricCheckJournalId = @electricCheckJournalId	
			WHERE BritishGasVoidNotificationId = @britishGasId
		END

			   	  
END TRY    
BEGIN CATCH     
 IF @@TRANCOUNT > 0    
 BEGIN         
  ROLLBACK TRANSACTION;       
  SET @isSaved = 0            
 END    
 DECLARE @ErrorMessage NVARCHAR(4000);    
 DECLARE @ErrorSeverity INT;    
 DECLARE @ErrorState INT;    
    
 SELECT @ErrorMessage = ERROR_MESSAGE(),    
 @ErrorSeverity = ERROR_SEVERITY(),    
 @ErrorState = ERROR_STATE();    
    
 -- Use RAISERROR inside the CATCH block to return     
 -- error information about the original error that     
 -- caused execution to jump to the CATCH block.    
 RAISERROR (@ErrorMessage, -- Message text.    
    @ErrorSeverity, -- Severity.    
    @ErrorState -- State.    
   );    
END CATCH;    
    
IF @@TRANCOUNT > 0    
 BEGIN      
  COMMIT TRANSACTION;      
  SET @isSaved = 1    
 END    
  
    
END
GO
/****** Object:  StoredProcedure [dbo].[V_ScheduleVoidRequiredWorks]    Script Date: 09/07/2015 15:17:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:           Ali Raza
-- Create date:      28/07/2015
-- Description:		 Schedule Void Required Works Appointments
-- History:          28/07/2015 AR : Schedule Void Required Works Appointments      
-- =============================================
CREATE PROCEDURE [dbo].[V_ScheduleVoidRequiredWorks]    
 -- Add the parameters for the stored procedure here    
 @userId int 
,@appointmentNotes varchar(1000)  
,@jobSheetNotes varchar(1000)    
,@appointmentStartDate date    
,@appointmentEndDate date    
,@startTime varchar(10)    
,@endTime varchar(10)    
,@operativeId int         
,@inspectionJournalId INT    
,@duration float 
--,@requiredWorkIds varchar(1000) 
,@voidRequiredWorkDuration   as VoidRequiredWorkDuration Readonly
,@isSaved int = 0 out    
,@appointmentIdOut int = -1 out 
,@journalIdOut int = -1 out     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
             
DECLARE 
@ArrangedId int    
,@JournalHistoryId int    
,@AppointmentId int    
,@AppointmentHistoryId int    
,@PropertyId varchar(100) = NULL
,@CustomerId int = NULL
,@TenancyId int = NULL   
,@MSATTypeId INT
,@MSATId INT
,@journalId INT 
,@terminationDate DATETIME
,@reletDate DATETIME  
BEGIN TRANSACTION;    
BEGIN TRY    
-- =============================================    
-- Get PropertyId,TenancyId and customerId by inspectionJournalId    
-- =============================================  

Select @PropertyId=PropertyId,@TenancyId=TenancyId,@CustomerId=CustomerId,@terminationDate=TerminationDate,@reletDate=ReletDate from PDR_MSAT
INNER JOIN PDR_JOURNAL ON PDR_JOURNAL.MSATID= PDR_MSAT.MSATId
Where JOURNALID=@inspectionJournalId
-- =============================================    
-- Get status id of "Arranged"    and MSATTypeId
-- =============================================     
	SELECT  @ArrangedId = PDR_STATUS.statusid 
	FROM	PDR_STATUS 
	WHERE	PDR_STATUS.title ='Arranged'   
	
	SELECT @MSATTypeId = MSATTypeId FROM PDR_MSATType WHERE MSATTypeName = 'Void Works'
	
-- ====================================================================================================    
--          INSERT PDR_MSAT AND  PDR_JOURNAL    
-- ====================================================================================================    	
	
	
INSERT INTO PDR_MSAT (PropertyId, MSATTypeId, CustomerId, TenancyId,TerminationDate,ReletDate)
	VALUES (@PropertyId, @MSATTypeId, @CustomerId, @TenancyId,@terminationDate,@reletDate)

SELECT
	@MSATId = SCOPE_IDENTITY()

INSERT INTO [PDR_JOURNAL] ([MSATID], [STATUSID], [CREATIONDATE], [CREATEDBY])
	VALUES (@MSATId, @ArrangedId, GETDATE(), @userId)

SELECT @journalId = SCOPE_IDENTITY()
	

-- ====================================================================================================    
--          INSERTION (PDR_APPOINTMENTS)    
-- ====================================================================================================    
    
	SELECT	@JournalHistoryId = MAX(JOURNALHISTORYID)    
	FROM	PDR_JOURNAL_HISTORY    
	WHERE	JOURNALID = @journalId    
      
 PRINT 'JOURNALHISTORYID = '+ CONVERT(VARCHAR,@JournalHistoryId)  
    
    
           
    INSERT INTO [PDR_APPOINTMENTS]
           (
            [JOURNALID]
           ,[JOURNALHISTORYID]
           ,[APPOINTMENTSTARTDATE]
           ,[APPOINTMENTENDDATE]
           ,[APPOINTMENTSTARTTIME]
           ,[APPOINTMENTENDTIME]
           ,[ASSIGNEDTO]
           ,[CREATEDBY]
           ,[LOGGEDDATE]
           ,[APPOINTMENTNOTES] 
           ,[CUSTOMERNOTES]          
           ,[APPOINTMENTSTATUS]
           ,[DURATION])
     VALUES
           (
            @journalId
           ,@JournalHistoryId
           ,@appointmentStartDate
           ,@appointmentEndDate
           ,@startTime
           ,@endTime
           ,@operativeId    
           ,@userId  
           ,GETDATE()
           ,@appointmentNotes
           ,@jobSheetNotes           
           ,'NotStarted' 
           ,@duration)   
          
      SELECT @AppointmentId = SCOPE_IDENTITY()    
      PRINT 'APPOINTMENTID = '+ CONVERT(VARCHAR,@AppointmentId)            
     
     IF(@AppointmentId>0)
		 BEGIN
		 
		 CREATE TABLE #TMPDURATION( TMP_SID int, TMP_DURATION float )
	
	INSERT	INTO #TMPDURATION (TMP_SID, TMP_DURATION) 
	SELECT	RequiredWorksId, Duration 
	FROM	@voidRequiredWorkDuration 
	
	
	UPDATE	V_RequiredWorks 
	SET		WorksJournalId=@journalId, IsScheduled=1, StatusId=@ArrangedId,Duration=TMP_DURATION,ModifiedDate=GETDATE()
	,VoidWorksNotes=@jobSheetNotes
	FROM	#TMPDURATION
	WHERE	V_RequiredWorks.RequiredWorksId = #TMPDURATION.TMP_SID   


	DROP TABLE #TMPDURATION 
		 
		 
		 --Update V_RequiredWorks SET WorksJournalId=@journalId, IsScheduled=1, StatusId=@ArrangedId WHERE RequiredWorksId IN (SELECT COLUMN1 from dbo.SPLIT_STRING(@requiredWorkIds,','))
		 
		 END
     ELSE
		 BEGIN
		  ROLLBACK TRANSACTION;       
		  SET @isSaved = 0          
		 END
	  SELECT @appointmentIdOut = @AppointmentId
       SELECT @journalIdOut =    @journalId
END TRY    
BEGIN CATCH     
 IF @@TRANCOUNT > 0    
 BEGIN         
  ROLLBACK TRANSACTION;       
  SET @isSaved = 0            
 END    
 DECLARE @ErrorMessage NVARCHAR(4000);    
 DECLARE @ErrorSeverity INT;    
 DECLARE @ErrorState INT;    
    
 SELECT @ErrorMessage = ERROR_MESSAGE(),    
 @ErrorSeverity = ERROR_SEVERITY(),    
 @ErrorState = ERROR_STATE();    
    
 -- Use RAISERROR inside the CATCH block to return     
 -- error information about the original error that     
 -- caused execution to jump to the CATCH block.    
 RAISERROR (@ErrorMessage, -- Message text.    
    @ErrorSeverity, -- Severity.    
    @ErrorState -- State.    
   );    
END CATCH;    
    
IF @@TRANCOUNT > 0    
 BEGIN      
  COMMIT TRANSACTION;      
  SET @isSaved = 1    
 END    
  
    
END
GO
/****** Object:  StoredProcedure [dbo].[V_SaveTenancyTermination]    Script Date: 09/07/2015 15:17:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:           Ali Raza
-- Create date:      28/05/2015
-- Description:		 Save Tenancy Termination for 1st Inspection
-- History:          28/05/2015 AR : Save Tenancy Termination for 1st Inspection    
-- =============================================


CREATE PROCEDURE [dbo].[V_SaveTenancyTermination]
	@PropertyId varchar(100) = NULL,
	@CustomerId int = NULL,
	@TenancyId int = NULL,
	@TerminationDate DateTime,
	@UpdatedBy int,
	@Inspection int=1
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from

SET NOCOUNT ON;

DECLARE @MSATTypeId INT,
		@MSATId INT,
		@ToBeArrangedStatusId INT
---Declare Cursor variable            
DECLARE @ItemToInsertCursor CURSOR

SELECT
	@ToBeArrangedStatusId = PDR_STATUS.STATUSID
FROM PDR_STATUS
WHERE TITLE = 'To be Arranged'

IF (@Inspection = 1)
	BEGIN
SELECT
	@MSATTypeId = MSATTypeId
FROM PDR_MSATType
WHERE MSATTypeName = 'Void Inspection'
END

IF Not EXISTS(SELECT MSATID from PDR_MSAT where PropertyId=@PropertyId AND CustomerId=@CustomerId AND TenancyId=@TenancyId)
 BEGIN
		INSERT INTO PDR_MSAT (PropertyId, MSATTypeId, CustomerId, TenancyId, TerminationDate)
			VALUES (@PropertyId, @MSATTypeId, @CustomerId, @TenancyId, @TerminationDate)

		SELECT
			@MSATId = SCOPE_IDENTITY()

		INSERT INTO [PDR_JOURNAL] ([MSATID], [STATUSID], [CREATIONDATE], [CREATEDBY])
			VALUES (@MSATId, @ToBeArrangedStatusId, GETDATE(), @UpdatedBy)

		return SCOPE_IDENTITY()
	END
END
GO
/****** Object:  StoredProcedure [dbo].[V_GetPropertyAndTenantInfoByJournalId]    Script Date: 09/07/2015 15:17:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:           Ali Raza
-- Create date:      28/05/2015
-- Description:		 get property and tenant Info
-- History:          28/05/2015 AR : get property and tenant Info to send notification from tenancy termination process   
-- =============================================
CREATE PROCEDURE [dbo].[V_GetPropertyAndTenantInfoByJournalId]
	(@journalId int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

 SELECT TenancyId,PropertyId,CustomerId from PDR_JOURNAL
 INNER JOIN PDR_MSAT ON PDR_JOURNAL.MSATID= PDR_MSAT.MSATId
 where JOURNALID=@journalId
END
GO
/****** Object:  StoredProcedure [dbo].[V_CancelGasElectricAppointment]    Script Date: 09/07/2015 15:16:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:           Ali Raza
-- Create date:      03/07/2015
-- Description:      Cancel Gas/Electric Appointment 
-- History:          03/07/2015 AR: Cancel Gas/Electric Appointment 
--                   26/08/2015 AR:	Added query to UPDATE V_AppointmentRecordedData for 'Void Gas Check' and 'Void Electric Check' appointment
-- =============================================

CREATE PROCEDURE [dbo].[V_CancelGasElectricAppointment]
	@journalId int	,
	@createdBy Int,
	@isCancelled int = 0 out
AS
BEGIN


BEGIN TRANSACTION;
BEGIN TRY

	-- ========================================================
	--	INSERT INTO PDR_CANCELLED_JOBS
	-- ========================================================
	INSERT INTO PDR_CANCELLED_JOBS (AppointmentId,RecordedOn)
	SELECT PDR_APPOINTMENTS.APPOINTMENTID as AppointmentId, GETDATE() as RecordedOn
	FROM PDR_APPOINTMENTS
	WHERE PDR_APPOINTMENTS.JournalId = @journalId
	AND PDR_APPOINTMENTS.APPOINTMENTSTATUS <> 'Complete'
	
	
	-- ========================================================
	--	UPDATE PDR_JOURNAL
	-- ========================================================
	
	UPDATE	PDR_JOURNAL
	SET		PDR_JOURNAL.STATUSID = (SELECT PDR_STATUS.STATUSID 
									FROM PDR_STATUS 
									WHERE PDR_STATUS.TITLE= 'Cancelled' )
	WHERE	PDR_JOURNAL.JOURNALID = @journalId

	-- ========================================================
	--	Insert New entry in PDR_Journal with status 'To Be Arranged'
	-- ========================================================
	DECLARE @MSATID INT,@statusId int,@newJournalId int,@msatType nvarchar(50)
	Select @MSATID=MSATID FROM PDR_JOURNAL 	WHERE	PDR_JOURNAL.JOURNALID = @journalId

	SELECT @statusId=PDR_STATUS.STATUSID FROM PDR_STATUS WHERE PDR_STATUS.TITLE= 'To be Arranged'
	
	INSERT INTO PDR_JOURNAL(MSATID,STATUSID,CREATIONDATE,CREATEDBY)
	VALUES(@MSATID,@statusId,GETDATE(), @createdBy)
	
	SET @newJournalId = SCOPE_IDENTITY()
	
	-- ========================================================
	--UPDATE V_AppointmentRecordedData for 'Void Gas Check' and 'Void Electric Check' appointment
	-- ========================================================
	SELECT @msatType=MSATTypeName from PDR_MSAT INNER JOIN PDR_MSATType on PDR_MSAT.MSATTypeId=PDR_MSATType.MSATTypeId
	Where MSATId=@MSATID
	
	IF @msatType='Void Gas Check'
		BEGIN
			UPDATE  V_AppointmentRecordedData SET GasCheckJournalId=@newJournalId where GasCheckJournalId=@journalId
		END
	ELSE IF @msatType='Void Electric Check'
		BEGIN
			UPDATE  V_AppointmentRecordedData SET ElectricCheckJournalId=@newJournalId where ElectricCheckJournalId=@journalId
		END	
	
	END TRY
BEGIN CATCH 
    IF @@TRANCOUNT > 0
  BEGIN     
   ROLLBACK TRANSACTION;   
 SET @isCancelled = 0
    
    DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    -- Use RAISERROR inside the CATCH block to return 
    -- error information about the original error that 
    -- caused execution to jump to the CATCH block.
    RAISERROR (@ErrorMessage, -- Message text.
         @ErrorSeverity, -- Severity.
         @ErrorState -- State.
         );
  END
END CATCH;

IF @@TRANCOUNT > 0
 BEGIN  
  COMMIT TRANSACTION;  
  SET @isCancelled = 1
 END

END
GO
/****** Object:  StoredProcedure [dbo].[V_GetVoidWorksRequired]    Script Date: 09/07/2015 15:17:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:           Ali Raza
-- Create date:      28/05/2015
-- Description:		 Get First Inspections To Be Arranged List 
-- History:          28/05/2015 AR : Get First Inspections To Be Arranged List 
-- =============================================

--  DECLARE	@totalCount int
--EXEC	 [dbo].[V_GetFirstInspectionArranged]
--		@searchText = NULL,
--		@pageSize = 100,
--		@pageNumber = 1,
--		@totalCount = @totalCount OUTPUT
--SELECT	@totalCount as N'@totalCount'
-- =============================================
CREATE PROCEDURE [dbo].[V_GetVoidWorksRequired]
-- Add the parameters for the stored procedure here
		@JournalId int,
		@isRearrange BIT = 0,
	--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'RequiredWorksId', 
		@sortOrder varchar (5) = 'DESC',
		@totalCount int = 0 output	
AS
BEGIN
	DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(2000),
        
        --variables for paging
        @offset int,
		@limit int
		
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		
		
		--=====================Search Criteria===============================
		SET @searchCriteria = ' (V_RequiredWorks.IsCanceled IS NULL OR V_RequiredWorks.IsCanceled=0) '
		IF(@isRearrange=1)
			BEGIN
					SET @searchCriteria =@searchCriteria + CHAR(10) +  ' AND  V_RequiredWorks.WorksJournalId= '+convert(varchar(10),@JournalId)+''

			END
		ELSE 
			BEGIN
					SET @searchCriteria = @searchCriteria + CHAR(10) + '  AND  V_RequiredWorks.InspectionJournalId= '+convert(varchar(10),@JournalId)+''

			END
		
		--=======================Select Clause=============================================
		SET @SelectClause = 'Select top ('+convert(varchar(10),@limit)+')
							 	 RequiredWorksId,PDR_JOURNAL.JOURNALID,Case WHEN PDR_JOURNAL.JOURNALID  IS NULL THEN ''-'' ELSE ISNULL( ''JSV''+CONVERT(VARCHAR, RIGHT(''000000''+ CONVERT(VARCHAR,RequiredWorksId),4)),''-'')END AS Ref,V_RoomList.Name AS Location,V_RequiredWorks.WorkDescription,
ISNULL(CONVERT(nvarchar(50),PDR_APPOINTMENTS.APPOINTMENTSTARTDATE, 103)+''</br> ''+CONVERT(VARCHAR(5), PDR_APPOINTMENTS.APPOINTMENTSTARTTIME, 108) +''(''+LEFT(E__EMPLOYEE.Firstname, 1)+''''+LEFT(E__EMPLOYEE.LASTNAME, 1)+'')'',''-'')
 as Appointment,ISNULL(TenantNeglectEstimation,0)as Neglect,ISNULL(IsTenantWorks,0) as IsTenantWorks,ISNULL(S_ORGANISATION.NAME,''-'') as Contractor,
 case When V_RequiredWorks.IsScheduled=1 OR IsTenantWorks=1 Then 0 Else 1 END as Checked,V_RequiredWorks.IsScheduled,
 V_RequiredWorks.RoomId As RoomId,ISNULL(E__EMPLOYEE.Firstname+'' ''+E__EMPLOYEE.LASTNAME,''-'') as Operative,
 ISNULL(CONVERT(nvarchar(50),PDR_APPOINTMENTS.APPOINTMENTSTARTDATE, 103)+'' ''+CONVERT(VARCHAR(5), PDR_APPOINTMENTS.APPOINTMENTSTARTTIME, 108),''-'') as AppointmentStartDateTime
,PDR_APPOINTMENTS.APPOINTMENTNOTES As AppointmentNotes,PDR_APPOINTMENTS.CUSTOMERNOTES as JobSheetNotes,ISNULL(PDR_Status.Title,''-'') AS WorkStatus,
PDR_APPOINTMENTS.AppointmentId AS AppointmentId,PDR_MSAT.TerminationDate as Termination ,
Case When PDR_MSAT.ReletDate is NULL then CONVERT(nvarchar(50),DATEADD(day,7,PDR_MSAT.TerminationDate), 103) Else	CONVERT(nvarchar(50),PDR_MSAT.ReletDate, 103) END as ReletDate,
ISNULL(P__PROPERTY.HouseNumber, '''')	+ ISNULL('' ''+P__PROPERTY.ADDRESS1, '''') 	+ ISNULL('', ''+P__PROPERTY.ADDRESS2, '''') AS Address
,InspectionJournalId
 '
		
		
		--============================From Clause============================================
		SET @fromClause = CHAR(10) +'  FROM
V_RequiredWorks
LEFT JOIN PDR_JOURNAL ON V_RequiredWorks.WorksJournalId= PDR_JOURNAL.JOURNALID 
LEFT JOIN	PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId 
LEFT JOIN PDR_APPOINTMENTS ON PDR_APPOINTMENTS.JOURNALID= PDR_JOURNAL.JOURNALID
INNER JOIN V_RoomList ON V_RequiredWorks.RoomId = V_RoomList.RoomId
LEFT JOIN E__EMPLOYEE ON PDR_APPOINTMENTS.ASSIGNEDTO = E__EMPLOYEE.EMPLOYEEID
LEFT JOIN PDR_CONTRACTOR_WORK ON PDR_CONTRACTOR_WORK.JournalId = PDR_JOURNAL.JOURNALID 
LEFT JOIN S_ORGANISATION ON PDR_CONTRACTOR_WORK.ContractorId = S_ORGANISATION.ORGID
LEFT JOIN	P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID
LEFT JOIN PDR_Status ON V_RequiredWorks.StatusId= PDR_Status.StatusId AND PDR_Status.TITLE <> ''Cancelled''
		'
							
		--============================Order Clause==========================================
		
				
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		--===============================Main Query ====================================
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
		select PDR_JOURNAL.JOURNALID,'JSV'+CONVERT(VARCHAR, RIGHT('000000'+ CONVERT(VARCHAR,PDR_JOURNAL.JOURNALID),4)) AS Ref,
			ISNULL(P__PROPERTY.HouseNumber, '')	+ ISNULL(' '+P__PROPERTY.ADDRESS1, '') 	+ ISNULL(', '+P__PROPERTY.ADDRESS2, '') AS Address 
			,CONVERT(nvarchar(50),PDR_MSAT.TerminationDate, 103) as Termination	,
		Case When PDR_MSAT.ReletDate is NULL then CONVERT(nvarchar(50),DATEADD(day,7,PDR_MSAT.TerminationDate), 103) Else	CONVERT(nvarchar(50),PDR_MSAT.ReletDate, 103) END as ReletDate
			,E__EMPLOYEE.Firstname+' '+E__EMPLOYEE.LASTNAME as Recorded,CONVERT(nvarchar(50),CreatedDate, 103) as CreatedDate
 FROM 

		V_AppointmentRecordedData
		INNER JOIN PDR_JOURNAL ON PDR_JOURNAL.JOURNALID= V_AppointmentRecordedData.InspectionJournalId
		INNER JOIN	PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId
		INNER JOIN	PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId		
		LEFT JOIN	P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID	
		LEFT JOIN E__EMPLOYEE ON V_AppointmentRecordedData.CreatedBy = E__EMPLOYEE.EMPLOYEEID
where V_AppointmentRecordedData.InspectionJournalId=@JournalId
				
		
		--============================ Exec Final Query =================================
		print(@finalQuery)
		EXEC (@finalQuery)
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
END
GO
/****** Object:  StoredProcedure [dbo].[V_RequiredWorksAssignWorkToContractor]    Script Date: 09/07/2015 15:17:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ali Raza
-- Create date: 25/08/2014
-- Description:	Assign Work to Contractor.
-- History:          25/08/2014 AR : Save Purchase order for Required works Assign Work to Contractor
-- =============================================
CREATE PROCEDURE [dbo].[V_RequiredWorksAssignWorkToContractor] 
	-- Add the parameters for the stored procedure here
	@pdrContractorId INT,
	@inspectionJournalId INT,
	@ContractorId INT,
	@ContactId INT,
	@userId int,
	@Estimate SMALLMONEY,
	@EstimateRef NVARCHAR(200),	
	@POStatus INT,
	@requiredWorkIds varchar(1000) ,
	@ContractorWorksDetail AS PDR_AssingToContractorWorksRequired READONLY,
	@isSaved BIT = 0 OUTPUT,
	@journalIdOut INT OUTPUT
	
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;


/* Working of this stored Procedure
** 1- Get Status Id of "Assigned To Contractor" from Planned_Status. In case a Status of "Assigned To Contractor" 
**    is not present add a new one and get its Id.

** 2- Insert a new record in F_PURCHASEORDER and get the identity value as Order Id.
**
** 3- Insert a new record in PDR_CONTRACTOR_WORK using the given input data and OrderId
**    and get Identity Value as PDRContractorId.
**
** Loop (Insert Purchase Items and Works Required Items.
**
**   4- Insert a Purchase Item in F_PURCHASEITEM from given an constant data
**      and get Identity Value PURCHASEORDERITEMID
**
**   5- Insert a new work required from given data and also insert PURCHASEORDERITEMID
**
** End Loop
*/


BEGIN TRANSACTION
BEGIN TRY

-- =====================================================
-- General Purpose Variable
-- =====================================================
DECLARE 
@PropertyId varchar(100) = NULL
,@CustomerId int = NULL
,@TenancyId int = NULL   
,@MSATTypeId INT
,@MSATId INT
,@journalId INT 
,@terminationDate DATETIME
,@reletDate DATETIME  
,@MsatType NVARCHAR(200)
-- To save same time stamp in all records 
DECLARE @CurrentDateTime AS datetime2 = GETDATE()

--================================================================================
--Get Status Id for Status Title "Assigned To Contractor"
--In case (for first time) it does not exists Insert it and get Status Id.

-- Variables to get Status Id and Status History Id
DECLARE @newStatusId int = NULL

-- =====================================================
-- get status history id of "Assigned To Contractor"
-- =====================================================

SELECT	@newStatusId = STATUSID
FROM	PDR_STATUS
WHERE	PDR_STATUS.TITLE = 'Assigned To Contractor'
-- =============================================    
-- Get PropertyId,TenancyId and customerId by inspectionJournalId    
-- =============================================  

Select @PropertyId=PropertyId,@TenancyId=TenancyId,@CustomerId=CustomerId,@terminationDate=TerminationDate,@reletDate=ReletDate from PDR_MSAT
INNER JOIN PDR_JOURNAL ON PDR_JOURNAL.MSATID= PDR_MSAT.MSATId
Where JOURNALID=@inspectionJournalId
-- =============================================    
-- Get status id of "Arranged"    and MSATTypeId
-- =============================================     
	 
	
	SELECT @MSATTypeId = MSATTypeId,@MsatType=MSATTypeName FROM PDR_MSATType WHERE MSATTypeName = 'Void Works'
	
-- ====================================================================================================    
--          INSERT PDR_MSAT AND  PDR_JOURNAL    
-- ====================================================================================================    	
	
	
INSERT INTO PDR_MSAT (PropertyId, MSATTypeId, CustomerId, TenancyId,TerminationDate,ReletDate)
	VALUES (@PropertyId, @MSATTypeId, @CustomerId, @TenancyId,@terminationDate,@reletDate)

SELECT
	@MSATId = SCOPE_IDENTITY()

INSERT INTO [PDR_JOURNAL] ([MSATID], [STATUSID], [CREATIONDATE], [CREATEDBY])
	VALUES (@MSATId, @newStatusId, GETDATE(), @userId)

SELECT @journalId = SCOPE_IDENTITY()
	
UPDATE	V_RequiredWorks 
	SET		WorksJournalId=@journalId, IsScheduled=1, StatusId=@newStatusId,ModifiedDate=GETDATE()
WHERE RequiredWorksId IN (SELECT COLUMN1 from dbo.SPLIT_STRING(@requiredWorkIds,','))




IF (@PdrContractorId = -1)
BEGIN

-- =====================================================
-- Insert new Purchase Order
-- =====================================================

DECLARE @Active bit = 1
, @POTYPE int = (SELECT	POTYPEID
				FROM F_POTYPE
				WHERE POTYPENAME = 'Repair') -- 2 = 'Repair'

-- To get Identity Value of Purchase Order.
, @purchaseOrderId int

INSERT INTO F_PURCHASEORDER (PONAME, PODATE, PONOTES, USERID, SUPPLIERID, ACTIVE,
							POTYPE, POSTATUS, GASSERVICINGYESNO)
VALUES (UPPER(@MsatType + ' Work Order'), @CurrentDateTime
, 'This purchase order was created for '+ @MsatType +' from the new '+ @MsatType +' process.'
, @userId, @ContractorId, @ACTIVE, @POTYPE, @POSTATUS, 0)

SET @purchaseOrderId = SCOPE_IDENTITY()

-- =====================================================
-- Insert new PDR_CONTRACTOR_WORK
-- =====================================================

	INSERT INTO [PDR_CONTRACTOR_WORK]
           ([JournalId],[ContractorId],[ContactId],[AssignedDate],[AssignedBy],[Estimate],[EstimateRef],[PurchaseOrderId])
	VALUES
           (@journalId,@ContractorId,@ContactId,@CurrentDateTime,@userId,@Estimate,@EstimateRef,@PurchaseOrderId)
    SET @PdrContractorId = SCOPE_IDENTITY()    
           
END
ELSE
BEGIN
	UPDATE [PDR_CONTRACTOR_WORK]
	SET [ContractorId] = @ContractorId
	,[ContactId] = @ContactId
	,[AssignedDate] = @CurrentDateTime
	,[AssignedBy] = @userId
	,[Estimate] = @Estimate
	,[EstimateRef] = @EstimateRef
	
	WHERE PDRContractorId = @PdrContractorId
END


-- =====================================================
-- Declare a cursor to enter works requied,
--  loop through record and instert in table
-- =====================================================

DECLARE worksRequiredCursor CURSOR FOR SELECT
	*
FROM @ContractorWorksDetail
OPEN worksRequiredCursor

-- Declare Variable to use with cursor
DECLARE
@WorkDetailId int ,
@ServiceRequired nvarchar(4000),
@NetCost smallmoney,
@VatType int,
@VAT smallmoney,
@GROSS smallmoney,
@PIStatus int,
@ExpenditureId int,
@CostCenterId int,
@BudgetHeadId int

-- Variable used within loop
DECLARE @PurchaseItemTITLE nvarchar(20) = @MsatType -- Title for Purchase Items, specially to inset in F_PurchaseItem

		-- =====================================================
		-- Loop (Start) through records and insert works required
		-- =====================================================		
		-- Fetch record for First loop iteration.
		FETCH NEXT FROM worksRequiredCursor INTO @WorkDetailId,@ServiceRequired, @NetCost, @VatType, @VAT,
		@GROSS, @PIStatus, @ExpenditureId,@CostCenterId,@BudgetHeadId
		WHILE @@FETCH_STATUS = 0 BEGIN
	
		IF (@WorkDetailId = -1)
		BEGIN
			-- =====================================================
			--Insert Values in F_PURCHASEITEM for each work required and get is identity value.
			-- =====================================================

			INSERT INTO F_PURCHASEITEM (ORDERID, EXPENDITUREID, ITEMNAME, ITEMDESC, PIDATE,
									NETCOST, VATTYPE, VAT, GROSSCOST, USERID, ACTIVE, PITYPE, PISTATUS)
			VALUES (@PurchaseOrderId, @ExpenditureId, @PurchaseItemTITLE, @ServiceRequired, 
				@CurrentDateTime,  @NetCost, @VatType, @VAT, @GROSS, @userId, @ACTIVE, @POTYPE, @POSTATUS)

			DECLARE @ORDERITEMID int = SCOPE_IDENTITY()

			-- =====================================================
			-- Insert values in PDR_CONTRACTOR_WORK_DETAIL for each work required
			-- =====================================================

			INSERT INTO [PDR_CONTRACTOR_WORK_DETAIL]
           ([PDRContractorId],[ServiceRequired],[NetCost],[VatId],[Vat],[Gross],[ExpenditureId],[CostCenterId],[BudgetHeadId],[PURCHASEORDERITEMID])
			VALUES
           (@pdrContractorId,@ServiceRequired,@NetCost,@VatType,@VAT,@GROSS,@ExpenditureId,@CostCenterId,@BudgetHeadId,@ORDERITEMID)
           
		END

-- Fetch record for next loop iteration.
FETCH NEXT FROM worksRequiredCursor INTO @WorkDetailId, @ServiceRequired, @NetCost, @VatType, @VAT,
				@GROSS, @PIStatus, @ExpenditureId,@CostCenterId,@BudgetHeadId
END

-- =====================================================
-- Loop (End) through records and insert works required
-- =====================================================

CLOSE worksRequiredCursor
DEALLOCATE worksRequiredCursor

END TRY
BEGIN CATCH 
	IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   
		SET @isSaved = 0        
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
			);
END CATCH;

IF @@TRANCOUNT > 0
	BEGIN  
		COMMIT TRANSACTION;  
		SET @isSaved = 1
	END

SET @journalIdOut = @journalId

END
GO
/****** Object:  StoredProcedure [dbo].[V_PaintPackAssignWorkToContractor]    Script Date: 09/07/2015 15:17:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ali Raza
-- Create date: 31/08/2014
-- Description:	Assign Work to Contractor.
-- History:          31/08/2014 AR : Save Purchase order for Paint Pack Assign Work to Contractor
-- =============================================
CREATE PROCEDURE [dbo].[V_PaintPackAssignWorkToContractor] 
	-- Add the parameters for the stored procedure here
	@pdrContractorId INT,
	@paintPackId INT,
	@contractorId INT,
	@contactId INT,
	@userId int,
	@Estimate SMALLMONEY,
	@EstimateRef NVARCHAR(200),	
	@POStatus INT,
	@ContractorWorksDetail AS PDR_AssingToContractorWorksRequired READONLY,
	@isSaved BIT = 0 OUTPUT,
	@journalIdOut INT OUTPUT
	
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;


/* Working of this stored Procedure
** 1- Get Status Id of "Assigned To Contractor" from Planned_Status. In case a Status of "Assigned To Contractor" 
**    is not present add a new one and get its Id.

** 2- Insert a new record in F_PURCHASEORDER and get the identity value as Order Id.
**
** 3- Insert a new record in PDR_CONTRACTOR_WORK using the given input data and OrderId
**    and get Identity Value as PDRContractorId.
**
** Loop (Insert Purchase Items and Works Required Items.
**
**   4- Insert a Purchase Item in F_PURCHASEITEM from given an constant data
**      and get Identity Value PURCHASEORDERITEMID
**
**   5- Insert a new work required from given data and also insert PURCHASEORDERITEMID
**
** End Loop
*/


BEGIN TRANSACTION
BEGIN TRY

-- To save same time stamp in all records 
DECLARE @CurrentDateTime AS datetime2 = GETDATE()

--================================================================================
--Get Status Id for Status Title "Assigned To Contractor"
--In case (for first time) it does not exists Insert it and get Status Id.

-- Variables to get Status Id and Status History Id
DECLARE @newStatusId int = NULL

-- =====================================================
-- get status history id of "Assigned To Contractor"
-- =====================================================

SELECT	@newStatusId = STATUSID
FROM	PDR_STATUS
WHERE	PDR_STATUS.TITLE = 'Assigned To Contractor'



IF (@PdrContractorId = -1)
BEGIN

-- =====================================================
-- Insert new Purchase Order
-- =====================================================

DECLARE @Active bit = 1
, @POTYPE int = (SELECT	POTYPEID
				FROM F_POTYPE
				WHERE POTYPENAME = 'Paint Packs') -- 2 = 'Repair'

-- To get Identity Value of Purchase Order.
, @purchaseOrderId int

INSERT INTO F_PURCHASEORDER (PONAME, PODATE, PONOTES, USERID, SUPPLIERID, ACTIVE,
							POTYPE, POSTATUS, GASSERVICINGYESNO)
VALUES (UPPER('Paint Pack Work Order'), @CurrentDateTime
, 'This purchase order was created for Paint Pack from the new Paint Pack Work process.'
, @userId, @ContractorId, @ACTIVE, @POTYPE, @POSTATUS, 0)

SET @purchaseOrderId = SCOPE_IDENTITY()

-- =====================================================
-- Insert new PDR_CONTRACTOR_WORK
-- =====================================================

	INSERT INTO [PDR_CONTRACTOR_WORK]
           ([ContractorId],[ContactId],[AssignedDate],[AssignedBy],[Estimate],[EstimateRef],[PurchaseOrderId],PaintPackId)
	VALUES
           (@ContractorId,@ContactId,@CurrentDateTime,@userId,@Estimate,@EstimateRef,@PurchaseOrderId,@paintPackId)
    SET @PdrContractorId = SCOPE_IDENTITY()    
           
END
ELSE
BEGIN
	UPDATE [PDR_CONTRACTOR_WORK]
	SET [ContractorId] = @ContractorId
	,[ContactId] = @ContactId
	,[AssignedDate] = @CurrentDateTime
	,[AssignedBy] = @userId
	,[Estimate] = @Estimate
	,[EstimateRef] = @EstimateRef
	
	WHERE PDRContractorId = @PdrContractorId
END


-- =====================================================
-- Declare a cursor to enter works requied,
--  loop through record and instert in table
-- =====================================================

DECLARE worksRequiredCursor CURSOR FOR SELECT
	*
FROM @ContractorWorksDetail
OPEN worksRequiredCursor

-- Declare Variable to use with cursor
DECLARE
@WorkDetailId int ,
@ServiceRequired nvarchar(4000),
@NetCost smallmoney,
@VatType int,
@VAT smallmoney,
@GROSS smallmoney,
@PIStatus int,
@ExpenditureId int,
@CostCenterId int,
@BudgetHeadId int

-- Variable used within loop
DECLARE @PurchaseItemTITLE nvarchar(20) = 'Paint Pack Work' -- Title for Purchase Items, specially to inset in F_PurchaseItem

		-- =====================================================
		-- Loop (Start) through records and insert works required
		-- =====================================================		
		-- Fetch record for First loop iteration.
		FETCH NEXT FROM worksRequiredCursor INTO @WorkDetailId,@ServiceRequired, @NetCost, @VatType, @VAT,
		@GROSS, @PIStatus, @ExpenditureId,@CostCenterId,@BudgetHeadId
		WHILE @@FETCH_STATUS = 0 BEGIN
	
		IF (@WorkDetailId = -1)
		BEGIN
			-- =====================================================
			--Insert Values in F_PURCHASEITEM for each work required and get is identity value.
			-- =====================================================

			INSERT INTO F_PURCHASEITEM (ORDERID, EXPENDITUREID, ITEMNAME, ITEMDESC, PIDATE,
									NETCOST, VATTYPE, VAT, GROSSCOST, USERID, ACTIVE, PITYPE, PISTATUS)
			VALUES (@PurchaseOrderId, @ExpenditureId, @PurchaseItemTITLE, @ServiceRequired, 
				@CurrentDateTime,  @NetCost, @VatType, @VAT, @GROSS, @userId, @ACTIVE, @POTYPE, @POSTATUS)

			DECLARE @ORDERITEMID int = SCOPE_IDENTITY()

			-- =====================================================
			-- Insert values in PDR_CONTRACTOR_WORK_DETAIL for each work required
			-- =====================================================

			INSERT INTO [PDR_CONTRACTOR_WORK_DETAIL]
           ([PDRContractorId],[ServiceRequired],[NetCost],[VatId],[Vat],[Gross],[ExpenditureId],[CostCenterId],[BudgetHeadId],[PURCHASEORDERITEMID])
			VALUES
           (@pdrContractorId,@ServiceRequired,@NetCost,@VatType,@VAT,@GROSS,@ExpenditureId,@CostCenterId,@BudgetHeadId,@ORDERITEMID)
           
		END

-- Fetch record for next loop iteration.
FETCH NEXT FROM worksRequiredCursor INTO @WorkDetailId, @ServiceRequired, @NetCost, @VatType, @VAT,
				@GROSS, @PIStatus, @ExpenditureId,@CostCenterId,@BudgetHeadId
END

-- =====================================================
-- Loop (End) through records and insert works required
-- =====================================================

CLOSE worksRequiredCursor
DEALLOCATE worksRequiredCursor

END TRY
BEGIN CATCH 
	IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   
		SET @isSaved = 0        
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
			);
END CATCH;

IF @@TRANCOUNT > 0
	BEGIN  
		COMMIT TRANSACTION;  
		SET @isSaved = 1
	END

SET @journalIdOut = (Select InspectionJournalId from V_PaintPack where PaintPackId=@paintPackId)

END
GO
/****** Object:  StoredProcedure [dbo].[V_GetVoidPDFDocumentInfo]    Script Date: 09/07/2015 15:17:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:           Salman Nazir
-- Create date:      03/07/2015
-- Description:      Get Void PDF Document Info
-- History:          
-- =============================================
CREATE PROCEDURE [dbo].[V_GetVoidPDFDocumentInfo]
@britishGasId int
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
--SET NOCOUNT ON;

-- Insert statements for procedure here
SELECT
	P.ADDRESS1 + ISNULL(' ' + P.ADDRESS2, '') AS [Address],
	P.POSTCODE,
	P.TOWNCITY,
	P.COUNTY,
	C.FIRSTNAME + ' ' + C.LASTNAME AS Customer,
	B.TenantFwAddress1 + ISNULL(' ' + B.TenantFwAddress2, '') + ISNULL(' ' + B.TenantFwCity, '') AS TenantAddress,
	CONVERT(NVARCHAR(50), B.DateOccupancyCease, 103)as DateOccupancyCease,
	B.GasMeterReading,
	CONVERT(NVARCHAR(50), B.GasMeterReadingDate, 103)as GasMeterReadingDate,
	B.ElectricMeterReading,
	CONVERT(NVARCHAR(50), B.ElectricMeterReadingDate, 103)as ElectricMeterReadingDate,
	B.GasDebtAmount,
	CONVERT(NVARCHAR(50), B.GasDebtAmountDate, 103)as GasDebtAmountDate,
	B.ElectricDebtAmount,
	CONVERT(NVARCHAR(50), B.ElectricDebtAmountDate, 103)as ElectricDebtAmountDate,
	B.NewTenantName,
	B.NewTenantPreviousAddress1 + ISNULL(' ' + B.NewTenantPreviousAddress2, '') + ISNULL(' ' + B.NewTenantPreviousTownCity, '') AS PreviousAddress,
	CONVERT(NVARCHAR(50), B.NewTenantOccupancyDate, 103)as   DateOccupancy,
	B.NewGasMeterReading AS NewGasMeterReading,
	B.NewElectricMeterReading,
	CONVERT(VARCHAR, 'DOB:'+B.NewTenantDateOfBirth) + ISNULL('<br/> Tel: ' + B.NewTenantTel, '') + ISNULL('<br/> Mobile: ' + B.NewTenantMobile, '') AS OtherInfo,
	PV.ValueDetail AS GasMeterType,
	PVE.ValueDetail AS ElectricMeterType,
	B.isNotificationSent,
	B.StageId,
	B.BritishGasEmail
FROM V_BritishGasVoidNotification B
INNER JOIN P__PROPERTY P
	ON P.PropertyId = B.PropertyId
INNER JOIN C__Customer C
	ON C.CustomerId = B.CustomerId
INNER JOIN C_Tenancy T
	ON T.TenancyId = B.TenancyId
LEFT JOIN PA_PARAMETER_VALUE PV
	ON B.GasMeterTypeId = PV.ValueID
LEFT JOIN PA_PARAMETER_VALUE PVE
	ON B.ElectricMeterTypeId = PVE.ValueID

WHERE B.BritishGasVoidNotificationId = @britishGasId
END
GO
/****** Object:  StoredProcedure [dbo].[V_GetCountNoEntry]    Script Date: 09/07/2015 15:17:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:           Ali Raza
-- Create date:      28/05/2015
-- Description:		 NoEntry Counts on Void Dashboard
-- History:          28/05/2015 AR : NoEntry Counts on Void Dashboard
-- =============================================
CREATE PROCEDURE [dbo].[V_GetCountNoEntry]
@getOnlyCount bit=0
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	Declare @statusId int	
	Select @statusId = STATUSID from PDR_STATUS Where TITLE = 'No Entry'
IF(@getOnlyCount=1)
	BEGIN
	Select COUNT(A.APPOINTMENTID) as total from 
			PDR_APPOINTMENT_HISTORY A
			INNER JOIN PDR_JOURNAL J on J.JOURNALID = A.JOURNALID
			INNER JOIN PDR_MSAT MS on MS.MSATId = J.MSATID
			INNER JOIN PDR_STATUS S on S.STATUSID = J.STATUSID
			
			Where S.STATUSID = @statusId
			AND MS.MSATTypeId IN (Select MSATTypeId from PDR_MSATTYPE where MSATTypeName Like '%Void%')
	END
ELSE
	BEGIN
			select P.PROPERTYID 	,ISNULL(P.HouseNumber, '')	+ ISNULL(' '+P.ADDRESS1, '') 	+ ISNULL(', '+P.ADDRESS2, '') AS Address
			,	P.TOWNCITY,P.COUNTY,	ISNULL(P.POSTCODE, '') AS Postcode 
			from 
				PDR_APPOINTMENT_HISTORY A
				INNER JOIN PDR_JOURNAL J on J.JOURNALID = A.JOURNALID
				INNER JOIN PDR_MSAT MS on MS.MSATId = J.MSATID
				INNER JOIN PDR_STATUS S on S.STATUSID = J.STATUSID
				INNER JOIN P__PROPERTY P ON MS.PropertyId=P.PROPERTYID 
				Where S.STATUSID = @statusId
				AND MS.MSATTypeId IN (Select MSATTypeId from PDR_MSATTYPE where MSATTypeName Like '%Void%')
	END	
	
	
	
	
END
GO
/****** Object:  StoredProcedure [dbo].[V_GetAppointmentDetailByJournalId]    Script Date: 09/07/2015 15:17:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:           Ali Raza
-- Create date:      06/07/2015
-- Description:      Get appointmentInfo by Journal Id
-- History:          06/07/2015 Void: Get appointment Info to send email cancel appointment detail to last Operative.
--                   
-- =============================================

CREATE PROCEDURE [dbo].[V_GetAppointmentDetailByJournalId](
	@journalId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	 		
	SELECT 
		'JSV' + CONVERT(VARCHAR, RIGHT('000000' + CONVERT(VARCHAR, ISNULL(
	CASE
		WHEN V.WorksJournalId IS NULL THEN J.JOURNALID
		ELSE V.WorksJournalId
	END, -1)), 4)) AS Ref,
		ISNULL(P.HouseNumber, '') + ISNULL(' ' + P.ADDRESS1, '') + ISNULL(', ' + P.ADDRESS2, '') AS Address,
		ISNULL(P.TOWNCITY,'') as TOWNCITY,
		ISNULL(P.COUNTY,'') as COUNTY,
		ISNULL(P.POSTCODE,'') as POSTCODE,		
		ISNULL(P_SCHEME.SCHEMENAME,'') as Scheme,		
		ISNULL(P_BLOCK.BLOCKNAME,'') as Block,CONVERT(NVARCHAR(50), PDR_MSAT.TerminationDate, 103) AS Termination,
		CONVERT(nvarchar(50),DATEADD(day,7,Case When PDR_MSAT.ReletDate IS NULL Then PDR_MSAT.TerminationDate ELSE PDR_MSAT.ReletDate END), 103)
		 as Relet,		
		E.EMPLOYEEID as EmployeeId, E.FIRSTNAME +' '+ E.LASTNAME as OperativeName , C.WORKEMAIL as Email,CONVERT(NVARCHAR(50), 
		A.APPOINTMENTSTARTDATE, 103)as StartDate,A.APPOINTMENTSTARTTIME as StartTime,A.APPOINTMENTENDTIME AS EndTime
	FROM
		PDR_JOURNAL J
		INNER JOIN PDR_MSAT ON J.MSATID =  PDR_MSAT.MSATId
		INNER JOIN PDR_APPOINTMENTS A ON J.JOURNALID=A.JOURNALID
		INNER JOIN P__PROPERTY P ON PDR_MSAT.PropertyId = P.PROPERTYID
		INNER JOIN E__EMPLOYEE E ON A.ASSIGNEDTO=E.EMPLOYEEID    
		LEFT JOIN E_CONTACT C ON E.EMPLOYEEID = C.EMPLOYEEID 
		LEFT JOIN P_SCHEME ON P.SCHEMEID = P_SCHEME.SCHEMEID 
		LEFT JOIN P_BLOCK ON P.BLOCKID = P_BLOCK.BLOCKID
		LEFT JOIN V_RequiredWorks V ON J.JOURNALID = V.WorksJournalId
	WHERE J.JOURNALID=@journalId	
		
					
END
GO
/****** Object:  StoredProcedure [dbo].[V_GetPostVoidInspectionsToBeArranged]    Script Date: 09/07/2015 15:17:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:           Ali Raza
-- Create date:      28/05/2015
-- Description:		 Get Post Void Inspections To Be Arranged List 
-- History:          28/05/2015 AR : Get Post Void Inspections To Be Arranged List 
-- =============================================
--  DECLARE	@totalCount int
--EXEC	 [dbo].[V_GetPostVoidInspectionsToBeArranged]
--		@searchText = NULL,
--		@pageSize = 100,
--		@pageNumber = 1,
--		@totalCount = @totalCount OUTPUT
--SELECT	@totalCount as N'@totalCount'
-- =============================================
CREATE PROCEDURE [dbo].[V_GetPostVoidInspectionsToBeArranged]
-- Add the parameters for the stored procedure here
		@searchText VARCHAR(200)='',
	--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'JournalId', 
		@sortOrder varchar (5) = 'DESC',
		@getOnlyCount bit=0,
		@totalCount int = 0 output	
AS
BEGIN
	DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(2000),
        
        --variables for paging
        @offset int,
		@limit int,
		@MSATTypeId int,
		@ToBeArrangedStatusId int,
		@propertyStatus int,
		@propertySubStatus int
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		
		SELECT 	@MSATTypeId = MSATTypeId  FROM PDR_MSATType WHERE MSATTypeName = 'Post Void Inspection'
		SELECT  @ToBeArrangedStatusId = PDR_STATUS.STATUSID FROM PDR_STATUS WHERE TITLE = 'To be Arranged'
		SELECT @propertyStatus=STATUSID from P_STATUS WHERE DESCRIPTION='Let'
		SELECT @propertySubStatus=SUBSTATUSID from P_SUBSTATUS WHERE DESCRIPTION='Pending Termination'

		
		--=====================Search Criteria===============================
		SET @searchCriteria = ' P__PROPERTY.STATUS= '+convert(varchar(10),@propertyStatus)+' AND P__PROPERTY.SUBSTATUS = '+convert(varchar(10),@propertySubStatus)
		SET @searchCriteria = @searchCriteria + CHAR(10) +'AND PDR_MSAT.MSATTypeId= '+convert(varchar(10),@MSATTypeId)+' AND PDR_JOURNAL.STATUSID= '+convert(varchar(10),@ToBeArrangedStatusId)+''
		
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND (ISNULL(P__PROPERTY.HouseNumber, '''')	+ ISNULL('' ''+P__PROPERTY.ADDRESS1, '''') 	+ ISNULL('', ''+P__PROPERTY.ADDRESS2, '''') LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR CONVERT(VARCHAR,Case When C__CUSTOMER.TITLE=6 then '''' Else  ISNULL(G_TITLE.[DESCRIPTION],'''')END)+ CONVERT(VARCHAR, ISNULL(C__CUSTOMER.FIRSTNAME,''''))+ CONVERT(VARCHAR, ISNULL(C__CUSTOMER.LASTNAME,'''')) LIKE ''%' + @searchText + '%'')'
		END	
		
		--=======================Select Clause=============================================
		SET @SelectClause = 'Select top ('+convert(varchar(10),@limit)+')
							 ''JSV''+CONVERT(VARCHAR, RIGHT(''000000''+ CONVERT(VARCHAR,ISNULL(PDR_JOURNAL.JOURNALID,-1)),4)) AS Ref
		,ISNULL(P__PROPERTY.HouseNumber, '''')	+ ISNULL('' ''+P__PROPERTY.ADDRESS1, '''') 	+ ISNULL('', ''+P__PROPERTY.ADDRESS2, '''') AS Address
		,	P__PROPERTY.TOWNCITY,P__PROPERTY.COUNTY,	ISNULL(P__PROPERTY.POSTCODE, '''') AS Postcode,PDR_JOURNAL.JOURNALID as JournalId 
		, CONVERT(VARCHAR,Case When C__CUSTOMER.TITLE=6 then '''' Else  ISNULL(G_TITLE.[DESCRIPTION],'''')END)+ CONVERT(VARCHAR, ISNULL(+'' ''+C__CUSTOMER.FIRSTNAME,''''))+'' ''+ CONVERT(VARCHAR, ISNULL(C__CUSTOMER.LASTNAME,'''')) AS Tenant 
		,CONVERT(nvarchar(50),PDR_MSAT.TerminationDate, 103) as Termination	
		,CONVERT(nvarchar(50),PDR_MSAT.ReletDate, 103) as Relet		
		,P__PROPERTY.PropertyId as PropertyId
		,C__CUSTOMER.CUSTOMERID  as CustomerId
		,C_TENANCY.TENANCYID as TenancyId'
		
		
		--============================From Clause============================================
		SET @fromClause = CHAR(10) +'FROM	PDR_JOURNAL
		INNER JOIN	PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId
		INNER JOIN	PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId		
		LEFT JOIN	P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID	
		INNER JOIN	PDR_STATUS ON PDR_JOURNAL.STATUSID = PDR_STATUS.STATUSID
		INNER JOIN C_TENANCY ON PDR_MSAT.TenancyId=	C_TENANCY.TENANCYID
		INNER JOIN C__CUSTOMER ON PDR_MSAT.CustomerId = C__CUSTOMER.CUSTOMERID
		INNER JOIN C_CUSTOMERTENANCY on C__CUSTOMER.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID and C_TENANCY.TENANCYID=C_CUSTOMERTENANCY.TENANCYID
		LEFT JOIN G_TITLE ON C__CUSTOMER.TITLE=G_TITLE.TITLEID
		'
							
		--============================Order Clause==========================================
		IF(@sortColumn = 'Ref')
		BEGIN
			SET @sortColumn = CHAR(10)+ ' PDR_JOURNAL.JOURNALID' 	
			
		END
		
		IF(@sortColumn = 'Address')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Address' 	
			
		END
		IF(@sortColumn = 'Tenant')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Tenant' 	
			
		END
		
		IF(@sortColumn = 'Termination')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Termination' 	
			
		END
		
		IF(@sortColumn = 'Relet')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Relet' 	
			
		END
				
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		--===============================Main Query ====================================
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
		IF(@getOnlyCount=0)
		BEGIN
			print(@finalQuery)
			EXEC (@finalQuery)
		END
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
END
GO
/****** Object:  StoredProcedure [dbo].[V_GetPostVoidInspectionArranged]    Script Date: 09/07/2015 15:17:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:           Ali Raza
-- Create date:      28/05/2015
-- Description:		 Get Post Void Inspections  Arranged List 
-- History:          28/05/2015 AR : Get Post Void Inspections  Arranged List 
-- =============================================
--  DECLARE	@totalCount int
--EXEC	 [dbo].[V_GetPostVoidInspectionArranged]
--		@searchText = NULL,
--		@pageSize = 100,
--		@pageNumber = 1,
--		@totalCount = @totalCount OUTPUT
--SELECT	@totalCount as N'@totalCount'
-- =============================================
CREATE PROCEDURE [dbo].[V_GetPostVoidInspectionArranged]
-- Add the parameters for the stored procedure here
		@searchText VARCHAR(200)='',
	--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'JournalId', 
		@sortOrder varchar (5) = 'DESC',
		@getOnlyCount bit=0,
		@totalCount int = 0 output	
AS
BEGIN
	DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(2000),
        
        --variables for paging
        @offset int,
		@limit int,
		@MSATTypeId int,
		@ToBeArrangedStatusId int
		
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		
		SELECT 	@MSATTypeId = MSATTypeId  FROM PDR_MSATType WHERE MSATTypeName = 'Post Void Inspection'
		SELECT  @ToBeArrangedStatusId = PDR_STATUS.STATUSID FROM PDR_STATUS WHERE TITLE = 'Arranged'
		
		--=====================Search Criteria===============================
		SET @searchCriteria = 'PDR_MSAT.MSATTypeId= '+convert(varchar(10),@MSATTypeId)+' AND PDR_JOURNAL.STATUSID= '+convert(varchar(10),@ToBeArrangedStatusId)+''
		
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND (ISNULL(P__PROPERTY.HouseNumber, '''')	+ ISNULL('' ''+P__PROPERTY.ADDRESS1, '''') 	+ ISNULL('', ''+P__PROPERTY.ADDRESS2, '''') LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR CONVERT(VARCHAR,Case When C__CUSTOMER.TITLE=6 then '''' Else  ISNULL(G_TITLE.[DESCRIPTION],'''')END)+ CONVERT(VARCHAR, ISNULL(C__CUSTOMER.FIRSTNAME,''''))+ CONVERT(VARCHAR, ISNULL(C__CUSTOMER.LASTNAME,'''')) LIKE ''%' + @searchText + '%'')'
		END	
		
		--=======================Select Clause=============================================
		SET @SelectClause = 'Select top ('+convert(varchar(10),@limit)+')
							 ''JSV''+CONVERT(VARCHAR, RIGHT(''000000''+ CONVERT(VARCHAR,ISNULL(PDR_JOURNAL.JOURNALID,-1)),4)) AS Ref
		,ISNULL(P__PROPERTY.HouseNumber, '''')	+ ISNULL('' ''+P__PROPERTY.ADDRESS1, '''') 	+ ISNULL('', ''+P__PROPERTY.ADDRESS2, '''') AS Address
	    ,P__PROPERTY.TOWNCITY,P__PROPERTY.COUNTY,	ISNULL(P__PROPERTY.POSTCODE, '''') AS Postcode,PDR_JOURNAL.JOURNALID as JournalId
			, CONVERT(VARCHAR,Case When C__CUSTOMER.TITLE=6 then '''' Else  ISNULL(G_TITLE.[DESCRIPTION],'''')END)+ CONVERT(VARCHAR, ISNULL(C__CUSTOMER.FIRSTNAME,''''))+ CONVERT(VARCHAR, ISNULL(C__CUSTOMER.LASTNAME,'''')) AS Tenant 
		,CONVERT(nvarchar(50),PDR_MSAT.TerminationDate, 103) as Termination	
		,CONVERT(nvarchar(50),PDR_MSAT.ReletDate, 103) as Relet	
		,LEFT(E__EMPLOYEE.Firstname, 1)+''''+LEFT(E__EMPLOYEE.LASTNAME, 1) as Surveyor
		,CONVERT(nvarchar(50),PDR_APPOINTMENTS.APPOINTMENTSTARTDATE, 103)+''</br> ''+CONVERT(VARCHAR(5), PDR_APPOINTMENTS.APPOINTMENTSTARTTIME, 108) 
		+''-''+CONVERT(VARCHAR(5), PDR_APPOINTMENTS.APPOINTMENTENDTIME, 108) as Appointment,PDR_MSAT.TenancyId,PDR_APPOINTMENTS.APPOINTMENTNOTES	
			'
		
		
		--============================From Clause============================================
		SET @fromClause = CHAR(10) +'FROM	PDR_JOURNAL
		INNER JOIN PDR_APPOINTMENTS ON PDR_JOURNAL.JOURNALID=PDR_APPOINTMENTS.JOURNALID
		INNER JOIN E__EMPLOYEE ON PDR_APPOINTMENTS.ASSIGNEDTO = E__EMPLOYEE.EMPLOYEEID
		INNER JOIN	PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId
		INNER JOIN	PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId		
		LEFT JOIN	P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID	
		INNER JOIN	PDR_STATUS ON PDR_JOURNAL.STATUSID = PDR_STATUS.STATUSID
		INNER JOIN C_TENANCY ON PDR_MSAT.TenancyId=	C_TENANCY.TENANCYID
		INNER JOIN C__CUSTOMER ON PDR_MSAT.CustomerId = C__CUSTOMER.CUSTOMERID
		INNER JOIN C_CUSTOMERTENANCY on C__CUSTOMER.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID and C_TENANCY.TENANCYID=C_CUSTOMERTENANCY.TENANCYID
		LEFT JOIN G_TITLE ON C__CUSTOMER.TITLE=G_TITLE.TITLEID
		'
							
		--============================Order Clause==========================================
		IF(@sortColumn = 'Ref')
		BEGIN
			SET @sortColumn = CHAR(10)+ ' PDR_JOURNAL.JOURNALID' 	
			
		END
		
		IF(@sortColumn = 'Address')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Address' 	
			
		END
		IF(@sortColumn = 'Tenant')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Tenant' 	
			
		END
		
		IF(@sortColumn = 'Termination')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Termination' 	
			
		END
		
		IF(@sortColumn = 'Relet')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Relet' 	
			
		END
				
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		--===============================Main Query ====================================
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
		IF(@getOnlyCount=0)
		BEGIN
			print(@finalQuery)
			EXEC (@finalQuery)
		END
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
END
GO
/****** Object:  StoredProcedure [dbo].[V_GetPendingTermination]    Script Date: 09/07/2015 15:17:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:           Ali Raza
-- Create date:      28/05/2015
-- Description:		 Get Pending Termination for  Void Dashboard 
-- History:          28/05/2015 AR : Get Pending Termination for  Void Dashboard 
-- =============================================
--  DECLARE	@totalCount int
--EXEC	 [dbo].[V_GetPendingTermination]
--		@searchText = NULL,
--		@checksRequired=1
--		@pageSize = 100,
--		@pageNumber = 1,
--		@totalCount = @totalCount OUTPUT
--SELECT	@totalCount as N'@totalCount'
-- =============================================
CREATE PROCEDURE [dbo].[V_GetPendingTermination]
-- Add the parameters for the stored procedure here
		@searchText VARCHAR(200)='',
		
	--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'Ref', 
		@sortOrder varchar (5) = 'DESC',
		@getOnlyCount bit=0,
		@totalCount int = 0 output	
AS
BEGIN
	DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(5000),
        
        --variables for paging
        @offset int,
		@limit int,
		@checksRequiredType varchar(200),
		@MSATTypeId int,
		@ArrangedStatusId int,
		@ToBeArrangedStatusId INT
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		
		SELECT
			@ToBeArrangedStatusId = PDR_STATUS.STATUSID
		FROM PDR_STATUS
		WHERE TITLE = 'To be Arranged'

		--=====================Search Criteria===============================
		SET @searchCriteria = ' 1=1 AND P.SUBSTATUS=22 AND  ITEMNATUREID = 27 AND CURRENTITEMSTATUSID = 13  '
		
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND (ISNULL(P.HouseNumber, '''')	+ ISNULL('' ''+P.ADDRESS1, '''') 	+ ISNULL('', ''+P.ADDRESS2, '''') LIKE ''%' + @searchText + '%'')'
		END	
		
		
		--=======================Select Clause=============================================
		SET @SelectClause = 'Select DISTINCT top ('+convert(varchar(10),@limit)+')
							 J.CUSTOMERID
	,ISNULL(P.HouseNumber, '''')	+ ISNULL('' ''+P.ADDRESS1, '''') 	+ ISNULL('', ''+P.ADDRESS2, '''') AS Address
		,	P.TOWNCITY,P.COUNTY,ISNULL(P.POSTCODE, '''') AS Postcode, Convert(Varchar(50), T.TERMINATIONDATE,103) as Termination,
		Case When M.ReletDate IS NULL Then CONVERT(nvarchar(50),DATEADD(day,7,T.TERMINATIONDATE), 103)
		ELSE Convert(Varchar(50), M.ReletDate,103)END as Relet,P.PROPERTYID As Ref,J.TENANCYID,ISNULL(ST.DESCRIPTION,''-'') AS Status
,ISNULL(SUB.DESCRIPTION,''-'') as SubStatus,ISNULL(S.SCHEMENAME,''-'') as Scheme,ISNULL(B.BLOCKNAME,''-'') AS Block'
			
		
		
		--============================From Clause============================================
		SET @fromClause = CHAR(10) +'from  P__PROPERTY P
	INNER JOIN C_JOURNAL J ON P.PROPERTYID=J.PROPERTYID	
	INNER JOIN C_TERMINATION T ON J.JOURNALID=T.JOURNALID
	INNER JOIN C__CUSTOMER C ON J.CUSTOMERID = C.CUSTOMERID
	INNER JOIN C_TENANCY CT ON J.TENANCYID=CT.TENANCYID
	INNER JOIN C_CUSTOMERTENANCY on C.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID and CT.TENANCYID=C_CUSTOMERTENANCY.TENANCYID
	LEFT JOIN P_BLOCK B ON P.BLOCKID=B.BLOCKID
	LEFT JOIN P_SCHEME S ON P.SCHEMEID=S.SCHEMEID
	INNER JOIN P_STATUS ST ON P.STATUS = ST.STATUSID
	LEFT JOIN P_SUBSTATUS SUB ON P.SUBSTATUS = SUB.SUBSTATUSID
	INNER JOIN PDR_MSAT M ON P.PropertyId =M.PropertyId AND C.CUSTOMERID= M.CustomerId AND CT.TENANCYID=M.TenancyId
	INNER JOIN PDR_JOURNAL VJ ON M.MSATID= VJ.MSATID AND VJ.STATUSID= '+convert(varchar(10),@ToBeArrangedStatusId)
	
		
							
		--============================Order Clause==========================================
		IF(@sortColumn = 'Ref')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Ref' 	
			
		END
		
		IF(@sortColumn = 'Address')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Address' 	
			
		END
		
		
		IF(@sortColumn = 'Termination')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Termination' 	
			
		END
		
		IF(@sortColumn = 'Relet')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Relet' 	
			
		END
				
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		--===============================Main Query ====================================
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
		IF(@getOnlyCount=0)
		BEGIN
			print(@finalQuery)
			EXEC (@finalQuery)
		END
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(DISTINCT P.PropertyID) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
END
GO
/****** Object:  StoredProcedure [dbo].[V_GetPaintPacksList]    Script Date: 09/07/2015 15:17:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:           Ali Raza
-- Create date:      28/05/2015
-- Description:		 Get Paint Packs List
-- History:          28/05/2015 AR : Get Paint Packs List
-- =============================================

--  DECLARE	@totalCount int
--EXEC	 [dbo].[V_GetPaintPacksList]

--		@pageSize = 100,
--		@pageNumber = 1,
--		@totalCount = @totalCount OUTPUT
--SELECT	@totalCount as N'@totalCount'
-- =============================================
CREATE PROCEDURE [dbo].[V_GetPaintPacksList]

	--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'JournalId', 
		@sortOrder varchar (5) = 'DESC',
		@totalCount int = 0 output	
AS
BEGIN
	DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(2000),
        
        --variables for paging
        @offset int,
		@limit int,
		@MSATTypeId int,
		@ToBeArrangedStatusId int
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		
		SELECT 	@MSATTypeId = MSATTypeId  FROM PDR_MSATType WHERE MSATTypeName = 'Void Inspection'
		
		--=====================Search Criteria===============================
		SET @searchCriteria = ' PDR_MSAT.MSATTypeId= '+convert(varchar(10),@MSATTypeId)+' '
		
		
		
		--=======================Select Clause=============================================
		SET @SelectClause = 'Select DISTINCT top ('+convert(varchar(10),@limit)+')
							 J.JournalId as JournalId 
		,ISNULL(P__PROPERTY.HouseNumber, '''')	+ ISNULL('' ''+P__PROPERTY.ADDRESS1, '''') 	+ ISNULL('', ''+P__PROPERTY.ADDRESS2, '''') AS Address
		
		,Case When C__CUSTOMER.CUSTOMERID IS NULL Then ''N/A''
		 Else CONVERT(VARCHAR,Case When C__CUSTOMER.TITLE=6 then '''' Else  ISNULL(G_TITLE.[DESCRIPTION],'''')END)+ CONVERT(VARCHAR, ISNULL(+'' ''+C__CUSTOMER.FIRSTNAME,''''))+'' ''+ CONVERT(VARCHAR, ISNULL(C__CUSTOMER.LASTNAME,''''))END AS Tenant 
		,CONVERT(nvarchar(50),PDR_MSAT.TerminationDate, 103) as Termination	
		,CONVERT(nvarchar(50),PDR_MSAT.ReletDate, 103) as Relet		
		,P__PROPERTY.PropertyId as PropertyId
		,ISNULL(C_ADDRESS.TEL,''N/A'') as Telephone
		,C_TENANCY.TENANCYID as TenancyId,PS.Title as [Status]
		,PPD.PaintPacks  as PaintPacks,PPD.PaintPackId'
		
		
		--============================From Clause============================================
		SET @fromClause = CHAR(10) +'FROM PDR_JOURNAL J
		INNER JOIN V_PaintPack P ON J.JOURNALID=P.InspectionJournalId
		INNER JOIN V_PaintStatus PS ON P.StatusId=PS.StatusId AND (PS.Title<> ''Complete'' OR P.StatusId IS NULL)
		CROSS APPLY ( Select Count(PaintPackId) as PaintPacks,PaintPackId  from V_PaintPackDetails Where V_PaintPackDetails.PaintPackId=P.PaintPackId
		GROUP BY PaintPackId) AS PPD
		INNER JOIN V_PaintPackDetails ON PPD.PaintPackId = V_PaintPackDetails.PaintPackId 	 		
		INNER JOIN	PDR_MSAT ON J.MSATID = PDR_MSAT.MSATId
		INNER JOIN	PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId		
		INNER JOIN	P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID			
		LEFT JOIN C_TENANCY ON P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID AND C_TENANCY.ENDDATE IS NULL
		LEFT JOIN C_CUSTOMERTENANCY ON C_TENANCY.TENANCYID = C_CUSTOMERTENANCY.TENANCYID
		LEFT JOIN C__CUSTOMER ON C_CUSTOMERTENANCY.CUSTOMERID = C__CUSTOMER.CUSTOMERID
		LEFT JOIN C_ADDRESS ON C__CUSTOMER.CUSTOMERID = C_ADDRESS.CUSTOMERID AND ISDEFAULT = 1
		LEFT JOIN G_TITLE ON C__CUSTOMER.TITLE = G_TITLE.TITLEID
		'
							
		--============================Order Clause==========================================
		IF(@sortColumn = 'Ref')
		BEGIN
			SET @sortColumn = CHAR(10)+ ' PDR_JOURNAL.JOURNALID' 	
			
		END
		
		IF(@sortColumn = 'Address')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Address' 	
			
		END
		IF(@sortColumn = 'Tenant')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Tenant' 	
			
		END
		
		IF(@sortColumn = 'Telephone')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Telephone' 	
			
		END
		
		
				
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		--===============================Main Query ====================================
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
		print(@finalQuery)
		EXEC (@finalQuery)
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(DISTINCT JournalId) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
END
GO
/****** Object:  StoredProcedure [dbo].[V_GetVoidInspectionsToBeArranged]    Script Date: 09/07/2015 15:17:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:           Ali Raza
-- Create date:      28/05/2015
-- Description:		 Get Void Inspections To Be Arranged List
-- History:          28/05/2015 AR : Get Void Inspections To Be Arranged List 
--					 25/05/2015 NoorM:Added property id, customerid, tenancyid in select statement
-- =============================================
--  DECLARE	@totalCount int
--EXEC	 [dbo].[V_GetFirstInspectionsToBeArranged]
--		@searchText = NULL,
--		@pageSize = 100,
--		@pageNumber = 1,
--		@totalCount = @totalCount OUTPUT
--SELECT	@totalCount as N'@totalCount'
-- =============================================
CREATE PROCEDURE [dbo].[V_GetVoidInspectionsToBeArranged]
-- Add the parameters for the stored procedure here
		@searchText VARCHAR(200)='',
	--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'JournalId', 
		@sortOrder varchar (5) = 'DESC',
		@getOnlyCount bit=0,
		@totalCount int = 0 output	
AS
BEGIN
	DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(2000),
        
        --variables for paging
        @offset int,
		@limit int,
		@MSATTypeId int,
		@ToBeArrangedStatusId int
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		
		SELECT 	@MSATTypeId = MSATTypeId  FROM PDR_MSATType WHERE MSATTypeName = 'Void Inspection'
		SELECT  @ToBeArrangedStatusId = PDR_STATUS.STATUSID FROM PDR_STATUS WHERE TITLE = 'To be Arranged'
		--=====================Search Criteria===============================
		SET @searchCriteria = ' PDR_MSAT.MSATTypeId= '+convert(varchar(10),@MSATTypeId)+' AND PDR_JOURNAL.STATUSID= '+convert(varchar(10),@ToBeArrangedStatusId)+''
		
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND (ISNULL(P__PROPERTY.HouseNumber, '''')	+ ISNULL('' ''+P__PROPERTY.ADDRESS1, '''') 	+ ISNULL('', ''+P__PROPERTY.ADDRESS2, '''') LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR CONVERT(VARCHAR,Case When C__CUSTOMER.TITLE=6 then '''' Else  ISNULL(G_TITLE.[DESCRIPTION],'''')END)+ CONVERT(VARCHAR, ISNULL(C__CUSTOMER.FIRSTNAME,''''))+ CONVERT(VARCHAR, ISNULL(C__CUSTOMER.LASTNAME,'''')) LIKE ''%' + @searchText + '%'')'
		END	
		
		--=======================Select Clause=============================================
		SET @SelectClause = 'Select top ('+convert(varchar(10),@limit)+')
							 ''JSV''+CONVERT(VARCHAR, RIGHT(''000000''+ CONVERT(VARCHAR,ISNULL(PDR_JOURNAL.JOURNALID,-1)),4)) AS Ref
		,ISNULL(P__PROPERTY.HouseNumber, '''')	+ ISNULL('' ''+P__PROPERTY.ADDRESS1, '''') 	+ ISNULL('', ''+P__PROPERTY.ADDRESS2, '''') AS Address
		,	P__PROPERTY.TOWNCITY,P__PROPERTY.COUNTY,	ISNULL(P__PROPERTY.POSTCODE, '''') AS Postcode,PDR_JOURNAL.JOURNALID as JournalId 
		, CONVERT(VARCHAR,Case When C__CUSTOMER.TITLE=6 then '''' Else  ISNULL(G_TITLE.[DESCRIPTION],'''')END)+ CONVERT(VARCHAR, ISNULL(+'' ''+C__CUSTOMER.FIRSTNAME,''''))+'' ''+ CONVERT(VARCHAR, ISNULL(C__CUSTOMER.LASTNAME,'''')) AS Tenant 
		,CONVERT(nvarchar(50),PDR_MSAT.TerminationDate, 103) as Termination	
		,CONVERT(nvarchar(50),DATEADD(day,7,PDR_MSAT.TerminationDate), 103) as Relet		
		,P__PROPERTY.PropertyId as PropertyId
		,C__CUSTOMER.CUSTOMERID  as CustomerId
		,C_TENANCY.TENANCYID as TenancyId'
		
		
		--============================From Clause============================================
		SET @fromClause = CHAR(10) +'FROM	PDR_JOURNAL
		INNER JOIN	PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId
		INNER JOIN	PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId		
		LEFT JOIN	P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID	
		INNER JOIN	PDR_STATUS ON PDR_JOURNAL.STATUSID = PDR_STATUS.STATUSID
		INNER JOIN C_TENANCY ON PDR_MSAT.TenancyId=	C_TENANCY.TENANCYID
		INNER JOIN C__CUSTOMER ON PDR_MSAT.CustomerId = C__CUSTOMER.CUSTOMERID
		INNER JOIN C_CUSTOMERTENANCY on C__CUSTOMER.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID and C_TENANCY.TENANCYID=C_CUSTOMERTENANCY.TENANCYID
		LEFT JOIN G_TITLE ON C__CUSTOMER.TITLE=G_TITLE.TITLEID
		'
							
		--============================Order Clause==========================================
		IF(@sortColumn = 'Ref')
		BEGIN
			SET @sortColumn = CHAR(10)+ ' PDR_JOURNAL.JOURNALID' 	
			
		END
		
		IF(@sortColumn = 'Address')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Address' 	
			
		END
		IF(@sortColumn = 'Tenant')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Tenant' 	
			
		END
		
		IF(@sortColumn = 'Termination')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Termination' 	
			
		END
		
		IF(@sortColumn = 'Relet')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Relet' 	
			
		END
				
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		--===============================Main Query ====================================
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
		IF(@getOnlyCount=0)
		BEGIN
			print(@finalQuery)
			EXEC (@finalQuery)
		END
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
END
GO
/****** Object:  StoredProcedure [dbo].[V_GetVoidInspectionArranged]    Script Date: 09/07/2015 15:17:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:           Ali Raza
-- Create date:      28/05/2015
-- Description:		 Get Void Inspections  Arranged List
-- History:          28/05/2015 AR : Get Void Inspections  Arranged List 
-- =============================================
--  DECLARE	@totalCount int
--EXEC	 [dbo].[V_GetVoidInspectionArranged]
--		@searchText = NULL,
--		@pageSize = 100,
--		@pageNumber = 1,
--		@totalCount = @totalCount OUTPUT
--SELECT	@totalCount as N'@totalCount'
-- =============================================
CREATE PROCEDURE [dbo].[V_GetVoidInspectionArranged]
-- Add the parameters for the stored procedure here
		@searchText VARCHAR(200)='',
	--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'JournalId', 
		@sortOrder varchar (5) = 'DESC',
		@getOnlyCount bit=0,
		@totalCount int = 0 output	
AS
BEGIN
	DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(2000),
        
        --variables for paging
        @offset int,
		@limit int,
		@MSATTypeId int,
		@ToBeArrangedStatusId int
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		
		SELECT 	@MSATTypeId = MSATTypeId  FROM PDR_MSATType WHERE MSATTypeName = 'Void Inspection'
		SELECT  @ToBeArrangedStatusId = PDR_STATUS.STATUSID FROM PDR_STATUS WHERE TITLE = 'Arranged'
		--=====================Search Criteria===============================
		SET @searchCriteria = ' PDR_MSAT.MSATTypeId= '+convert(varchar(10),@MSATTypeId)+' AND PDR_JOURNAL.STATUSID= '+convert(varchar(10),@ToBeArrangedStatusId)+''
		
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND (ISNULL(P__PROPERTY.HouseNumber, '''')	+ ISNULL('' ''+P__PROPERTY.ADDRESS1, '''') 	+ ISNULL('', ''+P__PROPERTY.ADDRESS2, '''') LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR CONVERT(VARCHAR,Case When C__CUSTOMER.TITLE=6 then '''' Else  ISNULL(G_TITLE.[DESCRIPTION],'''')END)+ CONVERT(VARCHAR, ISNULL(C__CUSTOMER.FIRSTNAME,''''))+ CONVERT(VARCHAR, ISNULL(C__CUSTOMER.LASTNAME,'''')) LIKE ''%' + @searchText + '%'')'
		END	
		
		--=======================Select Clause=============================================
		SET @SelectClause = 'Select DISTINCT top ('+convert(varchar(10),@limit)+')
							 ''JSV''+CONVERT(VARCHAR, RIGHT(''000000''+ CONVERT(VARCHAR,ISNULL(PDR_JOURNAL.JOURNALID,-1)),4)) AS Ref 
		,ISNULL(P__PROPERTY.HouseNumber, '''')	+ ISNULL('' ''+P__PROPERTY.ADDRESS1, '''') 	+ ISNULL('', ''+P__PROPERTY.ADDRESS2, '''') AS Address
		,	P__PROPERTY.TOWNCITY,P__PROPERTY.COUNTY,	ISNULL(P__PROPERTY.POSTCODE, '''') AS Postcode,PDR_JOURNAL.JOURNALID as JournalId
		, CONVERT(VARCHAR,Case When C__CUSTOMER.TITLE=6 then '''' Else  ISNULL(G_TITLE.[DESCRIPTION],'''')END)+ CONVERT(VARCHAR, ISNULL(C__CUSTOMER.FIRSTNAME,''''))+ CONVERT(VARCHAR, ISNULL(C__CUSTOMER.LASTNAME,'''')) AS Tenant 
		,CONVERT(nvarchar(50),PDR_MSAT.TerminationDate, 103) as Termination	
		,CONVERT(nvarchar(50),DATEADD(day,7,PDR_MSAT.TerminationDate), 103) as Relet	
		,LEFT(E__EMPLOYEE.Firstname, 1)+''''+LEFT(E__EMPLOYEE.LASTNAME, 1) as Surveyor
		,CONVERT(nvarchar(50),PDR_APPOINTMENTS.APPOINTMENTSTARTDATE, 103)+''</br> ''+CONVERT(VARCHAR(5), PDR_APPOINTMENTS.APPOINTMENTSTARTTIME, 108) 
		+''-''+CONVERT(VARCHAR(5), PDR_APPOINTMENTS.APPOINTMENTENDTIME, 108) as Appointment,PDR_MSAT.TenancyId,PDR_APPOINTMENTS.APPOINTMENTNOTES	
			'
		
		
		--============================From Clause============================================
		SET @fromClause = CHAR(10) +'FROM	PDR_JOURNAL
		INNER JOIN PDR_APPOINTMENTS ON PDR_JOURNAL.JOURNALID=PDR_APPOINTMENTS.JOURNALID
		INNER JOIN E__EMPLOYEE ON PDR_APPOINTMENTS.ASSIGNEDTO = E__EMPLOYEE.EMPLOYEEID
		INNER JOIN	PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId
		INNER JOIN	PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId		
		LEFT JOIN	P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID	
		INNER JOIN	PDR_STATUS ON PDR_JOURNAL.STATUSID = PDR_STATUS.STATUSID
		INNER JOIN C_TENANCY ON PDR_MSAT.TenancyId=	C_TENANCY.TENANCYID
		INNER JOIN C__CUSTOMER ON PDR_MSAT.CustomerId = C__CUSTOMER.CUSTOMERID
		INNER JOIN C_CUSTOMERTENANCY on C__CUSTOMER.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID and C_TENANCY.TENANCYID=C_CUSTOMERTENANCY.TENANCYID
		LEFT JOIN G_TITLE ON C__CUSTOMER.TITLE=G_TITLE.TITLEID
		'
							
		--============================Order Clause==========================================
		IF(@sortColumn = 'Ref')
		BEGIN
			SET @sortColumn = CHAR(10)+ ' PDR_JOURNAL.JOURNALID' 	
			
		END
		
		IF(@sortColumn = 'Address')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Address' 	
			
		END
		IF(@sortColumn = 'Tenant')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Tenant' 	
			
		END
		
		IF(@sortColumn = 'Termination')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Termination' 	
			
		END
		
		IF(@sortColumn = 'Relet')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Relet' 	
			
		END
				
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		--===============================Main Query ====================================
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
		IF(@getOnlyCount=0)
		BEGIN
			print(@finalQuery)
			EXEC (@finalQuery)
		END
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(DISTINCT P__PROPERTY.PropertyID) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
END
GO
/****** Object:  StoredProcedure [dbo].[V_GetVoidRoomList]    Script Date: 09/07/2015 15:17:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:           Ali Raza
-- Create date:      28/05/2015
-- Description:		 got room list to populate dropdown list
-- History:          22/05/2015 AR : got room list to populate dropdown list 
-- =============================================

CREATE PROCEDURE [dbo].[V_GetVoidRoomList] 
	
AS
BEGIN
	Select RoomId, Name from V_RoomList WHERE IsActive=1
END
GO
/****** Object:  StoredProcedure [dbo].[V_GetGasElectricChecksToBeArranged]    Script Date: 09/07/2015 15:17:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:           Ali Raza
-- Create date:      28/05/2015
-- Description:		 Get Gas Electric Checks To Be Arranged
-- History:          22/05/2015 AR : Get Gas Electric Checks To Be Arranged list 
-- =============================================

--  DECLARE	@totalCount int
--EXEC	 [dbo].[V_GetGasElectricChecksToBeArranged]
--		@searchText = NULL,
--		@checksRequired=1
--		@pageSize = 100,
--		@pageNumber = 1,
--		@totalCount = @totalCount OUTPUT
--SELECT	@totalCount as N'@totalCount'
-- =============================================
CREATE PROCEDURE [dbo].[V_GetGasElectricChecksToBeArranged]
-- Add the parameters for the stored procedure here
		@searchText VARCHAR(200)='',
		@checksRequired int = 1,
	--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'JournalId', 
		@sortOrder varchar (5) = 'DESC',
		@getOnlyCount bit=0,
		@totalCount int = 0 output	
AS
BEGIN
	DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(5000),
        
        --variables for paging
        @offset int,
		@limit int,
		@checksRequiredType varchar(200),
		@MSATTypeId int,
		@ArrangedStatusId int
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		
		SELECT  @ArrangedStatusId = PDR_STATUS.STATUSID FROM PDR_STATUS WHERE TITLE = 'To Be Arranged'
		SET @checksRequiredType='Gas/Electric Check'
		--=====================Search Criteria===============================
		SET @searchCriteria = ' 1=1'
		
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND (ISNULL(P.HouseNumber, '''')	+ ISNULL('' ''+P.ADDRESS1, '''') 	+ ISNULL('', ''+P.ADDRESS2, '''') LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR CONVERT(VARCHAR,Case When C.TITLE=6 then '''' Else  ISNULL(G_TITLE.[DESCRIPTION],'''')END)+ CONVERT(VARCHAR, ISNULL(C.FIRSTNAME,''''))+ CONVERT(VARCHAR, ISNULL(C.LASTNAME,'''')) LIKE ''%' + @searchText + '%'')'
		END	
		
		IF (@checksRequired = 1 AND @getOnlyCount=0)
			BEGIN
				--SET @searchCriteria = @searchCriteria + CHAR(10) +' AND (rec.IsGasCheckRequired=1 OR N.IsGasCheck=1)'
				SET @checksRequiredType='Gas Check'
				SELECT 	@MSATTypeId = MSATTypeId  FROM PDR_MSATType WHERE MSATTypeName = 'Void Gas Check'
				SET @searchCriteria = @searchCriteria + CHAR(10) +' AND  PDR_MSAT.MSATTypeId= '+convert(varchar(10),@MSATTypeId)
			END
		ELSE IF (@checksRequired = 2 AND @getOnlyCount=0)
			BEGIN
				--SET @searchCriteria = @searchCriteria + CHAR(10) +' AND (rec.IsElectricCheckRequired=1 OR N.IsElectricCheck=1)'
				SET @checksRequiredType='Electric Check'
				SELECT 	@MSATTypeId = MSATTypeId  FROM PDR_MSATType WHERE MSATTypeName = 'Void Electric Check'
				SET @searchCriteria = @searchCriteria + CHAR(10) +' AND  PDR_MSAT.MSATTypeId= '+convert(varchar(10),@MSATTypeId)
			END
			
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND PDR_JOURNAL.STATUSID= '+convert(varchar(10),@ArrangedStatusId)+''

		--=======================Select Clause=============================================
		SET @SelectClause = 'Select DISTINCT top ('+convert(varchar(10),@limit)+')
							 ''JSV''+CONVERT(VARCHAR, RIGHT(''000000''+ CONVERT(VARCHAR,ISNULL(PDR_JOURNAL.JOURNALID,-1)),4)) AS Ref
		,ISNULL(P.HouseNumber, '''')	+ ISNULL('' ''+P.ADDRESS1, '''') 	+ ISNULL('', ''+P.ADDRESS2, '''') AS Address
		,	P.TOWNCITY,P.COUNTY,	ISNULL(P.POSTCODE, '''') AS Postcode,PDR_JOURNAL.JOURNALID as JournalId 
		, CONVERT(VARCHAR,Case When C.TITLE=6 then '''' Else  ISNULL(G_TITLE.[DESCRIPTION],'''')END)+ CONVERT(VARCHAR, ISNULL(+'' ''+C.FIRSTNAME,''''))+ CONVERT(VARCHAR, ISNULL(+'' ''+C.LASTNAME,'''')) AS Tenant 
		,CONVERT(nvarchar(50),TerminationDate, 103) as Termination
		,Case When PDR_MSAT.ReletDate is NULL then CONVERT(nvarchar(50),DATEADD(day,7,PDR_MSAT.TerminationDate), 103) Else	CONVERT(nvarchar(50),PDR_MSAT.ReletDate, 103) END as Relet
		,PDR_MSAT.TenancyId	,'''+@checksRequiredType+''' AS Type'
			
		
		
		--============================From Clause============================================
		SET @fromClause = CHAR(10) +'From  PDR_JOURNAL 
			INNER JOIN	PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId
			INNER JOIN	PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId		
			INNER JOIN	PDR_STATUS ON PDR_JOURNAL.STATUSID = PDR_STATUS.STATUSID
			INNER JOIN	P__PROPERTY P ON PDR_MSAT.PropertyId = P.PROPERTYID	
			INNER JOIN C_TENANCY ON PDR_MSAT.TenancyId=	C_TENANCY.TENANCYID
			INNER JOIN C__CUSTOMER C ON PDR_MSAT.CustomerId = C.CUSTOMERID
			INNER JOIN C_CUSTOMERTENANCY on C.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID and C_TENANCY.TENANCYID=C_CUSTOMERTENANCY.TENANCYID
			LEFT JOIN G_TITLE ON C.TITLE=G_TITLE.TITLEID
			
		'
							
		--============================Order Clause==========================================
		IF(@sortColumn = 'Ref')
		BEGIN
			SET @sortColumn = CHAR(10)+ ' PDR_JOURNAL.JOURNALID' 	
			
		END
		
		IF(@sortColumn = 'Address')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Address' 	
			
		END
		IF(@sortColumn = 'Tenant')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Tenant' 	
			
		END
		
		IF(@sortColumn = 'Termination')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Termination' 	
			
		END
		
		IF(@sortColumn = 'Relet')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Relet' 	
			
		END
				
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		--===============================Main Query ====================================
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		print(@searchCriteria)
		
		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
		IF(@getOnlyCount=0)
		BEGIN
			
			EXEC (@finalQuery)
		END
		print(@finalQuery)
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
END
GO
/****** Object:  StoredProcedure [dbo].[V_GetGasElectricChecksArranged]    Script Date: 09/07/2015 15:17:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:           Ali Raza
-- Create date:      28/05/2015
-- Description:		 Get Gas Electric Checks  Arranged
-- History:          22/05/2015 AR : Get Gas Electric Checks  Arranged list 
-- =============================================
--  DECLARE	@totalCount int
--EXEC	 [dbo].[V_GetChecksArranged]
--		@searchText = NULL,
--		@checksRequired=1,
--		@pageSize = 100,
--		@pageNumber = 1,
--		@totalCount = @totalCount OUTPUT
--SELECT	@totalCount as N'@totalCount'
-- =============================================
CREATE PROCEDURE [dbo].[V_GetGasElectricChecksArranged]
-- Add the parameters for the stored procedure here
		@searchText VARCHAR(200),
		@checksRequired int = 1,
	--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'JournalId', 
		@sortOrder varchar (5) = 'DESC',
		@getOnlyCount bit=0,
		@totalCount int = 0 output	
AS
BEGIN
	DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(2000),
        
        --variables for paging
        @offset int,
		@limit int,
		@checksRequiredType varchar(200),
		@MSATTypeId int,
		@ArrangedStatusId int
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		
		SELECT  @ArrangedStatusId = PDR_STATUS.STATUSID FROM PDR_STATUS WHERE TITLE = 'Arranged'
		--=====================Search Criteria===============================
		SET @searchCriteria = ' 1=1 '
		
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND (ISNULL(P__PROPERTY.HouseNumber, '''')	+ ISNULL('' ''+P__PROPERTY.ADDRESS1, '''') 	+ ISNULL('', ''+P__PROPERTY.ADDRESS2, '''') LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR CONVERT(VARCHAR,Case When C__CUSTOMER.TITLE=6 then '''' Else  ISNULL(G_TITLE.[DESCRIPTION],'''')END)+ CONVERT(VARCHAR, ISNULL(C__CUSTOMER.FIRSTNAME,''''))+ CONVERT(VARCHAR, ISNULL(C__CUSTOMER.LASTNAME,'''')) LIKE ''%' + @searchText + '%'')'
		END	
		
		IF (@checksRequired = 1)
		BEGIN
			
			SET @checksRequiredType='Gas Check'
			SELECT 	@MSATTypeId = MSATTypeId  FROM PDR_MSATType WHERE MSATTypeName = 'Void Gas Check'
		
		END
		ELSE IF (@checksRequired = 2)
		BEGIN
			
			SET @checksRequiredType='Electric Check'
			SELECT 	@MSATTypeId = MSATTypeId  FROM PDR_MSATType WHERE MSATTypeName = 'Void Electric Check'
		END
		
		SET @searchCriteria = @searchCriteria + CHAR(10) +' AND PDR_MSAT.MSATTypeId= '+convert(varchar(10),@MSATTypeId)+' AND PDR_JOURNAL.STATUSID= '+convert(varchar(10),@ArrangedStatusId)+''
		
		--=======================Select Clause=============================================
		SET @SelectClause = 'Select top ('+convert(varchar(10),@limit)+')
							 ''JSV''+CONVERT(VARCHAR, RIGHT(''000000''+ CONVERT(VARCHAR,ISNULL(PDR_JOURNAL.JOURNALID,-1)),4)) AS Ref,PDR_JOURNAL.JOURNALID as JournalId 
		,ISNULL(P__PROPERTY.HouseNumber, '''')	+ ISNULL('' ''+P__PROPERTY.ADDRESS1, '''') 	+ ISNULL('', ''+P__PROPERTY.ADDRESS2, '''') AS Address
		,	ISNULL(P__PROPERTY.POSTCODE, '''') AS Postcode
		, CONVERT(VARCHAR,Case When C__CUSTOMER.TITLE=6 then '''' Else  ISNULL(G_TITLE.[DESCRIPTION],'''')END)+ CONVERT(VARCHAR, ISNULL(+'' ''+C__CUSTOMER.FIRSTNAME,''''))+ CONVERT(VARCHAR, ISNULL(+'' ''+C__CUSTOMER.LASTNAME,'''')) AS Tenant 
		,CONVERT(nvarchar(50),PDR_MSAT.TerminationDate, 103) as Termination	
		,Case When PDR_MSAT.ReletDate is NULL then CONVERT(nvarchar(50),DATEADD(day,7,PDR_MSAT.TerminationDate), 103) Else	CONVERT(nvarchar(50),PDR_MSAT.ReletDate, 103) END as Relet
		,PDR_MSAT.TenancyId	,'''+@checksRequiredType+''' AS Type
		,LEFT(E__EMPLOYEE.Firstname, 1)+''''+LEFT(E__EMPLOYEE.LASTNAME, 1) as Surveyor
		,CONVERT(nvarchar(50),PDR_APPOINTMENTS.APPOINTMENTSTARTDATE, 103)+''</br> ''+CONVERT(VARCHAR(5), PDR_APPOINTMENTS.APPOINTMENTSTARTTIME, 108) 
		as Appointment	,PDR_APPOINTMENTS.APPOINTMENTNOTES as AppointmentNotes 	
		
		
		'
			
		
		
		--============================From Clause============================================
		SET @fromClause = CHAR(10) +'FROM	PDR_JOURNAL
	
		INNER JOIN	PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId
		INNER JOIN	PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId		
		LEFT JOIN	P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID	
		INNER JOIN	PDR_STATUS ON PDR_JOURNAL.STATUSID = PDR_STATUS.STATUSID
		INNER JOIN C_TENANCY ON PDR_MSAT.TenancyId=	C_TENANCY.TENANCYID
		INNER JOIN C__CUSTOMER ON PDR_MSAT.CustomerId = C__CUSTOMER.CUSTOMERID
		INNER JOIN C_CUSTOMERTENANCY on C__CUSTOMER.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID and C_TENANCY.TENANCYID=C_CUSTOMERTENANCY.TENANCYID
		LEFT JOIN G_TITLE ON C__CUSTOMER.TITLE=G_TITLE.TITLEID
		INNER JOIN PDR_APPOINTMENTS ON PDR_JOURNAL.JOURNALID=PDR_APPOINTMENTS.JOURNALID
		INNER JOIN E__EMPLOYEE ON PDR_APPOINTMENTS.ASSIGNEDTO = E__EMPLOYEE.EMPLOYEEID
		
		'
							
		--============================Order Clause==========================================
		IF(@sortColumn = 'Ref')
		BEGIN
			SET @sortColumn = CHAR(10)+ ' PDR_JOURNAL.JOURNALID' 	
			
		END
		
		IF(@sortColumn = 'Address')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Address' 	
			
		END
		IF(@sortColumn = 'Tenant')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Tenant' 	
			
		END
		
		IF(@sortColumn = 'Termination')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Termination' 	
			
		END
		
		IF(@sortColumn = 'Relet')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Relet' 	
			
		END
				
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		--===============================Main Query ====================================
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
		IF(@getOnlyCount=0)
		BEGIN
			print(@finalQuery)
			EXEC (@finalQuery)
		END
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
END
GO

/****** Object:  StoredProcedure [dbo].[V_GetBritishGasNotificationList]    Script Date: 09/07/2015 15:17:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
/* =================================================================================    
    Page Description: Get British Gas Notification List 
    Author: Noor Muhammad
    Creation Date: May-20-2015

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0        May-20-2015     Noor Muhammad      Get British Gas Notification List. Currently for inProgress and Complete list is using the same SP 
												   because the screen data is same except the status. If there is some change in tables or join or some big change then 
												   please consider creating the separate SP for Inporgress and complete list of british gas notification. 
  =================================================================================*/
--DECLARE	@return_value int,
--		@totalCount int

--EXEC	@return_value = [dbo].[V_GetBritishGasNotificationList]
--		@searchText = NULL,
--		@notificationStatus = N'InProgress',
--		@pageSize = 30,
--		@pageNumber = 1,
--		@sortColumn = 'NotificationStageId',
--		@sortOrder = 'DESC',
--		@totalCount = @totalCount OUTPUT

--SELECT	@totalCount as N'@totalCount'

--SELECT	'Return Value' = @return_value
-- =============================================
CREATE PROCEDURE [dbo].[V_GetBritishGasNotificationList]
-- Add the parameters for the stored procedure here
		@searchText VARCHAR(200),
		@notificationStatus varchar(20) = 'InProgress',
	--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'NotificationStageId', 
		@sortOrder varchar (5) = 'DESC',
		@getOnlyCount bit=0,
		@totalCount int = 0 output	
AS
BEGIN
	DECLARE 
		
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(2000),
        @notificationStatusId int,

        --variables for paging
        @offset int,
		@limit int	
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		
		--=====================Search Criteria===============================		
		IF @notificationStatus = 'InProgress'
		BEGIN
			SELECT  @notificationStatusId = PDR_STATUS.STATUSID FROM PDR_STATUS WHERE TITLE = 'In Progress'
		END
		ELSE IF @notificationStatus = 'Complete'
		BEGIN
			SELECT  @notificationStatusId = PDR_STATUS.STATUSID FROM PDR_STATUS WHERE TITLE = 'Completed'
		END
		
		SET @searchCriteria = 'PDR_STATUS.StatusId = '+convert(varchar(10),@notificationStatusId)+''
		
		
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +'AND (ISNULL(P__PROPERTY.HouseNumber, '''')	+ ISNULL('' ''+P__PROPERTY.ADDRESS1, '''') 	+ ISNULL('', ''+P__PROPERTY.ADDRESS2, '''') + ISNULL(P__PROPERTY.POSTCODE, '''') LIKE ''%' + @searchText + '%'')'
		END	
		
		--=======================Select Clause=============================================
		SET @SelectClause = 'Select DISTINCT top ('+convert(varchar(10),@limit)+')							 
		ISNULL(P__PROPERTY.HouseNumber, '''')	+ ISNULL('' ''+P__PROPERTY.ADDRESS1, '''') 	+ ISNULL('', ''+P__PROPERTY.ADDRESS2, '''') AS Address
		,ISNULL(V_BritishGasVoidNotification.TenantFwAddress1, '''') + ISNULL('' ''+V_BritishGasVoidNotification.TenantFwAddress2, '''') + ISNULL('' ''+V_BritishGasVoidNotification.TenantFwPostCode, '''') + ISNULL('' ''+V_BritishGasVoidNotification.TenantFwCity, '''') AS FWAddress
		,ISNULL(P__PROPERTY.POSTCODE, '''') AS Postcode		
		,CONVERT(nvarchar(50),C_TERMINATION.TERMINATIONDATE, 103) as Termination	
		,C__CUSTOMER.CUSTOMERID as CustomerId 
		,P__PROPERTY.PROPERTYID as PropertyId 
		,C_TENANCY.TENANCYID as TenancyId 
		,PDR_STATUS.StatusId as NotificationStatusId
		,V_Stage.StageId as NotificationStageId 
		,PDR_STATUS.Title as NotificationStatusTitle
		,V_Stage.Name as NotificationStageTitle
		,isNotificationSent
		,BritishGasVoidNotificationId as NotificationId
		'
				
		--============================From Clause============================================
		SET @fromClause = CHAR(10) +'From V_BritishGasVoidNotification
		INNER JOIN PDR_STATUS on PDR_STATUS.StatusId = V_BritishGasVoidNotification.StatusId 
		INNER JOIN V_Stage ON V_BritishGasVoidNotification.StageId = V_Stage.StageId
		INNER JOIN P__PROPERTY ON V_BritishGasVoidNotification.PropertyId = P__PROPERTY.PROPERTYID	
		INNER JOIN C__CUSTOMER ON V_BritishGasVoidNotification.CustomerId = C__CUSTOMER.CUSTOMERID
		INNER JOIN C_TENANCY ON V_BritishGasVoidNotification.TenancyId=	C_TENANCY.TENANCYID
		INNER JOIN C_CUSTOMERTENANCY on C__CUSTOMER.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID and C_TENANCY.TENANCYID=C_CUSTOMERTENANCY.TENANCYID
		INNER JOIN C_JOURNAL ON C_JOURNAL.CUSTOMERID  = C__CUSTOMER.CUSTOMERID AND C_JOURNAL.TENANCYID = C_TENANCY.TENANCYID AND C_JOURNAL.PROPERTYID = P__PROPERTY.PROPERTYID
		INNER JOIN C_TERMINATION ON C_JOURNAL.JOURNALID = C_TERMINATION.JOURNALID'
							
		--============================Order Clause==========================================		
		IF(@sortColumn = 'NotificationStageId')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'NotificationStageId' 				
		END
		
		IF(@sortColumn = 'Address')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Address' 	
			
		END
		IF(@sortColumn = 'Postcode')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Postcode' 	
			
		END
		
		IF(@sortColumn = 'Termination')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Termination' 				
		END
		
		IF(@sortColumn = 'F/WAddress')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'FWAddress' 	
			
		END
				
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		--===============================Main Query ====================================
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
		IF(@getOnlyCount=0)
		BEGIN
			print(@finalQuery)
			EXEC (@finalQuery)
		END
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(DISTINCT P__PROPERTY.PROPERTYID) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
END
GO
/****** Object:  StoredProcedure [dbo].[V_GetPaintPacksDetail]    Script Date: 09/07/2015 15:17:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:           Ali Raza
-- Create date:      28/05/2015
-- Description:		 Get Paint Packs Detail
-- History:          22/05/2015 AR : Get Paint Packs Detail
-- =============================================
/* =================================================================================    
-- Execute Command   
	EXEC V_GetPaintPacksDetail 	1   
  =================================================================================*/
CREATE PROCEDURE [dbo].[V_GetPaintPacksDetail] 
	(
	@paintPackId int
	)
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;
--=====================================================
--Get Paint Pack Header values
--=====================================================

SELECT
	ISNULL(P__PROPERTY.HouseNumber, '') + ISNULL(' ' + P__PROPERTY.ADDRESS1, '') + ISNULL(', ' + P__PROPERTY.ADDRESS2, '') AS Address,
	ISNULL(CONVERT(VARCHAR, CASE
		WHEN C__CUSTOMER.TITLE = 6 THEN ''
		ELSE ISNULL(G_TITLE.[DESCRIPTION], '')
	END) + CONVERT(VARCHAR(50), ' ' + C__CUSTOMER.FIRSTNAME) + CONVERT(VARCHAR(50), ' ' + C__CUSTOMER.LASTNAME),'N/A') AS NewTenant,
	CONVERT(NVARCHAR(50), PDR_MSAT.TerminationDate, 103) AS Termination,
	CONVERT(NVARCHAR(50), DATEADD(DAY, 7, PDR_MSAT.TerminationDate), 103) AS Relet,
	ISNULL(C_ADDRESS.TEL,'N/A') AS NewTelephone,
	C_TENANCY.TENANCYID AS TenancyId,
	Vacating.Tenant AS VacatingTenant,
	ISNULL(Vacating.Tele,'N/A') AS VacatingTelephone,
	ISNULL(P.SupplierId,'-1') as Supplier,
	ISNULL(P.StatusId,'-1') as StatusId,
	ISNULL(CONVERT(NVARCHAR(50), P.DeliveryDueDate, 103),'') as DeliveryDueDate
FROM PDR_JOURNAL J
				INNER JOIN V_PaintPack P ON J.JOURNALID = P.InspectionJournalId

				INNER JOIN PDR_MSAT ON J.MSATID = PDR_MSAT.MSATId
				INNER JOIN PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId
				INNER JOIN P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID
				LEFT JOIN C_TENANCY ON P__PROPERTY.PROPERTYID = C_TENANCY.PROPERTYID
					AND C_TENANCY.ENDDATE IS NULL
				LEFT JOIN C_CUSTOMERTENANCY ON C_TENANCY.TENANCYID = C_CUSTOMERTENANCY.TENANCYID
				LEFT JOIN C__CUSTOMER ON C_CUSTOMERTENANCY.CUSTOMERID = C__CUSTOMER.CUSTOMERID
				LEFT JOIN C_ADDRESS ON C__CUSTOMER.CUSTOMERID = C_ADDRESS.CUSTOMERID AND ISDEFAULT = 1
				LEFT JOIN G_TITLE ON C__CUSTOMER.TITLE = G_TITLE.TITLEID
				INNER JOIN (SELECT CU.CUSTOMERID AS CID, CONVERT(VARCHAR, CASE WHEN CU.TITLE = 6 THEN '' ELSE ISNULL(G_TITLE.[DESCRIPTION], '')END) + CONVERT(VARCHAR, ISNULL(+' ' + CU.FIRSTNAME, '')) + ' ' + CONVERT(VARCHAR, ISNULL(CU.LASTNAME, '')) AS Tenant,
					AD.TEL AS Tele FROM C__CUSTOMER CU
				LEFT JOIN C_ADDRESS AD ON CU.CUSTOMERID = AD.CUSTOMERID AND ISDEFAULT = 1
				LEFT JOIN G_TITLE ON CU.TITLE = G_TITLE.TITLEID) AS Vacating
					ON PDR_MSAT.CustomerId = Vacating.CID

WHERE P.PaintPackId = @paintPackId

--=====================================================
--Get Paint Pack Detail 
--=====================================================

SELECT PD.PaintPackDetailId,ISNULL(PD.PaintRef,'-') AS PaintRef ,ISNULL(PD.PaintColor,'-') as PaintColor ,ISNULL(PD.PaintName,'-') as PaintName
,R.Name As Room from 
V_PaintPackDetails PD
INNER JOIN V_RoomList R ON PD.RoomId = R.RoomId
WHERE PD.PaintPackId = @paintPackId
--=====================================================
--Get Status For Dropdown
--=====================================================
Select StatusId,Title  from V_PaintStatus
--=====================================================
--Get Supplier for Dropdown
--=====================================================
SELECT DISTINCT O.ORGID AS [Id],NAME AS [Description] FROM S_ORGANISATION O ORDER BY NAME ASC
END
GO
/****** Object:  StoredProcedure [dbo].[V_GetBritishGasStage1Data]    Script Date: 09/07/2015 15:17:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =============================================
 Author:		<Author,Noor Muhamamd>
 Create date: <Create Date,25/05/2015>
 Description:	<Description,got room list to populate dropdown list>
 History:	25/06/2015 Noor : Query Get the record to populate the british gas notification of stage1
            30/09/2014 Name : Correct the query to fetch gas electric check boolean value
-- =============================================*/
CREATE PROCEDURE [dbo].[V_GetBritishGasStage1Data] 
	@propertyId varchar(20)
	,@customerId int
	,@tenancyId int
AS
BEGIN
	SELECT 
	case when  V_BritishGasVoidNotification.IsGasCheck=1 OR  V_BritishGasVoidNotification.IsElectricCheck=1 then 1 Else 0 END   AS IsGasElectricCheck
	,P__PROPERTY.PROPERTYID AS PropertyId
	,C__CUSTOMER.CUSTOMERID AS CustomerId
	,C_TENANCY.TENANCYID  AS TenancyId
	,P__PROPERTY.ADDRESS1 AS Address1
	,P__PROPERTY.ADDRESS2 AS Address2
	,P__PROPERTY.TOWNCITY AS City
	,P__PROPERTY.County As County
	,P__PROPERTY.POSTCODE AS PostCode
	, CONVERT(VARCHAR,Case When C__CUSTOMER.TITLE=6 then '' Else  ISNULL(G_TITLE.[DESCRIPTION],'')END)+ CONVERT(VARCHAR, ISNULL(+' '+C__CUSTOMER.FIRSTNAME,''))+' '+ CONVERT(VARCHAR, ISNULL(C__CUSTOMER.LASTNAME,'')) AS TenantName 
	,V_BritishGasVoidNotification.TenantFwAddress1 AS TenantFwAddress1
	,V_BritishGasVoidNotification.TenantFwAddress2 AS TenantFwAddress2
	,V_BritishGasVoidNotification.TenantFwCity AS TenantFwCity
	,V_BritishGasVoidNotification.TenantFwPostCode AS TenantFwPostCode	
	,Case When V_BritishGasVoidNotification.DateOccupancyCease IS NULL
		THEN  
			(SELECT TOP 1 PDR_MSAT.TerminationDate 
				FROM PDR_MSAT 
				WHERE 
				PDR_MSAT.PROPERTYID =@propertyId
				AND PDR_MSAT.TENANCYID = @tenancyId
				AND PDR_MSAT.CUSTOMERID = @customerId 
				AND TERMINATIONDATE IS NOT NULL) 
		ELSE V_BritishGasVoidNotification.DateOccupancyCease
		END
		AS DateOccupancyCease
	FROM V_BritishGasVoidNotification 
	INNER JOIN	P__PROPERTY ON V_BritishGasVoidNotification.PropertyId = P__PROPERTY.PROPERTYID	
	INNER JOIN C_TENANCY ON V_BritishGasVoidNotification.TenancyId=	C_TENANCY.TENANCYID
	INNER JOIN C__CUSTOMER ON V_BritishGasVoidNotification.CustomerId = C__CUSTOMER.CUSTOMERID
	INNER JOIN C_CUSTOMERTENANCY on C__CUSTOMER.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID and C_TENANCY.TENANCYID=C_CUSTOMERTENANCY.TENANCYID
	LEFT JOIN G_TITLE ON C__CUSTOMER.TITLE=G_TITLE.TITLEID
	WHERE 
	 P__PROPERTY.PROPERTYID =@propertyId
	 AND C_TENANCY.TENANCYID = @tenancyId
	 AND C__CUSTOMER.CUSTOMERID = @customerId

END
GO
/****** Object:  StoredProcedure [dbo].[V_GetVoidOperatives]    Script Date: 09/07/2015 15:17:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    Page Description: Get void Operatives for scheduling first inspection popUp.
    Author: Ali Raza
    Creation Date: May-15-2015

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         May-15-2015      Ali Raza        Get void Operatives for scheduling first inspection popUp.
  =================================================================================*/
CREATE PROCEDURE [dbo].[V_GetVoidOperatives]
	
AS
BEGIN
	SELECT distinct E__EMPLOYEE.employeeid as EmployeeId
	,E__EMPLOYEE.FirstName as FirstName
	,E__EMPLOYEE.LastName as LastName
	,E__EMPLOYEE.FirstName + ' '+ E__EMPLOYEE.LastName as FullName	
	,E_JOBDETAILS.PATCH as PatchId
	,E_PATCH.Location as PatchName
	
	
	FROM  E__EMPLOYEE 	
	INNER JOIN (SELECT Distinct EmployeeId,InspectionTypeID FROM AS_USER_INSPECTIONTYPE) AS_USER_INSPECTIONTYPE ON E__EMPLOYEE.EMPLOYEEID=AS_USER_INSPECTIONTYPE.EmployeeId
	INNER JOIN dbo.P_INSPECTIONTYPE  ON AS_USER_INSPECTIONTYPE.InspectionTypeID=P_INSPECTIONTYPE.InspectionTypeID 
	INNER JOIN E_JOBDETAILS ON E__EMPLOYEE.EMPLOYEEID = E_JOBDETAILS.EMPLOYEEID
	LEFT JOIN E_PATCH ON E_JOBDETAILS.PATCH = E_PATCH.PATCHID
	
	WHERE P_INSPECTIONTYPE.Description  ='void'
	AND E_JOBDETAILS.Active=1
	AND E__EMPLOYEE.EmployeeId NOT IN (
			SELECT 
				EMPLOYEEID 
				FROM E_JOURNAL
				INNER JOIN E_ABSENCE ON E_JOURNAL.JOURNALID = E_ABSENCE.JOURNALID 
				AND E_ABSENCE.ABSENCEHISTORYID IN (
													SELECT 
														MAX(ABSENCEHISTORYID) 
														FROM E_ABSENCE 
														GROUP BY JOURNALID
													)
				WHERE ITEMNATUREID = 1
				AND E_ABSENCE.RETURNDATE IS NULL
				AND E_ABSENCE.ITEMSTATUSID = 1
			)
END
GO
/****** Object:  StoredProcedure [dbo].[V_GetReactiveRepairContractors]    Script Date: 09/07/2015 15:17:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ali Raza
-- Create date: 25/08/2015
-- Description:	To get contractors having at lease one Reactive Repair contract, as drop down values for assgin work to contractor.
-- =============================================
CREATE PROCEDURE [dbo].[V_GetReactiveRepairContractors] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT O.ORGID AS [id],
			NAME AS [description]
	FROM S_ORGANISATION O
	INNER JOIN S_SCOPE S ON O.ORGID = S.ORGID
	INNER JOIN S_SCOPETOPATCHANDSCHEME SS ON S.SCOPEID = SS.SCOPEID 
	INNER JOIN S_AREAOFWORK AoW ON S.AREAOFWORK = AoW.AREAOFWORKID 
	WHERE AoW.DESCRIPTION = 'Reactive Repair' 			
			--AND (S.CREATIONDATE < GETDATE()
			--	AND S.RENEWALDATE > GETDATE())
	
END
GO
/****** Object:  StoredProcedure [dbo].[V_GetContractDetailByContractorId]    Script Date: 09/07/2015 15:17:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Ahmed Mehmood
-- Create date: 30/01/2015
-- Description:	Get Contract Detail By ContractorId
-- History:		25/08/2015 AR:To get contractors having at lease one planned contract, as drop down values for assgin work to contractor.
-- =============================================
CREATE PROCEDURE [dbo].[V_GetContractDetailByContractorId] 
	-- Add the parameters for the stored procedure here	 

	@contractorId int
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT DISTINCT S.STARTDATE StartDate, S.RENEWALDATE EndDate
	FROM S_ORGANISATION O
	INNER JOIN S_SCOPE S ON O.ORGID = S.ORGID
	INNER JOIN S_AREAOFWORK AoW ON S.AREAOFWORK = AoW.AREAOFWORKID
	INNER JOIN S_SCOPETOPATCHANDSCHEME SPC ON S.SCOPEID = SPC.SCOPEID
	WHERE O.ORGID = @contractorId 
	
END
GO
/****** Object:  StoredProcedure [dbo].[V_GetMeterTypes]    Script Date: 09/07/2015 15:17:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:           Noor Muhammad
-- Create date:      23/06/2015
-- Description:      Get the Meter Types
-- History:          23/06/2015 Noor : Query for gas meter type
--                   23/09/2014 Noor : Add query for electric meter type
-- =============================================
CREATE PROCEDURE [dbo].[V_GetMeterTypes] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT PA_PARAMETER_VALUE.ValueDetail as Title, PA_PARAMETER_VALUE.ValueID as Id FROM PA_ITEM 
	INNER JOIN PA_ITEM_PARAMETER ON  PA_ITEM.ItemID = PA_ITEM_PARAMETER.ItemId
	INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID
	INNER JOIN PA_PARAMETER_VALUE ON PA_PARAMETER.ParameterID  = PA_PARAMETER_VALUE.ParameterID
	WHERE ItemName ='Gas'
	AND PA_PARAMETER.ParameterName = 'Meter Type'
	AND PA_ITEM.IsActive = 1
	AND PA_ITEM_PARAMETER.IsActive =1 
	AND PA_PARAMETER.IsActive =1
	
	
	SELECT PA_PARAMETER_VALUE.ValueDetail as Title, PA_PARAMETER_VALUE.ValueID as Id FROM PA_ITEM 
	INNER JOIN PA_ITEM_PARAMETER ON  PA_ITEM.ItemID = PA_ITEM_PARAMETER.ItemId
	INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID
	INNER JOIN PA_PARAMETER_VALUE ON PA_PARAMETER.ParameterID  = PA_PARAMETER_VALUE.ParameterID
	WHERE ItemName ='Electric'
	AND PA_PARAMETER.ParameterName = 'Meter Type'
	AND PA_ITEM.IsActive = 1
	AND PA_ITEM_PARAMETER.IsActive =1 
	AND PA_PARAMETER.IsActive =1
END
GO
/****** Object:  StoredProcedure [dbo].[V_GetOperativesLeavesAndAppointment]    Script Date: 09/07/2015 15:17:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:           Ali Raza
-- Create date:      28/05/2015
-- Description:		 This stored procedure fetch  the employees
-- History:          22/05/2015 AR : This stored procedure fetch  the employees
-- =============================================

CREATE PROCEDURE [dbo].[V_GetOperativesLeavesAndAppointment] 
	-- Add the parameters for the stored procedure here
	
	@employeeId as int,
	@startDate as datetime	
AS
BEGIN

	--=================================================================================================================
	SELECT distinct E__EMPLOYEE.employeeid as EmployeeId
	,E__EMPLOYEE.FirstName as FirstName
	,E__EMPLOYEE.LastName as LastName
	,E__EMPLOYEE.FirstName + ' '+ E__EMPLOYEE.LastName as FullName	
	,E_JOBDETAILS.PATCH as PatchId
	,E_PATCH.Location as PatchName
	
	
	FROM  E__EMPLOYEE 	
	INNER JOIN (SELECT Distinct EmployeeId,InspectionTypeID FROM AS_USER_INSPECTIONTYPE) AS_USER_INSPECTIONTYPE ON E__EMPLOYEE.EMPLOYEEID=AS_USER_INSPECTIONTYPE.EmployeeId
	INNER JOIN dbo.P_INSPECTIONTYPE  ON AS_USER_INSPECTIONTYPE.InspectionTypeID=P_INSPECTIONTYPE.InspectionTypeID 
	INNER JOIN E_JOBDETAILS ON E__EMPLOYEE.EMPLOYEEID = E_JOBDETAILS.EMPLOYEEID
	LEFT JOIN E_PATCH ON E_JOBDETAILS.PATCH = E_PATCH.PATCHID
	
	WHERE P_INSPECTIONTYPE.Description  ='void'
	AND E_JOBDETAILS.Active=1
	AND E__EMPLOYEE.EmployeeId=@employeeId
	------------------------------------------------------ Step 3------------------------------------------------------						
	--This query selects the leaves of employees i.e the employess which we get in step1
	--M : morning - 08:00 AM - 12:00 PM
	--A : mean after noon - 01:00 PM - 05:00 PM
	--F : Single full day
	--F-F : Multiple full days
	--F-M : Multiple full days with last day  morning - 08:00 AM - 12:00 PM
	--A-F : From First day after noon - 01:00 PM - 05:00 PM with multiple full days
	


SELECT E_ABSENCE.STARTDATE as StartDate
	, E_ABSENCE.RETURNDATE	as EndDate
	, E_JOURNAL.employeeid as OperativeId
	,E_ABSENCE.HolType as HolType
	,E_ABSENCE.duration as Duration
	,CASE 
		WHEN HolType = 'M' THEN '09:00 AM'
		WHEN HolType = 'A' THEN '01:00 PM'
		WHEN HolType = 'F' THEN '00:00 AM'
		WHEN HolType = 'F-F' THEN '00:00 AM'
		WHEN HolType = 'F-M' THEN '00:00 AM'
		WHEN HolType = 'A-F' THEN '01:00 PM'
	END as StartTime
	,CASE 
		WHEN HolType = 'M' THEN '01:00 PM'
		WHEN HolType = 'A' THEN '05:00 PM'
		WHEN HolType = 'F' THEN '11:59 PM'
		WHEN HolType = 'F-F' THEN '11:59 PM'
		WHEN HolType = 'F-M' THEN '01:00 PM'
		WHEN HolType = 'A-F' THEN '11:59 PM'
	END as EndTime
	,CASE 
		WHEN HolType = 'M' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), StartDate,103) + ' ' + '09:00 AM',103)) 
		WHEN HolType = 'A' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), StartDate,103) + ' ' + '01:00 PM',103)) 
		WHEN HolType = 'F' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), StartDate,103),103)) 
		WHEN HolType = 'F-F' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), StartDate,103),103)) 
		WHEN HolType = 'F-M' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), StartDate,103),103)) 
		WHEN HolType = 'A-F' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), StartDate,103) + ' ' + '01:00 PM',103)) 
	END as StartTimeInMin
	,CASE 
		WHEN HolType = 'M' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), ReturnDate,103) + ' ' + '01:00 PM',103)) 
		WHEN HolType = 'A' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), ReturnDate,103) + ' ' + '05:00 PM',103)) 
		WHEN HolType = 'F' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), ReturnDate,103)+ ' ' + '11:59 PM',103)) 
		WHEN HolType = 'F-F' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), ReturnDate,103)+ ' ' + '11:59 PM',103)) 
		WHEN HolType = 'F-M' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), ReturnDate,103) + ' ' + '01:00 PM',103)) 
		WHEN HolType = 'A-F' THEN datediff(mi, '1970-01-01', convert(datetime,convert(varchar(10), ReturnDate,103)+ ' ' + '11:59 PM',103)) 
	END as EndTimeInMin
	FROM E_JOURNAL 
	INNER JOIN E_ABSENCE ON E_JOURNAL.JOURNALID = E_ABSENCE.JOURNALID AND E_ABSENCE.ABSENCEHISTORYID IN (SELECT MAX(ABSENCEHISTORYID) FROM E_ABSENCE GROUP BY JOURNALID)
	WHERE
	(
		-- To filter for M&E Servicing or Cyclic Maintenance i.e annual leaves etc. where approval is needed
		( 
			E_ABSENCE.ITEMSTATUSID = 5
			AND itemnatureid in (2,3,4,5,6,8,9,10,11,13,14,15,16,32,43,47)   
		)
		OR
		-- To filter for sickness leaves. where the operative is now returned to work.
		( 
			ITEMNATUREID = 1
			AND E_ABSENCE.ITEMSTATUSID = 2
			AND E_ABSENCE.RETURNDATE IS NOT NULL 
		)
	)
	AND E_ABSENCE.RETURNDATE >= CONVERT(DATE,@startDate)
	AND E_JOURNAL.employeeid =@employeeId							

	--=================================================================================================================
	------------------------------------------------------ Step 4------------------------------------------------------						
	--This query selects the appointments of employees i.e the employess which we get in step1
	--Fault Appointments
	--Gas Appointments
	--Planned Appointments
	--M&E Servicing or Cyclic Maintenance Appointments
	-----------------------------------------------------------------------------------------------------------------------
	-- Fault Appointments
	SELECT AppointmentDate as AppointmentStartDate
	,AppointmentDate as AppointmentEndDate
	,OperativeId as OperativeId
	,Time as StartTime
	,EndTime as EndTime
	,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), appointmentdate,103) + ' ' + Time,103)) as StartTimeInSec
	,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), appointmentdate,103) + ' ' + EndTime,103)) as EndTimeInSec
	,p__property.postcode as PostCode
	,ISNULL(P__PROPERTY.HouseNumber,'') +' '+ ISNULL(P__PROPERTY.ADDRESS1,'') +' '+ ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'')  AS Address			
	,P__PROPERTY.TownCity as TownCity    
	,P__PROPERTY.County as County
	,'Fault Appointment'  as AppointmentType
	FROM FL_CO_Appointment 
	inner join fl_fault_appointment on FL_co_APPOINTMENT.appointmentid = fl_fault_appointment.appointmentid
	inner join fl_fault_log on fl_fault_appointment.faultlogid = fl_fault_log.faultlogid
	INNER JOIN FL_FAULT_STATUS ON FL_FAULT_STATUS.FaultStatusID = FL_FAULT_LOG.StatusID
	inner join p__property on fl_fault_log.propertyid = p__property.propertyid				
	WHERE 
	1=1
	AND appointmentdate >= CONVERT(DATE,@startDate)
	AND (FL_FAULT_STATUS.Description <> 'Cancelled' AND FL_FAULT_STATUS.Description <> 'Complete')
	AND operativeid =@employeeId
	-----------------------------------------------------------------------------------------------------------------------
	--Appliance Appointments	
	UNION ALL 	
	SELECT 
		AS_APPOINTMENTS.AppointmentDate as AppointmentStartDate
		,AS_APPOINTMENTS.AppointmentDate as AppointmentEndDate
		,AS_APPOINTMENTS.ASSIGNEDTO as OperativeId
		,APPOINTMENTSTARTTIME as StartTime
		,APPOINTMENTENDTIME as EndTime
		,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), AS_APPOINTMENTS.AppointmentDate,103) + ' ' + APPOINTMENTSTARTTIME,103)) as StartTimeInSec
		,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), AS_APPOINTMENTS.AppointmentDate,103) + ' ' + APPOINTMENTENDTIME,103)) as EndTimeInSec
		,p__property.postcode as PostCode
		,ISNULL(P__PROPERTY.HouseNumber,'') +' '+ ISNULL(P__PROPERTY.ADDRESS1,'') +' '+ ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'')  AS Address
		,P__PROPERTY.TownCity as TownCity
		,P__PROPERTY.County as County
		,'Gas Appointment' as AppointmentType  
	FROM AS_APPOINTMENTS 
		INNER JOIN AS_JOURNAL ON AS_APPOINTMENTS.JournalId=AS_JOURNAL.JOURNALID 
		INNER JOIN AS_Status ON AS_JOURNAL.StatusId = AS_Status.StatusId
		INNER JOIN P__PROPERTY ON AS_JOURNAL.PROPERTYID=P__PROPERTY.PROPERTYID 
		INNER JOIN P_STATUS  ON P__PROPERTY.[STATUS] =P_STATUS.STATUSID  
		LEFT JOIN C_TENANCY ON P__PROPERTY.PROPERTYID=C_TENANCY.PROPERTYID AND (DBO.C_TENANCY.ENDDATE IS NULL OR DBO.C_TENANCY.ENDDATE > GETDATE())
	WHERE AS_JOURNAL.IsCurrent = 1		
		AND Convert(date,AS_APPOINTMENTS.AppointmentDate,103) >= Convert(date,@startDate,103)		
		AND AS_APPOINTMENTS.ASSIGNEDTO =@employeeId		
	-----------------------------------------------------------------------------------------------------------------------
	--Planned Appointments
	UNION ALL 
	SELECT 
		APPOINTMENTDATE as AppointmentStartDate
		,APPOINTMENTENDDATE as AppointmentEndDate
		,ASSIGNEDTO  as OperativeId
		,APPOINTMENTSTARTTIME as StartTime
		,APPOINTMENTENDTIME as EndTime
		,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), APPOINTMENTDATE,103) + ' ' + APPOINTMENTSTARTTIME ,103)) as StartTimeInSec
		,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), APPOINTMENTENDDATE,103) + ' ' + APPOINTMENTENDTIME,103)) as EndTimeInSec
		,p__property.postcode as PostCode
		,ISNULL(P__PROPERTY.HouseNumber,'') +' '+ ISNULL(P__PROPERTY.ADDRESS1,'') +' '+ ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'')  AS Address			
		,P__PROPERTY.TownCity as TownCity    
		,P__PROPERTY.County as County
		,'Planned Appointment' as AppointmentType
	FROM 
		PLANNED_APPOINTMENTS
		INNER JOIN PLANNED_JOURNAL ON PLANNED_JOURNAL.JOURNALID = PLANNED_APPOINTMENTS.JournalId 
		INNER JOIN P__PROPERTY ON PLANNED_JOURNAL.PROPERTYID = P__PROPERTY.PROPERTYID 
	WHERE 
		Convert(date,AppointmentDate,103) >= Convert(date,@startDate,103)		
		AND PLANNED_APPOINTMENTS.ASSIGNEDTO =@employeeId
		
	-----------------------------------------------------------------------------------------------------------------------
	--M&E Appointments
	UNION ALL 
	SELECT 
		APPOINTMENTSTARTDATE as AppointmentStartDate
		,APPOINTMENTENDDATE as AppointmentEndDate
		,ASSIGNEDTO  as OperativeId
		,APPOINTMENTSTARTTIME as StartTime
		,APPOINTMENTENDTIME as EndTime
		,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), APPOINTMENTSTARTDATE,103) + ' ' + APPOINTMENTSTARTTIME ,103)) as StartTimeInSec
		,datediff(s, '1970-01-01', convert(datetime,convert(varchar(10), APPOINTMENTENDDATE,103) + ' ' + APPOINTMENTENDTIME,103)) as EndTimeInSec
		,P__PROPERTY.postcode as PostCode
		,ISNULL(P__PROPERTY.HouseNumber,'') +' '+ ISNULL(P__PROPERTY.ADDRESS1,'') +' '+ ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'')  AS Address			
		,ISNULL(P__PROPERTY.TownCity,'') as TownCity    
		,ISNULL(P__PROPERTY.County,'') as County
		,PDR_MSATType.MSATTypeName + ' Appointment' as AppointmentType
	FROM 
		PDR_APPOINTMENTS
		INNER JOIN PDR_JOURNAL ON PDR_JOURNAL.JOURNALID = PDR_APPOINTMENTS.JournalId 
		INNER JOIN PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId
		INNER JOIN PDR_MSATType on PDR_MSAT.MSATTypeId=PDR_MSATType.MSATTypeId
		LEFT JOIN P__PROPERTY ON PDR_MSAT.PROPERTYID = P__PROPERTY.PROPERTYID 
	WHERE 
		Convert(date,APPOINTMENTSTARTDATE,103) >= Convert(date,@startDate,103)		
		AND PDR_APPOINTMENTS.ASSIGNEDTO =@employeeId
				

END
GO
/****** Object:  StoredProcedure [dbo].[V_GetPropertyCompenentDetails]    Script Date: 09/07/2015 15:17:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:           Ali Raza
-- Create date:      28/07/2015
-- Description:      Void Tenancy
-- History:          28/07/2015 Ali Raza : This SP used in PSAOffline to get planned compenent list

-- =============================================
CREATE PROCEDURE [dbo].[V_GetPropertyCompenentDetails] 
	@propertyId as varchar(20)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT	
		PC.COMPONENTNAME AS Component
		,ISNULL(COMPONENT_CONDITION.ValueDetail,'-') AS Condition
		,ISNULL(CONVERT(VARCHAR,COMPONENT_DATES.LastDone,103),'-') AS LastReplaced
		,ISNULL(CONVERT(VARCHAR,COMPONENT_DATES.DueDate,103),'-') AS DueDate
		,PC.COMPONENTID as ComponentId
	FROM	PLANNED_COMPONENT PC
		INNER JOIN PLANNED_COMPONENT_ITEM PCI ON PC.COMPONENTID = PCI.COMPONENTID
		INNER JOIN PA_ITEM I ON PCI.ITEMID = I.ItemID 
		LEFT JOIN PA_PARAMETER PP ON PCI.PARAMETERID = PP.ParameterID
		
		LEFT JOIN (	SELECT	ComponentId,ValueDetail, PPA.UPDATEDBY AS UPDATEDBY 
					FROM	PLANNED_CONDITIONWORKS PCW
							INNER JOIN PA_PROPERTY_ATTRIBUTES PPA ON PCW.AttributeId = PPA.ATTRIBUTEID 
							INNER JOIN PA_PARAMETER_VALUE PPV ON  PPA.VALUEID = PPV.ValueID
					WHERE	PPA.PROPERTYID= @propertyId) COMPONENT_CONDITION ON PC.COMPONENTID = COMPONENT_CONDITION.ComponentId 
		
		LEFT JOIN ( SELECT	PLANNED_COMPONENTID,MAX(SID) AS SUB_SID
						FROM	PA_PROPERTY_ITEM_DATES
						WHERE   PROPERTYID = @propertyId
						GROUP BY PLANNED_COMPONENTID )  AS PID_SUB ON PC.COMPONENTID = PID_SUB.PLANNED_COMPONENTID
		LEFT JOIN	PA_PROPERTY_ITEM_DATES COMPONENT_DATES ON PID_SUB.SUB_SID = COMPONENT_DATES.SID
	
END
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:           Ali Raza
-- Create date:      07/09/2015
-- Description:      Get Property Info By JournalId 
-- =============================================
-- =============================================
-- EXEC PDR_GetPropertyInfoByJournalId
	--@journalId = 24

-- =============================================
ALTER PROCEDURE [dbo].[V_GetPropertyInfoByJournalId](
	@journalId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	 		
	SELECT 
		ISNULL(C_TENANCY.TENANCYID,-1) as TenancyId,		
		ISNULL(G_TITLE.DESCRIPTION +' '+ C__CUSTOMER.FIRSTNAME + ' '+ C__CUSTOMER.LASTNAME ,'N/A') as TenantName,
		ISNULL(P__PROPERTY.HouseNumber,'') +' '+ ISNULL(P__PROPERTY.ADDRESS1,'') +' '+ ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'')+ISNULL(', '+P__PROPERTY.TOWNCITY ,'') +' '+ISNULL(P__PROPERTY.POSTCODE  ,'') as Address,
		ISNULL(P__PROPERTY.HOUSENUMBER, '') AS HOUSENUMBER,		
		ISNULL(P__PROPERTY.ADDRESS1,'') as ADDRESS1,
		ISNULL(P__PROPERTY.ADDRESS2,'') as ADDRESS2,
		ISNULL(P__PROPERTY.TOWNCITY,'') as TOWNCITY,
		ISNULL(P__PROPERTY.COUNTY,'') as COUNTY,
		ISNULL(P__PROPERTY.POSTCODE,'') as POSTCODE,
		ISNULL (G_TITLE.DESCRIPTION,'') + ISNULL(' ' + SUBSTRING(C__CUSTOMER.FIRSTNAME,1,1) ,'') + ISNULL(' '+ C__CUSTOMER.LASTNAME ,'') as TenantNameSalutation,
		ISNULL(C_ADDRESS.MOBILE ,'N/A') as Mobile,
		ISNULL(C_ADDRESS.TEL ,'N/A') as Telephone,
		ISNULL(P_SCHEME.SCHEMENAME,'N/A') AS SchemeName,
		ISNULL(C_ADDRESS.EMAIL,'N/A') as Email,
		ISNULL(C__CUSTOMER.CUSTOMERID,-1) as CustomerId,
		ISNULL(P_BLOCK.BLOCKNAME  ,'N/A') as BlockName
	FROM
		PDR_JOURNAL 	
		INNER JOIN PDR_MSAT ON PDR_JOURNAL.MSATID =  PDR_MSAT.MSATId
		INNER JOIN PDR_MSATType ON PDR_MSAT.MSATTypeId =  PDR_MSATType.MSATTypeId
		LEFT JOIN P__PROPERTY ON PDR_MSAT.PropertyId = P__PROPERTY.PROPERTYID		
		LEFT JOIN C_TENANCY ON C_TENANCY.TENANCYID = PDR_MSAT.TenancyId 		
		LEFT JOIN C__CUSTOMER ON C__CUSTOMER.CUSTOMERID = PDR_MSAT.CustomerId
		LEFT JOIN C_ADDRESS ON C_ADDRESS.CUSTOMERID = C__CUSTOMER.CUSTOMERID AND C_ADDRESS.ISDEFAULT = 1  
		LEFT JOIN G_TITLE ON C__CUSTOMER.TITLE = G_TITLE.TITLEID
		LEFT JOIN P_SCHEME ON PDR_MSAT.SCHEMEID = P_SCHEME.SCHEMEID OR P_SCHEME.SCHEMEID = P__PROPERTY.SCHEMEID
		LEFT JOIN P_BLOCK ON PDR_MSAT.BLOCKID = P_BLOCK.BLOCKID OR P_BLOCK.BLOCKID=P__PROPERTY.BLOCKID
							  
	WHERE PDR_JOURNAL.JOURNALID =@journalId
	
					
END
