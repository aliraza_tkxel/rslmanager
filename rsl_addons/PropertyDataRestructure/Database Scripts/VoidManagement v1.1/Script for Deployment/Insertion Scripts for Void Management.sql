-- =============================================
-- Author:           Ali Raza
-- Create date:      07/09/2015
-- Description:      Insertion Scripts for Void Management
-- =============================================


BEGIN TRANSACTION
	BEGIN TRY  

---=====================================
--- Insertion Script for V_Stage
---=====================================
INSERT [dbo].[V_Stage] ([StageId], [Name], [IsActive]) VALUES (1, N'Stage 1', 1)
INSERT [dbo].[V_Stage] ([StageId], [Name], [IsActive]) VALUES (2, N'Stage 2', 1)
INSERT [dbo].[V_Stage] ([StageId], [Name], [IsActive]) VALUES (3, N'Stage 3', 1)
---=====================================
--- Script for Paint Pack Purchase Order
---=====================================

	DECLARE @headId int,@expendId int


	INSERT INTO F_HEAD (DESCRIPTION, HEADDATE, USERID, ACTIVE_Z, LASTMODIFIED, HEADALLOCATION_Z, COSTCENTREID)
	Values('Paint Packs', GETDATE(),943,1,GETDATE(),6500, (Select COSTCENTREID from F_COSTCENTRE WHERE DESCRIPTION='Estate Maintenance'))
	 
	 SET @headId = SCOPE_IDENTITY()
	 
	  INSERT INTO F_HEAD_ALLOCATION(HEADID,FISCALYEAR, MODIFIED, MODIFIEDBY, HEADALLOCATION, ACTIVE)
	  VALUES(@headId,17,GETDATE(),943,6500,1)
	 
	 
	 
	 INSERT INTO F_EXPENDITURE (DESCRIPTION, EXPENDITUREDATE, USERID, ACTIVE_Z, LASTMODIFIED, HEADID)
	 VALUES('Paint Packs Work', GETDATE(), 943,1 , GETDATE(),@headId)
	  
	  SET @expendId = SCOPE_IDENTITY()
	  
	INSERT INTO F_EXPENDITURE_ALLOCATION(EXPENDITUREID, FISCALYEAR, EXPENDITUREALLOCATION, MODIFIED, MODIFIEDBY, ACTIVE)
	 VALUES(@expendId,17,6500,GETDATE(),943,1)
	

---=====================================
--- End Script for Paint Pack Purchase Order
---=====================================

---=======================================================================================================================
---=======================================================================================================================

---=====================================
---Script for Void access rights  
---=====================================
	
	DECLARE @PROPERTY_MODULEID INT,@Void_MENUID INT
	DECLARE @Void_SCHEDULING_PAGEID INT,@Void_Reports_PAGEID INT,@Void_Works_PAGEID INT,@Void_PaintPacks_PAGEID INT
--======================================================================================================
--												MODULES
--======================================================================================================

SELECT	@PROPERTY_MODULEID = MODULEID
FROM	AC_MODULES
WHERE	DESCRIPTION = 'Property'
--======================================================================================================
--												MENUS
--======================================================================================================
	SELECT @Void_MENUID=MENUID From AC_MENUS Where DESCRIPTION='Voids'
	Update AC_MENUS SET PAGE='~/../PropertyDataRestructure/Bridge.aspx?mn=Voids' Where DESCRIPTION='Voids'
	
--======================================================================================================
--												PAGES	(LEVEL 1)
--======================================================================================================
	
INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Dashboard',@Void_MENUID,1,1,'~/../PropertyDataRestructure/Views/Void/Dashboard/Dashboard.aspx','1',1,NULL,'',1)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Resources',@Void_MENUID,1,1,'','2',1,NULL,'',1)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Scheduling',@Void_MENUID,1,1,'','3',1,NULL,'',1)
SET @Void_SCHEDULING_PAGEID = SCOPE_IDENTITY()

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Calendar',@Void_MENUID,1,1,'','4',1,NULL,'',1)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Reports',@Void_MENUID,1,1,'','5',1,NULL,'',1)

SET @Void_Reports_PAGEID = SCOPE_IDENTITY()	

--======================================================================================================
--												PAGES	(LEVEL 2)
--======================================================================================================
 	 	
--=========================
-- Voids > Scheduling
--=========================

--Void Inspections
INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Void Inspections',@Void_MENUID,1,1,'~/../PropertyDataRestructure/Views/Void/VoidInspection/VoidInspections.aspx','1',2,@Void_SCHEDULING_PAGEID,'',1)

--Post Void Inspections
INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Post Void Inspections',@Void_MENUID,1,1,'~/../PropertyDataRestructure/Views/Void/VoidInspection/PostVoidInspections.aspx','2',2,@Void_SCHEDULING_PAGEID,'',1)

--Void Works
INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Void Works',@Void_MENUID,1,1,'~/../PropertyDataRestructure/Views/Void/VoidWorks/VoidAppointments.aspx','3',2,@Void_SCHEDULING_PAGEID,'',1)
SET @Void_Works_PAGEID = SCOPE_IDENTITY()	

--Paint Packs
INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Paint Packs',@Void_MENUID,1,1,'~/../PropertyDataRestructure/Views/Void/PaintPacks/PaintPacksList.aspx','4',2,@Void_SCHEDULING_PAGEID,'',1)
SET @Void_PaintPacks_PAGEID = SCOPE_IDENTITY()
	
--Gas/Electric Checks
INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Gas/Electric Checks',@Void_MENUID,1,1,'~/../PropertyDataRestructure/Views/Void/GasElectricCheck/GasElectricChecks.aspx','5',2,@Void_SCHEDULING_PAGEID,'',1)
	
	
--=========================
-- Voids > Reports
--=========================

--Available Properties
INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Available Properties',@Void_MENUID,1,1,'~/../PropertyDataRestructure/Views/Reports/VoidReportArea.aspx?rpt=ap','1',2,@Void_Reports_PAGEID,'',1)

--British Gas Notifications
INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('British Gas Notifications',@Void_MENUID,1,1,'~/../PropertyDataRestructure/Views/Void/BritishGasNotification/BritishGasNotificationList.aspx','2',2,@Void_Reports_PAGEID,'',1)

--No Entry Report
INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('No Entries',@Void_MENUID,1,1,'~/../PropertyDataRestructure/Views/Reports/VoidReportArea.aspx?rpt=ne','3',2,@Void_Reports_PAGEID,'',1)
	
--======================================================================================================
--												PAGES	(LEVEL 3)
--======================================================================================================

--Pending Termination
INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Pending Termination',@Void_MENUID,1,0,'~/../PropertyDataRestructure/Views/Reports/VoidReportArea.aspx?rpt=pt','1',3,@Void_Reports_PAGEID,'',0)

--Relet Due In Next 7 Days
INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Relet Due In Next 7 Days',@Void_MENUID,1,0,'~/../PropertyDataRestructure/Views/Reports/VoidReportArea.aspx?rpt=rd','1',3,@Void_Reports_PAGEID,'',0)

--================================================
-- Void > Scheduling > Required Void Works 
--================================================

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Void Works Required',@Void_MENUID,1,0,'~/../PropertyDataRestructure/Views/Void/VoidWorks/VoidWorksRequired.aspx','1',3,@Void_Works_PAGEID,'',0)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Scheduling Required Works',@Void_MENUID,1,0,'~/../PropertyDataRestructure/Views/Void/VoidWorks/SchedulingRequiredWorks.aspx','2',3,@Void_Works_PAGEID,'',0)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('ReScheduling Required Works',@Void_MENUID,1,0,'~/../PropertyDataRestructure/Views/Void/VoidWorks/RearrangeWorksRequired.aspx','3',3,@Void_Works_PAGEID,'',0)

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('JSV Job Sheet Summary',@Void_MENUID,1,0,'~/../PropertyDataRestructure/Views/Void/VoidWorks/JSVJobSheetSummary.aspx','4',3,@Void_Works_PAGEID,'',0)

--================================================
-- Void > Scheduling > Paint Pack List 
--================================================

INSERT INTO [AC_PAGES] ([DESCRIPTION],[MENUID],[ACTIVE],[LINK],[PAGE],[ORDERTEXT],[AccessLevel],[ParentPage],[BridgeActualPage],[DisplayInTree])
VALUES	('Paint Packs Detail',@Void_MENUID,1,0,'~/../PropertyDataRestructure/Views/Void/PaintPacks/PaintPacksDetail.aspx','1',3,@Void_PaintPacks_PAGEID,'',0)
 
 
 
---=====================================
---End Script for Void access rights  
---=====================================
---=======================================================================================================================
---=======================================================================================================================


---=====================================
---End Script for White board alerts
---=====================================
DECLARE @alertID int
Select @alertID=MAX(AlertID) from E_ALERTS
INSERT INTO E_ALERTS(AlertID,  AlertName,SOrder,AlertUrl)Values(@alertID+1,'Available Properties',0,'/PropertyDataRestructure/Bridge.aspx?pg=AvailableProperties') 

INSERT INTO E_ALERTS(AlertID,AlertName,SOrder,AlertUrl)Values(@alertID+2,'Void Inspections To Be Arranged',0,'/PropertyDataRestructure/Bridge.aspx?pg=Voids') 
INSERT INTO E_ALERTS(AlertID,AlertName,SOrder,AlertUrl)Values(@alertID+3,'Gas Void Notifications',0,'/PropertyDataRestructure/Bridge.aspx?pg=Notifications') 


---=======================================================================================================================
---=======================================================================================================================

---=====================================
--- Script for V_PaintStatus
---=====================================
 
 INSERT [dbo].[V_PaintStatus] ([StatusId], [Title], [IsActive]) VALUES (1, N'Logged', 1)
INSERT [dbo].[V_PaintStatus] ([StatusId], [Title], [IsActive]) VALUES (2, N'In Progress', 1)
INSERT [dbo].[V_PaintStatus] ([StatusId], [Title], [IsActive]) VALUES (3, N'Ordered', 1)
INSERT [dbo].[V_PaintStatus] ([StatusId], [Title], [IsActive]) VALUES (4, N'Delivered', 1)
INSERT [dbo].[V_PaintStatus] ([StatusId], [Title], [IsActive]) VALUES (5, N'Complete', 1)
---=======================================================================================================================
---=======================================================================================================================

---=====================================
--- Script for PDR_MSATType
---=====================================


INSERT [dbo].[PDR_MSATType] ([MSATTypeId], [MSATTypeName], [IsActive]) VALUES (5, N'Void Inspection', 1)
INSERT [dbo].[PDR_MSATType] ([MSATTypeId], [MSATTypeName], [IsActive]) VALUES (6, N'Post Void Inspection', 1)
INSERT [dbo].[PDR_MSATType] ([MSATTypeId], [MSATTypeName], [IsActive]) VALUES (7, N'Void Works', 1)
INSERT [dbo].[PDR_MSATType] ([MSATTypeId], [MSATTypeName], [IsActive]) VALUES (8, N'Void Gas Check', 1)
INSERT [dbo].[PDR_MSATType] ([MSATTypeId], [MSATTypeName], [IsActive]) VALUES (9, N'Void Electric Check', 1)
INSERT [dbo].[PDR_MSATType] ([MSATTypeId], [MSATTypeName], [IsActive]) VALUES (10, N'Void Major Works', 1)


---=======================================================================================================================
---=======================================================================================================================

---=====================================
--- Script for V_RoomList
---=====================================
INSERT INTO V_RoomList(Name,IsActive)
VALUES('Lounge',1),
('Kitchen/Diner',1),
('Kitchen',1),
('Hall',1),
('Stairwell/Landing',1),
('Bathroom',1),
('WC',1),
('Bedroom 1',1),
('Bedroom 2',1),
('Bedroom 3',1),
('Bedroom 4',1),
('Other',1)

---=======================================================================================================================
---=======================================================================================================================

---=====================================
--- Script for Heating Meters
---=====================================

	
--  Internals > Services > Heating & Water Supply > Heating
declare @itemid int;
declare @paramid int;
declare @paramSOrder int;
SELECT @itemid = ItemID from PA_ITEM where ItemName = 'Heating'
	
select @paramSOrder=ParameterSorder from PA_PARAMETER where ParameterName ='Meter Location'	

--For Gas Meter Type
insert into [RSLBHALive].[dbo].[PA_PARAMETER] (ParameterName,DataType,ControlType,[isdate],ParameterSorder,IsActive,ShowInApp)
values
('Gas Meter Type','String','Dropdown',0,@paramSOrder+1,1,1)

set @paramid = SCOPE_IDENTITY()

insert into [RSLBHALive].[dbo].[PA_ITEM_PARAMETER] (ItemId,ParameterId,IsActive)
values
(@itemid,@paramid,1)	

insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values(@paramid,'Coriolis',0,1)
insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values(@paramid,'Diaphragm/bellows',1,1)
insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values(@paramid,'Orifice',2,1)
insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values(@paramid,'Rotary',3,1)
insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values(@paramid,'Turbine',4,1)
insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values(@paramid,'Ultrasonic flow',5,1)
	
--For Gas Meter Reading
insert into [RSLBHALive].[dbo].[PA_PARAMETER] (ParameterName,DataType,ControlType,[isdate],ParameterSorder,IsActive,ShowInApp)
values
('Gas Meter Reading','String','TextBox',0,@paramSOrder+2,1,1)

set @paramid = SCOPE_IDENTITY()

insert into [RSLBHALive].[dbo].[PA_ITEM_PARAMETER] (ItemId,ParameterId,IsActive)
values
(@itemid,@paramid,1)	
	
--For Gas Meter Reading Date
insert into [RSLBHALive].[dbo].[PA_PARAMETER] (ParameterName,DataType,ControlType,[isdate],ParameterSorder,IsActive,ShowInApp)
values
('Gas Meter Reading Date','Date','Date',1,@paramSOrder+2,1,1)

set @paramid = SCOPE_IDENTITY()

insert into [RSLBHALive].[dbo].[PA_ITEM_PARAMETER] (ItemId,ParameterId,IsActive)
values
(@itemid,@paramid,1)


--For Electric Meter Type
insert into [RSLBHALive].[dbo].[PA_PARAMETER] (ParameterName,DataType,ControlType,[isdate],ParameterSorder,IsActive,ShowInApp)
values
('Electric Meter Type','String','Dropdown',0,@paramSOrder+3,1,1)

set @paramid = SCOPE_IDENTITY()

insert into [RSLBHALive].[dbo].[PA_ITEM_PARAMETER] (ItemId,ParameterId,IsActive)
values
(@itemid,@paramid,1)	
	
insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values(@paramid,'Standard Meter',0,1)
insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values(@paramid,'Variable Rate-Economy 7',1,1)
insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values(@paramid,'Pre-Payment',2,1)
insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values(@paramid,'Smart Meters',3,1)
insert into PA_PARAMETER_VALUE (ParameterID,ValueDetail,Sorder,IsActive) values(@paramid,'Economy 10',4,1)	
--For Electric Meter Reading
insert into [RSLBHALive].[dbo].[PA_PARAMETER] (ParameterName,DataType,ControlType,[isdate],ParameterSorder,IsActive,ShowInApp)
values
('Electric Meter Reading','String','TextBox',0,@paramSOrder+4,1,1)

set @paramid = SCOPE_IDENTITY()

insert into [RSLBHALive].[dbo].[PA_ITEM_PARAMETER] (ItemId,ParameterId,IsActive)
values
(@itemid,@paramid,1)	
	
--For Electric Meter Reading Date
insert into [RSLBHALive].[dbo].[PA_PARAMETER] (ParameterName,DataType,ControlType,[isdate],ParameterSorder,IsActive,ShowInApp)
values
('Electric Meter Reading Date','Date','Date',1,@paramSOrder+4,1,1)

set @paramid = SCOPE_IDENTITY()

insert into [RSLBHALive].[dbo].[PA_ITEM_PARAMETER] (ItemId,ParameterId,IsActive)
values
(@itemid,@paramid,1)






---=======================================================================================================================
---=======================================================================================================================



END TRY 
	BEGIN CATCH 	
		IF @@TRANCOUNT >0	  
		BEGIN	   
			  PRINT 'Some thing went wrong with this transaction'

			ROLLBACK TRANSACTION;	
		END 
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
		@ErrorSeverity, -- Severity.
		@ErrorState -- State.
		);
	END CATCH  
	 
	IF @@TRANCOUNT >0
	BEGIN
  PRINT 'Transaction completed successfully'
	
		COMMIT TRANSACTION;
	END 
