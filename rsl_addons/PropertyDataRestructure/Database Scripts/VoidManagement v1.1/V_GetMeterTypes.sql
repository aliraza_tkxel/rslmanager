-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:           Noor Muhammad
-- Create date:      23/06/2015
-- Description:      Get the Meter Types
-- History:          23/06/2015 Noor : Query for gas meter type
--                   23/09/2014 Noor : Add query for electric meter type
-- =============================================
CREATE PROCEDURE V_GetMeterTypes 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SELECT PA_PARAMETER_VALUE.ValueDetail as Title, PA_PARAMETER_VALUE.ValueID as Id FROM PA_ITEM 
	INNER JOIN PA_ITEM_PARAMETER ON  PA_ITEM.ItemID = PA_ITEM_PARAMETER.ItemId
	INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID
	INNER JOIN PA_PARAMETER_VALUE ON PA_PARAMETER.ParameterID  = PA_PARAMETER_VALUE.ParameterID
	WHERE ItemName ='Gas'
	AND PA_PARAMETER.ParameterName = 'Meter Type'
	AND PA_ITEM.IsActive = 1
	AND PA_ITEM_PARAMETER.IsActive =1 
	AND PA_PARAMETER.IsActive =1
	
	
	SELECT PA_PARAMETER_VALUE.ValueDetail as Title, PA_PARAMETER_VALUE.ValueID as Id FROM PA_ITEM 
	INNER JOIN PA_ITEM_PARAMETER ON  PA_ITEM.ItemID = PA_ITEM_PARAMETER.ItemId
	INNER JOIN PA_PARAMETER ON PA_ITEM_PARAMETER.ParameterId = PA_PARAMETER.ParameterID
	INNER JOIN PA_PARAMETER_VALUE ON PA_PARAMETER.ParameterID  = PA_PARAMETER_VALUE.ParameterID
	WHERE ItemName ='Electric'
	AND PA_PARAMETER.ParameterName = 'Meter Type'
	AND PA_ITEM.IsActive = 1
	AND PA_ITEM_PARAMETER.IsActive =1 
	AND PA_PARAMETER.IsActive =1
END
GO
