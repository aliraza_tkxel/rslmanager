/*
   Thursday, May 7, 20154:47:25 PM
   User: sa
   Server: DEV-PC4
   Database: RSLBHALive
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_V_Stage
	(
	StageId int NOT NULL,
	Name varchar(50) NOT NULL,
	IsActive bit NOT NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_V_Stage SET (LOCK_ESCALATION = TABLE)
GO
IF EXISTS(SELECT * FROM dbo.V_Stage)
	 EXEC('INSERT INTO dbo.Tmp_V_Stage (StageId, Name, IsActive)
		SELECT StageId, Name, IsActive FROM dbo.V_Stage WITH (HOLDLOCK TABLOCKX)')
GO
DROP TABLE dbo.V_Stage
GO
EXECUTE sp_rename N'dbo.Tmp_V_Stage', N'V_Stage', 'OBJECT' 
GO
ALTER TABLE dbo.V_Stage ADD CONSTRAINT
	PK_V_Stage PRIMARY KEY CLUSTERED 
	(
	StageId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
COMMIT
select Has_Perms_By_Name(N'dbo.V_Stage', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.V_Stage', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.V_Stage', 'Object', 'CONTROL') as Contr_Per 