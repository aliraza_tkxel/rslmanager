-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
    Page Description: Get void Operatives for scheduling first inspection popUp.
    Author: Ali Raza
    Creation Date: May-15-2015

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         May-15-2015      Ali Raza        Get void Operatives for scheduling first inspection popUp.
  =================================================================================*/
CREATE PROCEDURE V_GetVoidOperatives
	
AS
BEGIN
	SELECT distinct E__EMPLOYEE.employeeid as EmployeeId
	,E__EMPLOYEE.FirstName as FirstName
	,E__EMPLOYEE.LastName as LastName
	,E__EMPLOYEE.FirstName + ' '+ E__EMPLOYEE.LastName as FullName	
	,E_JOBDETAILS.PATCH as PatchId
	,E_PATCH.Location as PatchName
	
	
	FROM  E__EMPLOYEE 	
	INNER JOIN (SELECT Distinct EmployeeId,InspectionTypeID FROM AS_USER_INSPECTIONTYPE) AS_USER_INSPECTIONTYPE ON E__EMPLOYEE.EMPLOYEEID=AS_USER_INSPECTIONTYPE.EmployeeId
	INNER JOIN dbo.P_INSPECTIONTYPE  ON AS_USER_INSPECTIONTYPE.InspectionTypeID=P_INSPECTIONTYPE.InspectionTypeID 
	INNER JOIN E_JOBDETAILS ON E__EMPLOYEE.EMPLOYEEID = E_JOBDETAILS.EMPLOYEEID
	LEFT JOIN E_PATCH ON E_JOBDETAILS.PATCH = E_PATCH.PATCHID
	
	WHERE P_INSPECTIONTYPE.Description  ='void'
	AND E_JOBDETAILS.Active=1
	AND E__EMPLOYEE.EmployeeId NOT IN (
			SELECT 
				EMPLOYEEID 
				FROM E_JOURNAL
				INNER JOIN E_ABSENCE ON E_JOURNAL.JOURNALID = E_ABSENCE.JOURNALID 
				AND E_ABSENCE.ABSENCEHISTORYID IN (
													SELECT 
														MAX(ABSENCEHISTORYID) 
														FROM E_ABSENCE 
														GROUP BY JOURNALID
													)
				WHERE ITEMNATUREID = 1
				AND E_ABSENCE.RETURNDATE IS NULL
				AND E_ABSENCE.ITEMSTATUSID = 1
			)
END
GO
