USE [RSLBHALive]
GO
-- =============================================
-- Author:           Ali Raza
-- Create date:      03/07/2015
-- Description:      Cancel Gas/Electric Appointment 
-- History:          03/07/2015 Void: Cancel Gas/Electric Appointment 
--                   
-- =============================================

Create PROCEDURE [dbo].[V_CancelGasElectricAppointment]
	@journalId int	,
	@createdBy Int,
	@isCancelled int = 0 out
AS
BEGIN


BEGIN TRANSACTION;
BEGIN TRY

	-- ========================================================
	--	INSERT INTO PDR_CANCELLED_JOBS
	-- ========================================================
	INSERT INTO PDR_CANCELLED_JOBS (AppointmentId,RecordedOn)
	SELECT PDR_APPOINTMENTS.APPOINTMENTID as AppointmentId, GETDATE() as RecordedOn
	FROM PDR_APPOINTMENTS
	WHERE PDR_APPOINTMENTS.JournalId = @journalId
	AND PDR_APPOINTMENTS.APPOINTMENTSTATUS <> 'Complete'
	
	
	-- ========================================================
	--	UPDATE PDR_JOURNAL
	-- ========================================================
	
	UPDATE	PDR_JOURNAL
	SET		PDR_JOURNAL.STATUSID = (SELECT PDR_STATUS.STATUSID 
									FROM PDR_STATUS 
									WHERE PDR_STATUS.TITLE= 'Cancelled' )
	WHERE	PDR_JOURNAL.JOURNALID = @journalId

	-- ========================================================
	--	Insert New entry in PDR_Journal with status 'To Be Arranged'
	-- ========================================================
	DECLARE @MSATID INT,@statusId int
	Select @MSATID=MSATID FROM PDR_JOURNAL 	WHERE	PDR_JOURNAL.JOURNALID = @journalId

	SELECT @statusId=PDR_STATUS.STATUSID FROM PDR_STATUS WHERE PDR_STATUS.TITLE= 'To be Arranged'
	
	INSERT INTO PDR_JOURNAL(MSATID,STATUSID,CREATIONDATE,CREATEDBY)
	VALUES(@MSATID,@statusId,GETDATE(), @createdBy)
	

	
	END TRY
BEGIN CATCH 
    IF @@TRANCOUNT > 0
  BEGIN     
   ROLLBACK TRANSACTION;   
 SET @isCancelled = 0
    
    DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    -- Use RAISERROR inside the CATCH block to return 
    -- error information about the original error that 
    -- caused execution to jump to the CATCH block.
    RAISERROR (@ErrorMessage, -- Message text.
         @ErrorSeverity, -- Severity.
         @ErrorState -- State.
         );
  END
END CATCH;

IF @@TRANCOUNT > 0
 BEGIN  
  COMMIT TRANSACTION;  
  SET @isCancelled = 1
 END

END