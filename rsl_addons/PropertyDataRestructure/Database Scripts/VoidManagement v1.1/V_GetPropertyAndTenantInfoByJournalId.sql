
-- =============================================
-- Author:		<Author,ali raza>
-- Create date: <Create Date,19/08/2015>
-- Description:	<Description,get property and tenant Info to send notification from tenancy termination process>
-- =============================================
CREATE PROCEDURE V_GetPropertyAndTenantInfoByJournalId
	(@journalId int)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

 SELECT TenancyId,PropertyId,CustomerId from PDR_JOURNAL
 INNER JOIN PDR_MSAT ON PDR_JOURNAL.MSATID= PDR_MSAT.MSATId
 where JOURNALID=@journalId
END
GO
