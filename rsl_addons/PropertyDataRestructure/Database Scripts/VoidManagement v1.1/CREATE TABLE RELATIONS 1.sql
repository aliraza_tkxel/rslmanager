/*
   Thursday, May 7, 20156:42:16 PM
   User: sa
   Server: DEV-PC4
   Database: RSLBHALive
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.V_Status SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.V_Status', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.V_Status', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.V_Status', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.V_Stage SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.V_Stage', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.V_Stage', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.V_Stage', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.V_RoomList SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.V_RoomList', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.V_RoomList', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.V_RoomList', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.V_PaintPackDetails ADD CONSTRAINT
	FK_V_PaintPackDetails_V_Status FOREIGN KEY
	(
	StatusId
	) REFERENCES dbo.V_Status
	(
	StatusId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.V_PaintPackDetails SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.V_PaintPackDetails', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.V_PaintPackDetails', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.V_PaintPackDetails', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.V_BritishGasVoidNotification ADD CONSTRAINT
	FK_V_BritishGasVoidNotification_V_Stage FOREIGN KEY
	(
	StageId
	) REFERENCES dbo.V_Stage
	(
	StageId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.V_BritishGasVoidNotification ADD CONSTRAINT
	FK_V_BritishGasVoidNotification_V_Status FOREIGN KEY
	(
	StatusId
	) REFERENCES dbo.V_Status
	(
	StatusId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.V_BritishGasVoidNotification SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.V_BritishGasVoidNotification', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.V_BritishGasVoidNotification', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.V_BritishGasVoidNotification', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.PDR_JOURNAL SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.PDR_JOURNAL', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.PDR_JOURNAL', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.PDR_JOURNAL', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.V_AppointmentRecordedData ADD CONSTRAINT
	FK_V_AppointmentRecordedData_PDR_JOURNAL FOREIGN KEY
	(
	JournalId
	) REFERENCES dbo.PDR_JOURNAL
	(
	JOURNALID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.V_AppointmentRecordedData SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.V_AppointmentRecordedData', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.V_AppointmentRecordedData', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.V_AppointmentRecordedData', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.V_PaintPack ADD CONSTRAINT
	FK_V_PaintPack_PDR_JOURNAL FOREIGN KEY
	(
	JournalId
	) REFERENCES dbo.PDR_JOURNAL
	(
	JOURNALID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.V_PaintPack ADD CONSTRAINT
	FK_V_PaintPack_V_AppointmentRecordedData FOREIGN KEY
	(
	AppointmentRecordedDataId
	) REFERENCES dbo.V_AppointmentRecordedData
	(
	AppointmentRecordedDataId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.V_PaintPack ADD CONSTRAINT
	FK_V_PaintPack_V_RoomList FOREIGN KEY
	(
	RoomId
	) REFERENCES dbo.V_RoomList
	(
	RoomId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.V_PaintPack SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.V_PaintPack', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.V_PaintPack', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.V_PaintPack', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.V_RequiredWorks ADD CONSTRAINT
	FK_V_RequiredWorks_PDR_JOURNAL FOREIGN KEY
	(
	JournalId
	) REFERENCES dbo.PDR_JOURNAL
	(
	JOURNALID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.V_RequiredWorks ADD CONSTRAINT
	FK_V_RequiredWorks_V_AppointmentRecordedData FOREIGN KEY
	(
	AppointmentRecordedDataId
	) REFERENCES dbo.V_AppointmentRecordedData
	(
	AppointmentRecordedDataId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.V_RequiredWorks SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.V_RequiredWorks', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.V_RequiredWorks', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.V_RequiredWorks', 'Object', 'CONTROL') as Contr_Per 