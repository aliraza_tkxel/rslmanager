USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[V_GetChecksToBeArranged]    Script Date: 06/03/2015 14:36:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
/* =================================================================================    
    Page Description: Get First Inspections To Be Arranged List 
    Author: Ali Raza
    Creation Date: May-15-2015

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         May-15-2015      Ali Raza         Get First Inspections To Be Arranged List 
  =================================================================================*/
--  DECLARE	@totalCount int
--EXEC	 [dbo].[V_GetGasElectricChecksToBeArranged]
--		@searchText = NULL,
--		@checksRequired=1
--		@pageSize = 100,
--		@pageNumber = 1,
--		@totalCount = @totalCount OUTPUT
--SELECT	@totalCount as N'@totalCount'
-- =============================================
ALTER PROCEDURE [dbo].[V_GetGasElectricChecksToBeArranged]
-- Add the parameters for the stored procedure here
		@searchText VARCHAR(200)='',
		@checksRequired int = 1,
	--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'JournalId', 
		@sortOrder varchar (5) = 'DESC',
		@getOnlyCount bit=0,
		@totalCount int = 0 output	
AS
BEGIN
	DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(5000),
        
        --variables for paging
        @offset int,
		@limit int,
		@checksRequiredType varchar(200),
		@MSATTypeId int,
		@ArrangedStatusId int
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		
		SELECT  @ArrangedStatusId = PDR_STATUS.STATUSID FROM PDR_STATUS WHERE TITLE = 'To Be Arranged'

		--=====================Search Criteria===============================
		SET @searchCriteria = ' 1=1'
		
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND (ISNULL(P__PROPERTY.HouseNumber, '''')	+ ISNULL('' ''+P__PROPERTY.ADDRESS1, '''') 	+ ISNULL('', ''+P__PROPERTY.ADDRESS2, '''') LIKE ''%' + @searchText + '%'''
			SET @searchCriteria = @searchCriteria + CHAR(10) +' OR CONVERT(VARCHAR,Case When C__CUSTOMER.TITLE=6 then '''' Else  ISNULL(G_TITLE.[DESCRIPTION],'''')END)+ CONVERT(VARCHAR, ISNULL(C__CUSTOMER.FIRSTNAME,''''))+ CONVERT(VARCHAR, ISNULL(C__CUSTOMER.LASTNAME,'''')) LIKE ''%' + @searchText + '%'')'
		END	
		
		IF (@checksRequired = 1)
			BEGIN
				SET @searchCriteria = @searchCriteria + CHAR(10) +' AND IsGasCheckRequired = 1'
				SET @checksRequiredType='Gas Check'
				SELECT 	@MSATTypeId = MSATTypeId  FROM PDR_MSATType WHERE MSATTypeName = 'Void Gas Check'
			END
		ELSE IF (@checksRequired = 2)
			BEGIN
				SET @searchCriteria = @searchCriteria + CHAR(10) +' AND IsElectricCheckRequired = 1'
				SET @checksRequiredType='Electric Check'
				SELECT 	@MSATTypeId = MSATTypeId  FROM PDR_MSATType WHERE MSATTypeName = 'Void Electric Check'
			END
			
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND  PDR_MSAT.MSATTypeId= '+convert(varchar(10),@MSATTypeId)+' AND PDR_JOURNAL.STATUSID= '+convert(varchar(10),@ArrangedStatusId)+''

		--=======================Select Clause=============================================
		SET @SelectClause = 'Select top ('+convert(varchar(10),@limit)+')
							 ''JSV''+CONVERT(VARCHAR, RIGHT(''000000''+ CONVERT(VARCHAR,ISNULL(PDR_JOURNAL.JOURNALID,-1)),4)) AS Ref
		,ISNULL(T.HouseNumber, '''')	+ ISNULL('' ''+T.ADDRESS1, '''') 	+ ISNULL('', ''+T.ADDRESS2, '''') AS Address
		,	T.TOWNCITY,T.COUNTY,	ISNULL(T.POSTCODE, '''') AS Postcode,PDR_JOURNAL.JOURNALID as JournalId 
		, CONVERT(VARCHAR,Case When T.TITLE=6 then '''' Else  ISNULL(T.[DESCRIPTION],'''')END)+ CONVERT(VARCHAR, ISNULL(+'' ''+T.FIRSTNAME,''''))+ CONVERT(VARCHAR, ISNULL(+'' ''+T.LASTNAME,'''')) AS Tenant 
		,CONVERT(nvarchar(50),T.TerminationDate, 103) as Termination
		,CONVERT(nvarchar(50),rec.ReletDate, 103) as Relet,AppointmentRecordedDataId	
		,PDR_MSAT.TenancyId	,'''+@checksRequiredType+''' AS Type'
			
		
		
		--============================From Clause============================================
		SET @fromClause = CHAR(10) +'From V_AppointmentRecordedData rec
			INNER JOIN PDR_JOURNAL ON 	PDR_JOURNAL.JOURNALID = rec.GasCheckJournalId OR PDR_JOURNAL.JOURNALID=rec.ElectricCheckJournalId
			INNER JOIN	PDR_MSAT ON PDR_JOURNAL.MSATID = PDR_MSAT.MSATId
			INNER JOIN	PDR_MSATType ON PDR_MSAT.MSATTypeId = PDR_MSATType.MSATTypeId		
			INNER JOIN	PDR_STATUS ON PDR_JOURNAL.STATUSID = PDR_STATUS.STATUSID	

			INNER JOIN (Select J.JOURNALID,M.TerminationDate,M.MSATTypeId, J.STATUSID,P__PROPERTY.HouseNumber,P__PROPERTY.ADDRESS1 ,P__PROPERTY.ADDRESS2
			,P__PROPERTY.POSTCODE,P__PROPERTY.TOWNCITY,P__PROPERTY.COUNTY,C__CUSTOMER.TITLE,G_TITLE.[DESCRIPTION],C__CUSTOMER.FIRSTNAME,C__CUSTOMER.LASTNAME

			FROM PDR_JOURNAL J 
			INNER JOIN	PDR_MSAT M ON J.MSATID = M.MSATId
			LEFT JOIN	P__PROPERTY ON M.PropertyId = P__PROPERTY.PROPERTYID	
			INNER JOIN C_TENANCY ON M.TenancyId=	C_TENANCY.TENANCYID
			INNER JOIN C__CUSTOMER ON M.CustomerId = C__CUSTOMER.CUSTOMERID
			INNER JOIN C_CUSTOMERTENANCY on C__CUSTOMER.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID and C_TENANCY.TENANCYID=C_CUSTOMERTENANCY.TENANCYID
			LEFT JOIN G_TITLE ON C__CUSTOMER.TITLE=G_TITLE.TITLEID
			) AS T ON rec.InspectionJournalId = T.JOURNALID
		'
							
		--============================Order Clause==========================================
		IF(@sortColumn = 'Ref')
		BEGIN
			SET @sortColumn = CHAR(10)+ ' PDR_JOURNAL.JOURNALID' 	
			
		END
		
		IF(@sortColumn = 'Address')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Address' 	
			
		END
		IF(@sortColumn = 'Tenant')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Tenant' 	
			
		END
		
		IF(@sortColumn = 'Termination')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Termination' 	
			
		END
		
		IF(@sortColumn = 'Relet')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Relet' 	
			
		END
				
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		--===============================Main Query ====================================
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
		IF(@getOnlyCount=0)
		BEGIN
			print(@finalQuery)
			EXEC (@finalQuery)
		END
		
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
END
