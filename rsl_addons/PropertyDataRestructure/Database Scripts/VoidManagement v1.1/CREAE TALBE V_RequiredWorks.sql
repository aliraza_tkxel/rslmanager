USE [RSLBHALive]
GO

/****** Object:  Table [dbo].[V_RequiredWorks]    Script Date: 06/23/2015 13:21:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[V_RequiredWorks](
	[RequiredWorksId] [int] IDENTITY(1,1) NOT NULL,
	[InspectionJournalId] [int] NULL,
	[IsMajorWorksRequired] [bit] NULL,
	[RoomId] [int] NULL,
	[OtherLocation] [varchar](100) NULL,
	[WorkDescription] [ntext] NULL,
	[VoidWorksNotes] [ntext] NULL,
	[TenantNeglectEstimation] [money] NULL,
	[ComponentId] [int] NULL,
	[ReplacementDue] [date] NULL,
	[Condition] [varchar](100) NULL,
	[WorksJournalId] [int] NULL,
	[IsTenantWorks] [bit] NULL,
	[IsBrsWorks] [bit] NULL,
	[StatusId] [int] NULL,
	[IsScheduled] [bit] NULL,
	[CreatedDate] [datetime] NULL,
	[ModifiedDate] [datetime] NULL,
	[IsCanceled] [bit] NULL,
 CONSTRAINT [PK_V_RequiredWorks] PRIMARY KEY CLUSTERED 
(
	[RequiredWorksId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[V_RequiredWorks]  WITH CHECK ADD  CONSTRAINT [FK_V_RequiredWorks_PDR_JOURNAL] FOREIGN KEY([InspectionJournalId])
REFERENCES [dbo].[PDR_JOURNAL] ([JOURNALID])
GO

ALTER TABLE [dbo].[V_RequiredWorks] CHECK CONSTRAINT [FK_V_RequiredWorks_PDR_JOURNAL]
GO

ALTER TABLE [dbo].[V_RequiredWorks]  WITH CHECK ADD  CONSTRAINT [FK_V_RequiredWorks_PDR_JOURNAL1] FOREIGN KEY([WorksJournalId])
REFERENCES [dbo].[PDR_JOURNAL] ([JOURNALID])
GO

ALTER TABLE [dbo].[V_RequiredWorks] CHECK CONSTRAINT [FK_V_RequiredWorks_PDR_JOURNAL1]
GO


