/*
   Monday, May 18, 20157:33:00 PM
   User: sa
   Server: DEV-PC4
   Database: RSLBHALive
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.V_BritishGasVoidNotification
	DROP CONSTRAINT FK_V_BritishGasVoidNotification_V_Status
GO
ALTER TABLE dbo.V_Status SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.V_Status', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.V_Status', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.V_Status', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
ALTER TABLE dbo.V_BritishGasVoidNotification
	DROP CONSTRAINT FK_V_BritishGasVoidNotification_V_Stage
GO
ALTER TABLE dbo.V_Stage SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.V_Stage', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.V_Stage', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.V_Stage', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
GO
CREATE TABLE dbo.Tmp_V_BritishGasVoidNotification
	(
	BritishGasVoidNotificationId int NOT NULL IDENTITY (1, 1),
	CustomerId int NULL,
	PropertyId nvarchar(20) NULL,
	TenancyId int NULL,
	ArrangeGasElectricCheck bit NULL,
	GasElectricCheckJournalId int NULL,
	TenantFwAddress1 varchar(50) NULL,
	TenantFwAddress2 varchar(50) NULL,
	TenantFwCity varchar(50) NULL,
	TenantFwPostCode varchar(20) NULL,
	DateOccupancyCease date NULL,
	BritishGasEmail varchar(100) NULL,
	GasMeterTypeId int NULL,
	GasMeterReading int NULL,
	GasMeterReadingDate date NULL,
	ElectricMeterTypeId int NULL,
	ElectricMeterReading int NULL,
	ElectricMeterReadingDate date NULL,
	GasDebtAmount money NULL,
	GasDebtAmountDate date NULL,
	ElectricDebtAmount money NULL,
	ElectricDebtAmountDate date NULL,
	NewTenantName varchar(50) NULL,
	NewTenantDateOfBirth date NULL,
	NewTenantTel varchar(30) NULL,
	NewTenantMobile varchar(30) NULL,
	NewTenantOccupancyDate date NULL,
	NewTenantPreviousAddress1 varchar(50) NULL,
	NewTenantPreviousAddress2 varchar(50) NULL,
	NewTenantPreviousTownCity varchar(50) NULL,
	NewTenantPreviousPostCode varchar(20) NULL,
	NewMeterReading int NULL,
	NewGasMeterReadingDate date NULL,
	NewElectricMeterReading int NULL,
	NewElectricMeterReadingDate date NULL,
	StatusId int NULL,
	StageId int NULL,
	DocumentName varchar(50) NULL,
	CreatedDate datetime NULL,
	ModifiedDate datetime NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Tmp_V_BritishGasVoidNotification SET (LOCK_ESCALATION = TABLE)
GO
SET IDENTITY_INSERT dbo.Tmp_V_BritishGasVoidNotification ON
GO
IF EXISTS(SELECT * FROM dbo.V_BritishGasVoidNotification)
	 EXEC('INSERT INTO dbo.Tmp_V_BritishGasVoidNotification (BritishGasVoidNotificationId, ArrangeGasElectricCheck, GasElectricCheckJournalId, TenantFwAddress1, TenantFwAddress2, TenantFwCity, TenantFwPostCode, DateOccupancyCease, BritishGasEmail, GasMeterTypeId, GasMeterReading, GasMeterReadingDate, ElectricMeterTypeId, ElectricMeterReading, ElectricMeterReadingDate, GasDebtAmount, GasDebtAmountDate, ElectricDebtAmount, ElectricDebtAmountDate, NewTenantName, NewTenantDateOfBirth, NewTenantTel, NewTenantMobile, NewTenantOccupancyDate, NewTenantPreviousAddress1, NewTenantPreviousAddress2, NewTenantPreviousTownCity, NewTenantPreviousPostCode, NewMeterReading, NewGasMeterReadingDate, NewElectricMeterReading, NewElectricMeterReadingDate, StatusId, StageId, DocumentName, CreatedDate, ModifiedDate)
		SELECT BritishGasVoidNotificationId, ArrangeGasElectricCheck, GasElectricCheckJournalId, TenantFwAddress1, TenantFwAddress2, TenantFwCity, TenantFwPostCode, DateOccupancyCease, BritishGasEmail, GasMeterTypeId, GasMeterReading, GasMeterReadingDate, ElectricMeterTypeId, ElectricMeterReading, ElectricMeterReadingDate, GasDebtAmount, GasDebtAmountDate, ElectricDebtAmount, ElectricDebtAmountDate, NewTenantName, NewTenantDateOfBirth, NewTenantTel, NewTenantMobile, NewTenantOccupancyDate, NewTenantPreviousAddress1, NewTenantPreviousAddress2, NewTenantPreviousTownCity, NewTenantPreviousPostCode, NewMeterReading, NewGasMeterReadingDate, NewElectricMeterReading, NewElectricMeterReadingDate, StatusId, StageId, DocumentName, CreatedDate, ModifiedDate FROM dbo.V_BritishGasVoidNotification WITH (HOLDLOCK TABLOCKX)')
GO
SET IDENTITY_INSERT dbo.Tmp_V_BritishGasVoidNotification OFF
GO
DROP TABLE dbo.V_BritishGasVoidNotification
GO
EXECUTE sp_rename N'dbo.Tmp_V_BritishGasVoidNotification', N'V_BritishGasVoidNotification', 'OBJECT' 
GO
ALTER TABLE dbo.V_BritishGasVoidNotification ADD CONSTRAINT
	PK_V_BritishGasVoidNotification PRIMARY KEY CLUSTERED 
	(
	BritishGasVoidNotificationId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.V_BritishGasVoidNotification ADD CONSTRAINT
	FK_V_BritishGasVoidNotification_V_Stage FOREIGN KEY
	(
	StageId
	) REFERENCES dbo.V_Stage
	(
	StageId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
ALTER TABLE dbo.V_BritishGasVoidNotification ADD CONSTRAINT
	FK_V_BritishGasVoidNotification_V_Status FOREIGN KEY
	(
	StatusId
	) REFERENCES dbo.V_Status
	(
	StatusId
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
GO
COMMIT
select Has_Perms_By_Name(N'dbo.V_BritishGasVoidNotification', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.V_BritishGasVoidNotification', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.V_BritishGasVoidNotification', 'Object', 'CONTROL') as Contr_Per 