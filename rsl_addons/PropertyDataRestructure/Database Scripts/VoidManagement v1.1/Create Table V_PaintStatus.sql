/*
   Thursday, June 4, 201512:59:41 PM
   User: sa
   Server: dev-pc4
   Database: RSLBHALive
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.V_PaintStatus
	(
	StatusId int NOT NULL,
	Title nvarchar(200) NULL,
	IsActive bit NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.V_PaintStatus ADD CONSTRAINT
	PK_V_PaintStatus PRIMARY KEY CLUSTERED 
	(
	StatusId
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

GO
ALTER TABLE dbo.V_PaintStatus SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
select Has_Perms_By_Name(N'dbo.V_PaintStatus', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.V_PaintStatus', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.V_PaintStatus', 'Object', 'CONTROL') as Contr_Per 