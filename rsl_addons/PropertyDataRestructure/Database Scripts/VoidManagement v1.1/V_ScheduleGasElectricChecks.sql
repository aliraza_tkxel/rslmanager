USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[V_ScheduleVoidRequiredWorks]    Script Date: 06/01/2015 16:13:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
  Page Description:     Schedule Gas Electric Checks

  Author: Ali Raza
  Creation Date: June-15-2015

  Change History:

  Version      Date             By                      Description
  =======     ============    ========           ===========================
  v1.0         June-15-2015      Ali Raza        Schedule Gas Electric Checks
  Execution Command:
  
    
-- ============================================= */   
ALTER PROCEDURE [dbo].[V_ScheduleGasElectricChecks]    
 -- Add the parameters for the stored procedure here    
 @userId int 
,@appointmentNotes varchar(1000)  
,@appointmentStartDate date    
,@appointmentEndDate date    
,@startTime varchar(10)    
,@endTime varchar(10)    
,@operativeId int         
,@journalId INT    
,@duration float  
,@isSaved int = 0 out    
,@appointmentIdOut int = -1 out 
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
             
DECLARE     
    
@ArrangedId int    
,@JournalHistoryId int    
,@AppointmentId int    
,@AppointmentHistoryId int    
    
    
BEGIN TRANSACTION;    
BEGIN TRY    

-- =============================================    
-- Get status id of "Arranged"    
-- =============================================     
	SELECT  @ArrangedId = PDR_STATUS.statusid 
	FROM	PDR_STATUS 
	WHERE	PDR_STATUS.title ='Arranged'    
-- ====================================================================================================    
--          UPDATE (PDR_JOURNAL)    
-- ====================================================================================================    

     
 UPDATE [PDR_JOURNAL]
 SET	STATUSID = @ArrangedId
	    ,CREATEDBY = @userId
	    ,CREATIONDATE = GETDATE()	    
 WHERE	JOURNALID = @journalId
     
 PRINT 'JOURNALID = '+ CONVERT(VARCHAR,@journalId )    
-- ====================================================================================================    
--          INSERTION (PDR_APPOINTMENTS)    
-- ====================================================================================================    
    
	SELECT	@JournalHistoryId = MAX(JOURNALHISTORYID)    
	FROM	PDR_JOURNAL_HISTORY    
	WHERE	JOURNALID = @journalId    
      
 PRINT 'JOURNALHISTORYID = '+ CONVERT(VARCHAR,@JournalHistoryId)  
    
    
           
    INSERT INTO [PDR_APPOINTMENTS]
           (
            [JOURNALID]
           ,[JOURNALHISTORYID]
           ,[APPOINTMENTSTARTDATE]
           ,[APPOINTMENTENDDATE]
           ,[APPOINTMENTSTARTTIME]
           ,[APPOINTMENTENDTIME]
           ,[ASSIGNEDTO]
           ,[CREATEDBY]
           ,[LOGGEDDATE]
           ,[APPOINTMENTNOTES]           
           ,[APPOINTMENTSTATUS]
           ,[DURATION])
     VALUES
           (
            @journalId
           ,@JournalHistoryId
           ,@appointmentStartDate
           ,@appointmentEndDate
           ,@startTime
           ,@endTime
           ,@operativeId    
           ,@userId  
           ,GETDATE()
           ,@appointmentNotes           
           ,'NotStarted' 
           ,@duration)   
          
      SELECT @AppointmentId = SCOPE_IDENTITY()    
      PRINT 'APPOINTMENTID = '+ CONVERT(VARCHAR,@AppointmentId)            
     
	  SELECT @appointmentIdOut = @AppointmentId
END TRY    
BEGIN CATCH     
 IF @@TRANCOUNT > 0    
 BEGIN         
  ROLLBACK TRANSACTION;       
  SET @isSaved = 0            
 END    
 DECLARE @ErrorMessage NVARCHAR(4000);    
 DECLARE @ErrorSeverity INT;    
 DECLARE @ErrorState INT;    
    
 SELECT @ErrorMessage = ERROR_MESSAGE(),    
 @ErrorSeverity = ERROR_SEVERITY(),    
 @ErrorState = ERROR_STATE();    
    
 -- Use RAISERROR inside the CATCH block to return     
 -- error information about the original error that     
 -- caused execution to jump to the CATCH block.    
 RAISERROR (@ErrorMessage, -- Message text.    
    @ErrorSeverity, -- Severity.    
    @ErrorState -- State.    
   );    
END CATCH;    
    
IF @@TRANCOUNT > 0    
 BEGIN      
  COMMIT TRANSACTION;      
  SET @isSaved = 1    
 END    
  
    
END