

/* =================================================================================    
    Page Description:  Amend Paint packs details 
    Author: Ali Raza
    Creation Date: June-10-2015

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0        June-10-2015     Ali Raza		   Amend Paint packs details 
  =================================================================================*/
CREATE PROCEDURE V_AmendPaintPacks 
	-- Add the parameters for the stored procedure here
	(@paintPackDetailId INT,
	@paintRef nvarchar(50),
	@paintColor nvarchar(50),
	@paintName nvarchar(50)
	)
AS
BEGIN
	
	Update V_PaintPackDetails SET PaintRef=@paintRef,PaintColor=@paintColor,PaintName=@paintName
	
	Where PaintPackDetailId=@paintPackDetailId
END
GO
