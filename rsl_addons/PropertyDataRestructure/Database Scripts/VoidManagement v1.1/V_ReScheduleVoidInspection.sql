USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[V_ScheduleVoidInspection]    Script Date: 05/28/2015 16:50:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =================================================================================    
  Page Description:     Rescheduele Void Inspection Appointments.

  Author: Ali Raza
  Creation Date: June-15-2015

  Change History:

  Version      Date             By                      Description
  =======     ============    ========           ===========================
  v1.0         June-15-2015      Ali Raza           Rescheduele Void Inspection Appointments.
  Execution Command:
  
    
-- ============================================= */   
ALTER PROCEDURE [dbo].[V_ReScheduleVoidInspection]    
 -- Add the parameters for the stored procedure here    
 @userId int 
,@appointmentNotes varchar(1000)     
,@appointmentStartDate date    
,@appointmentEndDate date    
,@startTime varchar(10)    
,@endTime varchar(10)    
,@operativeId int         
,@journalId INT     
,@isSaved int = 0 out    

     
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
             
DECLARE 
@ArrangedId int    
,@JournalHistoryId int    
,@AppointmentHistoryId int    
,@appointmentId int
 
BEGIN TRANSACTION;    
BEGIN TRY    

-- =============================================    
-- Get status id of "Arranged"
-- =============================================     
	SELECT  @ArrangedId = PDR_STATUS.statusid 
	FROM	PDR_STATUS 
	WHERE	PDR_STATUS.title ='Arranged'   
	
	
-- ====================================================================================================    
--          Update PDR_JOURNAL    
-- ====================================================================================================    	
Update PDR_JOURNAL SET STATUSID = @ArrangedId ,CREATIONDATE=GETDATE(), CREATEDBY=@userId where JOURNALID = @journalId

-- ====================================================================================================    
--          Get Latest JournalHistoryId 
-- ====================================================================================================    
    
	SELECT	@JournalHistoryId = MAX(JOURNALHISTORYID)    
	FROM	PDR_JOURNAL_HISTORY    
	WHERE	JOURNALID = @journalId    
      
 PRINT 'JOURNALHISTORYID = '+ CONVERT(VARCHAR,@JournalHistoryId)  
    
-- ====================================================================================================    
--          Update PDR_Appointments    
-- ====================================================================================================  
   UPDATE PDR_APPOINTMENTS SET JOURNALHISTORYID=@JournalHistoryId,APPOINTMENTSTARTDATE=  @appointmentStartDate,APPOINTMENTENDDATE= @appointmentEndDate,
   APPOINTMENTSTARTTIME= @startTime,APPOINTMENTENDTIME= @endTime,ASSIGNEDTO=@operativeId,CREATEDBY= @userId,LOGGEDDATE=GETDATE(),
   APPOINTMENTNOTES= @appointmentNotes, APPOINTMENTSTATUS='NotStarted'
   Where JOURNALID =@journalId

Delete from V_AppointmentRecordedData where InspectionJournalId=@journalId
DELETE from V_RequiredWorks WHERE InspectionJournalId=@journalId

SELECT @appointmentId=APPOINTMENTID from PDR_APPOINTMENTS WHERE JOURNALID =@journalId 
       
  IF EXISTS(SELECT NoEntryId from V_NoEntry WHERE AppointmentId=@appointmentId)
	BEGIN
		UPDATE V_NoEntry SET IsNoEntryScheduled = 'TRUE' WHERE AppointmentId=@appointmentId
	END     
       
END TRY    
BEGIN CATCH     
 IF @@TRANCOUNT > 0    
 BEGIN         
  ROLLBACK TRANSACTION;       
  SET @isSaved = 0            
 END    
 DECLARE @ErrorMessage NVARCHAR(4000);    
 DECLARE @ErrorSeverity INT;    
 DECLARE @ErrorState INT;    
    
 SELECT @ErrorMessage = ERROR_MESSAGE(),    
 @ErrorSeverity = ERROR_SEVERITY(),    
 @ErrorState = ERROR_STATE();    
    
 -- Use RAISERROR inside the CATCH block to return     
 -- error information about the original error that     
 -- caused execution to jump to the CATCH block.    
 RAISERROR (@ErrorMessage, -- Message text.    
    @ErrorSeverity, -- Severity.    
    @ErrorState -- State.    
   );    
END CATCH;    
    
IF @@TRANCOUNT > 0    
 BEGIN      
  COMMIT TRANSACTION;      
  SET @isSaved = 1    
 END    
  
    
END