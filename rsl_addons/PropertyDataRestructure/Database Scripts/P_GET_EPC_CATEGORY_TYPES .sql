USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[P_GET_EPC_CATEGORY_TYPES]    Script Date: 12/29/2015 18:22:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
 =============================================
Exec P_GET_EPC_CATEGORY_TYPES 

Version			Author					Create date				Description						
-------			------					-----------				------------
1.0				Raja Aneeq				26/11/2015				Retrieve EPC  Category Types
			 
=============================================
 */
ALTER PROCEDURE [dbo].[P_GET_EPC_CATEGORY_TYPES]

AS
BEGIN
	
	SET NOCOUNT ON;
  
            SELECT  -1 AS CategoryTypeId ,
                    'Please select' AS CategoryType 
                    
            UNION
            SELECT  CategoryTypeId ,
                    CategoryType
          FROM    P_EpcCategory
            
	
        
	
	
END
