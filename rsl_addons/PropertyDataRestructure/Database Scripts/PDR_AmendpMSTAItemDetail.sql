USE [RSLBHALive] 

go 

/****** Object:  StoredProcedure [dbo].[PDR_AmendpMSTAItemDetail]    Script Date: 10/31/2015 15:51:36 ******/ 
SET ansi_nulls ON 

go 

SET quoted_identifier ON 

go 

/* =================================================================================      
  Page Description:     Get AmendpMSTA Item Detail For DropDown  

  Author: Ali Raza  
  Creation Date: Dec-30-2014  

  Change History:  

  Version      Date             By                      Description  
  =======     ============    ========           ===========================  
  v1.0         Dec-30-2014      Ali Raza           Get AmendpMSTA Item Detail For DropDown  
  v1.1     Jan-1-2015    Ahmed Mehmood    Added INSERT AND UPDATE query of PDR_JOURNAL table.
  Execution Command:  
    
  Exec PDR_AmendpMSTAItemDetail  
=================================================================================*/ 
ALTER PROCEDURE [dbo].[PDR_AmendpMSTAItemDetail] 
  -- Add the parameters for the stored procedure here              
  @PropertyId VARCHAR(100) = NULL, 
  @schemeId   INT = NULL, 
  @blockId    INT = NULL, 
  @ItemId     INT, 
  @UpdatedBy  INT, 
  @MSATDetail AS AS_MSATDETAIL readonly 
AS 
  BEGIN 
      -- SET NOCOUNT ON added to prevent extra result sets from              
      -- interfering with SELECT statements.              
      SET nocount ON; 

      --SELECT * FROM PDR_MSAT  
      ---Begin Iterate  @ItemDetail to insert or update data  in PA_PROPERTY_ATTRIBUTES            
      --=================================================================================          
      ---Variable to insert or update data in PA_PROPERTY_ATTRIBUTES              
      DECLARE @IsRequired           BIT, 
              @LastDate             DATETIME, 
              @Cycle                INT, 
              @CycleTypeId          INT, 
              @NextDate             DATETIME, 
              @AnnualApportionment  FLOAT, 
              @MSATTypeId           INT, 
              @MSATId               INT, 
              @ToBeArrangedStatusId INT, 
              @CompletedStatus      INT 
      ---Declare Cursor variable              
      DECLARE @ItemToInsertCursor CURSOR 

      SELECT @ToBeArrangedStatusId = pdr_status.statusid 
      FROM   pdr_status 
      WHERE  title = 'To be Arranged' 

      DECLARE @attributeTypeId INT 

      SELECT @attributeTypeId = pdr_attributestype.attributetypeid 
      FROM   pdr_attributestype 
      WHERE  pdr_attributestype.attributetype = 'Attribute' 

      SELECT @CompletedStatus = pdr_status.statusid 
      FROM   pdr_status 
      WHERE  title = 'Completed' 

      --Initialize cursor              
      SET @ItemToInsertCursor = CURSOR fast_forward 
      FOR SELECT [isrequired], 
                 [lastdate], 
                 [cycle], 
                 [cycletypeid], 
                 [nextdate], 
                 [annualapportionment], 
                 [msattypeid] 
          FROM   @MSATDetail; 

      --Open cursor              
      OPEN @ItemToInsertCursor 

      ---fetch row from cursor              
      FETCH next FROM @ItemToInsertCursor INTO @IsRequired, @LastDate, @Cycle, 
      @CycleTypeId, @NextDate, @AnnualApportionment, @MSATTypeId 

      ---Iterate cursor to get record row by row              
      WHILE @@FETCH_STATUS = 0 
        BEGIN 
            SET @MSATId = 0 

            SELECT @MSATId = msatid 
            FROM   pdr_msat 
            WHERE  ( propertyid = @PropertyId 
                      OR @PropertyId IS NULL ) 
                   AND ( schemeid = @schemeId 
                          OR @schemeId IS NULL ) 
                   AND ( blockid = @blockId 
                          OR @blockId IS NULL ) 
                   AND itemid = @ItemId 
                   AND msattypeid = @MSATTypeId 

            IF @LastDate = '1900-01-01 00:00:00.000' 
                OR @LastDate IS NULL 
              SET @LastDate = NULL 

            IF @NextDate = '1900-01-01 00:00:00.000' 
                OR @NextDate IS NULL 
              SET @NextDate = NULL 

            IF @MSATId > 0 
              BEGIN 
                  UPDATE pdr_msat 
                  SET    cycletypeid = @CycleTypeId, 
                         isrequired = @IsRequired, 
                         cycle = @Cycle, 
                         lastdate = @LastDate, 
                         nextdate = @NextDate, 
                         annualapportionment = @AnnualApportionment, 
                         isactive = 1 
                  WHERE  msatid = @MSATId 

                  IF @IsRequired = 1 
                    BEGIN 
                        IF EXISTS(SELECT 1 
                                  FROM   [pdr_journal] 
                                  WHERE  msatid = @MSATId) 
                          BEGIN 
                              UPDATE [pdr_journal] 
                              SET    [statusid] = @ToBeArrangedStatusId, 
                                     [creationdate] = Getdate(), 
                                     [createdby] = @UpdatedBy 
                              WHERE  msatid = @MSATId 
                                     AND statusid IN ( @ToBeArrangedStatusId, 
                                                       @CompletedStatus 
                                                     ) 
                          END 
                        ELSE 
                          BEGIN 
                              INSERT INTO [pdr_journal] 
                                          ([msatid], 
                                           [statusid], 
                                           [creationdate], 
                                           [createdby]) 
                              VALUES      (@MSATId, 
                                           @ToBeArrangedStatusId, 
                                           Getdate(), 
                                           @UpdatedBy) 
                          END 
                    END 
              END 
            ELSE 
              BEGIN 
                  INSERT INTO pdr_msat 
                              (isrequired, 
                               propertyid, 
                               itemid, 
                               lastdate, 
                               cycle, 
                               cycletypeid, 
                               nextdate, 
                               annualapportionment, 
                               msattypeid, 
                               isactive, 
                               schemeid, 
                               blockid, 
                               attributetypeid) 
                  VALUES      (@IsRequired, 
                               @PropertyId, 
                               @ItemId, 
                               @LastDate, 
                               @Cycle, 
                               @CycleTypeId, 
                               @NextDate, 
                               @AnnualApportionment, 
                               @MSATTypeId, 
                               1, 
                               @schemeId, 
                               @blockId, 
                               @attributeTypeId) 

                  SELECT @MSATId = Scope_identity() 

                  IF @IsRequired = 1 
                    BEGIN 
                        INSERT INTO [pdr_journal] 
                                    ([msatid], 
                                     [statusid], 
                                     [creationdate], 
                                     [createdby]) 
                        VALUES      (@MSATId, 
                                     @ToBeArrangedStatusId, 
                                     Getdate(), 
                                     @UpdatedBy) 
                    END 
              END 

            FETCH next FROM @ItemToInsertCursor INTO @IsRequired, @LastDate, 
            @Cycle, 
            @CycleTypeId, @NextDate, @AnnualApportionment, @MSATTypeId 
        END 

      --close & deallocate cursor                
      CLOSE @ItemToInsertCursor 

      DEALLOCATE @ItemToInsertCursor 
  END 