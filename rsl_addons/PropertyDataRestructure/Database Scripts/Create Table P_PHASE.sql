USE [RSLBHALive]
GO

/****** Object:  Table [dbo].[P_PHASE]    Script Date: 12/10/2014 11:14:50 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[P_PHASE](
	[PHASEID] [int] IDENTITY(1,1) NOT NULL,
	[ConstructionStart] [smalldatetime] NOT NULL,
	[AnticipatedCompletion] [smalldatetime] NOT NULL,
	[ActualCompletion] [smalldatetime] NOT NULL,
	[HandoverintoManagement] [smalldatetime] NOT NULL,
	[DEVELOPMENTID] [int] NULL,
	[PhaseName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_P_PHASE] PRIMARY KEY CLUSTERED 
(
	[PHASEID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


