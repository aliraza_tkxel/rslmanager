﻿using System;
using PropSurvey.Contracts.Fault;
using PropSurvey.Dal.Authentication;
using System.Data;
using PropSurvey.Utilities.Constants;
using PropSurvey.Utilities.ErrorSetter;
using PropSurvey.Contracts.Data;
using PropSurvey.Utilities.Helpers;
using PropSurvey.Dal.TestCase;



namespace PropSurvey.BuisnessLayer.Authentication
{
    public class AuthenticationBl
    {
        /// <summary>
        /// This function calls the dal layer to fetch the record against username and password
        /// </summary>
        /// <param name="userName">username of the surveyour</param>
        /// <param name="password">password of the surveyour</param>
        /// <returns>true or false</returns>
        //public DateTime? findUser(string userName, string password, string deviceToken)
        //{

        //    try
        //    {
        //        AuthenticationDal authDal = new AuthenticationDal();
        //        UserData usrData = new UserData();                                

        //        usrData.userId = authDal.findUser(userName, password, deviceToken);

        //        //if no user found then return false else ture
        //        if (usrData.userId == 0)
        //        {
        //            ErrorFaultSetGet.setErrorFault(MessageConstants.LoginSuccessMsg, true, ErrorCodesConstants.UserNamePasswordDoesNotExistCode);
        //            throw new ArgumentException(MessageConstants.LoginSuccessMsg, "username or password");
        //        }
        //        else
        //        {

        //            usrData = authDal.getLastLoggedInDate(usrData);


        //            if (usrData.lastLoggedInDate != null)
        //            {
        //                authDal.updateLastLoggedInDate(usrData);
        //            }
        //            else
        //            {
        //                usrData.lastLoggedInDate = DateTime.Now;
        //                authDal.insertLastLoggedInDate(usrData);                        
        //            }     

        //        }

        //        return usrData.lastLoggedInDate;


        //    }
        //    catch (EntityException entityexception)
        //    {
        //        ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);                               
        //        throw entityexception;
        //    }
        //    catch (Exception ex)
        //    {
        //        if (ErrorFault.isErrorOccured == true)
        //        {
        //            throw ex;
        //        }
        //        else
        //        {
        //            ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
        //            throw ex;
        //        }
        //    }            
        //}


        public UserData findUser(string userName, string password, string deviceToken)
        {

            try
            {
                AuthenticationDal authDal = new AuthenticationDal();
                UserData usrData = new UserData();

                usrData.userId = authDal.findUser(userName, password, deviceToken);
                string salt = string.Empty;
                //if no user found then return false else ture
                if (usrData.userId == 0)
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.LoginSuccessMsg, true, ErrorCodesConstants.UserNamePasswordDoesNotExistCode);
                    throw new ArgumentException(MessageConstants.LoginSuccessMsg, "username or password");
                }
                //else if (authDal.isUserLoggedIn(userName))
                //{
                //    //salt = "-9"; //User already logged in
                //    ErrorFaultSetGet.setErrorFault(MessageConstants.AlreadyLoggedInMsg, true, ErrorCodesConstants.UserNameAlreadyLoggedIn);
                //    throw new ArgumentException(MessageConstants.AlreadyLoggedInMsg, "Already loggedin");
                //}
                else
                {

                    usrData = authDal.getLastLoggedInDate(usrData);


                    if (usrData.lastLoggedInDate != null)
                    {
                        authDal.updateLastLoggedInDate(usrData);
                    }
                    else
                    {
                        usrData.lastLoggedInDate = DateTime.Now;
                        authDal.insertLastLoggedInDate(usrData);
                    }

                    salt = SecurityEncryption.GenerateSalt();
                    usrData.lastLoggedInDate = authDal.saveLoggedInUserSalt(userName, salt);
                    //usrData.OrgID = authDal.getUserOrgID(usrData);
                    usrData.salt = salt;
                    usrData.userName = SecurityEncryption.EncryptData(userName);

                    usrData.userId = authDal.getUserId(userName, password);
                    usrData.FullName = authDal.getUserFullName(userName, password);

                    usrData.isActive = 1;
                }

                return usrData;


            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == true)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
            }
        }


        #region log out user

        /// <summary>
        /// this fuction logouts the user
        /// </summary>
        /// <param name="username">username</param>
        /// <returns></returns>
        public void LogoutUser(string username)
        {

            try
            {
                AuthenticationDal authDal = new AuthenticationDal();
                authDal.LogoutUser(username);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == true)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
            }
        }
        #endregion

        #region check for Valid User session

        /// <summary>
        /// this fuction checks that the user session is a valid one
        /// </summary>
        /// <param name="username">username</param>
        /// <param name="salt">salt</param>
        /// <returns>true or false</returns>

        public bool checkForValidSession(string userName, string salt)
        {
            try
            {
                AuthenticationDal authDal = new AuthenticationDal();
                if (authDal.checkForValidSession(SecurityEncryption.DecryptData(userName), salt))
                {
                    return true;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.SessionExpiredMsg, true, ErrorCodesConstants.UserNameSessionExpired);
                    throw new ArgumentException(MessageConstants.SessionExpiredMsg, "Your session has expired.");
                }
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == true)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
            }



        }
        #endregion


        public string getSalt(string userName)
        {
            try
            {
                AuthenticationDal authDal = new AuthenticationDal();
                return authDal.getSalt(userName);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == true)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
            }
        }


        public int CheckUser(string Username, string Password)
        {
            TestCaseDal testDal = new TestCaseDal();
            return testDal.CheckUser(Username, Password);
        }

        //Change#16 - Behroz - 18/12/2012 - Start
        /// <summary>
        /// This function return the rights of tab
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        public bool GetGasTabRights(int userid)
        {
            try
            {
                AuthenticationDal authDal = new AuthenticationDal();
                return authDal.isUserExistRights(userid);

            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == false)
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
                else
                {
                    throw ex;
                }
            }
        }
        //Change#16 - Behroz - 18/12/2012 - End

    }
}
