﻿using System;
using System.Collections.Generic;
using System.Data;
using PropSurvey.Contracts.Data;
using PropSurvey.Contracts.Fault;
using PropSurvey.Dal.Appointment;
using PropSurvey.Dal.Authentication;
using PropSurvey.Dal.Customer;
using PropSurvey.Entities;
using PropSurvey.Utilities.Constants;
using PropSurvey.Utilities.ErrorSetter;
using PropSurvey.Dal.Property;
using PropSurvey.Utilities.Helpers;
using PropSurvey.Dal.Appliances;

namespace PropSurvey.BuisnessLayer.Appliances
{
    public class AppliancesBl
    {
        #region get All Appliances
        /// <summary>
        /// This function returns all the appliances
        /// </summary>
        /// <returns>List of appliances data objects</returns>
        public List<ApplianceData> getAllAppliancesByPropertyID(string propertyID, int appointmentID)
        {
            try
            {
                AppliancesDal appDal = new AppliancesDal();
                return appDal.getAllAppliancesByPropertyID(propertyID, appointmentID);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }

        //Change#31 - Behroz - 12/07/2012 - Start
        //Code modified by Abdul Wahhab - 22/05/2013 - START
        /// <summary>
        /// This function returns all the appliances for GAS
        /// </summary>
        /// <returns>List of appliances data objects</returns>
        public GasApplianceListData getAllAppliancesByPropertyIDGas(string propertyID, int journalId)
        {
            try
            {
                AppliancesDal appDal = new AppliancesDal();
                return appDal.getAllAppliancesByPropertyIDGas(propertyID, journalId);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        //Code modified by Abdul Wahhab - 22/05/2013 - END
        //Change#31 - Behroz - 12/07/2012 - End
        #endregion

        #region get All Appliances Type
        /// <summary>
        /// This function returns all the Appliances Type
        /// </summary>
        /// <returns>List of Appliances Type data objects</returns>
        public List<ApplianceTypeData> getAllApplianceTypes()
        {
            try
            {
                AppliancesDal appDal = new AppliancesDal();
                return appDal.getAllApplianceTypes();
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        #region get All Appliances Models
        /// <summary>
        /// This function returns all the Appliances Model
        /// </summary>
        /// <returns>List of Appliances Model data objects</returns>
        public List<ApplianceModelData> getAllApplianceModel()
        {
            try
            {
                AppliancesDal appDal = new AppliancesDal();
                return appDal.getAllApplianceModel();
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        #region get All Appliances Manufacturer
        /// <summary>
        /// This function returns all the Appliances Manufacturer
        /// </summary>
        /// <returns>List of Appliances Manufacturer data objects</returns>
        public List<ManufacturerData> getAllApplianceManufacturer()
        {
            try
            {
                AppliancesDal appDal = new AppliancesDal();
                return appDal.getAllApplianceManufacturer();
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        #region get All Appliances Locations
        /// <summary>
        /// This function returns all the Appliances Location
        /// </summary>
        /// <returns>List of Appliances Location data objects</returns>
        public List<AppliancesLocationData> getAllApplianceLocations()
        {
            try
            {
                AppliancesDal appDal = new AppliancesDal();
                return appDal.getAllApplianceLocations();
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        #region Save a new Appliance
        /// <summary>
        /// This function saves a new appliance
        /// </summary>
        /// <param name="appData">appliances data objects</param>
        /// <returns>ID in case of success</returns>
        public int saveAppliance(ApplianceData appData)
        {
            try
            {
                AppliancesDal appDal = new AppliancesDal();
                return appDal.saveAppliance(appData);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        #region Update a existing Appliance
        /// <summary>
        /// This function updates a existing appliance
        /// </summary>
        /// <param name="appData">appliances data objects</param>
        /// <returns>ID in case of success</returns>
        public bool updateAppliance(ApplianceData appData)
        {
            try
            {
                AppliancesDal appDal = new AppliancesDal();
                return appDal.updateAppliance(appData);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        #region save new Appliances Locations
        /// <summary>
        /// This function save new Appliances Location
        /// </summary>
        /// <returns>true for success</returns>
        public int saveApplianceLocation(AppliancesLocationData location)
        {
            try
            {
                AppliancesDal appDal = new AppliancesDal();
                return appDal.saveApplianceLocation(location);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        #region save new Appliances Type
        /// <summary>
        /// This function save new Appliances Type
        /// </summary>
        /// <returns>new inserted id in case of success</returns>
        public int saveApplianceType(ApplianceTypeData type)
        {
            try
            {
                AppliancesDal appDal = new AppliancesDal();
                return appDal.saveApplianceType(type);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        #region save new Appliances Model
        /// <summary>
        /// This function save new Appliances Model
        /// </summary>
        /// <returns>new inserted id in case of success</returns>
        public int saveApplianceModel(ApplianceModelData model)
        {
            try
            {
                AppliancesDal appDal = new AppliancesDal();
                return appDal.saveApplianceModel(model);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        #region save new Appliances manufacturer
        /// <summary>
        /// This function save new Appliances manufacturer
        /// </summary>
        /// <returns>new inserted id in case of success</returns>
        public int saveApplianceManufacturer(ManufacturerData manufacture)
        {
            try
            {
                AppliancesDal appDal = new AppliancesDal();
                return appDal.saveApplianceManufacturer(manufacture);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion


        #region get number of inspected Appliances

        /// <summary>
        /// This function returns number of inspected Appliances
        /// </summary>
        /// <returns>number of inspected Appliances</returns>
        public int getInspectedAppliances(int appointmentID)
        {
            try
            {
                AppliancesDal appDal = new AppliancesDal();
                return appDal.getInspectedAppliances(appointmentID);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }

        //Change#25 - Behroz - 20/12/2012 - Start
        /// <summary>
        /// This function returns number of inspected Appliances for gas
        /// </summary>
        /// <returns>number of inspected Appliances</returns>
        public int getInspectedAppliancesGas(int journalId)
        {
            try
            {
                AppliancesDal appDal = new AppliancesDal();
                return appDal.getInspectedAppliancesGas(journalId);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        //Change#25 - Behroz - 20/12/2012 - End

        #endregion

        #region get ApplianceInspection by ApplianceID
        /// <summary>
        /// This function returns all the ApplianceInspection
        /// </summary>
        /// <returns>List of ApplianceInspection data objects</returns>
        public ApplianceInspectionData getApplianceInspectionByApplianceID(int ApplianceID, int appointmentID)
        {
            try
            {
                AppliancesDal appDal = new AppliancesDal();
                return appDal.getApplianceInspectionByApplianceID(ApplianceID, appointmentID);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }

        //Change#26 - Behroz - 20/12/2012 - Start
        /// <summary>
        /// This function returns all the ApplianceInspection for gas
        /// </summary>
        /// <returns>List of ApplianceInspection data objects</returns>
        public ApplianceInspectionData getApplianceInspectionByApplianceIDGas(int ApplianceID, int journalId)
        {
            try
            {
                AppliancesDal appDal = new AppliancesDal();
                return appDal.getApplianceInspectionByApplianceIDGas(ApplianceID, journalId);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        //Change#26 - Behroz - 20/12/2012 - End
        #endregion

        #region get ApplianceInspection details by Appointment Id
        /// <summary>
        /// This function returns all the ApplianceInspection details
        /// </summary>
        /// <returns>List of ApplianceInspection data objects</returns>
        public List<ApplianceInspectionData> getApplianceInspectionDetails(int appointmentID)
        {
            try
            {
                AppliancesDal appDal = new AppliancesDal();
                return appDal.getApplianceInspectionDetails(appointmentID);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }

        //Change#40 - Behroz - 26/12/2012 - Start
        /// <summary>
        /// This function returns all the ApplianceInspection details for gas
        /// </summary>
        /// <returns>List of ApplianceInspection data objects</returns>
        public List<ApplianceInspectionData> getApplianceInspectionDetailsGas(int journalId)
        {
            try
            {
                AppliancesDal appDal = new AppliancesDal();
                return appDal.getApplianceInspectionDetailsGas(journalId);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        //Change#40 - Behroz - 26/12/2012 - End
        #endregion

        #region Save a new ApplianceInspection
        /// <summary>
        /// This function saves a new ApplianceInspection
        /// </summary>
        /// <param name="appData">ApplianceInspection data objects</param>
        /// <returns>ID in case of success</returns>
        public int saveApplianceInspection(ApplianceInspectionData appData)
        {
            try
            {
                AppliancesDal appDal = new AppliancesDal();
                return appDal.saveApplianceInspection(appData);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }

        //Change#24 - Behroz - 20/12/2012 - Start
        /// <summary>
        /// This function saves a new ApplianceInspection for gas
        /// </summary>
        /// <param name="appData">ApplianceInspection data objects</param>
        /// <returns>ID in case of success</returns>
        public int saveApplianceInspectionGas(ApplianceInspectionData appData)
        {
            try
            {
                AppliancesDal appDal = new AppliancesDal();
                return appDal.saveApplianceInspectionGas(appData);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        //Change#24 - Behroz - 20/12/2012 - End
        #endregion

        #region Update a existing ApplianceInspection
        /// <summary>
        /// This function updates a existing ApplianceInspection
        /// </summary>
        /// <param name="appData">ApplianceInspection data objects</param>
        /// <returns>ID in case of success</returns>
        public bool updateApplianceInspection(ApplianceInspectionData appData)
        {
            try
            {
                AppliancesDal appDal = new AppliancesDal();
                return appDal.updateApplianceInspection(appData);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        #region "Get Appliance Count"
        //Change#50 - Behroz - 16/01/2013 - Start
        /// <summary>
        /// This function returns the application count
        /// </summary>
        /// <param name="propertyId"></param>
        /// <returns></returns>
        public int getApplianceCount(string propertyId)
        {
            try
            {
                AppliancesDal appDal = new AppliancesDal();
                return appDal.getApplianceCount(propertyId);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        //Change#50 - Behroz - 16/01/2013 - End
        #endregion

        #region Update Detector Count
        //Code added by Abdul Wahhab - 23/05/2013 - START
        /// <summary>
        /// This function updates detector count against a property
        /// </summary>
        /// <param name="propertyId"></param>
        /// <param name="detectorCountData">DetectorCountData object</param>
        /// <returns>True in case of success</returns>        
        public ResultBoolData updateDetectorCount(string propertyId, DetectorCountData detectorCountData)
        {
            try
            {
                AppliancesDal appDal = new AppliancesDal();
                return appDal.updateDetectorCount(propertyId, detectorCountData);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        //Code added by Abdul Wahhab - 23/05/2013 - END
        #endregion

        #region Add Update Detector Info
        //Code added by Abdul Wahhab - 23/05/2013 - START
        /// <summary>
        /// This function adds or updates detector info
        /// </summary>
        /// <param name="detectorType"></param>
        /// <param name="detectorInspectionData">DetectorInspectionData object</param>
        /// <returns>ID in case of success</returns>        
        public ResultIntData addUpdateDetectorInfo(int detectorType, DetectorInspectionData detectorInspectionData)
        {
            try
            {
                AppliancesDal appDal = new AppliancesDal();
                return appDal.addUpdateDetectorInfo(detectorType, detectorInspectionData);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        //Code added by Abdul Wahhab - 23/05/2013 - END
        #endregion
    }
}
