﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PropSurvey.Contracts.Data;
using PropSurvey.Dal.InstallationPipework;
using System.Data;
using PropSurvey.Utilities.Constants;
using PropSurvey.Contracts.Fault;
using PropSurvey.Utilities.ErrorSetter;

namespace PropSurvey.BuisnessLayer.InstallationPipework
{
    public class InstallationPipeworkBl
    {

        #region get InstallationPipework Data
        /// <summary>
        /// This function returns the InstallationPipework data
        /// </summary>
        /// <returns>List of InstallationPipework data objects</returns>

        public InstallationPipeworkData getInstallationPipeworkFormData(int AppointmentID)
        {
            try
            {
                InstallationPipeworkDal insDal = new InstallationPipeworkDal();
                return insDal.getInstallationPipeworkFormData(AppointmentID);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }

        //Change#12 - Behroz - 12/14/2012 - Start
        /// <summary>
        /// This function returns the InstallationPipework data
        /// </summary>
        /// <returns>List of InstallationPipework data objects</returns>
        public InstallationPipeworkData getInstallationPipeworkFormDataGas(int journalID)
        {
            try
            {
                InstallationPipeworkDal insDal = new InstallationPipeworkDal();
                //return insDal.getInstallationPipeworkFormDataGas(PropertyID);
                return insDal.getInstallationPipeworkFormDataGas(journalID);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        //Change#12 - Behroz - 12/14/2012 - End
        #endregion

        #region save InstallationPipework Data
        /// <summary>
        /// This function saves the InstallationPipework Data in the database
        /// </summary>
        /// <param name="appData">The object of InstallationPipework Data</param>
        /// <returns>the InstallationPipework id if successfully save otherwise 0 </returns>

        public int saveInstallationPipework(InstallationPipeworkData workData)
        {
            try
            {
                InstallationPipeworkDal insDal = new InstallationPipeworkDal();
                if (insDal.checkAppointmentID(workData.AppointmentID))
                {
                    return insDal.saveInstallationPipework(workData);
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.AppointmentIDInvalid, true, ErrorCodesConstants.AppointmentIDInvalid);
                    throw new ArgumentException(MessageConstants.AppointmentIDInvalid);
                }

            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
            }
        }

        //Change#12 - Behroz - 12/13/2012 - Start
        /// <summary>
        /// This function saves the InstallationPipework Data in the database for gas
        /// </summary>
        /// <param name="appData">The object of InstallationPipework Data</param>
        /// <returns>the InstallationPipework id if successfully save otherwise 0 </returns>
        public int saveInstallationPipeworkGas(InstallationPipeworkData workData)
        {
            try
            {
                InstallationPipeworkDal insDal = new InstallationPipeworkDal();
                return insDal.saveInstallationPipeworkGas(workData);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
            }
        }
        //Change#12 - Behroz - 12/13/2012 - End
        #endregion

        #region update InstallationPipework Data
        /// <summary>
        /// This function updates theInstallationPipework Data in the database
        /// </summary>
        /// <param name="appData">The object of InstallationPipework Data</param>
        /// <returns>true if successfully save otherwise 0 </returns>

        public bool updateInstallationPipeworkData(InstallationPipeworkData insData)
        {
            try
            {
                InstallationPipeworkDal insDal = new InstallationPipeworkDal();
                if (insDal.checkAppointmentID(insData.AppointmentID))
                {
                    return insDal.updateInstallationPipeworkData(insData);    
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.AppointmentIDInvalid, true, ErrorCodesConstants.AppointmentIDInvalid);
                    throw new ArgumentException(MessageConstants.AppointmentIDInvalid);
                }
                
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                    throw ex;
                }   
            }
        }

        #endregion

    }
}
