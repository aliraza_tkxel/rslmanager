﻿using System;
using System.Collections.Generic;
using System.Data;
using PropSurvey.Contracts.Data;
using PropSurvey.Contracts.Fault;
using PropSurvey.Dal.Property;
using PropSurvey.Utilities.Constants;
using PropSurvey.Utilities.ErrorSetter;

namespace PropSurvey.BuisnessLayer.Property
{
    public class PropertyBl
    {
        #region find Property
        /// <summary>
        /// This function returns the property against specified parameters
        /// </summary>
        /// <param name="skip">how many records client application want to skip. e.g 10,20</param>
        /// <param name="top">how many records client application wants to get</param>
        /// <param name="reference">reference number </param>
        /// <param name="houseNumber">house number of customer</param>
        /// <param name="street">street of customer</param>
        /// <param name="postCode">post code of customer</param>
        /// <param name="surName">sur name of customer</param>
        /// <returns>It retruns the list of customer data's object</returns>
        public List<CustomerData> findProperty(string skip, string top, string reference = "", string houseNumber = "", string street = "", string postCode = "", string surName = "")
        {
            try
            {
                PropertyDal propDal = new PropertyDal();
                List<CustomerData> custData = new List<CustomerData>();               
                
                //convert skip to integer
                int skipNumber = 0;
                int topNumber = 0;

                skipNumber = Int32.Parse(skip);
                topNumber = Int32.Parse(top);

                custData = propDal.findProperty(skipNumber, topNumber, reference, houseNumber, street, postCode, surName);

                return custData;
            }
            catch (FormatException formatException)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.SkipTopInvalidFromtMsg, true, ErrorCodesConstants.IntegerToStringConversionExceptionCode);
                throw formatException;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }

        //Change - Behroz - 12/12/2012 - Start
        /// <summary>
        /// This function returns the property against specified parameters
        /// </summary>
        /// <param name="skip">how many records client application want to skip. e.g 10,20</param>
        /// <param name="top">how many records client application wants to get</param>
        /// <param name="reference">reference number </param>
        /// <param name="houseNumber">house number of customer</param>
        /// <param name="street">street of customer</param>
        /// <param name="postCode">post code of customer</param>
        /// <param name="surName">sur name of customer</param>
        /// <returns>It retruns the list of customer data's object</returns>
        public List<CustomerData> findPropertyGas(string skip, string top, string reference = "", string houseNumber = "", string street = "", string postCode = "", string surName = "")
        {
            try
            {
                PropertyDal propDal = new PropertyDal();
                List<CustomerData> custData = new List<CustomerData>();

                //convert skip to integer
                int skipNumber = 0;
                int topNumber = 0;

                skipNumber = Int32.Parse(skip);
                topNumber = Int32.Parse(top);

                custData = propDal.findPropertyGas(skipNumber, topNumber, reference, houseNumber, street, postCode, surName);

                return custData;
            }
            catch (FormatException formatException)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.SkipTopInvalidFromtMsg, true, ErrorCodesConstants.IntegerToStringConversionExceptionCode);
                throw formatException;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        //Change - Behroz - 12/12/2012 - End
        #endregion

        #region get Property Images

        /// <summary>
        /// This function returns the list of images which were previously saved against property.
        /// </summary>
        /// <param name="propertyId">property id</param>
        /// <returns>returns the list of image names against property</returns>

        public List<PropertyPictureData> getPropertyImages(string propertyId, int itemId)
        {
            try
            {
                PropertyDal propDal = new PropertyDal();
                List<PropertyPictureData> propData = new List<PropertyPictureData>();

                propData = propDal.getPropertyImages(propertyId, itemId);

                return propData;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion 

        #region get Default Property Image Name

        /// <summary>
        /// This function returns the default image name which was saved against property
        /// </summary>
        /// <param name="propertyId">property id</param>
        /// <returns>returns the name of the image</returns>

        public string getDefaultPropertyImageName(string propertyId)
        {
            try
            {
                PropertyDal propDal = new PropertyDal();
                return propDal.getDefaultPropertyImageName(propertyId);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        #region save Property Image

        /// <summary>
        /// This funciton updates the property image in the database( if there is already some appointment in the database against that property)
        /// </summary>
        /// <param name="propPicData">property picture data</param>        
        /// <returns>true or false upon successfull saving on disk and on db</returns>
        
        public bool savePropertyImage(PropertyPictureData propPicData)
        {
            try
            {
                PropertyDal propDal = new PropertyDal();
                return propDal.savePropertyImage(propPicData);                                
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;                                
            }
        }

        //Change#43 - Behroz - 09/01/2013 - Start
        /// <summary>
        /// This funciton updates the property image in the database( if there is already some appointment in the database against that property)
        /// </summary>
        /// <param name="propPicData">property picture data</param>        
        /// <returns>true or false upon successfull saving on disk and on db</returns>
        public bool savePropertyImageGas(PropertyPictureData propPicData)
        {
            try
            {
                PropertyDal propDal = new PropertyDal();
                return propDal.savePropertyImageGas(propPicData);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }

        public int getItemId()
        {
            try
            {
                PropertyDal propDal = new PropertyDal();
                return propDal.getItemId();
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }

        //Change#43 - Behroz - 09/01/2013 - End
        #endregion

        #region delete Property Image
        /// <summary>
        /// This function is used to delete the image against the property from the server
        /// </summary>
        /// <param name="propertyPictureId">property picture id</param>
        /// <returns>property picture data which is deleted</returns>
        
        public PropertyPictureData deletePropertyImage(int propertyPictureId)
        {
            try
            {
                PropertyDal propDal = new PropertyDal();
                PropertyPictureData propPicData = new PropertyPictureData();

                propPicData = propDal.deletePropertyImage(propertyPictureId);

                return propPicData;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion 

        #region get Property Dimensions

        /// <summary>
        /// This function returns ths property dimensions
        /// </summary>
        /// <param name="propertyId">property id</param>
        /// <returns>List of property dimension data object</returns>

        public List<PropertyDimData> getPropertyDimensions(string propertyId)
        {
            try
            {
                PropertyDal propDal = new PropertyDal();
                return propDal.getPropertyDimensions(propertyId);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        #region update Property Dimensions

        /// <summary>
        /// This function updates the property dimensions
        /// </summary>
        /// <param name="propDimData">This function accepts the property dimension data object</param>
        /// <returns>true on successful update and false on un successful update</returns>

        public bool updatePropertyDimensions(List<PropertyDimData> propertyDimBo)
        {
            try
            {                
                PropertyDal propertyDal = new PropertyDal();
                return propertyDal.updatePropertyDimensions(propertyDimBo);
                
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        #region is Property Appointment Exist
        /// <summary>
        /// This function checks that either appointment exist against this property
        /// </summary>
        /// <param name="propertyId">property id</param>
        /// <returns>true / false</returns>
        public void isPropertyAppointmentExist(string propertyId)
        {
            try 
            {
                PropertyDal propDal = new PropertyDal();

                //varifies property id exists in database against any appointment
                if (propDal.isPropertyAppointmentExist(propertyId) == false)
                {
                    ErrorFaultSetGet.setErrorFault(String.Format(MessageConstants.PropertyIdDoesNotExistMsg, propertyId), true, ErrorCodesConstants.PropertyIdDoesNotExistCode);
                    throw new ArgumentException(String.Format(MessageConstants.PropertyIdDoesNotExistMsg, propertyId), "propertyId");
                }                
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == true)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
            }
        }

        //Change#43 - Behroz - 09/01/2013 - Start
        /// <summary>
        /// This function checks that either appointment exist against this property
        /// </summary>
        /// <param name="propertyId">property id</param>
        /// <returns>true / false</returns>
        public void isPropertyAppointmentExist(int appointmentId)
        {
            try
            {
                PropertyDal propDal = new PropertyDal();

                //varifies property id exists in database against any appointment
                if (propDal.isPropertyAppointmentExist(appointmentId) == false)
                {
                    ErrorFaultSetGet.setErrorFault(String.Format(MessageConstants.PropertyIdDoesNotExistMsgGas, appointmentId), true, ErrorCodesConstants.AppointmentIdDoesNotExistCode);
                    throw new ArgumentException(String.Format(MessageConstants.PropertyIdDoesNotExistMsgGas, appointmentId), "appointmentId");
                }
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == true)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
            }
        }
        //Change#43 - Behroz - 09/01/2013 - End
        #endregion

        #region "get Property Asbestos Risk"
        /// <summary>
        /// This function returns the list of asbestos risk against the property.
        /// </summary>
        /// <param name="propertyId">property id</param>
        /// <returns>the list of asbestos risks</returns>

        public List<PropertyAsbestosData> getPropertyAsbestosRisk(string propertyId)
        {
            try
            {
                PropertyDal propertyDal = new PropertyDal();
                return propertyDal.getPropertyAsbestosRisk(propertyId);

            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion 
    }
}
