﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PropSurvey.Contracts.Data;
using PropSurvey.Dal.Survey;
using System.Data;
using PropSurvey.Utilities.ErrorSetter;
using PropSurvey.Utilities.Constants;
using PropSurvey.Contracts.Fault;


namespace PropSurvey.BuisnessLayer.Survey
{
    public class SurveyBl
    {
        #region save Survey Form

        /// <summary>
        /// This function save the survey form. Data should be post at specific url. 
        /// </summary>
        /// <param name="survData">survey data object</param>
        /// <returns>it returns ture false</returns>
        
        public bool saveSurveyForm(SurveyData survData)
        {
            try
            {
                SurveyDal survDal = new SurveyDal();  
                
                string propertyId = survData.propertyId;
                int customerId = survData.customerId;                
                int appointmentId = survData.appointmentId;                
                bool success = false;
                
                bool isAppointmentExist = survDal.isAppointmentForPropertyExist(propertyId, customerId, appointmentId);
                ////bool isSurveyFormAlreadySaved = survDal.isSurveyFormAlreadySaved(appointmentId, survData.propertyPortionName, survData.surveyFields);
                
                //if appointment does not exist then this is error
                if (isAppointmentExist == false)
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.AppointmentIdDoesNotExistForPropertyMsg, true, ErrorCodesConstants.PropertyOrCustomerOrAppointmentDoesNotExistCode);
                    throw new ArgumentException(String.Format(MessageConstants.AppointmentIdDoesNotExistForPropertyMsg, propertyId, customerId.ToString(), appointmentId.ToString()), "propertyId or customerId or appointmentId");
                }
                else
                {
                    
                    success = survDal.saveSurveyForm(survData);
                }

                ////if survey form is already saved then this is error
                //if (isSurveyFormAlreadySaved == true)
                //{
                //    bool isSurFieldsAleadySaved = survDal.isSurveyFormFieldsAlreadySaved(survData);
                //    if (isSurFieldsAleadySaved)
                //    {
                //        ErrorFaultSetGet.setErrorFault(MessageConstants.SurveyFormAlreadySavedForPropertyMsg, true, ErrorCodesConstants.SurveyFormAlreadySavedErrorCode);
                //        throw new ArgumentException(String.Format(MessageConstants.SurveyFormAlreadySavedForPropertyMsg, propertyId, survData.propertyPortionName), "Appointment id and Property Portion Id");
                //    }
                //    else
                //    {
                //        return survDal.saveSurveyNewFields(survData);
                //    }
                   
                //}
                //return survDal.saveSurveyForm(survData);

                return success;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == true)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
                
            }
        }
        #endregion

        #region get Saved Survey Form
        /// <summary>
        /// this funciton returns the saved survey form against appointment id and property portion name
        /// </summary>
        /// <param name="appointmentId">appointment id</param>
        /// <param name="propertyPortionName">property portion name is the semicolon separated portions of name e.g Internal;Rooof;Dwelling</param>
        /// <returns>it returns the saved data of property survey form</returns>
        
        public SurveyData getSavedSurveyForm(int appointmentId, string propertyPortionName)
        {
            try
            {
                SurveyDal survDal = new SurveyDal();
                return survDal.getSavedSurveyForm(appointmentId, propertyPortionName);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {                
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;                
            }
        }
        #endregion 

        #region get All Saved Survey Forms
        /// <summary>
        /// this funciton returns all the saved survey forms against appointment id
        /// </summary>
        /// <param name="appointmentId">appointment id</param>        
        /// <returns>it returns the list of saved forms data of property survey form</returns>

        public List<SurveyData> getAllSavedSurveyForms(int appointmentId)
        {
            //fetch all the survey forms against appointment id
            try
            {
                SurveyDal survDal = new SurveyDal();
                return survDal.getAllSavedSurveyForms(appointmentId);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        ///123456///
        #region get Surveyor Survey Forms
        /// <summary>
        /// this funciton returns all the saved survey forms against surveyor username
        /// </summary>
        /// <param name="userName">surveyor username</param>        
        /// <returns>it returns the list of saved forms data of property survey form</returns>

        public List<SurveyData> getSurveyorSurveyForms(string userName)
        {
            //fetch all the survey forms against serveyor username
            try
            {
                SurveyDal survDal = new SurveyDal();
                return survDal.getSurveyorSurveyForms(userName);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion
    }
}
