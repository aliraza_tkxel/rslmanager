﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using PropSurvey.Utilities.ErrorSetter;
using PropSurvey.Utilities.Constants;
using PropSurvey.Dal.Push;

namespace PropSurvey.BuisnessLayer.Push
{
    public class PushNotificationBl
    {
        #region send Push Notification for Fault Appointment
        /// <summary>
        /// This function will send push notification for fault appointment
        /// </summary>
        /// <returns>Success or failure</returns>
        public bool sendPushNotificationFault(int appointmentId, int type, string serverinUse)
        {
            try
            {
                PushNotificationDal pushNotificationDal = new PushNotificationDal();
                return pushNotificationDal.sendPushNotificationFault(appointmentId, type, serverinUse);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion
    }
}
