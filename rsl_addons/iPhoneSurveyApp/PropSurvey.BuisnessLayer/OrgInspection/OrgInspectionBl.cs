﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PropSurvey.Contracts.Data;
using PropSurvey.Dal.OrgInspection;
using System.Data;
using PropSurvey.Utilities.Constants;
using PropSurvey.Contracts.Fault;
using PropSurvey.Utilities.ErrorSetter;

namespace PropSurvey.BuisnessLayer.OrgInspection
{
    public class OrgInspectionBl
    {

        #region get Org list
        /// <summary>
        /// This function returns the Org list
        /// </summary>
        /// <returns>List of Org data objects</returns>

        public List<OrgData> getAllOrg(string userName)
        {
            try
            {
                OrgInspectionDal orgDal = new OrgInspectionDal();
                OrgData OrgData = new OrgData();
                return orgDal.getAllOrg(userName);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }

        #endregion

        #region get Gas Engineer list
        /// <summary>
        /// This function returns all the Gas Engineers working in a Org
        /// </summary>
        /// <returns>List of Emp data objects</returns>

        public List<EmployeData> getAllGasEngineer(string userName)
        {
            try
            {
                OrgInspectionDal orgDal = new OrgInspectionDal();
                OrgData OrgData = new OrgData();
                return orgDal.getAllGasEngineer(userName);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }

        #endregion

        #region get PaymentType, Payment Terms list
        /// <summary>
        /// This function returns the PaymentType, Payment Terms List
        /// </summary>
        /// <returns>List of PaymentType, Payment Terms data objects</returns>

        public List<PaymentTypeData> getPaymentType()
        {
            try
            {
                OrgInspectionDal orgDal = new OrgInspectionDal();
                return orgDal.getPaymentType();
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }

        #endregion

        #region get company Type list
        /// <summary>
        /// This function returns the company type List
        /// </summary>
        /// <returns>List of company type data objects</returns>

        public List<CompanyTypeData> getCompanyType()
        {
            try
            {
                OrgInspectionDal orgDal = new OrgInspectionDal();
                return orgDal.getCompanyType();
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }

        #endregion

        #region get Org Inspection Form

        /// <summary>
        /// This function returns Org Inspected form
        /// </summary>
        /// <returns>List of org inspection data objects</returns>

        public OrgInspectionData getOrgInspection(int appointmentId)
        {
            try
            {
                OrgInspectionDal orgDal = new OrgInspectionDal();
                OrgData OrgData = new OrgData();
                return orgDal.getOrgInspection(appointmentId);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }

        #endregion

        #region save Org Inspection
        /// <summary>
        /// This function saves the Org Inspection in the database
        /// </summary>
        /// <param name="appData">The object of Org Inspection</param>
        /// <returns>the Appliance id if successfully save otherwise 0 </returns>

        public int saveOrgInspection(OrgInspectionData orgData)
        {
            try
            {
                OrgInspectionDal orgDal = new OrgInspectionDal();
                if (orgDal.checkValidEmpID((int)orgData.GasEngineerID) && orgDal.checkValidAppointmentID((int)orgData.AppointmentID))
                {
                    return orgDal.saveOrgInspection(orgData);
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.OrgInspectionInvalidInput, true, ErrorCodesConstants.OrgInspectionInvalidInput);
                    throw new ArgumentException(MessageConstants.OrgInspectionInvalidInput);
                }
                
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                    throw ex;
                }   
            }
        }

        #endregion

        #region update Org Inspection
        /// <summary>
        /// This function updates the Org Inspection in the database
        /// </summary>
        /// <param name="appData">The object of Org Inspection</param>
        /// <returns>true if successfully save otherwise 0 </returns>

        public bool updateOrgInspection(OrgInspectionData orgData)
        {
            try
            {
                OrgInspectionDal orgDal = new OrgInspectionDal();
                if (orgDal.checkValidEmpID((int)orgData.GasEngineerID) && orgDal.checkValidAppointmentID((int)orgData.AppointmentID))
                {
                    return orgDal.updateOrgInspection(orgData);
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.OrgInspectionInvalidInput, true, ErrorCodesConstants.OrgInspectionInvalidInput);
                    throw new ArgumentException(MessageConstants.OrgInspectionInvalidInput);
                }
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                    throw ex;
                }   
            }
        }

        #endregion

        #region Save a new Org
        /// <summary>
        /// This function saves a new Org
        /// </summary>
        /// <param name="appData">Org data objects</param>
        /// <returns>ID in case of success</returns>

        public int saveOrg(OrgData orgData)
        {
            try
            {
                OrgInspectionDal orgDal = new OrgInspectionDal();
                OrgData OrgData = new OrgData();

                if (orgDal.checkValidPaymentType((int)orgData.PAYMENTTYPE) && orgDal.checkValidCompanyType((int)orgData.COMPANYTYPE) && orgDal.checkValidPayTerms((int)orgData.PAYMENTTERMS))
                {
                    return orgDal.saveOrg(orgData);
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.OrgPaymentTypeInvalid, true, ErrorCodesConstants.OrgPaymentTypeInvalid);
                    throw new ArgumentException(MessageConstants.OrgPaymentTypeInvalid);
                }
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                    throw ex;
                }   
            }
        }
        #endregion

    }
}
