﻿using System;
using System.Collections.Generic;
using System.Data;
using PropSurvey.Contracts.Data;
using PropSurvey.Contracts.Fault;
using PropSurvey.Dal.Appointment;
using PropSurvey.Dal.Authentication;
using PropSurvey.Dal.Customer;
using PropSurvey.Entities;
using PropSurvey.Utilities.Constants;
using PropSurvey.Utilities.ErrorSetter;
using PropSurvey.Dal.Property;
using PropSurvey.Utilities.Helpers;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;

namespace PropSurvey.BuisnessLayer.Appointment
{
    public class AppointmentBl
    {
        //#region get All Appointments
        ///// <summary>
        ///// This function returns all the appointments
        ///// </summary>
        ///// <returns>List of appointment data objects</returns>
        //public List<AppointmentData> getAllAppointments()
        //{
        //    try
        //    {
        //        AppointmentDal apptDal = new AppointmentDal();                                
        //        List<AppointmentData> apptData = new List<AppointmentData>();                                              
        //        apptData = apptDal.getAllAppointments();

        //        //add customer risk & vulnerability Data
        //        this.addRiskVulAsbestos(ref apptData);                

        //        return apptData;
        //    }
        //    catch (EntityException entityexception)
        //    {
        //        ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
        //        throw entityexception;
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
        //        throw ex;
        //    }
        //}
        //#endregion

        #region get All Appointments of all types for a user
        /// <summary>
        /// This function returns all appointments of all types for a user
        /// </summary>
        /// <returns>string of appointment</returns>
        public string getAllAppointmentsByUser(string userName, string filter)
        {
            try
            {
                string JSONFaultAppointment = string.Empty;
                string JSONStockAppointment = string.Empty;
                string JSONGasAppointment = string.Empty;
                string finalJSON = string.Empty;

                // Getting Fault appointments for this user
                List<AppointmentListFault> apptFault = this.getAllFaultAppointmentsByUser(userName);
                // List type changed to AppointmentListFault by Abdul Wahhab - 04/06/2013
                if (apptFault.Count > 0)
                {
                    JSONFaultAppointment = Utilities.Helpers.JsonHelper.SerializerToString(apptFault);
                    finalJSON = JSONFaultAppointment;
                }

                // Getting Stock appointments for this user
                List<AppointmentListStock> apptStock = this.getStockAppointmentsByUser(userName, filter);
                // List type changed to AppointmentListStock by Abdul Wahhab - 04/06/2013
                if (apptStock.Count > 0)
                {
                    JSONStockAppointment = Utilities.Helpers.JsonHelper.SerializerToString(apptStock);
                    finalJSON = JsonHelper.MergeJSONStrings(finalJSON, JSONStockAppointment);
                }

                // Getting Gas appointments for this user
                List<AppointmentListGas> apptGas = this.getUserAppointmentsGas(userName, filter);
                // List type changed to AppointmentListGas by Abdul Wahhab - 04/06/2013
                if (apptGas.Count > 0)
                {
                    JSONGasAppointment = Utilities.Helpers.JsonHelper.SerializerToString(apptGas);
                    finalJSON = JsonHelper.MergeJSONStrings(finalJSON, JSONGasAppointment);
                }

                if (finalJSON == string.Empty)
                {
                    finalJSON = "[]";
                }

                return finalJSON;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        #region get Fault Repair List
        /// <summary>
        /// This function returns the list of all fault repairs
        /// </summary>
        /// <returns>List of Fault Repair </returns>
        public List<FaultRepairData> getFaultRepairList()
        {
            try
            {
                AppointmentDal apptDal = new AppointmentDal();
                return apptDal.getFaultRepairList();
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        #region get pause reason list

        /// <summary>
        /// This function returns reason list for pausing a job
        /// </summary>
        /// <returns>List of reasons</returns>
        public List<string> getPauseReasonList()
        {
            try
            {
                AppointmentDal apptDal = new AppointmentDal();
                return apptDal.getPauseReasonList();
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        #region Update a Customer Contact data from fault appointment
        /// <summary>
        /// This function updates customer data from within fault appointment
        /// </summary>
        /// <param name="CustomerData">Customer Data's object</param>
        /// <returns>It retruns the true on success else returns false</returns>
        public bool UpdateCustomerContactFromAppontment(CustomerData cusData)
        {
            try
            {
                CustomerDal custDal = new CustomerDal();
                return custDal.UpdateCustomerContactFromAppointment(cusData);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        #region add Customer Risk And Vul
        // Code changed by Abdul Wahhab - 18/06/2013 - START
        /// <summary>
        /// This function add customer risk & vulnerability & also property asbestos risk in appointment data
        /// </summary>
        /// <param name="apptData">appointment data inculding customer id & property id</param>
        public void addRiskVulAsbestos(ref List<AppointmentListFault> apptData)
        {
            CustomerDal custDal = new CustomerDal();
            List<PS_GetCustomerRisk_Result> custRiskList = new List<PS_GetCustomerRisk_Result>();
            CustomerVulnerabilityData custVulData = new CustomerVulnerabilityData();

            foreach (AppointmentListFault singleApptData in apptData)
            {
                if (singleApptData.customerList != null)
                {
                    foreach (CustomerData custData in singleApptData.customerList)
                    {
                        int customerId = (int)custData.customerId; // Line changed by Abdul Wahhab - 19/06/2013

                        //add customer vulnerability data
                        custVulData = custDal.getCustomerVulnerabilities(customerId);

                        //for testing
                        //custVulData.vulCatDesc = "Vulnerability Category Description";
                        //custVulData.vulSubCatDesc = "Vulnerability Sub Category Description";

                        custData.customerVulnerabilityData = custVulData;

                        //get custoemr risk list
                        custRiskList = custDal.getCustomerRisk(customerId);

                        for (int i = 0; i < custRiskList.Count; i++)
                        {
                            CustomerRiskData custRiskData = new CustomerRiskData();
                            custRiskData.riskCatDesc = custRiskList[i].CATDESC;
                            custRiskData.riskSubCatDesc = custRiskList[i].SUBCATDESC;

                            //add object of risk to  appointment data 
                            custData.customerRiskData.Add(custRiskData);
                        }

                        custData.Address = null;
                        custData.property = null;
                    }
                }

                //add property asbestos risk
                PropertyDal propDal = new PropertyDal();
                List<PropertyAsbestosData> propAsbListData = new List<PropertyAsbestosData>();
                string propertyId = singleApptData.property.propertyId;

                //get property asbestos risk
                propAsbListData = propDal.getPropertyAsbestosRisk(propertyId);

                for (int j = 0; j < propAsbListData.Count; j++)
                {
                    PropertyAsbestosData propAsbData = new PropertyAsbestosData();
                    propAsbData.asbRiskLevelDesc = propAsbListData[j].asbRiskLevelDesc;
                    propAsbData.riskDesc = propAsbListData[j].riskDesc;

                    //add object of property risk to  appointment data 
                    singleApptData.property.propertyAsbestosData.Add(propAsbData);
                }

                singleApptData.property.propertyPicture = null;
            }
        }
        // Code changed by Abdul Wahhab - 18/06/2013 - END
        #endregion

        #region get All Fault Appointments Data by User
        // Code changed by Abdul Wahhab - 04/06/2013 - START
        /// <summary>
        /// This function returns all fault appointments data for a user
        /// </summary>
        /// <param name="operativeID">operativeID</param>
        /// <returns>List of Fault Appointments objects</returns>

        public List<AppointmentListFault> getAllFaultAppointmentsByUser(string userName)
        {
            try
            {
                AppointmentDal apptDal = new AppointmentDal();
                List<AppointmentListFault> apptFault = new List<AppointmentListFault>();
                apptFault = apptDal.getAllFaultAppointmentsByUser(userName);

                // Lines commented by Abdul Wahhab - 09/07/2013 - START
                ////add customer risk & vulnerability Data
                //this.addRiskVulAsbestos(ref apptFault);

                ////add job detail data
                //this.addJobDetailData(ref apptFault);
                // Lines commented by Abdul Wahhab - 09/07/2013 - END

                return apptFault;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        // Code changed by Abdul Wahhab - 04/06/2013 - END
        #endregion

        #region add Job Sheet Number Data for an appointment
        // Code changed by Abdul Wahhab - 04/06/2013 - START
        /// <summary>
        /// This function adds Job Sheet Numbers list to Jobs data
        /// </summary>
        /// <param name="jobsData">jobs Data Object List</param>
        /// <param name="appointmentID">appointment id</param>
        /// <returns></returns>
        private void addJobDetailData(ref List<AppointmentListFault> apptFaultList)
        {
            AppointmentDal apptDal = new AppointmentDal();
            List<JobData> JSNumberList = new List<JobData>();
            foreach (AppointmentListFault apptFaultData in apptFaultList)
            {
                apptFaultData.JobDataList = apptDal.getJobDataList(apptFaultData.appointmentId);
            }
        }
        // Code changed by Abdul Wahhab - 04/06/2013 - END
        #endregion

        #region get All Stock Appointments

        //Change Ticket# 8,9,10 - Zeeshan - 05/03/2013 - Start
        // Code changed by Abdul Wahhab - 04/06/2013 - START
        /// <summary>
        /// This function returns all the appointments
        /// </summary>
        /// <returns>List of appointment data objects</returns>
        public List<AppointmentListStock> getAllStockAppointments()
        {
            try
            {
                AppointmentDal apptDal = new AppointmentDal();
                List<AppointmentListStock> apptData = new List<AppointmentListStock>();
                // Lines commented by Abdul Wahhab as this call is no longer used - 09/07/2013 - START
                //apptData = apptDal.getAllStockAppointments();

                ////add customer risk & vulnerability Data
                //this.addRiskVulAsbestos(ref apptData);
                // Lines commented by Abdul Wahhab as this call is no longer used - 09/07/2013 - END

                return apptData;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        // Code changed by Abdul Wahhab - 04/06/2013 - END
        //Change Ticket# 8,9,10 - Zeeshan - 05/03/2013 - End
        #endregion


        #region Update Stock Appointment Progress Status

        /// <summary>
        /// This function update the status of appointment. This function use post method to accept data. 
        /// </summary>
        /// <param name="apptData">This fucntion accepts the AppointmentData's object </param>
        /// <param name="isAppointFinished">this flag represents the finished appointment flag </param>
        /// <param name="lastSurveyDate">The date of last survey which 'll be filled if status is finished</param>
        /// <returns>It returns true or false in update is successful</returns>

        public bool updateStockAppointmentProgressStatus(AppointmentDataStock apptData, bool isAppointFinished, DateTime lastSurveyDate)
        {
            try
            {
                AppointmentDal appointmentDal = new AppointmentDal();

                return appointmentDal.updateStockAppointmentProgressStatus(apptData, isAppointFinished, lastSurveyDate);

            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == false)
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
                else
                {
                    throw ex;
                }

            }

        }
        #endregion

        #region Update Fault Appointment Progress Status

        /// <summary>
        /// This function update the status of fault appointment. This function use post method to accept data. 
        /// </summary>
        /// <param name="apptData">This fucntion accepts the AppointmentDataFault's object </param>
        /// <param name="isAppointFinished">this flag represents the finished appointment flag </param>
        /// <param name="lastSurveyDate">The date of last survey which 'll be filled if status is finished</param>
        /// <returns>It returns true or false in update is successful</returns>

        public ResultBoolData updateFaultAppointmentProgressStatus(int appointmentID, string progressStatus)
        {
            try
            {
                bool isAppointFinished = false;
                DateTime lastSurveyDate = new DateTime();
                // Check if the appointment progress status is finished then put the last survey date
                if (progressStatus.Equals(FaultAppointmentProgressStatus.Complete.ToString()) == true)
                {
                    isAppointFinished = true;
                    lastSurveyDate = DateTime.Now;
                }
                AppointmentDal appointmentDal = new AppointmentDal();

                return appointmentDal.updateFaultAppointmentProgressStatus(appointmentID, progressStatus, isAppointFinished, lastSurveyDate);

            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == false)
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
                else
                {
                    throw ex;
                }

            }

        }
        #endregion

        #region update Fault Job Status

        /// <summary>
        /// This function update the status of all jobs of an appointment. This function use post method to accept data. 
        /// </summary>
        /// <param name="faultStatusData">This fucntion accepts the FaultStatusData's object </param>
        /// <param name="userName">user name </param>
        /// <param name="salt">salt </param>
        /// <returns>It returns true or false in update is successful</returns>

        public ResultBoolData updateFaultJobStatus(FaultStatusData faultStatusData, string userName)
        {
            try
            {
                AppointmentDal apptDal = new AppointmentDal();
                return apptDal.updateFaultJobStatus(faultStatusData, userName);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        #region update Appointment Status to NoEntry

        // Code added by Abdul Wahhab - 11/06/2013 - START

        /// <summary>
        /// This function update the status of all jobs of an appointment to NoEntry. This function use post method to accept data. 
        /// </summary>
        /// <param name="faultStatusData">This fucntion accepts the FaultStatusData's object </param>
        /// <param name="userName">user name </param>
        /// <param name="salt">salt </param>
        /// <returns>It returns true or false in update is successful</returns>

        public ResultBoolData noEntryAppointmentStatus(FaultStatusData faultStatusData, string userName)
        {
            try
            {
                AppointmentDal apptDal = new AppointmentDal();
                return apptDal.noEntryAppointmentStatus(faultStatusData, userName);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }

        // Code added by Abdul Wahhab - 11/06/2013 - END

        #endregion

        #region get And Set Appointment Progress Status
        /// <summary>
        /// This function checks the appointment progress status, If that is not started , it 'll start it otherwise it'll return false
        /// </summary>
        /// <param name="appointmentId">appointment id</param>
        /// <param name="userName">user name </param>
        /// <returns>it return true if appointment status changed from NotStarted to Pending other wise false</returns>
        public bool getAndSetStockAppointmentProgressStatus(AppointmentDataStock apptData)
        {
            try
            {
                AppointmentDal apptDal = new AppointmentDal();
                AuthenticationDal authDal = new AuthenticationDal();

                if (authDal.isUserExist(apptData.surveyorUserName) > 0)
                {
                    return apptDal.getAndSetStockAppointmentProgressStatus(apptData);
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(String.Format(MessageConstants.SurveyourUsernameInvalidUserMsg, apptData.surveyorUserName), true, ErrorCodesConstants.SurveyourUserNameInvalidCode);
                    throw new ArgumentException(String.Format(MessageConstants.SurveyourUsernameInvalidUserMsg, apptData.surveyorUserName), "surveyorUserName");
                }
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == false)
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
                else
                {
                    throw ex;
                }
            }
        }


        /// <summary>
        /// This function get fault appointment progress status
        /// </summary>
        /// <param name="appointmentId">appointment id</param>
        /// <param name="userName">user name </param>
        /// <returns>return the fault appointment progress status</returns>
        public string getFaultJobStatus(int appointmentId, string userName)
        {
            try
            {
                AppointmentDal apptDal = new AppointmentDal();
                AuthenticationDal authDal = new AuthenticationDal();

                // if (authDal.isUserExist(userName) > 0)
                //{
                return apptDal.getFaultJobStatus(appointmentId);
                //}
                //else
                //{
                //    ErrorFaultSetGet.setErrorFault(String.Format(MessageConstants.SurveyourUsernameInvalidUserMsg, userName), true, ErrorCodesConstants.SurveyourUserNameInvalidCode);
                //    throw new ArgumentException(String.Format(MessageConstants.SurveyourUsernameInvalidUserMsg, userName), "surveyorUserName");
                //}
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == false)
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
                else
                {
                    throw ex;
                }
            }
        }

        //Change#14 - Behroz - 12/07/2012 - Start
        /// <summary>
        /// This function checks the appointment progress status, If that is not started , it 'll start it otherwise it'll return false
        /// </summary>
        /// <param name="appointmentId">appointment id</param>
        /// <param name="userName">user name </param>
        /// <returns>it return true if appointment status changed from NotStarted to Pending other wise false</returns>
        public bool getAndSetAppointmentProgressStatus(AppointmentDataGas apptData)
        {
            try
            {
                AppointmentDal apptDal = new AppointmentDal();
                AuthenticationDal authDal = new AuthenticationDal();

                int assignedTo = authDal.GetUserId(apptData.assignedTo);

                if (assignedTo > 0)
                {
                    apptData.assignedTo = assignedTo;
                    return apptDal.getAndSetGasAppointmentProgressStatus(apptData);
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(String.Format(MessageConstants.SurveyourUsernameInvalidUserMsg, apptData.surveyorUserName), true, ErrorCodesConstants.SurveyourUserNameInvalidCode);
                    throw new ArgumentException(String.Format(MessageConstants.SurveyourUsernameInvalidUserMsg, apptData.surveyorUserName), "surveyorUserName");
                }
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == false)
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
                else
                {
                    throw ex;
                }
            }
        }
        //Change#14 - Behroz - 12/07/2012 - Start
        #endregion

        #region save Appointment
        // Code changed by Abdul Wahhab - 19/06/2013 - START
        /// <summary>
        /// This function saves the appointment in the database
        /// </summary>
        /// <param name="apptData">The object of appointment</param>
        /// <returns>the appointment id if successfully save otherwise 0 </returns>

        public int saveAppointment(AppointmentDataStock apptData)
        {
            try
            {
                AppointmentDal apptDal = new AppointmentDal();
                int appointmentId = 0;

                string propertyId = apptData.customer.property.propertyId;

                if (apptData.customer.customerId != null)
                {
                    int customerId = (int)apptData.customer.customerId; // Line changed by Abdul Wahhab - 19/06/2013

                    if (apptDal.isPropertyNCustomerExists(propertyId, customerId) == false)
                    {
                        ErrorFaultSetGet.setErrorFault(MessageConstants.PropertyIdOrCustomerIdDoesNotExistMsg, true, ErrorCodesConstants.PropertyOrCustomerDoesNotExistCode);
                        throw new ArgumentException(String.Format(MessageConstants.PropertyIdOrCustomerIdDoesNotExistMsg, propertyId, customerId.ToString()), "propertyId or customer Id");
                    }
                    else if (apptDal.isAppointmentExistsAgainstProperty(propertyId, apptData.appointmentType, AppointmentProgressStatus.InProgress.ToString()))
                    {
                        appointmentId = -1;
                        return appointmentId;
                    }
                }
                else
                {
                    if (apptDal.isPropertyExists(propertyId) == false)
                    {
                        ErrorFaultSetGet.setErrorFault(MessageConstants.PropertyDoesNotExistMsg, true, ErrorCodesConstants.PropertyOrCustomerDoesNotExistCode);
                        throw new ArgumentException(String.Format(MessageConstants.PropertyDoesNotExistMsg, propertyId), "propertyId");
                    }
                    else if (apptDal.isAppointmentExistsAgainstProperty(propertyId, apptData.appointmentType, AppointmentProgressStatus.InProgress.ToString()))
                    {
                        appointmentId = -1;
                        return appointmentId;
                    }
                }

                AuthenticationDal authDal = new AuthenticationDal();

                int loginId = authDal.isUserExist(apptData.surveyorUserName);

                if (loginId > 0)
                {
                    apptData.createdBy = loginId;
                    appointmentId = apptDal.saveAppointment(apptData);
                    SendMessage.sendPushMessage(authDal.getDeviceToken(loginId), "An appointment has been added against your account in property survey app.");
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(String.Format(MessageConstants.SurveyourUsernameInvalidUserMsg, apptData.surveyorUserName), true, ErrorCodesConstants.SurveyourUserNameInvalidCode);
                    throw new ArgumentException(String.Format(MessageConstants.SurveyourUsernameInvalidUserMsg, apptData.surveyorUserName), "surveyorUserName");
                }


                return appointmentId;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == true)
                {
                    throw ex;
                }
                else
                {
                    //ErrorFaultSetGet.setErrorFault(ex.Message+"Inner:"+ex.InnerException.Message, true, ErrorCodesConstants.GeneralExceptionCode);
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                    throw ex;
                }

            }

        }

        // Code changed by Abdul Wahhab - 19/06/2013 - END

        //Change#29 - Behroz - 12/04/2012 - Start
        // Code changed by Abdul Wahhab - 19/06/2013 - START
        public int saveAppointmentGas(AppointmentDataGas apptData)
        {
            try
            {
                AppointmentDal apptDal = new AppointmentDal();
                int appointmentId = 0;

                string propertyId = apptData.customer.property.propertyId;

                if (apptData.customer.customerId != null)
                {
                    int customerId = (int)apptData.customer.customerId; // Line changed by Abdul Wahhab - 19/06/2013

                    if (apptDal.isPropertyNCustomerExists(propertyId, customerId) == false)
                    {
                        ErrorFaultSetGet.setErrorFault(MessageConstants.PropertyIdOrCustomerIdDoesNotExistMsg, true, ErrorCodesConstants.PropertyOrCustomerDoesNotExistCode);
                        throw new ArgumentException(String.Format(MessageConstants.PropertyIdOrCustomerIdDoesNotExistMsg, propertyId, customerId.ToString()), "propertyId or customer Id");
                    }
                    else if (apptDal.isAppointmentExistsAgainstPropertyGas(propertyId, AppointmentProgressStatus.InProgress.ToString()))
                    {
                        appointmentId = -1;
                        return appointmentId;
                    }
                }
                else
                {
                    if (apptDal.isPropertyExists(propertyId) == false)
                    {
                        ErrorFaultSetGet.setErrorFault(MessageConstants.PropertyDoesNotExistMsg, true, ErrorCodesConstants.PropertyOrCustomerDoesNotExistCode);
                        throw new ArgumentException(String.Format(MessageConstants.PropertyDoesNotExistMsg, propertyId), "propertyId");
                    }
                    else if (apptDal.isAppointmentExistsAgainstPropertyGas(propertyId, AppointmentProgressStatus.InProgress.ToString()))
                    {
                        appointmentId = -1;
                        return appointmentId;
                    }
                }

                AuthenticationDal authDal = new AuthenticationDal();

                int assignedTo = authDal.isUserExistGas(apptData.surveyorUserName).Value;
                int createdBy = authDal.GetUserId(apptData.createdBy.Value);

                //if (assignedTo > 0)
                if (assignedTo > 0 && createdBy > 0)
                {
                    apptData.assignedTo = assignedTo;
                    apptData.createdBy = createdBy;
                    appointmentId = apptDal.saveAppointmentGas(apptData);
                    SendMessage.sendPushMessage(authDal.getDeviceToken(assignedTo), "An appointment has been added against your account in property survey app.");
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(String.Format(MessageConstants.SurveyourUsernameInvalidUserMsg, apptData.surveyorUserName), true, ErrorCodesConstants.SurveyourUserNameInvalidCode);
                    throw new ArgumentException(String.Format(MessageConstants.SurveyourUsernameInvalidUserMsg, apptData.surveyorUserName), "surveyorUserName");
                }

                return appointmentId;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == true)
                {
                    throw ex;
                }
                else
                {
                    //ErrorFaultSetGet.setErrorFault(ex.Message+"Inner:"+ex.InnerException.Message, true, ErrorCodesConstants.GeneralExceptionCode);
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                    throw ex;
                }

            }

        }
        // Code changed by Abdul Wahhab - 19/06/2013 - END
        //Change - Behroz - 12/04/2012 - End
        #endregion

        #region get User Appointments
        // Code changed by Abdul Wahhab - 04/-6/2013 - START
        /// <summary>
        /// This function returns all the appointments of the particular user
        /// </summary>
        /// <param name="username">username </param>
        /// <returns>the list of appointments </returns>

        public List<AppointmentListStock> getStockAppointmentsByUser(string userName, string filter)
        {
            try
            {
                AppointmentDal apptDal = new AppointmentDal();
                List<AppointmentListStock> apptData = new List<AppointmentListStock>();
                apptData = apptDal.getStockAppointmentsByUser(userName, filter);

                //Lines commented by Abdul Wahhab - 09/07/2013 - START
                ////add customer risk & vulnerability Data
                //this.addRiskVulAsbestos(ref apptData);
                //Lines commented by Abdul Wahhab - 09/07/2013 - END

                return apptData;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }

        // Code changed by Abdul Wahhab - 04/-6/2013 - END

        //Change#28 - Behroz - 12/11/2012 - Start
        // Code changed by Abdul Wahhab - 04/06/2013 - START
        /// <summary>
        /// This function returns all the appointments of the particular user related to GAS
        /// </summary>
        /// <param name="username">username </param>
        /// <returns>the list of appointments </returns>
        public List<AppointmentListGas> getUserAppointmentsGas(string userName, string filter)
        {
            try
            {
                AppointmentDal apptDal = new AppointmentDal();
                List<AppointmentListGas> apptData = new List<AppointmentListGas>();
                apptData = apptDal.getUserAppointmentsGas(userName, filter);

                // Lines commented by Abdul Wahhab - 09/07/2013 - START
                ////add customer risk & vulnerability Data
                //this.addRiskVulAsbestos(ref apptData);
                // Lines commented by Abdul Wahhab - 09/07/2013 - END

                return apptData;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        // Code changed by Abdul Wahhab - 04/06/2013 - END
        //Change#28 - Behroz - 12/11/2012 - End
        #endregion

        #region add Customer Risk And Vul
        // Code changed by Abdul Wahhab - 18/06/2013 - START
        /// <summary>
        /// This function add customer risk & vulnerability & also property asbestos risk in appointment data
        /// </summary>
        /// <param name="apptData">appointment data inculding customer id & property id</param>
        public void addRiskVulAsbestos(ref List<AppointmentListStock> apptData)
        {
            CustomerDal custDal = new CustomerDal();
            List<PS_GetCustomerRisk_Result> custRiskList = new List<PS_GetCustomerRisk_Result>();
            CustomerVulnerabilityData custVulData = new CustomerVulnerabilityData();

            foreach (AppointmentListStock singleApptData in apptData)
            {
                if (singleApptData.customerList != null)
                {

                    foreach (CustomerData custData in singleApptData.customerList)
                    {
                        int customerId = (int)custData.customerId; // Line changed by Abdul Wahhab - 19/06/2013

                        //add customer vulnerability data
                        custVulData = custDal.getCustomerVulnerabilities(customerId);

                        //for testing
                        //custVulData.vulCatDesc = "Vulnerability Category Description";
                        //custVulData.vulSubCatDesc = "Vulnerability Sub Category Description";

                        custData.customerVulnerabilityData = custVulData;

                        //get custoemr risk list
                        custRiskList = custDal.getCustomerRisk(customerId);

                        for (int i = 0; i < custRiskList.Count; i++)
                        {
                            CustomerRiskData custRiskData = new CustomerRiskData();
                            custRiskData.riskCatDesc = custRiskList[i].CATDESC;
                            custRiskData.riskSubCatDesc = custRiskList[i].SUBCATDESC;

                            //add object of risk to  appointment data 
                            custData.customerRiskData.Add(custRiskData);
                        }

                        custData.Address = null;
                        custData.property = null;
                    }
                }

                //add property asbestos risk
                PropertyDal propDal = new PropertyDal();
                List<PropertyAsbestosData> propAsbListData = new List<PropertyAsbestosData>();
                string propertyId = singleApptData.property.propertyId;


                //get property asbestos risk
                propAsbListData = propDal.getPropertyAsbestosRisk(propertyId);

                for (int j = 0; j < propAsbListData.Count; j++)
                {
                    PropertyAsbestosData propAsbData = new PropertyAsbestosData();
                    propAsbData.asbRiskLevelDesc = propAsbListData[j].asbRiskLevelDesc;
                    propAsbData.riskDesc = propAsbListData[j].riskDesc;

                    //add object of property risk to  appointment data 
                    singleApptData.property.propertyAsbestosData.Add(propAsbData);
                }

                singleApptData.property.propertyPicture = null;
            }
        }
        // Code changed by Abdul Wahhab - 18/06/2013 - END

        //Change - Behroz - 12/05/2012 - Start
        // Code changed by Abdul Wahhab - 18/06/2013 - START
        /// <summary>
        /// This function add customer risk & vulnerability & also property asbestos risk in appointment data
        /// </summary>
        /// <param name="apptData">appointment data inculding customer id & property id</param>
        public void addRiskVulAsbestos(ref List<AppointmentListGas> apptData)
        {
            CustomerDal custDal = new CustomerDal();
            List<PS_GetCustomerRisk_Result> custRiskList = new List<PS_GetCustomerRisk_Result>();
            CustomerVulnerabilityData custVulData = new CustomerVulnerabilityData();

            foreach (AppointmentListGas singleApptData in apptData)
            {
                if (singleApptData.customerList != null)
                {
                    foreach (CustomerData custData in singleApptData.customerList)
                    {
                        int customerId = (int)custData.customerId; // Line changed by Abdul Wahhab - 19/06/2013

                        //add customer vulnerability data
                        custVulData = custDal.getCustomerVulnerabilities(customerId);

                        //for testing
                        //custVulData.vulCatDesc = "Vulnerability Category Description";
                        //custVulData.vulSubCatDesc = "Vulnerability Sub Category Description";

                        custData.customerVulnerabilityData = custVulData;

                        //get custoemr risk list
                        custRiskList = custDal.getCustomerRisk(customerId);

                        for (int i = 0; i < custRiskList.Count; i++)
                        {
                            CustomerRiskData custRiskData = new CustomerRiskData();
                            custRiskData.riskCatDesc = custRiskList[i].CATDESC;
                            custRiskData.riskSubCatDesc = custRiskList[i].SUBCATDESC;

                            //add object of risk to  appointment data 
                            custData.customerRiskData.Add(custRiskData);
                        }

                        custData.Address = null;
                        custData.property = null;
                    }
                }

                //add property asbestos risk
                PropertyDal propDal = new PropertyDal();
                List<PropertyAsbestosData> propAsbListData = new List<PropertyAsbestosData>();
                string propertyId = singleApptData.property.propertyId;

                //get property asbestos risk
                propAsbListData = propDal.getPropertyAsbestosRisk(propertyId);

                for (int j = 0; j < propAsbListData.Count; j++)
                {
                    PropertyAsbestosData propAsbData = new PropertyAsbestosData();
                    propAsbData.asbRiskLevelDesc = propAsbListData[j].asbRiskLevelDesc;
                    propAsbData.riskDesc = propAsbListData[j].riskDesc;

                    //add object of property risk to  appointment data 
                    singleApptData.property.propertyAsbestosData.Add(propAsbData);
                }

                singleApptData.property.propertyPicture = null;
            }
        }
        // Code changed by Abdul Wahhab - 18/06/2013 - END

        ///// <summary>
        ///// This function add customer risk & vulnerability & also property asbestos risk in appointment data
        ///// </summary>
        ///// <param name="apptData">appointment data inculding customer id & property id</param>
        //public void addRiskVulAsbestos(ref List<AppointmentDataAll> apptData)
        //{
        //    CustomerDal custDal = new CustomerDal();
        //    List<PS_GetCustomerRisk_Result> custRiskList = new List<PS_GetCustomerRisk_Result>();
        //    CustomerVulnerabilityData custVulData = new CustomerVulnerabilityData();

        //    int apptCounter = 0;
        //    foreach (AppointmentDataAll singleApptData in apptData)
        //    {
        //        int customerId = singleApptData.customer.customerId;

        //        //add customer vulnerability data
        //        custVulData = custDal.getCustomerVulnerabilities(customerId);

        //        //for testing
        //        //custVulData.vulCatDesc = "Vulnerability Category Description";
        //        //custVulData.vulSubCatDesc = "Vulnerability Sub Category Description";

        //        apptData[apptCounter].customer.customerVulnerabilityData = custVulData;

        //        //get custoemr risk list
        //        custRiskList = custDal.getCustomerRisk(customerId);

        //        for (int i = 0; i < custRiskList.Count; i++)
        //        {
        //            CustomerRiskData custRiskData = new CustomerRiskData();
        //            custRiskData.riskCatDesc = custRiskList[i].CATDESC;
        //            custRiskData.riskSubCatDesc = custRiskList[i].SUBCATDESC;

        //            //add object of risk to  appointment data 
        //            apptData[apptCounter].customer.customerRiskData.Add(custRiskData);
        //        }

        //        //add property asbestos risk
        //        PropertyDal propDal = new PropertyDal();
        //        List<PropertyAsbestosData> propAsbListData = new List<PropertyAsbestosData>();
        //        string propertyId = singleApptData.customer.property.propertyId;

        //        //get property asbestos risk
        //        propAsbListData = propDal.getPropertyAsbestosRisk(propertyId);

        //        for (int j = 0; j < propAsbListData.Count; j++)
        //        {
        //            PropertyAsbestosData propAsbData = new PropertyAsbestosData();
        //            propAsbData.asbRiskLevelDesc = propAsbListData[j].asbRiskLevelDesc;
        //            propAsbData.riskDesc = propAsbListData[j].riskDesc;

        //            //add object of property risk to  appointment data 
        //            apptData[apptCounter].customer.property.propertyAsbestosData.Add(propAsbData);
        //        }

        //        apptCounter++;
        //    }
        //}

        //Change - Behroz - 12/05/2012 - End
        #endregion

        #region get All Users
        /// <summary>
        /// This function returns all the users of rsl manager database
        /// </summary>        
        /// <returns>list of all users</returns>

        public List<UserData> getAllUsers()
        {
            try
            {
                AppointmentDal apptDal = new AppointmentDal();
                List<UserData> userData = new List<UserData>();
                userData = apptDal.getAllUsers();

                return userData;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }

        //Change#17 - Behroz - 19/12/2012 - Start
        /// <summary>
        /// This function returns all the users of rsl manager database for gas
        /// </summary>        
        /// <returns>list of all users</returns>
        public List<UserData> getAllUsersGas()
        {
            try
            {
                AppointmentDal apptDal = new AppointmentDal();
                List<UserData> userData = new List<UserData>();
                userData = apptDal.getAllUsersGas();

                return userData;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        //Change#17 - Behroz - 19/12/2012 - End

        #endregion

        #region save NoEntry
        /// <summary>
        /// This function saves the appointment NoEntry in the database
        /// </summary>
        /// <param name="apptData">The object of NoEntry</param>
        /// <returns>the NoEntry id if successfully save otherwise 0 </returns>

        public int saveNoEntry(NoEntryData noEntryData)
        {
            try
            {
                AppointmentDal apptDal = new AppointmentDal();
                int status = apptDal.saveNoEntry(noEntryData);

                return status;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }

        //Change#10 - Behroz - 12/11/2012 - Start
        /// <summary>
        /// This function saves the appointment NoEntry in the database
        /// </summary>
        /// <param name="apptData">The object of NoEntry</param>
        /// <returns>the NoEntry id if successfully save otherwise 0 </returns>
        public int saveNoEntryGas(NoEntryData noEntryData)
        {
            try
            {
                AppointmentDal apptDal = new AppointmentDal();
                AuthenticationDal authDal = new AuthenticationDal();

                int recordedBy = authDal.GetUserId(noEntryData.RecordedBy);
                noEntryData.RecordedBy = recordedBy;

                int status = apptDal.saveNoEntryGas(noEntryData);

                return status;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        //Change#10 - Behroz - 12/11/2012 - End
        #endregion

        #region Get Total No of NoEntries by PropertyID
        /// <summary>
        /// This function get appointment NoEntry from the database
        /// </summary>
        /// <param name="propertyID">PropertyID</param>
        /// <returns>the no of NoEntries</returns>

        public AppointmentInfoData getTotalNoEntries(string propertyID)
        {
            try
            {
                AppointmentDal apptDal = new AppointmentDal();
                AppointmentInfoData totalNoEntry = apptDal.getTotalNoEntries(propertyID);

                return totalNoEntry;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        #region Update Appointment

        /// <summary>
        /// This function updates the appointment. This function use post method to accept data. 
        /// </summary>
        /// <param name="apptData">This fucntion accepts the AppointmentData's object </param>
        /// <returns>It returns true or false in update is successful</returns>

        public bool updateAppointment(AppointmentDataStock apptData)
        {
            try
            {
                AppointmentDal appointmentDal = new AppointmentDal();
                return appointmentDal.updateAppointment(apptData);

            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == false)
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
                else
                {
                    throw ex;
                }

            }

        }

        //Change#11 - Behroz - 12/12/2012 - Start
        /// <summary>
        /// This function updates the appointment. This function use post method to accept data. 
        /// </summary>
        /// <param name="apptData">This fucntion accepts the AppointmentData's object </param>
        /// <returns>It returns true or false in update is successful</returns>
        public bool updateAppointment(AppointmentDataGas apptData)
        {
            try
            {
                AppointmentDal appointmentDal = new AppointmentDal();
                AuthenticationDal authDal = new AuthenticationDal();

                int assignedTo = authDal.isUserExistGas(apptData.surveyorUserName).Value;
                int createdBy = authDal.GetUserId(apptData.createdBy.Value);

                //if (assignedTo > 0)
                if (assignedTo > 0 && createdBy > 0)
                {
                    apptData.assignedTo = assignedTo;
                    apptData.createdBy = createdBy;
                }

                return appointmentDal.updateAppointment(apptData);

            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == false)
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
                else
                {
                    throw ex;
                }

            }

        }
        //Change#11 - Behroz - 12/12/2012 - End

        #endregion

        #region delete Appointment
        /// <summary>
        /// This function is used to delete the appointment from the server
        /// </summary>
        /// <param name="appointmentId">appointment id</param>
        /// <returns>returns true or false based on deleteion</returns>

        public bool deleteAppointment(int appointmentId)
        {
            try
            {
                AppointmentDal appointmentDal = new AppointmentDal();
                return appointmentDal.deleteAppointment(appointmentId);

            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == false)
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
                else
                {
                    throw ex;
                }

            }

        }
        #endregion

        #region get Index Screen Info
        /// <summary>
        /// This function returns Index Screen Info
        /// </summary>        
        /// <returns>Index Screen Info</returns>

        public IndexStatusData GetIndexScreenStatus(int AppointmentID)
        {
            try
            {
                AppointmentDal apptDal = new AppointmentDal();
                IndexStatusData apptData = new IndexStatusData();
                apptData = apptDal.GetIndexScreenStatus(AppointmentID);

                return apptData;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }

        //Change#33 - Behroz - 20/12/2012 - Start
        /// <summary>
        /// This function returns Index Screen Info for gas
        /// </summary>        
        /// <returns>Index Screen Info</returns>
        public IndexStatusData GetIndexScreenStatusGas(int AppointmentID)
        {
            try
            {
                AppointmentDal apptDal = new AppointmentDal();
                IndexStatusData apptData = new IndexStatusData();
                apptData = apptDal.GetIndexScreenStatusGas(AppointmentID);

                return apptData;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        //Change#33 - Behroz - 20/12/2012 - End
        #endregion

        #region get Appointment
        /// <summary>
        /// This function returns an appointment based on appointment id
        /// </summary>
        /// <param name="appointmentId">appointment id</param>
        /// <returns>appointment data object</returns>
        public AppointmentDataStock getAppointment(int appointmentId)
        {
            try
            {
                AppointmentDal apptDal = new AppointmentDal();
                AppointmentDataStock apptData = new AppointmentDataStock();
                apptData = apptDal.getAppointment(appointmentId);

                //add customer risk & vulnerability Data
                this.addRiskVulAsbestos(ref apptData);

                return apptData;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        #region add Customer Risk And Vul to Single Appointment
        /// <summary>
        /// This function add customer risk & vulnerability & also property asbestos risk in appointment data
        /// </summary>
        /// <param name="apptData">appointment data inculding customer id & property id</param>
        public void addRiskVulAsbestos(ref AppointmentDataStock apptData)
        {
            CustomerDal custDal = new CustomerDal();
            List<PS_GetCustomerRisk_Result> custRiskList = new List<PS_GetCustomerRisk_Result>();
            CustomerVulnerabilityData custVulData = new CustomerVulnerabilityData();

            int customerId = (int)apptData.customer.customerId; // Line changed by Abdul Wahhab - 19/06/2013

            //add customer vulnerability data
            custVulData = custDal.getCustomerVulnerabilities(customerId);

            //for testing
            //custVulData.vulCatDesc = "Vulnerability Category Description";
            //custVulData.vulSubCatDesc = "Vulnerability Sub Category Description";

            apptData.customer.customerVulnerabilityData = custVulData;

            //get custoemr risk list
            custRiskList = custDal.getCustomerRisk(customerId);

            for (int i = 0; i < custRiskList.Count; i++)
            {
                CustomerRiskData custRiskData = new CustomerRiskData();
                custRiskData.riskCatDesc = custRiskList[i].CATDESC;
                custRiskData.riskSubCatDesc = custRiskList[i].SUBCATDESC;

                //add object of risk to  appointment data 
                apptData.customer.customerRiskData.Add(custRiskData);
            }

            //add property asbestos risk
            PropertyDal propDal = new PropertyDal();
            List<PropertyAsbestosData> propAsbListData = new List<PropertyAsbestosData>();
            string propertyId = apptData.customer.property.propertyId;

            //get property asbestos risk
            propAsbListData = propDal.getPropertyAsbestosRisk(propertyId);

            for (int j = 0; j < propAsbListData.Count; j++)
            {
                PropertyAsbestosData propAsbData = new PropertyAsbestosData();
                propAsbData.asbRiskLevelDesc = propAsbListData[j].asbRiskLevelDesc;
                propAsbData.riskDesc = propAsbListData[j].riskDesc;

                //add object of property risk to  appointment data 
                apptData.customer.property.propertyAsbestosData.Add(propAsbData);
            }



        }
        #endregion

        #region "Get All Status"
        //Change - Behroz - 12/10/2012 - Start
        /// <summary>
        /// This function calls the DAl method to get all the status from AS_Status table for gas only.
        /// </summary>
        /// <returns></returns>
        public List<StatusData> getAllStatusData()
        {
            try
            {
                List<StatusData> statusData = new List<StatusData>();
                AppointmentDal apptDal = new AppointmentDal();
                statusData = apptDal.getAllStatusData();
                return statusData;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        //Change - Behroz - 12/10/2012 - End
        #endregion
    }
}
