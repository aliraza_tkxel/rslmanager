﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PropSurvey.Contracts.Data;
using PropSurvey.Dal.Faults;
using System.Data;
using PropSurvey.Utilities.Constants;
using PropSurvey.Contracts.Fault;
using PropSurvey.Utilities.ErrorSetter;

namespace PropSurvey.BuisnessLayer.Faults
{
    public class FaultsBl
    {

        #region get Faults Data
        /// <summary>
        /// This function returns the Faults data
        /// </summary>
        /// <returns>List of Faults data objects</returns>

        public List<FaultsData> getFaultsData(int AppointmentID)
        {
            try
            {
                FaultsDal faultDal = new FaultsDal();
                return faultDal.getFaultsData(AppointmentID);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }

        //Change#18 - Behroz - 19/12/2012 - Start
        /// <summary>
        /// This function returns the Faults data for gas
        /// </summary>
        /// <returns>List of Faults data objects</returns>
        public List<FaultsDataGas> getFaultsDataGas(int journalId)
        {
            try
            {
                FaultsDal faultDal = new FaultsDal();
                return faultDal.getFaultsDataGas(journalId);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        //Change#18 - Behroz - 19/12/2012 - End

        #endregion

        #region get General Comments
        /// <summary>
        /// This function returns General Comments
        /// </summary>
        /// <returns>List of General Comments</returns>

        public List<FaultsData> getGeneralComments(int AppointmentID)
        {
            try
            {
                FaultsDal faultDal = new FaultsDal();
                return faultDal.getGeneralComments(AppointmentID);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }

        //Change#21 - Behroz - 19/12/2012 - Start
        /// <summary>
        /// This function returns General Comments for gas
        /// </summary>
        /// <returns>List of General Comments</returns>
        public List<FaultsDataGas> getGeneralCommentsGas(int journalId)
        {
            try
            {
                FaultsDal faultDal = new FaultsDal();
                return faultDal.getGeneralCommentsGas(journalId);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        //Change#21 - Behroz - 19/12/2012 - End

        #endregion

        #region get Faults only.
        /// <summary>
        /// This function returns Faults only.
        /// </summary>
        /// <returns>List of Faults.</returns>

        public List<FaultsData> getFaultsOnly(int AppointmentID)
        {
            try
            {
                FaultsDal faultDal = new FaultsDal();
                return faultDal.getFaultsOnly(AppointmentID);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }

        //Change#22 - Behroz - 19/12/2012 - Start
        /// <summary>
        /// This function returns Faults only for gas
        /// </summary>
        /// <returns>List of Faults.</returns>
        public List<FaultsDataGas> getFaultsOnlyGas(int journalId)
        {
            try
            {
                FaultsDal faultDal = new FaultsDal();
                return faultDal.getFaultsOnlyGas(journalId);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }

        //Change#22 - Behroz - 19/12/2012 - End

        #endregion

        #region save Faults Data
        /// <summary>
        /// This function saves the Faults Data in the database
        /// </summary>
        /// <param name="appData">The object of Faults Data</param>
        /// <returns>the Faults id if successfully save otherwise 0 </returns>

        public int saveFaultsData(FaultsData faultData)
        {
            try
            {
                FaultsDal faultDal = new FaultsDal();
                if (faultDal.checkAppointmentID(faultData.AppointmentID))
                {
                    return faultDal.saveFaultsData(faultData);
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.AppointmentIDInvalid, true, ErrorCodesConstants.AppointmentIDInvalid);
                    throw new ArgumentException(MessageConstants.AppointmentIDInvalid);
                }
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
            }
        }

        //Change#19 - Behroz - 19/12/2012 - Start
        /// <summary>
        /// This function saves the Faults Data in the database for gas
        /// </summary>
        /// <param name="appData">The object of Faults Data</param>
        /// <returns>the Faults id if successfully save otherwise 0 </returns>
        public int saveFaultsDataGas(FaultsDataGas faultData)
        {
            try
            {
                FaultsDal faultDal = new FaultsDal();
                if (faultDal.checkAppointmentIDGas(faultData.JournalId.Value))
                {
                    return faultDal.saveFaultsDataGas(faultData);
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.AppointmentIDInvalid, true, ErrorCodesConstants.AppointmentIDInvalid);
                    throw new ArgumentException(MessageConstants.AppointmentIDInvalid);
                }
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
            }
        }
        //Change#19 - Behroz - 19/12/2012 - End

        #endregion

        #region update fault Data
        /// <summary>
        /// This function updates fault Data in the database
        /// </summary>
        /// <param name="appData">The object of fault Data</param>
        /// <returns>true if successfully save otherwise 0 </returns>

        public bool updateFaultData(FaultsData faultData)
        {
            try
            {
                FaultsDal faultDal = new FaultsDal();
                if (faultDal.checkAppointmentID(faultData.AppointmentID))
                {
                    return faultDal.updateFaultData(faultData);
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.AppointmentIDInvalid, true, ErrorCodesConstants.AppointmentIDInvalid);
                    throw new ArgumentException(MessageConstants.AppointmentIDInvalid);
                }
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
            }
        }

        //Change#20 - Behroz - 19/12/2012 - Start
        /// <summary>
        /// This function updates fault Data in the database for gas
        /// </summary>
        /// <param name="appData">The object of fault Data</param>
        /// <returns>true if successfully save otherwise 0 </returns>
        public bool updateFaultDataGas(FaultsDataGas faultData)
        {
            try
            {
                FaultsDal faultDal = new FaultsDal();
                if (faultDal.checkAppointmentIDGas(faultData.JournalId.Value))
                {
                    return faultDal.updateFaultDataGas(faultData);
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.AppointmentIDInvalid, true, ErrorCodesConstants.AppointmentIDInvalid);
                    throw new ArgumentException(MessageConstants.AppointmentIDInvalid);
                }
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
            }
        }
        //Change#20 - Behroz - 19/12/2012 - End

        #endregion

        #region "Get all Defect Categories"
        //Change#15 - Behroz - 12/17/2012 - Start
        /// <summary>
        /// This function returns all the defect categories
        /// </summary>
        /// <returns>List of Defect Category Data</returns>
        public List<DefectCategoryData> getDefectCategories()
        {
            try
            {
                FaultsDal faultDal = new FaultsDal();
                return faultDal.getAllDefectCategories();
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        //Change#15 - Behroz - 12/17/2012 - End
        #endregion

        #region "Get Fault Id"
        //Change#51 - Behroz - 16/01/2013 - Start
        /// <summary>
        /// This function returns fault id
        /// </summary>
        /// <returns></returns>
        public int getFaultId(string propertyId)
        {
            try
            {
                FaultsDal faultDal = new FaultsDal();
                return faultDal.getFaultId(propertyId);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        //Change#51 - Behroz - 16/01/2013 - End
        #endregion

        #region "Delete Fault Id"
        //Change#52 - Behroz - 16/01/2013 - Start
        /// <summary>
        /// This function returns fault id
        /// </summary>
        /// <returns></returns>
        public bool deleteFault(int faultId)
        {
            try
            {
                FaultsDal faultDal = new FaultsDal();
                return faultDal.deleteFault(faultId);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        //Change#52 - Behroz - 16/01/2013 - End
        #endregion

        #region "Save Fault Image"
        public bool saveFaultImage(string fileName, int faultId)
        {
            try
            {
                FaultsDal faultDal = new FaultsDal();
                return faultDal.saveFaultImageData(fileName,faultId);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
            }
        }
        #endregion

        #region "Validate Fault Data"
        //Change#19 - Behroz -19/12/2012 - Start        
        /// <summary>
        /// This function validate Faults Data for gas
        /// </summary>
        /// <param name="appData">The object of Faults Data</param>
        /// <returns>true if successfully validate otherwise 0 </returns>
        public void validateFaultsData(FaultsDataGas insData)
        {
            FaultsDataGas validate = new FaultsDataGas();
            FaultsDal faultDal = new FaultsDal();

            string faultCategory = faultDal.validateFaultsCategories(insData.FaultCategory);
            if (faultCategory == string.Empty)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.InvalidFaultCategory, true, ErrorCodesConstants.InvalidFaultCategory);
                throw new ArgumentException(string.Format(MessageConstants.InvalidFaultCategory, faultCategory));
            }
        }
        //Change#19 - Behroz - 19/12/2012 - End

        //Change#53 - Behroz - 17/01/2013 - Start
        /// <summary>
        /// This function validate fault id
        /// </summary>
        /// <param name="appData"></param>
        /// <returns>true if successfully validate otherwise 0 </returns>
        public void validateFaultsId(int faultId)
        {
            FaultsDataGas validate = new FaultsDataGas();
            FaultsDal faultDal = new FaultsDal();

            bool faultExist = faultDal.validateFaultsId(faultId);
            if (!faultExist)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.InvalidFaultId, true, ErrorCodesConstants.InvalidFaultId);
                throw new ArgumentException(string.Format(MessageConstants.InvalidFaultId));
            }
        }
        //Change#53 - Behroz - 17/01/2013 - End
        #endregion
    }
}
