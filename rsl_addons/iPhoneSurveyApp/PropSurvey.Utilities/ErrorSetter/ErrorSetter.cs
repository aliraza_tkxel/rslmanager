﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PropSurvey.Contracts.Fault;


namespace PropSurvey.Utilities.ErrorSetter
{
    public static class ErrorFaultSetGet
    {
        public static void setErrorFault(string message, bool isErrorOccured, string errorCode)
        {
            ErrorFault.message = message;
            ErrorFault.isErrorOccured = isErrorOccured;
            ErrorFault.errorCode = errorCode;
        }

       

    }
}
