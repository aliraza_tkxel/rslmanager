﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PropSurvey.Utilities.Constants
{
    public class ApplicationConstants
    {
        public static string BHG_Dev_Name = "devcrm";
        public static string BHG_Test_Name = "testcrm";
        public static string BHG_Live_Name = "crm";
    }
}
