﻿
namespace PropSurvey.Utilities.Constants
{
    public static class MessageConstants
    {
        public const string DbConErrorMsg = "Error occured while connecting with data store.";
        public const string DbConEntityErrorMsg = "Error occured while connecting with data store entity.";

        //Property agent to appointment Messages
        public const string PropertyAgentInvalid = "Property Agent does not exists.";

        //Login Messages
        public const string LoginErrorMsg = "Authenticated";
        public const string LoginSuccessMsg = "Invalid username or password!";
        public const string AlreadyLoggedInMsg = "User is already already logged in!";
        public const string SessionExpiredMsg = "User session has expired!";

        //Faults Messages
        public const string InvalidFaultCategory = "Fault Category: {0} is invalid. Category should be GENERAL COMMENT / RIDDOR / IMMEDIATELY DANGEROUS / AT RISK / NOT TO CURRENT STANDARDS / OTHER";
        //Change#53 - Behroz - 17/01/2013 - Start
        public const string InvalidFaultId = "Fault is invalid.";
        //Change#53 - Behroz - 17/01/2013 - End
        //Installation Pipework Messages
        public const string InvalidReceivedOnBehalfOf = "Wronge Receiver provided. Value should be  TENANT/LANDLORD/AGENT/HOMEOWNER.";

        //Installation Pipework Messages
        public const string InstallationPipeWorkOptionInvalid = "Wronge code provided. Value should be NA/Yes/No.";
        public const string AppointmentIDInvalid = "Appointment Id is not valid. This ID does not exist in Database.";
        public const string AppointmentIDInvalidGas = "The Appointment ID does not exist in Database.";

        //Change#12 - Behroz - 22/12/2012 - Start
        public const string LGSRIdInvalid = "LGSR Id is not valid. This ID does not exist in Database.";
        //Change#12 - Behroz - 22/12/2012 - End

        //Customer Service Messages
        public const string CustomerIdInvalidFromtMsg = "Customer id is wrong. Customer id should be a positive integer(number)";
        //Change - Abdul Wahhab - 30/04/2013 - START
        public const string EmployeeIdInvalidFromtMsg = "Employee id is wrong. Employee id should be a positive integer(number)";
        public const string SignatureImageNotFoundMsg = "System is unable to locate the signature image file against this employee.";
        //Change - Abdul Wahhab - 30/04/2013 - END

        //Org Inspection Service Messages
        public const string OrgPaymentTypeInvalid = "Org PaymentType, or PaymentTerm or companyType is wronge.";
        public const string OrgInspectionInvalidInput = "Gas Engineer ID or Org ID or Appointment ID is wronge.";

        //Appliance Inspection Service Messages
        public const string AppInspectionValueInvalid = "Wronge code provided. Value should be NA/Yes/No/Pass/Fail.";

        //property service message
        public const string SkipTopInvalidFromtMsg = "Kkip OR top should be a positive integer(number)";
        public const string PropertyImageNotFoundMsg = "System is unable to locate the image file against this property.";
        public const string ImageExtensionInvalidMsg = "Image extension is invalid. It should be *.jpg or *.gif";
        public const string ImageSavingErrorMsg = "Error has been occured while saving the image on disk";
        public const string PropertyIdDoesNotExistMsg = "There is no appointment against this propertyId: {0}";
        //Change#43 - Behroz - 09/01/2013 -Start
        public const string PropertyIdDoesNotExistMsgGas = "There is no appointment against this appointmentId: {0}";
        //Change#43 - Behroz - 09/01/2013 -End
        public const string PropertyPictureIdInvalidMsg = "Property Picuture id: \"{0}\" is wrong.  Property picture id should be a positive integer(number)";
        public const string PropertyPictureIdDoesNotExistMsg = "There is no picture against this propertyPictureId: {0}";

        //appointment service message
        public const string AppointmentIdInvalidFromtMsg = "Appointment id is wrong. Appointment id should be a positive integer(number)";
        public const string AppointProgStatusInvalidValueMsg = "Value: \"{0}\"of appointment status is invalid. Appointment status should be \"NotStarted\", \"Pending\", \"Finished\"";
        public const string AppointFaultProgStatusInvalidValueMsg = "Value: \"{0}\"of appointment status is invalid. Appointment status should be \"NotStarted\", \"InProgress\", \"Complete\"";
        public const string FaultJobStatusInvalidValueMsg = "Value: \"{0}\"of appointment status is invalid. Appointment status should be \"NotStarted\", \"InProgress\", \"Started\", \"NoEntry\", \"Paused\", \"Complete\"";
        public const string SurveyourUsernameInvalidUserMsg = "Surveyour username \"{0}\" does not exist";
        public const string AppointTypeInvalidValueMsg = "Value: \"{0}\"of appointment type is invalid. Appointment type should be \"Stock\", \"Pre\", \"Post\", \"Void\"";
        public const string SurveyourStatusInvalidValueMsg = "Value: \"{0}\"of surveyour availability status is invalid. Surveyour availability status should be \"Busy\", \"Available\"";
        public const string SurveyourTypeInvalidValueMsg = "Value: \"{0}\"of survy type is invalid. Survey type status should be \"Appointments\", \"ConditionSurveys\",\"WorkOrders\"";
        public const string PropertyIdOrCustomerIdDoesNotExistMsg = "Propert id: \"{0}\"or customer id: \"{1}\" is wrong. There is no record against these in data store.";
        public const string PropertyDoesNotExistMsg = "Property Id: \"{0}\" is incorrect. There is no record against it in data store.";


        //survey
        public const string AppointmentIdDoesNotExistForPropertyMsg = "Propert id: \"{0}\"or customer id: \"{1}\" or appointment id: \"{2} \"is wrong. There is no record against these in data store.";
        public const string SurveyFormAlreadySavedForPropertyMsg = "This survey form is already saved in data store against this, appointment id: \"{0}\", propertyPortionName:\"{1}\"";

        public const string CP12IssuedNameInAS_Status = "CP12 issued";
        public const string AppointmentArrangedInAS_Status = "Arranged";
        public const string NoEntryInAS_Status = "No Entry";
        public const string AppointmentToBeArrangedStatus = "Appointment to be arranged";
        public const int GeneralCommentCategoryId = 1;
        //Change#43 - Behroz - 09/01/2013 - Start
        public const string GasItemName = "gas";
        //Change#43 - Behroz - 09/01/2013 - End

        public const string MovieExtension = ".mov";
        public const string DefaultImagePath = "~/vidthumb.jpg";
        public const string LineIdentifierForLogger = "***********";
        public const string PushNotificationSuccess = "Push notification has been sent.";
        public const string PushNotificationRejectByApple = "Push notification: Apple's server has rejected the push notification";
        public const string PushNotificationAppointmentIdDoesNotExistInDb = "Push notification: Appointment id does not exist in database";
        public const string PushNotificationDeviceTokenIsEmpty = "Push notification: Device token is empty";
    }
}
