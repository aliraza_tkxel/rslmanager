USE [ArrearsUK]
GO

/****** Object:  Table [dbo].[PS_NoEntry]    Script Date: 05/22/2012 12:05:23 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PS_NoEntry]') AND type in (N'U'))
DROP TABLE [dbo].[PS_NoEntry]
GO

USE [ArrearsUK]
GO

/****** Object:  Table [dbo].[PS_NoEntry]    Script Date: 05/22/2012 12:05:23 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[PS_NoEntry](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PropertyID] [varchar](50) NOT NULL,
	[AppointmentID] [int] NOT NULL,
	[isCardLeft] [bit] NOT NULL,
	[isGasAvailable] [bit] NOT NULL,
	[isPropertyAccessible] [bit] NOT NULL,
	[Date] [datetime] NOT NULL,
 CONSTRAINT [PK_PS_NoEntry] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


