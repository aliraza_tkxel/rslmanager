USE [ArrearsUK]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GS_IssuedReceivedBy_PS_Appointment]') AND parent_object_id = OBJECT_ID(N'[dbo].[GS_IssuedReceivedBy]'))
ALTER TABLE [dbo].[GS_IssuedReceivedBy] DROP CONSTRAINT [FK_GS_IssuedReceivedBy_PS_Appointment]
GO

USE [ArrearsUK]
GO

/****** Object:  Table [dbo].[GS_IssuedReceivedBy]    Script Date: 05/22/2012 12:04:17 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GS_IssuedReceivedBy]') AND type in (N'U'))
DROP TABLE [dbo].[GS_IssuedReceivedBy]
GO

USE [ArrearsUK]
GO

/****** Object:  Table [dbo].[GS_IssuedReceivedBy]    Script Date: 05/22/2012 12:04:17 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[GS_IssuedReceivedBy](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AppointmentID] [int] NOT NULL,
	[CP12No] [varchar](20) NOT NULL,
	[IssuedBy] [varchar](20) NOT NULL,
	[IssuedDate] [smalldatetime] NOT NULL,
	[ReceivedOnBehalf] [varchar](20) NOT NULL,
	[ReceivedDate] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_GS_IssuedReceivedBy] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[GS_IssuedReceivedBy]  WITH CHECK ADD  CONSTRAINT [FK_GS_IssuedReceivedBy_PS_Appointment] FOREIGN KEY([AppointmentID])
REFERENCES [dbo].[PS_Appointment] ([AppointId])
GO

ALTER TABLE [dbo].[GS_IssuedReceivedBy] CHECK CONSTRAINT [FK_GS_IssuedReceivedBy_PS_Appointment]
GO


