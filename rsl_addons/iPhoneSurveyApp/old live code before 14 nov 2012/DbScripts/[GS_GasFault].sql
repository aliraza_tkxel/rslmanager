USE [ArrearsUK]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GS_GasFault_PS_Appointment]') AND parent_object_id = OBJECT_ID(N'[dbo].[GS_GasFault]'))
ALTER TABLE [dbo].[GS_GasFault] DROP CONSTRAINT [FK_GS_GasFault_PS_Appointment]
GO

USE [ArrearsUK]
GO

/****** Object:  Table [dbo].[GS_GasFault]    Script Date: 05/22/2012 12:03:48 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GS_GasFault]') AND type in (N'U'))
DROP TABLE [dbo].[GS_GasFault]
GO

USE [ArrearsUK]
GO

/****** Object:  Table [dbo].[GS_GasFault]    Script Date: 05/22/2012 12:03:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[GS_GasFault](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AppointmentID] [int] NOT NULL,
	[FaultCategory] [varchar](30) NOT NULL,
	[DefectDesc] [varchar](200) NOT NULL,
	[RemedialAction] [varchar](300) NULL,
	[isAdviceNoteIssued] [bit] NOT NULL,
	[SerialNo] [varchar](50) NULL,
	[WarningTagFixed] [bit] NOT NULL,
 CONSTRAINT [PK_GS_GasFault] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[GS_GasFault]  WITH CHECK ADD  CONSTRAINT [FK_GS_GasFault_PS_Appointment] FOREIGN KEY([AppointmentID])
REFERENCES [dbo].[PS_Appointment] ([AppointId])
GO

ALTER TABLE [dbo].[GS_GasFault] CHECK CONSTRAINT [FK_GS_GasFault_PS_Appointment]
GO


