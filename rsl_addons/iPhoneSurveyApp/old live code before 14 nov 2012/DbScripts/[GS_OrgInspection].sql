USE [ArrearsUK]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GS_OrgInspection_PS_Appointment]') AND parent_object_id = OBJECT_ID(N'[dbo].[GS_OrgInspection]'))
ALTER TABLE [dbo].[GS_OrgInspection] DROP CONSTRAINT [FK_GS_OrgInspection_PS_Appointment]
GO

USE [ArrearsUK]
GO

/****** Object:  Table [dbo].[GS_OrgInspection]    Script Date: 05/22/2012 12:04:57 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GS_OrgInspection]') AND type in (N'U'))
DROP TABLE [dbo].[GS_OrgInspection]
GO

USE [ArrearsUK]
GO

/****** Object:  Table [dbo].[GS_OrgInspection]    Script Date: 05/22/2012 12:04:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[GS_OrgInspection](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AppointmentID] [int] NOT NULL,
	[OrgID] [int] NOT NULL,
	[RegNo] [varchar](20) NOT NULL,
	[GasEngineerID] [int] NOT NULL,
	[GSRENo] [varchar](20) NOT NULL,
	[Address] [varchar](100) NOT NULL,
	[City] [varchar](50) NOT NULL,
	[PostCode] [varchar](50) NOT NULL,
	[Telephone] [varchar](20) NOT NULL,
	[InspectedDate] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_GS_OrgInspection] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[GS_OrgInspection]  WITH CHECK ADD  CONSTRAINT [FK_GS_OrgInspection_PS_Appointment] FOREIGN KEY([AppointmentID])
REFERENCES [dbo].[PS_Appointment] ([AppointId])
GO

ALTER TABLE [dbo].[GS_OrgInspection] CHECK CONSTRAINT [FK_GS_OrgInspection_PS_Appointment]
GO


