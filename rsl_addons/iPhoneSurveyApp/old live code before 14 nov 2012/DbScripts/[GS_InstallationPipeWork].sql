USE [ArrearsUK]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GS_InstallationPipeWork_PS_Appointment]') AND parent_object_id = OBJECT_ID(N'[dbo].[GS_InstallationPipeWork]'))
ALTER TABLE [dbo].[GS_InstallationPipeWork] DROP CONSTRAINT [FK_GS_InstallationPipeWork_PS_Appointment]
GO

USE [ArrearsUK]
GO

/****** Object:  Table [dbo].[GS_InstallationPipeWork]    Script Date: 05/22/2012 12:04:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GS_InstallationPipeWork]') AND type in (N'U'))
DROP TABLE [dbo].[GS_InstallationPipeWork]
GO

USE [ArrearsUK]
GO

/****** Object:  Table [dbo].[GS_InstallationPipeWork]    Script Date: 05/22/2012 12:04:32 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[GS_InstallationPipeWork](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[AppointmentID] [int] NOT NULL,
	[EmergencyControl] [varchar](5) NOT NULL,
	[VisualInspection] [varchar](5) NOT NULL,
	[GasTightnessTest] [varchar](5) NOT NULL,
	[EquipotentialBonding] [varchar](5) NOT NULL,
	[Date] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_GS_InstallationPipeWork] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[GS_InstallationPipeWork]  WITH CHECK ADD  CONSTRAINT [FK_GS_InstallationPipeWork_PS_Appointment] FOREIGN KEY([AppointmentID])
REFERENCES [dbo].[PS_Appointment] ([AppointId])
GO

ALTER TABLE [dbo].[GS_InstallationPipeWork] CHECK CONSTRAINT [FK_GS_InstallationPipeWork_PS_Appointment]
GO


