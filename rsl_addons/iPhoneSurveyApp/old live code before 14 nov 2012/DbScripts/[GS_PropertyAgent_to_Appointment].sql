USE [ArrearsUK]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GS_PropertyAgent_to_Appointment_PS_Appointment]') AND parent_object_id = OBJECT_ID(N'[dbo].[GS_PropertyAgent_to_Appointment]'))
ALTER TABLE [dbo].[GS_PropertyAgent_to_Appointment] DROP CONSTRAINT [FK_GS_PropertyAgent_to_Appointment_PS_Appointment]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GS_PropertyAgent_to_Appointment_PS_PropertyAgent]') AND parent_object_id = OBJECT_ID(N'[dbo].[GS_PropertyAgent_to_Appointment]'))
ALTER TABLE [dbo].[GS_PropertyAgent_to_Appointment] DROP CONSTRAINT [FK_GS_PropertyAgent_to_Appointment_PS_PropertyAgent]
GO

USE [ArrearsUK]
GO

/****** Object:  Table [dbo].[GS_PropertyAgent_to_Appointment]    Script Date: 05/22/2012 12:02:30 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GS_PropertyAgent_to_Appointment]') AND type in (N'U'))
DROP TABLE [dbo].[GS_PropertyAgent_to_Appointment]
GO

USE [ArrearsUK]
GO

/****** Object:  Table [dbo].[GS_PropertyAgent_to_Appointment]    Script Date: 05/22/2012 12:02:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[GS_PropertyAgent_to_Appointment](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[PropertyAgentID] [int] NOT NULL,
	[AppointmentID] [int] NOT NULL,
	[Date] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_GS_PropertyAgent_to_Appointment] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[GS_PropertyAgent_to_Appointment]  WITH CHECK ADD  CONSTRAINT [FK_GS_PropertyAgent_to_Appointment_PS_Appointment] FOREIGN KEY([AppointmentID])
REFERENCES [dbo].[PS_Appointment] ([AppointId])
GO

ALTER TABLE [dbo].[GS_PropertyAgent_to_Appointment] CHECK CONSTRAINT [FK_GS_PropertyAgent_to_Appointment_PS_Appointment]
GO

ALTER TABLE [dbo].[GS_PropertyAgent_to_Appointment]  WITH CHECK ADD  CONSTRAINT [FK_GS_PropertyAgent_to_Appointment_PS_PropertyAgent] FOREIGN KEY([PropertyAgentID])
REFERENCES [dbo].[PS_PropertyAgent] ([ID])
GO

ALTER TABLE [dbo].[GS_PropertyAgent_to_Appointment] CHECK CONSTRAINT [FK_GS_PropertyAgent_to_Appointment_PS_PropertyAgent]
GO


