USE [ArrearsUK]
GO

/****** Object:  Table [dbo].[GS_ApplianceModel]    Script Date: 05/22/2012 12:05:40 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GS_ApplianceModel]') AND type in (N'U'))
DROP TABLE [dbo].[GS_ApplianceModel]
GO

USE [ArrearsUK]
GO

/****** Object:  Table [dbo].[GS_ApplianceModel]    Script Date: 05/22/2012 12:05:40 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[GS_ApplianceModel](
	[ModelID] [int] IDENTITY(1,1) NOT NULL,
	[Model] [varchar](50) NOT NULL,
 CONSTRAINT [PK_GS_ApplianceModel] PRIMARY KEY CLUSTERED 
(
	[ModelID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


