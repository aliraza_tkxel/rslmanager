﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Transactions;
using PropSurvey.Contracts.Data;
using PropSurvey.Dal.Base;
using PropSurvey.Entities;

namespace PropSurvey.Dal.Appliances
{
    public class AppliancesDal : BaseDal
    {
        #region get All Appliances
        /// <summary>
        /// This function returns all the appliances
        /// </summary>
        /// <returns>List of appliances data objects</returns>
        public List<ApplianceData> getAllAppliancesByPropertyID(string propertyID, int appointmentID)
        {
            var appliances = (from papp in context.GS_PROPERTY_APPLIANCE
                              join p2a in context.PS_Property2Appointment on papp.PROPERTYID equals p2a.PropertyId
                              join app in context.PS_Appointment on p2a.AppointId equals app.AppointId
                              join loc in context.GS_LOCATION on papp.LOCATIONID equals loc.LOCATIONID into tempLocation
                              from Locations in tempLocation.DefaultIfEmpty()
                              // Line modified by Abdul Wahhab - 05/07/2013 - START
                              join manu in context.GS_MANUFACTURER on papp.MANUFACTURERID equals manu.MANUFACTURERID into tempManufacturer
                              from Manufacturer in tempManufacturer.DefaultIfEmpty()
                              // Line modified by Abdul Wahhab - 05/07/2013 - END
                              join model in context.GS_ApplianceModel on papp.MODELID equals model.ModelID into tempModel
                              from Models in tempModel.DefaultIfEmpty()
                              //join isIns in context.GS_ApplianceInspection on app.AppointId equals isIns.APPOINTMENTID into Insp
                              //from isInspected in Insp.DefaultIfEmpty()
                              // Line modified by Abdul Wahhab - 05/07/2013 - START
                              join type in context.GS_APPLIANCE_TYPE on papp.APPLIANCETYPEID equals type.APPLIANCETYPEID into tempType
                              from ApplianceType in tempType.DefaultIfEmpty()
                              // Line modified by Abdul Wahhab - 05/07/2013 - END
                              where papp.PROPERTYID == propertyID && app.AppointId == appointmentID
                              select new ApplianceData
                              {
                                  ApplianceID = papp.PROPERTYAPPLIANCEID,
                                  //ApplianceOrgID = papp.ORGID,
                                  FluType = papp.FLUETYPE,
                                  InstalledDate = papp.DATEINSTALLED,
                                  PropertyID = propertyID,
                                  //isInspected = isInspected.APPLIANCEID != null,
                                  isLandlordAppliance = papp.ISLANDLORDAPPLIANCE,
                                  ReplacementDate = papp.REPLACEMENTDATE,
                                  Model = papp.MODEL,
                                  ApplianceLocation = new AppliancesLocationData
                                  {
                                      //Location = loc.LOCATION,
                                      //LocationID = loc.LOCATIONID
                                      Location = Locations.LOCATION != null ? Locations.LOCATION : "",
                                      LocationID = Locations.LOCATIONID != null ? Locations.LOCATIONID : 0
                                  },
                                  ApplianceManufacturer = new ManufacturerData
                                  {
                                      // Line modified by Abdul Wahhab - 05/07/2013 - START
                                      Manufacturer = Manufacturer.MANUFACTURER != null ? Manufacturer.MANUFACTURER : "",
                                      ManufacturerID = Manufacturer.MANUFACTURERID != null ? Manufacturer.MANUFACTURERID : 0
                                      // Line modified by Abdul Wahhab - 05/07/2013 - END
                                  },
                                  ApplianceModel = new ApplianceModelData
                                  {
                                      ApplianceModelID = Models.ModelID != null ? Models.ModelID : 0,
                                      ApplianceModel = Models.Model != null ? Models.Model : ""
                                  },
                                  ApplianceType = new ApplianceTypeData
                                  {
                                      // Line modified by Abdul Wahhab - 05/07/2013 - START
                                      ApplianceType = ApplianceType.APPLIANCETYPE != null ? ApplianceType.APPLIANCETYPE : "",
                                      ApplianceTypeID = ApplianceType.APPLIANCETYPEID != null ? ApplianceType.APPLIANCETYPEID : 0
                                      // Line modified by Abdul Wahhab - 05/07/2013 - END
                                  }
                              });

            List<ApplianceData> appliancesList = new List<ApplianceData>();
            if (appliances.Count() > 0)
            {
                appliancesList = appliances.ToList();
                foreach (ApplianceData appl in appliancesList)
                {
                    appl.isInspected = this.isInspected(appl.ApplianceID, appointmentID);
                }
            }

            return appliancesList;
        }

        //Change#31 - Behroz - 12/07/2012 - Start
        /// <summary>
        /// This function returns all the appliances
        /// </summary>
        /// <returns>List of appliances data objects</returns>
        //public List<ApplianceData> getAllAppliancesByPropertyIDGas(string propertyID, int appointmentID)
        //{
        //    var appliances = (from papp in context.GS_PROPERTY_APPLIANCE
        //                      join jor in context.AS_JOURNAL on papp.PROPERTYID equals jor.PROPERTYID
        //                      join app in context.AS_APPOINTMENTS on jor.JOURNALID equals app.JournalId
        //                      join loc in context.GS_LOCATION on papp.LOCATIONID equals loc.LOCATIONID                               
        //                      join manu in context.GS_MANUFACTURER on papp.MANUFACTURERID equals manu.MANUFACTURERID
        //                      join model in context.GS_ApplianceModel on papp.MODELID equals model.ModelID into tempModel
        //                      from Models in tempModel.DefaultIfEmpty()                              
        //                      join type in context.GS_APPLIANCE_TYPE on papp.APPLIANCETYPEID equals type.APPLIANCETYPEID
        //                      where papp.PROPERTYID == propertyID && app.APPOINTMENTID == appointmentID
        //                      && jor.ISCURRENT == true
        //                      select new ApplianceData
        //                      {
        //                          ApplianceID = papp.PROPERTYAPPLIANCEID,
        //                          //ApplianceOrgID = papp.ORGID,
        //                          FluType = papp.FLUETYPE,
        //                          InstalledDate = papp.DATEINSTALLED,
        //                          PropertyID = propertyID,
        //                          isLandlordAppliance = papp.ISLANDLORDAPPLIANCE,
        //                          //isInspected = isInspected.APPLIANCEID != null,
        //                          Model = papp.MODEL,
        //                          ApplianceLocation = new AppliancesLocationData
        //                          {
        //                              Location = loc.LOCATION,
        //                              LocationID = loc.LOCATIONID
        //                          },
        //                          ApplianceManufacturer = new ManufacturerData
        //                          {
        //                              Manufacturer = manu.MANUFACTURER,
        //                              ManufacturerID = manu.MANUFACTURERID
        //                          },
        //                          ApplianceModel = new ApplianceModelData
        //                          {
        //                              ApplianceModelID = Models.ModelID != null ? Models.ModelID : 0,
        //                              ApplianceModel = Models.Model != null ? Models.Model : ""
        //                          },
        //                          ApplianceType = new ApplianceTypeData
        //                          {
        //                              ApplianceType = type.APPLIANCETYPE,
        //                              ApplianceTypeID = type.APPLIANCETYPEID
        //                          }
        //                      });

        //    List<ApplianceData> appliancesList = new List<ApplianceData>();
        //    if (appliances.Count() > 0)
        //    {
        //        appliancesList = appliances.ToList();
        //        foreach (ApplianceData appl in appliancesList)
        //        {
        //            appl.isInspected = this.isInspectedGas(appl.ApplianceID);
        //        }
        //    }

        //    return appliancesList;
        //}
        //Change#31 - Behroz - 12/07/2012 - End
        //Code modified by Abdul Wahhab - 22/05/2013 - START
        public GasApplianceListData getAllAppliancesByPropertyIDGas(string propertyID, int jounalId)
        {
            var appliances = (from papp in context.GS_PROPERTY_APPLIANCE
                              join jor in context.AS_JOURNAL on papp.PROPERTYID equals jor.PROPERTYID
                              //join app in context.AS_APPOINTMENTS on jor.JOURNALID equals app.JournalId //Line commented by Abdul Wahhab - 28/05/2013
                              join loc in context.GS_LOCATION on papp.LOCATIONID equals loc.LOCATIONID into tempLocation
                              from Locations in tempLocation.DefaultIfEmpty()
                              // Line modified by Abdul Wahhab - 05/07/2013 - START
                              join manu in context.GS_MANUFACTURER on papp.MANUFACTURERID equals manu.MANUFACTURERID into tempManufacturer
                              from Manufacturer in tempManufacturer.DefaultIfEmpty()
                              // Line modified by Abdul Wahhab - 05/07/2013 - END
                              join model in context.GS_ApplianceModel on papp.MODELID equals model.ModelID into tempModel
                              from Models in tempModel.DefaultIfEmpty()
                              // Line modified by Abdul Wahhab - 05/07/2013 - START
                              join type in context.GS_APPLIANCE_TYPE on papp.APPLIANCETYPEID equals type.APPLIANCETYPEID into tempType
                              from ApplianceType in tempType.DefaultIfEmpty()
                              // Line modified by Abdul Wahhab - 05/07/2013 - END
                              where papp.PROPERTYID == propertyID && jor.JOURNALID == jounalId
                              && jor.ISCURRENT == true
                              select new ApplianceData
                              {
                                  ApplianceID = papp.PROPERTYAPPLIANCEID,
                                  //ApplianceOrgID = papp.ORGID,
                                  FluType = papp.FLUETYPE,
                                  InstalledDate = papp.DATEINSTALLED,
                                  PropertyID = propertyID,
                                  isLandlordAppliance = papp.ISLANDLORDAPPLIANCE,
                                  ReplacementDate = papp.REPLACEMENTDATE,
                                  //isInspected = isInspected.APPLIANCEID != null,
                                  Model = papp.MODEL,
                                  GCNumber = papp.GasCouncilNumber,
                                  SerialNumber = papp.SerialNumber,
                                  ApplianceLocation = new AppliancesLocationData
                                  {
                                      Location = Locations.LOCATION != null ? Locations.LOCATION : "",
                                      LocationID = Locations.LOCATIONID != null ? Locations.LOCATIONID : 0
                                  },
                                  ApplianceManufacturer = new ManufacturerData
                                  {
                                      // Line modified by Abdul Wahhab - 05/07/2013 - START
                                      Manufacturer = Manufacturer.MANUFACTURER != null ? Manufacturer.MANUFACTURER : "",
                                      ManufacturerID = Manufacturer.MANUFACTURERID != null ? Manufacturer.MANUFACTURERID : 0
                                      // Line modified by Abdul Wahhab - 05/07/2013 - END
                                  },
                                  ApplianceModel = new ApplianceModelData
                                  {
                                      ApplianceModelID = Models.ModelID != null ? Models.ModelID : 0,
                                      ApplianceModel = Models.Model != null ? Models.Model : ""
                                  },
                                  ApplianceType = new ApplianceTypeData
                                  {
                                      // Line modified by Abdul Wahhab - 05/07/2013 - START
                                      ApplianceType = ApplianceType.APPLIANCETYPE != null ? ApplianceType.APPLIANCETYPE : "",
                                      ApplianceTypeID = ApplianceType.APPLIANCETYPEID != null ? ApplianceType.APPLIANCETYPEID : 0
                                      // Line modified by Abdul Wahhab - 05/07/2013 - END
                                  }
                              });

            GasApplianceListData applianceListData = new GasApplianceListData();
            if (appliances.Count() > 0)
            {
                applianceListData.appliancesList = appliances.ToList();
                foreach (ApplianceData appl in applianceListData.appliancesList)
                {
                    appl.isInspected = this.isInspectedGas(appl.ApplianceID, jounalId);
                }
            }

            P_ATTRIBUTES propertyAttributesObject = this.getDetectorCount(propertyID);

            applianceListData.Co2Detector.detectorCount = propertyAttributesObject.Co2Detector != null ? (int)propertyAttributesObject.Co2Detector : 0;
            applianceListData.SmokeDetector.detectorCount = propertyAttributesObject.SmokeDetector != null ? (int)propertyAttributesObject.SmokeDetector : 0;

            applianceListData.Co2Detector.InspectionData = this.getCO2DetectorData(propertyID, jounalId);
            applianceListData.SmokeDetector.InspectionData = this.getSmokeDetectorData(propertyID, jounalId);

            return applianceListData;
        }
        //Code modified by Abdul Wahhab - 22/05/2013 - END
        #endregion

        #region get All Appliances Type
        /// <summary>
        /// This function returns all the Appliances Type
        /// </summary>
        /// <returns>List of Appliances Type data objects</returns>
        public List<ApplianceTypeData> getAllApplianceTypes()
        {
            var applianceType = (from type in context.GS_APPLIANCE_TYPE
                                 select new ApplianceTypeData
                                 {
                                     ApplianceTypeID = type.APPLIANCETYPEID,
                                     ApplianceType = type.APPLIANCETYPE
                                 });

            List<ApplianceTypeData> appliancesTypeList = new List<ApplianceTypeData>();
            if (applianceType.Count() > 0)
            {
                appliancesTypeList = applianceType.ToList();
            }

            return appliancesTypeList;
        }
        #endregion

        #region get All Appliances Models
        /// <summary>
        /// This function returns all the Appliances Model
        /// </summary>
        /// <returns>List of Appliances Model data objects</returns>
        public List<ApplianceModelData> getAllApplianceModel()
        {
            var applianceType = (from model in context.GS_ApplianceModel
                                 select new ApplianceModelData
                                 {
                                     ApplianceModelID = model.ModelID,
                                     ApplianceModel = model.Model
                                 });

            List<ApplianceModelData> appliancesTypeList = new List<ApplianceModelData>();
            if (applianceType.Count() > 0)
            {
                appliancesTypeList = applianceType.ToList();
            }

            return appliancesTypeList;
        }
        #endregion

        #region get All Appliances Manufacturer
        /// <summary>
        /// This function returns all the Appliances Manufacturer
        /// </summary>
        /// <returns>List of Appliances Manufacturer data objects</returns>
        public List<ManufacturerData> getAllApplianceManufacturer()
        {
            var applianceType = (from manufactur in context.GS_MANUFACTURER
                                 orderby manufactur.MANUFACTURER
                                 select new ManufacturerData
                                 {
                                     ManufacturerID = manufactur.MANUFACTURERID,
                                     Manufacturer = manufactur.MANUFACTURER
                                 });

            List<ManufacturerData> appliancesTypeList = new List<ManufacturerData>();
            if (applianceType.Count() > 0)
            {
                appliancesTypeList = applianceType.ToList();
            }

            return appliancesTypeList;
        }
        #endregion

        #region get All Appliances Locations
        /// <summary>
        /// This function returns all the Appliances Location
        /// </summary>
        /// <returns>List of Appliances Location data objects</returns>
        public List<AppliancesLocationData> getAllApplianceLocations()
        {
            var applianceType = (from location in context.GS_LOCATION
                                 select new AppliancesLocationData
                                 {
                                     LocationID = location.LOCATIONID,
                                     Location = location.LOCATION
                                 });

            List<AppliancesLocationData> appliancesTypeList = new List<AppliancesLocationData>();
            if (applianceType.Count() > 0)
            {
                appliancesTypeList = applianceType.ToList();
            }

            return appliancesTypeList;
        }
        #endregion

        #region Save a new Appliance
        /// <summary>
        /// This function saves a new appliance
        /// </summary>
        /// <param name="appData">appliances data objects</param>
        /// <returns>ID in case of success</returns>
        public int saveAppliance(ApplianceData appData)
        {
            bool success = false;
            GS_PROPERTY_APPLIANCE appliance = new GS_PROPERTY_APPLIANCE();

            using (TransactionScope trans = new TransactionScope())
            {
                appliance.PROPERTYID = appData.PropertyID;
                appliance.DATEINSTALLED = appData.InstalledDate;
                appliance.FLUETYPE = appData.FluType;

                if (appData.ApplianceLocation != null)
                {
                    appliance.LOCATIONID = appData.ApplianceLocation.LocationID;
                }

                if (appData.ApplianceModel != null)
                {
                    appliance.MODELID = appData.ApplianceModel.ApplianceModelID;
                }

                if (appData.ApplianceManufacturer != null)
                {
                    appliance.MANUFACTURERID = appData.ApplianceManufacturer.ManufacturerID;
                }

                if (appData.ApplianceType != null)
                {
                    appliance.APPLIANCETYPEID = appData.ApplianceType.ApplianceTypeID;
                }

                //Change#27 - Behroz - 20/12/2012 - Start
                appliance.MODEL = appData.Model;
                appliance.ISLANDLORDAPPLIANCE = appData.isLandlordAppliance;
                appliance.REPLACEMENTDATE = appData.ReplacementDate;
                //Change#27 - Behroz - 20/12/2012 - End
                // Code added by Abdul Wahhab 15/05/2013 - START
                appliance.SerialNumber = appData.SerialNumber;
                appliance.GasCouncilNumber = appData.GCNumber;
                // Code added by Abdul Wahhab 15/05/2013 - END
                context.AddToGS_PROPERTY_APPLIANCE(appliance);
                context.SaveChanges();
                trans.Complete();
                success = true;
            }

            if (success == true)
            {
                return appliance.PROPERTYAPPLIANCEID;
            }
            else
            {
                return 0;
            }
        }
        #endregion

        #region Update a existing Appliance
        /// <summary>
        /// This function updates a existing appliance
        /// </summary>
        /// <param name="appData">appliances data objects</param>
        /// <returns>ID in case of success</returns>
        public bool updateAppliance(ApplianceData appData)
        {
            bool success = false;

            using (TransactionScope trans = new TransactionScope())
            {
                var appl = context.GS_PROPERTY_APPLIANCE.Where(app => app.PROPERTYAPPLIANCEID == appData.ApplianceID);
                if (appl.Count() > 0)
                {
                    GS_PROPERTY_APPLIANCE appliance = appl.First();

                    appliance.PROPERTYID = appData.PropertyID;
                    appliance.DATEINSTALLED = appData.InstalledDate;
                    appliance.FLUETYPE = appData.FluType;

                    if (appData.ApplianceLocation != null)
                    {
                        appliance.LOCATIONID = appData.ApplianceLocation.LocationID;
                    }

                    if (appData.ApplianceModel != null)
                    {
                        appliance.MODELID = appData.ApplianceModel.ApplianceModelID;
                    }

                    if (appData.ApplianceManufacturer != null)
                    {
                        appliance.MANUFACTURERID = appData.ApplianceManufacturer.ManufacturerID;
                    }

                    if (appData.ApplianceType != null)
                    {
                        appliance.APPLIANCETYPEID = appData.ApplianceType.ApplianceTypeID;
                    }

                    //Change#27 - Behroz - 20/12/2012 - Start
                    appliance.MODEL = appData.Model;
                    appliance.ISLANDLORDAPPLIANCE = appData.isLandlordAppliance;
                    appliance.REPLACEMENTDATE = appData.ReplacementDate;
                    //Change#27 - Behroz - 20/12/2012 - End
                    // Code added by Abdul Wahhab 15/05/2013 - START
                    appliance.SerialNumber = appData.SerialNumber;
                    appliance.GasCouncilNumber = appData.GCNumber;
                    // Code added by Abdul Wahhab 15/05/2013 - END
                    //if (appData.ApplianceOrgID == 0)
                    //{
                    //    appliance.ORGID = null;
                    //}
                    //else
                    //{
                    //    appliance.ORGID = appData.ApplianceOrgID;
                    //}

                    context.SaveChanges();
                    trans.Complete();
                    success = true;
                }
            }
            return success;
        }
        #endregion

        #region save new Appliances Locations
        /// <summary>
        /// This function save new Appliances Location
        /// </summary>
        /// <returns>true for success</returns>
        public int saveApplianceLocation(AppliancesLocationData location)
        {
            bool success = false;
            GS_LOCATION app_location = new GS_LOCATION();

            using (TransactionScope trans = new TransactionScope())
            {
                app_location.LOCATION = location.Location;
                context.AddToGS_LOCATION(app_location);
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                trans.Complete();
                success = true;
            }

            if (success == true)
            {
                return app_location.LOCATIONID;
            }
            else
            {
                return 0;
            }
        }
        #endregion

        #region save new Appliances Type
        /// <summary>
        /// This function save new Appliances Type
        /// </summary>
        /// <returns>new inserted id in case of success</returns>
        public int saveApplianceType(ApplianceTypeData type)
        {
            bool success = false;
            GS_APPLIANCE_TYPE app_type = new GS_APPLIANCE_TYPE();

            using (TransactionScope trans = new TransactionScope())
            {
                app_type.APPLIANCETYPE = type.ApplianceType;
                context.AddToGS_APPLIANCE_TYPE(app_type);
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                trans.Complete();
                success = true;
            }

            if (success == true)
            {
                return app_type.APPLIANCETYPEID;
            }
            else
            {
                return 0;
            }
        }
        #endregion

        #region save new Appliances Model
        /// <summary>
        /// This function save new Appliances Model
        /// </summary>
        /// <returns>new inserted id in case of success</returns>
        public int saveApplianceModel(ApplianceModelData model)
        {
            bool success = false;
            GS_ApplianceModel app_model = new GS_ApplianceModel();

            using (TransactionScope trans = new TransactionScope())
            {
                app_model.Model = model.ApplianceModel;
                context.AddToGS_ApplianceModel(app_model);
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                trans.Complete();
                success = true;
            }

            if (success == true)
            {
                return app_model.ModelID;
            }
            else
            {
                return 0;
            }
        }
        #endregion

        #region save new Appliances manufacturer
        /// <summary>
        /// This function save new Appliances manufacturer
        /// </summary>
        /// <returns>new inserted id in case of success</returns>
        public int saveApplianceManufacturer(ManufacturerData manufacture)
        {
            bool success = false;
            GS_MANUFACTURER app_manufacture = new GS_MANUFACTURER();

            using (TransactionScope trans = new TransactionScope())
            {
                app_manufacture.MANUFACTURER = manufacture.Manufacturer;
                context.AddToGS_MANUFACTURER(app_manufacture);
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                trans.Complete();
                success = true;
            }

            if (success == true)
            {
                return app_manufacture.MANUFACTURERID;
            }
            else
            {
                return 0;
            }
        }
        #endregion

        #region get number of inspected Appliances

        /// <summary>
        /// This function returns number of inspected Appliances
        /// </summary>
        /// <returns>number of inspected Appliances</returns>
        public int getInspectedAppliances(int appointmentID)
        {
            var count = (from app in context.GS_ApplianceInspection
                         where app.APPOINTMENTID == appointmentID
                         select app.APPLIANCEINSPECTIONID);
            return count.Count();
        }

        //Change#25 - Behroz - 20/12/2012 - Start
        /// <summary>
        /// This function returns number of inspected Appliances for gas
        /// </summary>
        /// <returns>number of inspected Appliances</returns>
        //public int getInspectedAppliances(string propertyID)
        //{
        //    var count = (from app in context.GS_PROPERTY_APPLIANCE
        //                 where app.PROPERTYID == propertyID
        //                 select app.PROPERTYAPPLIANCEID);

        //    return count.Count();
        //}
        public int getInspectedAppliancesGas(int journalId)
        {
            var count = (from app in context.P_APPLIANCE_INSPECTION
                         where app.Journalid == journalId
                         select app.PROPERTYAPPLIANCEID);

            return count.Count();
        }
        //Change#25 - Behroz - 20/12/2012 - End

        #endregion

        #region get ApplianceInspection by ApplianceID
        /// <summary>
        /// This function returns all the ApplianceInspection
        /// </summary>
        /// <returns>List of ApplianceInspection data objects</returns>
        public ApplianceInspectionData getApplianceInspectionByApplianceID(int ApplianceID, int appointmentID)
        {
            var appliances = (from AppIns in context.GS_ApplianceInspection
                              where AppIns.APPLIANCEID == ApplianceID && AppIns.APPOINTMENTID == appointmentID
                              select new ApplianceInspectionData
                              {
                                  ADEQUATEVENTILATION = AppIns.ADEQUATEVENTILATION,
                                  APPLIANCEID = AppIns.APPLIANCEID,
                                  APPLIANCEINSPECTIONID = AppIns.APPLIANCEINSPECTIONID,
                                  APPLIANCESAFETOUSE = AppIns.APPLIANCESAFETOUSE,
                                  APPLIANCESERVICED = AppIns.APPLIANCESERVICED,
                                  APPOINTMENTID = AppIns.APPOINTMENTID,
                                  COMBUSTIONREADING = AppIns.COMBUSTIONREADING,
                                  FLUEPERFORMANCECHECKS = AppIns.FLUEPERFORMANCECHECKS,
                                  FLUEVISUALCONDITION = AppIns.FLUEVISUALCONDITION,
                                  INSPECTIONDATE = AppIns.INSPECTIONDATE,
                                  OPERATINGPRESSURE = AppIns.OPERATINGPRESSURE,
                                  ISINSPECTED = AppIns.ISINSPECTED,
                                  SAFETYDEVICEOPERATIONAL = AppIns.SAFETYDEVICEOPERATIONAL,
                                  SATISFACTORYTERMINATION = AppIns.SATISFACTORYTERMINATION,
                                  SMOKEPELLET = AppIns.SMOKEPELLET,
                                  SPILLAGETEST = AppIns.SPILLAGETEST
                              });

            ApplianceInspectionData appliancesList = new ApplianceInspectionData();
            if (appliances.Count() > 0)
            {
                appliancesList = appliances.First();
            }

            return appliancesList;
        }

        //Change#26 - Behroz - 20/12/2012 - Start
        /// <summary>
        /// This function returns all the ApplianceInspection for gas
        /// </summary>
        /// <returns>List of ApplianceInspection data objects</returns>
        //public ApplianceInspectionData getApplianceInspectionByApplianceID(int ApplianceID, string propertyID)
        //{
        //    var appliance = (from propApp in context.GS_PROPERTY_APPLIANCE
        //                     join AppIns in context.P_APPLIANCE_INSPECTION on
        //                     propApp.PROPERTYAPPLIANCEID equals AppIns.PROPERTYAPPLIANCEID
        //                     where propApp.PROPERTYAPPLIANCEID == ApplianceID && propApp.PROPERTYID == propertyID
        //                     select new ApplianceInspectionData {
        //                         ADEQUATEVENTILATION = AppIns.ADEQUATEVENTILATION,
        //                         APPLIANCEID = AppIns.PROPERTYAPPLIANCEID,
        //                         APPLIANCEINSPECTIONID = AppIns.APPLIANCEINSPECTIONID,
        //                         APPLIANCESAFETOUSE = AppIns.APPLIANCESAFETOUSE,
        //                         APPLIANCESERVICED = AppIns.APPLIANCESERVICED,                                 
        //                         COMBUSTIONREADING = AppIns.COMBUSTIONREADING,
        //                         FLUEPERFORMANCECHECKS = AppIns.FLUEPERFORMANCECHECKS,
        //                         FLUEVISUALCONDITION = AppIns.FLUEVISUALCONDITION,
        //                         INSPECTIONDATE = AppIns.INSPECTIONDATE,
        //                         OPERATINGPRESSURE = AppIns.OPERATINGPRESSURE,
        //                         ISINSPECTED = AppIns.ISINSPECTED,
        //                         SAFETYDEVICEOPERATIONAL = AppIns.SAFETYDEVICEOPERATIONAL,
        //                         SATISFACTORYTERMINATION = AppIns.SATISFACTORYTERMINATION,
        //                         SMOKEPELLET = AppIns.SMOKEPELLET,
        //                         SPILLAGETEST = AppIns.SPILLAGETEST,
        //                         INSPECTEDBY = AppIns.INSPECTEDBY.Value                                 
        //                     });            

        //    ApplianceInspectionData appliancesList = new ApplianceInspectionData();
        //    if (appliance.Count() > 0)
        //    {
        //        appliancesList = appliance.First();
        //    }

        //    return appliancesList;
        //}
        public ApplianceInspectionData getApplianceInspectionByApplianceIDGas(int ApplianceID, int journalId)
        {
            var appliance = (from AppIns in context.P_APPLIANCE_INSPECTION
                             where AppIns.PROPERTYAPPLIANCEID == ApplianceID && AppIns.Journalid == journalId
                             select new ApplianceInspectionData
                             {
                                 ADEQUATEVENTILATION = AppIns.ADEQUATEVENTILATION,
                                 APPLIANCEID = AppIns.PROPERTYAPPLIANCEID,
                                 APPLIANCEINSPECTIONID = AppIns.APPLIANCEINSPECTIONID,
                                 APPLIANCESAFETOUSE = AppIns.APPLIANCESAFETOUSE,
                                 APPLIANCESERVICED = AppIns.APPLIANCESERVICED,
                                 COMBUSTIONREADING = AppIns.COMBUSTIONREADING,
                                 FLUEPERFORMANCECHECKS = AppIns.FLUEPERFORMANCECHECKS,
                                 FLUEVISUALCONDITION = AppIns.FLUEVISUALCONDITION,
                                 INSPECTIONDATE = AppIns.INSPECTIONDATE,
                                 OPERATINGPRESSURE = AppIns.OPERATINGPRESSURE,
                                 ISINSPECTED = AppIns.ISINSPECTED,
                                 SAFETYDEVICEOPERATIONAL = AppIns.SAFETYDEVICEOPERATIONAL,
                                 SATISFACTORYTERMINATION = AppIns.SATISFACTORYTERMINATION,
                                 SMOKEPELLET = AppIns.SMOKEPELLET,
                                 SPILLAGETEST = AppIns.SPILLAGETEST,
                                 INSPECTEDBY = AppIns.INSPECTEDBY.Value
                             });

            ApplianceInspectionData appliancesList = new ApplianceInspectionData();
            if (appliance.Count() > 0)
            {
                appliancesList = appliance.First();
            }

            return appliancesList;
        }
        //Change#26 - Behroz - 20/12/2012 - End

        #endregion

        #region get ApplianceInspection details by Appointment Id
        /// <summary>
        /// This function returns all the ApplianceInspection details
        /// </summary>
        /// <returns>List of ApplianceInspection data objects</returns>
        public List<ApplianceInspectionData> getApplianceInspectionDetails(int appointmentID)
        {
            var appliances = (from AppIns in context.GS_ApplianceInspection
                              where AppIns.APPOINTMENTID == appointmentID
                              select new ApplianceInspectionData
                              {
                                  ADEQUATEVENTILATION = AppIns.ADEQUATEVENTILATION,
                                  APPLIANCEID = AppIns.APPLIANCEID,
                                  APPLIANCEINSPECTIONID = AppIns.APPLIANCEINSPECTIONID,
                                  APPLIANCESAFETOUSE = AppIns.APPLIANCESAFETOUSE,
                                  APPLIANCESERVICED = AppIns.APPLIANCESERVICED,
                                  APPOINTMENTID = AppIns.APPOINTMENTID,
                                  COMBUSTIONREADING = AppIns.COMBUSTIONREADING,
                                  FLUEPERFORMANCECHECKS = AppIns.FLUEPERFORMANCECHECKS,
                                  FLUEVISUALCONDITION = AppIns.FLUEVISUALCONDITION,
                                  INSPECTIONDATE = AppIns.INSPECTIONDATE,
                                  OPERATINGPRESSURE = AppIns.OPERATINGPRESSURE,
                                  ISINSPECTED = AppIns.ISINSPECTED,
                                  SAFETYDEVICEOPERATIONAL = AppIns.SAFETYDEVICEOPERATIONAL,
                                  SATISFACTORYTERMINATION = AppIns.SATISFACTORYTERMINATION,
                                  SMOKEPELLET = AppIns.SMOKEPELLET,
                                  SPILLAGETEST = AppIns.SPILLAGETEST
                              });

            List<ApplianceInspectionData> appliancesList = new List<ApplianceInspectionData>();
            if (appliances.Count() > 0)
            {
                appliancesList = appliances.ToList();
            }

            return appliancesList;
        }

        //Change#40 - Behroz - 26/12/2012 - Start
        ///// <summary>
        ///// This function returns all the ApplianceInspection details
        ///// </summary>
        ///// <returns>List of ApplianceInspection data objects</returns>
        //public List<ApplianceInspectionData> getApplianceInspectionDetails(string propertyID)
        //{
        //    var appliances = (from propApp in context.GS_PROPERTY_APPLIANCE
        //                      join AppIns in context.P_APPLIANCE_INSPECTION on
        //                      propApp.PROPERTYAPPLIANCEID equals AppIns.PROPERTYAPPLIANCEID
        //                      where propApp.PROPERTYID == propertyID
        //                      select new ApplianceInspectionData
        //                      {
        //                          ADEQUATEVENTILATION = AppIns.ADEQUATEVENTILATION,
        //                          APPLIANCEID = AppIns.PROPERTYAPPLIANCEID,
        //                          APPLIANCEINSPECTIONID = AppIns.APPLIANCEINSPECTIONID,
        //                          APPLIANCESAFETOUSE = AppIns.APPLIANCESAFETOUSE,
        //                          APPLIANCESERVICED = AppIns.APPLIANCESERVICED,
        //                          COMBUSTIONREADING = AppIns.COMBUSTIONREADING,
        //                          FLUEPERFORMANCECHECKS = AppIns.FLUEPERFORMANCECHECKS,
        //                          FLUEVISUALCONDITION = AppIns.FLUEVISUALCONDITION,
        //                          INSPECTIONDATE = AppIns.INSPECTIONDATE,
        //                          OPERATINGPRESSURE = AppIns.OPERATINGPRESSURE,
        //                          ISINSPECTED = AppIns.ISINSPECTED,
        //                          SAFETYDEVICEOPERATIONAL = AppIns.SAFETYDEVICEOPERATIONAL,
        //                          SATISFACTORYTERMINATION = AppIns.SATISFACTORYTERMINATION,
        //                          SMOKEPELLET = AppIns.SMOKEPELLET,
        //                          SPILLAGETEST = AppIns.SPILLAGETEST,
        //                          INSPECTEDBY = AppIns.INSPECTEDBY.Value  
        //                      });

        //    List<ApplianceInspectionData> appliancesList = new List<ApplianceInspectionData>();
        //    if (appliances.Count() > 0)
        //    {
        //        appliancesList = appliances.ToList();
        //    }

        //    return appliancesList;
        //}
        //Change#40 - Behroz - 26/12/2012 - End
        /// <summary>
        /// This function returns all the ApplianceInspection details
        /// </summary>
        /// <returns>List of ApplianceInspection data objects</returns>
        public List<ApplianceInspectionData> getApplianceInspectionDetailsGas(int journalId)
        {
            var appliances = (from AppIns in context.P_APPLIANCE_INSPECTION
                              where AppIns.Journalid == journalId
                              select new ApplianceInspectionData
                              {
                                  ADEQUATEVENTILATION = AppIns.ADEQUATEVENTILATION,
                                  APPLIANCEID = AppIns.PROPERTYAPPLIANCEID,
                                  APPLIANCEINSPECTIONID = AppIns.APPLIANCEINSPECTIONID,
                                  APPLIANCESAFETOUSE = AppIns.APPLIANCESAFETOUSE,
                                  APPLIANCESERVICED = AppIns.APPLIANCESERVICED,
                                  COMBUSTIONREADING = AppIns.COMBUSTIONREADING,
                                  FLUEPERFORMANCECHECKS = AppIns.FLUEPERFORMANCECHECKS,
                                  FLUEVISUALCONDITION = AppIns.FLUEVISUALCONDITION,
                                  INSPECTIONDATE = AppIns.INSPECTIONDATE,
                                  OPERATINGPRESSURE = AppIns.OPERATINGPRESSURE,
                                  ISINSPECTED = AppIns.ISINSPECTED,
                                  SAFETYDEVICEOPERATIONAL = AppIns.SAFETYDEVICEOPERATIONAL,
                                  SATISFACTORYTERMINATION = AppIns.SATISFACTORYTERMINATION,
                                  SMOKEPELLET = AppIns.SMOKEPELLET,
                                  SPILLAGETEST = AppIns.SPILLAGETEST,
                                  INSPECTEDBY = AppIns.INSPECTEDBY.Value,
                                  JOURNALID = AppIns.Journalid
                              });

            List<ApplianceInspectionData> appliancesList = new List<ApplianceInspectionData>();
            if (appliances.Count() > 0)
            {
                appliancesList = appliances.ToList();
            }

            return appliancesList;
        }
        #endregion

        #region Save a new ApplianceInspection
        /// <summary>
        /// This function saves a new ApplianceInspection
        /// </summary>
        /// <param name="appData">ApplianceInspection data objects</param>
        /// <returns>ID in case of success</returns>
        public int saveApplianceInspection(ApplianceInspectionData appData)
        {
            bool success = false;
            GS_ApplianceInspection appliance = new GS_ApplianceInspection();

            using (TransactionScope trans = new TransactionScope())
            {
                appliance.ADEQUATEVENTILATION = appData.ADEQUATEVENTILATION;
                appliance.APPLIANCEID = appData.APPLIANCEID;
                //appliance.APPLIANCEINSPECTIONID = appData.APPLIANCEINSPECTIONID;
                appliance.APPLIANCESAFETOUSE = appData.APPLIANCESAFETOUSE;
                appliance.APPLIANCESERVICED = appData.APPLIANCESERVICED;
                appliance.APPOINTMENTID = appData.APPOINTMENTID;
                appliance.COMBUSTIONREADING = appData.COMBUSTIONREADING;
                appliance.FLUEPERFORMANCECHECKS = appData.FLUEPERFORMANCECHECKS;
                appliance.FLUEVISUALCONDITION = appData.FLUEVISUALCONDITION;
                appliance.INSPECTIONDATE = appData.INSPECTIONDATE;
                appliance.OPERATINGPRESSURE = appData.OPERATINGPRESSURE;
                appliance.ISINSPECTED = appData.ISINSPECTED;
                appliance.SAFETYDEVICEOPERATIONAL = appData.SAFETYDEVICEOPERATIONAL;
                appliance.SATISFACTORYTERMINATION = appData.SATISFACTORYTERMINATION;
                appliance.SMOKEPELLET = appData.SMOKEPELLET;
                appliance.SPILLAGETEST = appData.SPILLAGETEST;

                context.AddToGS_ApplianceInspection(appliance);
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                trans.Complete();
                success = true;
            }

            if (success == true)
            {
                return appliance.APPLIANCEINSPECTIONID;
            }
            else
            {
                return 0;
            }
        }

        //Change#24 - Behroz - 20/12/2012 - Start
        /// <summary>
        /// This function saves a new ApplianceInspection
        /// </summary>
        /// <param name="appData">ApplianceInspection data objects</param>
        /// <returns>ID in case of success</returns>
        //public int saveApplianceInspectionGas(ApplianceInspectionData appData)
        //{
        //    bool success = false;            
        //    P_APPLIANCE_INSPECTION appliance = new P_APPLIANCE_INSPECTION();

        //    var PropertyApplianceExist = context.P_APPLIANCE_INSPECTION.Where(app => app.PROPERTYAPPLIANCEID == appData.APPLIANCEID);

        //    if (PropertyApplianceExist.Count() > 0)
        //    {
        //        appliance = PropertyApplianceExist.First();

        //        #region "Update"

        //        using (TransactionScope trans = new TransactionScope())
        //        {
        //            appliance.ADEQUATEVENTILATION = appData.ADEQUATEVENTILATION;
        //            appliance.PROPERTYAPPLIANCEID = appData.APPLIANCEID;
        //            appliance.APPLIANCESAFETOUSE = appData.APPLIANCESAFETOUSE;
        //            appliance.APPLIANCESERVICED = appData.APPLIANCESERVICED;                    
        //            appliance.COMBUSTIONREADING = appData.COMBUSTIONREADING;
        //            appliance.FLUEPERFORMANCECHECKS = appData.FLUEPERFORMANCECHECKS;
        //            appliance.FLUEVISUALCONDITION = appData.FLUEVISUALCONDITION;
        //            appliance.INSPECTIONDATE = appData.INSPECTIONDATE;
        //            appliance.OPERATINGPRESSURE = appData.OPERATINGPRESSURE;
        //            appliance.ISINSPECTED = appData.ISINSPECTED;
        //            appliance.SAFETYDEVICEOPERATIONAL = appData.SAFETYDEVICEOPERATIONAL;
        //            appliance.SATISFACTORYTERMINATION = appData.SATISFACTORYTERMINATION;
        //            appliance.SMOKEPELLET = appData.SMOKEPELLET;
        //            appliance.SPILLAGETEST = appData.SPILLAGETEST;
        //            appliance.INSPECTEDBY = appData.INSPECTEDBY;

        //            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
        //            trans.Complete();                    
        //        }
        //        success = true;

        //        if (success)
        //            return 1;
        //        else return 0;

        //        #endregion
        //    }
        //    else
        //    {
        //        #region "Insert"

        //        //Insert if the Property Appliance Id does not exist
        //        using (TransactionScope trans = new TransactionScope())
        //        {
        //            appliance.ADEQUATEVENTILATION = appData.ADEQUATEVENTILATION;
        //            appliance.PROPERTYAPPLIANCEID = appData.APPLIANCEID;
        //            appliance.APPLIANCESAFETOUSE = appData.APPLIANCESAFETOUSE;
        //            appliance.APPLIANCESERVICED = appData.APPLIANCESERVICED;
        //            appliance.COMBUSTIONREADING = appData.COMBUSTIONREADING;
        //            appliance.FLUEPERFORMANCECHECKS = appData.FLUEPERFORMANCECHECKS;
        //            appliance.FLUEVISUALCONDITION = appData.FLUEVISUALCONDITION;
        //            appliance.INSPECTIONDATE = appData.INSPECTIONDATE;
        //            appliance.OPERATINGPRESSURE = appData.OPERATINGPRESSURE;
        //            appliance.ISINSPECTED = appData.ISINSPECTED;
        //            appliance.SAFETYDEVICEOPERATIONAL = appData.SAFETYDEVICEOPERATIONAL;
        //            appliance.SATISFACTORYTERMINATION = appData.SATISFACTORYTERMINATION;
        //            appliance.SMOKEPELLET = appData.SMOKEPELLET;
        //            appliance.SPILLAGETEST = appData.SPILLAGETEST;
        //            appliance.INSPECTEDBY = appData.INSPECTEDBY;

        //            context.AddToP_APPLIANCE_INSPECTION(appliance);
        //            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
        //            trans.Complete();
        //            success = true;
        //        }

        //        if (success == true)
        //        {
        //            return appliance.APPLIANCEINSPECTIONID;
        //        }
        //        else
        //        {
        //            return 0;
        //        }

        //        #endregion
        //    }            
        //}

        //Change#24 - Behroz - 20/12/2012 - End
        public int saveApplianceInspectionGas(ApplianceInspectionData appData)
        {
            bool success = false;
            P_APPLIANCE_INSPECTION appliance = new P_APPLIANCE_INSPECTION();

            //var PropertyApplianceExist = context.P_APPLIANCE_INSPECTION.Where(app => app.PROPERTYAPPLIANCEID == appData.APPLIANCEID);
            var PropertyApplianceExist = context.P_APPLIANCE_INSPECTION.Where(app => app.PROPERTYAPPLIANCEID == appData.APPLIANCEID && app.Journalid == appData.JOURNALID);

            if (PropertyApplianceExist.Count() > 0)
            {
                appliance = PropertyApplianceExist.First();

                #region "Update"

                using (TransactionScope trans = new TransactionScope())
                {
                    appliance.ADEQUATEVENTILATION = appData.ADEQUATEVENTILATION;
                    appliance.PROPERTYAPPLIANCEID = appData.APPLIANCEID;
                    appliance.APPLIANCESAFETOUSE = appData.APPLIANCESAFETOUSE;
                    appliance.APPLIANCESERVICED = appData.APPLIANCESERVICED;
                    appliance.COMBUSTIONREADING = appData.COMBUSTIONREADING;
                    appliance.FLUEPERFORMANCECHECKS = appData.FLUEPERFORMANCECHECKS;
                    appliance.FLUEVISUALCONDITION = appData.FLUEVISUALCONDITION;
                    appliance.INSPECTIONDATE = appData.INSPECTIONDATE;
                    appliance.OPERATINGPRESSURE = appData.OPERATINGPRESSURE;
                    appliance.ISINSPECTED = appData.ISINSPECTED;
                    appliance.SAFETYDEVICEOPERATIONAL = appData.SAFETYDEVICEOPERATIONAL;
                    appliance.SATISFACTORYTERMINATION = appData.SATISFACTORYTERMINATION;
                    appliance.SMOKEPELLET = appData.SMOKEPELLET;
                    appliance.SPILLAGETEST = appData.SPILLAGETEST;
                    appliance.INSPECTEDBY = appData.INSPECTEDBY;
                    appliance.Journalid = appData.JOURNALID;

                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    trans.Complete();
                }
                success = true;

                if (success)
                    return 1;
                else return 0;

                #endregion
            }
            else
            {
                #region "Insert"

                //Insert if the Property Appliance Id does not exist
                using (TransactionScope trans = new TransactionScope())
                {
                    appliance.ADEQUATEVENTILATION = appData.ADEQUATEVENTILATION;
                    appliance.PROPERTYAPPLIANCEID = appData.APPLIANCEID;
                    appliance.APPLIANCESAFETOUSE = appData.APPLIANCESAFETOUSE;
                    appliance.APPLIANCESERVICED = appData.APPLIANCESERVICED;
                    appliance.COMBUSTIONREADING = appData.COMBUSTIONREADING;
                    appliance.FLUEPERFORMANCECHECKS = appData.FLUEPERFORMANCECHECKS;
                    appliance.FLUEVISUALCONDITION = appData.FLUEVISUALCONDITION;
                    appliance.INSPECTIONDATE = appData.INSPECTIONDATE;
                    appliance.OPERATINGPRESSURE = appData.OPERATINGPRESSURE;
                    appliance.ISINSPECTED = appData.ISINSPECTED;
                    appliance.SAFETYDEVICEOPERATIONAL = appData.SAFETYDEVICEOPERATIONAL;
                    appliance.SATISFACTORYTERMINATION = appData.SATISFACTORYTERMINATION;
                    appliance.SMOKEPELLET = appData.SMOKEPELLET;
                    appliance.SPILLAGETEST = appData.SPILLAGETEST;
                    appliance.INSPECTEDBY = appData.INSPECTEDBY;
                    appliance.Journalid = appData.JOURNALID;

                    context.AddToP_APPLIANCE_INSPECTION(appliance);
                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    trans.Complete();
                    success = true;
                }

                if (success == true)
                {
                    return appliance.APPLIANCEINSPECTIONID;
                }
                else
                {
                    return 0;
                }

                #endregion
            }
        }
        #endregion

        #region Update a existing ApplianceInspection
        /// <summary>
        /// This function updates a existing ApplianceInspection
        /// </summary>
        /// <param name="appData">ApplianceInspection data objects</param>
        /// <returns>ID in case of success</returns>
        public bool updateApplianceInspection(ApplianceInspectionData appData)
        {
            bool success = false;

            using (TransactionScope trans = new TransactionScope())
            {
                var appl = context.GS_ApplianceInspection.Where(app => app.APPLIANCEINSPECTIONID == appData.APPLIANCEINSPECTIONID);
                if (appl.Count() > 0)
                {
                    GS_ApplianceInspection appliance = appl.First();

                    appliance.ADEQUATEVENTILATION = appData.ADEQUATEVENTILATION;
                    appliance.APPLIANCEID = appData.APPLIANCEID;
                    appliance.APPLIANCESAFETOUSE = appData.APPLIANCESAFETOUSE;
                    appliance.APPLIANCESERVICED = appData.APPLIANCESERVICED;
                    appliance.APPOINTMENTID = appData.APPOINTMENTID;
                    appliance.COMBUSTIONREADING = appData.COMBUSTIONREADING;
                    appliance.FLUEPERFORMANCECHECKS = appData.FLUEPERFORMANCECHECKS;
                    appliance.FLUEVISUALCONDITION = appData.FLUEVISUALCONDITION;
                    appliance.INSPECTIONDATE = appData.INSPECTIONDATE;
                    appliance.OPERATINGPRESSURE = appData.OPERATINGPRESSURE;
                    appliance.ISINSPECTED = appData.ISINSPECTED;
                    appliance.SAFETYDEVICEOPERATIONAL = appData.SAFETYDEVICEOPERATIONAL;
                    appliance.SATISFACTORYTERMINATION = appData.SATISFACTORYTERMINATION;
                    appliance.SMOKEPELLET = appData.SMOKEPELLET;
                    appliance.SPILLAGETEST = appData.SPILLAGETEST;

                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    trans.Complete();
                    success = true;
                }
            }
            return success;
        }
        #endregion

        #region "Get Appliance Count"

        //Change#50 - Behroz - 16/01/2013 - Start
        /// <summary>
        /// This function returns the appliance count 
        /// </summary>
        /// <param name="propertyId"></param>
        /// <returns></returns>
        public int getApplianceCount(string propertyId)
        {
            var appliances = context.GS_PROPERTY_APPLIANCE.Where(app => app.PROPERTYID == propertyId);
            return appliances.Count();
        }

        //Change#50 - Behroz - 16/01/2013 - End
        #endregion

        public bool isInspected(int applianceID, int appointmentID)
        {
            var appId = (from appIns in context.GS_ApplianceInspection
                         where appIns.APPLIANCEID == applianceID && appIns.APPOINTMENTID == appointmentID
                         select appIns.APPLIANCEINSPECTIONID);
            if (appId.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //Change - Behroz - 12/07/2012 - Start
        public bool isInspectedGas(int propertyApplianceId, int journalId)
        {
            var appId = (from appIns in context.P_APPLIANCE_INSPECTION
                         where appIns.PROPERTYAPPLIANCEID == propertyApplianceId && appIns.Journalid == journalId
                         select appIns.APPLIANCEINSPECTIONID);
            if (appId.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        //Change - Behroz - 12/07/2012 - End

        #region get Detector Count
        //Code added by Abdul Wahhab - 22/05/2013 - START
        /// <summary>
        /// This function returns detector count against a property
        /// </summary>
        /// <returns>Property Attributes Object</returns>
        public P_ATTRIBUTES getDetectorCount(string propertyID)
        {
            var detectorsCount = context.P_ATTRIBUTES.Where(patt => patt.PROPERTYID == propertyID);

            if (detectorsCount.Count() > 0)
            {
                return detectorsCount.First();
            }
            else
            {
                return new P_ATTRIBUTES();
            }
        }
        //Code added by Abdul Wahhab - 22/05/2013 - END
        #endregion

        #region get CO2 Detector Data
        //Code added by Abdul Wahhab - 22/05/2013 - START
        /// <summary>
        /// This function returns CO2 detector data against propertyid and journalid
        /// </summary>
        /// <returns>CO2 Inspection Object</returns>
        public DetectorInspectionData getCO2DetectorData(string propertyID, int journalid)
        {
            var co2DetectorData = (from coIns in context.P_CO2_Inspection
                                   where coIns.PropertyId == propertyID && coIns.Journalid == journalid
                                   select new DetectorInspectionData
                                   {
                                       DetectorId = coIns.DectectorId,
                                       propertyId = coIns.PropertyId,
                                       journalId = coIns.Journalid,
                                       DateStamp = coIns.DateStamp,
                                       DetectorTest = coIns.DetectorTest,
                                       IsInspected = coIns.IsInspected
                                   });

            if (co2DetectorData.Count() > 0)
            {
                return co2DetectorData.First();
            }
            else
            {
                return null;
            }
        }
        //Code added by Abdul Wahhab - 22/05/2013 - END
        #endregion

        #region get Smoke Detector Data
        //Code added by Abdul Wahhab - 22/05/2013 - START
        /// <summary>
        /// This function returns smoke detector data against propertyid and journalid
        /// </summary>
        /// <returns>Smoke Inspection Object</returns>
        public DetectorInspectionData getSmokeDetectorData(string propertyID, int journalid)
        {
            var smokeDetectorData = (from smIns in context.P_Smoke_Inspection
                                     where smIns.PropertyId == propertyID && smIns.Journalid == journalid
                                     select new DetectorInspectionData
                                     {
                                         DetectorId = smIns.DectectorId,
                                         propertyId = smIns.PropertyId,
                                         journalId = smIns.Journalid,
                                         DateStamp = smIns.DateStamp,
                                         DetectorTest = smIns.DetectorTest,
                                         IsInspected = smIns.IsInspected
                                     });

            if (smokeDetectorData.Count() > 0)
            {
                return smokeDetectorData.First();
            }
            else
            {
                return null;
            }
        }
        //Code added by Abdul Wahhab - 22/05/2013 - END
        #endregion

        #region Update Detector Count
        /// <summary>
        /// This function updates detector count against a property
        /// </summary>
        /// <param name="propertyId"></param>
        /// <param name="detectorCountData">DetectorCountData object</param>
        /// <returns>True in case of success</returns>  
        public ResultBoolData updateDetectorCount(string propertyId, DetectorCountData detectorCountData)
        {
            bool success = false;
            ResultBoolData resultData = new ResultBoolData();

            using (TransactionScope trans = new TransactionScope())
            {
                var patt = context.P_ATTRIBUTES.Where(att => att.PROPERTYID == propertyId);

                if (patt.Count() > 0)
                {
                    P_ATTRIBUTES attributes = patt.First();

                    if (detectorCountData.detectorType == 1 && detectorCountData.detectorCount != null)
                    {
                        attributes.SmokeDetector = (byte?)detectorCountData.detectorCount;
                    }
                    if (detectorCountData.detectorType == 2 && detectorCountData.detectorCount != null)
                    {
                        attributes.Co2Detector = (byte?)detectorCountData.detectorCount;
                    }

                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    trans.Complete();
                    success = true;
                }
            }
            resultData.result = success;
            return resultData;
        }
        #endregion

        #region Add Update Detector Info
        /// <summary>
        /// This function adds or updates detector info
        /// </summary>
        /// <param name="detectorType"></param>
        /// <param name="detectorInspectionData">DetectorInspectionData object</param>
        /// <returns>ID in case of success</returns>  
        public ResultIntData addUpdateDetectorInfo(int detectorType, DetectorInspectionData detectorInspectionData)
        {
            int detectorInspectionId = 0;
            ResultIntData resultData = new ResultIntData();

            using (TransactionScope trans = new TransactionScope())
            {
                if (detectorType == 1)
                {
                    var smokeDetectorInfo = context.P_Smoke_Inspection.Where(psi => psi.PropertyId == detectorInspectionData.propertyId && psi.Journalid == detectorInspectionData.journalId);

                    if (smokeDetectorInfo.Count() > 0)
                    {
                        P_Smoke_Inspection smokeIspectionObject = smokeDetectorInfo.First();
                        smokeIspectionObject.DetectorTest = detectorInspectionData.DetectorTest;
                        smokeIspectionObject.IsInspected = detectorInspectionData.IsInspected;
                        context.SaveChanges();
                        trans.Complete();
                        detectorInspectionId = smokeIspectionObject.DectectorId;
                    }
                    else
                    {
                        P_Smoke_Inspection smokeIspectionObject = new P_Smoke_Inspection();
                        smokeIspectionObject.DateStamp = DateTime.Now;
                        smokeIspectionObject.DetectorTest = detectorInspectionData.DetectorTest;
                        smokeIspectionObject.IsInspected = detectorInspectionData.IsInspected;
                        smokeIspectionObject.Journalid = detectorInspectionData.journalId;
                        smokeIspectionObject.PropertyId = detectorInspectionData.propertyId;
                        context.AddToP_Smoke_Inspection(smokeIspectionObject);
                        context.SaveChanges();
                        trans.Complete();
                        detectorInspectionId = smokeIspectionObject.DectectorId;
                    }
                }
                else if (detectorType == 2)
                {
                    var coDetectorInfo = context.P_CO2_Inspection.Where(pci => pci.PropertyId == detectorInspectionData.propertyId && pci.Journalid == detectorInspectionData.journalId);

                    if (coDetectorInfo.Count() > 0)
                    {
                        P_CO2_Inspection coIspectionObject = coDetectorInfo.First();
                        coIspectionObject.DetectorTest = detectorInspectionData.DetectorTest;
                        coIspectionObject.IsInspected = detectorInspectionData.IsInspected;
                        context.SaveChanges();
                        trans.Complete();
                        detectorInspectionId = coIspectionObject.DectectorId;
                    }
                    else
                    {
                        P_CO2_Inspection coIspectionObject = new P_CO2_Inspection();
                        coIspectionObject.DateStamp = DateTime.Now;
                        coIspectionObject.DetectorTest = detectorInspectionData.DetectorTest;
                        coIspectionObject.IsInspected = detectorInspectionData.IsInspected;
                        coIspectionObject.Journalid = detectorInspectionData.journalId;
                        coIspectionObject.PropertyId = detectorInspectionData.propertyId;
                        context.AddToP_CO2_Inspection(coIspectionObject);
                        context.SaveChanges();
                        trans.Complete();
                        detectorInspectionId = coIspectionObject.DectectorId;
                    }
                }
            }

            resultData.result = detectorInspectionId;

            return resultData;
        }
        #endregion
    }
}
