﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Transactions;
using PropSurvey.Contracts.Data;
using PropSurvey.Dal.Base;
using PropSurvey.Entities;
using PropSurvey.Dal.IssuedReceivedBy;
using PropSurvey.Dal.Faults;
using PropSurvey.Dal.Customer;
using PropSurvey.Dal.Property;

namespace PropSurvey.Dal.Appointment
{
    public class AppointmentDal : BaseDal
    {
        #region get All Fault Appointments Data

        /// <summary>
        /// This function returns all fault appointments by user
        /// </summary>
        /// <param name="operativeID">operative id</param>
        /// <returns>List of Jobs data objects</returns>
        public List<AppointmentListFault> getAllFaultAppointmentsByUser(string userName)
        {
            DateTime limit = DateTime.Now;
            limit = limit.AddDays(-28);

            var _var = (from app in context.FL_CO_APPOINTMENT
                        join fltAppt in context.FL_FAULT_APPOINTMENT on app.AppointmentID equals fltAppt.AppointmentId
                        join fltl in context.FL_FAULT_LOG on fltAppt.FaultLogId equals fltl.FaultLogID
                        join flts in context.FL_FAULT_STATUS on fltl.StatusID equals flts.FaultStatusID
                        join ten in context.C_TENANCY on fltl.PROPERTYID equals ten.PROPERTYID into tenancy
                        from tenan in tenancy.DefaultIfEmpty()
                        join pro in context.P__PROPERTY on fltl.PROPERTYID equals pro.PROPERTYID
                        join login in context.AC_LOGINS on app.OperativeID equals login.EMPLOYEEID
                        join noentry in context.FL_FAULT_NOENTRY on fltl.FaultLogID equals noentry.FaultLogId into tempNoEntry
                        from noentry1 in tempNoEntry.DefaultIfEmpty()
                        orderby app.AppointmentID
                        where tenan.ENDDATE == null
                                && login.LOGIN == userName
                                && fltAppt.FaultAppointmentId == (context.FL_FAULT_APPOINTMENT.Where(a => a.AppointmentId == app.AppointmentID).Min(a => a.FaultAppointmentId))
                                && flts.Description != "Cancelled" && app.AppointmentDate >= limit
                        select new AppointmentListFault
                        {
                            appointmentId = app.AppointmentID,
                            appointmentNotes = app.Notes,
                            repairNotes = app.RepairNotes,
                            noEntryNotes = noentry1.Notes,
                            appointmentDate = app.AppointmentDate,
                            repairCompletionDateTime = app.RepairCompletionDateTime,
                            appointmentStartTime = (app.Time == null ? "00:00" : app.Time.Trim().Remove(5)),
                            appointmentEndTime = (app.EndTime == null ? "00:00" : app.EndTime.Trim().Remove(5)),
                            appointmentStatus = (app.AppointmentStatus == null ? "NotStarted" : (app.AppointmentStatus.Replace(" ", "").ToLower() == "AppointmentArranged" ? "NotStarted" : app.AppointmentStatus.Replace(" ", ""))),
                            appointmentType = "Fault",
                            surveyorUserName = login.LOGIN,
                            tenancyId = tenan.TENANCYID,
                            property = new PropertyData
                            {
                                propertyId = pro.PROPERTYID,
                                tenancyId = tenan.TENANCYID,
                                houseNumber = pro.HOUSENUMBER,
                                flatNumber = pro.FLATNUMBER,
                                address1 = pro.ADDRESS1,
                                address2 = pro.ADDRESS2,
                                address3 = pro.ADDRESS3,
                                townCity = pro.TOWNCITY,
                                postCode = pro.POSTCODE,
                                county = pro.COUNTY
                            }
                        });

            List<AppointmentListFault> faultAppointments = new List<AppointmentListFault>();
            if (_var.Count() > 0)
            {
                faultAppointments = _var.ToList();
                foreach (AppointmentListFault alf in faultAppointments)
                {
                    setAdditionalDataFault(alf);
                }
            }

            return faultAppointments;
        }

        public string GetJobStatusString(int statusID)
        {
            var fltStatusList = context.FL_FAULT_STATUS.Where(fltSts => fltSts.FaultStatusID == statusID);
            if (fltStatusList.Count() == 0)
                return "";

            FL_FAULT_STATUS fltStatus = fltStatusList.First();

            return fltStatus.Description;
        }

        public int GetJobStatusID(string status)
        {
            var fltStatusList = context.FL_FAULT_STATUS.Where(fltSts => fltSts.Description == status);
            if (fltStatusList.Count() == 0)
                return 0;

            FL_FAULT_STATUS fltStatus = fltStatusList.First();

            return fltStatus.FaultStatusID;
        }

        #endregion

        #region get Fault Repair List
        /// <summary>
        /// This function returns the list of all fault repairs
        /// </summary>
        /// <returns>List of Fault Repair </returns>
        public List<FaultRepairData> getFaultRepairList()
        {
            var faultRepairList = (from faultRepair in context.FL_FAULT_REPAIR_LIST
                                   select new FaultRepairData
            {
                FaultRepairID = faultRepair.FaultRepairListID,
                Description = faultRepair.Description
            }
            );
            List<FaultRepairData> faultRepairDataList = new List<FaultRepairData>();
            if (faultRepairList.Count() > 0)
            {
                faultRepairDataList = faultRepairList.ToList();
            }

            return faultRepairDataList;

        }
        #endregion

        #region get pause reason list

        /// <summary>
        /// This function returns reason list for pausing a job
        /// </summary>
        /// <returns>List of reasons</returns>
        public List<string> getPauseReasonList()
        {
            var pauseReasonList = (from pauseReason in context.FL_PAUSED_REASON
                                   select pauseReason.Reason).ToList();
            return pauseReasonList;
        }
        #endregion

        #region get All Stock Appointments

        /// <summary>
        /// This function returns all the appointments
        /// </summary>
        /// <returns>List of appointment data objects</returns>
        public List<AppointmentListStock> getAllStockAppointments(string username)
        {
            DateTime limit = DateTime.Now;
            limit = limit.AddDays(-28);
            var appointments = (from app in context.PS_Appointment
                                join p2a in context.PS_Property2Appointment on app.AppointId equals p2a.AppointId
                                join ten in context.C_TENANCY on p2a.TenancyId equals ten.TENANCYID into tenancy
                                from tenan in tenancy.DefaultIfEmpty()
                                join pro in context.P__PROPERTY on p2a.PropertyId equals pro.PROPERTYID
                                orderby app.AppointId
                                where tenan.ENDDATE == null && app.SurveyourUserName == username

                                && app.AppointStartDateTime >= limit

                                select new AppointmentListStock
                                {
                                    appointmentId = app.AppointId,
                                    surveyourAvailability = app.SurveyourStatus,
                                    appointmentLocation = app.AppointLocation,

                                    appointmentStartDateTime = app.AppointStartDateTime,
                                    appointmentEndDateTime = app.AppointEndDateTime,
                                    appointmentNotes = app.AppointNotes,

                                    appointmentValidity = app.AppointValidity,
                                    appointmentType = app.AppointType,
                                    appointmentStatus = app.AppointProgStatus,
                                    surveyorUserName = app.SurveyourUserName,
                                    surveyorAlert = app.AppointmentAlert,
                                    surveyType = app.SurveyType,
                                    appointmentCalendar = app.AppointmentCalendar,
                                    tenancyId = tenan.TENANCYID,
                                    property = new PropertyData
                                    {
                                        propertyId = p2a.PropertyId,
                                        tenancyId = p2a.TenancyId,
                                        houseNumber = pro.HOUSENUMBER,

                                        flatNumber = pro.FLATNUMBER,
                                        address1 = pro.ADDRESS1,
                                        address2 = pro.ADDRESS2,
                                        address3 = pro.ADDRESS3,

                                        townCity = pro.TOWNCITY,
                                        postCode = pro.POSTCODE,
                                        county = pro.COUNTY,
                                        lastSurveyDate = p2a.LastSurveyDate
                                    }
                                });



            List<AppointmentListStock> appointmentList = new List<AppointmentListStock>();

            if (appointments.Count() > 0)
            {
                appointmentList = appointments.ToList();
                foreach (AppointmentListStock item in appointmentList)
                {
                    setAdditionalDataStock(item);
                }
            }

            return appointmentList;
        }

        #endregion

        #region Get all gas appointments

        /// <summary>
        /// This function returns all the appointments
        /// </summary>
        /// <returns>List of appointment data objects</returns>
        public List<AppointmentListGas> getAllAppointmentsGas(string username)
        {
            DateTime limit = DateTime.Now;
            limit = limit.AddDays(-28);

            var appointments = (from app in context.AS_APPOINTMENTS
                                join jor in context.AS_JOURNAL on app.JournalId equals jor.JOURNALID
                                join pro in context.P__PROPERTY on jor.PROPERTYID equals pro.PROPERTYID
                                join ten in context.C_TENANCY on app.TENANCYID equals ten.TENANCYID into tenancy
                                from tenan in tenancy.DefaultIfEmpty()
                                join status in context.AS_Status on jor.STATUSID equals status.StatusId
                                join login in context.AC_LOGINS on app.ASSIGNEDTO equals login.EMPLOYEEID

                                orderby app.APPOINTMENTID
                                where
                                    //tenan.ENDDATE == null && 
                                login.LOGIN == username
                                && jor.INSPECTIONTYPEID == 1 && app.APPOINTMENTDATE >= limit
                                && (jor.ISCURRENT == true || (app.APPOINTMENTSTATUS == "Finished" && jor.ISCURRENT == false))
                                select new AppointmentListGas
                                {
                                    appointmentId = app.APPOINTMENTID,
                                    jsgNumber = app.JSGNUMBER,
                                    tenancyId = tenan.TENANCYID,
                                    journalId = app.JournalId,
                                    journalHistoryId = app.JOURNALHISTORYID,
                                    appointmentDate = app.APPOINTMENTDATE,
                                    appointmentStartTime = app.APPOINTMENTSTARTTIME,
                                    appointmentEndTime = app.APPOINTMENTENDTIME,
                                    assignedTo = app.ASSIGNEDTO,
                                    surveyorUserName = login.LOGIN,
                                    createdBy = app.CREATEDBY,
                                    loggedDate = app.LOGGEDDATE,
                                    appointmentShift = app.APPOINTMENTSHIFT,
                                    appointmentNotes = app.NOTES,
                                    appointmentStatus = (app.APPOINTMENTSTATUS == null ? "NotStarted" : app.APPOINTMENTSTATUS),
                                    appointmentCalendar = (app.APPOINTMENTCALENDER == null ? "default" : app.APPOINTMENTCALENDER),
                                    surveyorAlert = (app.APPOINTMENTALERT == null ? "15 minutes before" : app.APPOINTMENTALERT),
                                    surveyourAvailability = (app.SURVEYOURSTATUS == null ? "Free" : app.SURVEYOURSTATUS),
                                    appointmentType = "Gas",
                                    surveyType = app.SURVEYTYPE,
                                    journal = new JournalData
                                    {
                                        actionId = jor.ACTIONID,
                                        creationBy = jor.CREATEDBY,
                                        creationDate = jor.CREATIONDATE,
                                        inspectionTypeId = jor.INSPECTIONTYPEID,
                                        isCurrent = jor.ISCURRENT,
                                        journalId = jor.JOURNALID,
                                        propertyId = jor.PROPERTYID,
                                        statusId = jor.STATUSID
                                    },
                                    property = new PropertyData
                                    {
                                        propertyId = jor.PROPERTYID,
                                        tenancyId = tenan.TENANCYID,
                                        houseNumber = pro.HOUSENUMBER,

                                        flatNumber = pro.FLATNUMBER,
                                        address1 = pro.ADDRESS1,
                                        address2 = pro.ADDRESS2,
                                        address3 = pro.ADDRESS3,

                                        townCity = pro.TOWNCITY,
                                        postCode = pro.POSTCODE,
                                        county = pro.COUNTY
                                    }
                                });


            List<AppointmentListGas> appointmentList = new List<AppointmentListGas>();

            if (appointments.Count() > 0)
            {
                appointmentList = appointments.ToList();
                foreach (AppointmentListGas item in appointmentList)
                {
                    setAdditionalDataGas(item);
                }
            }

            return appointmentList;
        }

        #endregion

        #region Update Fault Appointment Progress Status

        /// <summary>
        /// This function update the status of fault appointment. This function use post method to accept data. 
        /// </summary>
        /// <param name="apptData">This fucntion accepts the AppointmentData's object </param>
        /// <param name="isAppointFinished">this flag represents the finished appointment flag </param>
        /// <param name="lastSurveyDate">The date of last survey which 'll be filled if status is finished</param>
        /// <returns>It returns true or false in update is successful</returns>

        public ResultBoolData updateFaultAppointmentProgressStatus(int appointmentID, string progressStatus, bool isAppointFinished, DateTime lastSurveyDate)
        {
            ResultBoolData resultBoolData = new ResultBoolData();
            bool success = false;
            int? operativeID = 0;

            if (progressStatus.Trim().Replace(" ", "") != FaultAppointmentProgressStatus.InProgress.ToString())
            {
                resultBoolData.result = success;
                return resultBoolData;
            }

            using (TransactionScope trans = new TransactionScope())
            {
                var appointment = context.FL_CO_APPOINTMENT.Where(app => app.AppointmentID == appointmentID).First();
                appointment.AppointmentStatus = progressStatus;
                operativeID = appointment.OperativeID;
                success = true;
                context.SaveChanges();
                trans.Complete();
            }
            if (success == false)
            {
                resultBoolData.result = success;
                return resultBoolData;
            }

            FaultsDal faultDal = new FaultsDal();
            FaultLogHistoryData faultLogHistory = new FaultLogHistoryData();

            if (isAppointFinished == true)
            {
                this.saveLastSurveyDateFault(appointmentID);
            }

            resultBoolData.result = success;
            return resultBoolData;
        }
        #endregion

        #region Update Fault Status

        /// <summary>
        /// This function update the status of all faults of an fault appointment. This function use post method to accept data. 
        /// </summary>
        /// <param name="apptData">This function accepts the AppointmentData's object </param>      
        /// <returns>It returns true or false in update is successful</returns>

        public bool updateAllFaultStatusId(PropertySurvey_Entities context, int appointmentID, string progressStatus, int? operativeID)
        {
            bool success = false;
            var fltApptList = context.FL_FAULT_APPOINTMENT.Where(app => app.AppointmentId == appointmentID);

            if (fltApptList.Count() > 0)
            {
                foreach (FL_FAULT_APPOINTMENT fltAppt in fltApptList)
                {

                    FL_FAULT_LOG faultLog = context.FL_FAULT_LOG.Where(fltl => fltl.FaultLogID == fltAppt.FaultLogId).First();
                    var fltStatusList = context.FL_FAULT_STATUS.Where(fltSts => fltSts.Description.Replace(" ", "") == progressStatus);

                    if (fltStatusList.Count() == 0)
                        return false;

                    FL_FAULT_STATUS fltStatus = fltStatusList.First();
                    faultLog.StatusID = fltStatus.FaultStatusID;

                    success = saveFaultLogHistoryData(context, faultLog, operativeID);
                    if (success == false)
                        return false;
                }

                success = true;
            }

            return success;
        }

        /// <summary>
        /// This function update the status of all faults of an fault appointment. This function use post method to accept data. 
        /// </summary>
        /// <param name="apptData">This function accepts the AppointmentData's object </param>      
        /// <returns>It returns true or false in update is successful</returns>

        public bool updateFaultStatusId(PropertySurvey_Entities context, int faultLogID, string progressStatus, int? operativeID)
        {
            bool success = false;

            FL_FAULT_LOG faultLog = context.FL_FAULT_LOG.Where(fltl => fltl.FaultLogID == faultLogID).First();
            var fltStatusList = context.FL_FAULT_STATUS.Where(fltSts => fltSts.Description.Replace(" ", "") == progressStatus);

            if (fltStatusList.Count() == 0)
                return false;

            FL_FAULT_STATUS fltStatus = fltStatusList.First();
            faultLog.StatusID = fltStatus.FaultStatusID;

            // insert record in fault log history
            success = saveFaultLogHistoryData(context, faultLog, operativeID);
            if (success == false)
                return false;

            success = true;
            return success;
        }

        #endregion

        #region is Property N Customer Exists
        /// <summary>
        /// This function checks the property id against the customer, if that exists then it returns true otherwise false
        /// </summary>
        /// <param name="propertyId">property id </param>
        /// <param name="customerId">customer id</param>
        /// <returns>returns true or false</returns>

        public bool isPropertyNCustomerExists(string propertyId, int customerId)
        {
            bool success = false;
            var custRecord = (from cte in context.C_TENANCY
                              join prop in context.P__PROPERTY on cte.PROPERTYID equals prop.PROPERTYID
                              join cusp in context.C_CUSTOMERTENANCY on cte.TENANCYID equals cusp.TENANCYID
                              join cust in context.C__CUSTOMER on cusp.CUSTOMERID equals cust.CUSTOMERID
                              where cte.PROPERTYID.ToLower() == propertyId.ToLower() && cust.CUSTOMERID == customerId
                              select cte);

            if (custRecord.Count() > 0)
            {
                success = true;
            }
            else
            {
                success = false;
            }

            return success;
        }
        #endregion

        #region save Faults Log History Data
        /// <summary>
        /// This function saves the Faults Log History Data in the database
        /// </summary>
        /// <param name="appData">The object of Faults Log History Data</param>
        /// <returns>the Faults id if successfully save otherwise 0 </returns>

        public bool saveFaultLogHistoryData(PropertySurvey_Entities context, FL_FAULT_LOG faultLog, int? operativeID)
        {
            FL_FAULT_LOG_HISTORY faultLogHistoryData = new FL_FAULT_LOG_HISTORY();

            var faultJournalList = context.FL_FAULT_JOURNAL.Where(jor => jor.FaultLogID == faultLog.FaultLogID);
            if (faultJournalList.Count() > 0)
            {
                FL_FAULT_JOURNAL faultJournal = faultJournalList.First();
                faultLogHistoryData.JournalID = faultJournal.JournalID;
                faultJournal.FaultStatusID = faultLog.StatusID;
            }
            faultLogHistoryData.FaultStatusID = faultLog.StatusID;
            // faultLogHistoryData.itemActionID;
            faultLogHistoryData.LastActionDate = faultLog.SubmitDate;
            faultLogHistoryData.LastActionUserID = operativeID;
            faultLogHistoryData.FaultLogID = faultLog.FaultLogID;
            faultLogHistoryData.ORGID = faultLog.ORGID;
            //faultLogHistoryData.scopeID;
            //faultLogHistoryData.title;
            faultLogHistoryData.Notes = faultLog.Notes;
            faultLogHistoryData.PROPERTYID = faultLog.PROPERTYID;
            faultLogHistoryData.ContractorID = faultLog.ContractorID;

            context.AddToFL_FAULT_LOG_HISTORY(faultLogHistoryData);

            return true;
        }
        #endregion

        #region Update Stock Appointment Progress Status

        /// <summary>
        /// This function update the status of stock appointment. This function use post method to accept data. 
        /// </summary>
        /// <param name="apptData">This fucntion accepts the AppointmentData's object </param>
        /// <param name="isAppointFinished">this flag represents the finished appointment flag </param>
        /// <param name="lastSurveyDate">The date of last survey which 'll be filled if status is finished</param>
        /// <returns>It returns true or false in update is successful</returns>

        public bool updateStockAppointmentProgressStatus(AppointmentDataStock apptData, bool isAppointFinished, DateTime lastSurveyDate)
        {
            bool success = false;

            using (TransactionScope trans = new TransactionScope())
            {
                var apptRecord = context.PS_Appointment.Where(app => app.AppointId == apptData.appointmentId);

                if (apptRecord.Count() > 0)
                {
                    PS_Appointment appointment = apptRecord.First();
                    appointment.AppointProgStatus = apptData.appointmentStatus;
                    context.SaveChanges();

                    if (isAppointFinished == true)
                    {
                        this.saveLastSurveyDate(apptData.appointmentId);
                    }

                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    trans.Complete();
                    success = true;
                }
            }
            return success;
        }
        #endregion

        #region Update all fault job status from fault appointment
        /// <summary>
        /// This function update the status of all jobs of an appointment. This function use post method to accept data. 
        /// </summary>
        /// <param name="faultStatusData">This fucntion accepts the FaultStatusData's object </param>
        /// <param name="userName">user name </param>
        /// <param name="salt">salt </param>
        /// <returns>It returns true or false in update is successful</returns>

        public ResultBoolData updateFaultJobStatus(FaultStatusData faultStatusData, string userName)
        {
            ResultBoolData resultBoolData = new ResultBoolData();
            bool success = false;
            string status = faultStatusData.ProgressStatus;
            var userRecord = context.AC_LOGINS.Where(ulogin => ulogin.LOGIN == userName);
            if (userRecord.Count() > 0)
            {
                AC_LOGINS loggedInUser = userRecord.First();
                faultStatusData.UserID = loggedInUser.EMPLOYEEID;
            }

            if (isFaultCancelled(faultStatusData.FaultLogID))
            {
                resultBoolData.result = success;
                return resultBoolData;
            }

            using (TransactionScope trans = new TransactionScope())
            {
                int faultLogID = faultStatusData.FaultLogID;

                // START an arranged appointment's Jobs
                if (status.ToLower() == FaultJobProgressStatus.Started.ToString().ToLower())
                {
                    //1.) INSERT INTO FL_FAULT_JOBTIMESHEET
                    FL_FAULT_JOBTIMESHEET faultJobTimeSheet = new FL_FAULT_JOBTIMESHEET();
                    faultJobTimeSheet.APPOINTMENTID = faultStatusData.AppointmentId;
                    faultJobTimeSheet.FaultLogId = faultLogID;
                    faultJobTimeSheet.StartTime = DateTime.Now;
                    context.AddToFL_FAULT_JOBTIMESHEET(faultJobTimeSheet);
                    this.updateFaultStatusId(context, faultLogID, FaultJobProgressStatus.InProgress.ToString(), faultStatusData.UserID);
                    this.saveLastSurveyDateFault(faultStatusData.AppointmentId);
                    success = true;
                }
                // RESTART PAUSED Jobs
                else if (status.ToLower() == FaultJobProgressStatus.ReStarted.ToString().ToLower())
                {
                    success = false;
                }
                // Complete appintment with NOENTRY
                else if (status.ToLower() == FaultJobProgressStatus.NoEntry.ToString().ToLower())
                {
                    success = false;
                }
                // PAUSE already starter Jobs of an appintment
                else if (status.ToLower() == FaultJobProgressStatus.Paused.ToString().ToLower())
                {
                    //1.) INSERT INTO FL_FAULT_PAUSED
                    FL_FAULT_PAUSED faultPaused = new FL_FAULT_PAUSED();
                    faultPaused.FaultLogId = faultLogID;
                    faultPaused.PausedOn = DateTime.Now;
                    faultPaused.Notes = faultStatusData.Notes;
                    faultPaused.Reason = faultStatusData.Reason;
                    faultPaused.PausedBy = faultStatusData.UserID;
                    context.AddToFL_FAULT_PAUSED(faultPaused);

                    //2.) UPDATE FL_FAULT_LOG SET FaultStatusId=’Paused’
                    //3.) INSERT INTO FL_FAULT_LOG_HISTORY (Status=’Paused)
                    //4.) UPDATE FL_FAULT_JOURNAL SET FaultStatusId=’Paused’                      
                    this.updateFaultStatusId(context, faultLogID, FaultJobProgressStatus.Paused.ToString(), faultStatusData.UserID);

                    //5.) UPDATE FL_FAULT_JOBTIMESHEET SET JobEndDate/Time                 
                    var faultJobTimeSheetList = context.FL_FAULT_JOBTIMESHEET.Where(flTime => flTime.FaultLogId == faultLogID
                                                              && flTime.APPOINTMENTID == faultStatusData.AppointmentId).ToList();
                    FL_FAULT_JOBTIMESHEET faultJobTimeSheet = faultJobTimeSheetList.Last();

                    faultJobTimeSheet.EndTime = DateTime.Now;
                    this.saveLastSurveyDateFault(faultStatusData.AppointmentId);
                    success = true;
                }
                // COMPLETE already starter Jobs of an appintment
                else if (status.ToLower() == FaultJobProgressStatus.Complete.ToString().ToLower())
                {
                    //1.) INSERT INTO FL_CO_FAULTLOG_TO_REPAIR
                    System.Data.Objects.ObjectParameter result = new ObjectParameter("result", typeof(int));
                    context.FL_ADD_FL_CO_FAULTLOG_TO_REPAIR(faultLogID, faultStatusData.FaultRepairIDs, faultStatusData.UserID, faultStatusData.Notes, result);

                    //1.) INSERT INTO C_JOURNAL
                    //2.) INSERT INTO C_REPAIR
                    //3.) INSERT INTO FL_FAULTJOURNAL_TO_CJOURNAL
                    //4.) Repeat Steps 1 To 3 For Each Repair Added
                    //If Fault is Rechargable
                    //5.) INSERT INTO C_RECHARGEABLE
                    //6.) INSERT INTO F_SALESINVOICE
                    //7.) INSERT INTO F_RENTJOURNAL
                    //8.) INSERT INTO F_SALESINVOICEITEM
                    //9.) INSERT INTO F_SALEITEM_TO_PURCHASEITEM
                    //10.) EXEC NL_SALESORDER @SaleId
                    //11.) UPDATE FL_FAULT_LOG SET FaultStatusId=’Completed’
                    //12.) UPDATE FL_FAULT_ JOURNAL SET FaultStatusId=’Completed’
                    //13.) INSERT INTO FL_FAULT_LOG_HISTORY
                    //14.) UPDATE FL_FAULT_JOBTIMESHEET SET JobEndDate/Time
                    //15.) UPDATE FL_CO_APPOINTMENT SET AppointmentStatus=’Completed’
                    if (faultStatusData.RepairCompletionDateTime == null)
                        faultStatusData.RepairCompletionDateTime = DateTime.Now;

                    System.Data.Objects.ObjectParameter result1 = new ObjectParameter("result", typeof(int));
                    context.FL_CO_COMPLETE_APPOINTMENT(faultStatusData.AppointmentId, faultLogID, faultStatusData.FollowOnNotes, faultStatusData.Notes,
                        faultStatusData.RepairCompletionDateTime, faultStatusData.IsAppointmentCompleted, result1);

                    if (Convert.ToInt32(result.Value) == 1 && Convert.ToInt32(result1.Value) == 1)
                        success = true;
                    else
                        success = false;
                }
                if (success == true)
                {
                    context.SaveChanges();
                    trans.Complete();
                }
            }

            resultBoolData.result = success;
            return resultBoolData;
        }

        public bool isFaultCancelled(int faultLogID)
        {
            int cancelledStatusID = GetJobStatusID("Cancelled");

            FL_FAULT_LOG faultLog = context.FL_FAULT_LOG.Where(fltl => fltl.FaultLogID == faultLogID).First();
            if (faultLog.StatusID == cancelledStatusID)
                return true;
            else
                return false;
        }

        public List<FL_FAULT_APPOINTMENT> getFaultAppointmentList(int appointmentID)
        {
            var fltApptList = (from fltAppt in context.FL_FAULT_APPOINTMENT
                               orderby fltAppt.FaultAppointmentId
                               where fltAppt.AppointmentId == appointmentID
                               select fltAppt);
            return fltApptList.ToList();
        }

        public List<FL_FAULT_LOG> getFaultLogList(int appointmentID)
        {
            var flLogList = (from fltl in context.FL_FAULT_LOG
                             join fltAppt in context.FL_FAULT_APPOINTMENT on fltl.FaultLogID equals fltAppt.FaultLogId
                             orderby fltl.FaultLogID
                             where fltAppt.AppointmentId == appointmentID
                             select fltl);
            return flLogList.ToList();
        }

        #endregion

        #region Update all fault job status from fault appointment to NoEntry

        /// <summary>
        /// This function update the status of all jobs of an appointment to noEntry. This function use post method to accept data. 
        /// </summary>
        /// <param name="faultStatusData">This fucntion accepts the FaultStatusData's object </param>
        /// <param name="userName">user name </param>
        /// <param name="salt">salt </param>
        /// <returns>It returns true or false in update is successful</returns>

        public ResultBoolData noEntryAppointmentStatus(FaultStatusData faultStatusData, string userName)
        {
            ResultBoolData resultBoolData = new ResultBoolData();
            bool success = false;
            string status = FaultJobProgressStatus.NoEntry.ToString();
            var userRecord = context.AC_LOGINS.Where(ulogin => ulogin.LOGIN == userName);
            if (userRecord.Count() > 0)
            {
                AC_LOGINS loggedInUser = userRecord.First();
                faultStatusData.UserID = loggedInUser.EMPLOYEEID;
            }

            List<FL_FAULT_APPOINTMENT> fltApptList = getFaultAppointmentList(faultStatusData.AppointmentId);

            // check if appointment has been cancelled from Web module
            if (fltApptList.Count() > 0)
            {
                var varfaultLogID = fltApptList.First().FaultLogId;
                if (isFaultCancelled(varfaultLogID))
                {
                    resultBoolData.result = success;
                    return resultBoolData;
                }
            }

            using (TransactionScope trans = new TransactionScope())
            {
                foreach (FL_FAULT_APPOINTMENT fltAppt in fltApptList)
                {
                    int faultLogID = fltAppt.FaultLogId;

                    //1.) INSERT INTO FL_FAULT_NOENTRY                       
                    FL_FAULT_NOENTRY faultNoEntry = new FL_FAULT_NOENTRY();
                    faultNoEntry.FaultLogId = faultLogID;
                    faultNoEntry.RecordedOn = DateTime.Now;
                    faultNoEntry.Notes = faultStatusData.Notes;
                    context.AddToFL_FAULT_NOENTRY(faultNoEntry);

                    //2.) INSERT INTO FL_FAULT_LOG_HISTORY (Status=’No Entry’)
                    //3.) UPDATE FL_FAULT_LOG SET FaultStatusId=’Completed’
                    //4.) INSERT INTO FL_FAULT_LOG_HISTORY (Status=’Completed’)
                    //5.) UPDATE FL_FAULT_JOURNAL SET FaultStatusId=’Completed’                       
                    this.updateFaultStatusId(context, faultLogID, FaultJobProgressStatus.NoEntry.ToString(), faultStatusData.UserID);
                    this.updateFaultStatusId(context, faultLogID, FaultJobProgressStatus.Complete.ToString(), faultStatusData.UserID);

                    //6.) UPDATE FL_FAULT_JOBTIMESHEET SET JobEndDate/Time                      
                    FL_FAULT_JOBTIMESHEET faultJobTimeSheet = new FL_FAULT_JOBTIMESHEET();
                    faultJobTimeSheet.APPOINTMENTID = faultStatusData.AppointmentId;
                    faultJobTimeSheet.FaultLogId = faultLogID;
                    faultJobTimeSheet.StartTime = DateTime.Now;
                    faultJobTimeSheet.EndTime = DateTime.Now;
                    context.AddToFL_FAULT_JOBTIMESHEET(faultJobTimeSheet);

                    if (faultStatusData.RepairCompletionDateTime == null)
                        faultStatusData.RepairCompletionDateTime = DateTime.Now;
                    //7.) UPDATE FL_CO_APPOINTMENT SET AppointmentStatus=’Completed’
                    var appointment = context.FL_CO_APPOINTMENT.Where(app => app.AppointmentID == faultStatusData.AppointmentId).First();
                    appointment.AppointmentStatus = FaultJobProgressStatus.Complete.ToString();
                    appointment.LastActionDate = DateTime.Now;
                    appointment.RepairCompletionDateTime = faultStatusData.RepairCompletionDateTime;
                    success = true;
                }

                if (success == true)
                {
                    context.SaveChanges();
                    trans.Complete();
                }
            }

            resultBoolData.result = success;
            return resultBoolData;
        }

        #endregion

        #region get And Set Appointment Progress Status

        /// <summary>
        /// This function checks the appointment progress status, If that is not started , it 'll start it otherwise it'll return false
        /// </summary>
        /// <param name="appointmentId">appointment id</param>
        /// <param name="userName">user name </param>
        /// <returns>it return true if appointment status changed from NotStarted to Pending other wise false</returns>

        public bool getAndSetStockAppointmentProgressStatus(AppointmentDataStock apptData)
        {
            bool success = false;
            bool updateStatus = false;

            PS_Appointment appointmentStatus = new PS_Appointment();
            var appointment = context.PS_Appointment.Where(app => app.AppointId == apptData.appointmentId && app.SurveyourUserName.ToLower() == apptData.surveyorUserName.ToLower());

            //if appointment exist against appointment id and surveyour
            if (appointment.Count() > 0)
            {
                appointmentStatus = appointment.First();

                //this transaction will update the status in appointment table & it will also save the last survey date 
                //in pa_property2appointment if survey status is finished
                using (TransactionScope trans = new TransactionScope())
                {
                    //If appointment progress status is "Not Started" then make it "Pending"
                    if (appointmentStatus.AppointProgStatus.Equals(AppointmentProgressStatus.NotStarted.ToString()))
                    {
                        appointmentStatus.AppointProgStatus = AppointmentProgressStatus.InProgress.ToString();
                        updateStatus = true;
                    }
                    else if (appointmentStatus.AppointProgStatus.Equals(AppointmentProgressStatus.InProgress.ToString()))
                    {
                        //If appointment progress status is "InProgress" then make it "Finished"
                        appointmentStatus.AppointProgStatus = AppointmentProgressStatus.Finished.ToString();
                        updateStatus = true;

                        //save the last survey date
                        this.saveLastSurveyDate(apptData.appointmentId);
                    }
                    else if (appointmentStatus.AppointProgStatus.Equals(AppointmentProgressStatus.Finished.ToString()))
                    {
                        //If appointment progress status is already "Finished" then return true
                        success = true;
                    }

                    if (updateStatus == true)
                    {
                        appointmentStatus.SurveyourUserName = apptData.surveyorUserName;
                        context.SaveChanges();

                        context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                        trans.Complete();
                        success = true;
                    }
                }
            }
            else
            {
                success = false;
            }
            return success;
        }

        /// <summary>
        /// This function get fault appointment progress status
        /// </summary>
        /// <param name="appointmentId">appointment id</param>
        /// <param name="userName">user name </param>
        /// <returns>return the fault appointment progress status</returns>

        public string getFaultJobStatus(int appointmentId)
        {
            string status = "";
            var fltList = (from fltl in context.FL_FAULT_LOG
                           join fltAppt in context.FL_FAULT_APPOINTMENT on fltl.FaultLogID equals fltAppt.FaultLogId
                           join fltStatus in context.FL_FAULT_STATUS on fltl.StatusID equals fltStatus.FaultStatusID
                           orderby fltl.FaultLogID
                           where fltAppt.AppointmentId == appointmentId
                           && fltAppt.FaultLogId == (context.FL_FAULT_APPOINTMENT.Where(ap => ap.AppointmentId == appointmentId).Min(ap => ap.FaultLogId))
                           select fltStatus.Description);

            if (fltList.Count() == 0)
                status = "{\"jobStatusNameKey\":\"\"}";

            status = fltList.First();
            
            if (status.Trim().Replace(" ", "").ToLower() == FaultAppointmentProgressStatus.AppointmentArranged.ToString().ToLower())
            {
                status = FaultJobProgressStatus.NotStarted.ToString();
            }
            else if (status.Trim().Replace(" ", "").ToLower() == FaultJobProgressStatus.InProgress.ToString().ToLower())
            {
                status = FaultJobProgressStatus.Started.ToString();
            }

            status = "{\"jobStatusNameKey\":\"" + status + "\"}";

            return status;
        }
        
        public bool getAndSetGasAppointmentProgressStatus(AppointmentDataGas apptData)
        {
            bool success = false;
            bool updateStatus = false;

            AS_APPOINTMENTS appointmentStatus = new AS_APPOINTMENTS();
            var appointment = context.AS_APPOINTMENTS.Where(app => app.APPOINTMENTID == apptData.appointmentId && app.ASSIGNEDTO == apptData.assignedTo);
            
            //if appointment exist against appointment id and surveyour            
            if (appointment.Count() > 0)
            {
                appointmentStatus = appointment.First();

                //this transaction will update the status in appointment table & it will also save the last survey date 
                //in pa_property2appointment if survey status is finished
                using (TransactionScope trans = new TransactionScope())
                {
                    //If appointment progress status is "Not Started" then make it "Pending"                    
                    //When the appointment is added by web then the appointmentstatus is null
                    if (appointmentStatus.APPOINTMENTSTATUS == null)
                    {
                        appointmentStatus.APPOINTMENTSTATUS = AppointmentProgressStatus.InProgress.ToString();
                        updateStatus = true;
                    }                    
                    else if (appointmentStatus.APPOINTMENTSTATUS.Equals(AppointmentProgressStatus.NotStarted.ToString()))
                    {
                        appointmentStatus.APPOINTMENTSTATUS = AppointmentProgressStatus.InProgress.ToString();
                        updateStatus = true;
                    }
                    else if (appointmentStatus.APPOINTMENTSTATUS.Equals(AppointmentProgressStatus.InProgress.ToString()))
                    {
                        //If appointment progress status is "InProgress" then make it "Finished"
                        appointmentStatus.APPOINTMENTSTATUS = AppointmentProgressStatus.Finished.ToString();
                        updateStatus = true;

                        //save the last survey date
                        //this.saveLastSurveyDate(apptData.appointmentId);
                    }
                    else if (appointmentStatus.APPOINTMENTSTATUS.Equals(AppointmentProgressStatus.Finished.ToString()))
                    {
                        //If appointment progress status is already "Finished" then return true
                        success = true;
                    }

                    if (updateStatus == true)
                    {
                        //appointmentStatus.SurveyourUserName = apptData.surveyorUserName;
                        appointmentStatus.ASSIGNEDTO = apptData.assignedTo;
                        context.SaveChanges();

                        context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                        trans.Complete();
                        success = true;
                    }
                }
            }
            else
            {
                success = false;
            }
            return success;
        }        
        #endregion

        #region save Appointment
        /// <summary>
        /// This function saves the appointment in the database
        /// </summary>
        /// <param name="apptData">The object of appointment</param>
        /// <returns>the appointment id if successfully save otherwise 0 </returns>

        public int saveAppointment(AppointmentDataStock apptData)
        {
            bool success = false;
            PS_Appointment appointment = new PS_Appointment();

            using (TransactionScope trans = new TransactionScope())
            {
                appointment.AppointLocation = apptData.appointmentLocation;
                appointment.AppointNotes = apptData.appointmentNotes;
                appointment.AppointProgStatus = apptData.appointmentStatus;

                appointment.AppointStartDateTime = apptData.appointmentStartDateTime;
                appointment.AppointEndDateTime = apptData.appointmentEndDateTime;
                appointment.AppointTitle = apptData.appointmentTitle;
                appointment.AppointType = apptData.appointmentType;

                appointment.AppointValidity = apptData.appointmentValidity;
                appointment.CreatedBy = apptData.createdBy;
                appointment.ModifiedBy = apptData.createdBy;

                appointment.SurveyourStatus = apptData.surveyourAvailability;
                appointment.SurveyType = apptData.surveyType;
                appointment.SurveyourUserName = apptData.surveyorUserName;
                appointment.CreatedDate = DateTime.Now;
                appointment.ModifiedDate = DateTime.Now;

                if (apptData.surveyorAlert != null)
                {
                    appointment.AppointmentAlert = apptData.surveyorAlert;
                }
                else
                {
                    appointment.AppointmentAlert = "None";
                }



                if (apptData.appointmentCalendar != null)
                {
                    appointment.AppointmentCalendar = apptData.appointmentCalendar;
                }
                else
                {
                    appointment.AppointmentCalendar = "Property Survey";
                }



                context.AddToPS_Appointment(appointment);
                context.SaveChanges();

                PS_Property2Appointment property = new PS_Property2Appointment();

                property.AppointId = appointment.AppointId;                
                if (apptData.customer.customerId != null)
                {
                    property.CustomerId = (int)apptData.customer.customerId;
                }
                property.PropertyId = apptData.customer.property.propertyId;
                if (apptData.customer.property.tenancyId != null)
                {
                    property.TenancyId = (int)apptData.customer.property.tenancyId;
                }

                context.AddToPS_Property2Appointment(property);
                context.SaveChanges();

                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                trans.Complete();
                success = true;
            }

            if (success == true)
            {
                return appointment.AppointId;
            }
            else
            {
                return 0;
            }
        }
                
        /// <summary>
        /// This function saves the appointment in the AS_APPointments table. It uses an existing stored procedure that is used
        /// by the web application. A few new fields were added that are not reflected in the stored procedure. Those fields
        /// are updated after the procedure call.
        /// </summary>
        /// <param name="apptData"></param>
        /// <returns></returns>
        public int saveAppointmentGas(AppointmentDataGas apptData)
        {
            var journalData = context.AS_JOURNAL.Where(jour => jour.PROPERTYID == apptData.customer.property.propertyId
                                                            && jour.ISCURRENT == true);

            //Get the status id from the AS_STATUS so, that the status can be saved in the database.
            var getStatusId = context.AS_Status.Where(status => status.Title == Utilities.Constants.MessageConstants.AppointmentArrangedInAS_Status).FirstOrDefault();

            if (journalData.Count() > 0)
            {
                //Change        
                AS_JOURNAL journal = new AS_JOURNAL();
                journal = journalData.First();

                //If the PropertyId already exist in AS_APPOINTMENT and its status is NOENTRY then donot insert a new appontment
                //update the old one
                var NoEntryStatusId = context.AS_Status.Where(status => status.Title == Utilities.Constants.MessageConstants.NoEntryInAS_Status).FirstOrDefault().StatusId;

                if (journal.STATUSID == NoEntryStatusId)
                {
                    #region "Update"

                    //Update the status id in the object
                    apptData.journal.statusId = getStatusId.StatusId;
                    apptData.journalId = journal.JOURNALID;
                    apptData.appointmentId = context.AS_APPOINTMENTS.Where(app => app.JournalId == journal.JOURNALID).FirstOrDefault().APPOINTMENTID;
                    //Update the old appointment
                    bool lAppointmentUpdated = updateAppointment(apptData);

                    if (lAppointmentUpdated)
                        return apptData.appointmentId;
                    else
                        return 0;

                    #endregion
                }
                else
                {
                    #region "Insert"

                    //Insert a new appointment
                    System.Data.Objects.ObjectParameter journalHistoryId = new ObjectParameter("journalHistoryId", typeof(int));

                    using (TransactionScope trans = new TransactionScope())
                    {
                        //Insert Appointment
                        //context.AS_SaveAppointment(journal.JOURNALID, apptData.customer.property.propertyId, apptData.createdBy,
                        //    apptData.journal.inspectionTypeId, apptData.appointmentDate, apptData.journal.statusId, apptData.journal.actionId,
                        //    apptData.appointmentNotes, false, false, apptData.appointmentStartTime, apptData.appointmentEndTime,
                        //    apptData.assignedTo, apptData.appointmentShift, apptData.customer.property.tenancyId, journalHistoryId);
                        context.AS_SaveAppointment(journal.JOURNALID, apptData.customer.property.propertyId, apptData.createdBy,
                            apptData.journal.inspectionTypeId, apptData.appointmentDate, getStatusId.StatusId, apptData.journal.actionId,
                            apptData.appointmentNotes, false, false, apptData.appointmentStartTime, apptData.appointmentEndTime,
                            apptData.assignedTo, apptData.appointmentShift, apptData.customer.property.tenancyId, journalHistoryId);

                        //Update the new columns that were added
                        long jorHisId = Convert.ToInt64(journalHistoryId.Value);

                        AS_APPOINTMENTS updatedAppointment = context.AS_APPOINTMENTS.Where(app => app.JOURNALHISTORYID == jorHisId).First();
                        AS_APPOINTMENTSHISTORY updateAppHistory = context.AS_APPOINTMENTSHISTORY.Where(appHis => appHis.JOURNALHISTORYID == jorHisId).First();

                        //For Appointment Table
                        updatedAppointment.SURVEYOURSTATUS = apptData.surveyourAvailability;
                        updatedAppointment.APPOINTMENTSTATUS = apptData.appointmentStatus;

                        //For AppointmentHistory Table
                        updateAppHistory.SURVEYOURSTATUS = apptData.surveyourAvailability;
                        updateAppHistory.APPOINTMENTSTATUS = apptData.appointmentStatus;

                        if (apptData.surveyorAlert != null)
                        {
                            updatedAppointment.APPOINTMENTALERT = apptData.surveyorAlert;
                            updateAppHistory.APPOINTMENTALERT = apptData.surveyorAlert;
                        }
                        else
                        {
                            updatedAppointment.APPOINTMENTALERT = "None";
                        }

                        if (apptData.appointmentCalendar != null)
                        {
                            updatedAppointment.APPOINTMENTCALENDER = apptData.appointmentCalendar;
                            updateAppHistory.APPOINTMENTCALENDER = apptData.appointmentCalendar;
                        }
                        else
                        {
                            updatedAppointment.APPOINTMENTCALENDER = "Property Survey";
                        }
                        //Added new column
                        if (apptData.surveyType != null)
                        {
                            updatedAppointment.SURVEYTYPE = apptData.surveyType;
                            updateAppHistory.SURVEYTYPE = apptData.surveyType;
                        }

                        context.SaveChanges();
                        context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                        trans.Complete();

                        return updatedAppointment.APPOINTMENTID;
                    }

                    #endregion
                }
            }
            else
                return 0;
        }
        
        #endregion

        #region save NoEntry
        /// <summary>
        /// This function saves the appointment NoEntry in the database
        /// </summary>
        /// <param name="apptData">The object of NoEntry</param>
        /// <returns>the NoEntry id if successfully save otherwise 0 </returns>

        public int saveNoEntry(NoEntryData noEntryData)
        {
            bool success = false;
            PS_NoEntry noEntry = new PS_NoEntry();

            using (TransactionScope trans = new TransactionScope())
            {
                noEntry.isCardLeft = noEntryData.isCardLeft;
                noEntry.isGasAvailable = noEntryData.isGasAvailable;
                noEntry.isPropertyAccessible = noEntryData.isPropertyAccessible;
                noEntry.PropertyID = noEntryData.PropertyID;
                noEntry.AppointmentID = noEntryData.AppointmentID;
                noEntry.Date = DateTime.Now;

                context.AddToPS_NoEntry(noEntry);

                PS_Appointment appData = context.PS_Appointment.Where(a => a.AppointId == noEntry.AppointmentID).FirstOrDefault();
                appData.AppointProgStatus = "Finished";

                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                trans.Complete();
                success = true;
            }

            if (success == true)
            {
                return noEntry.ID;
            }
            else
            {
                return 0;
            }
        }

        //Change#10 - Behroz - 12/11/2012 - Start
        /// <summary>
        /// This function saves the appointment NoEntry in the database
        /// </summary>
        /// <param name="apptData">The object of NoEntry</param>
        /// <returns>the NoEntry id if successfully save otherwise 0 </returns>
        public int saveNoEntryGas(NoEntryData noEntryData)
        {
            //Modify the fuction .....
            //"Please follow these steps
            //1- Insert into AS_NOEntry
            //2- Update AS_APPOINTMENTS (status = finished)
            //3- Insert AS_APPOINTMENTSHISTORY
            //4- Update AS_JOURNAL
            //5- Insert AS_JOURNALHISTORY "

            bool success = false;
            AS_NoEntry noEntry = new AS_NoEntry();

            using (TransactionScope trans = new TransactionScope())
            {
                #region "Insert into No Entry"

                //Insert into no Entry
                noEntry.isCardLeft = noEntryData.isCardLeft;
                noEntry.RecordedBy = noEntryData.RecordedBy;
                noEntry.RecordedDate = DateTime.Now;
                noEntry.JournalId = noEntryData.JournalId;

                context.AddToAS_NoEntry(noEntry);
                #endregion

                #region "Update Appointment"

                //Update Appointment and set status
                AS_APPOINTMENTS appData = context.AS_APPOINTMENTS.Where(app => app.JournalId == noEntryData.JournalId).FirstOrDefault();
                appData.APPOINTMENTSTATUS = "Finished";

                context.SaveChanges();
                #endregion

                #region "Insert into Appointment history"

                //Insert into Appointment History
                AS_APPOINTMENTSHISTORY apphis = new AS_APPOINTMENTSHISTORY()
                {
                    APPOINTMENTALERT = appData.APPOINTMENTALERT,
                    APPOINTMENTCALENDER = appData.APPOINTMENTCALENDER,
                    APPOINTMENTDATE = appData.APPOINTMENTDATE,
                    APPOINTMENTENDTIME = appData.APPOINTMENTENDTIME,
                    APPOINTMENTSTARTTIME = appData.APPOINTMENTSTARTTIME,
                    APPOINTMENTSTATUS = appData.APPOINTMENTSTATUS,
                    ASSIGNEDTO = appData.ASSIGNEDTO,
                    CREATEDBY = appData.CREATEDBY,
                    JournalId = appData.JournalId,
                    JSGNUMBER = appData.JSGNUMBER.ToString(),
                    APPOINTMENTID = appData.APPOINTMENTID,
                    APPOINTMENTSHIFT = appData.APPOINTMENTSHIFT,
                    JOURNALHISTORYID = appData.JOURNALHISTORYID,
                    LOGGEDDATE = DateTime.Now,
                    NOTES = appData.NOTES,
                    TENANCYID = appData.TENANCYID,
                    SURVEYOURSTATUS = appData.SURVEYOURSTATUS
                };

                context.AddToAS_APPOINTMENTSHISTORY(apphis);
                context.SaveChanges();
                #endregion

                #region "Update journal"
                //Update journal
                AS_JOURNAL jorData = context.AS_JOURNAL.Where(jor => jor.JOURNALID == noEntryData.JournalId).FirstOrDefault();
                var status = context.AS_Status.Where(s => s.Title == Utilities.Constants.MessageConstants.NoEntryInAS_Status).FirstOrDefault();

                jorData.STATUSID = status.StatusId;
                //jorData.STATUSID = 3;

                context.SaveChanges();
                #endregion

                #region "Insert into Journal History"

                AS_JOURNALHISTORY jorHis = new AS_JOURNALHISTORY()
                {
                    ACTIONID = jorData.ACTIONID,
                    CREATEDBY = jorData.CREATEDBY,
                    CREATIONDATE = DateTime.Now,
                    NOTES = appData.NOTES,
                    INSPECTIONTYPEID = jorData.INSPECTIONTYPEID,
                    STATUSID = jorData.STATUSID,
                    PROPERTYID = jorData.PROPERTYID,
                    JOURNALID = jorData.JOURNALID,
                    ISLETTERATTACHED = false,
                    IsDocumentAttached = false
                };

                context.AddToAS_JOURNALHISTORY(jorHis);
                context.SaveChanges();
                #endregion

                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                trans.Complete();
                success = true;
            }

            if (success == true)
            {
                return noEntry.NoEntryID;
            }
            else
            {
                return 0;
            }
        }
        //Change#10 - Behroz - 12/11/2012 - End

        #endregion

        #region Get Total No of NoEntries by PropertyID
        /// <summary>
        /// This function get appointment NoEntry from the database
        /// </summary>
        /// <param name="propertyID">PropertyID</param>
        /// <returns>the no of NoEntries</returns>

        public AppointmentInfoData getTotalNoEntries(string propertyID)
        {
            AppointmentInfoData noEntries = new AppointmentInfoData();

            var noEntriesData = (from entry in context.PS_NoEntry
                                 where entry.PropertyID == propertyID
                                 select entry);
            var totalAppointmentData = (from app in context.PS_Property2Appointment
                                        join a in context.PS_Appointment on app.AppointId equals a.AppointId
                                        where app.PropertyId == propertyID
                                        select a);
            noEntries.TotalNoEntries = noEntriesData.Count();
            noEntries.TotalAppointments = totalAppointmentData.Count();

            return noEntries;
        }

        //Change - 12/06/2012 - Behroz - Start
        /// <summary>
        /// This function get appointment NoEntry from the database related to GAS
        /// </summary>
        /// <param name="propertyID">PropertyID</param>
        /// <returns>the no of NoEntries</returns>
        public AppointmentInfoData getTotalNoEntriesGas(string propertyID, int journalId)
        {
            AppointmentInfoData noEntries = new AppointmentInfoData();

            //var noEntriesData = from entry in context.AS_NoEntry
            //                    where entry.JournalId == journalId
            //                    select entry;
            var noEntriesData = (from history in context.AS_JOURNALHISTORY
                                 where history.PROPERTYID == propertyID
                                 && history.STATUSID == (context.AS_Status.Where(status => status.Title == Utilities.Constants.MessageConstants.NoEntryInAS_Status).FirstOrDefault().StatusId)
                                 select history);

            //var totalAppointmentData = (from app in context.AS_APPOINTMENTS
            //                            join a in context.AS_APPOINTMENTSHISTORY on app.APPOINTMENTID equals a.APPOINTMENTID
            //                            join jor in context.AS_JOURNAL on app.JournalId equals jor.JOURNALID
            //                            where jor.PROPERTYID == propertyID
            //                            select a);
            var totalAppointmentData = (from history in context.AS_JOURNALHISTORY
                                        where history.PROPERTYID == propertyID
                                        && history.STATUSID == (context.AS_Status.Where(status => status.Title == Utilities.Constants.MessageConstants.AppointmentArrangedInAS_Status).FirstOrDefault().StatusId)
                                        select history.JOURNALID);

            noEntries.TotalNoEntries = noEntriesData.Count();
            noEntries.TotalAppointments = totalAppointmentData.Count();

            return noEntries;
        }
        //Change - 12/06/2012 - Behroz - End

        #endregion

        #region get User Stock Appointments
        /// <summary>
        /// This function returns all stock appointments of the particular user
        /// </summary>
        /// <param name="username">username </param>
        /// <returns>the list of appointments </returns>
        public List<AppointmentListStock> getStockAppointmentsByUser(string userName, string filter)
        {

            List<AppointmentListStock> appointmentList = new List<AppointmentListStock>();

            var appointments = new List<AppointmentListStock>();
            if (filter.ToLower() == "all" || filter == string.Empty)
            {
                //Change Ticket# 8,9,10 - Zeeshan - 05/03/2013 - Start
                //appointments = this.getAllAppointments().Where(appt => appt.surveyorUserName == userName).ToList();
                appointments = this.getAllStockAppointments(userName);
                //Change Ticket# 8,9,10 - Zeeshan - 05/03/2013 - End
            }
            else
            {
                //Change Ticket# 8,9,10 - Zeeshan - 05/03/2013 - Start
                //appointments = this.getAllAppointments().Where(appt => appt.surveyorUserName == userName && appt.appointmentStatus.ToLower() == filter.ToLower()).ToList();
                appointments = this.getAllStockAppointments(userName).Where(appt => appt.appointmentStatus.ToLower() == filter.ToLower()).ToList();
                //Change Ticket# 8,9,10 - Zeeshan - 05/03/2013 - End
            }

            if (appointments.Count() > 0)
            {
                appointmentList = appointments;
            }


            return appointmentList;
        }

        #region "get JobSheetNumber List for all jobs of an AppointmentID"
        /// <summary>
        /// This function returns the list of JobSheetNumber of all jobs of an appointment.
        /// </summary>
        /// <param name="appointmentId">appointment id</param>
        /// <returns>the list of JobSheetNumber Object</returns>

        public List<JobData> getJobDataList(int appointmentID)
        {
            var jobSheetNumbers = (from app in context.FL_CO_APPOINTMENT
                                   join fltAppt in context.FL_FAULT_APPOINTMENT on app.AppointmentID equals fltAppt.AppointmentId
                                   join fltl in context.FL_FAULT_LOG on fltAppt.FaultLogId equals fltl.FaultLogID
                                   join flts in context.FL_FAULT_STATUS on fltl.StatusID equals flts.FaultStatusID // Line added by Abdul Wahhab - 10/06/2013
                                   join flt in context.FL_FAULT on fltl.FaultID equals flt.FaultID
                                   // join flrepare in context.FL_CO_FAULTLOG_TO_REPAIR on fltl.FaultLogID equals flrepare.FaultLogID into fltRepair
                                   // from rep in fltRepair.DefaultIfEmpty()
                                   //join flrepare in context.FL_CO_FAULTLOG_TO_REPAIR on fltl.FaultLogID equals flrepare.FaultLogID
                                   join are in context.FL_AREA on flt.AREAID equals are.AreaID
                                   //join cus in context.C__CUSTOMER on fltl.CustomerId equals cus.CUSTOMERID
                                   //join gti in context.G_TITLE on cus.TITLE equals gti.TITLEID
                                   //join cad in context.C_ADDRESS on cus.CUSTOMERID equals cad.CUSTOMERID
                                   //join cut in context.C_CUSTOMERTENANCY on cus.CUSTOMERID equals cut.CUSTOMERID
                                   //join ten in context.C_TENANCY on cut.TENANCYID equals ten.TENANCYID
                                   //join pro in context.P__PROPERTY on ten.PROPERTYID equals pro.PROPERTYID
                                   join pri in context.FL_FAULT_PRIORITY on flt.PriorityID equals pri.PriorityID // Line added by Abdul Wahhab - 06/06/2013
                                   orderby app.AppointmentID
                                   where app.AppointmentID == appointmentID
                                   //&& rep.FaultLogToRepairID == (context.FL_CO_FAULTLOG_TO_REPAIR.Where(a => a.FaultLogID == fltl.FaultLogID).Min(a => a.FaultLogToRepairID))
                                   //&& ten.ENDDATE == null
                                   // && cut.ENDDATE == null
                                   //&& cad.ISDEFAULT == 1

                                   select new JobData
                                   {
                                       JSNumber = fltl.JobSheetNumber,
                                       FaultLogID = fltl.FaultLogID,
                                       JSNDescription = flt.Description,
                                       JSNNotes = fltl.Notes,
                                       JSNLocation = are.AreaName,
                                       // RepairNotes = rep.Notes,
                                       Duration = (flt.duration == null ? 0 : flt.duration),
                                       // Lines added by Abdul Wahhab - 06/06/2013 - START
                                       ReportedDate = fltl.SubmitDate,
                                       Priority = pri.PriorityName,
                                       ResponseTime = (pri.ResponseTime == 24 ? "24 Hours" : (pri.ResponseTime == 1 ? "1 Day" : (pri.ResponseTime == 7 ? "7 Days" : "28 Days"))),
                                       // Lines added by Abdul Wahhab - 06/06/2013 - END
                                       jobStatus = flts.Description //Line added by Abdul Wahhab - 10/06/2013
                                   });

            List<JobData> jobDataList = new List<JobData>();

            if (jobSheetNumbers.Count() > 0)
            {
                jobDataList = jobSheetNumbers.ToList();

                // Code modified by Abdul Wahhab - 17/06/2013 - START

                // Adding fault repair list for complete faults
                foreach (JobData jobData in jobDataList)
                {
                    jobData.RepairNotes = GetRepairNotes(jobData.FaultLogID);
                    if (jobData.jobStatus.Replace(" ", "") == FaultAppointmentProgressStatus.AppointmentArranged.ToString())
                    {
                        jobData.jobStatus = FaultJobProgressStatus.NotStarted.ToString();
                    }
                    else if (jobData.jobStatus.Replace(" ", "") == FaultJobProgressStatus.InProgress.ToString())
                    {
                        jobData.jobStatus = FaultJobProgressStatus.Started.ToString();
                    }

                    var completionDateData = context.C_REPAIR.Where(cr => cr.ITEMDETAILID == jobData.FaultLogID);

                    if (completionDateData.Count() > 0)
                    {
                        jobData.CompletionDate = completionDateData.First().LASTACTIONDATE;
                    }
                    else
                    {
                        jobData.CompletionDate = null;
                    }

                    var followOnData = context.FL_FAULT_FOLLOWON.Where(fff => fff.FaultLogId == jobData.FaultLogID);

                    if (followOnData.Count() > 0)
                    {
                        List<FL_FAULT_FOLLOWON> followOnList = followOnData.ToList();

                        jobData.followOnNotes = followOnList.Last().FollowOnNotes;
                    }
                    else
                    {
                        jobData.followOnNotes = null;
                    }

                    var faultRepair = (from flr in context.FL_CO_FAULTLOG_TO_REPAIR
                                       join frl in context.FL_FAULT_REPAIR_LIST on flr.FaultRepairListID equals frl.FaultRepairListID
                                       where flr.FaultLogID == jobData.FaultLogID
                                       select new FaultRepairData
                                       {
                                           FaultRepairID = flr.FaultRepairListID,
                                           Description = frl.Description
                                       });

                    if (faultRepair.Count() > 0)
                    {
                        jobData.faultRepairDataList = faultRepair.ToList();
                    }
                }

                // Code modified by Abdul Wahhab - 17/06/2013 - END
            }

            return jobDataList;
        }

        private string GetRepairNotes(int faultLogID)
        {
            var faultRepair = context.FL_CO_FAULTLOG_TO_REPAIR.Where(rep => rep.FaultLogID == faultLogID);

            string repairNotes = string.Empty;
            if (faultRepair != null && faultRepair.Count() > 0)
            {
                repairNotes = faultRepair.First().Notes;
            }
            return repairNotes;
        }
        #endregion

        /// <summary>
        /// This function returns all the appointments of the particular user related to GAS
        /// </summary>
        /// <param name="username">username </param>
        /// <returns>the list of appointments </returns>
        public List<AppointmentListGas> getUserAppointmentsGas(string userName, string filter)
        {
            List<AppointmentListGas> appointmentList = new List<AppointmentListGas>();

            var appointments = new List<AppointmentListGas>();
            if (filter.ToLower() == "all" || filter == string.Empty)
            {
                appointments = this.getAllAppointmentsGas(userName);
            }
            else
            {
                appointments = this.getAllAppointmentsGas(userName).Where(appt => appt.appointmentStatus.ToLower() == filter.ToLower()).ToList();
            }

            if (appointments.Count() > 0)
            {
                appointmentList = appointments;
            }

            return appointmentList;
        }

        #endregion

        #region get All Users
        /// <summary>
        /// This function returns all the users of rsl manager database
        /// </summary>        
        /// <returns>list of all users</returns>

        public List<UserData> getAllUsers()
        {
            var users = (from app in context.AC_LOGINS
                         join emp in context.E__EMPLOYEE on app.EMPLOYEEID equals emp.EMPLOYEEID
                         where app.ACTIVE == 1
                         orderby app.LOGIN
                         select new UserData
                         {
                             userId = app.LOGINID,
                             userName = app.LOGIN,
                             password = app.PASSWORD,
                             FullName = emp.FIRSTNAME + " " + emp.LASTNAME
                         });



            List<UserData> userList = new List<UserData>();
            if (users.Count() > 0)
            {
                userList = users.ToList();
            }

            return userList;
        }

        //Change#17 - Behroz - 19/12/2012 - Start
        /// <summary>
        /// This function returns all the users of rsl manager database for gas
        /// </summary>        
        /// <returns>list of all users</returns>
        public List<UserData> getAllUsersGas()
        {
            var users = from user in context.AS_USER
                        join login in context.AC_LOGINS on user.EmployeeId equals login.EMPLOYEEID
                        join emp in context.E__EMPLOYEE on user.EmployeeId equals emp.EMPLOYEEID
                        where user.IsActive == true
                        orderby login.LOGIN
                        select new UserData
                        {
                            userId = login.LOGINID,
                            userName = login.LOGIN,
                            password = login.PASSWORD,
                            FullName = emp.FIRSTNAME + " " + emp.LASTNAME
                        };

            List<UserData> userList = new List<UserData>();
            if (users.Count() > 0)
            {
                userList = users.ToList();
            }

            return userList;
        }
        //Change#17 - Behroz - 19/12/2012 - End
        #endregion

        #region get Index Screen Info
        /// <summary>
        /// This function returns Index Screen Info
        /// </summary>        
        /// <returns>Index Screen Info</returns>

        //public IndexStatusData GetIndexScreenStatus(int AppointmentID)
        //{
        //    IndexStatusData indexScreen = new IndexStatusData();
        //    var _data = (from app in context.PS_Property2Appointment
        //                 join cus in context.C__CUSTOMER on app.CustomerId equals cus.CUSTOMERID
        //                 join add in context.C_ADDRESS on cus.CUSTOMERID equals add.CUSTOMERID
        //                 join title in context.G_TITLE on cus.TITLE equals title.TITLEID
        //                 where app.AppointId == AppointmentID
        //                 select new CustomerData
        //                 {
        //                     customerId = cus.CUSTOMERID,
        //                     firstName = cus.FIRSTNAME,
        //                     middleName = cus.MIDDLENAME,
        //                     lastName = cus.LASTNAME,
        //                     title = title.DESCRIPTION,
        //                     Address = new CustomerAddressData
        //                     {
        //                         PostCode = add.POSTCODE,
        //                         Address = add.ADDRESS1,
        //                         TownCity = add.TOWNCITY,
        //                         HouseNo = add.HOUSENUMBER
        //                     }
        //                 });
        //    CustomerData customer = new CustomerData();
        //    if (_data.Count() > 0)
        //    {
        //        customer = _data.FirstOrDefault();
        //        indexScreen.AppointmentID = AppointmentID;
        //        indexScreen.ClientName = customer.title + ". " + customer.firstName + " " + customer.middleName + " " + customer.lastName;
        //        indexScreen.ClientAddress = customer.Address.HouseNo + " " + customer.Address.Address + ", " + customer.Address.TownCity + " " + customer.Address.PostCode;
        //    }

        //    var faultData = (from fault in context.GS_GasFault
        //                     where fault.AppointmentID == AppointmentID
        //                     select fault);

        //    indexScreen.isFaultInspected = faultData.Count() > 0;

        //    var Orgdata = (from app in context.PS_Appointment
        //                   join org in context.GS_OrgInspection on app.AppointId equals org.AppointmentID
        //                   where app.AppointId == AppointmentID
        //                   select org.ID);

        //    indexScreen.isOrgInspected = Orgdata.Count() > 0;

        //    var AgentData = (from app in context.PS_Appointment
        //                     join agent in context.GS_PropertyAgent_to_Appointment on app.AppointId equals agent.AppointmentID
        //                     where app.AppointId == AppointmentID
        //                     select agent.ID);

        //    indexScreen.isAgentInspected = AgentData.Count() > 0;

        //    int[] ApplData = (from app in context.PS_Property2Appointment
        //                      join appl in context.GS_PROPERTY_APPLIANCE on app.PropertyId equals appl.PROPERTYID
        //                      where app.AppointId == AppointmentID
        //                      select appl.PROPERTYAPPLIANCEID).ToArray<int>();
        //    for (int i = 0; i < ApplData.Count(); i++)
        //    {
        //        int applianceId = ApplData[i];
        //        var temp = (from appl in context.GS_PROPERTY_APPLIANCE
        //                    join insApp in context.GS_ApplianceInspection on appl.PROPERTYAPPLIANCEID equals insApp.APPLIANCEID
        //                    where insApp.APPLIANCEID == applianceId && insApp.APPOINTMENTID == AppointmentID
        //                    select insApp.APPLIANCEINSPECTIONID);
        //        if (temp.Count() < 1)
        //        {
        //            indexScreen.isApplianceInspected = false;
        //            break;
        //        }
        //        indexScreen.isApplianceInspected = true;
        //    }

        //    var GPWData = (from app in context.PS_Appointment
        //                   join gpw in context.GS_InstallationPipeWork on app.AppointId equals gpw.AppointmentID
        //                   where app.AppointId == AppointmentID
        //                   select gpw.ID);

        //    indexScreen.isGIPWInspected = GPWData.Count() > 0;

        //    var IssueData = (from app in context.PS_Appointment
        //                     join gpw in context.GS_IssuedReceivedBy on app.AppointId equals gpw.AppointmentID
        //                     where app.AppointId == AppointmentID
        //                     select gpw.ID);

        //    indexScreen.isIssuedReceivedInspected = IssueData.Count() > 0;

        //    return indexScreen;
        //}
        public IndexStatusData GetIndexScreenStatus(int AppointmentID)
        {
            IndexStatusData indexScreen = new IndexStatusData();
            var _data = (from app in context.PS_Property2Appointment
                         join cus in context.C__CUSTOMER on app.CustomerId equals cus.CUSTOMERID
                         join add in context.C_ADDRESS on cus.CUSTOMERID equals add.CUSTOMERID
                         join gtitle in context.G_TITLE on cus.TITLE equals gtitle.TITLEID into tempgti
                         from gti in tempgti.DefaultIfEmpty()
                         where app.AppointId == AppointmentID
                         select new CustomerData
                         {
                             customerId = cus.CUSTOMERID,
                             firstName = cus.FIRSTNAME,
                             middleName = cus.MIDDLENAME,
                             lastName = cus.LASTNAME,
                             title = gti.DESCRIPTION,
                             Address = new CustomerAddressData
                             {
                                 PostCode = add.POSTCODE,
                                 Address = add.ADDRESS1,
                                 TownCity = add.TOWNCITY,
                                 HouseNo = add.HOUSENUMBER
                             }
                         });
            CustomerData customer = new CustomerData();
            if (_data.Count() > 0)
            {
                customer = _data.FirstOrDefault();
                indexScreen.AppointmentID = AppointmentID;
                if (customer.title != null)
                {
                    indexScreen.ClientName = customer.title + ". " + customer.firstName + " " + customer.middleName + " " + customer.lastName;
                }
                else
                {
                    indexScreen.ClientName = customer.firstName + " " + customer.middleName + " " + customer.lastName;
                }
                indexScreen.ClientAddress = customer.Address.HouseNo + " " + customer.Address.Address + ", " + customer.Address.TownCity + " " + customer.Address.PostCode;
            }

            var faultData = (from fault in context.GS_GasFault
                             where fault.AppointmentID == AppointmentID
                             select fault);

            indexScreen.isFaultInspected = faultData.Count() > 0;

            var Orgdata = (from app in context.PS_Appointment
                           join org in context.GS_OrgInspection on app.AppointId equals org.AppointmentID
                           where app.AppointId == AppointmentID
                           select org.ID);

            indexScreen.isOrgInspected = Orgdata.Count() > 0;

            var AgentData = (from app in context.PS_Appointment
                             join agent in context.GS_PropertyAgent_to_Appointment on app.AppointId equals agent.AppointmentID
                             where app.AppointId == AppointmentID
                             select agent.ID);

            indexScreen.isAgentInspected = AgentData.Count() > 0;

            int[] ApplData = (from app in context.PS_Property2Appointment
                              join appl in context.GS_PROPERTY_APPLIANCE on app.PropertyId equals appl.PROPERTYID
                              where app.AppointId == AppointmentID
                              select appl.PROPERTYAPPLIANCEID).ToArray<int>();
            for (int i = 0; i < ApplData.Count(); i++)
            {
                int applianceId = ApplData[i];
                var temp = (from appl in context.GS_PROPERTY_APPLIANCE
                            join insApp in context.GS_ApplianceInspection on appl.PROPERTYAPPLIANCEID equals insApp.APPLIANCEID
                            where insApp.APPLIANCEID == applianceId && insApp.APPOINTMENTID == AppointmentID
                            select insApp.APPLIANCEINSPECTIONID);
                if (temp.Count() < 1)
                {
                    indexScreen.isApplianceInspected = false;
                    break;
                }
                indexScreen.isApplianceInspected = true;
            }

            var GPWData = (from app in context.PS_Appointment
                           join gpw in context.GS_InstallationPipeWork on app.AppointId equals gpw.AppointmentID
                           where app.AppointId == AppointmentID
                           select gpw.ID);

            indexScreen.isGIPWInspected = GPWData.Count() > 0;

            var IssueData = (from app in context.PS_Appointment
                             join gpw in context.GS_IssuedReceivedBy on app.AppointId equals gpw.AppointmentID
                             where app.AppointId == AppointmentID
                             select gpw.ID);

            indexScreen.isIssuedReceivedInspected = IssueData.Count() > 0;

            return indexScreen;
        }

        //Change#33 - Behroz - 20/12/2012 - Start
        public IndexStatusData GetIndexScreenStatusGas(int AppointmentID)
        {
            IndexStatusData indexScreen = new IndexStatusData();

            var mainData = (from app in context.AS_APPOINTMENTS
                            join
                            jor in context.AS_JOURNAL on app.JournalId equals jor.JOURNALID
                            where app.APPOINTMENTID == AppointmentID && jor.ISCURRENT == true && jor.INSPECTIONTYPEID == 1
                            select new { app.JournalId, jor.PROPERTYID }).FirstOrDefault();

            int journalId = mainData.JournalId;
            string PropertyId = mainData.PROPERTYID;

            var _data = (from app in context.AS_APPOINTMENTS
                         join cusTen in context.C_CUSTOMERTENANCY on app.TENANCYID equals cusTen.TENANCYID
                         join cus in context.C__CUSTOMER on cusTen.CUSTOMERID equals cus.CUSTOMERID
                         join add in context.C_ADDRESS on cus.CUSTOMERID equals add.CUSTOMERID
                         join gtitle in context.G_TITLE on cus.TITLE equals gtitle.TITLEID into tempgti
                         from gti in tempgti.DefaultIfEmpty()
                         where app.APPOINTMENTID == AppointmentID
                         select new CustomerData
                         {
                             customerId = cus.CUSTOMERID,
                             firstName = cus.FIRSTNAME,
                             middleName = cus.MIDDLENAME,
                             lastName = cus.LASTNAME,
                             title = gti.DESCRIPTION,
                             Address = new CustomerAddressData
                             {
                                 PostCode = add.POSTCODE,
                                 Address = add.ADDRESS1,
                                 TownCity = add.TOWNCITY,
                                 HouseNo = add.HOUSENUMBER
                             }
                         });



            CustomerData customer = new CustomerData();
            if (_data.Count() > 0)
            {
                customer = _data.FirstOrDefault();
                indexScreen.AppointmentID = AppointmentID;
                if (customer.title != null)
                {
                    indexScreen.ClientName = customer.title + ". " + customer.firstName + " " + customer.middleName + " " + customer.lastName;
                }
                else
                {
                    indexScreen.ClientName = customer.firstName + " " + customer.middleName + " " + customer.lastName;
                }
                indexScreen.ClientAddress = customer.Address.HouseNo + " " + customer.Address.Address + ", " + customer.Address.TownCity + " " + customer.Address.PostCode;

                //journalId = custom
            }

            //var faultData = (from fault in context.GS_GasFault
            //                 where fault.AppointmentID == AppointmentID
            //                 select fault);
            //var faultData = (from fault in context.P_PROPERTY_APPLIANCE_DEFECTS
            //                 join jor in context.AS_JOURNAL on fault.JournalId equals jor.JOURNALID
            //                 join app in context.AS_APPOINTMENTS on jor.JOURNALID  equals app.JournalId
            //                 where app.APPOINTMENTID== AppointmentID
            //                 select fault);
            var faultData = from fault in context.P_PROPERTY_APPLIANCE_DEFECTS where fault.JournalId == journalId select fault;

            indexScreen.isFaultInspected = faultData.Count() > 0;

            //Not Required
            //var Orgdata = (from app in context.PS_Appointment
            //               join org in context.GS_OrgInspection on app.AppointId equals org.AppointmentID
            //               where app.AppointId == AppointmentID
            //               select org.ID);

            //indexScreen.isOrgInspected = Orgdata.Count() > 0;

            //var AgentData = (from app in context.PS_Appointment
            //                 join agent in context.GS_PropertyAgent_to_Appointment on app.AppointId equals agent.AppointmentID
            //                 where app.AppointId == AppointmentID
            //                 select agent.ID);

            //indexScreen.isAgentInspected = AgentData.Count() > 0;

            //int[] ApplData = (from app in context.PS_Property2Appointment
            //                  join appl in context.GS_PROPERTY_APPLIANCE on app.PropertyId equals appl.PROPERTYID
            //                  where app.AppointId == AppointmentID
            //                  select appl.PROPERTYAPPLIANCEID).ToArray<int>();
            int[] ApplData = (from appl in context.GS_PROPERTY_APPLIANCE
                              where appl.PROPERTYID == PropertyId
                              select appl.PROPERTYAPPLIANCEID).ToArray<int>();

            for (int i = 0; i < ApplData.Count(); i++)
            {
                int applianceId = ApplData[i];
                //var temp = (from appl in context.GS_PROPERTY_APPLIANCE
                //            join insApp in context.GS_ApplianceInspection on appl.PROPERTYAPPLIANCEID equals insApp.APPLIANCEID
                //            where insApp.APPLIANCEID == applianceId && insApp.APPOINTMENTID == AppointmentID
                //            select insApp.APPLIANCEINSPECTIONID);

                var temp = (from appl in context.GS_PROPERTY_APPLIANCE
                            join insApp in context.P_APPLIANCE_INSPECTION on appl.PROPERTYAPPLIANCEID equals insApp.PROPERTYAPPLIANCEID
                            where insApp.PROPERTYAPPLIANCEID == applianceId && insApp.Journalid == journalId
                            select insApp.APPLIANCEINSPECTIONID);

                if (temp.Count() < 1)
                {
                    indexScreen.isApplianceInspected = false;
                    break;
                }
                indexScreen.isApplianceInspected = true;
            }

            //Code added by Abdul Wahhab - 27/05/2013 - START

            if (indexScreen.isApplianceInspected)
            {
                var smokeData = (from psi in context.P_Smoke_Inspection
                                 where psi.PropertyId == PropertyId && psi.Journalid == journalId
                                 select psi.IsInspected);

                if (smokeData.Count() > 0)
                {
                    if (!smokeData.First().Value)
                    {
                        indexScreen.isApplianceInspected = false;
                    }
                }
                else
                {
                    indexScreen.isApplianceInspected = false;
                }

                if (indexScreen.isApplianceInspected)
                {
                    var coData = (from pci in context.P_CO2_Inspection
                                  where pci.PropertyId == PropertyId && pci.Journalid == journalId
                                  select pci.IsInspected);

                    if (coData.Count() > 0)
                    {
                        if (!coData.First().Value)
                        {
                            indexScreen.isApplianceInspected = false;
                        }
                    }
                    else
                    {
                        indexScreen.isApplianceInspected = false;
                    }
                }
            }

            //Code added by Abdul Wahhab - 27/05/2013 - END

            //var GPWData = (from app in context.PS_Appointment
            //               join gpw in context.GS_InstallationPipeWork on app.AppointId equals gpw.AppointmentID
            //               where app.AppointId == AppointmentID
            //               select gpw.ID);
            //var GPWData = (from pipe in context.P_InstallationPipeWork
            //               where pipe.PropertyId == PropertyId
            //               select pipe.PipeWorkID);
            var GPWData = (from pipe in context.P_InstallationPipeWork
                           where pipe.Journalid == journalId
                           select pipe.PipeWorkID);

            indexScreen.isGIPWInspected = GPWData.Count() > 0;

            //var IssueData = (from app in context.PS_Appointment
            //                 join gpw in context.GS_IssuedReceivedBy on app.AppointId equals gpw.AppointmentID
            //                 where app.AppointId == AppointmentID
            //                 select gpw.ID);
            //var IssueData = (from lgsr in context.P_LGSR
            //                 where lgsr.PROPERTYID == PropertyId
            //                 select lgsr.LGSRID);
            var IssueData = (from lgsr in context.P_LGSR
                             where lgsr.JOURNALID == journalId
                             select lgsr.LGSRID);

            indexScreen.isIssuedReceivedInspected = IssueData.Count() > 0;

            return indexScreen;
        }
        //Change#33 - Behroz - 20/12/2012 - End
        #endregion

        #region Update Appointment

        /// <summary>
        /// This function updates the appointment. This function use post method to accept data. 
        /// </summary>
        /// <param name="apptData">This fucntion accepts the AppointmentData's object </param>
        /// <returns>It returns true or false in update is successful</returns>

        public bool updateAppointment(AppointmentDataStock apptData)
        {
            bool success = false;

            using (TransactionScope trans = new TransactionScope())
            {
                var apptRecord = context.PS_Appointment.Where(app => app.AppointId == apptData.appointmentId);

                if (apptRecord.Count() > 0)
                {
                    PS_Appointment appointment = apptRecord.First();
                    if (apptData.appointmentLocation != null)
                    {
                        appointment.AppointLocation = apptData.appointmentLocation;
                    }
                    if (apptData.appointmentNotes != null)
                    {
                        appointment.AppointNotes = apptData.appointmentNotes;
                    }
                    if (apptData.appointmentStatus != null)
                    {
                        appointment.AppointProgStatus = apptData.appointmentStatus;
                    }
                    if (apptData.appointmentStartDateTime != null)
                    {
                        appointment.AppointStartDateTime = apptData.appointmentStartDateTime;
                    }
                    if (apptData.appointmentEndDateTime != null)
                    {
                        appointment.AppointEndDateTime = apptData.appointmentEndDateTime;
                    }
                    if (apptData.appointmentTitle != null)
                    {
                        appointment.AppointTitle = apptData.appointmentTitle;
                    }
                    if (apptData.appointmentType != null)
                    {
                        appointment.AppointType = apptData.appointmentType;
                    }
                    if (apptData.appointmentValidity != null)
                    {
                        appointment.AppointValidity = apptData.appointmentValidity;
                    }
                    if (apptData.createdBy != 0)
                    {
                        appointment.CreatedBy = apptData.createdBy;
                    }

                    if (apptData.surveyourAvailability != null)
                    {
                        appointment.SurveyourStatus = apptData.surveyourAvailability;
                    }
                    if (apptData.surveyType != null)
                    {
                        appointment.SurveyType = apptData.surveyType;
                    }
                    if (apptData.surveyorUserName != null)
                    {
                        appointment.SurveyourUserName = apptData.surveyorUserName;
                    }
                    //appointment.CreatedDate = DateTime.Now;
                    appointment.ModifiedDate = DateTime.Now;
                    if (apptData.surveyorAlert != null)
                    {
                        appointment.AppointmentAlert = apptData.surveyorAlert;
                    }
                    if (apptData.appointmentCalendar != null)
                    {
                        appointment.AppointmentCalendar = apptData.appointmentCalendar;
                    }
                    context.SaveChanges();



                    var apptPropertyRecord = context.PS_Property2Appointment.Where(prop => prop.AppointId == apptData.appointmentId);
                    if (apptPropertyRecord.Count() > 0)
                    {
                        PS_Property2Appointment property = apptPropertyRecord.First();

                        property.AppointId = apptData.appointmentId;
                        if (apptData.customer != null)
                        {
                            // Code changed by Abdul Wahhab - 19/06/2013 - START
                            if (apptData.customer.customerId != null)
                            {
                                property.CustomerId = (int)apptData.customer.customerId;
                            }
                            // Code changed by Abdul Wahhab - 19/06/2013 - END
                            if (apptData.customer.property != null)
                            {
                                property.PropertyId = apptData.customer.property.propertyId;
                            }
                            if (apptData.customer.property != null)
                            {
                                property.TenancyId = (int)apptData.customer.property.tenancyId; // Line changed by Abdul Wahhab - 18/06/2013
                            }
                        }
                        context.SaveChanges();
                    }

                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);


                    trans.Complete();
                    success = true;
                }
            }
            return success;
        }

        //Change#11 - Behroz -  12/12/2012 - Start
        /// <summary>
        /// This function updates the appointment. This function use post method to accept data. 
        /// </summary>
        /// <param name="apptData">This fucntion accepts the AppointmentData's object </param>
        /// <returns>It returns true or false in update is successful</returns>
        public bool updateAppointment(AppointmentDataGas apptData)
        {
            System.Data.Objects.ObjectParameter journalHistoryId = new ObjectParameter("journalHistoryId", typeof(int));

            using (TransactionScope trans = new TransactionScope())
            {
                string lPropertyId = string.Empty;
                //If the customer has been changed then we have the updated data in the property.
                if (apptData.customer != null)
                {
                    if (apptData.customer.property != null)
                        lPropertyId = apptData.customer.property.propertyId;
                    else
                        lPropertyId = apptData.journal.propertyId;
                }
                else
                    lPropertyId = apptData.journal.propertyId;

                context.AS_AmendAppointment(apptData.journalId, lPropertyId, apptData.createdBy,
                        apptData.journal.inspectionTypeId, apptData.appointmentDate, apptData.journal.statusId, apptData.journal.actionId,
                        apptData.appointmentNotes, false, false, apptData.appointmentStartTime, apptData.appointmentEndTime,
                        apptData.assignedTo, apptData.appointmentShift, apptData.appointmentId, journalHistoryId);

                //Update the new columns that were added
                long jorHisId = Convert.ToInt64(journalHistoryId.Value);

                if (jorHisId != 0)
                {
                    AS_APPOINTMENTS updatedAppointment = context.AS_APPOINTMENTS.Where(app => app.JOURNALHISTORYID == jorHisId).First();
                    AS_APPOINTMENTSHISTORY updatedAppHist = context.AS_APPOINTMENTSHISTORY.Where(app => app.JOURNALHISTORYID == jorHisId).First();

                    if (apptData.surveyorAlert != null)
                    {
                        updatedAppointment.APPOINTMENTALERT = apptData.surveyorAlert;
                        updatedAppHist.APPOINTMENTALERT = apptData.surveyorAlert;
                    }

                    if (apptData.appointmentCalendar != null)
                    {
                        updatedAppointment.APPOINTMENTCALENDER = apptData.appointmentCalendar;
                        updatedAppHist.APPOINTMENTCALENDER = apptData.appointmentCalendar;
                    }

                    if (apptData.surveyourAvailability != null)
                    {
                        updatedAppointment.SURVEYOURSTATUS = apptData.surveyourAvailability;
                        updatedAppHist.SURVEYOURSTATUS = apptData.surveyourAvailability;
                    }

                    if (apptData.appointmentStatus != null)
                    {
                        updatedAppointment.APPOINTMENTSTATUS = apptData.appointmentStatus;
                        updatedAppHist.APPOINTMENTSTATUS = apptData.appointmentStatus;
                    }

                    //Added new column
                    if (apptData.surveyType != null)
                    {
                        updatedAppointment.SURVEYTYPE = apptData.surveyType;
                        updatedAppHist.SURVEYTYPE = apptData.surveyType;
                    }

                    context.SaveChanges();
                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    trans.Complete();

                    return true;
                }
                else
                {
                    return false;
                }

            }

            #region "Old Code"

            //bool success = false;

            //using (TransactionScope trans = new TransactionScope())
            //{
            //    var apptRecord = context.PS_Appointment.Where(app => app.AppointId == apptData.appointmentId);

            //    if (apptRecord.Count() > 0)
            //    {
            //        //PS_Appointment appointment = apptRecord.First();
            //        //if (apptData.appointmentLocation != null)
            //        //{
            //        //    appointment.AppointLocation = apptData.appointmentLocation;
            //        //}
            //        //if (apptData.appointmentNotes != null)
            //        //{
            //        //    appointment.AppointNotes = apptData.appointmentNotes;
            //        //}
            //        //if (apptData.appointmentStatus != null)
            //        //{
            //        //    appointment.AppointProgStatus = apptData.appointmentStatus;
            //        //}
            //        //if (apptData.appointmentStartDateTime != null)
            //        //{
            //        //    appointment.AppointStartDateTime = apptData.appointmentStartDateTime;
            //        //}
            //        //if (apptData.appointmentEndDateTime != null)
            //        //{
            //        //    appointment.AppointEndDateTime = apptData.appointmentEndDateTime;
            //        //}
            //        //if (apptData.appointmentTitle != null)
            //        //{
            //        //    appointment.AppointTitle = apptData.appointmentTitle;
            //        //}
            //        //if (apptData.appointmentType != null)
            //        //{
            //        //    appointment.AppointType = apptData.appointmentType;
            //        //}
            //        //if (apptData.appointmentValidity != null)
            //        //{
            //        //    appointment.AppointValidity = apptData.appointmentValidity;
            //        //}
            //        //if (apptData.createdBy != null)
            //        //{
            //        //    appointment.CreatedBy = apptData.createdBy;
            //        //}

            //        //if (apptData.surveyourAvailability != null)
            //        //{
            //        //    appointment.SurveyourStatus = apptData.surveyourAvailability;
            //        //}
            //        //if (apptData.surveyType != null)
            //        //{
            //        //    appointment.SurveyType = apptData.surveyType;
            //        //}
            //        //if (apptData.surveyorUserName != null)
            //        //{
            //        //    appointment.SurveyourUserName = apptData.surveyorUserName;
            //        //}
            //        ////appointment.CreatedDate = DateTime.Now;
            //        //appointment.ModifiedDate = DateTime.Now;
            //        //if (apptData.surveyorAlert != null)
            //        //{
            //        //    appointment.AppointmentAlert = apptData.surveyorAlert;
            //        //}
            //        //if (apptData.appointmentCalendar != null)
            //        //{
            //        //    appointment.AppointmentCalendar = apptData.appointmentCalendar;
            //        //}
            //        //context.SaveChanges();



            //        //var apptPropertyRecord = context.PS_Property2Appointment.Where(prop => prop.AppointId == apptData.appointmentId);
            //        //if (apptPropertyRecord.Count() > 0)
            //        //{
            //        //    PS_Property2Appointment property = apptPropertyRecord.First();

            //        //    property.AppointId = apptData.appointmentId;
            //        //    if (apptData.customer != null)
            //        //    {
            //        //        property.CustomerId = apptData.customer.customerId;

            //        //        if (apptData.customer.property != null)
            //        //        {
            //        //            property.PropertyId = apptData.customer.property.propertyId;
            //        //        }
            //        //        if (apptData.customer.property != null)
            //        //        {
            //        //            property.TenancyId = apptData.customer.property.tenancyId;
            //        //        }
            //        //    }
            //        //    context.SaveChanges();
            //        //}

            //        //context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);


            //        //trans.Complete();
            //        //success = true;
            //    }
            //}
            //return success;
            #endregion
        }
        //Change#11 - Behroz -  12/12/2012 - End

        #endregion

        #region delete Appointment
        /// <summary>
        /// This function is used to delete the appointment from the server
        /// </summary>
        /// <param name="appointmentId">appointment id</param>
        /// <returns>returns true or false based on deleteion</returns>

        public bool deleteAppointment(int appointmentId)
        {
            bool success = false;

            using (TransactionScope trans = new TransactionScope())
            {
                var apptRecord = context.PS_Appointment.Where(app => app.AppointId == appointmentId);

                //Find the survey aginst give appointment id. 
                var appoint2survey = context.PS_Appointment2Survey.Where(app => app.AppointId == appointmentId);

                //If appointment exist but survey is not saved against appointment then user can delete the appointment
                if (apptRecord.Count() > 0 && appoint2survey.Count() <= 0)
                {
                    List<PS_Property2Appointment> propertyChilds = new List<PS_Property2Appointment>();
                    //var propertyChildRecords = 
                    context.PS_Property2Appointment.Where(prop => prop.AppointId == appointmentId).ToList().ForEach(p => context.PS_Property2Appointment.DeleteObject(p));
                    context.PS_Appointment2Survey.Where(sur => sur.AppointId == appointmentId).ToList().ForEach(s => context.PS_Appointment2Survey.DeleteObject(s));

                    //context.PS_Property2Appointment.Where(prop => prop.AppointId == appointmentId).ToList().ForEach(r => context.PS_Property2Appointment.DeleteObject(r));
                    //detail.Regs.ToList().ForEach(r => db.Regs.DeleteObject(r));

                    PS_Appointment appointment = apptRecord.First();
                    context.PS_Appointment.DeleteObject(appointment);
                    context.SaveChanges();


                    trans.Complete();
                    success = true;
                }
            }
            return success;

        }
        #endregion

        #region get Appointment
        /// <summary>
        /// This function returns an appointment based on appointment id
        /// </summary>
        /// <param name="appointmentId">appointment id</param>
        /// <returns>appointment data object</returns>
        public AppointmentDataStock getAppointment(int appointmentId)
        {
            var appointment = (from app in context.PS_Appointment
                               join p2a in context.PS_Property2Appointment on app.AppointId equals p2a.AppointId
                               join cus in context.C__CUSTOMER on p2a.CustomerId equals cus.CUSTOMERID
                               join gtitle in context.G_TITLE on cus.TITLE equals gtitle.TITLEID into tempgti
                               from gti in tempgti.DefaultIfEmpty()
                               join cad in context.C_ADDRESS on cus.CUSTOMERID equals cad.CUSTOMERID
                               join cut in context.C_CUSTOMERTENANCY on cus.CUSTOMERID equals cut.CUSTOMERID
                               join ten in context.C_TENANCY on cut.TENANCYID equals ten.TENANCYID
                               join pro in context.P__PROPERTY on ten.PROPERTYID equals pro.PROPERTYID
                               where ten.ENDDATE == null && cad.ISDEFAULT == 1 && app.AppointId == appointmentId

                               select new AppointmentDataStock
                               {
                                   appointmentId = app.AppointId,
                                   surveyourAvailability = app.SurveyourStatus,
                                   appointmentLocation = app.AppointLocation,

                                   appointmentStartDateTime = app.AppointStartDateTime,
                                   appointmentEndDateTime = app.AppointEndDateTime,
                                   appointmentNotes = app.AppointNotes,

                                   appointmentValidity = app.AppointValidity,
                                   appointmentType = app.AppointType,
                                   appointmentStatus = app.AppointProgStatus,
                                   surveyorUserName = app.SurveyourUserName,

                                   customer = new CustomerData
                                   {
                                       customerId = cus.CUSTOMERID,
                                       title = gti.DESCRIPTION,
                                       firstName = cus.FIRSTNAME,

                                       middleName = cus.MIDDLENAME,
                                       lastName = cus.LASTNAME,
                                       telephone = cad.TEL,

                                       mobile = cad.MOBILE,
                                       fax = cad.FAX,
                                       email = cad.EMAIL,
                                       property = new PropertyData
                                       {
                                           propertyId = p2a.PropertyId,
                                           tenancyId = p2a.TenancyId,
                                           houseNumber = pro.HOUSENUMBER,

                                           flatNumber = pro.FLATNUMBER,
                                           address1 = pro.ADDRESS1,
                                           address2 = pro.ADDRESS2,
                                           address3 = pro.ADDRESS3,

                                           townCity = pro.TOWNCITY,
                                           postCode = pro.POSTCODE,
                                           county = pro.COUNTY,
                                           lastSurveyDate = p2a.LastSurveyDate


                                       }

                                   },
                               });



            AppointmentDataStock appointmentItem = new AppointmentDataStock();
            if (appointment.Count() > 0)
            {
                appointmentItem = appointment.FirstOrDefault();
            }

            return appointmentItem;
        }
        #endregion

        #region is Appointment Exists against property
        /// <summary>
        /// This function checks the property id against a valid appointment, if that exists then it returns true otherwise false
        /// </summary>
        /// <param name="propertyId">property id </param>
        /// <param name="appointmentType">appoinmtment type</param>
        /// <returns>returns true or false</returns>

        public bool isAppointmentExistsAgainstProperty(string propertyId, string appointmentType, string appointmentStatus)
        {
            bool success = false;
            var apptRecord = (from pte in context.PS_Property2Appointment
                              join apt in context.PS_Appointment on pte.AppointId equals apt.AppointId
                              where pte.PropertyId == propertyId && apt.AppointType.ToLower() == appointmentType.ToLower()
                              && apt.AppointProgStatus.ToLower() == appointmentStatus
                              //&& apt.AppointStartDateTime >= DateTime.Now
                              select apt);

            if (apptRecord.Count() > 0)
            {
                success = true;
            }
            else
            {
                success = false;
            }

            return success;
        }

        //Change#29 - Behroz - 19/12/2012 - Start
        public bool isAppointmentExistsAgainstPropertyGas(string propertyId, string appointmentStatus)
        {
            bool success = false;

            List<string> lStatusList = new List<string>() { "No Entry", "CP12 issued" };

            //var data = (from jor in context.AS_JOURNAL 
            //                join app in context.AS_APPOINTMENTS on jor.JOURNALID equals app.JournalId
            //                where jor.PROPERTYID == propertyId && app.APPOINTMENTSTATUS.ToLower() == appointmentStatus.ToLower()
            //                select app);

            var data = (from jor in context.AS_JOURNAL
                        join app in context.AS_APPOINTMENTS on jor.JOURNALID equals app.JournalId
                        where jor.PROPERTYID == propertyId &&
                        (app.APPOINTMENTSTATUS.ToLower() != "finished") &&
                        !lStatusList.Contains(context.AS_Status.Where(s => s.StatusId == jor.STATUSID).FirstOrDefault().Title)
                        select app);

            if (data.Count() > 0)
            {
                success = true;
            }
            else
            {
                success = false;
            }

            return success;
        }
        //Change#29 - Behroz - 19/12/2012 - End
        #endregion

        #region save last survey date
        /// <summary>
        /// This function will save the last survey date
        /// </summary>
        /// <param name="appointmentId">appointment id</param>
        public void saveLastSurveyDate(int appointmentId)
        {
            var p2app = context.PS_Property2Appointment.Where(app => app.AppointId == appointmentId);
            PS_Property2Appointment property2appointment = p2app.First();
            property2appointment.LastSurveyDate = DateTime.Now;
            context.SaveChanges();
        }
        #endregion

        #region save last survey date of a fault appointment
        /// <summary>
        /// This function will save the last survey date of a fault appointment
        /// </summary>
        /// <param name="appointmentId">appointment id</param>
        public void saveLastSurveyDateFault(int appointmentId)
        {
            var apptlist = context.FL_CO_APPOINTMENT.Where(app => app.AppointmentID == appointmentId);
            FL_CO_APPOINTMENT apptFault = apptlist.First();
            apptFault.LastActionDate = DateTime.Now;
            context.SaveChanges();
        }
        #endregion

        #region get last survey date

        public DateTime? getLastSurveyDate(string propertyId)
        {
            var p2app = context.PS_Property2Appointment.Where(app => app.PropertyId == propertyId).OrderByDescending(app => app.LastSurveyDate);
            PS_Property2Appointment property2appointment = p2app.First();

            return property2appointment.LastSurveyDate;
        }
        #endregion

        #region "Get All Status"
        //Change - Behroz - 12/10/2012 - Start

        /// <summary>
        /// This method returns status from AS_Status table for gas only
        /// </summary>
        /// <returns></returns>
        public List<StatusData> getAllStatusData()
        {
            var statusData = context.AS_Status.Where(status => status.InspectionTypeID == 1);

            List<StatusData> statuslist = new List<StatusData>();

            if (statusData.Count() > 0)
            {
                List<AS_Status> ldata = statusData.ToList();
                foreach (var item in ldata)
                {
                    StatusData litem = new StatusData();
                    litem.createdBy = item.CreatedBy;
                    litem.createdDate = item.CreatedDate;
                    litem.inspectionTypeId = item.InspectionTypeID;
                    litem.isEditable = item.IsEditable;
                    litem.modifiedBy = item.ModifiedBy;
                    litem.modifiedDate = item.ModifiedDate;
                    litem.ranking = item.Ranking;
                    litem.statusId = item.StatusId;
                    litem.title = item.Title;

                    statuslist.Add(litem);
                }
            }

            return statuslist;
        }

        //Change - Behroz - 12/10/2012 - End
        #endregion

        #region "Get Customer List against a tenancy"

        /// <summary>
        /// This method returns list of customers against a tenancy id
        /// </summary>
        /// <returns></returns>
        private List<CustomerData> getCustomersInfo(int? tenancyId)
        {
            var custData = (from ten in context.C_TENANCY
                            join cut in context.C_CUSTOMERTENANCY on ten.TENANCYID equals cut.TENANCYID
                            join cus in context.C__CUSTOMER on cut.CUSTOMERID equals cus.CUSTOMERID
                            join gtitle in context.G_TITLE on cus.TITLE equals gtitle.TITLEID into tempgti
                            from gti in tempgti.DefaultIfEmpty()
                            join cad in context.C_ADDRESS on cus.CUSTOMERID equals cad.CUSTOMERID
                            where cad.ISDEFAULT == 1 && (cut.ENDDATE == null || cut.ENDDATE > DateTime.Now) && ten.TENANCYID == tenancyId
                            select new CustomerData
                            {
                                customerId = cus.CUSTOMERID,
                                title = gti.DESCRIPTION,
                                firstName = cus.FIRSTNAME,
                                middleName = cus.MIDDLENAME,
                                lastName = cus.LASTNAME,
                                telephone = cad.TEL,
                                mobile = cad.MOBILE,
                                fax = cad.FAX,
                                email = cad.EMAIL
                            });

            if (custData.Count() > 0)
            {
                return custData.ToList();
            }
            else
            {
                return null;
            }
        }

        #endregion

        #region "Set additional data for Fault Appointment"

        /// <summary>
        /// This method sets additional data for Fault Appointment
        /// </summary>
        /// <returns></returns>
        private void setAdditionalDataFault(AppointmentListFault alf)
        {
            /* Setting the Tenancy Information */

            if (alf.tenancyId != null)
            {
                alf.defaultCustomerId = (from cut in context.C_CUSTOMERTENANCY where alf.tenancyId == cut.TENANCYID select cut.CUSTOMERID).FirstOrDefault();

                alf.customerList = this.getCustomersInfo(alf.tenancyId);

                if (alf.customerList != null)
                {
                    for (int i = 0; i < alf.customerList.Count(); i++)
                    {
                        if (alf.customerList.ElementAt(i).customerId == alf.defaultCustomerId)
                        {
                            alf.defaultCustomerIndex = i;
                        }
                    }
                }
            }
            else
            {
                alf.defaultCustomerId = 0;
                alf.customerList = null;
            }

            /* Setting the Customer Vulnerability Information */

            if (alf.customerList != null)
            {
                CustomerDal custDal = new CustomerDal();
                List<PS_GetCustomerRisk_Result> custRiskList = new List<PS_GetCustomerRisk_Result>();
                CustomerVulnerabilityData custVulData = new CustomerVulnerabilityData();

                foreach (CustomerData custData in alf.customerList)
                {
                    int customerId = (int)custData.customerId;

                    //add customer vulnerability data
                    custVulData = custDal.getCustomerVulnerabilities(customerId);

                    custData.customerVulnerabilityData = custVulData;

                    //get custoemr risk list
                    custRiskList = custDal.getCustomerRisk(customerId);

                    for (int i = 0; i < custRiskList.Count; i++)
                    {
                        CustomerRiskData custRiskData = new CustomerRiskData();
                        custRiskData.riskCatDesc = custRiskList[i].CATDESC;
                        custRiskData.riskSubCatDesc = custRiskList[i].SUBCATDESC;

                        //add object of risk to  appointment data 
                        custData.customerRiskData.Add(custRiskData);
                    }

                    custData.Address = null;
                    custData.property = null;
                }
            }

            /* Setting the Property Asbestos Risk Information */

            //add property asbestos risk
            PropertyDal propDal = new PropertyDal();
            List<PropertyAsbestosData> propAsbListData = new List<PropertyAsbestosData>();
            string propertyId = alf.property.propertyId;

            //get property asbestos risk
            propAsbListData = propDal.getPropertyAsbestosRisk(propertyId);

            for (int j = 0; j < propAsbListData.Count; j++)
            {
                PropertyAsbestosData propAsbData = new PropertyAsbestosData();
                propAsbData.asbRiskLevelDesc = propAsbListData[j].asbRiskLevelDesc;
                propAsbData.riskDesc = propAsbListData[j].riskDesc;

                //add object of property risk to  appointment data 
                alf.property.propertyAsbestosData.Add(propAsbData);
            }

            alf.property.propertyPicture = null;

            /* Setting the Appointment Job Data Information */

            alf.JobDataList = getJobDataList(alf.appointmentId);
        }
        #endregion

        #region "Set additional data for Stock Appointment"
        //Code added by Abdul Wahhab - 09/07/2013 - START

        /// <summary>
        /// This method sets additional data for Stock Appointment
        /// </summary>
        /// <returns></returns>
        private void setAdditionalDataStock(AppointmentListStock als)
        {
            /* Setting the Stock Appointment Specific Data Information */

            als.property.lastSurveyDate = this.getLastSurveyDate(als.property.propertyId);
            als.appInfoData = this.getTotalNoEntries(als.property.propertyId);
            als.CP12Info = new IssuedReceivedByDal().getIssuedReceivedByFormData(als.appointmentId);

            /* Setting Tenancy Data Information */

            if (als.tenancyId != null)
            {
                als.defaultCustomerId = (int)context.PS_Property2Appointment.Where(p2a => p2a.AppointId == als.appointmentId).FirstOrDefault().CustomerId;

                als.customerList = this.getCustomersInfo(als.tenancyId);

                for (int i = 0; i < als.customerList.Count(); i++)
                {
                    if (als.customerList.ElementAt(i).customerId == als.defaultCustomerId)
                    {
                        als.defaultCustomerIndex = i;
                    }
                }
            }
            else
            {
                als.defaultCustomerId = 0;
                als.customerList = null;
            }

            /* Setting the Customer Vulnerability Information */

            if (als.customerList != null)
            {
                CustomerDal custDal = new CustomerDal();
                List<PS_GetCustomerRisk_Result> custRiskList = new List<PS_GetCustomerRisk_Result>();
                CustomerVulnerabilityData custVulData = new CustomerVulnerabilityData();

                foreach (CustomerData custData in als.customerList)
                {
                    int customerId = (int)custData.customerId; // Line changed by Abdul Wahhab - 19/06/2013

                    //add customer vulnerability data
                    custVulData = custDal.getCustomerVulnerabilities(customerId);

                    custData.customerVulnerabilityData = custVulData;

                    //get custoemr risk list
                    custRiskList = custDal.getCustomerRisk(customerId);

                    for (int i = 0; i < custRiskList.Count; i++)
                    {
                        CustomerRiskData custRiskData = new CustomerRiskData();
                        custRiskData.riskCatDesc = custRiskList[i].CATDESC;
                        custRiskData.riskSubCatDesc = custRiskList[i].SUBCATDESC;

                        //add object of risk to  appointment data 
                        custData.customerRiskData.Add(custRiskData);
                    }

                    custData.Address = null;
                    custData.property = null;
                }
            }

            /* Setting the Property Asbestos Risk Information */

            //add property asbestos risk
            PropertyDal propDal = new PropertyDal();
            List<PropertyAsbestosData> propAsbListData = new List<PropertyAsbestosData>();
            string propertyId = als.property.propertyId;


            //get property asbestos risk
            propAsbListData = propDal.getPropertyAsbestosRisk(propertyId);

            for (int j = 0; j < propAsbListData.Count; j++)
            {
                PropertyAsbestosData propAsbData = new PropertyAsbestosData();
                propAsbData.asbRiskLevelDesc = propAsbListData[j].asbRiskLevelDesc;
                propAsbData.riskDesc = propAsbListData[j].riskDesc;

                //add object of property risk to  appointment data 
                als.property.propertyAsbestosData.Add(propAsbData);
            }

            als.property.propertyPicture = null;
        }

        //Code added by Abdul Wahhab - 09/07/2013 - END
        #endregion

        #region "Set additional data for Gas Appointment"

        /// <summary>
        /// This method sets additional data for Gas Appointment
        /// </summary>
        /// <returns></returns>
        private void setAdditionalDataGas(AppointmentListGas alg)
        {
            /* Setting the Stock Appointment Specific Data Information */

            alg.appInfoData = this.getTotalNoEntriesGas(alg.property.propertyId, alg.journalId);
            alg.CP12Info = new IssuedReceivedByDal().getIssuedReceivedByFormData(alg.journal.propertyId);

            /* Setting Tenancy Data Information */

            if (alg.tenancyId != null)
            {
                alg.defaultCustomerId = (from cut in context.C_CUSTOMERTENANCY where alg.tenancyId == cut.TENANCYID select cut.CUSTOMERID).FirstOrDefault();

                alg.customerList = this.getCustomersInfo(alg.tenancyId);

                if (alg.customerList != null)
                {
                    for (int i = 0; i < alg.customerList.Count(); i++)
                    {
                        if (alg.customerList.ElementAt(i).customerId == alg.defaultCustomerId)
                        {
                            alg.defaultCustomerIndex = i;
                        }
                    }
                }
            }
            else
            {
                alg.defaultCustomerId = 0;
                alg.customerList = null;
            }

            /* Setting the Customer Vulnerability Information */

            if (alg.customerList != null)
            {
                CustomerDal custDal = new CustomerDal();
                List<PS_GetCustomerRisk_Result> custRiskList = new List<PS_GetCustomerRisk_Result>();
                CustomerVulnerabilityData custVulData = new CustomerVulnerabilityData();

                foreach (CustomerData custData in alg.customerList)
                {
                    int customerId = (int)custData.customerId;

                    //add customer vulnerability data
                    custVulData = custDal.getCustomerVulnerabilities(customerId);

                    custData.customerVulnerabilityData = custVulData;

                    //get custoemr risk list
                    custRiskList = custDal.getCustomerRisk(customerId);

                    for (int i = 0; i < custRiskList.Count; i++)
                    {
                        CustomerRiskData custRiskData = new CustomerRiskData();
                        custRiskData.riskCatDesc = custRiskList[i].CATDESC;
                        custRiskData.riskSubCatDesc = custRiskList[i].SUBCATDESC;

                        //add object of risk to  appointment data 
                        custData.customerRiskData.Add(custRiskData);
                    }

                    custData.Address = null;
                    custData.property = null;
                }
            }

            /* Setting the Property Asbestos Risk Information */

            //add property asbestos risk
            PropertyDal propDal = new PropertyDal();
            List<PropertyAsbestosData> propAsbListData = new List<PropertyAsbestosData>();
            string propertyId = alg.property.propertyId;

            //get property asbestos risk
            propAsbListData = propDal.getPropertyAsbestosRisk(propertyId);

            for (int j = 0; j < propAsbListData.Count; j++)
            {
                PropertyAsbestosData propAsbData = new PropertyAsbestosData();
                propAsbData.asbRiskLevelDesc = propAsbListData[j].asbRiskLevelDesc;
                propAsbData.riskDesc = propAsbListData[j].riskDesc;

                //add object of property risk to  appointment data 
                alg.property.propertyAsbestosData.Add(propAsbData);
            }

            alg.property.propertyPicture = null;
        }

        #endregion

        #region is Property Exists

        /// <summary>
        /// This function checks the property id, if that exists then it returns true otherwise false
        /// </summary>
        /// <param name="propertyId">property id </param>        
        /// <returns>returns true or false</returns>
        public bool isPropertyExists(string propertyId)
        {
            bool success = false;
            var propertyRecord = context.P__PROPERTY.Where(pro => pro.PROPERTYID.ToLower() == propertyId.ToLower());

            if (propertyRecord.Count() > 0)
            {
                success = true;
            }
            else
            {
                success = false;
            }

            return success;
        }
        #endregion
    }
}
