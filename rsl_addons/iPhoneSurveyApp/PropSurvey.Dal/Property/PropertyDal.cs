﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Transactions;
using PropSurvey.Contracts.Data;
using PropSurvey.Dal.Base;
using PropSurvey.Entities;

namespace PropSurvey.Dal.Property
{
    public class PropertyDal : BaseDal
    {
        #region find Property
        // Code changed by Abdul Wahhab - 19/06/2013 - START
        /// <summary>
        /// This function returns the property against specified parameters
        /// </summary>
        /// <param name="skip">how many records client application want to skip. e.g 10,20</param>
        /// <param name="top">how many records client application wants to get</param>
        /// <param name="reference">reference number </param>
        /// <param name="houseNumber">house number of customer</param>
        /// <param name="street">street of customer</param>
        /// <param name="postCode">post code of customer</param>
        /// <param name="surName">sur name of customer</param>
        /// <returns>It retruns the list of customer data's object</returns>
        public List<CustomerData> findProperty(int skip, int top, string reference = "", string houseNumber = "", string street = "", string postCode = "", string surName = "")
        {
            var proWithCustomer = (from cus in context.C__CUSTOMER
                                   join gtitle in context.G_TITLE on cus.TITLE equals gtitle.TITLEID into tempgti
                                   from gti in tempgti.DefaultIfEmpty()
                                   join cad in context.C_ADDRESS on cus.CUSTOMERID equals cad.CUSTOMERID
                                   join cut in context.C_CUSTOMERTENANCY on cus.CUSTOMERID equals cut.CUSTOMERID
                                   join ten in context.C_TENANCY on cut.TENANCYID equals ten.TENANCYID
                                   join pro in context.P__PROPERTY on ten.PROPERTYID equals pro.PROPERTYID
                                   join psta in context.P_STATUS on pro.STATUS equals psta.STATUSID
                                   where ten.ENDDATE == null && cad.ISDEFAULT == 1 && psta.DESCRIPTION == "Let"
                                   select new CustomerData
                                   {
                                       customerId = cus.CUSTOMERID,
                                       title = gti.DESCRIPTION,
                                       firstName = cus.FIRSTNAME,
                                       middleName = cus.MIDDLENAME,
                                       lastName = cus.LASTNAME,
                                       property = new PropertyData
                                       {
                                           houseNumber = pro.HOUSENUMBER,
                                           flatNumber = pro.FLATNUMBER,
                                           address1 = pro.ADDRESS1,
                                           address2 = pro.ADDRESS2,
                                           address3 = pro.ADDRESS3,
                                           townCity = pro.TOWNCITY,
                                           postCode = pro.POSTCODE,
                                           county = pro.COUNTY,
                                           tenancyId = ten.TENANCYID,
                                           propertyId = pro.PROPERTYID
                                       }
                                   });

            var proWithoutCustomer = (from pro in context.P__PROPERTY
                                      join psta in context.P_STATUS on pro.STATUS equals psta.STATUSID
                                      where psta.DESCRIPTION != "Let"
                                      select new CustomerData
                                      {
                                          title = "N/A",
                                          firstName = "N/A",
                                          middleName = "N/A",
                                          lastName = "N/A",
                                          property = new PropertyData
                                          {
                                              houseNumber = pro.HOUSENUMBER,
                                              flatNumber = pro.FLATNUMBER,
                                              address1 = pro.ADDRESS1,
                                              address2 = pro.ADDRESS2,
                                              address3 = pro.ADDRESS3,
                                              townCity = pro.TOWNCITY,
                                              postCode = pro.POSTCODE,
                                              county = pro.COUNTY,
                                              propertyId = pro.PROPERTYID
                                          }
                                      });

            if (!string.IsNullOrEmpty(reference))
            {
                proWithCustomer = proWithCustomer.Where(p => p.property.propertyId == reference);
                proWithoutCustomer = proWithoutCustomer.Where(p => p.property.propertyId == reference);
            }

            if (!string.IsNullOrEmpty(houseNumber))
            {
                proWithCustomer = proWithCustomer.Where(p => p.property.houseNumber.ToLower() == houseNumber.ToLower());
                proWithoutCustomer = proWithoutCustomer.Where(p => p.property.houseNumber.ToLower() == houseNumber.ToLower());
            }

            if (!string.IsNullOrEmpty(street))
            {
                proWithCustomer = proWithCustomer.Where(p => p.property.address1.Contains(street) || p.property.address2.Contains(street) || p.property.address3.Contains(street));
                proWithoutCustomer = proWithoutCustomer.Where(p => p.property.address1.Contains(street) || p.property.address2.Contains(street) || p.property.address3.Contains(street));
            }

            if (!string.IsNullOrEmpty(postCode))
            {
                proWithCustomer = proWithCustomer.Where(p => p.property.postCode.ToLower() == postCode.ToLower());
                proWithoutCustomer = proWithoutCustomer.Where(p => p.property.postCode.ToLower() == postCode.ToLower());
            }

            if (!string.IsNullOrEmpty(surName))
            {
                proWithCustomer = proWithCustomer.Where(p => p.lastName.Contains(surName));
                proWithoutCustomer = proWithoutCustomer.Where(p => p.lastName.Contains(surName));
            }

            List<CustomerData> propertyWithCustomer = proWithCustomer.ToList();
            List<CustomerData> propertyWithoutCustomer = proWithoutCustomer.ToList();

            IEnumerable<CustomerData> property = propertyWithCustomer.Union(propertyWithoutCustomer);

            //property = property.OrderBy(p => p.customerId).Skip(skip).Take(top);
            property = property.Skip(skip).Take(top);

            List<CustomerData> propertyList = new List<CustomerData>();

            if (property.Count() > 0)
            {
                propertyList = property.ToList();
            }

            return propertyList;
        }

        //Change - Behroz - 12/12/2012 - Start
        /// <summary>
        /// This function returns the property against specified parameters
        /// </summary>
        /// <param name="skip">how many records client application want to skip. e.g 10,20</param>
        /// <param name="top">how many records client application wants to get</param>
        /// <param name="reference">reference number </param>
        /// <param name="houseNumber">house number of customer</param>
        /// <param name="street">street of customer</param>
        /// <param name="postCode">post code of customer</param>
        /// <param name="surName">sur name of customer</param>
        /// <returns>It retruns the list of customer data's object</returns>
        public List<CustomerData> findPropertyGas(int skip, int top, string reference = "", string houseNumber = "", string street = "", string postCode = "", string surName = "")
        {
            var proWithCustomer = (from cus in context.C__CUSTOMER
                                   join gtitle in context.G_TITLE on cus.TITLE equals gtitle.TITLEID into tempgti
                                   from gti in tempgti.DefaultIfEmpty()
                                   join cad in context.C_ADDRESS on cus.CUSTOMERID equals cad.CUSTOMERID
                                   join cut in context.C_CUSTOMERTENANCY on cus.CUSTOMERID equals cut.CUSTOMERID
                                   join ten in context.C_TENANCY on cut.TENANCYID equals ten.TENANCYID
                                   join pro in context.P__PROPERTY on ten.PROPERTYID equals pro.PROPERTYID
                                   join psta in context.P_STATUS on pro.STATUS equals psta.STATUSID
                                   join jor in context.AS_JOURNAL on pro.PROPERTYID equals jor.PROPERTYID
                                   //Do join with P_LGSR to get the certificate expiry
                                   join lgsr in context.P_LGSR on jor.PROPERTYID equals lgsr.PROPERTYID into result
                                   from data in result.DefaultIfEmpty()

                                   //orderby data.RECEVIEDDATE ascending

                                   where ten.ENDDATE == null && cad.ISDEFAULT == 1 && jor.ISCURRENT == true && psta.DESCRIPTION == "Let" &&
                                   cut.ENDDATE == null && jor.INSPECTIONTYPEID == 1
                                   && cut.CUSTOMERTENANCYID == (context.C_CUSTOMERTENANCY.Where(a => a.TENANCYID == ten.TENANCYID).Min(a => a.CUSTOMERTENANCYID))
                                   select new CustomerData
                                   {
                                       customerId = cus.CUSTOMERID,
                                       title = gti.DESCRIPTION,
                                       firstName = cus.FIRSTNAME,
                                       middleName = cus.MIDDLENAME,
                                       lastName = cus.LASTNAME,
                                       property = new PropertyData
                                       {
                                           houseNumber = pro.HOUSENUMBER,
                                           flatNumber = pro.FLATNUMBER,
                                           address1 = pro.ADDRESS1,
                                           address2 = pro.ADDRESS2,
                                           address3 = pro.ADDRESS3,
                                           townCity = pro.TOWNCITY,
                                           postCode = pro.POSTCODE,
                                           county = pro.COUNTY,
                                           tenancyId = ten.TENANCYID,
                                           propertyId = pro.PROPERTYID,
                                           //show expiry date in the property
                                           certificateExpiry = data.RECEVIEDDATE
                                       }
                                   });

            var proWithoutCustomer = (from pro in context.P__PROPERTY
                                      join psta in context.P_STATUS on pro.STATUS equals psta.STATUSID
                                      join jor in context.AS_JOURNAL on pro.PROPERTYID equals jor.PROPERTYID
                                      //Do join with P_LGSR to get the certificate expiry
                                      join lgsr in context.P_LGSR on jor.PROPERTYID equals lgsr.PROPERTYID into result
                                      from data in result.DefaultIfEmpty()

                                      //orderby data.RECEVIEDDATE ascending

                                      where jor.ISCURRENT == true && psta.DESCRIPTION != "Let" && jor.INSPECTIONTYPEID == 1
                                      select new CustomerData
                                      {
                                          title = "N/A",
                                          firstName = "N/A",
                                          middleName = "N/A",
                                          lastName = "N/A",
                                          property = new PropertyData
                                          {
                                              houseNumber = pro.HOUSENUMBER,
                                              flatNumber = pro.FLATNUMBER,
                                              address1 = pro.ADDRESS1,
                                              address2 = pro.ADDRESS2,
                                              address3 = pro.ADDRESS3,
                                              townCity = pro.TOWNCITY,
                                              postCode = pro.POSTCODE,
                                              county = pro.COUNTY,
                                              propertyId = pro.PROPERTYID,
                                              //show expiry date in the property
                                              certificateExpiry = data.RECEVIEDDATE
                                          }
                                      });

            if (!string.IsNullOrEmpty(reference))
            {
                proWithCustomer = proWithCustomer.Where(p => p.property.propertyId == reference);
                proWithoutCustomer = proWithoutCustomer.Where(p => p.property.propertyId == reference);
            }

            if (!string.IsNullOrEmpty(houseNumber))
            {
                proWithCustomer = proWithCustomer.Where(p => p.property.houseNumber.ToLower() == houseNumber.ToLower());
                proWithoutCustomer = proWithoutCustomer.Where(p => p.property.houseNumber.ToLower() == houseNumber.ToLower());
            }

            if (!string.IsNullOrEmpty(street))
            {
                proWithCustomer = proWithCustomer.Where(p => p.property.address1.Contains(street) || p.property.address2.Contains(street) || p.property.address3.Contains(street));
                proWithoutCustomer = proWithoutCustomer.Where(p => p.property.address1.Contains(street) || p.property.address2.Contains(street) || p.property.address3.Contains(street));
            }

            if (!string.IsNullOrEmpty(postCode))
            {
                proWithCustomer = proWithCustomer.Where(p => p.property.postCode.ToLower() == postCode.ToLower());
                proWithoutCustomer = proWithoutCustomer.Where(p => p.property.postCode.ToLower() == postCode.ToLower());
            }

            if (!string.IsNullOrEmpty(surName))
            {
                proWithCustomer = proWithCustomer.Where(p => p.lastName.Contains(surName));
                proWithoutCustomer = proWithoutCustomer.Where(p => p.lastName.Contains(surName));
            }

            List<CustomerData> propertyWithCustomer = proWithCustomer.ToList();
            List<CustomerData> propertyWithoutCustomer = proWithoutCustomer.ToList();

            IEnumerable<CustomerData> property = propertyWithCustomer.Union(propertyWithoutCustomer);

            //The properties that have null certificate expiry should be on the end.
            //property = property.OrderBy(p => p.property.certificateExpiry).Skip(skip).Take(top);
            property = property.OrderByDescending(p => p.property.certificateExpiry.HasValue).ThenBy(p => p.property.certificateExpiry).Skip(skip).Take(top);

            List<CustomerData> propertyList = new List<CustomerData>();
            if (property.Count() > 0)
            {
                propertyList = property.ToList();
            }

            return propertyList;
        }
        //Change - Behroz - 12/12/2012 - End
        // Code changed by Abdul Wahhab - 19/06/2013 - END
        #endregion

        #region get Property Images

        /// <summary>
        /// This function returns the list of images which were previously saved against property.
        /// </summary>
        /// <param name="propertyId">property id</param>
        /// <param name="itemId">item Id</param>
        /// <returns>returns the list of image names against property</returns>

        public List<PropertyPictureData> getPropertyImages(string propertyId, int itemId)
        {
            var propertyPic = (from prop in context.PA_PROPERTY_ITEM_IMAGES
                               orderby prop.PROPERTYID descending
                               where prop.PROPERTYID.ToLower() == propertyId.ToLower()
                               select new PropertyPictureData
                               {
                                   propertyPictureName = prop.ImageName,
                                   propertyPictureId = prop.SID,
                                   imagePath = prop.ImagePath,

                               });

            List<PropertyPictureData> propertyPicList = new List<PropertyPictureData>();

            if (propertyPic.Count() > 0)
            {
                propertyPicList = propertyPic.ToList();
            }
            return propertyPicList;
        }
        #endregion

        #region get Default Property Image Name
        /// <summary>
        /// This function returns the default image name which was saved against property
        /// </summary>
        /// <param name="propertyId">property id</param>
        /// <returns>returns the name of the image</returns>
        /// 
        public string getDefaultPropertyImageName(string propertyId)
        {
            string imageName = String.Empty;

            //var property = (from pro in context.PA_PROPERTY_ITEM_IMAGES
            //                where pro.PROPERTYID.ToLower() == propertyId.ToLower() && pro.isDefault == true
            //                select pro.ImageName);

            //if (property.Count() > 0)
            //{
            //    imageName = property.Single().ToString();
            //}

            return imageName;
        }
        #endregion

        #region save Property Image

        /// <summary>
        /// This funciton save the property image in the database
        /// </summary>
        /// <param name="propPicData">property picture data</param>
        /// <param name="propertyId">property id</param>
        /// <returns>true or false upon successfull saving on disk and on db</returns>

        public bool savePropertyImage(PropertyPictureData propPicData)
        {
            bool success = false;
            using (TransactionScope trans = new TransactionScope())
            {
                {
                    PS_Appointment2Survey savedSurvey = new PS_Appointment2Survey();
                    PS_Survey survey = new PS_Survey();

                    //this will contain the existing /already saved survey id
                    int existingSurveyId = 0;

                    //Find the survey aginst give appointment id. 
                    var appoint2survey = context.PS_Appointment2Survey.Where(app => app.AppointId == propPicData.appointmentId);

                    //If survey already saved against the appointment id then get the existing survey id
                    if (appoint2survey.Count() > 0)
                    {
                        //get the existing survey id
                        savedSurvey = appoint2survey.First();
                        existingSurveyId = savedSurvey.SurveyId;
                    }

                    //if survey already saved then save the property image against that survey id
                    if (existingSurveyId > 0)
                    {
                        //save the property image against exisiting survey id
                        this.savePsPropertyImage(propPicData, existingSurveyId);
                    }
                    else if (existingSurveyId == 0)
                    {
                        //save the new survey
                        survey.PROPERTYID = propPicData.propertyId;
                        survey.CompletedBy = propPicData.createdBy;
                        survey.SurveyDate = propPicData.createdOn;

                        context.AddToPS_Survey(survey);
                        context.SaveChanges();

                        //existing survey does not exist then entry will go to appointment2survey

                        PS_Appointment2Survey appSurv = new PS_Appointment2Survey();
                        appSurv.AppointId = propPicData.appointmentId;
                        appSurv.SurveyId = survey.SurveyId;

                        context.AddToPS_Appointment2Survey(appSurv);
                        context.SaveChanges();

                        //save the property image against new survey id
                        this.savePsPropertyImage(propPicData, survey.SurveyId);
                    }

                    //get the image from pa_property_item_images
                    PA_PROPERTY_ITEM_IMAGES paImage = new PA_PROPERTY_ITEM_IMAGES();

                    //prepare to insert the new one
                    paImage.ItemId = propPicData.itemId;
                    paImage.PROPERTYID = propPicData.propertyId;
                    paImage.ImageName = propPicData.propertyPictureName;
                    paImage.ImagePath = propPicData.imagePath;
                    paImage.CreatedBy = propPicData.createdBy;
                    paImage.CreatedOn = DateTime.Now;

                    context.AddToPA_PROPERTY_ITEM_IMAGES(paImage);
                    context.SaveChanges();

                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    trans.Complete();

                    success = true;
                }
            }

            return success;
        }

        //Change#43 - Behroz - 09/01/2013 - Start
        /// <summary>
        /// This funciton save the property image in the database
        /// </summary>
        /// <param name="propPicData">property picture data</param>
        /// <param name="propertyId">property id</param>
        /// <returns>true or false upon successfull saving on disk and on db</returns>
        public bool savePropertyImageGas(PropertyPictureData propPicData)
        {
            bool success = false;
            using (TransactionScope trans = new TransactionScope())
            {
                //get the image from pa_property_item_images
                PA_PROPERTY_ITEM_IMAGES paImage = new PA_PROPERTY_ITEM_IMAGES();

                //prepare to insert the new one
                paImage.ItemId = propPicData.itemId;
                paImage.PROPERTYID = propPicData.propertyId;
                paImage.ImageName = propPicData.propertyPictureName;
                paImage.ImagePath = propPicData.imagePath;
                paImage.CreatedBy = propPicData.createdBy;
                paImage.CreatedOn = DateTime.Now;

                context.AddToPA_PROPERTY_ITEM_IMAGES(paImage);
                context.SaveChanges();

                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                trans.Complete();

                success = true;
            }

            return success;
        }

        /// <summary>
        /// This function gets the item id against the text gas from the table PA_ITEM
        /// </summary>
        /// <returns></returns>
        public int getItemId()
        {
            return context.PA_ITEM.Where(item => item.ItemName.ToLower() == Utilities.Constants.MessageConstants.GasItemName).FirstOrDefault().ItemID;
        }
        //Change#43 - Behroz - 09/01/2013 - End
        #endregion

        #region save ps property image
        /// <summary>
        /// This function always save the new image against survey id
        /// </summary>
        /// <param name="propPicData">Property Pic Data</param>
        /// <param name="surveyId">Survey Id</param>
        public void savePsPropertyImage(PropertyPictureData propPicData, int surveyId)
        {
            PS_Survey_Item_Images psImage = new PS_Survey_Item_Images();

            psImage.ItemId = propPicData.itemId;
            psImage.SurveyId = surveyId;

            psImage.ImageName = propPicData.propertyPictureName;
            psImage.ImagePath = propPicData.imagePath;
            psImage.ImageDate = DateTime.Now;

            //insert the new one 
            context.AddToPS_Survey_Item_Images(psImage);
            context.SaveChanges();
        }
        #endregion

        #region delete Property Image
        /// <summary>
        /// This function is used to delete the image against the property from the server
        /// </summary>
        /// <param name="propertyPictureId">property picture id</param>
        /// <returns>property picture data which is deleted</returns>

        public PropertyPictureData deletePropertyImage(int propertyPictureId)
        {

            PA_PROPERTY_ITEM_IMAGES propDelPic = new PA_PROPERTY_ITEM_IMAGES();
            PropertyPictureData propPicData = new PropertyPictureData();
            string picName = string.Empty;

            var propPic = (from pric in context.PA_PROPERTY_ITEM_IMAGES
                           where pric.SID == propertyPictureId
                           select new PropertyPictureData
                           {
                               propertyId = pric.PROPERTYID,
                               propertyPictureName = pric.ImageName
                           }
                          );

            if (propPic.Count() > 0)
            {
                propPicData = propPic.First();
                propDelPic.SID = propertyPictureId;
                context.PA_PROPERTY_ITEM_IMAGES.Attach(propDelPic);
                context.PA_PROPERTY_ITEM_IMAGES.DeleteObject(propDelPic);
                context.SaveChanges();
            }

            return propPicData;

        }
        #endregion

        #region is Property Appointment Exist

        /// <summary>
        /// this fuction varifies that either specific property exists in database against any appointment
        /// </summary>
        /// <param name="userName">user name</param>
        /// <returns> true or false</returns>

        public bool isPropertyAppointmentExist(string propertyId)
        {
            bool success = false;

            var propRecord = context.PS_Property2Appointment.Where(acc => acc.PropertyId.ToLower() == propertyId.ToLower());

            if (propRecord.Count() > 0)
            {
                success = true;
            }
            else
            {
                success = false;
            }
            return success;
        }

        //Change#43 - Behroz - 09/01/2013 - Start
        /// <summary>
        /// this fuction varifies that either specific property exists in database against any appointment
        /// </summary>
        /// <param name="userName">user name</param>
        /// <returns> true or false</returns>
        public bool isPropertyAppointmentExist(int appointmentId)
        {
            bool success = false;

            var appRecord = context.AS_APPOINTMENTS.Where(app => app.APPOINTMENTID == appointmentId);

            if (appRecord.Count() > 0)
            {
                success = true;
            }
            else
            {
                success = false;
            }
            return success;
        }
        //Change#43 - Behroz - 09/01/2013 - End
        #endregion

        #region get Property Dimensions

        /// <summary>
        /// This function returns ths property dimensions
        /// </summary>
        /// <param name="propertyId">property id</param>
        /// <returns>List of property dimension data object</returns>

        public List<PropertyDimData> getPropertyDimensions(string propertyId)
        {
            var propDim = (from prop in context.PA_PropertyDimension
                           where prop.PropertyId.ToLower() == propertyId.ToLower()
                           select new PropertyDimData
                           {
                               propertyId = prop.PropertyId,
                               roomHeight = prop.RoomHeight,
                               roomLength = prop.RoomLength,
                               roomWidth = prop.RoomWidth,
                               roomName = prop.RoomName

                           });

            List<PropertyDimData> propDimDataList = new List<PropertyDimData>();

            if (propDim.Count() > 0)
            {
                propDimDataList = propDim.ToList();
                PropertyDimData propDimItem = new PropertyDimData();
                propDimItem = propDimDataList.Find(x => x.roomName.Equals("Living Room"));
                if (propDimItem == null)
                {
                    propDimItem = new PropertyDimData("Living Room");
                    propDimDataList.Insert(2, propDimItem);
                }

            }
            else
            {
                PropertyDimData propDimData = new PropertyDimData();
                propDimData = propDimData.getPropertyInitialValues("Kitchen/Diner");
                propDimDataList.Add(propDimData);

                propDimData = propDimData.getPropertyInitialValues("Dining Room");
                propDimDataList.Add(propDimData);

                propDimData = propDimData.getPropertyInitialValues("Living Room");
                propDimDataList.Add(propDimData);

                propDimData = propDimData.getPropertyInitialValues("Bathroom");
                propDimDataList.Add(propDimData);

                propDimData = propDimData.getPropertyInitialValues("Bedroom 1");
                propDimDataList.Add(propDimData);

                propDimData = propDimData.getPropertyInitialValues("Bedroom 2");
                propDimDataList.Add(propDimData);

                propDimData = propDimData.getPropertyInitialValues("Bedroom 3");
                propDimDataList.Add(propDimData);

                propDimData = propDimData.getPropertyInitialValues("Bedroom 4");
                propDimDataList.Add(propDimData);

                //string roomName = "Room";
                //for(int i=1; i< 5; i++){

                //    //PropertyDimData propDimData= new PropertyDimData(roomName+i.ToString());
                //    //propDimDataList.Add(propDimData);
                //}

            }
            return propDimDataList;
        }
        #endregion

        #region update Property Dimensions

        /// <summary>
        /// This function updates the property dimensions
        /// </summary>
        /// <param name="propDimData">This function accepts the list of property dimension data object</param>
        /// <returns>true on successful update and false on un successful update</returns>

        public bool updatePropertyDimensions(List<PropertyDimData> propDimData)
        {
            bool success = false;
            using (TransactionScope trans = new TransactionScope())
            {
                {
                    if (propDimData.Count() > 0)
                    {
                        string propertyId = propDimData[0].propertyId;
                        var dimRecord = context.PA_PropertyDimension.Where(app => app.PropertyId == propertyId);


                        if (dimRecord.Count() > 0)
                        {
                            foreach (var item in dimRecord)
                            {
                                context.PA_PropertyDimension.DeleteObject(item);
                                context.SaveChanges();
                            }
                        }

                        foreach (var item in propDimData)
                        {
                            PA_PropertyDimension pad = new PA_PropertyDimension();
                            pad.PropertyId = item.propertyId;
                            pad.RoomName = item.roomName;
                            pad.RoomHeight = item.roomHeight;
                            pad.RoomLength = item.roomLength;
                            pad.RoomWidth = item.roomWidth;
                            pad.UpdatedBy = item.updatedBy;
                            pad.UpdatedOn = item.updatedOn;

                            context.AddToPA_PropertyDimension(pad);

                            context.SaveChanges();
                            context.Detach(pad);
                        }

                        trans.Complete();
                        success = true;
                    }
                }
            }

            return success;
        }
        #endregion

        #region "get Property Asbestos Risk"
        /// <summary>
        /// This function returns the list of asbestos risk against the property.
        /// </summary>
        /// <param name="propertyId">property id</param>
        /// <returns>the list of asbestos risks</returns>

        public List<PropertyAsbestosData> getPropertyAsbestosRisk(string propertyId)
        {
            var propertyAsbestos = (from pro in context.P__PROPERTY
                                    join par in context.P_PROPERTY_ASBESTOS_RISKLEVEL on pro.PROPERTYID equals par.PROPERTYID
                                    join pas in context.P_PROPERTY_ASBESTOS_RISK on par.PROPASBLEVELID equals pas.PROPASBLEVELID
                                    join asb in context.P_ASBESTOS on par.ASBESTOSID equals asb.ASBESTOSID
                                    join asl in context.P_ASBRISKLEVEL on par.ASBRISKLEVELID equals asl.ASBRISKLEVELID
                                    where pro.PROPERTYID.ToLower() == propertyId.ToLower()

                                    select new PropertyAsbestosData
                                    {
                                        asbestosId = asb.ASBESTOSID,
                                        riskDesc = asb.RISKDESCRIPTION,
                                        asbRiskLevelDesc = asl.ASBRISKLEVELDESCRIPTION
                                    });
            List<PropertyAsbestosData> propAsbList = new List<PropertyAsbestosData>();
            if (propertyAsbestos.Count() > 0)
            {
                propAsbList = propertyAsbestos.ToList();
            }

            return propAsbList;
        }
        #endregion
    }
}
