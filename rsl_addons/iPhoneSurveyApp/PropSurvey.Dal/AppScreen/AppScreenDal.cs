﻿using System.Collections.Generic;
using System.Linq;
using PropSurvey.Dal.Base;
using PropSurvey.Entities;
using PropSurvey.Utilities.Constants;
using System;
using System.Text;

namespace PropSurvey.Dal.AppScreen
{
    public class AppScreenDal : BaseDal
    {
        string propertyId = string.Empty;

        public string getApplicationScreens(string propertyId)
        {
            try
            {

                if (propertyId != string.Empty)
                {
                    this.propertyId = propertyId;
                }

                string completeJson = string.Empty;

                string jsonOpen = string.Empty;
                string jsonClose = string.Empty;
                string innerBlock = string.Empty;

                //Added by Behroz - Start
                //string jsonComplete = string.Empty;
                StringBuilder jsonComplete = new StringBuilder();
                //Added by Behroz - End

                jsonComplete.Append("{\"Survey\":{\"Image\":true, \"NameAddressLabel\":true,\"Type\":\"Menu\",");
                //jsonOpen = "{Survey:{Image:true, NameAddressLabel:true,Type:Menu";
                //jsonClose = "}}";

                var location = (from loc in context.PA_LOCATION
                                orderby loc.LocationSorder ascending
                                select loc).ToList();

                if (location.Count() > 0)
                {
                    jsonComplete.Append( getLocations(location) );
                }

                //Added by Behroz - Start                
                jsonComplete.Append("}}");

                return jsonComplete.ToString();
                //jsonComplete = jsonOpen + innerBlock + jsonClose;                
                //return jsonComplete;
                //Added by Behroz - End
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string getLocations(List<PropSurvey.Entities.PA_LOCATION> location)
        {
            string locationNames = string.Empty;
            string singleLocationFieldBlock = string.Empty;
            string allLocationFieldBlocks = string.Empty;
            string completeLocations = string.Empty;

            //convert the result into list
            var locationList = location.ToList();

            foreach (var loc in locationList)
            {
                //the below statement will produce string like this : "Main Construction","Externals","Internals",
                locationNames = locationNames + "\"" + loc.LocationName + "\",";


                var area = (from ar in context.PA_AREA
                            where ar.LocationId == loc.LocationID
                            orderby ar.AreaSorder ascending
                            select ar).ToList();

                if (area.Count() > 0)
                {
                    //The below statement will produce the statement something like this: "Main Construction":{.....},"Externals":{.....},"Internals":{.....},
                    singleLocationFieldBlock = singleLocationFieldBlock + getArea(area, loc.LocationName) + ",";
                }
            }

            //removing the last comma from string like this: "Main Construction","Externals","Internals",
            locationNames = locationNames.Substring(0, locationNames.Length - 1);

            //the below statement 'll look like this 
            //"Order": ["Main Construction","Externals","Internals"]
            locationNames = "\"Order\":[" + locationNames + "]";


            if (singleLocationFieldBlock.Length > 1)
            {
                //the below statement 'll look like this with comma in the last
                //"Order": ["Main Construction","Externals","Internals"],                
                locationNames = locationNames + ",";

                allLocationFieldBlocks = singleLocationFieldBlock.Substring(0, singleLocationFieldBlock.Length - 1);

                //the below statement will make the string like this: "Fields":{Main Construction":{.....},"Externals":{.....},"Internals":{.....}}
                allLocationFieldBlocks = "\"Fields\":{" + allLocationFieldBlocks + "}";
            }

            //the below statement will make the string like this: 
            //"Order": ["Main Construction","Externals","Internals"],"Fields":{Main Construction":{.....},"Externals":{.....},"Internals":{.....}}            
            completeLocations = locationNames + allLocationFieldBlocks;

            return completeLocations;
        }

        public string getArea(List<PropSurvey.Entities.PA_AREA> area, string locationName)
        {
            string completeAreas = string.Empty;
            string areaNames = string.Empty;
            string singleAreaFieldBlock = string.Empty;
            string allAreaFieldBlocks = string.Empty;

            //convert the result into list
            var areaList = area.ToList();

            foreach (var ar in areaList)
            {
                //the below statement will produce string like this : "Dwelling","Garage","Outbuilding","Communal Building","Apartment Block",
                areaNames = areaNames + "\"" + ar.AreaName + "\",";

                var item = (from it in context.PA_ITEM
                            where it.AreaID == ar.AreaID
                            orderby it.ItemSorder ascending
                            select it).ToList();

                if (item.Count() > 0)
                {
                    //The below statement will produce the statement something like this: "Dwelling":{.....},"Garage":{.....},"Communal Building":{.....},"Apartment Block":{.....},
                    singleAreaFieldBlock = singleAreaFieldBlock + getItem(item, ar.AreaName) + ",";
                }
            }

            //removing the last comma from string like this: "Dwelling","Garage","Outbuilding","Communal Building","Apartment Block",
            areaNames = areaNames.Substring(0, areaNames.Length - 1);

            //the below statement 'll look like this 
            //"Order": ["Dwelling","Garage","Outbuilding","Communal Building","Apartment Block"],            
            areaNames = "\"Order\":[" + areaNames + "]";


            if (singleAreaFieldBlock.Length > 1)
            {
                //the below statement 'll look like this with comma in the last
                //"Order": ["Dwelling","Garage","Outbuilding","Communal Building","Apartment Block"],            
                areaNames = areaNames + ",";

                allAreaFieldBlocks = singleAreaFieldBlock.Substring(0, singleAreaFieldBlock.Length - 1);

                //the below statement will make the string like this: "Fields":{Dwelling":{.....},"Garage":{.....},"Outbuilding":{.....},"Communal Building":{.....},"Apartment Block":{.....}}
                allAreaFieldBlocks = "\"Fields\":{" + allAreaFieldBlocks + "}";

            }

            //the below statement will make the string like this: 
            /*"Main Construction": {"Image": false,"Order": 
                                                          ["Dwelling","Garage","Outbuilding","Communal Building","Apartment Block"],
                                                          "Fields":{"Dwelling":{.....},"Garage":{.....},"Outbuilding":{.....},"Communal Building":{.....},"Apartment Block":{.....}}            
             *                     }
            */
            completeAreas = "\"" + locationName + "\":{\"Type\":\"Menu\",\"Image\":false, " + areaNames + allAreaFieldBlocks + "}";

            return completeAreas;
        }

        public string getItem(List<PA_ITEM> item, string areaName)
        {

            string completeItems = string.Empty;
            string itemNames = string.Empty;
            string singleItemFieldBlock = string.Empty;
            string allItemFieldBlocks = string.Empty;

            //convert the result into list
            var itemList = item.ToList();

            foreach (var it in itemList)
            {
                //the below statement will produce string like this : "Dwelling","Garage","Outbuilding","Communal Building","Apartment Block",
                itemNames = itemNames + "\"" + it.ItemName + "\",";

                var itemPrameter = (from itp in context.PA_ITEM_PARAMETER
                                    join par in context.PA_PARAMETER on itp.ParameterId equals par.ParameterID 
                                    where itp.ItemId == it.ItemID
                                    orderby par.ParameterSorder
                                    select itp).ToList();

                //Special requirement from client. Gas item will be picked up from P_LGSR table & it will be read only
                if (it.ItemName == "Gas")
                {
                    singleItemFieldBlock = singleItemFieldBlock + this.getGasValues(it.ItemName, it.ItemID);
                }
                else
                {
                    if (itemPrameter.Count() > 0)
                    {
                        //The below statement will produce the statement something like this: "Dwelling":{.....},"Garage":{.....},"Communal Building":{.....},"Apartment Block":{.....},
                        singleItemFieldBlock = singleItemFieldBlock + getItemParameter(itemPrameter, it.ItemName, it.ItemID) + ",";
                    }
                }



            }

            //removing the last comma from string like this: "Dwelling","Garage","Outbuilding","Communal Building","Apartment Block",
            itemNames = itemNames.Substring(0, itemNames.Length - 1);

            //the below statement 'll look like this 
            //"Order": ["Dwelling","Garage","Outbuilding","Communal Building","Apartment Block"],            
            itemNames = "\"Order\":[" + itemNames + "]";


            if (singleItemFieldBlock.Length > 1)
            {
                //the below statement 'll look like this with comma in the last
                //"Order": ["Dwelling","Garage","Outbuilding","Communal Building","Apartment Block"],            
                itemNames = itemNames + ",";

                allItemFieldBlocks = singleItemFieldBlock.Substring(0, singleItemFieldBlock.Length - 1);

                //the below statement will make the string like this: "Fields":{Dwelling":{.....},"Garage":{.....},"Outbuilding":{.....},"Communal Building":{.....},"Apartment Block":{.....}}
                allItemFieldBlocks = "\"Fields\":{" + allItemFieldBlocks + "}";
            }

            //the below statement will make the string like this: 
            /*"Main Construction": {"Image": false,"Order": 
                                                          ["Dwelling","Garage","Outbuilding","Communal Building","Apartment Block"],
                                                          "Fields":{"Dwelling":{.....},"Garage":{.....},"Outbuilding":{.....},"Communal Building":{.....},"Apartment Block":{.....}}            
             *                     }
            */
            completeItems = "\"" + areaName + "\":{\"Type\":\"Menu\",\"Image\":false, " + itemNames + allItemFieldBlocks + "}";

            return completeItems;
        }

        public string getItemParameter(List<PA_ITEM_PARAMETER> itemParameter, string itemName, int itemId)
        {

            string paramNames = string.Empty;
            string completeItemParam = string.Empty;
            string singleItemParamFieldBlock = string.Empty;
            string itemDatesBlock = string.Empty;
            string allItemParamFieldBlocks = string.Empty;

            //convert the result into list
            var itemParamList = itemParameter.ToList();

            foreach (var itp in itemParamList)
            {

                var parameter = (from par in context.PA_PARAMETER
                                 where par.ParameterID == itp.ParameterId
                                 orderby par.ParameterSorder ascending
                                 select par).ToList();

                if (parameter.Count() > 0)
                {
                    //The below statement will produce the statement something like this: "Wall Materials":{.....},"Wall Type":{.....},"Wall Insulation":{.....},"Ground Floor Materials":{.....},
                    singleItemParamFieldBlock = singleItemParamFieldBlock + getParameter(parameter, itp.ItemParamID) + ",";
                    paramNames = paramNames + getParameterNames(parameter);
                }
            }


            if (singleItemParamFieldBlock.Length > 1)
            {

                //get the replacement due date and last done date for this item
                if (this.propertyId != string.Empty)
                {
                    itemDatesBlock = getItemDates(itemId);
                }

                if (itemDatesBlock.Length > 1)
                {
                    singleItemParamFieldBlock = singleItemParamFieldBlock + itemDatesBlock + ",";
                    paramNames = paramNames + "\"" + JsonConstants.LastReplaced + "\", \"" + JsonConstants.ReplacementDue + "\",";
                }

                allItemParamFieldBlocks = singleItemParamFieldBlock.Substring(0, singleItemParamFieldBlock.Length - 1);

                //the below statement will make the string like this: "Fields":{id:1, "Wall Materials":{.....},"Wall Type":{.....},"Wall Insulation":{.....},"Ground Floor Materials":{.....}}
                allItemParamFieldBlocks = "\"Fields\":{" + allItemParamFieldBlocks + "}";
            }

            if (paramNames.Length > 1)
            {
                //remove the comma from the last
                paramNames = paramNames.Substring(0, paramNames.Length - 1);
            }


            //the below statement will make the string something like this: "Order": ["Wall Materials","Wall Type","Wall Insulation","Ground Floor Materials"]
            paramNames = " \"Order\":[" + paramNames + "]";


            //the below statement will make the string like this: 
            /*"Main Construction": {"Fields":{"id":1, "Wall Materials":{.....},"Wall Type":{.....},"Wall Insulation":{.....},"Ground Floor Materials":{.....}},
             *                      "Order": ["Wall Materials","Wall Type","Wall Insulation","Ground Floor Materials"]
             *                     }
            */
            completeItemParam = "\"" + itemName + "\":{\"Type\":\"Form\",\"itemId\":" + itemId + ',' + allItemParamFieldBlocks + "," + paramNames + "}";


            //completeItemParameters = "\"" + itemName + "\":{\"Image\":false}";
            return completeItemParam;
        }

        public string getParameter(List<PA_PARAMETER> parameter, int itemParamId)
        {
            string paramNames = string.Empty;
            string singleParamBlock = string.Empty;
            string singleParamValuesBlock = string.Empty;
            string allPreviouslySelectedValuesBlocks = string.Empty;
            string allParamBlocks = string.Empty;

            //convert the result into list
            var paramList = parameter.ToList();

            foreach (var pl in paramList)
            {


                var paramValue = (from parv in context.PA_PARAMETER_VALUE
                                  where parv.ParameterID == pl.ParameterID
                                  orderby parv.Sorder ascending
                                  select parv).ToList();

                if (parameter.Count() > 0)
                {
                    //The below statement will produce the statement something like this: "Dwelling":{.....},"Garage":{.....},"Communal Building":{.....},"Apartment Block":{.....},
                    singleParamValuesBlock = singleParamValuesBlock + getParameterValue(paramValue);
                }

                if (this.propertyId != string.Empty)
                {
                    allPreviouslySelectedValuesBlocks = this.previouslySelectedValues(pl.ParameterID, itemParamId);
                }

                paramNames = "\"" + pl.ParameterName + "\":{\"itemParamId\":" + itemParamId + ", \"paramId\":" + pl.ParameterID + ", \"Type\":\"" + pl.ControlType + "\"," + allPreviouslySelectedValuesBlocks;

                if (singleParamValuesBlock.Length > 1)
                {

                    //the below statement will make the string like this:  
                    /*"Other Floors": {                                          
                                      "itemParamId":18,
                                      "paramId":6,
                                     "Type": "DropDown",
                                      "Values": [
                                            "Timber",
                                            "Chipboard",
                                            "Block and beam",
                                            "Solid",
                                            "Insulated"
                                       ],
                                       "Selected":{
                                            "id":1,
                                            "isCheckboxSelected": true
                                       },
                    */
                    singleParamBlock = singleParamBlock + paramNames + "," + singleParamValuesBlock + "},";
                }
                else
                {
                    //the below statement will make the string like this: "Condition": {"Type": "YesNo"},
                    singleParamBlock = singleParamBlock + paramNames + "},";
                }

            }


            if (singleParamBlock.Length > 1)
            {

                //the below statement will make the string like this: 
                /*
                    "Other Floors": {
                        "Type": "DropDown",
                        "Values": [
                            "Timber",
                            "Chipboard",
                            "Block and beam",
                            "Solid",
                            "Insulated"
                        ],
                        "Selected":{
                            "id":1,
                            "isCheckboxSelected": true
                        }
                    },
                    "Condition": {
                        "Type": "YesNo"
                    },
                    "Second Floor Material": {
                        "Type": "DropDown",
                        "Values": [
                            "Timber",
                            "Chipboard",
                            "Block and beam",
                            "Solid",
                            "Insulated"
                        ],
                        "Selected":{
                            "id":1,
                            "isCheckboxSelected": true
                        }
                    }
                            
                 */
                allParamBlocks = singleParamBlock.Substring(0, singleParamBlock.Length - 1);

            }

            return allParamBlocks;
        }

        public string getParameterValue(List<PA_PARAMETER_VALUE> paramValue)
        {
            string values = string.Empty;
            //convert the result into list
            var paramValList = paramValue.ToList();
            var completeValuesBlock = string.Empty;

            foreach (var pvl in paramValList)
            {

                //below line will make the string like this: "Timber","Chipboard","Block and beam","Solid","Insulated",
                values = values + "\"" + pvl.ValueDetail + "\":\"" + pvl.ValueID + "\",";
            }

            if (values.Length > 1)
            {
                //remove the comma and string will look like this: "Timber","Chipboard","Block and beam","Solid","Insulated"
                values = values.Substring(0, values.Length - 1);

                //the value of below line will look like this:"Values:["Timber","Chipboard","Block and beam","Solid","Insulated"]"
                completeValuesBlock = "\"Values\":{" + values + "}";
            }


            /*
                "Values":{...}
             */

            return completeValuesBlock;
        }

        public string getParameterNames(List<PA_PARAMETER> parameter)
        {

            string paramNames = string.Empty;
            //convert the result into list
            var paramValList = parameter.ToList();

            foreach (var par in paramValList)
            {

                //below line will make the string like this: "Timber","Chipboard","Block and beam","Solid","Insulated",
                paramNames = "\"" + par.ParameterName + "\"" + ",";
            }

            return paramNames;

        }

        public string previouslySelectedValues(int parameterId, int itemParamId)
        {
            string singleSelectedValueBlock = string.Empty;
            string allSelectedValueBlocks = string.Empty;

            //If parameter is Last Wierd & Cosumer Unit Replacement then previously selected will be saved in pa_property_item_dates
            if (parameterId == JsonConstants.LastRewiredId || parameterId == JsonConstants.ConsumerUnitReplacementId)
            {
                var datesRecord = (from pad in context.PA_PROPERTY_ITEM_DATES
                                   where pad.ParameterId == parameterId
                                   && pad.PROPERTYID == this.propertyId
                                   select new { pad.DueDate, pad.LastDone, pad.SID }).ToList();
                
                if (datesRecord.Count() > 0)
                {
                    var date = datesRecord.First();
                    singleSelectedValueBlock = singleSelectedValueBlock + "\"" + date.LastDone + "\":{\"id\":\"" + date.SID + "\",\"IsCheckBoxSelected\":\"\"},";
                }
            }
            //Added by Behroz - Start
            //To fetch the values of dates from the Item Dates Table
            else if (parameterId == JsonConstants.WcLastReplacedId || parameterId == JsonConstants.WcReplacementDueId ||
                     parameterId == JsonConstants.HandBasinLastReplacedId || parameterId == JsonConstants.HandBasinReplacementDueId ||
                     parameterId == JsonConstants.MainDwellingRoofLastReplacedId  || parameterId == JsonConstants.MainDwellingRoofReplacementDueId)
            {
                var datesRecord = (from pad in context.PA_PROPERTY_ITEM_DATES
                                   where pad.ParameterId == parameterId
                                   && pad.PROPERTYID == this.propertyId
                                   select new { pad.DueDate, pad.LastDone, pad.SID }).ToList();

                if (datesRecord.Count() > 0)
                {
                    var date = datesRecord.First();
                    if (parameterId == JsonConstants.WcLastReplacedId || parameterId == JsonConstants.HandBasinLastReplacedId || parameterId == JsonConstants.MainDwellingRoofLastReplacedId)                    
                        singleSelectedValueBlock = singleSelectedValueBlock + "\"" + date.LastDone + "\":{\"id\":\"" + date.SID + "\",\"IsCheckBoxSelected\":\"\"},";                    
                    else
                        singleSelectedValueBlock = singleSelectedValueBlock + "\"" + date.DueDate + "\":{\"id\":\"" + date.SID + "\",\"IsCheckBoxSelected\":\"\"},";
                }
            }
            //Added by Behroz - End
            else
            {
                var selectedValue = (from par in context.PA_PROPERTY_ATTRIBUTES
                                     where par.ITEMPARAMID == itemParamId
                                     && par.PROPERTYID == this.propertyId
                                     select par).ToList();

                if (selectedValue.Count() > 0)
                {
                    foreach (var item in selectedValue)
                    {
                        //the below statement will make the string like this: 
                        /*
                            "Roof": {
                                "id": 20,
                                "IsCheckBoxSelected":""
                            },
                            "Gabbled Roof": {
                                "id": 30,
                                "IsCheckBoxSelected":""
                            },
                          "No Roof": {
                                "id": 40,
                                "IsCheckBoxSelected":true
                            },
                            
                         */


                        singleSelectedValueBlock = singleSelectedValueBlock + "\"" + item.PARAMETERVALUE + "\":{\"id\":\"" + item.VALUEID + "\",\"IsCheckBoxSelected\":\"" + item.IsCheckBoxSelected.ToString() + "\"},";
                    }
                    //end for each selectedValueCount
                }
                //end if: selectedValueCount.count >0
            }//end else

            if (singleSelectedValueBlock.Length > 1)
            {
                allSelectedValueBlocks = singleSelectedValueBlock.Substring(0, singleSelectedValueBlock.Length - 1);
            }

            //the below statement will make the string like this: 
            /*
                "Selected:{"             
                    "Roof": {
                        "id": 20,
                        "IsCheckBoxSelected":""
                    },
                    "Gabbled Roof": {
                        "id": 30,
                        "IsCheckBoxSelected":""
                    },
                  "No Roof": {
                        "id": 40,
                        "IsCheckBoxSelected":true
                    }
               }
                            
             */
            allSelectedValueBlocks = "\"Selected\":{" + allSelectedValueBlocks + "}";



            return allSelectedValueBlocks;
        }

        public string getGasValues(string itemName, int itemId)
        {
            string issueDateBlock = string.Empty;
            string cp12NumberBlock = string.Empty;
            string expiryDateBlock = string.Empty;
            string cp12SelectedBlock = string.Empty;
            string issueDateSelectedBlock = string.Empty;
            string expiryDateSelectedBlock = string.Empty;

            string singleItemSelectedValues = string.Empty;
            string singleItemFieldBlock = string.Empty;
            string singleItemValues = string.Empty;
            string paramNames = string.Empty;
            string fieldBlock = string.Empty;
            string gasBlock = string.Empty;


            //Get values of gas from p_lgsr
            var gasQuery = (from plg in context.P_LGSR
                            where plg.PROPERTYID == this.propertyId
                            select plg);

            var gasValuesList = gasQuery.ToList();

            //if p_lgsr has values then make string out of it
            if (gasValuesList.Count() > 0)
            {
                var gasValues = gasQuery.First();

                cp12SelectedBlock = "\"Selected\":{\"" + ( gasValues.ISSUEDATE != null ? gasValues.ISSUEDATE.ToString().Substring(0, 10) : "" )   + "\":{\"id\":0, \"IsCheckBoxSelected\": \"\"}}";
                issueDateSelectedBlock = "\"Selected\":{\"" + gasValues.CP12NUMBER + "\":{\"id\":0, \"IsCheckBoxSelected\": \"\"}}";
                expiryDateSelectedBlock = "\"Selected\":{\"" + ( gasValues.RECEVIEDDATE != null ? gasValues.RECEVIEDDATE.ToString().Substring(0, 10) : "" ) + "\":{\"id\":0, \"IsCheckBoxSelected\": \"\"}}";

                issueDateBlock = "\"IssueDate\":{\"itemParamId\":0, \"paramId\":0, \"Type\":\"Textbox\", \"ReadOnly\": true, \"Values\":{\"" + (gasValues.ISSUEDATE != null ? gasValues.ISSUEDATE.ToString().Substring(0, 10) : "") + "\":0}," + cp12SelectedBlock + "}";

                cp12NumberBlock = "\"CP12Number\":{\"itemParamId\":0, \"paramId\":0, \"Type\":\"Textbox\", \"ReadOnly\": true, \"Values\":{\"" + gasValues.CP12NUMBER + "\":0}," + issueDateSelectedBlock + "}";

                expiryDateBlock = "\"ExpiryDate\":{\"itemParamId\":0, \"paramId\":0, \"Type\":\"Textbox\", \"ReadOnly\": true, \"Values\":{\"" + (gasValues.RECEVIEDDATE != null ? gasValues.RECEVIEDDATE.ToString().Substring(0, 10) : "") + "\":0}," + expiryDateSelectedBlock + "}";

                fieldBlock = issueDateBlock + "," + cp12NumberBlock + "," + expiryDateBlock + ",";
                paramNames = "\"IssueDate\", \"CP12Number\", \"ExpiryDate\",";
            }


            var itemList = (from it in context.PA_ITEM
                            join itp in context.PA_ITEM_PARAMETER on it.ItemID equals itp.ItemId
                            join par in context.PA_PARAMETER on itp.ParameterId equals par.ParameterID
                            where it.ItemID == itemId
                            orderby par.ParameterSorder ascending
                            select new { it, itp, par }).ToList();

            foreach (var it in itemList)
            {
                if (this.propertyId != string.Empty)
                {
                    var paramValue = (from parv in context.PA_PARAMETER_VALUE
                                      where parv.ParameterID == it.par.ParameterID
                                      orderby parv.Sorder ascending
                                      select parv).ToList();

                    paramNames = paramNames + "\"" + it.par.ParameterName + "\"" + ",";
                    singleItemValues = getParameterValue(paramValue);
                    singleItemSelectedValues = previouslySelectedValues(it.par.ParameterID, it.itp.ItemParamID);

                    singleItemFieldBlock = singleItemFieldBlock + "\"" + it.par.ParameterName + "\":{\"itemParamId\":" + it.itp.ItemParamID + ", \"paramId\":" + it.itp.ItemParamID + ", \"Type\":\"" + it.par.ControlType + "\", \"ReadOnly\": false, " + singleItemValues + "," + singleItemSelectedValues + "},";
                }
            }


            if (singleItemFieldBlock.Length > 0)
            {
                singleItemFieldBlock = singleItemFieldBlock.Substring(0, singleItemFieldBlock.Length - 1);
                fieldBlock = fieldBlock + singleItemFieldBlock + ",";
            }

            if (paramNames.Length > 0)
            {
                paramNames = paramNames.Substring(0, paramNames.Length - 1);
            }

            if (fieldBlock.Length > 0)
            {
                fieldBlock = fieldBlock.Substring(0, fieldBlock.Length - 1);
            }


            gasBlock = "\"" + itemName + "\":{\"Type\": \"Form\", \"itemId\":" + itemId + ", \"Fields\": {" + fieldBlock + "}, \"Order\":[" + paramNames + "]},";
            return gasBlock;
        }

        public string getItemDates(int itemId)
        {
            string lastReplacedBlock = string.Empty;
            string replacementDueBlock = string.Empty;
            string completeBlock = string.Empty;

            //Changed by Behroz
            //if (itemId != JsonConstants.ElectricsId)
            if (itemId != JsonConstants.ElectricsId && itemId != JsonConstants.SeparateId)
            {
                var datesRecord = (from pad in context.PA_PROPERTY_ITEM_DATES
                                   where pad.ItemId == itemId
                                   && pad.PROPERTYID == this.propertyId
                                   select new { pad.DueDate, pad.LastDone, pad.SID }).ToList();

                if (datesRecord.Count > 0)
                {
                    /* The below statemenet will produce the string like this
                       "Last Replaced": {
                            "Type": "Date",
                            "Values": [
                                "Timber",
                                "Chipboard",
                                "Block and beam",
                                "Solid",
                                "Insulated"
                            ],
                            "Selected":{
                               "20/9/2012"{
                                    "id":1,
                                    "isCheckboxSelected": true
                               } 
                            }
                        },
                        "Replacement Due": {
                            "Type": "Date",
                            "Values": [
                                "Timber",
                                "Chipboard",
                                "Block and beam",
                                "Solid",
                                "Insulated"
                            ],
                            "Selected":{
                               "20/9/2012"{
                                    "id":1,
                                    "isCheckboxSelected": true
                               } 
                            }
                        },
                     */
                    var dates = datesRecord.First();
                    string lastReplaced = dates.LastDone.ToString();
                    string replacementDue = dates.DueDate.ToString();
                    string sid = dates.SID.ToString();

                    lastReplacedBlock = "\"" + JsonConstants.LastReplaced + "\":{\"Type\":\"Date\", \"Values\":[\"" + lastReplaced + "\"], \"Selected\":{\"" + lastReplaced + "\":{\"id\":" + sid + ", \"IsCheckBoxSelected\":\"\"}}}";
                    replacementDueBlock = "\"" + JsonConstants.ReplacementDue + "\":{\"Type\":\"Date\", \"Values\":[\"" + replacementDue + "\"], \"Selected\":{\"" + replacementDue + "\":{\"id\":" + sid + ", \"IsCheckBoxSelected\":\"\"}}}";
                    completeBlock = lastReplacedBlock + "," + replacementDueBlock;
                }
                //Added by Behroz - Start
                else if (itemId == JsonConstants.StructureId || itemId == JsonConstants.CloakroomId || itemId == JsonConstants.FasciasId || itemId == JsonConstants.SurfaceId)
                {
                    string lastReplaced = string.Empty;
                    string replacementDue = string.Empty;
                    string sid = string.Empty;

                    lastReplacedBlock = "\"" + JsonConstants.LastReplaced + "\":{\"Type\":\"Date\", \"Values\":[\"" + lastReplaced + "\"], \"Selected\":{\"" + lastReplaced + "\":{\"id\":\"\", \"IsCheckBoxSelected\":\"\"}}}";
                    replacementDueBlock = "\"" + JsonConstants.ReplacementDue + "\":{\"Type\":\"Date\", \"Values\":[\"" + replacementDue + "\"], \"Selected\":{\"" + replacementDue + "\":{\"id\":\"\", \"IsCheckBoxSelected\":\"\"}}}";
                    completeBlock = lastReplacedBlock + "," + replacementDueBlock;
                }
                //Added by Behroz - End
            }
            

            return completeBlock;
        }
    }


}
