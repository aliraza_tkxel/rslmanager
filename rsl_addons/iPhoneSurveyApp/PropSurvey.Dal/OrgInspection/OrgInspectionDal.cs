﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Transactions;
using PropSurvey.Contracts.Data;
using PropSurvey.Dal.Base;
using PropSurvey.Entities;

namespace PropSurvey.Dal.OrgInspection
{
    public class OrgInspectionDal : BaseDal
    {

        #region get Org list
        /// <summary>
        /// This function returns the Org list
        /// </summary>
        /// <returns>List of Org data objects</returns>

        public List<OrgData> getAllOrg(string userName)
        {
            //int userOrgId = (from user in context.AC_LOGINS
            //                 join emp in context.E__EMPLOYEE on user.EMPLOYEEID equals emp.EMPLOYEEID
            //                 join org in context.S_ORGANISATION on emp.ORGID equals org.ORGID
            //                 where user.LOGIN == userName
            //                 select org.ORGID
            //                     ).FirstOrDefault<int>();

            //var org_var = (from org in context.S_ORGANISATION
            //               where org.ORGACTIVE == 1 && org.ORGID == userOrgId
            //               select new OrgData
            //               {
            //                   ORGID = org.ORGID,
            //                   ADDRESS1 = org.ADDRESS1,
            //                   POSTCODE = org.POSTCODE,
            //                   TELEPHONE1 = org.TELEPHONE1,
            //                   COUNTY = org.COUNTY,
            //                   TOWNCITY = org.TOWNCITY,
            //                   VATREGNUMBER = org.VATREGNUMBER,
            //                   SHOWDOCSONWHITEBOARD = org.SHOWDOCSONWHITEBOARD,
            //                   BUILDINGNAME = org.BUILDINGNAME,
            //                   ORGACTIVE = org.ORGACTIVE,
            //                   COMPANYTYPE = org.COMPANYTYPE,
            //                   CISEXPIRYDATE = org.CISEXPIRYDATE,
            //                   CISCERTIFICATENUMBER = org.CISCERTIFICATENUMBER,
            //                   ORGNAME = org.NAME,
            //                   PAYMENTTERMS = org.PAYMENTTERMS,
            //                   PAYMENTTYPE = org.PAYMENTTYPE

            //               });

            List<OrgData> orgList = new List<OrgData>();
            //if (org_var.Count() > 0)
            //{
            //    orgList = org_var.ToList();
            //}

            return orgList;
        }

        #endregion

        #region get PaymentType, Payment Terms list
        /// <summary>
        /// This function returns the PaymentType, Payment Terms List
        /// </summary>
        /// <returns>List of PaymentType, Payment Terms data objects</returns>

        public List<PaymentTypeData> getPaymentType()
        {
            var org_var = (from pay in context.F_PAYMENTTYPE
                           select new PaymentTypeData
                           {
                               ID = pay.PAYMENTTYPEID,
                               PaymentType = pay.DESCRIPTION

                           });

            List<PaymentTypeData> payList = new List<PaymentTypeData>();
            if (org_var.Count() > 0)
            {
                payList = org_var.ToList();
            }

            return payList;
        }

        #endregion

        #region get company Type list
        /// <summary>
        /// This function returns the company type List
        /// </summary>
        /// <returns>List of company type data objects</returns>

        public List<CompanyTypeData> getCompanyType()
        {
            var org_var = (from pay in context.S_COMPANYTYPE
                           select new CompanyTypeData
                           {
                               ID = pay.TYPEID,
                               CompanyType = pay.DESCRIPTION

                           });

            List<CompanyTypeData> companyList = new List<CompanyTypeData>();
            if (org_var.Count() > 0)
            {
                companyList = org_var.ToList();
            }

            return companyList;
        }

        #endregion

        #region get Gas Engineer list
        /// <summary>
        /// This function returns all the Gas Engineers 
        /// </summary>
        /// <returns>List of Emp data objects</returns>

        public List<EmployeData> getAllGasEngineer(string userName)
        {
            var emp_data = (from emp in context.E__EMPLOYEE
                            join log in context.AC_LOGINS on emp.EMPLOYEEID equals log.EMPLOYEEID
                            join job in context.E_JOBDETAILS on emp.EMPLOYEEID equals job.EMPLOYEEID
                            where log.LOGIN == userName
                            select new EmployeData
                            {
                                EmpID = emp.EMPLOYEEID,
                                EmpName = emp.FIRSTNAME +  " " + emp.LASTNAME,
                                GSRENo = job.GSRENo
                            });

            List<EmployeData> empList = new List<EmployeData>();
            if (emp_data.Count() > 0)
            {
                empList = emp_data.ToList();
            }

            return empList;
        }

        #endregion

        #region get Org Inspection Form

        /// <summary>
        /// This function returns Org Inspected form
        /// </summary>
        /// <returns>List of org inspection data objects</returns>

        public OrgInspectionData getOrgInspection(int appointmentId)
        {
            var orgIns = (from ins in context.GS_OrgInspection
                          //join org in context.S_ORGANISATION on ins.OrgID equals org.ORGID
                          join emp in context.E__EMPLOYEE on ins.GasEngineerID equals emp.EMPLOYEEID
                          where ins.AppointmentID == appointmentId
                          select new OrgInspectionData
                          {
                              ID = ins.ID,
                              AppointmentID = ins.AppointmentID,
                              //City = ins.City,
                              GasEngineerID = ins.GasEngineerID,
                              //Address = ins.Address,
                              GSRENo = ins.GSRENo,
                              InspectedDate = ins.InspectedDate,
                              //OrgID = ins.OrgID,
                              //PostCode = ins.PostCode,
                              //Telephone = ins.Telephone,
                              //RegNo = ins.RegNo,
                              GasEngineerName = emp.FIRSTNAME + " " + emp.LASTNAME
                              //OrgName = org.NAME
                          });

            OrgInspectionData OrgInspectionList = new OrgInspectionData();
            if (orgIns.Count() > 0)
            {
                OrgInspectionList = orgIns.FirstOrDefault();
            }

            return OrgInspectionList;
        }

        #endregion

        #region save Org Inspection
        /// <summary>
        /// This function saves the Org Inspection in the database
        /// </summary>
        /// <param name="appData">The object of Org Inspection</param>
        /// <returns>the Appliance id if successfully save otherwise 0 </returns>

        public int saveOrgInspection(OrgInspectionData orgData)
        {
            bool success = false;

            GS_OrgInspection orgIns = new GS_OrgInspection();
            var temp = context.GS_OrgInspection.Where(app => app.AppointmentID == orgData.AppointmentID);
            using (TransactionScope trans = new TransactionScope())
            {
                if (temp.Count() > 0)
                {
                    orgIns = temp.First();
                    //orgIns.OrgID = orgData.OrgID;
                    orgIns.InspectedDate = orgData.InspectedDate;
                    //orgIns.PostCode = orgData.PostCode;
                    //orgIns.Telephone = orgData.Telephone;
                    //orgIns.City = orgData.City;
                    //orgIns.RegNo = orgData.RegNo;
                    orgIns.GSRENo = orgData.GSRENo;
                    orgIns.GasEngineerID = orgData.GasEngineerID;
                    orgIns.AppointmentID = orgData.AppointmentID;

                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    trans.Complete();
                    success = true;
                }
                else
                {
                    //orgIns.Address = orgData.Address;
                    orgIns.AppointmentID = orgData.AppointmentID;
                    //orgIns.City = orgData.City;
                    orgIns.GasEngineerID = orgData.GasEngineerID;
                    orgIns.GSRENo = orgData.GSRENo;
                    orgIns.InspectedDate = orgData.InspectedDate;
                    //orgIns.OrgID = orgData.OrgID;
                    //orgIns.PostCode = orgData.PostCode;
                    //orgIns.RegNo = orgData.RegNo;
                    //orgIns.Telephone = orgData.Telephone;

                    context.AddToGS_OrgInspection(orgIns);
                    context.SaveChanges();
                    trans.Complete();
                    success = true;
                }
            }

            if (success == true)
            {
                return orgIns.ID;
            }
            else
            {
                return 0;
            }
        }

        #endregion

        #region update Org Inspection
        /// <summary>
        /// This function updates the Org Inspection in the database
        /// </summary>
        /// <param name="appData">The object of Org Inspection</param>
        /// <returns>true if successfully save otherwise 0 </returns>

        public bool updateOrgInspection(OrgInspectionData orgData)
        {
            bool success = false;
            using (TransactionScope trans = new TransactionScope())
            {
                GS_OrgInspection orgIns = (from ins in context.GS_OrgInspection
                                           where ins.ID == orgData.ID
                                           select ins).FirstOrDefault();
                //orgIns.OrgID = orgData.OrgID;
                orgIns.InspectedDate = orgData.InspectedDate;
                //orgIns.PostCode = orgData.PostCode;
                //orgIns.Telephone = orgData.Telephone;
                //orgIns.City = orgData.City;
                //orgIns.RegNo = orgData.RegNo;
                orgIns.GSRENo = orgData.GSRENo;
                orgIns.GasEngineerID = orgData.GasEngineerID;
                orgIns.AppointmentID = orgData.AppointmentID;

                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                trans.Complete();
                success = true;
            }

            return success;
        }

        #endregion

        #region Save a new Org
        /// <summary>
        /// This function saves a new Org
        /// </summary>
        /// <param name="appData">Org data objects</param>
        /// <returns>ID in case of success</returns>

        public int saveOrg(OrgData orgData)
        {
            //bool success = false;
            //S_ORGANISATION org = new S_ORGANISATION();

            //using (TransactionScope trans = new TransactionScope())
            //{
            //    org.ORGID = orgData.ORGID;
            //    org.ADDRESS1 = orgData.ADDRESS1;
            //    org.POSTCODE = orgData.POSTCODE;
            //    org.TELEPHONE1 = orgData.TELEPHONE1;
            //    org.COUNTY = orgData.COUNTY;
            //    org.TOWNCITY = orgData.TOWNCITY;
            //    org.VATREGNUMBER = orgData.VATREGNUMBER;
            //    org.SHOWDOCSONWHITEBOARD = orgData.SHOWDOCSONWHITEBOARD;
            //    org.BUILDINGNAME = orgData.BUILDINGNAME;
            //    org.ORGACTIVE = orgData.ORGACTIVE;
            //    org.COMPANYTYPE = orgData.COMPANYTYPE;
            //    org.CISEXPIRYDATE = orgData.CISEXPIRYDATE;
            //    org.CISCERTIFICATENUMBER = orgData.CISCERTIFICATENUMBER;
            //    org.NAME = orgData.ORGNAME;
            //    org.PAYMENTTERMS = orgData.PAYMENTTERMS;
            //    org.PAYMENTTYPE = orgData.PAYMENTTYPE;

            //    context.AddToS_ORGANISATION(org);
            //    context.SaveChanges();
            //    trans.Complete();
            //    success = true;
            //}
            //if (success)
            //{
            //    return org.ORGID;
            //}
            //else
            //{
            //    return 0;
            //}
            return 0;
        }
        #endregion


        #region Check for valid input

        public bool checkValidOrgID(int OrgID)
        {
            //var ID = (from org in context.S_ORGANISATION
            //          where org.ORGID == OrgID
            //          select org.ORGID
            //            );

            //if (ID.Count() > 0)
            //{
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}
            return false;
        }

        public bool checkValidEmpID(int empID)
        {
            var ID = context.E__EMPLOYEE.Where(emp => emp.EMPLOYEEID == empID);

            if (ID.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool checkValidAppointmentID(int appID)
        {
            var ID = context.PS_Appointment.Where(app => app.AppointId == appID);

            if (ID.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool checkValidCompanyType(int CompanyType)
        {
            var ID = context.S_COMPANYTYPE.Where(comType => comType.TYPEID == CompanyType);

            if (ID.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool checkValidPayTerms(int TermID)
        {
            var ID = context.F_PAYMENTTYPE.Where(pay => pay.PAYMENTTYPEID == TermID);

            if (ID.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool checkValidPaymentType(int PaymentTypeID)
        {
            var ID = context.F_PAYMENTTYPE.Where(pay => pay.PAYMENTTYPEID == PaymentTypeID);

            if (ID.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion
    }
}
