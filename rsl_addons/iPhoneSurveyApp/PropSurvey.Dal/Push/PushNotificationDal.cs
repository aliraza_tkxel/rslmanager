﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PropSurvey.Dal.Base;
using APNS;
using System.IO;
using System.Web;
using PropSurvey.Utilities.Helpers;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using PropSurvey.Utilities.Constants;

namespace PropSurvey.Dal.Push
{
    public class PushNotificationDal : BaseDal
    {

        #region send Push Notification for Fault Appointment
        /// <summary>
        /// This function will send push notification for fault appointment
        /// </summary>
        /// <returns></returns>
        public bool sendPushNotificationFault(int appointmentId, int type, string serverinUse)
        {
            if (type < 1 || type > 3)
            {
                return false;
            }

            string[] typeOptions = { "New JSN Scheduled", "JSN Cancelled", "JSN Rearranged" };

            var appDataList = (from appointment in context.FL_CO_APPOINTMENT
                               join login in context.AC_LOGINS on appointment.OperativeID equals login.EMPLOYEEID
                               where appointment.AppointmentID == appointmentId
                               select new { appointment.AppointmentDate, appointment.Time, appointment.EndTime, login.DEVICETOKEN });

            if (appDataList.Count() > 0)
            {
                var appData = appDataList.First();

                string deviceToken = appData.DEVICETOKEN;
                //string deviceToken = "094f4c06d2ed40bcd6cd2b41b742b1312c6b4e7677aab045dc450bdb892f0f14";


                if (deviceToken != null)
                {
                    if (deviceToken == "0" || deviceToken == "")
                    {
                        string msg = FileHelper.messageForLogger(MessageConstants.PushNotificationDeviceTokenIsEmpty);
                        Logger.Write(msg);
                        return false;
                    }
                    var propDataList = (from property in context.P__PROPERTY
                                        join fltl in context.FL_FAULT_LOG on property.PROPERTYID equals fltl.PROPERTYID
                                        join flta in context.FL_FAULT_APPOINTMENT on fltl.FaultLogID equals flta.FaultLogId
                                        where flta.AppointmentId == appointmentId &&
                                        flta.FaultLogId == context.FL_FAULT_LOG.Where(ffl => ffl.FaultLogID == flta.FaultLogId).FirstOrDefault().FaultLogID
                                        select new { property.HOUSENUMBER, property.ADDRESS1, property.TOWNCITY });

                    if (propDataList.Count() > 0)
                    {
                        var propData = propDataList.First();

                        DateTime appDate = DateTime.Parse(appData.AppointmentDate.ToString());
                        string appointmentDate = appDate.ToString("d MMM yyyy");

                        DateTime sTime = DateTime.Parse(appData.Time);
                        string startTime = sTime.ToString("HH:mm");

                        DateTime eTime = DateTime.Parse(appData.EndTime);
                        string endTime = eTime.ToString("HH:mm");

                        string propertyString = propData.HOUSENUMBER.ToString() + " " + propData.ADDRESS1.ToString() + ", " + propData.TOWNCITY.ToString();

                        string PNString = typeOptions[type - 1] + "\n\n" + appointmentDate + " " + startTime + " to " + endTime + "\n" + propertyString;

                        string serverPath = HttpContext.Current.Server.MapPath("");

                        serverPath = serverPath.Remove(serverPath.LastIndexOf('\\') + 1);
                        
                        //This is the dev certificate file name with path.
                        string filePath = serverPath + "APNS_DEV_BHGInspectorCertOnly.p12";
                        
                        //In case the server in use is Test or Live change the file name accordingly
                        if (serverinUse.Equals(ApplicationConstants.BHG_Test_Name))
                            filePath = serverPath + "APNS_Test_BHGInspector_CertOnly.p12";
                        else if (serverinUse.Equals(ApplicationConstants.BHG_Live_Name))
                            filePath = serverPath + "APNS_Live_BHGInspector_CertOnly.p12";
                        
                        var push = new PushNotification(true, filePath, "tkxel");

                        var payload1 = new NotificationPayload(deviceToken, PNString);

                        var p = new List<NotificationPayload> { payload1 };

                        var rejected = push.SendToApple(p);
                        
                        string notificationInfo = String.Format("device token:{0}, notification message: {1}, certificate path:{2} ", deviceToken, PNString, filePath);

                        if (rejected.Count == 0)
                        {
                            string msg = FileHelper.messageForLogger(MessageConstants.PushNotificationSuccess + notificationInfo);
                            Logger.Write(msg);
                            return true;
                        }
                        else
                        {
                            string msg = FileHelper.messageForLogger(MessageConstants.PushNotificationRejectByApple + notificationInfo);
                            Logger.Write(msg);
                            return false;
                        }                        
                    }
                    else
                    {
                        string msg = FileHelper.messageForLogger(MessageConstants.PushNotificationAppointmentIdDoesNotExistInDb + string.Format("appointment id :{0}",appointmentId));
                        Logger.Write(msg);
                        return false;
                    }
                }
                else
                {
                    string msg = FileHelper.messageForLogger(MessageConstants.PushNotificationDeviceTokenIsEmpty);
                    Logger.Write(msg);
                    return false;
                }
            }
            else
            {
                string msg = FileHelper.messageForLogger(MessageConstants.PushNotificationAppointmentIdDoesNotExistInDb + string.Format("appointment id :{0}", appointmentId));
                Logger.Write(msg);
                return false;
            }
        }
        #endregion

    }
}
