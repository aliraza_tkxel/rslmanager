﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PropSurvey.Dal.Base;
using PropSurvey.Contracts.Data;
using System.Transactions;
using PropSurvey.Entities;
using System.Data.Objects;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using PropSurvey.Utilities.Constants;
using System.Web.Script.Serialization;
using PropSurvey.Utilities.Helpers;

namespace PropSurvey.Dal.Survey
{
    public class SurveyDal : BaseDal
    {
        #region save Survey Form

        /// <summary>
        /// This function save the survey form. Data should be post at specific url. 
        /// </summary>
        /// <param name="survData">survey data object</param>
        /// <returns>it returns true or false</returns>

        public bool saveSurveyForm(SurveyData survData)
        {

            PS_Survey survey = new PS_Survey();
            bool success = false;


            using (TransactionScope trans = new TransactionScope())
            {

                //save the survey
                survey = this.savePsSurvey(survey, survData);

                if (survData.surveyNotes.surveyNotesDetail != string.Empty)
                {
                    //save the survey in ps_survey_item_notes
                    this.savePsSurveyItemNotes(survey, survData);

                    //save survey notes in pa_property_item_notes
                    this.savePaSurveyItemNotes(survData);
                }

                //save the survey fields
                this.saveSurveyFields(survey, survData);

                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                trans.Complete();
                success = true;
            }

            return success;
        }
        #endregion

        #region save Ps Survey
        /// <summary>
        /// This function will save the survey in ps_survey and ps_appointment2survey
        /// </summary>
        /// <param name="survey"></param>
        /// <param name="survData"></param>
        /// <returns></returns>

        private PS_Survey savePsSurvey(PS_Survey survey, SurveyData survData)
        {
            PS_Appointment2Survey savedSurvey = new PS_Appointment2Survey();

            //this will contain the existing /already saved survey id
            int existingSurveyId = 0;

            //Find the survey aginst give appointment id. 
            var appoint2survey = context.PS_Appointment2Survey.Where(app => app.AppointId == survData.appointmentId);

            //If survey already saved against the appointment id then get the existing survey id
            if (appoint2survey.Count() > 0)
            {
                //get the existing survey id
                savedSurvey = appoint2survey.First();
                existingSurveyId = savedSurvey.SurveyId;
            }

            if (existingSurveyId != 0)
            {
                survey.SurveyId = existingSurveyId;
            }


            survey.PROPERTYID = survData.propertyId;
            survey.CompletedBy = survData.completedBy;
            survey.SurveyDate = DateTime.Now;

            if (existingSurveyId == 0)
            {
                context.AddToPS_Survey(survey);
            }

            //save the data in ps_survey table
            context.SaveChanges();

            //if survey does not exist then entry will go to appointment2survey
            if (existingSurveyId == 0)
            {
                PS_Appointment2Survey appSurv = new PS_Appointment2Survey();
                appSurv.AppointId = survData.appointmentId;
                appSurv.SurveyId = survey.SurveyId;

                context.AddToPS_Appointment2Survey(appSurv);
                context.SaveChanges();
            }

            return survey;
        }

        #endregion

        #region Save Ps Survey Item Notes
        /// <summary>
        /// This function saves the notes of survey. Everytime new record will be inserted in PS_Survey_Item_Notes against ItemId 
        /// If item id already exist then previous record will be updated
        /// </summary>
        /// <param name="survey"></param>
        /// <param name="survData"></param>
        private void savePsSurveyItemNotes(PS_Survey survey, SurveyData survData)
        {
            var psNotes = context.PS_Survey_Item_Notes.Where(app => app.SurveyId == survey.SurveyId && app.ItemId == survData.itemId);

            PS_Survey_Item_Notes surveyNotes = new PS_Survey_Item_Notes();

            if (psNotes.Count() > 0)
            {
                surveyNotes = psNotes.First();
            }
            else
            {
                surveyNotes.ItemId = survData.itemId;
                surveyNotes.SurveyId = survey.SurveyId;
            }

            //save the notes in PS_Survey_Item_Notes            
            surveyNotes.Notes = survData.surveyNotes.surveyNotesDetail;
            surveyNotes.NotesDate = survData.surveyNotes.surveyNotesDate;


            if (psNotes.Count() <= 0)
            {
                context.AddToPS_Survey_Item_Notes(surveyNotes);
            }
            //save the changes
            context.SaveChanges();

        }
        #endregion

        #region save Pa Survey Item Notes
        /// <summary>
        /// This function will save the survey notes in pa_property_item_notes
        /// </summary>
        /// <param name="survData"></param>
        private void savePaSurveyItemNotes(SurveyData survData)
        {
            PA_PROPERTY_ITEM_NOTES paItemNotes = new PA_PROPERTY_ITEM_NOTES();

            //save the notes in PA_property_item_notes
            //search if previous notes already exist
            var paNotes = context.PA_PROPERTY_ITEM_NOTES.Where(prop => prop.ItemId == survData.itemId && prop.PROPERTYID == survData.propertyId);

            if (paNotes.Count() > 0)
            {
                //fetch the already existed notes
                paItemNotes = paNotes.First();
            }
            else
            {
                //if record does not already exist the set the property id
                paItemNotes.PROPERTYID = survData.propertyId;

                //set the item id
                paItemNotes.ItemId = survData.itemId;
            }


            paItemNotes.Notes = survData.surveyNotes.surveyNotesDetail;
            paItemNotes.CreatedOn = survData.surveyNotes.surveyNotesDate;
            paItemNotes.CreatedBy = survData.completedBy;


            //if previous notes does not already exist
            if (paNotes.Count() <= 0)
            {
                //insert the record
                context.AddToPA_PROPERTY_ITEM_NOTES(paItemNotes);
            }

            context.SaveChanges();
        }

        #endregion

        #region save Survey Fields
        /// <summary>
        /// This function saves the fields of survey.Every record will be inserted in ps_survey_parameters 
        /// and the record will be updated in pa_property attributes      
        /// </summary>
        /// <param name="survey"></param>
        /// <param name="survData"></param>
        private void saveSurveyFields(PS_Survey survey, SurveyData survData)
        {
            //save the survey fields value                        
            foreach (var item in survData.surveyFields)
            {
                //if there is something in param item field
                if (item.surveyParamItemField.Count() > 0)
                {
                    foreach (var field in item.surveyParamItemField)
                    {
                        //If user did not select the field value and left it empty then fieldId will be zero
                        //don't save it
                        if (item.controlType == "Textbox" || item.controlType == "CheckBoxes" || (item.controlType == "Dropdown" && field.surveyPramItemFieldId > 0))
                        {

                            PS_Survey_Parameters surveyFields = new PS_Survey_Parameters();
                            PA_PROPERTY_ATTRIBUTES paAttribItem = new PA_PROPERTY_ATTRIBUTES();

                            IQueryable<PS_Survey_Parameters> oldSurveyFields;

                            //if field is checkbox then surveyParamItemField will have multiple values and 
                            //we will add one more and condition to match valueid 
                            if (item.surveyParamItemField.Count() > 1)
                            {
                                oldSurveyFields = context.PS_Survey_Parameters.Where(app => app.SurveyId == survey.SurveyId && app.ItemParamId == item.surveyItemParamId && app.ValueId == field.surveyPramItemFieldId);
                            }
                            else
                            {
                                //Find same survey which was saved previously i.e text box and drop down
                                oldSurveyFields = context.PS_Survey_Parameters.Where(app => app.SurveyId == survey.SurveyId && app.ItemParamId == item.surveyItemParamId);
                            }

                            if (oldSurveyFields.Count() > 0)
                            {
                                surveyFields = oldSurveyFields.First();
                            }
                            else
                            {
                                surveyFields.SurveyId = survey.SurveyId;
                                surveyFields.ItemParamId = item.surveyItemParamId;
                            }

                            //Logger.Write(survData);
                            //if control type is not text box then this will not be selected
                            if (item.controlType != "Textbox")
                            {
                                surveyFields.ValueId = field.surveyPramItemFieldId;
                            }

                            //if the field has checkbox type then the isSeleccted will be Yes or No else it will be empty
                            if (field.isSelected != string.Empty)
                            {
                                surveyFields.IsCheckBoxSelected = field.isSelected == "Yes" ? true : false;
                            }

                            surveyFields.PARAMETERVALUE = field.surveyParamItemFieldValue;

                            if (oldSurveyFields.Count() <= 0)
                            {
                                context.AddToPS_Survey_Parameters(surveyFields);
                            }

                            context.SaveChanges();
                            context.Detach(surveyFields);


                            //search if field value already exist in PA_PROPERTY_ATTRIBUTES
                            var paAttrib = context.PA_PROPERTY_ATTRIBUTES.Where(prop => prop.PROPERTYID == survData.propertyId && prop.ITEMPARAMID == item.surveyItemParamId);

                            //if field is checkbox then surveyParamItemField will have multiple values and 
                            //we will add one more and condition to match valueid, because we 'll have to update all checkbox values
                            if (item.surveyParamItemField.Count() > 1)
                            {
                                paAttrib = context.PA_PROPERTY_ATTRIBUTES.Where(prop => prop.PROPERTYID == survData.propertyId && prop.ITEMPARAMID == item.surveyItemParamId && prop.VALUEID == field.surveyPramItemFieldId);
                            }

                            if (paAttrib.Count() > 0)
                            {
                                //fetch the already existed record
                                paAttribItem = paAttrib.First();
                            }
                            else
                            {
                                //if record does not already exist the set the property id and itemparamid
                                paAttribItem.PROPERTYID = survey.PROPERTYID;
                                paAttribItem.ITEMPARAMID = item.surveyItemParamId;
                            }

                            //set the value id and value                          
                            //if control type is not text box then this will not be selected
                            if (item.controlType != "Textbox")
                            {
                                paAttribItem.VALUEID = field.surveyPramItemFieldId;
                            }
                            paAttribItem.PARAMETERVALUE = field.surveyParamItemFieldValue;
                            paAttribItem.UPDATEDBY = survey.CompletedBy;
                            paAttribItem.UPDATEDON = survey.SurveyDate;
                            //if the field has checkbox type then the isSeleccted will be Yes or No else it will be empty
                            if (field.isSelected != string.Empty)
                            {
                                paAttribItem.IsCheckBoxSelected = field.isSelected == "Yes" ? true : false;
                            }


                            //if previous record does not already exist
                            if (paAttrib.Count() <= 0)
                            {
                                //insert the record
                                context.AddToPA_PROPERTY_ATTRIBUTES(paAttribItem);
                            }

                            context.SaveChanges();
                            context.Detach(paAttribItem);

                        }
                        //end condition:If FieldValueId is empty/zero && item.ControlType != date
                        else if (item.controlType == "Date")
                        {
                            //Added by Behroz - Start
                            //this.saveSurveyReplacementDates(survey, survData, item, field);
                            //Changed to Contain because of fields named W/C Last Replaced and Handsink Last Replaced
                            //if (JsonConstants.LastReplaced == item.surveyParamName || JsonConstants.ReplacementDue == item.surveyParamName)
                            if (item.surveyParamName.Contains(JsonConstants.LastReplaced) || item.surveyParamName.Contains(JsonConstants.ReplacementDue))
                            {
                                //If field has type date then we won't save it in Ps_survey_parameters & pa_item_attributes
                                //instead we 'll save it in pa_property_item_dates & ps_survey_item_dates
                                this.saveSurveyReplacementDates(survey, survData, item, field);
                            }
                            else
                            {
                                //This function will save dates other than replacement dates like CUR DUE, UPGRADE DATE
                                this.saveSurveyItemDates(survey, survData, item, field);
                            }
                            //Added by Behroz - End
                        }

                    }
                    //end for : surveyParamItemField
                }
                //end if: surveyParamItemField.Count                 


            }
            //end for: survey fields
        }
        #endregion

        #region Save Survey Replacement Dates
        /// <summary>
        /// This function 'll save the 
        /// </summary>
        /// <param name="survey"></param>
        /// <param name="survData"></param>

        private void saveSurveyReplacementDates(PS_Survey survey, SurveyData survData, SurveyFieldsData survFieldsData, SurveyParamItemFieldsData survItemsFieldsData)
        {
            DateTime? dueDate = new DateTime();
            DateTime? lastDone = new DateTime();

            PA_PROPERTY_ITEM_DATES paItemDates = new PA_PROPERTY_ITEM_DATES();
            PS_Survey_Item_Dates psItemDates = new PS_Survey_Item_Dates();

            //Added by Behroz - Start
            //The commented code has been taken to a common place
            DateTime surveyParamItemFieldValue = FileHelper.convertDate(survItemsFieldsData);

            //string dateString = survItemsFieldsData.surveyParamItemFieldValue.Replace("/Date(", "").Replace(")/", "");
            //int plusPosition = 0;
            //if (dateString.Contains("+"))
            //{
            //    plusPosition = dateString.IndexOf("+");
            //}
            //else if (dateString.Contains("-"))
            //{
            //    plusPosition = dateString.IndexOf("-");
            //}
            //double seconds = double.Parse(dateString.Substring(0, plusPosition)) / 1000;            
            //TimeZone zone = TimeZone.CurrentTimeZone;
            //// Get offset.
            //double offset = double.Parse(zone.GetUtcOffset(DateTime.Now).ToString().Substring(0, 2));
            //DateTime surveyParamItemFieldValue = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(seconds).AddHours(offset);
            //Added by Behroz - End

            dueDate = DateTime.Now;
            lastDone = DateTime.Now;

            IQueryable<PA_PROPERTY_ITEM_DATES> paItemDateResult;

            //If these parameters are of electric then query will be different. Property , itemid and paramter id will make the unique record in this case
            if (survFieldsData.surveyParameterId == JsonConstants.LastRewiredId || survFieldsData.surveyParameterId == JsonConstants.ConsumerUnitReplacementId)
            {
                paItemDateResult = context.PA_PROPERTY_ITEM_DATES.Where(app => app.ItemId == survData.itemId && app.PROPERTYID == survData.propertyId && app.ParameterId == survFieldsData.surveyParameterId).OrderBy(app => app.SID);

                if (paItemDateResult.Count() > 0)
                {
                    paItemDates = paItemDateResult.First();
                    //save due date in local variable, it will be used to store in ps_survey_item_dates
                    dueDate = paItemDates.DueDate;

                    paItemDates.LastDone = surveyParamItemFieldValue;
                    paItemDates.UPDATEDBY = survData.completedBy;
                    paItemDates.UPDATEDON = DateTime.Now;
                    context.SaveChanges();
                    context.Detach(paItemDates);
                }
            }
            else if (survFieldsData.surveyParameterId != JsonConstants.LastRewiredId || survFieldsData.surveyParameterId != JsonConstants.ConsumerUnitReplacementId)
            {
                //If the parameterid is among the values mentioned in the config file then use the following query
                if (survFieldsData.surveyParameterId == JsonConstants.WcLastReplacedId || survFieldsData.surveyParameterId == JsonConstants.WcReplacementDueId ||
                    survFieldsData.surveyParameterId == JsonConstants.HandBasinLastReplacedId || survFieldsData.surveyParameterId == JsonConstants.HandBasinReplacementDueId ||
                    survFieldsData.surveyParameterId == JsonConstants.MainDwellingRoofLastReplacedId || survFieldsData.surveyParameterId == JsonConstants.MainDwellingRoofReplacementDueId)
                {
                    paItemDateResult = context.PA_PROPERTY_ITEM_DATES.Where(app => app.ItemId == survData.itemId && app.PROPERTYID == survData.propertyId && app.ParameterId == survFieldsData.surveyParameterId).OrderBy(app => app.SID);
                }
                else
                {
                    //for others only propertyid + itemid will make the record unique
                    paItemDateResult = context.PA_PROPERTY_ITEM_DATES.Where(app => app.ItemId == survData.itemId && app.PROPERTYID == survData.propertyId).OrderBy(app => app.SID);
                }

                if (paItemDateResult.Count() > 0)
                {
                    paItemDates = paItemDateResult.First();
                }

                //temporarily save due date in local variable, it will be used to save in ps_survey_item_dates
                dueDate = paItemDates.DueDate;

                //if parameter name is Last Replaced
                //if (JsonConstants.LastReplaced == survFieldsData.surveyParamName)
                if (survFieldsData.surveyParamName.Contains(JsonConstants.LastReplaced))
                {
                    paItemDates.LastDone = surveyParamItemFieldValue;
                }
                else if (survFieldsData.surveyParamName.Contains(JsonConstants.ReplacementDue))
                {
                    //if parameter name is Replacement Due
                    paItemDates.DueDate = surveyParamItemFieldValue;
                }

                paItemDates.UPDATEDBY = survData.completedBy;
                paItemDates.UPDATEDON = DateTime.Now;

                if (paItemDateResult.Count() <= 0)
                {
                    paItemDates.PROPERTYID = survData.propertyId;
                    paItemDates.ItemId = survData.itemId;
                    paItemDates.ParameterId = survFieldsData.surveyParameterId;

                    context.AddToPA_PROPERTY_ITEM_DATES(paItemDates);
                }
                context.SaveChanges();
                context.Detach(paItemDates);
                //Added by Behroz - Start
                //else if (survData.itemId == JsonConstants.StructureId || survData.itemId == JsonConstants.CloakroomId || survData.itemId == JsonConstants.FasciasId || survData.itemId == JsonConstants.SurfaceId)
                //{                    
                //    paItemDates.PROPERTYID = survData.propertyId;
                //    paItemDates.ItemId = survData.itemId;
                //    paItemDates.UPDATEDON = DateTime.Now;
                //    paItemDates.UPDATEDBY = survData.completedBy;

                //    if (JsonConstants.LastReplaced == survFieldsData.surveyParamName)
                //    {
                //        paItemDates.LastDone = surveyParamItemFieldValue;
                //    }
                //    else if (JsonConstants.ReplacementDue == survFieldsData.surveyParamName)
                //    {
                //        //if parameter name is Replacement Due
                //        paItemDates.DueDate = surveyParamItemFieldValue;
                //    }

                //    context.AddToPA_PROPERTY_ITEM_DATES(paItemDates);
                //    context.SaveChanges();
                //    //context.Detach(paItemDates);
                //}
                //Added by Behroz - End
            }

            IQueryable<PS_Survey_Item_Dates> psItemDateResult;

            //If these parameters are of electric then query will be different. Property , itemid and paramter id will make the unique record in this case
            if (survFieldsData.surveyParameterId == JsonConstants.LastRewiredId || survFieldsData.surveyParameterId == JsonConstants.ConsumerUnitReplacementId)
            {
                psItemDateResult = context.PS_Survey_Item_Dates.Where(app => app.ItemId == survData.itemId && app.SurveyId == survey.SurveyId && app.ParameterId == survFieldsData.surveyParameterId).OrderBy(app => app.SurveyId);

                if (psItemDateResult.Count() > 0)
                {
                    psItemDates = psItemDateResult.First();
                }
                else
                {
                    psItemDates.SurveyId = survey.SurveyId;
                    psItemDates.ItemId = survData.itemId;
                    psItemDates.ParameterId = survFieldsData.surveyParameterId;
                }

                psItemDates.LastDone = surveyParamItemFieldValue;

                if (psItemDateResult.Count() == 0)
                {
                    context.AddToPS_Survey_Item_Dates(psItemDates);
                }

                context.SaveChanges();
                context.Detach(psItemDates);
            }
            else
            {
                //Added by Behroz - Start
                //psItemDateResult = context.PS_Survey_Item_Dates.Where(app => app.ItemId == survData.itemId && app.SurveyId == survey.SurveyId).OrderBy(app => app.SurveyId);
                if (survFieldsData.surveyParameterId == JsonConstants.WcLastReplacedId || survFieldsData.surveyParameterId == JsonConstants.WcReplacementDueId ||
                    survFieldsData.surveyParameterId == JsonConstants.HandBasinLastReplacedId || survFieldsData.surveyParameterId == JsonConstants.HandBasinReplacementDueId ||
                    survFieldsData.surveyParameterId == JsonConstants.MainDwellingRoofLastReplacedId || survFieldsData.surveyParameterId == JsonConstants.MainDwellingRoofReplacementDueId)
                {
                    psItemDateResult = context.PS_Survey_Item_Dates.Where(app => app.ItemId == survData.itemId && app.SurveyId == survey.SurveyId && app.ParameterId == survFieldsData.surveyParameterId).OrderBy(app => app.SurveyId);
                }
                else
                {
                    //Added by Behroz - End
                    psItemDateResult = context.PS_Survey_Item_Dates.Where(app => app.ItemId == survData.itemId && app.SurveyId == survey.SurveyId).OrderBy(app => app.SurveyId);
                }

                if (psItemDateResult.Count() > 0)
                {
                    psItemDates = psItemDateResult.First();
                }
                else
                {
                    psItemDates.SurveyId = survey.SurveyId;
                    psItemDates.ItemId = survData.itemId;
                    //Added by Behroz - Start
                    if (survFieldsData.surveyParameterId != 0)
                        psItemDates.ParameterId = survFieldsData.surveyParameterId;
                    //Added by Behroz - End
                }

                //Changed by Behroz - Start
                //if parameter name is Last Replaced
                //if (JsonConstants.LastReplaced == survFieldsData.surveyParamName)
                if (survFieldsData.surveyParamName.Contains(JsonConstants.LastReplaced))
                //Changed by Behroz - End
                {
                    psItemDates.LastDone = surveyParamItemFieldValue;
                }
                //Changed by Behroz - Start
                //else if (JsonConstants.ReplacementDue == survFieldsData.surveyParamName)
                else if (survFieldsData.surveyParamName.Contains(JsonConstants.ReplacementDue))
                //Changed by Behroz - End
                {
                    //if parameter name is Replacement Due
                    psItemDates.DueDate = surveyParamItemFieldValue;
                }

                if (psItemDateResult.Count() == 0)
                {
                    context.AddToPS_Survey_Item_Dates(psItemDates);
                }

                context.SaveChanges();
                context.Detach(psItemDates);
            }


        }
        #endregion

        #region Save Survey Item Dates

        //Added by Behroz - Start
        /// <summary>
        /// 
        /// </summary>
        /// <param name="survey"></param>
        /// <param name="survData"></param>
        /// <param name="survFieldsData"></param>
        /// <param name="survItemsFieldsData"></param>
        private void saveSurveyItemDates(PS_Survey survey, SurveyData survData, SurveyFieldsData survFieldsData, SurveyParamItemFieldsData survItemsFieldsData)
        {
            PS_Survey_Parameters surveyFields = new PS_Survey_Parameters();
            PA_PROPERTY_ATTRIBUTES paAttribItem = new PA_PROPERTY_ATTRIBUTES();

            //Convert date
            DateTime surveyParamItemFieldValue = FileHelper.convertDate(survItemsFieldsData);

            IQueryable<PS_Survey_Parameters> oldSurveyFields;
            oldSurveyFields = context.PS_Survey_Parameters.Where(app => app.SurveyId == survey.SurveyId && app.ItemParamId == survFieldsData.surveyItemParamId);

            if (oldSurveyFields.Count() > 0)
            {
                surveyFields = oldSurveyFields.First();
            }
            else
            {
                surveyFields.SurveyId = survey.SurveyId;
                surveyFields.ItemParamId = survFieldsData.surveyItemParamId;
            }

            //surveyFields.PARAMETERVALUE = survItemsFieldsData.surveyParamItemFieldValue;
            surveyFields.PARAMETERVALUE = surveyParamItemFieldValue.ToString();

            if (oldSurveyFields.Count() <= 0)
            {
                context.AddToPS_Survey_Parameters(surveyFields);
            }

            context.SaveChanges();
            context.Detach(surveyFields);


            //search if field value already exist in PA_PROPERTY_ATTRIBUTES
            var paAttrib = context.PA_PROPERTY_ATTRIBUTES.Where(prop => prop.PROPERTYID == survData.propertyId && prop.ITEMPARAMID == survFieldsData.surveyItemParamId);

            //if field is checkbox then surveyParamItemField will have multiple values and 
            //we will add one more and condition to match valueid, because we 'll have to update all checkbox values
            if (survFieldsData.surveyParamItemField.Count() > 1)
            {
                paAttrib = context.PA_PROPERTY_ATTRIBUTES.Where(prop => prop.PROPERTYID == survData.propertyId && prop.ITEMPARAMID == survFieldsData.surveyItemParamId && prop.VALUEID == survItemsFieldsData.surveyPramItemFieldId);
            }

            if (paAttrib.Count() > 0)
            {
                //fetch the already existed record
                paAttribItem = paAttrib.First();
            }
            else
            {
                //if record does not already exist the set the property id and itemparamid
                paAttribItem.PROPERTYID = survey.PROPERTYID;
                paAttribItem.ITEMPARAMID = survFieldsData.surveyItemParamId;
            }

            //paAttribItem.PARAMETERVALUE = survItemsFieldsData.surveyParamItemFieldValue;
            paAttribItem.PARAMETERVALUE = surveyParamItemFieldValue.ToString();
            paAttribItem.UPDATEDBY = survey.CompletedBy;
            paAttribItem.UPDATEDON = survey.SurveyDate;

            //if previous record does not already exist
            if (paAttrib.Count() <= 0)
            {
                //insert the record
                context.AddToPA_PROPERTY_ATTRIBUTES(paAttribItem);
            }

            context.SaveChanges();
            context.Detach(paAttribItem);
        }
        //Added by Behroz - End
        #endregion

        #region is Appointment For Property Exist

        /// <summary>
        /// This function check that either specific appointment exists against the proerpty.
        /// </summary>
        /// <param name="propertyId">property id</param>
        /// <param name="customerId">customer id</param>
        /// <param name="appointmentId">appointment id</param>
        /// <returns>true or false</returns>

        public bool isAppointmentForPropertyExist(string propertyId, int customerId, int appointmentId)
        {
            bool success = false;

            var apptRecord = (from appt in context.PS_Property2Appointment
                              where appt.PropertyId.ToLower() == propertyId.ToLower()
                                  //&& appt.CustomerId == customerId // Line commented for void property change by Abdul Wahhab - 18/06/2013
                              && appt.AppointId == appointmentId
                              select appt
                            );

            if (apptRecord.Count() > 0)
            {
                success = true;
            }
            else
            {
                success = false;
            }

            return success;

        }
        #endregion

        #region OLD Functions

        #region save Survey Fields

        /// <summary>
        /// This function save the survey form. Data should be post at specific url. 
        /// </summary>
        /// <param name="survData">survey data object</param>
        /// <returns>it returns true or false</returns>

        public bool saveSurveyNewFields(SurveyData survData)
        {

            bool success = false;
            //int Id = (from sur in context.PS_Survey 
            //          join sur2app in context.PS_Appointment2Survey on sur.SurveyId equals sur2app.SurveyId
            //              where sur.PropertyPortionName.ToLower() == survData.propertyPortionName.ToLower() && sur2app.AppointId == survData.appointment.appointmentId
            //              select sur.SurveyFormId).FirstOrDefault();

            //using (TransactionScope trans = new TransactionScope())
            //{
            //    PS_SurveyFormFields surveyFields = new PS_SurveyFormFields();

            //    foreach (var item in survData.surveyFields)
            //    {
            //        if (item.surveyFieldValue != string.Empty && item.surveyFieldValue != null)
            //        {
            //            surveyFields.SurveyFormId = Id;
            //            surveyFields.SurveyFromFieldName = item.surveyFieldName;
            //            surveyFields.SurveyFormFieldValue = item.surveyFieldValue;

            //            context.AddToPS_SurveyFormFields(surveyFields);
            //            context.SaveChanges();
            //            context.Detach(surveyFields); 
            //        } 
            //    }

            //    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            //    trans.Complete();
            //    success = true;
            //}

            return success;
        }
        #endregion



        #region is Survey Form Already Saved
        /// <summary>
        /// This function checks either this survey form has been saved against the specific appointment id and property portion id
        /// </summary>
        /// <param name="appointmentId"> appointment id</param>
        /// <param name="propertyProtionName">property portion name</param>
        /// <returns>true if survey form already exists & false if survey form does not exist</returns>
        public bool isSurveyFormAlreadySaved(int appointmentId, string propertyProtionName, List<SurveyFieldsData> fieldData)
        {
            bool success = false;
            //var surveyForm = (from surv in context.PS_Survey
            //                  join apsr in context.PS_Appointment2Survey on surv.SurveyId equals apsr.SurveyId
            //                  where surv.PropertyPortionName.ToLower() == propertyProtionName.ToLower() && apsr.AppointId == appointmentId
            //                  select surv);

            //if (surveyForm.Count() > 0)
            //{
            //    success = true;
            //}
            //else
            //{
            //    success = false;
            //}

            return success;
        }
        #endregion

        #region is Survey Form Fields Already Saved
        /// <summary>
        /// This function checks either this survey form has been saved against the specific appointment id and property portion id
        /// </summary>
        /// <param name="appointmentId"> appointment id</param>
        /// <param name="propertyProtionName">property portion name</param>
        /// <returns>true if survey form already exists & false if survey form does not exist</returns>
        public bool isSurveyFormFieldsAlreadySaved(SurveyData data)
        {
            bool success = false;
            //foreach (SurveyFieldsData item in data.surveyFields)
            //{
            //    if (item.surveyFieldValue != string.Empty || item.surveyFieldValue != null)
            //    {
            //        var existingFields = (from surF in context.PS_SurveyFormFields
            //                              join sur in context.PS_SurveyForm on surF.SurveyFormId equals sur.SurveyFormId
            //                              join sur2app in context.PS_Appointment2SurveyForm on sur.SurveyFormId equals sur2app.SurveyFormId
            //                              where sur2app.AppointId == data.appointment.appointmentId && sur.PropertyPortionName.ToLower() == data.propertyPortionName.ToLower() && surF.SurveyFormFieldValue == item.surveyFieldValue
            //                              select surF);
            //        if (existingFields.Count() > 0)
            //        {
            //            success = true;
            //        }
            //        else
            //        {
            //            success = false;
            //            break;
            //        }
            //    }
            //    success = true;
            //}
            return success;
        }
        #endregion

        #region get Saved Survey Form
        /// <summary>
        /// this funciton returns the saved survey form against appointment id and property portion name
        /// </summary>
        /// <param name="appointmentId">appointment id</param>
        /// <param name="propertyPortionName">property portion name is the semicolon separated portions of name e.g Internal;Rooof;Dwelling</param>
        /// <returns>it returns the saved data of property survey form</returns>

        public SurveyData getSavedSurveyForm(int appointmentId, string propertyPortionName)
        {
            SurveyData survData = new SurveyData();

            //var surveyForm = (from surv in context.PS_SurveyForm
            //                  join apsr in context.PS_Appointment2SurveyForm on surv.SurveyFormId equals apsr.SurveyFormId
            //                  join surp in context.PS_SurveyFormFields on surv.SurveyFormId equals surp.SurveyFormId
            //                  where apsr.AppointId == appointmentId && surv.PropertyPortionName.ToLower() == propertyPortionName.ToLower()
            //                  select new SurveyData
            //                  {
            //                      surveyFormId = surv.SurveyFormId,
            //                      surveyFormName = surv.SurveyFormName,
            //                      propertyPortionName = surv.PropertyPortionName,
            //                      notes = surv.Notes,     
            //                      appointment = new AppointmentData
            //                      {
            //                          appointmentId = apsr.AppointId
            //                      }                                  
            //                  }
            //                 );


            //if(surveyForm.Count() > 0)
            //{
            //    survData = surveyForm.First(); 
            //}


            ////Fetch the survey saved fields agaisnt the survey form
            //var surveyFields = (from surv in context.PS_SurveyForm
            //                    join surp in context.PS_SurveyFormFields on surv.SurveyFormId equals surp.SurveyFormId
            //                    where surv.SurveyFormId == survData.surveyFormId
            //                    select new SurveyFieldsData
            //                    {
            //                        surveyFieldId = surp.SurveyFormFieldId,
            //                        surveyFieldName = surp.SurveyFromFieldName,
            //                        surveyParamItemFieldValue = surp.SurveyFormFieldValue
            //                    });
            //List<SurveyFieldsData> survFieldData = new List<SurveyFieldsData>();

            //if (surveyFields.Count() > 0)
            //{
            //    foreach (var item in surveyFields)
            //    {
            //        survFieldData.Add(item);
            //    }
            //}

            //survData.surveyFields = survFieldData;

            return survData;
        }
        #endregion


        #region get All Saved Survey Forms
        /// <summary>
        /// this funciton returns all the saved survey forms against appointment id
        /// </summary>
        /// <param name="appointmentId">appointment id</param>        
        /// <returns>it returns the list of saved forms data of property survey form</returns>

        public List<SurveyData> getAllSavedSurveyForms(int appointmentId)
        {
            List<SurveyData> survData = new List<SurveyData>();
            List<SurveyData> allFormsData = new List<SurveyData>();

            ////fetch all the survey forms against appointment id
            //var surveyForm = (from surv in context.PS_SurveyForm
            //                  join apsr in context.PS_Appointment2SurveyForm on surv.SurveyFormId equals apsr.SurveyFormId                              
            //                  where apsr.AppointId == appointmentId 
            //                  select new SurveyData
            //                  {
            //                      surveyFormId = surv.SurveyFormId,
            //                      surveyFormName = surv.SurveyFormName,
            //                      propertyPortionName = surv.PropertyPortionName,
            //                      notes = surv.Notes,
            //                      appointment = new AppointmentData
            //                      {
            //                          appointmentId = apsr.AppointId
            //                      }
            //                  }
            //                 );



            ////if count of survey forms is greater than 0
            //if (surveyForm.Count() > 0)
            //{
            //    //convert the result into list
            //    survData = surveyForm.ToList();
            //}

            ////loop the survey forms to get the saved fields and value
            //foreach (var form in survData)
            //{
            //    //Fetch the survey saved fields agaisnt the survey form
            //    var surveyFields = (from surv in context.PS_SurveyForm
            //                        join surp in context.PS_SurveyFormFields on surv.SurveyFormId equals surp.SurveyFormId
            //                        where surv.SurveyFormId == form.surveyFormId
            //                        select new SurveyFieldsData
            //                        {
            //                            surveyFieldId = surp.SurveyFormFieldId,
            //                            surveyFieldName = surp.SurveyFromFieldName,
            //                            surveyParamItemFieldValue = surp.SurveyFormFieldValue
            //                        });
            //    List<SurveyFieldsData> survFieldData = new List<SurveyFieldsData>();

            //    if (surveyFields.Count() > 0)
            //    {
            //        foreach (var item in surveyFields)
            //        {
            //            survFieldData.Add(item);
            //        }
            //    }

            //    form.surveyFields = survFieldData;
            //    allFormsData.Add(form);
            //}

            return allFormsData;
        }
        #endregion


        ///123456///
        #region get Surveyor Survey Forms
        /// <summary>
        /// this funciton returns all the survey forms against a surveyor username
        /// </summary>
        /// <param name="userName">surveyor username</param>        
        /// <returns>it returns the list of saved forms data of property survey form against a surveyor</returns>

        public List<SurveyData> getSurveyorSurveyForms(string userName)
        {


            List<SurveyData> survData = new List<SurveyData>();
            List<SurveyData> allFormsData = new List<SurveyData>();

            ////fetch all the survey forms against surveyor username
            //var surveyForm = (from surv in context.PS_SurveyForm
            //                  join apsr in context.PS_Appointment2SurveyForm on surv.SurveyFormId equals apsr.SurveyFormId
            //                  join app in context.PS_Appointment on apsr.AppointId equals app.AppointId
            //                  where app.SurveyourUserName == userName
            //                  //where apsr.AppointId == appointmentId
            //                  select new SurveyData
            //                  {
            //                      surveyFormId = surv.SurveyFormId,
            //                      surveyFormName = surv.SurveyFormName,
            //                      propertyPortionName = surv.PropertyPortionName,
            //                      notes = surv.Notes,
            //                      appointment = new AppointmentData
            //                      {
            //                          appointmentId = apsr.AppointId,
            //                          appointmentStartDateTime = app.AppointStartDateTime,
            //                          appointmentEndDateTime = app.AppointEndDateTime,
            //                          appointmentStatus = app.AppointProgStatus

            //                      }
            //                  }
            //                 );


            ////if count of survey forms is greater than 0
            //if (surveyForm.Count() > 0)
            //{
            //    //convert the result into list
            //    survData = surveyForm.ToList();
            //}

            ////loop the survey forms to get the saved fields and value
            //foreach (var form in survData)
            //{
            //    //Fetch the survey saved fields agaisnt the survey form
            //    var surveyFields = (from surv in context.PS_SurveyForm
            //                        join surp in context.PS_SurveyFormFields on surv.SurveyFormId equals surp.SurveyFormId
            //                        where surv.SurveyFormId == form.surveyFormId
            //                        select new SurveyFieldsData
            //                        {
            //                            surveyFieldId = surp.SurveyFormFieldId,
            //                            surveyFieldName = surp.SurveyFromFieldName,
            //                            surveyParamItemFieldValue = surp.SurveyFormFieldValue
            //                        });
            //    List<SurveyFieldsData> survFieldData = new List<SurveyFieldsData>();

            //    if (surveyFields.Count() > 0)
            //    {
            //        foreach (var item in surveyFields)
            //        {
            //            survFieldData.Add(item);
            //        }
            //    }

            //    form.surveyFields = survFieldData;
            //    allFormsData.Add(form);
            //}

            return allFormsData;
        }
        #endregion

        #endregion
    }
}
