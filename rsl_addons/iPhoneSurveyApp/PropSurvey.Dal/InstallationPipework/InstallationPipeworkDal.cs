﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Transactions;
using PropSurvey.Contracts.Data;
using PropSurvey.Dal.Base;
using PropSurvey.Entities;

namespace PropSurvey.Dal.InstallationPipework
{
    public class InstallationPipeworkDal : BaseDal
    {

        #region get InstallationPipework Data
        /// <summary>
        /// This function returns the InstallationPipework data
        /// </summary>
        /// <returns>List of InstallationPipework data objects</returns>

        public InstallationPipeworkData getInstallationPipeworkFormData(int AppointmentID)
        {
            InstallationPipeworkData _var = (from work in context.GS_InstallationPipeWork
                           where work.AppointmentID == AppointmentID
                           select new InstallationPipeworkData 
                              {
                                  ID = work.ID,
                                  AppointmentID = work.AppointmentID,
                                  Date = work.Date,
                                  EmergencyControl = work.EmergencyControl,
                                  EquipotentialBonding = work.EquipotentialBonding,
                                  GasTightnessTest = work.GasTightnessTest,
                                  VisualInspection = work.VisualInspection
                              }).FirstOrDefault();

            return _var;
        }

        //Change#12 - Behroz - 12/14/2012 - Start
        /// <summary>
        /// This function returns the InstallationPipework data for gas
        /// </summary>
        /// <returns>List of InstallationPipework data objects</returns>
        //public InstallationPipeworkData getInstallationPipeworkFormDataGas(string propertyId)
        //{
        //    InstallationPipeworkData _var = (from work in context.P_InstallationPipeWork
        //                                     where work.PropertyId == propertyId
        //    //InstallationPipeworkData _var = (from work in context.P_InstallationPipeWork join jor in context.AS_JOURNAL
        //    //                                                                             on work.PropertyId equals jor.PROPERTYID
        //    //                                 where work.PropertyId == propertyId && jor.ISCURRENT
        //                                     select new InstallationPipeworkData
        //                                     {
        //                                         ID = work.PipeWorkID,
        //                                         PropertyId = work.PropertyId,
        //                                         Date = work.DateStamp,
        //                                         EmergencyControl = work.EmergencyControl,
        //                                         EquipotentialBonding = work.EquipotentialBonding,
        //                                         GasTightnessTest = work.GasTightnessTest,
        //                                         VisualInspection = work.VisualInspection,
        //                                         JournalId = work.JournalId
        //                                     }).FirstOrDefault();

        //    return _var;
        //}

        /// <summary>
        /// This function returns the InstallationPipework data for gas
        /// </summary>
        /// <returns>List of InstallationPipework data objects</returns>
        public InstallationPipeworkData getInstallationPipeworkFormDataGas(int journalId)
        {
            InstallationPipeworkData _var = (from work in context.P_InstallationPipeWork
                                             where work.Journalid == journalId                                             
                                             select new InstallationPipeworkData
                                             {
                                                 ID = work.PipeWorkID,
                                                 PropertyId = work.PropertyId,
                                                 Date = work.DateStamp,
                                                 EmergencyControl = work.EmergencyControl,
                                                 EquipotentialBonding = work.EquipotentialBonding,
                                                 GasTightnessTest = work.GasTightnessTest,
                                                 VisualInspection = work.VisualInspection,
                                                 JournalId = work.Journalid
                                             }).FirstOrDefault();

            return _var;
        }
        //Change#12 - Behroz - 12/14/2012 - End
        #endregion

        #region save InstallationPipework Data
        /// <summary>
        /// This function saves the InstallationPipework Data in the database
        /// </summary>
        /// <param name="appData">The object of InstallationPipework Data</param>
        /// <returns>the InstallationPipework id if successfully save otherwise 0 </returns>

        public int saveInstallationPipework(InstallationPipeworkData workData)
        {
            bool success = false;

            GS_InstallationPipeWork insPipework = new GS_InstallationPipeWork();
            using (TransactionScope trans = new TransactionScope())
            {
                insPipework.AppointmentID = workData.AppointmentID;
                insPipework.Date = DateTime.Now;
                insPipework.EmergencyControl = workData.EmergencyControl.ToUpper();
                insPipework.EquipotentialBonding = workData.EquipotentialBonding.ToUpper();
                insPipework.GasTightnessTest = workData.GasTightnessTest.ToUpper();
                insPipework.VisualInspection = workData.VisualInspection.ToUpper();

                context.AddToGS_InstallationPipeWork(insPipework);
                context.SaveChanges();
                trans.Complete();
                success = true;
            }

            if (success == true)
            {
                return insPipework.ID;
            }
            else
            {
                return 0;
            }
        }

        //Change#12 - Behroz - 12/13/2012 - Start
        ///// <summary>
        ///// This function saves the InstallationPipework Data in the database for gas
        ///// </summary>
        ///// <param name="appData">The object of InstallationPipework Data</param>
        ///// <returns>the InstallationPipework id if successfully save otherwise 0 </returns>
        //public int saveInstallationPipeworkGas(InstallationPipeworkData workData)
        //{
        //    P_InstallationPipeWork installationwork = new P_InstallationPipeWork();

        //    var installObj = context.P_InstallationPipeWork.Where(ins => ins.PropertyId == workData.PropertyId);

        //    if (installObj.Count() > 0)
        //    {
        //        //update
        //        installationwork = installObj.First();
        //        installationwork.DateStamp = DateTime.Now;
        //        installationwork.EmergencyControl = workData.EmergencyControl.ToUpper();
        //        installationwork.EquipotentialBonding = workData.EquipotentialBonding.ToUpper();
        //        installationwork.GasTightnessTest = workData.GasTightnessTest.ToUpper();
        //        installationwork.VisualInspection = workData.VisualInspection.ToUpper();

        //        context.SaveChanges();
        //        return 0;
        //    }
        //    else
        //    {
        //        //Property does not exist, so insert in DB
        //        using (TransactionScope trans = new TransactionScope())
        //        {
        //            installationwork.PropertyId = workData.PropertyId;
        //            installationwork.DateStamp = DateTime.Now;
        //            installationwork.EmergencyControl = workData.EmergencyControl.ToUpper();
        //            installationwork.EquipotentialBonding = workData.EquipotentialBonding.ToUpper();
        //            installationwork.GasTightnessTest = workData.GasTightnessTest.ToUpper();
        //            installationwork.VisualInspection = workData.VisualInspection.ToUpper();

        //            context.AddToP_InstallationPipeWork(installationwork);
        //            context.SaveChanges();
        //            trans.Complete();

        //            return installationwork.PipeWorkID;
        //        }
        //    }
        //}


        //Change#12 - Behroz - 12/13/2012 - End
        /// <summary>
        /// This function saves the InstallationPipework Data in the database for gas
        /// </summary>
        /// <param name="appData">The object of InstallationPipework Data</param>
        /// <returns>the InstallationPipework id if successfully save otherwise 0 </returns>
        public int saveInstallationPipeworkGas(InstallationPipeworkData workData)
        {
            P_InstallationPipeWork installationwork = new P_InstallationPipeWork();

            var installObj = context.P_InstallationPipeWork.Where(ins => ins.Journalid == workData.JournalId);

            if (installObj.Count() > 0)
            {
                //update
                installationwork = installObj.First();
                installationwork.DateStamp = DateTime.Now;
                installationwork.EmergencyControl = workData.EmergencyControl.ToUpper();
                installationwork.EquipotentialBonding = workData.EquipotentialBonding.ToUpper();
                installationwork.GasTightnessTest = workData.GasTightnessTest.ToUpper();
                installationwork.VisualInspection = workData.VisualInspection.ToUpper();
                //installationwork.JournalId = workData.JournalId;

                context.SaveChanges();
                return 0;
            }
            else
            {
                //Property does not exist, so insert in DB
                using (TransactionScope trans = new TransactionScope())
                {
                    installationwork.PropertyId = workData.PropertyId;
                    installationwork.DateStamp = DateTime.Now;
                    installationwork.EmergencyControl = workData.EmergencyControl.ToUpper();
                    installationwork.EquipotentialBonding = workData.EquipotentialBonding.ToUpper();
                    installationwork.GasTightnessTest = workData.GasTightnessTest.ToUpper();
                    installationwork.VisualInspection = workData.VisualInspection.ToUpper();
                    installationwork.Journalid = workData.JournalId;

                    context.AddToP_InstallationPipeWork(installationwork);
                    context.SaveChanges();
                    trans.Complete();

                    return installationwork.PipeWorkID;
                }
            }
        }
        #endregion

        #region update InstallationPipework Data
        /// <summary>
        /// This function updates theInstallationPipework Data in the database
        /// </summary>
        /// <param name="appData">The object of InstallationPipework Data</param>
        /// <returns>true if successfully save otherwise 0 </returns>

        public bool updateInstallationPipeworkData(InstallationPipeworkData insData)
        {
            bool success = false;
            using (TransactionScope trans = new TransactionScope())
            {
                var _var = (from ins in context.GS_InstallationPipeWork
                                           where ins.ID == insData.ID
                                           select ins);
                if (_var.Count() > 0)
                {
                    GS_InstallationPipeWork Ins = _var.FirstOrDefault();
                    Ins.VisualInspection = insData.VisualInspection.ToUpper();
                    Ins.GasTightnessTest = insData.GasTightnessTest.ToUpper();
                    Ins.EquipotentialBonding = insData.EquipotentialBonding.ToUpper();
                    Ins.EmergencyControl = insData.EmergencyControl.ToUpper();
                    Ins.AppointmentID = insData.AppointmentID;

                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    trans.Complete();
                    success = true;
                }
                else
                {
                    success = false;
                }
                
            }

            return success;
        }

        #endregion

        public bool checkAppointmentID(int appointmentID)
        {
            var id = context.PS_Appointment.Where(app=> app.AppointId == appointmentID);
            if (id.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
