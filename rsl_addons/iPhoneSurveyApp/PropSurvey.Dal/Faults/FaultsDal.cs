﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Transactions;
using PropSurvey.Contracts.Data;
using PropSurvey.Dal.Base;
using PropSurvey.Entities;
using System.IO;
using System.Web;

namespace PropSurvey.Dal.Faults
{
    public class FaultsDal : BaseDal
    {
        #region get Faults Data
        /// <summary>
        /// This function returns the Faults data
        /// </summary>
        /// <returns>List of Faults data objects</returns>

        public List<FaultsData> getFaultsData(int AppointmentID)
        {
            var _var = (from fault in context.GS_GasFault
                               where fault.AppointmentID == AppointmentID
                               select new FaultsData 
                              {
                                  ID = fault.ID,
                                  AppointmentID = fault.AppointmentID,
                                  DefectDesc = fault.DefectDesc,
                                  FaultCategory = fault.FaultCategory,
                                  isAdviceNoteIssued = fault.isAdviceNoteIssued,
                                  RemedialAction = fault.RemedialAction,
                                  SerialNo = fault.SerialNo,
                                  WarningTagFixed = fault.WarningTagFixed
                                  
                              });

           List<FaultsData> faults = new List<FaultsData>();
            if (_var.Count() > 0)
            {
                faults = _var.ToList();
            }

            return faults;
        }

        //Change#18 - Behroz - 19/12/2012 - Start
        /// <summary>
        /// This function returns the Faults data
        /// </summary>
        /// <returns>List of Faults data objects</returns>
        public List<FaultsDataGas> getFaultsDataGas(int journalId)
        {
            var data = from defects in context.P_PROPERTY_APPLIANCE_DEFECTS
                       where defects.JournalId == journalId
                       select new FaultsDataGas { 
                            RemedialAction = defects.ActionNotes,
                            ApplianceID = defects.ApplianceId,
                            FaultCategory = defects.CategoryId,
                            DefectDate = defects.DefectDate,
                            DefectDesc = defects.DefectNotes,
                            IsActionTaken = defects.IsActionTaken,
                            IsDefectIdentified = defects.IsDefectIdentified,
                            WarningTagFixed = defects.IsWarningFixed,
                            isAdviceNoteIssued = defects.IsWarningIssued,
                            JournalId = defects.JournalId,
                            ID = defects.PropertyDefectId,
                            PropertyId = defects.PropertyId,
                            SerialNo = defects.SerialNumber
                       };


            List<FaultsDataGas> faults = new List<FaultsDataGas>();
            if (data.Count() > 0)
            {
                faults = data.ToList();
            }

            return faults;
        }
        //Change#18 - Behroz - 19/12/2012 - End


        #endregion

        //#region save Faults Log History Data
        ///// <summary>
        ///// This function saves the Faults Log History Data in the database
        ///// </summary>
        ///// <param name="appData">The object of Faults Log History Data</param>
        ///// <returns>the Faults id if successfully save otherwise 0 </returns>

        //public int saveFaultLogHistoryData(FaultLogHistoryData faultLogHistoryData)
        //{
        //    bool success = false;

        //    FL_FAULT_LOG_HISTORY faultlLogHistory = new FL_FAULT_LOG_HISTORY();
        //    //using (TransactionScope trans = new TransactionScope())
        //    //{
        //        faultlLogHistory.JournalID = faultLogHistoryData.journalID;
        //        faultlLogHistory.FaultStatusID = faultLogHistoryData.faultStatusID;
        //        faultlLogHistory.ItemActionID = faultLogHistoryData.itemActionID;
        //        faultlLogHistory.LastActionDate = faultLogHistoryData.lastActionDate;
        //        faultlLogHistory.LastActionUserID = faultLogHistoryData.lastActionUserID;
        //        faultlLogHistory.FaultLogID = faultLogHistoryData.faultLogID;
        //        faultlLogHistory.ORGID = faultLogHistoryData.ORGID;
        //        faultlLogHistory.ScopeID = faultLogHistoryData.scopeID;
        //        faultlLogHistory.Title = faultLogHistoryData.title;
        //        faultlLogHistory.Notes = faultLogHistoryData.notes;
        //        faultlLogHistory.ORGID = faultLogHistoryData.ORGID;
        //        faultlLogHistory.PROPERTYID = faultLogHistoryData.propertyID;
        //        faultlLogHistory.ContractorID = faultLogHistoryData.contractorID;

        //        context.AddToFL_FAULT_LOG_HISTORY(faultlLogHistory);              
        //        context.SaveChanges();
        //        //trans.Complete();
        //        success = true;
        //    //}

        //    if (success == true)
        //    {
        //        return faultlLogHistory.FaultLogHistoryID;
        //    }
        //    else
        //    {
        //        return 0;
        //    }
        //}
        //#endregion

        #region save Faults Data
        /// <summary>
        /// This function saves the Faults Data in the database
        /// </summary>
        /// <param name="appData">The object of Faults Data</param>
        /// <returns>the Faults id if successfully save otherwise 0 </returns>

        public int saveFaultsData(FaultsData faultData)
        {
            bool success = false;

            GS_GasFault fault = new GS_GasFault();
            using (TransactionScope trans = new TransactionScope())
            {
                fault.AppointmentID = faultData.AppointmentID;
                fault.WarningTagFixed = faultData.WarningTagFixed;
                fault.SerialNo = faultData.SerialNo;
                fault.RemedialAction = faultData.RemedialAction;
                fault.isAdviceNoteIssued = faultData.isAdviceNoteIssued;
                fault.FaultCategory = faultData.FaultCategory;
                fault.DefectDesc = faultData.DefectDesc;

                context.AddToGS_GasFault(fault);
                context.SaveChanges();
                trans.Complete();
                success = true;
            }

            if (success == true)
            {
                return fault.ID;
            }
            else
            {
                return 0;
            }
        }

        //Change#19 - Behroz - 19/12/2012 - Start
        /// <summary>
        /// This function saves the Faults Data in the database for gas
        /// </summary>
        /// <param name="appData">The object of Faults Data</param>
        /// <returns>the Faults id if successfully save otherwise 0 </returns>
        public int saveFaultsDataGas(FaultsDataGas faultData)
        {
            bool success = false;
            P_PROPERTY_APPLIANCE_DEFECTS fault = new P_PROPERTY_APPLIANCE_DEFECTS();

            using (TransactionScope trans = new TransactionScope())
            { 
                fault.ActionNotes = HttpUtility.UrlDecode(faultData.RemedialAction); //Applied raw URL Decoding to save simple text string in datebase.
                fault.ApplianceId = faultData.ApplianceID;
                fault.CategoryId = faultData.FaultCategory;
                fault.DefectDate = DateTime.Now;
                fault.DefectNotes = HttpUtility.UrlDecode(faultData.DefectDesc); //Applied raw URL Decoding to save simple text string in datebase.
                fault.IsActionTaken = faultData.IsActionTaken;
                fault.IsDefectIdentified = faultData.IsDefectIdentified;
                fault.IsWarningFixed = faultData.WarningTagFixed;
                fault.IsWarningIssued = faultData.isAdviceNoteIssued;
                fault.JournalId = faultData.JournalId;
                fault.PropertyId = faultData.PropertyId;
                fault.SerialNumber = faultData.SerialNo;

                context.AddToP_PROPERTY_APPLIANCE_DEFECTS(fault);
                context.SaveChanges();
                trans.Complete();
                success = true;
            }

            if (success == true)
            {
                return fault.PropertyDefectId;
            }
            else
            {
                return 0;
            }
        }
        //Change#19 - Behroz - 19/12/2012 - End

        #endregion

        #region update fault Data
        /// <summary>
        /// This function updates fault Data in the database
        /// </summary>
        /// <param name="appData">The object of fault Data</param>
        /// <returns>true if successfully save otherwise 0 </returns>

        public bool updateFaultData(FaultsData faultData)
        {
            bool success = false;
            using (TransactionScope trans = new TransactionScope())
            {
                var _var = (from ins in context.GS_GasFault
                            where ins.ID == faultData.ID
                                           select ins);
                if (_var.Count() > 0)
                {
                    GS_GasFault fault = _var.FirstOrDefault();
                    fault.FaultCategory =  faultData.FaultCategory;
                    fault.DefectDesc = faultData.DefectDesc;
                    fault.AppointmentID = faultData.AppointmentID;
                    fault.isAdviceNoteIssued = faultData.isAdviceNoteIssued;
                    fault.RemedialAction = faultData.RemedialAction;
                    fault.SerialNo = faultData.SerialNo;
                    fault.WarningTagFixed = faultData.WarningTagFixed;

                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    trans.Complete();
                    success = true;
                }
                else
                {
                    success = false;
                }
                
            }

            return success;
        }

        //Change#20 - Behroz - 19/12/2012 - Start
        /// <summary>
        /// This function updates fault Data in the database for gas
        /// </summary>
        /// <param name="appData">The object of fault Data</param>
        /// <returns>true if successfully save otherwise 0 </returns>
        public bool updateFaultDataGas(FaultsDataGas faultData)
        {
            bool success = false;
            using (TransactionScope trans = new TransactionScope())
            {
                var _var = (from ins in context.P_PROPERTY_APPLIANCE_DEFECTS
                            where ins.PropertyDefectId == faultData.ID
                            select ins);

                if (_var.Count() > 0)
                {
                    P_PROPERTY_APPLIANCE_DEFECTS fault = _var.FirstOrDefault();

                    fault.ActionNotes = HttpUtility.UrlDecode(faultData.RemedialAction); //Applied raw URL Decoding to save simple text string in datebase.
                    fault.ApplianceId = faultData.ApplianceID;
                    fault.CategoryId = faultData.FaultCategory;
                    fault.DefectDate = DateTime.Now;
                    fault.DefectNotes = HttpUtility.UrlDecode(faultData.DefectDesc); //Applied raw URL Decoding to save simple text string in datebase.
                    fault.IsActionTaken = faultData.IsActionTaken;
                    fault.IsDefectIdentified = faultData.IsDefectIdentified;
                    fault.IsWarningFixed = faultData.WarningTagFixed;
                    fault.IsWarningIssued = faultData.isAdviceNoteIssued;
                    fault.JournalId = faultData.JournalId;
                    fault.PropertyId = faultData.PropertyId;
                    fault.SerialNumber = faultData.SerialNo;

                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    trans.Complete();
                    success = true;
                }
                else
                {
                    success = false;
                }
            }
            return success;
        }
        //Change#20 - Behroz - 19/12/2012 - End

        #endregion

        #region get General Comments
        /// <summary>
        /// This function returns General Comments
        /// </summary>
        /// <returns>List of General Comments</returns>
        public List<FaultsData> getGeneralComments(int AppointmentID)
        {
            var _var = (from fault in context.GS_GasFault
                        where fault.AppointmentID == AppointmentID && fault.FaultCategory.ToLower() == "general comment"
                        select new FaultsData
                        {
                            ID = fault.ID,
                            AppointmentID = fault.AppointmentID,
                            DefectDesc = fault.DefectDesc,
                            FaultCategory = fault.FaultCategory,
                            isAdviceNoteIssued = fault.isAdviceNoteIssued,
                            RemedialAction = fault.RemedialAction,
                            SerialNo = fault.SerialNo,
                            WarningTagFixed = fault.WarningTagFixed

                        });

            List<FaultsData> faults = new List<FaultsData>();
            if (_var.Count() > 0)
            {
                faults = _var.ToList();
            }

            return faults;
        }

        //Change#21 - Behroz - 19/12/2012 - Start
        /// <summary>
        /// This function returns General Comments for gas
        /// </summary>
        /// <returns>List of General Comments</returns>
        //public List<FaultsDataGas> getGeneralCommentsGas(string PropertyID)
        //{
        //    var _var = (from fault in context.P_PROPERTY_APPLIANCE_DEFECTS
        //                where fault.PropertyId == PropertyID && fault.CategoryId == 1
        //                select new FaultsDataGas
        //                {
        //                    RemedialAction = fault.ActionNotes,
        //                    ApplianceID = fault.ApplianceId,
        //                    FaultCategory = fault.CategoryId,
        //                    DefectDate = fault.DefectDate,
        //                    DefectDesc = fault.DefectNotes,
        //                    IsActionTaken = fault.IsActionTaken,
        //                    IsDefectIdentified = fault.IsDefectIdentified,
        //                    WarningTagFixed = fault.IsWarningFixed,
        //                    isAdviceNoteIssued = fault.IsWarningIssued,
        //                    JournalId = fault.JournalId,
        //                    ID = fault.PropertyDefectId,
        //                    PropertyId = fault.PropertyId,
        //                    SerialNo = fault.SerialNumber                            
        //                });

        //    List<FaultsDataGas> faults = new List<FaultsDataGas>();
        //    if (_var.Count() > 0)
        //    {
        //        faults = _var.ToList();
        //    }

        //    return faults;
        //}
        public List<FaultsDataGas> getGeneralCommentsGas(int journalId)
        {
            var _var = (from fault in context.P_PROPERTY_APPLIANCE_DEFECTS
                        where fault.JournalId == journalId && fault.CategoryId == Utilities.Constants.MessageConstants.GeneralCommentCategoryId
                        select new FaultsDataGas
                        {
                            RemedialAction = fault.ActionNotes,
                            ApplianceID = fault.ApplianceId,
                            FaultCategory = fault.CategoryId,
                            DefectDate = fault.DefectDate,
                            DefectDesc = fault.DefectNotes,
                            IsActionTaken = fault.IsActionTaken,
                            IsDefectIdentified = fault.IsDefectIdentified,
                            WarningTagFixed = fault.IsWarningFixed,
                            isAdviceNoteIssued = fault.IsWarningIssued,
                            JournalId = fault.JournalId,
                            ID = fault.PropertyDefectId,
                            PropertyId = fault.PropertyId,
                            SerialNo = fault.SerialNumber
                        });

            List<FaultsDataGas> faults = new List<FaultsDataGas>();
            if (_var.Count() > 0)
            {
                faults = _var.ToList();
            }

            return faults;
        }
        //Change#21 - Behroz - 19/12/2012 - End
        #endregion

        #region get Faults only.
        /// <summary>
        /// This function returns Faults only.
        /// </summary>
        /// <returns>List of Faults.</returns>

        public List<FaultsData> getFaultsOnly(int AppointmentID)
        {
            var _var = (from fault in context.GS_GasFault
                        where fault.AppointmentID == AppointmentID && fault.FaultCategory.ToLower() != "general comment"
                        select new FaultsData
                        {
                            ID = fault.ID,
                            AppointmentID = fault.AppointmentID,
                            DefectDesc = fault.DefectDesc,
                            FaultCategory = fault.FaultCategory,
                            isAdviceNoteIssued = fault.isAdviceNoteIssued,
                            RemedialAction = fault.RemedialAction,
                            SerialNo = fault.SerialNo,
                            WarningTagFixed = fault.WarningTagFixed

                        });

            List<FaultsData> faults = new List<FaultsData>();
            if (_var.Count() > 0)
            {
                faults = _var.ToList();
            }

            return faults;
        }

        //Change#22 - Behroz - 19/12/2012 - Start
        /// <summary>
        /// This function returns Faults only for gas
        /// </summary>
        /// <returns>List of Faults.</returns>
        //public List<FaultsDataGas> getFaultsOnlyGas(string PropertyID)
        //{
        //    var _var = (from fault in context.P_PROPERTY_APPLIANCE_DEFECTS
        //                where fault.PropertyId == PropertyID && fault.CategoryId != 1
        //                select new FaultsDataGas
        //                {
        //                    RemedialAction = fault.ActionNotes,
        //                    ApplianceID = fault.ApplianceId,
        //                    FaultCategory = fault.CategoryId,
        //                    DefectDate = fault.DefectDate,
        //                    DefectDesc = fault.DefectNotes,
        //                    IsActionTaken = fault.IsActionTaken,
        //                    IsDefectIdentified = fault.IsDefectIdentified,
        //                    WarningTagFixed = fault.IsWarningFixed,
        //                    isAdviceNoteIssued = fault.IsWarningIssued,
        //                    JournalId = fault.JournalId,
        //                    ID = fault.PropertyDefectId,
        //                    PropertyId = fault.PropertyId,
        //                    SerialNo = fault.SerialNumber
        //                });

        //    List<FaultsDataGas> faults = new List<FaultsDataGas>();
        //    if (_var.Count() > 0)
        //    {
        //        faults = _var.ToList();
        //    }

        //    return faults;
        //}
        //Change#22 - Behroz - 19/12/2012 - End
        public List<FaultsDataGas> getFaultsOnlyGas(int journalId)
        {
            var _var = (from fault in context.P_PROPERTY_APPLIANCE_DEFECTS
                        where fault.JournalId == journalId && fault.CategoryId != Utilities.Constants.MessageConstants.GeneralCommentCategoryId
                        select new FaultsDataGas
                        {
                            RemedialAction = fault.ActionNotes,
                            ApplianceID = fault.ApplianceId,
                            FaultCategory = fault.CategoryId,
                            DefectDate = fault.DefectDate,
                            DefectDesc = fault.DefectNotes,
                            IsActionTaken = fault.IsActionTaken,
                            IsDefectIdentified = fault.IsDefectIdentified,
                            WarningTagFixed = fault.IsWarningFixed,
                            isAdviceNoteIssued = fault.IsWarningIssued,
                            JournalId = fault.JournalId,
                            ID = fault.PropertyDefectId,
                            PropertyId = fault.PropertyId,
                            SerialNo = fault.SerialNumber
                        });

            List<FaultsDataGas> faults = new List<FaultsDataGas>();
            if (_var.Count() > 0)
            {
                faults = _var.ToList();
            }

            return faults;
        }

        #endregion
        
        #region Get All Defect Categories
        //Change#15 - Behroz - 12/17/2012 - Start
        /// <summary>
        /// This function returns all the defect categories
        /// </summary>
        /// <returns>List of DefectCategory data objects</returns>
        public List<DefectCategoryData> getAllDefectCategories()
        {
            var _var = (from cat in context.P_DEFECTS_CATEGORY                        
                        select new DefectCategoryData
                        {
                            CatId = cat.CategoryId,
                            CatDescription = cat.Description

                        });

            List<DefectCategoryData> faults = new List<DefectCategoryData>();
            if (_var.Count() > 0)
            {
                faults = _var.ToList();
            }

            return faults;
        }
        //Change#15 - Behroz - 12/17/2012 - End

        #endregion

        #region "Get Fault id"
        //Change#51 - Behroz - 16/01/2013 - Start
        /// <summary>
        /// This function returns the fault id
        /// </summary>
        /// <param name="propertyId"></param>
        /// <returns></returns>
        public int getFaultId(string propertyId)
        {
            bool success = false;
            P_PROPERTY_APPLIANCE_DEFECTS fault = new P_PROPERTY_APPLIANCE_DEFECTS();

            using (TransactionScope trans = new TransactionScope())
            {
                fault.CategoryId = 1;
                fault.PropertyId = propertyId;

                context.AddToP_PROPERTY_APPLIANCE_DEFECTS(fault);
                context.SaveChanges();
                trans.Complete();
                success = true;
            }

            if (success == true)
            {
                return fault.PropertyDefectId;
            }
            else
            {
                return 0;
            }
        }
        //Change#51 - Behroz - 16/01/2013 - End
        #endregion

        #region "Delete Fault"
        //Change#52 - Behroz - 16/01/2013 - Start
        /// <summary>
        /// This function deletes the fault from the database
        /// </summary>
        /// <param name="propertyId"></param>
        /// <returns></returns>
        public bool deleteFault(int faultId)
        {
            bool success = false;

            using (TransactionScope trans = new TransactionScope())
            {
                List<P_PROPERTY_APPLIANCE_DEFECTS_IMAGES>  faultImages = context.P_PROPERTY_APPLIANCE_DEFECTS_IMAGES.Where(img => img.PropertyDefectId == faultId).ToList();

                foreach (var item in faultImages)
                {
                    context.P_PROPERTY_APPLIANCE_DEFECTS_IMAGES.DeleteObject(item);
                    context.SaveChanges();
                }  
                
                
                P_PROPERTY_APPLIANCE_DEFECTS fault = context.P_PROPERTY_APPLIANCE_DEFECTS.Where(fau => fau.PropertyDefectId == faultId).First();
                context.P_PROPERTY_APPLIANCE_DEFECTS.DeleteObject(fault);
                context.SaveChanges();

                trans.Complete();
                success = true;
            }

            return success;
        }
        //Change#52 - Behroz - 16/01/2013 - End
        #endregion

        #region "Save Fault Image"
        //Change#53 - Behroz - 17/01/2013 - Start
        /// <summary>
        /// This function saves the fault image data in database
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="faultId"></param>
        /// <returns></returns>
        public bool saveFaultImageData(string fileName,int faultId)
        { 
            bool success = false;
            using (TransactionScope trans = new TransactionScope())
            {
                P_PROPERTY_APPLIANCE_DEFECTS_IMAGES appDefectImage = new P_PROPERTY_APPLIANCE_DEFECTS_IMAGES()
                {
                    PropertyDefectId = faultId,
                    ImageTitle = fileName,
                    ImagePath = Utilities.Helpers.FileHelper.getPropertyImageUploadPath()
                };

                context.AddToP_PROPERTY_APPLIANCE_DEFECTS_IMAGES(appDefectImage);
                context.SaveChanges();
                trans.Complete();
                success = true;
            }

            return success;
        }
        //Change#53 - Behroz - 17/01/2013 - End
        #endregion

        #region Check Appointment

        public bool checkAppointmentID(int appointmentID)
        {
            var id = context.PS_Appointment.Where(app=> app.AppointId == appointmentID);
            if (id.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //Change#19 - Behroz - 19/12/2012 - Start
        public bool checkAppointmentIDGas(int journalId)
        {   
            var id = context.AS_APPOINTMENTS.Where(app => app.JournalId == journalId);
            if (id.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        //Change#19 - Behroz - 19/12/2012 - End
        #endregion

        #region "Validate Category Id"
        //Change#19 - Behroz - 19/12/2012 - Start
        public string validateFaultsCategories(int CatId)
        {
            var id = context.P_DEFECTS_CATEGORY.Where(cat => cat.CategoryId == CatId);

            if (id.Count() > 0)
                return id.First().Description;
            else
                return string.Empty;
        }
        //Change#19 - Behroz - 19/12/2012 - End

        //Change#53 - Behroz - 17/01/2013 - Start
        /// <summary>
        /// This function validates the fault id
        /// </summary>
        /// <param name="CatId"></param>
        /// <returns></returns>
        public bool validateFaultsId(int faultId)
        {
            var id = context.P_PROPERTY_APPLIANCE_DEFECTS.Where(fault => fault.PropertyDefectId == faultId);

            if (id.Count() > 0)
                return true;
            else
                return false;
        }
        //Change#53 - Behroz - 17/01/2013 - End
        #endregion

    }
}
