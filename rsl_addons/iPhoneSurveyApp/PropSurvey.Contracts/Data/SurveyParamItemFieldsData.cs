﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class SurveyParamItemFieldsData
    {
        [DataMember]
        public int surveyPramItemFieldId { get; set; }

        [DataMember]
        public string surveyParamItemFieldValue { get; set; }

        [DataMember]
        public DateTime? surveyParamItemDateValue { get; set; }

        [DataMember]
        public string isSelected { get; set; }

    }
}
