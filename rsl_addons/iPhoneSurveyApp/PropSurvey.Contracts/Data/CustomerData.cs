﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class CustomerData
    {

        [DataMember]
        public int? customerId { get; set; } // Line changed by Abdul Wahhab - 19/06/2013

        [DataMember]
        public string firstName { get; set; }

        [DataMember]
        public string middleName { get; set; }

        [DataMember]
        public string lastName { get; set; }

        [DataMember]
        public string title { get; set; }

        [DataMember]
        public string telephone { get; set; }

        [DataMember]
        public string mobile { get; set; }

        [DataMember]
        public string fax { get; set; }

        [DataMember]
        public string email { get; set; }

        [DataMember]
        public PropertyData property = new PropertyData();

        [DataMember]
        public List<CustomerRiskData> customerRiskData = new List<CustomerRiskData>();

        [DataMember]
        public CustomerVulnerabilityData customerVulnerabilityData = new CustomerVulnerabilityData();

        [DataMember]
        public CustomerAddressData Address = new CustomerAddressData();
    }


}
