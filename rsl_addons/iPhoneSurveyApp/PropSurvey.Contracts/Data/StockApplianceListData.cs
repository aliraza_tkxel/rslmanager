﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    //Class added by Abdul Wahhab - 24/05/2013
    public class StockApplianceListData
    {
        [DataMember]
        public List<ApplianceData> appliancesList = new List<ApplianceData>();
    }
}
