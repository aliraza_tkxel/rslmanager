﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class FaultRepairData
    {
        [DataMember]
        public int? FaultRepairID { get; set; }

        [DataMember]
        public string Description { get; set; }
    }
}
