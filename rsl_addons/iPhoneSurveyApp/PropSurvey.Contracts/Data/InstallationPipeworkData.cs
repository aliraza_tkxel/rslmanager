﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class InstallationPipeworkData
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public int AppointmentID { get; set; }

        [DataMember]
        public string EmergencyControl { get; set; }

        [DataMember]
        public string VisualInspection { get; set; }

        [DataMember]
        public string GasTightnessTest { get; set; }

        [DataMember]
        public string EquipotentialBonding { get; set; }

        [DataMember]
        public DateTime Date { get; set; }

        [DataMember]
        public bool? isInspected { get; set; }
       
        //Change#12 - Behroz - 12/13/2012 - Start
        [DataMember]
        public string PropertyId { get; set; }
        //Change#12 - Behroz - 12/13/2012 - End

        [DataMember]
        public int? JournalId { get; set; }

        public bool validatePipeworkOption(string option)
        {
            if (Enum.IsDefined(typeof(PipeworkOption), option.ToUpper()) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public enum PipeworkOption
        {
            YES,
            NO,
            NA
        }
    }
}
