﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class JobSheetNumberListData
    {
        [DataMember]
        public string JSNumber { get; set; }

        [DataMember]
        public string JSNDescription { get; set; }

        [DataMember]
        public string JSNNotes { get; set; }

        [DataMember]
        public double? Duration { get; set; }
    }
}
