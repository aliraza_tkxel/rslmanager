﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class FaultLogHistoryData
    {
        [DataMember]
        public int faultLogHistoryID { get; set; }

        [DataMember]
        public int journalID { get; set; }

        [DataMember]
        public int? faultStatusID { get; set; }

        [DataMember]
        public int? itemActionID { get; set; }

        [DataMember]
        public DateTime? lastActionDate { get; set; }

        [DataMember]
        public int? lastActionUserID { get; set; }

        [DataMember]
        public int? faultLogID { get; set; }

        [DataMember]
        public int? ORGID { get; set; }

        [DataMember]
        public int? scopeID { get; set; }

        [DataMember]
        public string title { get; set; }

        [DataMember]
        public string notes { get; set; }

        [DataMember]
        public string propertyID { get; set; }

        [DataMember]
        public short? contractorID { get; set; }
    }
}
