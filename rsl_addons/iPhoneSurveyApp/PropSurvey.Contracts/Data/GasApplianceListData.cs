﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    //Class added by Abdul Wahhab - 22/05/2013
    public class GasApplianceListData
    {
        [DataMember]
        public DetectorInfoData SmokeDetector = new DetectorInfoData();

        [DataMember]
        public DetectorInfoData Co2Detector = new DetectorInfoData();

        [DataMember]
        public List<ApplianceData> appliancesList = new List<ApplianceData>();
    }
}
