﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class CustomerAddressData
    {
        [DataMember]
        public int AddressID { get; set; }

        [DataMember]
        public string Address { get; set; }

        [DataMember]
        public string PostCode { get; set; }

        [DataMember]
        public string TownCity { get; set; }

        [DataMember]
        public string HouseNo { get; set; }

        
    }
}
