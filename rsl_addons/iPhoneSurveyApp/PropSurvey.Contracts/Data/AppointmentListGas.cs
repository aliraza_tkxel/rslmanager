﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    //Class added by Abdul Wahhab - 04/06/2013
    public class AppointmentListGas
    {
        [DataMember]
        public int appointmentId { get; set; }

        [DataMember]
        public int journalId { get; set; }

        [DataMember]
        public int? jsgNumber { get; set; }

        [DataMember]
        public int? tenancyId { get; set; } // type changed to int? from int by Abdul Wahhab - 18/06/2013

        [DataMember]
        public long journalHistoryId { get; set; }

        [DataMember]
        public DateTime? appointmentDate { get; set; }

        [DataMember]
        public String appointmentShift { get; set; }

        [DataMember]
        public String appointmentStartTime { get; set; }

        [DataMember]
        public String appointmentEndTime { get; set; }

        [DataMember]
        public int assignedTo { get; set; }

        [DataMember]
        public String surveyorUserName { get; set; }

        [DataMember]
        public DateTime? loggedDate { get; set; }

        [DataMember]
        public int? createdBy { get; set; }

        [DataMember]
        public String appointmentNotes { get; set; }

        [DataMember]
        public String surveyorAlert { get; set; }

        [DataMember]
        public String appointmentCalendar { get; set; }

        [DataMember]
        public String surveyourAvailability { get; set; }

        [DataMember]
        public String appointmentType { get; set; }

        [DataMember]
        public JournalData journal = new JournalData();

        [DataMember]
        public String appointmentStatus { get; set; }

        [DataMember]
        public String surveyType { get; set; }

        // Code added by Abdul Wahhab - 07/06/2013 - START

        [DataMember]
        public int defaultCustomerId { get; set; }

        [DataMember]
        public int defaultCustomerIndex { get; set; }

        // Code added by Abdul Wahhab - 07/06/2013 - END

        [DataMember]
        public List<CustomerData> customerList = new List<CustomerData>();

        [DataMember]
        public PropertyData property = new PropertyData();

        [DataMember]
        public AppointmentInfoData appInfoData = new AppointmentInfoData();

        [DataMember]
        public LGSRData CP12Info = new LGSRData();

        public bool validateAppointmentProgressStatus(string status)
        {
            if (Enum.IsDefined(typeof(AppointmentProgressStatus), status) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool validateSurveyourAvailabilityStatus(string status)
        {
            if (Enum.IsDefined(typeof(SurveyourAvailabilityStatus), status) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool validateAppointmentTypes(string status)
        {
            if (Enum.IsDefined(typeof(AppointmentTypes), status) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool validateSurveyTypes(string status)
        {
            if (Enum.IsDefined(typeof(SurveyTypes), status) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}