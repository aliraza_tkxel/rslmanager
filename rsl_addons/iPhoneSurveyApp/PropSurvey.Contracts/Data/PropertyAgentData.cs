﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class PropertyAgentData
    {
        [DataMember]
        public int ID { get; set;}

        [DataMember]
        public string AgentName{ get; set; }

        [DataMember]
        public string Address{ get; set; }

        [DataMember]
        public string PhoneNo{ get; set; }
    }
}
