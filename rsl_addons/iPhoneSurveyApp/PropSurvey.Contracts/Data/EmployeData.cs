﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class EmployeData
    {
        [DataMember]
        public int EmpID { get; set; }

        [DataMember]
        public string EmpName { get; set; }

        [DataMember]
        public string GSRENo { get; set; }

    }
}
