﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class SurveyData
    {
        [DataMember]
        public int surveyId { get; set; }

        //[DataMember]        
        //public AppointmentData appointment = new AppointmentData();

        [DataMember]
        public int appointmentId { get; set; }

        [DataMember]
        public int customerId { get; set; }        

        [DataMember]
        public string propertyId { get; set; }                

        [DataMember]
        public int completedBy { get; set; }

        [DataMember]
        public DateTime? surveyDate { get; set; }

        [DataMember]
        public List<SurveyFieldsData> surveyFields = new List<SurveyFieldsData>();

        [DataMember]
        public SurveyNotesData surveyNotes = new SurveyNotesData();        

        [DataMember]
        public int itemId { get; set; }
    }
}
