﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class IssuedReceivedByData
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public int AppointmentID { get; set; }

        [DataMember]
        public string CP12No { get; set; }

        [DataMember]
        public string IssuedBy { get; set; }

        [DataMember]
        public DateTime IssuedDate { get; set; }

        [DataMember]
        public string ReceivedOnBehalfOf { get; set; }

        [DataMember]
        public DateTime ReceivedDate { get; set; }


        public bool validateReceiver(string option)
        {
            if (Enum.IsDefined(typeof(Receiver), option.ToUpper()) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public enum Receiver
        {
            TENANT,
            LANDLORD,
            AGENT,
            HOMEOWNER
        }
    }
}
