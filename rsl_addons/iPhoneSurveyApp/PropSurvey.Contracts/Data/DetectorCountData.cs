﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class DetectorCountData
    {
        [DataMember]
        public int? detectorCount { get; set; }

        [DataMember]
        public int detectorType { get; set; }
    }
}
