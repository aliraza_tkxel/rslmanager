﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class PropertyDimData
    {
        [IgnoreDataMember]
        public int propertyDimId { get; set; }

        [DataMember]
        public string propertyId { get; set; }

        [DataMember]
        public string roomName { get; set; }

        [DataMember]
        public double? roomWidth { get; set; }

        [DataMember]
        public double? roomHeight { get; set; }
        
        [DataMember]
        public double? roomLength { get; set; }

        [DataMember]
        public DateTime? updatedOn { get; set; }

        [DataMember]
        public int updatedBy { get; set; }

        public PropertyDimData()
        {
            this.roomWidth = 3.2D;
            this.roomHeight = 3.2D;
            this.roomLength = 3.2D;
        }

        public PropertyDimData(string roomName)
        {
            this.roomWidth = 3.2D;
            this.roomHeight = 3.2D;
            this.roomLength = 3.2D;
            this.roomName = roomName;
        }

        public PropertyDimData getPropertyInitialValues(string roomName)
        {
            PropertyDimData propDimData = new PropertyDimData(roomName);
            return propDimData;

        }
    }
}
