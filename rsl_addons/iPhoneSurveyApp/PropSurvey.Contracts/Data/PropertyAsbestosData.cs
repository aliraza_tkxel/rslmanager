﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class PropertyAsbestosData
    {
        [DataMember]
        public int asbestosId { get; set; }

        [DataMember]
        public string riskDesc { get; set; }

        [DataMember]
        public string asbRiskLevelDesc { get; set; }        
    }
}
