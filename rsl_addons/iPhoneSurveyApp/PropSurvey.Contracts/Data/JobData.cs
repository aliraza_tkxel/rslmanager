﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class JobData
    {
        [DataMember]
        public string JSNumber { get; set; }

        [DataMember]
        public int FaultLogID { get; set; }

        [DataMember]
        public string JSNDescription { get; set; }

        [DataMember]
        public string JSNNotes { get; set; }

        [DataMember]
        public string JSNLocation { get; set; }

        [DataMember]
        public string RepairNotes { get; set; }

        [DataMember]
        public double? Duration { get; set; }

        //Code added by Abdul Wahhab - 04/06/2013 - START

        [DataMember]
        public DateTime? ReportedDate { get; set; }

        [DataMember]
        public string ResponseTime { get; set; }

        [DataMember]
        public string Priority { get; set; }

        //Code added by Abdul Wahhab - 04/06/2013 - END

        //Code added by Abdul Wahhab - 17/06/2013 - START
        [DataMember]
        public string jobStatus { get; set; }

        [DataMember]
        public DateTime? CompletionDate { get; set; }

        [DataMember]
        public string followOnNotes { get; set; }

        //Code added by Abdul Wahhab - 17/06/2013 - END

        [DataMember]
        public List<FaultRepairData> faultRepairDataList = new List<FaultRepairData>();
    }
}
