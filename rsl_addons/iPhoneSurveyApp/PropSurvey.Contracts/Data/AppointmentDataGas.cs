﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class AppointmentDataGas
    {
        [DataMember]
        public int appointmentId { get; set; }

        [DataMember]
        public int journalId { get; set; }

        [DataMember]
        //public String jsgNumber { get; set; }
        public int? jsgNumber { get; set; }

        [DataMember]
        public int? tenancyId { get; set; }

        [DataMember]
        public long journalHistoryId { get; set; }

        [DataMember]
        public DateTime? appointmentDate { get; set; }

        [DataMember]
        public String appointmentShift { get; set; }

        [DataMember]
        public String appointmentStartTime { get; set; }

        [DataMember]
        public String appointmentEndTime { get; set; }

        [DataMember]
        public int assignedTo { get; set; }

        [DataMember]
        public String surveyorUserName { get; set; }

        [DataMember]
        public DateTime? loggedDate { get; set; }

        [DataMember]
        public int? createdBy { get; set; }

        [DataMember]
        public String appointmentNotes { get; set; }

        [DataMember]
        public String surveyorAlert { get; set; }

        [DataMember]
        public String appointmentCalendar { get; set; }

        [DataMember]
        public String surveyourAvailability { get; set; }

        [DataMember]
        public String appointmentType { get; set; }

        [DataMember]
        public JournalData journal = new JournalData();

        [DataMember]
        public String appointmentStatus { get; set; }

        [DataMember]
        public String surveyType { get; set; }

        [DataMember]
        public CustomerData customer = new CustomerData();

        [DataMember]
        public AppointmentInfoData appInfoData = new AppointmentInfoData();

        [DataMember]
        //public IssuedReceivedByData CP12Info = new IssuedReceivedByData();
        public LGSRData CP12Info = new LGSRData();

        public bool validateAppointmentProgressStatus(string status)
        {
            if (Enum.IsDefined(typeof(AppointmentProgressStatus), status) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool validateSurveyourAvailabilityStatus(string status)
        {
            if (Enum.IsDefined(typeof(SurveyourAvailabilityStatus), status) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool validateAppointmentTypes(string status)
        {
            if (Enum.IsDefined(typeof(AppointmentTypes), status) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool validateSurveyTypes(string status)
        {
            if (Enum.IsDefined(typeof(SurveyTypes), status) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
