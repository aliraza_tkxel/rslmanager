﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    public class MessageData
    {
        [DataMember]
        public string message = string.Empty;

    }
}
