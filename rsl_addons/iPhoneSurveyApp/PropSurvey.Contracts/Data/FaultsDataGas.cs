﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class FaultsDataGas
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string PropertyId { get; set; }

        [DataMember]
        public int FaultCategory { get; set; }

        [DataMember]
        public int? JournalId { get; set; }

        [DataMember]
        public bool? IsDefectIdentified { get; set; }

        [DataMember]
        public string DefectDesc { get; set; }

        [DataMember]
        public bool? IsActionTaken { get; set; }

        [DataMember]
        public string RemedialAction { get; set; }

        [DataMember]
        public bool? isAdviceNoteIssued { get; set; }

        [DataMember]
        public int? ApplianceID { get; set; }

        [DataMember]
        public string SerialNo { get; set; }

        [DataMember]
        public bool? WarningTagFixed { get; set; }

        [DataMember]
        public DateTime? DefectDate { get; set; }
    }
}
