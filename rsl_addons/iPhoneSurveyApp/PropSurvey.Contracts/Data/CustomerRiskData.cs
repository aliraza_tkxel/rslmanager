﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Collections;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class CustomerRiskData
    {
        [DataMember]
        public string riskCatDesc { get; set; }

        [DataMember]
        public string riskSubCatDesc { get; set; }        
    }
}
