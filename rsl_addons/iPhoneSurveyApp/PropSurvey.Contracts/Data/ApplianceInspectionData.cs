﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class ApplianceInspectionData
    {
        [DataMember]
        public int APPLIANCEINSPECTIONID { get; set; }

        [DataMember]
        public int APPLIANCEID { get; set; }

        [DataMember]
        public int APPOINTMENTID { get; set; }

        [DataMember]
        public bool ISINSPECTED { get; set; }

        [DataMember]
        public string COMBUSTIONREADING { get; set; }

        [DataMember]
        public string OPERATINGPRESSURE { get; set; }

        [DataMember]
        public string SAFETYDEVICEOPERATIONAL { get; set; }

        [DataMember]
        public string SPILLAGETEST { get; set; }

        [DataMember]
        public string SMOKEPELLET { get; set; }

        [DataMember]
        public string ADEQUATEVENTILATION { get; set; }

        [DataMember]
        public string FLUEVISUALCONDITION { get; set; }

        [DataMember]
        public string SATISFACTORYTERMINATION { get; set; }

        [DataMember]
        public string FLUEPERFORMANCECHECKS { get; set; }

        [DataMember]
        public string APPLIANCESERVICED { get; set; }

        [DataMember]
        public string APPLIANCESAFETOUSE { get; set; }

        [DataMember]
        public DateTime? INSPECTIONDATE { get; set; }

        //Change#24 - Behroz - 20/12/2012 - Start
        //[DataMember]
        //public int PROPERTYAPPLIANCEID { get; set; }

        [DataMember]
        public int INSPECTEDBY { get; set; }

        [DataMember]
        public int? JOURNALID { get; set; }
        //Change#24 - Behroz - 20/12/2012 - End


        public bool validateInspectionOptions(string Option)
        {
            if (Enum.IsDefined(typeof(InspectionOptions), Option.ToUpper()) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public enum InspectionOptions
        {
            FAIL,
            PASS,
            YES,
            NO,
            NA
            
        }
    }
}
