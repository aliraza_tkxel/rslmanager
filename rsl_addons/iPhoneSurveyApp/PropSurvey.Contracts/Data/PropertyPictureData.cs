﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class PropertyPictureData
    {
        [DataMember]
        public int propertyPictureId { get; set; }

        [DataMember]
        public int appointmentId { get; set; }

        [DataMember]
        public string propertyPictureName { get; set; }

        [DataMember]
        public int surveyId { get; set; }

        [DataMember]
        public bool? isDefault { get; set; }

        [DataMember]
        public string propertyId { get; set; }

        [DataMember]
        public int itemId { get; set; }

        [DataMember]
        public DateTime? createdOn { get; set; }

        [DataMember]
        public int createdBy { get; set; }

        public string imagePath { get; set; }

    }
}
