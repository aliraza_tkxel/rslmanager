﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class SurveyNotesData
    {        
        public int surveyNotesId { get; set; }

        [DataMember]
        public string surveyNotesDetail { get; set; }

        [DataMember]
        public DateTime? surveyNotesDate { get; set; }
        
    }
}
