﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class LGSRData
    {
        [DataMember]
        public int? LGSRID { get; set; }

        [DataMember]
        public string PropertyID { get; set; }

        [DataMember]
        public string CP12Number { get; set; }

        // Code added by Abdul Wahhab 15/05/2013 - START
        [DataMember]
        public int? jsgNumber { get; set; }
        // Code added by Abdul Wahhab 15/05/2013 - END

        [DataMember]
        public int? IssuedBy { get; set; }

        [DataMember]
        public DateTime? IssuedDate { get; set; }

        // Code added by Abdul Wahhab 07/06/2013 - START
        [DataMember]
        public string IssuedDateString { get; set; }
        // Code added by Abdul Wahhab 07/06/2013 - END

        [DataMember]
        public DateTime? DTimeStamp { get; set; }

        [DataMember]
        public string DocumentType { get; set; }

        [DataMember]
        public string CP12Document { get; set; }

        [DataMember]
        public string Notes { get; set; }

        [DataMember]
        public string ReceivedOnBehalfOf { get; set; }

        [DataMember]
        public DateTime? ReceivedDate { get; set; }

        // Code added by Abdul Wahhab 07/06/2013 - START
        [DataMember]
        public string ReceivedDateString { get; set; }
        // Code added by Abdul Wahhab 07/06/2013 - END

        [DataMember]
        public bool? TenantHanded { get; set; }

        [DataMember]
        public bool? InspectionCarried { get; set; }

        [DataMember]
        public int? JournalId { get; set; }

        public bool validateReceiver(string option)
        {
            if (Enum.IsDefined(typeof(Receiver), option.ToUpper()) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public enum Receiver
        {
            TENANT,
            LANDLORD,
            AGENT,
            HOMEOWNER
        }
    }

}
