﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    //Class added by Abdul Wahhab - 22/05/2013
    public class DetectorInspectionData
    {
        [DataMember]
        public int DetectorId { get; set; }

        [DataMember]
        public string propertyId { get; set; }

        [DataMember]
        public string DetectorTest { get; set; }

        [DataMember]
        public DateTime? DateStamp { get; set; }

        [DataMember]
        public int? journalId { get; set; }

        [DataMember]
        public bool? IsInspected { get; set; }
    }
}
