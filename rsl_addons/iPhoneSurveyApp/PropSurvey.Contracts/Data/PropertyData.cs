﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class PropertyData
    {
        [DataMember]
        public string propertyId { get; set; }

        [DataMember]
        public int? tenancyId { get; set; } // type changed to int? from int by Abdul Wahhab - 18/06/2013

        [DataMember]
        public string houseNumber { get; set; }

        [DataMember]
        public string flatNumber { get; set; }

        [DataMember]
        public string address1 { get; set; }

        [DataMember]
        public string address2 { get; set; }

        [DataMember]
        public string address3 { get; set; }

        [DataMember]
        public string townCity { get; set; }

        [DataMember]
        public string postCode { get; set; }

        [DataMember]
        public string county { get; set; }

        [DataMember]
        public DateTime? certificateExpiry { get; set; }

        [DataMember]
        public DateTime? lastSurveyDate { get; set; }

        [DataMember]
        public PropertyPictureData propertyPicture = new PropertyPictureData();

        [DataMember]
        public List<PropertyAsbestosData> propertyAsbestosData = new List<PropertyAsbestosData>();
    }
}
