﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class ApplianceTypeData
    {
        [DataMember]
        public int? ApplianceTypeID { get; set; }

        [DataMember]
        public string ApplianceType { get; set; }
    }
}
