﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class ApplianceModelData
    {
        [DataMember]
        public int? ApplianceModelID { get; set; }

        [DataMember]
        public string ApplianceModel { get; set; }
    }
}
