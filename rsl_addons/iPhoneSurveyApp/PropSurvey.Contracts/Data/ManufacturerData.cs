﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class ManufacturerData
    {
        [DataMember]
        public int? ManufacturerID { get; set; }

        [DataMember]
        public string Manufacturer { get; set; }
    }
}