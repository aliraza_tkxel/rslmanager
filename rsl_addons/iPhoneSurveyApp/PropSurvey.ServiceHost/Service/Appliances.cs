﻿using System;
using System.Collections.Generic;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PropSurvey.BuisnessLayer.Appliances;
using PropSurvey.Contracts.Data;
using PropSurvey.Contracts.Fault;
using PropSurvey.ServiceHost.IService;
using PropSurvey.Utilities.Constants;
using PropSurvey.Utilities.ErrorSetter;
using PropSurvey.BuisnessLayer.Authentication;
using PropSurvey.Utilities.Helpers;

namespace PropSurvey.ServiceHost.Service
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class Appliances : IAppliances
    {
        #region get All Appliances
        /// <summary>
        /// This function returns all the appointments
        /// </summary>
        /// <returns>List of appointment data objects</returns>
        /// 
        public StockApplianceListData getAllAppliances(string propertyID, int appointmentID, string userName, string salt)
        {
            // Return type changed by Abdul Wahhab - 24/05/2013
            try
            {
                StockApplianceListData apptData = new StockApplianceListData();
                AuthenticationBl authBl = new AuthenticationBl();
                if (authBl.checkForValidSession(userName, salt))
                {
                    AppliancesBl apptBl = new AppliancesBl();
                    apptData.appliancesList = apptBl.getAllAppliancesByPropertyID(propertyID, appointmentID);
                }
                else
                {
                    apptData.appliancesList.Add(returnInValidAppliance());
                }

                return apptData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }

        //Change#31 - Behroz - 12/07/2012 - Start
        //Code modified by Abdul Wahhab - 22/05/2013 - START
        /// <summary>
        /// This function returns all the appointments for GAS
        /// </summary>
        /// <returns>List of appointment data objects</returns>
        /// 
        public GasApplianceListData getAllAppliancesGas(string propertyID, int jounalId, string userName, string salt)
        {
            try
            {
                GasApplianceListData apptData = new GasApplianceListData();
                AuthenticationBl authBl = new AuthenticationBl();
                if (authBl.checkForValidSession(userName, salt))
                {
                    AppliancesBl apptBl = new AppliancesBl();
                    apptData = apptBl.getAllAppliancesByPropertyIDGas(propertyID, jounalId);
                }
                else
                {
                    apptData.appliancesList.Add(returnInValidAppliance());
                }

                return apptData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }
        //Code modified by Abdul Wahhab - 22/05/2013 - END
        //Change#31 - Behroz - 12/07/2012 - End

        #endregion

        #region get All Appliances Types
        /// <summary>
        /// This function returns all the Appliances Types
        /// </summary>
        /// <returns>List of Appliances Types data objects</returns>
        public List<ApplianceTypeData> getAllApplianceTypes(string userName, string salt)
        {
            try
            {
                List<ApplianceTypeData> apptData = new List<ApplianceTypeData>();
                AuthenticationBl authBl = new AuthenticationBl();
                if (authBl.checkForValidSession(userName, salt))
                {
                    AppliancesBl apptBl = new AppliancesBl();
                    apptData = apptBl.getAllApplianceTypes();
                }
                else
                {
                    apptData.Add(returnInValidType());
                }

                return apptData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }

        #endregion

        #region get All Appliances Model

        /// <summary>
        /// This function returns all the Appliances Model
        /// </summary>
        /// <returns>List of Appliances Model data objects</returns>
        public List<ApplianceModelData> getAllApplianceModel(string userName, string salt)
        {
            try
            {
                List<ApplianceModelData> apptData = new List<ApplianceModelData>();
                AuthenticationBl authBl = new AuthenticationBl();
                if (authBl.checkForValidSession(userName, salt))
                {
                    AppliancesBl apptBl = new AppliancesBl();
                    apptData = apptBl.getAllApplianceModel();
                }
                else
                {
                    apptData.Add(returnInValidModel());
                }

                return apptData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }
        #endregion

        #region get All Appliances location

        /// <summary>
        /// This function returns all the Appliances location
        /// </summary>
        /// <returns>List of Appliances location data objects</returns>
        public List<AppliancesLocationData> getAllApplianceLocation(string userName, string salt)
        {
            try
            {
                List<AppliancesLocationData> apptData = new List<AppliancesLocationData>();
                AuthenticationBl authBl = new AuthenticationBl();
                if (authBl.checkForValidSession(userName, salt))
                {
                    AppliancesBl apptBl = new AppliancesBl();
                    apptData = apptBl.getAllApplianceLocations();
                }
                else
                {
                    apptData.Add(returnInValidLocation());
                }

                return apptData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }
        #endregion

        #region get All Appliances Manufacturer
        /// <summary>
        /// This function returns all the Appliances Manufacturer
        /// </summary>
        /// <returns>List of Appliances Manufacturer data objects</returns>
        public List<ManufacturerData> getAllApplianceManufacturer(string userName, string salt)
        {
            try
            {
                List<ManufacturerData> apptData = new List<ManufacturerData>();
                AuthenticationBl authBl = new AuthenticationBl();
                if (authBl.checkForValidSession(userName, salt))
                {
                    AppliancesBl apptBl = new AppliancesBl();
                    apptData = apptBl.getAllApplianceManufacturer();
                }
                else
                {
                    apptData.Add(returnInValidManufacturer());
                }

                return apptData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }
        #endregion

        #region save Appliance
        /// <summary>
        /// This function saves the Appliance in the database
        /// </summary>
        /// <param name="appData">The object of Appliance</param>
        /// <returns>the Appliance id if successfully save otherwise 0 </returns>
        public int saveAppliance(ApplianceData appData, string userName, string salt)
        {
            try
            {
                AppliancesBl apptBl = new AppliancesBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                return apptBl.saveAppliance(appData);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                //throw new FaultException<MessageFault>(msgFault, new FaultReason(msgFault.message));
                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }
        #endregion

        #region update Appliance
        /// <summary>
        /// This function updates the Appliance in the database
        /// </summary>
        /// <param name="appData">The object of Appliance</param>
        /// <returns>the Appliance id if successfully save otherwise 0 </returns>
        public bool updateAppliance(ApplianceData appData, string userName, string salt)
        {
            try
            {
                AppliancesBl apptBl = new AppliancesBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                return apptBl.updateAppliance(appData);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }
        #endregion

        #region save Appliance Type
        /// <summary>
        /// This function saves the Appliance Type in the database
        /// </summary>
        /// <param name="appData">The object of Appliance Type</param>
        /// <returns>the Appliance id if successfully save otherwise 0 </returns>
        public int saveApplianceType(ApplianceTypeData typeData, string userName, string salt)
        {
            try
            {
                AppliancesBl apptBl = new AppliancesBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                return apptBl.saveApplianceType(typeData);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }

        #endregion

        #region save Appliance Model
        /// <summary>
        /// This function saves the Appliance Model in the database
        /// </summary>
        /// <param name="appData">The object of Appliance Model</param>
        /// <returns>the Appliance id if successfully save otherwise 0 </returns>
        public int saveApplianceModel(ApplianceModelData modelData, string userName, string salt)
        {
            try
            {
                AppliancesBl apptBl = new AppliancesBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                return apptBl.saveApplianceModel(modelData);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }

        #endregion

        #region save Appliance Location
        /// <summary>
        /// This function saves the Appliance Location in the database
        /// </summary>
        /// <param name="appData">The object of Appliance Location</param>
        /// <returns>the Appliance id if successfully save otherwise 0 </returns>
        public int saveApplianceLocation(AppliancesLocationData locationData, string userName, string salt)
        {
            try
            {
                AppliancesBl apptBl = new AppliancesBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                return apptBl.saveApplianceLocation(locationData);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }

        #endregion

        #region save Appliance Manufacturer
        /// <summary>
        /// This function saves the Appliance Manufacturer in the database
        /// </summary>
        /// <param name="appData">The object of Appliance Manufacturer</param>
        /// <returns>the Appliance id if successfully save otherwise 0 </returns>
        public int saveApplianceManufacturer(ManufacturerData manufactureData, string userName, string salt)
        {
            try
            {
                AppliancesBl apptBl = new AppliancesBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                return apptBl.saveApplianceManufacturer(manufactureData);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }

        #endregion

        #region get number of inspected Appliances

        /// <summary>
        /// This function returns number of inspected Appliances
        /// </summary>
        /// <returns>number of inspected Appliances</returns>
        public int getInspectedAppliances(int appointmentID, string userName, string salt)
        {
            try
            {
                AppliancesBl apptBl = new AppliancesBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                return apptBl.getInspectedAppliances(appointmentID);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }

        //Change#25 - Behroz - 20/12/2012 - Start
        /// <summary>
        /// This function returns number of inspected Appliances for gas
        /// </summary>
        /// <returns>number of inspected Appliances</returns>
        public int getInspectedAppliancesGas(int journalId, string userName, string salt)
        {
            try
            {
                AppliancesBl apptBl = new AppliancesBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                return apptBl.getInspectedAppliancesGas(journalId);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }
        //Change#25 - Behroz - 20/12/2012 - End

        #endregion

        #region get ApplianceInspection Details by appointmentId
        /// <summary>
        /// This function returns all the ApplianceInspection
        /// </summary>
        /// <returns>List of ApplianceInspection data objects</returns>

        public List<ApplianceInspectionData> getApplianceInspectionDetails(int appointmentID, string userName, string salt)
        {
            try
            {
                AppliancesBl apptBl = new AppliancesBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                return apptBl.getApplianceInspectionDetails(appointmentID);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }

        //Change#40 - Behroz - 26/12/2012 - Start
        /// <summary>
        /// This function returns all the ApplianceInspection for gas
        /// </summary>
        /// <returns>List of ApplianceInspection data objects</returns>
        public List<ApplianceInspectionData> getApplianceInspectionDetailsGas(int journalId, string userName, string salt)
        {
            try
            {
                AppliancesBl apptBl = new AppliancesBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                return apptBl.getApplianceInspectionDetailsGas(journalId);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }
        //Change#40 - Behroz - 26/12/2012 - End
        #endregion

        #region get ApplianceInspection by ApplianceID
        /// <summary>
        /// This function returns all the ApplianceInspection
        /// </summary>
        /// <returns>List of ApplianceInspection data objects</returns>
        public ApplianceInspectionData getApplianceInspectionByApplianceID(int ApplianceID, int appointmentID, string userName, string salt)
        {
            try
            {
                AppliancesBl apptBl = new AppliancesBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                return apptBl.getApplianceInspectionByApplianceID(ApplianceID, appointmentID);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }

        //Change#26 - Behroz - 20/12/2012 - Start
        /// <summary>
        /// This function returns all the ApplianceInspection for gas
        /// </summary>
        /// <returns>List of ApplianceInspection data objects</returns>
        public ApplianceInspectionData getApplianceInspectionByApplianceIDGas(int ApplianceID, int journalId, string userName, string salt)
        {
            try
            {
                AppliancesBl apptBl = new AppliancesBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                return apptBl.getApplianceInspectionByApplianceIDGas(ApplianceID, journalId);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }
        //Change#26 - Behroz - 20/12/2012 - End
        #endregion

        #region Save a new ApplianceInspection
        /// <summary>
        /// This function saves a new ApplianceInspection
        /// </summary>
        /// <param name="appData">ApplianceInspection data objects</param>
        /// <returns>ID in case of success</returns>
        public int saveApplianceInspection(ApplianceInspectionData appData, string userName, string salt)
        {
            try
            {
                AppliancesBl apptBl = new AppliancesBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                validateApplianceInspection(appData);
                return apptBl.saveApplianceInspection(appData);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }

        //Change#24 - Behroz - 20/12/2012 - Start
        /// <summary>
        /// This function saves a new ApplianceInspection for gas
        /// </summary>
        /// <param name="appData">ApplianceInspection data objects</param>
        /// <returns>ID in case of success</returns>
        public int saveApplianceInspectionGas(ApplianceInspectionData appData, string userName, string salt)
        {
            try
            {
                AppliancesBl apptBl = new AppliancesBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                validateApplianceInspection(appData);
                return apptBl.saveApplianceInspectionGas(appData);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }
        //Change#24 - Behroz - 20/12/2012 - End
        #endregion

        #region Update a existing ApplianceInspection
        /// <summary>
        /// This function updates a existing ApplianceInspection
        /// </summary>
        /// <param name="appData">ApplianceInspection data objects</param>
        /// <returns>ID in case of success</returns>
        public bool updateApplianceInspection(ApplianceInspectionData appData, string userName, string salt)
        {
            try
            {
                AppliancesBl apptBl = new AppliancesBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                validateApplianceInspection(appData);
                return apptBl.updateApplianceInspection(appData);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }
        #endregion

        #region "Get Appliance Count"

        //Change#50 - Behroz - 16/01/2013 - Start
        /// <summary>
        /// This function return the applicance count
        /// </summary>
        /// <param name="propertyId"></param>
        /// <returns></returns>
        public int getApplianceCount(string propertyId, string userName, string salt)
        {
            try
            {
                AppliancesBl apptBl = new AppliancesBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);

                return apptBl.getApplianceCount(propertyId);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }
        //Change#50 - Behroz - 16/01/2013 - End
        #endregion

        #region Validate ApplianceInspection Data
        private void validateApplianceInspection(ApplianceInspectionData appData)
        {
            ApplianceInspectionData appInsData = new ApplianceInspectionData();
            if (!(appInsData.validateInspectionOptions(appData.ADEQUATEVENTILATION) && appInsData.validateInspectionOptions(appData.APPLIANCESAFETOUSE) && appInsData.validateInspectionOptions(appData.APPLIANCESERVICED)
                && appInsData.validateInspectionOptions(appData.FLUEPERFORMANCECHECKS) && appInsData.validateInspectionOptions(appData.FLUEVISUALCONDITION)
                && appInsData.validateInspectionOptions(appData.SAFETYDEVICEOPERATIONAL) && appInsData.validateInspectionOptions(appData.SATISFACTORYTERMINATION)
                && appInsData.validateInspectionOptions(appData.SMOKEPELLET) && appInsData.validateInspectionOptions(appData.SPILLAGETEST)))
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.AppInspectionValueInvalid, true, ErrorCodesConstants.AppInspectionValueInvalid);
                throw new ArgumentException(MessageConstants.AppInspectionValueInvalid);
            }
        }
        #endregion

        #region Invalid Type, Appliance, Location, Model and Manufacturer

        public ApplianceData returnInValidAppliance()
        {
            try
            {
                ApplianceData apptData = new ApplianceData();

                apptData.ApplianceID = -1;
                return apptData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);

            }
        }

        public AppliancesLocationData returnInValidLocation()
        {
            try
            {
                AppliancesLocationData apptData = new AppliancesLocationData();

                apptData.LocationID = -1;
                return apptData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);

            }
        }

        public ApplianceModelData returnInValidModel()
        {
            try
            {
                ApplianceModelData apptData = new ApplianceModelData();

                apptData.ApplianceModelID = -1;
                return apptData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);

            }
        }

        public ApplianceTypeData returnInValidType()
        {
            try
            {
                ApplianceTypeData apptData = new ApplianceTypeData();

                apptData.ApplianceTypeID = -1;
                return apptData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);

            }
        }

        public ManufacturerData returnInValidManufacturer()
        {
            try
            {
                ManufacturerData apptData = new ManufacturerData();

                apptData.ManufacturerID = -1;
                return apptData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);

            }
        }

        #endregion

        #region Update Detector Count

        //Code added by Abdul Wahhab - 23/05/2013 - START
        /// <summary>
        /// This function updates detector count against a property
        /// </summary>
        /// <param name="propertyId"></param>
        /// <param name="detectorCountData">DetectorCountData object</param>
        /// <returns>True in case of success</returns>
        public ResultBoolData updateDetectorCount(DetectorCountData detectorCountData, string propertyId, string userName, string salt)
        {
            try
            {
                AppliancesBl apptBl = new AppliancesBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);

                return apptBl.updateDetectorCount(propertyId, detectorCountData);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }
        //Code added by Abdul Wahhab - 23/05/2013 - END
        #endregion

        #region Add Update Detector Info

        //Code added by Abdul Wahhab - 23/05/2013 - START
        /// <summary>
        /// This function adds or updates detector info
        /// </summary>
        /// <param name="detectorType"></param>
        /// <param name="detectorInspectionData">DetectorInspectionData object</param>
        /// <returns>ID in case of success</returns>
        public ResultIntData addUpdateDetectorInfo(DetectorInspectionData detectorInspectionData, int detectorType, string userName, string salt)
        {
            try
            {
                AppliancesBl apptBl = new AppliancesBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);

                return apptBl.addUpdateDetectorInfo(detectorType, detectorInspectionData);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }
        //Code added by Abdul Wahhab - 23/05/2013 - END
        #endregion

    }
}