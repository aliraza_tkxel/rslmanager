﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel.Activation;
using System.ServiceModel;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PropSurvey.Contracts.Fault;
using System.ServiceModel.Web;
using System.Net;
using PropSurvey.BuisnessLayer.Push;
using PropSurvey.ServiceHost.IService;
using PropSurvey.Utilities.Helpers;
using PropSurvey.Utilities.Constants;

namespace PropSurvey.ServiceHost.Service
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class PushService : IPush
    {

        #region send Push Notification for Fault Appointment

        /// <summary>
        /// This function will send push notification for fault appointment
        /// </summary>
        /// <returns>Success or failure</returns>
        public bool sendPushNotificationFault(int appointmentId, int type)
        {
            try
            {
                string host = OperationContext.Current.IncomingMessageHeaders.To.Host.ToString();

                string serverinUse = String.Empty;

                if (host.StartsWith(ApplicationConstants.BHG_Live_Name))
                    serverinUse = ApplicationConstants.BHG_Live_Name;
                else if (host.StartsWith(ApplicationConstants.BHG_Test_Name))
                    serverinUse = ApplicationConstants.BHG_Test_Name;
                else
                    serverinUse = ApplicationConstants.BHG_Dev_Name;
                
                PushNotificationBl pushNotificationBl = new PushNotificationBl();
                return pushNotificationBl.sendPushNotificationFault(appointmentId, type, serverinUse);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }
        #endregion

    }
}