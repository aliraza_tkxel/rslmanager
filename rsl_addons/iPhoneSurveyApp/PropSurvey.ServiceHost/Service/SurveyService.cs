﻿using System;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using PropSurvey.BuisnessLayer.Survey;
using PropSurvey.Contracts.Data;
using PropSurvey.Contracts.Fault;
using PropSurvey.ServiceHost.IService;
using System.Collections.Generic;
using PropSurvey.BuisnessLayer.Authentication;
using PropSurvey.Utilities.Helpers;

namespace PropSurvey.ServiceHost.Service
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class SurveyService:ISurvey
    {
        #region save Survey Form

        /// <summary>
        /// This function save the survey form. Data should be post at specific url. 
        /// </summary>
        /// <param name="survData">survey data object</param>
        /// <returns>it returns true false</returns>

        public bool saveSurveyForm(SurveyData survData, string userName, string salt)
        {
            try
            {
                //string abc = AppointmentProgressStatus.InProgress.ToString();
                var dump = ObjectDumper.Dump(survData);
                Logger.Write(dump);

                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                SurveyBl survBl = new SurveyBl();
                return survBl.saveSurveyForm(survData);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }
        #endregion

        #region get Saved Survey Form
        /// <summary>
        /// this funciton returns the saved survey form against appointment id and property portion name
        /// </summary>
        /// <param name="appointmentId">appointment id</param>
        /// <param name="propertyPortionName">property portion name is the semicolon separated portions of name e.g Internal;Rooof;Dwelling</param>
        /// <returns>it returns the saved data of property survey form</returns>
        
        public SurveyData getSavedSurveyForm(int appointmentId, string propertyPortionName)
        {
            try
            {
                SurveyBl survBl = new SurveyBl();
                return survBl.getSavedSurveyForm(appointmentId, propertyPortionName);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }
        #endregion

        #region get All Saved Survey Forms
        /// <summary>
        /// this funciton returns all the saved survey forms against appointment id
        /// </summary>
        /// <param name="appointmentId">appointment id</param>        
        /// <returns>it returns the list of saved forms data of property survey form</returns>

        public List<SurveyData> getAllSavedSurveyForms(int appointmentId)
        {
            try
            {
                SurveyBl survBl = new SurveyBl();
                return survBl.getAllSavedSurveyForms(appointmentId);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }
        #endregion

        ///123456///
        #region get Surveyor Survey Forms
        /// <summary>
        /// this funciton returns all the survey forms against a surveyor username
        /// </summary>
        /// <param name="userName">surveyor username</param>        
        /// <returns>it returns the list of saved forms data of property survey form against a surveyor</returns>

        public List<SurveyData> getSurveyorSurveyForms(string userName)
        {
            try
            {
                SurveyBl survBl = new SurveyBl();
                return survBl.getSurveyorSurveyForms(userName);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }
        #endregion
    }
}
