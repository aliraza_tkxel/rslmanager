﻿using System;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PropSurvey.BuisnessLayer.Customer;
using PropSurvey.Contracts.Data;
using PropSurvey.Contracts.Fault;
using PropSurvey.ServiceHost.IService;
using PropSurvey.Utilities.Constants;
using PropSurvey.Utilities.ErrorSetter;
using System.Collections.Generic;
using System.IO;
using PropSurvey.Utilities.Helpers;

namespace PropSurvey.ServiceHost.Service
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class CustomerService:ICustomer
    {
        #region get Customer Info
        /// <summary>
        /// This function returns the customer record 
        /// </summary>
        /// <param name="customerId">customer id </param>
        /// <returns>It returns the customer data object</returns>
        public CustomerData getCustomerInfo(string customerId) 
        {
            try
            {
                CustomerBl custBl = new CustomerBl();
                CustomerData custData = new CustomerData();
                int custId = 0;
                try
                {
                    custId = Int32.Parse(customerId.ToString());
                }
                catch (FormatException formatException)
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.CustomerIdInvalidFromtMsg, true, ErrorCodesConstants.IntegerToStringConversionExceptionCode);
                    throw formatException;
                }
                
                custData = custBl.getCustomerInformation(custId);

                //getAsbestosRisk();

                return custData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }
        #endregion 

        #region "get Customer Vulnerabilities"
        /// <summary>
        /// This function returns the list of vulnerabilities of the customer.
        /// </summary>
        /// <param name="propertyId">customer id</param>
        /// <returns>the object of vulnerabilities of the customer</returns>
        
        public CustomerVulnerabilityData getCustomerVulnerabilities(string customerId)
        {
            try
            {
                CustomerBl custBl = new CustomerBl();

                int custId = 0;
                try
                {
                    custId = Int32.Parse(customerId.ToString());
                }
                catch (FormatException formatException)
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.CustomerIdInvalidFromtMsg, true, ErrorCodesConstants.IntegerToStringConversionExceptionCode);
                    throw formatException;
                }

                return custBl.getCustomerVulnerabilities(custId);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }
        #endregion

        // Region added by Abdul Wahhab 30/04/2013 - START

        #region get Employee Signature Image

        /// <summary>
        /// This function returns the signature image which was saved against employee
        /// </summary>
        /// <param name="employeeId">employee id</param>
        /// <returns>it returns the stream of image (if found)</returns>

        public Stream getEmployeeSignatureImage(String employeeId)
        {
            try
            {
                CustomerBl custBl = new CustomerBl();

                int empId = 0;
                try
                {
                    empId = Int32.Parse(employeeId.ToString());
                }
                catch (FormatException formatException)
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.EmployeeIdInvalidFromtMsg, true, ErrorCodesConstants.IntegerToStringConversionExceptionCode);
                    throw formatException;
                }

                String imageName = custBl.getEmployeeSignatureImageName(empId);

                if (!imageName.Equals(String.Empty))
                {
                    string signatureImagePath = FileHelper.getSignatureImagePath();
                    string imageType = FileHelper.getPropertyImageType();
                    string imageFullPath = FileHelper.makeSignatureImageFullPath(signatureImagePath, imageName);

                    try
                    {
                        Stream imageStream = File.OpenRead(imageFullPath);
                        WebOperationContext.Current.OutgoingResponse.ContentType = imageType;

                        return imageStream;
                    }
                    catch (FileNotFoundException fileNotFoundException)
                    {
                        ErrorFaultSetGet.setErrorFault(MessageConstants.SignatureImageNotFoundMsg, true, ErrorCodesConstants.FileNotFoundExceptionCode);
                        throw fileNotFoundException;
                    }
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.SignatureImageNotFoundMsg, true, ErrorCodesConstants.DatabaseEntryNotFoundCode);
                    throw new FileNotFoundException();
                }
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }
        #endregion

        // Region added by Abdul Wahhab 30/04/2013 - END

    }
}
