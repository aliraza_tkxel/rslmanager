﻿using System;
using System.Collections.Generic;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PropSurvey.BuisnessLayer.InstallationPipework;
using PropSurvey.Contracts.Data;
using PropSurvey.Contracts.Fault;
using PropSurvey.ServiceHost.IService;
using PropSurvey.Utilities.Constants;
using PropSurvey.Utilities.ErrorSetter;
using PropSurvey.BuisnessLayer.Authentication;
using PropSurvey.Utilities.Helpers;

namespace PropSurvey.ServiceHost.Service
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class InstallationPipework : IInstallationPipework
    {

        #region get InstallationPipework Data
        /// <summary>
        /// This function returns the InstallationPipework data
        /// </summary>
        /// <returns>List of InstallationPipework data objects</returns>
        public InstallationPipeworkData getInstallationPipeworkFormData(int AppointmentID, string userName, string salt)
        {
            try
            {
                InstallationPipeworkBl apptBl = new InstallationPipeworkBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                return apptBl.getInstallationPipeworkFormData(AppointmentID);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }

        //Change#12 - Behroz - 12/14/2012 - Start
        /// <summary>
        /// This function returns the InstallationPipework data for gas
        /// </summary>
        /// <returns>List of InstallationPipework data objects</returns>
        public InstallationPipeworkData getInstallationPipeworkFormDataGas(int JournalId, string userName, string salt)
        {
            try
            {
                InstallationPipeworkBl apptBl = new InstallationPipeworkBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                //return apptBl.getInstallationPipeworkFormDataGas(PropertyId);
                return apptBl.getInstallationPipeworkFormDataGas(JournalId);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }
        //Change#12 - Behroz - 12/14/2012 - End

        #endregion

        #region save InstallationPipework Data
        /// <summary>
        /// This function saves the InstallationPipework Data in the database
        /// </summary>
        /// <param name="appData">The object of InstallationPipework Data</param>
        /// <returns>the InstallationPipework id if successfully save otherwise 0 </returns>

        public int saveInstallationPipework(InstallationPipeworkData workData, string userName, string salt)
        {
            try
            {
                InstallationPipeworkBl apptBl = new InstallationPipeworkBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                validateInstallationPipeworkData(workData);
                return apptBl.saveInstallationPipework(workData);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }

        //Change#12 - Behroz - 12/13/2012 - Start
        /// <summary>
        /// This function saves the InstallationPipework Data in the database for gas
        /// </summary>
        /// <param name="appData">The object of InstallationPipework Data</param>
        /// <returns>the InstallationPipework id if successfully save otherwise 0 </returns>
        public int saveInstallationPipeworkGas(InstallationPipeworkData workData, string userName, string salt)
        {
            try
            {
                InstallationPipeworkBl apptBl = new InstallationPipeworkBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                validateInstallationPipeworkData(workData);
                return apptBl.saveInstallationPipeworkGas(workData);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }
        //Change#12 - Behroz - 12/13/2012 - End
        #endregion

        #region update InstallationPipework Data
        /// <summary>
        /// This function updates theInstallationPipework Data in the database
        /// </summary>
        /// <param name="appData">The object of InstallationPipework Data</param>
        /// <returns>true if successfully save otherwise 0 </returns>

        public bool updateInstallationPipeworkData(InstallationPipeworkData insData, string userName, string salt)
        {
            try
            {
                InstallationPipeworkBl apptBl = new InstallationPipeworkBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                validateInstallationPipeworkData(insData);
                return apptBl.updateInstallationPipeworkData(insData);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }

        #endregion

        #region validate InstallationPipework Data
        /// <summary>
        /// This function validate InstallationPipework Data
        /// </summary>
        /// <param name="appData">The object of InstallationPipework Data</param>
        /// <returns>true if successfully validate otherwise 0 </returns>

        public void validateInstallationPipeworkData(InstallationPipeworkData insData)
        {
            InstallationPipeworkData validate = new InstallationPipeworkData();
            if (!(validate.validatePipeworkOption(insData.VisualInspection) && validate.validatePipeworkOption(insData.GasTightnessTest) &&
                validate.validatePipeworkOption(insData.EquipotentialBonding) && validate.validatePipeworkOption(insData.EmergencyControl)))
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.InstallationPipeWorkOptionInvalid, true, ErrorCodesConstants.InstallationPipeWorkOptionInvalid);
                throw new ArgumentException(MessageConstants.InstallationPipeWorkOptionInvalid);
            }
        }

        #endregion

    }
}