﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PropSurvey.BuisnessLayer.Property;
using PropSurvey.Contracts.Data;
using PropSurvey.Contracts.Fault;
using PropSurvey.ServiceHost.IService;
using PropSurvey.Utilities.Constants;
using PropSurvey.Utilities.ErrorSetter;
using PropSurvey.Utilities.Helpers;
using PropSurvey.Utilities.Validate;

namespace PropSurvey.ServiceHost.Service
{
    // Start the service and browse to http://<machine_name>:<port>/AppointmentService/help to view the service's generated help page
    // NOTE: By default, a new instance of the service is created for each call; change the InstanceContextMode to Single if you want
    // a single instance of the service to process all calls.	    
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    // NOTE: If the service is renamed, remember to update the global.asax.cs file
    public class PropertyService :IProperty
    {
        #region find Property
        /// <summary>
        /// This function returns the property against specified parameters
        /// </summary>
        /// <param name="skip">how many records client application want to skip. e.g 10,20</param>
        /// <param name="top">how many records client application wants to get</param>
        /// <param name="reference">reference number </param>
        /// <param name="houseNumber">house number of customer</param>
        /// <param name="street">street of customer</param>
        /// <param name="postCode">post code of customer</param>
        /// <param name="surName">sur name of customer</param>
        /// <returns>It retruns the list of customer data's object</returns>

        public List<CustomerData> findProperty(string skip, string top, string reference = "", string houseNumber = "", string street = "", string postCode = "", string surName = "")
        {
            try
            {
                PropertyBl propBl = new PropertyBl();
                List<CustomerData> custData = new List<CustomerData>();

                custData = propBl.findProperty(skip, top, reference, houseNumber, street, postCode, surName);

                return custData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }

        //Change - Behroz - 12/12/2012 - Start
        /// <summary>
        /// This function returns the property against specified parameters
        /// </summary>
        /// <param name="skip">how many records client application want to skip. e.g 10,20</param>
        /// <param name="top">how many records client application wants to get</param>
        /// <param name="reference">reference number </param>
        /// <param name="houseNumber">house number of customer</param>
        /// <param name="street">street of customer</param>
        /// <param name="postCode">post code of customer</param>
        /// <param name="surName">sur name of customer</param>
        /// <returns>It retruns the list of customer data's object</returns>
        public List<CustomerData> findPropertyGas(string skip, string top, string reference = "", string houseNumber = "", string street = "", string postCode = "", string surName = "")
        {
            try
            {
                PropertyBl propBl = new PropertyBl();
                List<CustomerData> custData = new List<CustomerData>();

                custData = propBl.findPropertyGas(skip, top, reference, houseNumber, street, postCode, surName);

                return custData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }
        //Change - Behroz - 12/12/2012 - End
        #endregion

        #region get Property Image Names
        
        /// <summary>
        /// This function returns the list of images which were previously saved against property.
        /// </summary>
        /// <param name="propertyId">property id</param>
        /// <param name="itemId">item id</param>
        /// <returns>returns the list of image names against property</returns>
        
        public List<PropertyPictureData> getPropertyImageNames(string propertyId, int itemId)
        {
            try
            {
                PropertyBl propBl = new PropertyBl();
                List<PropertyPictureData> propList = new List<PropertyPictureData>();
                propList = propBl.getPropertyImages(propertyId, itemId);

                return propList;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }
        #endregion

        #region get Property Image 

        /// <summary>
        /// This fucntion returns the property images against property name
        /// </summary>
        /// <param name="propertyId">property id </param>
        /// <param name="imageName">image name </param>
        /// <returns></returns>
        public Stream getPropertyImage(string propertyId, string imageName)
        {
            try
            {

                string imageUploadPath = FileHelper.getPropertyImageUploadPath();
                string imageType = FileHelper.getPropertyImageType();
                string imageFullPath = FileHelper.makePropertyImageFullPath(imageUploadPath, propertyId, imageName);

                try
                {

                    Stream imageStream = File.OpenRead(imageFullPath);
                    WebOperationContext.Current.OutgoingResponse.ContentType = imageType;
                    return imageStream;
                }
                catch (FileNotFoundException fileNotFoundException)
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.PropertyImageNotFoundMsg, true, ErrorCodesConstants.FileNotFoundExceptionCode);
                    throw fileNotFoundException;
                }                
                
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }
        #endregion

        #region get Property Default Image

        /// <summary>
        /// This function returns the default image which was saved against property
        /// </summary>
        /// <param name="propertyId">property id</param>
        /// <returns>it returns the stream of image (if found)</returns>
       
        public Stream getDefaultPropertyImage(String propertyId)
        {
            try
            {
                PropertyBl propBl = new PropertyBl();

                String imageName = propBl.getDefaultPropertyImageName(propertyId);

                if (!imageName.Equals(String.Empty))
                {
                    string imageUploadPath = FileHelper.getPropertyImageUploadPath();
                    string imageType = FileHelper.getPropertyImageType();
                    string imageFullPath = FileHelper.makePropertyImageFullPath(imageUploadPath, propertyId, imageName);

                    try
                    {
                        Stream imageStream = File.OpenRead(imageFullPath);
                        WebOperationContext.Current.OutgoingResponse.ContentType = imageType;

                        return imageStream;
                    }
                    catch (FileNotFoundException fileNotFoundException)
                    {
                        ErrorFaultSetGet.setErrorFault(MessageConstants.PropertyImageNotFoundMsg, true, ErrorCodesConstants.FileNotFoundExceptionCode);
                        throw fileNotFoundException;
                    }
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.PropertyImageNotFoundMsg, true, ErrorCodesConstants.DatabaseEntryNotFoundCode);
                    throw new FileNotFoundException();
                }
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }
        #endregion

        #region update Property Image

        /// <summary>
        /// This funciton updates the property image in the database( if there is already some appointment in the database against that property)
        /// </summary>
        /// <param name="stream">image stream</param>
        /// <param name="surveyId">property id</param>
        /// <param name="appointmentId">appointment id</param>
        /// <param name="itemId">item id</param>
        /// <param name="fileName">file name image</param>
        /// <param name="pictureTag">picutre tag , this 'll be like this: Internal;Roof;Dwelling</param>
        /// <param name="isDefault">true or false</param>
        /// <returns>true or false upon successfull saving on disk and on db</returns>

        public bool updatePropertyImage(Stream stream, string propertyId, int appointmentId, int itemId, string fileName, int updatedBy, string fileSize, string pictureTag = "", bool isDefault = false)
        {
            try
            {
                PropertyBl propBl = new PropertyBl();
                
                //check the existance of property 
                propBl.isPropertyAppointmentExist(propertyId);
                
                string imageExt = FileHelper.getFileExtension(fileName);
                string imageName = FileHelper.generateUniqueString() + imageExt;
                string imageUploadPath = FileHelper.getPropertyImageUploadPath();                
                //Changed by Behroz
                //string imageFullPath = FileHelper.makePropertyImageFullPath(imageUploadPath, propertyId, imageName);

                //The web application is saving on the path ../../Photographs/Images and uses only ImageName to fetch the Images
                //FileHelper.createDirectory(imageUploadPath + @"\" + propertyId);
                
                if(Validator.validateImageFileExtension(imageExt) == false)
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.ImageExtensionInvalidMsg, true, ErrorCodesConstants.ImageExtensionInvalidCode);
                    throw new ArgumentException(MessageConstants.ImageExtensionInvalidMsg, "fileName");
                }

                //this function can throw io exception
                
                //Changed by Behroz
                //this.saveImageOnDisk(stream, imageFullPath, fileSize);
                this.saveImageOnDisk(stream, imageUploadPath + imageName, fileSize);

                //Generate thumbnail
                FileHelper.generateThumbnail(imageName, FileHelper.getPropertyThumbnailUploadPath(), imageUploadPath + imageName);

                PropertyPictureData propPicData = new PropertyPictureData();
                propPicData.propertyPictureName = imageName;
                propPicData.imagePath = imageUploadPath;
                propPicData.isDefault = isDefault;
                propPicData.itemId = itemId;
                propPicData.propertyId = propertyId;
                propPicData.appointmentId = appointmentId;
                propPicData.createdBy = updatedBy;
                
                return propBl.savePropertyImage(propPicData);

            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }

        //Change#43 - Behroz - 09/01/2013 - Start
        /// <summary>
        /// This funciton updates the property image in the database( if there is already some appointment in the database against that property)
        /// </summary>
        /// <param name="stream">image stream</param>
        /// <param name="surveyId">property id</param>
        /// <param name="appointmentId">appointment id</param>
        /// <param name="itemId">item id</param>
        /// <param name="fileName">file name image</param>
        /// <param name="pictureTag">picutre tag , this 'll be like this: Internal;Roof;Dwelling</param>
        /// <param name="isDefault">true or false</param>
        /// <returns>true or false upon successfull saving on disk and on db</returns>
        public bool updatePropertyImageGas(Stream stream, string propertyId, int appointmentId, string fileName, int updatedBy, string fileSize, string pictureTag = "", bool isDefault = false)
        {
            try
            {
                PropertyBl propBl = new PropertyBl();

                //check the existance of property 
                propBl.isPropertyAppointmentExist(appointmentId);

                string imageExt = FileHelper.getFileExtension(fileName);
                string imageName = FileHelper.generateUniqueString() + imageExt;
                string imageUploadPath = FileHelper.getPropertyImageUploadPath();
                //Changed by Behroz
                //string imageFullPath = FileHelper.makePropertyImageFullPath(imageUploadPath, propertyId, imageName);
                //FileHelper.createDirectory(imageUploadPath + @"\" + propertyId);

                if (Validator.validateImageFileExtension(imageExt) == false)
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.ImageExtensionInvalidMsg, true, ErrorCodesConstants.ImageExtensionInvalidCode);
                    throw new ArgumentException(MessageConstants.ImageExtensionInvalidMsg, "fileName");
                }

                //this function can throw io exception
                
                //Changed by Behroz
                //this.saveImageOnDisk(stream, imageFullPath, fileSize);
                this.saveImageOnDisk(stream, imageUploadPath + imageName, fileSize);
                
                //Generate thumbnail
                if (imageExt != Utilities.Constants.MessageConstants.MovieExtension)
                {
                    FileHelper.generateThumbnail(imageName, FileHelper.getPropertyThumbnailUploadPath(), imageUploadPath + imageName);
                }
                else
                {
                    string defaultImgPath = System.Web.Hosting.HostingEnvironment.MapPath(Utilities.Constants.MessageConstants.DefaultImagePath);
                    FileHelper.generateThumbnail(imageName.Replace(".mov",".jpg"), FileHelper.getPropertyThumbnailUploadPath(), defaultImgPath);
                }

                PropertyPictureData propPicData = new PropertyPictureData();
                propPicData.propertyPictureName = imageName;
                propPicData.imagePath = imageUploadPath;
                propPicData.isDefault = isDefault;
                propPicData.itemId = propBl.getItemId();
                propPicData.propertyId = propertyId;
                propPicData.appointmentId = appointmentId;
                propPicData.createdBy = updatedBy;

                return propBl.savePropertyImageGas(propPicData);

            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }
        //Change#43 - Behroz - 09/01/2013 - End
        #endregion
               
        #region save Image On Disk

        /// <summary>
        /// This function save the image of property on disk
        /// </summary>
        /// <param name="stream">stream of image</param>
        /// <param name="imageFullPath">full path of image, where image should be saved</param>

        private void saveImageOnDisk(Stream stream, string imageFullPath, string fileSize)
        {
            try
            {
                //byte[] buffer = new byte[10485760];
                //stream.Read(buffer, 0, 10485760);
                //FileStream outstream = new FileStream(imageFullPath, FileMode.Create, FileAccess.Write);
                //outstream.Write(buffer, 0, buffer.Length);
                //outstream.Close();
                //stream.Close();

                //old method which wasn't working in case of png
                FileStream outstream = File.Open(imageFullPath, FileMode.Create, FileAccess.Write);
                //read from the input stream in 4K chunks and save to output stream

                int bufferLen = int.Parse(fileSize);
                byte[] buffer = new byte[bufferLen];
                int count = 0;
                while ((count = stream.Read(buffer, 0, bufferLen)) > 0)
                {
                    outstream.Write(buffer, 0, count);
                }
                outstream.Close();
                stream.Close();
            }
            catch (IOException ioException)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.ImageSavingErrorMsg, true, ErrorCodesConstants.ImageSavingErrorCode);
                throw ioException;
            }
        }
        #endregion 

        #region delete Property Image
        /// <summary>
        /// This function is used to delete the image against the property from the server
        /// </summary>
        /// <param name="propertyPictureId">property picture id</param>
        /// <returns>true if file deleted or not but return false in case of any error while connecting with database or property picture id is not valid integer</returns>
        
        public bool deletePropertyImage(string propertyPictureId)
        {
            try
            {
                PropertyBl propBl = new PropertyBl();
                PropertyPictureData propPicData = new PropertyPictureData();
                int propPicId = 0;
                bool success = true;
                try
                {
                    propPicId = Int32.Parse(propertyPictureId);
                }
                catch (FormatException formatException)
                {
                    ErrorFaultSetGet.setErrorFault(String.Format(MessageConstants.PropertyPictureIdInvalidMsg,propertyPictureId), true, ErrorCodesConstants.IntegerToStringConversionExceptionCode);
                    throw formatException;
                }

                propPicData = propBl.deletePropertyImage(propPicId);

                if (propPicData.propertyPictureName != string.Empty && propPicData.propertyPictureName != null)
                {
                    string imageUploadPath = FileHelper.getPropertyImageUploadPath();
                    string imageFullPath = FileHelper.makePropertyImageFullPath(imageUploadPath, propPicData.propertyId, propPicData.propertyPictureName);

                    try
                    {
                        File.Delete(imageFullPath);
                    }                                        
                    catch (FileNotFoundException)
                    {
                        //if file does not found then it means that its already deleted so no need to send false
                        success = true;
                    }
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(String.Format(MessageConstants.PropertyPictureIdDoesNotExistMsg, propertyPictureId), true, ErrorCodesConstants.PropertyPictureIdDoesNotExistCode);
                    throw new ArgumentException(String.Format(MessageConstants.PropertyPictureIdDoesNotExistMsg, propertyPictureId), "fileName");
                }

                return success;

            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }           
        }

        #endregion 

        #region get Property Dimensions

        /// <summary>
        /// This function returns ths property dimensions
        /// </summary>
        /// <param name="propertyId">property id</param>
        /// <returns>property dimension data object</returns>
        public List<PropertyDimData> getPropertyDimensions(string propertyId)
        {
            try
            {
                PropertyBl propBl = new PropertyBl();                
                return propBl.getPropertyDimensions(propertyId);
           
             }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }
        #endregion

        #region update Property Dimensions

        /// <summary>
        /// This function updates the property dimensions
        /// </summary>
        /// <param name="propDimData">This function accepts the property dimension data object</param>
        /// <returns>true on successful update and false on un successful update</returns>
        public bool updatePropertyDimensions(List<PropertyDimData> propertyDimBo)
        {
            try
            {
                PropertyBl propBl = new PropertyBl();

                //varifies property id exists in database against any appointment
                propBl.isPropertyAppointmentExist(propertyDimBo[0].propertyId);
                return propBl.updatePropertyDimensions(propertyDimBo);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }

        #endregion 
        
        //#region "get Property Asbestos Risk"
        ///// <summary>
        ///// This function returns the list of asbestos risk against the property.
        ///// </summary>
        ///// <param name="propertyId">property id</param>
        ///// <returns>the list of asbestos risks</returns>

        //public List<PropertyAsbestosData> getPropertyAsbestosRisk(string propertyId)
        //{
        //    try
        //    {
        //        PropertyBl propBl = new PropertyBl();
        //        return propBl.getPropertyAsbestosRisk(propertyId);
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionPolicy.HandleException(ex, "Exception Policy");

        //        MessageFault msgFault = new MessageFault();
        //        msgFault.message = ErrorFault.message;
        //        msgFault.isErrorOccured = true;
        //        msgFault.errorCode = ErrorFault.errorCode;

        //        throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
        //    }
        //}
        //#endregion
    }
}

