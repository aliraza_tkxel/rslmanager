﻿using System;
using System.Collections.Generic;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PropSurvey.BuisnessLayer.Faults;
using PropSurvey.Contracts.Data;
using PropSurvey.Contracts.Fault;
using PropSurvey.ServiceHost.IService;
using PropSurvey.Utilities.Constants;
using PropSurvey.Utilities.ErrorSetter;
using PropSurvey.BuisnessLayer.Authentication;
using PropSurvey.Utilities.Helpers;
using System.IO;

namespace PropSurvey.ServiceHost.Service
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class Faults : IFaults
    {
        #region get IFaults Data
        /// <summary>
        /// This function returns the IFaults data
        /// </summary>
        /// <returns>List of IFaults data objects</returns>
        public List<FaultsData> getFaultsData(int AppointmentID, string userName, string salt)
        {
            try
            {
                FaultsBl apptBl = new FaultsBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                return apptBl.getFaultsData(AppointmentID);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }

        //Change#18 - Behroz - 19/12/2012 - Start
        /// <summary>
        /// This function returns the Faults data for gas
        /// </summary>
        /// <returns>List of IFaults data objects</returns>
        public List<FaultsDataGas> getFaultsDataGas(int journalId, string userName, string salt)
        {
            try
            {
                FaultsBl apptBl = new FaultsBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                return apptBl.getFaultsDataGas(journalId);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }
        //Change#18 - Behroz - 19/12/2012 - End

        #endregion

        #region get general comments Data
        /// <summary>
        /// This function returns general comments
        /// </summary>
        /// <returns>List of general comments objects</returns>        
        public List<FaultsData> getGeneralComments(int AppointmentID, string userName, string salt)
        {
            try
            {
                FaultsBl apptBl = new FaultsBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                return apptBl.getGeneralComments(AppointmentID);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }

        //Change#21 - Behroz - 19/12/2012 - Start
        /// <summary>
        /// This function returns general comments for gas
        /// </summary>
        /// <returns>List of general comments objects</returns>
        public List<FaultsDataGas> getGeneralCommentsGas(int journalId, string userName, string salt)
        {
            try
            {
                FaultsBl apptBl = new FaultsBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                return apptBl.getGeneralCommentsGas(journalId);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }
        //Change#21 - Behroz - 19/12/2012 - End
        #endregion

        #region get faults data only
        /// <summary>
        /// This function returns faults data
        /// </summary>
        /// <returns>List of faults data objects</returns>
        public List<FaultsData> getFaultsOnly(int AppointmentID, string userName, string salt)
        {
            try
            {
                FaultsBl apptBl = new FaultsBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                return apptBl.getFaultsOnly(AppointmentID);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }

        //Change#22 - Behroz - 19/12/2012 - Start
        /// <summary>
        /// This function returns faults data for gas
        /// </summary>
        /// <returns>List of faults data objects</returns>
        public List<FaultsDataGas> getFaultsOnlyGas(int journalId, string userName, string salt)
        {
            try
            {
                FaultsBl apptBl = new FaultsBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                return apptBl.getFaultsOnlyGas(journalId);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }
        //Change#22 - Behroz - 19/12/2012 - End

        #endregion

        #region save Faults Data
        /// <summary>
        /// This function saves the Faults Data in the database
        /// </summary>
        /// <param name="appData">The object of Faults Data</param>
        /// <returns>the Faults id if successfully save otherwise 0 </returns>

        public int saveFaultsData(FaultsData faultData, string userName, string salt)
        {
            try
            {
                FaultsBl apptBl = new FaultsBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                validateFaultsData(faultData);
                return apptBl.saveFaultsData(faultData);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }

        //Change#19 - Behroz - 19/12/2012 - Start
        /// <summary>
        /// This function saves the Faults Data in the database for gas
        /// </summary>
        /// <param name="appData">The object of Faults Data</param>
        /// <returns>the Faults id if successfully save otherwise 0 </returns>
        public int saveFaultsDataGas(FaultsDataGas faultData, string userName, string salt)
        {
            try
            {
                FaultsBl apptBl = new FaultsBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                apptBl.validateFaultsData(faultData);
                return apptBl.saveFaultsDataGas(faultData);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }
        //Change#19 - Behroz - 19/12/2012 - End

        #endregion

        #region update fault Data
        /// <summary>
        /// This function updates fault Data in the database
        /// </summary>
        /// <param name="appData">The object of fault Data</param>
        /// <returns>true if successfully save otherwise 0 </returns>

        public bool updateFaultData(FaultsData faultData, string userName, string salt)
        {
            try
            {
                FaultsBl apptBl = new FaultsBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                validateFaultsData(faultData);
                return apptBl.updateFaultData(faultData);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }

        //Change#20 - Behroz - 19/12/2012 - Start
        /// <summary>
        /// This function updates fault Data in the database for gas
        /// </summary>
        /// <param name="appData">The object of fault Data</param>
        /// <returns>true if successfully save otherwise 0 </returns>
        public bool updateFaultDataGas(FaultsDataGas faultData, string userName, string salt)
        {
            try
            {
                FaultsBl apptBl = new FaultsBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                apptBl.validateFaultsData(faultData);
                return apptBl.updateFaultDataGas(faultData);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }
        //Change#20 - Behroz - 19/12/2012 - End
        #endregion

        #region validate Faults Data
        /// <summary>
        /// This function validate Faults Data
        /// </summary>
        /// <param name="appData">The object of Faults Data</param>
        /// <returns>true if successfully validate otherwise 0 </returns>

        public void validateFaultsData(FaultsData insData)
        {
            FaultsData validate = new FaultsData();
            if (!(validate.validateFaultsCategories(insData.FaultCategory)))
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.InvalidFaultCategory, true, ErrorCodesConstants.InvalidFaultCategory);
                throw new ArgumentException(string.Format(MessageConstants.InvalidFaultCategory, insData.FaultCategory));
            }
        }

       

        #endregion

        #region "Get All Defect Categories"

        //Change#15 - Behroz - 12/17/2012 - Start
        /// <summary>
        /// This function returns all the defect categories
        /// </summary>
        /// <returns>List of defect categories</returns>
        public List<DefectCategoryData> getDefectCategory(string userName, string salt)
        {
            try
            {
                FaultsBl apptBl = new FaultsBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                return apptBl.getDefectCategories();
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }
        //Change#15 - Behroz - 12/17/2012 - End

        #endregion

        #region "Get Fault Id"
        //Change#51 - Behroz - 16/01/2013 - Start
        /// <summary>
        /// This function return the fault id
        /// </summary>
        /// <param name="propertyId"></param>
        /// <param name="userName"></param>
        /// <param name="salt"></param>
        /// <returns></returns>
        public int getFaultId(string propertyId, string userName, string salt)
        {
            try
            {
                FaultsBl apptBl = new FaultsBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                return apptBl.getFaultId(propertyId);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }
        //Change#51 - Behroz - 16/01/2013 - End
        #endregion

        #region "Delete Fault"
        //Change#52 - Behroz - 16/01/2013 - Start
        /// <summary>
        /// This function deletes the fault from the database
        /// </summary>
        /// <param name="faultId"></param>
        /// <param name="userName"></param>
        /// <param name="salt"></param>
        /// <returns></returns>
        public bool deleteFaultId(int faultId, string userName, string salt)
        {
            try
            {
                FaultsBl apptBl = new FaultsBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                return apptBl.deleteFault(faultId);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }
        //Change#52 - Behroz - 16/01/2013 - End
        #endregion

        #region "Save Fault Image"
        //Change#53 - Behroz - 17/01/2013 - Start
        /// <summary>
        /// This function saves the image path in the database and the image on the local drive
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="faultId"></param>
        /// <param name="fileName"></param>
        /// <param name="userName"></param>
        /// <param name="salt"></param>
        /// <returns></returns>
        public bool saveFaultImage(Stream stream, int faultId, string fileName,string fileSize, string userName, string salt)
        {
            try
            {
                FaultsBl apptBl = new FaultsBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                
                apptBl.validateFaultsId(faultId);

                //Generate Path
                string imageExt = FileHelper.getFileExtension(fileName);
                string uniqueFileName = FileHelper.getUniqueFileName(fileName);
                string completePath = FileHelper.getPropertyImageUploadPath() + uniqueFileName;
                
                //Save Image
                FileHelper.saveImageOnDisk(stream, completePath, fileSize);
                //Save Thumbnail
                //FileHelper.generateThumbnail(uniqueFileName, FileHelper.getPropertyThumbnailUploadPath(), completePath);
                if (imageExt != Utilities.Constants.MessageConstants.MovieExtension)
                {
                    FileHelper.generateThumbnail(uniqueFileName, FileHelper.getPropertyThumbnailUploadPath(), completePath);
                }
                else
                {
                    string defaultImgPath = System.Web.Hosting.HostingEnvironment.MapPath(Utilities.Constants.MessageConstants.DefaultImagePath);
                    FileHelper.generateThumbnail(uniqueFileName.Replace(".mov", ".jpg"), FileHelper.getPropertyThumbnailUploadPath(), defaultImgPath);                    
                }

                return apptBl.saveFaultImage(uniqueFileName, faultId);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }
        //Change#53 - Behroz - 17/01/2013 - End
        #endregion
    }
}