﻿using System;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PropSurvey.BuisnessLayer.Authentication;
using PropSurvey.Contracts.Data;
using PropSurvey.Contracts.Fault;
using PropSurvey.ServiceHost.IService;
using PropSurvey.Utilities.Helpers;

namespace PropSurvey.ServiceHost.Service
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class AuthenticationService:IAuthentication
    {
        /// <summary>
        /// This function calls the business layer to authenticate the function from database.
        /// </summary>
        /// <param name="userName">username of surveyour</param>
        /// <param name="password">password of surveyour</param>
        /// <returns>Last Logged In Data Time</returns>


        //public DateTime? authenticateUser(string userName, string password, string deviceToken)
        //{
        //    try
        //    {

        //        AuthenticationBl authBl = new AuthenticationBl();
        //        MessageData msgData = new MessageData();
                
        //        return authBl.findUser(userName, password,deviceToken);                
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionPolicy.HandleException(ex, "Exception Policy");

        //        MessageFault msgFault = new MessageFault();
        //        msgFault.message = ErrorFault.message;
        //        msgFault.isErrorOccured = true;
        //        msgFault.errorCode = ErrorFault.errorCode;
                
        //        throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.NotFound);
        //    }
        //}
        public UserData authenticateUser(string userName, string password, string deviceToken)
        {
            try
            {
                userName = SecurityEncryption.DecryptData(userName);
                password = SecurityEncryption.DecryptData(password);  
                AuthenticationBl authBl = new AuthenticationBl();
                MessageData msgData = new MessageData();

                return authBl.findUser(userName, password, deviceToken);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

               throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.NotFound);
            }
        }

        public void LogoutUser(string userName, string salt)
        {
            try
            {
                
                AuthenticationBl authBl = new AuthenticationBl();
                if (authBl.checkForValidSession(userName, salt))
                {
                    authBl.LogoutUser(SecurityEncryption.DecryptData(userName));
                }
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.NotFound);
            }
        }

        public string getSalt(string userName)
        {
        try
            { 
                AuthenticationBl authBl = new AuthenticationBl();
                return authBl.getSalt(userName);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.NotFound);
            }
        }

        public int checkUser(string userName, string password)
        {
            AuthenticationBl authBl = new AuthenticationBl();
            return authBl.CheckUser(userName, password);
        }

        //Change#16 - Behroz - 18/12/2012 - Start
        #region "Can use access Gas Tab"
        /// <summary>
        /// This function returns a boolean indicating that user can access the gas tab or not.
        /// </summary>
        /// <returns></returns>
        public bool GasTabRights(int userid, string userName, string salt)
        {
            try
            {   
                AuthenticationBl authBl = new AuthenticationBl();
                if (authBl.checkForValidSession(userName, salt))
                {
                    return authBl.GetGasTabRights(userid);
                }

                return false;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;
                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);

            }
        }

        #endregion
        //Change#16 - Behroz - 18/12/2012 - End
    }
}
