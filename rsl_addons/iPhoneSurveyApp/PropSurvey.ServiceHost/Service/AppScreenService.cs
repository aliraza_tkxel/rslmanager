﻿using System;
using System.IO;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PropSurvey.BuisnessLayer.AppScreen;
using PropSurvey.BuisnessLayer.Authentication;
using PropSurvey.Contracts.Fault;
using PropSurvey.ServiceHost.IService;

namespace PropSurvey.ServiceHost.Service
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class AppScreenService : IAppScreen
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="propertyId"></param>
        /// <param name="userName"></param>
        /// <param name="salt"></param>
        /// <returns></returns>
        public Stream getApplicationScreens(string propertyId, string userName, string salt)
        {
            try
            {

                string appScreenJson = string.Empty;

                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                AppScreenBl appScreenBl = new AppScreenBl();
                appScreenJson = appScreenBl.getApplicationScreens(propertyId);

                WebOperationContext.Current.OutgoingResponse.ContentType = "application/json";
                //WebOperationContext.Current.OutgoingResponse.Headers.Add("Content-Disposition", "attachment; filename=MyFile.json");

                MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(appScreenJson));
                return ms;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }



    }
}
