using System;
using System.Collections.Generic;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PropSurvey.BuisnessLayer.Appointment;
using PropSurvey.Contracts.Data;
using PropSurvey.Contracts.Fault;
using PropSurvey.ServiceHost.IService;
using PropSurvey.Utilities.Constants;
using PropSurvey.Utilities.ErrorSetter;
using PropSurvey.BuisnessLayer.Authentication;
using PropSurvey.Utilities.Helpers;
using System.IO;
using System.Text;

namespace PropSurvey.ServiceHost.Service
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class AppointmentService : IAppointmentService
    {

        //#region get All Appointments
        ///// <summary>
        ///// This function returns all the appointments
        ///// </summary>
        ///// <returns>List of appointment data objects</returns>
        ///// 

        //public List<AppointmentData> getAllAppointments(string userName, string salt)
        //{
        //    try
        //    {
        //        List<AppointmentData> apptData = new List<AppointmentData>();
        //        AuthenticationBl authBl = new AuthenticationBl();
        //        if (authBl.checkForValidSession(userName, salt))
        //        {
        //            AppointmentBl apptBl = new AppointmentBl();
        //            apptData = apptBl.getAllAppointments();
        //        }
        //        else
        //        {
        //            apptData.Add(returnInValidAppointment());
        //        }

        //        return apptData;
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionPolicy.HandleException(ex, "Exception Policy");

        //        MessageFault msgFault = new MessageFault();
        //        msgFault.message = ErrorFault.message;
        //        msgFault.isErrorOccured = true;
        //        msgFault.errorCode = ErrorFault.errorCode;

        //        //throw new FaultException<MessageFault>(msgFault, new FaultReason(msgFault.message));
        //        throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);

        //    }
        //}
        //#endregion    

        #region get All Appointments by User - Wrapper Function
        /// <summary>
        /// This is a wrapper function which returns all the appointments of all types
        /// </summary>
        /// <returns>List of all appointments in string</returns>
        /// 

        public Stream getAllAppointmentsByUser(string userName, string salt, string filter)
        {
            try
            {
                string reponseJSON;
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);

                AppointmentBl apptBl = new AppointmentBl();
                reponseJSON = apptBl.getAllAppointmentsByUser(userName, filter);

                WebOperationContext.Current.OutgoingResponse.ContentType = "text/plain";
                MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(reponseJSON));

                return ms;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                //throw new FaultException<MessageFault>(msgFault, new FaultReason(msgFault.message));
                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);

            }
        }
        #endregion

        #region get Fault Repair List

        /// <summary>
        /// This function returns the list of all fault repairs
        /// </summary>
        /// <returns>List of Fault Repair in stream</returns>
        public List<FaultRepairData> getFaultRepairList(string userName, string salt)
        {
            try
            {
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);

                AppointmentBl apptBl = new AppointmentBl();
                return apptBl.getFaultRepairList();

                //WebOperationContext.Current.OutgoingResponse.ContentType = "text/plain";
                //MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(faultRepairlist.ToString()));

                //return ms;

            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }
        #endregion


        #region get pause reason list
        /// <summary>
        /// This function returns reason list for pausing a job
        /// </summary>
        /// <returns>List of reasons</returns>

        public List<string> getPauseReasonList(string userName, string salt)
        {
            try
            {
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);

                AppointmentBl apptBl = new AppointmentBl();
                return apptBl.getPauseReasonList();
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }
        #endregion



        //Change Ticket# 8,9,10 - Zeeshan - 05/03/2013 - Start
        //Code modified by Abdul Wahhab - 04/06/2013 - START
        #region get All Stock Appointments
        /// <summary>
        /// This function returns all stock appointments
        /// </summary>
        /// <returns>all appointment data objects</returns>
        /// 
        //public List<AppointmentData> getAllAppointments(string userName, string salt)
        public List<AppointmentListStock> getAllStockAppointments(string userName, string salt)
        {
            try
            {
                List<AppointmentListStock> apptData = new List<AppointmentListStock>();
                AuthenticationBl authBl = new AuthenticationBl();
                if (authBl.checkForValidSession(userName, salt))
                {
                    AppointmentBl apptBl = new AppointmentBl();
                    //apptData = apptBl.getAllAppointments();
                    apptData = apptBl.getAllStockAppointments();
                }
                else
                {
                    apptData.Add(returnInValidAppointment());
                }

                return apptData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                //throw new FaultException<MessageFault>(msgFault, new FaultReason(msgFault.message));
                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);

            }
        }
        //Code modified by Abdul Wahhab - 04/06/2013 - END
        //Change Ticket# 8,9,10 - Zeeshan - 05/03/2013 - End
        #endregion

        #region update Stock Appointment Progress Status

        /// <summary>
        /// This function update the status of Stock appointment. This function use post method to accept data. 
        /// </summary>
        /// <param name="apptData">This fucntion accepts the AppointmentData's object </param>
        /// <returns>It returns true or false in update is successful</returns>

        public bool updateStockAppointmentProgressStatus(AppointmentDataStock apptData)
        {
            try
            {
                AppointmentBl apptBl = new AppointmentBl();
                bool isAppointFinished = false;
                DateTime lastSurveyDate = new DateTime();
                bool success = false;

                apptData.appointmentId = Convert.ToInt32(apptData.appointmentId);

                //Check the appointment status against the enumerated data type
                if (apptData.validateAppointmentProgressStatus(apptData.appointmentStatus) == false)
                {
                    ErrorFaultSetGet.setErrorFault(String.Format(MessageConstants.AppointProgStatusInvalidValueMsg, apptData.appointmentStatus), true, ErrorCodesConstants.AppointmentProgStatusInvalidSelectionCode);
                    throw new ArgumentException(String.Format(MessageConstants.AppointProgStatusInvalidValueMsg, apptData.appointmentStatus), "appointmentStatus");
                }
                else
                {
                    // Check if the appointment progress status is finished then put the last survey date
                    if (apptData.appointmentStatus.Equals(AppointmentProgressStatus.Finished.ToString()) == true)
                    {
                        isAppointFinished = true;
                        lastSurveyDate = DateTime.Now;
                    }
                    success = apptBl.updateStockAppointmentProgressStatus(apptData, isAppointFinished, lastSurveyDate);

                }

                return success;
            }
            catch (FormatException formatException)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.SurveyourUsernameInvalidUserMsg, true, ErrorCodesConstants.IntegerToStringConversionExceptionCode);
                throw formatException;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }
        #endregion

        #region update Fault Appointment Progress Status

        /// <summary>
        /// This function update the status of appointment. This function use post method to accept data. 
        /// </summary>
        /// <param name="apptData">This fucntion accepts the AppointmentData's object </param>
        /// <returns>It returns true or false in update is successful</returns>

        public ResultBoolData updateFaultAppointmentProgressStatus(string userName, string appointmentId, string progressStatus, string salt)
        {
            try
            {
                ResultBoolData resultBoolData = new ResultBoolData();
                resultBoolData.result = false;
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);

                int appointID = Int32.Parse(appointmentId);
                AppointmentBl apptBl = new AppointmentBl();
                AppointmentDataFault apptData = new AppointmentDataFault();
                apptData.appointmentId = Convert.ToInt32(appointmentId);
                apptData.appointmentStatus = progressStatus;
                //Check the appointment status against the enumerated data type
                if (apptData.validateFaultAppointmentProgressStatus(apptData.appointmentStatus.Replace(" ", "")) == false)
                {
                    ErrorFaultSetGet.setErrorFault(String.Format(MessageConstants.AppointFaultProgStatusInvalidValueMsg, apptData.appointmentStatus), true, ErrorCodesConstants.AppointmentProgStatusInvalidSelectionCode);
                    throw new ArgumentException(String.Format(MessageConstants.AppointFaultProgStatusInvalidValueMsg, apptData.appointmentStatus), "appointmentStatus");
                }
                else
                {
                    resultBoolData = apptBl.updateFaultAppointmentProgressStatus(appointID, progressStatus);
                }

                return resultBoolData;
            }
            catch (FormatException formatException)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.SurveyourUsernameInvalidUserMsg, true, ErrorCodesConstants.IntegerToStringConversionExceptionCode);
                throw formatException;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }
        #endregion

        #region Update a Customer Contact data from fault appointment
        /// <summary>
        /// This function updates customer data from within fault appointment
        /// </summary>
        /// <param name="CustomerData">Customer Data's object</param>
        /// <returns>It retruns the true on success else returns false</returns>
        public bool UpdateCustomerContactFromAppointment(CustomerData cusData, string userName, string salt)
        {
            try
            {
                AppointmentBl apptBl = new AppointmentBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);

                return apptBl.UpdateCustomerContactFromAppontment(cusData);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }
        #endregion

        #region get And Set Appointment Progress Status

        /// <summary>
        /// This function checks the appointment progress status, If that is not started , it 'll start it otherwise it'll return false
        /// </summary>
        /// <param name="appointmentId">appointment id</param>
        /// <param name="userName">user name </param>
        /// <returns>it return true if appointment status changed from NotStarted to Pending other wise false</returns>
        public bool getAndSetStockAppointmentProgressStatus(string appointmentId, string userName)
        {
            try
            {
                AppointmentBl apptBl = new AppointmentBl();
                AppointmentDataStock apptData = new AppointmentDataStock();
                apptData.appointmentId = Convert.ToInt32(appointmentId);
                apptData.surveyorUserName = userName;

                return apptBl.getAndSetStockAppointmentProgressStatus(apptData);

            }
            catch (FormatException formatException)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.AppointmentIdInvalidFromtMsg, true, ErrorCodesConstants.IntegerToStringConversionExceptionCode);
                throw formatException;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }

        }

        //Change#14 - behroz - 12/07/2012 - Start
        /// <summary>
        /// This function checks the appointment progress status related to Gas Tab, If that is not started , it 'll start it otherwise it'll return false
        /// </summary>
        /// <param name="appointmentId">appointment id</param>
        /// <param name="userName">user name </param>
        /// <returns>it return true if appointment status changed from NotStarted to Pending other wise false</returns>
        public bool getAndSetGasAppointmentProgressStatus(string appointmentId, int userId)
        {
            try
            {
                AppointmentBl apptBl = new AppointmentBl();
                AppointmentDataGas apptData = new AppointmentDataGas();
                apptData.appointmentId = Convert.ToInt32(appointmentId);
                //apptData.assignedToUsername = userName;
                apptData.assignedTo = userId;

                return apptBl.getAndSetAppointmentProgressStatus(apptData);

            }
            catch (FormatException formatException)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.AppointmentIdInvalidFromtMsg, true, ErrorCodesConstants.IntegerToStringConversionExceptionCode);
                throw formatException;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }

        }
        //Change#14 - behroz - 12/07/2012 - End

        /// <summary>
        /// This function get fault appointment progress status
        /// </summary>
        /// <param name="appointmentId">appointment id</param>
        /// <param name="userName">user name </param>
        /// <returns>return the fault appointment progress status</returns>
        public Stream getFaultJobStatus(string appointmentId, string userName)
        {
            try
            {
                AppointmentBl apptBl = new AppointmentBl();
                int apptID = -1;
                string reponseJSON = "";
                apptID = Convert.ToInt32(appointmentId);

                reponseJSON = apptBl.getFaultJobStatus(apptID, userName);
                WebOperationContext.Current.OutgoingResponse.ContentType = "text/plain";
                MemoryStream ms = new MemoryStream(Encoding.UTF8.GetBytes(reponseJSON));
                return ms;
            }
            catch (FormatException formatException)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.AppointmentIdInvalidFromtMsg, true, ErrorCodesConstants.IntegerToStringConversionExceptionCode);
                throw formatException;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }

        }

        #region update Fault Job Status

        /// <summary>
        /// This function update the status of all jobs of an appointment. This function use post method to accept data. 
        /// </summary>
        /// <param name="faultStatusData">This fucntion accepts the FaultStatusData's object </param>
        /// <param name="userName">user name </param>
        /// <param name="salt">salt </param>
        /// <returns>It returns true or false in update is successful</returns>

        public ResultBoolData updateFaultJobStatus(FaultStatusData faultStatusData, string userName, string salt)
        {
            try
            {
                ResultBoolData resultBoolData = new ResultBoolData();
                resultBoolData.result = false;
                AppointmentBl apptBl = new AppointmentBl();
                AuthenticationBl authBl = new AuthenticationBl();
                // authBl.checkForValidSession(userName, salt);

                //Check the fault status against the enumerated data type
                if (faultStatusData.validateFaultJobProgressStatus(faultStatusData.ProgressStatus.Replace(" ", "")) == false)
                {
                    ErrorFaultSetGet.setErrorFault(String.Format(MessageConstants.FaultJobStatusInvalidValueMsg, faultStatusData.ProgressStatus), true, ErrorCodesConstants.FaultJobProgStatusInvalidSelectionCode);
                    throw new ArgumentException(String.Format(MessageConstants.FaultJobStatusInvalidValueMsg, faultStatusData.ProgressStatus), "JobStatus");
                }
                else
                {
                    resultBoolData = apptBl.updateFaultJobStatus(faultStatusData, userName);
                }

                return resultBoolData;
            }
            catch (FormatException formatException)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.SurveyourUsernameInvalidUserMsg, true, ErrorCodesConstants.IntegerToStringConversionExceptionCode);
                throw formatException;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }
        #endregion

        #region update Appointment Status to NoEntry

        // Code added by Abdul Wahhab - 11/06/2013 - START

        /// <summary>
        /// This function update the status of all jobs of an appointment to no entry. This function use post method to accept data. 
        /// </summary>
        /// <param name="faultStatusData">This fucntion accepts the FaultStatusData's object </param>
        /// <param name="userName">user name </param>
        /// <param name="salt">salt </param>
        /// <returns>It returns true or false in update is successful</returns>

        public ResultBoolData noEntryAppointmentStatus(FaultStatusData faultStatusData, string userName, string salt)
        {
            try
            {
                AppointmentBl apptBl = new AppointmentBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                return apptBl.noEntryAppointmentStatus(faultStatusData, userName);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }

        // Code added by Abdul Wahhab - 11/06/2013 - END

        #endregion

        #endregion

        #region "Get Status"
        //Change - Behroz - 12/10/2012 - Start
        public List<StatusData> getStatus(string userName, string salt)
        {
            try
            {
                List<StatusData> statusData = new List<StatusData>();
                AuthenticationBl authBl = new AuthenticationBl();
                if (authBl.checkForValidSession(userName, salt))
                {
                    AppointmentBl apptBl = new AppointmentBl();
                    statusData = apptBl.getAllStatusData();
                    return statusData;
                }
                else
                {
                    statusData.Add(returnInValidStatusData());
                }

                return statusData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                //throw new FaultException<MessageFault>(msgFault, new FaultReason(msgFault.message));
                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);

            }
        }
        //Change - Behroz - 12/10/2012 - End
        #endregion

        #region save Appointment
        /// <summary>
        /// This function saves the appointment in the database
        /// </summary>
        /// <param name="apptData">The object of appointment</param>
        /// <returns>the appointment id if successfully save otherwise 0 </returns>
        public int saveAppointment(AppointmentDataStock apptData, string userName, string salt)
        {
            try
            {
                AppointmentBl apptBl = new AppointmentBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                //validate appointment data
                this.validateAppointmentData(apptData);
                return apptBl.saveAppointment(apptData);

            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                //throw new FaultException<MessageFault>(msgFault, new FaultReason(msgFault.message));
                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }

        }

        //Change#29 - Behroz - 12/05/2012 - Start
        /// <summary>
        /// This function saves the appointment in the database for Gas
        /// </summary>
        /// <param name="apptData">The object of appointment</param>
        /// <returns>the appointment id if successfully save otherwise 0 </returns>
        public int saveAppointmentGas(AppointmentDataGas apptData, string userName, string salt)
        {
            try
            {
                AppointmentBl apptBl = new AppointmentBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                //validate appointment data - NEEDS RECHECKING
                this.validateAppointmentDataGas(apptData);
                return apptBl.saveAppointmentGas(apptData);

            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                //throw new FaultException<MessageFault>(msgFault, new FaultReason(msgFault.message));
                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }

        }
        //Change#29 - Behroz - 12/05/2012 - End
        #endregion

        #region validate Appointment Data
        /// <summary>
        /// This function validates the appointment data such as appointment type, appointment status, surveyour availability
        /// </summary>
        /// <param name="apptData">the object of appointment data</param>
        private void validateAppointmentData(AppointmentDataStock apptData)
        {
            //validate the appointment type
            if (!apptData.appointmentType.Equals(null) && !apptData.appointmentType.Equals(String.Empty))
            {
                if (apptData.validateAppointmentTypes(apptData.appointmentType) == false)
                {
                    ErrorFaultSetGet.setErrorFault(String.Format(MessageConstants.AppointTypeInvalidValueMsg, apptData.appointmentType), true, ErrorCodesConstants.AppointmentTypeInvalidSelectionCode);
                    throw new ArgumentException(String.Format(MessageConstants.AppointTypeInvalidValueMsg, apptData.appointmentType), "appointmentType");
                }
            }

            //validate the appointment progress status
            if (apptData.validateAppointmentProgressStatus(apptData.appointmentStatus) == false)
            {
                ErrorFaultSetGet.setErrorFault(String.Format(MessageConstants.AppointProgStatusInvalidValueMsg, apptData.appointmentStatus), true, ErrorCodesConstants.AppointmentProgStatusInvalidSelectionCode);
                throw new ArgumentException(String.Format(MessageConstants.AppointProgStatusInvalidValueMsg, apptData.appointmentStatus), "appointmentStatus");
            }

            //validate the surveyour availability status
            if (apptData.validateSurveyourAvailabilityStatus(apptData.surveyourAvailability) == false)
            {
                ErrorFaultSetGet.setErrorFault(String.Format(MessageConstants.SurveyourStatusInvalidValueMsg, apptData.surveyourAvailability), true, ErrorCodesConstants.SurveyourStatusInvalidSelectionCode);
                throw new ArgumentException(String.Format(MessageConstants.SurveyourStatusInvalidValueMsg, apptData.surveyourAvailability), "surveyourAvailability");
            }

            //validate the survey type
            //if (apptData.surveyType != null)
            //{
            //    if (apptData.validateSurveyTypes(apptData.surveyType) == false)
            //    {
            //        ErrorFaultSetGet.setErrorFault(String.Format(MessageConstants.SurveyourTypeInvalidValueMsg, apptData.surveyType), true, ErrorCodesConstants.SurveyTypeInvalidSelectionCode);
            //        throw new ArgumentException(String.Format(MessageConstants.SurveyourTypeInvalidValueMsg, apptData.surveyType), "surveyType");
            //    }
            //}

        }

        /// <summary>
        /// This function validates the appointment data such as appointment type, appointment status, surveyour availability
        /// </summary>
        /// <param name="apptData">the object of appointment data</param>
        private void validateAppointmentDataGas(AppointmentDataGas apptData)
        {
            //confirm the validations and apply

            //validate the appointment type
            //if (!apptData.appointmentType.Equals(null) && !apptData.appointmentType.Equals(String.Empty))
            //{
            //    if (apptData.validateAppointmentTypes(apptData.appointmentType) == false)
            //    {
            //        ErrorFaultSetGet.setErrorFault(String.Format(MessageConstants.AppointTypeInvalidValueMsg, apptData.appointmentType), true, ErrorCodesConstants.AppointmentTypeInvalidSelectionCode);
            //        throw new ArgumentException(String.Format(MessageConstants.AppointTypeInvalidValueMsg, apptData.appointmentType), "appointmentType");
            //    }
            //}

            //validate the appointment progress status
            if (apptData.validateAppointmentProgressStatus(apptData.appointmentStatus) == false)
            {
                ErrorFaultSetGet.setErrorFault(String.Format(MessageConstants.AppointProgStatusInvalidValueMsg, apptData.appointmentStatus), true, ErrorCodesConstants.AppointmentProgStatusInvalidSelectionCode);
                throw new ArgumentException(String.Format(MessageConstants.AppointProgStatusInvalidValueMsg, apptData.appointmentStatus), "appointmentStatus");
            }

            //validate the surveyour availability status
            //if (apptData.validateSurveyourAvailabilityStatus(apptData. surveyourAvailability) == false)
            //{
            //    ErrorFaultSetGet.setErrorFault(String.Format(MessageConstants.SurveyourStatusInvalidValueMsg, apptData.surveyourAvailability), true, ErrorCodesConstants.SurveyourStatusInvalidSelectionCode);
            //    throw new ArgumentException(String.Format(MessageConstants.SurveyourStatusInvalidValueMsg, apptData.surveyourAvailability), "surveyourAvailability");
            //}
        }
        #endregion

        //Change Ticket# 8,9,10 - Zeeshan - 05/03/2013 - Start
        //Code modified by Abdul Wahhab - 04/06/2013 - START
        #region get Stock Appointments by User
        /// <summary>
        /// This function returns all stock appointments of the particular user
        /// </summary>
        /// <param name="username">username </param>
        /// <param name="filter">appointment type</param>
        /// <param name="salt">security salt</param>
        /// <returns>the list of appointments </returns>
        //public List<AppointmentData> getUserAppointments(string userName, string filter, string salt)
        public List<AppointmentListStock> getStockAppointmentsByUser(string userName, string filter, string salt)
        {
            try
            {
                AppointmentBl apptBl = new AppointmentBl();
                List<AppointmentListStock> apptData = new List<AppointmentListStock>();
                AuthenticationBl authBl = new AuthenticationBl();

                if (authBl.checkForValidSession(userName, salt))
                {
                    //apptData = apptBl.getUserAppointments(SecurityEncryption.DecryptData(userName), filter);
                    apptData = apptBl.getStockAppointmentsByUser(SecurityEncryption.DecryptData(userName), filter);
                }
                else
                {
                    apptData.Add(returnInValidAppointment());
                }
                return apptData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
                //throw ex;

            }
        }
        //Code modified by Abdul Wahhab - 04/06/2013 - END
        //Change Ticket# 8,9,10 - Zeeshan - 05/03/2013 - End

        //Change#28 - Behroz - 12/05/2012 - Start
        //Code modified by Abdul Wahhab - 04/06/2013 - START
        /// <summary>
        /// This function returns all the appointments of the particular user for type GAS
        /// </summary>
        /// <param name="username">username </param>
        /// <param name="filter">appointment type</param>
        /// <param name="salt">security salt</param>
        /// <returns>the list of appointments </returns>
        public List<AppointmentListGas> getUserAppointmentsGas(string userName, string filter, string salt)
        {
            try
            {
                AppointmentBl apptBl = new AppointmentBl();
                List<AppointmentListGas> apptData = new List<AppointmentListGas>();
                AuthenticationBl authBl = new AuthenticationBl();

                if (authBl.checkForValidSession(userName, salt))
                {
                    apptData = apptBl.getUserAppointmentsGas(SecurityEncryption.DecryptData(userName), filter);
                }
                else
                {
                    apptData.Add(returnInValidAppointmentGas());
                }

                return apptData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }
        //Code modified by Abdul Wahhab - 04/06/2013 - END
        //Change#28 - Behroz - 12/05/2012 - End
        #endregion

        #region get All Users
        /// <summary>
        /// This function returns all the users of rsl manager database
        /// </summary>        
        /// <returns>list of all users</returns>

        public List<UserData> getAllUsers(string userName, string salt)
        {
            try
            {
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                AppointmentBl apptBl = new AppointmentBl();
                List<UserData> userData = new List<UserData>();

                userData = apptBl.getAllUsers();
                return userData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                //throw new FaultException<MessageFault>(msgFault, new FaultReason(msgFault.message));
                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);

            }
        }

        //Change#17 - Behroz - 19/12/2012 - Start

        /// <summary>
        /// This function returns all the users of rsl manager database
        /// </summary>        
        /// <returns>list of all users</returns>
        public List<UserData> getAllUsersGas(string userName, string salt)
        {
            try
            {
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                AppointmentBl apptBl = new AppointmentBl();
                List<UserData> userData = new List<UserData>();

                userData = apptBl.getAllUsersGas();
                return userData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }

        //Change#17 - Behroz - 19/12/2012 - End

        #endregion

        #region get Total number of No Entries and appointments

        /// <summary>
        /// This function returns all No Entries and appointments
        /// </summary> 
        /// <returns>List of No Entries data objects</returns>
        public AppointmentInfoData getAllNoEntriesAppointments(string propertyID, string userName, string salt)
        {
            try
            {
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                AppointmentBl apptBl = new AppointmentBl();

                AppointmentInfoData totalNoEntries = apptBl.getTotalNoEntries(propertyID);
                return totalNoEntries;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);

            }
        }

        #endregion

        #region save a No Entry

        /// <summary>
        /// This function saves a No Entry
        /// </summary>
        /// <param>No entry data object</param>
        /// <returns>Id of no entry in case of success else 0</returns>
        public int saveNoEntry(NoEntryData noEntryData, string userName, string salt)
        {
            try
            {
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                AppointmentBl apptBl = new AppointmentBl();

                int status = apptBl.saveNoEntry(noEntryData);
                return status;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);

            }
        }

        //Change#10 - Behroz -12/11/2012 - Start
        /// <summary>
        /// This function saves a No Entry
        /// </summary>
        /// <param>No entry data object</param>
        /// <returns>Id of no entry in case of success else 0</returns>
        public int saveNoEntryGas(NoEntryData noEntryData, string userName, string salt)
        {
            try
            {
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                AppointmentBl apptBl = new AppointmentBl();

                int status = apptBl.saveNoEntryGas(noEntryData);
                return status;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);

            }
        }
        //Change#10 - Behroz -12/11/2012 - End
        #endregion

        #region get Index Screen Info
        /// <summary>
        /// This function returns Index Screen Info
        /// </summary>        
        /// <returns>Index Screen Info</returns>

        public IndexStatusData GetIndexScreenStatus(int AppointmentID, string userName, string salt)
        {
            try
            {
                AppointmentBl apptBl = new AppointmentBl();
                IndexStatusData apptData = new IndexStatusData();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);

                apptData = apptBl.GetIndexScreenStatus(AppointmentID);
                return apptData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);

            }
        }

        //Change#33 - Behroz - 20/12/2012 - Start
        /// <summary>
        /// This function returns Index Screen Info for Gas
        /// </summary>        
        /// <returns>Index Screen Info</returns>
        public IndexStatusData GetIndexScreenStatusGas(int AppointmentID, string userName, string salt)
        {
            try
            {
                AppointmentBl apptBl = new AppointmentBl();
                IndexStatusData apptData = new IndexStatusData();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);

                apptData = apptBl.GetIndexScreenStatusGas(AppointmentID);
                return apptData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);

            }
        }
        //Change#33 - Behroz - 20/12/2012 - End
        #endregion

        #region update Appointment
        /// <summary>
        /// This function updates the appointment in the database
        /// </summary>
        /// <param name="apptData">The object of appointment</param>
        /// <returns>returns </returns>
        public bool updateAppointment(AppointmentDataStock apptData, string userName, string salt)
        {

            try
            {
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                AppointmentBl apptBl = new AppointmentBl();
                return apptBl.updateAppointment(apptData);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                //throw new FaultException<MessageFault>(msgFault, new FaultReason(msgFault.message));
                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }

        }

        //Change#11 - Behroz - 12/12/2012 - Start
        /// <summary>
        /// This function updates the appointment in the database
        /// </summary>
        /// <param name="apptData">The object of appointment</param>
        /// <returns>returns </returns>
        public bool updateAppointmentGas(AppointmentDataGas apptData, string userName, string salt)
        {
            try
            {
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                AppointmentBl apptBl = new AppointmentBl();
                return apptBl.updateAppointment(apptData);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                //throw new FaultException<MessageFault>(msgFault, new FaultReason(msgFault.message));
                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }

        }
        //Change#11 - Behroz - 12/12/2012 - End
        #endregion

        #region delete Appointment
        /// <summary>
        /// This function deletes the appointment in the database
        /// </summary>
        /// <param name="appointmentId">Appointment Id</param>
        /// <returns>returns true based on success and false otherwise </returns>
        public bool deleteAppointment(int appointmentId, string userName, string salt)
        {
            try
            {
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                AppointmentBl apptBl = new AppointmentBl();

                return apptBl.deleteAppointment(appointmentId);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                //throw new FaultException<MessageFault>(msgFault, new FaultReason(msgFault.message));
                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }

        }
        #endregion

        #region get Appointment
        /// <summary>
        /// This function returns an appointment based on appointment id
        /// </summary>
        /// <param name="appointmentid">appointment id </param>
        /// <returns>appointment data </returns>

        public AppointmentDataStock getAppointment(int appointmentid)
        {
            try
            {
                AppointmentBl apptBl = new AppointmentBl();
                AppointmentDataStock apptData = new AppointmentDataStock();

                apptData = apptBl.getAppointment(appointmentid);
                return apptData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);

            }
        }

        #endregion

        #region get Appointment
        //Code modified by Abdul Wahhab - 04/06/2013 - START
        /// <summary>
        /// This function returns an Invalid appointment for security
        /// </summary>
        /// <param name="appointmentid"></param>
        /// <returns>appointment data </returns>

        public AppointmentListStock returnInValidAppointment()
        {
            try
            {
                AppointmentListStock apptData = new AppointmentListStock();

                apptData.appointmentId = -1;
                return apptData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);

            }
        }
        //Code modified by Abdul Wahhab - 04/06/2013 - END

        //Change#28 - Behroz - 12/05/2012 - Start
        //Code modified by Abdul Wahhab - 04/06/2013 - START
        public AppointmentListGas returnInValidAppointmentGas()
        {
            try
            {
                AppointmentListGas apptData = new AppointmentListGas();

                apptData.appointmentId = -1;
                return apptData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);

            }
        }
        //Code modified by Abdul Wahhab - 04/06/2013 - END

        public StatusData returnInValidStatusData()
        {
            try
            {
                StatusData apptData = new StatusData();

                apptData.statusId = -1;
                return apptData;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);

            }
        }
        //Change#28 - Behroz - 12/05/2012 - End

        #endregion

    }
}
