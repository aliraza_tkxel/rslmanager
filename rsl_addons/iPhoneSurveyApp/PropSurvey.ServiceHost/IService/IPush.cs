﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ComponentModel;
using PropSurvey.Utilities.Constants;
using System.ServiceModel.Web;

namespace PropSurvey.ServiceHost.IService
{
    // Start the service and browse to http://<machine_name>:<port>/appliances/help to view the service's generated help page
    // NOTE: By default, a new instance of the service is created for each call; change the InstanceContextMode to Single if you want
    // a single instance of the service to process all calls.	
    [ServiceContract]
    // NOTE: If the service is renamed, remember to update the global.asax.cs file
    interface IPush
    {

        #region send Push Notification for Fault Appointment

        /// <summary>
        /// This function will send push notification for fault appointment
        /// </summary>
        /// <returns>Success or failure</returns>
        [OperationContract]
        [Description(ServiceDescConstants.SendPushNotificationFault)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.SendPushNotificationFault)]
        bool sendPushNotificationFault(int appointmentId, int type);

        #endregion

    }
}
