﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ServiceModel;
using System.ServiceModel.Web;
using PropSurvey.Contracts.Data;
using PropSurvey.Utilities.Constants;
using System.IO;


namespace PropSurvey.ServiceHost.IService
{
    // Start the service and browse to http://<machine_name>:<port>/appliances/help to view the service's generated help page
    // NOTE: By default, a new instance of the service is created for each call; change the InstanceContextMode to Single if you want
    // a single instance of the service to process all calls.	
    [ServiceContract]
    // NOTE: If the service is renamed, remember to update the global.asax.cs file

    interface IFaults
    {

        #region get IFaults Data
        /// <summary>
        /// This function returns the IFaults data
        /// </summary>
        /// <returns>List of IFaults data objects</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetFaults)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetFaults)]
        List<FaultsData> getFaultsData(int AppointmentID, string userName, string salt);

        //Change#18 - Behroz - 19/12/2012 - Start
        /// <summary>
        /// This function returns the IFaults data for gas
        /// </summary>
        /// <returns>List of IFaults data objects</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetFaultsGas)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetFaultsGas)]
        List<FaultsDataGas> getFaultsDataGas(int journalId, string userName, string salt);
        //Change#18 - Behroz - 19/12/2012 - End
        #endregion

        #region get general comments Data
        /// <summary>
        /// This function returns general comments
        /// </summary>
        /// <returns>List of general comments objects</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetGeneralComments)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetGeneralComments)]
        List<FaultsData> getGeneralComments(int AppointmentID, string userName, string salt);

        //Change#21 - Behroz - 19/12/2012 - Start
        /// <summary>
        /// This function returns general comments for gas
        /// </summary>
        /// <returns>List of general comments objects</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetGeneralCommentsGas)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetGeneralCommentsGas)]
        List<FaultsDataGas> getGeneralCommentsGas(int journalId, string userName, string salt);
        //Change#21 - Behroz - 19/12/2012 - End

        #endregion

        #region get faults data only
        /// <summary>
        /// This function returns faults data
        /// </summary>
        /// <returns>List of faults data objects</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetFaultsOnly)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetFaultsOnly)]
        List<FaultsData> getFaultsOnly(int AppointmentID, string userName, string salt);

        //Change#22 - Behroz - 19/12/2012 - Start
        /// <summary>
        /// This function returns faults data for gas
        /// </summary>
        /// <returns>List of faults data objects</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetFaultsOnlyGas)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetFaultsOnlyGas)]
        List<FaultsDataGas> getFaultsOnlyGas(int journalId, string userName, string salt);
        //Change#22 - Behroz - 19/12/2012 - End

        #endregion

        #region save Faults Data
        /// <summary>
        /// This function saves the Faults Data in the database
        /// </summary>
        /// <param name="appData">The object of Faults Data</param>
        /// <returns>the Faults id if successfully save otherwise 0 </returns>

        [OperationContract]
        [Description(ServiceDescConstants.SaveFaults)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.SaveFaults)]
        int saveFaultsData(FaultsData faultData, string userName, string salt);

        //Change#19 - Behroz - 19/12/2012 - Start
        /// <summary>
        /// This function saves the Faults Data in the database for gas
        /// </summary>
        /// <param name="appData">The object of Faults Data</param>
        /// <returns>the Faults id if successfully save otherwise 0 </returns>
        [OperationContract]
        [Description(ServiceDescConstants.SaveFaultsGas)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.SaveFaultsGas)]
        int saveFaultsDataGas(FaultsDataGas faultData, string userName, string salt);
        //Change#19 - Behroz - 19/12/2012 - End
        #endregion

        #region update fault Data
        /// <summary>
        /// This function updates fault Data in the database
        /// </summary>
        /// <param name="appData">The object of fault Data</param>
        /// <returns>true if successfully save otherwise 0 </returns>
        
        [OperationContract]
        [Description(ServiceDescConstants.UpdateFaults)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.UpdateFaults)]
        bool updateFaultData(FaultsData faultData, string userName, string salt);

        //Change#20 - Behroz - 19/12/2012 - Start
        
        /// <summary>
        /// This function updates fault Data in the database for gas
        /// </summary>
        /// <param name="appData">The object of fault Data</param>
        /// <returns>true if successfully save otherwise 0 </returns>
        [OperationContract]
        [Description(ServiceDescConstants.UpdateFaultsGas)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.UpdateFaultsGas)]
        bool updateFaultDataGas(FaultsDataGas faultData, string userName, string salt);

        //Change#20 - Behroz - 19/12/2012 - End
        
        #endregion

        #region "Get Defect Category"

        //Change#15 - Behroz - 12/17/2012 - Start
        /// <summary>
        /// This function returns all the defects category
        /// </summary>
        /// <returns>List of Defect Category Data objects</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetDefectCategory)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetDefectCategory)]
        List<DefectCategoryData> getDefectCategory(string userName, string salt);
        //Change#15 - Behroz - 12/17/2012 - End
        #endregion

        #region "Fault Image"
        /// <summary>
        /// This funciton updates the fault image in the database
        /// </summary>
        /// <param name="stream">image stream</param>
        /// <param name="propertyId">property Id</param>
        /// <param name="surveyId">survey id</param>
        /// <param name="itemId">item Id</param>
        /// <param name="fileName">file name image</param>
        /// <param name="pictureTag">picutre tag , this 'll be like this: Internal;Roof;Dwelling</param>
        /// <param name="isDefault">true or false</param>
        /// <returns>true or false upon successfull saving on disk and on db</returns>
        //[OperationContract]
        //[Description(ServiceDescConstants.UpdatePropertyImageGas)]
        //[WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.UpdatePropertyImageGas, BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        //bool updateFaultImage(Stream stream, string propertyId, int appointmentId, string fileName, int updatedBy, string fileSize, string pictureTag, bool isDefault = false);
        #endregion

        #region "Get Fault Id"
        //Change#51 - Behroz - 16/01/2013 - Start
        /// <summary>
        /// This function returns a new fault id
        /// </summary>
        /// <returns>fault id</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetFaultId)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetFaultId)]
        int getFaultId(string propertyId,string userName, string salt);
        //Change#51 - Behroz - 16/01/2013 - End
        #endregion

        #region "Delete Fault"
        /// <summary>
        /// This function deletes the fault
        /// </summary>
        /// <returns>fault id</returns>
        [OperationContract]
        [Description(ServiceDescConstants.DeleteFault)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.DeleteFaultId)]
        bool deleteFaultId(int faultId, string userName, string salt);
        #endregion

        #region "Save Image"
        //Change#53 - Behroz - 17/01/2013 - Start
        /// <summary>
        /// This function saves the path of the image in database and the image on the server.
        /// </summary>
        /// <returns>fault id</returns>
        [OperationContract]
        [Description(ServiceDescConstants.SaveFaultImage)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.SaveFaultImage)]
        bool saveFaultImage(Stream stream, int faultId, string fileName,string fileSize, string userName, string salt);
        //Change#53 - Behroz - 17/01/2013 - End
        #endregion
    }
}
