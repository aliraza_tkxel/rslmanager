﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ServiceModel;
using System.ServiceModel.Web;
using PropSurvey.Contracts.Data;
using PropSurvey.Utilities.Constants;
using System.IO;


namespace PropSurvey.ServiceHost.IService
{
    // Start the service and browse to http://<machine_name>:<port>/appliances/help to view the service's generated help page
    // NOTE: By default, a new instance of the service is created for each call; change the InstanceContextMode to Single if you want
    // a single instance of the service to process all calls.	
    [ServiceContract]
    // NOTE: If the service is renamed, remember to update the global.asax.cs file

    interface IIssuedReceivedBy
    {

        #region get IssuedReceivedBy Data
        /// <summary>
        /// This function returns the IssuedReceivedBy data
        /// </summary>
        /// <returns>List of IssuedReceivedBy data objects</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetInstallationPipework)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetInstallationPipework)]
        IssuedReceivedByData getIssuedReceivedByFormData(int AppointmentID, string userName, string salt);

        //Change#12 - Behroz - 12/17/2012 - Start
        /// <summary>
        /// This function returns the IssuedReceivedBy data for gas
        /// </summary>
        /// <returns>List of IssuedReceivedBy data objects</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetIssuedReceivedByGas)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetInstallationPipeworkGas)]
        LGSRData getIssuedReceivedByFormDataGas(int journalId, string userName, string salt);
        //Change#12 - Behroz - 12/17/2012 - End

        #endregion

        #region save IssuedReceivedBy Data
        /// <summary>
        /// This function saves the IssuedReceivedBy Data in the database
        /// </summary>
        /// <param name="appData">The object of IssuedReceivedBy Data</param>
        /// <returns>the IssuedReceivedBy id if successfully save otherwise 0 </returns>

        [OperationContract]
        [Description(ServiceDescConstants.SaveInstallationPipework)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.SaveInstallationPipework)]
        int saveIssuedReceivedByData(IssuedReceivedByData workData, string userName, string salt);

        //Change#12 - Behroz - 12/13/2012 - Start
        /// <summary>
        /// This function saves the IssuedReceivedBy Data in the database for gas
        /// </summary>
        /// <param name="appData">The object of IssuedReceivedBy Data</param>
        /// <returns>the IssuedReceivedBy id if successfully save otherwise 0 </returns>
        [OperationContract]
        [Description(ServiceDescConstants.SaveInstallationPipeworkGas)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.SaveInstallationPipeworkGas)]
        int saveIssuedReceivedByDataGas(LGSRData workData, string userName, string salt);
        //Change#12 - Behroz - 12/13/2012 - End
        #endregion

        #region update IssuedReceivedBy Data
        /// <summary>
        /// This function updates IssuedReceivedBy Data in the database
        /// </summary>
        /// <param name="appData">The object of IssuedReceivedBy Data</param>
        /// <returns>true if successfully save otherwise 0 </returns>
        
        [OperationContract]
        [Description(ServiceDescConstants.UpdateInstallationPipework)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.UpdateInstallationPipework)]
        bool updateIssuedReceivedByData(IssuedReceivedByData insData, string userName, string salt);

        //Change#35 - Behroz - 12/17/2012 - Start
        /// <summary>
        /// This function updates IssuedReceivedBy Data in the database
        /// </summary>
        /// <param name="appData">The object of IssuedReceivedBy Data</param>
        /// <returns>true if successfully save otherwise 0 </returns>
        [OperationContract]
        [Description(ServiceDescConstants.UpdateIssueReceivedByGas)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.UpdateIssuedReceivedGas)]
        bool updateIssuedReceivedByDataGas(LGSRData insData, string userName, string salt);

        /// <summary>
        /// This function updates IssuedReceivedBy Data in the database
        /// </summary>
        /// <param name="appData">The object of IssuedReceivedBy Data</param>
        /// <returns>true if successfully save otherwise 0 </returns>
        [OperationContract]
        [Description(ServiceDescConstants.UpdateCP12DocumentGas)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.UpdateCP12DocumentGas)]
        bool updateCP12Document(Stream stream, int id,string userName, string salt, bool tenantHanded);

        //Change#35 - Behroz - 12/17/2012 - End
        #endregion

    }
}
