﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ComponentModel;
using System.ServiceModel.Web;
using PropSurvey.Contracts.Data;
using PropSurvey.Utilities.Constants;

namespace PropSurvey.ServiceHost.IService
{
    // Start the service and browse to http://<machine_name>:<port>/survey/help to view the service's generated help page
    // NOTE: By default, a new instance of the service is created for each call; change the InstanceContextMode to Single if you want
    // a single instance of the service to process all calls.	
    [ServiceContract]
    // NOTE: If the service is renamed, remember to update the global.asax.cs file
    public interface ISurvey
    {
        #region save Survey Form

        /// <summary>
        /// This function save the survey form. Data should be post at specific url. 
        /// </summary>
        /// <param name="survData">survey data object</param>
        /// <returns>it returns survey id or zero</returns>

        [OperationContract]
        [Description(ServiceDescConstants.SaveSurveyForm)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.SaveSurveyForm)]
        bool saveSurveyForm(SurveyData survData, string userName, string salt);        
        #endregion

        #region get Saved Survey Form
        /// <summary>
        /// this funciton returns the saved survey form against appointment id and property portion name
        /// </summary>
        /// <param name="appointmentId">appointment id</param>
        /// <param name="propertyPortionName">property portion name is the semicolon separated portions of name e.g Internal;Rooof;Dwelling</param>
        /// <returns>it returns the saved data of property survey form</returns>

        [OperationContract]
        [Description(ServiceDescConstants.GetSavedSurveyForm)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "GET", UriTemplate = UriTemplateConstants.GetSavedSurveyForm)]
        SurveyData getSavedSurveyForm(int appointmentId, string propertyPortionName);
        #endregion


        #region get All Saved Survey Forms
        /// <summary>
        /// this funciton returns all the saved survey forms against appointment id
        /// </summary>
        /// <param name="appointmentId">appointment id</param>        
        /// <returns>it returns the list of saved forms data of property survey form</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetAllSavedSurveyForms)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "GET", UriTemplate = UriTemplateConstants.GetAllSavedSurveyForms)]
        List<SurveyData> getAllSavedSurveyForms(int appointmentId);
        #endregion


        ///123456///
        #region get Surveyor Survey Forms
        /// <summary>
        /// this funciton returns all the survey forms against a surveyor username
        /// </summary>
        /// <param name="userName">surveyor username</param>        
        /// <returns>it returns the list of saved forms data of property survey form against a surveyor</returns>
        [OperationContract]
        [Description(ServiceDescConstants.getSurveyorSurveyForms)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "GET", UriTemplate = UriTemplateConstants.getSurveyorSurveyForms)]
        List<SurveyData> getSurveyorSurveyForms(string userName);
        #endregion
    }
}
