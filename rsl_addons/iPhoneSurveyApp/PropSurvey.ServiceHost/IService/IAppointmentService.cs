﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ServiceModel;
using System.ServiceModel.Web;
using PropSurvey.Contracts.Data;
using PropSurvey.Utilities.Constants;
using System.IO;


namespace PropSurvey.ServiceHost.IService
{
    // Start the service and browse to http://<machine_name>:<port>/appointment/help to view the service's generated help page
    // NOTE: By default, a new instance of the service is created for each call; change the InstanceContextMode to Single if you want
    // a single instance of the service to process all calls.	
    [ServiceContract]
    // NOTE: If the service is renamed, remember to update the global.asax.cs file
    public interface IAppointmentService
    {
        //Change Ticket# 8,9,10 - Zeeshan - 05/03/2013 - Start
        #region get All Stock Appointments

        /// <summary>
        /// This function returns all the appointments
        /// </summary>
        /// <returns>List of appointment data objects</returns>
        /// 
        [OperationContract]
        [Description(ServiceDescConstants.GetAllStockAppointments)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetAllStockAppointments)]
        List<AppointmentListStock> getAllStockAppointments(string userName, string salt); //Return type changed to AppointmentListStock by Abdul Wahhab - 04/06/2013
        //[OperationContract]
        //[Description(ServiceDescConstants.GetAllAppointments)]
        //[WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetAllAppointments)]
        //List<AppointmentData> getAllAppointments(string userName, string salt);

        #endregion
        //Change Ticket# 8,9,10 - Zeeshan - 05/03/2013 - End


        #region get All Appointments of all types by user

        /// <summary>
        /// This function returns all the appointments
        /// </summary>
        /// <returns>List of appointments in stream</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetAllAppointmentsByUser)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetAllAppointmentsByUser)]
        Stream getAllAppointmentsByUser(string userName, string salt, string filter);

        #endregion

        #region get Fault Repair List

        /// <summary>
        /// This function returns the list of all fault repairs
        /// </summary>
        /// <returns>List of Fault Repair in stream</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetFaultRepairList)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetFaultRepairList)]
        List<FaultRepairData> getFaultRepairList(string userName, string salt);

        #endregion

        #region get pause reason list

        /// <summary>
        /// This function returns reason list for pausing a job
        /// </summary>
        /// <returns>List of reasons</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetPauseReasonList)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetPauseReasonList)]
        List<string> getPauseReasonList(string userName, string salt);

        #endregion

        #region update Stock Appointment Progress Status
        /// <summary>
        /// This function update the status of Stock appointment. This function use post method to accept data. 
        /// </summary>
        /// <param name="apptData">This fucntion accepts the AppointmentData's object </param>
        /// <returns>It returns true or false in update is successful</returns>
        [OperationContract]
        [Description(ServiceDescConstants.UpdateAppointmentProgressStatus)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.UpdateAppointmentProgressStatus)]
        bool updateStockAppointmentProgressStatus(AppointmentDataStock apptData);

        [OperationContract]
        [Description(ServiceDescConstants.UpdateFaultAppointmentProgressStatus)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.UpdateFaultAppointmentProgressStatus)]
        ResultBoolData updateFaultAppointmentProgressStatus(string userName, string appointmentId, string progressStatus, string salt);
        // Return type changed by Abdul Wahhab - 17/06/2013
        #endregion

        #region update Appointment
        /// <summary>
        /// This function updates the appointment in the database
        /// </summary>
        /// <param name="apptData">The object of appointment</param>
        /// <returns>returns true on success and false otherwise </returns>

        [OperationContract]
        [Description(ServiceDescConstants.UpdateCustomerContactFaultAppointment)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.UpdateCustomerContactFaultAppointment)]
        bool UpdateCustomerContactFromAppointment(CustomerData cusData, string userName, string salt);

        #endregion

        #region get And Set Appointment Progress Status

        /// <summary>
        /// This function checks the appointment progress status, If that is not started , it 'll start it otherwise it'll return false
        /// </summary>
        /// <param name="appointmentId">appointment id</param>
        /// <param name="userName">user name </param>
        /// <returns>it return true if appointment status changed from NotStarted to Pending other wise false</returns>

        [OperationContract]
        [Description(ServiceDescConstants.GetAndSetStockAppointmentProgressStatus)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetAndSetStockAppointmentProgressStatus)]
        bool getAndSetStockAppointmentProgressStatus(string appointmentId, string userName);

        //Change#14 - Behroz - 12/07/2012 - Start
        [OperationContract]
        [Description(ServiceDescConstants.GetAndSetGasAppointmentProgressStatus)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetAndSetGasAppointmentProgressStatus)]
        bool getAndSetGasAppointmentProgressStatus(string appointmentId, int userId);
        //Change#14 - Behroz - 12/07/2012 - End

        #endregion

        #region Get and Update Fault Status
        /// <summary>
        /// This function get fault appointment progress status
        /// </summary>
        /// <param name="appointmentId">appointment id</param>
        /// <param name="userName">user name </param>
        /// <returns>return the fault appointment progress status</returns>

        [OperationContract]
        [Description(ServiceDescConstants.GetFaultJobStatus)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetFaultJobStatus)]
        Stream getFaultJobStatus(string appointmentId, string userName);


        /// <summary>
        /// This function set fault appointment progress status
        /// </summary>
        /// <param name="appointmentId">appointment id</param>
        /// <param name="userName">user name </param>
        /// <returns>return the fault appointment progress status</returns>

        [OperationContract]
        [Description(ServiceDescConstants.UpdateFaultJobStatus)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.UpdateFaultJobStatus)]
        ResultBoolData updateFaultJobStatus(FaultStatusData faultStatusData, string userName, string salt);
        // Return type changed by Abdul Wahhab - 17/06/2013

        // Code added by Abdul Wahhab - 11/06/2013 - START

        /// <summary>
        /// This function set fault appointment progress status to NoEntry
        /// </summary>
        /// <param name="appointmentId">appointment id</param>
        /// <param name="userName">user name </param>
        /// <returns>return the fault appointment progress status</returns>

        [OperationContract]
        [Description(ServiceDescConstants.NoEntryAppointmentStatus)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.NoEntryAppointmentStatus)]
        ResultBoolData noEntryAppointmentStatus(FaultStatusData faultStatusData, string userName, string salt);
        // Return type changed by Abdul Wahhab - 17/06/2013

        // Code added by Abdul Wahhab - 11/06/2013 - END

        #endregion

        #region save Appointment
        /// <summary>
        /// This function saves the appointment in the database
        /// </summary>
        /// <param name="apptData">The object of appointment</param>
        /// <returns>the appointment id if successfully save otherwise 0 </returns>

        [OperationContract]
        [Description(ServiceDescConstants.SaveAppointment)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.SaveAppointment)]
        int saveAppointment(AppointmentDataStock apptData, string userName, string salt);

        //Change Behroz - 12/04/2012 - Start
        /// <summary>
        /// This function saves the appointment related to Gas in the database
        /// </summary>
        /// <param name="apptData"></param>
        /// <param name="userName"></param>
        /// <param name="salt"></param>
        /// <returns></returns>
        [OperationContract]
        [Description(ServiceDescConstants.SaveAppointmentGas)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.SaveAppointmentGas)]
        int saveAppointmentGas(AppointmentDataGas apptData, string userName, string salt);
        //Change Behroz - 12/04/2012 - End

        #endregion

        //Change Ticket# 8,9,10 - Zeeshan - 05/03/2013 - Start
        #region get Stock Appointments by User
        /// <summary>
        /// This function returns all the appointments of the particular user
        /// </summary>
        /// <param name="username">username </param>
        /// <returns>the list of appointments </returns>

        [OperationContract]
        [Description(ServiceDescConstants.GetStockAppointmentsByUser)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "GET", UriTemplate = UriTemplateConstants.GetStockAppointmentsByUser)]
        List<AppointmentListStock> getStockAppointmentsByUser(string userName, string filter, string salt); //Return type changed to AppointmentListStock by Abdul Wahhab - 04/06/2013
        //[OperationContract]
        //[Description(ServiceDescConstants.GetUserAppointments)]
        //[WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "GET", UriTemplate = UriTemplateConstants.GetUserAppointments)]
        //List<AppointmentData> getUserAppointments(string userName, string filter, string salt);
        //Change Ticket# 8,9,10 - Zeeshan - 05/03/2013 - End
        #endregion

        //Change - Behroz - 12/05/2012 - Start
        #region

        /// <summary>
        /// This function returns all the appointments of the particular user
        /// </summary>
        /// <param name="username">username </param>
        /// <returns>the list of appointments </returns>

        [OperationContract]
        [Description(ServiceDescConstants.GetUserAppointmentsGas)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "GET", UriTemplate = UriTemplateConstants.GetUserAppointmentsGas)]
        List<AppointmentListGas> getUserAppointmentsGas(string userName, string filter, string salt); //Return type changed to AppointmentListGas by Abdul Wahhab - 04/06/2013

        //[OperationContract]
        //[Description(ServiceDescConstants.GetUserAppointmentsAll)]
        //[WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "GET", UriTemplate = UriTemplateConstants.GetUserAppointmentsAll)]
        //List<AppointmentDataAll> getUserAppointmentsAll(string userName, string filter, string salt);
        //Change - Behroz - 12/05/2012 - End
        #endregion

        //Change - Behroz - 12/10/2012 - Start
        #region "Get Status"

        [OperationContract]
        [Description(ServiceDescConstants.GetStatus)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "GET", UriTemplate = UriTemplateConstants.GetStatus)]
        List<StatusData> getStatus(string userName, string salt);

        #endregion
        //Change - Behroz - 12/10/2012 - End

        #region get All Users
        /// <summary>
        /// This function returns all the users of rsl manager database
        /// </summary>        
        /// <returns>list of all users</returns>

        [OperationContract]
        [Description(ServiceDescConstants.GetAllUsers)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "GET", UriTemplate = UriTemplateConstants.GetAllUsers)]
        List<UserData> getAllUsers(string userName, string salt);

        //Change#17 - Behroz - 19/12/2012 - Start
        /// <summary>
        /// This function returns all the users of rsl manager database for gas
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="salt"></param>
        /// <returns>list of all users</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetAllUsersGas)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "GET", UriTemplate = UriTemplateConstants.GetAllUsersGas)]
        List<UserData> getAllUsersGas(string userName, string salt);
        //Change#17 - Behroz - 19/12/2012 - End

        #endregion

        #region get Total number of No Entries

        /// <summary>
        /// This function returns all No Entries
        /// </summary>
        /// <returns>List of No Entries data objects</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetNoEntries)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.getNoEntries)]
        AppointmentInfoData getAllNoEntriesAppointments(string propertyID, string userName, string salt);

        #endregion

        #region save a No Entry

        /// <summary>
        /// This function saves a No Entry
        /// </summary>
        /// <param>No entry data object</param>
        /// <returns>Id of no entry in case of success else 0</returns>
        [OperationContract]
        [Description(ServiceDescConstants.SaveNoEntry)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.saveNoEntry)]
        int saveNoEntry(NoEntryData noEntryData, string userName, string salt);

        //Change#10 - Behroz -12/11/2012 - Start
        /// <summary>
        /// This function saves a No Entry when the button is clicked from the GAS Tab.
        /// </summary>
        /// <param>No entry data object</param>
        /// <returns>Id of no entry in case of success else 0</returns>
        [OperationContract]
        [Description(ServiceDescConstants.SaveNoEntryGas)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.saveNoEntryGas)]
        int saveNoEntryGas(NoEntryData noEntryData, string userName, string salt);
        //Change#10 - Behroz -12/11/2012 - End
        #endregion

        #region update Appointment
        /// <summary>
        /// This function updates the appointment in the database
        /// </summary>
        /// <param name="apptData">The object of appointment</param>
        /// <returns>returns true on success and false otherwise </returns>

        [OperationContract]
        [Description(ServiceDescConstants.UpdateAppointment)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.UpdateAppointment)]
        bool updateAppointment(AppointmentDataStock apptData, string userName, string salt);

        //Change#11 - Behroz - 12/12/2012 - Start
        /// <summary>
        /// This function updates the appointment in the database
        /// </summary>
        /// <param name="apptData">The object of appointment</param>
        /// <returns>returns true on success and false otherwise </returns>

        [OperationContract]
        [Description(ServiceDescConstants.UpdateAppointmentGas)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.UpdateAppointmentGas)]
        bool updateAppointmentGas(AppointmentDataGas apptData, string userName, string salt);
        //Change#11 - Behroz - 12/12/2012 - Start
        #endregion

        #region delete Appointment
        /// <summary>
        /// This function deletes the appointment in the database
        /// </summary>
        /// <param name="appointmentId">appointment id</param>
        /// <returns>returns true on success and false otherwise </returns>

        [OperationContract]
        [Description(ServiceDescConstants.DeleteAppointment)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "GET", UriTemplate = UriTemplateConstants.DeleteAppointment)]
        bool deleteAppointment(int appointmentId, string userName, string salt);

        #endregion

        #region get Index Screen Info
        /// <summary>
        /// This function returns Index Screen Info
        /// </summary>        
        /// <returns>Index Screen Info</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetIndexScreenInfo)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "GET", UriTemplate = UriTemplateConstants.GetIndexScreenInfo)]
        IndexStatusData GetIndexScreenStatus(int AppointmentID, string userName, string salt);

        //Change#28 - Behroz - 20/12/2012 - Start
        /// <summary>
        /// This function returns Index Screen Info
        /// </summary>        
        /// <returns>Index Screen Info</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetIndexScreenInfoGas)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "GET", UriTemplate = UriTemplateConstants.GetIndexScreenInfoGas)]
        IndexStatusData GetIndexScreenStatusGas(int AppointmentID, string userName, string salt);
        //Change#28 - Behroz - 20/12/2012 - End

        #endregion

        #region get Appointment by Id
        /// <summary>
        /// This function returns an appointment based on appointment id
        /// </summary>
        /// <param name="appointmentid">appointment id </param>
        /// <returns>appointment data </returns>

        [OperationContract]
        [Description(ServiceDescConstants.GetAppointment)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "GET", UriTemplate = UriTemplateConstants.GetAppointment)]
        AppointmentDataStock getAppointment(int appointmentid);

        #endregion

    }
}