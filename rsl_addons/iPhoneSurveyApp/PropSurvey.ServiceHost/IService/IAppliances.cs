﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ServiceModel;
using System.ServiceModel.Web;
using PropSurvey.Contracts.Data;
using PropSurvey.Utilities.Constants;


namespace PropSurvey.ServiceHost.IService
{
    // Start the service and browse to http://<machine_name>:<port>/appliances/help to view the service's generated help page
    // NOTE: By default, a new instance of the service is created for each call; change the InstanceContextMode to Single if you want
    // a single instance of the service to process all calls.	
    [ServiceContract]
    // NOTE: If the service is renamed, remember to update the global.asax.cs file

    interface IAppliances
    {
        #region get All Appliances

        /// <summary>
        /// This function returns all the Appliances
        /// </summary>
        /// <returns>List of Appliances data objects</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetAppliancesByPropertyID)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetAppliancesByPropertyID)]
        StockApplianceListData getAllAppliances(string propertyID, int appointmentID, string userName, string salt); // Return type changed by Abdul Wahhab - 24/05/2013

        //Change - Behroz Sikander - 12/07/2012 - Start
        //Code modified by Abdul Wahhab - 22/05/2013 - START
        /// <summary>
        /// This function returns all the Appliances
        /// </summary>
        /// <returns>List of Appliances data objects</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetAppliancesByPropertyIDGas)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetAppliancesByPropertyIDGas)]
        GasApplianceListData getAllAppliancesGas(string propertyID, int journalId, string userName, string salt);
        //Code modified by Abdul Wahhab - 22/05/2013 - END
        //Change - Behroz Sikander - 12/07/2012 - End
        #endregion

        #region get All Appliances Types

        /// <summary>
        /// This function returns all the Appliances Types
        /// </summary>
        /// <returns>List of Appliances Types data objects</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetApplianceTypeList)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetApplianceTypeList)]
        List<ApplianceTypeData> getAllApplianceTypes(string userName, string salt);

        #endregion

        #region get All Appliances Model

        /// <summary>
        /// This function returns all the Appliances Model
        /// </summary>
        /// <returns>List of Appliances Model data objects</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetApplianceModelList)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetApplianceModelList)]
        List<ApplianceModelData> getAllApplianceModel(string userName, string salt);

        #endregion

        #region get All Appliances location

        /// <summary>
        /// This function returns all the Appliances location
        /// </summary>
        /// <returns>List of Appliances location data objects</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetAppliancesLocationList)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetApplianceLocationList)]
        List<AppliancesLocationData> getAllApplianceLocation(string userName, string salt);

        #endregion

        #region get All Appliances Manufacturer

        /// <summary>
        /// This function returns all the Appliances Manufacturer
        /// </summary>
        /// <returns>List of Appliances Manufacturer data objects</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetApplianceManufacturList)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetApplianceManufacturList)]
        List<ManufacturerData> getAllApplianceManufacturer(string userName, string salt);

        #endregion

        #region save Appliance
        /// <summary>
        /// This function saves the Appliance in the database
        /// </summary>
        /// <param name="appData">The object of Appliance</param>
        /// <returns>the Appliance id if successfully save otherwise 0 </returns>

        [OperationContract]
        [Description(ServiceDescConstants.SaveAnAppliance)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.SaveAnAppliance)]
        int saveAppliance(ApplianceData appData, string userName, string salt);

        #endregion

        #region update Appliance
        /// <summary>
        /// This function updates the Appliance in the database
        /// </summary>
        /// <param name="appData">The object of Appliance</param>
        /// <returns>the Appliance id if successfully save otherwise 0 </returns>

        [OperationContract]
        [Description(ServiceDescConstants.UpdateAnAppliance)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.UpdateAnAppliance)]
        bool updateAppliance(ApplianceData appData, string userName, string salt);

        #endregion

        #region save Appliance Type
        /// <summary>
        /// This function saves the Appliance Type in the database
        /// </summary>
        /// <param name="appData">The object of Appliance Type</param>
        /// <returns>the Appliance id if successfully save otherwise 0 </returns>

        [OperationContract]
        [Description(ServiceDescConstants.SaveAnApplianceType)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.SaveAnApplianceType)]
        int saveApplianceType(ApplianceTypeData typeData, string userName, string salt);

        #endregion

        #region save Appliance Model
        /// <summary>
        /// This function saves the Appliance Model in the database
        /// </summary>
        /// <param name="appData">The object of Appliance Model</param>
        /// <returns>the Appliance id if successfully save otherwise 0 </returns>

        [OperationContract]
        [Description(ServiceDescConstants.SaveAnApplianceModel)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.SaveAnApplianceModel)]
        int saveApplianceModel(ApplianceModelData modelData, string userName, string salt);

        #endregion

        #region save Appliance Location
        /// <summary>
        /// This function saves the Appliance Location in the database
        /// </summary>
        /// <param name="appData">The object of Appliance Location</param>
        /// <returns>the Appliance id if successfully save otherwise 0 </returns>

        [OperationContract]
        [Description(ServiceDescConstants.SaveAnApplianceLocation)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.SaveApplianceLocation)]
        int saveApplianceLocation(AppliancesLocationData locationData, string userName, string salt);

        #endregion

        #region save Appliance Manufacturer
        /// <summary>
        /// This function saves the Appliance Manufacturer in the database
        /// </summary>
        /// <param name="appData">The object of Appliance Manufacturer</param>
        /// <returns>the Appliance id if successfully save otherwise 0 </returns>

        [OperationContract]
        [Description(ServiceDescConstants.SaveAnApplianceManufacturer)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.SaveAnApplianceManufacturer)]
        int saveApplianceManufacturer(ManufacturerData manufactureData, string userName, string salt);

        #endregion

        #region get number of inspected Appliances

        /// <summary>
        /// This function returns number of inspected Appliances
        /// </summary>
        /// <returns>number of inspected Appliances</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetApplianceInspecedByAppointmentID)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetApplianceInspecedByAppointmentID)]
        int getInspectedAppliances(int appointmentID, string userName, string salt);

        //Change#25 - Behroz - 20/12/2012 - Start
        /// <summary>
        /// This function returns number of inspected Appliances
        /// </summary>
        /// <returns>number of inspected Appliances</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetApplianceInspecedByAppointmentIDGas)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetApplianceInspecedByAppointmentIDGas)]
        int getInspectedAppliancesGas(int journalId, string userName, string salt);
        //Change#25 - Behroz - 20/12/2012 - End

        #endregion

        #region get ApplianceInspection by ApplianceID
        /// <summary>
        /// This function returns all the ApplianceInspection
        /// </summary>
        /// <returns>List of ApplianceInspection data objects</returns>

        [OperationContract]
        [Description(ServiceDescConstants.GetApplianceInspectionForm)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetApplianceInspectionByID)]
        ApplianceInspectionData getApplianceInspectionByApplianceID(int ApplianceID, int appointmentID, string userName, string salt);

        //Change#26 - Behroz - 20/12/2012 - Start
        /// <summary>
        /// This function returns all the ApplianceInspection for gas
        /// </summary>
        /// <returns>List of ApplianceInspection data objects</returns>

        [OperationContract]
        [Description(ServiceDescConstants.GetApplianceInspectionForm)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetApplianceInspectionByIDGas)]
        ApplianceInspectionData getApplianceInspectionByApplianceIDGas(int ApplianceID, int journalId, string userName, string salt);
        //Change#26 - Behroz - 20/12/2012 - End
        #endregion

        #region get ApplianceInspection Details by appointmentId
        /// <summary>
        /// This function returns all the ApplianceInspection
        /// </summary>
        /// <returns>List of ApplianceInspection data objects</returns>

        [OperationContract]
        [Description(ServiceDescConstants.GetApplianceInspectionDetailsByAppointmentID)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetApplianceInspectionDetailsByAppointmentID)]
        List<ApplianceInspectionData> getApplianceInspectionDetails(int appointmentID, string userName, string salt);

        //Change#40 - Behroz - 26/12/2012 - Start
        /// <summary>
        /// This function returns all the ApplianceInspection for gas
        /// </summary>
        /// <returns>List of ApplianceInspection data objects</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetApplianceInspectionDetailsByPropertyID)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetApplianceInspectionDetailsByPropertyIDGas)]
        List<ApplianceInspectionData> getApplianceInspectionDetailsGas(int journalId, string userName, string salt);
        //Change#40 - Behroz - 26/12/2012 - End
        #endregion

        #region Save a new ApplianceInspection
        /// <summary>
        /// This function saves a new ApplianceInspection
        /// </summary>
        /// <param name="appData">ApplianceInspection data objects</param>
        /// <returns>ID in case of success</returns>

        [OperationContract]
        [Description(ServiceDescConstants.SaveAnApplianceInspection)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.SaveApplianceInspection)]
        int saveApplianceInspection(ApplianceInspectionData appData, string userName, string salt);


        //Change#24 - Behroz - 20/12/2012 - Start
        /// <summary>
        /// This function saves a new ApplianceInspection for gas
        /// </summary>
        /// <param name="appData">ApplianceInspection data objects</param>
        /// <returns>ID in case of success</returns>
        [OperationContract]
        [Description(ServiceDescConstants.SaveAnApplianceInspectionGas)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.SaveApplianceInspectionGas)]
        int saveApplianceInspectionGas(ApplianceInspectionData appData, string userName, string salt);
        //Change#24 - Behroz - 20/12/2012 - End
        #endregion

        #region Update a existing ApplianceInspection
        /// <summary>
        /// This function updates a existing ApplianceInspection
        /// </summary>
        /// <param name="appData">ApplianceInspection data objects</param>
        /// <returns>ID in case of success</returns>

        [OperationContract]
        [Description(ServiceDescConstants.UpdateAnApplianceInspection)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.UpdateApplianceInspection)]
        bool updateApplianceInspection(ApplianceInspectionData appData, string userName, string salt);
        #endregion

        #region Get Appliance Count
        //Change#50 - Behroz - 16/01/2013 - Start
        /// <summary>
        /// This function return the appliance count
        /// </summary>        
        /// <returns>count of appliances</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetApplianceCount)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "GET", UriTemplate = UriTemplateConstants.GetApplianceCount)]
        int getApplianceCount(string propertyId, string userName, string salt);
        //Change#50 - Behroz - 16/01/2013 - Ends
        #endregion

        #region Update Detector Count
        //Code added by Abdul Wahhab - 23/05/2013 - START
        /// <summary>
        /// This function updates detector count against a property
        /// </summary>        
        /// <returns>True in case of success</returns>
        [OperationContract]
        [Description(ServiceDescConstants.UpdateDetectorCount)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.UpdateDetectorCount)]
        ResultBoolData updateDetectorCount(DetectorCountData detectorCountData, string propertyId, string userName, string salt);
        //Code added by Abdul Wahhab - 23/05/2013 - END
        #endregion

        #region Add/Update Detector Info
        //Code added by Abdul Wahhab - 23/05/2013 - START
        /// <summary>
        /// This function adds or updates detector info
        /// </summary>        
        /// <returns>ID in case of success</returns>
        [OperationContract]
        [Description(ServiceDescConstants.AddUpdateDetectorInfo)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.AddUpdateDetectorInfo)]
        ResultIntData addUpdateDetectorInfo(DetectorInspectionData detectorInspectionData, int detectorType, string userName, string salt);
        //Code added by Abdul Wahhab - 23/05/2013 - END
        #endregion
    }
}
