﻿using System;
using System.ComponentModel;
using System.ServiceModel;
using System.ServiceModel.Web;
using PropSurvey.Utilities.Constants;
using PropSurvey.Contracts.Data;

namespace PropSurvey.ServiceHost.IService
{
    // Start the service and browse to http://<machine_name>:<port>/authenticate/help to view the service's generated help page
    // NOTE: By default, a new instance of the service is created for each call; change the InstanceContextMode to Single if you want
    // a single instance of the service to process all calls.	
    [ServiceContract]
    // NOTE: If the service is renamed, remember to update the global.asax.cs file
    public interface IAuthentication
    {
        /// <summary>
        /// This function calls the business layer to authenticate the function from database.
        /// </summary>
        /// <param name="userName">username of surveyour</param>
        /// <param name="password">password of surveyour</param>
        /// <returns>Last Logged In Data Time</returns>
        [OperationContract]
        [Description(ServiceDescConstants.AuthenticateUser)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.AuthenticateUser)]
        //DateTime? authenticateUser(string userName, string password, string deviceToken);
        UserData authenticateUser(string userName, string password, string deviceToken);

        /// <summary>
        /// This function calls the business layer to logout the user from database.
        /// </summary>
        /// <param name="userName">username</param>
        /// <param name="salt">salt</param>
        /// <returns></returns>
        [OperationContract]
        [Description(ServiceDescConstants.LogoutUser)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.LogoutUser)]
        void LogoutUser(string userName, string salt);

        
        [OperationContract]
        [Description(ServiceDescConstants.LogoutUser)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetSalt)]
        string getSalt(string userName);

        [OperationContract]
        [Description(ServiceDescConstants.LogoutUser)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = "test?username={username}&password={password}")]
        int checkUser(string userName,string password);

        #region Get Gas Tab Rights
        //Change#16 - Behroz - 18/12/2012 - Start
        /// <summary>
        /// This services returns the rights on the user for the gas tab.
        /// </summary>
        /// <param name="userid"></param>
        /// <param name="userName"></param>
        /// <param name="salt"></param>
        /// <returns></returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetGasTabRights)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "GET", UriTemplate = UriTemplateConstants.GetGasTabRights)]
        bool GasTabRights(int userid, string userName, string salt);
        //Change#16 - Behroz - 18/12/2012 - End

        #endregion
    }
}
