using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Routing;
using System.ServiceModel.Activation;
using PropSurvey.ServiceHost.Service;
using System.ServiceModel.Web;
using System.ServiceModel;

namespace PropSurvey.ServiceHost
{
    public class Global : System.Web.HttpApplication
    {

        protected void Application_Start(object sender, EventArgs e)
        {
            RegisterRoutes();
        }

        private void RegisterRoutes()
        {
            // Edit the base address of Service1 by replacing the "appointment" string below
            RouteTable.Routes.Clear();
            RouteTable.Routes.Add(new ServiceRoute("appointment", new WebServiceHostFactory(), typeof(AppointmentService)));
            RouteTable.Routes.Add(new ServiceRoute("property", new WebServiceHostFactory(), typeof(PropertyService)));
            RouteTable.Routes.Add(new ServiceRoute("authenticate", new WebServiceHostFactory(), typeof(AuthenticationService)));
            RouteTable.Routes.Add(new ServiceRoute("customer", new WebServiceHostFactory(), typeof(CustomerService)));
            RouteTable.Routes.Add(new ServiceRoute("survey", new WebServiceHostFactory(), typeof(SurveyService)));
            RouteTable.Routes.Add(new ServiceRoute("propertyagent", new WebServiceHostFactory(), typeof(PropertyAgentServices)));
            RouteTable.Routes.Add(new ServiceRoute("appliances", new WebServiceHostFactory(), typeof(Appliances)));
            RouteTable.Routes.Add(new ServiceRoute("orginspection", new WebServiceHostFactory(), typeof(OrgInspection)));
            RouteTable.Routes.Add(new ServiceRoute("installationpipework", new WebServiceHostFactory(), typeof(InstallationPipework)));
            RouteTable.Routes.Add(new ServiceRoute("issuedreceivedby", new WebServiceHostFactory(), typeof(IssuedReceivedBy)));
            RouteTable.Routes.Add(new ServiceRoute("faults", new WebServiceHostFactory(), typeof(Faults)));
            RouteTable.Routes.Add(new ServiceRoute("appscreen", new WebServiceHostFactory(), typeof(AppScreenService)));
            RouteTable.Routes.Add(new ServiceRoute("push", new WebServiceHostFactory(), typeof(PushService)));

        }
    }
}