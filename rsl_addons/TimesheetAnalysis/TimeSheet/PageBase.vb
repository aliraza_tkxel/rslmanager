﻿Imports TS_Utilities
Imports System.Web
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class PageBase
    Inherits System.Web.UI.Page

    Public uiMessageHelper As UIMessageHelper = New UIMessageHelper()

    Sub New()

    End Sub


#Region "is Session Exist"

    Public Sub isSessionExist()

        'Dim userId As Integer = SessionManager.getFaultSchedulingUserId()
        'If (userId = 0) Then
        '    Response.Redirect(PathConstants.LoginPath)
        'End If
    End Sub
#End Region

#Region "Session Time out"
    ''' <summary>
    ''' This event handles the page_init event of the base page
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        Me.isSessionExist()
        'If Not Me.DesignMode Then
        '    If Context.Session IsNot Nothing Then
        '        If Session.IsNewSession Then
        '            Dim newSessionIdCookie As HttpCookie = Request.Cookies("sessionCookie")
        '            If newSessionIdCookie IsNot Nothing Then
        '                Dim newSessionIdCookieValue As String = newSessionIdCookie.Value
        '                If newSessionIdCookieValue <> String.Empty Then
        '                    ' This means Session was timed Out and New Session was started
        '                    Response.Redirect(PathConstants.AccessDeniedPath & "?src=sessionExpired")
        '                End If
        '            End If
        '        End If
        '    End If
        'End If
    End Sub
#End Region

#Region "OnInit"
    ''' <summary>
    ''' This function is used to destroy browser cache. so that every time page load 'll be called
    ''' </summary>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Overrides Sub OnInit(ByVal e As EventArgs)

        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetNoStore()
        Response.Cache.SetExpires(DateTime.Now.AddDays(-1))
        Response.ExpiresAbsolute = DateTime.Now.AddDays(-1D)
        Response.Expires = -1500
        Response.CacheControl = "no-cache"

        MyBase.OnInit(e)
    End Sub
#End Region

End Class
