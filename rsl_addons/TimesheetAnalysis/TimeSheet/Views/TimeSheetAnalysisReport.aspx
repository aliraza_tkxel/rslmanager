﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/TimeSheet.Master"
    CodeBehind="TimeSheetAnalysisReport.aspx.vb" Inherits="TimeSheet.TimeSheetAnalysisReport" %>

<%@ Import Namespace="TS_Utilities" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script language="JavaScript" type="text/javascript" src="../Scripts/jquery-1.10.2.js"></script>
    <link href="../../Styles/layout.css" rel="stylesheet" media="screen" />
    <script type="text/javascript">


        //setup before functions
        var typingTimer;                //timer identifier
        var doneTypingInterval = 1 * 1000;  //time in ms, 5 second for example is 5 * 1000

        function TypingInterval() {

            clearTimeout(typingTimer);
            typingTimer = setTimeout(searchTextChanged, doneTypingInterval);

        }

        function searchTextChanged() {
            __doPostBack('<%= txtBoxQuickFind.ClientID %>', '');
        }

        function pageLoad(sender, args) {
            $("#<%= txtBoxQuickFind.ClientID %>").focus().val($("#<%= txtBoxQuickFind.ClientID %>").val());
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <%--========================================================================================--%>
    <%-- Start - Page Message Section --%>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                <asp:Label ID="lblMessage" runat="server">
                </asp:Label>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%-- End - Page Message Section --%>
    <%--========================================================================================--%>
    <%-- Start - Controls (Top) Section, From Date, To Date, Quick Search (Operative's Name)  --%>
    <asp:UpdatePanel ID="updPnlSearchControls" runat="server" style="padding-left: 5%;
        vertical-align: middle;">
        <ContentTemplate>
            <%--From Date Controls--%>
            <asp:Label ID="lblFrom" Text="From: " runat="server" />
            <asp:TextBox ID="txtBoxFromDate" runat="server" CssClass="dateBox" Enabled="false" />&nbsp;&nbsp;
            <img alt="Open Calendar" src="../Images/calendar.png" id="imgFromCal" />
            <ajaxToolkit:CalendarExtender ID="calFromDate" runat="server" DaysModeTitleFormat="dd/MM/yyyy"
                TargetControlID="txtBoxFromDate" PopupButtonID="imgFromCal" PopupPosition="BottomLeft"
                TodaysDateFormat="dd/MM/yyyy" Format="dd/MM/yyyy" />
            <%--To Date Controls--%>
            &nbsp &nbsp
            <asp:Label ID="lblTo" Text="To: " runat="server" />
            <asp:TextBox ID="txtBoxToDate" runat="server" CssClass="dateBox" Enabled="false" />&nbsp;&nbsp;
            <img alt="Open Calendar" src="../Images/calendar.png" id="imgToCal" />
            <ajaxToolkit:CalendarExtender ID="calToDate" runat="server" DaysModeTitleFormat="dd/MM/yyyy"
                TargetControlID="txtBoxToDate" PopupButtonID="imgToCal" PopupPosition="BottomLeft"
                TodaysDateFormat="dd/MM/yyyy" Format="dd/MM/yyyy" />
            <%--Submit Button--%>
            &nbsp &nbsp &nbsp &nbsp
            <asp:Button ID="btnSubmit" Text="Submit" runat="server" UseSubmitBehavior="False" />
            <%--Quick Find--%>
            <asp:TextBox ID="txtBoxQuickFind" runat="server" Width="25%" AutoPostBack="false"
                ToolTip="Search for an operative" AutoCompleteType="Search" class="searchbox"
                Style="margin-right: 5%;" onkeyup="TypingInterval();"></asp:TextBox>
            <ajaxToolkit:TextBoxWatermarkExtender ID="txtBoxWatermarkExtenderQuickFind" runat="server"
                TargetControlID="txtBoxQuickFind" WatermarkText="Quick find based on operative's name"
                WatermarkCssClass="searchbox searchText">
            </ajaxToolkit:TextBoxWatermarkExtender>
        </ContentTemplate>
    </asp:UpdatePanel>
    <br />
    <br />
    <%-- End - Controls (Top) Section, From Date, To Date, Quick Search (Operative's Name)  --%>
    <%--========================================================================================--%>
    <%--Start - Time Sheet Section--%>
    <asp:UpdatePanel ID="updPnlTimeSheet" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:GridView ID="grdTimeSheet" runat="server" AutoGenerateColumns="False" HeaderStyle-HorizontalAlign="Center"
                CellPadding="5" CellSpacing="5" ShowFooter="True" Width="100%" CssClass="TimeSheet">
                <RowStyle CssClass="timeSheetRow" />
                <HeaderStyle CssClass="timeSheetHeader" />
                <FooterStyle CssClass="timeSheetFooter" />
                <EmptyDataTemplate>
                    <h2 style="color: Red; text-align: left;">
                        <%= UserMessageConstants.NoRecordFound %>
                    </h2>
                </EmptyDataTemplate>
                <Columns>
                    <%--Column 1 - Operative--%>
                    <asp:TemplateField HeaderText="Operative:" SortExpression="operative" HeaderStyle-HorizontalAlign="Left">
                        <ItemTemplate>
                            <asp:Label Text='<%# EVAL(ApplicationConstants.operative) %>' runat="server" />
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Left" />
                        <FooterTemplate>
                            <asp:Label ID="lblTotal" Text="Total" runat="server" Font-Bold="True" />
                        </FooterTemplate>
                    </asp:TemplateField>
                    <%--Column 2 - Available Hours--%>
                    <asp:TemplateField HeaderText="Available Hours:" SortExpression="availableHours">
                        <ItemTemplate>
                            <asp:Label Text='<%# EVAL(ApplicationConstants.availableHours) %>' runat="server" />
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblTotalAvailablehours" runat="server" Font-Bold="True" />
                        </FooterTemplate>
                        <ItemStyle CssClass="coloredCell" />
                        <HeaderStyle CssClass="coloredCell" />
                    </asp:TemplateField>
                    <%--Column 3 - Repairs Hours (Jobs) --%>
                    <asp:TemplateField HeaderText="Repairs Hours (Jobs):" SortExpression="repairsHours">
                        <ItemTemplate>
                            <asp:Label Text='<%# EVAL(ApplicationConstants.repairsHours).toString()  %>' runat="server" />
                            <asp:LinkButton ID="lnkBtnRepairJobs" OnClick="ViewRepairJobs" runat="server" Text='<%# "(" +EVAL(ApplicationConstants.repairJobs).toString()+ ")"   %>'
                                CommandArgument='<%# Eval("operativeId") %>'></asp:LinkButton>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblTotalRepairsHours" runat="server" Font-Bold="True" />
                        </FooterTemplate>
                    </asp:TemplateField>
                    <%--Column 4 - Fuel Servicing Hours (Jobs)--%>
                    <asp:TemplateField HeaderText="Fuel Servicing Hours (Jobs):" SortExpression="fuelServicingHours">
                        <ItemTemplate>
                            <asp:Label Text='<%# EVAL(ApplicationConstants.fuelServicingHours).toString()  %>'
                                runat="server" />
                            <asp:LinkButton ID="lnkBtnFuelServicingJobs" OnClick="ViewFuelServicingJobs" runat="server"
                                Text='<%# "(" +EVAL(ApplicationConstants.fuelServicingJobs).toString()+ ")"   %>'
                                CommandArgument='<%# Eval("operativeId") %>'></asp:LinkButton>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblTotalFuelServicingHours" runat="server" Font-Bold="True" />
                        </FooterTemplate>
                    </asp:TemplateField>
                    <%--Column 5 - Planned Hours (Jobs)--%>
                    <asp:TemplateField HeaderText="Planned Hours (Jobs):" SortExpression="plannedHours">
                        <ItemTemplate>
                            <asp:Label Text='<%# EVAL(ApplicationConstants.plannedHours).toString()  %>' runat="server" />
                            <asp:LinkButton ID="lnkBtnPlannedJobs" OnClick="ViewPlannedJobs" runat="server" Text='<%# "(" +EVAL(ApplicationConstants.plannedJobs).toString()+ ")"   %>'
                                CommandArgument='<%# Eval("operativeId") %>'></asp:LinkButton>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblTotalPlannedHours" runat="server" Font-Bold="True" />
                        </FooterTemplate>
                    </asp:TemplateField>
                    <%--Column 6 - Void Hours (Jobs)--%>
                    <asp:TemplateField HeaderText="Void Hours (Jobs):" SortExpression="voidHours">
                        <ItemTemplate>
                            <asp:Label Text='<%# EVAL(ApplicationConstants.voidHours).toString()  %>' runat="server" />
                            <asp:LinkButton ID="lnkBtnVoidJobs" OnClick="ViewVoidJobs" runat="server" Text='<%# "(" +EVAL(ApplicationConstants.voidJobs).toString()+ ")"   %>'
                                CommandArgument='<%# Eval("operativeId") %>'></asp:LinkButton>
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblTotalVoidHours" runat="server" Font-Bold="True" />
                        </FooterTemplate>
                    </asp:TemplateField>
                    <%--Column 7 - Marerials Hours--%>
                    <asp:TemplateField HeaderText="Materials Hours:" SortExpression="materialsHours">
                        <ItemTemplate>
                            <asp:Label Text='<%# EVAL(ApplicationConstants.materialsHours) %>' runat="server" />
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblTotalMaterialsHours" runat="server" Font-Bold="True" />
                        </FooterTemplate>
                    </asp:TemplateField>
                    <%--Column 8 - Total Worked Hours--%>
                    <asp:TemplateField HeaderText="Total Worked Hours:" SortExpression="totalWorkedHours">
                        <ItemTemplate>
                            <asp:Label Text='<%# EVAL(ApplicationConstants.totalWorkedHours).toString() + "(" + EVAL(ApplicationConstants.totalWorkPercentage).toString() + "%)" %>'
                                runat="server" />
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblGrandTotalWorkedHours" runat="server" Font-Bold="True" />
                        </FooterTemplate>
                        <ItemStyle CssClass="coloredCell" />
                        <HeaderStyle CssClass="coloredCell" />
                    </asp:TemplateField>
                    <%--Column 9 - Difference--%>
                    <asp:TemplateField HeaderText="Difference:" SortExpression="difference">
                        <ItemTemplate>
                            <asp:Label ID="Label1" Text='<%# EVAL(ApplicationConstants.difference) %>' runat="server" />
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblTotalDifference" runat="server" Font-Bold="True" />
                        </FooterTemplate>
                    </asp:TemplateField>
                    <%--Column 10 Absent Hours Sick--%>
                    <asp:TemplateField HeaderText="Sick:" SortExpression="absentHoursSick">
                        <ItemTemplate>
                            <%--<asp:Label ID="Label2" Text='<%# EVAL(ApplicationConstants.absentHoursSick) %>' runat="server" />--%>

                             <asp:LinkButton ID="lnkSickHours" OnClick="ViewSickHours"  runat="server"  Text='<%# EVAL(ApplicationConstants.absentHoursSick) %>'
                                CommandArgument='<%# Eval("operativeId") %>'></asp:LinkButton>

                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblTotalAbsentHoursSick" runat="server" Font-Bold="True" />
                        </FooterTemplate>
                        <ItemStyle CssClass="coloredCell" />
                        <HeaderStyle CssClass="coloredCell" VerticalAlign="Bottom" />
                    </asp:TemplateField>
                    <%--Column 11 Absent Hours Other--%>
                    <asp:TemplateField HeaderText="Absent<br />Hours:<br />Other:" SortExpression="absentHoursSick">
                        <ItemTemplate>
                            <%--<asp:Label Text='<%# EVAL(ApplicationConstants.absentHoursOther) %>' runat="server" />--%>

                             <asp:LinkButton ID="lnkOtherHours" OnClick="ViewOtherHours" runat="server"  Text='<%# EVAL(ApplicationConstants.absentHoursOther) %>'
                                CommandArgument='<%# Eval("operativeId") %>'></asp:LinkButton>

                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblTotalAbsentHoursOther" runat="server" Font-Bold="True" />
                        </FooterTemplate>
                        <ItemStyle CssClass="coloredCell" />
                        <HeaderStyle CssClass="coloredCell" VerticalAlign="Bottom" />
                    </asp:TemplateField>
                    <%--Column 12 Absent Hours Total--%>
                    <asp:TemplateField HeaderText="Total:" SortExpression="absentHoursTotal">
                        <ItemTemplate>
                            <asp:Label Text='<%# EVAL(ApplicationConstants.absentHoursTotal) %>' runat="server" />
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblTotalAbsentHoursTotal" runat="server" Font-Bold="True" />
                        </FooterTemplate>
                        <ItemStyle CssClass="coloredCell" />
                        <HeaderStyle CssClass="coloredCell" VerticalAlign="Bottom" />
                    </asp:TemplateField>
                    <%--Column 13 Total Hours--%>
                    <asp:TemplateField HeaderText="Total Hours:" SortExpression="totalHours">
                        <ItemTemplate>
                            <asp:Label Text='<%# EVAL(ApplicationConstants.totalHours) %>' runat="server" />
                        </ItemTemplate>
                        <FooterTemplate>
                            <asp:Label ID="lblGrandTotalHours" runat="server" Font-Bold="True" />
                        </FooterTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%--End - Time Sheet Section--%>
    <%--========================================================================================--%>
    <%-- Start - Controls (Button) Section, Export Button  --%>
    <div style="clear: both">
    </div>
    <asp:Button ID="btnExport" Text="Export" runat="server" OnClick="btnExport_Click"
        UseSubmitBehavior="False" Style="float: right;" />
    <%-- End - Controls (Button) Section, Export Button  --%>
    <%--========================================================================================--%>
    <%-- Modal PopUp Operative Jobs --%>
    <cc1:ModalPopupExtender ID="mdlOperativeJobsPopup" runat="server" TargetControlID="lblRepairJobDismiss"
        Drag="true" PopupControlID="pnlRepairJobs" BackgroundCssClass="modalBackgroundRepairJobs"
        CancelControlID="btnClose">
    </cc1:ModalPopupExtender>
    <asp:Panel ID="pnlRepairJobs" runat="server" Visible="true" CssClass="modalPopupRepairJobs"
        align="center">
        <asp:UpdatePanel runat="server" ID="updPanelRepairJobs">
            <ContentTemplate>
                <div style="text-align: left; font-size: 14px; font-weight: bold; padding-left: 10px;
                    margin-top: 5px;">
                    <asp:Label ID="lblTitle" Text='' runat="server" Font-Bold="true" />
                </div>
                <br />
                <div style="width: 100%;">
                    <asp:Panel ID="pnlControlMessage" runat="server" Visible="false">
                        <asp:Label ID="lblControlMessage" runat="server">
                        </asp:Label>
                    </asp:Panel>
                    <asp:Panel ID="pnlRepairJobsGridView" runat="server" Visible="true" align="center"
                        CssClass="RepairJobsGridView">
                        <asp:GridView ID="grdOperativeJobs" runat="server" AutoGenerateColumns="False" CellPadding="3"
                            CellSpacing="3" ShowFooter="false" HeaderStyle-CssClass="timeSheetHeader" Width="100%"
                            GridLines="None">
                            <Columns>
                                <%--Column 1 - JsRef--%>
                                <asp:TemplateField HeaderText="JS Ref:" SortExpression="JsRef" HeaderStyle-Font-Size="14px" HeaderStyle-HorizontalAlign="Left"
                                    HeaderStyle-Font-Bold="true">
                                    <ItemTemplate>
                                        <%--<asp:Label ID="lblJsRef" Text='<%# Bind("JsRef") %>' runat="server" />--%>
                                        <asp:LinkButton ID="lnkbtnRef" OnClick="ViewFault" runat="server" Text='<%#Eval("JsRef") %>'></asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" Width="20%" />
                                </asp:TemplateField>
                                <%--Column 2 - Address--%>
                                <asp:TemplateField HeaderText="Address:" SortExpression="AppointmentAddress" HeaderStyle-Font-Size="14px" HeaderStyle-HorizontalAlign="Left"
                                    HeaderStyle-Font-Bold="true">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAppointmentAddress" Text='<%# Bind("AppointmentAddress") %>' runat="server" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" Width="30%" />
                                </asp:TemplateField>
                                <%--Column 3 - Title--%>
                                <asp:TemplateField HeaderText="Title:" SortExpression="Title" HeaderStyle-Font-Size="14px" HeaderStyle-HorizontalAlign="Left"
                                    HeaderStyle-Font-Bold="true">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTitle" Text='<%# Bind("Title") %>' runat="server" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" Width="0%" />
                                </asp:TemplateField>
                                <%--Column 4 - Component--%>
                                 <asp:TemplateField HeaderText="Component:" SortExpression="ComponentName" HeaderStyle-Font-Size="14px" HeaderStyle-HorizontalAlign="Left"
                                    HeaderStyle-Font-Bold="true">
                                    <ItemTemplate>
                                        <asp:Label ID="lblComponent" Text='<%# Bind("ComponentName") %>' runat="server" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" Width="20%" />
                                </asp:TemplateField>
                                <%--Column 5 - Hours--%>
                                <asp:TemplateField HeaderText="Hours:" SortExpression="Hours" HeaderStyle-Font-Size="14px" HeaderStyle-HorizontalAlign="Center"  
                                    HeaderStyle-Font-Bold="true">
                                    <ItemTemplate>
                                        <asp:Label ID="lblHours" Text='<%#Bind("Hours") %>' runat="server" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center"  Width="20%" />

                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Type:">
                                    <ItemTemplate>
                                        <asp:Label ID="lblInspectionType" style=" display:none;" runat="server" Text='<%#Bind("InspectionType") %>'></asp:Label>
                                    </ItemTemplate>
                                    <HeaderStyle Font-Bold="True" HorizontalAlign="left" />
                                    <ItemStyle Width="15%" />
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle BackColor="White" ForeColor="Black" Font-Bold="True" HorizontalAlign="Left"
                                CssClass="gridHeader"></HeaderStyle>
                        </asp:GridView>
                    </asp:Panel>
                    <br />
                    <asp:Panel runat="server" ID="pnlPagination" Visible="false" CssClass="gridPagerContainer">
                        <table width="80%"  >
                            <tbody>
                                <tr>
                                    <td>
                                        <asp:LinkButton ID="lnkbtnPagerFirst" Text="First" runat="server" CommandName="Page"
                                            CommandArgument="First" />
                                        &nbsp;
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lnkbtnPagerPrev" Text="Prev" runat="server" CommandName="Page"
                                            CommandArgument="Prev" />
                                        &nbsp;
                                    </td>
                                    <td>
                                        Page:
                                        <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" ForeColor="Red" />
                                        of
                                        <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />
                                    </td>
                                    <td>
                                        Records:
                                        <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                                        to
                                        <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                                        of
                                        <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lnkbtnPagerNext" Text="Next" runat="server" CommandName="Page"
                                            CommandArgument="Next" />
                                        &nbsp;
                                    </td>
                                    <td>
                                        <asp:LinkButton ID="lnkbtnPagerLast" Text="Last" runat="server" CommandName="Page"
                                            CommandArgument="Last" />
                                        &nbsp;
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </asp:Panel>
                </div>
                
                
                <div style="text-align: right; padding-right: 20px; padding-top: 10px">
                    <asp:Button ID="btnClose" runat="server" Text="Close window" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
    <!-- ModalPopupExtender -->
    <asp:Button ID="btnHidden" runat="server" Text="" Style="display: none;" />
    <cc1:ModalPopupExtender ID="popupJobSheet" runat="server" PopupControlID="pnlJobSheet"
        TargetControlID="btnHidden" CancelControlID="btnCloseJobSheet" BackgroundCssClass="modalBackground">
    </cc1:ModalPopupExtender>
    <asp:Panel ID="pnlJobSheet" runat="server" CssClass="modalPopup" Style="height: 615px; border: 1px solid black; background-color: White; margin-top: 0px;
        width: 900px; overflow: hidden;">
        <asp:UpdatePanel runat="server" ID="updPanelJobSheets">
            <ContentTemplate>
       
        <iframe runat="server" id="ifrmJobSheet" class="ifrmJobSheets" style="height: 575px;
            overflow: auto; width: 100%; border: 0px none transparent;" />
     </ContentTemplate>
        </asp:UpdatePanel>
        <div style="width: 100%; text-align: left; clear: both;">
            <div style="padding-right:20px; float: right;">
                <asp:Button ID="btnCloseJobSheet" runat="server" Text="Close" CssClass="margin_right20" />
              
            </div>
        </div>
    </asp:Panel>
    <%-- Modal PopUp Repair Jobs --%>
    <asp:Label ID="lblRepairJobDismiss" runat="server"></asp:Label>
    
    <!-- start sickness hours-->

    <cc1:ModalPopupExtender ID="SickHourModalPopupExtender" runat="server" TargetControlID="lblSickHours"
        Drag="true" PopupControlID="PnlSickHours" BackgroundCssClass="modalBackgroundRepairJobs"
        CancelControlID="btnPnlSickHoursClose">
    </cc1:ModalPopupExtender>
    <asp:Panel ID="PnlSickHours" runat="server" Visible="true" CssClass="modalPopupRepairJobs"
        align="center">
        <asp:UpdatePanel runat="server" ID="UpdatePnlSickHours">
            <ContentTemplate>
                <div style="text-align: left; font-size: 14px; font-weight: bold; padding-left: 10px;
                    margin-top: 5px;">
                    <asp:Label ID="Label2" Text='' runat="server" Font-Bold="true" />
                </div>
                <br />
                <div style="width: 100%;">
                    <asp:Panel ID="Panel2" runat="server" Visible="true" align="center"
                        CssClass="RepairJobsGridView">
                        <asp:GridView ID="GridViewSickHours" runat="server" AutoGenerateColumns="False" CellPadding="3"
                            CellSpacing="3" ShowFooter="false" HeaderStyle-CssClass="timeSheetHeader" Width="100%"
                            GridLines="None">
                            <Columns>
                                <%--Column 1 - JsRef--%>
                                <asp:TemplateField HeaderText="Leave Type" SortExpression="LeaveType" HeaderStyle-Font-Size="14px" HeaderStyle-HorizontalAlign="Center"  
                                    HeaderStyle-Font-Bold="true" >
                                    <ItemTemplate>
                                        <asp:Label ID="lblLeaveType" Text='<%# Bind("LeaveType") %>' runat="server" style="margin-left:20px" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" Width="20%" />
                                </asp:TemplateField>
                                <%--Column 2 - Address--%>
                                <asp:TemplateField HeaderText="Start Date:" SortExpression="StarDate" HeaderStyle-Font-Size="14px" HeaderStyle-HorizontalAlign="Center"  
                                    HeaderStyle-Font-Bold="true">
                                    <ItemTemplate>
                                        <asp:Label ID="lblStarDate" Text='<%# Bind("StarDate") %>' runat="server" style="margin-left:50px" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" Width="30%" />
                                </asp:TemplateField>
                                <%--Column 3 - Title--%>
                                <asp:TemplateField HeaderText="End Date:" SortExpression="EndDate" HeaderStyle-Font-Size="14px" HeaderStyle-HorizontalAlign="Center"  
                                    HeaderStyle-Font-Bold="true">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEndDate" Text='<%# Bind("EndDate") %>' runat="server" style="margin-left:110px" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" Width="0%" />
                                </asp:TemplateField>                                
                            </Columns>
                            <HeaderStyle BackColor="White" ForeColor="Black" Font-Bold="True" HorizontalAlign="Left"
                                CssClass="gridHeader"></HeaderStyle>
                        </asp:GridView>
                    </asp:Panel>
                    <br />                   
                </div>
                <div style="text-align: right; padding-right: 20px; padding-top: 10px">
                    <asp:Button ID="btnPnlSickHoursClose"  runat="server" Text="Close window" />
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>

    <asp:Label ID="lblSickHours" runat="server"></asp:Label>


</asp:Content>
