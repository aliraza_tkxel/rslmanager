﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated. 
' </auto-generated>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class TimeSheetAnalysisReport

    '''<summary>
    '''pnlMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlMessage As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lblMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblMessage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''updPnlSearchControls control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents updPnlSearchControls As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''lblFrom control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblFrom As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtBoxFromDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtBoxFromDate As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''calFromDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents calFromDate As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''lblTo control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTo As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''txtBoxToDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtBoxToDate As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''calToDate control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents calToDate As Global.AjaxControlToolkit.CalendarExtender

    '''<summary>
    '''btnSubmit control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnSubmit As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''txtBoxQuickFind control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtBoxQuickFind As Global.System.Web.UI.WebControls.TextBox

    '''<summary>
    '''txtBoxWatermarkExtenderQuickFind control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents txtBoxWatermarkExtenderQuickFind As Global.AjaxControlToolkit.TextBoxWatermarkExtender

    '''<summary>
    '''updPnlTimeSheet control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents updPnlTimeSheet As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''grdTimeSheet control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents grdTimeSheet As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''btnExport control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnExport As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''mdlOperativeJobsPopup control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents mdlOperativeJobsPopup As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''pnlRepairJobs control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlRepairJobs As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''updPanelRepairJobs control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents updPanelRepairJobs As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''lblTitle control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblTitle As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''pnlControlMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlControlMessage As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lblControlMessage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblControlMessage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''pnlRepairJobsGridView control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlRepairJobsGridView As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''grdOperativeJobs control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents grdOperativeJobs As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''pnlPagination control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlPagination As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''lnkbtnPagerFirst control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkbtnPagerFirst As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''lnkbtnPagerPrev control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkbtnPagerPrev As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''lblPagerCurrentPage control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPagerCurrentPage As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPagerTotalPages control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPagerTotalPages As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPagerRecordStart control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPagerRecordStart As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPagerRecordEnd control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPagerRecordEnd As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lblPagerRecordTotal control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblPagerRecordTotal As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''lnkbtnPagerNext control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkbtnPagerNext As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''lnkbtnPagerLast control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lnkbtnPagerLast As Global.System.Web.UI.WebControls.LinkButton

    '''<summary>
    '''btnClose control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnClose As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''btnHidden control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnHidden As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''popupJobSheet control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents popupJobSheet As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''pnlJobSheet control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents pnlJobSheet As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''updPanelJobSheets control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents updPanelJobSheets As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''ifrmJobSheet control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents ifrmJobSheet As Global.System.Web.UI.HtmlControls.HtmlGenericControl

    '''<summary>
    '''btnCloseJobSheet control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnCloseJobSheet As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''lblRepairJobDismiss control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblRepairJobDismiss As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''SickHourModalPopupExtender control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents SickHourModalPopupExtender As Global.AjaxControlToolkit.ModalPopupExtender

    '''<summary>
    '''PnlSickHours control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents PnlSickHours As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''UpdatePnlSickHours control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents UpdatePnlSickHours As Global.System.Web.UI.UpdatePanel

    '''<summary>
    '''Label2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Label2 As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Panel2 control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents Panel2 As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''GridViewSickHours control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents GridViewSickHours As Global.System.Web.UI.WebControls.GridView

    '''<summary>
    '''btnPnlSickHoursClose control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents btnPnlSickHoursClose As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''lblSickHours control.
    '''</summary>
    '''<remarks>
    '''Auto-generated field.
    '''To modify move field declaration from designer file to code-behind file.
    '''</remarks>
    Protected WithEvents lblSickHours As Global.System.Web.UI.WebControls.Label
End Class
