﻿Imports TS_BusinessLogic
Imports TS_BusinessObject
Imports TS_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Globalization
Imports System.IO
Imports System.Drawing

Public Class TimeSheetAnalysisReport
    Inherits PageBase

#Region "Events"

#Region "Page Load"
    ''' <summary>
    ''' This is the page load function
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            If Not IsPostBack Then
                setDefaultValues()
                populateTimeSheetReport()
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Submit button click"
    ''' <summary>
    ''' This is the submit button against the date selector. It fetches the records between the specified dates
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click

        Try
            populateTimeSheetReport()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Text Box text changed"
    ''' <summary>
    ''' Thisis the event fired when there is change in text of Text Box. The text is used for searching for operatives
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub txtBoxQuickFind_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtBoxQuickFind.TextChanged

        Try
            populateTimeSheetReport()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Export Button Click"
    ''' <summary>
    ''' This is the event fired when export button is clicked by user. it export the gridview data in excel file.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Try
            exportToExcel()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Repair jobs popup"

    Protected Sub ViewRepairJobs(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim linkBtn As LinkButton = DirectCast(sender, LinkButton)
            Dim operativeId As Integer = CType(linkBtn.CommandArgument, Integer)
            showOperativeJobs(operativeId, ApplicationConstants.RepairsAppointmentTitle, True)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Planned jobs popup"

    Protected Sub ViewPlannedJobs(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim linkBtn As LinkButton = DirectCast(sender, LinkButton)
            Dim operativeId As Integer = CType(linkBtn.CommandArgument, Integer)
            showOperativeJobs(operativeId, ApplicationConstants.PlannedAppointmentTitle, True)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Fuel Servicing jobs popup"

    Protected Sub ViewFuelServicingJobs(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim linkBtn As LinkButton = DirectCast(sender, LinkButton)
            Dim operativeId As Integer = CType(linkBtn.CommandArgument, Integer)
            showOperativeJobs(operativeId, ApplicationConstants.FuelServicingAppointmentTitle, True)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

#End Region

#Region "Void jobs popup"

    Protected Sub ViewVoidJobs(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim linkBtn As LinkButton = DirectCast(sender, LinkButton)
            Dim operativeId As Integer = CType(linkBtn.CommandArgument, Integer)
            showOperativeJobs(operativeId, ApplicationConstants.VoidAppointmentTitle, True)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub



    Protected Sub ViewSickHours(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim linkBtn As LinkButton = DirectCast(sender, LinkButton)
            Dim operativeId As Integer = CType(linkBtn.CommandArgument, Integer)
            showSickHours(operativeId)
            'showOperativeJobs(operativeId, ApplicationConstants.VoidAppointmentTitle, True)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub

    Protected Sub ViewOtherHours(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim linkBtn As LinkButton = DirectCast(sender, LinkButton)
            Dim operativeId As Integer = CType(linkBtn.CommandArgument, Integer)
            showOtherHours(operativeId)
            'showOperativeJobs(operativeId, ApplicationConstants.VoidAppointmentTitle, True)
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub



#End Region

#Region "Close button click"
    Protected Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        mdlOperativeJobsPopup.Hide()
    End Sub
#End Region

#Region "Row data bound of Time Sheet Report"

    Protected Sub grdTimeSheet_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdTimeSheet.RowDataBound
        Dim drview As DataRowView = TryCast(e.Row.DataItem, DataRowView)
        If e.Row.RowType = DataControlRowType.DataRow Then

            Dim lnkBtnRepairJobs As LinkButton = TryCast(e.Row.FindControl("lnkBtnRepairJobs"), LinkButton)
            Dim lnkBtnFuelServicingJobs As LinkButton = TryCast(e.Row.FindControl("lnkBtnFuelServicingJobs"), LinkButton)
            Dim lnkBtnPlannedJobs As LinkButton = TryCast(e.Row.FindControl("lnkBtnPlannedJobs"), LinkButton)
            Dim lnkBtnVoidJobs As LinkButton = TryCast(e.Row.FindControl("lnkBtnVoidJobs"), LinkButton)
            Dim lnkBtnSickHours As LinkButton = TryCast(e.Row.FindControl("lnkSickHours"), LinkButton)
            Dim lnkBtnOtherHours As LinkButton = TryCast(e.Row.FindControl("lnkOtherHours"), LinkButton)


            If lnkBtnOtherHours.Text = 0 Then
                lnkBtnOtherHours.Enabled = False
            End If

            If lnkBtnSickHours.Text = 0 Then
                lnkBtnSickHours.Enabled = False
            End If


            If lnkBtnRepairJobs.Text = 0 Then
                lnkBtnRepairJobs.Enabled = False
            End If

            If lnkBtnFuelServicingJobs.Text = 0 Then
                lnkBtnFuelServicingJobs.Enabled = False
            End If

            If lnkBtnPlannedJobs.Text = 0 Then
                lnkBtnPlannedJobs.Enabled = False
            End If

            If lnkBtnVoidJobs.Text = 0 Then
                lnkBtnVoidJobs.Enabled = False
            End If

        End If
    End Sub

#End Region

#Region "Pager LinkButtons Click"

    Protected Sub lnkbtnPager_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkbtnPagerFirst.Click _
        , lnkbtnPagerLast.Click, lnkbtnPagerPrev.Click, lnkbtnPagerNext.Click
        Try
            Dim pagebutton As LinkButton = DirectCast(sender, LinkButton)

            Dim objPageSortBo As PageSortBO = getPageSortBoViewState()

            If pagebutton.CommandName = "Page" AndAlso pagebutton.CommandArgument = "First" Then
                objPageSortBo.PageNumber = 1
            ElseIf pagebutton.CommandName = "Page" AndAlso pagebutton.CommandArgument = "Last" Then
                objPageSortBo.PageNumber = objPageSortBo.TotalPages
            ElseIf pagebutton.CommandName = "Page" AndAlso pagebutton.CommandArgument = "Next" Then
                objPageSortBo.PageNumber += 1
            ElseIf pagebutton.CommandName = "Page" AndAlso pagebutton.CommandArgument = "Prev" Then
                objPageSortBo.PageNumber -= 1
            End If

            setPageSortBoViewState(objPageSortBo)
            Dim operativeId As Integer = Convert.ToInt32(ViewState.Item(ViewStateConstants.OperativeId))
            Dim appointmentType As String = ViewState.Item(ViewStateConstants.AppointmentType)
            showOperativeJobs(operativeId, appointmentType, False)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "View Fault Link Button"

    Protected Sub ViewFault(ByVal sender As Object, ByVal e As EventArgs)

        Try
            Dim lnkBtnJobSheet As LinkButton = DirectCast(sender, LinkButton)

            Dim row As GridViewRow = DirectCast(lnkBtnJobSheet.NamingContainer, GridViewRow)
            Dim lblInspectionType As Label = DirectCast(row.FindControl("lblInspectionType"), Label)

            Me.displayJobSheetInfo(CType(lnkBtnJobSheet.Text, String), lblInspectionType.Text)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.Message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblControlMessage, pnlControlMessage, uiMessageHelper.Message, True)
            End If
        End Try

    End Sub

#End Region

#End Region

#Region "Functions"

#Region "Set Default Values"
    ''' <summary>
    ''' This function fetchs records from start of current month to end of current month with an empty search string
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub setDefaultValues()
        'Set from date to First day of current month
        txtBoxFromDate.Text = New DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).ToString("dd/MM/yyyy")
        'Set to date to Last day of current month
        txtBoxToDate.Text = New DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month)).ToString("dd/MM/yyyy")
        txtBoxQuickFind.Text = String.Empty
    End Sub

#End Region

#Region "Populate Time Sheet Report"
    ''' <summary>
    ''' This is the main function that populates the records in the grid
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub populateTimeSheetReport()

        Dim resultDS As New DataSet()
        Dim objTimeSheetBO As New TimeSheetBO()
        Dim objTimeSheetBL As New TimeSheetBL()
        Dim cultureInfo As New CultureInfo("en-GB")
        DateTime.TryParseExact(txtBoxFromDate.Text, "dd/MM/yyyy", cultureInfo, DateTimeStyles.None, objTimeSheetBO.fromDate)
        DateTime.TryParseExact(txtBoxToDate.Text, "dd/MM/yyyy", cultureInfo, DateTimeStyles.None, objTimeSheetBO.toDate)
        objTimeSheetBO.searchText = Trim(txtBoxQuickFind.Text)

        If objTimeSheetBO.toDate < objTimeSheetBO.fromDate Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.IncorrectDateInterval, True)
        Else
            objTimeSheetBL.getTimeSheetReport(objTimeSheetBO, resultDS)

            Dim dtTimeSheet = resultDS.Tables(0)

            grdTimeSheet.DataSource = dtTimeSheet
            grdTimeSheet.DataBind()

            If (grdTimeSheet.Rows.Count > 0) Then

                grdTimeSheet.UseAccessibleHeader = True
                grdTimeSheet.HeaderRow.TableSection = TableRowSection.TableHeader
                grdTimeSheet.FooterRow.TableSection = TableRowSection.TableFooter

                'Calculating Total Available Hours
                Dim totalAvailableHours As Label = grdTimeSheet.FooterRow.FindControl("lblTotalAvailablehours")
                totalAvailableHours.Text = dtTimeSheet.Compute("Sum(availableHours)", "")

                'Calculating Total Repairs Hours
                Dim totalRepairsHours As Label = grdTimeSheet.FooterRow.FindControl("lblTotalRepairsHours")
                totalRepairsHours.Text = dtTimeSheet.Compute("Sum(repairsHours)", "")

                'Calculating Total Fuel Servicing Hours
                Dim totalFuelServicingHours As Label = grdTimeSheet.FooterRow.FindControl("lblTotalFuelServicingHours")
                totalFuelServicingHours.Text = dtTimeSheet.Compute("Sum(fuelServicingHours)", "")

                'Calculating Total Planned Hours
                Dim totalPlannedHours As Label = grdTimeSheet.FooterRow.FindControl("lblTotalPlannedHours")
                totalPlannedHours.Text = dtTimeSheet.Compute("Sum(plannedHours)", "")

                'Calculating Total Void Hours
                Dim totalVoidHours As Label = grdTimeSheet.FooterRow.FindControl("lblTotalVoidHours")
                totalVoidHours.Text = dtTimeSheet.Compute("Sum(voidHours)", "")

                'Calculating Total Material Hours
                Dim totalMaterialsHours As Label = grdTimeSheet.FooterRow.FindControl("lblTotalMaterialsHours")
                totalMaterialsHours.Text = dtTimeSheet.Compute("Sum(materialsHours)", "")

                'Calculating Grand Total Worked Hours
                Dim totalWorkedHours As Label = grdTimeSheet.FooterRow.FindControl("lblGrandTotalWorkedHours")
                totalWorkedHours.Text = dtTimeSheet.Compute("Sum(totalWorkedHours)", "").ToString()
                totalWorkedHours.Text = totalWorkedHours.Text + "(" + Math.Round((totalWorkedHours.Text / totalAvailableHours.Text) * 100, 2).ToString() + "%)"

                'Calculating Total Difference
                Dim totalDifference As Label = grdTimeSheet.FooterRow.FindControl("lblTotalDifference")
                totalDifference.Text = dtTimeSheet.Compute("Sum(difference)", "")

                'Calculating Total Absent Hours Sick
                Dim totalAbsentHoursSick As Label = grdTimeSheet.FooterRow.FindControl("lblTotalAbsentHoursSick")
                totalAbsentHoursSick.Text = dtTimeSheet.Compute("Sum(absentHoursSick)", "")

                'Calculating Total Absent Hours Other
                Dim totalAbsentHoursOther As Label = grdTimeSheet.FooterRow.FindControl("lblTotalAbsentHoursOther")
                totalAbsentHoursOther.Text = dtTimeSheet.Compute("Sum(absentHoursOther)", "")

                'Calculating Grand Total Absent Hours
                Dim totalAbsentHoursTotal As Label = grdTimeSheet.FooterRow.FindControl("lblTotalAbsentHoursTotal")
                totalAbsentHoursTotal.Text = dtTimeSheet.Compute("Sum(absentHoursTotal)", "")

                'Calculating Grand Total Hours
                Dim totalHours As Label = grdTimeSheet.FooterRow.FindControl("lblGrandTotalHours")
                totalHours.Text = dtTimeSheet.Compute("Sum(totalHours)", "")

            End If

            updPnlTimeSheet.Update()

        End If

    End Sub

#End Region

#Region "Show operative jobs"
    ''' <summary>
    ''' This is the main function that populates the records in the grid
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub showOperativeJobs(ByVal operativeId As Integer, ByVal appointmentType As String, ByVal setSession As Boolean)

        If setSession = True Then
            removePageSortBoViewState()
        End If
        Dim objPageSortBo As PageSortBO = getPageSortBoViewState()

        ViewState.Add(ViewStateConstants.OperativeId, operativeId)
        ViewState.Add(ViewStateConstants.AppointmentType, appointmentType)

        Dim resultDataSet As DataSet = New DataSet()
        Dim objTimeSheetBL As New TimeSheetBL()
        lblTitle.Text = String.Format("{0} Jobs:", appointmentType)

        Dim objTimeSheetBO As New TimeSheetBO()
        objTimeSheetBO.fromDate = txtBoxFromDate.Text
        objTimeSheetBO.toDate = txtBoxToDate.Text
        objTimeSheetBO.operativeId = operativeId
        objTimeSheetBO.appointmentType = appointmentType
        Dim totalCount As Integer = objTimeSheetBL.getOperativeJobs(objTimeSheetBO, resultDataSet, objPageSortBo)

        objPageSortBo.TotalRecords = totalCount
        objPageSortBo.TotalPages = Math.Ceiling(totalCount / objPageSortBo.PageSize)

        If setSession Then            
            ViewState.Add(ViewStateConstants.PageSortBo, objPageSortBo)
            ViewState.Add(ViewStateConstants.TotalCount, totalCount)
        End If

        'grdOperativeJobs.VirtualItemCount = totalCount
        grdOperativeJobs.DataSource = resultDataSet
        grdOperativeJobs.DataBind()

        ' (0-4 column) column # 3 is "Component" that show only for "Planned Job Sheet"
        grdOperativeJobs.Columns(3).Visible = False

        If appointmentType = ApplicationConstants.PlannedAppointmentTitle Then
            grdOperativeJobs.Columns(3).Visible = True
        End If


        setPageSortBoViewState(objPageSortBo)

        GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)

        If grdOperativeJobs.PageCount > 1 AndAlso objPageSortBo.PageNumber = grdOperativeJobs.PageCount Then
            setVirtualItemCountViewState(totalCount)            
        End If

        mdlOperativeJobsPopup.Show()

    End Sub

#End Region

    Public Sub showOtherHours(ByVal operativeId As Integer)



        ViewState.Add(ViewStateConstants.OperativeId, operativeId)


        Dim resultDataSet As DataSet = New DataSet()
        Dim objTimeSheetBL As New TimeSheetBL()


        Dim objTimeSheetBO As New TimeSheetBO()
        objTimeSheetBO.fromDate = txtBoxFromDate.Text
        objTimeSheetBO.toDate = txtBoxToDate.Text
        objTimeSheetBO.operativeId = operativeId

        resultDataSet = objTimeSheetBL.getGetOtherkHours(objTimeSheetBO, resultDataSet)

        'objPageSortBo.TotalRecords = totalCount
        'objPageSortBo.TotalPages = Math.Ceiling(totalCount / objPageSortBo.PageSize)

        'If setSession Then
        '    ViewState.Add(ViewStateConstants.PageSortBo, objPageSortBo)
        '    ViewState.Add(ViewStateConstants.TotalCount, totalCount)
        'End If

        'grdOperativeJobs.VirtualItemCount = totalCount


        GridViewSickHours.DataSource = resultDataSet
        GridViewSickHours.DataBind()
        SickHourModalPopupExtender.Show()

    End Sub



    Public Sub showSickHours(ByVal operativeId As Integer)



        ViewState.Add(ViewStateConstants.OperativeId, operativeId)


        Dim resultDataSet As DataSet = New DataSet()
        Dim objTimeSheetBL As New TimeSheetBL()


        Dim objTimeSheetBO As New TimeSheetBO()
        objTimeSheetBO.fromDate = txtBoxFromDate.Text
        objTimeSheetBO.toDate = txtBoxToDate.Text
        objTimeSheetBO.operativeId = operativeId

        resultDataSet  = objTimeSheetBL.getGetSickHours(objTimeSheetBO, resultDataSet)

        'objPageSortBo.TotalRecords = totalCount
        'objPageSortBo.TotalPages = Math.Ceiling(totalCount / objPageSortBo.PageSize)

        'If setSession Then
        '    ViewState.Add(ViewStateConstants.PageSortBo, objPageSortBo)
        '    ViewState.Add(ViewStateConstants.TotalCount, totalCount)
        'End If

        'grdOperativeJobs.VirtualItemCount = totalCount


        GridViewSickHours.DataSource = resultDataSet
        GridViewSickHours.DataBind()
        SickHourModalPopupExtender.Show()

    End Sub

    Protected Sub btnCancelAction_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnPnlSickHoursClose.Click
       
        SickHourModalPopupExtender.Hide()
    End Sub



#Region "Export to excel"
    ''' <summary>
    ''' This is the function that export the gridview data in excel sheet.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub exportToExcel()
        Response.Clear()
        Response.Buffer = True
        Dim fileName As String = "TimeSheetReport_" + Convert.ToString(DateTime.Now.Day) + "_" + Convert.ToString(DateTime.Now.Month) + "_" + Convert.ToString(DateTime.Now.Year) + ".xls"
        Response.AddHeader("content-disposition", "attachment;filename=" + fileName)
        Response.Charset = ""
        Response.ContentType = "application/vnd.ms-excel"
        Dim strWrite As New StringWriter()
        Dim htmWrite As New HtmlTextWriter(strWrite)
        Dim htmfrm As New HtmlForm()
        grdTimeSheet.Parent.Controls.Add(htmfrm)
        htmfrm.Attributes("runat") = "server"
        htmfrm.Controls.Add(grdTimeSheet)
        htmfrm.RenderControl(htmWrite)
        Response.Write(strWrite.ToString())
        Response.Flush()
        Response.[End]()
    End Sub
#End Region

#Region "Page Sort BO"

    Protected Sub setPageSortBoViewState(ByRef pageSortBO As PageSortBO)
        ViewState.Item(ViewStateConstants.PageSortBo) = pageSortBO
    End Sub

    Protected Function getPageSortBoViewState()
        Dim pageSortBO As PageSortBO = New PageSortBO("DESC", "JsRef", 1, 30)

        If Not IsNothing(ViewState.Item(ViewStateConstants.PageSortBo)) Then
            pageSortBO = ViewState.Item(ViewStateConstants.PageSortBo)
        End If

        Return pageSortBO
    End Function

    Protected Sub removePageSortBoViewState()
        If Not IsNothing(ViewState.Item(ViewStateConstants.PageSortBo)) Then
            ViewState.Remove(ViewStateConstants.PageSortBo)
        End If
    End Sub

#End Region

#Region "Virtual Item Count Set/Get/Remove"

#Region "Virtual Item Count Set"

    Protected Sub setVirtualItemCountViewState(ByRef VirtualItemCount As Integer)
        ViewState.Item(ViewStateConstants.VirtualItemCount) = VirtualItemCount
    End Sub

#End Region

#Region "Virtual Item Count Get"

    Protected Function getVirtualItemCountViewState() As Integer
        If IsNothing(ViewState.Item(ViewStateConstants.VirtualItemCount)) Then
            Return -1
        Else
            Return CType(ViewState.Item(ViewStateConstants.VirtualItemCount), Integer)
        End If
    End Function

#End Region

#Region "Virtual Item Count Remove"

    Protected Sub removeVirtualItemCountViewState()
        ViewState.Remove(ViewStateConstants.VirtualItemCount)
    End Sub

#End Region

#End Region

    

#Region "Display Job Sheet Details"
    Private Sub displayJobSheetInfo(ByVal jobSheetNumber As String, ByVal appointmentType As String)
        Dim jsnLink As String = String.Empty
        If appointmentType = ApplicationConstants.FaultAppointment Then
            jsnLink = GeneralHelper.getJobSheetSummaryAddress() + jobSheetNumber
        ElseIf appointmentType.Contains("Void") Then
            jsnLink = GeneralHelper.getVoidJobSheetSummaryAddress() + jobSheetNumber + "&appointmentType=" + appointmentType
        ElseIf appointmentType = ApplicationConstants.PlannedAppointment _
        Or appointmentType = ApplicationConstants.MiscAppointment _
        Or appointmentType = ApplicationConstants.AdaptationAppointment _
        Or appointmentType = ApplicationConstants.ConditionAppointment Then
            jsnLink = GeneralHelper.getPlannedJobSheetSummaryAddress() + jobSheetNumber
        ElseIf appointmentType = ApplicationConstants.GasAppointment _
            Or appointmentType = ApplicationConstants.ApplianceServicingAppointment Then
            jsnLink = GeneralHelper.getGasJobSheetSummaryAddress() + jobSheetNumber
        ElseIf appointmentType = ApplicationConstants.DefectAppointment Then
            jsnLink = GeneralHelper.getDefectsJobSheetSummaryAddress() + jobSheetNumber
        ElseIf appointmentType = ApplicationConstants.CyclicMaintenanceAppointment _
         Or appointmentType = ApplicationConstants.MEServicingAppointment Then
            jsnLink = GeneralHelper.getMEJobSheetSummary() + jobSheetNumber
        End If
        ifrmJobSheet.Attributes("src") = jsnLink

        Me.popupJobSheet.Show()

    End Sub

#End Region

#End Region

End Class