﻿Imports System
Imports System.Web.HttpContext
Imports System.Web
Imports TS_BusinessObject

Namespace TS_Utilities

    Public Class SessionManager

#Region "Login In User "

#Region "Set / Get  Fault Scheduling User Id"

#Region "Set  Fault Scheduling User Id"
        Public Shared Sub setFaultSchedulingUserId(ByRef faultSchedulingUserId As Integer)
            Current.Session(SessionConstants.FaultSchedulingUserId) = faultSchedulingUserId
        End Sub
#End Region

#Region "get Fault Scheduling User Id"
        Public Shared Function getFaultSchedulingUserId() As Integer

            If (IsNothing(Current.Session(SessionConstants.FaultSchedulingUserId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionConstants.FaultSchedulingUserId), Integer)
            End If
        End Function
#End Region

#Region "remove  Fault Scheduling User Id"

        Public Sub removeFaultSchedulingUserId()
            If (Not IsNothing(Current.Session(SessionConstants.FaultSchedulingUserId))) Then
                Current.Session.Remove(SessionConstants.FaultSchedulingUserId)
            End If

        End Sub

#End Region

#End Region

#Region "Set / Get User Full Name"

#Region "Set User Full Name"

        Public Shared Sub setUserFullName(ByRef userFullName As String)
            Current.Session(SessionConstants.UserFullName) = userFullName
        End Sub

#End Region

#Region "get User Full Name"

        Public Shared Function getUserFullName() As String
            If (IsNothing(Current.Session(SessionConstants.UserFullName))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionConstants.UserFullName), String)
            End If
        End Function

#End Region

#Region "remove User Full Name"

        Public Shared Sub removeUserFullName()
            If (Not IsNothing(Current.Session(SessionConstants.UserFullName))) Then
                Current.Session.Remove(SessionConstants.UserFullName)
            End If

        End Sub

#End Region

#End Region

#Region "Set / Get User Employee Id"

#Region "Set User Employee Id"

        Public Shared Sub setUserEmployeeId(ByRef employeeId As String)
            Current.Session(SessionConstants.EmployeeId) = employeeId
        End Sub

#End Region

#Region "get User Employee Id"

        Public Shared Function getUserEmployeeId() As Integer
            If (IsNothing(Current.Session(SessionConstants.EmployeeId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionConstants.EmployeeId), Integer)
            End If
        End Function

#End Region

#Region "remove User Employee Id"

        Public Shared Sub removeUserEmployeeId()
            If (Not IsNothing(Current.Session(SessionConstants.EmployeeId))) Then
                Current.Session.Remove(SessionConstants.EmployeeId)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get User Type"

#Region "Set Logged In User Type"

        Public Shared Sub setLoggedInUserType(ByRef userType As String)
            Current.Session(SessionConstants.LoggedInUserType) = userType
        End Sub

#End Region

#Region "get Logged In User Type"

        Public Shared Function getLoggedInUserType() As String
            If (IsNothing(Current.Session(SessionConstants.LoggedInUserType))) Then
                Return String.Empty
            Else
                Return CType(Current.Session(SessionConstants.LoggedInUserType), String)
            End If
        End Function

#End Region

#Region "remove Logged In User Type"

        Public Shared Sub removeLoggedInUserType()
            If (Not IsNothing(Current.Session(SessionConstants.LoggedInUserType))) Then
                Current.Session.Remove(SessionConstants.LoggedInUserType)
            End If
        End Sub

#End Region

#End Region

#End Region

    End Class

End Namespace

