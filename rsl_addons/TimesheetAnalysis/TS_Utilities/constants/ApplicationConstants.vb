﻿Imports System
Imports System.Text

Namespace TS_Utilities

    Public Class ApplicationConstants

#Region "General"
        Public Shared DropDownDefaultValue As String = "-1"
        Public Shared Title As String = "Title"
        Public Shared RepairsAppointmentTitle As String = "Repairs"
        Public Shared FuelServicingAppointmentTitle As String = "Fuel Servicing"
        Public Shared PlannedAppointmentTitle As String = "Planned"
        Public Shared VoidAppointmentTitle As String = "Void"
#End Region

#Region "Master Layout"

        Public Shared ConWebBackgroundColor As String = "#e6e6e6"
        Public Shared ConWebBackgroundProperty As String = "background-color"
#End Region

#Region "Page Sort"

        Public Shared Ascending As String = "ASC"
        Public Shared Descending As String = "DESC"
        Public Shared DefaultPageNumber As String = 1
        Public Shared DefaultPageSize As String = 5
        Public Shared PageSortBo As String = "PageSortBo"
#End Region

#Region "Logged In User Type"
        Public Enum LoggedInUserTypes
            Manager = 1
            Scheduler = 2
        End Enum

#End Region

#Region "Time Sheet Report Data Table Column Names"

        Public Shared operative As String = "operative"
        Public Shared availableHours As String = "availableHours"
        Public Shared repairsHours As String = "repairsHours"
        Public Shared repairJobs As String = "repairJobs"
        Public Shared fuelServicingHours As String = "fuelServicingHours"
        Public Shared fuelServicingJobs As String = "fuelServicingJobs"
        Public Shared plannedHours As String = "plannedHours"
        Public Shared voidHours As String = "voidHours"
        Public Shared plannedJobs As String = "plannedJobs"
        Public Shared voidJobs As String = "voidJobs"
        Public Shared materialsHours As String = "materialsHours"

        Public Shared totalWorkedHours As String = "totalWorkedHours"
        Public Shared totalWorkPercentage As String = "percentage"
        Public Shared difference As String = "difference"

        Public Shared absentHoursSick As String = "absentHoursSick"
        Public Shared absentHoursOther As String = "absentHoursOther"

        Public Shared absentHoursTotal As String = "absentHoursTotal"

        Public Shared totalHours As String = "totalHours"

#End Region

        Public Shared VoidAppointment As String = "Void"
        Public Shared AppointmentTypeColumn As String = "AppointmentType"
        Public Shared PlannedAppointment As String = "Planned"
        Public Shared FaultAppointment As String = "Fault Repair"
        Public Shared MiscAppointment As String = "Misc"
        Public Shared ConditionAppointment As String = "Condition"
        Public Shared AdaptationAppointment As String = "Adaptation"
        Public Shared SbFaultAppointment As String = "SbFault"
        Public Shared CyclicMaintenanceAppointment As String = "Cyclic Maintenance"
        Public Shared MEServicingAppointment As String = "M&E Servicing"
        Public Shared GasAppointment As String = "GAS"
        Public Shared ApplianceServicingAppointment As String = "Appliance Servicing"
        Public Shared DefectAppointment As String = "Defects"

    End Class

End Namespace



