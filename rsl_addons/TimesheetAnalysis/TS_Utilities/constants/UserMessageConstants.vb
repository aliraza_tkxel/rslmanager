﻿
Namespace TS_Utilities


    Public Class UserMessageConstants


        Public Shared InvalidInput As String = "Invalid selection or input."
        Public Shared RecordNotFound As String = "Record not found."
        Public Shared ProblemLoadingData As String = "Problem loading data."
        Public Shared ErrorDataBaseConnection As String = "Error while connecting with database."
        Public Shared NoRecordFound As String = "No record found."
        Public Shared InvalidUrl As String = "Url is invalid."
        Public Shared UserDoesNotExist As String = "You don't have access to appliance servicing. Please contant administrator."

        Public Shared LoginRslManager As String = "Please use the login from rsl manager."

        Public Shared ClearPanelMessage As String = ""

        Public Shared InvalidSrc As String = "Invalid Src"
        Public Shared SrcDoesNotExist As String = "Src does not exist"
        Public Shared IncorrectDateInterval As String = "From date should not be later than to date"




#Region "Generic Messages"
        Public Shared RecordNotExist As String = "No record exists"
#End Region

#Region "Calendar Popup"
        Public Shared SelectActionDate As String = "Please select date."
#End Region

#Region "Users"

        Public Shared ErrorUserUpdate As String = "Error Updating Record"
        Public Shared ErrorUserInsert As String = "Error Inserting Record"

#End Region

    End Class
End Namespace

