﻿Imports System

Namespace TS_Utilities

    Public Class SessionConstants

#Region "User Session"

        Public Shared ConSessionUserId As String = "SESSION_USER_ID"
        Public Shared FaultSchedulingUserId As String = "AppServicingUserId"
        Public Shared UserFullName As String = "UserFullName"
        Public Shared EmployeeId As String = "UserEmployeeId"
        Public Shared LoggedInUserType As String = "LoggedInUserType"

#End Region
        
    End Class

End Namespace


