﻿Namespace TS_Utilities

    Public Class PathConstants

#Region "URL Constants"
        Public Const LoginPath As String = "~/../BHAIntranet/Login.aspx"
        Public Const AccessDeniedPath As String = "~/AccessDenied.aspx"
        Public Const TimeSheetPath As String = "~/Views/TimeSheetAnalysisReport.aspx"

        'bridge file path
        Public Const Bridge As String = "~/Bridge.aspx"


#End Region

#Region "Query String Constants"

        'QueryString Constants

#End Region

    End Class

End Namespace