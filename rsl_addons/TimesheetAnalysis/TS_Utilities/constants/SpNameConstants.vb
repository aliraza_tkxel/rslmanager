﻿Imports System

Namespace TS_Utilities

    Public Class SpNameConstants

#Region "Time Sheet"

        Public Shared getTimeSheetReport As String = "TS_GetTimeSheetAnalysisReport"
        Public Shared getRepairJobsDetails As String = "TS_GetRepairJobsDetails"
        Public Shared getOperativeJobsByAppointmenttType As String = "TS_GetOperativeJobsByAppointmenttType"
        Public Shared getLeavesHoursForEmployee As String = "TS_GetLeavesForEmployee"

#End Region


#Region "Login"

        Public Shared GetEmployeeById As String = "FL_GetEmployeeById"

#End Region

    End Class

End Namespace



