﻿Imports System

Namespace TS_Utilities

    Public Class ViewStateConstants

#Region "Page Sort Constants"
        Public Shared SortDirection As String = "SortDirection"
        Public Shared SortExpression As String = "SortExpression"
        Public Shared Ascending As String = "Ascending"
        Public Shared Descending As String = "Descending"
        Public Shared DefaultPageNumber As String = "10"
        Public Shared DefaultPageSize As String = "10"
        Public Shared PageSortBo As String = "PageSortBo"
        Public Shared TotalPageNumbers As String = "TotalPageNumbers"
        Public Shared CurrentPageNumber As String = "CurrentPageNumber"
#End Region

#Region "General "
        Public Shared AppointmentType As String = "AppointmentType"
        Public Shared OperativeId As String = "OperativeId"
        Public Shared GridDataSet As String = "GridDataSet"
        Public Shared ResultDataSet As String = "ResultDataSet"
        Public Shared SlideIndex As String = "SlideIndex"
        Public Shared PopupSlideIndex As String = "PopupSlideIndex"
        Public Shared RedirectTempCount As String = "RedirectTempCount"
        Public Shared TotalCount As String = "TotalCount"
        Public Shared LastBlockId As String = "LastBlockId"
        Public Shared VirtualItemCount As String = "VirtualItemCount"

#End Region

    End Class

End Namespace