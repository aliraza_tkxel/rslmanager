﻿Imports System.Globalization
Imports System.Web.HttpContext
Imports System.Web.UI.Page
Imports TS_BusinessObject
Imports System.Web.UI.WebControls


Namespace TS_Utilities

    Public Class GeneralHelper

#Region "get Us Cultured Date Time"

        ''' <summary>
        ''' Returns the US cultured date time.
        ''' </summary>
        ''' <param name="date">date to be converted.</param>
        ''' <returns>US Cultured DateTime</returns>

        Public Shared Function getUsCulturedDateTime(ByVal [date] As String) As DateTime
            Dim infoUS As New CultureInfo("en-US")
            Return Convert.ToDateTime([date].ToString(), infoUS)
        End Function
#End Region

#Region "get Us Cultured Date Time"

        ''' <summary>
        ''' Returns the UK cultured date time.
        ''' </summary>
        ''' <param name="date">date to be converted.</param>
        ''' <returns>UK Cultured DateTime</returns>

        Public Shared Function getUKCulturedDateTime(ByVal [date] As String) As DateTime
            Dim infoUK As New CultureInfo("en-GB")
            Return Convert.ToDateTime([date].ToString(), infoUK)
        End Function
#End Region

#Region "Populate Path Data"
        Public Shared Function populatePathsData() As Dictionary(Of String, String)
            Dim pathsData As Dictionary(Of String, String) = New Dictionary(Of String, String)

            Return pathsData
        End Function
#End Region

#Region "get Day Start Hour"
        Public Shared Function getDayStartHour() As String
            Return CType(System.Configuration.ConfigurationManager.AppSettings("DayStartHour"), Double)
        End Function
#End Region

#Region "get Day End Hour"
        Public Shared Function getDayEndHour() As String
            Return CType(System.Configuration.ConfigurationManager.AppSettings("DayEndHour"), Double)
        End Function
#End Region

#Region "get Unique Keys"
        Public Shared Function getUniqueKey(ByVal uniqueCounter As Integer) As String
            'this 'll be used to create the unique index of dictionary
            Dim uniqueKey As String = uniqueCounter.ToString() + SessionManager.getFaultSchedulingUserId().ToString() + DateTime.Now.ToString("ddMMyyyyhhmmss")
            Return uniqueKey
        End Function
#End Region

#Region "Convert Date In Seconds"
        Public Shared Function convertDateInSec(ByVal timeToConvert As Date) As Integer
            Dim calendarStartDate As New DateTime(1970, 1, 1)
            Dim timeInSec As Integer = (timeToConvert - calendarStartDate).TotalSeconds
            Return timeInSec
        End Function
#End Region

#Region "Convert Date In Minute"
        Public Shared Function convertDateInMin(ByVal timeToConvert As Date) As Integer
            Dim calendarStartDate As New DateTime(1970, 1, 1)
            Dim timeInSec As Integer = (timeToConvert - calendarStartDate).TotalSeconds
            Return timeInSec
        End Function
#End Region

#Region "Set Grid View Pager"

        Public Shared Sub setGridViewPager(ByRef pnlPagination As Panel, ByVal objPageSortBO As PageSortBO)
            pnlPagination.Visible = False

            If objPageSortBO.TotalRecords >= 1 Then
                pnlPagination.Visible = True

                Dim pageNumber As Integer = objPageSortBO.PageNumber
                Dim pageSize As Integer = objPageSortBO.PageSize
                Dim TotalRecords As Integer = objPageSortBO.TotalRecords
                Dim totalPages As Integer = objPageSortBO.TotalPages

                Dim lnkbtnPagerFirst As LinkButton = DirectCast(pnlPagination.FindControl("lnkbtnPagerFirst"), LinkButton)
                Dim lnkbtnPagerPrev As LinkButton = DirectCast(pnlPagination.FindControl("lnkbtnPagerPrev"), LinkButton)
                Dim lnkbtnPagerNext As LinkButton = DirectCast(pnlPagination.FindControl("lnkbtnPagerNext"), LinkButton)
                Dim lnkbtnPagerLast As LinkButton = DirectCast(pnlPagination.FindControl("lnkbtnPagerLast"), LinkButton)

                lnkbtnPagerFirst.Enabled = (pageNumber <> 1)
                lnkbtnPagerFirst.Font.Underline = lnkbtnPagerFirst.Enabled

                lnkbtnPagerPrev.Enabled = lnkbtnPagerFirst.Enabled
                lnkbtnPagerPrev.Font.Underline = lnkbtnPagerPrev.Enabled

                lnkbtnPagerNext.Enabled = (pageNumber <> totalPages)
                lnkbtnPagerNext.Font.Underline = lnkbtnPagerNext.Enabled

                lnkbtnPagerLast.Enabled = lnkbtnPagerNext.Enabled
                lnkbtnPagerLast.Font.Underline = lnkbtnPagerLast.Enabled

                Dim lblPagerCurrentPage As Label = DirectCast(pnlPagination.FindControl("lblPagerCurrentPage"), Label)
                lblPagerCurrentPage.Text = pageNumber

                Dim lblPagerTotalPages As Label = DirectCast(pnlPagination.FindControl("lblPagerTotalPages"), Label)
                lblPagerTotalPages.Text = totalPages

                Dim lblPagerRecordStart As Label = DirectCast(pnlPagination.FindControl("lblPagerRecordStart"), Label)
                Dim pagerRecordStart As Integer = ((pageNumber - 1) * pageSize) + 1
                lblPagerRecordStart.Text = pagerRecordStart

                Dim lblPagerRecordEnd As Label = DirectCast(pnlPagination.FindControl("lblPagerRecordEnd"), Label)
                Dim pagerRecordEnd As Integer = pagerRecordStart + pageSize - 1
                lblPagerRecordEnd.Text = IIf(pagerRecordEnd < TotalRecords, pagerRecordEnd, TotalRecords)

                Dim lblPagerRecordTotal As Label = DirectCast(pnlPagination.FindControl("lblPagerRecordTotal"), Label)
                lblPagerRecordTotal.Text = TotalRecords

            End If
        End Sub

#End Region

#Region "Get Job Sheet Summary Address"
        Public Shared Function getJobSheetSummaryAddress() As String
            Return "../../RSLJobSheetSummary/Views/JobSheets/JobSheetSummary.aspx?jsn="
        End Function
#End Region


#Region "Get Job Sheet Summary Address"

        Public Shared Function getSbJobSheetSummaryAddress() As String
            Return "../../RSLJobSheetSummary/Views/JobSheets/SbJobSheetSummary.aspx?jsn="
        End Function

#End Region

#Region "Get Job Sheet Summary  for Voids"

        Public Shared Function getVoidJobSheetSummaryAddress() As String
            Return "../../RSLJobSheetSummary/Views/JobSheets/JSVJobSheetSummary.aspx?jsn="
        End Function

#End Region

#Region "Get Job Sheet Summary for Planned"

        Public Shared Function getPlannedJobSheetSummaryAddress() As String
            Return "../../RSLJobSheetSummary/Views/JobSheets/JobSheetSummaryPlanned.aspx?jsn="
        End Function

#End Region

#Region "Get Job Sheet Summary for Gas"

        Public Shared Function getGasJobSheetSummaryAddress() As String
            Return "../../RSLJobSheetSummary/Views/JobSheets/ServicingJobSheetSummary.aspx?jsn="
        End Function

#End Region

#Region "Get Job Sheet Summary for Gas"

        Public Shared Function getDefectsJobSheetSummaryAddress() As String
            Return "../../RSLJobSheetSummary/Views/JobSheets/DefectsJobSheetSummary.aspx?jsn="
        End Function

#End Region
#Region "Get Job Sheet Summary for Cyclic Maintenance and "

        Public Shared Function getMEJobSheetSummary() As String
            Return "../../RSLJobSheetSummary/Views/JobSheets/MaintenanceJobSheetSummary.aspx?jsn="
        End Function

#End Region

    End Class

End Namespace
