﻿Imports System.Data
Imports System.Net
Imports System.IO
Imports System.Xml

Namespace TS_Utilities

    Public Class CalendarUtilities

#Region "Formating Date with suffix like 1st, 2nd, 3rd"
        Public Function FormatDayNumberSuffix(ByVal calendarDay As String) As String
            Select Case calendarDay
                Case 1, 21, 31
                    calendarDay = calendarDay + "st"
                Case 2, 22
                    calendarDay = calendarDay + "nd"
                Case 3, 23
                    calendarDay = calendarDay + "rd"
                Case Else
                    calendarDay = calendarDay + "th"
            End Select

            Return calendarDay
        End Function
#End Region

#Region "Calculate Last Monday Of The Month"
        Public Sub calculateMonday()
            For dayCount = 0 To 10
                'If (DateAdd("d", -(dayCount), ApplicationConstants.startDate).ToString("dddd") = "Monday") Then
                '    ApplicationConstants.startDate = DateAdd("d", -(dayCount), ApplicationConstants.startDate)
                '    ApplicationConstants.weekDays = ApplicationConstants.weekDays + dayCount
                '    dayCount = 10
                'End If
            Next
        End Sub
#End Region

#Region "Calculate Last Sunday of the Month"
        Public Sub calculateSunday()
            For dayCount = 0 To 10
                'If (DateAdd("d", dayCount, ApplicationConstants.endDate).ToString("dddd") = "Sunday") Then
                '    ApplicationConstants.endDate = DateAdd("d", dayCount, ApplicationConstants.endDate)
                '    ApplicationConstants.weekDays = ApplicationConstants.weekDays + dayCount
                '    dayCount = 10
                'End If
            Next
        End Sub
#End Region

#Region "Format Date Function"

        'this utility functions converts date from mm/dd/yyyy formate to dd/mm/yyyy
        Public Shared Function FormatDate(ByVal str As String) As String
            If Not str.Equals("") And Not (str Is Nothing) Then
                Dim strArr As String() = str.Split(" ")
                Return Convert.ToDateTime(strArr(0)).ToString("dd/MM/yyyy")
            Else
                Return ("")
            End If
        End Function

#End Region

    End Class

End Namespace