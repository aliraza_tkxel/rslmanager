﻿Imports TS_Utilities
Imports TS_DataAccess
Imports TS_BusinessObject

Namespace TS_BusinessLogic

    Public Class TimeSheetBL

#Region "Attributes"
        Dim objTimeSheetDAL As New TimeSheetDAL()
#End Region

#Region "Functions"

#Region "Get Time Sheet Report"
        Public Sub getTimeSheetReport(ByRef timeSheetBO As TimeSheetBO, ByRef resultDS As DataSet)
            objTimeSheetDAL.getTimeSheetReport(timeSheetBO, resultDS)
        End Sub
#End Region

#Region "Get Operative Jobs"
        Public Function getOperativeJobs(ByRef timeSheetBO As TimeSheetBO, ByRef resultDS As DataSet, ByVal objPageSortBo As PageSortBO)
            Return objTimeSheetDAL.getOperativeJobs(timeSheetBO, resultDS, objPageSortBo)
        End Function
#End Region

#Region "Get Sick Hours"
        Public Function getGetSickHours(ByRef timeSheetBO As TimeSheetBO, ByRef resultDS As DataSet)
            Return objTimeSheetDAL.GetSickHours(timeSheetBO, resultDS)
        End Function
#End Region

#Region "Get Other Hours"
        Public Function getGetOtherkHours(ByRef timeSheetBO As TimeSheetBO, ByRef resultDS As DataSet)
            Return objTimeSheetDAL.GetotherHours(timeSheetBO, resultDS)
        End Function
#End Region


#End Region
        
    End Class

End Namespace
