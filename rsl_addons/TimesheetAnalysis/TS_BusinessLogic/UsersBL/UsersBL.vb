﻿Imports System
Imports TS_BusinessObject
Imports System.Data.SqlClient
Imports TS_DataAccess
Imports System.Web
Imports System.Drawing
Imports System.Web.UI.WebControls
Imports TS_Utilities


Namespace TS_BusinessLogic

    Public Class UsersBL

#Region "Attributes"
        Dim objUserDAL As UsersDAL = New UsersDAL()
#End Region

#Region "Functions"
        Function getEmployeeById(ByVal employeeId As Integer) As DataSet
            Return objUserDAL.getEmployeeById(employeeId)
        End Function
#End Region

    End Class

End Namespace
