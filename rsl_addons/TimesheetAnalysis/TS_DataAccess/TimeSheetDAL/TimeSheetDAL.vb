﻿Imports TS_BusinessObject
Imports TS_Utilities

Namespace TS_DataAccess

    Public Class TimeSheetDAL : Inherits BaseDAL

#Region "Functions"

#Region "Get Time Sheet Report"

        Public Sub getTimeSheetReport(ByRef timeSheetBO As TimeSheetBO, ByRef resultDS As DataSet)

            Dim inParametersList As ParameterList = New ParameterList()

            'Add in Parameters
            Dim fromDateInParam As ParameterBO = New ParameterBO("fromDate", timeSheetBO.fromDate, DbType.DateTime2)
            inParametersList.Add(fromDateInParam)

            Dim toDateInParam As ParameterBO = New ParameterBO("toDate", timeSheetBO.toDate, DbType.DateTime2)
            inParametersList.Add(toDateInParam)

            Dim searchTextInParam As ParameterBO = New ParameterBO("searchText", timeSheetBO.searchText, DbType.String)
            inParametersList.Add(searchTextInParam)

            'Call base class function to fill dataset by providing parameters and StoredProcedure Name
            LoadDataSet(resultDS, inParametersList, SpNameConstants.getTimeSheetReport)

        End Sub

#End Region



#Region "Get Sickness hours"

        Public Function GetSickHours(ByRef timeSheetBO As TimeSheetBO, ByRef resultDS As DataSet)

            Dim inParametersList As ParameterList = New ParameterList()
            Dim outParametersList As New ParameterList()

            Dim operativeIdParam As ParameterBO = New ParameterBO("employeeId", timeSheetBO.operativeId, DbType.Int32)
            inParametersList.Add(operativeIdParam)

            Dim fromDateInParam As ParameterBO = New ParameterBO("fromDate", timeSheetBO.fromDate, DbType.DateTime2)
            inParametersList.Add(fromDateInParam)

            Dim toDateInParam As ParameterBO = New ParameterBO("toDate", timeSheetBO.toDate, DbType.DateTime2)
            inParametersList.Add(toDateInParam)

            Dim isSick As ParameterBO = New ParameterBO("isSick", 1, DbType.Int32)

            inParametersList.Add(isSick)

            MyBase.LoadDataSet(resultDS, inParametersList, outParametersList, SpNameConstants.getLeavesHoursForEmployee)
            Return resultDS

        End Function



#End Region

#Region "Get Other hours"

        Public Function GetotherHours(ByRef timeSheetBO As TimeSheetBO, ByRef resultDS As DataSet)

            Dim inParametersList As ParameterList = New ParameterList()
            Dim outParametersList As New ParameterList()

            Dim operativeIdParam As ParameterBO = New ParameterBO("employeeId", timeSheetBO.operativeId, DbType.Int32)
            inParametersList.Add(operativeIdParam)

            Dim fromDateInParam As ParameterBO = New ParameterBO("fromDate", timeSheetBO.fromDate, DbType.DateTime2)
            inParametersList.Add(fromDateInParam)

            Dim toDateInParam As ParameterBO = New ParameterBO("toDate", timeSheetBO.toDate, DbType.DateTime2)
            inParametersList.Add(toDateInParam)

            Dim isSick As ParameterBO = New ParameterBO("isSick", 0, DbType.Int32)

            inParametersList.Add(isSick)

            MyBase.LoadDataSet(resultDS, inParametersList, outParametersList, SpNameConstants.getLeavesHoursForEmployee)
            Return resultDS

        End Function



#End Region




#Region "Get Operative jobs"

        Public Function getOperativeJobs(ByRef timeSheetBO As TimeSheetBO, ByRef resultDS As DataSet, ByVal objPageSortBo As PageSortBO)

            Dim inParametersList As ParameterList = New ParameterList()
            Dim outParametersList As New ParameterList()

            Dim operativeIdParam As ParameterBO = New ParameterBO("operativeId", timeSheetBO.operativeId, DbType.Int32)
            inParametersList.Add(operativeIdParam)

            Dim fromDateInParam As ParameterBO = New ParameterBO("fromDate", timeSheetBO.fromDate, DbType.DateTime2)
            inParametersList.Add(fromDateInParam)

            Dim toDateInParam As ParameterBO = New ParameterBO("toDate", timeSheetBO.toDate, DbType.DateTime2)
            inParametersList.Add(toDateInParam)

            Dim aptTypeParam As ParameterBO = New ParameterBO("aptType", timeSheetBO.appointmentType, DbType.String)
            inParametersList.Add(aptTypeParam)

            Dim pageSize As New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            inParametersList.Add(pageSize)

            Dim pageNumber As New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            inParametersList.Add(pageNumber)

            Dim sortColumn As New ParameterBO("sortColumn", objPageSortBo.SortExpression, DbType.String)
            inParametersList.Add(sortColumn)

            Dim sortOrder As New ParameterBO("sortOrder", objPageSortBo.SmallSortDirection, DbType.String)
            inParametersList.Add(sortOrder)

            Dim totalCountParam As New ParameterBO("totalCount", 0, DbType.Int32)
            outParametersList.Add(totalCountParam)

            MyBase.LoadDataSet(resultDS, inParametersList, outParametersList, SpNameConstants.getOperativeJobsByAppointmenttType)
            Return outParametersList.Item(0).Value.ToString()

        End Function

#End Region

#End Region

    End Class

End Namespace

