-- Author:		Muhammad Abdul Wahhab
-- Create date: 30/07/2013

USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[E_ABSENCEHOURS](
	[AbsenceHourID] [int] IDENTITY(1,1) NOT NULL,
	[EmployeeID] [int] NOT NULL,
	[AbsenceDate] [date] NOT NULL,
	[AbsenceHoursSick] [float] NOT NULL,
	[AbsenceHoursOthers] [float] NOT NULL,
 CONSTRAINT [PK_E_AbsentHours] PRIMARY KEY CLUSTERED 
(
	[AbsenceHourID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO