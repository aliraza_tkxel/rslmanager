USE [RSLBHALive1]
GO

/****** Object:  UserDefinedFunction [dbo].[TS_GetRepairHoursAndJobs]    Script Date: 08/03/2013 15:22:58 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Muhammad Abdul Wahhab
-- Create date: 26/07/2013
-- Description:	Gets the Repair Hours and Job Data for an operative with in specific dates
-- =============================================
CREATE FUNCTION [dbo].[TS_GetRepairHoursAndJobs] 
(
	-- Add the parameters for the function here
	@operativeID int,
	@fromDate DateTime,
	@toDate DateTime
)
RETURNS 
@result TABLE 
(
	-- Add the column definitions for the TABLE variable here
	repairHours float,
	repairJobs int
)
AS
BEGIN
	-- Fill the table variable with the rows for your result set
	DECLARE @repairHours float = 0	
	DECLARE @repairJobs int = 0
	
	-- Fetching the repair hours and jobs
	
	SELECT @repairHours = ROUND(SUM(DATEDIFF(mi,FL_FAULT_JOBTIMESHEET.StartTime,FL_FAULT_JOBTIMESHEET.EndTime))/60.0,2), @repairJobs = COUNT(FL_FAULT_JOBTIMESHEET.FaultLogId)
	FROM FL_FAULT_JOBTIMESHEET
	JOIN FL_CO_APPOINTMENT ON FL_CO_APPOINTMENT.AppointmentID = FL_FAULT_JOBTIMESHEET.APPOINTMENTID
	WHERE FL_CO_APPOINTMENT.OperativeID = @operativeID AND FL_FAULT_JOBTIMESHEET.StartTime >= @fromDate AND
	FL_FAULT_JOBTIMESHEET.EndTime <= @toDate
	
	IF @repairHours IS NULL
		SET @repairHours = 0
		
	IF @repairJobs IS NULL
		SET @repairJobs = 0
	
	INSERT INTO @result values (@repairHours,@repairJobs)
	
	RETURN 
END

GO


