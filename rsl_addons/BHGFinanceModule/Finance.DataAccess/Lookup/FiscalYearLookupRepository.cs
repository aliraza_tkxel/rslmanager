﻿using Finance.DataAccess.Common;
using Finance.DataAccess.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Finance.BusinessObjects.Common;

namespace Finance.DataAccess.Lookup
{
    public class FiscalYearLookupRepository : GenericRepository<F_FISCALYEARS>, IFiscalYearLookupRepository
    {
        public FiscalYearLookupRepository(IUnitOfWork uow) : base(uow)
        {

        }

        public int GetCurrentFiscalYearId()
        {
            var currentFiscalYear = Db.F_FISCALYEARS.Where(e => DbFunctions.TruncateTime(e.YStart.Value) <= DbFunctions.TruncateTime(DateTime.Now)
                && DbFunctions.TruncateTime(e.YEnd) >= DbFunctions.TruncateTime(DateTime.Now)).FirstOrDefault();

            return currentFiscalYear.YRange;
        }

        public FiscalYear GetCurrentFiscalYearMonth()
        {
            // TODO:
            var currentFiscalYear = Db.F_FISCALYEARS.Where(e => DbFunctions.TruncateTime(e.YStart.Value) <= DbFunctions.TruncateTime(DateTime.Now)
                && DbFunctions.TruncateTime(e.YEnd) >= DbFunctions.TruncateTime(DateTime.Now)).FirstOrDefault();

            if (currentFiscalYear == null)
            {
                return new FiscalYear();
            }

            //var response = new FiscalYear() {  YEnd = new DateTime(2017, 3, 31), YRange = 18, YStart= new DateTime(2016, 4, 1) };
            var response = new FiscalYear();
            response.YStart = currentFiscalYear.YStart.Value;
            response.YRange = currentFiscalYear.YRange;
            response.YEnd = currentFiscalYear.YEnd.Value;

            return response;
        }

        public List<FiscalYear> GetAllFiscalYear()
        {
            return Db.F_FISCALYEARS.Select(e =>
                new FiscalYear()
                {
                    YRange = e.YRange,
                    YStart = e.YStart ?? e.YStart.Value,
                    YEnd = e.YEnd ?? e.YEnd.Value
                }).ToList();
        }

        public string GetFiscalYears(DateTime fromDate, DateTime toDate)
        {
            return string.Join(",", Db.F_FISCALYEARS.Where(e => DbFunctions.TruncateTime(e.YStart.Value) >= DbFunctions.TruncateTime(fromDate) || DbFunctions.TruncateTime(toDate) <= DbFunctions.TruncateTime(e.YStart.Value) ||
                DbFunctions.TruncateTime(e.YEnd.Value) >= DbFunctions.TruncateTime(fromDate) || DbFunctions.TruncateTime(toDate) <= DbFunctions.TruncateTime(e.YEnd.Value))
                                 .Select(e => e.YRange.ToString()));
        }
    }
}
