﻿using Finance.DataAccess.Common;
using Finance.DataAccess.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Finance.BusinessObjects.Common;

namespace Finance.DataAccess.Lookup
{
    public class CompanyLookupRepository : GenericRepository<G_Company>, ICompanyLookupRepository
    {
        public CompanyLookupRepository(IUnitOfWork uow) : base(uow)
        {

        }

        public List<G_Company> GetCompany()
        {
            return Db.G_Company.ToList();
        }
    }
}
