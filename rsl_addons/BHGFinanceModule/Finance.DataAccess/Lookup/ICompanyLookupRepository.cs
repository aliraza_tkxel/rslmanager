﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finance.BusinessObjects.Common;
using Finance.DataAccess.DomainModel;

namespace Finance.DataAccess.Lookup
{
    public interface ICompanyLookupRepository
    {
        List<G_Company> GetCompany();
    }
}
