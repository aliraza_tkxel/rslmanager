﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finance.BusinessObjects.Common;

namespace Finance.DataAccess.Lookup
{
    public interface IFiscalYearLookupRepository
    {
        int GetCurrentFiscalYearId();
        FiscalYear GetCurrentFiscalYearMonth();
        string GetFiscalYears(DateTime fromDate, DateTime toDate);
        List<FiscalYear> GetAllFiscalYear();
    }
}
