﻿using Finance.BusinessObjects.Menu;
using F.BusinessObjects.Navigation;
using Finance.DataAccess.Common;
using Finance.DataAccess.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finance.DataAccess.Menu
{
    public interface IMenuRepository : IRepository<AC_MENUS>
    {
        MenuResponse GetMenus(MenuRequest menuRequest);
        List<SubMenuItem> GetSubMenus(int menuId, int userId);
        List<ModuleItem> GetSideMenus(int userId);
        List<MenuViewModel> GetMenusByModule(int userId, int moduleId);
        int GetModuleId(string moduleName);
        List<ModuleViewModel> GetModules(int userId);
    }
}
