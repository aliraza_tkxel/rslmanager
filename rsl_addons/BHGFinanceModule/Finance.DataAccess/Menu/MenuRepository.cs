﻿using Finance.BusinessObjects.Menu;
using F.BusinessObjects.Navigation;
using Finance.DataAccess.Common;
using Finance.DataAccess.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finance.BusinessObjects.Common;

namespace Finance.DataAccess.Menu
{
    public class MenuRepository : GenericRepository<AC_MENUS>, IMenuRepository
    {
        #region >>> Prop & Constructor <<< 

        public MenuRepository(IUnitOfWork uow) : base(uow)
        {

        }

        #endregion

        #region >>> Helper Method <<<



        #endregion

        #region >>> Methods <<<

        #region Get Sub Menus on top bar
        public List<SubMenuItem> GetSubMenus(int menuId, int userId)
        {

            return Db.E__EMPLOYEE
                 .Join(
                     Db.AC_PAGES_ACCESS,
                     emp => emp.JobRoleTeamId,
                     access => access.JobRoleTeamId,
                     (emp, access) => new { emp, access })
                 .Join(
                     Db.AC_PAGES,
                     acc => acc.access.PageId,
                     page => page.PAGEID,
                     (acc, page) => new
                     {
                         Access = acc.access,
                         Emp = acc.emp,
                         Page = page
                     })
                 .Join(
                     Db.AC_MENUS, acc => acc.Page.MENUID, menu => menu.MENUID,
                     (acc, menu) => new
                     {
                         Access = acc.Access,
                         Emp = acc.Emp,
                         Page = acc.Page,
                         Menu = menu
                     })
                 .Join(
                     Db.AC_MODULES,
                     acc => acc.Menu.MODULEID,
                     module => module.MODULEID,
                     (acc, module) => new
                     {
                         Access = acc.Access,
                         Emp = acc.Emp,
                         Page = acc.Page,
                         Menu = acc.Menu,
                         Module = module
                     })
                 .Where(e => e.Page.ACTIVE == 1
                     && e.Page.LINK == 1
                     && e.Emp.EMPLOYEEID == userId
                     && e.Menu.MENUID == menuId)
                 .Select(m => new SubMenuItem()
                 {
                     SubMenuName = m.Page.DESCRIPTION,
                     MenuId = m.Menu.MENUID,
                     Path = string.Concat(m.Module.DIRECTORY, "/", m.Page.PAGE)
                 }).OrderBy(e => e.SubMenuName).ToList();
        }
        #endregion

        #region Get Customer module Menus
        public List<ModuleItem> GetSideMenus(int userId)
        {
            return Db.P_GetRSLModulesList(userId, 1).Select(e => new ModuleItem()
            {
                ModuleId = e.MODULEID,
                ModuleName = e.DESCRIPTION,
                Url = e.THEPATH
            }).ToList();
        }
        #endregion

        #region Displaying Menu on top bar
        public MenuResponse GetMenus(MenuRequest objMenuRequest)
        {
            //Althought a store procedure exist which did same,but we need to skip customer from menu as per requirement document. so for that purpose, this function is created

            //start getting moduleId from module name

            int? moduleId = Db.AC_MODULES.Where(e => e.DESCRIPTION == objMenuRequest.ModuleName).FirstOrDefault()?.MODULEID;
            if (moduleId == null)
            {
                moduleId = 0;
            }

            //end getting moduleId from module name

            //start filling menus list

            var MenusAndSubMenuList = Db.E__EMPLOYEE
                .Join(
                    Db.AC_MENUS_ACCESS,
                    emp => emp.JobRoleTeamId,
                    access => access.JobRoleTeamId,
                    (emp, access) => new { emp, access })
                .Join(
                    Db.AC_MENUS,
                    acc => acc.access.MenuId,
                    menu => menu.MENUID,
                    (acc, menu) => new { Access = acc.access, Emp = acc.emp, Menu = menu })
                .Join(
                    Db.AC_MODULES,
                    acc => acc.Menu.MODULEID,
                    module => module.MODULEID,
                    (acc, module) => new { Access = acc.Access, Emp = acc.Emp, Menu = acc.Menu, Module = module })
                .Where(e => e.Menu.ACTIVE == 1
                    && e.Emp.EMPLOYEEID == objMenuRequest.UserId
                    && e.Module.MODULEID == moduleId
                    && string.Compare(e.Menu.DESCRIPTION, objMenuRequest.ModuleName, true) == 1)
                .Select(m => new MenuItem()
                {
                    MenuId = m.Menu.MENUID,
                    MenuName = m.Menu.DESCRIPTION
                }).ToList();

            //end filling menus list

            //start filling sub menus list
            foreach (var item in MenusAndSubMenuList)
            {
                item.objLstSubMenus = GetSubMenus(item.MenuId, objMenuRequest.UserId);
            }
            //end filling sub menus list

            //start creating response, cosisting of menu,subMenus and Customer module menus
            MenuResponse objMenusResponseBO = new MenuResponse();
            objMenusResponseBO.objMenusBO = MenusAndSubMenuList.ToList();
            objMenusResponseBO.objLstCustomerModuleMenus = GetSideMenus(objMenuRequest.UserId);//get customer module menus
            return objMenusResponseBO;
        }
        #endregion

        #region Get Menus By ModuleId
        public List<MenuViewModel> GetMenusByModule(int userId, int moduleId)
        {
            List<MenuViewModel> menuList = null;
            var menus = (from emp in Db.E__EMPLOYEE
                         join access in Db.AC_MENUS_ACCESS on emp.JobRoleTeamId equals access.JobRoleTeamId
                         join menu in Db.AC_MENUS on access.MenuId equals menu.MENUID
                         join module in Db.AC_MODULES on menu.MODULEID equals module.MODULEID
                         where menu.ACTIVE == 1
                         && emp.EMPLOYEEID == userId
                         && module.MODULEID == moduleId
                         orderby menu.ORDERTEXT, menu.DESCRIPTION
                         select new MenuViewModel
                         {
                             menuId = menu.MENUID,
                             menuName = menu.DESCRIPTION,
                             path = menu.PAGE
                         }
                        );

            if (menus.Count() > 0)
            {
                menuList = menus.ToList();
                foreach (var item in menuList)
                {
                    item.subMenus = GetSubMenuList(item.menuId, userId);
                }
            }

            return menuList;
        }
        #endregion

        #region Get Sub Menus
        public List<SubmenuViewModel> GetSubMenuList(int menuId, int userId)
        {
            var employeeId = userId.ToString();

            var subMenus = (from emp in Db.E__EMPLOYEE
                            join access in Db.AC_PAGES_ACCESS on emp.JobRoleTeamId equals access.JobRoleTeamId
                            join page in Db.AC_PAGES on access.PageId equals page.PAGEID
                            join menu in Db.AC_MENUS on page.MENUID equals menu.MENUID
                            join module in Db.AC_MODULES on menu.MODULEID equals module.MODULEID
                            where page.ACTIVE == 1
                            && page.LINK == 1
                            && emp.EMPLOYEEID == userId
                            && menu.MENUID == menuId
                            orderby menu.ORDERTEXT, menu.DESCRIPTION
                            select new SubmenuViewModel
                            {
                                subMenuName = page.DESCRIPTION,
                                menuId = menu.MENUID,
                                path = (page.DESCRIPTION == ApplicationConstants.ActiveMenuPage )
                                        ? string.Concat("/", module.DIRECTORY, "/", page.PAGE, employeeId)
                                        : string.Concat("/", module.DIRECTORY, "/", page.PAGE)
                            }
                        );
            return subMenus.ToList();
        }
        #endregion

        #region "Get Module Id by Module name"
        public int GetModuleId(string moduleName)
        {
            int moduleId = 0;
            var module = (from mdl in Db.AC_MODULES
                          where mdl.DESCRIPTION == moduleName
                          select mdl).FirstOrDefault();
            if (module != null)
            {
                moduleId = module.MODULEID;
            }
            return moduleId;
        }
        #endregion

        #region "Get Modules List"
        public List<ModuleViewModel> GetModules(int userId)
        {

            var orderDes = 1;
            var modules = (from sideMenus in Db.P_GetRSLModulesList(userId, orderDes)
                           select new ModuleViewModel
                           {
                               moduleId = sideMenus.MODULEID,
                               moduleName = sideMenus.DESCRIPTION,
                               url = sideMenus.THEPATH
                           }
                );

            return modules.ToList();
        }
        #endregion

        #endregion
    }
}
