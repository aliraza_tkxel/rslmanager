﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Finance.DataAccess.Common
{
    public interface IRepository<T>
    {
        IQueryable<T> GetAll();

        T Delete(T entity);

        IQueryable<T> FindBy(Expression<Func<T, bool>> predicate);

        void Edit(T entity);

        void Save();

        T Add(T entity);
    }
}
