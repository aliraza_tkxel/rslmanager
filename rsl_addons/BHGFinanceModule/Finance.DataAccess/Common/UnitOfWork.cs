﻿using Finance.DataAccess.DomainModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Data.Entity;
using System.Threading.Tasks;
using System.Data.Entity.Core;

namespace Finance.DataAccess.Common
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {

        private readonly RSLBHALiveEntities _dbContext = null;

        /// <summary>
        /// _dbTransaction
        /// </summary>
        //private DbContextTransaction _dbTransaction = null;

        /// <summary>
        /// Initializes a new instance of the <see cref="UnitOfWork"/> class.
        /// </summary>
        public UnitOfWork()
        {
            //_dbContext = new ProjectContext(ConnectionStringProvider.GetDataConnectionString());
            //_dbContext = new RSLBHALiveEntities("ProjectConnection");
            _dbContext = new RSLBHALiveEntities();
        }

        /// <summary>
        /// Data Context.
        /// </summary>
        /// <returns></returns>
        public RSLBHALiveEntities DataContext()
        {
            //return _dbContext ?? new ProjectContext(ConnectionStringProvider.GetDataConnectionString());
            //return _dbContext ?? new RSLBHALiveEntities("ProjectConnection");
            return _dbContext ?? new RSLBHALiveEntities();
        }

        /// <summary>
        /// Begins the transaction where multiple saveChanges are required or multiple contexts involved.
        /// </summary>
        /// <returns></returns>
        public void BeginTransaction()
        {
            _dbContext.Database.Connection.Open();
           // _dbTransaction = _dbContext.Database.BeginTransaction();
        }

        public void CommitTransaction()
        {
            //if (_dbTransaction != null)
            //    _dbTransaction.Commit();

            if (_dbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                _dbContext.Database.Connection.Close();
        }

        public void RollBackTransaction()
        {
            //if (_dbTransaction != null)
            //    _dbTransaction.Rollback();
            if (_dbContext.Database.Connection.State == System.Data.ConnectionState.Open)
                _dbContext.Database.Connection.Close();
        }

        public void Save()
        {
            try
            {
                _dbContext.SaveChanges();
            }
            catch (OptimisticConcurrencyException)
            {
                RollBackTransaction();
            }
        }

        public void Dispose()
        {
            //if (_dbTransaction != null)
            //{
            //    _dbTransaction.Dispose();
            //}
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }
    }
}
