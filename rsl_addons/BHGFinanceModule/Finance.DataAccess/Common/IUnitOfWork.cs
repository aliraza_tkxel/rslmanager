﻿using Finance.DataAccess.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finance.DataAccess.Common
{
    public interface IUnitOfWork
    {
        /// <summary>
        /// Saves this instance
        /// </summary>
        void Save();

        /// <summary>
        /// DB context
        /// </summary>
        /// <returns></returns>
        RSLBHALiveEntities DataContext();

        /// <summary>
        /// Begins transaction
        /// </summary>
        /// <returns></returns>
        void BeginTransaction();

        /// <summary>
        /// Commits transaction
        /// </summary>
        void CommitTransaction();

        /// <summary>
        /// Rolls back transaction
        /// </summary>
        void RollBackTransaction();

    }
}
