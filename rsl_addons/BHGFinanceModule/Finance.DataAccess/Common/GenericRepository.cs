﻿using Finance.DataAccess.DomainModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Finance.DataAccess.Common
{
    public class GenericRepository<T> : IRepository<T>
        where T : class
    {
        protected RSLBHALiveEntities Db = null;
        protected readonly DbSet<T> Table;

        public GenericRepository(IUnitOfWork uow)
        {
            //
            if (uow == null)
            {
                throw new ArgumentNullException();
            }
            Db = uow.DataContext();
            Table = Db.Set<T>();
        }

        public virtual IQueryable<T> GetAll()
        {
            return Table;
        }

        public virtual IQueryable<T> FindBy(Expression<Func<T, bool>> predicate)
        {
            return Table.Where(predicate);
        }

        public virtual T Add(T entity)
        {
            return Table.Add(entity);
        }

        public virtual T Delete(T entity)
        {
            return Table.Remove(entity);
        }

        public virtual void Edit(T entity)
        {
            Db.Entry(entity).State = EntityState.Modified;
        }

        public virtual void Save()
        {
            Db.SaveChanges();
        }
    }
}
