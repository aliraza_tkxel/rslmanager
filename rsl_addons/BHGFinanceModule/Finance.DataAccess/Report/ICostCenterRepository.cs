﻿using Finance.BusinessObjects.Report;
using Finance.DataAccess.Common;
using Finance.DataAccess.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finance.DataAccess.Report
{
    public interface ICostCenterRepository : IRepository<F_COSTCENTRE>
    {
        List<CostCenter> GetAllCostCenter();

        decimal GetTotalCostCentre(DateTime? fromDate, DateTime? toDate, int? company);
        CostCenterAllResponse GetCostCenterAll(DateTime fromDate, DateTime toDate, int company);
        CostCenterAllResponse GetExpense(DateTime fromDate, DateTime toDate, int headId);
        CostCenterAllResponse GetHead(DateTime fromDate, DateTime toDate, int costCenterId);
    }
}
