﻿using Finance.DataAccess.Common;
using Finance.DataAccess.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finance.BusinessObjects.Report;
using Finance.DataAccess.Lookup;

namespace Finance.DataAccess.Report
{
    public class CostCenterRepository : GenericRepository<F_COSTCENTRE>, ICostCenterRepository
    {

        private IFiscalYearLookupRepository _fiscalYearRepository = null;

        public CostCenterRepository(IUnitOfWork uow) : base(uow)
        {

        }

        public CostCenterRepository(IUnitOfWork uow, IFiscalYearLookupRepository fiscalYearRepository) : base(uow)
        {
            _fiscalYearRepository = fiscalYearRepository;
        }

        #region  >>> Methods <<<

        public List<CostCenter> GetAllCostCenter()
        {
            return new List<CostCenter>();
        }

        public CostCenterAllResponse GetCostCenterAll(DateTime fromDate, DateTime toDate, int company)
        {
            fromDate = new DateTime(fromDate.Year, fromDate.Month, fromDate.Day, 0, 0, 0);
            toDate = new DateTime(toDate.Year, toDate.Month, toDate.Day, 23, 59, 0);
            var costCenterAllResponse = new CostCenterAllResponse();
            costCenterAllResponse.TotalCostCenter = Db.F_Get_CostCenter_TotalCostCenter(fromDate, toDate, company).FirstOrDefault();
            costCenterAllResponse.CostCenterAllList = Db.F_Get_CostCenter_CostCenterAll(fromDate, toDate, company).Select(c => new CostCenterAll()
            {
                CostCenterId = c.COSTCENTREID,
                CostCenterName = c.CostCenterName,
                Budget = c.BUDGET,
                NominalLedger = c.NL,
                Purchase = c.PURCHASES,
                Balance = c.BAL
            }).ToList();

            return costCenterAllResponse;
        }

        public CostCenterAllResponse GetHead(DateTime fromDate, DateTime toDate, int costCenterId)
        {
            fromDate = new DateTime(fromDate.Year, fromDate.Month, fromDate.Day, 0, 0, 0);
            toDate = new DateTime(toDate.Year, toDate.Month, toDate.Day, 23, 59, 0);
            var costCenterAllResponse = new CostCenterAllResponse();

            costCenterAllResponse.HeadList = Db.F_Get_CostCenter_Head(fromDate, toDate, costCenterId).Select(h => new Head()
            {
                HeadId = h.HEADID,
                CostCentreId = h.COSTCENTREID,
                HeadName = h.HEADNAME,
                Budget = h.BUDGET,
                NominalLedger = h.NL,
                Purchase = h.PURCHASES,
                Balance = h.BAL
            }).ToList();

            return costCenterAllResponse;
        }

        public CostCenterAllResponse GetExpense(DateTime fromDate, DateTime toDate, int headId)
        {
            fromDate = new DateTime(fromDate.Year, fromDate.Month, fromDate.Day, 0, 0, 0);
            toDate = new DateTime(toDate.Year, toDate.Month, toDate.Day, 23, 59, 0);
            var costCenterAllResponse = new CostCenterAllResponse();

            costCenterAllResponse.ExpenseList = Db.F_Get_CostCenter_Expense(fromDate, toDate, headId).Select(e => new Expense()
            {
                ExpenditureId = e.EXPENDITUREID,
                HeadId = e.HEADID,
                ExpenseName = e.ExpenseName,
                Budget = e.BUDGET,
                NominalLedger = e.NL,
                Purchase = e.PURCHASES,
                Balance = e.BAL
            }).ToList();

            return costCenterAllResponse;
        }

        public List<CostCenter> GetSearchCostCenter(DateTime fromDate, DateTime toDate)
        {


            return new List<CostCenter>();
        }

        public decimal GetTotalCostCentre(DateTime? fromDate, DateTime? toDate, int? company)
        {
            var fiscalYears = "";
            decimal totalCostCenter = 0;
            var defaultValue = Db.RSL_DEFAULTS.Where(e => e.DEFAULTNAME == "DEVELOPMENTID").Select(x => x.DEFAULTVALUE).ToArray();
            if (fromDate.HasValue && toDate.HasValue)
            {
                fiscalYears = _fiscalYearRepository.GetFiscalYears(fromDate.Value, toDate.Value);
            }
            else
            {
                fiscalYears = _fiscalYearRepository.GetCurrentFiscalYearId().ToString();
            }

            totalCostCenter = Db.F_COSTCENTRE
                .GroupJoin(
                    Db.F_COSTCENTRE_ALLOCATION,
                    cc => (Int32?)(cc.COSTCENTREID),
                    cca => cca.COSTCENTREID,
                    (cc, ccaGroup) =>
                        new
                        {
                            cc = cc,
                            ccaGroup = ccaGroup
                        }
                )
                .SelectMany(
                    temp0 => temp0.ccaGroup,
                    (temp0, item) =>
                        new
                        {
                            temp0 = temp0,
                            item = item
                        }
                )
                .Where(
                    temp1 =>
                        (temp1.item.FISCALYEAR.Value.ToString().Contains(fiscalYears)
                         &&
                            !(Db.RSL_DEFAULTS
                                    .Where(o => (o.DEFAULTNAME == "DEVELOPMENTID"))
                                    .Select(o => o.DEFAULTVALUE)
                                    .Contains(temp1.temp0.cc.COSTCENTREID.ToString())
                                )
                        )
                )
                .Sum(e => e.item.COSTCENTREALLOCATION).Value;


            return totalCostCenter;
        }

        #endregion

    }
}
