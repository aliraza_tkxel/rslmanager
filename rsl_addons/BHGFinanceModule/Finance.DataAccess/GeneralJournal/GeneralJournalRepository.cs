﻿using F.BusinessObjects.GeneralJournal;
using Finance.DataAccess.Common;
using Finance.DataAccess.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finance.DataAccess.GeneralJournal
{
    public class GeneralJournalRepository : GenericRepository<NL_GENERALJOURNAL>, IGeneralJournalRepository
    {
        public GeneralJournalRepository(IUnitOfWork uow) : base(uow)
        {

        }


        public GeneralJournalTemplateListResponse GetGeneralJournalTemplateList()
        {
            var templateListResponse = new GeneralJournalTemplateListResponse();
            var templateListQuery = (from t in Db.NL_GeneralJournalTemplate
                                 join e in Db.E__EMPLOYEE on t.CreatedBy equals e.EMPLOYEEID into eg
                                 from e in eg.DefaultIfEmpty()
                                 where t.IsActive == true
                                 orderby t.CreatedDate descending
                                 select new GeneralJournalTemplate {
                                   templateId = t.TemplateId,
                                   templateName = t.TemplateName,
                                   createdBy = e == null ? "-" : e.FIRSTNAME.Substring(0,1) +" "+ e.LASTNAME ,
                                   createdDate = t.CreatedDate                                                                     
                                 });

            if (templateListQuery.Count() > 0)
            {
                var templateList = templateListQuery.ToList();

                foreach (GeneralJournalTemplate template in templateList)
                {
                    if (template.createdDate.HasValue)
                    {
                        DateTime createdDate = (DateTime)template.createdDate;
                        template.createdDateString = createdDate.ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        template.createdDateString = "-";
                    }
                }
                templateListResponse.templateList = templateList;
            }
        
            return templateListResponse;
        }

        public GeneralJournalTemplateDetailResponse GetGeneralJournalTemplateDetail(int templateId)
        {
            var templateDetailResponse = new GeneralJournalTemplateDetailResponse();
            var templateQuery = (from t in Db.NL_GeneralJournalTemplate
                                 join e in Db.E__EMPLOYEE on t.CreatedBy equals e.EMPLOYEEID into eg
                                 from e in eg.DefaultIfEmpty()
                                 join c in Db.G_Company on t.CompanyId equals c.CompanyID into cg
                                 from c in cg.DefaultIfEmpty()
                                 join s in Db.P_SCHEME on t.SchemeId equals s.SCHEMEID into sg
                                 from s in sg.DefaultIfEmpty()
                                 join b in Db.P_BLOCK on t.BlockId equals b.BLOCKID into bg
                                 from b in bg.DefaultIfEmpty()
                                 join p in Db.P__PROPERTY on t.PropertyId equals p.PROPERTYID into pg
                                 from p in pg.DefaultIfEmpty()
                                 where t.TemplateId == templateId
                                 orderby t.CreatedDate descending
                                 select new GeneralJournalTemplate
                                 {
                                     templateId = t.TemplateId,
                                     templateName = t.TemplateName,
                                     createdDate = t.CreatedDate,
                                     createdBy = e == null ? "-" : e.FIRSTNAME.Substring(0, 1) + " " + e.LASTNAME,
                                     company = c.Description,
                                     scheme = s == null ? "-" : s.SCHEMENAME,
                                     block = b == null ? "-" : b.BLOCKNAME,
                                     property = p == null ? "-" : p.HOUSENUMBER + " " + p.ADDRESS1
                                                                         
                                 });

            var template = templateQuery.FirstOrDefault();
            if (template != null)
            {                
                if (template.createdDate.HasValue)
                {
                    DateTime createdDate = (DateTime)template.createdDate;
                    template.createdDateString = createdDate.ToString("dd/MM/yyyy");
                }
                else
                {
                    template.createdDateString = "-";
                }

                templateDetailResponse.templateDetail = template;


                var accountList = (from t in Db.NL_GeneralJournalAccountTemplate
                                   join a in Db.NL_ACCOUNT on t.AccountId equals a.ACCOUNTID

                                   join c in Db.F_COSTCENTRE on t.CostCenterId equals c.COSTCENTREID into cg
                                   from c in cg.DefaultIfEmpty()

                                   join h in Db.F_HEAD on t.HeadId equals h.HEADID into hg
                                   from h in hg.DefaultIfEmpty()

                                   join e in Db.F_EXPENDITURE on t.ExpenditureId equals e.EXPENDITUREID into eg
                                   from e in eg.DefaultIfEmpty()

                                   where t.GeneralJournalTemplateId == templateId
                                   orderby t.Sequence ascending
                                   select new GeneralJournalAccount
                                   {
                                       templateId = t.TemplateId,
                                       details = t.Details,
                                       account = a.ACCOUNTNUMBER + " " + a.NAME,
                                       isDebit = t.IsDebit,
                                       costCentre = c == null ? "-" : c.DESCRIPTION,
                                       head = h == null ? "-" : h.DESCRIPTION,
                                       expenditure = e == null ? "-" : e.DESCRIPTION,
                                       reason = t.Reason,
                                       defaultDebitAmount = t.DefaultDebitAmount ?? 0,
                                       defaultCreditAmount = t.DefaultCreditAmount ?? 0
                                   }).ToList();
                templateDetailResponse.accountList = accountList;

            }

            return templateDetailResponse;
        }

        public bool DeleteTemplate(int templateId)
        {
            var template = Db.NL_GeneralJournalTemplate.Where(t => t.TemplateId == templateId).FirstOrDefault();
            var isDeleted = false;
            if (template != null)
            {
                template.IsActive = false;
                isDeleted = true;
                Db.SaveChanges();
            }
            
            return isDeleted;
        }

        public GeneralJournalAccount GetGeneralJournalAccountDetail(int templateId)
        {
            var account = new GeneralJournalAccount();

            var accountQuery = (from t in Db.NL_GeneralJournalAccountTemplate
                               join a in Db.NL_ACCOUNT on t.AccountId equals a.ACCOUNTID

                               join c in Db.F_COSTCENTRE on t.CostCenterId equals c.COSTCENTREID into cg
                               from c in cg.DefaultIfEmpty()

                               join h in Db.F_HEAD on t.HeadId equals h.HEADID into hg
                               from h in hg.DefaultIfEmpty()

                               join e in Db.F_EXPENDITURE on t.ExpenditureId equals e.EXPENDITUREID into eg
                               from e in eg.DefaultIfEmpty()

                               where t.TemplateId == templateId
                               select new GeneralJournalAccount
                               {
                                   templateId = t.TemplateId,
                                   details = t.Details,
                                   account = a.ACCOUNTNUMBER + " " + a.NAME,
                                   isDebit = t.IsDebit,
                                   costCentre = c == null ? "-" : c.DESCRIPTION,
                                   head = h == null ? "-" : h.DESCRIPTION,
                                   expenditure = e == null ? "-" : e.DESCRIPTION,
                                   reason = t.Reason,
                                   defaultDebitAmount = t.DefaultDebitAmount ?? 0,
                                   defaultCreditAmount = t.DefaultCreditAmount ?? 0
                               }).FirstOrDefault();

            if (accountQuery != null)
            {
                account = accountQuery;
            }

            return account;
        }

    }
}
