﻿using F.BusinessObjects.GeneralJournal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finance.DataAccess.GeneralJournal
{
    public interface IGeneralJournalRepository
    {
        GeneralJournalTemplateListResponse GetGeneralJournalTemplateList();
        GeneralJournalTemplateDetailResponse GetGeneralJournalTemplateDetail(int templateId);
        bool DeleteTemplate(int templateId);
        GeneralJournalAccount GetGeneralJournalAccountDetail(int templateId);
    }
}
