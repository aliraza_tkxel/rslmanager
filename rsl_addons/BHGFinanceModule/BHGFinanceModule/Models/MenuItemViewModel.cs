﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGFinanceModule.Models
{
    /// <summary>
    /// Menu Reponse 
    /// </summary>
    public class MenuItemViewModel
    {
        /// <summary>
        /// Menu Id
        /// </summary>
        public int MenuId { get; set; }
        /// <summary>
        /// Menu name to be shown on Top Navigation
        /// </summary>
        public string MenuName { get; set; }
        /// <summary>
        /// List os Sub Menus
        /// </summary>
        public List<SubMenuItemViewModel> objLstSubMenus { get; set; } = new List<SubMenuItemViewModel>();
    }
}