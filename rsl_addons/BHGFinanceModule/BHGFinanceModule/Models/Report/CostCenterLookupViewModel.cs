﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BHGFinanceModule.Models.Report
{
    public class CostCenterLookupViewModel
    {

        public int CategoryId { get; set; }

        /// <summary>
        /// ReadOnly property. 
        /// </summary>
        [Display(Name = "From")]
        public DateTime MonthFrom { get; set; }
       
        public DateTime MonthTo { get; set; }

        [Display(Name = "Financial year from April to:")]
        public int MonthToId { get; set; }

        [Display(Name = "From:")]
        public DateTime DateFrom { get; set; }

        [Display(Name = "To:")]
        public DateTime DateTo { get; set; }

        public string ModelDateFormate { get; set; }


        public bool IsYear { get; set; } = false;

        [Display(Name = "Financial Year")]
        public int FiscalYearId { get; set; }

        public List<SelectListItem> FiscalYear { get; set; } = new List<SelectListItem>();

        public List<SelectListItem> FiscalYearMonth { get; set; } = new List<SelectListItem>();

        public List<SelectListItem> Company { get; set; } = new List<SelectListItem>();
    }
}