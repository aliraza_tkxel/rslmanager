﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace BHGFinanceModule.Models.Report
{
    public class CostCenterListViewModel
    {
        public int CostCenterId { get; set; }
        public string Name { get; set; }
        public decimal AnnualBudget { get; set; }
        public decimal? SpentToDate { get; set; }
        public decimal? CommittedToDate { get; set; }
        public decimal Balance { get; set; }


        public string AnnualBudgetString { get { return AnnualBudget.ToString("C", CultureInfo.GetCultureInfo("en-GB")); } }
        public string SpentToDateString { get { return SpentToDate.HasValue ? SpentToDate.Value.ToString("C", CultureInfo.GetCultureInfo("en-GB")) : "--"; } }
        public string CommittedToDateString { get { return CommittedToDate.HasValue? CommittedToDate.Value.ToString("C", CultureInfo.GetCultureInfo("en-GB")) : "--"; } }
        public string BalanceString { get { return Balance.ToString("C", CultureInfo.GetCultureInfo("en-GB")); } }

        public List<HeadListViewModel> HeadList { get; set; } = new List<HeadListViewModel>();
    }
}