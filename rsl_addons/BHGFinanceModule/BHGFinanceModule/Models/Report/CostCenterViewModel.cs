﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace BHGFinanceModule.Models.Report
{
    public class CostCenterViewModel
    {
        public CostCenterLookupViewModel CostCenterLookup { get; set; } = new CostCenterLookupViewModel();

        public List<CostCenterListViewModel> CostCenterList { get; set; } = new List<CostCenterListViewModel>();

        public string TotalBudgetValue { get; set; }

        public string TotalBudgetString { get { return "£" + TotalBudgetValue; } }
    }
}