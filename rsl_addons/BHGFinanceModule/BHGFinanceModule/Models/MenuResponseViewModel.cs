﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGFinanceModule.Models
{
    /// <summary>
    /// Menu Response
    /// </summary>
    public class MenuResponseViewModel
    {
        /// <summary>
        /// Sub Menu Items
        /// </summary>
        public List<MenuItemViewModel> objMenusBO { get; set; }
        /// <summary>
        /// Sub Menu Items of Customer Module
        /// </summary>
        public List<ModuleItemViewModel> objLstCustomerModuleMenus { get; set; }
        /// <summary>
        /// Converts JSON response into Object
        /// </summary>
        /// <param name="jsonResponse">string response of JSON</param>
        /// <returns><see cref="MenuResponse"/> object</returns>
        public static MenuResponseViewModel ToObject(string jsonResponse)
        {
            MenuResponseViewModel obj = (MenuResponseViewModel)Newtonsoft.Json.JsonConvert.DeserializeObject(jsonResponse,
                typeof(MenuResponseViewModel));
            return obj;
        }
    }
}