﻿using Finance.BusinessLogic.GeneralJournal;
using Finance.BusinessLogic.Lookup;
using Finance.BusinessLogic.Menu;
using Finance.BusinessLogic.Report;
using Finance.DataAccess.Common;
using Finance.DataAccess.GeneralJournal;
using Finance.DataAccess.Lookup;
using Finance.DataAccess.Menu;
using Finance.DataAccess.Report;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BHGFinanceModule.App_Start
{
    public class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            return container;
        });

        /// <summary>
        /// Gets the configured Unity container.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            return container.Value;
        }
        #endregion

        /// <summary>Registers the type mappings with the Unity container.</summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>There is no need to register concrete types such as controllers or API controllers (unless you want to 
        /// change the defaults), as Unity allows resolving a concrete type even if it was not previously registered.</remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            //Unit Of Work 
            container.RegisterType<IUnitOfWork, UnitOfWork>();
            container.RegisterType<ICostCenterManager, CostCenterManager>();
            container.RegisterType<IMenuManager, MenuManager>();
            container.RegisterType<IFiscalYearLookupManager, FiscalYearLookupManager>();
            container.RegisterType<ICompanyLookupManager, CompanyLookupManager>();
            container.RegisterType<IGeneralJournalManager, GeneralJournalManager>();

            // NOTE: To load from web.config uncomment the line below. Make sure to add a Microsoft.Practices.Unity.Configuration to the using statements.
            // container.LoadConfiguration();

            // TODO: Register your types here
            container.RegisterType<ICostCenterRepository, CostCenterRepository>();
            container.RegisterType<IMenuRepository, MenuRepository>();
            container.RegisterType<IFiscalYearLookupRepository, FiscalYearLookupRepository>();
            container.RegisterType<ICompanyLookupRepository, CompanyLookupRepository>();
            container.RegisterType<IGeneralJournalRepository, GeneralJournalRepository>();
        }

    }
}