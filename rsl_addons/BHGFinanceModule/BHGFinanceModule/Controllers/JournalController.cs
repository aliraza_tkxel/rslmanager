﻿using Finance.BusinessLogic.GeneralJournal;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BHGFinanceModule.Controllers
{
    public class JournalController : BaseController
    {        
        private IGeneralJournalManager _generalJournalManager = null;

        public JournalController(IGeneralJournalManager generalJournalManager)
        {
            _generalJournalManager = generalJournalManager;
        }

        public ActionResult GeneralJournalTemplate()
        {            
            return View();
        }

        public ActionResult GetGeneralJournalTemplateList() {
            var model = _generalJournalManager.GetGeneralJournalTemplateList();
            return PartialView("~/Views/Journal/_GeneralJournalTemplateList.cshtml", model);
        }

        public ActionResult GetGeneralJournalTemplateDetail(int templateId)
        {
            var model = _generalJournalManager.GetGeneralJournalTemplateDetail(templateId);
            return PartialView("~/Views/Journal/_GeneralJournalTemplateDetail.cshtml", model);
        }

        public bool DeleteTemplate(int templateId)
        {
            return _generalJournalManager.DeleteTemplate(templateId);
        }
        
        public JsonResult GetAccountTemplateDetail(int templateId)
        {
            var model = _generalJournalManager.GetGeneralJournalAccountDetail(templateId);
            return Json(JsonConvert.SerializeObject(model),JsonRequestBehavior.AllowGet);            
        }
    }
}