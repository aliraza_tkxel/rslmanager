﻿using BHGFinanceModule.Models.Report;
using Finance.BusinessLogic.Lookup;
using Finance.BusinessLogic.Report;
using Finance.BusinessObjects.Common;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BHGFinanceModule.Controllers
{
    public class CostCenterController : BaseController
    {
        private ICostCenterManager _costCenterManager = null;
        private IFiscalYearLookupManager _fiscalYearLookupManager = null;
        private ICompanyLookupManager _companyLookupManager = null;

        public CostCenterController(ICostCenterManager costCenterManager, IFiscalYearLookupManager fiscalYearLookupManager, ICompanyLookupManager companyLookupManager)
        {
            _costCenterManager = costCenterManager;
            _fiscalYearLookupManager = fiscalYearLookupManager;
            _companyLookupManager = companyLookupManager;
        }

        private void PopulateCostCenterList(DateTime startDate, DateTime endDate, int company, CostCenterViewModel model)
        {
            var response = _costCenterManager.GetCostCenterAll(startDate, endDate, company);
            model.TotalBudgetValue = response.TotalCostCenter;
            model.CostCenterList = response.CostCenterAllList.Select(c =>  new CostCenterListViewModel()
            {
                CostCenterId = c.CostCenterId,
                Name = c.CostCenterName,
                AnnualBudget = c.Budget,
                CommittedToDate = c.Purchase,
                SpentToDate = c.NominalLedger,
                Balance = c.Balance,
                HeadList = response.HeadList.Where(h => h.CostCentreId == c.CostCenterId).Select(h => new HeadListViewModel()
                {
                    HeadId = h.HeadId,
                    Name = h.HeadName,
                    CommittedToDate = h.Purchase,
                    SpentToDate = h.NominalLedger,
                    Balance = h.Balance,
                    AnnualBudget = h.Budget,
                    ExpenditureList = response.ExpenseList.Where(e => e.HeadId == h.HeadId).Select(e => new ExpenditureListViewModel()
                    {
                        ExpenditureId = e.ExpenditureId,
                        Name = e.ExpenseName,
                        CommittedToDate = e.Purchase,
                        SpentToDate = e.NominalLedger,
                        Balance = e.Balance,
                        AnnualBudget = e.Budget,
                    }).ToList()
                }).ToList()
            }).ToList();
        }

        //
        // GET: /CostCenter/
        [HttpGet]
        public ActionResult Index()
        {
            var model = new CostCenterViewModel();
            var startDate = new DateTime();
            var endDate = new DateTime();
            var company = new int();
            company = 1;

            model.CostCenterLookup.FiscalYearMonth = _fiscalYearLookupManager.GetCurrentFiscalYearMonth(out startDate, out endDate)
                .Select(e => new SelectListItem() { Text = e.Name, Value = e.MonthNo }).ToList();

            model.CostCenterLookup.FiscalYear = _fiscalYearLookupManager.GetAllFiscalYear()
                .Select(e => new SelectListItem() { Text = e.Name, Value = e.FiscalYearId }).ToList();

            model.CostCenterLookup.Company = _companyLookupManager.GetCompany()
                .Select(e => new SelectListItem() { Text = e.Description, Value = e.CompanyId }).ToList();

            model.CostCenterLookup.ModelDateFormate = DATE_FORMAT_FOR_MODEL;
            model.CostCenterLookup.MonthFrom = startDate;
            model.CostCenterLookup.DateFrom = startDate;
            model.CostCenterLookup.DateTo = endDate;

            PopulateCostCenterList(startDate, endDate, company,  model);

            return View(model);
        }

        
        public JsonResult DoSearch(string fromDate, string toDate, int company)
        {
            var model = new CostCenterViewModel();
            DateTime _fromDate = DateTime.ParseExact(fromDate, "dd/MM/yyyy", null);
            DateTime _toDate = DateTime.ParseExact(toDate, "dd/MM/yyyy", null);

            PopulateCostCenterList(_fromDate, _toDate, company,  model);

            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadHead(string fromDate, string toDate, int costCenterId)
        {
            DateTime _fromDate = DateTime.ParseExact(fromDate, "dd/MM/yyyy", null);
            DateTime _toDate = DateTime.ParseExact(toDate, "dd/MM/yyyy", null);

            var response = _costCenterManager.GetHead(_fromDate, _toDate, costCenterId);
            var headList = response.HeadList.Select(h => new HeadListViewModel()
            {
                HeadId = h.HeadId,
                Name = h.HeadName,
                CommittedToDate = h.Purchase,
                SpentToDate = h.NominalLedger,
                Balance = h.Balance,
                AnnualBudget = h.Budget,
            }).ToList();

            return Json(headList, JsonRequestBehavior.AllowGet);
        }

        public JsonResult LoadExpense(string fromDate, string toDate, int headId)
        {
            DateTime _fromDate = DateTime.ParseExact(fromDate, "dd/MM/yyyy", null);
            DateTime _toDate = DateTime.ParseExact(toDate, "dd/MM/yyyy", null);

            var response = _costCenterManager.GetExpense(_fromDate, _toDate, headId);
            var expenseList = response.ExpenseList.Select(e => new ExpenditureListViewModel()
            {
                ExpenditureId = e.ExpenditureId,
                HeadId = e.HeadId,
                Name = e.ExpenseName,
                CommittedToDate = e.Purchase,
                SpentToDate = e.NominalLedger,
                Balance = e.Balance,
                AnnualBudget = e.Budget,
            }).ToList();

            return Json(expenseList, JsonRequestBehavior.AllowGet);
        }
    }
}