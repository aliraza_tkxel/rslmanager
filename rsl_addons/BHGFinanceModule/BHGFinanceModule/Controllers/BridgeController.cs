﻿using BHGFinanceModule.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BHGFinanceModule.Controllers
{
    public class BridgeController : Controller
    {
        // GET: Bridge
        public ActionResult Index()
        {
            int userId = int.Parse(Request.QueryString["UserId"]);
            Session["UserId"] = userId;

           var module = Request.QueryString["Module"];
            if (module.Equals(ApplicationConstants.AccountsModuleName))
            {
                return RedirectToAction("GeneralJournalTemplate", "Journal");
            }

            return RedirectToAction("Index", "CostCenter");
        }
    }
}