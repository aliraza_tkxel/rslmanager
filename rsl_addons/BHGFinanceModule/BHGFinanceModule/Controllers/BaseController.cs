﻿using Finance.BusinessObjects.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace BHGFinanceModule.Controllers
{
    public class BaseController : Controller
    {
        protected readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        Bridge objBridge = new Bridge();
        protected string DEFAULT_DATE_FORMAT = ConfigurationManager.AppSettings[ApplicationConstants.KeyDateFormat].ToString();
        protected string DATE_FORMAT_FOR_MODEL = ConfigurationManager.AppSettings[ApplicationConstants.KeyDateFormatModel].ToString();
        public BaseController()
        {

        }

        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            if (User == null) return;
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            try
            {
                if (filterContext.ExceptionHandled)
                {
                    return;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ex.StackTrace + ex.InnerException);
                HandleException(filterContext, ex.Message);
            }
        }
        private void HandleException(ExceptionContext context, string Message)
        {
            context.Result = new ViewResult
            {
                ViewData = new ViewDataDictionary(),
                ViewName = "ErrorView"
            };
            ViewBag.ErrorMessage = ApplicationConstants.UsersAccountDeactivated;
            context.ExceptionHandled = true;
        }

        protected override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            filterContext.HttpContext.Response.Cache.SetExpires(DateTime.UtcNow.AddDays(-1));
            filterContext.HttpContext.Response.Cache.SetValidUntilExpires(false);
            filterContext.HttpContext.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
            filterContext.HttpContext.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            filterContext.HttpContext.Response.Cache.SetNoStore();
            base.OnResultExecuting(filterContext);
        }
        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            // start 
            //var userId = 113;
            // TODO: comment this portion when run on local 
            var userId = GetUserIdFromSession();
            //if (objBridge.LoadUser(userId) == false)
            //{
            //    throw new Exception();
            //}
            //end
            if (userId == 0)
            {
                objBridge.redirectToLoginPage();
            }
        }
        /// <summary>
        /// Get user id from session
        /// </summary>
        /// <returns>UserId</returns>
        /// <exception cref="Exception">If Session variable is not set then set UserId = 0</exception>
        protected int GetUserIdFromSession()
        {
            int userId = 0;

            // start 
            // comment this portion when run on local 
            try
            {
                userId = int.Parse(Session["UserId"].ToString());
            }
            catch
            {
                userId = 0;
            }
            //end
            
            return userId;
        }
    }
}