﻿using BHGFinanceModule.Models;
using Finance.BusinessLogic.Common;
using Finance.BusinessLogic.Menu;
using Finance.BusinessObjects.Common;
using Finance.BusinessObjects.Menu;
using F.BusinessObjects.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BHGFinanceModule.Controllers
{
    public class MenuController : BaseController
    {
        #region >>> Prop & Constructor <<<

        private IMenuManager _menuManager = null;

        public MenuController(IMenuManager menuManager)
        {
            _menuManager = menuManager;
        }

        #endregion


        #region >>> Helper Method <<<

        /// <summary>
        /// Adds Host url in Menu Items path
        /// </summary>
        /// <param name="url">Host url</param>
        /// <param name="menuItems"><see cref="IEnumerable{MenuItem}"/></param>
        /// <returns></returns>
        private List<MenuItem> AddHostUrlInMenuPath(string url, List<MenuItem> menuItems)
        {
            string subMenu;
            foreach (var parent in menuItems)
            {
                foreach (var sub in parent.objLstSubMenus)
                {
                    subMenu = sub.SubMenuName;
                    if (string.Compare(subMenu, ApplicationConstants.AsbMenu, true) == 0)
                    {
                        sub.Path += GetUserIdFromSession();
                    }
                }

                parent.objLstSubMenus.Select(c => { c.Path = url + "/" + c.Path; return c; }).ToList();
            }
            return menuItems;
        }
        /// <summary>
        /// Adds Host url in Menu Items path
        /// </summary>
        /// <param name="url">Host url</param>
        /// <param name="menuItems"><see cref="IEnumerable{ModuleItem}"/></param>
        /// <returns></returns>
        private List<ModuleItem> AddHostUrlInMenuPath(string url, List<ModuleItem> menuItems)
        {
            menuItems.Select(m => { m.Url = url + m.Url; return m; }).ToList();
            return menuItems;
        }

        #endregion


        #region >>> Action Methods <<<

        // GET: Menu
        public ActionResult GetTopNavigationMenu()
        {
            var incomingRequest = "~/../..";
            MenuRequest request = new MenuRequest();
            request.UserId = GetUserIdFromSession();
            MenuResponse menus = _menuManager.GetTopNavigationMenu(request);
            menus.objMenusBO = AddHostUrlInMenuPath(incomingRequest, menus.objMenusBO);
            menus.objLstCustomerModuleMenus = AddHostUrlInMenuPath(incomingRequest, menus.objLstCustomerModuleMenus);


            // mapper use here to convert MenuResponse to MenuResponseViewModel
            MenuResponseViewModel menuResponse = new MenuResponseViewModel();
            // mapp values
            var lstModuleItemViewModel = new List<ModuleItemViewModel>();
            menus.objLstCustomerModuleMenus.ToList().ForEach(e =>
            {
                var objModuleItemViewModel = new ModuleItemViewModel();
                objModuleItemViewModel.ModuleId = e.ModuleId;
                objModuleItemViewModel.ModuleName = e.ModuleName;
                objModuleItemViewModel.Url = e.Url;

                lstModuleItemViewModel.Add(objModuleItemViewModel);
            });

            var lstMenuItemViewModel = new List<MenuItemViewModel>();
            //var lstSubMenuItemViewModel = new List<SubMenuItemViewModel>();
            menus.objMenusBO.ToList().ForEach(e =>
            {
                var objMenuItemViewModel = new MenuItemViewModel();
                objMenuItemViewModel.MenuId = e.MenuId;
                objMenuItemViewModel.MenuName = e.MenuName;

                e.objLstSubMenus.ToList().ForEach(m =>
                {
                    var objSubMenuItemViewModel = new SubMenuItemViewModel();
                    objSubMenuItemViewModel.MenuId = m.MenuId;
                    objSubMenuItemViewModel.SubMenuName = m.SubMenuName;
                    objSubMenuItemViewModel.Path = m.Path;

                    //lstSubMenuItemViewModel.Add(objSubMenuItemViewModel);
                    objMenuItemViewModel.objLstSubMenus.Add(objSubMenuItemViewModel);
                });


                lstMenuItemViewModel.Add(objMenuItemViewModel);
            });

            menuResponse.objMenusBO = lstMenuItemViewModel;

            menuResponse.objLstCustomerModuleMenus = lstModuleItemViewModel;
            return PartialView("~/Views/Shared/_TopNavigation.cshtml", menuResponse);
        }

        public ActionResult GetAccountsNavigationMenu()
        {
            var request = new NavigationRequestViewModel();
            request.userId = GetUserIdFromSession();
            request.moduleName = ApplicationConstants.AccountsModule;
            NavigationViewModel navViewmodel = _menuManager.GetMenus(request);
            navViewmodel.currentModule = ApplicationConstants.AccountsModule;
            navViewmodel.currentMenu = ApplicationConstants.NominalMenu;
            return PartialView("~/Views/Shared/_AccountsNavigation.cshtml", navViewmodel);
        }

        #endregion

    }
}