﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BHGFinanceModule.Startup))]
namespace BHGFinanceModule
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
