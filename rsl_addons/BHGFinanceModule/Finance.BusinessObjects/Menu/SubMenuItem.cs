﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finance.BusinessObjects.Menu
{
    public class SubMenuItem
    {
        /// <summary>
        /// Parent MenuItem id
        /// </summary>
        public int MenuId { get; set; }
        /// <summary>
        /// Menu text
        /// </summary>
        public string SubMenuName { get; set; }
        /// <summary>
        /// Redirect Path
        /// </summary>
        public string Path { get; set; }
    }
}
