﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finance.BusinessObjects.Menu
{
    /// <summary>
    /// Menu Response
    /// </summary>
    public class MenuResponse
    {
        /// </summary>
        public List<MenuItem> objMenusBO { get; set; }
        /// <summary>
        /// Sub Menu Items of Customer Module
        /// </summary>
        public List<ModuleItem> objLstCustomerModuleMenus { get; set; }
        /// <summary>
        /// Converts JSON response into Object
        /// </summary>
        /// <param name="jsonResponse">string response of JSON</param>
        /// <returns><see cref="MenuResponse"/> object</returns>
        public static MenuResponse ToObject(string jsonResponse)
        {
            MenuResponse obj = (MenuResponse)Newtonsoft.Json.JsonConvert.DeserializeObject(jsonResponse,
                typeof(MenuResponse));
            return obj;
        }

    }
}
