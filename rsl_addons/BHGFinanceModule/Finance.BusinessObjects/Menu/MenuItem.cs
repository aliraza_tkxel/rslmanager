﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finance.BusinessObjects.Menu
{
    public class MenuItem
    {
        /// <summary>
        /// Menu Id
        /// </summary>
        public int MenuId { get; set; }
        /// <summary>
        /// Menu name to be shown on Top Navigation
        /// </summary>
        public string MenuName { get; set; }
        /// <summary>
        /// List os Sub Menus
        /// </summary>
        public List<SubMenuItem> objLstSubMenus { get; set; }
    }
}
