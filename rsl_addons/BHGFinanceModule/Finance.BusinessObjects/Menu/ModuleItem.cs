﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finance.BusinessObjects.Menu
{
    public class ModuleItem
    {
        /// <summary>
        /// Module ID
        /// </summary>
        public int ModuleId { get; set; }
        /// <summary>
        /// Module Name
        /// </summary>
        public string ModuleName { get; set; }
        /// <summary>
        /// Module Url for Page redirection
        /// </summary>
        public string Url { get; set; }
    }
}
