﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finance.BusinessObjects.Common
{
    public class SessionResponse
    {
        public int EmployeeId { get; set; }
        public string Name { get; set; }
        public int? IsActive { get; set; }
        public string userType { get; set; }
        public static SessionResponse ToObject(string jsonResponse)
        {
            SessionResponse obj = (SessionResponse)Newtonsoft.Json.JsonConvert.DeserializeObject(jsonResponse,
                typeof(SessionResponse));
            return obj;
        }
    }
}
