﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finance.BusinessObjects.Common
{
    public class ApplicationConstants
    {
        public const string LoginPath = "~/../BHAIntranet/Login.aspx";
        public const string UsersAccountDeactivated = "Your access to ASB Module has been de-activiated. Please contact administrator.";
        public const string AsbMenu = "ASB Management";
        /// <summary>
        /// DateFormat key. Saved in Web.config
        /// </summary>
        public const string KeyDateFormat = "DateFormat";
        /// <summary>
        /// DateFormat key for Model. Saved in Web.config
        /// </summary>
        public const string KeyDateFormatModel = "DateFormatForModel";
        /// <summary>
        /// Virtual Directory Key. This key is used to get virtual directory name from Web.config 
        /// </summary>
        public const string VirtualDirectory = "VirtualDirectory";

        public const string AccountsModule = "Accounts";
        public const string NominalMenu = "Nominal";
        public const string ActiveMenuPage = "Journal Templates";
    }
}
