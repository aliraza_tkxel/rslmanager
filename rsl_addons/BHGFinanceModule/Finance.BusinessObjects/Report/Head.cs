﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finance.BusinessObjects.Report
{
    public class Head
    {
        public int HeadId { get; set; }
        public int? CostCentreId { get; set; }
        public string HeadName { get; set; }
        public decimal Budget { get; set; }
        public decimal? NominalLedger { get; set; }
        public decimal Purchase { get; set; }
        public decimal Balance { get; set; }
    }
}
