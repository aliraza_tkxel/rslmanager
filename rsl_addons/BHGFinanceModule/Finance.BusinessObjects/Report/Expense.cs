﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finance.BusinessObjects.Report
{
    public class Expense
    {
        public int ExpenditureId { get; set; }
        public int? HeadId { get; set; }
        public decimal? Budget { get; set; }
        public string ExpenseName { get; set; }
        public decimal NominalLedger { get; set; }
        public decimal Purchase { get; set; }
        public decimal Balance { get; set; }
    }
}
