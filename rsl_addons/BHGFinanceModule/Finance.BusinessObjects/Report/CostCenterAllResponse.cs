﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finance.BusinessObjects.Report
{
    public class CostCenterAllResponse
    {
        public string TotalCostCenter { get; set; }

        public List<CostCenterAll> CostCenterAllList { get; set; } = new List<CostCenterAll>();
        public List<Head> HeadList { get; set; } = new List<Head>();

        public List<Expense> ExpenseList { get; set; } = new List<Expense>();
    }
}
