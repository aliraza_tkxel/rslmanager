﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finance.BusinessObjects.Report
{
    public class CostCenterAll
    {
        public string CostCenterName { get; set; }
        public int CostCenterId { get; set; }
        public decimal Budget { get; set; }
        public decimal NominalLedger { get; set; }
        public decimal Purchase { get; set; }
        public decimal Balance { get; set; }
        public decimal? NotAllocated { get; set; }
    }
}
