﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finance.BusinessObjects.Lookup
{
    public class FiscalYearMonthLookup
    {
        public string MonthNo { get; set; }

        public string Name { get; set; }

        public bool Selected { get; set; }
    }
}
