﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finance.BusinessObjects.Lookup
{
    public class FiscalYearLookup
    {
        public string FiscalYearId { get; set; }

        public string Name { get; set; }

        public bool Selected { get; set; }
    }
}
