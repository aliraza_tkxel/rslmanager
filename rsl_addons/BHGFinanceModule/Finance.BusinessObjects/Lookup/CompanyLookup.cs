﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finance.BusinessObjects.Lookup
{
    public class CompanyLookup
    {
        public string CompanyId { get; set; }

        public string Description { get; set; }

        public bool Selected { get; set; }
    }
}
