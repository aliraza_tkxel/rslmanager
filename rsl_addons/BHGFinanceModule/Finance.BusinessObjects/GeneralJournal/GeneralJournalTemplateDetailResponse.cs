﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F.BusinessObjects.GeneralJournal
{
    public class GeneralJournalTemplateDetailResponse
    {
        public GeneralJournalTemplate templateDetail { get; set; } = new GeneralJournalTemplate();
        public List<GeneralJournalAccount> accountList { get; set; } = new List<GeneralJournalAccount>();
    }
}
