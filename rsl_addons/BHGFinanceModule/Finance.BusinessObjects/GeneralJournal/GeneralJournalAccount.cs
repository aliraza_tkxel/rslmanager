﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F.BusinessObjects.GeneralJournal
{
    public class GeneralJournalAccount
    {
        public int templateId { get; set; }
        public string details { get; set; }
        public string account { get; set; }
        public bool? isDebit { get; set; }
        public string costCentre { get; set; }
        public string head { get; set; }
        public string expenditure { get; set; }
        public string reason { get; set; }
        public decimal? defaultDebitAmount { get; set; }
        public decimal? defaultCreditAmount { get; set; }
    }
}
