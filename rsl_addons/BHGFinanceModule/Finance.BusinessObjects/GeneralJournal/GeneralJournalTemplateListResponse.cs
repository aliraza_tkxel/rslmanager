﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F.BusinessObjects.GeneralJournal
{
    public class GeneralJournalTemplateListResponse
    {
        public List<GeneralJournalTemplate> templateList { get; set; } = new List<GeneralJournalTemplate>();
    }
}
