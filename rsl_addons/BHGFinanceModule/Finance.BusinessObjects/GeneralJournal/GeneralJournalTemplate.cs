﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F.BusinessObjects.GeneralJournal
{
    public class GeneralJournalTemplate
    {
        public int templateId { get; set; }
        public string templateName { get; set; }
        public DateTime? createdDate { get; set; }
        public string createdDateString { get; set; }
        public string createdBy { get; set; }
        public string company { get; set; }
        public string scheme { get; set; }
        public string block { get; set; }
        public string property { get; set; }
    }
}

