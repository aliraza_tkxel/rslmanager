﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F.BusinessObjects.Navigation
{
    public class NavigationViewModel
    {
        public string currentModule { get; set; }
        public string currentMenu { get; set; }
        public List<MenuViewModel> menues { get; set; }
        public List<ModuleViewModel> modules { get; set; }
    }
}
