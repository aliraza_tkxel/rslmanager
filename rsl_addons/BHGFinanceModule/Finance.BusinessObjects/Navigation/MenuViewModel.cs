﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F.BusinessObjects.Navigation
{
    public class MenuViewModel
    {
        public int menuId { get; set; }
        public string menuName { get; set; }
        public string path { get; set; }
        public List<SubmenuViewModel> subMenus { get; set; }
    }
}
