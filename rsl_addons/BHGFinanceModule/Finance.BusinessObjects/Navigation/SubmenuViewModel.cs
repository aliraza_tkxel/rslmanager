﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace F.BusinessObjects.Navigation
{
    public class SubmenuViewModel
    {

        public int menuId { get; set; }
        public string subMenuName { get; set; }
        public string path { get; set; }

    }
}
