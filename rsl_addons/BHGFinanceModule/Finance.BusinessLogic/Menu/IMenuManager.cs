﻿using Finance.BusinessObjects.Menu;
using F.BusinessObjects.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finance.BusinessLogic.Menu
{
    public interface IMenuManager
    {
        MenuResponse GetTopNavigationMenu(MenuRequest menuRequest);
        NavigationViewModel GetMenus(NavigationRequestViewModel request);
    }
}
