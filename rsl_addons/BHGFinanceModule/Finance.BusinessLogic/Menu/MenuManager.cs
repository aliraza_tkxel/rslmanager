﻿using Finance.BusinessLogic.Common;
using Finance.BusinessObjects.Menu;
using F.BusinessObjects.Navigation;
using Finance.DataAccess.Common;
using Finance.DataAccess.Menu;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Finance.BusinessLogic.Menu
{
    public class MenuManager : BaseManager, IMenuManager
    {
        #region  >>> Prop & Constructor <<<        

        private readonly IUnitOfWork uow = null;
        private IMenuRepository _repository = null;

        public MenuManager()
        {
            if (uow == null) uow = new UnitOfWork();
            _repository = new MenuRepository(uow);
            new MenuManager(uow, _repository);
        }
        public MenuManager(IUnitOfWork unitOfWork, IMenuRepository repository)
        {
            uow = unitOfWork;
            _repository = repository;
        }

        #endregion

        #region >>> Helper Method <<<



        #endregion


        #region >>> Methods <<<

        /// <summary>
        /// Get Top Navigation Menu items based upon user
        /// </summary>
        /// <param name="request"><see cref="MenuRequest"/></param>
        /// <returns><see cref="MenuResponse"/> object</returns>
        public MenuResponse GetTopNavigationMenu(MenuRequest request)
        {
            try
            {
                MenuResponse menuItems = _repository.GetMenus(request);
                return menuItems;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message + ex.StackTrace);
                throw;
            }
        }

        #endregion

        #region Displaying Menu on top bar
        public NavigationViewModel GetMenus(NavigationRequestViewModel request)
        {
            NavigationViewModel menusBo = new NavigationViewModel();

            int moduleId = _repository.GetModuleId(request.moduleName);
            var menus = _repository.GetMenusByModule(request.userId, moduleId);
            menusBo.menues = menus;
            menusBo.modules = _repository.GetModules(request.userId);
            return menusBo;
        }
        #endregion

    }
}
