﻿using Finance.BusinessObjects.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finance.BusinessLogic.Lookup
{
    public interface ICompanyLookupManager
    {
      
        List<CompanyLookup> GetCompany();
    }
}
