﻿using Finance.BusinessLogic.Common;
using Finance.DataAccess.Common;
using Finance.DataAccess.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finance.BusinessObjects.Lookup;

namespace Finance.BusinessLogic.Lookup
{
    public class FiscalYearLookupManager : BaseManager, IFiscalYearLookupManager
    {
        #region  >>> Prop & Constructor <<<        

        private readonly IUnitOfWork uow = null;
        private IFiscalYearLookupRepository _repository = null;

        public FiscalYearLookupManager()
        {
            if (uow == null) uow = new UnitOfWork();
            _repository = new FiscalYearLookupRepository(uow);
            new FiscalYearLookupManager(uow, _repository);
        }
        public FiscalYearLookupManager(IUnitOfWork unitOfWork, IFiscalYearLookupRepository repository)
        {
            uow = unitOfWork;
            _repository = repository;
        }

        #endregion

        #region >>> Helper Method <<<



        #endregion


        #region >>> Methods <<<

        public List<FiscalYearMonthLookup> GetCurrentFiscalYearMonth(out DateTime startDate, out DateTime endDate)
        {
            var responseLst = new List<FiscalYearMonthLookup>();
            var currentFiscalYear = _repository.GetCurrentFiscalYearMonth();

            var YStart = new DateTime();
            var YEnd = new DateTime();

            YStart = startDate = currentFiscalYear.YStart;
            YEnd = endDate = currentFiscalYear.YEnd;


            for (int i = 0; i < 12; i++)
            {
                DateTime last_date = new DateTime(YStart.Year, YStart.Month, DateTime.DaysInMonth(YStart.Year, YStart.Month));
                var fiscalYear = new FiscalYearMonthLookup();
                fiscalYear.MonthNo = last_date.ToShortDateString();
                fiscalYear.Name = YStart.ToString("MMMM") + " " + YStart.ToString("yyyy");

                responseLst.Add(fiscalYear);

                YStart = YStart.AddMonths(1);
            }
            
            return responseLst;
        }

        public List<FiscalYearLookup> GetAllFiscalYear()
        {
            var responseLst = new List<FiscalYearLookup>();
            var fiscalYears = _repository.GetAllFiscalYear().OrderByDescending(e => e.YRange).ToList();

            if (fiscalYears != null && fiscalYears.Count >0)
            {
                foreach (var fiscalYear in fiscalYears)
                {
                    var fiscalYearLookup = new FiscalYearLookup();
                    fiscalYearLookup.FiscalYearId = fiscalYear.YRange.ToString();
                    fiscalYearLookup.Name = fiscalYear.YStart.ToString("dd/MM/yyyy") + " - " + fiscalYear.YEnd.ToString("dd/MM/yyyy");

                    responseLst.Add(fiscalYearLookup);
                }
            }
            
            return responseLst;
        }

        #endregion

    }
}
