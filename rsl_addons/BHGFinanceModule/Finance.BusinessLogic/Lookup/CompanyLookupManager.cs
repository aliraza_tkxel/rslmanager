﻿using Finance.BusinessLogic.Common;
using Finance.DataAccess.Common;
using Finance.DataAccess.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finance.BusinessObjects.Lookup;

namespace Finance.BusinessLogic.Lookup
{
    public class CompanyLookupManager : BaseManager, ICompanyLookupManager
    {
        #region  >>> Prop & Constructor <<<        

        private readonly IUnitOfWork uow = null;
        private ICompanyLookupRepository _repository = null;

        public CompanyLookupManager()
        {
            if (uow == null) uow = new UnitOfWork();
            _repository = new CompanyLookupRepository(uow);
            new CompanyLookupManager(uow, _repository);
        }
        public CompanyLookupManager(IUnitOfWork unitOfWork, ICompanyLookupRepository repository)
        {
            uow = unitOfWork;
            _repository = repository;
        }

        #endregion

     
        #region >>> Methods <<<


        public List<CompanyLookup> GetCompany()
        {
            var responseLst = new List<CompanyLookup>();
            
            var Company = _repository.GetCompany().OrderBy(e => e.CompanyID).ToList();
            
            if (Company != null && Company.Count > 0)
            {
                foreach (var Companies in Company)
                {
                    var companyLookup = new CompanyLookup();
                    companyLookup.CompanyId = Companies.CompanyID.ToString();
                    companyLookup.Description = Companies.Description;

                    responseLst.Add(companyLookup);
                }
            }

            return responseLst;

        }

        #endregion

    }
}
