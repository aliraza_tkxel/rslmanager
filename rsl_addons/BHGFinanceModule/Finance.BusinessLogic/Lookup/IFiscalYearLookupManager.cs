﻿using Finance.BusinessObjects.Lookup;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finance.BusinessLogic.Lookup
{
    public interface IFiscalYearLookupManager
    {
        List<FiscalYearMonthLookup> GetCurrentFiscalYearMonth(out DateTime startDate, out DateTime endDate);
        List<FiscalYearLookup> GetAllFiscalYear();
    }
}
