﻿using Finance.BusinessObjects.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Finance.BusinessLogic.Common
{
    public class SessionManager : BaseManager, ISessionManager
    {
        public SessionResponse CreateSession(int userId)
        {
            string endpoint = "Session/CreateSession?userId=" + userId;
            WebClient client = new WebClient();
            Task<SessionResponse> result;

            result = Task.Factory.StartNew<SessionResponse>(() =>
            {
                try
                {
                    return JsonConvert.DeserializeObject<SessionResponse>(client.DownloadString(GetBaseUrl() + endpoint));
                }
                catch (Exception ex)
                {
                    logger.Error(ex.Message + ex.StackTrace);
                    throw;
                }
                finally
                {
                    client.Dispose();
                }
            });
            return result.Result;
        }
    }
}
