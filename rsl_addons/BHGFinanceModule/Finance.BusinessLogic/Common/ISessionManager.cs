﻿using Finance.BusinessObjects.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finance.BusinessLogic.Common
{
    public interface ISessionManager
    {
        SessionResponse CreateSession(int userId);
    }
}
