﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Finance.BusinessLogic.Common
{
    public class BaseManager
    {
        /// <summary>
        /// Base URL 
        /// </summary>
        private string BASE_URL = ConfigurationManager.AppSettings["URL"].ToString();
        private string domain = HttpContext.Current.Request.Url.AbsoluteUri;
        readonly protected log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected string GetBaseUrl()
        {
            string servername = domain.Substring((domain.IndexOf("//") + 2), domain.Substring((domain.IndexOf("//") + 2), domain.Substring((domain.IndexOf("//") + 2)).IndexOf("/")).Length);
            //return "https://" + servername + BASE_URL;
            //return "https://devcrm.broadlandhousinggroup.org/RSLASBModuleApi/api/"; // For Development only. Do not remove
            return "http://localhost:22988/api/"; // For Development only. Do not remove
        }
    }
}
