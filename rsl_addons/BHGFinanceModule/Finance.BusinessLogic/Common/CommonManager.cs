﻿using Finance.BusinessObjects.Menu;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Finance.BusinessLogic.Common
{
    /// <summary>
    /// Services that are not related to any specific module
    /// </summary>
    public class CommonManager : BaseManager, ICommonManager
    {
        
    }
}
