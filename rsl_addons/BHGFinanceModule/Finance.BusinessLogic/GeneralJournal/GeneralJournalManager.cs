﻿using F.BusinessObjects.GeneralJournal;
using Finance.DataAccess.Common;
using Finance.DataAccess.GeneralJournal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finance.BusinessLogic.GeneralJournal
{
    public class GeneralJournalManager : IGeneralJournalManager
    {
        private readonly IUnitOfWork uow = null;
        private IGeneralJournalRepository repository = null;

        public GeneralJournalManager()
        {
            if (uow == null) uow = new UnitOfWork();
            repository = new GeneralJournalRepository(uow);            
            new GeneralJournalManager(uow, repository);
        }
        public GeneralJournalManager(IUnitOfWork unitOfWork, IGeneralJournalRepository _repository)
        {
            uow = unitOfWork;
            repository = _repository;            
        }

        public GeneralJournalTemplateListResponse GetGeneralJournalTemplateList()
        {
            return repository.GetGeneralJournalTemplateList();
        }

        public GeneralJournalTemplateDetailResponse GetGeneralJournalTemplateDetail(int templateId)
        {
            return repository.GetGeneralJournalTemplateDetail(templateId);
        }

        public bool DeleteTemplate(int templateId)
        {
            return repository.DeleteTemplate(templateId);
        }
        public GeneralJournalAccount GetGeneralJournalAccountDetail(int templateId)
        {
            return repository.GetGeneralJournalAccountDetail(templateId);
        }

    }
}
