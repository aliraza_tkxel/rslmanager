﻿using Finance.DataAccess.Common;
using Finance.DataAccess.Report;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Finance.BusinessObjects.Report;
using Finance.DataAccess.Lookup;

namespace Finance.BusinessLogic.Report
{
    public class CostCenterManager : ICostCenterManager
    {

        #region  >>> Prop & Constructor <<<        

        private readonly IUnitOfWork uow = null;
        private ICostCenterRepository repository = null;
        private IFiscalYearLookupRepository fiscalYearLookUpRepository = null;

        public CostCenterManager()
        {
            if (uow == null) uow = new UnitOfWork();
            repository = new CostCenterRepository(uow);
            fiscalYearLookUpRepository = new FiscalYearLookupRepository(uow);
            new CostCenterManager(uow, repository, fiscalYearLookUpRepository);
        }
        public CostCenterManager(IUnitOfWork unitOfWork, ICostCenterRepository _repository, IFiscalYearLookupRepository _fiscalYearLookUpRepository)
        {
            uow = unitOfWork;
            repository = _repository;
            fiscalYearLookUpRepository = _fiscalYearLookUpRepository;
        }


        #endregion

        #region >>> Helper Method <<<



        #endregion

        #region >>> Methods <<<

        public List<CostCenter> GetAllCostCenter()
        {
            return repository.GetAllCostCenter();
        }

        public decimal GetTotalCostCentre(DateTime? fromDate, DateTime? toDate, int? company)
        {
            return repository.GetTotalCostCentre(fromDate, toDate, company);
        }

        public CostCenterAllResponse GetCostCenterAll(DateTime fromDate, DateTime toDate, int company)
        {
            return repository.GetCostCenterAll(fromDate, toDate, company);
        }

        public CostCenterAllResponse GetHead(DateTime fromDate, DateTime toDate, int costCenterId)
        {
            return repository.GetHead(fromDate, toDate, costCenterId);
        }

        public CostCenterAllResponse GetExpense(DateTime fromDate, DateTime toDate, int headId)
        {
            return repository.GetExpense(fromDate, toDate, headId);
        }

        #endregion
    }
}
