﻿using Finance.BusinessObjects.Report;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Finance.BusinessLogic.Report
{
    public interface ICostCenterManager
    {
        List<CostCenter> GetAllCostCenter();
        CostCenterAllResponse GetCostCenterAll(DateTime fromDate, DateTime toDate, int company);
        CostCenterAllResponse GetHead(DateTime fromDate, DateTime toDate, int costCenterId);
        CostCenterAllResponse GetExpense(DateTime fromDate, DateTime toDate, int headId);
        decimal GetTotalCostCentre(DateTime? fromDate, DateTime? toDate, int? company);
    }
}
