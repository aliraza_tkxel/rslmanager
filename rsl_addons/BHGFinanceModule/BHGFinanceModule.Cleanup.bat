#@del /F /Q *.user
@PUSHD BHGFinanceModule
  @del /F /Q *.user
  @del /F /Q *.log
  @rmdir /S /Q bin
  @rmdir /S /Q obj
@POPD

@PUSHD .\Finance.BusinessLogic
  @rmdir /S /Q bin
  @rmdir /S /Q obj
@POPD

@PUSHD Finance.BusinessObjects
  @rmdir /S /Q bin
  @rmdir /S /Q obj
@POPD

@PUSHD Finance.DataAccess
  @rmdir /S /Q bin
  @rmdir /S /Q obj
@POPD

@PAUSE