﻿Imports System
Imports TSR_BusinessObject
Imports System.Data.SqlClient
Imports TSR_DataAccess
Imports System.Web
Imports System.Drawing
Imports System.Web.UI.WebControls
Imports TSR_Utilities


Namespace TSR_BusinessLogic

    Public Class CustomerBL

#Region "Attributes"
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="stageDataSet"></param>
        ''' <remarks></remarks>

#End Region

#Region "Functions"


#Region "Get Stages"
        Public Sub getStages(ByRef stageDataSet As DataSet)
            Dim objCustomerDAL As CustomerDAL = New CustomerDAL()
            objCustomerDAL.getStages(stageDataSet)
        End Sub
#End Region

#Region "Get Actions"
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="reportDataSet"></param>
        ''' <remarks></remarks>
        Public Sub reportActions(ByRef reportDataSet As DataSet)
            Dim objCustomerDAL As CustomerDAL = New CustomerDAL()
            objCustomerDAL.reportActions(reportDataSet)
        End Sub
#End Region

#Region "Get Status"
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="statusDataSet"></param>
        ''' <remarks></remarks>
        Public Sub reportStatus(ByRef statusDataSet As DataSet)
            Dim objCustomerDAL As CustomerDAL = New CustomerDAL()
            objCustomerDAL.reportStatus(statusDataSet)
        End Sub
#End Region

#Region "Get YesNoUnknown"
        Public Sub getYesNoUnknown(ByRef yesNoUnknownDataSet As DataSet)
            Dim objCustomerDAL As CustomerDAL = New CustomerDAL()
            objCustomerDAL.getYesNoUnknown(yesNoUnknownDataSet)
        End Sub
#End Region

#Region "Get YesNo"
        Public Sub getYesNo(ByRef yesNoDataSet As DataSet)
            Dim objCustomerDAL As CustomerDAL = New CustomerDAL()
            objCustomerDAL.getYesNo(yesNoDataSet)
        End Sub
#End Region

#Region "Load CheckBox List"
        Public Sub loadCheckBoxList(ByRef chkBoxListDataSet As DataSet, ByVal helpcategoryid As Int32)
            Dim objCustomerDAL As CustomerDAL = New CustomerDAL()
            objCustomerDAL.loadCheckBoxList(chkBoxListDataSet, helpcategoryid)
        End Sub

#End Region

#Region "Save Referral Data"
        Public Function saveReferralData(ByRef objCustomerDetailBO As CustomerDetailBO)
            Dim objCustomerDAL As CustomerDAL = New CustomerDAL()
            Return objCustomerDAL.saveReferralData(objCustomerDetailBO)
        End Function
#End Region

#Region "Save SubCategory"

        Sub saveSubCategory(ByVal JournalId As Int32, ByVal chkListMoneyIssues As CheckBoxList, ByVal chkListPersonalProblem As CheckBoxList, ByVal chkListPracticalProblems As CheckBoxList, ByVal chkListWork As CheckBoxList)
            'objCustomerDAL.saveSubCategory(chkListMoneyIssues, chkListPersonalProblem, chkListPracticalProblems, chkListWork)
            Dim objCustomerDAL As CustomerDAL = New CustomerDAL()
            'Dim dtMoneyIssueChkList As DataTable = New DataTable()
            For Each item As ListItem In chkListMoneyIssues.Items
                If item.Selected = True Then
                    objCustomerDAL.saveSubCategory(item.Value, JournalId)
                End If
            Next

            For Each item As ListItem In chkListPersonalProblem.Items
                If item.Selected = True Then
                    objCustomerDAL.saveSubCategory(item.Value, JournalId)
                End If
            Next

            For Each item As ListItem In chkListPracticalProblems.Items
                If item.Selected = True Then
                    objCustomerDAL.saveSubCategory(item.Value, JournalId)
                End If
            Next

            For Each item As ListItem In chkListWork.Items
                If item.Selected = True Then
                    objCustomerDAL.saveSubCategory(item.Value, JournalId)
                End If
            Next



        End Sub

#End Region

#Region "Get Support Referral Data"
        Public Function getSupportReferralData(ByRef objPageSortBO As PageSortBO, ByRef objReportDataBO As ReportDataBO, ByRef supportReferralDataSet As DataSet)
            Dim objCustomerDAL As CustomerDAL = New CustomerDAL()
            Return objCustomerDAL.getSupportReferralData(objPageSortBO, objReportDataBO, supportReferralDataSet)
        End Function

#End Region

#Region "Get Tenancy Support Form Data"
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <remarks></remarks>
        Sub getTenancySupportFormData(ByRef resultDataSet As DataSet, ByVal referralHistoryId As Int32)
            Dim objCustomerDAL As CustomerDAL = New CustomerDAL()
            objCustomerDAL.getTenancySupportFormData(resultDataSet, referralHistoryId)

        End Sub



#End Region

#Region "Get AssignTO DropDown Data"
        ''' <summary>
        ''' Get AssignTo DropDown Data on Assign Popup
        ''' </summary>
        ''' <param name="assignToDataSet"></param>
        ''' <remarks></remarks>
        Public Sub getAssignToData(ByRef assignToDataSet As DataSet)
            Dim objCustomerDAL As CustomerDAL = New CustomerDAL()
            objCustomerDAL.getAssignToData(assignToDataSet)
        End Sub
#End Region

#Region "Get Tenant Info"
        ''' <summary>
        ''' Get Tenant Info for Assign PopUp
        ''' </summary>
        ''' <param name="tenantInfoDataSet"></param>
        ''' <param name="tenancyId"></param>
        ''' <remarks></remarks>
        Public Sub getTenantInfo(ByRef tenantInfoDataSet As DataSet, ByVal tenancyId As Integer)
            Dim objCustomerDAL As CustomerDAL = New CustomerDAL()
            objCustomerDAL.getTenantInfo(tenantInfoDataSet, tenancyId)
        End Sub
#End Region

#Region "Save Assign Info"
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="asigneeId"></param>
        ''' <param name="referralHistoryId"></param>
        ''' <remarks></remarks>
        Public Sub saveAssignInfo(ByVal referralHistoryId As Integer, ByVal asigneeId As Integer, ByVal assignNotes As String)
            Dim objCustomerDAL As CustomerDAL = New CustomerDAL()
            objCustomerDAL.saveAssignInfo(referralHistoryId, asigneeId, assignNotes)
        End Sub
#End Region

#Region "Confirm Referral Rejection"
        ''' Confirm Referral Rejection
        ''' </summary>        
        ''' <returns></returns>
        Public Function confirmReferralRejection(ByVal refHistoryId As Integer, ByVal rejectedBy As Integer, ByVal reasonNotes As String)
            Dim objCustomerDAL As CustomerDAL = New CustomerDAL()

            Return objCustomerDAL.confirmReferralRejection(refHistoryId, rejectedBy, reasonNotes)

        End Function
#End Region

#Region "Get Update Referral Data "
        ''' <summary>
        ''' Get Update Referral Data 
        ''' </summary>
        ''' <remarks></remarks>
        Sub getUpdateReferralData(ByRef resultDataset As DataSet, ByVal actionId As Integer, ByVal statusId As Integer, ByVal employeeId As Integer, ByVal refHistoryId As Integer)

            Dim objCustomerDAL As CustomerDAL = New CustomerDAL()
            objCustomerDAL.getUpdateReferralData(resultDataset, actionId, statusId, employeeId, refHistoryId)

        End Sub

#End Region

#Region "Update Referral"
        ''' Update Referral
        ''' </summary>        
        ''' <returns></returns>
        Public Function updateReferral(ByVal objUpdateReferralBO As UpdateReferralBO)
            Dim objCustomerDAL As CustomerDAL = New CustomerDAL()

            Return objCustomerDAL.updateReferral(objUpdateReferralBO)

        End Function
#End Region

#End Region

    End Class

End Namespace