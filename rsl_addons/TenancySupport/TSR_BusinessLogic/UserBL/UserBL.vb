﻿Imports System
Imports TSR_BusinessObject
Imports System.Data.SqlClient
Imports TSR_DataAccess
Imports System.Web
Imports System.Drawing
Imports System.Web.UI.WebControls
Imports TSR_Utilities



Namespace TSR_BusinessLogic

    Public Class UserBL

#Region "Attributes"
        Dim objUserDAL As UserDAL = New UserDAL()
#End Region

#Region "Functions"

#Region "Returns Employee detail"
        ''' <summary>
        ''' Returns Employee detail.
        ''' </summary>
        ''' <param name="employeeId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Function getEmployeeById(ByVal employeeId As Integer) As DataSet
            Return objUserDAL.getEmployeeById(employeeId)
        End Function
#End Region

#End Region

    End Class

End Namespace

