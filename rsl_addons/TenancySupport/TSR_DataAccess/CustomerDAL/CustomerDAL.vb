﻿Imports System
Imports System.IO
Imports System.Data.Common
Imports System.Web
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports TSR_BusinessObject
Imports TSR_Utilities
Imports TSR_DataAccess



Namespace TSR_DataAccess


    Public Class CustomerDAL : Inherits BaseDAL

#Region "Functions"

#Region "Get Stages"
        Sub getStages(ByRef stageDataSet As DataSet)

            Dim parametersList As ParameterList = New ParameterList()
            MyBase.LoadDataSet(stageDataSet, parametersList, SpNameConstants.GetStages)

        End Sub

#End Region

#Region "Get Report Actions"
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="reportDataSet"></param>
        ''' <remarks></remarks>
        Public Sub reportActions(ByRef reportDataSet As DataSet)
            Dim parameterList As ParameterList = New ParameterList()
            MyBase.LoadDataSet(reportDataSet, parameterList, SpNameConstants.GetReportActions)
        End Sub
#End Region

#Region "Get Report Status"
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="statusDataSet"></param>
        ''' <remarks></remarks>
        Public Sub reportStatus(ByRef statusDataSet As DataSet)
            Dim parameterList As ParameterList = New ParameterList()
            MyBase.LoadDataSet(statusDataSet, parameterList, SpNameConstants.GetReportStatus)
        End Sub
#End Region

#Region "Get YesNoUnknown"
        Sub getYesNoUnknown(ByRef yesNoUnknownDataSet As DataSet)

            Dim parametersList As ParameterList = New ParameterList()
            MyBase.LoadDataSet(yesNoUnknownDataSet, parametersList, SpNameConstants.GetYESNO_UNKNOWN)

        End Sub

#End Region

#Region "Get YesNo"
        Sub getYesNo(ByRef yesNoDataSet As DataSet)

            Dim parametersList As ParameterList = New ParameterList()
            MyBase.LoadDataSet(yesNoDataSet, parametersList, SpNameConstants.GetYESNO)

        End Sub

#End Region

#Region "load CheckBox List"
        Sub loadCheckBoxList(ByRef chkBoxListDataSet As DataSet, ByVal helpcategoryId As Int32)

            Dim parametersList As ParameterList = New ParameterList()

            Dim helpcategoryIdParam As ParameterBO = New ParameterBO("helpcategory", helpcategoryId, DbType.Int32)
            parametersList.Add(helpcategoryIdParam)

            MyBase.LoadDataSet(chkBoxListDataSet, parametersList, SpNameConstants.GetCheckBoxList)

        End Sub

#End Region

#Region "save Referral Data"

        Function saveReferralData(ByVal objCustomerDetailBO As CustomerDetailBO)
            Dim parameterList As ParameterList = New ParameterList()
            Dim outParameterList As ParameterList = New ParameterList()

            Dim paramUserId As ParameterBO = New ParameterBO("UserId", objCustomerDetailBO.UserId, DbType.Int32)
            parameterList.Add(paramUserId)

            Dim paramPropertyId As ParameterBO = New ParameterBO("PropertyId", objCustomerDetailBO.PropertyId, DbType.String)
            parameterList.Add(paramPropertyId)

            Dim paramCustomerId As ParameterBO = New ParameterBO("CustomerId", objCustomerDetailBO.CustomerId, DbType.Int32)
            parameterList.Add(paramCustomerId)

            Dim paramItemId As ParameterBO = New ParameterBO("ItemId", objCustomerDetailBO.ItemId, DbType.Int32)
            parameterList.Add(paramItemId)

            Dim paramTenancyId As ParameterBO = New ParameterBO("TenancyId", objCustomerDetailBO.TenancyId, DbType.Int32)
            parameterList.Add(paramTenancyId)

            Dim paramItemNatureId As ParameterBO = New ParameterBO("ItemNatureId", objCustomerDetailBO.ItemNatureId, DbType.Int32)
            parameterList.Add(paramItemNatureId)

            Dim paramTitle As ParameterBO = New ParameterBO("Title", objCustomerDetailBO.Title, DbType.String)
            parameterList.Add(paramTitle)

            'Dim paramJournalId As ParameterBO = New ParameterBO("journalId", objCustomerDetailBO.JournalId, DbType.Int32)
            'parameterList.Add(paramJournalId)

            Dim paramItemActionId As ParameterBO = New ParameterBO("itemActionId", objCustomerDetailBO.ItemActionId, DbType.Int32)
            parameterList.Add(paramItemActionId)

            Dim paramItemStatusId As ParameterBO = New ParameterBO("itemStatusId", objCustomerDetailBO.ItemStatusId, DbType.Int32)
            parameterList.Add(paramItemStatusId)

            Dim paramIsTypeArears As ParameterBO = New ParameterBO("isTypeArears", objCustomerDetailBO.IsTypeArears, DbType.Int32)
            parameterList.Add(paramIsTypeArears)

            Dim paramIsTypeDamage As ParameterBO = New ParameterBO("isTypeDamage", objCustomerDetailBO.IsTypeDamage, DbType.Int32)
            parameterList.Add(paramIsTypeDamage)

            Dim paramIsTypeASB As ParameterBO = New ParameterBO("isTypeASB", objCustomerDetailBO.IsTypeASB, DbType.Int32)
            parameterList.Add(paramIsTypeASB)

            Dim paramStageId As ParameterBO = New ParameterBO("stageId", objCustomerDetailBO.StageId, DbType.Int32)
            parameterList.Add(paramStageId)

            Dim paramSafetyIssue As ParameterBO = New ParameterBO("safetyIssue", objCustomerDetailBO.SafetyIssue, DbType.Int32)
            parameterList.Add(paramSafetyIssue)

            Dim paramConviction As ParameterBO = New ParameterBO("conviction", objCustomerDetailBO.Conviction, DbType.Int32)
            parameterList.Add(paramConviction)

            Dim paramSubStanceMisUse As ParameterBO = New ParameterBO("subStanceMisUse", objCustomerDetailBO.SubStanceMisUse, DbType.Int32)
            parameterList.Add(paramSubStanceMisUse)

            Dim paramSelfHarm As ParameterBO = New ParameterBO("selfHarm", objCustomerDetailBO.SelfHarm, DbType.Int32)
            parameterList.Add(paramSelfHarm)

            Dim paramNotes As ParameterBO = New ParameterBO("notes", objCustomerDetailBO.Notes, DbType.String)
            parameterList.Add(paramNotes)

            Dim paramIsTenantAware As ParameterBO = New ParameterBO("isTenantAware", objCustomerDetailBO.IsTenantAware, SqlDbType.Int)
            parameterList.Add(paramIsTenantAware)

            Dim paramNeglectNotes As ParameterBO = New ParameterBO("neglectNotes", objCustomerDetailBO.NeglectNotes, DbType.String)
            parameterList.Add(paramNeglectNotes)

            Dim paramOtherNotes As ParameterBO = New ParameterBO("otherNotes", objCustomerDetailBO.OtherNotes, DbType.String)
            parameterList.Add(paramOtherNotes)

            Dim paramMiscNotes As ParameterBO = New ParameterBO("miscNotes", objCustomerDetailBO.MiscNotes, DbType.String)
            parameterList.Add(paramMiscNotes)

            Dim paramJournalId As ParameterBO = New ParameterBO("JournalId", objCustomerDetailBO.JournalId, DbType.Int32)
            outParameterList.Add(paramJournalId)

            MyBase.SaveRecord(parameterList, outParameterList, SpNameConstants.SaveReferralData)

            Return outParameterList.Item(0).Value

        End Function
#End Region

#Region "Save subCategory"
        Public Sub saveSubCategory(ByVal subCategory As Int32, ByVal journalId As Int32)

            Dim parameterList As ParameterList = New ParameterList()
            Dim outParameterList As ParameterList = New ParameterList()

            Dim ParamSubCategoryId As ParameterBO = New ParameterBO("subCategoryId", subCategory, DbType.Int32)
            parameterList.Add(ParamSubCategoryId)

            Dim paramJournalId As ParameterBO = New ParameterBO("journalId", journalId, DbType.Int32)
            parameterList.Add(paramJournalId)

            MyBase.SaveRecord(parameterList, outParameterList, SpNameConstants.SaveSubCategory)



        End Sub
#End Region

#Region "Get Support Referral Data"
        Function getSupportReferralData(ByRef objPageSortBO As PageSortBO, ByRef objReportDataBO As ReportDataBO, ByRef supportReferralDataSet As DataSet)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParamList As ParameterList = New ParameterList()

            'Dim fromDate As String = String.Empty            'Dim toDate As String = String.Empty
            'Dim lastAction As String = String.Empty
            'Dim status As String = String.Empty
            Dim paramSearchText As ParameterBO = New ParameterBO("searchText", objPageSortBO.SearchText, DbType.String)
            parametersList.Add(paramSearchText)

            Dim paramFromDate As ParameterBO = New ParameterBO("fromDate", objReportDataBO.FromDate, DbType.String)
            parametersList.Add(paramFromDate)

            Dim paramToDate As ParameterBO = New ParameterBO("toDate", objReportDataBO.ToDate, DbType.String)
            parametersList.Add(paramToDate)

            Dim paramLastAction As ParameterBO = New ParameterBO("lastAction", objReportDataBO.LastAction, DbType.Int32)
            parametersList.Add(paramLastAction)

            Dim paramStatus As ParameterBO = New ParameterBO("status", objReportDataBO.Status, DbType.Int32)
            parametersList.Add(paramStatus)

            Dim paramPageSize As ParameterBO = New ParameterBO("pageSize", objPageSortBO.PageSize, DbType.Int32)
            parametersList.Add(paramPageSize)

            Dim paramPageNumber As ParameterBO = New ParameterBO("pageNumber", objPageSortBO.PageNumber, DbType.Int32)
            parametersList.Add(paramPageNumber)

            Dim totalCountParam As ParameterBO = New ParameterBO("totalCount", 0, DbType.Int32)
            outParamList.Add(totalCountParam)

            MyBase.LoadDataSet(supportReferralDataSet, parametersList, outParamList, SpNameConstants.GetSupportReferralReportData)
            If Not IsDBNull(outParamList.Item(0).Value) Then
                Return outParamList.Item(0).Value
            Else
                Return 0
            End If

        End Function

#End Region

#Region "Get Tenancy Support Form Data"
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <remarks></remarks>
        Sub getTenancySupportFormData(ByRef resultDataSet As DataSet, ByVal referralHistoryId As Int32)


            Dim parametersList As ParameterList = New ParameterList()
            Dim referralHistoryIdParam As ParameterBO = New ParameterBO("referralHistoryId", referralHistoryId, DbType.Int32)
            parametersList.Add(referralHistoryIdParam)

            Dim stagesDt As DataTable = New DataTable()
            Dim yesNoUnknownDt As DataTable = New DataTable()
            Dim yesNoDt As DataTable = New DataTable()
            Dim helpCategoriesDt As DataTable = New DataTable()
            Dim helpSubCategoriesDt As DataTable = New DataTable()
            Dim formDataDt As DataTable = New DataTable()
            'Dim householdDt As DataTable = New DataTable()
            Dim agenciesDt As DataTable = New DataTable()

            stagesDt.TableName = ApplicationConstants.StagesDt
            yesNoUnknownDt.TableName = ApplicationConstants.YesNoUnknownDt
            yesNoDt.TableName = ApplicationConstants.YesNoDt
            helpCategoriesDt.TableName = ApplicationConstants.HelpCategoriesDt
            helpSubCategoriesDt.TableName = ApplicationConstants.HelpSubCategoriesDt
            formDataDt.TableName = ApplicationConstants.FormDataDt
            'householdDt.TableName = ApplicationConstants.HouseholdDt
            agenciesDt.TableName = ApplicationConstants.AgenciesDt

            resultDataSet.Tables.Add(stagesDt)
            resultDataSet.Tables.Add(yesNoUnknownDt)
            resultDataSet.Tables.Add(yesNoDt)
            resultDataSet.Tables.Add(helpCategoriesDt)
            resultDataSet.Tables.Add(helpSubCategoriesDt)
            resultDataSet.Tables.Add(formDataDt)
            'resultDataSet.Tables.Add(householdDt)
            resultDataSet.Tables.Add(agenciesDt)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetTenancySupportFormData)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, stagesDt, yesNoUnknownDt, yesNoDt, helpCategoriesDt, helpSubCategoriesDt, formDataDt, agenciesDt)
            'resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, stagesDt, yesNoUnknownDt, yesNoDt, helpCategoriesDt, helpSubCategoriesDt, formDataDt, householdDt, agenciesDt)

        End Sub

#End Region

#Region "Confirm Referral Rejection"
        ''' <summary>
        ''' Confirm Referral Rejection
        ''' </summary>        
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function confirmReferralRejection(ByVal refHistoryId As Integer, ByVal rejectedBy As Integer, ByVal reasonNotes As String)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim referralHistoryIdParam As ParameterBO = New ParameterBO("referralHistoryId", refHistoryId, DbType.Int32)
            parametersList.Add(referralHistoryIdParam)

            Dim rejectedByParam As ParameterBO = New ParameterBO("rejectedBy", rejectedBy, DbType.Int32)
            parametersList.Add(rejectedByParam)

            Dim reasonNotesParam As ParameterBO = New ParameterBO("reasonNotes", reasonNotes, DbType.String)
            parametersList.Add(reasonNotesParam)

            Dim isSavedParam As ParameterBO = New ParameterBO("isSaved", 0, DbType.Boolean)
            outParametersList.Add(isSavedParam)

            Dim result As Integer = MyBase.SaveRecord(parametersList, outParametersList, SpNameConstants.ConfirmReferralRejection)
            If outParametersList.Item(0).Value = True Then
                Return True
            Else
                Return False
            End If

        End Function


#End Region

#Region "Get AssignTO DropDown Data"
        ''' <summary>
        ''' Get AssignTo DropDown Data on Assign Popup
        ''' </summary>
        ''' <param name="assignToDataSet"></param>
        ''' <remarks></remarks>
        Public Sub getAssignToData(ByRef assignToDataSet As DataSet)
            Dim parametersList As ParameterList = New ParameterList()
            MyBase.LoadDataSet(assignToDataSet, parametersList, SpNameConstants.GetAssignToData)
        End Sub
#End Region

#Region " Get Tenant Info"
        ''' Get Tenant Info for Assign PopUp
        ''' </summary>
        ''' <param name="tenantInfoDataSet"></param>
        ''' <param name="tenancyId"></param>
        Public Sub getTenantInfo(ByRef tenantInfoDataSet As DataSet, ByVal tenancyId As Integer)
            Dim parametersList As ParameterList = New ParameterList()
            Dim ParamTenancyId As ParameterBO = New ParameterBO("tenancyId", tenancyId, DbType.Int32)
            parametersList.Add(ParamTenancyId)
            MyBase.LoadDataSet(tenantInfoDataSet, parametersList, SpNameConstants.GetTenantInfo)


        End Sub

#End Region

#Region "Save Assign Info"
        ''' <summary>
        ''' Save Assign PopUp Info
        ''' </summary>
        ''' <param name="asigneeId"></param>
        ''' <param name="referralHistoryId"></param>
        ''' <remarks></remarks>
        Public Sub saveAssignInfo(ByVal referralHistoryId As Integer, ByVal asigneeId As Integer, ByVal assignNotes As String)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim ParamReferralHistoryId As ParameterBO = New ParameterBO("referralHistoryId", referralHistoryId, DbType.Int32)
            parametersList.Add(ParamReferralHistoryId)
            Dim ParamAsigneeId As ParameterBO = New ParameterBO("asigneeId", asigneeId, DbType.Int32)
            parametersList.Add(ParamAsigneeId)
            Dim ParamAssignNotes As ParameterBO = New ParameterBO("assignNotes", assignNotes, DbType.String)
            parametersList.Add(ParamAssignNotes)

            MyBase.SaveRecord(parametersList, outParametersList, SpNameConstants.SaveAssignInfo)

        End Sub


#End Region

#Region "Get Update Referral Data "
        ''' <summary>
        ''' Get Update Referral Data 
        ''' </summary>
        ''' <remarks></remarks>
        Sub getUpdateReferralData(ByRef resultDataset As DataSet, ByVal actionId As Integer, ByVal statusId As Integer, ByVal employeeId As Integer, ByVal refHistoryId As Integer)

            Dim parametersList As ParameterList = New ParameterList()

            Dim actionIdParam As ParameterBO = New ParameterBO("actionId", actionId, DbType.Int32)
            parametersList.Add(actionIdParam)

            Dim statusIdParam As ParameterBO = New ParameterBO("statusId", statusId, DbType.Int32)
            parametersList.Add(statusIdParam)

            Dim employeeIdParam As ParameterBO = New ParameterBO("employeeId", employeeId, DbType.Int32)
            parametersList.Add(employeeIdParam)

            Dim refHistoryIdParam As ParameterBO = New ParameterBO("refHistoryId", refHistoryId, DbType.Int32)
            parametersList.Add(refHistoryIdParam)

            Dim actionsDt As DataTable = New DataTable()
            Dim tenantInfoDt As DataTable = New DataTable()
            Dim assignToDt As DataTable = New DataTable()
            Dim isTsmDt As DataTable = New DataTable()

            actionsDt.TableName = ApplicationConstants.ActionsDt
            tenantInfoDt.TableName = ApplicationConstants.TenantInfoDt
            assignToDt.TableName = ApplicationConstants.AssignToDt
            isTsmDt.TableName = ApplicationConstants.IsTsmDt

            resultDataset.Tables.Add(actionsDt)
            resultDataset.Tables.Add(tenantInfoDt)
            resultDataset.Tables.Add(assignToDt)
            resultDataset.Tables.Add(isTsmDt)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetUpdateReferralData)
            resultDataset.Load(lResultDataReader, LoadOption.OverwriteChanges, actionsDt, tenantInfoDt, assignToDt, isTsmDt)

        End Sub

#End Region

#Region "Update Referral"
        ''' Update Referral
        ''' </summary>        
        ''' <returns></returns>
        Public Function updateReferral(ByVal objUpdateReferralBO As UpdateReferralBO)

            Dim parametersList As ParameterList = New ParameterList()
            Dim outParametersList As ParameterList = New ParameterList()

            Dim referralHistoryIdParam As ParameterBO = New ParameterBO("referralHistoryId", objUpdateReferralBO.ReferralHistoryId, DbType.Int32)
            parametersList.Add(referralHistoryIdParam)

            Dim actionIdParam As ParameterBO = New ParameterBO("actionId", objUpdateReferralBO.ActionId, DbType.Int32)
            parametersList.Add(actionIdParam)

            Dim assigneeIdParam As ParameterBO = New ParameterBO("assignedTo", objUpdateReferralBO.AssignToId, DbType.Int32)
            parametersList.Add(assigneeIdParam)

            Dim notesParam As ParameterBO = New ParameterBO("notes", objUpdateReferralBO.Notes, DbType.String)
            parametersList.Add(notesParam)

            Dim rejectedByParam As ParameterBO = New ParameterBO("updatedBy", objUpdateReferralBO.UpdatedBy, DbType.Int32)
            parametersList.Add(rejectedByParam)

            Dim reasonNotesParam As ParameterBO = New ParameterBO("reason", objUpdateReferralBO.Reason, DbType.String)
            parametersList.Add(reasonNotesParam)

            Dim isSavedParam As ParameterBO = New ParameterBO("isSaved", 0, DbType.Boolean)
            outParametersList.Add(isSavedParam)

            Dim result As Integer = MyBase.SaveRecord(parametersList, outParametersList, SpNameConstants.UpdateReferral)
            If outParametersList.Item(0).Value = True Then
                Return True
            Else
                Return False
            End If

        End Function
#End Region

#End Region
    End Class

End Namespace
