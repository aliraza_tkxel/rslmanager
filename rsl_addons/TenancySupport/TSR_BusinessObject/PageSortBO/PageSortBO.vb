﻿Namespace TSR_BusinessObject


    <Serializable()> _
    Public Class PageSortBO
    
        Public Sub New()

        End Sub
        Public Sub New(ByVal sSDirection As String, ByVal sExpression As String, ByVal pNumber As Integer, ByVal pSize As Integer)
            SortDirection = "Ascending"
            SmallSortDirection = "ASC"
            SortExpression = sExpression
            PageNumber = pNumber
            PageSize = pSize
        End Sub

        Private _sortExpression As String
        Public Property SortExpression() As String
            Get
                Return _sortExpression
            End Get
            Set(ByVal value As String)
                _sortExpression = value
            End Set
        End Property

        Private _sortDirection As String
        Public Property SortDirection() As String
            Get
                Return _sortDirection
            End Get
            Set(ByVal value As String)
                _sortDirection = value
            End Set
        End Property

        Private _smallSortDirection As String
        Public Property SmallSortDirection() As String
            Get
                Return _smallSortDirection
            End Get
            Set(ByVal value As String)
                _smallSortDirection = value
            End Set
        End Property


        Private _pageNumber As Integer
        Public Property PageNumber() As Integer
            Get
                Return _pageNumber
            End Get
            Set(ByVal value As Integer)
                _pageNumber = value
            End Set
        End Property

        Private _pageSize As Integer
        Public Property PageSize() As Integer
            Get
                Return _pageSize
            End Get
            Set(ByVal value As Integer)
                _pageSize = value
            End Set
        End Property

        Private _totalPages As Integer
        Public Property TotalPages() As Integer
            Get
                Return _totalPages
            End Get
            Set(ByVal value As Integer)
                _totalPages = value
            End Set
        End Property

        Private _totalRecords As Integer
        Public Property TotalRecords() As Integer
            Get
                Return _totalRecords
            End Get
            Set(ByVal value As Integer)
                _totalRecords = value
            End Set
        End Property

        Private _searchText As String
        Public Property SearchText() As String
            Get
                Return _searchText
            End Get
            Set(ByVal value As String)
                _searchText = value
            End Set
        End Property

#Region "set Sort Direction"
        Public Sub setSortDirection()
            If Me.SortDirection = "Ascending" Then

                Me.SortDirection = "Descending"
                Me.SmallSortDirection = "DESC"

            Else
                Me.SortDirection = "Ascending"
                Me.SmallSortDirection = "ASC"
            End If
        End Sub
#End Region



    End Class
End Namespace
