﻿Imports Microsoft.Practices.EnterpriseLibrary.Validation.Validators
Imports Microsoft.Practices.EnterpriseLibrary.Validation
Imports System

Public Class RejectReferralBO

#Region "Attributes"

    Private _referralHistoryId As Integer
    Private _rejectedBy As Integer
    Private _customerId As Integer
    Private _reason As String
    Private _address As String
    Private _tenantName As String
    Private _referredEmail As String
    Private _referredBy As String

#End Region

#Region "Properties"

    Public Property AssignedBy() As String

    Public Property ReferredBy() As String
        Get
            Return _referredBy
        End Get
        Set(ByVal value As String)
            _referredBy = value
        End Set
    End Property

    Public Property ReferredEmail() As String
        Get
            Return _referredEmail
        End Get
        Set(ByVal value As String)
            _referredEmail = value
        End Set
    End Property

    Public Property CustomerId() As Integer
        Get
            Return _customerId
        End Get
        Set(ByVal value As Integer)
            _customerId = value
        End Set
    End Property

    Public Property ReferralHistoryId() As Integer
        Get
            Return _referralHistoryId
        End Get
        Set(ByVal value As Integer)
            _referralHistoryId = value
        End Set
    End Property

    <StringLengthValidator(1, 1000, MessageTemplate:="Please enter reason.")> _
    Public Property Reason() As String
        Get
            Return _reason
        End Get
        Set(ByVal value As String)
            _reason = value
        End Set
    End Property

    Public Property RejectedBy() As Integer
        Get
            Return _rejectedBy
        End Get
        Set(ByVal value As Integer)
            _rejectedBy = value
        End Set
    End Property

    Public Property Address() As String
        Get
            Return _address
        End Get
        Set(ByVal value As String)
            _address = value
        End Set
    End Property

    Public Property TenantName() As String
        Get
            Return _tenantName
        End Get
        Set(ByVal value As String)
            _tenantName = value
        End Set
    End Property



#End Region

End Class

