﻿Namespace TSR_BusinessObject

    Public Class CustomerDetailBO

        Private _userId As Integer
        Public Property UserId() As Integer
            Get
                Return _userId
            End Get
            Set(ByVal value As Integer)
                _userId = value
            End Set
        End Property
        Private _propertyId As String
        Public Property PropertyId() As String
            Get
                Return _propertyId
            End Get
            Set(ByVal value As String)
                _propertyId = value
            End Set
        End Property

        Private _tenancyId As Integer
        Public Property TenancyId() As Integer
            Get
                Return _tenancyId
            End Get
            Set(ByVal value As Integer)
                _tenancyId = value
            End Set
        End Property

        Private _itemNatureId As Integer
        Public Property ItemNatureId() As Integer
            Get
                Return _itemNatureId
            End Get
            Set(ByVal value As Integer)
                _itemNatureId = value
            End Set
        End Property

        Private _itemId As Integer
        Public Property ItemId() As Integer
            Get
                Return _itemId
            End Get
            Set(ByVal value As Integer)
                _itemId = value
            End Set
        End Property

        Private _customerId As Integer
        Public Property CustomerId() As Integer
            Get
                Return _customerId
            End Get
            Set(ByVal value As Integer)
                _customerId = value
            End Set
        End Property
        Private _title As String
        Public Property Title() As String
            Get
                Return _title
            End Get
            Set(ByVal value As String)
                _title = value
            End Set
        End Property

        Private _journalId As Integer
        Public Property JournalId() As Integer
            Get
                Return _journalId
            End Get
            Set(ByVal value As Integer)
                _journalId = value
            End Set
        End Property

        Private _itemStatusId As Integer
        Public Property ItemStatusId() As Integer
            Get
                Return _itemStatusId
            End Get
            Set(ByVal value As Integer)
                _itemStatusId = value
            End Set
        End Property

        Private _itemActionId As Integer
        Public Property ItemActionId() As Integer
            Get
                Return _itemActionId
            End Get
            Set(ByVal value As Integer)
                _itemActionId = value
            End Set
        End Property

        Private _isTypeArears As Boolean
        Public Property IsTypeArears() As Boolean
            Get
                Return _isTypeArears
            End Get
            Set(ByVal value As Boolean)
                _isTypeArears = value
            End Set
        End Property

        Private _isTypeDamage As Boolean
        Public Property IsTypeDamage() As Boolean
            Get
                Return _isTypeDamage
            End Get
            Set(ByVal value As Boolean)
                _isTypeDamage = value
            End Set
        End Property

        Private _isTypeASB As Boolean
        Public Property IsTypeASB() As Boolean
            Get
                Return _isTypeASB
            End Get
            Set(ByVal value As Boolean)
                _isTypeASB = value
            End Set
        End Property

        Private _stageId As Integer
        Public Property StageId() As Integer
            Get
                Return _stageId
            End Get
            Set(ByVal value As Integer)
                _stageId = value
            End Set
        End Property

        Private _safetyIssue As Integer
        Public Property SafetyIssue() As Integer
            Get
                Return _safetyIssue
            End Get
            Set(ByVal value As Integer)
                _safetyIssue = value
            End Set
        End Property

        Private _conviction As Integer
        Public Property Conviction() As Integer
            Get
                Return _conviction
            End Get
            Set(ByVal value As Integer)
                _conviction = value
            End Set
        End Property

        Private _subStanceMisUse As Integer
        Public Property SubStanceMisUse() As Integer
            Get
                Return _subStanceMisUse
            End Get
            Set(ByVal value As Integer)
                _subStanceMisUse = value
            End Set
        End Property

        Private _selfHarm As Integer
        Public Property SelfHarm() As Integer
            Get
                Return _selfHarm
            End Get
            Set(ByVal value As Integer)
                _selfHarm = value
            End Set
        End Property

        Private _notes As String
        Public Property Notes() As String
            Get
                Return _notes
            End Get
            Set(ByVal value As String)
                _notes = value
            End Set
        End Property

        Private _isTenantAware As Integer
        Public Property IsTenantAware() As Integer
            Get
                Return _isTenantAware
            End Get
            Set(ByVal value As Integer)
                _isTenantAware = value
            End Set
        End Property

        Private _neglectNotes As String
        Public Property NeglectNotes() As String
            Get
                Return _neglectNotes
            End Get
            Set(ByVal value As String)
                _neglectNotes = value
            End Set
        End Property

        Private _miscNotes As String
        Public Property MiscNotes() As String
            Get
                Return _miscNotes
            End Get
            Set(ByVal value As String)
                _miscNotes = value
            End Set
        End Property

        Private _otherNotes As String
        Public Property OtherNotes() As String
            Get
                Return _otherNotes
            End Get
            Set(ByVal value As String)
                _otherNotes = value
            End Set
        End Property

    End Class

End Namespace
