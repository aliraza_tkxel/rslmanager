﻿
Namespace TSR_BusinessObject

    <Serializable()> _
    Public Class ReportDataBO


        Private _fromDate As String
        Public Property FromDate() As String
            Get
                Return _fromDate
            End Get
            Set(ByVal value As String)
                _fromDate = value
            End Set
        End Property

        Private _toDate As String
        Public Property ToDate() As String
            Get
                Return _toDate
            End Get
            Set(ByVal value As String)
                _toDate = value
            End Set
        End Property

        Private _lastAction As Integer
        Public Property LastAction() As Integer
            Get
                Return _lastAction
            End Get
            Set(ByVal value As Integer)
                _lastAction = value
            End Set
        End Property

        Private _status As Integer
        Public Property Status() As Integer
            Get
                Return _status
            End Get
            Set(ByVal value As Integer)
                _status = value
            End Set
        End Property



    End Class

End Namespace