﻿Imports Microsoft.Practices.EnterpriseLibrary.Validation.Validators
Imports Microsoft.Practices.EnterpriseLibrary.Validation
Imports System

Public Class UpdateReferralBO

#Region "Attributes"

    Private _referralHistoryId As Integer
    Private _actionId As Integer
    Private _assignToId As Integer
    Private _updatedBy As Integer
    Private _reason As String
    Private _notes As String

#End Region

#Region "Constructor"

    Public Sub New()

        _referralHistoryId = -1
        _actionId = -1
        _assignToId = -1
        _updatedBy = -1
        _reason = String.Empty
        _notes = String.Empty

    End Sub

#End Region

#Region "Properties"

    Public Property ReferralHistoryId() As Integer
        Get
            Return _referralHistoryId
        End Get
        Set(ByVal value As Integer)
            _referralHistoryId = value
        End Set
    End Property

    Public Property ActionId() As Integer
        Get
            Return _actionId
        End Get
        Set(ByVal value As Integer)
            _actionId = value
        End Set
    End Property

    Public Property UpdatedBy() As Integer
        Get
            Return _updatedBy
        End Get
        Set(ByVal value As Integer)
            _updatedBy = value
        End Set
    End Property

    Public Property AssignToId() As Integer
        Get
            Return _assignToId
        End Get
        Set(ByVal value As Integer)
            _assignToId = value
        End Set
    End Property

    <StringLengthValidator(1, 1000, MessageTemplate:="Please enter reason.")> _
    Public Property Reason() As String
        Get
            Return _reason
        End Get
        Set(ByVal value As String)
            _reason = value
        End Set
    End Property


    Public Property Notes() As String
        Get
            Return _notes
        End Get
        Set(ByVal value As String)
            _notes = value
        End Set
    End Property


#End Region

End Class
