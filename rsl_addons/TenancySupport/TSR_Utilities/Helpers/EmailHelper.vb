﻿Imports System.Net.Mail

Namespace TSR_Utilities

    Public Class EmailHelper
        Public Shared Sub sendHtmlFormattedEmail(ByVal recepientName As String, ByVal recepientEmail As String, ByVal subject As String, ByVal body As String)

            Dim mailMessage As MailMessage = New MailMessage
            mailMessage.Subject = subject
            mailMessage.Body = body
            mailMessage.IsBodyHtml = True
            mailMessage.To.Add(New MailAddress(recepientEmail, recepientName))

            'The SmtpClient gets configuration from Web.Config
            Dim smtp As SmtpClient = New SmtpClient

            smtp.Send(mailMessage)

        End Sub

        Public Shared Sub sendHtmlFormattedEmail(ByVal recepientName As String, ByVal recepientEmail As String, ByVal subject As String, ByVal body As String, ByVal sender As String)

            Dim mailMessage As MailMessage = New MailMessage
            mailMessage.Subject = subject
            mailMessage.Body = body
            mailMessage.IsBodyHtml = True
            mailMessage.To.Add(New MailAddress(recepientEmail, recepientName))
            mailMessage.From = New MailAddress("service@broadlandgroup.org", sender)

            'The SmtpClient gets configuration from Web.Config
            Dim smtp As SmtpClient = New SmtpClient

            smtp.Send(mailMessage)

        End Sub


        Public Shared Sub sendHtmlFormattedEmailWithAttachment(ByVal recepientName As String, ByVal recepientEmail As String, ByVal subject As String, ByVal body As String, ByVal filePath As String)

            Dim mailMessage As MailMessage = New MailMessage
            mailMessage.Subject = subject
            mailMessage.Body = body
            mailMessage.IsBodyHtml = True
            mailMessage.To.Add(New MailAddress(recepientEmail, recepientName))


            Dim iCalAttachement As Attachment = New Attachment(filePath)
            mailMessage.Attachments.Add(iCalAttachement)
            'The SmtpClient gets configuration from Web.Config
            Dim smtp As SmtpClient = New SmtpClient

            smtp.Send(mailMessage)

        End Sub

        Public Shared Sub sendHtmlFormattedEmailWithCC(ByVal recepientName As String, ByVal recepientEmail As String, ByVal ccEmail As String, ByVal subject As String, ByVal body As String, ByVal sender As String)

            Dim mailMessage As MailMessage = New MailMessage
            mailMessage.Subject = subject
            mailMessage.Body = body
            mailMessage.IsBodyHtml = True
            mailMessage.To.Add(New MailAddress(recepientEmail, recepientName))
            mailMessage.From = New MailAddress("service@broadlandgroup.org", sender)
            mailMessage.CC.Add(ccEmail)
            'The SmtpClient gets configuration from Web.Config
            Dim smtp As SmtpClient = New SmtpClient

            smtp.Send(mailMessage)

        End Sub

    End Class

End Namespace


