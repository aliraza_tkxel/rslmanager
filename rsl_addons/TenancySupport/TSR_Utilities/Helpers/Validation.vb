﻿Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
Imports System.Web
Imports System.Text.RegularExpressions
Imports System.Collections
Imports System.Web.Caching

Namespace TSR_Utilities
    Public Class TSR_Validation


        Public Shared Function emptyString(ByVal value As String) As Boolean
            If value.Equals(String.Empty) Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Shared Function lengthString(ByVal value As String, ByVal length As Integer) As Boolean
            If value.Length > length Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Shared Function isNumber(value As String) As Boolean
            Dim rgx As New Regex(RegularExpConstants.numbersExp)
            If Not rgx.IsMatch(value) Then
                Return False
            Else
                Return True
            End If
        End Function


        Public Shared Function isEmail(ByVal value As String) As Boolean
            Dim rgx As New Regex(RegularExpConstants.emailExp)
            If rgx.IsMatch(value) Then
                Return True
            Else
                Return False
            End If
        End Function


    End Class
End Namespace
