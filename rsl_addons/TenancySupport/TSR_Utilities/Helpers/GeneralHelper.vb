﻿Imports System.Globalization
Imports System.Web.HttpContext
Imports System.Web.UI.Page
Imports System.Text
Imports System.Web
Imports System.IO
Imports System.Net
Imports System.Net.Mail
Imports TSR_BusinessObject
Imports System.Web.UI.WebControls
Imports System.Text.RegularExpressions



Namespace TSR_Utilities
    Public Class GeneralHelper

#Region "get Document Upload Path"
        Public Shared Function getDocumentUploadPath() As String
            Return System.Configuration.ConfigurationSettings.AppSettings("DocumentsUploadPath")
        End Function
#End Region

#Region "get Us Cultured Date Time"

        ''' <summary>
        ''' Returns the US cultured date time.
        ''' </summary>
        ''' <param name="date">date to be converted.</param>
        ''' <returns>US Cultured DateTime</returns>

        Public Shared Function getUsCulturedDateTime(ByVal [date] As String) As DateTime
            Dim infoUS As New CultureInfo("en-US")
            Return Convert.ToDateTime([date].ToString(), infoUS)
        End Function
#End Region

#Region "get Us Cultured Date Time"

        ''' <summary>
        ''' Returns the UK cultured date time.
        ''' </summary>
        ''' <param name="date">date to be converted.</param>
        ''' <returns>UK Cultured DateTime</returns>

        Public Shared Function getUKCulturedDateTime(ByVal [date] As String) As DateTime
            Dim infoUK As New CultureInfo("en-GB")
            Return Convert.ToDateTime([date].ToString(), infoUK)
        End Function
#End Region

#Region "get Official Day Start Hour"
        Public Shared Function getOfficialDayStartHour() As String
            Return CType(System.Configuration.ConfigurationSettings.AppSettings("OfficialDayStartHour"), Double)
        End Function
#End Region

#Region "get Official Day End Hour"
        Public Shared Function getOfficialDayEndHour() As String
            Return CType(System.Configuration.ConfigurationSettings.AppSettings("OfficialDayEndHour"), Double)
        End Function
#End Region

#Region "get Appointment Duration Hours"
        ''' <summary>
        ''' 'Appointment Duration represents total hours to complete the appointment e.g 1 hour
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getAppointmentDurationHours() As String
            Return CType(System.Configuration.ConfigurationSettings.AppSettings("AppointmentDurationHours"), Double)
        End Function
#End Region

#Region "Get Today Start Hour"

        Public Shared Function getTodayStartHour(ByVal startDate As Date) As Double
            Return Math.Floor(CType(startDate.ToString("HH"), Double) + (CType(startDate.ToString("mm"), Double) / 60))
        End Function

#End Region

        '#Region "get Unique Keys"
        '        Public Shared Function getUniqueKey(ByVal uniqueCounter As Integer) As String
        '            'this 'll be used to create the unique index of dictionary
        '            Dim uniqueKey As String = uniqueCounter.ToString() + SessionManager.getUserEmployeeId().ToString() + DateTime.Now.ToString("ddMMyyyyhhmmssffff")
        '            Return uniqueKey
        '        End Function
        '#End Region

#Region "Convert Date In Seconds"
        Public Shared Function convertDateInSec(ByVal timeToConvert As Date) As Integer
            Dim calendarStartDate As New DateTime(1970, 1, 1)
            Dim timeInSec As Integer = (timeToConvert - calendarStartDate).TotalSeconds
            Return timeInSec
        End Function
#End Region

#Region "Convert Date In Minute"
        Public Shared Function convertDateInMin(ByVal timeToConvert As Date) As Integer
            Dim calendarStartDate As New DateTime(1970, 1, 1)
            Dim timeInSec As Integer = (timeToConvert - calendarStartDate).TotalMinutes
            Return timeInSec
        End Function
#End Region

#Region "Convert Date to Us Culture"
        ''' <summary>
        ''' This fucntion 'll convert the date in following format e.g Mon 12th Nov 2012
        ''' </summary>
        ''' <param name="dateToConvert"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function convertDateToUsCulture(ByVal dateToConvert As Date) As String
            Return CType(dateToConvert, Date).ToString("dddd d MMMM yyyy", CultureInfo.CreateSpecificCulture("en-US"))
        End Function

#End Region

#Region "Convert Time To 24 Hrs Format"
        ''' <summary>
        ''' This funciton will convert the datetime in 24 hours format
        ''' </summary>
        ''' <param name="timeToConvert"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function ConvertTimeTo24hrsFormat(ByVal timeToConvert As Date) As String
            Return timeToConvert.ToString("HH:mm")
        End Function
#End Region

#Region "repalce Rc RB Td"
        Public Shared Function repalceRcRBTd(ByRef letterBodyValue As String, ByRef rentBalance As Double, ByRef rentCharge As Double, ByRef todayDate As Date)
            letterBodyValue = letterBodyValue.Replace("[RB]", rentBalance)
            letterBodyValue = letterBodyValue.Replace("[RC]", rentCharge)
            letterBodyValue = letterBodyValue.Replace("[TD]", todayDate.ToShortDateString())
            Return letterBodyValue
        End Function
#End Region

#Region "Convert UTF8 String to ASCII String"

        Public Shared Function UTF8toASCII(ByVal text As String) As String
            Dim utf8 As System.Text.Encoding = System.Text.Encoding.UTF8
            Dim encodedBytes As [Byte]() = utf8.GetBytes(text)
            Dim convertedBytes As [Byte]() = Encoding.Convert(Encoding.UTF8, Encoding.ASCII, encodedBytes)
            Dim ascii As System.Text.Encoding = System.Text.Encoding.ASCII

            Return ascii.GetString(convertedBytes)
        End Function

#End Region

#Region "Get 12 Hour Time Format From Date"
        ''' <summary>
        ''' This funciton 'll extract the time(in 12 hours Format) from date and return that
        ''' </summary>
        ''' <param name="aDate">date with time in 12 hours format</param>
        ''' <returns>time in 12 hours format</returns>
        ''' <remarks></remarks>
        Public Shared Function get12HourTimeFormatFromDate(ByVal aDate As Date) As Date
            Return CType(aDate.ToString("HH") + ":" + aDate.ToString("mm"), Date)
        End Function
#End Region

#Region "Get 24 Hour Time Format From Date"
        ''' <summary>
        ''' This funciton 'll extract the time(in 24 hours Format) from date and return that
        ''' </summary>
        ''' <param name="aDate">date with time in 24 hours format</param>
        ''' <returns>time in 12 hours format</returns>
        ''' <remarks></remarks>
        Public Shared Function get24HourTimeFormatFromDate(ByVal aDate As Date)
            Return aDate.ToString("HH:mm")
        End Function
#End Region

#Region "Get Date with weekday format"
        ''' <summary>
        ''' Get Date with weekday format e.g "Tuesday 13th December 2013"
        ''' </summary>
        ''' <param name="aDate">date time</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getDateWithWeekdayFormat(ByVal aDate As DateTime)
            Dim startDay As String = aDate.DayOfWeek.ToString()
            Dim startDate As String = aDate.ToString("dd") + "th"
            Dim startMonth As String = aDate.ToString("MMMMMMMMMMMMM")
            Dim startYear As String = aDate.ToString("yyyy")
            Return startDay + " " + startDate + " " + startMonth + " " + startYear
        End Function
#End Region

#Region "Set Grid View Pager"

#End Region

#Region "Get Hours From Time"
        ''' <summary>
        ''' This funciton 'll extract the hours from time and return that
        ''' </summary>
        ''' <param name="aTime">time </param>
        ''' <returns>number as double</returns>
        ''' <remarks></remarks>
        Public Shared Function getHoursFromTime(ByVal aTime As Date) As Double
            Return CType(aTime.ToString("HH"), Double) + (CType(aTime.ToString("mm"), Double) / 60)
        End Function

#End Region

#Region "Get Date Time From Date And Number"
        ''' <summary>
        ''' This function 'll append the date with number to get date time and return it
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getDateTimeFromDateAndNumber(ByVal aDate As Date, ByVal aHour As Double) As Date
            Return CType(aDate.Date.ToLongDateString() + " " + aHour.ToString() + ":00", Date)
        End Function
#End Region

#Region "Combine Date and Time"
        ''' <summary>
        ''' this function 'll append the time with date and returns that 
        ''' </summary>
        ''' <param name="aDate"></param>
        ''' <param name="aTime"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function combineDateAndTime(ByVal aDate As Date, ByVal aTime As String)
            Return CType(aDate.ToLongDateString() + " " + aTime, Date)
        End Function
#End Region

#Region "convert Date Time In to Date Time String"
        ''' <summary>
        ''' This function 'll return the date in the following format in string (HH:mm dddd d MMMM yyyy)
        ''' </summary>
        ''' <param name="aDate"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function convertDateToCustomString(ByVal aDate As Date)
            Return (aDate.ToString("HH:mm dddd d MMMM yyyy", CultureInfo.CreateSpecificCulture("en-US")))
        End Function
#End Region

#Region "Convert Date Time to UK Date format."
        ''' <summary>
        ''' This function 'll return the date in the following format in string (dd/mm/yyyy)
        ''' </summary>
        ''' <param name="aDate"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function convertDateToUkDateFormat(ByVal aDate As String)
            Dim format As String = "dd/MM/yyyy"
            Return DateTime.ParseExact(aDate, format, CultureInfo.CreateSpecificCulture("en-UK"))
        End Function
#End Region

#Region "get Hours And Minute String"
        ''' <summary>
        ''' This function 'll convert the date into following format : (HH:mm)
        ''' </summary>
        ''' <param name="aDateTime"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getHrsAndMinString(ByVal aDateTime As DateTime)
            Return aDateTime.ToString("HH:mm")
        End Function
#End Region

#Region "set Grid View Pager"
        Public Shared Sub setGridViewPager(ByRef pnlPagination As Panel, ByVal objPageSortBO As PageSortBO)

            pnlPagination.Visible = False
            If objPageSortBO.TotalRecords >= 1 Then
                pnlPagination.Visible = True

                Dim pageNumber As Integer = objPageSortBO.PageNumber
                Dim pageSize As Integer = objPageSortBO.PageSize
                Dim TotalRecords As Integer = objPageSortBO.TotalRecords
                Dim totalPages As Integer = objPageSortBO.TotalPages

                Dim lnkbtnPagerFirst As LinkButton = DirectCast(pnlPagination.FindControl("lnkbtnPagerFirst"), LinkButton)
                Dim lnkbtnPagerPrev As LinkButton = DirectCast(pnlPagination.FindControl("lnkbtnPagerPrev"), LinkButton)
                Dim lnkbtnPagerNext As LinkButton = DirectCast(pnlPagination.FindControl("lnkbtnPagerNext"), LinkButton)
                Dim lnkbtnPagerLast As LinkButton = DirectCast(pnlPagination.FindControl("lnkbtnPagerLast"), LinkButton)

                lnkbtnPagerFirst.Enabled = (pageNumber <> 1)
                lnkbtnPagerFirst.Font.Underline = lnkbtnPagerFirst.Enabled

                lnkbtnPagerPrev.Enabled = lnkbtnPagerFirst.Enabled
                lnkbtnPagerPrev.Font.Underline = lnkbtnPagerPrev.Enabled

                lnkbtnPagerNext.Enabled = (pageNumber <> totalPages)
                lnkbtnPagerNext.Font.Underline = lnkbtnPagerNext.Enabled

                lnkbtnPagerLast.Enabled = lnkbtnPagerNext.Enabled
                lnkbtnPagerLast.Font.Underline = lnkbtnPagerLast.Enabled

                Dim lblPagerCurrentPage As Label = DirectCast(pnlPagination.FindControl("lblPagerCurrentPage"), Label)
                lblPagerCurrentPage.Text = pageNumber

                Dim lblPagerTotalPages As Label = DirectCast(pnlPagination.FindControl("lblPagerTotalPages"), Label)
                lblPagerTotalPages.Text = totalPages

                Dim lblPagerRecordStart As Label = DirectCast(pnlPagination.FindControl("lblPagerRecordStart"), Label)
                Dim pagerRecordStart As Integer = ((pageNumber - 1) * pageSize) + 1
                lblPagerRecordStart.Text = pagerRecordStart

                Dim lblPagerRecordEnd As Label = DirectCast(pnlPagination.FindControl("lblPagerRecordEnd"), Label)
                Dim pagerRecordEnd As Integer = pagerRecordStart + pageSize - 1
                lblPagerRecordEnd.Text = IIf(pagerRecordEnd < TotalRecords, pagerRecordEnd, TotalRecords)

                Dim lblPagerRecordTotal As Label = DirectCast(pnlPagination.FindControl("lblPagerRecordTotal"), Label)
                lblPagerRecordTotal.Text = TotalRecords
               
            End If
        End Sub
#End Region

#Region "get Appointment Creation Limit For Single Request"
        ''' <summary>
        ''' This function 'll return the appointment creation limit for single request
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getAppointmentCreationLimitForSingleRequest()
            Return 5
        End Function

#End Region

#Region "get Misc Appointment Creation Limit For Single Request"
        ''' <summary>
        ''' This function 'll return the misc appointment creation limit for single request
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getMiscAppointmentCreationLimitForSingleRequest()
            Return 20
        End Function

#End Region

#Region "append Day Label"
        ''' <summary>
        ''' This function 'll append day or days with number
        ''' </summary>
        ''' <param name="aNumber"></param>
        ''' <remarks></remarks>
        Public Shared Function appendDayLabel(ByVal aNumber As Double)
            If aNumber > 0 Then
                Return aNumber.ToString() + " days"
            Else
                Return aNumber.ToString() + " day"
            End If

        End Function
#End Region

#Region "append Hour Label"
        ''' <summary>
        ''' This function 'll append day or days with number
        ''' </summary>
        ''' <param name="aNumber"></param>
        ''' <remarks></remarks>
        Public Shared Function appendHourLabel(ByVal aNumber As Double)
            If aNumber > 0 Then
                Return aNumber.ToString() + " hours"
            Else
                Return aNumber.ToString() + " hour"
            End If

        End Function
#End Region

        Public Shared Function isEmail(ByVal value As String) As Boolean
            Dim rgx As New Regex(RegularExpConstants.emailExp)
            If rgx.IsMatch(value) Then
                Return True
            Else
                Return False
            End If
        End Function

    End Class


End Namespace
