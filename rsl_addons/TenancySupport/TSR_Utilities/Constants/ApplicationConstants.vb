﻿
Namespace TSR_Utilities
    Public Class ApplicationConstants

        Public Enum ReferralOutcome
            Assigned
            Rejected
        End Enum

#Region "General"
        Public Const DropDownDefaultValue As String = "-1"
        ''' <summary>
        ''' This variable shall be used to as key in a list and that list shall be used to highlight the menu items
        ''' </summary>
        ''' <remarks></remarks>
        Public Const LinkButtonType As String = "LinkButtonType"
        ''' <summary>
        ''' This variable shall be used to as key in a list and that list shall be used to highlight the menu items
        ''' </summary>
        ''' <remarks></remarks>
        Public Const UpdatePanelType As String = "UpdatePanelType"
        Public Const NotAvailable As String = "N/A"
        Public Const DefaultValue As String = "-1"

#End Region

#Region "Master Layout"

        Public Const ConWebBackgroundColor As String = "#e6e6e6"
        Public Const ConWebBackgroundProperty As String = "background-color"
#End Region

#Region "CSS"
        Public Const TabClickedCssClass As String = "TabClicked"
        Public Const TabInitialCssClass As String = "TabInitial"
#End Region

#Region "Referral form"

        ' Data table
        Public Const StagesDt As String = "StagesDt"
        Public Const YesNoUnknownDt As String = "YesNoUnknownDt"
        Public Const YesNoDt As String = "YesNoDt"
        Public Const HelpCategoriesDt As String = "HelpCategoriesDt"
        Public Const HelpSubCategoriesDt As String = "HelpSubCategoriesDt"
        Public Const FormDataDt As String = "FormDataDt"
        Public Const HouseholdDt As String = "HouseholdDt"
        Public Const AgenciesDt As String = "AgenciesDt"

        ' Fields
        Public Const ReferrerCol As String = "Referrer"
        Public Const RefDateCol As String = "RefDate"
        Public Const TenancyIdCol As String = "TenancyId"
        Public Const CustomerNameCol As String = "CustomerName"
        Public Const DateOfBirthCol As String = "DateOfBirth"
        Public Const AddressCol As String = "Address"
        Public Const TelephoneCol As String = "Telephone"
        Public Const PreferedContactCol As String = "PreferedContact"
        Public Const IsTypeArrearsCol As String = "isTypeArrears"
        Public Const IsTypeDamageCol As String = "isTypeDamage"
        Public Const IsTypeASBCol As String = "isTypeASB"
        Public Const StageIdCol As String = "STAGEID"
        Public Const SafetyIssuesCol As String = "SafetyIssues"
        Public Const ConvictionCol As String = "Conviction"
        Public Const SubstanceMisuseCol As String = "SubstanceMisuse"
        Public Const SelfHarmCol As String = "SelfHarm"
        Public Const YesNotesCol As String = "YesNotes"
        Public Const NeglectNotesCol As String = "NeglectNotes"
        Public Const OtherNotesCol As String = "OtherNotes"
        Public Const MiscNotesCol As String = "MiscNotes"
        Public Const IsTenantAwareCol As String = "IsTenantAware"
        Public Const refferalForm As String = "refferalForm"
        Public Const viewRefferalForm As String = "viewReferralForm"


#End Region

#Region "Report"

        Public Const emailSender As String = "BHG Training and Support Manager"
        Public Const rejectEmailSubject As String = "Tenant Support Referral - Rejected"
        Public Const assignEmailSubject As String = "Tenant Support Referral - Assigned"
        Public Const reportName As String = "rptTenancySupport"


        ' Actions
        Public Const ReferredActionId As Integer = 17
        Public Const RejectedActionId As Integer = 18
        Public Const RejectActionId As Integer = 23
        Public Const ClosedActionId As Integer = 21
        Public Const NoteActionId As Integer = 20
        Public Const AssignActionId As Integer = 22
        Public Const AssignedActionId As Integer = 19

        ' Status
        Public Const PendingStatusId As Integer = 30
        Public Const OpenStatusId As Integer = 13
        Public Const CloseStatusId As Integer = 14

        ' Fields
        Public Const TenantNameCol As String = "TenantName"
        Public Const TenantAddressCol As String = "TenantAddress"

        Public Const AssignTextField As String = "AssigneName"
        Public Const AssignValueField As String = "AssigneeId"

        'Datatable
        Public Const ActionsDt As String = "ActionsDt"
        Public Const TenantInfoDt As String = "TenantInfoDt"
        Public Const AssignToDt As String = "AssignToDt"
        Public Const IsTsmDt As String = "IsTsmDt"

#End Region


    End Class
End Namespace

