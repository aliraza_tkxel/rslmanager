﻿
Namespace TSR_Utilities
    Public Class UserMessageConstants
#Region "Generic Messages"

        Public Const InvalidPageNumber As String = "Invalid Records per page."
        Public Const RecordNotFound As String = "Record not found."
        Public Const RecordNotExist As String = "No record exists"
        Public Const RecordNotSelected As String = "Please select the record."
        Public Const RecordExceed As String = "Please apply filter to load more results."
        Public Shared DistanceError As String = "Unable to get distance."
        Public Const INVALIDREQUEST As String = "Message from Google: The provided request was invalid."
        Public Const MAXELEMENTSEXCEEDED As String = "Message from Google: The product of origins and destinations exceeds the per-query limit."
        Public Const OVERQUERYLIMIT As String = "Message from Google: The service has received too many requests from your application within the allowed time period."
        Public Const REQUESTDENIED As String = "Message from Google: The service denied use of the Distance Matrix service by your application."
        Public Const UNKNOWNERROR As String = "Message from Google: The distance Matrix request could not be processed due to a server error. The request may succeed if you try again."
        Public Const NoRecordFound As String = "No record found."
        Public Const TradeAlreadyAdded = "The Operative has already been added."

        Public Const UserDoesNotExist As String = "You don't have access to Planned Maintenance. Please contact administrator."
        Public Const UsersAccountDeactivated As String = "Your access to Planned Maintenance has been de-activiated. Please contact administrator."

        Public Const SuccessfulSchedule As String = "Successfully scheduled."
        Public Const CompletedArrangedStatusNotSchedule As String = "Properties with appointment status Completed or Arranged can not be scheduled."
        Public Const InvalidPropertyId As String = "Invalid PropertyId."
        Public Const ComponentTradeDeletionFailed As String = "Selected component trade cannot be deleted because it is referenced in another table."
        Public Const TenantNotExist As String = "Tenant do not exist against this property."

        Public Const ErrorDocumentUpload As String = "Error occurred while uploading the document.Please try again."
        Public Const InspectionArrangedSuccessfully As String = "Inspection arranged successfully."

        Public Const SelectOperative As String = "Please select operative."
        Public Const SelectAction As String = "Please select Action."
        Public Const SelectInspectionTime As String = "Please select Inspection time."
        Public Const SelectTrade As String = "Please select Trade."
        Public Const SelectInspectionDate As String = "Please select Inspection date."
        Public Const SelectFutureDate As String = "Please select future date."
        Public Const SelectDropdowns As String = "Please select drop downs."

        Public Const EnterWorksRequired As String = "Please enter works required description."
        Public Const SelectDuration As String = "Please select duration."
        Public Const InvalidDateFormat As String = "Invalid date. Please enter date in this format dd/MM/yyyy."
        Public Const SelectProperty As String = "Please select property."

        Public Const InspectionArrangedEmailError As String = "Inspection saved but unable to send email due to : "
        Public Const ProblemLoadingData As String = "Problem loading data."
        Public Const InvalidJobSheetIndex As String = "Invalid Job Sheet Index."
        Public Const MaxLengthExceed As String = "Please reduce length of notes. Maximum 1000 characters are allowed."
        Public Const ErrorStatus As String = "Error!"

        Public Const InvalidReferenceNumber As String = "Invalid Reference Number."
        Public Const ProblemRejectReferral As String = "Problem while rejecting referral."
        Public Const ReferralRejectedEmailFailed As String = "Referral rejected but unable to send email."
        Public Const ReferralRejectedInvalidEmail As String = "Referral rejected but unable to send email due to invalid email address."

        Public Const InvalidQueryString As String = "Invalid query string parameters."
        Public Const SelectAssignee As String = "Please select assignee."
        Public Const EnterReason As String = "Please enter reason."
        Public Const EnterNotes As String = "Please enter notes."
        Public Const SuccessfullyUpdated As String = "Referral successfully updated."
        Public Const FailedUpdateReferral As String = "Failed to update Referral."
        Public Const SuccessfullyRecorded As String = "Referral successfully recorded."

#End Region

#Region "File / Directory Path"
        Public Const DirectoryDoesNotExist As String = "This path {0} does not exist on hard drive. Please create this path on drive."
        Public Const DocumentUploadPathKeyDoesNotExist As String = "Document upload path key does not exist in web.config. Please insert the document upload path key in web.config under app settings"
        Public Const FileDoesNotExist As String = "This file {0} does not exist on hard drive."
        Public Const InvalidDocumentType As String = "Invlaid document type."
        Public Const InvalidImageType As String = "Invlaid image type."
        Public Const SelectDocument As String = "Please select the document"
        Public Const FileUploadedSuccessfully As String = "File is uploaded successfully."
#End Region

#Region "Email"

        Public Const InvalidEmail As String = "Invalid Email address."
        Public Const InspectionSavedEmailError As String = "Inspection arranged but unable to send email due to : "

#End Region

#Region "Customer"
        Public Const InvalidTelephone As String = "Invalid Telephone number."
        Public Const InvalidMobile As String = "Invalid Mobile number."
#End Region

#Region "Users"
        Public Const UserSavedSuccessfuly As String = "User Saved Successfuly"
        Public Const userAlreadyExists As String = "User already exists"
        Public Const UserNameNotValid As String = "User name is not valid"
        Public Const ErrorUserUpdate As String = "Error Updating Record"
        Public Const ErrorUserInsert As String = "Error Inserting Record"
        Public Const ErrorNoTradeExist As String = "No Trade Exist Against This Fault"

#End Region



    End Class
End Namespace


