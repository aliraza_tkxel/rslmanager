﻿Imports System

Namespace TSR_Utilities
    Public Class SpNameConstants

#Region "Master Page"

        Public Const GetEmployeeById As String = "FL_GetEmployeeById"

#End Region

#Region "Customer Detail"
        Public Const GetStages As String = "TSR_GetStage"
        Public Const GetYESNO_UNKNOWN As String = "TSR_G_YESNO_UNKNOWN"
        Public Const GetYESNO As String = "TSR_YESNO"
        Public Const GetCheckBoxList As String = "TSR_CutomerCheckBoxList"
        Public Const SaveReferralData As String = "TSR_SaveReferral"
        Public Const SaveSubCategory As String = "TSR_SaveSubCategory"        
        Public Const ConfirmReferralRejection As String = "TSR_ConfirmReferralRejection"

#End Region

#Region "Tenancy Support Form"
        Public Const GetTenancySupportFormData As String = "TSR_GetTenancySupportFormData"
#End Region

#Region "Support Tenancy Report "
        Public Const GetSupportReferralReportData As String = "TSR_SupportReferralReport"
        Public Const GetAssignToData As String = "TSR_AssignTODropDown"
        Public Const GetTenantInfo As String = "TSR_GetTenantInfo"
        Public Const SaveAssignInfo As String = "TSR_SaveAssignInfo"

        Public Const GetUpdateReferralData As String = "TSR_GetUpdateReferralData"
        Public Const UpdateReferral As String = "TSR_UpdateReferral"
        Public Const GetReportActions As String = "TSR_getReportActions"
        Public Const GetReportStatus As String = "TSR_getReportStatuses"

#End Region

    End Class
End Namespace

