﻿Namespace TSR_Utilities
    Public Class PathConstants

#Region "URL Constants"

        Public Const LoginPath As String = "~/../BHAIntranet/Login.aspx"
        '  Public Const BridgePath As String = "~/Bridge.aspx"
        Public Const TenancyReferralForm As String = "~/Views/TenancyReferralForm.aspx"
        Public Const TenancySupportReport As String = "~/Views/TenancySupportReport.aspx"
        Public Const CustomerDetail As String = "~/Views/CustomerDetail.aspx"
        Public Const JournalTabTenantRecord As String = "https://{0}/Customer/CRM.asp?CustomerID="
        Public Const UpdateReferral As String = "~/Views/UpdateReferral.aspx"

#End Region

#Region "Query String Constants"

        Public Const Src As String = "src"
        Public Const Resources As String = "resources"
        Public Const Index As String = "index"
        Public Const Page As String = "page"


        Public Const RefId As String = "refid"
        Public Const ActionId As String = "actid"
        Public Const StatusId As String = "statid"

        Public Const CustomerId As String = "CustomerId"
        Public Const TenancyId As String = "TenancyId"
        Public Const ItemId As String = "ItemId"
        Public Const ItemNatureId As String = "ItemNatureId"
        Public Const title As String = "title"
        Public Const PropertyId As String = "PropertyId"
        Public Const Status As String = "status"
        Public Const StatusPending As String = "Pending"
#End Region

#Region "Page Name Constants"

        Public Const updateRefferalPage As String = "updateReferralPage"

#End Region



    End Class
End Namespace

