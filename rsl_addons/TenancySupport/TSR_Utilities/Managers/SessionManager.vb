﻿Imports System.Web.HttpContext



Namespace TSR_Utilities
    Public Class SessionManager

#Region "Set / Get  Tenancy User Id "
        ''' <summary>
        ''' This key shall be used to save the planned maintenance user id
        ''' </summary>
        ''' <remarks></remarks>
        Public Const TSRUserId As String = "TSRUserId"
#Region "Set  Planned Maintenance User Id"
        ''' <summary>
        ''' This function shall be used to save the planned maintenance user id in session
        ''' </summary>
        ''' <param name="TSRUserId"></param>
        ''' <remarks></remarks>
        Public Shared Sub setTSRUserId(ByRef TSRUserId As Integer)
            Current.Session(SessionManager.TSRUserId) = TSRUserId
        End Sub
#End Region

#Region "get Planned Maintenance User Id"
        ''' <summary>
        ''' This function shall be used to get the planned maintenance user id from session
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getTSRUserId() As Integer

            If (IsNothing(Current.Session(SessionManager.TSRUserId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionManager.TSRUserId), Integer)
            End If
        End Function
#End Region

#Region "remove Planned Maintenance User Id"
        ''' <summary>
        ''' This function shall be used to remove the planned maintenance user id in session
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeTSRUserId()
            If (Not IsNothing(Current.Session(SessionManager.TSRUserId))) Then
                Current.Session.Remove(SessionManager.TSRUserId)
            End If

        End Sub

#End Region

#End Region

#Region "Set / Get User Full Name"
        ''' <summary>
        ''' This key shall be used to save the logged in user's name
        ''' </summary>
        ''' <remarks></remarks>
        Public Const UserFullName As String = "UserFullName"
#Region "Set User Full Name"
        ''' <summary>
        ''' This function shall be used to save the logged in user's name in session
        ''' </summary>
        ''' <param name="userFullName"></param>
        ''' <remarks></remarks>
        Public Shared Sub setUserFullName(ByRef userFullName As String)
            Current.Session(SessionManager.UserFullName) = userFullName
        End Sub

#End Region

#Region "get User Full Name"
        ''' <summary>
        ''' This function shall be used to get the logged in user's name from session
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getUserFullName() As String
            If (IsNothing(Current.Session(SessionManager.UserFullName))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionManager.UserFullName), String)
            End If
        End Function

#End Region

#Region "remove User Full Name"
        ''' <summary>
        ''' This function shall be used to remove the logged in user's name from session
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeUserFullName()
            If (Not IsNothing(Current.Session(SessionManager.UserFullName))) Then
                Current.Session.Remove(SessionManager.UserFullName)
            End If

        End Sub

#End Region

#End Region

#Region "Set / Get User Employee Id"
        ''' <summary>
        ''' This key shall be used to get the Employee Id of logged in user
        ''' </summary>
        ''' <remarks></remarks>
        Public Const EmployeeId As String = "UserEmployeeId"
#Region "Set User Employee Id"
        ''' <summary>
        ''' This function shall be used to save the Employee Id of logged in user in session
        ''' </summary>
        ''' <param name="employeeId"></param>
        ''' <remarks></remarks>
        Public Shared Sub setUserEmployeeId(ByRef employeeId As Integer)
            Current.Session(SessionManager.EmployeeId) = employeeId
        End Sub

#End Region

#Region "get User Employee Id"
        ''' <summary>
        ''' This function shall be used to get the Employee Id of logged in user from session
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getUserEmployeeId() As Integer
            If (IsNothing(Current.Session(SessionManager.EmployeeId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionManager.EmployeeId), Integer)
            End If
        End Function

#End Region

#Region "remove User Employee Id"
        ''' <summary>
        ''' This function shall be used to remove the Employee Id of logged in user from session
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeUserEmployeeId()
            If (Not IsNothing(Current.Session(SessionManager.EmployeeId))) Then
                Current.Session.Remove(SessionManager.EmployeeId)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Tenancy Referral History Id"
        ''' <summary>
        ''' This key shall be used to get the Tenancy Referral History Id 
        ''' </summary>
        ''' <remarks></remarks>
        Public Const TenancyReferralHistoryId As String = "TenancyReferralHistoryId"

#Region "Set Tenancy Referral History Id"
        ''' <summary>
        ''' This function shall be used to save the Tenancy Referral History Id in session
        ''' </summary>
        ''' <param name="tenancyReferralHistoryId"></param>
        ''' <remarks></remarks>
        Public Shared Sub setTenancyReferralHistoryId(ByRef tenancyReferralHistoryId As Integer)
            Current.Session(SessionManager.TenancyReferralHistoryId) = tenancyReferralHistoryId
        End Sub

#End Region

#Region "get Tenancy Referral History Id"
        ''' <summary>
        ''' This function shall be used to get the Tenancy Referral History Id from session
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getTenancyReferralHistoryId() As Integer
            If (IsNothing(Current.Session(SessionManager.TenancyReferralHistoryId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionManager.TenancyReferralHistoryId), Integer)
            End If
        End Function

#End Region

#Region "remove Tenancy Referral History Id"
        ''' <summary>
        ''' This function shall be used to remove the Tenancy Referral History Id from session
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeTenancyReferralHistoryId()
            If (Not IsNothing(Current.Session(SessionManager.TenancyReferralHistoryId))) Then
                Current.Session.Remove(SessionManager.TenancyReferralHistoryId)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Tenancy Form Dataset"
        ''' <summary>
        ''' This key shall be used to set/get Tenancy Form Dataset.
        ''' </summary>
        ''' <remarks></remarks>
        Public Const TenancyFormDs As String = "TenancyFormDs"

#Region "Set Tenancy Form Dataset"
        ''' <summary>
        ''' This function shall be used to save Tenancy Form Dataset in session
        ''' </summary>
        ''' <param name="TenancyFormDs"></param>
        ''' <remarks></remarks>
        Public Shared Sub setTenancyFormDs(ByRef tenancyFormDs As DataSet)
            Current.Session(SessionManager.TenancyFormDs) = tenancyFormDs
        End Sub

#End Region

#Region "Get Tenancy Form Dataset"
        ''' <summary>
        ''' This function shall be used to get Tenancy Form Dataset from session
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getTenancyFormDs() As DataSet
            If (IsNothing(Current.Session(SessionManager.TenancyFormDs))) Then
                Return Nothing
            Else
                Return CType(Current.Session.Item(SessionManager.TenancyFormDs), DataSet)
            End If
        End Function

#End Region

#Region "Remove Tenancy Form Dataset"
        ''' <summary>
        ''' This function shall be used to remove Tenancy Form Dataset from session
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeTenancyFormDs()
            If (Not IsNothing(Current.Session(SessionManager.TenancyFormDs))) Then
                Current.Session.Remove(SessionManager.TenancyFormDs)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Reject Referral BO"
        ''' <summary>
        ''' This key shall be used to set/get Reject Referral BO.
        ''' </summary>
        ''' <remarks></remarks>
        Public Const RejectReferralBO As String = "RejectReferralBO"

#Region "Set Reject Referral BO"
        ''' <summary>
        ''' This function shall be used to save Reject Referral BO in session
        ''' </summary>
        ''' <param name="rejectReferralBO"></param>
        ''' <remarks></remarks>
        Public Shared Sub setRejectReferralBo(ByRef rejectReferralBO As RejectReferralBO)
            Current.Session(SessionManager.RejectReferralBO) = rejectReferralBO
        End Sub

#End Region

#Region "Get RejectReferralBO"
        ''' <summary>
        ''' This function shall be used to get RejectReferralBO from session
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getRejectReferralBO() As RejectReferralBO
            If (IsNothing(Current.Session(SessionManager.RejectReferralBO))) Then
                Return Nothing
            Else
                Return CType(Current.Session.Item(SessionManager.RejectReferralBO), RejectReferralBO)
            End If
        End Function

#End Region

#Region "Remove Reject Referral BO"
        ''' <summary>
        ''' This function shall be used to remove RejectReferralBO from session
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeRejectReferralBO()
            If (Not IsNothing(Current.Session(SessionManager.RejectReferralBO))) Then
                Current.Session.Remove(SessionManager.RejectReferralBO)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Assignee Dataset"
        ''' <summary>
        ''' This key shall be used to set/get Tenancy Form Dataset.
        ''' </summary>
        ''' <remarks></remarks>
        Public Const AssigneeDs As String = "AssigneeDs"

#Region "Set AssigneeDs Dataset"
        ''' <summary>
        ''' This function shall be used to save Tenancy Form Dataset in session
        ''' </summary>
        ''' <param name="AssigneeDs"></param>
        ''' <remarks></remarks>
        Public Shared Sub setAssigneeDs(ByRef assigneeDs As DataSet)
            Current.Session(SessionManager.AssigneeDs) = assigneeDs
        End Sub

#End Region

#Region "Get Assignee Dataset"
        ''' <summary>
        ''' This function shall be used to get Tenancy Form Dataset from session
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getAssigneeDs() As DataSet
            If (IsNothing(Current.Session(SessionManager.AssigneeDs))) Then
                Return Nothing
            Else
                Return CType(Current.Session.Item(SessionManager.AssigneeDs), DataSet)
            End If
        End Function

#End Region

#Region "Remove Assignee Dataset"
        ''' <summary>
        ''' This function shall be used to remove Tenancy Form Dataset from session
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeAssigneeDs()
            If (Not IsNothing(Current.Session(SessionManager.AssigneeDs))) Then
                Current.Session.Remove(SessionManager.AssigneeDs)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Update Referral Dataset"
        ''' <summary>
        ''' This key shall be used to set/get Update Referral Dataset.
        ''' </summary>
        ''' <remarks></remarks>
        Public Const UpdateReferralDs As String = "UpdateReferralDs"

#Region "Set Update Referral Dataset"
        ''' <summary>
        ''' This function shall be used to save Update Referral Dataset in session
        ''' </summary>
        ''' <param name="updateReferralDs"></param>
        ''' <remarks></remarks>
        Public Shared Sub setUpdateReferralDs(ByRef updateReferralDs As DataSet)
            Current.Session(SessionManager.UpdateReferralDs) = updateReferralDs
        End Sub

#End Region

#Region "Get Update Referral Dataset"
        ''' <summary>
        ''' This function shall be used to get Update Referral Dataset from session
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getUpdateReferralDs() As DataSet
            If (IsNothing(Current.Session(SessionManager.UpdateReferralDs))) Then
                Return Nothing
            Else
                Return CType(Current.Session.Item(SessionManager.UpdateReferralDs), DataSet)
            End If
        End Function

#End Region

#Region "Remove Update Referral Dataset"
        ''' <summary>
        ''' This function shall be used to remove Update Referral Dataset from session
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeUpdateReferralDs()
            If (Not IsNothing(Current.Session(SessionManager.UpdateReferralDs))) Then
                Current.Session.Remove(SessionManager.UpdateReferralDs)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Update Referral BO"
        ''' <summary>
        ''' This key shall be used to set/get Update Referral BO.
        ''' </summary>
        ''' <remarks></remarks>
        Public Const UpdateReferralBO As String = "UpdateReferralBO"

#Region "Set Update Referral BO"
        ''' <summary>
        ''' This function shall be used to save Update Referral BO in session
        ''' </summary>
        ''' <param name="updateReferralBO"></param>
        ''' <remarks></remarks>
        Public Shared Sub setUpdateReferralBo(ByRef updateReferralBO As UpdateReferralBO)
            Current.Session(SessionManager.UpdateReferralBO) = updateReferralBO
        End Sub

#End Region

#Region "Get UpdateReferralBO"
        ''' <summary>
        ''' This function shall be used to get UpdateReferralBO from session
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getUpdateReferralBO() As UpdateReferralBO
            If (IsNothing(Current.Session(SessionManager.UpdateReferralBO))) Then
                Return Nothing
            Else
                Return CType(Current.Session.Item(SessionManager.UpdateReferralBO), UpdateReferralBO)
            End If
        End Function

#End Region

#Region "Remove UpdateReferralBO"
        ''' <summary>
        ''' This function shall be used to remove UpdateReferralBO from session
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeUpdateReferralBO()
            If (Not IsNothing(Current.Session(SessionManager.UpdateReferralBO))) Then
                Current.Session.Remove(SessionManager.UpdateReferralBO)
            End If
        End Sub

#End Region

#End Region

    End Class
End Namespace