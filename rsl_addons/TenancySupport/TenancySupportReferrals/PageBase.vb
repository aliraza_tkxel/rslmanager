﻿Imports TSR_Utilities
Imports System.Web
Public Class PageBase
    Inherits System.Web.UI.Page

    Public uiMessageHelper As UIMessageHelper = New UIMessageHelper()

    Public pathsData As New Dictionary(Of String, String)

    Sub New()

    End Sub


#Region "Is Session Exist"
    ''' <summary>
    ''' Checks if session is free.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub isSessionExist()

        Dim userId As Integer = SessionManager.getTSRUserId()
        If (userId = 0) Then
            Response.Redirect(PathConstants.LoginPath)
        End If

    End Sub
#End Region

#Region "Session Time out"
    ''' <summary>
    ''' This event handles the page_init event of the base page
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init

        Me.isSessionExist()


    End Sub
#End Region

#Region "OnInit"
    ''' <summary>
    ''' This function is used to destroy browser cache. so that every time page load 'll be called
    ''' </summary>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Overrides Sub OnInit(ByVal e As EventArgs)
        MyBase.OnInit(e)
    End Sub

#End Region



End Class
