﻿Imports TSR_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports Microsoft.Practices.EnterpriseLibrary.Validation.Validators
Imports Microsoft.Practices.EnterpriseLibrary.Validation
Imports TSR_BusinessLogic
Imports TSR_BusinessObject
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Threading
Imports System.Globalization

Public Class UpdateReferral
    Inherits PageBase


#Region "Events"

#Region "Page Load Event"
    ''' <summary>
    ''' Page Load Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            If Not IsPostBack Then

                If (IsNothing(Request.QueryString(PathConstants.RefId)) Or IsNothing(Request.QueryString(PathConstants.ActionId)) Or IsNothing(Request.QueryString(PathConstants.StatusId))) Then
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidQueryString, True)
                Else
                    setQueryStringInViewState()
                    getreferralData()
                    displayPanels()
                End If

            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                'ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Referred Action Drop down Selected Index Event"
    ''' <summary>
    ''' Referred Action Drop down Selected Index Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ddlReferredAction_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlReferredAction.SelectedIndexChanged
        Try
            referredActionChanged()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                'ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Rejected Action Drop down Selected Index Event"
    ''' <summary>
    ''' Rejected Action Drop down Selected Index Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub ddlRejectedAction_SelectedIndexChanged(ByVal sender As Object, ByVal e As EventArgs) Handles ddlRejectedAction.SelectedIndexChanged
        Try
            rejectedActionChanged()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                'ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Btn Save Click"
    ''' <summary>
    ''' Btn Save Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnSaveAssign_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSaveAssign.Click
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            Dim objUpdateReferralBO As UpdateReferralBO = New UpdateReferralBO()

            If (Convert.ToInt32(ddlReferredAction.SelectedValue) = ApplicationConstants.AssignActionId) Then

                If (Convert.ToInt32(ddlAssignTo.SelectedValue) = -1) Then
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SelectAssignee, True)
                Else
                    objUpdateReferralBO.AssignToId = ddlAssignTo.SelectedValue
                    objUpdateReferralBO.Notes = txtBoxAssignNotes.Text
                    objUpdateReferralBO.UpdatedBy = SessionManager.getTSRUserId()
                    objUpdateReferralBO.ActionId = ddlReferredAction.SelectedValue
                    objUpdateReferralBO.ReferralHistoryId = ViewState.Item(ViewStateConstants.RefId)
                    updateReferral(objUpdateReferralBO)
                    'lblMessage.Text = "ActionId:" + objUpdateReferralBO.ActionId + " AssignToID:" + objUpdateReferralBO.AssignToId + " RefHistoryId:" + objUpdateReferralBO.ReferralHistoryId
                    Me.closeWindow()
                    'Me.ReloadWindow()
                End If

            Else

                If (txtReason.Text.ToString.Length = 0) Then
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.EnterReason, True)
                Else
                    objUpdateReferralBO.Reason = txtReason.Text
                    objUpdateReferralBO.UpdatedBy = SessionManager.getTSRUserId()
                    objUpdateReferralBO.ActionId = ddlReferredAction.SelectedValue
                    objUpdateReferralBO.ReferralHistoryId = ViewState.Item(ViewStateConstants.RefId)

                    SessionManager.setUpdateReferralBo(objUpdateReferralBO)
                    mdlPopupConfirmReject.Show()

                End If

            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                'ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Button Reject Save Click"
    ''' <summary>
    ''' Button Reject Save Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnRejectedSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnRejectedSave.Click
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            Dim objUpdateReferralBO As UpdateReferralBO = New UpdateReferralBO()

            If (Convert.ToInt32(ddlRejectedAction.SelectedValue) = ApplicationConstants.AssignActionId) Then

                If (Convert.ToInt32(ddlRejectedAssignTo.SelectedValue) = -1) Then
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SelectAssignee, True)
                Else
                    objUpdateReferralBO.AssignToId = ddlRejectedAssignTo.SelectedValue
                    objUpdateReferralBO.Notes = txtRejectedNotes.Text
                    objUpdateReferralBO.UpdatedBy = SessionManager.getTSRUserId()
                    objUpdateReferralBO.ActionId = ddlRejectedAction.SelectedValue
                    objUpdateReferralBO.ReferralHistoryId = ViewState.Item(ViewStateConstants.RefId)
                    updateReferral(objUpdateReferralBO)
                    'lblMessage.Text = "ActionId:" + objUpdateReferralBO.ActionId + " AssignToID:" + objUpdateReferralBO.AssignToId + " RefHistoryId:" + objUpdateReferralBO.ReferralHistoryId
                    Me.closeWindow()
                    'Me.ReloadWindow()
                End If

            Else

                If (txtRejectedNotes.Text.ToString.Length = 0) Then
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.EnterNotes, True)
                Else
                    objUpdateReferralBO.Notes = txtRejectedNotes.Text
                    objUpdateReferralBO.UpdatedBy = SessionManager.getTSRUserId()
                    objUpdateReferralBO.ActionId = ddlRejectedAction.SelectedValue
                    objUpdateReferralBO.ReferralHistoryId = ViewState.Item(ViewStateConstants.RefId)
                    updateReferral(objUpdateReferralBO)
                    'lblMessage.Text = "ActionId:" + objUpdateReferralBO.ActionId + " AssignToID:" + objUpdateReferralBO.AssignToId + " RefHistoryId:" + objUpdateReferralBO.ReferralHistoryId
                    Me.closeWindow()
                    'Me.ReloadWindow()
                End If

            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                'ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Button Confirm Reject Click"
    ''' <summary>
    ''' Button Confirm Reject Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnConfirm_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnConfirm.Click
        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)
            Dim objUpdateReferralBO As UpdateReferralBO = New UpdateReferralBO()
            objUpdateReferralBO = SessionManager.getUpdateReferralBO()
            updateReferral(objUpdateReferralBO)
            Me.closeWindow()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                'ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#End Region

#Region "Functions"

#Region "Disable controls"
    ''' <summary>
    ''' Disable controls
    ''' </summary>
    ''' <remarks></remarks>
    Sub disableControls()

        ddlReferredAction.Enabled = False
        txtReason.Enabled = False
        ddlAssignTo.Enabled = False
        txtBoxAssignNotes.Enabled = False
        btnSaveAssign.Enabled = False
        btnCancelAssign.Enabled = False
        ddlRejectedAction.Enabled = False
        ddlRejectedAssignTo.Enabled = False
        txtRejectedNotes.Enabled = False
        btnRejectedSave.Enabled = False
        btnRejectedCancel.Enabled = False

    End Sub

#End Region

#Region "Close window"
    ''' <summary>
    ''' Close window
    ''' </summary>
    ''' <remarks></remarks>
    Sub closeWindow()
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "close", "ReloadWindow();closeWindow();", True)
    End Sub

#End Region

#Region "Update Referral"
    ''' <summary>
    ''' Update Referral
    ''' </summary>
    ''' <remarks></remarks>
    Sub updateReferral(ByVal objUpdateReferralBO As UpdateReferralBO)
        Dim objCustomerBL As CustomerBL = New CustomerBL()
        Dim isUpdated As Boolean = False
        isUpdated = objCustomerBL.updateReferral(objUpdateReferralBO)
        If (isUpdated) Then

            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.SuccessfullyUpdated, False)
            'lblMessage.Text = "ActionId:" + objUpdateReferralBO.ActionId + " AssignToID:" + objUpdateReferralBO.AssignToId + " RefHistoryId:" + objUpdateReferralBO.ReferralHistoryId
            disableControls()
        Else
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.FailedUpdateReferral, True)
        End If

    End Sub

#End Region

#Region "Set query string in view state"
    ''' <summary>
    ''' Set query string in view state
    ''' </summary>
    ''' <remarks></remarks>
    Sub setQueryStringInViewState()

        ViewState.Add(ViewStateConstants.RefId, Convert.ToInt32(Request.QueryString(PathConstants.RefId)))
        ViewState.Add(ViewStateConstants.ActionId, Convert.ToInt32(Request.QueryString(PathConstants.ActionId)))
        ViewState.Add(ViewStateConstants.StatusId, Convert.ToInt32(Request.QueryString(PathConstants.StatusId)))

    End Sub

#End Region

#Region "Get referral data"
    ''' <summary>
    ''' Get referral data
    ''' </summary>
    ''' <remarks></remarks>
    Sub getreferralData()

        Dim refHistoryId As Integer = Convert.ToInt32(ViewState.Item(ViewStateConstants.RefId))
        Dim actionId As Integer = Convert.ToInt32(ViewState.Item(ViewStateConstants.ActionId))
        Dim statusId As Integer = Convert.ToInt32(ViewState.Item(ViewStateConstants.StatusId))
        Dim employeeId As Integer = SessionManager.getTSRUserId()

        Dim resultDataset As DataSet = New DataSet()
        Dim objCustomerBL As CustomerBL = New CustomerBL()
        objCustomerBL.getUpdateReferralData(resultDataset, actionId, statusId, employeeId, refHistoryId)
        SessionManager.setUpdateReferralDs(resultDataset)

    End Sub

#End Region

#Region "Display panels"
    ''' <summary>
    ''' Display panels
    ''' </summary>
    ''' <remarks></remarks>
    Sub displayPanels()

        Dim refHistoryId As Integer = Convert.ToInt32(ViewState.Item(ViewStateConstants.RefId))
        Dim actionId As Integer = Convert.ToInt32(ViewState.Item(ViewStateConstants.ActionId))
        Dim statusId As Integer = Convert.ToInt32(ViewState.Item(ViewStateConstants.StatusId))

        If (actionId = ApplicationConstants.ReferredActionId And statusId = ApplicationConstants.PendingStatusId) Then
            displayReferredPanel()
        ElseIf ((actionId = ApplicationConstants.RejectedActionId Or actionId = ApplicationConstants.ClosedActionId) And statusId = ApplicationConstants.CloseStatusId) Then
            displayRejectedPanel()
        ElseIf ((actionId = ApplicationConstants.AssignedActionId Or actionId = ApplicationConstants.NoteActionId) And statusId = ApplicationConstants.OpenStatusId) Then
            displayAssignedPanel()
        Else
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidQueryString, True)
        End If

    End Sub

#End Region

#Region "Display Referred Panel"
    ''' <summary>
    ''' Display Referred Panel
    ''' </summary>
    ''' <remarks></remarks>
    Sub displayReferredPanel()

        Dim resultDataset As DataSet = SessionManager.getUpdateReferralDs()

        If (IsNothing(resultDataset)) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingData, True)
        Else
            Dim actionDt As DataTable = resultDataset.Tables(0)
            Dim tenantInfoDt As DataTable = resultDataset.Tables(1)
            Dim assignToDt As DataTable = resultDataset.Tables(2)

            ddlReferredAction.DataSource = actionDt
            ddlReferredAction.DataValueField = "id"
            ddlReferredAction.DataTextField = "value"
            ddlReferredAction.DataBind()

            If (tenantInfoDt.Rows.Count > 0) Then
                lblName.Text = tenantInfoDt.Rows(0)(ApplicationConstants.TenantNameCol)
                lblAssignAddress.Text = tenantInfoDt.Rows(0)(ApplicationConstants.TenantAddressCol)
            End If

            ddlAssignTo.DataSource = assignToDt
            ddlAssignTo.DataTextField = ApplicationConstants.AssignTextField
            ddlAssignTo.DataValueField = ApplicationConstants.AssignValueField
            ddlAssignTo.DataBind()

            Dim assignItem As ListItem = New ListItem("Please Select", "-1")
            ddlAssignTo.Items.Insert(0, assignItem)

            pnlReferred.Visible = True
            pnlRejected.Visible = False
            referredActionChanged()

        End If


    End Sub

#End Region

#Region "Display Rejected Panel"
    ''' <summary>
    ''' Display Rejected Panel
    ''' </summary>
    ''' <remarks></remarks>
    Sub displayRejectedPanel()

        Dim resultDataset As DataSet = SessionManager.getUpdateReferralDs()

        If (IsNothing(resultDataset)) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemLoadingData, True)
        Else
            Dim actionDt As DataTable = resultDataset.Tables(ApplicationConstants.ActionsDt)
            Dim assignToDt As DataTable = resultDataset.Tables(ApplicationConstants.AssignToDt)

            ddlRejectedAction.DataSource = actionDt
            ddlRejectedAction.DataValueField = "id"
            ddlRejectedAction.DataTextField = "value"
            ddlRejectedAction.DataBind()

            ddlRejectedAssignTo.DataSource = assignToDt
            ddlRejectedAssignTo.DataTextField = ApplicationConstants.AssignTextField
            ddlRejectedAssignTo.DataValueField = ApplicationConstants.AssignValueField
            ddlRejectedAssignTo.DataBind()

            Dim assignItem As ListItem = New ListItem("Please Select", "-1")
            ddlRejectedAssignTo.Items.Insert(0, assignItem)

            pnlReferred.Visible = False
            pnlRejected.Visible = True
            rejectedActionChanged()

        End If

    End Sub

#End Region

#Region "Display Assigned Panel"
    ''' <summary>
    ''' Display Assigned Panel
    ''' </summary>
    ''' <remarks></remarks>
    Sub displayAssignedPanel()
        displayRejectedPanel()
    End Sub

#End Region

#Region "Change Referred Action"
    ''' <summary>
    ''' Change Referred Action
    ''' </summary>
    ''' <remarks></remarks>
    Sub referredActionChanged()

        If (Convert.ToInt32(ddlReferredAction.SelectedValue) = ApplicationConstants.AssignActionId) Then
            pnlReject.Visible = False
            pnlAssign.Visible = True
        ElseIf (Convert.ToInt32(ddlReferredAction.SelectedValue) = ApplicationConstants.RejectActionId) Then
            pnlReject.Visible = True
            pnlAssign.Visible = False
        End If

    End Sub

#End Region

#Region "Rejected Action Changed"
    ''' <summary>
    ''' Rejected Action Changed
    ''' </summary>
    ''' <remarks></remarks>
    Sub rejectedActionChanged()

        If (ddlRejectedAction.SelectedValue = ApplicationConstants.AssignActionId) Then
            ddlRejectedAssignTo.Enabled = True
        Else
            ddlRejectedAssignTo.Enabled = False
        End If


    End Sub

#End Region

#Region "Reload Window"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub ReloadWindow()
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "reload", "ReloadWindow();", True)
    End Sub
#End Region

#End Region


End Class