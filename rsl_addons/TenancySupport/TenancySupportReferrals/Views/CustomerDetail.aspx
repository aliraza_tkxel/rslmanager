﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="CustomerDetail.aspx.vb"
    Inherits="TenancySupportReferrals.CustomerDetail" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title>RSL Manager -- > CustomerDetail</title>
    <link href="../Styles/RSL.css" rel="stylesheet" type="text/css" />
    <link href="../Styles/Site.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div>
         <asp:Label ID="lblMessageStatus" Font-Bold="true"  ForeColor="Red" runat="server" ></asp:Label>
        <asp:Panel runat="server" ID="pnlSectionA">
        <table>
                <th colspan="3">
                    Section A - Customer Details:
                </th>
                <tr style="font-size:9px;">
                    <td>
                        Type(s)<%--<span style="color:Red">*</span>:--%>
                    </td>
                    <td >
                        <asp:CheckBoxList ID="chkListType" runat="server" RepeatDirection="Horizontal">
                            <asp:ListItem Text="Arrears" Value="Arrears"></asp:ListItem>
                            <asp:ListItem Text="Damage" Value="Damage"></asp:ListItem>
                            <asp:ListItem Text="ASB" Value="ASB"></asp:ListItem>
                        </asp:CheckBoxList>
                    </td>
                </tr>
                <tr>
                    <td>
                        Stage<%--<span style="color:Red">*</span>:--%>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlStage" runat="server" CssClass="textbox200 ">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        Are there any safety issues involved?<%--<span style="color:Red">*</span>:--%>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlSafetyIssue" runat="server" CssClass="textbox200">
                            
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        Has the tenant ever had a criminal conviction?<%--<span style="color:Red">*</span>:--%>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlCriminal" runat="server" CssClass="textbox200">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        Is there a history of substance misuse?<%--<span style="color:Red">*</span>:--%>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlSubstanceMisuse" runat="server" CssClass="textbox200">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        Is there a history of self harm/suicide attempts?<%--<span style="color:Red">*</span>:--%>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlSuicide" runat="server" CssClass="textbox200">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        If you have answered Yes to any of the above please give brief details:
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:TextBox runat="server" CssClass="textbox_textArea" TextMode="MultiLine" ID="txtBoxNotes" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        Is the tenant aware of this referral?<%--<span style="color:Red">*</span>:--%>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlTenant" runat="server" CssClass="textbox200">
                         </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="right" colspan="3">
                        <asp:Button ID="btnNext" runat="server" CssClass="RSLButton" 
                            Text="Go to next section &gt;" />
                    </td>
                </tr>
            </table>
         </asp:Panel>
         <asp:Label ID="lblSubmitMessage" Font-Bold="true"  ForeColor="Green" runat="server" ></asp:Label>
        <asp:Panel runat="server" ID="pnlSectionB" Visible="false"  >
        <table>
                <th colspan="2">
                    Section B - How can we help?
                </th>
                <tr >
                <td colspan="2">
                <div style="border-bottom: 1px solid gray; height: 0px\9; clear: both; margin-left: 2px;
                    width: 400px;">
                </div>
                </td>
                </tr>
                <tr>
                    <td style="float: left;">
                        Money issues:
                    </td>
                    <td style="color:Black;">
                        <asp:CheckBoxList ID="chkListMoneyIssues" runat="server">
                        </asp:CheckBoxList>
                    </td>
                </tr>
                <tr >
                <td colspan="2">
                <div style="border-bottom: 1px solid gray; height: 0px\9; clear: both; margin-left: 2px;
                    width: 400px;">
                </div>
                </td>
                </tr>
                <tr>
                    <td style="float: left;">
                        Personal problems:
                    </td>
                    <td>
                        <asp:CheckBoxList ID="chkListPersonalProblem" runat="server" >
                        </asp:CheckBoxList>
                    </td>
                </tr>
                <tr >
                <td colspan="2">
                <div style="border-bottom: 1px solid gray; height: 0px\9; clear: both; margin-left: 2px;
                    width: 400px;">
                </div>
                </td>
                </tr>
                <tr>
                    <td style="float: left;">
                        Work or training:
                    </td>
                    <td>
                        <asp:CheckBoxList ID="chkListWork" runat="server">
                        </asp:CheckBoxList>
                    </td>
                </tr>
                <tr >
                <td colspan="2">
                <div style="border-bottom: 1px solid gray; height: 0px\9; clear: both; margin-left: 2px;
                    width: 400px;">
                </div>
                </td>
                </tr>
                <tr>
                    <td style="float: left;">
                        Practical problems:
                    </td>
                    <td>
                        <asp:CheckBoxList ID="chkListPracticalProblems" runat="server">
                        </asp:CheckBoxList>
                    </td>
                </tr>
                <tr >
                <td colspan="2">
                <div style="border-bottom: 1px solid gray; height: 0px\9; clear: both; margin-left: 2px;
                    width: 400px;">
                </div>
                </td>
                </tr>
                <tr>
                    <td colspan="2">
                        Property neglect (ASB and damage information):
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:TextBox ID="txtBoxNeglect" runat="server" CssClass="textbox_textArea" 
                            TextMode="MultiLine" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        Other issues (e.g. relationships, children, isolated):
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:TextBox ID="txtBoxOther" runat="server" CssClass="textbox_textArea" 
                            TextMode="MultiLine" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        Any further information (e.g. benefit issues, income, debts, amount <br /> of rent 
                        arrears):
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:TextBox ID="txtBoxMisc" runat="server" CssClass="textbox_textArea" 
                            TextMode="MultiLine" />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="right" colspan="2">
                        <asp:Button ID="btnRecordReferral" runat="server" CssClass="RSLButton" 
                            Text="Record referral" />
                    </td>
                </tr>
        </table>
        </asp:Panel>
    </div>
    </form>
            <%--<asp:ModalPopupExtender ID="mdlPopupMessage" runat="server" TargetControlID="btnHidden3"
                PopupControlID="pnlUserMessage" Enabled="true" DropShadow="true" BackgroundCssClass="modalBackground" CancelControlID="btnClose">
            </asp:ModalPopupExtender>
            <asp:Panel ID="pnlUserMessage" CssClass="left" runat="server" BackColor="White" Width="350px"
                Style=" border: 1px solid black; top:200px;
                font-weight: normal; font-size: 13px; position: fixed !important; -bracket-: hack(;
                left: 430px !important; );">
                <br />
                <div >
                    <table style="width: 100%; margin-top:-16px; margin-bottom:20px;">
                        <tr style=" background-color:Gray; height:40px;">
                            <td align="center" >
                                <asp:Label ID="lblMessageStatus" Font-Bold="true"  ForeColor="White" runat="server" ></asp:Label>
                            </td>
                           
                        </tr>

                        <tr>
                            <td align="center">
                                &nbsp
                            </td>
                         
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Label ID="lblUserMessage" runat="server" ></asp:Label>
                            </td>
                         
                        </tr>
                        <tr>
                            <td align="center">
                                &nbsp
                            </td>
                         
                        </tr>
                        <tr>
                            <td align="center"  >
                                <asp:Button ID="btnClose" CssClass="btn" Width="100px" runat="server" Text="Close" />
                            </td>
                         
                        </tr>
                                                
                    </table>
                    
                </div>
            </asp:Panel>--%>


</body>
</html>
