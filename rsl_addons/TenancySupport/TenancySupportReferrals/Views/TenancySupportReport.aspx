﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="TenancySupportReport.aspx.vb"
    MasterPageFile="~/MasterPage/TenancySupportReferral.Master" Inherits="TenancySupportReferrals.TenancySupportReport" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<%@ Register Assembly="PagingGridView" Namespace="Fadrian.Web.Control" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../Scripts/jquery-v1.9.1.js" type="text/javascript"></script>
    <link href="../Styles/Report.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            width: 200px;
        }
        .modalBackground
        {
            background-color: Black;
            filter: alpha(opacity=30);
            opacity: 0.3;
        }
        .btnGridView
        {
            width: 50px;
            background-color: #EEEEEE;
            border: none;
        }
    </style>
    <script language="javascript" type="text/javascript">
        var typingTimer;                //timer identifier
        var doneTypingInterval = 1000;  //time in ms, 5 second for example


        function OpenForm() {
            // javascript: window.open("../Views/TenancyReferralForm.aspx");
            var str_win
            str_win = "../Views/TenancyReferralForm.aspx"
            window.open(str_win, "display", 'width=800,height=800, left=200,top=200, scrollbars=1');
        }

        function TypingInterval() {
            clearTimeout(typingTimer);
            if ($("#<%= txtBoxSearch.ClientID %>").val()) {

                typingTimer = setTimeout(searchTextChanged, doneTypingInterval);
            }
        }
        function searchTextChanged() {
            __doPostBack('<%= txtBoxSearch.ClientID %>', '');
        }



    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <%--<div id="header" style="margin-bottom: 25px;font-family:inherit;width: 100%;height:40px;float:none;	padding:0px;vertical-align:middle;padding-bottom:5px;"> </div>--%>
    <%--<div style="float: left; ">
                <span style="text-align:center;color: white;font-size: large;">Tenancy Support Referral Report</span>
        </div>--%>
    <asp:Button ID="btnBack" runat="server" Text="< Back" UseSubmitBehavior="False" />
    <asp:UpdatePanel ID="updPnlReport" runat="server" UpdateMode="Conditional">
        <Triggers>
            <asp:PostBackTrigger ControlID="btnExportToExcel" />
        </Triggers>
        <ContentTemplate>
            <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                <asp:Label ID="lblMessage" runat="server"></asp:Label>
                <br />
            </asp:Panel>
            <div>
                <table style="margin-bottom: 8px; font-size: 12px;">
                    <tr>
                        <td style="padding-left: 8px;">
                            From:
                        </td>
                        <td>
                            <asp:TextBox ID="txtBoxDateFrom" name="txtDateFrom" runat="server" ToolTip="Date format &quot;DD/MM/YYYY&quot;"></asp:TextBox><asp:Image
                                ID="imgDateFrom" runat="server" src="../Images/calendar.png" /><asp:CalendarExtender
                                    ID="calDateFrom" runat="server" DaysModeTitleFormat="dd/MM/yyyy" PopupButtonID="imgDateFrom"
                                    PopupPosition="Right" TargetControlID="txtBoxDateFrom" TodaysDateFormat="dd/MM/yyyy"
                                    Format="dd/MM/yyyy">
                                </asp:CalendarExtender>
                        </td>
                        <td>
                            To:
                        </td>
                        <td>
                            <asp:TextBox ID="txtBoxDateTo" name="txtDateTo" runat="server" ToolTip="Date format &quot;DD/MM/YYYY&quot;"></asp:TextBox><asp:Image
                                ID="imgDateTo" runat="server" src="../Images/calendar.png" /><asp:CalendarExtender
                                    ID="calDateTo" runat="server" DaysModeTitleFormat="dd/MM/yyyy" PopupButtonID="imgDateTo"
                                    PopupPosition="Right" TargetControlID="txtBoxDateTo" TodaysDateFormat="dd/MM/yyyy"
                                    Format="dd/MM/yyyy">
                                </asp:CalendarExtender>
                        </td>
                        <td>
                            Last Action:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlAction" runat="server" Style="height: 22px; width: 121px;">
                            </asp:DropDownList>
                        </td>
                        <td style="padding-left: 8px;">
                            Status:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlStatus" runat="server" Style="height: 22px; width: 121px;">
                            </asp:DropDownList>
                        </td>
                        <td style="padding-left: 8px;">
                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" />
                        </td>
                        <td style="padding-left: 8px;">
                            <asp:Button ID="btnExportToExcel" runat="server" Text="ExportToExcel" />
                        </td>
                        <td colspan="4" class="style1">
                            <asp:TextBox ID="txtBoxSearch" runat="server" AutoPostBack="false" Style="background-image: url(../Images/glass.png);
                                background-position: right; background-repeat: no-repeat; float: right; outline: none;"
                                class="searchbox" onkeyup="TypingInterval();"></asp:TextBox>
                        </td>
                    </tr>
                </table>
                <div style="border: 1px solid black; font-size: 11px;">
                    <asp:Button ID="btnHidden" runat="server" Text="" Style="display: none;" />
                    <asp:Button ID="btnViewHidden" runat="server" Text="" Style="display: none;" />
                    <cc1:PagingGridView ID="grdReport" runat="server" AllowPaging="false" AllowSorting="false"
                        AllowCustomPaging="true" AutoGenerateColumns="False" BackColor="White" ShowHeaderWhenEmpty="True"
                        CssClass="sort-expression" HeaderStyle-CssClass="gridfaults-header" CellPadding="4"
                        ForeColor="Black" GridLines="None" OrderBy="" Width="100%" Style="border: none;">
                        <Columns>
                            <asp:TemplateField HeaderText="Tenancy:">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdnREFERRALHISTORYID" runat="server" Value='<%# Bind("REFERRALHISTORYID") %>' />
                                    <asp:HiddenField ID="hdnReferredByEmail" runat="server" Value='<%# Bind("Email") %>' />
                                    <asp:HiddenField ID="hdnCustomerId" runat="server" Value='<%# Bind("CustomerId") %>' />
                                    <asp:HiddenField ID="hdnTenancyId" runat="server" Value='<%# Bind("TenancyID") %>' />
                                    <asp:HiddenField ID="hdnJournalId" runat="server" Value='<%# Bind("JournalId") %>' />
                                    <asp:Label ID="lblTenancyId" runat="server" Text='<%# Bind("TenancyID") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" CssClass="gridHeader" />
                                <ItemStyle HorizontalAlign="Left" Width="8%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Tenant:">
                                <ItemTemplate>
                                    <asp:HiddenField ID="hdnTenantName" runat="server" Value='<%# Bind("TenantName") %>' />
                                    <asp:Label ID="lblTenantName" runat="server" Text='<%# Bind("TenantName") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" CssClass="gridHeader" />
                                <ItemStyle HorizontalAlign="Left" Width="8%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Referred:">
                                <ItemTemplate>
                                    <asp:Label ID="lblReferredDate" runat="server" Text='<%# Bind("ReferredDate") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" CssClass="gridHeader" />
                                <ItemStyle HorizontalAlign="Left" Width="8%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="By:">
                                <ItemTemplate>
                                    <asp:Label ID="lblReferredBy" runat="server" Text='<%# Bind("ReferredBy") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" CssClass="gridHeader" />
                                <ItemStyle HorizontalAlign="Left" Width="8%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Assigned:">
                                <ItemTemplate>
                                    <asp:Label ID="lblAssignedDate" runat="server" Text='<%# If(isDBNull(Eval("AssignedDate")), "-",Eval("AssignedDate")) %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" CssClass="gridHeader" />
                                <ItemStyle HorizontalAlign="Left" Width="8%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="To:">
                                <ItemTemplate>
                                    <asp:Label ID="lblAssignedTo" runat="server" Text='<%# If(isDBNull(Eval("AssignedTo")), "-",Eval("AssignedTo")) %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" CssClass="gridHeader" />
                                <ItemStyle HorizontalAlign="Left" Width="8%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Rejected:">
                                <ItemTemplate>
                                    <asp:Label ID="lblRejectedDate" runat="server" Text='<%# If(isDBNull(Eval("RejectedDate")), "-",Eval("RejectedDate")) %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" CssClass="gridHeader" />
                                <ItemStyle HorizontalAlign="Left" Width="8%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Last Action:">
                                <ItemTemplate>
                                    <asp:Label ID="lblLastActionDate" runat="server" Text='<%# Bind("LastActionDate") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" CssClass="gridHeader" />
                                <ItemStyle HorizontalAlign="Left" Width="8%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="By:">
                                <ItemTemplate>
                                    <asp:Label ID="lblLastActionBy" runat="server" Text='<%# Bind("LastActionBy") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" CssClass="gridHeader" />
                                <ItemStyle HorizontalAlign="Left" Width="10%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Status:">
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" CssClass="gridHeader" />
                                <ItemStyle HorizontalAlign="Left" Width="8%" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:Button ButtonType="Button" runat="server" Text="Assign" ID="btnAssign" OnClick="btnAssign_Click"
                                        CssClass="btnGridView" />
                                    <asp:Button ButtonType="Button" runat="server" Text="Reject" ID="btnReject" OnClick="btnReject_Click"
                                        CssClass="btnGridView" />
                                    <asp:Button ButtonType="Button" runat="server" Text="View" ID="btnView" OnClick="btnView_Click"
                                        CssClass="btnGridView" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" Width="15%" />
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" />
                    </cc1:PagingGridView>
                    <%--Pager Template Start--%>
                    <asp:Panel runat="server" ID="pnlPagination" Visible="false" Style="color: Black;
                        background-color: #ffffff; font-weight: normal; vertical-align: middle; border-top: 0px;">
                        <hr />
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td style="width: 77%;">
                                        <table>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <asp:LinkButton ID="lnkbtnPagerFirst" Text="First" runat="server" CommandName="Page"
                                                            CommandArgument="First" />
                                                        &nbsp;
                                                    </td>
                                                    <td>
                                                        &nbsp
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="lnkbtnPagerPrev" Text="Previous" runat="server" CommandName="Page"
                                                            CommandArgument="Prev" />
                                                        &nbsp;
                                                    </td>
                                                    <td>
                                                        &nbsp
                                                    </td>
                                                    <td>
                                                        Records:
                                                        <asp:Label Text="1" runat="server" ID="lblPagerRecordStart" />
                                                        to
                                                        <asp:Label Text="30" runat="server" ID="lblPagerRecordEnd" />
                                                        of
                                                        <asp:Label Text="30" runat="server" ID="lblPagerRecordTotal" />
                                                        &nbsp &nbsp Page:
                                                        <asp:Label Text="1" runat="server" ID="lblPagerCurrentPage" ForeColor="Red" />
                                                        of
                                                        <asp:Label Text="1" runat="server" ID="lblPagerTotalPages" />
                                                    </td>
                                                    <td>
                                                        &nbsp
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="lnkbtnPagerNext" Text="Next" runat="server" CommandName="Page"
                                                            CommandArgument="Next" />
                                                        &nbsp;
                                                    </td>
                                                    <td>
                                                        &nbsp
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="lnkbtnPagerLast" Text="Last" runat="server" CommandName="Page"
                                                            CommandArgument="Last" />
                                                        &nbsp;
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td align="right">
                                        Records per page:
                                        <asp:TextBox ID="txtPageNumber" runat="server" Width="25px" ValidationGroup="pageNumber"
                                            onchange="document.getElementById('txtPageNumberHidden').value = this.value;" />
                                        <asp:Button Text="Go" runat="server" ID="btnGoPageNumber" OnClick="changePageNumber"
                                            ValidationGroup="pageNumber" UseSubmitBehavior="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </asp:Panel>
                    <%--Pager Template End--%>
                    <%-- REJECT POPUP --%>
                    <asp:Button ID="btnHidden2" runat="server" Text="" Style="display: none;" />
                    <asp:ModalPopupExtender ID="mdlPopupReject" runat="server" TargetControlID="btnHidden2"
                        BackgroundCssClass="modalBackground" PopupControlID="pnlReject" Enabled="true"
                        DropShadow="true" CancelControlID="btnCancel">
                    </asp:ModalPopupExtender>
                    <asp:Panel ID="pnlReject" CssClass="left" runat="server" BackColor="White" Style="border: 1px solid black;
                        font-weight: normal; font-size: 13px;">
                        <div class="ref-form">
                            <div class="form-header">
                                <h2>
                                    Reject the selected referral</h2>
                            </div>
                            <div class="form-container">
                                <asp:Panel ID="pnlPopupMessage" runat="server" Visible="false" CssClass="message">
                                    <asp:Label ID="lblPopupMessage" runat="server"></asp:Label>
                                    <br />
                                </asp:Panel>
                                <div class="row">
                                    <div class="aleft field-label">
                                        Tenant name:
                                    </div>
                                    <div class="aleft field-name">
                                        <asp:Label ID="lblTenantName" runat="server" Text=""></asp:Label>
                                    </div>
                                    <div class="clear">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="aleft field-label">
                                        Address:
                                    </div>
                                    <div class="aleft field-name">
                                        <asp:Label ID="lblAddress" runat="server" Text=""></asp:Label>
                                    </div>
                                    <div class="clear">
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="aleft field-label">
                                        Reason:
                                    </div>
                                    <div class="aleft field-name">
                                        <asp:TextBox ID="txtReason" TextMode="MultiLine" Height="150px" runat="server"></asp:TextBox>
                                    </div>
                                    <div class="clear">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="aright">
                                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" />
                                    </div>
                                    <div class="aright">
                                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" />
                                    </div>
                                    <div class="clear">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <%-- CONFIRM REJECT POPUP --%>
                    <asp:Button ID="btnHidden3" runat="server" Text="" Style="display: none;" />
                    <asp:ModalPopupExtender ID="mdlPopupConfirmReject" runat="server" TargetControlID="btnHidden3"
                        PopupControlID="pnlConfirmReject" Enabled="true" DropShadow="true" BackgroundCssClass="modalBackground"
                        CancelControlID="btnConfirmCancel">
                    </asp:ModalPopupExtender>
                    <asp:Panel ID="pnlConfirmReject" CssClass="left" runat="server" BackColor="White"
                        Width="350px" Style="font-weight: normal; font-size: 13px;">
                        <div>
                            <table class="ref-form" style="width: 100%; margin-top: -16px; height: 100px;">
                                <tr>
                                    <td colspan="2">
                                        <div class="form-header">
                                            <h2>
                                                Confirm Reject</h2>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        &nbsp
                                    </td>
                                </tr>
                                <tr class="row">
                                    <td align="center" colspan="2">
                                        <asp:Label ID="lblUserMessage" runat="server" Text="Are you sure you want to Reject and Close the referral?"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="2">
                                        &nbsp
                                    </td>
                                </tr>
                                <tr class="row">
                                    <td colspan="2">
                                        <div class="aright">
                                            <asp:Button ID="btnConfirm" runat="server" Text="Confirm" OnClick="btnConfirm_Click" />
                                        </div>
                                        <div class="aright">
                                            <asp:Button ID="btnConfirmCancel" runat="server" Text="Cancel" />
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>
                    <!-- Modal PopUp Assign -->
                    <asp:Button ID="btnHiddenAssign" runat="server" Text="" Style="display: none;" />
                    <asp:ModalPopupExtender ID="mdlPopUpAssign" runat="server" TargetControlID="btnHiddenAssign"
                        PopupControlID="pnlUserMessage" Enabled="true" DropShadow="False" BackgroundCssClass="modalBackground"
                        CancelControlID="btnCancelAssign">
                    </asp:ModalPopupExtender>
                    <asp:Panel ID="pnlUserMessage" CssClass="ref-form" runat="server" BackColor="White"
                        Style="font-weight: normal; font-size: 13px;">
                        <div class="form-header">
                            <h2>
                                Assign the selected referral</h2>
                        </div>
                        <div class="form-container">
                            <div class="row">
                                <div class="aleft field-label">
                                    Tenant Name:
                                </div>
                                <div class="aleft field-name">
                                    <asp:Label ID="lblName" runat="server"></asp:Label>
                                </div>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="row">
                                <div class="aleft field-label">
                                    Address:
                                </div>
                                <div class="aleft field-name">
                                    <asp:Label ID="lblAssignAddress" runat="server"></asp:Label>
                                </div>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="row ref-assign">
                                <div class="aleft field-label">
                                    Assign to:
                                </div>
                                <div class="aleft field-name" style="border: 2px solid;">
                                    <asp:DropDownList runat="server" ID="ddlAssignTo">
                                    </asp:DropDownList>
                                </div>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="row">
                                <div class="aleft field-label">
                                    Notes:
                                </div>
                                <div class="aleft field-name">
                                    <asp:TextBox runat="server" TextMode="MultiLine" ID="txtBoxAssignNotes" Height="67px"
                                        Width="237px"></asp:TextBox>
                                </div>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="row">
                                <div style="margin-left: 264px;">
                                    <div class="aright">
                                        <asp:Button runat="server" Text="Save" ID="btnSaveAssign" />
                                    </div>
                                    <div class="aright">
                                        <asp:Button runat="server" Text="Cancel" ID="btnCancelAssign" />
                                    </div>
                                </div>
                                <div class="clear">
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <!-- *********-->
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <!-- Modal Popup for Assign -->
</asp:Content>
