﻿Imports TSR_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports Microsoft.Practices.EnterpriseLibrary.Validation.Validators
Imports Microsoft.Practices.EnterpriseLibrary.Validation
Imports TSR_BusinessLogic
Imports TSR_BusinessObject
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.Threading
Imports System.Globalization

Public Class TenancyReferralForm
    Inherits PageBase

#Region "Events"

#Region "Page Load Event"
    ''' <summary>
    ''' Page Load Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            If Not IsPostBack Then
                populateForm()
                ' printForm()
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                'ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Category Repeater Item Data bound Event"
    ''' <summary>
    ''' Category Repeater Item Data bound Event
    ''' </summary>
    ''' <remarks></remarks>
    Sub rptCategory_DataBound(Sender As Object, e As RepeaterItemEventArgs) Handles rptCategory.ItemDataBound
        Try

            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim rptSubcategory As Repeater = DirectCast(e.Item.FindControl("rptSubcategory"), Repeater)
                Dim lblCategoryId As Label = DirectCast(e.Item.FindControl("lblCategoryId"), Label)
                Dim categoryId As Integer = Convert.ToInt32(lblCategoryId.Text)

                Dim resultDataset As DataSet = SessionManager.getTenancyFormDs()

                Dim subCategoryDt As DataTable = resultDataset.Tables(ApplicationConstants.HelpSubCategoriesDt)
                Dim filterExp As String = "CategoryId = " + Convert.ToString(categoryId)
                Dim drarray() As DataRow
                drarray = subCategoryDt.Select(filterExp)
                rptSubcategory.DataSource = drarray.CopyToDataTable()
                rptSubcategory.DataBind()

            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                'ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Sub Category Repeater Item Data bound Event"
    ''' <summary>
    ''' Sub Category Repeater Item Data bound Event
    ''' </summary>
    ''' <remarks></remarks>
    Sub rptSubCategory_DataBound(Sender As Object, e As RepeaterItemEventArgs)
        Try

            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

                Dim chkOption As CheckBox = DirectCast(e.Item.FindControl("chkOption"), CheckBox)
                Dim lblCheckState As Label = DirectCast(e.Item.FindControl("lblCheckState"), Label)
                chkOption.Checked = IIf(Convert.ToInt32(lblCheckState.Text) = 1, True, False)

            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                'ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Stage Repeater Item Data bound Event"
    ''' <summary>
    ''' Stage Repeater Item Data bound Event
    ''' </summary>
    ''' <remarks></remarks>
    Sub rptStage_DataBound(Sender As Object, e As RepeaterItemEventArgs) Handles rptStage.ItemDataBound
        Try

            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then

                Dim chkOption As CheckBox = DirectCast(e.Item.FindControl("chkOption"), CheckBox)
                Dim lblCheckState As Label = DirectCast(e.Item.FindControl("lblCheckState"), Label)
                chkOption.Checked = IIf(Convert.ToInt32(lblCheckState.Text) = 1, True, False)

            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                'ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#End Region

#Region "Function"

#Region "Print Form"
    ''' <summary>
    ''' Print Form
    ''' </summary>
    ''' <remarks></remarks>
    Sub printForm()
        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "print", "printForm();", True)
    End Sub

#End Region

#Region "Populate form"
    ''' <summary>
    ''' Populate form
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateForm()

        Dim objCustomerBL As CustomerBL = New CustomerBL()

        Dim referralHistoryId As Integer = SessionManager.getTenancyReferralHistoryId()

        If (referralHistoryId = 0) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.InvalidReferenceNumber, True)
        Else
            Dim resultDataSet As DataSet = New DataSet()
            objCustomerBL.getTenancySupportFormData(resultDataSet, referralHistoryId)
            SessionManager.setTenancyFormDs(resultDataSet)

            createOptions(resultDataSet)
            createHelpCategories(resultDataSet)
            populateFields(resultDataSet)
        End If
       

    End Sub

#End Region

#Region "Create options"
    ''' <summary>
    ''' Create options
    ''' </summary>
    ''' <remarks></remarks>
    Sub createOptions(ByVal resultDataset As DataSet)

        'Stage
        rptStage.DataSource = resultDataset.Tables(ApplicationConstants.StagesDt).DefaultView
        rptStage.DataBind()

        'Yes/No/Unknown
        rptSafetyIssuesOptions.DataSource = resultDataset.Tables(ApplicationConstants.YesNoUnknownDt).DefaultView
        rptSafetyIssuesOptions.DataBind()

        rptCriminalConviction.DataSource = resultDataset.Tables(ApplicationConstants.YesNoUnknownDt).DefaultView
        rptCriminalConviction.DataBind()

        rptSubstanceMisuse.DataSource = resultDataset.Tables(ApplicationConstants.YesNoUnknownDt).DefaultView
        rptSubstanceMisuse.DataBind()

        rptSuicideAttempts.DataSource = resultDataset.Tables(ApplicationConstants.YesNoUnknownDt).DefaultView
        rptSuicideAttempts.DataBind()

        'Yes/No
        rptTenantAware.DataSource = resultDataset.Tables(ApplicationConstants.YesNoDt).DefaultView
        rptTenantAware.DataBind()


    End Sub

#End Region

#Region "Create Help Categories"
    ''' <summary>
    ''' Create Help Categories
    ''' </summary>
    ''' <param name="resultDataset"></param>
    ''' <remarks></remarks>
    Sub createHelpCategories(ByVal resultDataset As DataSet)

        rptCategory.DataSource = resultDataset.Tables(ApplicationConstants.HelpCategoriesDt).DefaultView
        rptCategory.DataBind()

        lblNeglectCounter.Text = Convert.ToString(resultDataset.Tables(ApplicationConstants.HelpCategoriesDt).Rows.Count + 1)

    End Sub

#End Region

#Region "Populate Fields"
    ''' <summary>
    ''' Populate Fields
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateFields(resultDataset As DataSet)

        Dim formDataDt As DataTable = resultDataset.Tables(ApplicationConstants.FormDataDt)

        If (formDataDt.Rows.Count > 0) Then

            Dim row As DataRow = formDataDt.Rows(0)
            txtReferrer.Text = row(ApplicationConstants.ReferrerCol)
            txtReferralDate.Text = row(ApplicationConstants.RefDateCol)
            txtReferenceNumber.Text = row(ApplicationConstants.TenancyIdCol)
            txtCustomerName.Text = row(ApplicationConstants.CustomerNameCol)
            txtDateOfBirth.Text = row(ApplicationConstants.DateOfBirthCol)
            txtCustomerAddress.Text = row(ApplicationConstants.AddressCol)
            txtTelephoneNumber.Text = row(ApplicationConstants.TelephoneCol)
            txtPreferredContactMethod.Text = row(ApplicationConstants.PreferedContactCol)

            chkArears.Checked = IIf(Convert.ToInt32(row(ApplicationConstants.IsTypeArrearsCol)) = 1, True, False)
            chkAsb.Checked = IIf(Convert.ToInt32(row(ApplicationConstants.IsTypeASBCol)) = 1, True, False)
            chkDamage.Checked = IIf(Convert.ToInt32(row(ApplicationConstants.IsTypeDamageCol)) = 1, True, False)

            'Dim householdDt As DataTable = resultDataset.Tables(ApplicationConstants.HouseholdDt)
            'rptHouseHoldDetail.DataSource = householdDt
            'rptHouseHoldDetail.DataBind()

            Dim agenciesDt As DataTable = resultDataset.Tables(ApplicationConstants.AgenciesDt)
            rptAgencies.DataSource = agenciesDt
            rptAgencies.DataBind()

            Dim safetyIssue As Integer = row(ApplicationConstants.SafetyIssuesCol)
            Dim conviction As Integer = row(ApplicationConstants.ConvictionCol)
            Dim substance As Integer = row(ApplicationConstants.SubstanceMisuseCol)
            Dim selfHarm As Integer = row(ApplicationConstants.SelfHarmCol)

            applyCheck(rptSafetyIssuesOptions, safetyIssue)
            applyCheck(rptCriminalConviction, conviction)
            applyCheck(rptSubstanceMisuse, substance)
            applyCheck(rptSuicideAttempts, selfHarm)

            lblAnsweredYesDetail.Text = row(ApplicationConstants.YesNotesCol)
            txtPropertyNeglect.Text = row(ApplicationConstants.NeglectNotesCol)
            txtOtherIssues.Text = row(ApplicationConstants.OtherNotesCol)
            txtAnyFurtherInformation.Text = row(ApplicationConstants.MiscNotesCol)

            Dim tenantAware As Integer = row(ApplicationConstants.IsTenantAwareCol)
            applyCheck(rptTenantAware, tenantAware)



        Else
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
        End If

    End Sub

#End Region

#Region "Apply check"
    ''' <summary>
    ''' Apply check
    ''' </summary>
    ''' <remarks></remarks>
    Sub applyCheck(ByVal rpt As Repeater, ByVal checkedValue As Integer)

        Dim chkBox As CheckBox
        Dim lblId As Label
        Dim id As Integer
        For Each item In rpt.Items

            chkBox = DirectCast(item.FindControl("chkOption"), CheckBox)
            lblId = DirectCast(item.FindControl("lblId"), Label)
            id = Convert.ToInt32(lblId.Text)

            If (id = checkedValue) Then
                chkBox.Checked = True
            End If

        Next

    End Sub
#End Region

#End Region

End Class