﻿Imports TSR_BusinessLogic
Imports TSR_BusinessObject
Imports TSR_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class CustomerDetail
    Inherits PageBase

#Region "Attributes"
    Dim objCustomerBL As CustomerBL = New CustomerBL()
#End Region


#Region "Events"

#Region "Page load event"
    ''' <summary>
    ''' Page load event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                lblMessageStatus.Text = String.Empty
                lblSubmitMessage.Text = String.Empty
                loadDDLControls()
            End If


        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            'If uiMessageHelper.IsExceptionLogged = False Then
            '    ExceptionPolicy.HandleException(ex, "Exception Policy")
            'End If

        Finally
            If uiMessageHelper.IsError = True Then
                'uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
                showMessagePopup(UserMessageConstants.ErrorStatus, uiMessageHelper.message)
            End If

        End Try



    End Sub
#End Region

#Region "btn next click event"
    ''' <summary>
    ''' btn next click event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnNext_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnNext.Click

        Dim objCustomerDetailBO As CustomerDetailBO = New CustomerDetailBO()
        objCustomerDetailBO.StageId = ddlStage.SelectedItem.Value
        objCustomerDetailBO.SafetyIssue = ddlSafetyIssue.SelectedItem.Value
        objCustomerDetailBO.Conviction = ddlCriminal.SelectedItem.Value
        objCustomerDetailBO.SubStanceMisUse = ddlSubstanceMisuse.SelectedItem.Value
        objCustomerDetailBO.SelfHarm = ddlSuicide.SelectedItem.Value
        objCustomerDetailBO.IsTenantAware = ddlTenant.SelectedItem.Value

        'If chkListType.SelectedItem Is Nothing Then
        '    uiMessageHelper.message = "Please select atleast one type"
        '    showMessagePopup(UserMessageConstants.ErrorStatus, uiMessageHelper.message)
        '    Exit Sub
        'End If
        'If objCustomerDetailBO.StageId = 0 Then
        '    uiMessageHelper.message = "Please select stage."
        '    showMessagePopup(UserMessageConstants.ErrorStatus, uiMessageHelper.message)
        '    Exit Sub
        'End If
        'If objCustomerDetailBO.SafetyIssue = 0 Then
        '    uiMessageHelper.message = "Please select value from safety issue dropdown."
        '    showMessagePopup(UserMessageConstants.ErrorStatus, uiMessageHelper.message)
        '    Exit Sub
        'End If
        'If objCustomerDetailBO.Conviction = 0 Then
        '    uiMessageHelper.message = "Please select value from criminal conviction dropdown."
        '    showMessagePopup(UserMessageConstants.ErrorStatus, uiMessageHelper.message)
        '    Exit Sub
        'End If
        'If objCustomerDetailBO.SubStanceMisUse = 0 Then
        '    uiMessageHelper.message = "Please select value from substance misuse dropdown."
        '    showMessagePopup(UserMessageConstants.ErrorStatus, uiMessageHelper.message)
        '    Exit Sub
        'End If
        'If objCustomerDetailBO.SelfHarm = 0 Then
        '    uiMessageHelper.message = "Please select value from selfharm/suicide dropdown."
        '    showMessagePopup(UserMessageConstants.ErrorStatus, uiMessageHelper.message)
        '    Exit Sub
        'End If
        'If objCustomerDetailBO.IsTenantAware = 0 Then
        '    uiMessageHelper.message = "Please select value from Tenant Aware dropdown."
        '    showMessagePopup(UserMessageConstants.ErrorStatus, uiMessageHelper.message)
        '    Exit Sub
        'End If
        lblMessageStatus.Text = String.Empty
        pnlSectionA.Visible = False
        pnlSectionB.Visible = True

    End Sub
#End Region

#Region "btn record referral click event"
    ''' <summary>
    ''' btn record referral click event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnRecordReferral_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnRecordReferral.Click
        'Dim val As String = ddlSafetyIssue.SelectedItem.Text.ToString()
        Try
            Dim objCustomerDetailBO As CustomerDetailBO = New CustomerDetailBO()

            objCustomerDetailBO.CustomerId = Request.QueryString("CustomerId")
            objCustomerDetailBO.ItemId = Request.QueryString("ItemId")
            objCustomerDetailBO.TenancyId = Request.QueryString("TenancyId")
            objCustomerDetailBO.ItemNatureId = Request.QueryString("ItemNatureId")
            objCustomerDetailBO.Title = Request.QueryString("title")
            objCustomerDetailBO.PropertyId = Request.QueryString("PropertyId").ToString()
            objCustomerDetailBO.UserId = SessionManager.getTSRUserId()
            'objCustomerDetailBO.JournalId = 236  'get the values from session
            objCustomerDetailBO.ItemStatusId = 30 'get the values from session
            objCustomerDetailBO.ItemActionId = 17 'get the values from session


            'If chkListType.SelectedItem Is Nothing Then
            '    uiMessageHelper.message = "Please select atleast one type"
            '    showMessagePopup(UserMessageConstants.ErrorStatus, uiMessageHelper.message)
            '    Exit Sub
            'End If

            If chkListType.Items(0).Selected = True Then
                objCustomerDetailBO.IsTypeArears = True
            End If
            If chkListType.Items(1).Selected = True Then
                objCustomerDetailBO.IsTypeDamage = True
            End If
            If chkListType.Items(2).Selected = True Then
                objCustomerDetailBO.IsTypeASB = True
            End If
            objCustomerDetailBO.StageId = ddlStage.SelectedItem.Value
            objCustomerDetailBO.SafetyIssue = ddlSafetyIssue.SelectedItem.Value
            objCustomerDetailBO.Conviction = ddlCriminal.SelectedItem.Value
            objCustomerDetailBO.SubStanceMisUse = ddlSubstanceMisuse.SelectedItem.Value
            objCustomerDetailBO.SelfHarm = ddlSuicide.SelectedItem.Value

            ' Drop down validations 

            'If objCustomerDetailBO.StageId = 0 Then
            '    uiMessageHelper.message = "Please select stage."
            '    showMessagePopup(UserMessageConstants.ErrorStatus, uiMessageHelper.message)
            '    Exit Sub
            'End If
            'If objCustomerDetailBO.SafetyIssue = 0 Then
            '    uiMessageHelper.message = "Please select value from safety issue dropdown."
            '    showMessagePopup(UserMessageConstants.ErrorStatus, uiMessageHelper.message)
            '    Exit Sub
            'End If
            'If objCustomerDetailBO.Conviction = 0 Then
            '    uiMessageHelper.message = "Please select value from criminal conviction dropdown."
            '    showMessagePopup(UserMessageConstants.ErrorStatus, uiMessageHelper.message)
            '    Exit Sub
            'End If
            'If objCustomerDetailBO.SubStanceMisUse = 0 Then
            '    uiMessageHelper.message = "Please select value from substance misuse dropdown."
            '    showMessagePopup(UserMessageConstants.ErrorStatus, uiMessageHelper.message)
            '    Exit Sub
            'End If
            'If objCustomerDetailBO.SelfHarm = 0 Then
            '    uiMessageHelper.message = "Please select value from selfharm/suicide dropdown."
            '    showMessagePopup(UserMessageConstants.ErrorStatus, uiMessageHelper.message)
            '    Exit Sub
            'End If
            '***************************
            objCustomerDetailBO.Notes = txtBoxNotes.Text
            objCustomerDetailBO.IsTenantAware = ddlTenant.SelectedIndex

            objCustomerDetailBO.NeglectNotes = txtBoxNeglect.Text
            objCustomerDetailBO.OtherNotes = txtBoxOther.Text
            objCustomerDetailBO.MiscNotes = txtBoxMisc.Text

            objCustomerDetailBO.JournalId = objCustomerBL.saveReferralData(objCustomerDetailBO)
            objCustomerBL.saveSubCategory(objCustomerDetailBO.JournalId, chkListMoneyIssues, chkListPersonalProblem, chkListPracticalProblems, chkListWork)
            disableControls()
            lblSubmitMessage.Text = "Referral recorded successfuly."

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                'ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                showMessagePopup(UserMessageConstants.ErrorStatus, uiMessageHelper.message)
            End If


        End Try

    End Sub
#End Region
#End Region

#Region "Functions"

#Region "load DDLControls"

    Private Sub loadDDLControls()


        Dim stagedDataSet As DataSet = New DataSet()
        Dim yesNoUnknownDataSet As DataSet = New DataSet()
        Dim yesNoDataSet As DataSet = New DataSet()
        Dim obj As ListItem = New ListItem("Please Select", "0")


        ' Load ddlStage
        objCustomerBL.getStages(stagedDataSet)
        If (stagedDataSet.Tables(0).Rows.Count > 0) Then
            ddlStage.DataSource = stagedDataSet
            ddlStage.DataValueField = "StageID"
            ddlStage.DataTextField = "Description"
            ddlStage.DataBind()
            ddlStage.Items.Insert(0, obj)
        End If

        'Load ddlSafetyIssue
        objCustomerBL.getYesNoUnknown(yesNoUnknownDataSet)
        If (yesNoUnknownDataSet.Tables(0).Rows.Count > 0) Then
            ddlSafetyIssue.DataSource = yesNoUnknownDataSet
            ddlSafetyIssue.DataValueField = "YesNoID"
            ddlSafetyIssue.DataTextField = "Description"
            ddlSafetyIssue.DataBind()
            ddlSafetyIssue.Items.Insert(0, obj)
        End If

        'Load ddlCriminal
        If (yesNoUnknownDataSet.Tables(0).Rows.Count > 0) Then
            ddlCriminal.DataSource = yesNoUnknownDataSet
            ddlCriminal.DataValueField = "YesNoID"
            ddlCriminal.DataTextField = "Description"
            ddlCriminal.DataBind()
            ddlCriminal.Items.Insert(0, obj)
        End If

        ' Load ddlSubstanceMisuse
        If (yesNoUnknownDataSet.Tables(0).Rows.Count > 0) Then
            ddlSubstanceMisuse.DataSource = yesNoUnknownDataSet
            ddlSubstanceMisuse.DataValueField = "YesNoID"
            ddlSubstanceMisuse.DataTextField = "Description"
            ddlSubstanceMisuse.DataBind()
            ddlSubstanceMisuse.Items.Insert(0, obj)
        End If

        ' Load ddlSuicide
        If (yesNoUnknownDataSet.Tables(0).Rows.Count > 0) Then
            ddlSuicide.DataSource = yesNoUnknownDataSet
            ddlSuicide.DataValueField = "YesNoID"
            ddlSuicide.DataTextField = "Description"
            ddlSuicide.DataBind()
            ddlSuicide.Items.Insert(0, obj)
        End If

        'Load ddlTenant
        objCustomerBL.getYesNo(yesNoDataSet)
        If (yesNoDataSet.Tables(0).Rows.Count > 0) Then
            ddlTenant.DataSource = yesNoDataSet
            ddlTenant.DataValueField = "YesNoID"
            ddlTenant.DataTextField = "Description"
            ddlTenant.DataBind()
            ddlTenant.Items.Insert(0, obj)
        End If

        'Load Money issues DDL
        Dim chkBoxListDataSet As DataSet = New DataSet()
        objCustomerBL.loadCheckBoxList(chkBoxListDataSet, 1)
        chkListMoneyIssues.DataSource = chkBoxListDataSet
        chkListMoneyIssues.DataTextField = "subcategoryDescription"
        chkListMoneyIssues.DataValueField = "subcategoryid"
        chkListMoneyIssues.DataBind()

        'Load Personal problems DDL
        Dim chkBoxListPersonalDataSet As DataSet = New DataSet()
        objCustomerBL.loadCheckBoxList(chkBoxListPersonalDataSet, 2)
        chkListPersonalProblem.DataSource = chkBoxListPersonalDataSet
        chkListPersonalProblem.DataTextField = "subcategoryDescription"
        chkListPersonalProblem.DataValueField = "subcategoryid"
        chkListPersonalProblem.DataBind()

        'Load Work or training DDL
        Dim chkBoxListWorkDataSet As DataSet = New DataSet()
        objCustomerBL.loadCheckBoxList(chkBoxListWorkDataSet, 3)
        chkListWork.DataSource = chkBoxListWorkDataSet
        chkListWork.DataTextField = "subcategoryDescription"
        chkListWork.DataValueField = "subcategoryid"
        chkListWork.DataBind()

        'Load Practical problems DDL
        Dim chkBoxListPracticalDataSet As DataSet = New DataSet()
        objCustomerBL.loadCheckBoxList(chkBoxListPracticalDataSet, 4)
        chkListPracticalProblems.DataSource = chkBoxListPracticalDataSet
        chkListPracticalProblems.DataTextField = "subcategoryDescription"
        chkListPracticalProblems.DataValueField = "subcategoryid"
        chkListPracticalProblems.DataBind()



    End Sub

#End Region


#Region "Show user message popup"
    ''' <summary>
    ''' Show user message popup
    ''' </summary>
    ''' <remarks></remarks>
    Sub showMessagePopup(ByVal status As String, ByVal message As String)

        lblMessageStatus.Text = message
        'lblUserMessage.Text = message
        'mdlPopupMessage.Show()

    End Sub

#End Region

#Region "Disable Controls"

    Private Sub disableControls()

        chkListType.Enabled = False
        ddlStage.Enabled = False
        ddlCriminal.Enabled = False
        ddlSafetyIssue.Enabled = False
        ddlSubstanceMisuse.Enabled = False
        ddlSuicide.Enabled = False
        ddlTenant.Enabled = False
        chkListMoneyIssues.Enabled = False
        chkListPersonalProblem.Enabled = False
        chkListPracticalProblems.Enabled = False
        chkListWork.Enabled = False
        txtBoxNotes.Enabled = False
        txtBoxNeglect.Enabled = False
        txtBoxOther.Enabled = False
        txtBoxMisc.Enabled = False
        chkListType.ForeColor = Drawing.Color.Black
        ddlStage.ForeColor = Drawing.Color.Black
        ddlCriminal.ForeColor = Drawing.Color.Black
        ddlSafetyIssue.ForeColor = Drawing.Color.Black
        ddlSubstanceMisuse.ForeColor = Drawing.Color.Black
        ddlSuicide.ForeColor = Drawing.Color.Black
        ddlTenant.ForeColor = Drawing.Color.Black
        chkListMoneyIssues.ForeColor = Drawing.Color.Black
        chkListPersonalProblem.ForeColor = Drawing.Color.Black
        chkListPracticalProblems.ForeColor = Drawing.Color.Black
        chkListWork.ForeColor = Drawing.Color.Black
        btnRecordReferral.Enabled = False
        'WebControl.DisabledCssClass = "aspNetDisabled"

    End Sub

#End Region

#Region "Enable Controls"

    Private Sub enableControls()
        chkListType.Enabled = True
        ddlStage.Enabled = True
        ddlCriminal.Enabled = True
        ddlSafetyIssue.Enabled = True
        ddlSubstanceMisuse.Enabled = True
        ddlSuicide.Enabled = True
        ddlTenant.Enabled = True
        chkListMoneyIssues.Enabled = True
        chkListPersonalProblem.Enabled = True
        chkListPracticalProblems.Enabled = True
        chkListWork.Enabled = True
        txtBoxNotes.Enabled = True
        txtBoxNeglect.Enabled = True
        txtBoxOther.Enabled = True
        txtBoxMisc.Enabled = True
    End Sub

#End Region

#End Region




End Class