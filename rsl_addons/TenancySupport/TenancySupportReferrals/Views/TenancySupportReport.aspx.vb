﻿Imports TSR_BusinessLogic
Imports TSR_BusinessObject
Imports TSR_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports Microsoft.Practices.EnterpriseLibrary.Validation.Validators
Imports Microsoft.Practices.EnterpriseLibrary.Validation
Imports System.Drawing
Imports System.Text
Imports System.IO

Public Class TenancySupportReport
    Inherits PageBase

#Region "Attributes"

    Public Shared totalRecord As Integer = 20

#End Region

#Region "Events"

#Region "Page Load Event"
    ''' <summary>
    ''' Page Load Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Dim objCustomerBL As CustomerBL = New CustomerBL()
            Dim supportReferralDataSet As DataSet = New DataSet()
            Dim objReportDataBO As ReportDataBO = New ReportDataBO()
            Dim objPageSortBO As PageSortBO = New PageSortBO("", "", 1, totalRecord)

            If Not IsPostBack Then

                If Not ViewState.Item(ViewStateConstants.PageSortBo) Is Nothing Then
                    objPageSortBO = ViewState.Item(ViewStateConstants.PageSortBo)
                End If
                loadDropDowns()

                If Not IsNothing(Request.QueryString(PathConstants.Status)) Then
                    ddlStatus.Items.FindByText(Request.QueryString(PathConstants.Status)).Selected = True
                    If Request.QueryString(PathConstants.Status) = PathConstants.StatusPending Then
                        objReportDataBO.Status = ddlStatus.SelectedValue
                    End If
                End If

                setPageSortBoViewState(objPageSortBO)
                setReportDataBOViewState(objReportDataBO)

                uiMessageHelper.resetMessage(lblMessage, pnlMessage)
                Me.getReportData()
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                'ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                'showMessagePopup(UserMessageConstants.ErrorStatus, uiMessageHelper.message)
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub
#End Region

#Region " Row Data Bound"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub grdReport_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grdReport.RowDataBound
        Try
            If (e.Row.RowType = DataControlRowType.DataRow) Then
                Dim row As GridViewRow = e.Row

                Dim lblStatus As Label = DirectCast(row.FindControl("lblStatus"), Label)
                Dim btnAssign As Button = DirectCast(row.FindControl("btnAssign"), Button)
                Dim btnRejected As Button = DirectCast(row.FindControl("btnReject"), Button)

                If (Not lblStatus.Text.Equals("Pending")) Then
                    btnAssign.Enabled = False
                    btnRejected.Enabled = False
                End If


            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                'ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then

                'showMessagePopup(UserMessageConstants.ErrorStatus, uiMessageHelper.message)
            End If

        End Try
    End Sub
#End Region

#Region "Export To Excel"
    ''' <summary>
    ''' Export to Excel
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnExportToExcel_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnExportToExcel.Click
        Try
            ExportGridToExcel()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                'ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
                'showMessagePopup(UserMessageConstants.ErrorStatus, uiMessageHelper.message)
            End If

        End Try
    End Sub
#End Region

#Region "Btn View Click"
    ''' <summary>
    ''' Btn View Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnView_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim objCustomerBL As CustomerBL = New CustomerBL()
            Dim btnView As Button = DirectCast(sender, Button)
            Dim row As GridViewRow = DirectCast(btnView.NamingContainer, GridViewRow)
            Dim hdnHistoryId As HiddenField = DirectCast(row.FindControl("hdnREFERRALHISTORYID"), HiddenField)
            Dim refHistoryId As Integer = Convert.ToInt32(hdnHistoryId.Value())
            SessionManager.setTenancyReferralHistoryId(refHistoryId)
            OpenWindow()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                'ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then

                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Btn Reject Click"
    ''' <summary>
    ''' Btn Reject Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnReject_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim btnReject As Button = DirectCast(sender, Button)
            Dim objCustomerBL As CustomerBL = New CustomerBL()
            Dim tenantInfoDataSet As DataSet = New DataSet()
            Dim row As GridViewRow = DirectCast(btnReject.NamingContainer, GridViewRow)

            Dim hdnHistoryId As HiddenField = DirectCast(row.FindControl("hdnREFERRALHISTORYID"), HiddenField)
            Dim hdnTenantName As HiddenField = DirectCast(row.FindControl("hdnTenantName"), HiddenField)
            Dim hdnReferredByEmail As HiddenField = DirectCast(row.FindControl("hdnReferredByEmail"), HiddenField)
            Dim lblReferredBy As Label = DirectCast(row.FindControl("lblReferredBy"), Label)
            Dim hdnTenancyId As HiddenField = DirectCast(row.FindControl("hdnTenancyId"), HiddenField)
            Dim hdnCustomerId As HiddenField = DirectCast(row.FindControl("hdnCustomerId"), HiddenField)

            Dim tenantId As Integer = Convert.ToInt32(hdnTenancyId.Value())
            objCustomerBL.getTenantInfo(tenantInfoDataSet, tenantId)
            Dim address As String = String.Empty
            If (tenantInfoDataSet.Tables(0).Rows.Count > 0) Then
                address = tenantInfoDataSet.Tables(0).Rows(0).Item(1).ToString()
            End If
            Dim customerId As Integer = hdnCustomerId.Value()
            Dim tenantName As String = hdnTenantName.Value()
            Dim referredByEmail As String = hdnReferredByEmail.Value()
            Dim referredBy As String = lblReferredBy.Text
            Dim refHistoryId As Integer = Convert.ToInt32(hdnHistoryId.Value())

            Dim objRejectReferralBO As RejectReferralBO = New RejectReferralBO()
            objRejectReferralBO.ReferralHistoryId = refHistoryId
            objRejectReferralBO.TenantName = tenantName
            objRejectReferralBO.Address = address
            objRejectReferralBO.ReferredEmail = referredByEmail
            objRejectReferralBO.CustomerId = customerId
            objRejectReferralBO.ReferredBy = referredBy
            SessionManager.setRejectReferralBo(objRejectReferralBO)

            openRejectPopup()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                'ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Btn Confirm Click"
    ''' <summary>
    ''' Btn Confirm Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnConfirm_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnConfirm.Click
        Try
            confirmReject()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                'ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Btn Save Click"
    ''' <summary>
    ''' Btn Save Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSave.Click
        Try
            uiMessageHelper.resetMessage(lblPopupMessage, pnlPopupMessage)

            Dim objRejectReferralBO As RejectReferralBO = New RejectReferralBO()
            objRejectReferralBO = SessionManager.getRejectReferralBO()

            Dim rejectedBy As Integer = SessionManager.getTSRUserId()
            Dim reasonNotes As String = txtReason.Text

            objRejectReferralBO.RejectedBy = rejectedBy
            objRejectReferralBO.Reason = reasonNotes

            SessionManager.setRejectReferralBo(objRejectReferralBO)

            Dim results As ValidationResults = Validation.Validate(objRejectReferralBO)
            If results.IsValid Then

                mdlPopupReject.Show()
                mdlPopupConfirmReject.Show()

            Else
                Dim message = String.Empty
                For Each result As ValidationResult In results
                    message += result.Message
                    Exit For
                Next
                uiMessageHelper.setMessage(lblPopupMessage, pnlPopupMessage, message, True)
                mdlPopupReject.Show()

            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ' ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "btn SubmitClick"
    ''' <summary>
    ''' Submit Click Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSubmit.Click
        Try
            Dim objCustomerBL As CustomerBL = New CustomerBL()
            Dim totalCount As Integer = 0
            Dim objPageSortBo As PageSortBO = getPageSortBoViewState()
            Dim objReportDataBO As ReportDataBO = getReportDataBOViewState()
            Dim resultDataSet As DataSet = New DataSet()

            ''===============
            ''Validate
            ''===============

            If txtBoxDateFrom.Text <> String.Empty Or txtBoxDateTo.Text <> String.Empty Then
                Dim fromDate As DateTime
                Dim toDate As DateTime
                Dim today = DateTime.Today.Date
                Dim culture = New System.Globalization.CultureInfo("en-GB", True)

                If txtBoxDateFrom.Text.Trim <> String.Empty Then
                    fromDate = DateTime.Parse(txtBoxDateFrom.Text, culture)
                End If

                If txtBoxDateTo.Text.Trim <> String.Empty Then
                    toDate = DateTime.Parse(txtBoxDateTo.Text, culture)
                End If

                If txtBoxDateFrom.Text.Trim <> String.Empty AndAlso txtBoxDateTo.Text.Trim <> String.Empty AndAlso fromDate.Date > toDate.Date Then
                    uiMessageHelper.message = "To date should be greater than From date."
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
                    Exit Sub
                End If
                If fromDate.Date > today Or toDate > today Then
                    uiMessageHelper.message = "From/To date should not be greater than today."
                    uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
                    Exit Sub
                End If
            End If

            objReportDataBO.FromDate = txtBoxDateFrom.Text
            objReportDataBO.ToDate = txtBoxDateTo.Text
            objReportDataBO.LastAction = ddlAction.SelectedItem.Value
            objReportDataBO.Status = ddlStatus.SelectedItem.Value

            objPageSortBo.SearchText = txtBoxSearch.Text.Trim()
            objPageSortBo.PageNumber = 1

            setPageSortBoViewState(objPageSortBo)
            setReportDataBOViewState(objReportDataBO)

            Me.getReportData()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                'ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "btn Assign Click"
    ''' Btn Assign Click
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    Protected Sub btnAssign_Click(ByVal sender As Object, ByVal e As EventArgs)
        Try
            Dim n As Integer = 0
            txtBoxAssignNotes.Text = String.Empty
            Dim objCustomerBL As CustomerBL = New CustomerBL()
            Dim arrAttributes As ArrayList = New ArrayList()
            Dim btnAssign As Button = DirectCast(sender, Button)
            Dim assignToDataSet As DataSet = New DataSet()
            Dim tenantInfoDataSet As DataSet = New DataSet()
            Dim row As GridViewRow = DirectCast(btnAssign.NamingContainer, GridViewRow)
            Dim hdnTenancyId As HiddenField = DirectCast(row.FindControl("hdnTenancyId"), HiddenField)
            Dim hdnCustomerId As HiddenField = DirectCast(row.FindControl("hdnCustomerId"), HiddenField)

            Dim hdnREFERRALHISTORYID As HiddenField = DirectCast(row.FindControl("hdnREFERRALHISTORYID"), HiddenField)
            Dim referralHistoryId As Integer = Convert.ToInt32(hdnREFERRALHISTORYID.Value())
            Dim hdnReferreEmail As HiddenField = DirectCast(row.FindControl("hdnReferredByEmail"), HiddenField)
            Dim ReferrerEmail As String = Convert.ToString(hdnReferreEmail.Value())
            Dim lblReferreName As Label = DirectCast(row.FindControl("lblReferredBy"), Label)
            Dim ReferrerName As String = lblReferreName.Text

            Dim customerId As Integer = Convert.ToInt32(hdnCustomerId.Value())

            ViewState("referralHistoryId") = referralHistoryId
            ViewState("referrerEmail") = ReferrerEmail
            ViewState("referrerName") = ReferrerName
            ViewState("customerId") = customerId

            Dim tenantId As Integer = Convert.ToInt32(hdnTenancyId.Value())

            'get populate the dropdown
            objCustomerBL.getAssignToData(assignToDataSet)
            ddlAssignTo.DataSource = assignToDataSet
            ddlAssignTo.DataValueField = "AssigneeId"
            ddlAssignTo.DataTextField = "AssigneName"
            'For Each a As DataRow In assignToDataSet.Tables(0).Rows
            '    arrAttributes.Add(a("Email").ToString())
            'Next

            'For Each lItem As ListItem In ddlAssignTo.Items
            '    lItem.Attributes.Add("Email", arrAttributes(n).ToString())
            '    n = n + 1
            'Next
            ddlAssignTo.DataBind()
            SessionManager.setAssigneeDs(assignToDataSet)
            'populate tenant info
            objCustomerBL.getTenantInfo(tenantInfoDataSet, tenantId)
            lblName.Text = tenantInfoDataSet.Tables(0).Rows(0).Item(0).ToString()
            ViewState("tenantName") = tenantInfoDataSet.Tables(0).Rows(0).Item(0).ToString()
            lblAssignAddress.Text = tenantInfoDataSet.Tables(0).Rows(0).Item(1).ToString()
            mdlPopUpAssign.Show()
            updPnlReport.Update()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message
            If uiMessageHelper.IsExceptionLogged = False Then
                'ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Btn Save Assign  "
    ''' <summary>
    ''' Save Assign PopUp Values
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnSaveAssign_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnSaveAssign.Click
        Try
            Dim objCustomerBL As CustomerBL = New CustomerBL()
            Dim saveAssignDataSet As DataSet = New DataSet()
            Dim referralHistoryId As Integer = ViewState("referralHistoryId")
            Dim asigneeId As Integer = Convert.ToInt32(ddlAssignTo.SelectedItem.Value)
            Dim assignNotes As String = txtBoxAssignNotes.Text
            Dim assigneeDataSet As DataSet = SessionManager.getAssigneeDs()
            Dim assigneeDT As DataTable = assigneeDataSet.Tables(0)

            Dim query = From emails In assigneeDT
                                  Where emails("AssigneeId") = asigneeId
                                  Select New With {.Email = emails("Email"), .Name = emails("AssigneName")}



            objCustomerBL.saveAssignInfo(referralHistoryId, asigneeId, assignNotes)
            'Send an email to both who perofrm the action and to whom this referral is assigned.
            Dim referrerEmail As String = ViewState("referrerEmail")
            Dim assigneEmail As String = query.First.Email.ToString()
            Dim referrerName As String = ViewState("referrerName")
            Dim assignedBy As String = query.First.Name.ToString()
            Dim customerId As String = ViewState("customerId")
            Dim tenantName As String = ViewState("tenantName")

            'Dim objRejectRefferBO As RejectReferralBO = New RejectReferralBO()
            sendAssignEmail(referrerEmail, assigneEmail, referrerName, assignNotes, assignedBy, customerId, tenantName)
            ' Refresh Report Data
            getReportData()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                'ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region " txtBox Search Text Changed"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub txtBoxSearch_TextChanged(ByVal sender As Object, ByVal e As EventArgs) Handles txtBoxSearch.TextChanged
        Dim searchText As String = txtBoxSearch.Text.Trim()
        Dim objReportDataBO As ReportDataBO = New ReportDataBO()
        Dim objPageSortBO As PageSortBO = New PageSortBO("", "", 1, totalRecord)
        objPageSortBO.SearchText = searchText
        setPageSortBoViewState(objPageSortBO)
        Me.getReportData()



    End Sub

#End Region

#Region "Back button Click"

    Protected Sub btnBack_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnBack.Click
        Response.Redirect("~/../Customer/CustomerSearch.asp", True)
    End Sub

#End Region

#End Region

#Region "Functions"

#Region "Load DropDowns"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub loadDropDowns()
        Dim objCustomerBL As CustomerBL = New CustomerBL()
        Dim reportDataSet As DataSet = New DataSet()
        Dim statusDataSet As DataSet = New DataSet()
        objCustomerBL.reportActions(reportDataSet)
        objCustomerBL.reportStatus(statusDataSet)
        ddlAction.DataSource = reportDataSet
        ddlAction.DataValueField = "ITEMACTIONID"
        ddlAction.DataTextField = "ActionName"
        ddlAction.DataBind()
        ddlAction.Items.Insert(0, New ListItem("All", "-1"))
        ddlStatus.DataSource = statusDataSet
        ddlStatus.DataValueField = "ITEMSTATUSID"
        ddlStatus.DataTextField = "StatusName"
        ddlStatus.DataBind()
        ddlStatus.Items.Insert(0, New ListItem("All", "-1"))
    End Sub

#End Region

#Region "Reset reject popup"
    ''' <summary>
    ''' Reset reject popup
    ''' </summary>
    ''' <remarks></remarks>
    Sub resetRejectPopup()

        uiMessageHelper.resetMessage(lblPopupMessage, pnlPopupMessage)
        lblTenantName.Text = String.Empty
        lblAddress.Text = String.Empty
        txtReason.Text = String.Empty

    End Sub

#End Region

#Region "Open reject popup"
    ''' <summary>
    ''' Open reject popup
    ''' </summary>
    ''' <remarks></remarks>
    Sub openRejectPopup()

        resetRejectPopup()

        Dim objRejectReferralBO As RejectReferralBO = New RejectReferralBO()
        objRejectReferralBO = SessionManager.getRejectReferralBO()

        lblTenantName.Text = objRejectReferralBO.TenantName
        lblAddress.Text = objRejectReferralBO.Address

        mdlPopupReject.Show()

    End Sub

#End Region

#Region "Confirm reject"
    ''' <summary>
    ''' Confirm reject
    ''' </summary>
    ''' <remarks></remarks>
    Sub confirmReject()

        Dim objCustomerBL As CustomerBL = New CustomerBL()
        Dim objRejectReferralBO As RejectReferralBO = New RejectReferralBO()
        objRejectReferralBO = SessionManager.getRejectReferralBO()

        Dim isSaved As Boolean = objCustomerBL.confirmReferralRejection(objRejectReferralBO.ReferralHistoryId, objRejectReferralBO.RejectedBy, objRejectReferralBO.Reason)
        mdlPopupReject.Hide()
        mdlPopupConfirmReject.Hide()

        If (isSaved) Then
            sendEmail()
            mdlPopupReject.Hide()
            mdlPopupConfirmReject.Hide()
            getReportData()
        Else
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ProblemRejectReferral, True)
        End If
    End Sub

#End Region

#Region "Send Email"
    ''' <summary>
    ''' Send Email
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub sendEmail()

        Dim objRejectReferralBO As RejectReferralBO = New RejectReferralBO()
        objRejectReferralBO = SessionManager.getRejectReferralBO()

        Dim referredBy As String = objRejectReferralBO.ReferredBy
        Dim recepientEmail As String = objRejectReferralBO.ReferredEmail
        Dim tenantName As String = objRejectReferralBO.TenantName
        Dim reason As String = objRejectReferralBO.Reason
        Dim customerId As Integer = objRejectReferralBO.CustomerId

        Dim url As String = String.Format(PathConstants.JournalTabTenantRecord, Request.ServerVariables.Item("HTTP_HOST")) + Convert.ToString(customerId)
        Dim body As String = populateBody(url, ApplicationConstants.ReferralOutcome.Rejected)
        Dim recepientName As String = referredBy
        Dim sender As String = ApplicationConstants.emailSender
        Dim subject As String = ApplicationConstants.rejectEmailSubject

        Try
            If GeneralHelper.isEmail(recepientEmail) Then
                EmailHelper.sendHtmlFormattedEmail(recepientName, recepientEmail, subject, body, sender)
            Else
                uiMessageHelper.IsError = True
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ReferralRejectedInvalidEmail, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ReferralRejectedEmailFailed + ex.Message, True)

            If uiMessageHelper.IsExceptionLogged = False Then
                'ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError Then
            End If
        End Try

    End Sub
#End Region

#Region "Populate Email body"
    ''' <summary>
    ''' Populate Email body
    ''' </summary>
    ''' <param name="url"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function populateBody(ByVal url As String, ByVal outcome As ApplicationConstants.ReferralOutcome) As String

        Dim objRejectReferralBO As RejectReferralBO = New RejectReferralBO()
        objRejectReferralBO = SessionManager.getRejectReferralBO()

        Dim body As String = String.Empty
        Dim server = HttpContext.Current.Server
        Dim emailtemplatepath As String = String.Empty

        If outcome = ApplicationConstants.ReferralOutcome.Assigned Then
            emailtemplatepath = server.MapPath("~/Email/AssignReferral.htm")
        Else
            emailtemplatepath = server.MapPath("~/Email/RejectReferral.htm")
        End If

        Dim reader As StreamReader = New StreamReader(emailtemplatepath)
        body = reader.ReadToEnd.ToString

        If outcome = ApplicationConstants.ReferralOutcome.Assigned Then
            body = body.Replace("{ReferredBy}", objRejectReferralBO.AssignedBy)
        Else
            body = body.Replace("{ReferredBy}", objRejectReferralBO.ReferredBy)
        End If

        body = body.Replace("{TenantName}", objRejectReferralBO.TenantName)
        body = body.Replace("{Reason}", objRejectReferralBO.Reason)
        body = body.Replace("{URL}", url)

        Return body
    End Function
#End Region

#Region "Open Window"
    ''' <summary>
    ''' Open Window
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub OpenWindow()

        ScriptManager.RegisterStartupScript(Me.Page, Me.GetType(), "openForm", "OpenForm();", True)

    End Sub
#End Region

#Region "Page Sort BO"

    Protected Sub setPageSortBoViewState(ByRef pageSortBO As PageSortBO)
        ViewState.Item(ViewStateConstants.PageSortBo) = pageSortBO
    End Sub

    Protected Function getPageSortBoViewState()

        Dim objPageSortBo As PageSortBO = New PageSortBO("", "", 1, 10)
        objPageSortBo = ViewState.Item(ViewStateConstants.PageSortBo)
        Dim pageSortBO = objPageSortBo

        If Not IsNothing(ViewState.Item(ViewStateConstants.PageSortBo)) Then
            pageSortBO = ViewState.Item(ViewStateConstants.PageSortBo)
        End If

        Return pageSortBO
    End Function

    Protected Sub removePageSortBoViewState()
        ViewState.Remove(ViewStateConstants.PageSortBo)
    End Sub

#End Region

#Region "Report Data BO"

    Protected Sub setReportDataBOViewState(ByRef objReportDataBO As ReportDataBO)
        ViewState.Item(ViewStateConstants.SearchReportData) = objReportDataBO
    End Sub

    Protected Function getReportDataBOViewState()

        Dim objReportDataBO As ReportDataBO = New ReportDataBO()
        objReportDataBO = ViewState.Item(ViewStateConstants.SearchReportData)
        Dim objSearchReportData = objReportDataBO

        If Not IsNothing(ViewState.Item(ViewStateConstants.SearchReportData)) Then
            objSearchReportData = ViewState.Item(ViewStateConstants.SearchReportData)
        End If

        Return objSearchReportData
    End Function

    Protected Sub removeReportDataBOViewState()
        ViewState.Remove(ViewStateConstants.SearchReportData)
    End Sub

#End Region

#Region "Change Page Number based of given value"
    ''' <summary>
    ''' Change Page Number based of given value
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Sub changePageNumber(ByVal sender As Object, ByVal e As EventArgs) Handles btnGoPageNumber.Click
        Try
            Dim btnGo As Button = DirectCast(sender, Button)
            If txtPageNumber.Text <> String.Empty Then
                Integer.TryParse(txtPageNumber.Text, totalRecord)
                Dim objPageSortBo As PageSortBO = New PageSortBO("", "", 1, totalRecord)
                setPageSortBoViewState(objPageSortBo)
                Me.getReportData()
            Else
                lblMessage.Text = UserMessageConstants.InvalidPageNumber
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                lblMessage.Text = uiMessageHelper.message
                'showMessagePopup(UserMessageConstants.ErrorStatus, uiMessageHelper.message)
            End If
        End Try

    End Sub

#End Region

#Region "Pager LinkButtons Click"
    ''' <summary>
    ''' Pager LinkButtons Click
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>

    Protected Sub lnkbtnPager_Click(ByVal sender As Object, ByVal e As EventArgs) Handles lnkbtnPagerFirst.Click _
, lnkbtnPagerLast.Click, lnkbtnPagerPrev.Click, lnkbtnPagerNext.Click
        Try
            Dim pagebuttton As LinkButton = DirectCast(sender, LinkButton)

            Dim objPageSortBo As PageSortBO = New PageSortBO()
            objPageSortBo = getPageSortBoViewState()

            If pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "First" Then
                objPageSortBo.PageNumber = 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Last" Then
                objPageSortBo.PageNumber = objPageSortBo.TotalPages
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Next" Then
                objPageSortBo.PageNumber += 1
            ElseIf pagebuttton.CommandName = "Page" AndAlso pagebuttton.CommandArgument = "Prev" Then
                objPageSortBo.PageNumber -= 1
            End If

            setPageSortBoViewState(objPageSortBo)
            Me.getReportData()
            'Dim a As EventArgs
            'Dim b As Object
            'Me.Page_Load(b, a)

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                lblMessage.Text = uiMessageHelper.message
                'showMessagePopup(UserMessageConstants.ErrorStatus, uiMessageHelper.message)
            End If
        End Try

    End Sub

#End Region

#Region "Load Report Grid"
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub loadReportGrid(ByRef objPageSortBO As PageSortBO)
        Dim supportReferralDataSet As DataSet = New DataSet()
        Dim objReportDataBO As ReportDataBO = New ReportDataBO()
        Dim objCustomerBL As CustomerBL = New CustomerBL()
        'uiMessageHelper.resetMessage(lblMessage, pnlMessage)
        objCustomerBL.getSupportReferralData(objPageSortBO, objReportDataBO, supportReferralDataSet)
        grdReport.DataSource = supportReferralDataSet
        grdReport.DataBind()
        updPnlReport.Update()
    End Sub


#End Region

#Region "Export Grid To Excel"
    ''' <summary>
    ''' Export Grid To Excel
    ''' </summary>
    ''' <remarks></remarks>
    Sub ExportGridToExcel()

        '=========================================================
        '=== Bing the grid with complete report
        '=========================================================

        uiMessageHelper.resetMessage(lblMessage, pnlMessage)
        Dim objCustomerBL As CustomerBL = New CustomerBL()

        ' Get the report page and sorting information from view state
        Dim objPageSortBo As PageSortBO = New PageSortBO()
        objPageSortBo = getPageSortBoViewState()

        ' Get the report search filters from view state.
        Dim objReportDataBO As ReportDataBO = getReportDataBOViewState()

        ' Set page size to total count of record to get all records for report and set the page number to 1
        ' This change will not effect the report shown on the web page.
        objPageSortBo.PageSize = objPageSortBo.TotalRecords
        objPageSortBo.PageNumber = 1

        ' Get the report from database to export to Excel
        Dim resultDataSet As DataSet = New DataSet()
        objCustomerBL.getSupportReferralData(objPageSortBo, objReportDataBO, resultDataSet)
        grdReport.DataSource = resultDataSet
        grdReport.DataBind()

        '=========================================================
        '=== Export Grid to Excel
        '=========================================================

        Response.Clear()
        Response.Buffer = True
        Dim fileName As String = "TenancySupportReferralReport_" + Convert.ToString(DateTime.Now.Day) + "_" + Convert.ToString(DateTime.Now.Month) + "_" + Convert.ToString(DateTime.Now.Year) + ".xls"

        Response.AddHeader("content-disposition", "attachment;filename=" + fileName)
        Response.Charset = ""
        Response.ContentType = "application/vnd.ms-excel"
        Dim strWrite As New StringWriter()
        Dim htmWrite As New HtmlTextWriter(strWrite)
        Dim htmfrm As New HtmlForm()

        grdReport.Parent.Controls.Add(htmfrm)
        grdReport.Columns(10).Visible = False

        grdReport.HeaderRow.BorderStyle = BorderStyle.None
        htmfrm.Attributes("runat") = "server"
        htmfrm.Controls.Add(grdReport)
        htmfrm.RenderControl(htmWrite)
        'style to format numbers to string
        Dim style As String = "<style> .textmode{mso-number-format:\@;}</style>"
        Response.Write(style)
        Response.Write(strWrite.ToString())
        Response.Flush()
        Response.[End]()
    End Sub

#End Region

#Region "Send Assign Email"
    ''' <summary>
    ''' Sending Assign Email to Referrer and Assignee
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub sendAssignEmail(ByVal referrerEmail As String, ByVal assigneEmail As String, ByVal referrerName As String, ByVal assignNotes As String, ByVal assignedBy As String, ByVal customerId As String, ByVal tenantName As String)

        Dim objRejectReferralBO As RejectReferralBO = New RejectReferralBO()
        If Not SessionManager.getRejectReferralBO() Is Nothing Then
            objRejectReferralBO = SessionManager.getRejectReferralBO()
        End If

        objRejectReferralBO.ReferredBy = referrerName
        objRejectReferralBO.ReferredEmail = referrerEmail
        objRejectReferralBO.Reason = assignNotes
        objRejectReferralBO.AssignedBy = assignedBy
        objRejectReferralBO.TenantName = tenantName

        SessionManager.setRejectReferralBo(objRejectReferralBO)

        Dim url As String = String.Format(PathConstants.JournalTabTenantRecord, Request.ServerVariables.Item("HTTP_HOST")) + customerId
        Dim body As String = populateBody(url, ApplicationConstants.ReferralOutcome.Assigned)
        Dim recepientName As String = referrerName
        Dim sender As String = ApplicationConstants.emailSender
        Dim subject As String = ApplicationConstants.assignEmailSubject

        Try
            If GeneralHelper.isEmail(referrerEmail) Then
                EmailHelper.sendHtmlFormattedEmailWithCC(referrerName, assigneEmail, assigneEmail, subject, body, sender)
            Else
                uiMessageHelper.IsError = True
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ReferralRejectedInvalidEmail, True)
            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.ReferralRejectedEmailFailed + ex.Message, True)

            If uiMessageHelper.IsExceptionLogged = False Then
                'ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError Then
            End If
        End Try
    End Sub
#End Region

#Region "Populate Email body"
    ''' <summary>
    ''' Populate Email body
    ''' </summary>
    ''' <param name="url"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function populateAssignEmailBody(ByVal url As String) As String

        Dim objRejectReferralBO As RejectReferralBO = New RejectReferralBO()
        objRejectReferralBO = SessionManager.getRejectReferralBO()


        Dim body As String = String.Empty
        Dim server = HttpContext.Current.Server
        Dim emailtemplatepath = server.MapPath("~/Email/AssignReferral.htm")

        Dim reader As StreamReader = New StreamReader(emailtemplatepath)
        body = reader.ReadToEnd.ToString
        body = body.Replace("{ReferredBy}", objRejectReferralBO.ReferredBy)
        body = body.Replace("{TenantName}", objRejectReferralBO.TenantName)
        body = body.Replace("{Reason}", objRejectReferralBO.Reason)
        body = body.Replace("{URL}", url)

        Return body
    End Function
#End Region

#Region "Populate Report Data"
    Sub getReportData()
        uiMessageHelper.resetMessage(lblMessage, pnlMessage)
        Dim objCustomerBL As CustomerBL = New CustomerBL()
        Dim totalCount As Integer = 0
        Dim objPageSortBo As PageSortBO = New PageSortBO()
        objPageSortBo = getPageSortBoViewState()
        Dim objReportDataBO As ReportDataBO = getReportDataBOViewState()
        Dim resultDataSet As DataSet = New DataSet()

        totalCount = objCustomerBL.getSupportReferralData(objPageSortBo, objReportDataBO, resultDataSet)
        grdReport.DataSource = resultDataSet
        grdReport.DataBind()

        If (resultDataSet.Tables(0).Rows.Count() > 0) Then
            btnExportToExcel.Enabled = True
            pnlPagination.Visible = True
            objPageSortBo.TotalRecords = totalCount
            objPageSortBo.TotalPages = Math.Ceiling(totalCount / objPageSortBo.PageSize)
            setPageSortBoViewState(objPageSortBo)
            GeneralHelper.setGridViewPager(pnlPagination, objPageSortBo)
        Else
            btnExportToExcel.Enabled = False
            pnlPagination.Visible = False
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
        End If
        updPnlReport.Update()
    End Sub

#End Region

#End Region

End Class