﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="TenancyReferralForm.aspx.vb"
    Inherits="TenancySupportReferrals.TenancyReferralForm" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>


    <style type="text/css">
        .logoDiv
        {
            text-align: right;
        }
        
        .heading
        {
            text-align: center;
        }
        
        .subHeading
        {
            text-align: center;
            background-color: #cdcdcd;
            height: 20px;
        }
        
        .subHeading h4
        {
            margin: 0;
        }
        
        
        
        @media print
        {
            .subHeading
            {
                background-color: #cdcdcd !important;
            }
            .pageBreak
            {
                page-break-after: always;
            }
        
        
        }
        
        td
        {
            max-width: 50%;
            width: 50%;
            text-align: left;
            padding: 5px;
        }
        
        .tsr-wrap .in-panels
        {
            border: 1px solid #ccc;
            width: 100%;
            padding: 5px;
        }
        
        .tsr-wrap
        {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 13px;
        }
        
        .tsr-wrap .txtfield
        {
            width: 100%;
            border: 1px solid #aaa;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 12px;
            padding: 5px 0;
            padding-left: 5px;
            background-color: White;
        }
        
        .tsr-wrap .slct
        {
            padding: 5px;
            width: 100%;
            border: 1px solid #aaa;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 12px;
        }
        
        .option
        {
            margin-left: 50px;
            width: 250px;
        }
        
       
        .tsr-wrap .btn
        {
            width: 100%;
            border: 1px solid #aaa;
            border-radius: 5px;
            background-color: #fff;
            padding: 5px;
        }
    </style>

     <script language="javascript" type="text/javascript">

            function printForm() {
                window.print();
            }

     </script>

</head>
<body>
    <form id="form1" runat="server">
    <asp:Panel ID="pnlMessage" runat="server" Visible="false">
        <asp:Label ID="lblMessage" Font-Size="14px" runat="server"></asp:Label>
        <br />
    </asp:Panel>
    <div class="tsr-wrap">
        <div class="logoDiv">
            <img src="../Images/broadland.png" width="106" height="92" alt="logo" />
        </div>
        <div class="heading">
            <h1>
                Tenancy support referral form
            </h1>
        </div>
        <table style="width: 100%;">
            <tr>
                <td>
                    <b>Referrer:</b>
                </td>
                <td>
                    <asp:TextBox CssClass="txtfield" ID="txtReferrer" Enabled="false" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Date:</b>
                </td>
                <td>
                    <asp:TextBox CssClass="txtfield" ID="txtReferralDate" Enabled="false" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp
                </td>
                <td>
                    &nbsp
                </td>
            </tr>
            <tr style="height: 20px !important;">
                <td colspan="2" class="subHeading">
                    <h4>
                        SECTION A - CUSTOMER DETAILS</h4>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                </td>
            </tr>
            <tr>
                <td>
                    <b>Tenancy reference number:</b>
                </td>
                <td>
                    <asp:TextBox CssClass="txtfield" Enabled="false" ID="txtReferenceNumber" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Name:</b>
                </td>
                <td>
                    <asp:TextBox CssClass="txtfield" ID="txtCustomerName" Enabled="false" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Date of birth:</b>
                </td>
                <td>
                    <asp:TextBox CssClass="txtfield" ID="txtDateOfBirth" Enabled="false" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <b>Address:</b>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:TextBox CssClass="txtfield" Height="60px" ReadOnly="true" ID="txtCustomerAddress"
                        TextMode="MultiLine" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <b>Telephone number:</b>
                </td>
                <td>
                    <asp:TextBox CssClass="txtfield" ID="txtTelephoneNumber" Enabled="false" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <b>Preferred contact method e.g. Phone/letter/email:</b>
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:TextBox CssClass="txtfield" ID="txtPreferredContactMethod" Enabled="false" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <b>Type:</b>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <span class="option">
                        <asp:Label runat="server" Font-Italic="true" Text="Arrears"></asp:Label>
                        <asp:Label runat="server" Text="............................."></asp:Label>
                        <asp:CheckBox ID="chkArears" runat="server" Enabled="false" />
                    </span><span class="option">
                        <asp:Label runat="server" Font-Italic="true" Text="Damage"></asp:Label>
                        <asp:Label runat="server" Text="............................"></asp:Label>
                        <asp:CheckBox ID="chkDamage" runat="server" Enabled="false" />
                    </span><span class="option">
                        <asp:Label runat="server" Font-Italic="true" Text="ASB"></asp:Label>
                        <asp:Label runat="server" Text="............................."></asp:Label>
                        <asp:CheckBox ID="chkAsb" runat="server" Enabled="false" />
                    </span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <b>Stage:</b>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Repeater ID="rptStage" runat="server">
                        <ItemTemplate>
                            <span class="option">
                                <asp:Label ID="lblOption" Font-Italic="true" runat="server" Text='<%# Bind("value") %>'></asp:Label>
                                <asp:Label ID="lblCheckState" Visible="false" runat="server" Text='<%# Bind("IsChecked") %>'></asp:Label>
                                <asp:Label ID="Label3" runat="server" Text="............................"></asp:Label>
                                <asp:CheckBox ID="chkOption" runat="server" Enabled="false" />
                            </span>
                        </ItemTemplate>
                    </asp:Repeater>
                </td>
            </tr>
        </table>
        <div class="pageBreak">
            &nbsp
        </div>
        <table style="width: 100%;">
            <tr>
                <td colspan="2">
                    <b>Details of household e.g. partner, children:</b>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="in-panels" style="height: 70px; overflow: auto;">
                        <asp:Repeater ID="rptHouseHoldDetail" runat="server">
                            <ItemTemplate>
                                <li>
                                    <asp:Label runat="server" Text="Customer Ref:"></asp:Label>
                                    <asp:Label ID="lblCustomerRefNumber" runat="server" Text='<%# Bind("CustomerRefNumber") %>'></asp:Label>
                                    <asp:Label runat="server" Text=" : "></asp:Label>
                                    <asp:Label ID="lblCustomerName" runat="server" Text='<%# Bind("CustomerName") %>'></asp:Label>
                                    <asp:Label runat="server" Text=" : "></asp:Label>
                                    <asp:Label ID="lblRelation" runat="server" Text='<%# Bind("relation") %>'></asp:Label>
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <b>Are there any other agencies involved? If so, please give name and contact details:</b>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="in-panels" style="height: 70px; overflow: auto;">
                        <asp:Repeater ID="rptAgencies" runat="server">
                            <ItemTemplate>
                                <li>
                                    <asp:Label ID="lblSupportAgency" runat="server" Text='<%# Bind("SUPPORTAGENCY") %>'></asp:Label>
                                </li>
                            </ItemTemplate>                            
                        </asp:Repeater>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <b>Are there any safety issues involved?</b>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Repeater ID="rptSafetyIssuesOptions" runat="server">
                        <ItemTemplate>
                            <span class="option">
                                <asp:Label ID="lblOption" Font-Italic="true" runat="server" Text='<%# Bind("value") %>'></asp:Label>
                                <asp:Label ID="lblId" runat="server" Visible="false" Text='<%# Bind("id") %>'></asp:Label>
                                <asp:Label ID="Label3" runat="server" Text="..............................."></asp:Label>
                                <asp:CheckBox ID="chkOption" Enabled="false" runat="server" />
                            </span>
                        </ItemTemplate>
                    </asp:Repeater>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <b>Has the tenant ever had a criminal conviction?</b>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Repeater ID="rptCriminalConviction" runat="server">
                        <ItemTemplate>
                            <span class="option">
                                <asp:Label ID="lblOption" Font-Italic="true" runat="server" Text='<%# Bind("value") %>'></asp:Label>
                                <asp:Label ID="lblId" runat="server" Visible="false" Text='<%# Bind("id") %>'></asp:Label>
                                <asp:Label ID="Label3" runat="server" Text="..............................."></asp:Label>
                                <asp:CheckBox Enabled="false" ID="chkOption" runat="server" />
                            </span>
                        </ItemTemplate>
                    </asp:Repeater>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <b>Is there a history of substance misuse?</b>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Repeater ID="rptSubstanceMisuse" runat="server">
                        <ItemTemplate>
                            <span class="option">
                                <asp:Label ID="lblOption" Font-Italic="true" runat="server" Text='<%# Bind("value") %>'></asp:Label>
                                <asp:Label ID="lblId" runat="server" Visible="false" Text='<%# Bind("id") %>'></asp:Label>
                                <asp:Label ID="Label3" runat="server" Text="..............................."></asp:Label>
                                <asp:CheckBox ID="chkOption" Enabled="false" runat="server" />
                            </span>
                        </ItemTemplate>
                    </asp:Repeater>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <b>Is there a history of self-harm/suicide attempts?</b>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Repeater ID="rptSuicideAttempts" runat="server">
                        <ItemTemplate>
                            <span class="option">
                                <asp:Label ID="lblOption" Font-Italic="true" runat="server" Text='<%# Bind("value") %>'></asp:Label>
                                <asp:Label ID="lblId" runat="server" Visible="false" Text='<%# Bind("id") %>'></asp:Label>
                                <asp:Label ID="Label3" runat="server" Text="..............................."></asp:Label>
                                <asp:CheckBox ID="chkOption" Enabled="false" runat="server" />
                            </span>
                        </ItemTemplate>
                    </asp:Repeater>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <b>If you have answered yes to any of the above,please give brief details :</b>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="in-panels" style="height: 70px; overflow: auto;">
                        <asp:Label ID="lblAnsweredYesDetail" runat="server" Text=""></asp:Label>
                    </div>
                </td>
            </tr>
        </table>
        <div class="pageBreak">
            &nbsp
        </div>
        <table style="width: 100%;">
            <tr style="height: 20px !important;">
                <td colspan="2" class="subHeading">
                    <h4>
                        SECTION B - HOW CAN WE HELP?</h4>
                </td>
            </tr>
            <asp:Repeater ID="rptCategory" runat="server">
                <ItemTemplate>
                    <tr>
                        <td style="width: 5%;">
                            <b>
                                <asp:Label ID="lblCounter" runat="server" Text='<%# Bind("Row") %>'></asp:Label>.</b>
                        </td>
                        <td>
                            <b>
                                <asp:Label ID="lblCategoryId" Visible="false" runat="server" Text='<%# Bind("id") %>'></asp:Label>
                                <asp:Label ID="lblCategory" runat="server" Text='<%# Bind("value") %>'></asp:Label>:</b>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 5%;">
                            <b></b>
                        </td>
                        <td>
                            <asp:Repeater ID="rptSubcategory" runat="server" OnItemDataBound="rptSubCategory_DataBound">
                                <ItemTemplate>
                                    <span class="option"><span style="display: inline-block; width:130px;margin-top:20px;"  >
                                        <asp:Label ID="lblOption" runat="server" Font-Italic="true" Text='<%# Bind("Description") %>'></asp:Label>
                                        <asp:Label ID="lblCheckState" runat="server" Visible="false" Text='<%# Bind("IsChecked") %>'></asp:Label>
                                        <asp:Label ID="Label3" runat="server" Text="........."></asp:Label>
                                    </span>
                                        <asp:CheckBox ID="chkOption" Enabled="false" runat="server" />
                                    </span>
                                </ItemTemplate>
                            </asp:Repeater>
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
            <tr>
                <td style="width: 5%;">
                    <b>
                        <asp:Label ID="lblNeglectCounter" runat="server" Text=""></asp:Label>.</b>
                </td>
                <td>
                    <b>Property neglect (ASP and damage information):</b>
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                    <b></b>
                </td>
                <td colspan="2">
                    <asp:TextBox CssClass="txtfield" TextMode="MultiLine" Height="50px" ReadOnly="true"
                        ID="txtPropertyNeglect" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                    <b></b>
                </td>
                <td>
                    <b>Other issues e.g. relationships, children, isolated:</b>
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                    <b></b>
                </td>
                <td colspan="2">
                    <asp:TextBox CssClass="txtfield" TextMode="MultiLine" Height="50px" ID="txtOtherIssues"
                        ReadOnly="true" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                    <b></b>
                </td>
                <td>
                    <b>Any further information e.g. benefit issues, income, debts, amount of rent arrears:</b>
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                    <b></b>
                </td>
                <td colspan="2">
                    <asp:TextBox CssClass="txtfield" TextMode="MultiLine" Height="70px" ReadOnly="true"
                        ID="txtAnyFurtherInformation" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                    <b></b>
                </td>
                <td>
                    <b>Is tenant aware of this referral?</b>
                </td>
            </tr>
            <tr>
                <td style="width: 5%;">
                    <b></b>
                </td>
                <td>
                    <asp:Repeater ID="rptTenantAware" runat="server">
                        <ItemTemplate>
                            <span class="option">
                                <asp:Label ID="lblOption" runat="server" Font-Italic="true" Text='<%# Bind("value") %>'></asp:Label>
                                <asp:Label ID="lblId" runat="server" Visible="false" Text='<%# Bind("id") %>'></asp:Label>
                                <asp:Label ID="Label3" runat="server" Text="......................................................."></asp:Label>
                                <asp:CheckBox BackColor="White" ID="chkOption" Enabled="false" runat="server" />
                            </span>
                        </ItemTemplate>
                    </asp:Repeater>
                </td>
            </tr>
        </table>
    </div>
    </form>
</body>
</html>
