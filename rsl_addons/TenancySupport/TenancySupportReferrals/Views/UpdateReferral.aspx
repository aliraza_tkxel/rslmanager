﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPage/Blank.Master"
    CodeBehind="UpdateReferral.aspx.vb" Inherits="TenancySupportReferrals.UpdateReferral" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="../Styles/Report.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        
    </style>
    <script type="text/javascript" language="javascript">

        function closeWindow() {
            var browserName = navigator.appName;
            var browserVer = parseInt(navigator.appVersion);

            if (browserName == "Microsoft Internet Explorer") {
                var ie7 = (document.all && !window.opera && window.XMLHttpRequest) ? true : false;
                if (ie7) {
                    //This method is required to close a window without any prompt for IE7 & greater versions.
                    window.open('', '_parent', '');
                    window.close();
                }
                else {
                    //This method is required to close a window without any prompt for IE6
                    this.focus();
                    self.opener = this;
                    self.close();
                }
            } else {
                //For NON-IE Browsers except Firefox which doesnt support Auto Close
                try {
                    this.focus();
                    self.opener = this;
                    self.close();
                }
                catch (e) {

                }

                try {
                    window.open('', '_self', '');
                    window.close();
                }
                catch (e) {

                }
            }
            return false;
        }
        function ReloadWindow() {
            window.opener.location.reload();
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="updPanelUpdateReferral" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Panel ID="pnlUserMessage" CssClass="ref-form" runat="server" BackColor="White"
                Style="font-weight: normal; font-size: 13px;">
                <div class="form-header">
                    <h2>
                        Update the selected referral</h2>
                </div>
                <div style="margin-left: 20px;">
                    <asp:Panel ID="pnlMessage" runat="server" Visible="false">
                        <asp:Label ID="lblMessage" Font-Size="14px" runat="server"></asp:Label>
                        <br />
                    </asp:Panel>
                </div>
                <%-------- Referred Case --------%>
                <asp:Panel ID="pnlReferred" runat="server" Visible="false">
                    <div class="form-container">
                        <div class="row ref-assign">
                            <div class="aleft field-label">
                                Action:
                            </div>
                            <div class="aleft field-name">
                                <asp:DropDownList runat="server" ID="ddlReferredAction" CssClass="slct" AutoPostBack="true"
                                    Height="30px">
                                </asp:DropDownList>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="row">
                            <div class="aleft field-label">
                                Tenant Name:
                            </div>
                            <div class="aleft field-name">
                                <asp:Label ID="lblName" runat="server"></asp:Label>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="row">
                            <div class="aleft field-label">
                                Address:
                            </div>
                            <div class="aleft field-name">
                                <asp:Label ID="lblAssignAddress" runat="server"></asp:Label>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <asp:Panel ID="pnlReject" runat="server" Visible="false">
                            <div class="row">
                                <div class="aleft field-label">
                                    Reason:
                                </div>
                                <div class="aleft field-name">
                                </div>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="row">
                                <div class="aleft field-name">
                                    <asp:TextBox runat="server" ID="txtReason" TextMode="MultiLine" Height="85px"></asp:TextBox>
                                </div>
                                <div class="clear">
                                </div>
                            </div>
                        </asp:Panel>
                        <asp:Panel ID="pnlAssign" runat="server" Visible="false">
                            <div class="row">
                                <div class="aleft field-label">
                                    Assign to:
                                </div>
                                <div class="aleft field-name">
                                    <asp:DropDownList runat="server" CssClass="slct" ID="ddlAssignTo">
                                    </asp:DropDownList>
                                </div>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="row">
                                <div class="aleft field-label">
                                    Notes:
                                </div>
                                <div class="aleft field-name">
                                </div>
                                <div class="clear">
                                </div>
                            </div>
                            <div class="row">
                                <div class="aleft field-name">
                                    <asp:TextBox runat="server" ID="txtBoxAssignNotes" TextMode="MultiLine" Height="85px"></asp:TextBox>
                                </div>
                                <div class="clear">
                                </div>
                            </div>
                        </asp:Panel>
                        <div class="row">
                            <div class="aright">
                                <asp:Button runat="server" Text="Save" ID="btnSaveAssign" />
                            </div>
                            <div class="aright">
                                <asp:Button runat="server" Text="Cancel" OnClientClick="closeWindow()" ID="btnCancelAssign" />
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <%-------------------------------%>
                <%-- Referred , Assigned , Rejected Closed Case  --%>
                <asp:Panel ID="pnlRejected" runat="server" Visible="false">
                    <div class="form-container">
                        <div class="row ref-assign">
                            <div class="aleft field-label">
                                Action:
                            </div>
                            <div class="aleft field-name">
                                <asp:DropDownList runat="server" CssClass="slct" ID="ddlRejectedAction" Height="30px">
                                </asp:DropDownList>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="row ref-assign">
                            <div class="aleft field-label">
                                Assign to:
                            </div>
                            <div class="aleft field-name">
                                <asp:DropDownList runat="server" CssClass="slct" ID="ddlRejectedAssignTo" Height="30px">
                                </asp:DropDownList>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="row">
                            <div class="aleft field-label">
                                Notes:
                            </div>
                            <div class="aleft field-name">
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="row">
                            <div class="aleft field-name">
                                <asp:TextBox runat="server" ID="txtRejectedNotes" TextMode="MultiLine" Height="85px"></asp:TextBox>
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="row">
                            <div class="aright">
                                <asp:Button runat="server" Text="Save" ID="btnRejectedSave" />
                            </div>
                            <div class="aright">
                                <asp:Button runat="server" Text="Cancel" OnClientClick="closeWindow()" ID="btnRejectedCancel" />
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                    </div>
                </asp:Panel>
                <%-------------------------------%>
                <%-- CONFIRM REJECT POPUP --%>
                <asp:Button ID="btnHidden3" runat="server" Text="" Style="display: none;" />
                <asp:ModalPopupExtender ID="mdlPopupConfirmReject" runat="server" TargetControlID="btnHidden3"
                    PopupControlID="pnlConfirmReject" Enabled="true" DropShadow="true" BackgroundCssClass="modalBackground"
                    CancelControlID="btnConfirmCancel">
                </asp:ModalPopupExtender>
                <asp:Panel ID="pnlConfirmReject" CssClass="left" runat="server" BackColor="White"
                    Width="350px" Style="font-weight: normal; font-size: 13px;">
                    <div>
                        <table class="ref-form" style="width: 100%; margin-top: -16px; height: 100px;">
                            <tr>
                                <td colspan="2">
                                    <div class="form-header">
                                        <h2>
                                            Confirm Reject</h2>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2">
                                    &nbsp
                                </td>
                            </tr>
                            <tr class="row">
                                <td align="center" colspan="2">
                                    <asp:Label ID="lblUserMessage" runat="server" Text="Are you sure you want to Reject and Close the referral?"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2">
                                    &nbsp
                                </td>
                            </tr>
                            <tr class="row">
                                <td colspan="2">
                                    <div class="aright">
                                        <asp:Button ID="btnConfirm" runat="server" Text="Confirm" OnClick="btnConfirm_Click" />
                                    </div>
                                    <div class="aright">
                                        <asp:Button ID="btnConfirmCancel" runat="server" Text="Cancel" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </asp:Panel>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
