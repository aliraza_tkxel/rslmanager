﻿Imports System
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports TSR_BusinessLogic
Imports TSR_Utilities
Imports TSR_BusinessObject

Public Class Bridge
    Inherits MSDN.SessionPage
    'Inherits System.Web.UI.Page

    Public uiMessageHelper As UIMessageHelper = New UIMessageHelper()
    Dim customerId As Integer = 0
    Dim tenancyId As Integer = 0
    Dim ItemId As Integer = 0
    Dim ItemNatureId As Integer = 0
    Dim pageToRedirect As String = String.Empty
    Dim propertyId As String = String.Empty
    Dim classicUserId As Integer = 0
    Dim isReportView As Boolean = False
    Dim referralHistoryId As Integer = 0
    Dim queryString As String = String.Empty

#Region "Events"

#Region "Page Load"
    ''' <summary>
    ''' Event fires on page load.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Try
            Me.loadUser()

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#End Region

#Region "Functions"

#Region "Load User"
    ''' <summary>
    ''' Load User
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub loadUser()

        Dim objUserBo As UserBO = New UserBO()
        Dim objUserBl As UserBL = New UserBL()
        Dim referralHistoryId As Integer
        Dim resultDataSet As DataSet = New DataSet()

        Me.getQueryStringValues()
        Me.checkClassicAspSession()

        resultDataSet = objUserBl.getEmployeeById(classicUserId)

        If (resultDataSet.Tables(0).Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.UserDoesNotExist, True)
        Else
            Dim isActive As String = resultDataSet.Tables(0).Rows(0).Item("IsActive").ToString()

            If isActive = False Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.UsersAccountDeactivated, True)
            Else
                setUserSession(resultDataSet)
                'Response.Redirect(PathConstants.TenancySupportReport, True)

                queryString = getQueryStringForRedirect()

                If Request.QueryString(PathConstants.Page) = ApplicationConstants.reportName Then
                    Response.Redirect(PathConstants.TenancySupportReport + IIf(queryString <> String.Empty, "?" + queryString, ""), True)
                ElseIf (Request.QueryString(PathConstants.Page) = PathConstants.updateRefferalPage) Then
                    Response.Redirect(PathConstants.UpdateReferral + IIf(queryString <> String.Empty, "?" + queryString, ""), True)
                ElseIf Request.QueryString(PathConstants.Page) = ApplicationConstants.refferalForm Then
                    Response.Redirect(PathConstants.CustomerDetail + "?" + PathConstants.CustomerId + "=" + Me.customerId.ToString() + "&" + PathConstants.PropertyId + "=" + Me.propertyId + "&" + PathConstants.ItemId + "=" + Me.ItemId.ToString() + "&" + PathConstants.ItemNatureId + "=" + Me.ItemNatureId.ToString + "&" + PathConstants.title + "=" + Me.Title + "&" + PathConstants.TenancyId + "=" + Me.tenancyId.ToString())
                    ' Response.Redirect(PathConstants.CustomerDetail, True)
                ElseIf Request.QueryString(PathConstants.Page) = ApplicationConstants.viewRefferalForm Then
                    If ASPSession("referralHistoryId") IsNot Nothing Then
                        referralHistoryId = Integer.Parse(ASPSession("referralHistoryId").ToString())
                        SessionManager.setTenancyReferralHistoryId(referralHistoryId)
                    End If
                    Response.Redirect(PathConstants.TenancyReferralForm, True)
                End If
            End If

        End If
    End Sub
#End Region

#Region "Set User Session"
    ''' <summary>
    ''' Set User Session.
    ''' </summary>
    ''' <param name="resultDataSet"></param>
    ''' <remarks></remarks>
    Private Sub setUserSession(ByRef resultDataSet)

        SessionManager.setTSRUserId(classicUserId)
        SessionManager.setUserFullName(resultDataSet.Tables(0).Rows(0).Item("FullName").ToString())
        SessionManager.setUserEmployeeId(resultDataSet.Tables(0).Rows(0).Item("EmployeeId").ToString())

    End Sub
#End Region

#Region "Check Classic Asp Session"
    ''' <summary>
    ''' Check Classic Asp Session
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub checkClassicAspSession()

        If ASPSession("USERID") IsNot Nothing Then
            classicUserId = Integer.Parse(ASPSession("USERID").ToString())
        Else
            Me.redirectToLoginPage()
        End If

    End Sub
#End Region

#Region "Redirect To Login Page"
    ''' <summary>
    ''' Redirect To Login Page
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub redirectToLoginPage()
        Response.Redirect(PathConstants.LoginPath, True)
    End Sub
#End Region

#Region "get Query String Values"

    Private Sub getQueryStringValues()
        If Request.QueryString(PathConstants.CustomerId) IsNot Nothing And Request.QueryString(PathConstants.ItemId) IsNot Nothing And Request.QueryString(PathConstants.ItemNatureId) IsNot Nothing And Request.QueryString(PathConstants.PropertyId) IsNot Nothing And Request.QueryString(PathConstants.title) IsNot Nothing And Request.QueryString(PathConstants.TenancyId) IsNot Nothing Then

            Me.customerId = CType(Request.QueryString(PathConstants.CustomerId), Integer)
            Me.propertyId = CType(Request.QueryString(PathConstants.PropertyId), String)
            Me.Title = CType(Request.QueryString(PathConstants.title), String)
            Me.ItemId = CType(Request.QueryString(PathConstants.ItemId), Integer)
            Me.ItemNatureId = CType(Request.QueryString(PathConstants.ItemNatureId), Integer)
            Me.tenancyId = CType(Request.QueryString(PathConstants.TenancyId), Integer)
        ElseIf Request.QueryString(PathConstants.Page) IsNot Nothing Then

            Me.pageToRedirect = Request.QueryString(PathConstants.Page)
        Else
            Me.redirectToLoginPage()
        End If
    End Sub
#End Region

#Region "Get Query String for redirect"

    Public Function getQueryStringForRedirect() As String
        Dim retrunSting As String = String.Empty
        If Request.QueryString.Count > 0 Then
            For Each key In Request.QueryString.Keys
                If key <> PathConstants.Page Then
                    If retrunSting <> String.Empty Then
                        retrunSting += "&"
                    End If
                    retrunSting += key.ToString + "=" + Request.QueryString(key.ToString)
                End If
            Next
        End If

        Return retrunSting

    End Function

#End Region


#End Region

End Class
