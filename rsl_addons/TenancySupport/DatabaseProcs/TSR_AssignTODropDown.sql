USE [RSLBHALive1]
GO
/****** Object:  StoredProcedure [dbo].[TSR_AssignTODropDown]    Script Date: 02/06/2014 12:52:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC TSR_AssignTODropDown
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,03/02/2014>
-- Description:	<Description,,Populate the assign to dropdown on Assign PopUp>
-- WebPage: TenancySupportReport.aspx
-- =============================================
CREATE PROCEDURE [dbo].[TSR_AssignTODropDown]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	
	Select (ISNULL(E__EMPLOYEE.FIRSTNAME,' ')+' '+ISNULL(E__EMPLOYEE.LASTNAME,' ')) as AssigneName,E_JOBDETAILS.EMPLOYEEID as AssigneeId,E_CONTACT.WORKEMAIL as Email  from E_CONTACT 
	INNER JOIN E__EMPLOYEE on E__EMPLOYEE.EMPLOYEEID = E_CONTACT.EMPLOYEEID
	INNER JOIN E_JOBDETAILS on E_JOBDETAILS.EMPLOYEEID = E__EMPLOYEE.EMPLOYEEID 
	
	where E_JOBDETAILS.active=1 and E_JOBDETAILS.jobtitle like '%Tenancy Support Co-ordinator%'
END
