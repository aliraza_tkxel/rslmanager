-- =============================================  
-- EXEC TSR_CutomerCheckBoxList @helpcategory = 1  
-- Author:  <Author,,Salman Nazir>  
-- Create date: <Create Date,,01/16/2014>  
-- Description: <Description,,get the checkbox lists for each category>  
-- WebPage: CustomerDetail.aspx  
-- =============================================  
Alter PROCEDURE TSR_CutomerCheckBoxList   
(  
@helpcategory int  
)  
AS  
BEGIN  
 -- SET NOCOUNT ON added to prevent extra result sets from  
 -- interfering with SELECT statements.  
 SET NOCOUNT ON;  
  
    -- Insert statements for procedure here  
 Select C_REFERRAL_HELPCATEGORY.categoryid,C_REFERRAL_HELPSUBCATEGORY.subcategoryid as subcategoryid,  
 C_REFERRAL_HELPCATEGORY.description,C_REFERRAL_HELPSUBCATEGORY.description as subcategoryDescription  from C_REFERRAL_HELPCATEGORY   
 Inner Join C_REFERRAL_HELPSUBCATEGORY on C_REFERRAL_HELPCATEGORY.CategoryID = C_REFERRAL_HELPSUBCATEGORY.CategoryID  
 Where C_REFERRAL_HELPCATEGORY.CategoryId = @helpcategory  
END  