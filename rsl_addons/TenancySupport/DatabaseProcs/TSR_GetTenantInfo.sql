
/****** Object:  StoredProcedure [dbo].[TSR_GetTenantInfo]    Script Date: 02/10/2014 19:41:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Exec TSR_GetTenantInfo @tenancyId = 231940
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,03/02/2014>
-- Description:	<Description,,get the tenant info for Assign PopUp>
-- WebPage: TenancySupportReport.aspx
-- =============================================
ALTER PROCEDURE [dbo].[TSR_GetTenantInfo](
@tenancyId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	Select (C__CUSTOMER.FIRSTNAME +' '+C__CUSTOMER.LASTNAME)as TenantName,(ISNULL(C_ADDRESS.ADDRESS1,' ') +' '+ISNULL(C_ADDRESS.ADDRESS2,' ')+' '+ISNULL(C_ADDRESS.ADDRESS3,' ')) as TenantAddress  from C_CUSTOMERTENANCY inner join
C__CUSTOMER on C_CUSTOMERTENANCY.CUSTOMERID = C__CUSTOMER.CUSTOMERID
INNER JOIN C_ADDRESS on  C_ADDRESS.CUSTOMERID = C__CUSTOMER.CUSTOMERID
Where C_CUSTOMERTENANCY.TENANCYID = @tenancyId



END
