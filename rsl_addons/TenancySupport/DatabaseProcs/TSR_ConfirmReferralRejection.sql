
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC TSR_ConfirmReferralRejection @referralHistoryId = 1,@rejectedBy = 156,@reasonNotes='Some Reason'
-- Author:		<Author,,Ahmed Mehmood>
-- Create date: <Create Date,,04/02/2014>
-- Description:	<Description,,Confirm Referral Rejection>
-- WebPage: TenancySupportReport.aspx
-- =============================================
ALTER PROCEDURE TSR_ConfirmReferralRejection 
(
@referralHistoryId int
,@rejectedBy int
,@reasonNotes varchar(1000)
,@isSaved bit out
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;


BEGIN TRANSACTION;
BEGIN TRY

	
Declare @journalId int

SELECT	@journalId = C_REFERRAL.JOURNALID 
FROM	C_REFERRAL
WHERE	C_REFERRAL.REFERRALHISTORYID = @referralHistoryId


--===================================================
-- Update C_JOURNAL
--===================================================
Declare @closeStatusId int
SELECT  @closeStatusId = C_STATUS.ITEMSTATUSID 
FROM	C_STATUS
WHERE	C_STATUS.DESCRIPTION = 'Closed'

UPDATE	C_JOURNAL
SET		C_JOURNAL.CURRENTITEMSTATUSID = @closeStatusId
WHERE	C_JOURNAL.JOURNALID = @journalId

--===================================================
-- Insert into C_REFERRAL
--===================================================

Declare @rejectedActionId int
SELECT  @rejectedActionId = C_ACTION.ITEMACTIONID  
FROM	C_ACTION
WHERE	C_ACTION.DESCRIPTION = 'Rejected'

INSERT INTO C_REFERRAL 
(C_REFERRAL.JOURNALID,C_REFERRAL.ITEMSTATUSID,C_REFERRAL.ITEMACTIONID ,C_REFERRAL.isTypeArrears,C_REFERRAL.isTypeDamage,C_REFERRAL.isTypeASB 
,C_REFERRAL.STAGEID,C_REFERRAL.SAFETYISSUES,C_REFERRAL.CONVICTION,C_REFERRAL.SUBSTANCEMISUSE,C_REFERRAL.SELFHARM,C_REFERRAL.NOTES,C_REFERRAL.ISTENANTAWARE  
,C_REFERRAL.NEGLECTNOTES,C_REFERRAL.OTHERNOTES,C_REFERRAL.MISCNOTES,C_REFERRAL.REASONNOTES,C_REFERRAL.LASTACTIONDATE,C_REFERRAL.ASSIGNTO 
,C_REFERRAL.LASTACTIONUSER  )
SELECT SRC.JOURNALID,@closeStatusId,@rejectedActionId,SRC.isTypeArrears,SRC.isTypeDamage,SRC.isTypeASB,SRC.STAGEID,SRC.SAFETYISSUES 
,SRC.CONVICTION,SRC.SUBSTANCEMISUSE,SRC.SELFHARM,SRC.NOTES,SRC.ISTENANTAWARE ,SRC.NEGLECTNOTES,SRC.OTHERNOTES,SRC.MISCNOTES,@reasonNotes
,GETDATE(),NULL,@rejectedBy       
FROM C_REFERRAL AS SRC
WHERE SRC.REFERRALHISTORYID = @referralHistoryId


END TRY
BEGIN CATCH 
    IF @@TRANCOUNT > 0
  BEGIN     
   ROLLBACK TRANSACTION;   
 SET @isSaved = 0
    
    DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    -- Use RAISERROR inside the CATCH block to return 
    -- error information about the original error that 
    -- caused execution to jump to the CATCH block.
    RAISERROR (@ErrorMessage, -- Message text.
         @ErrorSeverity, -- Severity.
         @ErrorState -- State.
         );
  END
END CATCH;

IF @@TRANCOUNT > 0
 BEGIN  
  COMMIT TRANSACTION;  
  SET @isSaved = 1
 END
	
END
GO
