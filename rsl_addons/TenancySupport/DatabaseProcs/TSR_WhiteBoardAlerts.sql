USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[TSR_AssignTODropDown]    Script Date: 02/06/2014 12:52:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC TSR_WhiteBoardAlerts
-- Author:		Aamir Waheed
-- Create date: 17/03/2014
-- Description:	Get Alert Count for Tenancy Support to show on my white board.
-- WebPage: /BHAIntranet/CustomPages/MyWhiteboard.aspx
-- =============================================
ALTER PROCEDURE [dbo].[TSR_WhiteBoardAlerts]	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.	
	DECLARE @AlertCount INT = 0	
	
	DECLARE	@totalCountPending INT = 0
	DECLARE @pendingStatus INT
	SELECT @pendingStatus = ITEMSTATUSID FROM C_STATUS WHERE DESCRIPTION = 'Pending'
	
	CREATE TABLE #tblPending(REFERRALHISTORYID INT,	LASTACTIONDATE NVARCHAR(17), 
			ReferredBy NVARCHAR(200), Email NVARCHAR(200),	ReferredDate NVARCHAR(17)
			, AssignedDate NVARCHAR(17), AssignedTo NVARCHAR(200), TenantName NVARCHAR(200)
			, [Status] NVARCHAR(50), StatusID INT,Rejecteddate NVARCHAR(17), LastActionBy NVARCHAR(200)
			, TENANCYID INT, JournalId INT, CustomerId INT,row INT)
	
	INSERT #tblPending	
	EXEC [dbo].[TSR_SupportReferralReport]
		@status = @pendingStatus,		
		@totalCount = @totalCountPending OUTPUT
		
	DROP TABLE #tblPending
		
	IF @totalCountPending > 0
		SET @AlertCount = @AlertCount + @totalCountPending
		
	SELECT @AlertCount AS [AlertCount]
END