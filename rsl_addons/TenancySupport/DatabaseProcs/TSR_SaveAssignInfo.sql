
/****** Object:  StoredProcedure [dbo].[TSR_SaveAssignInfo]    Script Date: 02/11/2014 19:36:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC TSR_SaveAssignInfo @REFERRALHISTORYID = 2
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,04/02/2014>
-- Description:	<Description,,Save the assign popUp values on the report>
-- Webpage: TenancySupportReport.aspx
-- =============================================
ALTER PROCEDURE [dbo].[TSR_SaveAssignInfo]
(
@REFERRALHISTORYID int,
@asigneeId int,
@assignNotes varchar(1000)
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	Declare @JournalId int
    -- Insert statements for procedure here
	INSERT INTO C_REFERRAL ([JOURNALID],[ITEMSTATUSID],[ITEMACTIONID],[isTypeArrears],[isTypeDamage],[isTypeASB]
      ,[STAGEID],[SAFETYISSUES],[CONVICTION],[SUBSTANCEMISUSE],[SELFHARM],[NOTES],[ISTENANTAWARE],[NEGLECTNOTES]
      ,[OTHERNOTES],[MISCNOTES],[REASONNOTES],[LASTACTIONDATE],[ASSIGNTO],[LASTACTIONUSER])
 (Select [JOURNALID],13,19,[isTypeArrears],[isTypeDamage],[isTypeASB],[STAGEID]
      ,[SAFETYISSUES],[CONVICTION],[SUBSTANCEMISUSE],[SELFHARM],@assignNotes,[ISTENANTAWARE],[NEGLECTNOTES],[OTHERNOTES]
      ,[MISCNOTES],[REASONNOTES],GETDATE(),@asigneeId,[LASTACTIONUSER] from C_REFERRAL Where REFERRALHISTORYID = @REFERRALHISTORYID)
      
      Select @JournalId = JournalId from C_REFERRAL Where REFERRALHISTORYID = @REFERRALHISTORYID
      Update C_journal Set CURRENTITEMSTATUSID = 13 Where JOURNALID = @JournalId
END
