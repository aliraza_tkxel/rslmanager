
-- =============================================
-- EXEC TSR_UpdateReferral @referralHistoryId = 1
-- Author:		<Author,,Ahmed Mehmood>
-- Create date: <Create Date,,04/02/2014>
-- Description:	<Description,,Confirm Referral Rejection>
-- WebPage: TenancySupportReport.aspx
-- =============================================
ALTER PROCEDURE [dbo].[TSR_UpdateReferral] 
(
@referralHistoryId int
,@actionId int
,@assignedTo int
,@notes varchar(1000)
,@updatedBy int
,@reason varchar(1000)
,@isSaved bit out
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

BEGIN TRANSACTION;
BEGIN TRY


Declare @closeStatusId int
SELECT  @closeStatusId = C_STATUS.ITEMSTATUSID 
FROM	C_STATUS
WHERE	C_STATUS.DESCRIPTION = 'Closed'

Declare @openStatusId int
SELECT  @openStatusId = C_STATUS.ITEMSTATUSID 
FROM	C_STATUS
WHERE	C_STATUS.DESCRIPTION = 'Open'

--=====================================
-- ASSIGN (22)
--=====================================
IF(@actionId = 22)
BEGIN
	EXEC TSR_SaveAssignInfo
		@REFERRALHISTORYID = @referralHistoryId,
		@asigneeId = @assignedTo,
		@assignNotes = @notes
END

--=====================================
-- REJECT (23)
--=====================================
ELSE IF(@actionId = 23)
BEGIN

	EXEC TSR_ConfirmReferralRejection
		@referralHistoryId = @referralHistoryId,
		@rejectedBy = @updatedBy,
		@reasonNotes =@reason,
		@isSaved = @isSaved OUTPUT

END
ELSE 
BEGIN 

--=====================================
-- NOTE(20) / CLOSED(21)
--=====================================

-- Insert into C_JOURNAL

Declare @journalId int,@itemStatusId int,@itemActionId int

if(@actionId = 20)
BEGIN
	SET @itemStatusId = @openStatusId
	SET	@itemActionId = 20
END	
else if(@actionId = 21)
BEGIN
	SET @itemStatusId = @closeStatusId
	SET	@itemActionId = 21
END

SELECT	@journalId = C_REFERRAL.JOURNALID 
FROM	C_REFERRAL
WHERE	C_REFERRAL.REFERRALHISTORYID = @referralHistoryId

UPDATE	C_JOURNAL
SET		C_JOURNAL.CURRENTITEMSTATUSID = @itemStatusId
WHERE	C_JOURNAL.JOURNALID = @journalId


-- Insert into C_REFERRAL
INSERT INTO C_REFERRAL 
(C_REFERRAL.JOURNALID,C_REFERRAL.ITEMSTATUSID,C_REFERRAL.ITEMACTIONID ,C_REFERRAL.isTypeArrears,C_REFERRAL.isTypeDamage,C_REFERRAL.isTypeASB 
,C_REFERRAL.STAGEID,C_REFERRAL.SAFETYISSUES,C_REFERRAL.CONVICTION,C_REFERRAL.SUBSTANCEMISUSE,C_REFERRAL.SELFHARM,C_REFERRAL.NOTES,C_REFERRAL.ISTENANTAWARE  
,C_REFERRAL.NEGLECTNOTES,C_REFERRAL.OTHERNOTES,C_REFERRAL.MISCNOTES,C_REFERRAL.REASONNOTES,C_REFERRAL.LASTACTIONDATE,C_REFERRAL.ASSIGNTO 
,C_REFERRAL.LASTACTIONUSER  )
SELECT SRC.JOURNALID,@itemStatusId,@itemActionId,SRC.isTypeArrears,SRC.isTypeDamage,SRC.isTypeASB,SRC.STAGEID,SRC.SAFETYISSUES 
,SRC.CONVICTION,SRC.SUBSTANCEMISUSE,SRC.SELFHARM,@notes,SRC.ISTENANTAWARE ,SRC.NEGLECTNOTES,SRC.OTHERNOTES,SRC.MISCNOTES,NULL
,GETDATE(),NULL,@updatedBy       
FROM C_REFERRAL AS SRC
WHERE SRC.REFERRALHISTORYID = @referralHistoryId


END

END TRY
BEGIN CATCH 
    IF @@TRANCOUNT > 0
  BEGIN     
   ROLLBACK TRANSACTION;   
 SET @isSaved = 0
    
    DECLARE @ErrorMessage NVARCHAR(4000);
    DECLARE @ErrorSeverity INT;
    DECLARE @ErrorState INT;

    SELECT @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();

    -- Use RAISERROR inside the CATCH block to return 
    -- error information about the original error that 
    -- caused execution to jump to the CATCH block.
    RAISERROR (@ErrorMessage, -- Message text.
         @ErrorSeverity, -- Severity.
         @ErrorState -- State.
         );
  END
END CATCH;

IF @@TRANCOUNT > 0
 BEGIN  
  COMMIT TRANSACTION;  
  SET @isSaved = 1
 END
	
END
