USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================  
-- Author:  <Author,,Salman Nazir>  
-- Create date: <Create Date,,01/24/2014>  
-- Description: <Description,,Get the Tenancy Support Referral Report Data>
-- WebPage: TenancySupportReport.aspx
-- Modified By: <Aamir Waheed, 20/03/2014, Completely modified the logic to create and filter the report.>
-- =============================================  
ALTER PROCEDURE [dbo].[TSR_SupportReferralReport]   
(  
@searchText varchar(500) = '',
@fromDate varchar(50) = '',
@toDate varchar(50) = '',
@lastAction int = -1,
@status int = -1,
@pageSize int = 20,
@pageNumber int = 1,  
@totalCount int = 0 output  
)  
AS  
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from  
-- interfering with SELECT statements.  
SET NOCOUNT ON;
DECLARE @SelectClause varchar(3000),
@fromClause varchar(2000),
@whereClause varchar(1500),
@mainSelectQuery varchar(6000),
@rowNumberQuery varchar(7000),
@finalQuery varchar(8000),
@selectCount nvarchar(4000),
@parameterDef nvarchar(500),
@searchCriteria varchar(1500),
@sortOrder varchar(1000),
@offset int,
@limit int

SET @offset = 1 + (@pageNumber - 1) * @pageSize
SET @limit = (@offset + @pageSize) - 1

SET @searchCriteria = ' 1 = 1 '
	
IF (@status <> 0 AND @status <> -1)
BEGIN
	SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND R.ITEMSTATUSID = ' + CONVERT(NVARCHAR,@status)
END

IF (@lastAction <> 0 AND @lastAction <> -1)
BEGIN
	SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND R.ITEMACTIONID = ' + CONVERT(NVARCHAR,@lastAction)
END

IF (ISNULL(@fromDate,'') <> '')
BEGIN
	SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND R.LASTACTIONDATE >= ''' + @fromDate + ''''
END

IF (ISNULL(@toDate,'') <> '')
BEGIN
	SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND R.LASTACTIONDATE <= ''' + @toDate + ''''
END

IF (ISNULL(@searchText,'') <> '')
BEGIN
	SET @searchCriteria = @searchCriteria + CHAR(10) + ' AND ( tblReferred.ReferredBy LIKE ''%' + @searchText + '%'' '
	SET @searchCriteria = @searchCriteria + CHAR(10) + ' OR J.TENANCYID LIKE ''%' + @searchText + '%'' '	
	SET @searchCriteria = @searchCriteria + CHAR(10) + ' OR ISNULL(NULLIF(ISNULL(AssignedTo.FIRSTNAME,'''') + ISNULL('' '' + AssignedTo.LASTNAME,''''),''''),''-'') LIKE ''%' + @searchText + '%'' '
	SET @searchCriteria = @searchCriteria + CHAR(10) + ' OR C.FirstName LIKE ''%' + @searchText + '%'' '
	SET @searchCriteria = @searchCriteria + CHAR(10) + ' OR C.LASTNAME LIKE ''%' + @searchText + '%'' '
	SET @searchCriteria = @searchCriteria + CHAR(10) + ' OR C.FirstName + ' + ' C.LASTNAME LIKE ''%' + @searchText + '%'' )'
END

SET @SelectClause = ' SELECT TOP( ' + CONVERT(NVARCHAR,@limit) +') 
						REFERRALHISTORYID AS REFERRALHISTORYID
						, CONVERT(NVARCHAR,R.LASTACTIONDATE,103) AS LASTACTIONDATE
						, ReferredBy AS ReferredBy
						, Email AS Email	
						, ReferredDate AS ReferredDate
						, CASE A.[DESCRIPTION] WHEN ''Assigned'' THEN CONVERT(NVARCHAR, R.LASTACTIONDATE,103) ELSE ''-'' END AS AssignedDate
						, ISNULL(NULLIF(ISNULL(AssignedTo.FIRSTNAME,'''') + ISNULL('' '' + AssignedTo.LASTNAME,''''),''''),''-'')  AS AssignedTo
						, ISNULL(NULLIF(ISNULL(C.FIRSTNAME,'''') + ISNULL('' '' + C.LASTNAME,''''),''''),''--'') AS TenantName
						, S.[DESCRIPTION] AS [Status]
						, R.ITEMSTATUSID AS StatusID
						, CASE A.[DESCRIPTION] WHEN ''Rejected'' THEN CONVERT(NVARCHAR, R.LASTACTIONDATE,103) ELSE ''-'' END AS Rejecteddate
						, ISNULL(NULLIF(ISNULL(E.FIRSTNAME,'''') + ISNULL('' '' + E.LASTNAME,''''),''''),''-'') AS LastActionBy
						, J.TENANCYID AS TENANCYID
						, J.JournalId AS JournalId
						, C.CUSTOMERID AS CustomerId '

SET @fromClause = ' FROM C_REFERRAL R
						INNER JOIN C_JOURNAL J ON J.JOURNALID = R.JOURNALID 
							AND R.REFERRALHISTORYID IN (SELECT MAX(REFERRALHISTORYID) FROM C_REFERRAL GROUP BY JOURNALID)
						INNER JOIN C_ACTION A ON A.ITEMACTIONID = R.ITEMACTIONID
						INNER JOIN C__CUSTOMER C ON C.CUSTOMERID = J.CUSTOMERID
						INNER JOIN C_STATUS S ON S.ITEMSTATUSID = R.ITEMSTATUSID
						LEFT JOIN E__EMPLOYEE E ON R.LASTACTIONUSER = E.EMPLOYEEID
						LEFT JOIN E__EMPLOYEE AssignedTo ON AssignedTo.EMPLOYEEID = R.ASSIGNTO
						LEFT JOIN (SELECT JOURNALID AS JOURNALID
										, CONVERT(NVARCHAR, C_REFERRAL.LASTACTIONDATE,103) AS ReferredDate
										, ISNULL(NULLIF(ISNULL(FIRSTNAME,'''') + ISNULL('' '' + LASTNAME,''''),''''),''-'') AS ReferredBy
										, WORKEMAIL AS Email
									FROM C_REFERRAL 
									INNER JOIN E__EMPLOYEE ON E__EMPLOYEE.EMPLOYEEID = C_REFERRAL.LASTACTIONUSER 
										AND C_REFERRAL.REFERRALHISTORYID IN (SELECT REFERRALHISTORYID FROM C_REFERRAL INNER JOIN C_ACTION ON C_REFERRAL.ITEMACTIONID = C_ACTION.ITEMACTIONID AND C_ACTION.[DESCRIPTION] = ''Referred'')
									INNER JOIN E_CONTACT ON E_CONTACT.EMPLOYEEID = C_REFERRAL.LASTACTIONUSER
							) tblReferred ON tblReferred.JOURNALID = R.JOURNALID '


SET @sortOrder = '   REFERRALHISTORYID DESC '

SET @whereClause = CHAR(10) + ' WHERE ' + CHAR(10) + @searchCriteria

SET @mainSelectQuery = @selectClause + @fromClause + @whereClause

--========================================================================================  
-- Begin building the row number query  

SET @rowNumberQuery = '  SELECT *,row_number() over (order by ' + CHAR(10) + @sortOrder + CHAR(10) + ') as row   
        FROM (' + CHAR(10) + @mainSelectQuery + CHAR(10) + ')AS Records'

-- End building the row number query
--========================================================================================  
--========================================================================================  
-- Begin building the final query   

SET @finalQuery = ' SELECT *  
       FROM(' + CHAR(10) + @rowNumberQuery + CHAR(10) + ') AS Result   
       WHERE  
       Result.row between' + CHAR(10) + CONVERT(varchar(10), @offset) + CHAR(10) + 'and' + CHAR(10) + CONVERT(varchar(10), @limit)

-- End building the final query  
--========================================================================================   
PRINT (@finalQuery)
EXEC (@finalQuery)

SET @parameterDef = '@totalCount int OUTPUT';
SET @selectCount = 'SELECT @totalCount =  count(REFERRALHISTORYID) ' + @fromClause + @whereClause

EXECUTE sp_executesql	@selectCount,
						@parameterDef,
						@totalCount OUTPUT;
END