-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--EXEC TSR_getReportStatuses
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,>
-- Description:	<Description,,get Statuses for Report>
-- WebPage : TenancySupportReferral.aspx
-- =============================================
CREATE PROCEDURE TSR_getReportStatuses
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select ITEMSTATUSID,DESCRIPTION as StatusName from C_STATUS Where ITEMID = 2 AND ITEMSTATUSID NOT IN (23,24,25)
END
GO
