-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC TSR_SupportReferral @journalid=236
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,05/02/2014>
-- Description:	<Description,,Get data for Assigned Support Referral forms>
-- WebPage: Customer.asp -> Journal Tab
-- =============================================
CREATE PROCEDURE TSR_SupportReferral
(
@journalid int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select      ISNULL(CONVERT(NVARCHAR, CF.LastActiondate, 103) ,'') + ' ' + ISNULL(CONVERT(NVARCHAR(5), CF.LastActiondate, 108) ,'') AS ReferralDate ,ISNULL(CF.NOTES, ' ') as Notes,ISNULL(S.DESCRIPTION, ' ') as Status, (E__EMPLOYEE.FIRSTNAME+' '+E__EMPLOYEE.LASTNAME)as EmployeeName 
                               ,ISNULL(J.TITLE,' ') as Title, A.DESCRIPTION as Action 
                    FROM       C_REFERRAL CF
                               Left JOIN C_STATUS S on CF.ITEMSTATUSID = S.ITEMSTATUSID 
                               Left JOIN E__EMPLOYEE on CF.LASTACTIONUSER = E__EMPLOYEE.EMPLOYEEID 
                               Left JOIN C_JOURNAL J on CF.JOURNALID = J.JOURNALID 
                              Left JOIN C_ACTION A on CF.ITEMACTIONID = A.ITEMACTIONID 
                    Where      CF.JOURNALID = @journalId and A.ITEMACTIONID = 17
                    ORDER      BY  REFERRALHISTORYID DESC
END
GO
