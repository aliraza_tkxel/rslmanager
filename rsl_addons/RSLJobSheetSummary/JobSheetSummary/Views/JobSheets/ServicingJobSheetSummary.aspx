﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ServicingJobSheetSummary.aspx.vb"
    Inherits="JobSheetSummary.ServicingJobSheetSummary" %>

<!DOCTYPE html>
<html>
<head id="Head1" runat="server">
    <link href="../../Styles/default.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../../Styles/JobSheet.css" />
    <title></title>
    <style type="text/css">
        .headingTitle
        {
            background: #000000;
            color: #ffffff;
            width: 98%;
            height: 20px;
            padding-left: 20px;
            padding-top: 10px;
            padding-bottom: 10px;
            padding-right: 12px;
            margin-bottom: 10px;
        }
        .leftDiv
        {
            float: left;
            margin-left: 10px;
            width: 48%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Panel ID="pnlMessage" runat="server" Visible="False">
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
            <br />
        </asp:Panel>
        <div class="mainContainer">
            <div class="headingTitle">
                <b>
                    <asp:Label Text="" ID="lblSummary" runat="server" /></b>
            </div>
            
            <hr />
            <br />
            <div style="padding: 10px; border: 1px solid;">
                <table runat="server" id="tblRequiredWork" width="90%">
                </table>
            </div>
            <br />
            <br />
            <asp:Panel ID="pnlAppointmentDetail" runat="server">
                <div class="leftDiv">
                    <table style="width: 100%;" cellspacing="10" cellpadding="5">
                        <tr>
                            <td class="labelTd" style="width: 33%;">
                                <asp:Label ID="Label2" runat="server" Font-Bold="true" Text="Operative:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblOperative" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label3" runat="server" Font-Bold="true" Text="Duration:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblDuration" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                              <tr>
                                <td class="labelTd">
                                    <asp:Label ID="Label4" runat="server" Font-Bold="true" Text="Cp12 Issued:"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblCp12Issued" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="labelTd">
                                    <asp:Label ID="Label1" runat="server" Font-Bold="true" Text="CP12 Expiry:"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblCP12Expiry" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                    </table>
                </div>
                <div class="leftDiv">
                    <table style="width: 100%;" cellspacing="10" cellpadding="5">
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label23" runat="server" Font-Bold="true" Text="Status:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label5" runat="server" Font-Bold="true" Text="Time:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblTime" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label6" runat="server" Font-Bold="true" Text="Start Date:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblStartDate" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label7" runat="server" Font-Bold="true" Text="End Date:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblEndDate" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <div style="clear: both">
            </div>
            <hr />
            <div style="clear: both">
            </div>
            <asp:Panel ID="pnlPropertyDetail" runat="server">
                <div class="leftDiv">
                    <table style="width: 100%;" cellspacing="10" cellpadding="5">
                        <tr>
                            <td class="labelTd" style=" width:33%;">
                                <asp:Label ID="Label8" runat="server" Font-Bold="true" Text="Scheme:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblScheme" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label9" runat="server" Font-Bold="true" Text="Address:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblAddress" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                &nbsp;
                            </td>
                            <td>
                                <asp:Label ID="lblTowncity" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                &nbsp;
                            </td>
                            <td>
                                <asp:Label ID="lblCounty" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                &nbsp;
                            </td>
                            <td>
                                <asp:Label ID="lblPostcode" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="leftDiv">
                    <table style="width: 100%;" cellspacing="10" cellpadding="5">
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label13" runat="server" Font-Bold="true" Text="Customer:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblCustomerName" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label14" runat="server" Font-Bold="true" Text="Telephone:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblCustomerTelephone" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label15" runat="server" Font-Bold="true" Text="Mobile:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblCustomerMobile" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label16" runat="server" Font-Bold="true" Text="Email:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblCustomerEmail" runat="server"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
                    </table>
                </div>
                <div style="clear: both">
                </div>
                <div style="text-align: right;">
                </div>
            </asp:Panel>
            <div style="clear: both">
            </div>
            <hr />
            <div style="clear: both">
            </div>
            <asp:Panel ID="pnlNotes" runat="server">
                <div class="leftDiv">
                    <table style="width: 100%;" cellspacing="10" cellpadding="5">
                        <tr>
                            <td>
                                <asp:Label ID="Label19" Font-Bold="true" runat="server" Text="Appointment Notes:"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtCustAppointmentNotes" MaxLength="1000" runat="server" TextMode="MultiLine"
                                    Height="150px" Width="100%" ReadOnly="true"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="leftDiv">
                    <table style="width: 100%;" cellspacing="10" cellpadding="5">
                        <tr>
                            <td>
                                <asp:Label ID="lblJobSheetNotes" Font-Bold="true" runat="server" Text="Asbestos:"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div style="height: 100px; width: 400px; overflow: auto;">
                                    <asp:GridView ID="grdAsbestos" runat="server" AutoGenerateColumns="False" ShowHeader="False"
                                        BorderStyle="None" GridLines="None" Width="100%">
                                        <Columns>
                                            <asp:BoundField DataField="AsbRiskID" />
                                            <asp:BoundField DataField="Description">                                               
                                            </asp:BoundField>
                                            <asp:BoundField DataField="DateAdded"/>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <div style="clear: both">
            </div>
            <div style="text-align: right;">
            </div>
            <br />
            <div style="text-align: right; width: 100%;">
                &nbsp &nbsp &nbsp &nbsp
            </div>
        </div>
    </div>
    </form>
</body>
</html>
