﻿Imports JSS_Utilities
Imports System.Threading
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports JSS_BusinessLogic
Imports System.Globalization

Public Class MaintenanceJobSheetSummary
    Inherits PageBase

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                Dim Jsn As String = CType(Request.QueryString(PathConstants.Jsn), String)
                populateJobSheet(Jsn)
            End If
        Catch ex As ThreadAbortException
            uiMessageHelper.message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub



    Private Sub populateJobSheet(jsn As String)

        Dim objJobSheetSummaryBL As New JobSheetSummaryBL
        Dim jobSheetDataSet As DataSet = New DataSet

        txtCustAppointmentNotes.Attributes.Add("readonly", "readonly")
        txtJobSheetNotes.Attributes.Add("readonly", "readonly")

        objJobSheetSummaryBL.GetMEJobSheetSummaryDetails(jsn, jobSheetDataSet)

        Dim dtAppointments As DataTable = jobSheetDataSet.Tables("AppointmentInfo")
        Dim drAppointment As DataRow() = Nothing
        drAppointment = dtAppointments.[Select]("JSN = '" + jsn + "'")
        populateAppointmentInfo(drAppointment(0))
        bindPropertyInfo(jobSheetDataSet.Tables("PropertyInfo"))
    End Sub


#Region "Populate Appointment Info"
    ''' <summary>
    ''' Populate Appointment Info
    ''' </summary>
    ''' <remarks></remarks>

    Public Sub populateAppointmentInfo(ByVal drAppointment As DataRow)


        lblHeading.Text = drAppointment("MsatType")
        lblWorkType.Text = drAppointment("MsatType")
        lblJsnHeader.Text = Convert.ToString(drAppointment(ApplicationConstants.AppointmentIdColumn)).PadLeft(6, "0"c)
        lblItemName.Text = Convert.ToString(drAppointment(ApplicationConstants.ItemNameColumn))
        lblTrade.Text = Convert.ToString(drAppointment(ApplicationConstants.TradesColumn))

        'lblJsn.Text = Convert.ToString(drAppointment[ApplicationConstants.AppointmentIdColumn]).PadLeft(6, '0');
        lblStatus.Text = Convert.ToString(drAppointment(ApplicationConstants.InterimStatusColumn))
        lblOperative.Text = Convert.ToString(drAppointment(ApplicationConstants.OperativeColumn))

        lblDuration.Text = GeneralHelper.appendHourLabel(Convert.ToDouble(drAppointment(ApplicationConstants.DurationsColumn)))
        lblDurationTotal.Text = GeneralHelper.appendHourLabel(Convert.ToDouble(drAppointment(ApplicationConstants.TotalDurationColumn)))

        Dim startDateTime As DateTime = DateTime.ParseExact(drAppointment(ApplicationConstants.StartDateColumn) + " " + drAppointment(ApplicationConstants.StartTimeColumn), "dd/MM/yyyy HH:mm", CultureInfo.CreateSpecificCulture("en-UK"))
        Dim endDateTime As DateTime = DateTime.ParseExact(drAppointment(ApplicationConstants.EndDateColumn) + " " + drAppointment(ApplicationConstants.EndTimeColumn), "dd/MM/yyyy HH:mm", CultureInfo.CreateSpecificCulture("en-UK"))

        lblStartDate.Text = Convert.ToDateTime(startDateTime).ToString("HH:mm") + " " + GeneralHelper.getDateWithWeekdayFormat(startDateTime)
        lblEndDate.Text = Convert.ToDateTime(endDateTime).ToString("HH:mm") + " " + GeneralHelper.getDateWithWeekdayFormat(endDateTime)




        txtCustAppointmentNotes.Text = Convert.ToString(drAppointment(ApplicationConstants.CustomerNotesColumn))
        txtJobSheetNotes.Text = Convert.ToString(drAppointment(ApplicationConstants.JobsheetNotesColumn))

        lblMsatType.Text = Convert.ToString(drAppointment("MsatType"))

    End Sub

#End Region


#Region "Bind Property Info"

    Private Sub bindPropertyInfo(propertyDt As DataTable)
        Dim tenancyId As Integer
        'Property Information
        lblScheme.Text = propertyDt.Rows(0)("SchemeName")
        lblAddress.Text = propertyDt.Rows(0)("HOUSENUMBER") + " " + propertyDt.Rows(0)("ADDRESS1") + " " + propertyDt.Rows(0)("ADDRESS2")
        lblTowncity.Text = propertyDt.Rows(0)("TOWNCITY")
        lblCounty.Text = propertyDt.Rows(0)("COUNTY")
        lblPostcode.Text = propertyDt.Rows(0)("POSTCODE")

        'Customer Information

        lblCustomerName.Text = propertyDt.Rows(0)("TenantName")
        lblCustomerTelephone.Text = propertyDt.Rows(0)("Telephone")
        lblCustomerMobile.Text = propertyDt.Rows(0)("Mobile")
        lblCustomerEmail.Text = propertyDt.Rows(0)("Email")
        tenancyId = propertyDt.Rows(0)("TenancyId")


    End Sub

#End Region
End Class