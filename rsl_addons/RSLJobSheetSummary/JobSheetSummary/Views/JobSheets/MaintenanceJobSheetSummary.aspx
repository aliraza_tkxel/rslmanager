﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="MaintenanceJobSheetSummary.aspx.vb"
    Inherits="JobSheetSummary.MaintenanceJobSheetSummary" %>

<%@ Import Namespace="JSS_Utilities" %>
<!DOCTYPE html>
<html>
<head id="Head1" runat="server">
    <link href="../../Styles/default.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../../Styles/JobSheet.css" />
    <title></title>
    <style type="text/css">
        .headingTitle
        {
            background: #000000;
            color: #ffffff;
            width: 98%;
            height: 20px;
            padding-left: 20px;
            padding-top: 10px;
            padding-bottom: 10px;
            padding-right: 12px;
            margin-bottom: 10px;
        }
        .leftDiv
        {
            float: left;
            margin-left: 10px;
            width: 48%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div>
        <asp:Panel ID="pnlMessage" runat="server" Visible="False">
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
            <br />
        </asp:Panel>
        <div class="mainContainer">
            <asp:UpdatePanel runat="server" UpdateMode="Always" ID="updPanelDefectJobSheet">
                <ContentTemplate>
                    <p class="headingTitle ">
                        <asp:Label runat="server" ID="lblHeading" Font-Bold="true" Style="margin-left: 5px;"
                            Text=""></asp:Label>
                        <b>job sheet summary</b>
                    </p>
                    <asp:Panel ID="Panel1" runat="server" Visible="false">
                        <asp:Label ID="Label1" runat="server"></asp:Label>
                    </asp:Panel>
                    <span style="font-size: 14px;">Appointment Summary:</span>
                    <hr />
                    <div style="padding: 10px; border: 1px solid;">
                        <table>
                            <tr>
                                <td style="width: 20%;">
                                   <b> JSN:</b> &nbsp &nbsp
                                    <asp:Label ID="lblJsnHeader" runat="server" Text=""></asp:Label>
                                </td>
                                <td style="width: 66%;">
                                    <asp:Label runat="server" ID="lblMsatType"  Font-Bold="true"  Text=""></asp:Label>
                                   <b> Attribute:</b>
                                    <asp:Label ID="lblItemName" runat="server" Text=""></asp:Label>
                                </td>
                                <td style="width: 15%;">
                                   <b> Trade:</b> &nbsp &nbsp
                                    <asp:Label ID="lblTrade" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="clear">
                    </div>
                    <hr />
                    <div class="clear">
                    </div>
                    <asp:Panel ID="pnlAppointmentDetail" runat="server">
                        <div class="leftDiv">
                            <table style="width: 100%;" cellspacing="10" cellpadding="5">
                                <%--<tr>
                                <td class="labelTd">
                                    <asp:Label ID="Label22" runat="server" Text="JSN:"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblJsn" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>--%>
                                <tr>
                                    <td class="labelTd">
                                        <asp:Label ID="Label2" runat="server" Font-Bold="true"  Text="Operative:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblOperative" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelTd">
                                        <asp:Label ID="Label3" runat="server" Font-Bold="true"  Text="Duration:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblDuration" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelTd">
                                        <asp:Label ID="Label4" runat="server" Font-Bold="true"  Text="Duration Total:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblDurationTotal" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="leftDiv">
                            <table style="width: 100%;" cellspacing="10" cellpadding="5">
                                <tr>
                                    <td class="labelTd">
                                        <asp:Label ID="Label23" runat="server"  Font-Bold="true"  Text="Status:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelTd">
                                        <asp:Label ID="Label6" runat="server"  Font-Bold="true"  Text="Start Date:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblStartDate" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelTd">
                                        <asp:Label ID="Label7" runat="server"  Font-Bold="true"  Text="End Date:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblEndDate" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>
                    <div class="clear">
                    </div>
                    <hr />
                    <asp:Panel ID="pnlPropertyDetail" runat="server">
                        <div class="leftDiv">
                            <table style="width: 100%;" cellspacing="10" cellpadding="5">
                                <tr>
                                    <td class="labelTd">
                                        <asp:Label ID="Label9" runat="server" Font-Bold="true" Text="Scheme:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblScheme" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelTd">
                                        <asp:Label ID="Label10" runat="server" Font-Bold="true" Text="Address:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblAddress" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelTd">
                                    </td>
                                    <td>
                                        <asp:Label ID="lblTowncity" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelTd">
                                    </td>
                                    <td>
                                        <asp:Label ID="lblCounty" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelTd">
                                    </td>
                                    <td>
                                        <asp:Label ID="lblPostcode" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="leftDiv">
                            <table style="width: 100%;" cellspacing="10" cellpadding="5">
                                <tr>
                                    <td class="labelTd">
                                        <asp:Label ID="Label13" runat="server" Font-Bold="true" Text="Customer:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblCustomerName" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelTd">
                                        <asp:Label ID="Label14" runat="server" Font-Bold="true" Text="Telephone:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblCustomerTelephone" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelTd">
                                        <asp:Label ID="Label15" runat="server" Font-Bold="true" Text="Mobile:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblCustomerMobile" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelTd">
                                        <asp:Label ID="Label16" runat="server" Font-Bold="true" Text="Email:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblCustomerEmail" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelTd">
                                    </td>
                                    <td>
                                        <div style="text-align: right;">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="clear">
                        </div>
                    </asp:Panel>
                    <div class="clear">
                    </div>
                    <hr />
                    <div class="clear">
                    </div>
                    <asp:Panel ID="pnlNotes" runat="server">
                        <div class="leftDiv">
                            <table style="width: 100%;" cellspacing="10" cellpadding="5">
                                <tr>
                                    <td>
                                        <b>Customer Appointment Notes:</b>
                                    </td>
                                    <tr>
                                        <td>
                                            If the customer has any preferred contact time,<br />
                                            or ways in which we could contact them,<br />
                                            please enter them in the box below:
                                        </td>
                                    </tr>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtCustAppointmentNotes" MaxLength="1000" runat="server" TextMode="MultiLine"
                                            Height="150px" Width="100%"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="leftDiv">
                            <table style="width: 100%;" cellspacing="10" cellpadding="5">
                                <tr>
                                    <td>
                                        <asp:Label ID="Label20" Font-Bold="true" runat="server" Text="Job Sheet Notes:"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Please enter information specific to the
                                        <br />
                                        <asp:Label runat="server" ID="lblWorkType" Text=""></asp:Label>
                                        works in the box below:
                                        <br />
                                        &nbsp
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtJobSheetNotes" MaxLength="1000" runat="server" TextMode="MultiLine"
                                            Height="150px" Width="100%"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div style="clear: both">
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    </form>
</body>
</html>
