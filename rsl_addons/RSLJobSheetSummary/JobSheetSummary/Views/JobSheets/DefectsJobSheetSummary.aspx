﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DefectsJobSheetSummary.aspx.vb"
    Inherits="JobSheetSummary.DefectsJobSheetSummary" %>

<%@ Import Namespace="JSS_Utilities" %>
<!DOCTYPE html>
<html>
<head id="Head1" runat="server">
    <link href="../../Styles/default.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../../Styles/JobSheet.css" />
    <title></title>
    <style type="text/css">
        .headingTitle
        {
            background: #000000;
            color: #ffffff;
            width: 98%;
            height: 20px;
            padding-left: 20px;
            padding-top: 10px;
            padding-bottom: 10px;
            padding-right: 12px;
            margin-bottom: 10px;
        }
        .leftDiv
        {
            float: left;
            margin-left: 10px;
            width: 48%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <div>
        <asp:Panel ID="pnlMessage" runat="server" Visible="False">
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
            <br />
        </asp:Panel>
        <div class="mainContainer">
            <asp:UpdatePanel runat="server" UpdateMode="Always" ID="updPanelDefectJobSheet">
                <ContentTemplate>
                    <p class="headingTitle ">
                        <b>Defects job sheet summary</b>
                    </p>
                    <asp:Panel ID="Panel1" runat="server" Visible="false">
                        <asp:Label ID="Label1" runat="server"></asp:Label>
                    </asp:Panel>
                    <span style="font-size: 14px;">Appointment Summary:</span>
                    <hr />
                    <asp:HiddenField ID="_lblDefectId" Value='<%# Eval("DefectId:") %>' runat="server" />
                    <asp:GridView ID="grdDefects" runat="server" AutoGenerateColumns="false" ShowHeader="true"
                        BackColor="White" BorderColor="Gray" BorderStyle="None" BorderWidth="0px" ForeColor="Black"
                        GridLines="None" Width="100%" ShowHeaderWhenEmpty="true" AllowSorting="false">
                        <Columns>
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="JSD:">
                                <ItemTemplate>
                                    <asp:Label ID="lblDefectId" Text='<%# Eval("DefectId:") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Appliance" SortExpression="Appliance">
                                <ItemTemplate>
                                    <asp:Label ID="lblAppliance" Text='<%# Eval("Appliance:") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Trade:" SortExpression="DefectTrade">
                                <ItemTemplate>
                                    <asp:Label ID="lblDefectTrade" Text='<%# Eval("Trade:") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="2 Person" SortExpression="isTwoPersonsJob">
                                <ItemTemplate>
                                    <asp:Label ID="lblisTwoPersonsJob" Text='<%# Eval("2 Person:") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Disconnected?"
                                SortExpression="isApplianceDisconnected">
                                <ItemTemplate>
                                    <asp:Label ID="lblisApplianceDisconnected" Text='<%# Eval("Disconnected:") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Duration:" SortExpression="Duration">
                                <ItemTemplate>
                                    <asp:Label ID="lblDefectDuration" Text='<%# Eval("Duration:") %>' runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderStyle-HorizontalAlign="Left" HeaderText="Parts Due:" SortExpression="DefectPartsDue">
                                <ItemTemplate>
                                    <asp:Label ID="lblPartsDue" Text='<%# GeneralHelper.getFormatedDateDefaultString(Eval("Parts Due:")) %>'
                                        runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:GridView>
                    <div class="clear">
                    </div>
                    <hr />
                    <div class="clear">
                    </div>
                    <asp:Panel ID="pnlAppointmentDetail" runat="server">
                        <div class="leftDiv">
                            <table style="width: 100%;" cellspacing="10" cellpadding="5">
                                <tr>
                                    <td class="labelTd">
                                        <asp:Label ID="Label2" runat="server" Font-Bold="true" Text="Inspection Ref:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblInspectionRef" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelTd">
                                        <asp:Label ID="Label3" runat="server" Font-Bold="true" Text="Operative:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblOperative" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelTd">
                                        <asp:Label ID="Label4" runat="server" Font-Bold="true" Text="Duration:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblDuration" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelTd">
                                        <asp:Label ID="Label5" runat="server" Font-Bold="true" Text="Trade:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblTrade" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="leftDiv">
                            <table style="width: 100%;" cellspacing="10" cellpadding="5">
                                <tr>
                                    <td class="labelTd">
                                        <asp:Label ID="Label7" runat="server" Font-Bold="true" Text="Start:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblStartDateTime" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelTd">
                                        <asp:Label ID="Label8" runat="server" Font-Bold="true" Text="End:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblEndDateTime" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>
                    <div class="clear">
                    </div>
                    <hr />
                    <asp:Panel ID="pnlPropertyDetail" runat="server">
                        <div class="leftDiv">
                            <table style="width: 100%;" cellspacing="10" cellpadding="5">
                                <tr>
                                    <td class="labelTd">
                                        <asp:Label ID="Label9" runat="server" Font-Bold="true" Text="Scheme:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblScheme" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelTd">
                                        <asp:Label ID="Label10" runat="server" Font-Bold="true" Text="Address:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblAddress" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelTd">
                                    </td>
                                    <td>
                                        <asp:Label ID="lblTowncity" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelTd">
                                    </td>
                                    <td>
                                        <asp:Label ID="lblCounty" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelTd">
                                    </td>
                                    <td>
                                        <asp:Label ID="lblPostcode" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="leftDiv">
                            <table style="width: 100%;" cellspacing="10" cellpadding="5">
                                <tr>
                                    <td class="labelTd">
                                        <asp:Label ID="Label13" runat="server" Font-Bold="true" Text="Customer:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblCustomerName" runat="server" Text=""></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelTd">
                                        <asp:Label ID="Label14" runat="server" Font-Bold="true" Text="Telephone:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblCustomerTelephone" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelTd">
                                        <asp:Label ID="Label15" runat="server" Font-Bold="true" Text="Mobile:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblCustomerMobile" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelTd">
                                        <asp:Label ID="Label16" runat="server" Font-Bold="true" Text="Email:"></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="lblCustomerEmail" runat="server"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="labelTd">
                                    </td>
                                    <td>
                                        <div style="text-align: right;">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="clear">
                        </div>
                    </asp:Panel>
                    <div class="clear">
                    </div>
                    <hr />
                    <div class="clear">
                    </div>
                    <asp:Panel ID="pnlNotes" runat="server">
                        <div class="leftDiv">
                            <table style="width: 100%;" cellspacing="10" cellpadding="5">
                                <tr>
                                    <td>
                                        <b>Customer Appointment Notes:</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtCustAppointmentNotes" MaxLength="1000" runat="server" TextMode="MultiLine"
                                            Height="150px" Width="100%"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="leftDiv">
                            <table style="width: 100%;" cellspacing="10" cellpadding="5">
                                <tr>
                                    <td>
                                        <b>Job Sheet Notes:</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtJobSheetNotes" MaxLength="1000" runat="server" TextMode="MultiLine"
                                            Height="150px" Width="100%"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                            <td rowspan="5">
                        </div>
                         <div style="clear: both">
                        <div class="leftDiv">
                            <table>
                                <tr>
                                    <td>
                                        <asp:ListView runat="server" OnItemDataBound="lstViewPhotos_ItemDatabound" ID="lstViewPhotosBefore">
                                            <LayoutTemplate>
                                                <table id="groupPlaceholderContainer" runat="server" frame="box" style="border: 2px solid gray;">
                                                    <tr id="itemPlaceholder" runat="server">
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div style="font-size: 10px;">
                                                                <span class="bold">Before</span>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </LayoutTemplate>
                                            <ItemTemplate>
                                                <tr id="Tr1" runat="server" style="padding: 1px;">
                                                    <td style="padding: 1px;">
                                                        <asp:HiddenField ID="hdnImageName" runat="server" Value='<%# Eval("ImageTitle") %>' />
                                                        <asp:ImageButton ID="imgFaultImage" runat="server" AlternateText='<%# Eval("ImageTitle") %>'
                                                            Width="156px" Height="120px" CommandName="ShowImage" CommandArgument='<%# Eval("ImageTitle")%>'
                                                            ImageUrl='<%# Eval("ImageTitle") %>' />
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <EmptyItemTemplate>
                                            </EmptyItemTemplate>
                                        </asp:ListView>
                                    </td>
                                    <td>
                                        <asp:ListView runat="server" OnItemDataBound="lstViewPhotos_ItemDatabound" ID="lstViewPhotosAfter">
                                            <LayoutTemplate>
                                                <table id="groupPlaceholderContainer" runat="server" frame="box" style="border: 2px solid gray;">
                                                    <tr id="itemPlaceholder" runat="server">
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div style="font-size: 10px;">
                                                                <span class="bold">After</span>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </LayoutTemplate>
                                            <ItemTemplate>
                                                <tr id="Tr2" runat="server">
                                                    <td style="padding: 1px;">
                                                        <asp:HiddenField ID="hdnImageName" runat="server" Value='<%# Eval("ImageTitle") %>' />
                                                        <asp:ImageButton ID="imgFaultImage" runat="server" AlternateText='<%# Eval("ImageTitle") %>'
                                                            Width="156px" Height="120px" CommandName="ShowImage" CommandArgument='<%# Eval("ImageTitle")%>'
                                                            ImageUrl='<%# Eval("ImageTitle") %>' />
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <EmptyItemTemplate>
                                            </EmptyItemTemplate>
                                        </asp:ListView>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div style="clear: both">
                        </div>
                    </asp:Panel>
                    <asp:Label ID="lblPopUpShowImage" runat="server"></asp:Label><ajaxToolkit:ModalPopupExtender
                        ID="mdlPopUpViewImage" runat="server" DynamicServicePath="" Enabled="True" TargetControlID="lblPopUpShowImage"
                        PopupControlID="pnlLargeImage" CancelControlID="btnClose" BackgroundCssClass="modalBackground">
                    </ajaxToolkit:ModalPopupExtender>
                    <asp:Panel ID="pnlLargeImage" runat="server" CssClass="modalPopupImage" Style="display: none;">
                        <div style='width: 100%;'>
                            <asp:ImageButton ID="btnClose" runat="server" Style="position: absolute; top: -17px;
                                right: -16px; width: 22px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
                            <asp:Image ID="imgDynamicImage" runat="server" CssClass="imgDynamicImage" />
                        </div>
                    </asp:Panel>
                </ContentTemplate>
            </asp:UpdatePanel>
        </div>
    </div>
    </form>
</body>
</html>
