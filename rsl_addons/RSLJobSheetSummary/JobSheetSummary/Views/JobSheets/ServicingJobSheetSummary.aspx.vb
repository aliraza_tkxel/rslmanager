﻿Imports System.Threading
Imports JSS_BusinessLogic
Imports JSS_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class ServicingJobSheetSummary
    Inherits PageBase

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If Not IsPostBack Then
                Dim Jsn As String = CType(Request.QueryString(PathConstants.Jsn), String)
                populateJobSheet(Jsn)
            End If
        Catch ex As ThreadAbortException
            uiMessageHelper.message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#Region "Populate Job Sheet"
    ''' <summary>
    ''' Populate Job Sheet Summary page
    ''' </summary>
    ''' <param name="jsn"></param>
    ''' <remarks></remarks>
    Public Sub populateJobSheet(ByVal jsn As String)

        Try
            Dim objJobSheetSummaryBL As New JobSheetSummaryBL
            Dim resultDataset As DataSet = New DataSet()

            objJobSheetSummaryBL.GetGasJobSheetSummaryDetails(jsn, resultDataset)

            Dim asbestosDt As DataTable = resultDataset.Tables("PropertyInfo")
            Dim dtAppointmentsInfo As DataTable = resultDataset.Tables("AppointmentInfo")

            If dtAppointmentsInfo.Rows.Count > 0 Then
                For Each requiredBo As DataRow In dtAppointmentsInfo.Rows
                    Dim newRowWork As New HtmlTableRow()
                    Dim newCellJSVLabel As New HtmlTableCell()
                    newCellJSVLabel.InnerText = "JSG: " + requiredBo.Item("JobsheetNumber").ToString()

                    Dim newCellLocationLabel As New HtmlTableCell()

                    newCellLocationLabel.InnerText = "CP12 Expiry Date: " + requiredBo.Item("CP12Expiry")


                    newCellJSVLabel.Width = "55%"
                    newCellJSVLabel.Style.Add("font-weight", "bold")
                    newCellLocationLabel.Width = "45%"
                    newCellLocationLabel.Style.Add("font-weight", "bold")
                    newRowWork.Cells.Add(newCellJSVLabel)
                    newRowWork.Cells.Add(newCellLocationLabel)
                    tblRequiredWork.Rows.Add(newRowWork)
                Next

                grdAsbestos.DataSource = asbestosDt
                grdAsbestos.DataBind()


                lblSummary.Text = dtAppointmentsInfo.Rows(0).Item("AppointmentType") + " Job Sheet Summary"


                lblStatus.Text = dtAppointmentsInfo.Rows(0).Item("Status")
                lblOperative.Text = dtAppointmentsInfo.Rows(0).Item(ApplicationConstants.OperativeColumn)
                lblTime.Text = dtAppointmentsInfo.Rows(0).Item(ApplicationConstants.StartTimeColumn) + " - " + dtAppointmentsInfo.Rows(0).Item(ApplicationConstants.EndTimeColumn)


                lblDuration.Text = GeneralHelper.appendHourLabel(1)
                lblCp12Issued.Text = dtAppointmentsInfo.Rows(0)("CP12Issued")
                lblCP12Expiry.Text = dtAppointmentsInfo.Rows(0)("CP12Expiry")

                Dim startDateTime As DateTime = Convert.ToDateTime(dtAppointmentsInfo.Rows(0).Item(ApplicationConstants.StartDateColumn))
                Dim startDay As String = startDateTime.DayOfWeek.ToString()
                Dim startDate As String = GeneralHelper.getOrdinal(Convert.ToInt32(startDateTime.ToString("dd")))
                Dim startMonth As String = startDateTime.ToString("MMMMMMMMMMMMM")
                Dim startYear As String = startDateTime.ToString("yyyy")

                Dim endDateTime As DateTime = Convert.ToDateTime(dtAppointmentsInfo.Rows(0).Item(ApplicationConstants.EndDateColumn))
                Dim endDay As String = endDateTime.DayOfWeek.ToString()
                Dim endDate As String = GeneralHelper.getOrdinal(Convert.ToInt32(endDateTime.ToString("dd")))
                Dim endMonth As String = endDateTime.ToString("MMMMMMMMMMMMM")
                Dim endYear As String = endDateTime.ToString("yyyy")

                lblStartDate.Text = startDay + " " + startDate + " " + startMonth + " " + startYear
                lblEndDate.Text = endDay + " " + endDate + " " + endMonth + " " + endYear


                If (IsDBNull(dtAppointmentsInfo.Rows(0).Item(ApplicationConstants.CustomerNotesColumn))) Then
                    txtCustAppointmentNotes.Text = String.Empty
                Else
                    txtCustAppointmentNotes.Text = dtAppointmentsInfo.Rows(0).Item(ApplicationConstants.CustomerNotesColumn)
                End If




                Dim propertyId As String = dtAppointmentsInfo.Rows(0).Item(ApplicationConstants.PropertyIdColumn)
                ViewState.Add(ViewStateConstants.PropertyId, propertyId)

                'Property Information
                lblScheme.Text = dtAppointmentsInfo.Rows(0)("SCHEMENAME")
                lblAddress.Text = dtAppointmentsInfo.Rows(0)("Address")
                lblTowncity.Text = dtAppointmentsInfo.Rows(0)("TownCity")
                lblCounty.Text = dtAppointmentsInfo.Rows(0)("COUNTY")
                lblPostcode.Text = dtAppointmentsInfo.Rows(0)("POSTCODE")

                'Customer Information

                lblCustomerName.Text = dtAppointmentsInfo.Rows(0)("ClientName")
                lblCustomerTelephone.Text = dtAppointmentsInfo.Rows(0)("ClientTel")
                lblCustomerMobile.Text = dtAppointmentsInfo.Rows(0)("ClientMobile")
                lblCustomerEmail.Text = dtAppointmentsInfo.Rows(0)("ClientEmail")
            Else
                lblMessage.Text = "No Record Found"
            End If
        Catch ex As ThreadAbortException
            uiMessageHelper.message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub
#End Region
End Class