﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="JSVJobSheetSummary.aspx.vb"
    Inherits="JobSheetSummary.JSVJobSheetSummary" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html>
<html>
<head id="Head1" runat="server">
    <link href="../../Styles/default.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../../Styles/JobSheet.css" />
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 44%;
        }
        .style2
        {
            width: 13%;
        }
        .headingTitle
        {
            background: #000000;
            color: #ffffff;
            width: 98%;
            height: 20px;
            padding-left: 20px;
            padding-top: 10px;
            padding-bottom: 10px;
            padding-right: 12px;
            margin-bottom: 10px;
        }
        .leftDiv
        {
            float: left;
            margin-left: 10px;
            width: 48%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">

     <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>

    <div>
        <asp:Panel ID="pnlMessage" runat="server" Visible="False">
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
            <br />
        </asp:Panel>
        <div class="mainContainer">
            <div class="headingTitle">
                <b>
                    <asp:Label Text="" ID="lblSummary" runat="server" /></b>
            </div>
           
            <br />
            <div style="padding: 10px; border: 1px solid;">
                <table runat="server" id="tblRequiredWork" width="90%">
                </table>
            </div>
            <br />
            <br />
            <asp:Panel ID="pnlAppointmentDetail" runat="server">
                <div class="leftDiv">
                    <table style="width: 100%;" cellspacing="10" cellpadding="5">
                      
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label2" runat="server" Text="Operative:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblOperative" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label3" runat="server" Text="Duration:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblDuration" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <%--      <tr>
                                <td class="labelTd">
                                    <asp:Label ID="Label4" runat="server" Text="Duration Total:"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblDurationTotal" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>--%>
                    </table>
                </div>
                <div class="leftDiv">
                    <table style="width: 100%;" cellspacing="10" cellpadding="5">
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label23" runat="server" Text="Status:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label5" runat="server" Text="Time:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblTime" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label6" runat="server" Text="Start Date:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblStartDate" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label7" runat="server" Text="End Date:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblEndDate" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <div style="clear: both">
            </div>
            <hr />
            <div style="clear: both">
            </div>
            <asp:Panel ID="pnlPropertyDetail" runat="server">
                <div class="leftDiv">
                    <table style="width: 100%;" cellspacing="10" cellpadding="5">
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label8" runat="server" Font-Bold="true" Text="Scheme:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblScheme" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label9" runat="server" Font-Bold="true" Text="Address:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblAddress" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label10" runat="server" Text=""></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblTowncity" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label11" runat="server" Text=""></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblCounty" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label12" runat="server" Text=""></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblPostcode" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="leftDiv">
                    <table style="width: 100%;" cellspacing="10" cellpadding="5">
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label13" runat="server" Text="Customer:"></asp:Label>
                            </td>
                            <td>
                               
                                <asp:Label ID="lblCustomerName" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label14" runat="server" Text="Telephone:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblCustomerTelephone" runat="server"></asp:Label>
                               
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label15" runat="server" Text="Mobile:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblCustomerMobile" runat="server"></asp:Label>
                               
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label16" runat="server" Text="Email:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblCustomerEmail" runat="server"></asp:Label>
                                
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label17" runat="server" Text=""></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="Label18" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="clear: both">
                </div>
                <div style="text-align: right;">
                </div>
            </asp:Panel>
            <div style="clear: both">
            </div>
            <hr />
            <div style="clear: both">
            </div>
            <asp:Panel ID="pnlNotes" runat="server">
                <div class="leftDiv">
                    <table style="width: 100%;" cellspacing="10" cellpadding="5">
                        <tr>
                            <td>
                                <asp:Label ID="Label19" Font-Bold="true" runat="server" Text="Customer Appointment Notes:"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtCustAppointmentNotes" MaxLength="1000" runat="server" TextMode="MultiLine"
                                    Height="150px" Width="100%" ReadOnly="true"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="leftDiv">
                    <table style="width: 100%;" cellspacing="10" cellpadding="5">
                        <tr>
                            <td>
                                <asp:Label ID="lblJobSheetNotes" Font-Bold="true" runat="server" Text="Job Sheet Notes:"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtJobSheetNotes" MaxLength="1000" runat="server" TextMode="MultiLine"
                                    Height="150px" Width="100%" ReadOnly="true"></asp:TextBox>
                            </td>                           
                        </tr>
                    </table>
                </div>
                <div class="leftDiv">
                
                <table>
                
                <tr>
                
                 <td>
                            
                            
                             <asp:ListView runat="server" OnItemDataBound="lstViewPhotos_ItemDatabound" ID="lstViewPhotosBefore">
                                            <LayoutTemplate>
                                                <table id="groupPlaceholderContainer" runat="server" frame="box" style="border: 2px solid gray;">
                                                    <tr id="itemPlaceholder" runat="server">
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div style="font-size: 10px;">
                                                                <span class="bold">Before</span> </div></td></tr></table></LayoutTemplate><ItemTemplate>
                                                <tr id="Tr1" runat="server" style="padding: 1px;">
                                                    <td style="padding: 1px;">
                                                        <asp:HiddenField ID="hdnImageName" runat="server" Value='<%# Eval("ImageName") %>' />
                                                        <asp:ImageButton ID="imgFaultImage" runat="server" AlternateText='<%# Eval("ImageName") %>'
                                                            Width="156px" Height="120px" CommandName="ShowImage" CommandArgument='<%# Eval("ImageName")%>' ImageUrl='<%# Eval("ImageName") %>'/>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <EmptyItemTemplate>
                                            </EmptyItemTemplate>
                                        </asp:ListView>
                                    </td>
                                    <td>
                                        <asp:ListView runat="server" OnItemDataBound="lstViewPhotos_ItemDatabound" ID="lstViewPhotosAfter">
                                            <LayoutTemplate>
                                                <table id="groupPlaceholderContainer" runat="server" frame="box" style="border: 2px solid gray;">
                                                    <tr id="itemPlaceholder" runat="server">
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div style="font-size: 10px;">
                                                                <span class="bold">After</span> </div></td></tr></table></LayoutTemplate><ItemTemplate>
                                                <tr id="Tr2" runat="server">
                                                    <td style="padding: 1px;">
                                                        <asp:HiddenField ID="hdnImageName" runat="server" Value='<%# Eval("ImageName") %>' />
                                                        <asp:ImageButton ID="imgFaultImage" runat="server" AlternateText='<%# Eval("ImageName") %>'
                                                            Width="156px" Height="120px" CommandName="ShowImage" CommandArgument='<%# Eval("ImageName")%>' ImageUrl='<%# Eval("ImageName") %>'/>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <EmptyItemTemplate>
                                            </EmptyItemTemplate>
                                        </asp:ListView>

                            
                            </td>
                
                </tr>
                

                </table>
                
                </div>
            </asp:Panel>

            <asp:Label ID="lblPopUpShowImage" runat="server"></asp:Label>
            <ajaxToolkit:ModalPopupExtender
                ID="mdlPopUpViewImage" runat="server" DynamicServicePath="" Enabled="True" TargetControlID="lblPopUpShowImage"
                PopupControlID="pnlLargeImage" CancelControlID="btnClose" BackgroundCssClass="modalBackground">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnlLargeImage" runat="server" CssClass="modalPopupImage" Style="display: none;">
                <div style='width: 100%;'>
                    <asp:ImageButton ID="btnClose" runat="server" Style="position: absolute; top: -17px;
                        right: -16px; width: 22px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
                    <asp:Image ID="imgDynamicImage" runat="server" CssClass="imgDynamicImage" />
                </div>
            </asp:Panel>

            <div style="clear: both">
            </div>
            <div style="text-align: right;">
            </div>
            <br />
            <div style="text-align: right; width: 100%;">
                &nbsp &nbsp &nbsp &nbsp
            </div>
        </div>
    </div>
    </form>
</body>
</html>
