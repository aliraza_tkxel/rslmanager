﻿Imports JSS_BusinessLogic
Imports JSS_BusinessObject
Imports JSS_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling

Public Class SbJobSheetSummary
    Inherits PageBase

    Public uploadedImageUri As String

#Region "Events"

#Region "Page load Event"

    ''' <summary>
    ''' Page load Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            uiMessageHelper.resetMessage(lblMessage, pnlMessage)

            If Not IsPostBack Then
                Me.getSetQueryStringParams()
                If uiMessageHelper.IsError = False Then
                    Dim jobSheetNumber As String = SessionManager.getSbJobSheetNumber()
                    populateJobSheetDetail(jobSheetNumber)
                End If

            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "lstView Photos Item Command"
    ''' <summary>
    ''' lstView Photos Item Command
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lstViewPhotos_ItemCommand(sender As Object, e As System.Web.UI.WebControls.ListViewCommandEventArgs) Handles lstViewPhotosBefore.ItemCommand, lstViewPhotosAfter.ItemCommand
        Try
            If e.CommandName = "ShowImage" Then
                Dim fileName As String = e.CommandArgument.ToString()
                imgDynamicImage.ImageUrl = fileName
                'mdlPopUpViewImage.X = 100
                'mdlPopUpViewImage.Y = 20
                mdlPopUpViewImage.Show()
            End If
        Catch ex As Exception
            ExceptionHelper.IsError = True
            ExceptionHelper.ExcpetionMessage = ex.Message
            If ExceptionHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If ExceptionHelper.IsError = True Then
                ExceptionHelper.setMessage(lblMessage, pnlMessage, ExceptionHelper.ExcpetionMessage, True)
            End If
        End Try
    End Sub
#End Region

#Region "lstView Photos Item data bound"
    ''' <summary>
    ''' lstView Photos Item data bound
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lstViewPhotos_ItemDatabound(ByVal sender As Object, ByVal e As ListViewItemEventArgs) Handles lstViewPhotosBefore.ItemDataBound, lstViewPhotosAfter.ItemDataBound
        Try

            If e.Item.ItemType = ListViewItemType.DataItem Then

                Dim imgFaultImage As Image = CType(e.Item.FindControl("imgFaultImage"), Image)
                Dim hdnImageName As HiddenField = CType(e.Item.FindControl("hdnImageName"), HiddenField)
                imgFaultImage.ImageUrl = hdnImageName.Value

            End If

        Catch ex As Exception
            ExceptionHelper.IsError = True
            ExceptionHelper.ExcpetionMessage = ex.Message
            If ExceptionHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If ExceptionHelper.IsError = True Then
                ExceptionHelper.setMessage(lblMessage, pnlMessage, ExceptionHelper.ExcpetionMessage, True)
            End If
        End Try
    End Sub
#End Region

#End Region

#Region "Functions"

#Region "Get Existed Images"
    ''' <summary>
    ''' Get Existed Images
    ''' </summary>
    ''' <param name="dtImages"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function getExistedImages(ByVal dtImages As DataTable) As DataTable

        For Each dr As DataRowView In dtImages.DefaultView
            Dim imageName As String = dr("ImageName").ToString()
            

            Dim fullPath As String = getFilePath(SessionManager.getSchemeId, SessionManager.getBlockId) + imageName

            'If Not System.IO.File.Exists(Server.MapPath(fullPath)) Then
            '    dr.Row.Delete()
            'End If
            dr("ImageName") = fullPath
        Next

        dtImages.AcceptChanges()

        Return dtImages

    End Function

#End Region

#Region "Populate fault images"
    ''' <summary>
    ''' Populate fault images
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateJobSheetImages()

        Dim resultDataSet As DataSet = New DataSet()
        Dim objJobSheetSummaryBL As New JobSheetSummaryBL
        Dim jsn As String = lblFaultId.Text
        Dim propertyId As String = SessionManager.getPropertyId()

        objJobSheetSummaryBL.getSbJobSheetImages(resultDataSet, jsn)

        Dim exsistingImagesdt = getExistedImages(resultDataSet.Tables(0))
        lstViewPhotosBefore.DataSource = New DataView(exsistingImagesdt, "Status = 'Before'", "", DataViewRowState.CurrentRows)
        lstViewPhotosBefore.DataBind()

        lstViewPhotosAfter.DataSource = New DataView(exsistingImagesdt, "Status = 'After'", "", DataViewRowState.CurrentRows)
        lstViewPhotosAfter.DataBind()

    End Sub

#End Region

#Region "Populate Job Sheet Details"
    ''' <summary>
    ''' Get Job Sheet detail from Data Source and populate 
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub populateJobSheetDetail(ByVal jsn As String)

        Try
            ' Reset Job sheet controls
            resetJobsheetControls()

            Dim resultDataSet As DataSet = New DataSet()
            Dim objJobSheetSummaryBL As New JobSheetSummaryBL
            objJobSheetSummaryBL.getSbJobSheetDetails(resultDataSet, jsn)

            '1- Job Sheet Detail
            If (resultDataSet.Tables(0).Rows.Count = 0) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
            Else
                With resultDataSet.Tables(ApplicationConstants.JobSheetAndAppointmentDetailsDtName).Rows(0)
                    lblFaultId.Text = .Item("JSN").ToString
                    lblContractor.Text = .Item("ContractorName").ToString
                    lblOperativeName.Text = .Item("OperativeName").ToString
                    lblFaultPriority.Text = .Item("Priority").ToString
                    lblFaultCompletionDue.Text = .Item("CompletionDue").ToString
                    lblReportedDate.Text = .Item("ReportedDate").ToString
                    lblFaultLocation.Text = .Item("Location").ToString
                    lblFaultDescription.Text = .Item("Description").ToString
                    lblFaultOperatorNote.Text = .Item("FaultNotes").ToString
                    lblTrade.Text = .Item("Trade").ToString
                    lblAppointment.Text = .Item("AppointmentTimeDate").ToString
                    lblFollowOnWork.Text = .Item("FollowOnNotes").ToString
                    lblCompletionDate.Text = .Item("CompletionDateTime").ToString
                    SessionManager.setSchemeId(.Item("SchemeId"))
                    SessionManager.setBlockId(.Item("BlockId"))
                    lblFollowOnMessage.Visible = False
                    If Not IsDBNull(.Item("FollowOnFaultLogId")) Then
                        Dim followOnFaultLogId As Integer = .Item("FollowOnFaultLogId")
                        lnkFollowOnFaultLogId.Text = followOnFaultLogId
                        lnkFollowOnFaultLogId.NavigateUrl = String.Format("{0}?{1}={2}", PathConstants.SbJobSheetSummary, PathConstants.Jsn, followOnFaultLogId)
                        lblFollowOnMessage.Visible = True
                    End If

                    'Populate Job Sheet Images - Only get images when valid job sheet is available.
                    populateJobSheetImages()
                End With
            End If

            '2- Customer Info
            If (resultDataSet.Tables(1).Rows.Count = 0) Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)

            Else
                ' Body customer Labels

                lblSchemeName.Text = resultDataSet.Tables(1).Rows(0)(0).ToString
                lblBlockName.Text = resultDataSet.Tables(1).Rows(0)(1).ToString
                lblAddress.Text = resultDataSet.Tables(1).Rows(0)(2).ToString
                lblTown.Text = resultDataSet.Tables(1).Rows(0)(3).ToString
                lblCounty.Text = resultDataSet.Tables(1).Rows(0)(4).ToString
                lblPostCode.Text = resultDataSet.Tables(1).Rows(0)(5).ToString
               

            End If

            '3- Repair Detail
            If (Not IsNothing(resultDataSet.Tables(ApplicationConstants.RepairDetailsDtname))) Then
                grdRepairDetail.DataSource = resultDataSet.Tables(ApplicationConstants.RepairDetailsDtname)
                grdRepairDetail.DataBind()
                With resultDataSet.Tables(ApplicationConstants.RepairDetailsDtname)
                    If (.Rows.Count > 0) Then
                        txtBoxRepairNotes.Text = .Rows(0)("RepairNotes").ToString()
                    End If
                End With
            End If

            '4- Job Sheet Activities Detail
            If (Not IsNothing(resultDataSet.Tables(ApplicationConstants.JobSheetActivitiesDtName))) Then
                grdJobSheetActivities.DataSource = resultDataSet.Tables(ApplicationConstants.JobSheetActivitiesDtName)
                grdJobSheetActivities.DataBind()
            End If


            '5- Asbestos Detail(s)
            If (Not IsNothing(resultDataSet.Tables(ApplicationConstants.AsbestosDtname))) Then
                grdAsbestos.DataSource = resultDataSet.Tables(ApplicationConstants.AsbestosDtname)
                grdAsbestos.DataBind()
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Reset Job sheet control "
    ''' <summary>
    ''' Reset Job sheet control
    ''' </summary>
    ''' <remarks></remarks>
    Protected Sub resetJobsheetControls()

        ' Job Sheet Detail
        lblFaultId.Text = String.Empty
        lblContractor.Text = String.Empty
        lblOperativeName.Text = String.Empty
        lblFaultPriority.Text = String.Empty
        lblFaultCompletionDue.Text = String.Empty
        lblReportedDate.Text = String.Empty
        lblFaultLocation.Text = String.Empty
        lblFaultDescription.Text = String.Empty
        lblFaultOperatorNote.Text = String.Empty
        lblTrade.Text = String.Empty
        txtBoxRepairNotes.Text = String.Empty

        ' Body customer Labels

        lblSchemeName.Text = String.Empty
        lblBlockName.Text = String.Empty
        lblAddress.Text = String.Empty
        lblTown.Text = String.Empty
        lblCounty.Text = String.Empty
        lblPostCode.Text = String.Empty

        grdRepairDetail.DataSource = Nothing

        lblFollowOnWork.Text = String.Empty

    End Sub

#End Region

#Region "get Set Query String Params"
    Private Sub getSetQueryStringParams()
        If Not IsNothing(Request.QueryString(PathConstants.Jsn)) Then
            SessionManager.setSbJobSheetNumber(Convert.ToString(Request.QueryString(PathConstants.Jsn)))
        Else
            uiMessageHelper.IsError = True
            uiMessageHelper.message = UserMessageConstants.InvalidJobSheetNumber
        End If
    End Sub
#End Region


    Private Function getFilePath(ByVal schemeId As Integer, ByVal blockId As Integer) As String
        Dim requestURL = HttpContext.Current.Request.Url
        Dim imagePath As String = String.Empty
        If blockId > 0 Then
            imagePath = requestURL.GetLeftPart(UriPartial.Authority) + "/PDRDocuments/Blocks/" + blockId.ToString + "/Images/"
        Else
            imagePath = requestURL.GetLeftPart(UriPartial.Authority) + "/PDRDocuments/Schemes/" + schemeId.ToString + "/Images/"
        End If

        Return imagePath
    End Function
#End Region

End Class
