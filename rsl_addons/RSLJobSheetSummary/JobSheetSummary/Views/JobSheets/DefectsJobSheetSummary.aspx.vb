﻿Imports JSS_Utilities
Imports JSS_BusinessLogic
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Threading

Public Class DefectsJobSheetSummary
    Inherits PageBase

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                Dim Jsn As String = CType(Request.QueryString(PathConstants.Jsn), String)
                populateJobSheet(Jsn)
            End If
        Catch ex As ThreadAbortException
            uiMessageHelper.message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub


#Region "Populate Arranged JobSheet"

    Private Sub populateJobSheet(jsn As String)

        Dim objJobSheetSummaryBL As New JobSheetSummaryBL
        Dim jobSheetDataSet As DataSet = New DataSet

        txtCustAppointmentNotes.Attributes.Add("readonly", "readonly")
        txtJobSheetNotes.Attributes.Add("readonly", "readonly")

        objJobSheetSummaryBL.GetDefectJobSheetSummaryDetails(jsn, jobSheetDataSet)

        If jobSheetDataSet.Tables.Count > 0 AndAlso jobSheetDataSet.Tables(0).Rows.Count > 0 Then
            grdDefects.DataSource = jobSheetDataSet.Tables(0)
            grdDefects.DataBind()

            ' lblJSD.Text = Right("00000" + jobSheetDataSet.Tables(0).Rows(0)("DefectId:").ToString(), 5)
        Else
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.NoRecordFound, True)
        End If

        If jobSheetDataSet.Tables.Count > 1 AndAlso jobSheetDataSet.Tables(1).Rows.Count > 0 Then
            Dim drAppointmentInfo = jobSheetDataSet.Tables(1).Rows(0)
            SessionManager.setPropertyId(drAppointmentInfo("PropertyId"))
            lblInspectionRef.Text = drAppointmentInfo("InspectionRef")
            lblOperative.Text = drAppointmentInfo("Operative")
            lblDuration.Text = GeneralHelper.appendHourLabel(drAppointmentInfo("Duration"))
            lblTrade.Text = drAppointmentInfo("Trade")

            lblStartDateTime.Text = String.Format("{0:HH:mm dddd dd MM yyyy}", drAppointmentInfo("StartDateTime"))
            lblEndDateTime.Text = String.Format("{0:HH:mm dddd dd MM yyyy}", drAppointmentInfo("EndDateTime"))
            txtCustAppointmentNotes.Text = drAppointmentInfo("AppointmentNotes")
            txtJobSheetNotes.Text = drAppointmentInfo("JobSheetNotes")
        End If

        If jobSheetDataSet.Tables.Count > 2 AndAlso jobSheetDataSet.Tables(2).Rows.Count > 0 Then
            bindPropertyInfo(jobSheetDataSet.Tables(2))
        End If

        populateJobSheetImages()

    End Sub

#End Region

#Region "Populate Defect images"
    ''' <summary>
    ''' Populate fault images
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateJobSheetImages()

        Dim resultDataSet As DataSet = New DataSet()
        Dim objJobSheetSummaryBL As New JobSheetSummaryBL

        Dim Jsn As String = CType(Request.QueryString(PathConstants.Jsn), String)

        Dim defectIdString As String = Jsn.Substring(4, 4)


        Dim defectID As Integer
        Int32.TryParse(defectIdString, defectID)

        Dim propertyId As String = SessionManager.getPropertyId()

        objJobSheetSummaryBL.getDefectJobSheetImages(resultDataSet, defectID)

        Dim exsistingImagesdt = getExistedImages(resultDataSet.Tables(0))
        lstViewPhotosBefore.DataSource = New DataView(exsistingImagesdt, "Status = 'Before'", "", DataViewRowState.CurrentRows)
        lstViewPhotosBefore.DataBind()

        lstViewPhotosAfter.DataSource = New DataView(exsistingImagesdt, "Status = 'After'", "", DataViewRowState.CurrentRows)
        lstViewPhotosAfter.DataBind()

    End Sub

#End Region

#Region "lstView Photos Item data bound"
    ''' <summary>
    ''' lstView Photos Item data bound
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lstViewPhotos_ItemDatabound(ByVal sender As Object, ByVal e As ListViewItemEventArgs) Handles lstViewPhotosBefore.ItemDataBound, lstViewPhotosAfter.ItemDataBound
        Try

            If e.Item.ItemType = ListViewItemType.DataItem Then

                Dim imgFaultImage As Image = CType(e.Item.FindControl("imgFaultImage"), Image)
                Dim hdnImageName As HiddenField = CType(e.Item.FindControl("hdnImageName"), HiddenField)
                imgFaultImage.ImageUrl = hdnImageName.Value

            End If

        Catch ex As Exception
            ExceptionHelper.IsError = True
            ExceptionHelper.ExcpetionMessage = ex.Message
            If ExceptionHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If ExceptionHelper.IsError = True Then
                ExceptionHelper.setMessage(lblMessage, pnlMessage, ExceptionHelper.ExcpetionMessage, True)
            End If
        End Try
    End Sub
#End Region



#Region "lstView Photos Item Command"
    ''' <summary>
    ''' lstView Photos Item Command
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lstViewPhotos_ItemCommand(sender As Object, e As System.Web.UI.WebControls.ListViewCommandEventArgs) Handles lstViewPhotosBefore.ItemCommand, lstViewPhotosAfter.ItemCommand
        Try
            If e.CommandName = "ShowImage" Then
                Dim fileName As String = e.CommandArgument.ToString()
                imgDynamicImage.ImageUrl = fileName
                'mdlPopUpViewImage.X = 100
                'mdlPopUpViewImage.Y = 20
                mdlPopUpViewImage.Show()
            End If
        Catch ex As Exception
            ExceptionHelper.IsError = True
            ExceptionHelper.ExcpetionMessage = ex.Message
            If ExceptionHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If ExceptionHelper.IsError = True Then
                ExceptionHelper.setMessage(lblMessage, pnlMessage, ExceptionHelper.ExcpetionMessage, True)
            End If
        End Try
    End Sub
#End Region


#Region "Get Existed Images"
    ''' <summary>
    ''' Get Existed Images
    ''' </summary>
    ''' <param name="dtImages"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function getExistedImages(ByVal dtImages As DataTable) As DataTable

        For Each dr As DataRowView In dtImages.DefaultView
            Dim imageName As String = dr("ImageTitle").ToString()


            Dim fullPath As String = getLogicalPropertyImagePath(SessionManager.getPropertyId, imageName)

            'If Not System.IO.File.Exists(Server.MapPath(fullPath)) Then
            '    dr.Row.Delete()
            'End If
            dr("ImageTitle") = fullPath
        Next

        dtImages.AcceptChanges()

        Return dtImages

    End Function

#End Region

  Public Shared Function getLogicalPropertyImagePath(propertyId As String, imageName As String) As String
        Dim requestURL = HttpContext.Current.Request.Url
        Dim imagePath As String = Convert.ToString((Convert.ToString(requestURL.GetLeftPart(UriPartial.Authority) + "/PropertyImages/") & propertyId) + "/Images/") & imageName

        Return imagePath
    End Function
#Region "Bind Property Info"

    Private Sub bindPropertyInfo(propertyDt As DataTable)
        Dim tenancyId As Integer
        'Property Information
        lblScheme.Text = propertyDt.Rows(0)("SchemeName")
        lblAddress.Text = propertyDt.Rows(0)("HOUSENUMBER") + " " + propertyDt.Rows(0)("ADDRESS1") + " " + propertyDt.Rows(0)("ADDRESS2")
        lblTowncity.Text = propertyDt.Rows(0)("TOWNCITY")
        lblCounty.Text = propertyDt.Rows(0)("COUNTY")
        lblPostcode.Text = propertyDt.Rows(0)("POSTCODE")

        'Customer Information

        lblCustomerName.Text = propertyDt.Rows(0)("TenantName")
        lblCustomerTelephone.Text = propertyDt.Rows(0)("Telephone")
        lblCustomerMobile.Text = propertyDt.Rows(0)("Mobile")
        lblCustomerEmail.Text = propertyDt.Rows(0)("Email")
        tenancyId = propertyDt.Rows(0)("TenancyId")


    End Sub

#End Region
End Class