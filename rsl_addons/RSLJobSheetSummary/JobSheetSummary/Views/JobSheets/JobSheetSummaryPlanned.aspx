﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="JobSheetSummaryPlanned.aspx.vb"
    Inherits="JobSheetSummary.JobSheetSummaryPlanned" %>

<!DOCTYPE html>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<html>
<head id="Head1" runat="server">
    <link href="../../Styles/default.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../../Styles/JobSheet.css" />
    <title></title>
    <style type="text/css">
        .style1
        {
            width: 44%;
        }
        .style2
        {
            width: 13%;
        }
        .headingTitle
        {
            background: #000000;
            color: #ffffff;
            width: 98%;
            height: 20px;
            padding-left: 20px;
            padding-top: 10px;
            padding-bottom: 10px;
            padding-right: 12px;
            margin-bottom: 10px;
        }
        .leftDiv
        {
            float: left;
            margin-left: 10px;
            width: 48%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Panel ID="pnlMessage" runat="server" Visible="False">
            <asp:Label ID="lblMessage" runat="server"></asp:Label>
            <br />
        </asp:Panel>
        <div class="mainContainer">
            <div class="headingTitle">
                <b>
                    <asp:Label Text="" ID="lblSummary" runat="server" />
                </b>
            </div>
            <div style="text-align: right;">
                &nbsp;<asp:Button ID="btnPrevious" runat="server" Text="&lt; Previous" Width="100px" Enabled="false" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnNext" runat="server" Text="Next >" Width="100px" Enabled="false"  /></div>
            <br />
            <asp:Panel ID="pnlHeader" Width="100%" runat="server">
                <asp:Label ID="Label1" runat="server" CssClass="leftControl" Font-Bold="True" Text=""></asp:Label>
                
                <asp:Label ID="lblSheetNumber" runat="server" CssClass="rightControl" Font-Bold="true"
                    Text=""></asp:Label>
                <asp:Label ID="Label21" runat="server" CssClass="rightControl" Font-Bold="true" Text="of"></asp:Label>
                <asp:Label ID="lblTotalSheets" runat="server" CssClass="rightControl" Font-Bold="true"
                    Text=""></asp:Label>
                <asp:Label ID="Label33" runat="server" CssClass="rightControl" Font-Bold="true" Text="Job Sheet"></asp:Label>
            </asp:Panel>
            <br />
            <hr />
            <br />
            <div style="padding: 10px; border: 1px solid;">
                <table>
                    <tr>
                        <td style="width: 20%;">
                            PMO: &nbsp &nbsp
                            <asp:Label ID="lblPmo" runat="server" Text=""></asp:Label>
                        </td>
                        <td style="width: 60%;">
                            Planned works component:
                            <asp:Label ID="lblComponent" runat="server" Text=""></asp:Label>
                        </td>
                        <td style="width: 20%;">
                            Trade: &nbsp &nbsp
                            <asp:Label ID="lblTrade" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
            <br />
            <br />
            <asp:Panel ID="pnlAppointmentDetail" runat="server">
                <div class="leftDiv">
                    <table style="width: 100%;" cellspacing="10" cellpadding="5">
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label22" runat="server" Text="JSN:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblJsn" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label2" runat="server" Text="Operative:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblOperative" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label3" runat="server" Text="Duration:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblDuration" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <%--      <tr>
                                <td class="labelTd">
                                    <asp:Label ID="Label4" runat="server" Text="Duration Total:"></asp:Label>
                                </td>
                                <td>
                                    <asp:Label ID="lblDurationTotal" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>--%>
                    </table>
                </div>
                <div class="leftDiv">
                    <table style="width: 100%;" cellspacing="10" cellpadding="5">
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label23" runat="server" Text="Status:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblStatus" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label5" runat="server" Text="Time:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblTime" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label6" runat="server" Text="Start Date:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblStartDate" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="Label7" runat="server" Text="End Date:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblEndDate" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <div style="clear: both">
            </div>
            <hr />
            <div style="clear: both">
            </div>
            <asp:Panel ID="pnlPropertyDetail" runat="server">
                <div class="leftDiv">
                    <table style="width: 100%;" cellspacing="10" cellpadding="5">
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="lblForScheme" runat="server" Font-Bold="true" Text="Scheme:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblScheme" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                         <tr>
                            <td class="labelTd">
                                <asp:Label ID="lblForBlock" runat="server" Font-Bold="true" Text="Block:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblBlock" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="lblForAddress" runat="server" Font-Bold="true" Text="Address:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblAddress" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="lblForTowncity" runat="server" Text=""></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblTowncity" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="lblForCounty" runat="server" Text=""></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblCounty" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="lblForPostcode" runat="server" Text=""></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblPostcode" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="leftDiv">
                    <table style="width: 100%;" cellspacing="10" cellpadding="5">
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="lblForCustomerName" runat="server" Text="Customer:"></asp:Label>
                            </td>
                            <td>
                                <asp:HiddenField ID="hdnCustomerId" runat="server" />
                                <asp:Label ID="lblCustomerName" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="lblForCustomerTelephone" runat="server" Text="Telephone:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblCustomerTelephone" runat="server"></asp:Label>
                                <asp:TextBox ID="txtCustomerTelephone" runat="server" Visible="False"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="lblForCustomerMobile" runat="server" Text="Mobile:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblCustomerMobile" runat="server"></asp:Label>
                                <asp:TextBox ID="txtCustomerMobile" runat="server" Visible="False"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="lblForCustomerEmail" runat="server" Text="Email:"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblCustomerEmail" runat="server"></asp:Label>
                                <asp:TextBox ID="txtCustomerEmail" runat="server" Visible="False"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td class="labelTd">
                                <asp:Label ID="LabelFor18" runat="server" Text=""></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="Label18" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="clear: both">
                </div>
                <div style="text-align: right;">
                </div>
            </asp:Panel>
            <div style="clear: both">
            </div>
            <hr />
            <div style="clear: both">
            </div>
            <asp:Panel ID="pnlNotes" runat="server">
                <div class="leftDiv">
                    <table style="width: 100%;" cellspacing="10" cellpadding="5">
                        <tr>
                            <td>
                                <asp:Label ID="Label19" Font-Bold="true" runat="server" Text="Customer Appointment Notes:"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtCustAppointmentNotes" MaxLength="1000" runat="server" TextMode="MultiLine"
                                    Height="150px" Width="100%" ReadOnly="true"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="leftDiv">
                    <table style="width: 100%;" cellspacing="10" cellpadding="5">
                        <tr>
                            <td>
                                <asp:Label ID="lblJobSheetNotes" Font-Bold="true" runat="server" Text="Job Sheet Notes:"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtJobSheetNotes" MaxLength="1000" runat="server" TextMode="MultiLine"
                                    Height="150px" Width="100%" ReadOnly="true"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <div style="clear: both">
            </div>
            <div style="text-align: right;">
            </div>
            <br />
            <div style="text-align: right; width: 100%;">
                &nbsp &nbsp &nbsp &nbsp
            </div>
        </div>
    </div>
    </form>
</body>
</html>
