﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPages/JSS.Master"
    CodeBehind="SbJobSheetSummary.aspx.vb" Inherits="JobSheetSummary.SbJobSheetSummary" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">
    <link rel="stylesheet" type="text/css" href="../../Styles/JobSheet.css" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="server">
    <asp:UpdatePanel runat="server" ID="updPnlCompleteJobsheet">
        <ContentTemplate>
            <div style="height: auto; overflow: auto; clear: both; padding: 10px; width: auto;">
                <table class="jobsheet_detail_table">
                    <tr>
                        <td colspan="3">
                            <div class="header-Popup">
                                <p class="header_label">
                                    BHG Fault Locator
                                </p>
                                <div class="header_box">
                                </div>
                            </div>
                            <br />
                            <asp:Panel ID="pnlMessage" runat="server" Visible="False">
                                <asp:Label ID="lblMessage" runat="server"></asp:Label>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <span class="bold">Job sheet summary for :
                                <asp:Label ID="lblFaultId" runat="server" ClientIDMode="Static"></asp:Label>
                                <asp:Label ID="lblFollowOnMessage" runat="server" Visible="false">(Follow On Works for
                                    <asp:HyperLink ID="lnkFollowOnFaultLogId" NavigateUrl="#" runat="server" Target="_blank"
                                        Text="" ForeColor="#175778" Font-Underline="true" />
                                    ) </asp:Label></span></td></tr><tr>
                        <td colspan="3">
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Contractor : </td><td>
                            <asp:Label ID="lblContractor" runat="server"></asp:Label></td><td>
                            <asp:Label ID="lblSchemeName" runat="server" CssClass="bold"></asp:Label></td></tr><tr>
                        <td>
                            Priority : </td><td>
                            <asp:Label ID="lblFaultPriority" runat="server"></asp:Label></td><td>
                            <asp:Label ID="lblBlockName" runat="server"></asp:Label></td></tr><tr>
                        <td>
                            Completion Due : </td><td>
                            <asp:Label ID="lblFaultCompletionDue" runat="server"></asp:Label></td><td>
                            <asp:Label ID="lblAddress" runat="server"></asp:Label></td></tr><tr>
                        <td>
                            Reported Date : </td><td>
                            <asp:Label ID="lblReportedDate" runat="server"></asp:Label></td><td>
                            <asp:Label ID="lblTown" runat="server" Text=""></asp:Label></td></tr><tr>
                        <td>
                            Location : </td><td>
                            <asp:Label ID="lblFaultLocation" runat="server"></asp:Label></td><td>
                            <asp:Label ID="lblCounty" runat="server" Text=""></asp:Label></td></tr><tr>
                        <td>
                            Description : </td><td>
                            <asp:Label ID="lblFaultDescription" runat="server" CssClass="bold" Text=""></asp:Label></td><td>
                            <asp:Label ID="lblPostCode" runat="server" CssClass="bold" Text=""></asp:Label></td></tr><tr>
                        <td>
                            Trade : </td><td>
                            <asp:Label ID="lblTrade" runat="server" Text=""></asp:Label></td><td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Note : </td><td>
                            <asp:TextBox ID="lblFaultOperatorNote" runat="server" Enabled="false" Height="65px"
                                TextMode="MultiLine" Width="248px"></asp:TextBox></td><td>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <hr />
                            <span class="bold">Repair details:</span> </td></tr><tr>
                        <td>
                            Appointment: </td><td>
                            <asp:Label Text="" runat="server" ID="lblAppointment" />
                        </td>
                        <td rowspan="5">
                            <table>
                                <tr>
                                    <td>
                                        <asp:ListView runat="server" OnItemDataBound="lstViewPhotos_ItemDatabound" ID="lstViewPhotosBefore">
                                            <LayoutTemplate>
                                                <table id="groupPlaceholderContainer" runat="server" frame="box" style="border: 2px solid gray;">
                                                    <tr id="itemPlaceholder" runat="server">
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div style="font-size: 10px;">
                                                                <span class="bold">Before</span> </div></td></tr></table></LayoutTemplate><ItemTemplate>
                                                <tr runat="server" style="padding: 1px;">
                                                    <td style="padding: 1px;">
                                                        <asp:HiddenField ID="hdnImageName" runat="server" Value='<%# Eval("ImageName") %>' />
                                                        <asp:ImageButton ID="imgFaultImage" runat="server" AlternateText='<%# Eval("ImageName") %>'
                                                            Width="156px" Height="120px" CommandName="ShowImage" CommandArgument='<%# Eval("ImageName")%>' />
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <EmptyItemTemplate>
                                            </EmptyItemTemplate>
                                        </asp:ListView>
                                    </td>
                                    <td>
                                        <asp:ListView runat="server" OnItemDataBound="lstViewPhotos_ItemDatabound" ID="lstViewPhotosAfter">
                                            <LayoutTemplate>
                                                <table id="groupPlaceholderContainer" runat="server" frame="box" style="border: 2px solid gray;">
                                                    <tr id="itemPlaceholder" runat="server">
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div style="font-size: 10px;">
                                                                <span class="bold">After</span> </div></td></tr></table></LayoutTemplate><ItemTemplate>
                                                <tr runat="server">
                                                    <td style="padding: 1px;">
                                                        <asp:HiddenField ID="hdnImageName" runat="server" Value='<%# Eval("ImageName") %>' />
                                                        <asp:ImageButton ID="imgFaultImage" runat="server" AlternateText='<%# Eval("ImageName") %>'
                                                            Width="156px" Height="120px" CommandName="ShowImage" CommandArgument='<%# Eval("ImageName")%>' />
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <EmptyItemTemplate>
                                            </EmptyItemTemplate>
                                        </asp:ListView>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Completion Date: </td><td>
                            <asp:Label Text="" runat="server" ID="lblCompletionDate" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Operative : </td><td>
                            <asp:Label ID="lblOperativeName" runat="server"></asp:Label></td></tr><tr>
                        <td>
                            Repair(s): </td><td>
                            <asp:GridView ID="grdRepairDetail" runat="server" AutoGenerateColumns="False" BorderColor="White"
                                BorderStyle="None" ShowHeader="false" BorderWidth="0px" GridLines="None">
                                <Columns>
                                    <asp:TemplateField ShowHeader="False" ItemStyle-Wrap="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRepairDescription" runat="server" Text='<%# Bind("Description") %>'></asp:Label></ItemTemplate><ItemStyle HorizontalAlign="Left" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Repair Notes : </td><td>
                            <asp:TextBox ID="txtBoxRepairNotes" runat="server" Enabled="false" Height="65px"
                                TextMode="MultiLine" Width="248px"></asp:TextBox></td></tr><tr>
                        <td colspan="3">
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <span class="bold">Activity:</span> </td><td>
                            <span class="bold">Asbestos :</span> </td></tr><tr>
                        <td colspan="2">
                            <asp:GridView ID="grdJobSheetActivities" runat="server" AutoGenerateColumns="false"
                                ShowHeader="true" ShowHeaderWhenEmpty="true" HeaderStyle-HorizontalAlign="Left"
                                CssClass="activityGrid">
                                <Columns>
                                    <asp:TemplateField HeaderText="Date" ItemStyle-Width="135px">
                                        <ItemTemplate>
                                            <asp:Label Text='<%# Eval("Date","{0:d MMM yyyy HH:mm}") %>' runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Activity" HeaderText="Activity" ItemStyle-Width="200px" />
                                    <asp:BoundField DataField="ActivityUserInitials" HeaderText="By" ItemStyle-Width="40px" />
                                </Columns>
                            </asp:GridView>
                        </td>
                        <td>
                            <asp:GridView ID="grdAsbestos" runat="server" AutoGenerateColumns="False" ShowHeader="false"
                                CssClass="asbestosGrid">
                                <Columns>
                                    <asp:BoundField DataField="AsbRiskID" />
                                    <asp:BoundField DataField="Description" />
                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Follow On Work : </b></td><td>
                            <asp:TextBox ID="lblFollowOnWork" runat="server" CssClass="roundcornerby5" Enabled="false"
                                Height="65px" TextMode="MultiLine" Width="248px"></asp:TextBox><br /></td><td>
                        </td>
                    </tr>
                </table>
            </div>
            <br />
            <asp:Label ID="lblPopUpShowImage" runat="server"></asp:Label><ajaxToolkit:ModalPopupExtender
                ID="mdlPopUpViewImage" runat="server" DynamicServicePath="" Enabled="True" TargetControlID="lblPopUpShowImage"
                PopupControlID="pnlLargeImage" CancelControlID="btnClose" BackgroundCssClass="modalBackground">
            </ajaxToolkit:ModalPopupExtender>
            <asp:Panel ID="pnlLargeImage" runat="server" CssClass="modalPopupImage" Style="display: none;">
                <div style='width: 100%;'>
                    <asp:ImageButton ID="btnClose" runat="server" Style="position: absolute; top: -17px;
                        right: -16px; width: 22px;" ImageAlign="Top" ImageUrl="~/Images/cross2.png" BorderWidth="0" />
                    <asp:Image ID="imgDynamicImage" runat="server" CssClass="imgDynamicImage" />
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
