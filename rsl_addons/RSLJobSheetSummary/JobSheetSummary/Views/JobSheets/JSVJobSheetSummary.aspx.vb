﻿Imports System.Threading
Imports JSS_Utilities
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports JSS_BusinessLogic

Public Class JSVJobSheetSummary
    Inherits PageBase

#Region "Events"

#Region "Page Load Event"
    ''' <summary>
    ''' Page Load Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            If Not IsPostBack Then
                Dim Jsn As String = CType(Request.QueryString(PathConstants.Jsn), String)
                Dim appointmentType As String = CType(Request.QueryString(PathConstants.AppointmentType), String)
               
                populateJobSheet(Jsn, appointmentType)
            End If
        Catch ex As ThreadAbortException
            uiMessageHelper.message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try

    End Sub

#End Region

#Region "Populate Job Sheet"
    ''' <summary>
    ''' Populate Job Sheet Summary page
    ''' </summary>
    ''' <param name="jsn"></param>
    ''' <remarks></remarks>
    Public Sub populateJobSheet(ByVal jsn As String, ByVal appointmentType As String)

        Try
            Dim objJobSheetSummaryBL As New JobSheetSummaryBL
            Dim resultDataset As DataSet = New DataSet()
            ViewState(ViewStateConstants.JournalHistoryId) = jsn
            objJobSheetSummaryBL.GetVoidJobSheetSummaryDetails(jsn, appointmentType, resultDataset)

            Dim resultDt As DataTable = resultDataset.Tables("PropertyInfo")
            Dim dtAppointmentsInfo As DataTable = resultDataset.Tables("AppointmentInfo")

            If dtAppointmentsInfo.Rows.Count > 0 Then

                Dim currentIndex As Integer = Convert.ToInt32(ViewState(ViewStateConstants.CurrentIndex))
                For Each requiredBo As DataRow In dtAppointmentsInfo.Rows
                    Dim newRowWork As New HtmlTableRow()
                    Dim newCellJSVLabel As New HtmlTableCell()
                    newCellJSVLabel.InnerText = requiredBo.Item("JobsheetNumber").ToString()

                    Dim newCellLocationLabel As New HtmlTableCell()
                    If requiredBo.Item("AppointmentType").ToString() = "Void Works" Then
                        newCellLocationLabel.InnerText = requiredBo.Item("Location") + ": " + requiredBo.Item("WorkDescription")
                    Else
                        newCellLocationLabel.InnerText = requiredBo.Item("AppointmentType")
                    End If

                    newCellJSVLabel.Width = "20%"
                    newCellJSVLabel.Style.Add("font-weight", "bold")
                    newCellLocationLabel.Width = "80%"
                    newCellLocationLabel.Style.Add("font-weight", "bold")
                    newRowWork.Cells.Add(newCellJSVLabel)
                    newRowWork.Cells.Add(newCellLocationLabel)
                    tblRequiredWork.Rows.Add(newRowWork)
                Next




                lblSummary.Text = dtAppointmentsInfo.Rows(0).Item("AppointmentType") + " Job Sheet Summary"


                lblStatus.Text = dtAppointmentsInfo.Rows(0).Item("Status")
                lblOperative.Text = dtAppointmentsInfo.Rows(0).Item(ApplicationConstants.OperativeColumn)
                lblTime.Text = dtAppointmentsInfo.Rows(0).Item(ApplicationConstants.StartTimeColumn) + " - " + dtAppointmentsInfo.Rows(0).Item(ApplicationConstants.EndTimeColumn)


                lblDuration.Text = GeneralHelper.appendHourLabel(Convert.ToDouble(dtAppointmentsInfo.Rows(0).Item(ApplicationConstants.DurationsColumn)))



                Dim startDateTime As DateTime = Convert.ToDateTime(dtAppointmentsInfo.Rows(0).Item(ApplicationConstants.StartDateColumn))
                Dim startDay As String = startDateTime.DayOfWeek.ToString()
                Dim startDate As String = GeneralHelper.getOrdinal(Convert.ToInt32(startDateTime.ToString("dd")))
                Dim startMonth As String = startDateTime.ToString("MMMMMMMMMMMMM")
                Dim startYear As String = startDateTime.ToString("yyyy")

                Dim endDateTime As DateTime = Convert.ToDateTime(dtAppointmentsInfo.Rows(0).Item(ApplicationConstants.EndDateColumn))
                Dim endDay As String = endDateTime.DayOfWeek.ToString()
                Dim endDate As String = GeneralHelper.getOrdinal(Convert.ToInt32(endDateTime.ToString("dd")))
                Dim endMonth As String = endDateTime.ToString("MMMMMMMMMMMMM")
                Dim endYear As String = endDateTime.ToString("yyyy")

                lblStartDate.Text = startDay + " " + startDate + " " + startMonth + " " + startYear
                lblEndDate.Text = endDay + " " + endDate + " " + endMonth + " " + endYear


                If (IsDBNull(dtAppointmentsInfo.Rows(0).Item(ApplicationConstants.CustomerNotesColumn))) Then
                    txtCustAppointmentNotes.Text = String.Empty
                Else
                    txtCustAppointmentNotes.Text = dtAppointmentsInfo.Rows(0).Item(ApplicationConstants.CustomerNotesColumn)
                End If

                If (IsDBNull(dtAppointmentsInfo.Rows(0).Item(ApplicationConstants.JobsheetNotesColumn))) Then
                    txtJobSheetNotes.Text = String.Empty
                Else
                    txtJobSheetNotes.Text = dtAppointmentsInfo.Rows(0).Item(ApplicationConstants.JobsheetNotesColumn)
                End If
                If lblStatus.Text.ToUpper = "Complete".ToUpper Then
                    lblJobSheetNotes.Text = "Completion Notes:"
                Else
                    lblJobSheetNotes.Text = "Job Sheet Notes:"
                End If

                Dim propertyId As String = dtAppointmentsInfo.Rows(0).Item(ApplicationConstants.PropertyIdColumn)
                ViewState.Add(ViewStateConstants.PropertyId, propertyId)
            Else
                lblMessage.Text = "No Record Found"
            End If
            If resultDt.Rows.Count > 0 Then


                'Property Information
                lblScheme.Text = resultDt.Rows(0)("SchemeName")
                lblAddress.Text = resultDt.Rows(0)("HOUSENUMBER") + " " + resultDt.Rows(0)("ADDRESS1") + " " + resultDt.Rows(0)("ADDRESS2")
                lblTowncity.Text = resultDt.Rows(0)("TOWNCITY")
                lblCounty.Text = resultDt.Rows(0)("COUNTY")
                lblPostcode.Text = resultDt.Rows(0)("POSTCODE")

                SessionManager.setPropertyId(resultDt.Rows(0)("PROPERTYID"))

                'Customer Information

                lblCustomerName.Text = resultDt.Rows(0)("TenantName")
                lblCustomerTelephone.Text = resultDt.Rows(0)("Telephone")
                lblCustomerMobile.Text = resultDt.Rows(0)("Mobile")
                lblCustomerEmail.Text = resultDt.Rows(0)("Email")
            Else
                lblMessage.Text = "No Record Found"
            End If


            populateJobSheetImages()


        Catch ex As ThreadAbortException
            uiMessageHelper.message = ex.Message
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try




    End Sub
#End Region

#Region "Populate Void images"
    ''' <summary>
    ''' Populate fault images
    ''' </summary>
    ''' <remarks></remarks>
    Sub populateJobSheetImages()

        Dim resultDataSet As DataSet = New DataSet()
        Dim objJobSheetSummaryBL As New JobSheetSummaryBL

        Dim Jsn As String = CType(Request.QueryString(PathConstants.Jsn), String)

        Dim workdString As String = Jsn.Substring(3, Jsn.Length - 3)


        Dim workID As Integer
        Int32.TryParse(workdString, workID)

        Dim propertyId As String = SessionManager.getPropertyId()

        objJobSheetSummaryBL.getVoidWorkJobSheetImages(resultDataSet, workID)

        Dim exsistingImagesdt = getExistedImages(resultDataSet.Tables(0))
        lstViewPhotosBefore.DataSource = New DataView(exsistingImagesdt, "Status = 'After'", "", DataViewRowState.CurrentRows)
        lstViewPhotosBefore.DataBind()

        lstViewPhotosAfter.DataSource = New DataView(exsistingImagesdt, "Status = 'Before'", "", DataViewRowState.CurrentRows)
        lstViewPhotosAfter.DataBind()

    End Sub

#End Region

#Region "lstView Photos Item data bound"
    ''' <summary>
    ''' lstView Photos Item data bound
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lstViewPhotos_ItemDatabound(ByVal sender As Object, ByVal e As ListViewItemEventArgs) Handles lstViewPhotosBefore.ItemDataBound, lstViewPhotosAfter.ItemDataBound
        Try

            If e.Item.ItemType = ListViewItemType.DataItem Then

                Dim imgFaultImage As Image = CType(e.Item.FindControl("imgFaultImage"), Image)
                Dim hdnImageName As HiddenField = CType(e.Item.FindControl("hdnImageName"), HiddenField)
                imgFaultImage.ImageUrl = hdnImageName.Value

            End If

        Catch ex As Exception
            ExceptionHelper.IsError = True
            ExceptionHelper.ExcpetionMessage = ex.Message
            If ExceptionHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If ExceptionHelper.IsError = True Then
                ExceptionHelper.setMessage(lblMessage, pnlMessage, ExceptionHelper.ExcpetionMessage, True)
            End If
        End Try
    End Sub
#End Region

#Region "lstView Photos Item Command"
    ''' <summary>
    ''' lstView Photos Item Command
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub lstViewPhotos_ItemCommand(sender As Object, e As System.Web.UI.WebControls.ListViewCommandEventArgs) Handles lstViewPhotosBefore.ItemCommand, lstViewPhotosAfter.ItemCommand
        Try
            If e.CommandName = "ShowImage" Then
                Dim fileName As String = e.CommandArgument.ToString()
                imgDynamicImage.ImageUrl = fileName
                'mdlPopUpViewImage.X = 100
                'mdlPopUpViewImage.Y = 20
                mdlPopUpViewImage.Show()
            End If
        Catch ex As Exception
            ExceptionHelper.IsError = True
            ExceptionHelper.ExcpetionMessage = ex.Message
            If ExceptionHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If ExceptionHelper.IsError = True Then
                ExceptionHelper.setMessage(lblMessage, pnlMessage, ExceptionHelper.ExcpetionMessage, True)
            End If
        End Try
    End Sub
#End Region


#Region "Get Existed Images"
    ''' <summary>
    ''' Get Existed Images
    ''' </summary>
    ''' <param name="dtImages"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Function getExistedImages(ByVal dtImages As DataTable) As DataTable

        For Each dr As DataRowView In dtImages.DefaultView
            Dim imageName As String = dr("ImageName").ToString()


            Dim fullPath As String = getLogicalPropertyImagePath(SessionManager.getPropertyId, imageName)

            'If Not System.IO.File.Exists(Server.MapPath(fullPath)) Then
            '    dr.Row.Delete()
            'End If
            dr("ImageName") = fullPath
        Next

        dtImages.AcceptChanges()

        Return dtImages

    End Function

#End Region


    Public Shared Function getLogicalPropertyImagePath(propertyId As String, imageName As String) As String
        Dim requestURL = HttpContext.Current.Request.Url
        Dim imagePath As String = Convert.ToString((Convert.ToString(requestURL.GetLeftPart(UriPartial.Authority) + "/PropertyImages/") & propertyId) + "/Images/") & imageName

        Return imagePath
    End Function

#End Region

End Class