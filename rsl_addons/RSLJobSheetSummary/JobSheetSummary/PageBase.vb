﻿Imports JSS_Utilities
Imports System.Web
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports JSS_BusinessLogic

Public Class PageBase
    Inherits System.Web.UI.Page

#Region "Properties"

    Public uiMessageHelper As UIMessageHelper = New UIMessageHelper()

#End Region

#Region "Constructor(s)"

    Sub New()

    End Sub

#End Region

#Region "Events"

#Region "Page_Init"

    ''' <summary>
    ''' This event handles the page_init event of the base page
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub Page_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        ' TODO: Use these function, when implementing security.
        'Me.isSessionExist()
        'Me.checkPageAccess()

        'If Not Me.DesignMode Then
        '    If Context.Session IsNot Nothing Then
        '        If Session.IsNewSession Then
        '            Dim newSessionIdCookie As HttpCookie = Request.Cookies("sessionCookie")
        '            If newSessionIdCookie IsNot Nothing Then
        '                Dim newSessionIdCookieValue As String = newSessionIdCookie.Value
        '                If newSessionIdCookieValue <> String.Empty Then
        '                    ' This means Session was timed Out and New Session was started
        '                    Response.Redirect(PathConstants.AccessDeniedPath & "?src=sessionExpired")
        '                End If
        '            End If
        '        End If
        '    End If
        'End If
    End Sub

#End Region

#Region "OnInit"

    ''' <summary>
    ''' This function is used to destroy browser cache. so that every time page load 'll be called
    ''' </summary>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Overrides Sub OnInit(e As EventArgs)

        Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.Cache.SetNoStore()
        Response.Cache.SetExpires(DateTime.Now.AddDays(-1))
        Response.ExpiresAbsolute = DateTime.Now.AddDays(-1D)
        Response.Expires = -1500
        Response.CacheControl = "no-cache"

        MyBase.OnInit(e)
    End Sub

#End Region

#End Region

#Region "Functions"

#Region "is Session Exist"

    Public Sub isSessionExist()

        'SessionManager.setLoggedInUserEmployeeId(943)

        Dim userId As Integer = SessionManager.getLoggedInUserEmployeeId
        If (userId = 0) Then
            Response.Redirect(PathConstants.LoginPath)
        End If
    End Sub

#End Region

#Region "Check Page Access"

    ''' <summary>
    ''' Check Page Access
    ''' </summary>
    ''' <remarks></remarks>
    Sub checkPageAccess()

        'Dim objDashboard As MasterPageBL = New MasterPageBL

        'If (Not objDashboard.checkPageAccess(ApplicationConstants.RepairsMenu)) Then
        '    Response.Redirect(PathConstants.AccessDeniedPath)
        'End If

    End Sub

#End Region

#End Region

End Class
