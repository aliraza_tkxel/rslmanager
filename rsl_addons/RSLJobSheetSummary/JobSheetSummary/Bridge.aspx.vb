﻿Imports System
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports JSS_BusinessLogic
Imports JSS_Utilities


Public Class Bridge
    Inherits MSDN.SessionPage


    Public uiMessageHelper As UIMessageHelper = New UIMessageHelper()
    Dim customerId As Integer = 0
    Dim propertyId As String = String.Empty
    Dim classicUserId As Integer = 0
    Dim isCalendarView As Boolean = False
    Dim isReportView As Boolean = False
    Dim isAccessDenied As Boolean = False
    Dim isRepairsDashboard As Boolean = False
    Dim reportPrifix As String = String.Empty
    Dim isSchedule As Boolean = False

#Region "Page Load"
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Try
            Me.loadUser()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "load User"
    Private Sub loadUser()

        'Dim objUserBo As UserBO = New UserBO()
        'Dim objUserBl As UsersBL = New UsersBL()
        'Dim resultDataSet As DataSet = New DataSet()

        'Me.getQueryStringValues()
        'Me.checkClassicAspSession()

        'resultDataSet = objUserBl.getEmployeeById(classicUserId)

        'If (resultDataSet.Tables(0).Rows.Count = 0) Then
        '    uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.UserDoesNotExist, True)
        'Else
        '    Dim isActive As String = resultDataSet.Tables(0).Rows(0).Item("IsActive").ToString()

        '    If isActive = False Then
        '        uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.UsersAccountDeactivated, True)
        '    Else
        '        setUserSession(resultDataSet)
        '    End If

        'End If
    End Sub
#End Region

#Region "set User Session"
    Private Sub setUserSession(ByRef resultDataSet)

        SessionManager.setUserFullName(resultDataSet.Tables(0).Rows(0).Item("FullName").ToString())
        SessionManager.setLoggedInUserEmployeeId(resultDataSet.Tables(0).Rows(0).Item("EmployeeId").ToString())
        SessionManager.setLoggedInUserType(resultDataSet.Tables(0).Rows(0).Item("UserType").ToString())

    End Sub
#End Region

#Region "get Query String Values"

    Private Sub getQueryStringValues()
        If Request.QueryString(PathConstants.AccessDenied) IsNot Nothing Then
            isAccessDenied = IIf(Request.QueryString(PathConstants.AccessDenied) = PathConstants.Yes, True, False)
        Else
            Me.redirectToLoginPage()
        End If
    End Sub
#End Region

#Region "check Classic Asp Session"
    Private Sub checkClassicAspSession()
        '''''''''comment this line below for production
        ' classicUserId = 201
        '''''''''comment this line ábove for production

        ''''''''''Un comment these below lines for production
        If ASPSession("USERID") IsNot Nothing Then
            classicUserId = Integer.Parse(ASPSession("USERID").ToString())
        Else
            Me.redirectToLoginPage()
        End If
        ''''''''''Un comment these above lines for production
    End Sub
#End Region

#Region "redirect To Login Page"
    Private Sub redirectToLoginPage()
        Response.Redirect(PathConstants.LoginPath, True)
    End Sub
#End Region
End Class
