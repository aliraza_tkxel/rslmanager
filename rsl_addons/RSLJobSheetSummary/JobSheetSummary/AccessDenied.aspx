﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/MasterPages/JSS.Master"
    CodeBehind="AccessDenied.aspx.vb" Inherits="JobSheetSummary.AccessDenied" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphHead" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphBody" runat="server">
    <asp:Panel ID="pnlMessage" runat="server" Visible="True">
        <div>
            <asp:Image ID="imgAccessDenied" Style="position: absolute; margin: auto; top: 0;
                left: 0; right: 0; bottom: 0;" ImageUrl="~/Images/access_denied.png" runat="server" />
        </div>
    </asp:Panel>
</asp:Content>
