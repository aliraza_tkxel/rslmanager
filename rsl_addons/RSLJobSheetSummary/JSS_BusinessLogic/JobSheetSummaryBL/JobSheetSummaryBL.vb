﻿Imports JSS_DataAccess
Imports System

Namespace JSS_BusinessLogic

    Public Class JobSheetSummaryBL

#Region "Get Job Sheet Images"

        ''' <summary>
        ''' Get Job Sheet Images
        ''' </summary>
        ''' <remarks></remarks>
        Sub getJobSheetImages(ByRef resultDataSet As DataSet, ByVal jobSheetNumber As String)
            Dim objJobSheetSummaryDAL As New JobSheetSummaryDAL
            objJobSheetSummaryDAL.getJobSheetImages(resultDataSet, jobSheetNumber)
        End Sub

#End Region

#Region "Get Job Sheet details"

        ''' <summary>
        ''' Get Job Sheet details
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="jobSheetNumber"></param>
        ''' <remarks></remarks>
        Public Sub getJobSheetDetails(ByRef resultDataSet As DataSet, ByVal jobSheetNumber As String)
            Dim objJobSheetSummaryDAL As New JobSheetSummaryDAL
            objJobSheetSummaryDAL.getJobSheetDetails(resultDataSet, jobSheetNumber)
        End Sub

#End Region

#Region "Get Sb Job Sheet details"

        ''' <summary>
        ''' Get Job Sheet details
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="jobSheetNumber"></param>
        ''' <remarks></remarks>
        Public Sub getSbJobSheetDetails(ByRef resultDataSet As DataSet, ByVal jobSheetNumber As String)
            Dim objJobSheetSummaryDAL As New JobSheetSummaryDAL
            objJobSheetSummaryDAL.getSbJobSheetDetails(resultDataSet, jobSheetNumber)
        End Sub

#End Region

#Region "Get Sb Job Sheet Images"

        ''' <summary>
        ''' Get Job Sheet Images
        ''' </summary>
        ''' <remarks></remarks>
        Sub getSbJobSheetImages(ByRef resultDataSet As DataSet, ByVal jobSheetNumber As String)
            Dim objJobSheetSummaryDAL As New JobSheetSummaryDAL
            objJobSheetSummaryDAL.getJobSheetImages(resultDataSet, jobSheetNumber)
        End Sub

#End Region

#Region "Get Defect Job Sheet Images"

        ''' <summary>
        ''' Get Job Sheet Images
        ''' </summary>
        ''' <remarks></remarks>
        Sub getDefectJobSheetImages(ByRef resultDataSet As DataSet, ByVal jobSheetNumber As String)
            Dim objJobSheetSummaryDAL As New JobSheetSummaryDAL
            objJobSheetSummaryDAL.getDefectJobSheetImages(resultDataSet, jobSheetNumber)
        End Sub

#End Region


#Region "Get Void work Job Sheet Images"

        ''' <summary>
        ''' Get Job Sheet Images
        ''' </summary>
        ''' <remarks></remarks>
        Sub getVoidWorkJobSheetImages(ByRef resultDataSet As DataSet, ByVal jobSheetNumber As String)
            Dim objJobSheetSummaryDAL As New JobSheetSummaryDAL
            objJobSheetSummaryDAL.getVoidWorkJobSheetImages(resultDataSet, jobSheetNumber)
        End Sub

#End Region




#Region "Populate Job Sheet Summary Page"
        Sub GetPlannedJobSheetSummaryDetails(ByVal jsn As String, ByRef resultDataset As DataSet)
            Dim objJobSheetSummaryDAL As New JobSheetSummaryDAL
            objJobSheetSummaryDAL.GetPlannedJobSheetSummaryDetails(jsn, resultDataset)

        End Sub

#End Region

#Region "Populate Job Sheet Summary Page"
        Sub GetVoidJobSheetSummaryDetails(ByVal jsn As String, ByVal appointmentType As String, ByRef resultDataset As DataSet)
            Dim objJobSheetSummaryDAL As New JobSheetSummaryDAL
            objJobSheetSummaryDAL.GetVoidJobSheetSummaryDetails(jsn,appointmentType, resultDataset)

        End Sub

#End Region


#Region "Populate Gas Job Sheet Summary Page"
        Sub GetGasJobSheetSummaryDetails(ByVal jsn As String, ByRef resultDataset As DataSet)
            Dim objJobSheetSummaryDAL As New JobSheetSummaryDAL
            objJobSheetSummaryDAL.GetGasJobSheetSummaryDetails(jsn, resultDataset)

        End Sub

#End Region

#Region "Populate Scheme/Block Gas Job Sheet Summary Page"
        Sub GetSchemeBlockGasJobSheetSummaryDetails(ByVal jsn As String, ByRef resultDataset As DataSet)
            Dim objJobSheetSummaryDAL As New JobSheetSummaryDAL
            objJobSheetSummaryDAL.GetSchemeBlockGasJobSheetSummaryDetails(jsn, resultDataset)

        End Sub

#End Region


#Region "Populate Gas Job Sheet Summary Page"
        Sub GetDefectJobSheetSummaryDetails(ByVal jsn As String, ByRef resultDataset As DataSet)
            Dim objJobSheetSummaryDAL As New JobSheetSummaryDAL
            objJobSheetSummaryDAL.GetDefectJobSheetSummaryDetails(resultDataset, jsn)

        End Sub

#End Region


#Region "Populate Gas Job Sheet Summary Page"
        Sub GetMEJobSheetSummaryDetails(ByVal jsn As String, ByRef resultDataset As DataSet)
            Dim objJobSheetSummaryDAL As New JobSheetSummaryDAL
            objJobSheetSummaryDAL.GetMEJobSheetSummaryDetails(resultDataset, jsn)

        End Sub

#End Region
    End Class

End Namespace