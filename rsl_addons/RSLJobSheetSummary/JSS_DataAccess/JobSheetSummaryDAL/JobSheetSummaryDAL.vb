﻿Imports System
Imports System.IO
Imports System.Data.Common
Imports Microsoft.Practices.EnterpriseLibrary.Common
Imports Microsoft.Practices.EnterpriseLibrary.Data
Imports JSS_Utilities
Imports JSS_BusinessObject

Namespace JSS_DataAccess

    Public Class JobSheetSummaryDAL
        Inherits BaseDAL

#Region "Get Job Sheet Images"

        ''' <summary>
        ''' Get Job Sheet Images
        ''' </summary>
        ''' <remarks></remarks>
        Sub getJobSheetImages(ByRef resultDataSet As DataSet, ByVal jobSheetNumber As String)

            Dim parametersList As ParameterList = New ParameterList()
            Dim jobsheetParam As ParameterBO = New ParameterBO("jobsheetnumber", jobSheetNumber, DbType.String)
            parametersList.Add(jobsheetParam)
            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetSbJobSheetImages)

        End Sub

#End Region



#Region "Get Defect Jobsheet Sheet Images"

        ''' <summary>
        ''' Get Job Sheet Images
        ''' </summary>
        ''' <remarks></remarks>
        Sub getDefectJobSheetImages(ByRef resultDataSet As DataSet, ByVal jobSheetNumber As String)

            Dim parametersList As ParameterList = New ParameterList()
            Dim jobsheetParam As ParameterBO = New ParameterBO("defectId", jobSheetNumber, DbType.String)
            parametersList.Add(jobsheetParam)
            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetDefectJobSheetImages)

        End Sub

#End Region


#Region "Get Void work Jobsheet Sheet Images"

        ''' <summary>
        ''' Get Job Sheet Images
        ''' </summary>
        ''' <remarks></remarks>
        Sub getVoidWorkJobSheetImages(ByRef resultDataSet As DataSet, ByVal jobSheetNumber As String)

            Dim parametersList As ParameterList = New ParameterList()
            Dim jobsheetParam As ParameterBO = New ParameterBO("requireWorkId", jobSheetNumber, DbType.String)
            parametersList.Add(jobsheetParam)
            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetVoidWorkJobSheetImages)

        End Sub

#End Region

#Region "Get Job Sheet details"

        ''' <summary>
        ''' Get Job Sheet details
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="jobSheetNumber"></param>
        ''' <remarks></remarks>
        Public Sub getJobSheetDetails(ByRef resultDataSet As DataSet, ByVal jobSheetNumber As String)

            Dim parametersList As ParameterList = New ParameterList()

            'Dim dtJobSheetDetail As New DataTable
            'Dim dtCustomerDetail As New DataTable
            'Dim dtRepairDetail As New DataTable
            'Dim dtFollowOnWorkDetail As New DataTable
            'Dim dtAsbestos As New DataTable

            'dtJobSheetDetail.TableName = ApplicationConstants.JobSheetDetailDtName
            'dtCustomerDetail.TableName = ApplicationConstants.CustomerDetailDtname
            'dtRepairDetail.TableName = ApplicationConstants.RepairDetailDtname
            'dtFollowOnWorkDetail.TableName = ApplicationConstants.FollowOnWorkDetailDtname
            'dtAsbestos.TableName = ApplicationConstants.AsbestosDtname

            'resultDataSet.Tables.Add(dtJobSheetDetail)
            'resultDataSet.Tables.Add(dtCustomerDetail)
            'resultDataSet.Tables.Add(dtRepairDetail)
            'resultDataSet.Tables.Add(dtFollowOnWorkDetail)
            Dim tablesList As New List(Of String)

            tablesList.Add(ApplicationConstants.JobSheetAndAppointmentDetailsDtName)
            tablesList.Add(ApplicationConstants.CustomerDetailsDtname)
            tablesList.Add(ApplicationConstants.RepairDetailsDtname)
            tablesList.Add(ApplicationConstants.JobSheetActivitiesDtName)
            tablesList.Add(ApplicationConstants.AsbestosDtname)


            Dim followOnParam As ParameterBO = New ParameterBO("jobSheetNumber", jobSheetNumber, DbType.String)
            parametersList.Add(followOnParam)


            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetJobSheetDetails)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, tablesList.ToArray)

        End Sub

#End Region

#Region "Get Sb Job Sheet details"

        ''' <summary>
        ''' Get Job Sheet details
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="jobSheetNumber"></param>
        ''' <remarks></remarks>
        Public Sub getSbJobSheetDetails(ByRef resultDataSet As DataSet, ByVal jobSheetNumber As String)

            Dim parametersList As ParameterList = New ParameterList()

            'Dim dtJobSheetDetail As New DataTable
            'Dim dtCustomerDetail As New DataTable
            'Dim dtRepairDetail As New DataTable
            'Dim dtFollowOnWorkDetail As New DataTable
            'Dim dtAsbestos As New DataTable

            'dtJobSheetDetail.TableName = ApplicationConstants.JobSheetDetailDtName
            'dtCustomerDetail.TableName = ApplicationConstants.CustomerDetailDtname
            'dtRepairDetail.TableName = ApplicationConstants.RepairDetailDtname
            'dtFollowOnWorkDetail.TableName = ApplicationConstants.FollowOnWorkDetailDtname
            'dtAsbestos.TableName = ApplicationConstants.AsbestosDtname

            'resultDataSet.Tables.Add(dtJobSheetDetail)
            'resultDataSet.Tables.Add(dtCustomerDetail)
            'resultDataSet.Tables.Add(dtRepairDetail)
            'resultDataSet.Tables.Add(dtFollowOnWorkDetail)
            Dim tablesList As New List(Of String)

            tablesList.Add(ApplicationConstants.JobSheetAndAppointmentDetailsDtName)
            tablesList.Add(ApplicationConstants.CustomerDetailsDtname)
            tablesList.Add(ApplicationConstants.RepairDetailsDtname)
            tablesList.Add(ApplicationConstants.JobSheetActivitiesDtName)
            tablesList.Add(ApplicationConstants.AsbestosDtname)


            Dim followOnParam As ParameterBO = New ParameterBO("jobSheetNumber", jobSheetNumber, DbType.String)
            parametersList.Add(followOnParam)


            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetSbJobSheetDetails)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, tablesList.ToArray)

        End Sub

#End Region

#Region "Get Job Sheet Images"

        ''' <summary>
        ''' Get Job Sheet Images
        ''' </summary>
        ''' <remarks></remarks>
        Sub getSbJobSheetImages(ByRef resultDataSet As DataSet, ByVal jobSheetNumber As String)

            Dim parametersList As ParameterList = New ParameterList()
            Dim jobsheetParam As ParameterBO = New ParameterBO("jobsheetnumber", jobSheetNumber, DbType.String)
            parametersList.Add(jobsheetParam)
            MyBase.LoadDataSet(resultDataSet, parametersList, SpNameConstants.GetSbJobSheetImages)

        End Sub

#End Region

#Region "Populate Job Sheet Summary Page"
        Sub GetPlannedJobSheetSummaryDetails(ByVal jsn As String, ByRef resultDataset As DataSet)
            Dim parametersList As ParameterList = New ParameterList()

            Dim journalHistoryIdParam As ParameterBO = New ParameterBO("JobSheetNumber", jsn, DbType.String)
            parametersList.Add(journalHistoryIdParam)

            Dim dtAppointmentsInfo As DataTable = New DataTable()
            dtAppointmentsInfo.TableName = "AppointmentInfo"
            resultDataset.Tables.Add(dtAppointmentsInfo)

            Dim dtPropertyInfo As DataTable = New DataTable()
            dtPropertyInfo.TableName = "PropertyInfo"
            resultDataset.Tables.Add(dtPropertyInfo)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetPlannedJobSheetSummaryDetails)
            resultDataset.Load(lResultDataReader, LoadOption.OverwriteChanges, dtAppointmentsInfo, dtPropertyInfo)
        End Sub
#End Region

#Region "Populate Void Job Sheet Summary Page"
        Sub GetVoidJobSheetSummaryDetails(ByVal jsn As String, ByVal appointmentType As String, ByRef resultDataset As DataSet)
            Dim parametersList As ParameterList = New ParameterList()

            Dim journalHistoryIdParam As ParameterBO = New ParameterBO("JobSheetNumber", jsn, DbType.String)
            parametersList.Add(journalHistoryIdParam)
            Dim appointmentTypeParam As ParameterBO = New ParameterBO("appointmentType", appointmentType, DbType.String)
            parametersList.Add(appointmentTypeParam)

            Dim dtAppointmentsInfo As DataTable = New DataTable()
            dtAppointmentsInfo.TableName = "AppointmentInfo"
            resultDataset.Tables.Add(dtAppointmentsInfo)

            Dim dtPropertyInfo As DataTable = New DataTable()
            dtPropertyInfo.TableName = "PropertyInfo"
            resultDataset.Tables.Add(dtPropertyInfo)


            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetVoidJobSheetSummaryDetails)
            resultDataset.Load(lResultDataReader, LoadOption.OverwriteChanges, dtAppointmentsInfo, dtPropertyInfo)
        End Sub
#End Region

#Region "Populate Gas Job Sheet Summary Page"
        Sub GetGasJobSheetSummaryDetails(ByVal jsn As String, ByRef resultDataset As DataSet)
            Dim parametersList As ParameterList = New ParameterList()

            Dim journalHistoryIdParam As ParameterBO = New ParameterBO("JobSheetNumber", jsn, DbType.String)
            parametersList.Add(journalHistoryIdParam)



            Dim dtAppointmentsInfo As DataTable = New DataTable()
            dtAppointmentsInfo.TableName = "AppointmentInfo"
            resultDataset.Tables.Add(dtAppointmentsInfo)

            Dim dtPropertyInfo As DataTable = New DataTable()
            dtPropertyInfo.TableName = "PropertyInfo"
            resultDataset.Tables.Add(dtPropertyInfo)


            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetGasJobSheetSummaryDetails)
            resultDataset.Load(lResultDataReader, LoadOption.OverwriteChanges, dtAppointmentsInfo, dtPropertyInfo)
        End Sub
#End Region

#Region "Populate Scheme/Block Gas Job Sheet Summary Page"
        Sub GetSchemeBlockGasJobSheetSummaryDetails(ByVal jsn As String, ByRef resultDataset As DataSet)
            Dim parametersList As ParameterList = New ParameterList()

            Dim journalHistoryIdParam As ParameterBO = New ParameterBO("JobSheetNumber", jsn, DbType.String)
            parametersList.Add(journalHistoryIdParam)

            Dim dtAppointmentsInfo As DataTable = New DataTable()
            dtAppointmentsInfo.TableName = "AppointmentInfo"
            resultDataset.Tables.Add(dtAppointmentsInfo)

            Dim dtPropertyInfo As DataTable = New DataTable()
            dtPropertyInfo.TableName = "PropertyInfo"
            resultDataset.Tables.Add(dtPropertyInfo)

            Dim dtBoilerInfo As DataTable = New DataTable()
            dtBoilerInfo.TableName = "BoilerInfo"
            resultDataset.Tables.Add(dtBoilerInfo)

            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parametersList, SpNameConstants.GetGasJobSheetSummaryDetailsForSchemeBlock)
            resultDataset.Load(lResultDataReader, LoadOption.OverwriteChanges, dtAppointmentsInfo, dtPropertyInfo, dtBoilerInfo)
        End Sub
#End Region


#Region "Get Job Sheet Details by defectId"

        Public Sub GetDefectJobSheetSummaryDetails(jobSheetDataSet As DataSet, JobSheetNumber As String)

            Dim inParameters As New ParameterList
            inParameters.Add(New ParameterBO("JobSheetNumber", JobSheetNumber, DbType.String))

            Dim lDataReader = MyBase.SelectRecord(inParameters, SpNameConstants.GetDefectJobSheetSummaryDetails)
            jobSheetDataSet.Load(lDataReader, LoadOption.OverwriteChanges, "Defects", "AppointmentInfo", "PropertyInfo")

        End Sub

#End Region

#Region "Get Job Sheet Details by defectId"

        Public Sub GetMEJobSheetSummaryDetails(jobSheetDataSet As DataSet, JobSheetNumber As String)

            Dim inParameters As New ParameterList
            inParameters.Add(New ParameterBO("JobSheetNumber", JobSheetNumber, DbType.String))

            Dim lDataReader = MyBase.SelectRecord(inParameters, SpNameConstants.GetMEJobSheetSummaryDetails)
            jobSheetDataSet.Load(lDataReader, LoadOption.OverwriteChanges, "AppointmentInfo", "PropertyInfo")

        End Sub

#End Region

    End Class

End Namespace
