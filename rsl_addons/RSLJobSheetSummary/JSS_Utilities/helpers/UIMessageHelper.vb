﻿Imports System.Web.UI.WebControls
Imports System.Drawing

Namespace JSS_Utilities


    Public Class UIMessageHelper

#Region "UIMessageHelper"
        Public IsExceptionLogged As Boolean = True
        Public IsError As Boolean = False
        Public message As String
#End Region

#Region "set Message"

        Public Sub setMessage(ByRef lblMessage As Label, ByRef pnlMessage As Panel, ByVal message As String, ByVal isError As Boolean)
            Me.message = message

            If isError = True Then
                lblMessage.Text = message
                lblMessage.ForeColor = Color.Red
                lblMessage.Font.Bold = True

            Else

                lblMessage.Text = message
                lblMessage.ForeColor = Color.Green
                lblMessage.Font.Bold = True
            End If
            pnlMessage.Visible = True

        End Sub
#End Region

#Region "reset Message"
        Public Sub resetMessage(ByRef lblMessage As Label, ByRef pnlMessage As Panel)
            lblMessage.Text = String.Empty
            pnlMessage.Visible = False
            Me.IsError = False
            Me.message = String.Empty
        End Sub
#End Region

    End Class
End Namespace