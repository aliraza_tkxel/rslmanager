﻿Imports System.Globalization
Imports System.Web.HttpContext
Imports System.Web
Imports System.Web.UI.Page
Imports System.IO
Imports System.Net
Imports System.Net.Mail
Imports System.Text
Imports JSS_BusinessObject
Imports System.Web.UI.WebControls

Namespace JSS_Utilities

    Public Class GeneralHelper

#Region "get Us Cultured Date Time"

        ''' <summary>
        ''' Returns the US cultured date time.
        ''' </summary>
        ''' <param name="date">date to be converted.</param>
        ''' <returns>US Cultured DateTime</returns>

        Public Shared Function getUsCulturedDateTime(ByVal [date] As String) As DateTime
            Dim infoUS As New CultureInfo("en-US")
            Return Convert.ToDateTime([date].ToString(), infoUS)
        End Function
#End Region

#Region "get Uk Cultured Date Time"

        ''' <summary>
        ''' Returns the UK cultured date time.
        ''' </summary>
        ''' <param name="date">date to be converted.</param>
        ''' <returns>UK Cultured DateTime</returns>

        Public Shared Function getUKCulturedDateTime(ByVal [date] As String) As DateTime
            Dim infoUK As New CultureInfo("en-GB")
            Return Convert.ToDateTime([date].ToString(), infoUK)
        End Function
#End Region

#Region "Convert Date In Seconds"
        Public Shared Function convertDateInSec(ByVal timeToConvert As Date) As Integer
            Dim calendarStartDate As New DateTime(1970, 1, 1)
            Dim timeInSec As Integer = (timeToConvert - calendarStartDate).TotalSeconds
            Return timeInSec
        End Function
#End Region

#Region " Unix time stamp is seconds past epoch"
        Public Shared Function unixTimeStampToDateTime(unixTimeStamp As Double) As DateTime
            ' Unix time stamp is seconds past epoch
            Dim dtDateTime As System.DateTime = New DateTime(1970, 1, 1, 0, 0, 0, _
             0, System.DateTimeKind.Utc)
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime()
            Return dtDateTime
        End Function
#End Region

#Region "Convert Date In Minute"
        Public Shared Function convertDateInMin(ByVal timeToConvert As Date) As Integer
            Dim calendarStartDate As New DateTime(1970, 1, 1)
            Dim timeInSec As Integer = (timeToConvert - calendarStartDate).TotalMinutes
            Return timeInSec
        End Function
#End Region

#Region "Get daysuffix Like th,st,nd,rd to show (1st, 2nd, 3rd etc.)"
        ''' <summary>
        ''' This function is to get the day suffix like st, nd, rd, th to show it with date like 1st, 2nd, 3rd, 4th etc.        ''' 
        ''' </summary>
        ''' <param name="dateparam">date to get suffix.</param>
        ''' <returns>It return the suffix one of these four, st, nd, rd, th</returns>
        ''' <remarks></remarks>
        Public Shared Function getDaySuffix(ByVal dateparam As Date) As String
            Dim daySuffix = "th"
            If (dateparam.Day = 1 OrElse dateparam.Day = 21 OrElse dateparam.Day = 31) Then
                daySuffix = "st"
            ElseIf (dateparam.Day = 2 OrElse dateparam.Day = 22) Then
                daySuffix = "nd"
            ElseIf (dateparam.Day = 3 OrElse dateparam.Day = 23) Then
                daySuffix = "rd"
            End If

            Return daySuffix
        End Function



#End Region

#Region "get Document Upload Path"
        Public Shared Function getImageUploadPath() As String
            Return System.Configuration.ConfigurationManager.AppSettings("ImageUploadPath")
        End Function
#End Region
#Region "append Hour Label"
        ''' <summary>
        ''' This function 'll append day or days with number
        ''' </summary>
        ''' <param name="aNumber"></param>
        ''' <remarks></remarks>
        Public Shared Function appendHourLabel(ByVal aNumber As Double)
            If aNumber > 1 Then
                Return aNumber.ToString() + " hours"
            Else
                Return aNumber.ToString() + " hour"
            End If

        End Function
#End Region
#Region "Get Ordinal"
        ''' <summary>
        ''' Get Ordinal
        ''' </summary>
        ''' <param name="number"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getOrdinal(number As Integer) As String
            Dim suffix As String = [String].Empty

            Dim ones As Integer = number Mod 10
            Dim tens As Integer = CInt(Math.Floor(number / 10D)) Mod 10

            If tens = 1 Then
                suffix = "th"
            Else
                Select Case ones
                    Case 1
                        suffix = "st"
                        Exit Select

                    Case 2
                        suffix = "nd"
                        Exit Select

                    Case 3
                        suffix = "rd"
                        Exit Select
                    Case Else

                        suffix = "th"
                        Exit Select
                End Select
            End If
            Return [String].Format("{0}{1}", number, suffix)
        End Function

#End Region

#Region "Get Formated Date or Sting N/A if null"

        Public Shared Function getFormatedDateDefaultString(ByVal inputDate As Object, Optional ByVal formatString As String = "dd/MM/yyyy", Optional ByVal defaultOutput As String = "N/A") As String
            Dim outputString = defaultOutput
            Dim parsedInputDate As Date
            If Not (IsDBNull(inputDate) OrElse IsNothing(inputDate)) AndAlso Date.TryParse(inputDate, parsedInputDate) Then
                outputString = String.Format("{0:" + formatString + "}", parsedInputDate)
            End If
            Return outputString
        End Function

#End Region


#Region "Get Date with weekday format"
        ''' <summary>
        ''' Get Date with weekday format e.g "Tuesday 13th December 2013"
        ''' </summary>
        ''' <param name="aDate">date time</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getDateWithWeekdayFormat(aDate As DateTime) As Object
            Dim startDay As String = aDate.DayOfWeek.ToString()
            Dim startDate As String = GeneralHelper.getOrdinal(Convert.ToInt32(aDate.ToString("dd")))
            Dim startMonth As String = aDate.ToString("MMMMMMMMMMMMM")
            Dim startYear As String = aDate.ToString("yyyy")
            Return Convert.ToString((Convert.ToString((Convert.ToString(startDay & Convert.ToString(" ")) & startDate) + " ") & startMonth) + " ") & startYear
        End Function
#End Region

    End Class

End Namespace
