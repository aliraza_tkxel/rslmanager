﻿Imports System
Imports System.Text

Namespace JSS_Utilities
    Public Class ApplicationConstants

        Public Shared JobSheetAndAppointmentDetailsDtName = "JobSheetAndAppointmentDetails"
        Public Shared CustomerDetailsDtname = "CustomerDetails"
        Public Shared RepairDetailsDtname = "RepairDetails"
        Public Shared JobSheetActivitiesDtName = "JobSheetActivities"
        Public Shared ReasonColName = "Reason"
        Public Shared AsbestosDtname = "Asbestos"
        Public Shared FollowOnWorkDetailsDtname = "FollowOnWorkDetails"
#Region "Job Sheet Summary"
        Public Const PmoColumn As String = "PMO"
        Public Const ComponentColumn As String = "Component"
        Public Const TradesColumn As String = "Trade"
        Public Const JsnColumn As String = "JSN"
        Public Const JsnSearchColumn As String = "JSNSearch"
        Public Const StatusColumn As String = "Status"
        Public Const OperativeColumn As String = "Operative"
        Public Const StartTimeColumn As String = "StartTime"
        Public Const EndTimeColumn As String = "EndTime"
        Public Const DurationsColumn As String = "Duration"
        Public Const TotalDurationColumn As String = "TotalDuration"
        Public Const StartDateColumn As String = "StartDate"
        Public Const EndDateColumn As String = "EndDate"
        Public Const CustomerNotesColumn As String = "CustomerNotes"
        Public Const JobsheetNotesColumn As String = "JobsheetNotes"
        Public Const IsMiscAppointmentColumn As String = "IsMiscAppointment"
        Public Const PropertyIdColumn As String = "PropertyId"
        Public Const TypeColumn As String = "Type"
        Public Const AppointmentIdColumn As String = "AppointmentId"
        Public Const InterimStatusColumn As String = "InterimStatus"
        Public Const NotAvailable As String = "N/A"

        Public Const jobSheetSummaryDetailTable As String = "JobSheetSummaryDetailTable"
        Public Const jobSheetSummaryAsbestosTable As String = "JobSheetSummaryAsbestosTable"

        Public Const ItemNameColumn As String = "ItemName"

#End Region

#Region "Misc Appointment Types Names"

        Public Const propertyType As String = "Property"
        Public Const schemeType As String = "Scheme"
        Public Const blockType As String = "Block"

#End Region
    End Class
End Namespace



