﻿Imports System

Namespace JSS_Utilities


    Public Class ViewStateConstants
#Region "Property Summary Tab"
        Public Const FaultsResultDataSet As String = "FaultsResultDataSet"
        Public Const JournalHistoryId As String = "JournalHistoryId"
        Public Const CreationDate As String = "CREATIONDATE"
        Public Const CurrentIndex As String = "CurrentIndex"
        Public Const TotalJobsheets As String = "TotalJobsheets"
        Public Const PropertyId As String = "PropertyId"
        Public Const Type As String = "Type"
#End Region
    End Class
End Namespace