﻿Namespace JSS_Utilities

    Public Class SpNameConstants

        Public Shared GetJobSheetDetails As String = "JSS_GetJobSheetDetails"
        Public Shared GetJobSheetImages As String = "JSS_GetJobSheetImages"
        Public Shared GetSbJobSheetDetails As String = "JSS_GetSbJobSheetDetails"
        Public Shared GetSbJobSheetImages As String = "JSS_GetSbJobSheetImages"
        Public Shared GetDefectJobSheetImages As String = "JSS_GetDefectJobsheetImages"
        Public Shared GetVoidWorkJobSheetImages As String = "JSS_GetVoidWorkJobsheetImages"



        Public Shared GetPlannedJobSheetSummaryDetails As String = "JSS_GetPlannedJobSheetSummaryDetails"
        Public Shared GetVoidJobSheetSummaryDetails As String = "JSS_GetVoidJobSheetSummaryDetails"
        Public Shared GetGasJobSheetSummaryDetails As String = "JSS_GetGasJobSheetSummaryDetails"
        Public Shared GetGasJobSheetSummaryDetailsForSchemeBlock As String = "JSS_GetGasJobSheetSummaryDetailsForSchemeBlock"
        Public Shared GetDefectJobSheetSummaryDetails As String = "JSS_GetDefectJobSheetSummaryDetails"
        Public Shared GetMEJobSheetSummaryDetails As String = "JSS_GetMEJobSheetSummaryDetails"
    End Class

End Namespace



