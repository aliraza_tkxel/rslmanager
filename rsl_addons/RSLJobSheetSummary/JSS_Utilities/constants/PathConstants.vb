﻿Namespace JSS_Utilities

    Public Class PathConstants

#Region "URL Constants"
        Public Const LoginPath As String = "~/../BHAIntranet/Login.aspx"
        Public Const AccessDeniedPath As String = "~/AccessDenied.aspx"
        
        'bridge file path
        Public Const Bridge As String = "~/Bridge.aspx"
        Public Const JobSheetSummary As String = "~/Views/JobSheets/JobSheetSummary.aspx"
        Public Const SbJobSheetSummary As String = "~/Views/JobSheets/SbJobSheetSummary.aspx"
#End Region

#Region "Query String Constants"
        'QueryString Constants
        Public Const Jsn As String = "jsn"

        Public Const AppointmentType As String = "appointmentType"

        Public Const AccessDenied As String = "accessDenied"
        Public Const Yes As String = "yes"
        Public Const JournalHistoryId As String = "JournalHistoryId"
#End Region

    End Class

End Namespace