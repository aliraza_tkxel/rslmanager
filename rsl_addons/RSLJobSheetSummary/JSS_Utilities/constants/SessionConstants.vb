﻿Imports System

Namespace JSS_Utilities

    Public Class SessionConstants

#Region "User Session"

        Public Shared ConSessionUserId As String = "SESSION_USER_ID"
        Public Shared FaultSchedulingUserId As String = "AppServicingUserId"
        Public Shared UserFullName As String = "UserFullName"
        Public Shared EmployeeId As String = "UserEmployeeId"
        Public Shared LoggedInUserType As String = "LoggedInUserType"

#End Region

        Public Shared JobSheetNumber As String = "JobSheetNumber"
        Public Shared FaultLogId As String = "FaultLogId"
        Public Shared RedirectCheck As String = "redirectCheck"
        Public Shared PropertyId As String = "PropertyId"
        Public Shared SbJobSheetNumber As String = "SbJobSheetNumber"
        Public Shared BlockId As String = "BlockId"
        Public Shared SchemeId As String = "SchemeId"
    End Class

End Namespace


