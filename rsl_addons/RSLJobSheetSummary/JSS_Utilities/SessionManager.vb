﻿Imports System
Imports System.Web.HttpContext
Imports System.Web
Imports JSS_BusinessObject

Namespace JSS_Utilities

    Public Class SessionManager

#Region "Login In User "

#Region "Set / Get User Full Name"

#Region "Set User Full Name"

        Public Shared Sub setUserFullName(ByRef userFullName As String)
            Current.Session(SessionConstants.UserFullName) = userFullName
        End Sub

#End Region

#Region "get User Full Name"

        Public Shared Function getUserFullName() As String
            If (IsNothing(Current.Session(SessionConstants.UserFullName))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionConstants.UserFullName), String)
            End If
        End Function

#End Region

#Region "remove User Full Name"

        Public Shared Sub removeUserFullName()
            If (Not IsNothing(Current.Session(SessionConstants.UserFullName))) Then
                Current.Session.Remove(SessionConstants.UserFullName)
            End If

        End Sub

#End Region

#End Region

#Region "Set / Get Logged In User Employee Id"

#Region "Set Logged In User Employee Id"

        Public Shared Sub setLoggedInUserEmployeeId(ByRef employeeId As String)
            Current.Session(SessionConstants.EmployeeId) = employeeId
        End Sub

#End Region

#Region "get Logged In User Employee Id"

        Public Shared Function getLoggedInUserEmployeeId() As Integer
            If (IsNothing(Current.Session(SessionConstants.EmployeeId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionConstants.EmployeeId), Integer)
            End If
        End Function

#End Region

#Region "remove Logged In User Employee Id"

        Public Shared Sub removeLoggedInUserEmployeeId()
            If (Not IsNothing(Current.Session(SessionConstants.EmployeeId))) Then
                Current.Session.Remove(SessionConstants.EmployeeId)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get User Type"

#Region "Set Logged In User Type"

        Public Shared Sub setLoggedInUserType(ByRef userType As String)
            Current.Session(SessionConstants.LoggedInUserType) = userType
        End Sub

#End Region

#Region "get Logged In User Type"

        Public Shared Function getLoggedInUserType() As String
            If (IsNothing(Current.Session(SessionConstants.LoggedInUserType))) Then
                Return String.Empty
            Else
                Return CType(Current.Session(SessionConstants.LoggedInUserType), String)
            End If
        End Function

#End Region

#Region "remove Logged In User Type"

        Public Shared Sub removeLoggedInUserType()
            If (Not IsNothing(Current.Session(SessionConstants.LoggedInUserType))) Then
                Current.Session.Remove(SessionConstants.LoggedInUserType)
            End If
        End Sub

#End Region

#End Region

#End Region

#Region "Set / Get Job Sheet Number"

#Region "Set Job Sheet Number"

        Public Shared Sub setJobSheetNumber(ByRef JobSheetNumber As String)
            Current.Session(SessionConstants.JobSheetNumber) = JobSheetNumber
        End Sub

#End Region

#Region "get Job Sheet Number"

        Public Shared Function getJobSheetNumber()
            If (IsNothing(Current.Session(SessionConstants.JobSheetNumber))) Then
                Return False
            Else
                Return CType(Current.Session(SessionConstants.JobSheetNumber), String)
            End If
        End Function

#End Region

#Region "remove Job Sheet Number"

        Public Shared Sub removeJobSheetNumber()
            If (Not IsNothing(Current.Session(SessionConstants.JobSheetNumber))) Then
                Current.Session.Remove(SessionConstants.JobSheetNumber)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Property Id"

#Region "Set Property Id"

        Public Shared Sub setPropertyId(ByVal propertyId As String)
            Current.Session(SessionConstants.PropertyId) = propertyId
        End Sub

#End Region

#Region "get Property Id"

        Public Shared Function getPropertyId()
            If (IsNothing(Current.Session(SessionConstants.PropertyId))) Then
                Return String.Empty
            Else
                Return CType(Current.Session(SessionConstants.PropertyId), String)
            End If
        End Function

#End Region

#Region "remove Property Id"

        Public Shared Sub removePropertyId()
            If (Not IsNothing(Current.Session(SessionConstants.PropertyId))) Then
                Current.Session.Remove(SessionConstants.PropertyId)
            End If
        End Sub

#End Region

#End Region


#Region "Set / Get Sb Job Sheet Number"

#Region "Set Sb Job Sheet Number"

        Public Shared Sub setSbJobSheetNumber(ByRef JobSheetNumber As String)
            Current.Session(SessionConstants.SbJobSheetNumber) = JobSheetNumber
        End Sub

#End Region

#Region "get Job Sheet Number"

        Public Shared Function getSbJobSheetNumber()
            If (IsNothing(Current.Session(SessionConstants.SbJobSheetNumber))) Then
                Return False
            Else
                Return CType(Current.Session(SessionConstants.SbJobSheetNumber), String)
            End If
        End Function

#End Region

#Region "remove Job Sheet Number"

        Public Shared Sub removeSbJobSheetNumber()
            If (Not IsNothing(Current.Session(SessionConstants.SbJobSheetNumber))) Then
                Current.Session.Remove(SessionConstants.SbJobSheetNumber)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Block Id"

#Region "Set Block Id"

        Public Shared Sub setBlockId(ByRef blockId As Integer)
            Current.Session(SessionConstants.BlockId) = blockId
        End Sub

#End Region

#Region "get BlockId"

        Public Shared Function getBlockId()
            If (IsNothing(Current.Session(SessionConstants.BlockId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionConstants.BlockId), Integer)
            End If
        End Function

#End Region

#Region "remove BlockId"

        Public Shared Sub removeBlockId()
            If (Not IsNothing(Current.Session(SessionConstants.BlockId))) Then
                Current.Session.Remove(SessionConstants.BlockId)
            End If
        End Sub

#End Region

#End Region


#Region "Set / Get SchemeId"

#Region "Set SchemeId"

        Public Shared Sub setSchemeId(ByRef schemeId As Integer)
            Current.Session(SessionConstants.SchemeId) = schemeId
        End Sub

#End Region

#Region "get SchemeId"

        Public Shared Function getSchemeId()
            If (IsNothing(Current.Session(SessionConstants.SchemeId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionConstants.SchemeId), Integer)
            End If
        End Function

#End Region

#Region "remove SchemeId"

        Public Shared Sub removeSchemeId()
            If (Not IsNothing(Current.Session(SessionConstants.SchemeId))) Then
                Current.Session.Remove(SessionConstants.SchemeId)
            End If
        End Sub

#End Region

#End Region

    End Class

End Namespace

