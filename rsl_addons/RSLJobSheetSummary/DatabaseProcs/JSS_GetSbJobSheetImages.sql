USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[JSS_GetJobSheetImages]    Script Date: 01/28/2015 15:59:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =============================================
--EXEC FL_GetCompletedFaultImages
-- @jobsheetnumber ='JS20171'
-- Author:		<Aamir Waheed>
-- Create date: <16/09/2014>
-- Description:	<Returns Job Sheet Images Detail>
-- Webpage:JobSheetSummay.aspx
-- ============================================= */
ALTER PROCEDURE [dbo].[JSS_GetSbJobSheetImages]
	-- Add the parameters for the stored procedure here
	(
	@jobsheetnumber varchar(50)
	)
AS
BEGIN

SELECT
	SchemeId,
	BlockId,
	JobSheetNumber,
	ImageName,
	CASE
		WHEN IsBeforeImage = 1 THEN 'Before'
		ELSE 'After'
	END AS Status

FROM FL_FAULT_REPAIR_IMAGES

WHERE JobSheetNumber = @jobsheetnumber

END