USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[V_GetNoEntryList]    Script Date: 10/27/2015 19:40:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
/* =================================================================================    
    Page Description: Get No Entry List 
    Author: Ali Raza
    Creation Date: June-25-2015

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0        June-25-2015    Ali Raza           Get No Entry List 
  =================================================================================*/
--  DECLARE	@totalCount int
--EXEC	 [dbo].[V_GetNoEntryList]
--		@searchText = NULL,
--		@pageSize = 100,
--		@pageNumber = 1,
--		@totalCount = @totalCount OUTPUT
--SELECT	@totalCount as N'@totalCount'
-- =============================================
ALTER PROCEDURE [dbo].[V_GetNoEntryList]
-- Add the parameters for the stored procedure here
		@searchText VARCHAR(200)='',
	--Parameters which would help in sorting and paging
		@pageSize int = 30,
		@pageNumber int = 1,
		@sortColumn varchar(500) = 'JournalId', 
		@sortOrder varchar (5) = 'DESC',
		@getOnlyCount bit=0,
		@totalCount int = 0 output	
AS
BEGIN
	DECLARE 
	
		@SelectClause varchar(3000),
        @fromClause   varchar(3000),
        @whereClause  varchar(2000),	        
        @orderClause  varchar(2000),	
        @mainSelectQuery varchar(7000),        
        @rowNumberQuery varchar(7000),
        @finalQuery varchar(7000),
        -- used to add in conditions in WhereClause based on search criteria provided
        @searchCriteria varchar(2000),
        
        --variables for paging
        @offset int,
		@limit int,
		@MSATTypeId int,
		@ToBeArrangedStatusId int
		--Paging Formula
		SET @offset = 1+(@pageNumber-1) * @pageSize
		SET @limit = (@offset + @pageSize)-1
		
		
		--=====================Search Criteria===============================
		SET @searchCriteria = ' N.IsNoEntryScheduled=0 '
		
		IF(@searchText != '' OR @searchText != NULL)
		BEGIN						
			SET @searchCriteria = @searchCriteria + CHAR(10) +' AND (ISNULL(P.HouseNumber, '''')	+ ISNULL('' ''+P.ADDRESS1, '''') 	+ ISNULL('', ''+P.ADDRESS2, '''') LIKE ''%' + @searchText + '%'')'
		
		END	
		
		--=======================Select Clause=============================================
		SET @SelectClause = 'Select top ('+convert(varchar(10),@limit)+')
								''JSV'' + CONVERT(VARCHAR, RIGHT(''000000'' + CONVERT(VARCHAR, ISNULL(
	CASE
		WHEN V.WorksJournalId IS NULL THEN J.JOURNALID
		ELSE V.WorksJournalId
	END, -1)), 4)) AS Ref,
	ISNULL(P.HouseNumber, '''') + ISNULL('' '' + P.ADDRESS1, '''') + ISNULL('', '' + P.ADDRESS2, '''') AS Address,
	P.TOWNCITY,
	P.COUNTY,
	ISNULL(P.POSTCODE, '''') AS Postcode,
	J.JOURNALID AS JournalId,
	CONVERT(NVARCHAR(50), M.TerminationDate, 103) AS Termination,
	CASE when M.ReletDate IS NULL THEN CONVERT(nvarchar(50),DATEADD(day,7,M.TerminationDate), 103)
	
	 ELSE CONVERT(NVARCHAR(50), M.ReletDate, 103) END AS Relet,
	CONVERT(NVARCHAR(50), N.RecordedOn, 103) AS RecordedOn,
	LEFT(E.Firstname, 1) + '''' + LEFT(E.LASTNAME, 1) AS RecordedBy,
	MT.MSATTypeName AS AppointmentType,
	A.APPOINTMENTID AS AppointmentId
			
			'
		
		
		--============================From Clause============================================
		SET @fromClause = CHAR(10) +' FROM V_NoEntry N
INNER JOIN PDR_APPOINTMENTS A
	ON N.AppointmentId = A.APPOINTMENTID
INNER JOIN PDR_JOURNAL J
	ON A.JOURNALID = J.JOURNALID
INNER JOIN PDR_MSAT M
	ON J.MSATID = M.MSATId
INNER JOIN PDR_MSATType MT
	ON M.MSATTypeId = MT.MSATTypeId
INNER JOIN PDR_STATUS S
	ON J.STATUSID = S.STATUSID
INNER JOIN P__PROPERTY P
	ON M.PropertyId = P.PROPERTYID
INNER JOIN E__EMPLOYEE E
	ON A.ASSIGNEDTO = E.EMPLOYEEID
LEFT JOIN V_RequiredWorks V
	ON J.JOURNALID = V.WorksJournalId
	AND V.IsCanceled = 0
		'
							
		--============================Order Clause==========================================
		IF(@sortColumn = 'Ref')
		BEGIN
			SET @sortColumn = CHAR(10)+ ' J.JOURNALID' 	
			
		END
		
		IF(@sortColumn = 'Address')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Address' 	
			
		END
		
		
		IF(@sortColumn = 'Termination')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Termination' 	
			
		END
		
		IF(@sortColumn = 'Relet')
		BEGIN
			SET @sortColumn = CHAR(10)+ 'Relet' 	
			
		END
				
		SET @orderClause =  CHAR(10) + ' Order By ' + @sortColumn + CHAR(10) + @sortOrder
		
		--=================================	Where Clause ================================
		
		SET @whereClause =	CHAR(10) + 'WHERE' + CHAR(10) + @searchCriteria 
		
		--===============================Main Query ====================================
		Set @mainSelectQuery = @selectClause +@fromClause + @whereClause + @orderClause 
		
		--=============================== Row Number Query =============================
		Set @rowNumberQuery ='  SELECT *, row_number() over (order by '+CHAR(10) + @sortColumn+ CHAR(10) +@sortOrder+CHAR(10)+') as row	
								FROM ('+CHAR(10)+@mainSelectQuery+CHAR(10)+')AS Records'
		
		--============================== Final Query ===================================
		Set @finalQuery  =' SELECT *
							FROM('+CHAR(10)+@rowNumberQuery+CHAR(10)+') AS Result 
							WHERE
							Result.row between'+ CHAR(10) + convert(varchar(10), @offset) + CHAR(10)+ 'and' + CHAR(10)+ convert(varchar(10),@limit)				
		
		--============================ Exec Final Query =================================
		IF(@getOnlyCount=0)
		BEGIN
			print(@finalQuery)
			EXEC (@finalQuery)
		END
		--========================================================================================
		-- Begin building Count Query 
		
		Declare @selectCount nvarchar(4000), 
		@parameterDef NVARCHAR(500)
		
		SET @parameterDef = '@totalCount int OUTPUT';
		SET @selectCount= 'SELECT @totalCount =  count(*) ' + @fromClause + @whereClause
		
		--print @selectCount
		EXECUTE sp_executesql @selectCount, @parameterDef, @totalCount OUTPUT;
				
		-- End building the Count Query
		--========================================================================================	
END
