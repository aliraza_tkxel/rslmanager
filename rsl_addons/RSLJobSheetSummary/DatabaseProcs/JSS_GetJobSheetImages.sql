/* =============================================
--EXEC FL_GetCompletedFaultImages
-- @jobsheetnumber ='JS20171'
-- Author:		<Aamir Waheed>
-- Create date: <16/09/2014>
-- Description:	<Returns Job Sheet Images Detail>
-- Webpage:JobSheetSummay.aspx
-- ============================================= */
CREATE PROCEDURE [dbo].[JSS_GetJobSheetImages]
	-- Add the parameters for the stored procedure here
	(
	@jobsheetnumber varchar(50)
	)
AS
BEGIN

SELECT
	PropertyId,
	JobSheetNumber,
	ImageName,
	CASE
		WHEN IsBeforeImage = 1 THEN 'Before'
		ELSE 'After'
	END AS [Status]

FROM FL_FAULT_REPAIR_IMAGES

WHERE JobSheetNumber = @jobsheetnumber

END