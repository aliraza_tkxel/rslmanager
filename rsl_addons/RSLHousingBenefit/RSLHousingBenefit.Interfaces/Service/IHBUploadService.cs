﻿using System.Collections.Generic;
using RSLHousingBenefit.Models.ViewModels.HBUploadFile;
using RSLHousingBenefit.Models.Models;
using RSLHousingBenefit.Models.ViewModels;

namespace RSLHousingBenefit.Interfaces.Service
{
    public interface IHBUploadService
    {
        List<LocalAuthorityViewModel> GetLocalAuthorities();
        int AddHBUploadFile(HouseBenefitFile houseBenefitFile);
        void AddHBUploadFileData(List<Models.HouseBenefitModel> houseBenfitModels, int HBFileId);
        UploadFileListViewModel GetUploadFileData(PaginationViewModel page);
        List<ExceptionFileListModel> GetExceptionUploadFileData(int hbUploadId);

    }
}
