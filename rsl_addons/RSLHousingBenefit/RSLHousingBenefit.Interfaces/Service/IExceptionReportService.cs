﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSLHousingBenefit.Models.ViewModels;

namespace RSLHousingBenefit.Interfaces.Service
{
    public interface IExceptionReportService
    {
        void validateHBUploadedData(int hbUploadId);
        ExceptionsListViewModel GetExceptionReportList(int hbUploadId);
        CustomerListViewModel SearchCustomer(string firstName, string lastName, string address, string postcode);
        CustomerHBListingViewModel GetCustomerHBListing(int customerId, int tenancyId);
        HBTenancyListingViewModel GetHbTenancyListingByHBref(int exceptionId);
        ExceptionsListViewModel ValidateExceptions(int hbUploadId);
        void processPayment(int hbUploadId);
		bool UpdateHbReferance(int HBId, int CustomerId, Int64 TenancyId, string HbRef);
        bool ResolveException(int HBUploadExceptionId, string HbRef, DateTime newStartDate, DateTime newEndDate, double newHb);
        int SubmitNewHBSchedule( int exceptionId ,int customerId, int tenancyId, string HBRef, double initialPayment, DateTime startDate, DateTime endDate);

    }
}
