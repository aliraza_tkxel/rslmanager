﻿using RSLHousingBenefit.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSLHousingBenefit.Interfaces.Service
{
    public interface IHBActualScheduleServices
    {
        HBCustomerRecordModel GetCustomerRecordById(int HBId);
        int? GetHBRow(int exceptionId);
        List<HBPaymentModel> GetNextPayment(int HBId);
        bool UpdateHbActualScheduleService(int HBId, DateTime newStartDate, DateTime newEndDate, double newHb, int CustomerId, Int64 TenancyId);
        bool ExcludeHbUpload(int HbUploadFileDataId, bool isExclude);
        string GetCustomerName(int CustomerId);
        string GetTenancyAddress(int customerId, int tenancyId);
        string GetExceptionReason(int exceptionId);
        List<HBPaymentModel> GetDummyNextPayment(DateTime newStartDate, DateTime newEndDate, double newHb);
    }
}
