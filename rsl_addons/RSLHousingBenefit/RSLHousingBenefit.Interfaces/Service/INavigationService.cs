﻿using RSLHousingBenefit.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSLHousingBenefit.Interfaces.Service
{
    public interface INavigationService
    {
        NavigationViewModel GetMenus(NavigationRequestViewModel request);
    }
}
