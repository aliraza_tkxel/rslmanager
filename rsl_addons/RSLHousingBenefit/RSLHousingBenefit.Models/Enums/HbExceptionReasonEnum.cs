﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSLHousingBenefit.Models.Enums
{
    public enum HbExceptionReasonEnum
    {
        HBRefMismatch = 1,
        StartDateIncorrect = 2,
        EndDateIncorrect = 3,
        AmountIncorrect = 4,
        PaymentExists = 5
    }
}
