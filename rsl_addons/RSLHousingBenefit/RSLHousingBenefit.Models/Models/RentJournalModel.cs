﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSLHousingBenefit.Models.Models
{
    public class RentJournalModel
    {
        public int JOURNALID { get; set; }
        public int TENANCYID { get; set; }
        public Nullable<System.DateTime> TRANSACTIONDATE { get; set; }
        public Nullable<int> ITEMTYPE { get; set; }
        public Nullable<int> PAYMENTTYPE { get; set; }
        public Nullable<System.DateTime> PAYMENTSTARTDATE { get; set; }
        public Nullable<System.DateTime> PAYMENTENDDATE { get; set; }
        public Nullable<decimal> AMOUNT { get; set; }
        public int ISDEBIT { get; set; }
        public Nullable<int> STATUSID { get; set; }
        public Nullable<System.DateTime> ACCOUNTTIMESTAMP { get; set; }
        public int QBSENT { get; set; }
    }
}
