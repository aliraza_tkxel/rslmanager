﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSLHousingBenefit.Models.Models
{
    public class HBCustomerRecordModel
    {
        public int HBUploadExceptionId { get; set; }
        public int HBId { get; set; }
        public int CustomerId { get; set; }
        public int? TenancyId { get; set; }
        public int? HBRow { get; set; }
        public List<HBPaymentModel> hBPaymentModel { set; get; }
        public HBNextEstimatedPaymentModel hBNextEstimatedPaymentModel{ get; set; }
        public HBInformation hBInformation { get; set; }
        public List<HBPaymentModel> NextHBPayment { set; get; }
        public string Address { set; get; }
        public string CustomerName { get; set; }

    }
}
