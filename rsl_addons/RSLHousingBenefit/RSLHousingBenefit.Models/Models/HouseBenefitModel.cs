﻿
using System;
using System.Text.RegularExpressions;

namespace RSLHousingBenefit.Models
{
    public class HouseBenefitModel
    {
        private string periodStartDate;
        private string periodEndDate;
        private string amount;
        private string hbReferance;
        private string tenantName;
        private string address;
        public string GetAddress()
        {
            return address;
        }
        public bool SetAddress(string value)
        {
            address = value;
            return true;
        }
        public string GetTenantName()
        {
            return tenantName;
        }
        public bool SetTenantName(string value)
        {
            tenantName = value;
            return true;
        }
        public string GetHBReferance()
        {
            return hbReferance;
        }
        public bool SetHBReferance(string value)
        {
            string regex = "^[a-zA-Z0-9]{4,8}$";
            var match = Regex.Match(value, regex);
            if (match.Success)
            {
                hbReferance = value;
                return true;
            }
            else
            {
                hbReferance = null;
                return false;
            }
        }
        public string GetAmount()
        {
            return amount;
        }
        public bool SetAmount(string value)
        {
            string regex = @"^-?\d+(\.\d+)?$";
            char[] extraSymbol = { '€', ' ', '£' };
            value = value.Trim(extraSymbol); 
            var match = Regex.Match(value, regex);
            if (match.Success)
            {
                amount = value;
                return true;
            }
            else
            {
                amount= null;
                return false;
            }
        }
        public string GetPeriodEndDate()
        {
            return periodEndDate;
        }
        public bool SetPeriodEndDate(string value)
        {
            //date formate checking 
            string regex = @"^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$";

                var match = Regex.Match(value, regex);
                if (match.Success)
                {
                DateTime endDate = new DateTime();
                if (DateTime.TryParse(value, out endDate))
                {
                    periodEndDate = value;
                    return true;
                }
                return false;
                }
                else
                {
                    periodEndDate = null;
                    return false;
                }

        }
        public string GetPeriodStartDate()
        {
            return periodStartDate;
        }
        public bool SetPeriodStartDate(string value)
        {
            //date formate checking 
            string regex = @"^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2])\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)0?2\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9])|(?:1[0-2]))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$";

                var match = Regex.Match(value, regex);
                if (match.Success)
                {
                DateTime startDate = new DateTime();
                if (DateTime.TryParse(value, out startDate))
                {
                    periodStartDate = value;
                    return true;
                }
                return false;
                }
                else
                {
                    periodStartDate = null;
                    return false;
                }
        }

    }
}