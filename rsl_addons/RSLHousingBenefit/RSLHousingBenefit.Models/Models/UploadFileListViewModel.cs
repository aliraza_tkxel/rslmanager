﻿using RSLHousingBenefit.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSLHousingBenefit.Models.Models
{
    public class UploadFileListViewModel
    {
        public List<UploadFileListModel> uploadFileListModel { get; set; }= new List<UploadFileListModel>();
        public PaginationViewModel pagination { get; set; } = new PaginationViewModel();
    }
}
