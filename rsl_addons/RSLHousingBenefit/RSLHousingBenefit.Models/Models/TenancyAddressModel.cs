﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSLHousingBenefit.Models.Models
{
    public class TenancyAddressModel
    {
        public string  HouseNumber { get; set; }
        public string  Address { get; set; }

    }
}
