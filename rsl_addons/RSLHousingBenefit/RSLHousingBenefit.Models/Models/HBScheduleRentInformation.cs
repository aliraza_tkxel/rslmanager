﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSLHousingBenefit.Models
{
    public class HBScheduleRentInformation
    {
        public int? TenancyId { get; set; }
        public DateTime? TransactionDate { get; set; }
        public int? ItemType { get; set; }
        public int? PaymentType { get; set; }
        public DateTime? PaymentStartDate { get; set; }
        public DateTime? PaymentEndDate { get; set; }
        public double? Amount { get; set; }
        public int? HBRow { get; set; }
    }
}
