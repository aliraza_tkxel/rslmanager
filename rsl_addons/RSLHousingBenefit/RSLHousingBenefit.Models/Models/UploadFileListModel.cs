﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSLHousingBenefit.Models.Models
{
    public class UploadFileListModel
    {
        public int? HbUploadId { set; get; }
        public string UploadDate { get; set; }
        public string LocalAuthority { get; set; }
        public string FileName { get; set; }
        public string UploadBy { get; set; }
        public int UploadRecord { get; set; }
        public double UploadRecordTotal { get; set; }
        public int ExcludeRecord { get; set; }
        public double ExcludeRecordTotal { get; set; }
        public int OrignalRecord { get; set; }
        public double OrignalRecordTotal { get; set; }
        public string  FilePath { get; set; }
    }
}
