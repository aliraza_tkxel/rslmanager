﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSLHousingBenefit.Models.Models
{
    public class ExceptionFileListModel
    {
        public int HBUploadFileDataId { get; set; }
        public Nullable<int> HbUploadId { get; set; }
        public string HBRef { get; set; }
        public Nullable<double> Amount { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public DateTime ? startDate { get; set; }
        public DateTime ? endDate { get; set; }
        public string TenantName { get; set; }
        public string Address { get; set; }
        public Nullable<bool> IsExcluded { get; set; }
    }
}
