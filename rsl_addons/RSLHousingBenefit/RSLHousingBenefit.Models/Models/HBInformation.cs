﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSLHousingBenefit.Models.Models
{
    public class HBInformation
    {
        public int Hbid { get; set; }
        public int CustomerId { get; set; }
        public Nullable<int> TenancyId { get; set; }
        public string HbRef { get; set; }
        public Nullable<double> InitialPayment { get; set; }
        public string InitialStartDate { get; set; }
        public string InitialEndDate { get; set; }
        public string ActualEndDate { get; set; }
        public int Status { get; set; }
        public Nullable<int> Hbla { get; set; }
        public double DefaultCycle { get; set; }
    }
}
