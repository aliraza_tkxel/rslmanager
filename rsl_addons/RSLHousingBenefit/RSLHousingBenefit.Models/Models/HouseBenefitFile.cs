﻿using System;

namespace RSLHousingBenefit.Models.Models
{
    public class HouseBenefitFile
    {
        public int HbUploadId { get; set; }
        public Nullable<int> LocalAuthorityId { get; set; }
        public Nullable<System.DateTime> TransactionDate { get; set; }
        public string FileName { get; set; }
        public string ActualFileName { get; set; }
        public Nullable<bool> IsProcessed { get; set; }
        public Nullable<int> UploadedBy { get; set; }
        public Nullable<System.DateTime> UploadedDate { get; set; }
    }
}
