﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSLHousingBenefit.Models.Models
{
    public class GetNewHBScheduleModel
    {
        public GetNewHBScheduleModel()
        {
            hbInformation = new HBInformation();
        }
        public HBInformation hbInformation { get; set; }
        public int ExceptionId { get; set; }
        public string Address { set; get; }
        public string  CustomerName { get; set; }
    }
}
