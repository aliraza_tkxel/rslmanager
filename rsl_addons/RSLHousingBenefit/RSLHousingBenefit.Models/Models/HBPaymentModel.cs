﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSLHousingBenefit.Models.Models
{
    public class HBPaymentModel
    {
        public int HbRow { get; set; }
        public int HbId { get; set; }
        public string StartDate { get; set; }
        public string  EndDate { get; set; }
        public Nullable<double> Hb { get; set; }
        public Nullable<int> Sent { get; set; }
        public Nullable<int> Validated { get; set; }
        public Nullable<int> JournalId { get; set; }
        public double DefaultCycle { get; set; }
        public DateTime Start { get; set; }
    }
}
