﻿using System;

namespace RSLHousingBenefit.Models.ViewModels.HBUploadFile
{
    public class LocalAuthorityViewModel
    {
        public int LOCALAUTHORITYID { get; set; }
        public string DESCRIPTION { get; set; }
        public Nullable<int> LINKTOSUPPLIER { get; set; }
        public string Nrosh_Code { get; set; }
    }
}
