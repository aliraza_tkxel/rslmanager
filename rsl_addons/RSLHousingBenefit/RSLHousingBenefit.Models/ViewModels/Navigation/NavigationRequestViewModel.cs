﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSLHousingBenefit.Models.ViewModels
{
    public class NavigationRequestViewModel
    {
        public int userId { get; set; }
        public string moduleName { get; set; }
    }
}
