﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSLHousingBenefit.Models.ViewModels
{
    public class SubmenuViewModel
    {

        public int menuId { get; set; }
        public string subMenuName { get; set; }
        public string path { get; set; }

    }
}
