﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSLHousingBenefit.Models.ViewModels
{
    public class ExceptionViewModel
    {
        public int exceptionId { get; set; }
        public string reason { get; set; }
        public int? reasonId { get; set; }
        public bool? isResolved { get; set; } = false;
        public bool? isExcluded { get; set; } = false;

        public int? hbUploadDataId { get; set; }
        public string hbRef { get; set; }
        public int? hbId { get; set; }
        public string tenantName { get; set; }
        public string address { get; set; }
        public string startDateString { get; set; }
        public string endDateString { get; set; }
        public DateTime? startDate { get; set; }
        public DateTime? endDate { get; set; }
        public double? amount { get; set; }
    }
}
