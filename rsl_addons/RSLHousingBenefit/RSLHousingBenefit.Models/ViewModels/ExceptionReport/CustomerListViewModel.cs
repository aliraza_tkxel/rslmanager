﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSLHousingBenefit.Models.ViewModels
{
    public class CustomerListViewModel
    {        
        public List<CustomerViewModel> customers { get; set; }        
    }
}
