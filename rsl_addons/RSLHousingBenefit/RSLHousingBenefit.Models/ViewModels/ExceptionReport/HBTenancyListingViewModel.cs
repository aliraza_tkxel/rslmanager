﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSLHousingBenefit.Models.ViewModels
{
    public class HBTenancyListingViewModel
    {
        public int exceptionId { get; set; }
        public List<HBTenancyViewModel> hbTenancyListing { get; set; }

    }
}
