﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSLHousingBenefit.Models.ViewModels
{
   public class HBTenancyViewModel
    {
        public int HbId { get; set; }
        public int CustomerId { get; set; }
        public Nullable<int> TenancyId { get; set; }
        public string CustomerName { get; set; }
        public string FullAddress { get; set; }
    }
}
