﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSLHousingBenefit.Models.ViewModels
{
    public class CustomerSearchRequestViewModel
    {
        public int exceptionId { get; set; }

        [Display(Name = "First Name:")]
        public string firstName { get; set; }

        [Display(Name = "Last Name:")]
        public string lastName { get; set; }

        [Display(Name = "Address:")]
        public string address { get; set; }

        [Display(Name = "Postcode:")]
        public string postcode { get; set; }
    }
}
