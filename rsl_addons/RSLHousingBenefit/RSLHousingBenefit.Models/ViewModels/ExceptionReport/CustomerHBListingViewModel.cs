﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSLHousingBenefit.Models.ViewModels
{
    public class CustomerHBListingViewModel
    {
        public string customerName { get; set; }
        public string customerAddress { get; set; }
        public int exceptionId { get; set; }
        public int tenancyId { get; set; }
        public int customerId { get; set; }
        public List<HBInfoViewModel> hbListing { get; set; }
    }
}
