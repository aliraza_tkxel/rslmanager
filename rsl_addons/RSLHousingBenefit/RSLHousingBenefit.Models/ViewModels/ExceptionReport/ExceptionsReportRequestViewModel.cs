﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSLHousingBenefit.Models.ViewModels
{
    public class ExceptionsReportRequestViewModel
    {
        public int hbUploadId { get; set; }
        public PaginationViewModel pagination { get; set; } = new PaginationViewModel();
    }
}
