﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSLHousingBenefit.Models.ViewModels
{
    public class ExceptionsListViewModel
    {
        public int hbUploadId { get; set; }
        public List<ExceptionViewModel> hbExceptions { get; set; }
    }
}
