﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSLHousingBenefit.Models.ViewModels
{
    public class HBInfoViewModel
    {
        public int hbId { get; set; }
        public string hbref { get; set; }
        public int? tenancyId { get; set; }
        public int? customerId { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string localAuthority { get; set; }
    }
}
