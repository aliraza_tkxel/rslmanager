﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSLHousingBenefit.Models.ViewModels
{
    public class CustomerViewModel
    {
        public int customerId { get; set; }

        public string customerName { get; set; }

        public string tenant { get; set; }

        public string address { get; set; }

        public int tenancyCount { get; set; }

        public int customerType { get; set; }

        public string customerTypeDesc { get; set; }

        public string imgTitle { get; set; }

        public int tenancyId { get; set; }
    }
}
