﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RSLHousingBenefit.Common.Constants;
using RSLHousingBenefit.Interfaces.Service;
using RSLHousingBenefit.Models.ViewModels;
using RSLHousingBenefit.Models.Models;

namespace RSLHousingBenefit.Web.Controllers.HBUpload
{
    public class ExceptionsReportController : ABaseController
    {
        private IExceptionReportService _exceptionReportService;
        private IHBActualScheduleServices _hBActualScheduleServices;

        public ExceptionsReportController(IExceptionReportService exceptionReportService, IHBActualScheduleServices hBActualScheduleServices)
        {
            _exceptionReportService = exceptionReportService;
            _hBActualScheduleServices = hBActualScheduleServices;
        }

        #region "Hb Exceptions Report"
        public ActionResult HbExceptionsReport(int hbUploadId)
        {
            ExceptionsListViewModel model = _exceptionReportService.GetExceptionReportList(hbUploadId);
            model.hbUploadId = hbUploadId;
            ViewBag.HBUploadId = hbUploadId;

            return View(model);
        }
        #endregion

        #region "Get HB Exceptions List"
        public ActionResult GetHBExceptionsList(int hbUploadId)
        {
            ExceptionsListViewModel model = _exceptionReportService.GetExceptionReportList(hbUploadId);
            model.hbUploadId = hbUploadId;
            return PartialView("~/Views/ExceptionsReport/_HbExceptionsList.cshtml", model);
        }
        #endregion

        #region "Validate Exceptions"
        public ActionResult ValidateExceptions(int hbUploadId)
        {
            ExceptionsListViewModel model = _exceptionReportService.ValidateExceptions(hbUploadId);
            model.hbUploadId = hbUploadId;
            return PartialView("~/Views/ExceptionsReport/_HbExceptionsList.cshtml", model);
        }
        #endregion

        #region "Search Customer"
        public ActionResult CustomerSearch(int exceptionId)
        {
            ViewBag.ExceptionId = exceptionId;
            return PartialView("~/Views/ExceptionsReport/_CustomerSearch.cshtml");
        }
        #endregion

        #region "Amend Start And End Date Flow"
        public ActionResult AmendStartAndEndDateFlow(int exceptionId)
        {
            ViewBag.ExceptionId = exceptionId;
            HBTenancyListingViewModel response = _exceptionReportService.GetHbTenancyListingByHBref(exceptionId);
            response.exceptionId = exceptionId;

            if (response.hbTenancyListing.Count() > 1)
            {
                return PartialView("~/Views/ExceptionsReport/_MultipleTenancy.cshtml", response);
            }
            else
            {
                return RedirectToAction("GetCustomerHBDetails", new { hbId = response.hbTenancyListing.First().HbId, exceptionId=response.exceptionId });
               // return PartialView("~/Views/ExceptionsReport/GetCustomerHBDetails.cshtml", response);
            }

        }
        #endregion

        #region "Process HB Payments"
        public void ProcessHBPayment(int hbUploadId)
        {
            _exceptionReportService.processPayment(hbUploadId);
        }
        #endregion


        #region "Get Customer List"

        public ActionResult GetCustomerList(string firstName, string lastName, string address, string postcode)
        {
            CustomerListViewModel model = _exceptionReportService.SearchCustomer(firstName, lastName, address, postcode);

            #region "Setup Customer Image Icon"

            string imgTitle = string.Empty;
            foreach (var customer in model.customers)
            {
                string custType = customer.customerTypeDesc;
                string tenantType = customer.tenant;
                if (custType.Equals(ApplicationConstants.CustomerTypeTenant)
                    || custType.Equals(ApplicationConstants.CustomerTypePreviousTenant))
                {
                    if (tenantType.Equals(ApplicationConstants.ImgTitleTenant))
                    {
                        customer.imgTitle = ApplicationConstants.ImgTitleTenant;
                        customer.customerTypeDesc = ApplicationConstants.CustomerTypeTenant;
                    }
                    else
                    {
                        customer.imgTitle = ApplicationConstants.ImgTitlePreviousTenant;
                        customer.customerTypeDesc = ApplicationConstants.CustomerTypePreviousTenant;
                    }
                }
                else if (custType.Equals(ApplicationConstants.CustomerTypeGeneral))
                {
                    customer.imgTitle = ApplicationConstants.ImgTitleGeneral;
                }
                else if (custType.Equals(ApplicationConstants.CustomerTypeProspectiveTenant))
                {
                    customer.imgTitle = ApplicationConstants.ImgTitleProspectiveTenant;
                }
                else if (custType.Equals(ApplicationConstants.CustomerTypeStakeholder))
                {
                    customer.imgTitle = ApplicationConstants.ImgTitleStakeholder;
                }
            }

            #endregion

            return PartialView("~/Views/ExceptionsReport/_CustomerListing.cshtml", model);
        }

        #endregion

        #region "Get Customer Housing Benefit List"

        public ActionResult GetCustomerHousingBenefitList(int customerId, int tenancyId, string customerName, string address, int exceptionId)
        {
            CustomerHBListingViewModel model = _exceptionReportService.GetCustomerHBListing(customerId, tenancyId);
            model.customerName = customerName;
            model.customerAddress = address;
            model.exceptionId = exceptionId;
            model.customerId = customerId;
            model.tenancyId = tenancyId;
            ViewBag.ExceptionId = exceptionId;
            ViewBag.CustomerId = customerId;
            ViewBag.TenancyId = tenancyId;

            return PartialView("~/Views/ExceptionsReport/_CustomerHousingBenefitListing.cshtml", model);
        }

        #endregion
        [HttpGet]
        public ActionResult GetCustomerHBDetails(int hbId, int exceptionId)
        {
            try
            {
                var hBActualScheduleServices = _hBActualScheduleServices.GetCustomerRecordById(hbId);
                var HBRow = _hBActualScheduleServices.GetHBRow(exceptionId);
                hBActualScheduleServices.HBRow = HBRow;
                hBActualScheduleServices.HBId = hbId;
                hBActualScheduleServices.HBUploadExceptionId = exceptionId;
                hBActualScheduleServices.CustomerId = hBActualScheduleServices.hBInformation.CustomerId;
                hBActualScheduleServices.TenancyId = hBActualScheduleServices.hBInformation.TenancyId;
                hBActualScheduleServices.CustomerName = _hBActualScheduleServices.GetCustomerName(hBActualScheduleServices.CustomerId);
                hBActualScheduleServices.Address = _hBActualScheduleServices.GetTenancyAddress(hBActualScheduleServices.CustomerId, hBActualScheduleServices.TenancyId.Value);
                ViewBag.exceptionReason = _hBActualScheduleServices.GetExceptionReason(exceptionId);
                return PartialView("CustomerHBDetails", hBActualScheduleServices);
            }
            catch (Exception e)
            {
                return null;
            }
        }


        [HttpPost]
        public ActionResult CustomerHBDetails(int hbId, int exceptionId)
        {
            try
            {
                var hBActualScheduleServices = _hBActualScheduleServices.GetCustomerRecordById(hbId);
                var HBRow = _hBActualScheduleServices.GetHBRow(exceptionId);
                hBActualScheduleServices.HBRow = HBRow;
                hBActualScheduleServices.HBId = hbId;
                hBActualScheduleServices.HBUploadExceptionId = exceptionId;
                hBActualScheduleServices.CustomerId = hBActualScheduleServices.hBInformation.CustomerId;
                hBActualScheduleServices.TenancyId = hBActualScheduleServices.hBInformation.TenancyId;
                hBActualScheduleServices.CustomerName= _hBActualScheduleServices.GetCustomerName(hBActualScheduleServices.CustomerId);
                hBActualScheduleServices.Address = _hBActualScheduleServices.GetTenancyAddress(hBActualScheduleServices.CustomerId, hBActualScheduleServices.TenancyId.Value);
                ViewBag.exceptionReason = _hBActualScheduleServices.GetExceptionReason(exceptionId);
                return PartialView("CustomerHBDetails", hBActualScheduleServices);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        [HttpPost]
        public ActionResult InculdePaymentInUpload(int HBId, int HBUploadExceptionId, string HbRef, DateTime newStartDate, DateTime newEndDate, double newHb, int CustomerId, Int64 TenancyId)
        {
            bool status = false;
            if (_exceptionReportService.UpdateHbReferance(HBId, CustomerId, TenancyId, HbRef) == true)
            {
                status = _hBActualScheduleServices.UpdateHbActualScheduleService(HBId, newStartDate, newEndDate, newHb, CustomerId, TenancyId);
                status = _exceptionReportService.ResolveException(HBUploadExceptionId, HbRef, newStartDate, newEndDate, newHb);
                Response.StatusCode = 200;
                Response.StatusDescription = "update successfully";
            }
            else
            {
                Response.StatusCode = 400;
                Response.StatusDescription = "HB Ref already exist for another customer";
            }
            return null;


        }

        [HttpGet]
        public ActionResult GetNextPayments(int HBId)
        {
            var hBActualScheduleServices = _hBActualScheduleServices.GetNextPayment(HBId);
            return PartialView("_GetNextPayments", hBActualScheduleServices);

        }

        [HttpGet]
        public ActionResult GetDummyNextPayments(DateTime newStartDate, DateTime newEndDate, double newHb)
        {

            var hBActualScheduleServices = _hBActualScheduleServices.GetDummyNextPayment(newStartDate, newEndDate, newHb);
            return PartialView("_GetNextPayments", hBActualScheduleServices);

        }

        [HttpPost]
        public void UpdateHbActualScheduleService(int HBId, DateTime newStartDate, DateTime newEndDate, double newHb, int CustomerId, Int64 TenancyId)
        {
            _hBActualScheduleServices.UpdateHbActualScheduleService(HBId, newStartDate, newEndDate, newHb, CustomerId, TenancyId);
            //var hBActualScheduleServices = _hBActualScheduleServices.GetNextPayment(HBId);
            //return PartialView("_GetNextPayments", hBActualScheduleServices);
        }

        [HttpPost]
        public void RemovePayment(int HbUploadFileDataId, bool isExclude)
        {
            _hBActualScheduleServices.ExcludeHbUpload(HbUploadFileDataId, isExclude);
        }


        [HttpPost]
        public ActionResult GetNewHBSchedule(int exceptionId, int customerId, int tenancyId)
        {

            GetNewHBScheduleModel getNewHBSchedule = new GetNewHBScheduleModel();
            getNewHBSchedule.ExceptionId = exceptionId;
            getNewHBSchedule.hbInformation.CustomerId = customerId;
            getNewHBSchedule.hbInformation.TenancyId = tenancyId;
            getNewHBSchedule.CustomerName = _hBActualScheduleServices.GetCustomerName(customerId);
            getNewHBSchedule.Address = _hBActualScheduleServices.GetTenancyAddress(customerId, tenancyId);
            return PartialView("_GetNewHBSchedule", getNewHBSchedule);
        }


        [HttpPost]
        public int SubmitNewHBSchedule(int exceptionId, int customerId, int tenancyId, string HBRef, double initialPayment, DateTime startDate, DateTime endDate)
        {
            try
            {
                var HBId = _exceptionReportService.SubmitNewHBSchedule(exceptionId, customerId, tenancyId, HBRef, initialPayment, startDate, endDate);
                if (HBId >= 0)
                {
                    Response.StatusCode = 200;
                    return HBId;
                }
                else
                {
                    Response.StatusCode = 400;
                    Response.StatusDescription = "HB Ref already exist for another customer";
                    return -1;
                }

            }
            catch (Exception ex)
            {
                Response.StatusCode = 500;
                Response.StatusDescription = "Invalid Request";
                return -1;
            }
        }

    }
}