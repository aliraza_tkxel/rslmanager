﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using RSLHousingBenefit.Interfaces.Service;
using System.IO;
using RSLHousingBenefit.Common.Helper;
using OfficeOpenXml;
using System.Net;
using Newtonsoft.Json;
using Rotativa;
using RSLHousingBenefit.Services.Services;
using RSLHousingBenefit.Models;
using RSLHousingBenefit.Models.Models;
using RSLHousingBenefit.Common.Constants;
using RSLHousingBenefit.Models.ViewModels;
using Spire.Xls;

namespace RSLHousingBenefit.Web.Controllers
{
    public class FileUploaderController : ABaseController
    {
        private IHBUploadService _hBUploadService;
        private IExceptionReportService _exceptionReportService;
        public FileUploaderController(IHBUploadService hBUploadService, IExceptionReportService exceptionReportService)
        {
            _hBUploadService = hBUploadService;
            _exceptionReportService = exceptionReportService;
        }
        [HttpGet]
        public ActionResult UploadFile()
        {
            return View();
        }
        [HttpPost]
        public JsonResult UploadFile(HttpPostedFileBase UploadedFile)
        {
            try
            {
                string fileName = Request.Headers["X-File-Name"];
                var fileExtention = fileName.Split('.');
                string folderName = "";
                if (fileExtention.Length == 2)
                {
                    switch (fileExtention[1])
                    {
                        case "xls":
                            folderName = "HBDoc";
                            fileName = "HBReferance" + DateTime.Now.ToString("hh_mm_ss_ffffff") + ".xlsx";
                            break;
                        case "csv":
                            folderName = "HBDoc";
                            fileName = "HBReferance" + DateTime.Now.ToString("hh_mm_ss_ffffff") + "." + fileExtention[1];
                            break;
                            //add default option 
                    }
                }
                //string fileType = Request.Headers["X-File-Type"];
                //int fileSize = Convert.ToInt32(Request.Headers["X-File-Size"]);
                System.IO.Stream fileContent = Request.InputStream;
                //string directorypath = SetupDirectoryPath(); Uncomment this line before deploying
                //System.IO.FileStream fileStream = System.IO.File.Create( directorypath + fileName); Uncomment this line before deploying

                //string projectPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;//remove these lines before deploying 
                string projectPath = FileHelper.getFileDirectoryPath(ApplicationConstants.TypeHBDoc);
                string filePath = Path.Combine(projectPath, folderName);//remove these lines before deploying 
                System.IO.Directory.CreateDirectory(filePath);//remove these lines before deploying 
                

                List<String> FileHeading = new List<string>();
                if (fileExtention[1].CompareTo("xls") == 0)
                {
                    Workbook workbook = new Workbook();
                    workbook.LoadFromStream(fileContent);
                    workbook.SaveToFile(filePath + "\\"+fileName , ExcelVersion.Version2013);
                    FileHeading = GetXlsFileHeadings(filePath + "\\" + fileName);
                }
                else
                {
                    System.IO.FileStream fileStream = System.IO.File.Create(filePath + "\\" + fileName);
                    fileContent.Seek(0, System.IO.SeekOrigin.Begin);
                    fileContent.CopyTo(fileStream);
                    fileStream.Dispose();
                    FileHeading = GetCsvFileHeadings(filePath + "\\" + fileName);
                }
                
                string lastHeading = FileHeading.LastOrDefault();
                var localAuthorities = _hBUploadService.GetLocalAuthorities().Select(l => new { l.DESCRIPTION, l.LOCALAUTHORITYID }).ToList();
                var jsonString = JsonConvert.SerializeObject(new { fileHeading = FileHeading, localAuthorities = localAuthorities, fileName = fileName, lastHeading= lastHeading });
                return Json(jsonString);
            }
            catch (Exception ex)
            {

                //throw new NullReferenceException();
                return Json(HttpStatusCode.BadRequest);
            }
        }
        [HttpGet]
        public ActionResult UploadFileForm()
        {
            //List<string> localAuthorities = _hBUploadService.GetLocalAuthorities().Select(l => l.DESCRIPTION).ToList();
            //ViewData["LocalAuthorities"] = localAuthorities;
            return PartialView();
        }
        [HttpPost]
        public JsonResult UploadFileForm(FormCollection collection)
        {
            try
            {
                int[] dataLocation = new int[7];
                HouseBenefitModel houseBenefitModel = new HouseBenefitModel();
                string localAuthority = collection["LocalAuthority"];
                string HBFileNameOriginal = collection["HBFileName"];
                string HBFileName = collection["HBFilePath"];
                string Transaction = collection["Transaction"];
                dataLocation[0] = Convert.ToInt32(collection["InitialRow"]);
                dataLocation[1] = Convert.ToInt32(collection["HBRefarance"]);
                dataLocation[2] = Convert.ToInt32(collection["Amount"]);
                dataLocation[3] = Convert.ToInt32(collection["PeriodStartDate"]);
                dataLocation[4] = Convert.ToInt32(collection["PeriodEndDate"]);
                dataLocation[5] = Convert.ToInt32(collection["TanantName"]);
                dataLocation[6] = Convert.ToInt32(collection["Address"]);
                var fileExtension = HBFileNameOriginal.Split('.');
                var HBFileData = new List<HouseBenefitModel>(); ;
                dynamic status;
                int uploadFileId = 0;
                string result = string.Empty;
                if (fileExtension.Length == 2)
                {
                    switch (fileExtension[1])
                    {

                        case "xls":
                            XlsFileReader xslFileReader = new XlsFileReader();
                            status = xslFileReader.ReadExcelFile(HBFileName, dataLocation);
                            result = status.Item1;
                            HBFileData = status.Item2;
                            break;
                        case "csv":
                            CsvFileReader CsvFileReader = new CsvFileReader();
                            status = CsvFileReader.ReadCsvFile(HBFileName, dataLocation);
                            result = status.Item1;
                            HBFileData = status.Item2;
                            break;
                    }
                }
                if (result == "successful")
                {
                    HouseBenefitFile houseBenefitFile = new HouseBenefitFile
                    {
                        ActualFileName = HBFileNameOriginal,
                        FileName = HBFileName,
                        LocalAuthorityId = Convert.ToInt32(localAuthority),
                        TransactionDate = Convert.ToDateTime(Transaction),
                        UploadedBy = SessionManager.GetSessionUserId(),
                        UploadedDate = DateTime.Now
                    };
                     uploadFileId = _hBUploadService.AddHBUploadFile(houseBenefitFile);                    
                    _hBUploadService.AddHBUploadFileData(HBFileData, uploadFileId);
                    _exceptionReportService.validateHBUploadedData(uploadFileId);
                }
                var jsonString = JsonConvert.SerializeObject(new { result = result, hBUploadId= uploadFileId });
                return Json(jsonString);
            }
            catch (Exception ex)
            {
                return Json(HttpStatusCode.BadRequest);
            }
        }
        public JsonResult PreviewHtml(string fileName, string lastColumn)
        {
            string folderName = "HBDoc"; ;
            //string projectPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;//remove these lines before deploying 
            string projectPath = FileHelper.getFileDirectoryPath(ApplicationConstants.TypeHBDoc);
            string filePath = Path.Combine(projectPath, folderName);//remove these lines before deploying 
            ExcelToHtml excelToHtml = new ExcelToHtml();
            string path = excelToHtml.ConvertToHtml(filePath, fileName,lastColumn);
            return Json(JsonConvert.SerializeObject(path));
        }
        public ActionResult partialViewResult(string fileName,string lastColumn)
        {
            try
            {
                string folderName = "HBDoc";
                //string projectPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;//remove these lines before deploying 
                string projectPath = FileHelper.getFileDirectoryPath(ApplicationConstants.TypeHBDoc);
                string filePath = Path.Combine(projectPath, folderName);//remove these lines before deploying 
                ExcelToHtml excelToHtml = new ExcelToHtml();
                string path = excelToHtml.ConvertToHtml(filePath, fileName,lastColumn);

                return new FilePathResult(path, "text/html");
            }
            catch (Exception ex)
            {
                return Json(HttpStatusCode.BadRequest);
            }
        }
        public ActionResult UploadList()
     {
            PaginationViewModel page = new PaginationViewModel();
          
            int pageNo = 1;
            if (HttpContext.Request.QueryString[QueryStringConstants.Page] != null
                && HttpContext.Request.QueryString[QueryStringConstants.Page] != "0")
            {
                pageNo = Convert.ToInt32(HttpContext.Request.QueryString[QueryStringConstants.Page]);
            }

            page.pageSize = _pageSize;
            page.pageNo = pageNo;
            page.sortBy = ApplicationConstants.HBUploadFileCol;
            page.sortOrder = ApplicationConstants.OrderDesc;
            if (HttpContext.Request.QueryString[QueryStringConstants.SortBy] != null
                && HttpContext.Request.QueryString[QueryStringConstants.SortOrder] != null)
            {
                page.sortBy = HttpContext.Request.QueryString[QueryStringConstants.SortBy].ToString();
                page.sortOrder = HttpContext.Request.QueryString[QueryStringConstants.SortOrder].ToString();
            }

            var uploadFileData = _hBUploadService.GetUploadFileData(page);
            ViewBag.TotalRows = uploadFileData.pagination.totalRows;
            ViewBag.PageSize = uploadFileData.pagination.pageSize;
            return PartialView("UploadList", uploadFileData.uploadFileListModel);
        }
        //private string SetupDirectoryPath()
        //{
        //    string directorypath = string.Empty;
        //    directorypath = FileHelper.getFileDirectoryPath("HBDoc", "");
        //    FileHelper.createDirectory(directorypath);
        //    return directorypath;

        //}
        [HttpGet]
        public ActionResult ExceptionReport(int hbUploadId)
        {
            try
            {
                var exceptionFileData = _hBUploadService.GetExceptionUploadFileData(hbUploadId);
                Response.StatusCode = 200;
                return PartialView("ExceptionReport", exceptionFileData);
            }
            catch(Exception e)
            {
                Response.StatusCode = 400;
                return null;
            }
        }
        [HttpGet]
        public ActionResult DownloadExceptionReport(int hbUploadId)
        {
            try
            {
                var exceptionFileData = _hBUploadService.GetExceptionUploadFileData(hbUploadId);
                Response.StatusCode = 200;
                return new ViewAsPdf("~/Views/FileUploader/ExceptionReport.cshtml", exceptionFileData)
                {
                    FileName = "Exception Report.pdf",
                    CustomSwitches = "--footer-spacing \"5\" --footer-font-size \"8\" --footer-center \"Page [page] of [toPage]\" --footer-right \"PDF Created " + DateTime.Now.ToString() + "\" ",
                    PageMargins = { Top = 15, Bottom = 20 },
                    PageSize = Rotativa.Options.Size.A4
                };
            }
            catch (Exception)
            {
                Response.StatusCode = 400;
                return null;
            }
            }
        private List<string> GetXlsFileHeadings(string Path)
        {
            List<string> ColumnHeading = new List<string>(); 
            var package = new ExcelPackage(new FileInfo(Path));
            ExcelWorksheet workSheet = package.Workbook.Worksheets[1];
            var end = workSheet.Dimension.End;
            int FirstLateral = 32;
            int SecondLateral = 65;
            for (int i = 0,characterIncrement = 0; i < end.Column; i++,characterIncrement++)
            {
                ColumnHeading.Add(((char)FirstLateral).ToString() + ((char)(SecondLateral+characterIncrement)));
                if (characterIncrement == 25)
                {
                    characterIncrement = 0;
                    SecondLateral = 65;
                    FirstLateral = (64+i / 25);
                }
            }
            return ColumnHeading;
        }
        private List<string> GetCsvFileHeadings(string Path)
        {
            
            List<string> ColumnHeading = new List<string>();
            System.IO.StreamReader file =
                new System.IO.StreamReader(Path);
            var line = file.ReadLine();
            var end = line.Split(',').Length;
            int FirstLateral = 32;
            int SecondLateral = 65;
            for (int i = 0, characterIncrement = 0; i < end; i++, characterIncrement++)
            {
                ColumnHeading.Add(((char)FirstLateral).ToString() + ((char)(SecondLateral + characterIncrement)));
                if (characterIncrement == 25)
                {
                    characterIncrement = 0;
                    SecondLateral = 65;
                    FirstLateral = (64 + i / 25);
                }
            }
            file.Close();
            return ColumnHeading;
        }
    }
}