﻿using RSLHousingBenefit.Common.Helper;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RSLHousingBenefit.Web.Controllers
{
    public class ABaseController : Controller
    {        
        public int _pageSize = 15;
        public int _reportPageSize = 25;
        public int exportPageSize = 200000;

        public ABaseController()
        {

        }
        protected override void OnException(ExceptionContext filterContext)
        {
            try
            {
                if (filterContext.ExceptionHandled)
                {
                    return;
                }
            }
            catch (Exception ex)
            {                
                HandleException(filterContext, ex.Message);
            }
        }
        private void HandleException(ExceptionContext context, string Message)
        {
            context.Result = new ViewResult
            {
                ViewData = new ViewDataDictionary(),
                ViewName = "ErrorView"
            };
            ViewBag.ErrorMessage = Message;
            context.ExceptionHandled = true;
        }

        protected override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            HttpContext.Response.Cache.SetExpires(DateTime.UtcNow.AddDays(-1));
            HttpContext.Response.Cache.SetValidUntilExpires(false);
            HttpContext.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
            HttpContext.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            HttpContext.Response.Cache.SetNoStore();
            filterContext.HttpContext.Response.Cache.SetExpires(DateTime.UtcNow.AddDays(-1));
            filterContext.HttpContext.Response.Cache.SetValidUntilExpires(false);
            filterContext.HttpContext.Response.Cache.SetRevalidation(HttpCacheRevalidation.AllCaches);
            filterContext.HttpContext.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            filterContext.HttpContext.Response.Cache.SetNoStore();
            base.OnResultExecuting(filterContext);
        }
        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            // Need to remove user session id            
            SessionManager.SetSessionUserId(423);
            var userId = SessionManager.GetSessionUserId();
            if (userId == 0)
            {
                redirectToLoginPage();
            }
        }

        public void ExportXLS(object objSource, string fileName)
        {
            GridView gv = new GridView();
            gv.DataSource = objSource;
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
            Response.ContentType = "application/ms-excel";
            Response.Charset = "";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            gv.RenderControl(htw);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }

        public void redirectToLoginPage()
        {
            HttpContext.Response.Redirect(ConfigHelper.GetLoginPagePath(), true);
        }

    }
}