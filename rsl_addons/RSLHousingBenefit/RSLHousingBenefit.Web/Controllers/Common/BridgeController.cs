﻿
using RSLHousingBenefit.Common.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RSLHousingBenefit.Web.Controllers
{
    public class BridgeController : Controller
    {
        // GET: Bridge
        public ActionResult Index()
        {
            if (Request.QueryString["UserId"] != null)
            {
                int userId = int.Parse(Request.QueryString["UserId"]);
                SessionManager.SetSessionUserId(userId);
            }

            return RedirectToAction("UploadFile", "fileUploader");
        }
    }
}