﻿using RSLHousingBenefit.Interfaces.Service;
using RSLHousingBenefit.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RSLHousingBenefit.Web.Controllers.HBCustomerRecord
{
    public class HBCustomerRecordController : Controller
    {
        private IHBActualScheduleServices _hBActualScheduleServices;
        private IExceptionReportService  _exceptionReportService;

        public HBCustomerRecordController(IHBActualScheduleServices hBActualScheduleServices, IExceptionReportService exceptionReportService)
        {
            _hBActualScheduleServices = hBActualScheduleServices;
            _exceptionReportService = exceptionReportService;
        }
        [HttpGet]
        public ActionResult HBCustomerRecord(/*int HBId , int HBUploadExceptionId*/)
        {
            int HBId = 16255;
            int HBUploadExceptionId = 0;
            var hBActualScheduleServices =_hBActualScheduleServices.GetCustomerRecordById(HBId);            
            return PartialView("HBCustomerRecord", hBActualScheduleServices);
        }
        public void  InculdePaymentInUpload(int HBId, int HBUploadExceptionId,string HbRef,DateTime newStartDate,DateTime newEndDate,double newHb,int CustomerId,Int64 TenancyId)
        {
            _exceptionReportService.UpdateHbReferance(HBId,CustomerId,TenancyId ,HbRef);
            _hBActualScheduleServices.UpdateHbActualScheduleService(HBId, newStartDate, newEndDate, newHb,CustomerId,TenancyId);


            

        }
    }
}