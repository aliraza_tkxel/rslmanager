﻿using RSLHousingBenefit.Common.Constants;
using RSLHousingBenefit.Common.Helper;
using RSLHousingBenefit.Interfaces.Service;
using RSLHousingBenefit.Models.ViewModels;
using System.Web.Mvc;

namespace RSLHousingBenefit.Web.Controllers
{
    public class NavigationController : ABaseController
    {

        private INavigationService _navigationService;

        public NavigationController(INavigationService navigationService)
        {
            _navigationService = navigationService;
        }

        public ActionResult GetTopNavigationMenu()
        {            
            var request = new NavigationRequestViewModel();
            request.userId = SessionManager.GetSessionUserId();
            request.moduleName = ApplicationConstants.FinanceModuleName;
            NavigationViewModel navViewmodel = _navigationService.GetMenus(request);            
            navViewmodel.currentModule = ApplicationConstants.FinanceModuleName;
            navViewmodel.currentMenu = ApplicationConstants.ValidationMenuName;
            return PartialView("~/Views/Shared/_Navigation.cshtml", navViewmodel);
        }
    }
}