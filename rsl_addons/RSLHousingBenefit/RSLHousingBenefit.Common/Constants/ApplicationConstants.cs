﻿
namespace RSLHousingBenefit.Common.Constants
{
    public class ApplicationConstants
    {
        #region "Navigation"
        public const string LoginPath = "~/Home/Login";
        public const string FinanceModuleName = "Finance";
        public const string ValidationMenuName = "Validation";        
        public const string UsersAccountDeactivated = "Your access to Contractor Module has been de-activiated. Please contact administrator.";
        public const string KeyDateFormat = "DateFormat";
        public const string KeyDateFormatModel = "DateFormatForModel";
        public const string VirtualDirectory = "VirtualDirectory";
        public const string ApplicationDateForamt = "dd-MM-yyyy";        
        #endregion

        #region "General"
        public const string TypeScheme = "scheme";
        public const string TypeBlock = "block";
        public const string TypeProperty = "property";
        public const string TypeHBDoc = "HBDoc";
        public const string OrderDesc = "Desc";
        public const int OrderDes = 1;
        public const string OrderAsc = "Asc";
        public const string DefaultCycle = "28";
        public const int  NextPaymentCount = 12;
        #endregion

        #region "Exception Report"

        public const string CustomerTypeProspectiveTenant = "Prospective Tenant";
        public const string CustomerTypeTenant = "Tenant";
        public const string CustomerTypePreviousTenant = "Previous Tenant";
        public const string CustomerTypeStakeholder = "Stakeholder";
        public const string CustomerTypeGeneral = "General";
        public const string ItemTypeRent = "Rent";
        public const string PaymentTypeHousingBenefit = "Housing Benefit";        
        public const string StatusTypeEstimated = "Estimated";
        public const string StatusTypePaid = "Paid";

        public const string ImgTitleProspectiveTenant = "pt";
        public const string ImgTitleTenant = "t";
        public const string ImgTitlePreviousTenant = "ft";
        public const string ImgTitleStakeholder = "sh";
        public const string ImgTitleGeneral = "g";

        public const string HBRefMismatchReason = "HB Ref mismatch";        
        public const string StartDateIncorrectReason = "Start date incorrect";        
        public const string EndDateIncorrectReason = "End date incorrect";        
        public const string AmountIncorrectReason = "Amount incorrect";
        public const string PaymentExistsReason = "Payment exists in RSL";

        public const int MaxListingRecords = 300;

        #endregion

        #region "Default Columns"
        public const string HBUploadExceptionIdCol = "exceptionId";
        public const string HBUploadFileCol = "UploadDate";
        #endregion

        #region "HBPaymentRecords"
        public const int HBPaymentRecord = 6;
        public const int NextHBPayment = 13;
        #endregion

        #region "HBException Reason"
        public const string HBRefMismatch = "HB Ref mismatch";
        public const string StartDateIncorrect = "Start date incorrect";
        public const string EndDateIncorrect = "End date incorrect";
        public const string AmountIncorrect = "Amount incorrect";
        public const string PaymentExistsInRSLExistsInRSL = "Payment exists in RSL";
        #endregion
    }
}
