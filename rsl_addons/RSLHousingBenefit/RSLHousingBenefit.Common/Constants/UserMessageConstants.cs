﻿
namespace RSLHousingBenefit.Common.Constants
{
    public class UserMessageConstants
    {        
        public const string ErrorSendingEmailMsg = "An error occurred while sending email, please try again.";
        public const string ExceptionReasonNotfound = "Exception reason not found.";
    }
}
