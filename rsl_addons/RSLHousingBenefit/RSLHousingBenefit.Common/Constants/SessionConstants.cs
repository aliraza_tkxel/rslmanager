﻿namespace RSLHousingBenefit.Common.Constants
{
    public class SessionConstants
    {
        public const string SessionUserId = "SESSION_USER_ID";
        public const string UserFullName = "UserFullName";
        public const string EmployeeId = "UserEmployeeId";
    }
}
