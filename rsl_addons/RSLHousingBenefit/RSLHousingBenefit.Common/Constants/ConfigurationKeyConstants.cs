﻿namespace RSLHousingBenefit.Common.Constants
{
    public class ConfigurationKeyConstants
    {           
        public const string PropertyDocumentUploadPath = "PropertyDocumentUploadPath";
        public const string SchemeDocumentUploadPath = "SchemeDocumentUploadPath";
        public const string BlockDocumentUploadPath = "BlockDocumentUploadPath";
        public const string LoginPagePath = "LoginPath";
        public const string HBDocUploadPath = "HBDocUploadPath";
    }
}
