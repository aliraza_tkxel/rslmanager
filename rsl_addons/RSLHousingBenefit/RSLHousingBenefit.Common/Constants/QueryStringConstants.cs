﻿namespace RSLHousingBenefit.Common.Constants
{
    public class QueryStringConstants
    {

        #region "Common"
        public const string Page = "page";
        public const string SortBy = "sort";
        public const string SortOrder = "sortdir";
        #endregion

        #region "Exception Report"
        public const string HbUploadId = "hbUploadId";
        #endregion

    }
}
