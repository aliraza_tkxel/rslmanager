﻿using RSLHousingBenefit.Common.Constants;
using System;
using System.Web;

namespace RSLHousingBenefit.Common.Helper
{
    public class SessionManager
    {
        #region Set / Get / Remove Session User Id 

        public static void SetSessionUserId(int userId)
        {
            HttpContext.Current.Session[SessionConstants.SessionUserId] = userId;
        }

        public static int GetSessionUserId()
        {
            if (HttpContext.Current.Session[SessionConstants.SessionUserId] != null)
            {
                return Convert.ToInt32(HttpContext.Current.Session[SessionConstants.SessionUserId]);
            }

            return 0;
        }

        public static void RemoveSessionUserId()
        {
            if (HttpContext.Current.Session[SessionConstants.SessionUserId] != null)
            {
                HttpContext.Current.Session[SessionConstants.SessionUserId] = null;
            }
        }

        #endregion
    }
}
