﻿using RSLHousingBenefit.Common.Constants;
using System.Configuration;

namespace RSLHousingBenefit.Common.Helper
{
    public class ConfigHelper
    {
        #region "Get Login Page Path"
        public static string GetLoginPagePath()
        {
            string returnVal = string.Empty;
            var configKey = ConfigurationManager.AppSettings[ConfigurationKeyConstants.LoginPagePath];
            if (configKey != null)
                return configKey.ToString();

            return returnVal;
        }
        #endregion

        #region "Get Property/Scheme/Block Doc Upload Path"
        public static string GetSchemeDocUploadPath()
        {
            string returnVal = string.Empty;
            var configKey = ConfigurationManager.AppSettings[ConfigurationKeyConstants.SchemeDocumentUploadPath];
            if (configKey != null)
                return configKey + "\\Schemes";

            return returnVal;
        }

        public static string GetPropertyDocUploadPath()
        {
            string returnVal = string.Empty;
            var configKey = ConfigurationManager.AppSettings[ConfigurationKeyConstants.PropertyDocumentUploadPath];
            if (configKey != null)
                return configKey + "";

            return returnVal;
        }

        public static string GetBlockDocUploadPath()
        {
            string returnVal = string.Empty;
            var configKey = ConfigurationManager.AppSettings[ConfigurationKeyConstants.BlockDocumentUploadPath];
            if (configKey != null)
                return configKey + "\\Blocks";

            return returnVal;
        }

        public static string GetHBDocUploadPath()
        {
            string returnVal = string.Empty;
            var configKey = ConfigurationManager.AppSettings[ConfigurationKeyConstants.HBDocUploadPath];
            if (configKey != null)
                return configKey ;
            return returnVal;
        }

        #endregion


    }
}
