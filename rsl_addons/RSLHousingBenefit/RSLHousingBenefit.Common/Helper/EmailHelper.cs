﻿
using RSLHousingBenefit.Common.Constants;
using System;
using System.IO;
using System.Net.Mail;

namespace RSLHousingBenefit.Common.Helper
{
    public class EmailHelper
    {
        public static void sendHtmlFormattedEmail(String recepientName, String recepientEmail, String subject, String body)
        {
            try
            {

                MailMessage mailMessage = new MailMessage();
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = true;
                mailMessage.To.Add(new MailAddress(recepientEmail, recepientName));

                //The SmtpClient gets configuration from Web.Config
                SmtpClient smtp = new SmtpClient();
                smtp.Send(mailMessage);

            }
            catch (IOException)
            {
                throw new ArgumentException(UserMessageConstants.ErrorSendingEmailMsg);
            }
            catch (Exception ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        public static void sendEmail(ref MailMessage mailMessage)
        {
            //The SmtpClient gets configuration from Web.Config
            SmtpClient smtp = new SmtpClient();

            smtp.Send(mailMessage);
        }
    }
}
