#@del /F /Q *.user
@PUSHD RSLHousingBenefit.Common
  @del /F /Q *.user
  @del /F /Q *.log
  @rmdir /S /Q bin
  @rmdir /S /Q obj
@POPD

@PUSHD .\RSLHousingBenefit.Interfaces
  @rmdir /S /Q bin
  @rmdir /S /Q obj
@POPD

@PUSHD RSLHousingBenefit.Models
  @rmdir /S /Q bin
  @rmdir /S /Q obj
@POPD

@PUSHD RSLHousingBenefit.Repository
  @rmdir /S /Q bin
  @rmdir /S /Q obj
@POPD

@PUSHD RSLHousingBenefit.Services
  @rmdir /S /Q bin
  @rmdir /S /Q obj
@POPD

@PUSHD RSLHousingBenefit.Web
  @rmdir /S /Q bin
  @rmdir /S /Q obj
@POPD

@PUSHD PushSharp\PushSharp.Apple
  @rmdir /S /Q bin
  @rmdir /S /Q obj
@POPD

@PAUSE