﻿using RSLHousingBenefit.Repository.DomainModel;
using RSLHousingBenefit.Repository.Repositories;
using System;

namespace RSLHousingBenefit.Repository.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {

        #region "Properties"

        private EmployeeRepository _employeeRepository;
        public EmployeeRepository EmployeeRepo
        {
            get { return (_employeeRepository == null) ? _employeeRepository = new EmployeeRepository(this._context) : _employeeRepository; }
        }

        private CustomerRepo _customerRepo;
        public CustomerRepo customerRepo
        {
            get { return (_customerRepo == null) ? _customerRepo = new CustomerRepo(this._context) : _customerRepo; }
        }

        private TenancyRepo _tenancyRepo;
        public TenancyRepo tenancyRepo
        {
            get { return (_tenancyRepo == null) ? _tenancyRepo = new TenancyRepo(this._context) : _tenancyRepo; }
        }


        private PageRepository _pageRepository;
        public PageRepository PageRepo
        {
            get { return (_pageRepository == null) ? _pageRepository = new PageRepository(this._context) : _pageRepository; }
        }

        private PageAccessRepository _pageAccessRepository;
        public PageAccessRepository PageAccessRepo
        {
            get { return (_pageAccessRepository == null) ? _pageAccessRepository = new PageAccessRepository(this._context) : _pageAccessRepository; }
        }

        private MenuRepository _menuRepository;
        public MenuRepository MenuRepo
        {
            get { return (_menuRepository == null) ? _menuRepository = new MenuRepository(this._context) : _menuRepository; }
        }

        private MenuAccessRepository _menuAccessRepository;
        public MenuAccessRepository MenuAccessRepo
        {
            get { return (_menuAccessRepository == null) ? _menuAccessRepository = new MenuAccessRepository(this._context) : _menuAccessRepository; }
        }

        private ModuleRepository _moduleRepository;
        public ModuleRepository ModuleRepo
        {
            get { return (_moduleRepository == null) ? _moduleRepository = new ModuleRepository(this._context) : _moduleRepository; }
        }

        private ModuleAccessRepository _moduleAccessRepository;
        public ModuleAccessRepository ModuleAccessRepo
        {
            get { return (_moduleAccessRepository == null) ? _moduleAccessRepository = new ModuleAccessRepository(this._context) : _moduleAccessRepository; }
        }

        private HBActualScheduleRepository _hbActualScheduleRepository;
        public HBActualScheduleRepository HBActualScheduleRepo
        {
            get { return (_hbActualScheduleRepository == null) ? _hbActualScheduleRepository = new HBActualScheduleRepository(this._context) : _hbActualScheduleRepository; }
        }

        private HBInformationRepository _hbInformationRepository;
        public HBInformationRepository HBInformationRepo
        {
            get { return (_hbInformationRepository == null) ? _hbInformationRepository = new HBInformationRepository(this._context) : _hbInformationRepository; }
        }

        private HBUploadExceptionRepository _hbUploadExceptionRepository;
        public HBUploadExceptionRepository HBUploadExceptionRepo
        {
            get { return (_hbUploadExceptionRepository == null) ? _hbUploadExceptionRepository = new HBUploadExceptionRepository(this._context) : _hbUploadExceptionRepository; }
        }

        private HBUploadFileDataRepository _hbUploadFileDataRepository;
        public HBUploadFileDataRepository HBUploadFileDataRepo
        {
            get { return (_hbUploadFileDataRepository == null) ? _hbUploadFileDataRepository = new HBUploadFileDataRepository(this._context) : _hbUploadFileDataRepository; }
        }

        private HBUploadRepository _hbUploadRepository;
        public HBUploadRepository HBUploadRepo
        {
            get { return (_hbUploadRepository == null) ? _hbUploadRepository = new HBUploadRepository(this._context) : _hbUploadRepository; }
        }

        private HBExceptionReasonRepository _hbExceptionReasonRepository;
        public HBExceptionReasonRepository HBExceptionReasonRepo
        {
            get { return (_hbExceptionReasonRepository == null) ? _hbExceptionReasonRepository = new HBExceptionReasonRepository(this._context) : _hbExceptionReasonRepository; }
        }
		
		private LocalAuthorityRepository _localAuthorityRepository;
        public LocalAuthorityRepository localAuthorityRepository
        {
            get { return (_localAuthorityRepository == null) ? _localAuthorityRepository = new LocalAuthorityRepository(this._context) : _localAuthorityRepository; }
        }

        private RentJournalRepository _rentJournalRepository;
        public RentJournalRepository RentJournalRepo
        {
            get { return (_rentJournalRepository == null) ? _rentJournalRepository = new RentJournalRepository(this._context) : _rentJournalRepository; }
        }

        #endregion

        #region "Constructor"

        RSLBHALiveEntities _context;

        public UnitOfWork(RSLBHALiveEntities dbContext)
        {
            this._context = dbContext;
        }

        #endregion

        #region "Functions"

        public void Save()
        {
            _context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        #endregion

    }
}
