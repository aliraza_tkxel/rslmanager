﻿using RSLHousingBenefit.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSLHousingBenefit.Repository.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {

        EmployeeRepository EmployeeRepo { get; }
        PageRepository PageRepo { get; }
        PageAccessRepository PageAccessRepo { get; }
        MenuRepository MenuRepo { get; }
        MenuAccessRepository MenuAccessRepo { get; }
        ModuleRepository ModuleRepo { get; }
        ModuleAccessRepository ModuleAccessRepo { get; }
        LocalAuthorityRepository localAuthorityRepository { get; }
        HBActualScheduleRepository HBActualScheduleRepo { get; }
        HBInformationRepository HBInformationRepo { get; }
        HBUploadExceptionRepository HBUploadExceptionRepo { get; }
        HBUploadFileDataRepository HBUploadFileDataRepo { get; }
        HBUploadRepository HBUploadRepo { get; }
        HBExceptionReasonRepository HBExceptionReasonRepo { get; }
        RentJournalRepository RentJournalRepo { get; }
        CustomerRepo customerRepo { get; }
        TenancyRepo tenancyRepo { get; }

        void Save();

    }
}
