//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace RSLHousingBenefit.Repository.DomainModel
{
    using System;
    
    public partial class HB_SearchCustomer_Result
    {
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string Tenant { get; set; }
        public string FullAddress { get; set; }
        public int TenancyCount { get; set; }
        public int CustomerType { get; set; }
        public string CustomerTypeDesc { get; set; }
        public int TenancyId { get; set; }
    }
}
