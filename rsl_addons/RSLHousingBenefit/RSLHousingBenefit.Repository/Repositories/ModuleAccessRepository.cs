﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSLHousingBenefit.Repository.DomainModel;

namespace RSLHousingBenefit.Repository.Repositories
{
    public class ModuleAccessRepository : BaseRepository<AC_MODULES_ACCESS>
    {
        public ModuleAccessRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {

        }
    }
}
