﻿using RSLHousingBenefit.Models.ViewModels;
using RSLHousingBenefit.Repository.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSLHousingBenefit.Models.Models;

namespace RSLHousingBenefit.Repository.Repositories
{
    public class HBInformationRepository : BaseRepository<F_HBINFORMATION>
    {
        public HBInformationRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {

        }

        #region Get Customer HB Listing
        public CustomerHBListingViewModel GetCustomerHBListing(int customerId, int tenancyId)
        {
            CustomerHBListingViewModel response = new CustomerHBListingViewModel();

            var hbListingQuery = (from hb in DbContext.HB_GetCustomerHBList(customerId, tenancyId)
                                  select new HBInfoViewModel
                                  {
                                      hbId = hb.HbId,
                                      hbref = hb.HbRef,
                                      tenancyId = hb.TenancyId,
                                      customerId = hb.CustomerId,
                                      startDate = hb.StartDate,
                                      endDate = hb.EndDate,
                                      localAuthority = hb.LocalAuthority
                                  });

            response.hbListing = hbListingQuery.ToList();

            return response;
        }
        #endregion

        #region Get HB Tenancy listing by HBRef
        public HBTenancyListingViewModel GetHbTenancyListingByHBref(string hbRef)
        {
            HBTenancyListingViewModel response = new HBTenancyListingViewModel();

            var tenancyList = (from hb in DbContext.HB_GetTenancyListByHBRef(hbRef)
                               select new HBTenancyViewModel
                               {
                                   HbId = hb.HbId,
                                   CustomerId = hb.CustomerId,
                                   TenancyId = hb.TenancyId,
                                   CustomerName = hb.CustomerName,
                                   FullAddress = hb.FullAddress
                               });

            response.hbTenancyListing = tenancyList.ToList();
            return response;
        }
        #endregion

        public bool UpdateHbReferance(int HBId, int CustomerId, Int64 TenancyId, string HbRef)
        {
            try
            {
                bool allowSave = false;
                var HBinformation = DbContext.F_HBINFORMATION.Where(info => info.HBREF == HbRef).FirstOrDefault();
                if (HBinformation == null)
                {
                    allowSave = true;
                }
                else if (HBinformation.CUSTOMERID != CustomerId)
                {
                    allowSave = false;
                }
                else
                {
                    allowSave = true;
                }
                if (allowSave)
                {
                    var hbInformation = DbContext.F_HBINFORMATION.Where(i => i.HBID == HBId && i.CUSTOMERID == CustomerId && i.TENANCYID == TenancyId).FirstOrDefault();
                    hbInformation.HBREF = HbRef;
                    DbContext.SaveChanges();
                }
                return allowSave;
                //if (!DbContext.F_HBINFORMATION.Where(info => info.HBREF == HbRef).Any())
                //{
                //    //initialpayment, initialstartdate, initialenddate these are disable fields so they remain same that is why no need to update again.
                //    var hbInformation = DbContext.F_HBINFORMATION.Where(i => i.HBID == HBId && i.CUSTOMERID == CustomerId && i.TENANCYID == TenancyId).FirstOrDefault();
                //    hbInformation.HBREF = HbRef;
                //    DbContext.SaveChanges();
                //    return true;
                //}
                //return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }

        public int SubmitNewHBSchedule(int customerId, int tenancyId, string HBRef, double initialPayment, DateTime startDate, DateTime endDate)
        {
            try
            {
                if (DbContext.F_HBINFORMATION.Where(info => info.HBREF == HBRef&& info.CUSTOMERID!= customerId).Any())
                {
                    return -1;
                }
                var HBInfo = DbContext.F_HBINFORMATION.Add(new F_HBINFORMATION
                {
                    CUSTOMERID = customerId,
                    HBREF = HBRef,
                    TENANCYID = tenancyId,
                    INITIALPAYMENT = initialPayment,
                    INITIALSTARTDATE = startDate,
                    INITIALENDDATE = endDate
                });
                DbContext.SaveChanges();
                return HBInfo.HBID;
            }
            catch (Exception ex)
            {
                return -1;
            }

        }
    }
}
