﻿using RSLHousingBenefit.Models.Models;
using RSLHousingBenefit.Repository.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSLHousingBenefit.Repository.Repositories
{
    public class TenancyRepo :BaseRepository<C_TENANCY>
    {
        public TenancyRepo(RSLBHALiveEntities DbContext) : base(DbContext) { }

        public string GetTenancyAddres(int customerId, int tenancyId)
        {
            var tenancyAddress =(from ten in DbContext.C_TENANCY
                          join prop in DbContext.P__PROPERTY on ten.PROPERTYID equals prop.PROPERTYID
                          join cust in DbContext.C__CUSTOMER on customerId equals cust.CUSTOMERID
                          join addrs in DbContext.C_ADDRESS on cust.CUSTOMERID equals addrs.CUSTOMERID
                          where ten.TENANCYID==tenancyId
                          select new TenancyAddressModel {
                                            Address=prop.ADDRESS1==null ? addrs.ADDRESS1:prop.ADDRESS1,
                                            HouseNumber=prop.HOUSENUMBER==null? addrs.HOUSENUMBER: prop.HOUSENUMBER
                                            }).FirstOrDefault();
            return tenancyAddress.HouseNumber + " " + tenancyAddress.Address;
        }
    }
}
