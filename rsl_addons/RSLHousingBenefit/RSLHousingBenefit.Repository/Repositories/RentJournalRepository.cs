﻿using RSLHousingBenefit.Common.Constants;
using RSLHousingBenefit.Models;
using RSLHousingBenefit.Repository.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSLHousingBenefit.Repository.Repositories
{
    public class RentJournalRepository : BaseRepository<F_RENTJOURNAL>
    {
        public RentJournalRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {

        }

        public int InsertRentJournal(HBScheduleRentInformation rentInfo)
        {
            var estimatedStatus = DbContext.F_TRANSACTIONSTATUS
                                           .Where(s => s.DESCRIPTION == ApplicationConstants.StatusTypePaid)
                                           .First();

            F_RENTJOURNAL rentJournal = new F_RENTJOURNAL() {
               TENANCYID = (int) rentInfo.TenancyId,
               TRANSACTIONDATE = rentInfo.TransactionDate,
               ITEMTYPE = rentInfo.ItemType,
               PAYMENTTYPE = rentInfo.PaymentType,
               PAYMENTSTARTDATE = rentInfo.PaymentStartDate,
               PAYMENTENDDATE = rentInfo.PaymentEndDate,
               AMOUNT = -((decimal?)rentInfo.Amount),
               ISDEBIT = 0,
               STATUSID = estimatedStatus.TRANSACTIONSTATUSID
            };

            DbContext.F_RENTJOURNAL.Add(rentJournal);
            DbContext.SaveChanges();

            return rentJournal.JOURNALID;


        }
		
		public bool DeleterentJournal(int CustomerID, Int64 TenancyID)
        {
            try
            {
                var journal = (from jrnl in DbContext.F_RENTJOURNAL
                              join act in DbContext.F_HBACTUALSCHEDULE on jrnl.JOURNALID equals act.JOURNALID
                              join info in DbContext.F_HBINFORMATION on act.HBID equals info.HBID
                              where info.CUSTOMERID == CustomerID && info.TENANCYID == TenancyID && jrnl.STATUSID == 1
                              && jrnl.PAYMENTTYPE == 1 && jrnl.ITEMTYPE == 1
                              select jrnl).ToList();
                DbContext.F_RENTJOURNAL.RemoveRange(journal);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

    }
}
