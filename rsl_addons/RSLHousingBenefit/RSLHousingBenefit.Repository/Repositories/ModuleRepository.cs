﻿using RSLHousingBenefit.Common.Constants;
using RSLHousingBenefit.Models.ViewModels;
using RSLHousingBenefit.Repository.DomainModel;
using System.Collections.Generic;
using System.Linq;

namespace RSLHousingBenefit.Repository.Repositories
{
    public class ModuleRepository : BaseRepository<AC_MODULES>
    {

        public ModuleRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {

        }

        #region "Get Module Id by Module name"
        public int GetModuleId(string moduleName)
        {
            int moduleId = 0;
            var module = (from mdl in DbContext.AC_MODULES                                          
                               where mdl.DESCRIPTION == moduleName
                               select mdl).FirstOrDefault();
            if (module != null)
            {
                moduleId = module.MODULEID;
            }            
            return moduleId;
        }
        #endregion

        #region "Get Modules List"
        public List<ModuleViewModel> GetModules(int userId)
        {            
            var modules = (from sideMenus in DbContext.P_GetRSLModulesList(userId, ApplicationConstants.OrderDes)
                        select new ModuleViewModel
                        {
                            moduleId = sideMenus.MODULEID,
                            moduleName = sideMenus.DESCRIPTION,
                            url = sideMenus.THEPATH
                        }
                );
       
            return modules.ToList();
        }
        #endregion

    }
}
