﻿using RSLHousingBenefit.Common.Constants;
using RSLHousingBenefit.Models;
using RSLHousingBenefit.Repository.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSLHousingBenefit.Repository.Repositories
{
    public class HBUploadFileDataRepository : BaseRepository<F_HBUploadFileData>
    {
        public HBUploadFileDataRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {

        }

        #region Update HB row
        public void UpdateHBrow(int hbUploadFileDataId, int hbrow)
        {
            var fileData = DbContext.F_HBUploadFileData.Where(x=> x.HBUploadFileDataId == hbUploadFileDataId).First();
            fileData.HBRow = hbrow;
        }
        #endregion

        #region Get File Entries To Be Processed
        public List<HBScheduleRentInformation> GetFileEntriesToBeProcessed(int hbUploadId)
        {

            var itemType = DbContext.F_ITEMTYPE.Where(p => p.DESCRIPTION == ApplicationConstants.ItemTypeRent).First();
            var pType = DbContext.F_PAYMENTTYPE.Where(pt => pt.DESCRIPTION == ApplicationConstants.PaymentTypeHousingBenefit).First();

            var fileData = (from fh in DbContext.F_HBUploadFileData
                            join fhb in DbContext.F_HBACTUALSCHEDULE on fh.HBRow equals fhb.HBROW
                            join fhi in DbContext.F_HBINFORMATION on fhb.HBID equals fhi.HBID
                            join fu in DbContext.F_HBUpload on fh.HbUploadId equals fu.HbUploadId
                            where fh.HbUploadId == hbUploadId && fh.IsExcluded != true
                            select new HBScheduleRentInformation
                                {
                                    TenancyId = fhi.TENANCYID,
                                    Amount = fhb.HB,
                                    PaymentStartDate = fhb.STARTDATE,
                                    PaymentEndDate = fhb.ENDDATE,
                                    TransactionDate = fu.TransactionDate,
                                    ItemType = itemType.ITEMTYPEID ,
                                    HBRow  = fh.HBRow  ,
                                    PaymentType = pType.PAYMENTTYPEID
                            }
                            ).ToList();

            return fileData;
        }
        #endregion
		
		        public bool ExcludeHbUpload(int HbUploadFileDataId,bool isExclude)
        {
            try
            {
                var HbUpload = DbContext.F_HBUploadFileData.Where(u => u.HBUploadFileDataId == HbUploadFileDataId).FirstOrDefault();
                HbUpload.IsExcluded = isExclude;
                DbContext.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }

    }
}
