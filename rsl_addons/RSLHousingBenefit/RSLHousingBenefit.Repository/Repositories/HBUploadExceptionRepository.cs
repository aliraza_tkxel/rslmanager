﻿using RSLHousingBenefit.Common.Constants;
using RSLHousingBenefit.Repository.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSLHousingBenefit.Models.ViewModels;

namespace RSLHousingBenefit.Repository.Repositories
{
    public class HBUploadExceptionRepository : BaseRepository<F_HBUploadExceptions>
    {

        public HBUploadExceptionRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {

        }

        #region Add HB exception
        public void AddHBException(int hbDataId, string reason)
        {
            var reasonResult = DbContext.F_HBExceptionReason.Where(r => r.Reason.Equals(reason)).FirstOrDefault();
            if (reasonResult != null)
            {
                var exception = new F_HBUploadExceptions
                {
                    HBUploadFileDataId = hbDataId,
                    ReasonId = reasonResult.ReasonId,
                    IsResolved = false,
                    Display = true
                };
                DbContext.F_HBUploadExceptions.Add(exception);
            }
            else
            {
                throw new Exception(UserMessageConstants.ExceptionReasonNotfound);
            }
        }
        #endregion

        #region Reset previous exceptions of HB Upload
        public void ResetPreviousExceptions(int hbUploadId)
        {
            var previousExceptions = (from uex in DbContext.F_HBUploadExceptions
                                      join up in DbContext.F_HBUploadFileData on uex.HBUploadFileDataId equals up.HBUploadFileDataId
                                      where up.HbUploadId == hbUploadId
                                      select uex);            
            foreach (var exception in previousExceptions)
            {
                exception.Display = false;
            }
        }
        #endregion

        #region Get HB Exceptions By HB UploadId
        public ExceptionsListViewModel GetHBExceptionsByHBUploadId(int hbUploadId)
        {
            ExceptionsListViewModel response = new ExceptionsListViewModel();

            var exceptionsQuery = (from uex in DbContext.F_HBUploadExceptions
                                      join up in DbContext.F_HBUploadFileData on uex.HBUploadFileDataId equals up.HBUploadFileDataId
                                      join r in DbContext.F_HBExceptionReason on uex.ReasonId equals r.ReasonId
                                      join hs in DbContext.F_HBACTUALSCHEDULE on up.HBRow equals hs.HBROW into hsp
                                      from hs in hsp.DefaultIfEmpty()
                                      where up.HbUploadId == hbUploadId && uex.Display == true
                                      select new ExceptionViewModel {
                                          exceptionId = uex.HBUploadExceptionId,
                                          reason = r.Reason,
                                          reasonId = r.ReasonId,
                                          isResolved = uex.IsResolved,
                                          isExcluded = up.IsExcluded,
                                          hbUploadDataId = up.HBUploadFileDataId,
                                          hbRef = up.HBRef,
                                          tenantName = up.TenantName,
                                          address = up.Address,
                                          amount = up.Amount,
                                          startDate = up.StartDate,
                                          endDate = up.EndDate,
                                          hbId = hs != null ? (int?)hs.HBID : 0 
                                      });

            var finalData = exceptionsQuery.ToList();
            
            foreach (ExceptionViewModel item in finalData)
            {
               
                if (item.startDate.HasValue)
                {
                    DateTime startDate = (DateTime)item.startDate;
                    item.startDateString = startDate.ToString("dd/MM/yyyy");
                }

                if (item.endDate.HasValue)
                {
                    DateTime endDate = (DateTime)item.endDate;
                    item.endDateString = endDate.ToString("dd/MM/yyyy");
                }

            }
            response.hbExceptions = finalData;
            response.hbUploadId = hbUploadId;

            return response;
        }
        #endregion

        #region Search Customer
        public CustomerListViewModel SearchCustomer(string firstName, string lastName, string address, string postcode)
        {
            CustomerListViewModel response = new CustomerListViewModel();

            var customerQuery = (from sc in DbContext.HB_SearchCustomer(firstName, lastName, address, postcode)                                   
                                 select new CustomerViewModel
                                 {
                                    customerId = sc.CustomerId,
                                    customerName = sc.CustomerName,
                                    address = sc.FullAddress,
                                    customerType = sc.CustomerType,
                                    tenancyCount = sc.TenancyCount,
                                    customerTypeDesc = sc.CustomerTypeDesc,
                                    tenant = sc.Tenant,
                                    tenancyId= sc.TenancyId
                                 });

            response.customers = customerQuery.ToList();

            return response;
        }
        #endregion    
		
		public bool UpdataExceptionReport(int exceptionReportId, bool isResolved)
        {
            try
            {
                var exception = DbContext.F_HBUploadExceptions.Where(e => e.HBUploadExceptionId == exceptionReportId).First();
                exception.IsResolved = isResolved;
                DbContext.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }    

    }
}
