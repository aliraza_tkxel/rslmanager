﻿using RSLHousingBenefit.Common.Constants;
using RSLHousingBenefit.Models;
using RSLHousingBenefit.Models.Models;
using RSLHousingBenefit.Repository.DomainModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSLHousingBenefit.Repository.Repositories
{
    public class HBActualScheduleRepository : BaseRepository<F_HBACTUALSCHEDULE>
    {
        public HBActualScheduleRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {

        }
        public List<HBPaymentModel> GetTopHBPayments(int HBId)
        {
            var file = from hBACTUALSCHEDUL in DbContext.F_HBACTUALSCHEDULE
                       where hBACTUALSCHEDUL.VALIDATED == 1 && hBACTUALSCHEDUL.HBID == HBId
                       orderby hBACTUALSCHEDUL.STARTDATE
                       select new HBPaymentModel
                       {
                           EndDate = hBACTUALSCHEDUL.ENDDATE.ToString(),
                           Hb = hBACTUALSCHEDUL.HB,
                           HbId = hBACTUALSCHEDUL.HBID,
                           HbRow = hBACTUALSCHEDUL.HBROW,
                           JournalId = hBACTUALSCHEDUL.JOURNALID,
                           Sent = hBACTUALSCHEDUL.SENT,
                           StartDate = hBACTUALSCHEDUL.STARTDATE.ToString(),
                           Validated = hBACTUALSCHEDUL.VALIDATED,
                           DefaultCycle = 0
                       };
            var finalData = file.Take(ApplicationConstants.HBPaymentRecord).ToList();
            foreach (var item in finalData)
            {
                if (!string.IsNullOrEmpty(item.EndDate))
                    item.EndDate = Convert.ToDateTime(item.EndDate).ToShortDateString();
                if (!string.IsNullOrEmpty(item.StartDate))
                    item.StartDate = Convert.ToDateTime(item.StartDate).ToShortDateString();
                item.DefaultCycle = (Convert.ToDateTime(item.EndDate) - Convert.ToDateTime(item.StartDate)).TotalDays;
                item.Hb = Math.Round(item.Hb.Value, 2);
            }
            return finalData;
        }
        public HBNextEstimatedPaymentModel GetNextEstimatedHBPayments(int HBId)
        {
            var file = from hBACTUALSCHEDULE in DbContext.F_HBACTUALSCHEDULE
                       where hBACTUALSCHEDULE.VALIDATED == null && hBACTUALSCHEDULE.HBID == HBId
                       orderby hBACTUALSCHEDULE.STARTDATE
                       select new HBNextEstimatedPaymentModel
                       {
                           EndDate = hBACTUALSCHEDULE.ENDDATE.ToString(),
                           Hb = hBACTUALSCHEDULE.HB,
                           HbId = hBACTUALSCHEDULE.HBID,
                           HbRow = hBACTUALSCHEDULE.HBROW,
                           JournalId = hBACTUALSCHEDULE.JOURNALID,
                           Sent = hBACTUALSCHEDULE.SENT,
                           StartDate = hBACTUALSCHEDULE.STARTDATE.ToString(),
                           Validated = hBACTUALSCHEDULE.VALIDATED
                       };
            HBNextEstimatedPaymentModel fileData = file.FirstOrDefault();
            if (!string.IsNullOrEmpty(fileData.StartDate))
                fileData.StartDate = Convert.ToDateTime(fileData.StartDate).ToShortDateString();
            if (!string.IsNullOrEmpty(fileData.EndDate))
                fileData.EndDate = Convert.ToDateTime(fileData.EndDate).ToShortDateString();
            fileData.Hb = Math.Round(fileData.Hb.Value, 2);
            return fileData;
        }
        public bool UpdateStartDate(int HBId, DateTime newStartDate)
        {
            try
            {
                var actualSchedule = DbContext.F_HBACTUALSCHEDULE.Where(a => a.HBID == HBId && a.VALIDATED == null)
                                                                 .OrderBy(a => a.STARTDATE).First();
                actualSchedule.STARTDATE = newStartDate;
                DbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool UpdateEndDate(int HBId, DateTime newEndDate)
        {
            try
            {
                var actualSchedule = DbContext.F_HBACTUALSCHEDULE.Where(a => a.HBID == HBId && a.VALIDATED == null)
                                                                 .OrderBy(a => a.STARTDATE).First();
                actualSchedule.ENDDATE = newEndDate;
                DbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool UpdateHB(int HBId, double newHb)
        {
            try
            {
                var actualSchedule = DbContext.F_HBACTUALSCHEDULE.Where(a => a.HBID == HBId && a.VALIDATED == null)
                                                 .OrderBy(a => a.STARTDATE).First();
                actualSchedule.HB = newHb;
                DbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            { return false; }
        }
        public bool UpdateNextPayments(int HBId, DateTime startDate, DateTime endDate,double newHb)
        {
            try
            {
                var nextPayments = DbContext.F_HBACTUALSCHEDULE.Where(h => h.HBID == HBId && h.VALIDATED == null)
                                   .OrderBy(s => s.STARTDATE).ToList();
                var Initial_Cycle = Convert.ToInt32((endDate + new TimeSpan(7, 0, 0) - startDate + new TimeSpan(3, 0, 0)).TotalDays) + 1;
                var Initial_Payment = Math.Abs(newHb);
                var dr = -1 * Initial_Payment / Initial_Cycle;
                var HB_Start = startDate + new TimeSpan(3, 0, 0);
                var HB_End = endDate + new TimeSpan(7, 0, 0);
                var initial = 1;
                var Payment_Cycle = Convert.ToInt32(ApplicationConstants.DefaultCycle);
                var oneDay = 24 * 60 * 60 * 1000;
                var DateCounter = HB_End.AddMilliseconds(oneDay);
                var DateCounterPlusCycle = HB_End.AddDays(Payment_Cycle);
                foreach (var payment in nextPayments)
                {
                    if (initial == 1)
                    {
                        var AM = Math.Round(dr * Initial_Cycle, 2);
                        payment.HB = AM;
                        payment.STARTDATE = HB_Start.Date;
                        payment.ENDDATE = HB_End.Date;
                        payment.HBID = HBId;
                        initial = 2;
                    }
                    else
                    {
                        var AM = Math.Round(dr * Payment_Cycle, 2);
                        payment.HB = AM;
                        payment.STARTDATE = DateCounter.Date;
                        payment.ENDDATE = DateCounterPlusCycle.Date;
                        payment.HBID = HBId;
                        DateCounter = DateCounter.AddDays(Payment_Cycle);
                        DateCounterPlusCycle = (DateCounterPlusCycle.AddDays(Payment_Cycle));
                    }
                }
                DbContext.SaveChanges();
                DbContext.F_AUTO_HBSCHEDULE_ENTRY();

                //double perDayPayment = newHb/((endDate.Date - startDate.Date).Days);
                //var nextPayments = DbContext.F_HBACTUALSCHEDULE.Where(h => h.HBID == HBId && h.VALIDATED == null)
                //                          .OrderBy(s => s.STARTDATE).ToList();
                //var top = nextPayments.FirstOrDefault();
                //top.HB = newHb;
                //top.STARTDATE = startDate;
                //top.ENDDATE = endDate;
                //nextPayments.RemoveAt(0);
                //DateTime lastEndDate = endDate.AddDays(1);
                //foreach (var nextPayment in nextPayments)
                //{
                //    nextPayment.STARTDATE = lastEndDate;
                //    nextPayment.ENDDATE = lastEndDate.AddDays(Convert.ToInt32(ApplicationConstants.DefaultCycle)-1);
                //    nextPayment.HB = perDayPayment*(nextPayment.ENDDATE-nextPayment.STARTDATE).Value.TotalDays;
                //    lastEndDate = nextPayment.ENDDATE.Value.AddDays(1);
                //}
                //DbContext.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public List<HBPaymentModel> NextHBPayments(int HBId)
        {
            var file = from hBACTUALSCHEDUL in DbContext.F_HBACTUALSCHEDULE
                       where hBACTUALSCHEDUL.VALIDATED == null && hBACTUALSCHEDUL.HBID == HBId
                       orderby hBACTUALSCHEDUL.STARTDATE
                       select new HBPaymentModel
                       {
                           EndDate = hBACTUALSCHEDUL.ENDDATE.ToString(),
                           Hb = hBACTUALSCHEDUL.HB,
                           HbId = hBACTUALSCHEDUL.HBID,
                           HbRow = hBACTUALSCHEDUL.HBROW,
                           JournalId = hBACTUALSCHEDUL.JOURNALID,
                           Sent = hBACTUALSCHEDUL.SENT,
                           StartDate = hBACTUALSCHEDUL.STARTDATE.ToString(),
                           Validated = hBACTUALSCHEDUL.VALIDATED,
                           DefaultCycle = 0
                       };
            var finalData = file.Take(ApplicationConstants.NextHBPayment).ToList();
            foreach (var item in finalData)
            {
                if (!string.IsNullOrEmpty(item.EndDate))
                    item.EndDate = Convert.ToDateTime(item.EndDate).ToShortDateString();
                if (!string.IsNullOrEmpty(item.StartDate))
                    item.StartDate = Convert.ToDateTime(item.StartDate).ToShortDateString();
                item.DefaultCycle = (Convert.ToDateTime(item.EndDate) - Convert.ToDateTime(item.StartDate)).TotalDays;
                item.Hb = Math.Round(item.Hb.Value, 2);
            }
            return finalData;
        }
        public bool AddNextPayments(int HBId, DateTime startDate, DateTime endDate, double newHb)
        {
            try
            {
                var HB_InitialStart = startDate;
                var HB_InitialEnd = endDate;
                var nextHbPayments = new List<HBPaymentModel>();
                var HB_StartYear = HB_InitialStart.Year;

                var financialStartDate = Convert.ToDateTime("01/04/" + startDate.Year);
                financialStartDate = financialStartDate + new TimeSpan(3, 0, 0);
                var financialEndDate = Convert.ToDateTime("31/03/" + startDate.AddYears(1).Year);
                financialEndDate = financialEndDate + new TimeSpan(7, 0, 0);
                var hbinitialStart = startDate + new TimeSpan(7, 0, 0);

                var YearChecker = (financialStartDate - hbinitialStart).TotalDays;
                if (YearChecker < 0) HB_StartYear = HB_StartYear - 1;

                var DaysInYear = (financialStartDate - (financialStartDate.AddYears(1) + new TimeSpan(7, 0, 0))).TotalDays;
                var Payment_Cycle = Convert.ToInt32(ApplicationConstants.DefaultCycle);

                //var Initial_Cycle = (HB_InitialStart + new TimeSpan(3, 0, 0) - HB_InitialEnd + new TimeSpan(7, 0, 0)).TotalDays + 1;
                var Initial_Cycle = Convert.ToInt32((HB_InitialEnd + new TimeSpan(7, 0, 0) - HB_InitialStart + new TimeSpan(3, 0, 0)).TotalDays) + 1;
                var Initial_Payment = Math.Abs(newHb);
                var DR = -1 * Initial_Payment / Initial_Cycle;
                var ONE12 = (DR * DaysInYear) / 12;
                var RE_YHBE = ONE12 * 12;
                var OneDay = 24 * 60 * 60 * 1000;
                var HB_Start = HB_InitialStart + new TimeSpan(3, 0, 0);
                var HB_End = HB_InitialEnd + new TimeSpan(7, 0, 0);
                var INITIAL = 1;
                var DateCounter = HB_End.AddMilliseconds(OneDay);
                //            var DateCounterPlusCycle = HB_End.AddMilliseconds(OneDay * Payment_Cycle);
                var DateCounterPlusCycle = HB_End.AddDays(Payment_Cycle);

                var ER_AM = 0.0;
                var ToAM = 0.0;
                for (int i = 1; i <= ApplicationConstants.NextHBPayment; i++)
                {
                    if (INITIAL == 1)
                    {
                        ER_AM = DR * Payment_Cycle;
                       var AM = Math.Round(DR * Initial_Cycle, 2);
                        ToAM = AM;
                        var iDays = Convert.ToInt32((HB_End - HB_Start).TotalDays) + 1;
                       INITIAL = 2;
                        DbContext.F_HBACTUALSCHEDULE.Add(new F_HBACTUALSCHEDULE
                        {
                            HB =  AM,
                            STARTDATE = HB_Start.Date,
                            ENDDATE = HB_End.Date,
                            HBID=HBId
                          // DefaultCycle = iDays
                        });
                    }
                    else
                    {
                        ER_AM += DR * Payment_Cycle;
                        var AM = Math.Round(DR * Payment_Cycle, 2);
                        ToAM += AM;
                       
                        var iDays = (DateCounterPlusCycle - DateCounter).TotalDays + 1;

                        //F_HBACTUALSCHEDULE scheduleHB = new F_HBACTUALSCHEDULE();


                        DbContext.F_HBACTUALSCHEDULE.Add(new F_HBACTUALSCHEDULE
                        {
                        HB = AM,
                        STARTDATE = DateCounter.Date,
                        ENDDATE = DateCounterPlusCycle.Date,
                        HBID = HBId
                       //DefaultCycle = iDays
                        });
                        DateCounter = DateCounter.AddDays(Payment_Cycle);
                        DateCounterPlusCycle = (DateCounterPlusCycle.AddDays(Payment_Cycle));
                    }
                }
                //var list= DbContext.ChangeTracker.Entries().Where(x=>x.State==EntityState.Added);
                DbContext.SaveChanges();
                DbContext.F_AUTO_HBSCHEDULE_ENTRY();

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public int? GetHBRow(int exceptionId)
        {
            try
            {
                int? HBRow = (from exception in DbContext.F_HBUploadExceptions
                             join upData in DbContext.F_HBUploadFileData on exception.HBUploadFileDataId equals upData.HBUploadFileDataId
                             where exception.HBUploadExceptionId == exceptionId
                             select upData.HBRow
                             ).FirstOrDefault();
                return HBRow;
            }
            catch (Exception ex)
            {
                return -1;
            }
        }
        public void UpdateJournalId(HBScheduleRentInformation fileEntry, int journalId)
        {
            var hbSchedule = DbContext.F_HBACTUALSCHEDULE.Where(ha => ha.HBROW == fileEntry.HBRow).First();
            hbSchedule.SENT = 1;
            hbSchedule.VALIDATED = 1;
            hbSchedule.JOURNALID = journalId;
            DbContext.SaveChanges();
        }


    }
}
