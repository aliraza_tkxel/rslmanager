﻿using RSLHousingBenefit.Repository.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSLHousingBenefit.Repository.Repositories
{
    public class HBExceptionReasonRepository : BaseRepository<F_HBExceptionReason>
    {

        public HBExceptionReasonRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {

        }

    }
}
