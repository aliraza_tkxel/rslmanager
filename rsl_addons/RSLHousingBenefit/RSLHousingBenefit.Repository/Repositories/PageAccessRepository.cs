﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSLHousingBenefit.Repository.DomainModel;

namespace RSLHousingBenefit.Repository.Repositories
{
    public class PageAccessRepository : BaseRepository<AC_PAGES_ACCESS>
    {
        public PageAccessRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {

        }
    }
}
