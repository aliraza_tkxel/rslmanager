﻿using RSLHousingBenefit.Common.Constants;
using RSLHousingBenefit.Common.Helper;
using RSLHousingBenefit.Models.Models;
using RSLHousingBenefit.Models.ViewModels;
using RSLHousingBenefit.Repository.DomainModel;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSLHousingBenefit.Repository.Repositories
{
    public class HBUploadRepository : BaseRepository<F_HBUpload>
    {
        public HBUploadRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {

        }
        public UploadFileListViewModel GetUploadFileData(PaginationViewModel pageInfo)
        {
            string projectPath = FileHelper.getLogicalDocumentPath(ApplicationConstants.TypeHBDoc);
            UploadFileListViewModel uploadFileListViewModel = new UploadFileListViewModel();
            var fileData = DbContext.F_HBUploadFileData;
            var file = from f in DbContext.F_HBUpload
                       join e in DbContext.E__EMPLOYEE on f.UploadedBy equals e.EMPLOYEEID
                       join l in DbContext.G_LOCALAUTHORITY on f.LocalAuthorityId equals l.LOCALAUTHORITYID
                       where f.IsProcessed == true
                       select new UploadFileListModel()
                       {
                           HbUploadId=f.HbUploadId,
                           UploadDate = f.UploadedDate.ToString(),
                           LocalAuthority = l.DESCRIPTION,
                           FileName = f.ActualFileName,
                           FilePath= f.FileName,
                           UploadBy = e.FIRSTNAME + " " + e.LASTNAME,
                           UploadRecord = fileData.Where(y => y.HbUploadId == f.HbUploadId && y.IsExcluded != true).Count(),
                           UploadRecordTotal = fileData.Where(y => y.HbUploadId == f.HbUploadId && y.IsExcluded != true).Sum(a => a.Amount) ?? default(int),
                           ExcludeRecord = fileData.Where(y => y.HbUploadId == f.HbUploadId && y.IsExcluded == true).Count(),
                           ExcludeRecordTotal = fileData.Where(y => y.HbUploadId == f.HbUploadId && y.IsExcluded == true).Sum(a => a.Amount) ?? default(int),
                           OrignalRecord = fileData.Where(y => y.HbUploadId == f.HbUploadId).Count(),
                           OrignalRecordTotal = fileData.Where(y => y.HbUploadId == f.HbUploadId).Sum(a => a.Amount) ?? default(int)
                       };
            PaginationViewModel page = new PaginationViewModel();
            page.totalRows = file.ToList().Count();
            page.totalPages = (int)Math.Ceiling((double)file.ToList().Count / pageInfo.pageSize);
            page.pageNo = pageInfo.pageNo;
            page.pageSize = pageInfo.pageSize;
            int _page = (page.pageNo - 1);

            uploadFileListViewModel.pagination = page;

            // Counting skip
            int _skip = _page * page.pageSize;
            var finalData = file.ToList();

            if (pageInfo.sortOrder.ToLower().Equals(ApplicationConstants.OrderDesc.ToLower()))
            {
                finalData = finalData.OrderByDescending(c => c.GetType().GetProperty(pageInfo.sortBy).GetValue(c, null)).Skip(_skip).Take(pageInfo.pageSize).ToList();
            }
            else
            {
                finalData = finalData.OrderBy(c => c.GetType().GetProperty(pageInfo.sortBy).GetValue(c, null)).Skip(_skip).Take(pageInfo.pageSize).ToList();
            }
            foreach (var item in finalData)
            {
                if (item.UploadDate.Length>0)
                    {
                    item.UploadDate = Convert.ToDateTime(item.UploadDate).ToShortDateString();
                    }
                item.FilePath=FileHelper.getLogicalDocumentPath(item.FilePath);
                uploadFileListViewModel.uploadFileListModel.Add(new UploadFileListModel { ExcludeRecord = item.ExcludeRecord,
                                                                                          ExcludeRecordTotal = item.ExcludeRecordTotal,
                                                                                          FileName = item.FileName,
                                                                                          LocalAuthority = item.LocalAuthority,
                                                                                          OrignalRecord = item.OrignalRecord,
                                                                                          OrignalRecordTotal = item.OrignalRecordTotal,
                                                                                          UploadBy = item.UploadBy,
                                                                                          UploadDate = item.UploadDate,
                                                                                          UploadRecord = item.UploadRecord,
                                                                                          UploadRecordTotal = item.UploadRecordTotal
                                                                                        });
            }
                uploadFileListViewModel.uploadFileListModel = finalData;
            return uploadFileListViewModel;
        }
        public List<ExceptionFileListModel> GetExceptionFileData(int hbUploadId)
        {
            try
            {
                ExceptionFileListModel exceptionFileListModel = new ExceptionFileListModel();
                var ExcludedRecords = (from record in DbContext.F_HBUploadFileData
                                      where record.HbUploadId == hbUploadId && record.IsExcluded == true
                                      select new ExceptionFileListModel()
                                      {
                                          HBUploadFileDataId = record.HBUploadFileDataId,
                                          HbUploadId = record.HbUploadId,
                                          IsExcluded = record.IsExcluded,
                                          Address = record.Address,
                                          Amount = record.Amount,
                                          endDate= record.EndDate,
                                          HBRef = record.HBRef,
                                          startDate = record.StartDate,
                                          TenantName = record.TenantName
                                      }).ToList() ;
                foreach (var ExcludedRecord in ExcludedRecords)
                {
                    if (ExcludedRecord.endDate.HasValue)
                    {
                        DateTime endDate = (DateTime)ExcludedRecord.endDate;
                        ExcludedRecord.EndDate = endDate.ToString("dd/MM/yyyy");
                    }
                    if (ExcludedRecord.startDate.HasValue)
                    {
                        DateTime startDate = (DateTime)ExcludedRecord.startDate;
                        ExcludedRecord.StartDate = startDate.ToString("dd/MM/yyyy");
                    }

                }
                return ExcludedRecords;
            }
            catch (Exception e)
            {
                throw e;
            }                                  
        }
        public void ProcessHBFile(int hbUploadId)
        {
            var hbUpload = DbContext.F_HBUpload.Where(hu => hu.HbUploadId == hbUploadId).FirstOrDefault();
            hbUpload.IsProcessed = true;
            DbContext.SaveChanges();
        }

    }
}
