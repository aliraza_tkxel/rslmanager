﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSLHousingBenefit.Repository.DomainModel;

namespace RSLHousingBenefit.Repository.Repositories
{
    public class EmployeeRepository : BaseRepository<E__EMPLOYEE>
    {
        public EmployeeRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {
        }
    }

}
