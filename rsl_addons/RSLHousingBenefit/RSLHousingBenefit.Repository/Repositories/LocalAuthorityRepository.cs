﻿using RSLHousingBenefit.Models.ViewModels.HBUploadFile;
using RSLHousingBenefit.Repository.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSLHousingBenefit.Repository.Repositories
{
    public class LocalAuthorityRepository : BaseRepository<G_LOCALAUTHORITY>
    {
        public LocalAuthorityRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {

        }
        public List<G_LOCALAUTHORITY> GetLocalAuthorities()
        {
            return DbContext.G_LOCALAUTHORITY.ToList();
        }
    }
}
