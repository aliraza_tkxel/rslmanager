﻿using RSLHousingBenefit.Repository.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSLHousingBenefit.Repository.Repositories
{
    public class CustomerRepo :BaseRepository<C__CUSTOMER>
    {
        public CustomerRepo(RSLBHALiveEntities DbContext) : base(DbContext)
        {

        }
    }
}
