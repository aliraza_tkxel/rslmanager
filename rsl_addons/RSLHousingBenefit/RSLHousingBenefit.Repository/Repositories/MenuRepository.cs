﻿using RSLHousingBenefit.Models.ViewModels;
using RSLHousingBenefit.Repository.DomainModel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSLHousingBenefit.Repository.Repositories
{
    public class MenuRepository : BaseRepository<AC_MENUS>
    {

        public MenuRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {

        }

        #region Get Menus By ModuleId
        public List<MenuViewModel> GetMenusByModule(int userId,int moduleId)
        {
            List<MenuViewModel> menuList=null;
            var menus = (from emp in DbContext.E__EMPLOYEE
                         join access in DbContext.AC_MENUS_ACCESS on emp.JobRoleTeamId equals access.JobRoleTeamId
                         join menu in DbContext.AC_MENUS on access.MenuId equals menu.MENUID
                         join module in DbContext.AC_MODULES on menu.MODULEID equals module.MODULEID
                         where menu.ACTIVE == 1
                         && emp.EMPLOYEEID == userId
                         && module.MODULEID == moduleId
                         orderby menu.ORDERTEXT, menu.DESCRIPTION
                         select new MenuViewModel
                         {
                             menuId = menu.MENUID,
                             menuName = menu.DESCRIPTION,
                             path = menu.PAGE
                         }
                        );

            if (menus.Count() > 0)
            {
                menuList = menus.ToList();
                foreach (var item in menuList)
                {
                    item.subMenus = GetSubMenus(item.menuId, userId);
                }
            }

            return menuList;
        }
        #endregion

        #region Get Sub Menus
        public List<SubmenuViewModel> GetSubMenus(int menuId, int userId)
        {
            var subMenus = (from emp in DbContext.E__EMPLOYEE
                        join access in DbContext.AC_PAGES_ACCESS on emp.JobRoleTeamId equals access.JobRoleTeamId
                        join page in DbContext.AC_PAGES on access.PageId equals page.PAGEID
                        join menu in DbContext.AC_MENUS on page.MENUID equals menu.MENUID
                        join module in DbContext.AC_MODULES on menu.MODULEID equals module.MODULEID
                        where page.ACTIVE == 1
                        && page.LINK == 1
                        && emp.EMPLOYEEID == userId
                        && menu.MENUID == menuId
                        orderby menu.ORDERTEXT, menu.DESCRIPTION
                        select new SubmenuViewModel
                        {
                            subMenuName = page.DESCRIPTION,
                            menuId = menu.MENUID,
                            path = string.Concat("/", module.DIRECTORY, "/", page.PAGE)
                        }
                        );            
            return subMenus.ToList();
        }
        #endregion


    }
}
