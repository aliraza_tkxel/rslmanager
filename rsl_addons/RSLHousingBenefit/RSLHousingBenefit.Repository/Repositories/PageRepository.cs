﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSLHousingBenefit.Repository.DomainModel;

namespace RSLHousingBenefit.Repository.Repositories
{
    public class PageRepository : BaseRepository<AC_PAGES>
    {
        public PageRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {

        }
    }

}
