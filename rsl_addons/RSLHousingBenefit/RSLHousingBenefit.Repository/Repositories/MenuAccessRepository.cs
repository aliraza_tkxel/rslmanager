﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSLHousingBenefit.Repository.DomainModel;

namespace RSLHousingBenefit.Repository.Repositories
{
    public class MenuAccessRepository : BaseRepository<AC_MENUS_ACCESS>
    {
        public MenuAccessRepository(RSLBHALiveEntities dbContext) : base(dbContext)
        {

        }

    }
}
