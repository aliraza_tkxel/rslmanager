﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace RSLHousingBenefit.Repository
{
    public interface IBaseRepository<T> where T : class
    {
        T Add(T entity);
        IQueryable<T> AsQueryable();
        void Attach(T entity);
        void Delete(T entity);
        void Dispose();
        IEnumerable<T> Find(Expression<Func<T, bool>> predicate);
        IEnumerable<T> FindWithTracking(Expression<Func<T, bool>> predicate);
        T First(Expression<Func<T, bool>> predicate);
        IEnumerable<T> GetAll();
        T GetById(int id);
        T Single(Expression<Func<T, bool>> predicate);
        T SingleOrDefault(Expression<Func<T, bool>> predicate);
        T Update(T entity, int key);
        T UpdateStringTypeId(T entity, string key);
    }
}
