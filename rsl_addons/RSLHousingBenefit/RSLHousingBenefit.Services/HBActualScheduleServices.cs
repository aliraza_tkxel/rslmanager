﻿using RSLHousingBenefit.Interfaces.Service;
using RSLHousingBenefit.Models.Models;
using RSLHousingBenefit.Repository.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSLHousingBenefit.Common.Constants;

namespace RSLHousingBenefit.Services
{
    public class HBActualScheduleServices : IHBActualScheduleServices
    {
        private IUnitOfWork _uow;
        public HBActualScheduleServices(IUnitOfWork uow)
        {
            _uow = uow;
        }
        public HBCustomerRecordModel GetCustomerRecordById(int HBId)
        {
            HBCustomerRecordModel hBCustomerRecordModel = new HBCustomerRecordModel();
            hBCustomerRecordModel.hBPaymentModel = _uow.HBActualScheduleRepo.GetTopHBPayments(HBId);
            hBCustomerRecordModel.hBNextEstimatedPaymentModel = _uow.HBActualScheduleRepo.GetNextEstimatedHBPayments(HBId);
            var hbInformation = _uow.HBInformationRepo.GetById(HBId);
            hBCustomerRecordModel.hBInformation = new HBInformation
            {
                ActualEndDate = hbInformation.ACTUALENDDATE.ToString(),
                CustomerId = hbInformation.CUSTOMERID,
                Hbid = hbInformation.HBID,
                Hbla = hbInformation.HBLA,
                HbRef = hbInformation.HBREF,
                InitialEndDate = hbInformation.INITIALENDDATE.ToString(),
                InitialPayment = hbInformation.INITIALPAYMENT,
                InitialStartDate = hbInformation.INITIALSTARTDATE.ToString(),
                Status = hbInformation.STATUS,
                TenancyId = hbInformation.TENANCYID
            };
            if (!string.IsNullOrEmpty(hBCustomerRecordModel.hBInformation.InitialEndDate))
                hBCustomerRecordModel.hBInformation.InitialEndDate = Convert.ToDateTime(hBCustomerRecordModel.hBInformation.InitialEndDate).ToShortDateString();
            if (!string.IsNullOrEmpty(hBCustomerRecordModel.hBInformation.InitialStartDate))
                hBCustomerRecordModel.hBInformation.InitialStartDate = Convert.ToDateTime(hBCustomerRecordModel.hBInformation.InitialStartDate).ToShortDateString();

            if (!(string.IsNullOrEmpty(hBCustomerRecordModel.hBInformation.InitialEndDate) &&
               string.IsNullOrEmpty(hBCustomerRecordModel.hBInformation.InitialStartDate)))
            {
                //hBCustomerRecordModel.hBInformation.DefaultCycle = (Convert.ToDateTime(hBCustomerRecordModel.hBInformation.InitialEndDate) - Convert.ToDateTime(hBCustomerRecordModel.hBInformation.InitialStartDate)).TotalDays;
                hBCustomerRecordModel.hBInformation.DefaultCycle = Double.Parse(ApplicationConstants.DefaultCycle);
            }
            return hBCustomerRecordModel;
        }
        public List<HBPaymentModel> GetNextPayment(int HBId)
        {
            var nextHbPayments = _uow.HBActualScheduleRepo.NextHBPayments(HBId);
            return nextHbPayments;
        }
        public List<HBPaymentModel> GetDummyNextPayment(DateTime startDate, DateTime endDate, double newHb)
        {
            var HB_InitialStart = startDate;
            var HB_InitialEnd = endDate;
            var nextHbPayments = new List<HBPaymentModel>();
            var HB_StartYear = HB_InitialStart.Year;

            var financialStartDate = Convert.ToDateTime("01/04/" + startDate.Year);
            financialStartDate = financialStartDate + new TimeSpan(3, 0, 0);
            var financialEndDate = Convert.ToDateTime("31/03/" + startDate.AddYears(1).Year);
            financialEndDate = financialEndDate + new TimeSpan(7, 0, 0);
            var hbinitialStart = startDate + new TimeSpan(7, 0, 0);

            var YearChecker = (financialStartDate - hbinitialStart).TotalDays;
            if (YearChecker < 0) HB_StartYear = HB_StartYear - 1;

            var DaysInYear = (financialStartDate - (financialStartDate.AddYears(1) + new TimeSpan(7, 0, 0))).TotalDays;
            var Payment_Cycle = Convert.ToInt32(ApplicationConstants.DefaultCycle);

            //var Initial_Cycle = (HB_InitialStart + new TimeSpan(3, 0, 0) - HB_InitialEnd + new TimeSpan(7, 0, 0)).TotalDays + 1;
            var Initial_Cycle = Convert.ToInt32((HB_InitialEnd + new TimeSpan(7, 0, 0)- HB_InitialStart + new TimeSpan(3, 0, 0)).TotalDays) + 1;
            var Initial_Payment = Math.Abs(newHb);
            var DR = -1*Initial_Payment / Initial_Cycle;
            var ONE12 = (DR * DaysInYear) / 12;
            var RE_YHBE = ONE12 * 12;
            var OneDay = 24 * 60 * 60 * 1000;
            var HB_Start = HB_InitialStart + new TimeSpan(3, 0, 0);
            var HB_End = HB_InitialEnd + new TimeSpan(7, 0, 0);
            var INITIAL = 1;
            var DateCounter = HB_End.AddMilliseconds(OneDay);
//          var DateCounterPlusCycle = HB_End.AddMilliseconds(OneDay * Payment_Cycle);
            var DateCounterPlusCycle = HB_End.AddDays(Payment_Cycle);

            var ER_AM = 0.0;
            var ToAM = 0.0;
            for (int i = 1; i <= ApplicationConstants.NextHBPayment; i++)
            {
                if (INITIAL == 1)
                {
                    ER_AM = DR * Payment_Cycle;
                    var AM = Math.Round(DR * Initial_Cycle, 2);
                    ToAM = AM;
                    var iDays = Convert.ToInt32((HB_End- HB_Start).TotalDays) + 1;
                    INITIAL = 2;
                    nextHbPayments.Add(new HBPaymentModel
                    {
                        Hb = AM,
                        StartDate = HB_Start.ToShortDateString(),
                        EndDate = HB_End.ToShortDateString(),
                        Start=HB_Start,
                        DefaultCycle = iDays
                    });
                }
                else
                {
                    ER_AM += DR * Payment_Cycle;
                    var AM = Math.Round(DR * Payment_Cycle, 2);
                    ToAM += AM;
                    var iDays = (DateCounterPlusCycle - DateCounter).TotalDays + 1;
                    nextHbPayments.Add(new HBPaymentModel
                    {
                        Hb = AM,
                        StartDate = DateCounter.ToShortDateString(),
                        EndDate = DateCounterPlusCycle.ToShortDateString(),
                        Start = HB_Start,
                        DefaultCycle = iDays
                    });
                    DateCounter = DateCounter.AddDays(Payment_Cycle );
                    DateCounterPlusCycle = (DateCounterPlusCycle.AddDays(Payment_Cycle ));
                }
            }
            //double perDayPayment = newHb / (endDate.Date - startDate.Date).Days;
            //for (int i = 0; i < ApplicationConstants.NextHBPayment; i++)
            //{
            //    nextHbPayments.Add(new HBPaymentModel
            //    {
            //        StartDate = startDate.ToShortDateString(),
            //        EndDate = endDate.ToShortDateString(),
            //        Hb = Math.Round(newHb,3),
            //        DefaultCycle = (endDate.Date-startDate.Date).Days
            //    });

            //    startDate = endDate.AddDays(1);
            //    endDate = startDate.AddDays(Convert.ToInt32(ApplicationConstants.DefaultCycle)-1);
            //    newHb = perDayPayment * (endDate.Date - startDate.Date).Days;
            //}
            nextHbPayments=nextHbPayments.OrderBy(x => x.Start).ToList();
            return nextHbPayments;
        }
        public bool UpdateHbActualScheduleService(int HBId, DateTime newStartDate, DateTime newEndDate, double newHb, int CustomerId, Int64 TenancyId)
        {
            try
            {
                //_uow.HBActualScheduleRepo.AddNextPayments(HBId, newStartDate, newEndDate, newHb);
                _uow.HBActualScheduleRepo.UpdateStartDate(HBId, newStartDate);
                _uow.HBActualScheduleRepo.UpdateEndDate(HBId, newEndDate);
                _uow.HBActualScheduleRepo.UpdateHB(HBId, newHb);
                _uow.RentJournalRepo.DeleterentJournal(CustomerId, TenancyId);
                _uow.HBActualScheduleRepo.UpdateNextPayments(HBId, newStartDate, newEndDate, newHb);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
        public bool ExcludeHbUpload(int HBUploadExceptionId, bool isExclude)
        {
            var exception = _uow.HBUploadExceptionRepo.GetById(HBUploadExceptionId);
            return _uow.HBUploadFileDataRepo.ExcludeHbUpload(exception.HBUploadFileDataId.Value, isExclude);
        }
        public int? GetHBRow(int exceptionId)
        {
            try
            {
                return _uow.HBActualScheduleRepo.GetHBRow(exceptionId);
            }
            catch (Exception ex)
            {

                return -1;
            }
        }
        public string GetExceptionReason(int exceptionId)
        {
            var reasonId = _uow.HBUploadExceptionRepo.GetById(exceptionId).ReasonId;
            return _uow.HBExceptionReasonRepo.GetById(reasonId.Value).Reason;
        }
        public string GetCustomerName(int CustomerId)
        {
            var customer = _uow.customerRepo.GetById(CustomerId);
            string customerName = customer.FIRSTNAME + " " + customer.LASTNAME;
            return customerName;
        }
        public string GetTenancyAddress(int customerId, int tenancyId)
        {
            return _uow.tenancyRepo.GetTenancyAddres(customerId, tenancyId);
        }
    }
}
