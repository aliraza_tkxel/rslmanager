﻿using GemBox.Spreadsheet;
using System;
using System.Web.Configuration;

namespace RSLHousingBenefit.Services.Services
{
    public class ExcelToHtml
    {
        public string ConvertToHtml(string Path,string fileName,string lastColumn)
        {
            string key = WebConfigurationManager.AppSettings["GemBox"];
            SpreadsheetInfo.SetLicense(key);
            var fileType = fileName.Split('.');
            ExcelFile ef = new ExcelFile();
            if (fileType[fileType.Length - 1] == "csv")
            {
                ef = ExcelFile.Load(Path + "\\" + fileName);
            }
            else if (fileType[fileType.Length-1]=="xlsx")
            {
                ef.LoadXlsx(Path + "\\" + fileName, XlsxOptions.None);
            }
            var ws = ef.Worksheets[0];
            ws.PrintOptions.PrintHeadings = true;
            ws.PrintOptions.PrintGridlines = true;

            ws.NamedRanges.SetPrintArea(ws.Cells.GetSubrange("A1", lastColumn+1000));
            //ws.NamedRanges.SetPrintArea(ws.Cells.GetSubrange("A1", "CZ1000"));
            HtmlSaveOptions options = new HtmlSaveOptions()
            {
                HtmlType = HtmlType.Html,
                SelectionType = SelectionType.EntireFile
            };
            fileName = "\\HBReferance" + DateTime.Now.ToString("hh-mm-ss-ffffff");
            ef.Save(Path+fileName, options);
            return Path +fileName+ "_files\\sheet001.html";
        }
    }
}
