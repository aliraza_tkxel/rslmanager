﻿using RSLHousingBenefit.Common.Constants;
using RSLHousingBenefit.Common.Helper;
using RSLHousingBenefit.Models;
using System;
using System.Collections.Generic;
using System.IO;

namespace RSLHousingBenefit.Services.Services
{
    public class CsvFileReader
    {
        public Tuple <string,List<HouseBenefitModel>> ReadCsvFile(string fileName, int[] dataLocation)
        {
            //string directorypath = SetupDirectoryPath(); Uncomment this line before deploying
            //System.IO.FileStream fileStream = System.IO.File.Create( directorypath + fileName); Uncomment this line before deploying
            string line = string.Empty,result=string.Empty;
            int startingPosition = dataLocation[0],row = 1;
            //string projectPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;//remove these lines before deploying 
            string projectPath = FileHelper.getFileDirectoryPath(ApplicationConstants.TypeHBDoc);
            string folderName = Path.Combine(projectPath, "HBDoc");//remove these lines before deploying 
            List<HouseBenefitModel> houseBenefitModel = new List<HouseBenefitModel>();
            System.IO.StreamReader file =new System.IO.StreamReader(folderName + "\\" + fileName);
            
            while ((line = file.ReadLine()) != null)
            {
                if (startingPosition <= row)
                {
                    var data = line.Split(',');
                    HouseBenefitModel houseBenefit = new HouseBenefitModel();
                    if (!houseBenefit.SetHBReferance(data[dataLocation[1] - 1]))
                    {
                        result = string.Format("Invalid HBRef format at Line {0}", row);
                        return Tuple.Create(result,houseBenefitModel);
                    }
                    if(!houseBenefit.SetAmount(data[dataLocation[2]-1]))
                    {
                        result = string.Format("Invalid Amount format at Line {0}", row);
                        return Tuple.Create(result, houseBenefitModel);
                    }
                    if(!houseBenefit.SetPeriodStartDate((data[dataLocation[3]-1])))
                    {
                        result = string.Format("Invalid Period Start Date format at Line {0}", row);
                        return Tuple.Create(result, houseBenefitModel);
                    }
                    if(!houseBenefit.SetPeriodEndDate((data[dataLocation[4]-1])))
                    {
                        result = string.Format("Invalid Period End Date format at Line {0}", row);
                        return Tuple.Create(result, houseBenefitModel);
                    }
                    if(!houseBenefit.SetTenantName(data[dataLocation[5]-1]))
                    {
                        result = string.Format("Invalid Tenant Name format at Line {0}", row);
                        return Tuple.Create(result, houseBenefitModel);
                    }
                    if(!houseBenefit.SetAddress(data[dataLocation[6]-1]))
                    {
                        result = string.Format("Invalid Address format at Line {0}", row);
                        return Tuple.Create(result, houseBenefitModel);
                    }
                    houseBenefitModel.Add(houseBenefit);
                }
                row++;
            }
            result = "successful";
            return Tuple.Create(result,houseBenefitModel);
        }
    }
}
