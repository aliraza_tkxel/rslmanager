﻿using OfficeOpenXml;
using System.IO;
using System.Collections.Generic;
using System;
using RSLHousingBenefit.Models;
using RSLHousingBenefit.Common.Constants;
using RSLHousingBenefit.Common.Helper;

namespace RSLHousingBenefit.Services.Services
{
    public class XlsFileReader
    {
        public Tuple<string, List<HouseBenefitModel>>  ReadExcelFile(string fileName, int[] dataLocation)
        {
            //string directorypath = SetupDirectoryPath(); Uncomment this line before deploying
            //System.IO.FileStream fileStream = System.IO.File.Create( directorypath + fileName); Uncomment this line before deploying


            //string projectPath = Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName;//remove these lines before deploying 
            string projectPath = FileHelper.getFileDirectoryPath(ApplicationConstants.TypeHBDoc);
            string folderName = Path.Combine(projectPath, "HBDoc");//remove these lines before deploying 
            string result = string.Empty;
            List<HouseBenefitModel> houseBenefitModel = new List<HouseBenefitModel>();
            var package = new ExcelPackage(new FileInfo(folderName+"\\"+fileName));
            ExcelWorksheet workSheet = package.Workbook.Worksheets[1];
            var x1=workSheet.Cells[1, 1].ToString();
            var x2 = workSheet.Cells.Text;
            
            for (int row= dataLocation[0];
                     row <= workSheet.Dimension.End.Row;
                     row++)
            {
                HouseBenefitModel houseBenefit = new HouseBenefitModel();
                if(!houseBenefit.SetHBReferance (workSheet.Cells[row , dataLocation[1] ].Text))
                {
                    result = string.Format("Invalid HBRef format in Document at Line {0}", row);
                    return Tuple.Create(result, houseBenefitModel);
                }
                if(!houseBenefit.SetAmount(workSheet.Cells[row , dataLocation[2] ].Value.ToString()))
                {
                    result = string.Format("Invalid Amount format in Document at Line {0}", row);
                    return Tuple.Create(result, houseBenefitModel);
                }
                if(!houseBenefit.SetPeriodStartDate ((workSheet.Cells[row , dataLocation[3] ].Value.ToString().Split(' ')[0])))
                {
                    result = string.Format("Invalid Period Start Date format in Document at Line {0}", row);
                    return Tuple.Create(result, houseBenefitModel);
                }
                if(!houseBenefit.SetPeriodEndDate ((workSheet.Cells[row , dataLocation[4] ].Value.ToString().Split(' ')[0])))
                {
                    result = string.Format("Invalid Period End Date format in Document at Line {0}", row);
                    return Tuple.Create(result, houseBenefitModel);
                }
                if(!houseBenefit.SetTenantName (workSheet.Cells[row , dataLocation[5] ].Text))
                {
                    result = string.Format("Invalid Tenant Name format in Document at Line {0}", row);
                    return Tuple.Create(result, houseBenefitModel);
                }
                if(!houseBenefit.SetAddress(workSheet.Cells[row , dataLocation[6] ].Text))
                {
                    result = string.Format("Invalid Address format in Document at Line {0}", row);
                    return Tuple.Create(result, houseBenefitModel);
                }
                houseBenefitModel.Add(houseBenefit);
            }
            result = "successful";
            return Tuple.Create(result, houseBenefitModel);
        }
    }
}
