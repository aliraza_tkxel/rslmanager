﻿using RSLHousingBenefit.Interfaces.Service;
using RSLHousingBenefit.Models.ViewModels.HBUploadFile;
using RSLHousingBenefit.Repository.DomainModel;
using RSLHousingBenefit.Repository.UnitOfWork;
using System;
using System.Collections.Generic;
using RSLHousingBenefit.Models;
using RSLHousingBenefit.Models.Models;
using System.Linq;
using RSLHousingBenefit.Models.ViewModels;

namespace RSLHousingBenefit.Services.Services
{
   public  class HBUploadServices : IHBUploadService
    {
        private IUnitOfWork _uow;
        public HBUploadServices(IUnitOfWork unitOfWork)
        {
            _uow = unitOfWork;
        }
        public int AddHBUploadFile(HouseBenefitFile houseBenefitFile)
        {
            F_HBUpload hbUpload = new F_HBUpload
            {
                ActualFileName = houseBenefitFile.ActualFileName,
                FileName = houseBenefitFile.FileName,
                TransactionDate = houseBenefitFile.TransactionDate,
                UploadedBy = houseBenefitFile.UploadedBy,
                UploadedDate = houseBenefitFile.UploadedDate,
                LocalAuthorityId = houseBenefitFile.LocalAuthorityId,
            };

            _uow.HBUploadRepo.Add(hbUpload);
            _uow.Save();
            return hbUpload.HbUploadId;
        }
        public void AddHBUploadFileData(List<HouseBenefitModel> houseBenfitModels,int HBFileId)
        {
            foreach (var houseBenfitModel in houseBenfitModels)
            {
                _uow.HBUploadFileDataRepo.Add(new F_HBUploadFileData
                {

                    Address = houseBenfitModel.GetAddress(),
                    EndDate = Convert.ToDateTime(houseBenfitModel.GetPeriodEndDate()),
                    HBRef = houseBenfitModel.GetHBReferance(),
                    StartDate = Convert.ToDateTime(houseBenfitModel.GetPeriodStartDate()),
                    TenantName = houseBenfitModel.GetTenantName(),
                    Amount = Convert.ToDouble(houseBenfitModel.GetAmount()),
                    HbUploadId = HBFileId
                });
            }
            _uow.Save();
        }
        public List<LocalAuthorityViewModel> GetLocalAuthorities()
        {

            List<LocalAuthorityViewModel> localAuthoritiesList = new List<LocalAuthorityViewModel>();
            var localAuthorities = _uow.localAuthorityRepository.GetAll();
            foreach (var localAuthority in localAuthorities)
            {
                localAuthoritiesList.Add(new LocalAuthorityViewModel
                {
                    DESCRIPTION = localAuthority.DESCRIPTION,
                    LINKTOSUPPLIER = localAuthority.LINKTOSUPPLIER,
                    LOCALAUTHORITYID = localAuthority.LOCALAUTHORITYID,
                    Nrosh_Code = localAuthority.Nrosh_Code
                });
            }
            return localAuthoritiesList;
        }
        public UploadFileListViewModel GetUploadFileData(PaginationViewModel page)
        {
            return _uow.HBUploadRepo.GetUploadFileData(page);
        }
        public List <ExceptionFileListModel> GetExceptionUploadFileData(int hbUploadId)
        {
            return _uow.HBUploadRepo.GetExceptionFileData(hbUploadId);
        }
    }
}
