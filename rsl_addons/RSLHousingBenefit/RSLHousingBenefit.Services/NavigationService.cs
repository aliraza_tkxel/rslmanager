﻿using RSLHousingBenefit.Interfaces.Service;
using RSLHousingBenefit.Models.ViewModels;
using RSLHousingBenefit.Repository.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RSLHousingBenefit.Services
{
    public class NavigationService : INavigationService
    {
        private IUnitOfWork _uow;

        public NavigationService(IUnitOfWork unitOfWork)
        {
            _uow = unitOfWork;
        }

        #region Displaying Menu on top bar
        public NavigationViewModel GetMenus(NavigationRequestViewModel request)
        {            
            NavigationViewModel menusBo = new NavigationViewModel();
            
            int moduleId = _uow.ModuleRepo.GetModuleId(request.moduleName);            
            var menus = _uow.MenuRepo.GetMenusByModule(request.userId, moduleId);
            menusBo.menues = menus;            
            menusBo.modules = _uow.ModuleRepo.GetModules(request.userId);
            return menusBo;
        }
        #endregion

    }
}
