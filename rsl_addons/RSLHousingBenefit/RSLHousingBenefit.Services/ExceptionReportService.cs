﻿using RSLHousingBenefit.Common.Constants;
using RSLHousingBenefit.Interfaces.Service;
using RSLHousingBenefit.Repository.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RSLHousingBenefit.Models.ViewModels;

namespace RSLHousingBenefit.Services
{
    public class ExceptionReportService : IExceptionReportService
    {
        private IUnitOfWork _uow;

        public ExceptionReportService(IUnitOfWork unitOfWork)
        {
            _uow = unitOfWork;
        }

        #region Validate HB uploaded data
        /// <summary>
        /// -- Reset previous exceptions
        /// -- Get All entries from F_HBUploadFileData by HbUploadId
        ///  -- For each item of resulted F_HBUploadFileData
        ///	-- Get active F_HBINFORMATION based on HB Ref from F_HBUploadFileData item
        ///	-- If no record found
        ///		-- INSERT in F_HBUploadExceptions with reason 'HB Ref Mismatch'
        ///	-- If record found
        ///		-- Based upon HBID, StartDate and EndDate get record from F_HBACTUALSCHEDULE
        ///		-- If no record found
        ///			-- Filter by HBID and StartDate
        ///			-- If no record found
        ///				-- INSERT in F_HBUploadExceptions with reason 'START DATE INCORRECT'
        ///			-- Filter by HBID and EndDate
        ///				-- INSERT in F_HBUploadExceptions with reason 'END DATE INCORRECT' 
        ///		-- If record found 
        ///			-- Check the amount matches
        ///			-- If does not match 
        ///				-- INSERT in F_HBUploadExceptions with reason 'AMOUNT INCORRECT' 
        ///			-- If does matches
        ///				-- Check the journalid exists
        ///				-- If exists
        ///					-- INSERT in F_HBUploadExceptions with reason 'PAYMENT EXISTS IN RSL'
        /// </summary>
        /// <param name="hbUploadId"></param>
        /// <returns></returns>
        public void validateHBUploadedData(int hbUploadId)
        {
            _uow.HBUploadExceptionRepo.ResetPreviousExceptions(hbUploadId);
            var uploadEntries = _uow.HBUploadFileDataRepo.Find(dt => dt.HbUploadId == hbUploadId && dt.IsExcluded != true);
            foreach (var item in uploadEntries)
            {
                var hbInfoList = _uow.HBInformationRepo.Find(hbi => hbi.HBREF == item.HBRef && hbi.ACTUALENDDATE == null);
                if (hbInfoList.Count() == 0)
                {
                    _uow.HBUploadExceptionRepo.AddHBException(item.HBUploadFileDataId, ApplicationConstants.HBRefMismatchReason);
                }
                else
                {
                    var hbids = hbInfoList.Select(hbi => hbi.HBID);
                    var startDate = item.StartDate.Value.Date;
                    var endDate = item.EndDate.Value.Date;
                    var hbScheduleList = _uow.HBActualScheduleRepo.Find(hbs => hbids.Contains(hbs.HBID)).ToList();
                    hbScheduleList = hbScheduleList.Where(hb => hb.STARTDATE.Date == startDate
                                                && hb.ENDDATE.Value.Date == endDate).ToList();
                                                                               
                    if (hbScheduleList.Count() == 0)
                    {
                        var startResult = _uow.HBActualScheduleRepo.Find(hbs => hbids.Contains(hbs.HBID)).ToList();
                        startResult=startResult.Where(hbs => hbs.STARTDATE.Date == startDate).ToList();
                        if (startResult.Count() == 0)
                        {
                            _uow.HBUploadExceptionRepo.AddHBException(item.HBUploadFileDataId, ApplicationConstants.StartDateIncorrectReason);
                        }

                        var endDateResult = _uow.HBActualScheduleRepo.Find(hbs => hbids.Contains(hbs.HBID)).ToList();
                        endDateResult=endDateResult.Where(hbs => hbs.ENDDATE.Value.Date == endDate).ToList();
                        if (endDateResult.Count() == 0)
                        {
                            _uow.HBUploadExceptionRepo.AddHBException(item.HBUploadFileDataId, ApplicationConstants.EndDateIncorrectReason);
                        }
                    }
                    else
                    {
                        var hbSchedule = hbScheduleList.FirstOrDefault();                        

                        if (hbSchedule.HB != -(item.Amount))
                        {
                            _uow.HBUploadExceptionRepo.AddHBException(item.HBUploadFileDataId, ApplicationConstants.AmountIncorrectReason);                            
                        }
                        else
                        {
                            if (!DBNull.Value.Equals(hbSchedule.JOURNALID ) && hbSchedule.VALIDATED==1)
                            {
                                _uow.HBUploadExceptionRepo.AddHBException(item.HBUploadFileDataId, ApplicationConstants.PaymentExistsReason);
                            }                           
                        }
                        _uow.HBUploadFileDataRepo.UpdateHBrow(item.HBUploadFileDataId, hbSchedule.HBROW);
                    }
                }
            }

            _uow.Save();

        }
        #endregion

        #region Validate Exceptions
        /// <summary>
        /// Validate Exceptions
        /// </summary>
        /// <param name="hbUploadId"></param>
        public ExceptionsListViewModel ValidateExceptions(int hbUploadId)
        {
            validateHBUploadedData(hbUploadId);
            return _uow.HBUploadExceptionRepo.GetHBExceptionsByHBUploadId(hbUploadId); ;
        }
        #endregion

        #region Get Exception Report List
        /// <summary>
        /// Get Exception Report List
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public ExceptionsListViewModel GetExceptionReportList(int hbUploadId)
        {
            return _uow.HBUploadExceptionRepo.GetHBExceptionsByHBUploadId(hbUploadId);
        }
        #endregion

        #region Search Customer
        /// <summary>
        /// Search Customers
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="address"></param>
        /// <param name="postcode"></param>
        /// <returns></returns>
        public CustomerListViewModel SearchCustomer(string firstName, string lastName, string address, string postcode)
        {
            return _uow.HBUploadExceptionRepo.SearchCustomer(firstName,lastName, address, postcode); ;
        }
        #endregion

        #region Get Customer HB Listing
        /// <summary>
        /// Get Customer HB Listing
        /// </summary>
        /// <param name="customerId"></param>
        /// <param name="tenancyId"></param>
        /// <returns></returns>
        public CustomerHBListingViewModel GetCustomerHBListing(int customerId, int tenancyId)
        {
            return _uow.HBInformationRepo.GetCustomerHBListing(customerId,tenancyId); 
        }
        #endregion

        #region Get HB Tenancy Listing By HBref
        /// <summary>
        /// Get HB Tenancy Listing By HBref
        /// </summary>
        /// <param name="hbref"></param>
        /// <returns></returns>
        public HBTenancyListingViewModel GetHbTenancyListingByHBref(int exceptionId)
        {
           var exceptionQuery = _uow.HBUploadExceptionRepo.Find(x => x.HBUploadExceptionId == exceptionId).FirstOrDefault();
           var uploadDataQuery = _uow.HBUploadFileDataRepo.Find(d => d.HBUploadFileDataId == exceptionQuery.HBUploadFileDataId).FirstOrDefault();
           return _uow.HBInformationRepo.GetHbTenancyListingByHBref(uploadDataQuery.HBRef);
        }
        #endregion


        #region Process payment
        /// <summary>
        /// Steps for insertion in F_RentJournal and F_HBACTUALSCHEDULE are followed from existing process
        /// mentioned in classic asp system 
        /// </summary>
        /// <param name="hbUploadId"></param>
        public void processPayment(int hbUploadId)
        {

            var fileEntries = _uow.HBUploadFileDataRepo.GetFileEntriesToBeProcessed(hbUploadId);
            foreach (var fileEntry in fileEntries)
            {
                var journalId = _uow.RentJournalRepo.InsertRentJournal(fileEntry);
                _uow.HBActualScheduleRepo.UpdateJournalId(fileEntry, journalId);

            }

            // SET isProcessed in F_HBUpload table to true
            _uow.HBUploadRepo.ProcessHBFile(hbUploadId);
            //_uow.Save();

        }
        #endregion
		
		public bool UpdateHbReferance(int HBId, int CustomerId, Int64 TenancyId,string HbRef)
        {
            return _uow.HBInformationRepo.UpdateHbReferance(HBId, CustomerId,TenancyId, HbRef);
        }
		
		public bool ResolveException(int HBUploadExceptionId, string HbRef, DateTime newStartDate, DateTime newEndDate, double newHb)
        {
            try
            {
                bool isResolved = false; 
                var exception = _uow.HBUploadExceptionRepo.GetById(HBUploadExceptionId);
                var uploadFileData = _uow.HBUploadFileDataRepo.GetById(exception.HBUploadFileDataId.Value);
                var exceptionReason = _uow.HBExceptionReasonRepo.GetById(exception.ReasonId.Value).Reason;
                switch (exceptionReason)
                {
                    case ApplicationConstants.HBRefMismatch:
                        if (uploadFileData.HBRef == HbRef)
                            isResolved = true;
                        else
                            isResolved = false;
                        break;
                    case ApplicationConstants.StartDateIncorrect:
                        if (uploadFileData.StartDate == newStartDate)
                            isResolved = true;
                        else
                            isResolved = false;
                        break;
                    case ApplicationConstants.EndDateIncorrect:
                        if (uploadFileData.EndDate == newEndDate)
                            isResolved = true;
                        else
                            isResolved = false;
                        break;
                    case ApplicationConstants.AmountIncorrect:
                        if (uploadFileData.Amount == newHb)
                            isResolved = true;
                        else
                            isResolved = false;
                        break;
                }
                return _uow.HBUploadExceptionRepo.UpdataExceptionReport(exception.HBUploadExceptionId, isResolved);
            }
            catch (Exception)
            {
                return false;
            }
        }


        public int SubmitNewHBSchedule(int exceptionId, int customerId, int tenancyId, string HBRef, double initialPayment, DateTime startDate, DateTime endDate)
        {
            try
            {

                var HBId=_uow.HBInformationRepo.SubmitNewHBSchedule(customerId, tenancyId, HBRef, initialPayment, startDate, endDate);
                if (HBId >= 0)
                {
                    _uow.HBActualScheduleRepo.AddNextPayments(HBId, startDate, endDate, initialPayment);
                    ResolveException(exceptionId, HBRef, startDate, endDate, initialPayment);
                    return HBId;
                }
                else
                {
                    return -1;
                }
            }
            catch (Exception ex)
            {

                return -1;
            }
        }

        

    }
}
