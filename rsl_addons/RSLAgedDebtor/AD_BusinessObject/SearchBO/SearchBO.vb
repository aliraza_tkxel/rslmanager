﻿Imports System
Namespace AD_BusinessObject
    Public Class SearchBO

#Region "Attributes"
        Private _salesCustomerName As String
        Private _employeeName As String
        Private _organisationName As String
        Private _isTenantOnlt As Boolean
        Private _sABalance As Decimal
#End Region
#Region "Construtor"
        Public Sub New()
            _salesCustomerName = String.Empty
            _employeeName = String.Empty
            _organisationName = String.Empty
            _isTenantOnlt = False
            _sABalance = 0.0
        End Sub
#End Region

#Region "Properties"
        ' Get / Set property for _SalesCustomerName
        Public Property SalesCustomerName() As String

            Get
                Return _salesCustomerName
            End Get

            Set(ByVal value As String)
                _salesCustomerName = value
            End Set

        End Property

        ' Get / Set property for _organisationName
        Public Property OrganisationName() As String

            Get
                Return _organisationName
            End Get

            Set(ByVal value As String)
                _organisationName = value
            End Set

        End Property

        ' Get / Set property for _employeeName
        Public Property EmployeeName() As String

            Get
                Return _employeeName
            End Get

            Set(ByVal value As String)
                _employeeName = value
            End Set

        End Property
        ' Get / Set property for _isTenantOnlt
        Public Property IsTenantOnlt() As Boolean

            Get
                Return _isTenantOnlt
            End Get

            Set(ByVal value As Boolean)
                _isTenantOnlt = value
            End Set

        End Property

        ' Get / Set property for _sABalance
        Public Property SABalance() As Decimal

            Get
                Return _sABalance
            End Get

            Set(ByVal value As Decimal)
                _sABalance = value
            End Set

        End Property



#End Region
    End Class
End Namespace
