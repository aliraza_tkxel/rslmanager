﻿'' Class Name -- BaseBO
'' Base Class -- System
'' Summary -- All BOs will inherit from this class. Holds common attributes 
'' Methods -- None
Imports System
Namespace AD_BusinessObject
    <Serializable()> _
    Public Class BaseBO

#Region "Attributes"

        Protected _flagStatus As Boolean
        Protected _userMsg As String

        Protected _exceptionGenerated As Boolean
        Protected _exceptionMsg As String


#End Region

#Region "Constructors"

        Public Sub New()

            _flagStatus = False
            _userMsg = String.Empty
            _exceptionGenerated = False
            _exceptionMsg = String.Empty

        End Sub

#End Region

#Region "Properties"

        ' Get/Set property for _flagStatus
        Public Property IsFlagStatus() As Boolean

            Get
                Return _flagStatus
            End Get

            Set(ByVal value As Boolean)
                _flagStatus = value
            End Set

        End Property

        ' Get/Set property for _exceptionGenerated
        Public Property IsExceptionGenerated() As Boolean

            Get
                Return _exceptionGenerated
            End Get

            Set(ByVal value As Boolean)
                _exceptionGenerated = value
            End Set

        End Property


        ' Get/Set property for _exceptionMsg
        Public Property ExceptionMsg() As String

            Get
                Return _exceptionMsg
            End Get

            Set(ByVal value As String)
                _exceptionMsg = value
            End Set

        End Property

        ' Get/Set property for _userMsg
        Public Property UserMsg() As String

            Get
                Return _userMsg
            End Get

            Set(ByVal value As String)
                _userMsg = value
            End Set

        End Property

#End Region

    End Class

End Namespace