﻿Imports System
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports AD_BusinessLogic
Imports AD_Utilities
Imports AD_BusinessObject

Public Class Bridge
    Inherits MSDN.SessionPage
    'Inherits System.Web.UI.Page

    Public uiMessageHelper As UIMessageHelper = New UIMessageHelper()
    Dim accountId As Integer = 0
    Dim accountType As Integer = 0
    Dim classicUserId As Integer = 0
    Dim isReportView As Boolean = False
    Dim isCreatePDF As Boolean = False

#Region "Events"

#Region "Page Load"
    ''' <summary>
    ''' Event fires on page load.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As EventArgs) Handles Me.Load
        Try
            Me.loadUser()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#End Region

#Region "Functions"

#Region "Load User"
    ''' <summary>
    ''' Load User
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub loadUser()

        Dim objUserBo As UserBO = New UserBO()
        Dim objUserBl As UsersBL = New UsersBL()
        Dim resultDataSet As DataSet = New DataSet()
        Me.getQueryStringValues()
        Me.checkClassicAspSession()

        resultDataSet = objUserBl.getEmployeeById(classicUserId)
        'resultDataSet = objUserBl.getEmployeeById(615)

        If (resultDataSet.Tables(0).Rows.Count = 0) Then
            uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.UserDoesNotExist, True)
        Else
            Dim isActive As String = resultDataSet.Tables(0).Rows(0).Item("IsActive").ToString()

            If isActive = False Then
                uiMessageHelper.setMessage(lblMessage, pnlMessage, UserMessageConstants.UsersAccountDeactivated, True)
            Else
                setUserSession(resultDataSet)
                If isCreatePDF = True Then
                    If (accountId > 0 And accountType > 0) Then
                        Response.Redirect(PathConstants.RechargeStatementDoc + "?" + PathConstants.accountId + "=" + Me.accountId.ToString() + "&" + PathConstants.accountType + "=" + Me.accountType.ToString() + "&" + PathConstants.create + "=" + PathConstants.yes, True)
                    End If
                ElseIf (accountId > 0 And accountType > 0) Then
                    Response.Redirect(PathConstants.RechargeStatementDoc + "?" + PathConstants.accountId + "=" + Me.accountId.ToString() + "&" + PathConstants.accountType + "=" + Me.accountType.ToString(), True)
                ElseIf isReportView = True Then
                    Response.Redirect(PathConstants.ReportAreaPath, True)
                End If
                ' Response.Redirect(PathConstants.DashboardPath, True)


            End If

        End If
    End Sub
#End Region

#Region "get Query String Values"

    Private Sub getQueryStringValues()
        If Request.QueryString(PathConstants.create) IsNot Nothing Then
            'the user want to go to report area
            isCreatePDF = IIf(Request.QueryString(PathConstants.create) = PathConstants.yes, True, False)
        End If
        If Request.QueryString(PathConstants.ReportView) IsNot Nothing Then
            'the user want to go to report area
            isReportView = IIf(Request.QueryString(PathConstants.ReportView) = PathConstants.yes, True, False)
        End If
        If Request.QueryString(PathConstants.accountId) IsNot Nothing And Request.QueryString(PathConstants.accountType) IsNot Nothing Then
            'the user want to go to report area
            accountId = Convert.ToInt32(Request.QueryString(PathConstants.accountId))
            accountType = Convert.ToInt32(Request.QueryString(PathConstants.accountType))
        End If
    End Sub
#End Region

#Region "Set User Session"
    ''' <summary>
    ''' Set User Session.
    ''' </summary>
    ''' <param name="resultDataSet"></param>
    ''' <remarks></remarks>
    Private Sub setUserSession(ByRef resultDataSet)

        SessionManager.setAgedDebtorUserId(classicUserId)
        SessionManager.setUserFullName(resultDataSet.Tables(0).Rows(0).Item("FullName").ToString())
        SessionManager.setUserEmployeeId(resultDataSet.Tables(0).Rows(0).Item("EmployeeId").ToString())

    End Sub
#End Region

#Region "Check Classic Asp Session"
    ''' <summary>
    ''' Check Classic Asp Session
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub checkClassicAspSession()

        If ASPSession("USERID") IsNot Nothing Then
            classicUserId = Integer.Parse(ASPSession("USERID").ToString())
        Else
            Me.redirectToLoginPage()
        End If

    End Sub
#End Region

#Region "Redirect To Login Page"
    ''' <summary>
    ''' Redirect To Login Page
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub redirectToLoginPage()
        Response.Redirect(PathConstants.LoginPath, True)
    End Sub
#End Region

#End Region

End Class
