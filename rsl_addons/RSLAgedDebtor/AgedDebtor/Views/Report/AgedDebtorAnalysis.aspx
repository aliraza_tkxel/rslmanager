﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AgedDebtorAnalysis.aspx.vb"
    Inherits="AgedDebtor.AgedDebtorAnalysis" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Broadland Housing Aged Debtors Analysis</title>
    <link href="../../Styles/Site.css" rel="stylesheet" type="text/css" />
    <style type="text/css" media="print">
        @page
        {
            height: 800px;
            border: 1px solid #828282;
            margin-bottom: 20px;
            margin-top: 10px;
            size: Portrait;
        }
        body
        {
            margin: 10px;
        }
    </style>
    <script type="text/javascript">
        function PrintPage() {
            document.getElementById('divSearch').style.display = 'none';
            document.getElementById('divPager').style.display = 'none';
            // document.getElementById('pnlMessage1').style.display = 'none';
            window.print();

        }
        function reappear(inputName, message) {
            if (document.getElementById(inputName).value == "") {
                document.getElementById(inputName).value = message;
            }
        } 
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <b>Broadland Housing Aged Debtors Analysis</b>
    <asp:UpdatePanel ID="updPanelSearch" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="divSearch">
                <table style="width: 1020px; margin-top: 10px;" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 20%">
                        </td>
                        <td style="width: 15%">
                        </td>
                        <td style="width: 15%">
                        </td>
                        <td style="width: 15%">
                        </td>
                        <td style="width: 15%">
                        </td>
                        <td style="width: 15%">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="txtCustomerName" Width="100%" runat="server" CssClass="textbox200"
                                Text="" onclick=" this.value=''" onBlur="reappear(this.id,'Search customer');"></asp:TextBox>
                        </td>
                        <td colspan="5" style="text-align: left; padding-left: 10px;" align="left">
                            <b>Tenants only</b>
                            <asp:CheckBox ID="chkTenantOnly" runat="server" class="checkbox200" />
                        </td>
                    </tr>
                    <tr style="height: 10px;">
                        <td colspan="6">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">
                            <b>Sales Acc Balance: > £ &nbsp;</b>
                        </td>
                        <td>
                            <asp:TextBox ID="txtAccBalance" runat="server" Width="95%" Text="0.00"></asp:TextBox></b>&nbsp;
                        </td>
                        <td>
                            <asp:Button ID="btnUpdate" runat="server" Width="80%" Height="20px" Text="Update Results"
                                class="RSLButton" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td colspan="2">
                            <asp:Button ID="btnDetailedReport" runat="server" Height="20px" Text="View Detailed Report"
                                class="RSLButton" />&nbsp;
                            <asp:Button ID="btnPrint" runat="server" Text="Print" Height="20px" class="RSLButton"
                                OnClientClick="javascript:PrintPage();" />
                        </td>
                    </tr>
                    <%--<table style="width: 920px; margin-top: 5px;" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="width: 100px">
                            <b>Sales Acc Balance: >£
                        </td>
                        <td style="width: 100px">
                            <asp:TextBox ID="txtAccBalance" runat="server" Width="100px"></asp:TextBox></b>
                        </td>
                        <td style="width: 100px" align="left">
                            <asp:Button ID="btnUpdate" runat="server" Width="100px" Text="Update Results" class="RSLButton" />
                        </td>
                        <td class="checkbox200" style="width: 300px">
                            <asp:Button ID="btnDetailedReport" runat="server" Text="View Detailed Report" class="RSLButton" />
                            <asp:Button ID="btnJob" runat="server" Text="Execute Job" class="RSLButton" />
                            <asp:Button ID="btnPrint" runat="server" Text="Print" class="RSLButton" OnClientClick="javascript:PrintPage();" />
                        </td>
                    </tr>
                </table>--%>
                </table>
                <asp:Panel ID="pnlMessage1" runat="server" Visible="False">
                    <asp:Label ID="lblMessage1" runat="server"></asp:Label>
                </asp:Panel>
            </div>
            <div style='border: 1px solid #b1b1b1; margin-top: 10px; width: 1020px;' id="divCustomerSummary"
                runat="server">
                <asp:GridView ID="rptCustomerSummary" runat="server" AutoGenerateColumns="false"
                    Width="1020px">
                    <Columns>
                        <asp:BoundField HeaderText="Customer" DataField="CONTACT" />
                        <asp:BoundField HeaderText="Total" DataField="Balance" DataFormatString="{0:c}" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField HeaderText="Current" DataField="CURRENTBALANCE" DataFormatString="{0:c}"
                            ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField HeaderText="30 to 60 days" DataField="BALANCE60DAYS" DataFormatString="{0:c}"
                            ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField HeaderText="60 to 90 days" DataField="BALANCE90DAYS" DataFormatString="{0:c}"
                            ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField HeaderText="90 to 120 days" DataField="BALANCE120DAYS" DataFormatString="{0:c}"
                            ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField HeaderText="Older" DataField="OLDERBALANCE" DataFormatString="{0:c}"
                            ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField DataField="ACCOUNTID" Visible="false"  />
                    </Columns>
                    <FooterStyle></FooterStyle>
                </asp:GridView>
                <%-- 
                <asp:Repeater ID="rptCustomerSummary" runat="server">
                    <HeaderTemplate>
                        <table width="920px" id="TblrepeaterHeader" runat="server">
                            <tr>
                                <td align="left" style='width: 250px;'>
                                    <b>Customer</b>
                                </td>
                                <td align="right" style='width: 80px;'>
                                    <b>Total </b>
                                </td>
                                <td align="right" style='width: 80px;'>
                                    <b>Current </b>
                                </td>
                                <td align="right" style='width: 150px;'>
                                    <b>30-60 days </b>
                                </td>
                                <td align="right" style='width: 150px;'>
                                    <b>60-90 days </b>
                                </td>
                                <td align="right" style='width: 150px;'>
                                    <b>90-120 days </b>
                                </td>
                                <td align="right" style='width: 80px;'>
                                    <b>Older </b>
                                </td>
                                <td align="left" style='width: 0px;'>
                                </td>
                            </tr>
                        </table>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <table width="920px" id="TblSummaryrepeater" runat="server">
                            <tr class="repeaterHeader">
                                <td align="left" style='width: 250px;'>
                                    <%# CheckOrganization(DataBinder.Eval(Container.DataItem, "ORGANIZATION"))%>
                                    </b>
                                </td>
                                <td align="right" style='width: 80px;'>
                                    £<%=Math.Round(totalBalance, 2).ToString()%>
                                </td>
                                <td align="right" style='text-align: right; width: 80px;'>
                                    £<%=Math.Round(totalCurrent, 2).ToString()%>
                                </td>
                                <td align="right" style='text-align: right; width: 150px;'>
                                    £<%=Math.Round(totalThirtyBalance, 2).ToString()%>
                                </td>
                                <td align="right" style='text-align: right; width: 150px;'>
                                    £<%=Math.Round(totalSixtyBalance, 2).ToString()%>
                                </td>
                                <td align="right" style='text-align: right; width: 150px;'>
                                    £<%=Math.Round(totalNintyBalance, 2).ToString()%>
                                </td>
                                <td align="right" style='text-align: right; width: 80px;'>
                                    £<%=Math.Round(totalOlderBalance, 2).ToString()%>
                                </td>
                                <td align="left" style='width: 0px;'>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:Repeater>
                <table width="920px" id="tblGrandSummary" runat="server">
                    <tr>
                        <td style='width: 80px;'>
                            &nbsp;
                        </td>
                        <td style='width: 80px;'>
                            &nbsp;
                        </td>
                        <td style='width: 200px;'>
                            &nbsp;
                        </td>
                        <td style='width: 80px;'>
                            &nbsp;
                        </td>
                        <td style='width: 80px;'>
                            &nbsp;
                        </td>
                        <td style='width: 80px;'>
                            &nbsp;
                        </td>
                        <td style='width: 65px;'>
                            &nbsp;
                        </td>
                        <td style='width: 65px;'>
                            &nbsp;
                        </td>
                        <td style='width: 65px;'>
                            &nbsp;
                        </td>
                        <td style='width: 80px;'>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" align="right" style='padding-right: 20px;'>
                            <b>Sales Acc Balance Totals: £<%=Math.Round(TotalSalesAccountBalance, 2).ToString()%></b>
                        </td>
                    </tr>
                </table>
                <asp:Repeater ID="rptCustomerDetail" runat="server">
                    <ItemTemplate>
                        <table width="920px" id="Tblrepeater" runat="server">
                            <tr>
                                <td align="left" style='width: 100px;'>
                                    <b>Customer: </b>
                                </td>
                                <td align="left" style='width: 100px;'>
                                    <b>Total: </b>
                                </td>
                                <td align="left" style='width: 100px;'>
                                    <b>Current: </b>
                                </td>
                                <td align="left" style='width: 200px;'>
                                    <b>30 to 60 days: </b>
                                </td>
                                <td align="left" style='width: 200px;'>
                                    <b>60 to 90 days: </b>
                                </td>
                                <td align="left" style='width: 200px;'>
                                    <b>90 to 120 days: </b>
                                </td>
                                <td align="left" style='width: 200px;'>
                                    <b>Older: </b>
                                </td>
                                <td align="right" style='width: 200px;'>
                                    <b>Sales Acc Balance: (£ </b>
                                </td>
                            </tr>
                            <tr class="repeaterHeader">
                                <td align="left" style='width: 100px;'>
                                    <%# DataBinder.Eval(Container.DataItem, "CustName")%>
                                </td>
                                <td align="left" style='width: 100px;'>
                                </td>
                                <td align="left" style='width: 100px;'>
                                    <%# DataBinder.Eval(Container.DataItem, "Current")%>
                                </td>
                                <td align="left" style='width: 200px;'>
                                    <%# DataBinder.Eval(Container.DataItem, "30 to 60 days")%>
                                </td>
                                <td align="left" style='width: 200px;'>
                                    <%# DataBinder.Eval(Container.DataItem, "60 to 90 days")%>
                                </td>
                                <td align="left" style='width: 200px;'>
                                    <%# DataBinder.Eval(Container.DataItem, "90 to 120 days")%>
                                </td>
                                <td align="left" style='width: 200px;'>
                                    <%# DataBinder.Eval(Container.DataItem, "Older")%>
                                </td>
                                <td align="right" style='width: 200px;'>
                                    <b>Sales Acc Balance: (£ </b>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:Repeater>
                <table width="920px" id="tableGrand" runat="server">
                    <tr>
                        <td style='width: 80px;'>
                            &nbsp;
                        </td>
                        <td style='width: 80px;'>
                            &nbsp;
                        </td>
                        <td style='width: 200px;'>
                            &nbsp;
                        </td>
                        <td style='width: 80px;'>
                            &nbsp;
                        </td>
                        <td style='width: 80px;'>
                            &nbsp;
                        </td>
                        <td style='width: 80px;'>
                            &nbsp;
                        </td>
                        <td style='width: 65px;'>
                            &nbsp;
                        </td>
                        <td style='width: 65px;'>
                            &nbsp;
                        </td>
                        <td style='width: 65px;'>
                            &nbsp;
                        </td>
                        <td style='width: 80px;'>
                            &nbsp;
                        </td>
                    </tr>
                </table>--%>
            </div>
            <div style='border: 1px solid #b1b1b1; margin-top: 10px; width: 1020px; padding: 10px 10px 10px 10px'
                id="divRepeater" runat="server">
                <asp:Repeater ID="rptCustomerDetail" runat="server">
                    <HeaderTemplate>
                        <table width="1020px" id="TblrepeaterDetailHeader" runat="server">
                            <tr>
                                <td align="left" style='width: 250px;'>
                                    <b>Customer</b>
                                </td>
                                <td align="left" style='width: 80px;'>
                                    <b>Reference </b>
                                </td>
                                <td align="right" style='width: 80px;'>
                                    <b>Total </b>
                                </td>
                                <td align="right" style='width: 80px;'>
                                    <b>Current </b>
                                </td>
                                <td align="right" style='width: 150px;'>
                                    <b>30-60 days </b>
                                </td>
                                <td align="right" style='width: 150px;'>
                                    <b>60-90 days </b>
                                </td>
                                <td align="right" style='width: 150px;'>
                                    <b>90-120 days </b>
                                </td>
                                <td align="right" style='width: 80px;'>
                                    <b>Older </b>
                                </td>
                                <%--<td align="left" style='width: 0px;'>
                                </td>
                                <td align="right" style='width: 200px;'>
                                    <b>Sales Acc Balance: (£ </b>
                                </td>--%>
                            </tr>
                        </table>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <table width="1020px" id="Tblrepeater" runat="server">
                            <tr class="repeaterHeader">
                                <td align="left" colspan="8">
                                    <%# CheckOrganization(DataBinder.Eval(Container.DataItem, "ORGANIZATION"))%>
                                    </b>
                                </td>
                                <%--<td align="right" style='width: 200px;'>
                                    <%# Math.Round(Convert.ToDecimal(CheckBalance(DataBinder.Eval(Container.DataItem, "BALANCE"))), 2).ToString()%>)
                                </td>--%>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:Repeater>
                <table width="1020px" id="tableGrand" runat="server">
                    <tr>
                        <td style='width: 250px;'>
                            &nbsp;
                        </td>
                        <td style='width: 80px;'>
                            &nbsp;
                        </td>
                        <td style='width: 80px;'>
                            &nbsp;
                        </td>
                        <td style='width: 80px;'>
                            &nbsp;
                        </td>
                        <td style='width: 150px;'>
                            &nbsp;
                        </td>
                        <td style='width: 150px;'>
                            &nbsp;
                        </td>
                        <td style='width: 150px;'>
                            &nbsp;
                        </td>
                        <td style='width: 80px;'>
                            &nbsp;
                        </td>
                    </tr>
                    <tr style='border-top-style: solid; border-bottom-style: solid; border-top-width: 1px;
                        border-bottom-width: 1px; border-top-color: #C0C0C0; border-bottom-color: #C0C0C0;'>
                        <td colspan="2" align="right">
                            <b>Grand Totals:
                        </td>
                        <td align="right" style='text-align: right;'>
                            £<%=Math.Round(GrandTotalBalance, 2).ToString()%></b>
                        </td>
                        <td align="right" style='text-align: right;'>
                            £<%=Math.Round(GrandTotalCurrent, 2).ToString()%>
                        </td>
                        <td align="right" style='text-align: right;'>
                            £<%=Math.Round(GrandTotalThirtyBalance, 2).ToString()%>
                        </td>
                        <td align="right" style='text-align: right;'>
                            £<%=Math.Round(GrandTotalSixtyBalance, 2).ToString()%>
                        </td>
                        <td align="right" style='text-align: right;'>
                            £<%=Math.Round(GrandTotalNintyBalance, 2).ToString()%>
                        </td>
                        <td align="right" style='text-align: right;'>
                            £<%=Math.Round(GrandTotalOlderBalance, 2).ToString()%>
                        </td>
                    </tr>
                    <%-- <tr>
                        <td colspan="5" align="right" style='padding-right: 20px;'>
                            <b>Sales Acc Balance Totals: £<%=Math.Round(TotalSalesAccountBalance, 2).ToString()%></b>
                        </td>
                    </tr>--%>
                </table>
            </div>
            <div style='width: 1020px; float: left; margin-top: 20px;' id="divPager" runat="server">
                <div style='width: 80%; float: left; font-style: italic; text-align: center;'>
                    &nbsp;
                    <asp:Repeater ID="rptPager" runat="server">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkPage" runat="server" Text='<%#Eval("Text") %>' CommandArgument='<%# Eval("Value") %>'
                                Enabled='<%# Eval("Enabled") %>' OnClick="Page_Changed" Style='text-decoration: none;
                                color: #000000;'></asp:LinkButton>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                <div style='width: 20%; float: left; font-style: italic; text-align: right;'>
                    Results per page:<asp:TextBox ID="txtResults" runat="server" Width="50" Text="25"
                        CssClass="textbox200"></asp:TextBox>
                </div>
                <%-- <div style='width: 1020px; float: left; margin-top: 20px;'>
                    <div style='width: 80%; float: left; font-style: italic; text-align: Left;'>
                        Sql job Execution Time:
                        <asp:Literal ID="lblExecutionTime" runat="server"></asp:Literal>
                    </div>
                    <div style='width: 20%; float: left; text-align: right;'>
                    </div>
                </div>--%>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="updateProgressReportsArea" runat="server" DisplayAfter="10">
        <ProgressTemplate>
            <div class="loading-image">
                <img alt="Please Wait" src="../../Images/progress.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    </form>
</body>
</html>
