﻿Imports AD_Utilities
Imports AD_BusinessLogic
Imports AD_BusinessObject
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports System.Drawing
Imports System.IO
Imports System.Web.UI.HtmlControls


Public Class AgedDebtorAnalysis
    Inherits PageBase

#Region "Properties"
    'Dim dsAccountDetail As DataSet = New DataSet()
    Public dsAccountDetail As New DataSet()
    Public tempgrid As GridView
    Public totalBalance As Decimal
    Public totalCurrent As Decimal
    Public totalThirtyBalance As Decimal
    Public totalSixtyBalance As Decimal
    Public totalNintyBalance As Decimal
    Public totalOlderBalance As Decimal

    Public GrandTotalBalance As Decimal
    Public GrandTotalCurrent As Decimal
    Public GrandTotalThirtyBalance As Decimal
    Public GrandTotalSixtyBalance As Decimal
    Public GrandTotalNintyBalance As Decimal
    Public GrandTotalOlderBalance As Decimal
    Public TotalSalesAccountBalance As Decimal
    Dim totalCount As Integer = 0

    Private PageSize As Integer = 25
    Dim objPageSortBo As PageSortBO = New PageSortBO()

    Dim requestPath As String = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority)
    Dim browserAgent As String

#End Region

#Region "Events"

#Region "Page Load Event"
    ''' <summary>
    ''' Page Load Event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If Not IsPostBack Then
                'MA Me.GetSqlJobExecTime()
                divCustomerSummary.Style.Add("display", "none")
                divRepeater.Style.Add("display", "none")
                'MA' tableGrand.Style.Add("display", "none")
                divPager.Style.Add("display", "none")
            End If
            pnlMessage1.Visible = False
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage1, pnlMessage1, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "Update Button click event"
    ''' <summary>
    ''' Update Button click event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As EventArgs) Handles btnUpdate.Click
        Try
            If (btnDetailedReport.Text = "View Detailed Report") Then
                divRepeater.Style.Add("display", "none")
                divCustomerSummary.Style.Add("display", "block")
                Me.SearchSalesAccountSummary()
                'Me.SearchSalesAccountDetail()
            ElseIf (btnDetailedReport.Text = "View Summary Report") Then
                divRepeater.Style.Add("display", "block")
                divCustomerSummary.Style.Add("display", "none")
                Me.SearchSalesAccountDetail()
            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage1, pnlMessage1, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region

#Region "RptCustomerDetail ItemDataBound event"

    ''' <summary>
    ''' RptCustomerDetail ItemDataBound event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>

    Protected Sub rptCustomerDetail_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptCustomerDetail.ItemDataBound
        If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim tb As HtmlTable = e.Item.FindControl("Tblrepeater")
            Dim gridBGColor As String

            Dim id As Int32 = CType(DataBinder.Eval(e.Item.DataItem, "ACCOUNTID").ToString(), Int32)
            TotalSalesAccountBalance += CType(CheckBalance(DataBinder.Eval(e.Item.DataItem, "BALANCE")), Decimal)

            'Filter the data according to ACCOUNTID
            Dim filteredSalesDt As DataTable = New DataTable()
            Dim SalesResult = (From ps In dsAccountDetail.Tables("SalesDetail") Where ps.Item("ACCOUNTID").ToString() = id.ToString() Select ps)
            Dim SalesDv As DataView = New DataView()
            SalesDv = dsAccountDetail.Tables("SalesDetail").AsDataView()
            SalesDv.RowFilter = "ACCOUNTID =  " + id.ToString()
            filteredSalesDt = SalesDv.ToTable()
            If filteredSalesDt.Rows.Count > 0 Then
                If e.Item.ItemIndex Mod 2 = 0 Then
                    tb.Style(HtmlTextWriterStyle.BackgroundColor) = "White"
                    gridBGColor = "White"
                Else
                    tb.Style(HtmlTextWriterStyle.BackgroundColor) = "#F5F5F5"
                    gridBGColor = "#F5F5F5"
                End If
                totalBalance = 0
                totalCurrent = 0
                totalNintyBalance = 0
                totalOlderBalance = 0
                totalSixtyBalance = 0
                totalThirtyBalance = 0

                tempgrid = New GridView()
                'view.EmptyDataText = "No data found"
                tempgrid.ShowHeaderWhenEmpty = False
                tempgrid.BorderStyle = BorderStyle.None
                'MA' tempgrid.HeaderStyle.CssClass = "tempgridHeader"
                tempgrid.BackColor = Color.FromName(gridBGColor)
                tempgrid.ShowFooter = True
                tempgrid.GridLines = GridLines.None
                AddHandler tempgrid.RowDataBound, AddressOf tempGrid_RowDataBound
                tempgrid.DataSource = filteredSalesDt
                tempgrid.DataBind()
                'Set grid style
                tempgrid.FooterRow.Style.Add("text-align", "right")
                tempgrid.HeaderRow.Style.Add("text-align", "left")
                tempgrid.Style.Add("Width", "1020px")
                tempgrid.Attributes.Add("Border", "0")
                tempgrid.Attributes.Add("rules", "None")
                tempgrid.Attributes.Add("AutoGenerateColumns", "False")
                tempgrid.Attributes.Add("HeaderStyle-HorizontalAlign", "right")
                tempgrid.HeaderRow.Visible = False

                tempgrid.HeaderRow.Cells(2).HorizontalAlign = HorizontalAlign.Right
                tempgrid.HeaderRow.Cells(3).HorizontalAlign = HorizontalAlign.Right
                tempgrid.HeaderRow.Cells(4).HorizontalAlign = HorizontalAlign.Right
                tempgrid.HeaderRow.Cells(5).HorizontalAlign = HorizontalAlign.Right
                tempgrid.HeaderRow.Cells(6).HorizontalAlign = HorizontalAlign.Right
                tempgrid.HeaderRow.Cells(7).HorizontalAlign = HorizontalAlign.Right

                'Set Grid View Row Style
                For Each gvr As GridViewRow In tempgrid.Rows
                    gvr.Cells(0).Style.Add("colspan", "2") 'Cust Name
                    gvr.Cells(0).Style.Add("width", "330px") 'Cust Name
                    gvr.Cells(0).Style.Add("text-align", "right")
                    gvr.Cells(1).Visible = False 'SI
                    'gvr.Cells(1).Style.Add("width", "80px") 'SI
                    'gvr.Cells(1).Style.Add("text-align", "right")
                    gvr.Cells(2).Style.Add("width", "80px") 'Total
                    gvr.Cells(3).Style.Add("width", "80px") 'Current
                    gvr.Cells(4).Style.Add("width", "150px") '30-60days
                    gvr.Cells(5).Style.Add("width", "150px") '60-90days
                    gvr.Cells(6).Style.Add("width", "150px") '90-120days
                    gvr.Cells(7).Style.Add("width", "80px") 'Older
                    'gvr.Cells(8).Style.Add("width", "0px")
                    'gvr.Cells(8).Style.Add("display", "none")
                    gvr.Cells(8).Visible = False 'AccountId
                    gvr.Cells(9).Visible = False 'Sort Order

                    gvr.Cells(2).Style.Add("text-align", "Right")
                    gvr.Cells(3).Style.Add("text-align", "Right")
                    gvr.Cells(4).Style.Add("text-align", "Right")
                    gvr.Cells(5).Style.Add("text-align", "Right")
                    gvr.Cells(6).Style.Add("text-align", "Right")
                    gvr.Cells(7).Style.Add("text-align", "Right")

                Next

                ''set grid view footer style
                tempgrid.FooterRow.Cells(1).Visible = False 'AccountId
                tempgrid.FooterRow.Cells(8).Visible = False 'AccountId
                tempgrid.FooterRow.Cells(9).Visible = False 'Sort Order
                'tempgrid.FooterRow.Cells(3).Style.Add("border-top", "1px solid #C0C0C0")
                'tempgrid.FooterRow.Cells(4).Style.Add("border-top", "1px solid #C0C0C0")
                'tempgrid.FooterRow.Cells(5).Style.Add("border-top", "1px solid #C0C0C0")
                'tempgrid.FooterRow.Cells(6).Style.Add("border-top", "1px solid #C0C0C0")
                'tempgrid.FooterRow.Cells(7).Style.Add("border-top", "1px solid #C0C0C0")
                'tempgrid.FooterRow.Cells(8).Style.Add("border-top", "1px solid #C0C0C0")
                'tempgrid.FooterRow.Cells(9).Style.Add("border-top", "1px solid #C0C0C0")
                'tempgrid.FooterRow.Cells(10).Style.Add("border-top", "1px solid #C0C0C0")

                e.Item.Controls.Add(tempgrid)
            End If

        End If

    End Sub
#End Region

#Region "RptCustomerSummary ItemDataBound event"

    ''' <summary>
    ''' RptCustomerSummary ItemDataBound event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>

    Protected Sub rptCustomerSummary_RowDataBound(sender As Object, e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles rptCustomerSummary.RowDataBound
        Try

            If e.Row.RowType = DataControlRowType.DataRow Then
                Dim SalesDv As DataView = New DataView()
                Dim filteredSalesDt As DataTable = New DataTable()
                'Dim gridBGColor As String
                Dim id, index As Int32
                Dim dr As DataRow

                'id = CType(DataBinder.Eval(e.Item.DataItem, "ACCOUNTID").ToString(), Int32)

                dr = DirectCast(e.Row.DataItem, DataRowView).Row
                id = dr("ACCOUNTID")

                If (IsDBNull(id) Or id <= 0) Then
                    Return
                End If

                'id = CType(e.Row.Cells(7).Text.ToString(), Int32)
                'TotalSalesAccountBalance += CType(CheckBalance(DataBinder.Eval(e.Item.DataItem, "BALANCE")), Decimal)

                'Filter the data according to ACCOUNTID
                SalesDv = dsAccountDetail.Tables("SalesDetail").AsDataView()
                SalesDv.RowFilter = "ACCOUNTID =  " + id.ToString()
                filteredSalesDt = SalesDv.ToTable()

                totalBalance = 0
                totalCurrent = 0
                totalNintyBalance = 0
                totalOlderBalance = 0
                totalSixtyBalance = 0
                totalThirtyBalance = 0

                If filteredSalesDt.Rows.Count > 0 Then
                    'If e.Item.ItemIndex Mod 2 = 0 Then
                    '    tb.Style(HtmlTextWriterStyle.BackgroundColor) = "White"
                    '    gridBGColor = "White"
                    'Else
                    '    tb.Style(HtmlTextWriterStyle.BackgroundColor) = "#F5F5F5"
                    '    gridBGColor = "#F5F5F5"
                    'End If

                    For index = 0 To filteredSalesDt.Rows.Count - 1 Step 1
                        If (Not IsDBNull(filteredSalesDt.Rows(index)("BALANCE")) And
                           filteredSalesDt.Rows(index)("BALANCE").ToString() <> "") Then
                            totalBalance += Convert.ToDecimal(filteredSalesDt.Rows(index)("BALANCE").ToString())
                        End If

                        If (Not IsDBNull(filteredSalesDt.Rows(index)("CURRENTBALANCE")) And
                           filteredSalesDt.Rows(index)("CURRENTBALANCE").ToString() <> "") Then
                            totalCurrent += Convert.ToDecimal(filteredSalesDt.Rows(index)("CURRENTBALANCE").ToString())
                        End If

                        If (Not IsDBNull(filteredSalesDt.Rows(index)("BALANCE60DAYS")) And
                           filteredSalesDt.Rows(index)("BALANCE60DAYS").ToString() <> "") Then
                            totalThirtyBalance += Convert.ToDecimal(filteredSalesDt.Rows(index)("BALANCE60DAYS").ToString())
                        End If

                        If (Not IsDBNull(filteredSalesDt.Rows(index)("BALANCE90DAYS")) And
                           filteredSalesDt.Rows(index)("BALANCE90DAYS").ToString() <> "") Then
                            totalSixtyBalance += Convert.ToDecimal(filteredSalesDt.Rows(index)("BALANCE90DAYS").ToString())
                        End If


                        If (Not IsDBNull(filteredSalesDt.Rows(index)("BALANCE120DAYS")) And
                           filteredSalesDt.Rows(index)("BALANCE120DAYS").ToString() <> "") Then
                            totalNintyBalance += Convert.ToDecimal(filteredSalesDt.Rows(index)("BALANCE120DAYS").ToString())
                        End If

                        If (Not IsDBNull(filteredSalesDt.Rows(index)("OLDERBALANCE")) And
                           filteredSalesDt.Rows(index)("OLDERBALANCE").ToString() <> "") Then
                            totalOlderBalance += Convert.ToDecimal(filteredSalesDt.Rows(index)("OLDERBALANCE").ToString())
                        End If

                    Next

                    e.Row.Cells(1).Text = "£" + Math.Round(totalBalance, 2).ToString()
                    e.Row.Cells(2).Text = "£" + Math.Round(totalCurrent, 2).ToString()
                    e.Row.Cells(3).Text = "£" + Math.Round(totalThirtyBalance, 2).ToString()
                    e.Row.Cells(4).Text = "£" + Math.Round(totalSixtyBalance, 2).ToString()
                    e.Row.Cells(5).Text = "£" + Math.Round(totalNintyBalance, 2).ToString()
                    e.Row.Cells(6).Text = "£" + Math.Round(totalOlderBalance, 2).ToString()
                End If

            End If
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage1, pnlMessage1, uiMessageHelper.message, True)
            End If
        End Try
    End Sub


    'Protected Sub rptCustomerSummary_ItemDataBound(sender As Object, e As System.Web.UI.WebControls.RepeaterItemEventArgs) Handles rptCustomerSummary.ItemDataBound
    '    Try

    '        If e.Item.ItemType = ListItemType.Item OrElse e.Item.ItemType = ListItemType.AlternatingItem Then
    '            Dim tb As HtmlTable = e.Item.FindControl("TblSummaryrepeater")
    '            Dim SalesDv As DataView = New DataView()
    '            Dim filteredSalesDt As DataTable = New DataTable()
    '            Dim gridBGColor As String
    '            Dim id, index As Int32

    '            id = CType(DataBinder.Eval(e.Item.DataItem, "ACCOUNTID").ToString(), Int32)
    '            TotalSalesAccountBalance += CType(CheckBalance(DataBinder.Eval(e.Item.DataItem, "BALANCE")), Decimal)

    '            'Filter the data according to ACCOUNTID
    '            SalesDv = dsAccountDetail.Tables("SalesDetail").AsDataView()
    '            SalesDv.RowFilter = "ACCOUNTID =  " + id.ToString()
    '            filteredSalesDt = SalesDv.ToTable()

    '            totalBalance = 0
    '            totalCurrent = 0
    '            totalNintyBalance = 0
    '            totalOlderBalance = 0
    '            totalSixtyBalance = 0
    '            totalThirtyBalance = 0

    '            If filteredSalesDt.Rows.Count > 0 Then
    '                If e.Item.ItemIndex Mod 2 = 0 Then
    '                    tb.Style(HtmlTextWriterStyle.BackgroundColor) = "White"
    '                    gridBGColor = "White"
    '                Else
    '                    tb.Style(HtmlTextWriterStyle.BackgroundColor) = "#F5F5F5"
    '                    gridBGColor = "#F5F5F5"
    '                End If

    '                For index = 0 To filteredSalesDt.Rows.Count - 1 Step 1
    '                    If (Not IsDBNull(filteredSalesDt.Rows(index)("BALANCE")) And
    '                       filteredSalesDt.Rows(index)("BALANCE").ToString() <> "") Then
    '                        totalBalance += Convert.ToDecimal(filteredSalesDt.Rows(index)("BALANCE").ToString())
    '                    End If

    '                    If (Not IsDBNull(filteredSalesDt.Rows(index)("CURRENTBALANCE")) And
    '                       filteredSalesDt.Rows(index)("CURRENTBALANCE").ToString() <> "") Then
    '                        totalCurrent += Convert.ToDecimal(filteredSalesDt.Rows(index)("CURRENTBALANCE").ToString())
    '                    End If

    '                    If (Not IsDBNull(filteredSalesDt.Rows(index)("BALANCE60DAYS")) And
    '                       filteredSalesDt.Rows(index)("BALANCE60DAYS").ToString() <> "") Then
    '                        totalThirtyBalance += Convert.ToDecimal(filteredSalesDt.Rows(index)("BALANCE60DAYS").ToString())
    '                    End If

    '                    If (Not IsDBNull(filteredSalesDt.Rows(index)("BALANCE90DAYS")) And
    '                       filteredSalesDt.Rows(index)("BALANCE90DAYS").ToString() <> "") Then
    '                        totalSixtyBalance += Convert.ToDecimal(filteredSalesDt.Rows(index)("BALANCE90DAYS").ToString())
    '                    End If


    '                    If (Not IsDBNull(filteredSalesDt.Rows(index)("BALANCE120DAYS")) And
    '                       filteredSalesDt.Rows(index)("BALANCE120DAYS").ToString() <> "") Then
    '                        totalNintyBalance += Convert.ToDecimal(filteredSalesDt.Rows(index)("BALANCE120DAYS").ToString())
    '                    End If

    '                    If (Not IsDBNull(filteredSalesDt.Rows(index)("OLDERBALANCE")) And
    '                       filteredSalesDt.Rows(index)("OLDERBALANCE").ToString() <> "") Then
    '                        totalOlderBalance += Convert.ToDecimal(filteredSalesDt.Rows(index)("OLDERBALANCE").ToString())
    '                    End If

    '                Next

    '                ''e.Cells(2).Text = "£" + totalBalance.ToString()

    '                'tempgrid = New GridView()
    '                ''view.EmptyDataText = "No data found"
    '                'tempgrid.ShowHeaderWhenEmpty = False
    '                'tempgrid.BorderStyle = BorderStyle.None
    '                ''MA' tempgrid.HeaderStyle.CssClass = "tempgridHeader"
    '                'tempgrid.BackColor = Color.FromName(gridBGColor)
    '                'tempgrid.ShowFooter = True
    '                'tempgrid.GridLines = GridLines.None
    '                'AddHandler tempgrid.RowDataBound, AddressOf tempGrid_RowDataBoundSummary
    '                'tempgrid.DataSource = filteredSalesDt
    '                'tempgrid.DataBind()
    '                ''Set grid style
    '                'tempgrid.FooterRow.Style.Add("text-align", "right")
    '                'tempgrid.HeaderRow.Style.Add("text-align", "left")
    '                'tempgrid.Style.Add("Width", "920px")
    '                'tempgrid.Attributes.Add("Border", "0")
    '                'tempgrid.Attributes.Add("rules", "None")
    '                'tempgrid.Attributes.Add("AutoGenerateColumns", "False")
    '                'tempgrid.Attributes.Add("HeaderStyle-HorizontalAlign", "right")
    '                'tempgrid.HeaderRow.Visible = False

    '                ''set Grid View Header Text and header visibility
    '                ''tempgrid.HeaderRow.Cells(0).Text = ""
    '                ''tempgrid.HeaderRow.Cells(0).Visible = False
    '                ''tempgrid.HeaderRow.Cells(1).Text = ""
    '                ' ''MA' tempgrid.HeaderRow.Cells(2).Text = "Item name:"
    '                ' ''MA' tempgrid.HeaderRow.Cells(3).Text = "Ten ID:"
    '                ''tempgrid.HeaderRow.Cells(2).Visible = False
    '                ''tempgrid.HeaderRow.Cells(3).Visible = False
    '                ''tempgrid.HeaderRow.Cells(4).Visible = False
    '                ''tempgrid.HeaderRow.Cells(5).Text = "Balance:"
    '                ''tempgrid.HeaderRow.Cells(6).Text = "Current:"
    '                ''tempgrid.HeaderRow.Cells(7).Text = "30-60 days:"
    '                ''tempgrid.HeaderRow.Cells(8).Text = "60-90 days:"
    '                ''tempgrid.HeaderRow.Cells(9).Text = "90-120 days:"
    '                ''tempgrid.HeaderRow.Cells(10).Text = "Older:"
    '                'tempgrid.HeaderRow.Cells(2).HorizontalAlign = HorizontalAlign.Right
    '                'tempgrid.HeaderRow.Cells(3).HorizontalAlign = HorizontalAlign.Right
    '                'tempgrid.HeaderRow.Cells(4).HorizontalAlign = HorizontalAlign.Right
    '                'tempgrid.HeaderRow.Cells(5).HorizontalAlign = HorizontalAlign.Right
    '                'tempgrid.HeaderRow.Cells(6).HorizontalAlign = HorizontalAlign.Right
    '                'tempgrid.HeaderRow.Cells(7).HorizontalAlign = HorizontalAlign.Right

    '                ''Set Grid View Row Style
    '                'For Each gvr As GridViewRow In tempgrid.Rows
    '                '    gvr.Cells(0).Style.Add("width", "250px") 'Cust Name
    '                '    gvr.Cells(1).Style.Add("width", "80px") 'SI
    '                '    gvr.Cells(2).Style.Add("width", "80px") 'Total
    '                '    gvr.Cells(3).Style.Add("width", "80px") 'Current
    '                '    gvr.Cells(4).Style.Add("width", "150px") '30-60days
    '                '    gvr.Cells(5).Style.Add("width", "150px") '60-90days
    '                '    gvr.Cells(6).Style.Add("width", "150px") '90-120days
    '                '    gvr.Cells(7).Style.Add("width", "80px") 'Older
    '                '    gvr.Cells(8).Style.Add("width", "0px")
    '                '    gvr.Cells(8).Style.Add("display", "none")
    '                '    gvr.Cells(8).Visible = False 'AccountId
    '                '    'gvr.Cells(8).Width = 0

    '                '    gvr.Cells(2).Style.Add("text-align", "Right")
    '                '    gvr.Cells(3).Style.Add("text-align", "Right")
    '                '    gvr.Cells(4).Style.Add("text-align", "Right")
    '                '    gvr.Cells(5).Style.Add("text-align", "Right")
    '                '    gvr.Cells(6).Style.Add("text-align", "Right")
    '                '    gvr.Cells(7).Style.Add("text-align", "Right")

    '                'Next

    '                ' ''set grid view footer style
    '                ''tempgrid.FooterRow.Cells(3).Style.Add("border-top", "1px solid #C0C0C0")
    '                ''tempgrid.FooterRow.Cells(4).Style.Add("border-top", "1px solid #C0C0C0")
    '                ''tempgrid.FooterRow.Cells(5).Style.Add("border-top", "1px solid #C0C0C0")
    '                ''tempgrid.FooterRow.Cells(6).Style.Add("border-top", "1px solid #C0C0C0")
    '                ''tempgrid.FooterRow.Cells(7).Style.Add("border-top", "1px solid #C0C0C0")
    '                ''tempgrid.FooterRow.Cells(8).Style.Add("border-top", "1px solid #C0C0C0")
    '                ''tempgrid.FooterRow.Cells(9).Style.Add("border-top", "1px solid #C0C0C0")
    '                ''tempgrid.FooterRow.Cells(10).Style.Add("border-top", "1px solid #C0C0C0")

    '                'e.Item.Controls.Add(tempgrid)
    '            End If

    '        End If
    '    Catch ex As Exception
    '        uiMessageHelper.IsError = True
    '        uiMessageHelper.message = ex.Message

    '        If uiMessageHelper.IsExceptionLogged = False Then
    '            ExceptionPolicy.HandleException(ex, "Exception Policy")
    '        End If
    '    Finally
    '        If uiMessageHelper.IsError = True Then
    '            uiMessageHelper.setMessage(lblMessage1, pnlMessage1, uiMessageHelper.message, True)
    '        End If
    '    End Try

    'End Sub

#End Region

#Region "temp Grid Row Data Bound"
    ''' <summary>
    ''' This event makes the SI text concatinate woth saleid and calculate the balances
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub tempGrid_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim saleId As String = e.Row.Cells(1).Text.ToString().Trim()

            If Not (saleId.ToLower().Contains("opening balance")) Then
                Dim urlFullPath As String = ""
                Dim url As String = requestPath + PathConstants.SalesInvoice + saleId + "&Random=" + Today.Date

                If (browserAgent.ToLower().Contains("chrome")) Then
                    urlFullPath = "javascript:window.open('" + url + "','mywindowtitle','width=740,height=410')"
                Else
                    urlFullPath = "javascript:window.showModalDialog('" + url + "','mywindowtitle','dialogWidth=740,dialogHeight=360')"
                End If
                e.Row.Cells(0).Text = "<a href=""" + urlFullPath + """ >" +
                "SI " + saleId + "</a>"
            Else
                e.Row.Cells(0).Text = saleId
            End If

            Dim resultBalance = Math.Round(Convert.ToDecimal(Server.HtmlDecode(e.Row.Cells(2).Text)), 2).ToString()
            totalBalance += Convert.ToDecimal(Server.HtmlDecode(e.Row.Cells(2).Text))
            e.Row.Cells(2).Text = "£" + resultBalance.ToString()

            Dim resultCurrent = Math.Round(Convert.ToDecimal(Server.HtmlDecode(e.Row.Cells(3).Text)), 2).ToString()
            totalCurrent += Convert.ToDecimal(Server.HtmlDecode(e.Row.Cells(3).Text))
            e.Row.Cells(3).Text = "£" + resultCurrent.ToString()

            Dim result30Days = Math.Round(Convert.ToDecimal(Server.HtmlDecode(e.Row.Cells(4).Text)), 2).ToString()
            totalThirtyBalance += Convert.ToDecimal(Server.HtmlDecode(e.Row.Cells(4).Text))
            e.Row.Cells(4).Text = "£" + result30Days.ToString()

            Dim result60Days = Math.Round(Convert.ToDecimal(Server.HtmlDecode(e.Row.Cells(5).Text)), 2).ToString()
            totalSixtyBalance += Convert.ToDecimal(Server.HtmlDecode(e.Row.Cells(5).Text))
            e.Row.Cells(5).Text = "£" + result60Days.ToString()

            Dim result90Days = Math.Round(Convert.ToDecimal(Server.HtmlDecode(e.Row.Cells(6).Text)), 2).ToString()
            totalNintyBalance += Convert.ToDecimal(Server.HtmlDecode(e.Row.Cells(6).Text))
            e.Row.Cells(6).Text = "£" + result90Days.ToString()

            Dim resultOlder = Math.Round(Convert.ToDecimal(Server.HtmlDecode(e.Row.Cells(7).Text)), 2).ToString()
            totalOlderBalance += Convert.ToDecimal(Server.HtmlDecode(e.Row.Cells(7).Text))
            e.Row.Cells(7).Text = "£" + resultOlder.ToString()
        End If

        If e.Row.RowType = DataControlRowType.Footer Then
            GrandTotalBalance += totalBalance
            GrandTotalCurrent += totalCurrent
            GrandTotalThirtyBalance += totalThirtyBalance
            GrandTotalSixtyBalance += totalSixtyBalance
            GrandTotalNintyBalance += totalNintyBalance
            GrandTotalOlderBalance += totalOlderBalance
            e.Row.Cells(0).Text = "<b>Total:</b>"
            e.Row.Cells(2).Text = "<b>£" + Math.Round(totalBalance, 2).ToString() + "</b>"
            e.Row.Cells(3).Text = "<b>£" + Math.Round(totalCurrent, 2).ToString() + "</b>"
            e.Row.Cells(4).Text = "<b>£" + Math.Round(totalThirtyBalance, 2).ToString() + "</b>"
            e.Row.Cells(5).Text = "<b>£" + Math.Round(totalSixtyBalance, 2).ToString() + "</b>"
            e.Row.Cells(6).Text = "<b>£" + Math.Round(totalNintyBalance, 2).ToString() + "</b>"
            e.Row.Cells(7).Text = "<b>£" + Math.Round(totalOlderBalance, 2).ToString() + "</b>"
        End If

    End Sub
#End Region

#Region "temp Grid Row Data Bound Summary"
    ''' <summary>
    ''' This event makes the SI text concatinate woth saleid and calculate the balances
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub tempGrid_RowDataBoundSummary(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        'Dim url As String = ResolveClientUrl(PathConstants.SalesInvoice + e.Row.Cells(1).Text.ToString() + "&Random=" + Today.Date)

        'If e.Row.RowType = DataControlRowType.DataRow Then
        '    'e.Row.Cells(1).Text = "<a href='" + url + "' target='_blank'>" +
        '    '    "SI " + Server.HtmlDecode(e.Row.Cells(1).Text).ToString() + "</a>"

        '    Dim resultBalance = Math.Round(Convert.ToDecimal(Server.HtmlDecode(e.Row.Cells(2).Text)), 2).ToString()
        '    totalBalance += Convert.ToDecimal(Server.HtmlDecode(e.Row.Cells(2).Text))
        '    'e.Row.Cells(2).Text = "£" + resultBalance.ToString()

        '    Dim resultCurrent = Math.Round(Convert.ToDecimal(Server.HtmlDecode(e.Row.Cells(3).Text)), 2).ToString()
        '    totalCurrent += Convert.ToDecimal(Server.HtmlDecode(e.Row.Cells(3).Text))
        '    'e.Row.Cells(3).Text = "£" + resultCurrent.ToString()

        '    Dim result30Days = Math.Round(Convert.ToDecimal(Server.HtmlDecode(e.Row.Cells(4).Text)), 2).ToString()
        '    totalThirtyBalance += Convert.ToDecimal(Server.HtmlDecode(e.Row.Cells(4).Text))
        '    'e.Row.Cells(4).Text = "£" + result30Days.ToString()

        '    Dim result60Days = Math.Round(Convert.ToDecimal(Server.HtmlDecode(e.Row.Cells(5).Text)), 2).ToString()
        '    totalSixtyBalance += Convert.ToDecimal(Server.HtmlDecode(e.Row.Cells(5).Text))
        '    'e.Row.Cells(5).Text = "£" + result60Days.ToString()

        '    Dim result90Days = Math.Round(Convert.ToDecimal(Server.HtmlDecode(e.Row.Cells(6).Text)), 2).ToString()
        '    totalNintyBalance += Convert.ToDecimal(Server.HtmlDecode(e.Row.Cells(6).Text))
        '    'e.Row.Cells(6).Text = "£" + result90Days.ToString()

        '    Dim resultOlder = Math.Round(Convert.ToDecimal(Server.HtmlDecode(e.Row.Cells(7).Text)), 2).ToString()
        '    totalOlderBalance += Convert.ToDecimal(Server.HtmlDecode(e.Row.Cells(7).Text))
        '    'e.Row.Cells(7).Text = "£" + resultOlder.ToString()
        'End If

        'If e.Row.RowType = DataControlRowType.Footer Then
        '    GrandTotalBalance += totalBalance
        '    GrandTotalCurrent += totalCurrent
        '    GrandTotalThirtyBalance += totalThirtyBalance
        '    GrandTotalSixtyBalance += totalSixtyBalance
        '    GrandTotalNintyBalance += totalNintyBalance
        '    GrandTotalOlderBalance += totalOlderBalance
        '    e.Row.Cells(1).Text = "<b>Total:</b>"
        '    e.Row.Cells(2).Text = "<b>£" + Math.Round(totalBalance, 2).ToString() + "</b>"
        '    e.Row.Cells(3).Text = "<b>£" + Math.Round(totalCurrent, 2).ToString() + "</b>"
        '    e.Row.Cells(4).Text = "<b>£" + Math.Round(totalThirtyBalance, 2).ToString() + "</b>"
        '    e.Row.Cells(5).Text = "<b>£" + Math.Round(totalSixtyBalance, 2).ToString() + "</b>"
        '    e.Row.Cells(6).Text = "<b>£" + Math.Round(totalNintyBalance, 2).ToString() + "</b>"
        '    e.Row.Cells(7).Text = "<b>£" + Math.Round(totalOlderBalance, 2).ToString() + "</b>"
        'End If

    End Sub
#End Region
#Region " Pager Change Event"
    ''' <summary>
    ''' Page index page
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Changed(sender As Object, e As EventArgs)
        Dim pageIndex As Integer = Integer.Parse(TryCast(sender, LinkButton).CommandArgument)
        'PageSize = Convert.ToInt32(txtResults.Text.Trim())
        objPageSortBo.PageNumber = pageIndex
        'objPageSortBo.PageSize = PageSize
        Me.SearchSalesAccountDetail()
    End Sub
#End Region

    ''#Region "Execute SQL Job"
    ''    Protected Sub btnJob_Click(sender As Object, e As EventArgs) Handles btnJob.Click
    ''        Try
    ''            Me.ExecuteSqlJob()
    ''            Me.GetSqlJobExecTime()


    ''        Catch ex As Exception
    ''            uiMessageHelper.IsError = True
    ''            uiMessageHelper.message = ex.Message

    ''            If uiMessageHelper.IsExceptionLogged = False Then
    ''                ExceptionPolicy.HandleException(ex, "Exception Policy")
    ''            End If
    ''        Finally
    ''            If uiMessageHelper.IsError = True Then
    ''                uiMessageHelper.setMessage(lblMessage1, pnlMessage1, uiMessageHelper.message, True)
    ''            End If
    ''        End Try
    ''    End Sub
    ''#End Region

#End Region

#Region "Functions"
#Region "Search Sales AccountDetail"
    ''' <summary>
    ''' Search Sales AccountDetail
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub SearchSalesAccountDetail()
        ' Dim dsAccountDetail As DataSet = New DataSet()
        Dim objSearchBO As SearchBO = New SearchBO()
        Dim ObjReportBL As ReportBL = New ReportBL()

        browserAgent = Request.ServerVariables("HTTP_USER_AGENT")

        PageSize = Convert.ToInt32(txtResults.Text.Trim())
        objPageSortBo.PageSize = PageSize
        If txtCustomerName.Text <> ApplicationConstants.SalesCustomerName Then
            objSearchBO.SalesCustomerName = txtCustomerName.Text.Trim()
        End If
        'If txtEmployeesName.Text <> ApplicationConstants.EmployeeName Then
        '    objSearchBO.EmployeeName = txtEmployeesName.Text.Trim()
        'End If
        'If txtOrganizationName.Text <> ApplicationConstants.OrganisationName Then
        '    objSearchBO.OrganisationName = txtOrganizationName.Text.Trim()
        'End If
        If chkTenantOnly.Checked Then
            objSearchBO.IsTenantOnlt = True
        End If
        If txtAccBalance.Text <> String.Empty Then
            objSearchBO.SABalance = Convert.ToDecimal(txtAccBalance.Text.Trim())
        End If
        ObjReportBL.searchSalesAccount(dsAccountDetail, objSearchBO, objPageSortBo)
        SessionManager.setSalesAccountDetail(dsAccountDetail.Tables("SalesDetail"))
        rptCustomerDetail.DataSource = dsAccountDetail.Tables("AccountDetail")
        rptCustomerDetail.DataBind()
        tableGrand.Style.Add("display", "Block")
        divPager.Style.Add("display", "Block")
        divRepeater.Style.Add("display", "Block")
        Dim toRecord As Integer = Convert.ToInt32(dsAccountDetail.Tables("AccountDetail").Rows.Count)
        Dim recordCount As Integer = Convert.ToInt32(dsAccountDetail.Tables("TotalCount").Rows(0).Item("totalCount"))
        Me.PopulatePager(recordCount, objPageSortBo.PageNumber, toRecord)
        'Me.PopulatePager(toRecord, objPageSortBo.PageNumber, toRecord)
    End Sub
#End Region


#Region "Search Sales Account Summary"
    ''' <summary>
    ''' Search Sales Account Summary
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub SearchSalesAccountSummary()
        ' Dim dsAccountDetail As DataSet = New DataSet()
        Dim objSearchBO As SearchBO = New SearchBO()
        Dim ObjReportBL As ReportBL = New ReportBL()
        PageSize = Convert.ToInt32(txtResults.Text.Trim())
        objPageSortBo.PageSize = PageSize
        If txtCustomerName.Text <> ApplicationConstants.SalesCustomerName Then
            objSearchBO.SalesCustomerName = txtCustomerName.Text.Trim()
        End If
        'If txtEmployeesName.Text <> ApplicationConstants.EmployeeName Then
        '    objSearchBO.EmployeeName = txtEmployeesName.Text.Trim()
        'End If
        'If txtOrganizationName.Text <> ApplicationConstants.OrganisationName Then
        '    objSearchBO.OrganisationName = txtOrganizationName.Text.Trim()
        'End If
        If chkTenantOnly.Checked Then
            objSearchBO.IsTenantOnlt = True
        End If
        If txtAccBalance.Text <> String.Empty Then
            objSearchBO.SABalance = Convert.ToDecimal(txtAccBalance.Text.Trim())
        End If

        ObjReportBL.searchSalesAccount(dsAccountDetail, objSearchBO, objPageSortBo)
        SessionManager.setSalesAccountDetail(dsAccountDetail.Tables("SalesDetail"))
        rptCustomerSummary.DataSource = dsAccountDetail.Tables("AccountDetail")
        rptCustomerSummary.DataBind()

        tableGrand.Style.Add("display", "Block")
        divPager.Style.Add("display", "Block")
        divCustomerSummary.Style.Add("display", "Block")
        Dim toRecord As Integer = Convert.ToInt32(dsAccountDetail.Tables("AccountDetail").Rows.Count)
        Dim recordCount As Integer = Convert.ToInt32(dsAccountDetail.Tables("TotalCount").Rows(0).Item("totalCount"))
        Me.PopulatePager(recordCount, objPageSortBo.PageNumber, toRecord)

        '' Dim dsAccountDetail As DataSet = New DataSet()
        'Dim objSearchBO As SearchBO = New SearchBO()
        'Dim ObjReportBL As ReportBL = New ReportBL()
        'PageSize = Convert.ToInt32(txtResults.Text.Trim())
        'objPageSortBo.PageSize = PageSize
        'If txtCustomerName.Text <> ApplicationConstants.SalesCustomerName Then
        '    objSearchBO.SalesCustomerName = txtCustomerName.Text.Trim()
        'End If

        'If chkTenantOnly.Checked Then
        '    objSearchBO.IsTenantOnlt = True
        'End If
        'If txtAccBalance.Text <> String.Empty Then
        '    objSearchBO.SABalance = Convert.ToDecimal(txtAccBalance.Text.Trim())
        'End If
        'ObjReportBL.searchSalesAccountSumamry(dsAccountDetail, objSearchBO, objPageSortBo)
        '' ''SessionManager.setSalesAccountDetail(dsAccountDetail.Tables("SalesDetail"))
        ''MA' rptCustomerDetail.DataSource = dsAccountDetail.Tables("AccountDetail")
        ''MA' rptCustomerDetail.DataBind()
        ''MA' tableGrand.Style.Add("display", "Block")

        ''grdCustomerSummary.DataSource = dsAccountDetail.Tables("CustomerSummary")
        ''grdCustomerSummary.DataBind()

        'divPager.Style.Add("display", "Block")
        'divCustomerSummary.Style.Add("display", "Block")
        'Dim toRecord As Integer = Convert.ToInt32(dsAccountDetail.Tables("CustomerSummary").Rows.Count)
        '' ''Dim recordCount As Integer = Convert.ToInt32(dsAccountDetail.Tables("TotalCount").Rows(0).Item("totalCount"))
        '' ''Me.PopulatePager(recordCount, objPageSortBo.PageNumber, toRecord)
        'Me.PopulatePager(toRecord, objPageSortBo.PageNumber, toRecord)
    End Sub
#End Region


#Region "check AccountType"
    Protected Function CheckAccountType(ByVal m As Object) As String
        If m = 1 Then
            Return "TN "
        ElseIf m = 2 Then
            Return "EMP "
        ElseIf m = 3 Then
            Return "SUP "
        Else
            Return "SCU "
        End If


    End Function
#End Region

#Region "Check Organization"
    Protected Function CheckOrganization(ByVal org As Object) As String
        If IsDBNull(org) Then
            Return "NA "

        Else
            Return org
        End If


    End Function
#End Region

#Region "check Tenancy"
    Protected Function CheckTenancy(ByVal ten As String) As String
        If IsDBNull(ten.Trim()) Or ten.Trim() = "" Or ten.Trim() = String.Empty Then

            Return "NA "

        Else
            Return ten
        End If


    End Function
#End Region

#Region "check BALANCE"
    Protected Function CheckBalance(ByVal bal As Object) As String
        If IsDBNull(bal) Then
            Return "0 "

        Else
            Return bal
        End If


    End Function
#End Region

#Region "Populate Pager"
    ''' <summary>
    ''' Populate Pager for paging
    ''' </summary>
    ''' <param name="recordCount"></param>
    ''' <param name="currentPage"></param>
    ''' <param name="toRecords"></param>
    ''' <remarks></remarks>
    Private Sub PopulatePager(recordCount As Integer, currentPage As Integer, toRecords As Integer)
        PageSize = Convert.ToInt32(txtResults.Text.Trim())
        Dim dblPageCount As Double = CDbl(CDec(recordCount) / Convert.ToDecimal(PageSize))
        Dim pageCount As Integer = CInt(Math.Ceiling(dblPageCount))
        Dim pages As New List(Of ListItem)()
        If pageCount > 0 Then
            pages.Add(New ListItem("<<", "1", currentPage > 1))
            pages.Add(New ListItem("<", currentPage - 1, currentPage > 1))
            pages.Add(New ListItem("Page " + currentPage.ToString() + " of " + pageCount.ToString() + " :", "", False))
            pages.Add(New ListItem("Records " + ((currentPage - 1) * PageSize + 1).ToString() + " to " + (((currentPage - 1) * PageSize) + toRecords).ToString() + " of " + recordCount.ToString(), "", False))
            ' For i As Integer = 1 To pageCount
            'pages.Add(New ListItem(i.ToString(), i.ToString(), i <> currentPage))
            ' Next
            pages.Add(New ListItem(">", currentPage + 1, currentPage < pageCount))
            pages.Add(New ListItem(">>", pageCount.ToString(), currentPage < pageCount))
        End If
        rptPager.DataSource = pages
        rptPager.DataBind()
    End Sub
#End Region

    '#Region "Set SQL Job execution Date time"
    '    Private Sub GetSqlJobExecTime()
    '        Dim objReportBL As ReportBL = New ReportBL()
    '        Dim dsExecTime As DataSet = New DataSet()
    '        objReportBL.GetSqlJobExecTime(dsExecTime)
    '        If dsExecTime.Tables.Count > 0 Then
    '            If dsExecTime.Tables(0).Rows.Count > 0 Then
    '                Dim execDateTime As DateTime
    '                execDateTime = Convert.ToDateTime(dsExecTime.Tables(0).Rows(0).Item("STARTTIME"))
    '                SessionManager.setSqlJobExecDate(execDateTime)
    '                lblExecutionTime.Text = execDateTime.ToString()
    '            End If
    '        End If
    '    End Sub
    '#End Region

#Region "Execute Sql Job"
    Private Sub ExecuteSqlJob()
        Dim objReportBL As ReportBL = New ReportBL()
        Dim dsExecSqlJob As DataSet = New DataSet()
        objReportBL.ExecuteSqlJob(dsExecSqlJob)
        If dsExecSqlJob.Tables.Count > 0 Then
            If dsExecSqlJob.Tables(0).Rows.Count > 0 Then
                Dim execDateTime As Boolean
                execDateTime = Convert.ToBoolean(dsExecSqlJob.Tables(0).Rows(0).Item("RESULT"))
                If execDateTime = True Then
                    uiMessageHelper.setMessage(lblMessage1, pnlMessage1, UserMessageConstants.SqlJobExecTrue, False)
                Else
                    uiMessageHelper.setMessage(lblMessage1, pnlMessage1, UserMessageConstants.SqlJobExecFalse, True)
                End If
                'lblExecutionTime.Text = execDateTime.ToString()
            End If
        End If
    End Sub

#End Region
#End Region

    Protected Sub btnDetailedReport_Click(sender As Object, e As EventArgs) Handles btnDetailedReport.Click
        If (btnDetailedReport.Text = "View Detailed Report") Then
            btnDetailedReport.Text = "View Summary Report"
            divRepeater.Style.Add("display", "block")
            divCustomerSummary.Style.Add("display", "none")
            Me.SearchSalesAccountDetail()
        ElseIf (btnDetailedReport.Text = "View Summary Report") Then
            btnDetailedReport.Text = "View Detailed Report"
            divRepeater.Style.Add("display", "none")
            divCustomerSummary.Style.Add("display", "block")
            Me.SearchSalesAccountSummary()
        End If
    End Sub

End Class