﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="AgedDebtorAnalysis_19022015.aspx.vb"
    Inherits="AgedDebtor.AgedDebtorAnalysis_19022015" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Broadland Housing Aged Debtors Analysis</title>
    <link href="../../Styles/Site.css" rel="stylesheet" type="text/css" />
    <style type="text/css" media="print">
        @page
        {
            height: 800px;
            border: 1px solid #828282;
            margin-bottom: 20px;
            margin-top: 10px;
            size: Portrait;
        }
        body
        {
            margin: 10px;
        }
    </style>
    <script type="text/javascript">
        function PrintPage() {
            document.getElementById('divSearch').style.display = 'none';
            document.getElementById('divPager').style.display = 'none';
            // document.getElementById('pnlMessage1').style.display = 'none';
            window.print();

        }
        function reappear(inputName, message) {
            if (document.getElementById(inputName).value == "") {
                document.getElementById(inputName).value = message;
            }
        } 
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <b>Broadland Housing Aged Debtors Analysis</b>
    <asp:UpdatePanel ID="updPanelSearch" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div id="divSearch">
                <table width="920" border="0" cellpadding="2" cellspacing="0">
                    <tr>
                        <td colspan="3">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="txtCustomerName" runat="server" CssClass="textbox200" Text="Sales Customer name"
                                onclick=" this.value=''" onBlur="reappear(this.id,'Sales Customer name');"></asp:TextBox>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="txtEmployeesName" runat="server" CssClass="textbox200" Text="Employee name"
                                onclick=" this.value=''" onBlur="reappear(this.id,'Employee name');"></asp:TextBox>
                        </td>
                        <td align="right" class="checkbox200">
                            <b>Tenants only</b>
                            <asp:CheckBox ID="chkTenantOnly" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="txtOrganizationName" runat="server" CssClass="textbox200" Text="Organisation name"
                                onclick=" this.value=''" onBlur="reappear(this.id,'Organisation name');"></asp:TextBox>
                        </td>
                        <td class="checkbox200">
                            <b>Sales Acc Balance: >£
                        </td>
                        <td align="left" style="text-align: center;">
                            <asp:TextBox ID="txtAccBalance" runat="server" Width="80px" Text="0.00"></asp:TextBox></b>
                        </td>
                        <td class="box200" align="left">
                            <asp:Button ID="btnUpdate" runat="server" Text="Update Result" class="RSLButton" />
                        </td>
                        <td class="checkbox200">
                            <asp:Button ID="btnJob" runat="server" Text="Execute Job" class="RSLButton" />
                            <asp:Button ID="btnPrint" runat="server" Text="Print" class="RSLButton" OnClientClick="javascript:PrintPage();" />
                        </td>
                    </tr>
                </table>
                <asp:Panel ID="pnlMessage1" runat="server" Visible="False">
                    <asp:Label ID="lblMessage1" runat="server"></asp:Label>
                </asp:Panel>
            </div>
            <%-- </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="updPanelReport" runat="server" UpdateMode="Conditional"  >
        <ContentTemplate>--%>
            <div style='border: 1px solid #b1b1b1; margin-top: 10px; width: 920px; padding: 10px 10px 10px 10px'
                id="divRepeater" runat="server">
                <asp:Repeater ID="rptCustomerDetail" runat="server">
                    <ItemTemplate>
                        <table width="920px" id="Tblrepeater" runat="server">
                            <tr class="repeaterHeader">
                                <td align="left" style='width: 100px;'>
                                    <b>Ref:
                                        <%# CheckAccountType(DataBinder.Eval(Container.DataItem, "ACCOUNTTYPE"))%>
                                        <%# DataBinder.Eval(Container.DataItem, "ACCOUNTID")%>
                                    </b>
                                </td>
                                <td align="left" style='width: 250px;'>
                                    <b>Organisation:
                                        <%# CheckOrganization(DataBinder.Eval(Container.DataItem, "ORGANIZATION"))%>
                                    </b>
                                </td>
                                <td align="left" style='width: 250px;'>
                                    <b>Contact:
                                        <%# DataBinder.Eval(Container.DataItem, "CONTACT")%>
                                    </b>
                                </td>
                                <td align="left" style='width: 120px;'>
                                    <b>PostCode:
                                        <%# DataBinder.Eval(Container.DataItem, "POSTCODE")%>
                                    </b>
                                </td>
                                <td align="right" style='width: 200px;'>
                                    <b>Sales Acc Balance: (£
                                        <%# Math.Round(Convert.ToDecimal(CheckBalance(DataBinder.Eval(Container.DataItem, "BALANCE"))), 2).ToString()%>)
                                    </b>
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:Repeater>
                <table width="920px" id="tableGrand" runat="server">
                    <tr>
                        <td style='width: 80px;'>
                            &nbsp;
                        </td>
                        <td style='width: 80px;'>
                            &nbsp;
                        </td>
                        <td style='width: 200px;'>
                            &nbsp;
                        </td>
                        <td style='width: 80px;'>
                            &nbsp;
                        </td>
                        <td style='width: 80px;'>
                            &nbsp;
                        </td>
                        <td style='width: 80px;'>
                            &nbsp;
                        </td>
                        <td style='width: 65px;'>
                            &nbsp;
                        </td>
                        <td style='width: 65px;'>
                            &nbsp;
                        </td>
                        <td style='width: 65px;'>
                            &nbsp;
                        </td>
                        <td style='width: 80px;'>
                            &nbsp;
                        </td>
                    </tr>
                    <tr style='border-top-style: solid; border-bottom-style: solid; border-top-width: 1px;
                        border-bottom-width: 1px; border-top-color: #C0C0C0; border-bottom-color: #C0C0C0;'>
                        <td colspan="5" align="right">
                            <b>Grand Totals: £<%=Math.Round(GrandTotalBalance, 2).ToString()%></b>
                        </td>
                        <td align="right" style='text-align: right;'>
                            £<%=Math.Round(GrandTotalCurrent, 2).ToString()%>
                        </td>
                        <td align="right" style='text-align: right;'>
                            £<%=Math.Round(GrandTotalThirtyBalance, 2).ToString()%>
                        </td>
                        <td align="right" style='text-align: right;'>
                            £<%=Math.Round(GrandTotalSixtyBalance, 2).ToString()%>
                        </td>
                        <td align="right" style='text-align: right;'>
                            £<%=Math.Round(GrandTotalNintyBalance, 2).ToString()%>
                        </td>
                        <td align="right" style='text-align: right;'>
                            £<%=Math.Round(GrandTotalOlderBalance, 2).ToString()%>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" align="right" style='padding-right: 20px;'>
                            <b>Sales Acc Balance Totals: £<%=Math.Round(TotalSalesAccountBalance, 2).ToString()%></b>
                        </td>
                    </tr>
                </table>
            </div>
            <div style='width: 940px; float: left; margin-top: 20px;' id="divPager" runat="server">
                <div style='width: 80%; float: left; font-style: italic; text-align: center;'>
                    &nbsp;
                    <asp:Repeater ID="rptPager" runat="server">
                        <ItemTemplate>
                            <asp:LinkButton ID="lnkPage" runat="server" Text='<%#Eval("Text") %>' CommandArgument='<%# Eval("Value") %>'
                                Enabled='<%# Eval("Enabled") %>' OnClick="Page_Changed" Style='text-decoration: none;
                                color: #000000;'></asp:LinkButton>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                <div style='width: 20%; float: left; font-style: italic; text-align: right;'>
                    Results per page:<asp:TextBox ID="txtResults" runat="server" Width="50" Text="25"
                        CssClass="textbox200"></asp:TextBox>
                </div>
                <div style='width: 940px; float: left; margin-top: 20px;'>
                    <div style='width: 80%; float: left; font-style: italic; text-align: Left;'>
                        Sql job Execution Time:
                        <asp:Literal ID="lblExecutionTime" runat="server"></asp:Literal>
                    </div>
                    <div style='width: 20%; float: left; text-align: right;'>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="updateProgressReportsArea" runat="server" DisplayAfter="10">
        <ProgressTemplate>
            <div class="loading-image">
                <img alt="Please Wait" src="../../Images/progress.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    </form>
</body>
</html>
