﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="RechargeStatement.aspx.vb"
    Inherits="AgedDebtor.RechargeStatement" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Sales Invoice Statement</title>
    <link href="../Styles/Site.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .style1
        {
            width: 150px;
        }
    </style>
    <script type="text/javascript">
        function PrintPage() {
            document.getElementById('tdClear').style.display = 'none';
            document.getElementById('tdFrom').style.display = 'none';
            document.getElementById('btnEmail').style.display = 'none';
            document.getElementById('btnPrint').style.display = 'none';
            window.print();
        }
        function ResetPage() {
            document.getElementById('txtFromDate').value = '';

        }
    </script>
</head>
<body>
    <form name="RSLFORM" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <asp:Panel ID="pnlMessage1" runat="server" Visible="False">
        <asp:Label ID="lblMessage1" runat="server"></asp:Label>
    </asp:Panel>
    <asp:UpdatePanel ID="updanelSearch" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <table border="0" cellpadding="0" cellspacing="0" style="margin-left: 10px; width: 750px">
                <tr>
                    <td style='font-weight: 900; font-size: 12px; color: #133E71; text-decoration: underline;
                        width: 175px;'>
                        Sales Invoice Statement
                    </td>
                    <td align="right" runat="server" id="tdFrom" style='width: 300px;'>
                        From:
                        <asp:TextBox ID="txtFromDate" runat="server" Style='border: 1px solid #133E71; width: 150px;'
                            TabIndex="2"></asp:TextBox>
                        <cc1:CalendarExtender ID="cntrlCalendarExtender" runat="server" TargetControlID="txtFromDate"
                            PopupButtonID="imgCalendar" Format="dd/MM/yyyy" />
                    </td>
                    <td align="left" runat="server" id="tdClear" style='width: 150px;'>
                        <asp:Button ID="btnClear" runat="server" Text="Clear" Style='border: 1px solid #133E71;
                            background-color: White; margin-left: 10px; color: #133E71; margin-right: 10px;'
                            OnClientClick="javascript:ResetPage();" TabIndex="1" />
                        <asp:Button ID="btnGo" runat="server" Text="Go" Style='border: 1px solid #133E71;
                            background-color: White; margin-left: 10px; color: #133E71;' />
                    </td>
                    <td align="right">
                        <img alt="" src="../Images/LOGOLETTERFinance.gif" class="classBodypopup" />
                    </td>
                </tr>
            </table>
            <table border="0" style='margin-left: 10px; margin-top: -20px' cellspacing="0" cellpadding="3"
                width="750">
                <tr runat="server" id="trTanancy">
                    <td width="150" style='border: 1px solid #133E71;'>
                        Tenancy Ref: <b>
                            <asp:Label ID="lblTanancy" runat="server"></asp:Label>
                        </b>
                    </td>
                    <td>
                        </b>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblContact" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblAddress1" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblAddress2" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblTown" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblCountry" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblPostCode" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td height="100%" class="style1">
                    </td>
                    <td align="right" style='vertical-align: bottom;'>
                        <asp:Button ID="btnEmail" runat="server" Text="Email Statement" Style='border: 1px solid #133E71;
                            background-color: White; margin-left: 10px; width: 150px; color: #133E71; margin-right: 50px;' />
                    </td>
                </tr>
                <tr>
                    <td height="100%" class="style1">
                        <b>Date:<%=ndate.ToString()%></b></td>
                    <td align="right">
                        <asp:Button ID="btnPrint" runat="server" Text="Print Statement" Style='border: 1px solid #133E71;
                            background-color: White; margin-left: 10px; width: 150px; color: #133E71; margin-right: 50px;'
                            OnClientClick="javascript:PrintPage();" UseSubmitBehavior="False" />
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="updPanelSales" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <div style='margin-left: 10px; width: 750px;'>
                <table border="0" style="margin-top: 10px; margin-bottom: 10px;" cellspacing="0"
                    cellpadding="0" width="100%">
                    <tr>
                        <td style="width: 80%" align="right">
                            <b>
                                <asp:Label ID="lblBalBFwd" runat="server" Text="Balance Brought Forward"></asp:Label>
                            </b>
                        </td>
                        <td style="width: 20%" align="right">
                            <b style='color: Red;'>
                                <%=FormatNegativeNumber(salesOpeningBalance)%></b>
                        </td>
                    </tr>
                </table>
                <asp:GridView ID="grdSalesInvoice" runat="server" AutoGenerateColumns="False" BorderStyle="None"
                    EmptyDataText="No repair work order entries exist for selected criteria." Width="100%">
                    <Columns>
                        <asp:BoundField HeaderText="Date" DataField="TDATE">
                            <HeaderStyle CssClass="gridHeader" HorizontalAlign="Left" />
                            <ItemStyle Width="10%" BorderStyle="None" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Category" DataField="PAYMENTTYPE">
                            <HeaderStyle CssClass="gridHeader" HorizontalAlign="Left" />
                            <ItemStyle Width="20%" BorderStyle="None" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Item Description" DataField="DESC">
                            <HeaderStyle CssClass="gridHeader" HorizontalAlign="Left" />
                            <ItemStyle Width="30%" BorderStyle="None" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Monies Due" DataField="DEBIT">
                            <HeaderStyle CssClass="gridHeader" />
                            <ItemStyle Width="10%" BorderStyle="None" HorizontalAlign="Right" />
                        </asp:BoundField>
                        <asp:BoundField HeaderText="Monies In" DataField="CREDIT">
                            <HeaderStyle CssClass="gridHeader" />
                            <ItemStyle Width="10%" BorderStyle="None" HorizontalAlign="Right" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Balance">
                            <ItemTemplate>
                                <asp:Label ID="lblTime" runat="server" Text='<%# FormatNegativeNumber(Eval("RUNNINGBALANCE"))%>'
                                    Style='color: Red;'></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle CssClass="gridHeader" Width="20%" />
                            <ItemStyle BorderStyle="None" HorizontalAlign="Right" />
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataRowStyle BorderStyle="None" ForeColor="Red" />
                </asp:GridView>
                <div class="gridfooter" style="width: 100%;">
                    &nbsp;</div>
                <table border="0" width="100%">
                    <tr>
                        <td align="right" style="width: 80%">
                            <b>Owed to BHA:</b>
                        </td>
                        <td align="right" style="width: 20%">
                            <b style='color: Red;'>
                                <%=FormatNegativeNumber(salesBalance)%></b>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>Payment made within the last 3 working days may not appear on your statement
                            </b>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 50%;">
                            Credits are not always refundable where there is money outstanding from<br />
                            Housing Benefit. Please Contact Customer Services on 0303 303 0003 for<br />
                            further information about this or any aspect of your statement.
                        </td>
                        <td style="width: 50%;">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <b>All our documents can be supplied in large print, Brialle, audio tape and in<br />
                                languages other than english. Please contact 0303 303 0003 if you
                                <br />
                                require this service.</b>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            Broadland Housing Association
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="updateProgressReportsArea" runat="server" DisplayAfter="10">
        <ProgressTemplate>
            <div class="loading-image">
                <img alt="Please Wait" src="../Images/progress.gif" />
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    </form>
</body>
</html>
