﻿Imports AD_Utilities
Imports AD_BusinessLogic
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports EO.Pdf
Imports System.Drawing
Imports System.IO
Imports System.Globalization
Imports System.Net.Mail
Imports System.Net.Mime
Imports System.Math

Public Class RechargeStatement
    Inherits System.Web.UI.Page
#Region "Properties"
    Dim accountId As Integer = 0
    Dim accountType As Integer = 0
    Dim documentTemplateUrl As String = String.Empty
    Dim salesInvoiceDocumentPath As String = String.Empty
    Dim salesInvoiceRootPath As String = String.Empty
    Dim salesInvoiceDoucmentName As String = String.Empty
    Dim salesInvoiceInByteForm As Byte()
    Dim isCreatePDF As Boolean = False
    Public salesBalance As String = String.Empty
    Public salesOpeningBalance As String = "0.00"
    Dim el As New AD_Utilities.ErrorLogger
    Public uiMessageHelper As UIMessageHelper = New UIMessageHelper()
    'Public ndate As String = Date.Now.ToString("F", CultureInfo.CreateSpecificCulture("en-US"))
    Public ndate As String = Now.ToString("dd") + " " + Now.ToString("MMMM") + " " + Now.ToString("yyyy")
#End Region

#Region "Events"
#Region "PageLoad event"
    ''' <summary>
    ''' Pageload event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try

            Me.getQueryStringValues()
            If (accountId > 0 And accountType > 0) Then
                Me.getAccountDetail(accountId, accountType)
                Me.getSalesInvoiceDetail(accountId, accountType)
                Me.getSalesInvoiceBalance(accountId, accountType)
            End If

            If isCreatePDF Then
                tdClear.Style.Add("Display", "none")
                tdFrom.Style.Add("Display", "none")
                btnEmail.Style.Add("Display", "none")
                btnPrint.Style.Add("Display", "none")

            End If
            updanelSearch.Update()
            updPanelSales.Update()
        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If

        Finally
            If uiMessageHelper.IsError = True Then
                uiMessageHelper.setMessage(lblMessage1, pnlMessage1, uiMessageHelper.message, True)
            End If
        End Try
    End Sub
#End Region
#Region "btnGo Click event"

    Protected Sub btnGo_Click(sender As Object, e As EventArgs) Handles btnGo.Click
        Dim fromDate As DateTime

        Me.getQueryStringValues()
        If (accountId > 0 And accountType > 0) Then
            Me.getAccountDetail(accountId, accountType)
            Me.getSalesInvoiceDetail(accountId, accountType, txtFromDate.Text.Trim())
            Me.getSalesInvoiceBalance(accountId, accountType)

            If (Not IsDBNull(txtFromDate.Text) And txtFromDate.Text.Trim() <> "" And txtFromDate.Text.Trim() <> "01/01/1900") Then
                fromDate = Convert.ToDateTime(txtFromDate.Text.Trim())
                Me.getSalesInvoiceBalBghtFwd(accountId, accountType, fromDate)
            End If
        End If
    End Sub
#End Region
#Region "btn Email Click event"
    ''' <summary>
    ''' btn Email Click event
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Sub btnEmail_Click(sender As Object, e As EventArgs) Handles btnEmail.Click
        Try
            Dim accountDetailDS As DataTable = New DataTable()
            accountDetailDS = SessionManager.getAccountDetail()
            If accountDetailDS.Rows.Count > 0 And Not IsNothing(accountDetailDS.Rows(0).Item("EMAIL")) Then
                If Validation.isEmail(accountDetailDS.Rows(0).Item("EMAIL")) Then
                    Me.getQueryStringValues()
                    Me.processsalesInvoiceDocument()
                    Me.emailStatement(accountDetailDS.Rows(0).Item("EMAIL"))
                End If

            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        End Try
    End Sub
#End Region
#End Region

#Region "Function"
#Region "get Query String Values"

    Private Sub getQueryStringValues()
        If Request.QueryString(PathConstants.create) IsNot Nothing Then
            'the user want to go to report area
            isCreatePDF = IIf(Request.QueryString(PathConstants.create) = PathConstants.yes, True, False)
        End If
        If Request.QueryString(PathConstants.accountId) IsNot Nothing And Request.QueryString(PathConstants.accountType) IsNot Nothing Then
            'the user want to go to report area
            accountId = Convert.ToInt32(Request.QueryString(PathConstants.accountId))
            accountType = Convert.ToInt32(Request.QueryString(PathConstants.accountType))
        End If


    End Sub
#End Region

#Region "get account Detail"
    ''' <summary>
    ''' get Account Detail 
    ''' </summary>
    ''' <param name="accId"></param>
    ''' <param name="accType"></param>
    ''' <remarks></remarks>
    Private Sub getAccountDetail(ByVal accId As Integer, ByVal accType As Integer)
        Dim dsAccountDetail As DataSet = New DataSet()
        Dim ObjcustomerBL As CustomerBL = New CustomerBL()
        ObjcustomerBL.GetCustomerDetail(dsAccountDetail, accId, accType)
        Dim dtAccountDetail As DataTable = New DataTable()
        If dsAccountDetail.Tables.Count > 0 Then
            dtAccountDetail = dsAccountDetail.Tables(0)
            If dtAccountDetail.Rows.Count > 0 Then
                SessionManager.setAccountDetail(dtAccountDetail)
                If (dtAccountDetail.Rows(0).Item("TENANCYID").ToString() = "0") Then
                    trTanancy.Visible = False
                Else
                    lblTanancy.Text = dtAccountDetail.Rows(0).Item("TENANCYID").ToString()
                    trTanancy.Visible = True
                End If
                lblContact.Text = dtAccountDetail.Rows(0).Item("CONTACT").ToString()
                If accType = 3 Then
                    lblAddress1.Text = dtAccountDetail.Rows(0).Item("BUILDING").ToString() + " <br/>" + dtAccountDetail.Rows(0).Item("ADDRESS1").ToString()
                Else
                    lblAddress1.Text = dtAccountDetail.Rows(0).Item("BUILDING").ToString() + " " + dtAccountDetail.Rows(0).Item("ADDRESS1").ToString()

                End If
                lblAddress2.Text = dtAccountDetail.Rows(0).Item("ADDRESS2").ToString()
                lblTown.Text = dtAccountDetail.Rows(0).Item("TOWNCITY").ToString()
                lblPostCode.Text = dtAccountDetail.Rows(0).Item("POSTCODE").ToString()
                lblCountry.Text = dtAccountDetail.Rows(0).Item("COUNTY").ToString()
            Else
                uiMessageHelper.setMessage(lblMessage1, pnlMessage1, UserMessageConstants.RecordNotExist, True)
            End If
        End If



    End Sub
#End Region

#Region "get Sales Invoice Detail"
    ''' <summary>
    ''' get Sales invoice Detail 
    ''' </summary>
    ''' <param name="accId"></param>
    ''' <param name="accType"></param>
    ''' <remarks></remarks>
    Private Sub getSalesInvoiceDetail(ByVal accId As Integer, ByVal accType As Integer, Optional ByVal fromDate As String = "")
        Dim dsSalesDetail As DataSet = New DataSet()
        Dim ObjSalesBL As SalesBL = New SalesBL()
        Dim salesDataView As DataView = New DataView()
        ObjSalesBL.GetSalesInvoiceDetail(dsSalesDetail, accId, accType, fromDate)
        salesDataView = dsSalesDetail.Tables(0).DefaultView()
        grdSalesInvoice.DataSource = salesDataView
        grdSalesInvoice.DataBind()


    End Sub
#End Region

#Region "get Sales Invoice Balance"
    ''' <summary>
    ''' get Sales invoice Detail 
    ''' </summary>
    ''' <param name="accId"></param>
    ''' <param name="accType"></param>
    ''' <remarks></remarks>
    Private Sub getSalesInvoiceBalance(ByVal accId As Integer, ByVal accType As Integer)
        Dim dsSalesBalance As DataSet = New DataSet()
        Dim ObjSalesBL As SalesBL = New SalesBL()
        'Dim salesDataView As DataView = New DataView()
        ObjSalesBL.GetSalesInvoiceBalance(dsSalesBalance, accId, accType)
        If dsSalesBalance.Tables.Count > 0 And dsSalesBalance.Tables(0).Rows.Count > 0 Then
            Dim substring As String = dsSalesBalance.Tables(0).Rows(0).Item("BALANCE").ToString()
            salesBalance = Convert.ToString(Math.Round(Convert.ToDecimal(substring), 2))
        End If

    End Sub
#End Region

#Region "get Sales Invoice Balance Brought Forward"
    ''' <summary>
    ''' get Sales Invoice Balance Brought Forward
    ''' </summary>
    ''' <param name="accId"></param>
    ''' <param name="accType"></param>
    ''' <remarks></remarks>
    Private Sub getSalesInvoiceBalBghtFwd(ByVal accId As Integer, ByVal accType As Integer, ByVal filterDate As DateTime)
        Dim dsSalesBalBghtFwd As DataSet = New DataSet()
        Dim ObjSalesBL As SalesBL = New SalesBL()

        ObjSalesBL.GetSalesInvoiceBalBrghtFwd(dsSalesBalBghtFwd, accId, accType, filterDate)
        If dsSalesBalBghtFwd.Tables.Count > 0 And dsSalesBalBghtFwd.Tables(0).Rows.Count > 0 Then
            Dim substring As String = dsSalesBalBghtFwd.Tables(0).Rows(0).Item("OPENINGBALANCE").ToString()
            salesOpeningBalance = Convert.ToString(Math.Round(Convert.ToDecimal(substring), 2))
        End If
    End Sub
#End Region

#Region "process  Document for pdf"
    Public Sub processsalesInvoiceDocument()
        Me.documentTemplateUrl = Me.ToAbsoluteUrl(PathConstants.RechargeStatementDoc + "?" + PathConstants.accountId + "=" + Me.accountId.ToString() + "&" + PathConstants.accountType + "=" + Me.accountType.ToString() + "&" + PathConstants.create + "=" + PathConstants.yes)
        'Me.documentTemplateUrl = PathConstants.Bridge + "?" + PathConstants.accountId + "=" + Me.accountId.ToString() + "&" + PathConstants.accountType + "=" + Me.accountType.ToString() + "&" + PathConstants.create + "=" + PathConstants.yes
        el.WriteToErrorLog(documentTemplateUrl, "URL", "URL")
        Me.setFilePath()
        Me.makeEoPdf()
        Me.readsalesInvoiceDocument()
    End Sub
#End Region

#Region "return Absolute Url "

    Public Function ToAbsoluteUrl(relativeUrl As String) As String
        If String.IsNullOrEmpty(relativeUrl) Then
            Return relativeUrl
        End If

        If HttpContext.Current Is Nothing Then
            Return relativeUrl
        End If

        If relativeUrl.StartsWith("/") Then
            relativeUrl = relativeUrl.Insert(0, "~")
        End If
        If Not relativeUrl.StartsWith("~/") Then
            relativeUrl = relativeUrl.Insert(0, "~/")
        End If

        Dim url = HttpContext.Current.Request.Url
        Dim port = [String].Empty 'If(url.Port <> 80, (":" + url.Port.ToString()), [String].Empty)

        Return [String].Format("{0}://{1}{2}{3}", url.Scheme, url.Host, port, VirtualPathUtility.ToAbsolute(relativeUrl))
    End Function

#End Region

#Region "Convert the Url to PDF"
    Public Sub makeEoPdf()

        'Convert the Url to PDF
        EO.Pdf.Runtime.AddLicense( _
     "HOG4cai1wuCvdabw+g7kp+rp2g+9RoGkscufdePt9BDtrNzpz+eupeDn9hny" + _
     "ntzCnrWfWZekzQzrpeb7z7iJWZekscufWZfA8g/jWev9ARC8W7zTv/vjn5mk" + _
     "BxDxrODz/+ihbaW0s8uud4SOscufWbOz8hfrqO7CnrWfWZekzRrxndz22hnl" + _
     "qJfo8h/kdpm5wN23aKm0wt6hWe3pAx7oqOXBs9+hWabCnrWfWZekzR7ooOXl" + _
     "BSDxnrXo4xSwsc/Z6hLla8z83/LRe+fN59rmdrTAwB7ooOXlBSDxnrWRm+eu" + _
     "peDn9hnynrWRm3Xj7fQQ7azcwp61n1mXpM0X6Jzc8gQQyJ21ucM=")

        HtmlToPdf.Options.OutputArea = New RectangleF(0.0F, 0.5F, 8.0F, 20.0F)
        HtmlToPdf.Options.PageSize = PdfPageSizes.A4
        HtmlToPdf.Options.AutoFitY = HtmlToPdfAutoFitMode.ScaleToFit
        HtmlToPdf.ConvertUrl(Me.documentTemplateUrl, salesInvoiceDocumentPath)


    End Sub
#End Region

#Region "set pdf File Path"
    Public Sub setFilePath()

        'gernate unique String  
        Dim uniqueString As String = DateTime.Now.ToString("ddMMyyyyhhmmss")
        salesInvoiceRootPath = Request.PhysicalApplicationPath + "PDF\"
        salesInvoiceDoucmentName = "Sales Invoice Statement " + uniqueString + ".pdf"
        salesInvoiceDocumentPath = salesInvoiceRootPath + salesInvoiceDoucmentName

        If (Directory.Exists(salesInvoiceRootPath) = False) Then
            Directory.CreateDirectory(salesInvoiceRootPath)
        End If
    End Sub
#End Region

#Region "read pdf stream"


    Private Sub readSalesInvoiceDocument()
        Using stream = New FileStream(salesInvoiceDocumentPath, FileMode.Open, FileAccess.Read)
            Using reader = New BinaryReader(stream)
                salesInvoiceInByteForm = reader.ReadBytes(CInt(stream.Length))
            End Using
        End Using
    End Sub
#End Region

#Region " send Email Statement"

    Public Sub emailStatement(ByVal emailTo As String)
        Dim mailMessage As New MailMessage()
        Try
            mailMessage.To.Add(emailTo)
            mailMessage.Subject = "Sales Statement"
            mailMessage.Body = "kindly find the " + mailMessage.Subject

            Dim contentType As New ContentType(MediaTypeNames.Application.Pdf)

            Dim attachment As New Attachment(New MemoryStream(salesInvoiceInByteForm), contentType)

            Dim disposition = attachment.ContentDisposition
            disposition.FileName = "Sales Statement " + DateTime.Now.ToString("dd/MM/yyyy") + ".pdf"
            mailMessage.Attachments.Add(attachment)
            If (mailMessage.To.Count > 0) Then

                EmailHelper.sendEmail(mailMessage)

            End If

        Catch ex As Exception
            uiMessageHelper.IsError = True
            uiMessageHelper.message = ex.Message

            If uiMessageHelper.IsExceptionLogged = False Then
                ExceptionPolicy.HandleException(ex, "Exception Policy")
            End If
        Finally
            If (Not IsNothing(salesInvoiceDocumentPath) And salesInvoiceDocumentPath <> String.Empty) Then
                mailMessage.Dispose()
                File.Delete(salesInvoiceDocumentPath)

            End If



        End Try
    End Sub
#End Region

#Region "Format Negative Number"

    Protected Function FormatNegativeNumber(ByVal Number As Object) As String
        Dim retNumber As String

        Dim inputNumber As Decimal = 0.0

        Decimal.TryParse(Number.ToString(), inputNumber)

        retNumber = "£" + Abs(inputNumber).ToString()

        If inputNumber < 0 Then
            retNumber = "(" + retNumber + ")"
        End If

        Return retNumber
    End Function

#End Region

#End Region
End Class