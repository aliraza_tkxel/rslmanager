USE [RSLBHALive]
GO

/****** Object:  StoredProcedure [dbo].[AD_GETSQLJOBEXECUTIONDATETIME]    Script Date: 09/30/2013 12:38:46 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Ali Raza>
-- Create date: <Create Date,9/27/2013,>
-- Description:	<Description,,get Sql job execution date time>
-- =============================================
CREATE PROCEDURE [dbo].[AD_GETSQLJOBEXECUTIONDATETIME]
	-- Add the parameters for the stored procedure here

AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;
SELECT
	j.name NAME,
	[STARTTIME] = CONVERT(datetime, RTRIM(run_date) + ' '
	+ STUFF(STUFF(REPLACE(STR(RTRIM(h.run_time), 6, 0),
	' ', '0'), 3, 0, ':'), 6, 0, ':'))
FROM msdb.dbo.sysjobs AS j
INNER JOIN (SELECT
	job_id,
	instance_id = MAX(instance_id)
FROM msdb.dbo.sysjobhistory
GROUP BY job_id) AS l
	ON j.job_id = l.job_id
INNER JOIN msdb.dbo.sysjobhistory AS h
	ON h.job_id = l.job_id
	AND h.instance_id = l.instance_id
WHERE j.name = 'CalculateSalesAccountBalance'
ORDER BY CONVERT(int, h.run_duration) DESC,
[STARTTIME] DESC;
END

GO

