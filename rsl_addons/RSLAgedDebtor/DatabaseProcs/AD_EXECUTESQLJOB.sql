USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AD_EXECUTESQLJOB]    Script Date: 10/07/2013 13:07:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Ali raza>
-- Create date: <Create Date,9/27/2013,>
-- Description:	<Description,,>
--EXEC AD_EXECUTESQLJOB
-- =============================================
ALTER PROCEDURE [dbo].[AD_EXECUTESQLJOB] -- Add the parameters for the stored procedure here

  AS BEGIN -- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

 DECLARE @result  bit
 BEGIN TRAN UpdateTransaction 
 BEGIN 
 
 --DECLARE @counter int = 1

 --EXEC msdb.dbo.sp_start_job N'CalculateSalesAccountBalance' 

DECLARE @JOB_NAME SYSNAME = N'CalculateSalesAccountBalance'; 

IF NOT EXISTS(     
        select 1 
        from msdb.dbo.sysjobs_view job  
        inner join msdb.dbo.sysjobactivity activity on job.job_id = activity.job_id 
        where  
            activity.run_Requested_date is not null  
        and activity.stop_execution_date is null  
        and job.name = @JOB_NAME 
        ) 
BEGIN      
    PRINT 'Starting job ''' + @JOB_NAME + ''''; 
    EXEC msdb.dbo.sp_start_job @JOB_NAME; 
    PRINT 'COMMITING'
		SET @result = 1  
		Select @result RESULT
		COMMIT 
    
END 
ELSE 
BEGIN 
EXEC msdb.dbo.sp_stop_job @JOB_NAME;
 WAITFOR DELAY '00:00:05' ---- 5 Second Delay
    IF NOT EXISTS(     
        select 1 
        from msdb.dbo.sysjobs_view job  
        inner join msdb.dbo.sysjobactivity activity on job.job_id = activity.job_id 
        where  
            activity.run_Requested_date is not null  
        and activity.stop_execution_date is null  
        and job.name = @JOB_NAME 
        ) 
BEGIN      
    PRINT 'Starting job ''' + @JOB_NAME + ''''; 
    EXEC msdb.dbo.sp_start_job @JOB_NAME; 
    PRINT 'COMMITING'
		SET @result = 1  
		Select @result RESULT
		COMMIT 
    
END 
ELSE
BEGIN
    
    PRINT 'Job ''' + @JOB_NAME + ''' is already started '; 
    PRINT 'ROLLBACK TRAN AS THERE IS AN ERROR OR NO UPDATE FOR ANY ONE OF TRANSACTIONS'
		ROLLBACK
		SET @result = 0 
		Select @result RESULT
	END
 END
	 END

END
