-- Author:		Ali Raza
-- Create date: 27 Sep 2013
-- Description:	To Calculate Sales Account balance for All Account types Tenant,Employee, Supplier and SalesCustomer

USE [msdb]
GO

-- Job Configration

DECLARE @Database NVARCHAR(256) = N'RSLBHALive',
		@jobName NVARCHAR(256) = N'CalculateSalesAccountBalance',
		@jobDescription NVARCHAR(2000) =N'Calculate Sales Account balance for All Account types Tenant,Employee, Supplier and SalesCustomer' ,
		@Step1Name NVARCHAR(256) = N'CalculateBalance',
		@ScheduleName NVARCHAR(256) = N'Daily_At_1_InNight',
		@scheduleuid NVARCHAR(40) = LOWER(NEWID()),
		@ownerName NVARCHAR(256) = N'sa'
		
-- Job Configration

--=================================================================================
-- Delete the job if already exists with same name.
IF  EXISTS (SELECT job_id FROM msdb.dbo.sysjobs_view WHERE name = @jobName)
BEGIN
	DECLARE @OldJobID NVARCHAR(256) = (SELECT job_id FROM msdb.dbo.sysjobs_view WHERE name = @jobName)
	EXEC msdb.dbo.sp_delete_job @job_id= @OldJobID , @delete_unused_schedule=1
END
--=================================================================================


BEGIN TRANSACTION
--=================================================================================
-- Add Category if Not Already Exists - Default Category is used for this job.

DECLARE @ReturnCode INT
SELECT @ReturnCode = 0

IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
	EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
	IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
END

--=================================================================================


--=================================================================================
-- Add Job and get its @jobId
DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=@jobName, 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description= @jobDescription, 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=@ownerName, @job_id = @jobId OUTPUT

IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
--=================================================================================

--=================================================================================
-- Add job step(s)
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name = @Step1Name, 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=5, 
		@retry_interval=10, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'Exec AD_SALESACCOUNTBALANCE 1
		Exec AD_SALESACCOUNTBALANCE 2
		Exec AD_SALESACCOUNTBALANCE 3
		Exec AD_SALESACCOUNTBALANCE 4', 
		@database_name=@Database, 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
--=================================================================================

--=================================================================================
--Set a job step as start step - Step 1 in this case
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
--=================================================================================


--=================================================================================
--Create and Attach a scheduler to Job

EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=@ScheduleName,
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20130821, 
		@active_end_date=99991231, 
		@active_start_time=10000, 
		@active_end_time=235959,		
		@schedule_uid=@scheduleuid		
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

--=================================================================================


--=================================================================================
--Specify server(s) for the job, it is only local for this job.

EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

--=================================================================================
--This will be executed after all steps has performed sucessfully
COMMIT TRANSACTION
GOTO EndSave

--=================================================================================
--This will be executed in case of error in any step and any changes made will be Rolled back
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
--=================================================================================

EndSave:

GO


