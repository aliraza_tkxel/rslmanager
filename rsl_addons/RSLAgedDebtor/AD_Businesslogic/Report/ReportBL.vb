﻿Imports AD_DataAccess
Imports AD_BusinessObject

Namespace AD_BusinessLogic
    Public Class ReportBL
        Dim objReportDAL As ReportDAL = New ReportDAL()
#Region "search Sales Account Detail"
        ''' <summary> 
        ''' search Sales Account Detail
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="objSearchBO"></param>
        ''' <remarks></remarks>
        Sub searchSalesAccount(ByRef resultDataSet As DataSet, ByVal objSearchBO As SearchBO, ByRef objPageSortBo As PageSortBO)
            objReportDAL.searchSalesAccount(resultDataSet, objSearchBO, objPageSortBo)
        End Sub
#End Region

        '#Region "search Sales Account Summary "
        '        ''' <summary> 
        '        ''' search Sales Account Summary
        '        ''' </summary>
        '        ''' <param name="resultDataSet"></param>
        '        ''' <param name="objSearchBO"></param>
        '        ''' <remarks></remarks>
        '        Sub searchSalesAccountSumamry(ByRef resultDataSet As DataSet, ByVal objSearchBO As SearchBO, ByRef objPageSortBo As PageSortBO)
        '            objReportDAL.searchSalesAccountSummary(resultDataSet, objSearchBO, objPageSortBo)
        '        End Sub
        '#End Region


#Region "Execute Sql Job"
        ''' <summary>
        ''' Execute Sql Job
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Sub ExecuteSqlJob(ByRef resultDataSet As DataSet)
            objReportDAL.ExecuteSqlJob(resultDataSet)
        End Sub
#End Region

#Region "Get Sql Job Exec Time"
        ''' <summary>
        ''' Get Sql Job Exec Time
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Sub GetSqlJobExecTime(ByRef resultDataSet As DataSet)
            objReportDAL.GetSqlJobExecTime(resultDataSet)
        End Sub
#End Region
    End Class
End Namespace
