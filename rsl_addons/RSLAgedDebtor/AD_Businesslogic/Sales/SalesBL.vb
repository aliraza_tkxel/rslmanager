﻿Imports AD_DataAccess
Namespace AD_BusinessLogic
    Public Class SalesBL
        Dim objSalesDAL As SalesDAL = New SalesDAL()
#Region "Get Sales Invoicement Detail"
        ''' <summary>
        ''' Get Sales Invoicement Detail
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="accId"></param>
        ''' <param name="accType"></param>
        ''' <remarks></remarks>
        Sub GetSalesInvoiceDetail(ByRef resultDataSet As DataSet, ByVal accId As Integer, ByVal accType As Integer, Optional ByVal fromDate As String = "")
            objSalesDAL.GetSalesInvoiceDetail(resultDataSet, accId, accType, fromDate)
        End Sub
#End Region

#Region "Get Sales Invoice Balance"
        ''' <summary>
        ''' Get Sales Invoice Balance
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="accId"></param>
        ''' <param name="accType"></param>
        ''' <remarks></remarks>
        Sub GetSalesInvoiceBalance(ByRef resultDataSet As DataSet, ByVal accId As Integer, ByVal accType As Integer)
            objSalesDAL.GetSalesInvoiceBalance(resultDataSet, accId, accType)
        End Sub
#End Region

#Region "Get Sales Invoice Balance Brought Forward"
        ''' <summary>
        ''' Get Sales Invoice Balance Brought Forward
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="accId"></param>
        ''' <param name="accType"></param>
        ''' <remarks></remarks>
        Sub GetSalesInvoiceBalBrghtFwd(ByRef resultDataSet As DataSet, ByVal accId As Integer, ByVal accType As Integer, ByVal filterDate As DateTime)
            objSalesDAL.GetSalesInvoiceBalBrghtFwd(resultDataSet, accId, accType, filterDate)
        End Sub
#End Region

    End Class
End Namespace
