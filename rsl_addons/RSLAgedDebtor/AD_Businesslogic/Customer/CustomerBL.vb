﻿Imports AD_DataAccess
Namespace AD_BusinessLogic

    Public Class CustomerBL
        Dim objCustomerDAL As CustomerDAL = New CustomerDAL()
#Region "Get Customer detail"
        ''' <summary>
        ''' Get Customer detail
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="accId"></param>
        ''' <param name="accType"></param>
        ''' <remarks></remarks>
        Sub GetCustomerDetail(ByRef resultDataSet As DataSet, ByVal accId As Integer, ByVal accType As Integer)
            objCustomerDAL.GetCustomerDetail(resultDataSet, accId, accType)
        End Sub
#End Region

    End Class
End Namespace