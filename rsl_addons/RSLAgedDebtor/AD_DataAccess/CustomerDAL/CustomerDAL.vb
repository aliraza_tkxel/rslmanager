﻿Imports AD_BusinessObject
Imports AD_Utilities

Namespace AD_DataAccess
    Public Class CustomerDAL : Inherits BaseDAL

#Region "Get Customer Detail"
        ''' <summary>
        ''' Get Customer Detail according to account type
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="accId"></param>
        ''' <param name="accType"></param>
        ''' <remarks></remarks>
        Sub GetCustomerDetail(ByRef resultDataSet As DataSet, ByVal accId As Integer, ByVal accType As Integer)

            Dim parameterList As ParameterList = New ParameterList()
            Dim accIdParam As ParameterBO = New ParameterBO("accountId", accId, DbType.Int32)
            parameterList.Add(accIdParam)

            Dim accTypeParam As ParameterBO = New ParameterBO("accountType", accType, DbType.Int32)
            parameterList.Add(accTypeParam)
            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.GetCustomerDetail)

        End Sub
#End Region



    End Class
End Namespace
