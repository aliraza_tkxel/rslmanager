﻿Imports AD_BusinessObject
Imports AD_Utilities

Namespace AD_DataAccess

    Public Class ReportDAL : Inherits BaseDAL
        '#Region "Search Sales Account Summary "
        '        ''' <summary>
        '        ''' Search Sales account Summary
        '        ''' </summary>
        '        ''' <param name="resultDataSet"></param>
        '        ''' <param name="objSearchBO"></param>
        '        ''' <remarks></remarks>

        '        Sub searchSalesAccountSummary(ByRef resultDataSet As DataSet, ByVal objSearchBO As SearchBO, ByRef objPageSortBo As PageSortBO)

        '            Dim parameterList As ParameterList = New ParameterList()
        '            Dim salesCustNameParam As ParameterBO = New ParameterBO("CustName", objSearchBO.SalesCustomerName, DbType.String)
        '            parameterList.Add(salesCustNameParam)

        '            Dim tanentOnlyParam As ParameterBO = New ParameterBO("tanentOnly", objSearchBO.IsTenantOnlt, DbType.Boolean)
        '            parameterList.Add(tanentOnlyParam)

        '            Dim sABalanceParam As ParameterBO = New ParameterBO("SABalance", objSearchBO.SABalance, DbType.Decimal)
        '            parameterList.Add(sABalanceParam)

        '            Dim pageSize As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
        '            parameterList.Add(pageSize)

        '            Dim pageNumber As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
        '            parameterList.Add(pageNumber)
        '            Dim dtAccountSummary As DataTable = New DataTable()
        '            dtAccountSummary.TableName = "CustomerSummary"
        '            resultDataSet.Tables.Add(dtAccountSummary)

        '            Dim sp As String = "" 'SpNameConstants.SearchSalesAccountSummary
        '            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parameterList, sp)
        '            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtAccountSummary)

        '        End Sub
        '#End Region

#Region "Search Sales Account Detail "
        ''' <summary>
        ''' Search Sales account 
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="objSearchBO"></param>
        ''' <remarks></remarks>

        Sub searchSalesAccount(ByRef resultDataSet As DataSet, ByVal objSearchBO As SearchBO, ByRef objPageSortBo As PageSortBO)

            Dim parameterList As ParameterList = New ParameterList()
            Dim salesCustNameParam As ParameterBO = New ParameterBO("CustName", objSearchBO.SalesCustomerName, DbType.String)
            parameterList.Add(salesCustNameParam)

            Dim empNameParam As ParameterBO = New ParameterBO("empName", objSearchBO.EmployeeName, DbType.String)
            parameterList.Add(empNameParam)

            Dim orgNameParam As ParameterBO = New ParameterBO("orgName", objSearchBO.OrganisationName, DbType.String)
            parameterList.Add(orgNameParam)

            Dim tanentOnlyParam As ParameterBO = New ParameterBO("tanentOnly", objSearchBO.IsTenantOnlt, DbType.Boolean)
            parameterList.Add(tanentOnlyParam)

            Dim sABalanceParam As ParameterBO = New ParameterBO("SABalance", objSearchBO.SABalance, DbType.Decimal)
            parameterList.Add(sABalanceParam)

            Dim pageSize As ParameterBO = New ParameterBO("pageSize", objPageSortBo.PageSize, DbType.Int32)
            parameterList.Add(pageSize)

            Dim pageNumber As ParameterBO = New ParameterBO("pageNumber", objPageSortBo.PageNumber, DbType.Int32)
            parameterList.Add(pageNumber)
            Dim dtAccountDetail As DataTable = New DataTable()
            dtAccountDetail.TableName = "AccountDetail"
            resultDataSet.Tables.Add(dtAccountDetail)

            Dim dtSalesDetail As DataTable = New DataTable()
            dtSalesDetail.TableName = "SalesDetail"
            resultDataSet.Tables.Add(dtSalesDetail)

            Dim dtTotalCount As DataTable = New DataTable()
            dtTotalCount.TableName = "TotalCount"
            resultDataSet.Tables.Add(dtTotalCount)

            Dim sp As String = SpNameConstants.SearchSalesAccount
            Dim lResultDataReader As IDataReader = MyBase.SelectRecord(parameterList, sp)
            resultDataSet.Load(lResultDataReader, LoadOption.OverwriteChanges, dtTotalCount, dtAccountDetail, dtSalesDetail)
        End Sub
#End Region

#Region "Execute SQL JOB"
        ''' <summary>
        ''' Execute SQL JOB
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Sub ExecuteSqlJob(ByRef resultDataSet As DataSet)

            Dim parameterList As ParameterList = New ParameterList()

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.ExecuteSqlJob)

        End Sub
#End Region

#Region "Get SQL JOB Execution Date Time"
        ''' <summary>
        ''' Execute SQL JOB
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <remarks></remarks>
        Sub GetSqlJobExecTime(ByRef resultDataSet As DataSet)

            Dim parameterList As ParameterList = New ParameterList()

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.SqlJobExecTime)

        End Sub
#End Region
    End Class

End Namespace
