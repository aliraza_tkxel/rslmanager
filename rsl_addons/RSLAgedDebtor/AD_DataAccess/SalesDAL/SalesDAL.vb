﻿Imports AD_BusinessObject
Imports AD_Utilities

Namespace AD_DataAccess
    Public Class SalesDAL : Inherits BaseDAL
#Region "Get Sales Invoice Detail"
        ''' <summary>
        ''' Get Customer Detail according to account type
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="accId"></param>
        ''' <param name="accType"></param>
        ''' <remarks></remarks>
        Sub GetSalesInvoiceDetail(ByRef resultDataSet As DataSet, ByVal accId As Integer, ByVal accType As Integer, Optional ByVal fromDate As String = "")

            Dim parameterList As ParameterList = New ParameterList()
            Dim accIdParam As ParameterBO = New ParameterBO("ACCID", accId, DbType.Int32)
            parameterList.Add(accIdParam)

            Dim accTypeParam As ParameterBO = New ParameterBO("ACCTYPE", accType, DbType.Int32)
            parameterList.Add(accTypeParam)

            If Not fromDate = "" Then
                Dim fromDateParam As ParameterBO = New ParameterBO("Date", fromDate, DbType.String)
                parameterList.Add(fromDateParam)
            End If

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.SaleInvoiceStatement)

        End Sub
#End Region

#Region "Get Sales Invoice Balance"
        ''' <summary>
        ''' Get Customer Detail according to account type
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="accId"></param>
        ''' <param name="accType"></param>
        ''' <remarks></remarks>
        Sub GetSalesInvoiceBalance(ByRef resultDataSet As DataSet, ByVal accId As Integer, ByVal accType As Integer)

            Dim parameterList As ParameterList = New ParameterList()
            Dim accIdParam As ParameterBO = New ParameterBO("ACCID", accId, DbType.Int32)
            parameterList.Add(accIdParam)

            Dim accTypeParam As ParameterBO = New ParameterBO("ACCTYPE", accType, DbType.Int32)
            parameterList.Add(accTypeParam)
            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.SaleInvoiceBalance)

        End Sub
#End Region

#Region "Get Sales Invoice Balance Brought Forward"
        ''' <summary>
        ''' Get Sales Invoice Balance Brought Forward
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="accId"></param>
        ''' <param name="accType"></param>
        ''' <param name="filterdate"></param>
        ''' <remarks></remarks>
        Sub GetSalesInvoiceBalBrghtFwd(ByRef resultDataSet As DataSet, ByVal accId As Integer, ByVal accType As Integer, ByVal filterdate As DateTime)

            Dim parameterList As ParameterList = New ParameterList()
            Dim accIdParam As ParameterBO = New ParameterBO("ACCID", accId, DbType.Int32)
            parameterList.Add(accIdParam)

            Dim accTypeParam As ParameterBO = New ParameterBO("ACCTYPE", accType, DbType.Int32)
            parameterList.Add(accTypeParam)

            Dim filterDateParam As ParameterBO = New ParameterBO("FILTERDATE", filterdate, DbType.DateTime)
            parameterList.Add(filterDateParam)

            MyBase.LoadDataSet(resultDataSet, parameterList, SpNameConstants.SaleInvoiceOpeningBalance)

        End Sub
#End Region


    End Class
End Namespace
