﻿Imports System
Imports System.Web.HttpContext
Imports System.Web
Imports AD_BusinessObject
Namespace AD_Utilities
    Public Class SessionManager


#Region "Set Account Detail"
        Public Shared Sub setAccountDetail(ByRef dt As DataTable)
            Current.Session(SessionConstants.AccountDetailDataSet) = dt
        End Sub
#End Region

#Region "Get Account Detail"
        Public Shared Function getAccountDetail() As DataTable
            Return CType(Current.Session(SessionConstants.AccountDetailDataSet), DataTable)
        End Function
#End Region

#Region "Login In User "

#Region "Set / Get  Repairs Dashboard User Id"

#Region "Set  Repairs Dashboard User Id"
        Public Shared Sub setAgedDebtorUserId(ByRef repairsDashboardUserId As Integer)
            Current.Session(SessionConstants.AgedDebtorUserId) = repairsDashboardUserId
        End Sub
#End Region

#Region "get Repairs Dashboard User Id"
        Public Shared Function getAgedDebtorUserId() As Integer

            If (IsNothing(Current.Session(SessionConstants.AgedDebtorUserId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionConstants.AgedDebtorUserId), Integer)
            End If
        End Function
#End Region

#Region "remove Repairs Dashboard User Id"

        Public Sub removeAgedDebtorUserId()
            If (Not IsNothing(Current.Session(SessionConstants.AgedDebtorUserId))) Then
                Current.Session.Remove(SessionConstants.AgedDebtorUserId)
            End If

        End Sub

#End Region

#End Region

#Region "Set / Get User Full Name"

#Region "Set User Full Name"

        Public Shared Sub setUserFullName(ByRef userFullName As String)
            Current.Session(SessionConstants.UserFullName) = userFullName
        End Sub

#End Region

#Region "get User Full Name"

        Public Shared Function getUserFullName() As String
            If (IsNothing(Current.Session(SessionConstants.UserFullName))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionConstants.UserFullName), String)
            End If
        End Function

#End Region

#Region "remove User Full Name"

        Public Shared Sub removeUserFullName()
            If (Not IsNothing(Current.Session(SessionConstants.UserFullName))) Then
                Current.Session.Remove(SessionConstants.UserFullName)
            End If

        End Sub

#End Region

#End Region

#Region "Set / Get User Employee Id"

#Region "Set User Employee Id"

        Public Shared Sub setUserEmployeeId(ByRef employeeId As String)
            Current.Session(SessionConstants.EmployeeId) = employeeId
        End Sub

#End Region

#Region "get User Employee Id"

        Public Shared Function getUserEmployeeId() As Integer
            If (IsNothing(Current.Session(SessionConstants.EmployeeId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionConstants.EmployeeId), Integer)
            End If
        End Function

#End Region

#Region "remove User Employee Id"

        Public Shared Sub removeUserEmployeeId()
            If (Not IsNothing(Current.Session(SessionConstants.EmployeeId))) Then
                Current.Session.Remove(SessionConstants.EmployeeId)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get User Type"

#Region "Set Logged In User Type"

        Public Shared Sub setLoggedInUserType(ByRef userType As String)
            Current.Session(SessionConstants.LoggedInUserType) = userType
        End Sub

#End Region

#Region "get Logged In User Type"

        Public Shared Function getLoggedInUserType() As String
            If (IsNothing(Current.Session(SessionConstants.LoggedInUserType))) Then
                Return String.Empty
            Else
                Return CType(Current.Session(SessionConstants.LoggedInUserType), String)
            End If
        End Function

#End Region

#Region "remove Logged In User Type"

        Public Shared Sub removeLoggedInUserType()
            If (Not IsNothing(Current.Session(SessionConstants.LoggedInUserType))) Then
                Current.Session.Remove(SessionConstants.LoggedInUserType)
            End If
        End Sub

#End Region

#End Region

#End Region

#Region "Set Account Detail"
        Public Shared Sub setSalesAccountDetail(ByRef dt As DataTable)
            Current.Session(SessionConstants.SalesAccountDetailDataSet) = dt
        End Sub
#End Region

#Region "Get Account Detail"
        Public Shared Function getSalesAccountDetail() As DataTable
            Return CType(Current.Session(SessionConstants.SalesAccountDetailDataSet), DataTable)
        End Function
#End Region

#Region "Set Sql Job Exec Date"
        Public Shared Sub setSqlJobExecDate(ByRef dateTime As DateTime)
            Current.Session(SessionConstants.SqlJobExecDate) = dateTime
        End Sub
#End Region

#Region "Get Sql Job Exec Date"
        Public Shared Function getSqlJobExecDate() As DateTime
            Return CType(Current.Session(SessionConstants.SqlJobExecDate), DateTime)
        End Function
       
#End Region

    End Class

End Namespace
