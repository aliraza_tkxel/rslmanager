﻿
Namespace AD_Utilities


    Public Class UserMessageConstants

#Region "not categorized"

        Public Shared UserDoesNotExist As String = "You don't have access to Aged Debtor. Please contant administrator."
        Public Shared UsersAccountDeactivated As String = "Your access to Aged Debtor has been de-activiated. Please contant administrator."
        Public Shared LoginRslManager As String = "Please use the login from rsl manager."
#End Region

#Region "Generic Messages"
        Public Shared RecordNotExist As String = "Details Could not be found! Contact Support Team immediately."
        Public Shared NoInvoiceDetail As String = "Details Could not be found! Contact Support Team immediately."
        Public Shared SqlJobExecTrue As String = "SQL Job started successfully."
             Public Shared SqlJobExecFalse As String = "SQL Job was in running state and has been stopped. please execute job again."


#End Region


    End Class
End Namespace

