﻿Imports System
Imports System.Text

Namespace AD_Utilities
    Public Class ApplicationConstants

#Region "Report"
        Public Shared SalesCustomerName As String = "Search customer"
        Public Shared EmployeeName As String = "Employee name"
        Public Shared OrganisationName As String = "Organisation name"
        Public Shared TenantOnly As Boolean

        Public Shared DocumentWord As String = "Document"
        Public Shared DefaultDropDownDataValueField As String = "id"
        Public Shared DefaultDropDownDataTextField As String = "title"
        Public Shared ManagerUserType As String = "Manager" ' this value should be same as stored in db

#End Region


#Region "Page Sort"

        Public Shared Ascending As String = "ASC"
        Public Shared Descending As String = "DESC"
        Public Shared DefaultPageNumber As String = 1
        Public Shared DefaultPageSize As String = 5
        Public Shared PageSortBo As String = "PageSortBo"
#End Region
    End Class


End Namespace

