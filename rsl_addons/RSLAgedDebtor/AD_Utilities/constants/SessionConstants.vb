﻿Imports System

Namespace AD_Utilities

    Public Class SessionConstants



#Region "User Session"

        Public Shared ConSessionUserId As String = "SESSION_USER_ID"
        Public Shared AgedDebtorUserId As String = "AgedDebtorUserId"
        Public Shared UserFullName As String = "UserFullName"
        Public Shared EmployeeId As String = "UserEmployeeId"
        Public Shared LoggedInUserType As String = "LoggedInUserType"

#End Region

        Public Shared AccountDetailDataSet As String = "AccountDetailDataSet"
        Public Shared SalesAccountDetailDataSet As String = "SalesAccountDetailDataSet"
        Public Shared SqlJobExecDate As String = "SqlJobExecDate"

    End Class
End Namespace


