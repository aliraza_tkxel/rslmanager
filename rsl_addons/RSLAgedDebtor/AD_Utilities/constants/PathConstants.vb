﻿Namespace AD_Utilities


    Public Class PathConstants

#Region "URL Constants"
        Public Const RechargeStatementDoc As String = "~/Views/RechargeStatement.aspx"
        Public Const LoginPath As String = "~/../BHAIntranet/Login.aspx"
        Public Const ReportAreaPath As String = "~/Views/Report/AgedDebtorAnalysis.aspx"
        Public Const Bridge As String = "~/Bridge.aspx"
        Public Const SalesInvoice As String = "/Finance/SalesInvoice/Popups/SalesInvoice.asp?SaleID="
#End Region

#Region "Query String Constants"

        Public Const accountType As String = "accountType"
        Public Const accountId As String = "accountId"
        Public Const create As String = "create"
        Public Const yes As String = "yes"
        Public Const print As String = "print"
        Public Const ReportView As String = "report"

#End Region

#Region "appSetting Keys"
   
#End Region


    End Class
End Namespace