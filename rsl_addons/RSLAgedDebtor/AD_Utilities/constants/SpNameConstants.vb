﻿Imports System
Namespace AD_Utilities

    Public Class SpNameConstants

#Region "ViewDetails"

        'Executed for Retrieving data for view screen
        Public Shared GetCustomerDetail As String = "AD_getAccountDetail"
        Public Shared SaleInvoiceStatement As String = "AD_SALESINVOICESTATEMENT"
        Public Shared SaleInvoiceBalance As String = "AD_SALESINVOICESTATEMENT_BALANCE"
        Public Shared SaleInvoiceOpeningBalance As String = "AD_SALESINVOICESTATEMENT_OPENINGBALANCE"
        Public Shared ExecuteSqlJob As String = "AD_EXECUTESQLJOB"
        Public Shared SqlJobExecTime As String = "AD_GETSQLJOBEXECUTIONDATETIME"
        Public Shared SearchSalesAccount As String = "AD_SEARCHFORAGEDDEBOTREPORT"  '' Aged Debtor - detail and summary report
        'Public Shared SearchSalesAccountSummary As String = "AD_SEARCHFORAGEDDEBOTREPORTSUMMARY"
#End Region
#Region "Login"
        Public Shared GetEmployeeById As String = "FL_GetEmployeeById"
#End Region
    End Class
End Namespace



