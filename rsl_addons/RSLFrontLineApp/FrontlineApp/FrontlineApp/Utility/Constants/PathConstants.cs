﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.Utility.Constants
{
    public static class PathConstants
    {
        #region "Base URL"
            #if FrontLineDev
                public const string BaseURL = ApplicationConstants.ServerURLDev;
            #elif FrontLineTest
                public const string BaseURL = ApplicationConstants.ServerURLTest;
            #elif FrontLineUAT
                public const string BaseURL = ApplicationConstants.ServerURLUAT;
            #elif FrontLineLive
                public const string BaseURL = ApplicationConstants.ServerURLLive;
            #else
                public const string BaseURL = ApplicationConstants.ServerURLLive;
            #endif
        #endregion

        #region " Local Image Path Constants"

        public const string ImageBase_1x = @"ms-appx:///Assets/1x/";
        public const string ImageBase_1_5x = @"ms-appx:///Assets/1.5x/";
        public const string ImageBase_2x = @"ms-appx:///Assets/2x/";
        public const string LocalFolderPath = @"ms-appdata:///local/";
        
        #endregion

        #region "Customer"
        public const string CustomerProfileThumb = BaseURL+ "/CustomerProfilePic/Thumbs/";
        public const string CustomerProfileNormal = BaseURL + "/CustomerProfilePic/Images/";
        #endregion

        #region "Dashboard"
        public const string ImagePathRisk = "/Assets/2x/open_risks.png";
        public const string ImagePathVulnerability = "/Assets/2x/open_vulnerabilities.png";        
     
        #endregion


    }
}
