﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.Utility.Constants
{
    public static class ApplicationConstants
    {
        #region "Base URL"
        public const string ServerURLDev = "https://devcrm.broadlandhousinggroup.org";
        public const string ServerURLTest = "https://testcrm.broadlandhousinggroup.org";
        public const string ServerURLUAT = "https://uatcrm.broadlandhousinggroup.org";
        public const string ServerURLLive = "https://crm.broadlandhousinggroup.org";
        #endregion

        #region "Lookup key constants"

        public const string CustomerTitleLookupKey = "CustomerTitle";
        public const string PreferredContactLookupKey = "PreferredContact";
        public const string InternetAccessLookupKey = "InternetAccess";
        public const string MaritalStatusLookupKey = "MaritalStatus";
        public const string EthnicityLookupKey = "Ethnicity";
        public const string ReligionLookupKey = "Religion";
        public const string SexualityLookupKey = "Sexuality";
        public const string NationalityLookupKey = "Nationality";
        public const string FirstLanguageLookupKey = "FirstLanguage";
        public const string YesNoLookupKey = "YesNo";
        public const string LandlordTypeLookupKey = "LandlordType";
        public const string HouseTypeLookupKey = "HouseType";
        public const string SpecialRequirementLookupKey = "SpecialRequirement";
        public const string ReferralStagesLookupKey = "ReferralStages";
        public const string YesNoUnknownLookupKey = "YesNoUnknown";
        public const string ReferralHelpCategoryLookupKey = "ReferralHelpCategory";
        public const string ReferralHelpSubCategoryLookupKey = "ReferralHelpSubCategory";
        public const string TerminationReasonLookupKey = "TerminationReason";
        public const string TerminationReasonCategoryLookupKey = "TerminationReasonCategory";
        public const string EconomicStatusLookupKey = "EconomicStatus";
        public const string BenefitsLookupKey = "Benefits";
        public const string BankingLookupKey = "Banking";
        public const string HealthProblemLookupKey = "HealthProblem";
        public const string AgencySupportLookupKey = "AgencySupport";
        public const string AdaptationLookupKey = "Adaptation";
        public const string RelationshipStatusLookupKey = "RelationshipStatus";
        public const string EmploymentStatusLookupKey = "EmploymentStatus";
        public const string EvictionReasonLookupKey = "EvictionReason";

        #endregion

        #region "Viewing"
        public const int ViewingsMaxDaysThreshold = 20;
        public const int ViewingsMinDaysThreshold = -20;
        public const int ViewingStatusWaiting = 1;
        public const int ViewingStatusRejected = 2;
        public const int ViewingStatusAccepted = 3;
        public const int ViewingStatusCancelled = 4;

        public const string ViewingGroupToday = "Today";
        public const string ViewingGroupTomorrow = "Tomorrow";
        public const string ViewingGroupPast = "Past Viewings";
        public const string ViewingGroupUncategorized = "Uncategorized";

        public const string ViewingSourceNewOrEditKey = "masterPage";
        public const string ViewingSourceLoggedKey = "Dashboard";
        public const string ViewingSourceApprovalKey = "DashboardApproval";
        public const string ViewingSourcePropertyKey = "PropertyPage";
        public const string ViewingSourceCustomerKey = "CustomerPage";

        public const string ViewingSelectedPropertyKey = "selProp";
        public const string ViewingSelectedCustomerKey = "SelectedCustomer";
        public const string ViewingSelectedObjectKey = "selectedObj";


        public const string ViewingCommentTypeGeneral = "GeneralFeedback";
        public const string ViewingCommentTypeEdit = "EditFeedback";
        public const string ViewingCommentTypeRearrange = "RearrangeFeedback";


        #endregion

        #region "Property"
        #region "Filter"
        public const string SchemePlaceholder = "Select Scheme";
        public const string PropertyTypePlaceHolder = "Select Property Type";
        #endregion
        #endregion

        #region "Title"

        public const string AddCustomerTitle = "ADD CUSTOMER";
        public const string EditCustomerTitle = "EDIT CUSTOMER";
        public const string AddReferralTitle = "ADD REFERRAL";
        public const string EditReferralTitle = "EDIT REFERRAL";
        public const string AddReferralSubTitle = "Add Tenancy Referrals";
        public const string EditReferralSubTitle = "Edit Tenancy Referrals";
        public const string ErrorTitle = "Error";
        public const string InformationTitle = "Information";

        #endregion

        #region "General"

        public const ulong MaxProfileImageFileSizeInBytes = 1000000;

        #endregion

        #region "Customer"

        public const string HouseTypeFlatTitle = "Flat";
        public const string CustomerTypeProspectiveTenantTitle = "Prospective Tenant";
        public const string CustomerTypeTenantTitle = "Tenant";
        public const string MoneyIssueDescription = "Money Issues";
        public const string PersonalProblemsDescription = "Personal Problems";
        public const string WorkOrTrainingDescription = "Work or training";
        public const string PracticalProblemsDescription = "Practical problems";
        public const string AdaptionTypeArrears = "Arrears";
        public const string AdaptionTypeDamage = "Damage";
        public const string AdaptionTypeASB = "ASB";
        public const string CloseStatusTitle = "Closed";
        public const int TerminationDefaultDaysThreshold = 28;

        public const string TerminationReasonMutualExchangeBHA = "Mutual Exchange – BHA";
        public const string TerminationReasonMutualExchangeLA = "Mutual Exchange – LA";
        public const string TerminationReasonMutualExchangeRP = "Mutual Exchange – RP";

        #endregion

        #region "Alerts"
        public const string OpenStatusDescription = "Open";
        #endregion

        #region "Dashboard"
        public const string TitleOpenRisks = "Open Risks";
        public const string TitleOpenVulnerabilities = "Open Vulnerabilities";
        #endregion
    }
}
