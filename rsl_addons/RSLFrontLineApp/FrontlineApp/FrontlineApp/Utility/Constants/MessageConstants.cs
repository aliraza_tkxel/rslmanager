﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.Utility.Constants
{
    public static class MessageConstants
    {

        #region "General"

        public const string ProblemDataSaving = "Problem while saving data";
        public const string NoRecordFound = "No record found";
        public const string FailedToSyncCustomer = "Failed to sync offline records, please try again later";
        public const string SuccessFullUploadImage = "Image uploaded successfully";
        public const string InternetNotAvailableSyncCustomer = "Please enable internet in order to sync offline customer records.";
        public const string InternetNotAvailableShowOffline = "Internet not available, press Ok to display offline records.";
        public const string ErrorMessageBoxHeader = "Error";
        public const string AlertMessageBoxHeader = "Alert";
        public const string InformationMessageBoxHeader = "Information";
        public const string Loading = "Loading ...";
        public const string Processing = "Processing ...";

        #endregion

        #region "Properties"
        public const string FetchingPropertiesList = "Fetching Properties from server.....";
        public const string FetchingPropertyDetails = "Fetching Property details from server.....";
        #endregion

        #region "Customers"
        public const string SavingReferral = "Saving referral information ...";
        public const string SavingOfflineTenancyData = "Saving Tenancy Data....";
        public const string FetchingOfflineTenancyData = "Fetching Tenancy Data....";
        public const string LoadingCustomers = "Loading customers ...";
        public const string SyncingOfflineCustomerRecords = "Syncing offline customer records ...";
        public const string FetchingCustomerRecordsFromServer = "Loading customer records from server ...";
        public const string FetchingCustomerDetailsFromServer = "Loading customer details from server ...";
        public const string FetchingCustomerViewingsFromServer = "Loading customer viewings from server ...";
        public const string FetchingCustomerRisksFromServer = "Loading customer risks from server ...";
        public const string FetchingCustomerVulnerabilitiesFromServer = "Loading customer vulnerabilities from server ...";
        public const string FetchingCustomerReferralFromServer = "Loading customer referral from server ...";
        public const string SavingCustomerRecord = "Saving customer record ...";
        public const string OfflineCustomersExistWarning = "Sync with the server is required due to modifications in some customer records. Post data first to sync or press Cancel to display offline records.";
        public const string FailedToUploadCustomerImage = "Failed to upload customer image";
        public const string CustomerRecordNotFound = "Customer record not found";
        public const string CustomerSavedSuccessFully = "Customer saved successfully";
        public const string WarningTitle = "Warning";
        public const string SuccessTitle = "Success";
        public const string TenancyRecordNotFound = "Tenancy not found";
        public const string TenancyReferralNotFound = "Tenancy referral not found";
        public const string ReferralNotFound = "No Referrals added yet!";
        public const string FailedToLoadCustomerRecords = "Failed to load customer records from server";
        public const string FailedToLoadCustomerDetailFromServer = "Failed to load customer detail from server";
        public const string FailedToLoadCustomerViewingsFromServer = "Failed to load customer viewings from server";
        public const string CustomerSavedOffline = "Customer record has been saved successfully in local database. Please enable internet to sync with server.";
        public const string CustomerSavedOfflineFailedToSyncedWithServer = "Customer record saved successfully in local database but unable to synced with the server.";
        public const string CustomerSavedServer = "Customer record has been saved successfully and synced with server.";
        public const string ProfileImagesizeExceed = "Can't update customer profile image, please select an image of size less than 1 MB";
        public const string TenancyTerminationOfflineError = "Tenancy termination is not available in offline mode, please connect with internet to proceed";
        public const string TenancySetupOfflineError = "Tenancy setup is not available in offline mode, please connect with internet to proceed";
        public const string AddEditReferralOfflineError = "Add/Edit Referral is not available in offline mode, please connect with internet to proceed";        
        public const string ReferralSavedSuccess = "Referral saved successfully.";
        public const string EnquiryClosedSuccessfully = "Enquiry closed successfully.";
        public const string EnquiryClosedFailed = "Failed to close enquiry.";
        public const string TenancyTerminatedSuccessfully = "Tenancy terminated successfully.";
        public const string TenancyTerminatedJournalIdNull = "Failed to retrieve Termination Journal Id from the Server. Void Inspection Scheduling aborted.";
        public const string InternetNotAvailableError = "Internet not available, please connect with internet to proceed";
        public const string FetchingVoidSupervisors = "Fetching Void Supervisors....";
        public const string CreatingVoidInspection = "Creating Void Inspection Appointment....";
        public const string NoVoidSupervisorsFound = "No Void supervisors found for the given time period.";
        public const string VoidInspectionStartDateNull = "Please select a Start  date first.";
        public const string VoidInspectionTypeNotSelected = "Please select void inspection type first.";
        public const string VoidInspectionStartDateInvalid = "Start Date must not be in the past.";
        public const string VoidInspectionStartTimeNull = "Please select Start time.";
        public const string VoidInspectionStartTimeInvalid = "Start Time must be earlier than End Time.";
        public const string VoidInspectionEndTimeNull = "Please select End time.";
        public const string VoidInspectionSupervisorNull = "Please select a void Supervisor first.";
        public const string VoidInspectionServerError = "No response received. Some internal server error occurred on the back end.";

        public const string ReferralSavedFailure = "Unable to save Referral, please try again.";
        public const string ArrangeViewingOfflineError = "Viewing cannot be arranged in offline mode, please connect with internet to proceed";
        public const string AppOfflineFeatureUnavailable = "App is currently running in offline mode (Read-only). Some features may be unavailable. To avail them, please connect to internet";
        public const string AppOfflineHeader = "Internet Unavailable";

        public const string ForwardingAddressError = "You have not set up a forwarding address for this customer !!";
        public const string ForwardingAddressJointTenantError = "You have not set up a forwarding address for this customer !! This is a joint tenancy and forwarding addresses should be set up for all tenants.";
        public const string CloseEnquiry = "Closing enquiry ...";

        public const string PayThruConnectingToServer = "Connecting to PayThru Server....";
        public const string PayThruConnectionFailure = "Failed to connect to PayThru Server";
        public const string PayThruInvalidParams = "Invalid Parameters provided. Please close this window and contact BHG support";

        #endregion

        #region "Viewings"
        public const string FetchingViewingsList = "Fetching Viewings from server.....";
        public const string FetchingViewingDetails = "Fetching Viewing details from server.....";
        public const string CreatingNewViewing = "Creating Viewing.....";
        public const string EditingViewing = "Editing Viewing.....";
        public const string UpdatingViewingStatus = "Updating Viewing Status.....";
        public const string RecordingFeedBackForViewing = "Recording Feedback.....";
        #endregion

        #region "Authentication"
        public const string LoggingIn = "Logging in.....";
        public const string ChangePassword = "Requesting password reset.....";
        #endregion

        #region "Dashboard"
        public const string UpdatingDashboardStats = "Updating Dashboard Stats from server.....";
        #endregion

    }
}
