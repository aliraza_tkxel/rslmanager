﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.Utility.Constants
{
    public static class UriTemplateConstants
    {
        #region "Base Service"

#if FrontLineDev
        public const string BaseURL = "https://devcrm.broadlandhousinggroup.org/RSLFrontLineApi";
#elif FrontLineTest
            public const string BaseURL = "https://testcrm.broadlandhousinggroup.org/RSLFrontLineApi";
#elif FrontLineUAT
        public const string BaseURL = "https://uatcrm.broadlandhousinggroup.org/RSLFrontLineApi";
#elif FrontLineLive
        public const string BaseURL = "https://crm.broadlandhousinggroup.org/RSLFrontLineApi";
#else
 public const string BaseURL = "https://devcrm.broadlandhousinggroup.org/RSLFrontLineApi";
#endif
        #endregion

        #region "Shared Lookups"
        public const string GetSharedLookUps = "api/SharedLookup/GetSharedLookups";
        #endregion

        #region "Authentication"
        public const string AuthenticateUser = "api/authentication/AuthenticateUser";
        public const string EmailForgottenPassword = "api/authentication/EmailForgotPassword";
        #endregion

        #region "Property"
        public const string SearchProperty = "api/Properties/SearchProperties";
        public const string GetPropertyDetails = "api/Properties/GetPropertyDetails";
        public const string GetPropertyViewingsList = "api/Properties/GetPropertyViewings";
        public const string GetPropertyFinancialInfo = "api/Properties/GetPropertyFinancialInfo";
        #endregion

        #region "Customer"
        public const string SearchCustomer = "api/Customer/SearchCustomer";
        public const string GetStaffMember = "api/Viewings/GetHousingOfficer";
		public const string AddEditcustomer = "api/Customer/AddEditCustomer";
        public const string UploadCustomerProfilePic = "api/Customer/UploadProfilePic";
        public const string GetCustomerDetail = "api/Customer/GetCustomerDetail";
        public const string GetCustomerViewings = "api/Customer/GetCustomerViewings";
        public const string GetReferralDetails = "api/Customer/GetReferralDetails";
        public const string CreateTenancy = "api/Customer/CreateTenancy";
		public const string SaveReferralDetails = "api/Customer/AddEditReferral";
        public const string TerminateTenancy = "api/Customer/TerminateTenancy";
        public const string AddUpdateRisk = "api/Alerts/AddEditRisks";
        public const string AddUpdateVulnerability = "api/Alerts/AddEditVulnerabilities";
        public const string GetVoidInspectionSupervisors = "api/Customer/GetSupervisorsForVoidInspectionList";
        public const string GetCustomerRisksList = "api/Customer/GetCustomerRisksList";
        public const string GetCustomerVulnerabilitiesList = "api/Customer/GetCustomerVulnerabilitiesList";
        public const string CreateVoidInspectionAppointment = "api/Customer/SaveVoidInspection";
        #endregion

        #region "Viewings"
        public const string GetAllViewings = "api/Viewings/GetViewingList";
        public const string GetViewingDetail = "api/Viewings/GetViewingDetails";
        public const string ArrangeOrEditViewing = "api/Viewings/ArrangeViewing";
        public const string AddCommentToViewing = "api/Viewings/AddCommentToViewing";
        #endregion

        #region "Alerts"
        public const string GetRisksList = "api/Alerts/GetRisksList";
        public const string GetVulnerabilitiesList = "api/Alerts/GetVulnerabilitiesList";
        #endregion


    }
}
