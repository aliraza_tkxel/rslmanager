﻿using AutoMapper;
using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.DataLayer.Entities;
using FrontlineApp.DataLayer.OfflineTenancyEntities;
using FrontlineApp.ServiceLayer.ServiceObjects;
using FrontlineApp.Utility.Helper.Extensions;
using System.Linq;

namespace FrontlineApp.Utility.Configuration
{
    public static class AutomapperConfiguration
    {
        public static void Configure()
        {
            ConfigureCustomerMapping();

        }

        private static void ConfigureCustomerMapping()
        {
            // Model - Entity
            Mapper.CreateMap<CustomerAddressModel, CustomerAddress>();
            Mapper.CreateMap<CurrentHomeModel, CurrentHome>();
            Mapper.CreateMap<CustomerImageModel, CustomerImage>();

            // Model - Offline Temp Entity
            Mapper.CreateMap<CustomerModel, TenancyCustomerEnt>();
            Mapper.CreateMap<PropertyModel, TenancyPropertyEnt>();
            Mapper.CreateMap<TenancyAddressModel, TenancyAddressEnt>();
            Mapper.CreateMap<EmploymentInfoModel, TenancyEmploymentInfoEnt>();
            Mapper.CreateMap<CustomerGeneralInfoModel, TenancyCustomerGeneralInfoEnt>();
            Mapper.CreateMap<TenancyEmergencyContactModel, TenancyEmergencyContactEnt>();
            Mapper.CreateMap<ReferralModel, TenancyReferralEnt>()
                .ForMember(
                dest => dest.MoneyIssues,
                opt => opt.MapFrom(src => (src.MoneyIssues!=null)?string.Join(",", src.MoneyIssues):""))
                .ForMember(
                dest => dest.PersonalProblems,
                opt => opt.MapFrom(src => (src.PersonalProblems != null) ? string.Join(",", src.PersonalProblems) : ""))
                .ForMember(
                dest => dest.WorkOrTrainings,
                opt => opt.MapFrom(src => (src.WorkOrTrainings != null) ? string.Join(",", src.WorkOrTrainings) : ""))
                .ForMember(
                dest => dest.PracticalProblems,
                opt => opt.MapFrom(src => (src.PracticalProblems != null) ? string.Join(",", src.PracticalProblems) : "")
                ); 
            Mapper.CreateMap<PropertyRentModel, TenancyRentEnt>();
            Mapper.CreateMap<TenancyDisclosureModel, TenancyDisclosureEnt>();
            Mapper.CreateMap<TenantOnlineSetupModel, TenancyOnlineSetupEnt>();
            Mapper.CreateMap<OccupantModel, TenancyOccupantEnt>();

            //   Offline Temp Entity - Model
            Mapper.CreateMap<TenancyCustomerEnt,CustomerModel>();
            Mapper.CreateMap<TenancyPropertyEnt,PropertyModel>();
            Mapper.CreateMap<TenancyAddressEnt,TenancyAddressModel>();
            Mapper.CreateMap<TenancyEmploymentInfoEnt,EmploymentInfoModel>();
            Mapper.CreateMap<TenancyCustomerGeneralInfoEnt,CustomerGeneralInfoModel>();
            Mapper.CreateMap<TenancyEmergencyContactEnt,TenancyEmergencyContactModel>();
            Mapper.CreateMap<TenancyReferralEnt,ReferralModel>()
                .ForMember(
                dest => dest.MoneyIssues,
                opt => opt.MapFrom(src => src.MoneyIssues.Split(',').ToList()))
                .ForMember(
                dest => dest.PersonalProblems,
                opt => opt.MapFrom(src => src.PersonalProblems.Split(',').ToList()))
                .ForMember(
                dest => dest.WorkOrTrainings,
                opt => opt.MapFrom(src => src.WorkOrTrainings.Split(',').ToList()))
                .ForMember(
                dest => dest.PracticalProblems,
                opt => opt.MapFrom(src => src.PracticalProblems.Split(',').ToList())
                );
            Mapper.CreateMap<TenancyRentEnt,PropertyRentModel>();
            Mapper.CreateMap<TenancyDisclosureEnt,TenancyDisclosureModel>();
            Mapper.CreateMap<TenancyOnlineSetupEnt,TenantOnlineSetupModel>();
            Mapper.CreateMap<TenancyOccupantEnt,OccupantModel>();

            // Entity - Model
            Mapper.CreateMap<Customer, CustomerModel>();
            Mapper.CreateMap<CustomerAddress, CustomerAddressModel>();
            Mapper.CreateMap<CurrentHome, CurrentHomeModel>();
            Mapper.CreateMap<CustomerImage, CustomerImageModel>();
            Mapper.CreateMap<EmploymentInfo, EmploymentInfoModel>();
            Mapper.CreateMap<CustomerGeneralInfo, CustomerGeneralInfoModel>();
            Mapper.CreateMap<CustomerReferral, ReferralModel>();
            Mapper.CreateMap<Tenancy, TenancyModel>();
            Mapper.CreateMap<TenancyAddress, TenancyAddressModel>();

            //Service Object - Model

            Mapper.CreateMap<PropertyRentSO, PropertyRentModel>();
            Mapper.CreateMap<TenancySO, TenancyModel>();
            Mapper.CreateMap<EmploymentInfoSO,EmploymentInfoModel>();
            Mapper.CreateMap<CustomerGeneralInfoSO,CustomerGeneralInfoModel>();
            Mapper.CreateMap<ReferralSO,ReferralModel>();
            Mapper.CreateMap<TenancySO,TenancyModel>();
            Mapper.CreateMap<EmergencyContactSO,TenancyEmergencyContactModel>();
            Mapper.CreateMap<JointTenantSO,JointTenantModel>();
            Mapper.CreateMap<OccupantSO,OccupantModel>();
            Mapper.CreateMap<TenancyDisclosureSO,TenancyDisclosureModel>();
            Mapper.CreateMap<TenantOnlineInfoSO,TenantOnlineSetupModel>();
            Mapper.CreateMap<TenancyAddressSO, TenancyAddressModel>();

            // Model - Service Object
            Mapper.CreateMap<ViewingCommentModel, ViewingCommentSO>();
            Mapper.CreateMap<TerminationModel, TerminationSO>();
            Mapper.CreateMap<TenantForwardingAddress, ForwardingAddressSO>();
            Mapper.CreateMap<PaginationModel, PaginationSO>();
            Mapper.CreateMap<CustomerModel, CustomerSO>().ForMember(
                dest => dest.customerAddress,
                opt => opt.MapFrom(src => src.CustomerAddresses)
                ); ;
            Mapper.CreateMap<CustomerAddressModel, CustomerAddressSO>();
            Mapper.CreateMap<CurrentHomeModel, CurrentHomeSO>();
            Mapper.CreateMap<CustomerImageModel, CustomerImageSO>();
            Mapper.CreateMap<EmploymentInfoModel, EmploymentInfoSO>();
            Mapper.CreateMap<CustomerGeneralInfoModel, CustomerGeneralInfoSO>();
            Mapper.CreateMap<ReferralModel, ReferralSO>();
            Mapper.CreateMap<TerminationModel, TerminationSO>();
            Mapper.CreateMap<TenancyModel, TenancySO>();
            Mapper.CreateMap<TenancyEmergencyContactModel, EmergencyContactSO>()
            .ForMember(
                dest => dest.address1,
                opt => opt.MapFrom(src => src.CompleteAddress)
                );
            Mapper.CreateMap<JointTenantModel, JointTenantSO>()
                .ForMember(
                dest => dest.isCorrespondingAddress,
                opt => opt.MapFrom(src => (src.IsCorrespondingAddress)?1:0))
                .ForMember(
                dest => dest.startDate,
                opt => opt.MapFrom(src => src.StarteDate)

                );
            Mapper.CreateMap<OccupantModel, OccupantSO>();
            Mapper.CreateMap<PropertyRentModel, PropertyRentSO>();
            Mapper.CreateMap<TenancyDisclosureModel, TenancyDisclosureSO>();
            Mapper.CreateMap<TenantOnlineSetupModel, TenantOnlineInfoSO>();
			Mapper.CreateMap<CustomerRiskModel, CustomerRiskSO>();
            Mapper.CreateMap<CustomerVulnerabilityModel, CustomerVulnerabilitySO>();
            Mapper.CreateMap<TenancyAddressModel, TenancyAddressSO>();

            // Entity - Service Object
            Mapper.CreateMap<Customer, CustomerSO>()
                .ForMember(
                dest => dest.customerAddress,
                opt => opt.MapFrom(src => src.CustomerAddresses)
                ); 
            Mapper.CreateMap<CustomerAddress, CustomerAddressSO>();
            Mapper.CreateMap<CurrentHome, CurrentHomeSO>();
            Mapper.CreateMap<CustomerImage, CustomerImageSO>();
            Mapper.CreateMap<EmploymentInfo, EmploymentInfoSO>();
            Mapper.CreateMap<CustomerGeneralInfo, CustomerGeneralInfoSO>();
            Mapper.CreateMap<Tenancy, TenancySO>();
            Mapper.CreateMap<TenancyAddress, TenancyAddressSO>();

            // Service Object - Entity
            Mapper.CreateMap<CustomerSO,Customer>().ForMember(
                dest => dest.CustomerAddresses,
                opt => opt.MapFrom(src => src.customerAddress)
                );
            Mapper.CreateMap<CustomerAddressSO,CustomerAddress>();
            Mapper.CreateMap<CurrentHomeSO,CurrentHome>();
            Mapper.CreateMap<CustomerImageSO,CustomerImage>();
            Mapper.CreateMap<EmploymentInfoSO,EmploymentInfo>();
            Mapper.CreateMap<CustomerGeneralInfoSO,CustomerGeneralInfo>();
            Mapper.CreateMap<TenancySO,Tenancy>();
            Mapper.CreateMap<TenancyAddressSO, TenancyAddress>();
            Mapper.CreateMap<CustomerViewingSO, CustomerViewing>();
            Mapper.CreateMap<ReferralSO, CustomerReferral>();
            Mapper.CreateMap<TerminationSO, TenancyTermination>();
            Mapper.CreateMap<AlertRiskSO, AlertRisk>();
            Mapper.CreateMap<AlertVulnerabilitySO, AlertVulnerability>();
            Mapper.CreateMap<CustomerRiskSO, CustomerRisk>();
            Mapper.CreateMap<CustomerVulnerabilitySO, CustomerVulnerability>();
        }

    }
}
