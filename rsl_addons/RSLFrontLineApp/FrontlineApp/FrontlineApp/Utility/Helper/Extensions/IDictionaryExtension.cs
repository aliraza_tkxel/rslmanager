﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FrontlineApp.Utility.Helper.Extensions
{
    public static partial class IDictionaryExtension
    {

        public static string ToQueryString(this IDictionary<string, string> dictionary)
        {
            return '?' + string.Join("&", dictionary.Select(p => p.Key + '=' + Uri.EscapeUriString(p.Value)).ToArray());
        }

    }
}
