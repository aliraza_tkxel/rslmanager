﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.Utility.Helper.Extensions
{
    public static class ObservableCollectionExtension
    {

        public static void ForEach<T>(this IEnumerable<T> enumerable, Action<T> action)
        {
            foreach (var e in enumerable)
            {
                action(e);
            }
        }

        public static ObservableCollection<T> ToObservableCollection<T>(this IEnumerable<T> enumerable)
        {
            if (enumerable == null)
                return null;

            return new ObservableCollection<T>(enumerable);
        }

        public static string ToCommaSeparatedString<T>(this IEnumerable<T> enumerable)
        {
            return enumerable != null && enumerable.Count() > 0 ? string.Join(",", enumerable) : null;
        }

        
    }
}
