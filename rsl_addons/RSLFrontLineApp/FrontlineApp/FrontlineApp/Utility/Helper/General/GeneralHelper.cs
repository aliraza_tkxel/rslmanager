﻿using FrontlineApp.ApplicationLayer.Enums;
using FrontlineApp.ApplicationLayer.Models;
using System;
using FrontlineApp.Utility.Constants;
using System.Collections.Generic;
using Windows.ApplicationModel.DataTransfer;
using Windows.Networking.Connectivity;
using Windows.ApplicationModel;

namespace FrontlineApp.Utility
{
    public static class GeneralHelper
    {
        /// <summary>
        /// Get Enums information
        /// </summary>
        /// <param name="description"></param>
        /// <returns></returns>
        public static ItemModel GetEnumsInfo(string description)
        {
            Dictionary<string, ItemModel> enumDict= new Dictionary<string, ItemModel>();
            enumDict.Add(CustomerType.General.ToString(), new ItemModel() { Title = CustomerType.General.ToString(), NormalStateImage = "prospect.png", SelectedStateImage = "prospect_white.png" });
            enumDict.Add(CustomerType.PreviousTenant.ToString(), new ItemModel() { Title = CustomerType.PreviousTenant.ToString(), NormalStateImage = "terminated_tenant.png", SelectedStateImage = "terminated_tenant_white.png" });
            enumDict.Add(CustomerType.ProspectiveTenant.ToString(), new ItemModel() { Title = CustomerType.ProspectiveTenant.ToString(), NormalStateImage = "prospect.png", SelectedStateImage = "prospect_white.png" });
            enumDict.Add(CustomerType.Stakeholder.ToString(), new ItemModel() { Title = CustomerType.Stakeholder.ToString(), NormalStateImage = "prospect.png", SelectedStateImage = "prospect_white.png" });
            enumDict.Add(CustomerType.Tenant.ToString(), new ItemModel() { Title = CustomerType.Tenant.ToString(), NormalStateImage = "tenant.png", SelectedStateImage = "tenant_white.png" });
            enumDict.Add(Gender.Male.ToString(), new ItemModel() { Title = Gender.Male.ToString(), NormalStateImage = "male.png", SelectedStateImage = "male_white.png" });
            enumDict.Add(Gender.Female.ToString(), new ItemModel() { Title = Gender.Female.ToString(), NormalStateImage = "female.png", SelectedStateImage = "female_white.png" });
            enumDict.Add(Gender.Transgender.ToString(), new ItemModel() { Title = Gender.Transgender.ToString(), NormalStateImage = "female.png", SelectedStateImage = "female_white.png" });            
            return enumDict[description];
        }

        /// <summary>
        /// Copy a string to Windows Clipboard
        /// </summary>
        /// <param name="str"></param>
        public static void CopyToClipBoard(string str)
        {
            var dp = new DataPackage
            {
                RequestedOperation = DataPackageOperation.Copy,
            };
            dp.SetText(str);
            Clipboard.SetContent(dp);
        }

        /// <summary>
        /// Detect if current device is connected to the internet
        /// </summary>
        /// <returns></returns>
        public static bool HasInternetConnection()
        {
            ConnectionProfile connections = NetworkInformation.GetInternetConnectionProfile();
            bool internet = connections != null && connections.GetNetworkConnectivityLevel() == NetworkConnectivityLevel.InternetAccess;
            return internet;
        }

        public static DateTime AddOrSubtractDaysToCurrentDate(string operatorSymbol)
        {
            DateTime currentDate = DateTime.UtcNow;
            DateTime resultDate = currentDate;
            if (operatorSymbol.Equals("+"))
            {
                resultDate = currentDate.AddDays(ApplicationConstants.ViewingsMaxDaysThreshold);
            }
            else if (operatorSymbol.Equals("-"))
            {
                resultDate = resultDate.AddDays(ApplicationConstants.ViewingsMinDaysThreshold);
            }
            return resultDate;
        }
        public static string GetDateStringFromServerDate (string serverDate)
        {
            
            DateTime dt = DateTime.Parse(serverDate).ToLocalTime();
            string dateStr = "NA";
            if ( dt.Date == DateTime.Today)
            {
                dateStr = ApplicationConstants.ViewingGroupToday;
            }
            else if(dt.Date == DateTime.Today.AddDays(1))
            {
                dateStr = ApplicationConstants.ViewingGroupTomorrow;
            }
            else
            {
                dateStr = String.Format("{0: MMM dd}", dt);
            }
            
            return dateStr;
        }
        public static string GetViewingGroupKeyFromDate(string serverDate)
        {
            string groupKey = ApplicationConstants.ViewingGroupUncategorized;
            DateTime dt = DateTime.Parse(serverDate).ToLocalTime();
            if (dt != null)
            {
                if (dt.Date == DateTime.Today)
                {
                    groupKey = ApplicationConstants.ViewingGroupToday;
                }
                else if (dt.Date == DateTime.Today.AddDays(1))
                {
                    groupKey = ApplicationConstants.ViewingGroupTomorrow;
                }
                else if (dt < DateTime.Today)
                {
                    groupKey = ApplicationConstants.ViewingGroupPast;
                }
                else
                {
                    groupKey = String.Format("{0: MMM dd}", dt);
                }
            }
            return groupKey;
        }
        public static string GetCompleteDateStringFromServerDate(string serverDate)
        {

            DateTime dt;
            string dateStr = "NA";
            if (!string.IsNullOrWhiteSpace(serverDate))
            {
                dateStr = String.Format("{0: MMM dd, yyyy}", DateTime.Parse(serverDate).ToLocalTime());
                dt = DateTime.Parse(serverDate).ToLocalTime();
                if (dt.Date == DateTime.Today)
                {
                    dateStr += string.Format(" ({0})", ApplicationConstants.ViewingGroupToday);
                }
                else if (dt.Date == DateTime.Today.AddDays(1))
                {
                    dateStr += string.Format(" ({0})", ApplicationConstants.ViewingGroupTomorrow);
                }
            }
                
           
            return dateStr;
        }
        public static string GetCompleteCreationDateStringFromServerDate(string serverDate)
        {
            string dateStr = "NA";
            if (!string.IsNullOrWhiteSpace(serverDate))
            {
                dateStr = String.Format("{0: MMM dd, yyyy}", DateTime.Parse(serverDate).ToLocalTime());
                
            }


            return dateStr;
        }
        public static string GetTimeStringFromServerDate(string serverDate)
        {
            
            string dateStr = "NA";
            dateStr = String.Format("{0:HH:mm}", DateTime.Parse(serverDate));
            return dateStr;
        }
        public static string GetFormattedTimeFromTimeString(string userTime)
        {
            
            if (!string.IsNullOrWhiteSpace(userTime))
            {
                DateTime dateStr = DateTime.Parse(userTime).ToLocalTime(); 
                return String.Format("{0: HH:mm}", dateStr);
            }
            return "NA";
        }
        public static DateTime GetCombinedDateTimeStringForViewing(string date, string time)
        {
            DateTime currentDT = DateTime.Parse(date);
            DateTime currentTime = DateTime.Parse(time);
            DateTime JointDT = new DateTime(currentDT.Year, currentDT.Month, currentDT.Day,currentTime.Hour,currentTime.Minute,currentTime.Second).ToUniversalTime();
            return JointDT;
        }
		/// <summary>
        /// Get Current App Version. e.g. 2.4.1.0
        /// </summary>
        /// <returns>App Version</returns>
        public static string GetAppVersion()
        {
            var ver = Windows.ApplicationModel.Package.Current.Id.Version;
            return $"{ver.Major}.{ver.Minor}.{ver.Build}.{ver.Revision}";
        }
        public static string Base64Encode(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }
		/// <summary>
        /// Get Current App Logo Image Uri
        /// </summary>
        /// <returns>Uri</returns>
        public static Uri GetAppLogoUri() => Windows.ApplicationModel.Package.Current.Logo;

        /// <summary>
        /// Get Current App Display Name
        /// </summary>
        /// <returns>App Display Name</returns>
        public static string GetAppDisplayName() => Windows.ApplicationModel.Package.Current.DisplayName;

        /// <summary>
        /// Get Current App Publisher Name
        /// </summary>
        /// <returns>App Publisher Name</returns>
        public static string GetAppPublisher() => Windows.ApplicationModel.Package.Current.PublisherDisplayName;

        public static string Architecture
        {
            get
            {
                Package package = Package.Current;
                return package.Id.Architecture.ToString();
            }
        }

    }
}
