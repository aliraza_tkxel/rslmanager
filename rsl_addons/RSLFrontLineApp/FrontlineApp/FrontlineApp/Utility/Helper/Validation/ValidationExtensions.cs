﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace FrontlineApp.Utility.Helper.Validation
{
    public static class ValidationExtensions
    {
        public static readonly DependencyProperty AttachValidationProperty =
            DependencyProperty.RegisterAttached(
                "AttachValidation",
                typeof(bool), 
                typeof(ValidationExtensions),
                new PropertyMetadata(false, OnAttachValidationChanged));

        private static readonly DependencyProperty ValidationHandlerProperty =
            DependencyProperty.RegisterAttached(
                "ValidationHandler",
                typeof(ValidationHandler),
                typeof(ValidationExtensions),
                new PropertyMetadata(null));

        public static bool GetAttachValidation(DependencyObject obj)
        {
            return (bool)obj.GetValue(AttachValidationProperty);
        }

        public static void SetAttachValidation(DependencyObject obj, bool value)
        {
            obj.SetValue(AttachValidationProperty, value);
        }

        private static void OnAttachValidationChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var attach = (bool)e.NewValue;
            if (attach)
            {
                var handler = ValidationHandlerFactory.CreateValidationHandler(d as Control);
                SetValidationHandler(d, handler);
            }
            else
            {
                SetValidationHandler(d, null);
            }
        }

        private static ValidationHandler GetValidationHandler(DependencyObject d)
        {
            return (ValidationHandler)d.GetValue(ValidationHandlerProperty);
        }

        private static void SetValidationHandler(DependencyObject d, ValidationHandler value)
        {
            d.SetValue(ValidationHandlerProperty, value);
        }
    }
}
