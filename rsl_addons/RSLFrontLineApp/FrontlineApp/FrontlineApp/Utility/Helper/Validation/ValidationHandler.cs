﻿using System;
using System.ComponentModel;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace FrontlineApp.Utility.Helper.Validation
{
    public class ValidationHandler
    {
        private readonly Control control;
        private readonly DependencyProperty validateProperty;

        private INotifyDataErrorInfo dataErrorInfo;
        private string propertyValuePath;

        public ValidationHandler(Control control, DependencyProperty validateProperty)
        {
            this.control = control;
            this.validateProperty = validateProperty;

            control.Loaded += ControlLoaded;
        }

        private void Validate()
        {
            var errorMsg = GetErrors();

            if (!string.IsNullOrEmpty(errorMsg))
            {
                SetIsInvalid();
                return;
            }

            SetIsValid();
        }

        private void ControlLoaded(object sender, RoutedEventArgs e)
        {
            var bindingExpression = control.GetBindingExpression(validateProperty);
            if (bindingExpression != null && 
                bindingExpression.DataItem is INotifyDataErrorInfo && 
                bindingExpression.ParentBinding != null)
            {
                dataErrorInfo = bindingExpression.DataItem as INotifyDataErrorInfo;
                propertyValuePath = bindingExpression.ParentBinding.Path.Path;

                Validate();

                dataErrorInfo.ErrorsChanged += DataErrorInfoErrorsChanged;
            }
        }

        private void DataErrorInfoErrorsChanged(object sender, DataErrorsChangedEventArgs e)
        {
            if (e.PropertyName == propertyValuePath)
            {
                Validate();
            }
        }

        private string GetErrors()
        {
            var errorString = string.Empty;

            var errors = dataErrorInfo.GetErrors(propertyValuePath);
            foreach (var error in errors)
            {
                errorString += error.ToString();
                errorString += Environment.NewLine;
            }

            return errorString;
        }

        private void SetIsInvalid()
        {
            VisualStateManager.GoToState(control, "Invalid", true);
        }

        private void SetIsValid()
        {
            VisualStateManager.GoToState(control, "Valid", true);
        }
    }
}
