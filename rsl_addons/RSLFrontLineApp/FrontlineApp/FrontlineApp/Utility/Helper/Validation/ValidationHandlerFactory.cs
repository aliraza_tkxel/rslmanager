﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace FrontlineApp.Utility.Helper.Validation
{
    public static class ValidationHandlerFactory
    {
        private static readonly Dictionary<Type, DependencyProperty> propertyToValidate
            = new Dictionary<Type, DependencyProperty>
            {
               { typeof(TextBox), TextBox.TextProperty },
            };

        public static ValidationHandler CreateValidationHandler(Control control)
        {
            if (control == null)
            {
                throw new ArgumentException("d is not type of control or is null");
            }

            var dp = GetPropertyToValidate(control);

            return new ValidationHandler(control, dp);
        }

        private static DependencyProperty GetPropertyToValidate(Control control)
        {
            if (propertyToValidate.ContainsKey(control.GetType()))
            {
                return propertyToValidate[control.GetType()];
            }

            Debugger.Break();
            throw new NotImplementedException("Missing implementation. Unknown contorl"); 
        }
    }
}
