﻿using FrontlineApp.ApplicationLayer.Enums;
using FrontlineApp.DataLayer.Entities;
using System;

namespace FrontlineApp.Utility.Helper.AppSession
{
    public static class AppSession
    {

        #region Is Customer Lookup Fetched
        private static bool isCustomerLookupFetched = false;
        public static bool IsCustomerLookupFetched
        {
            get { return isCustomerLookupFetched; }
            set { isCustomerLookupFetched = value; }
        }

        #endregion

        #region Is Shared Lookup Fetched

        private static bool isSharedLookupsFetched = false;
        public static bool IsSharedLookupsFetched
        {
            get { return isSharedLookupsFetched; }
            set { isSharedLookupsFetched = value; }
        }

        #endregion

        #region Is Property Lookup Fetched

        private static bool isPropertyLookupFetched = false;
        public static bool IsPropertyLookupFetched
        {
            get { return isPropertyLookupFetched; }
            set { isPropertyLookupFetched = value; }
        }

        #endregion

        #region Logged in UserId

        private static int loggedinUserId;
        public static int LoggedinUserId
        {
            get { return loggedinUserId; }
            set { loggedinUserId = value; }
        }

        #endregion

        #region Logged in HousingOfficer

        private static HousingOfficer loggedInUser;
        public static HousingOfficer LoggedInUser
        {
            get { return loggedInUser; }
            set { loggedInUser = value; }
        }

        #endregion

        #region Selected Tenant Item

        private static TenantItems selectedTenantItem;
        public static TenantItems SelectedTenantItem
        {
            get { return selectedTenantItem; }
            set { selectedTenantItem = value; }
        }

        #endregion

        #region Selected Prospect Item

        private static ProspectItems selectedProspectItem;
        public static ProspectItems SelectedProspectItem
        {
            get { return selectedProspectItem; }
            set { selectedProspectItem = value; }
        }

        #endregion

        #region "Reset Session"
        public static void Reset()
        {
            IsCustomerLookupFetched = false;
            IsSharedLookupsFetched = false;
            IsPropertyLookupFetched = false;
        }
        #endregion
    }
}
