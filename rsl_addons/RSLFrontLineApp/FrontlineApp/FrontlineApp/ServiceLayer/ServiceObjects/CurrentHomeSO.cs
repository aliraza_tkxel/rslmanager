﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ServiceLayer.ServiceObjects
{
    public class CurrentHomeSO
    {
        public int? currentHomeId { get; set; }
        public int? landlordTypeId { get; set; }
        public string landlordName { get; set; }
        public int? houseTypeId { get; set; }
        public int? floor { get; set; }
        public int? noOfBedrooms { get; set; }
        public int? livingWithFamilyId { get; set; }
        public DateTimeOffset? tenancyStartDate { get; set; }
        public DateTimeOffset? tenancyEndDate { get; set; }
        public string previousLandlordName { get; set; }
    }
}
