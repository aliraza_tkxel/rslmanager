﻿using System.Collections.Generic;
using FrontlineApp.DataLayer.Entities;
namespace FrontlineApp.ServiceLayer.ServiceObjects
{
    public class PropertySearchResultSO
    {
        public List<Property> propertiesData { get; set; }
        public PropertyLookUpSO lookups { get; set; }
    }
}
