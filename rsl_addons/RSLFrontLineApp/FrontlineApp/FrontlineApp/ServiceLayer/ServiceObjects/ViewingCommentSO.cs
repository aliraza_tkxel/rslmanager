﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ServiceLayer.ServiceObjects
{
    public class ViewingCommentSO
    {
        public int CommentId { get; set; }
        public string CommentText { get; set; }
        public int? ViewingId { get; set; }
        public DateTime? CommentedDate { get; set; }
        public int? CommentTypeId { get; set; }
        public int? CommentedBy { get; set; }

        public bool? isActive { get; set; }
    }
}
