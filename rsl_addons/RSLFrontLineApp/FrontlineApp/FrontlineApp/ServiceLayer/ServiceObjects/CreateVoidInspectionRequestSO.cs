﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ServiceLayer.ServiceObjects
{
    public class CreateVoidInspectionRequestSO
    {
        public int UserId { get; set; }
        public string AppointmentNotes { get; set; }
        public DateTime AppointmentStartDate { get; set; }
        public DateTime AppointmentEndDate { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public int OperativeId { get; set; }
        public int JournalId { get; set; }
        public decimal? Duration { get; set; }
        public bool IsRearrange { get; set; }
    }
}
