﻿using System.Collections.Generic;
using FrontlineApp.DataLayer.Entities;

namespace FrontlineApp.ServiceLayer.ServiceObjects
{
    public class ViewingLookUpSO
    {
        public List<ViewingStatus> viewingStatus { get; set; }
    }
}
