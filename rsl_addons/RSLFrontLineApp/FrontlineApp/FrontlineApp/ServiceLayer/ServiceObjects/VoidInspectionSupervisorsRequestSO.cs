﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ServiceLayer.ServiceObjects
{
    public class VoidInspectionSupervisorsRequestSO
    {
        public string tradeIds
        {
            get
            {
                return null;
            }
        }
        public string msattype
        {
            get
            {
                return "Void";
            }
        }
        public DateTime startDate { get; set; } = DateTime.Now;
        public DateTime aptStartDateTime
        {
            get
            {
                return startDate;
            }
        }
        public DateTime aptEndDateTime
        {
            get
            {
                return startDate;
            }
        }
    }
}
