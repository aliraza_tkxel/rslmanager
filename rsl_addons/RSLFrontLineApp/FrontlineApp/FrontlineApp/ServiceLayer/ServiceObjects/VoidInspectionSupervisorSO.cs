﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ServiceLayer.ServiceObjects
{
    public class VoidInspectionSupervisorSO
    {
        public int EmployeeId { get; set; }
        public string FullName { get; set; }
    }
}
