﻿using FrontlineApp.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ServiceLayer.ServiceObjects
{
    public class ArrangeEditViewingRequestSO
    {
        public ArrangeEditViewingRequestSO()
        {
            appVersion = GeneralHelper.GetAppVersion();
            viewingId = 0;
            recordingSource = "UWP App";
        }
        public int viewingId { get; set; }
        public int customerId { get; set; }
        public string propertyId { get; set; }
        public int housingOfficerId { get; set; }
        public int viewingStatusId { get; set; }
        public DateTime viewingDate { get; set; }
        public string viewingTime { get; set; }
        public string endTime { get; set; }
        public int duration { get; set; }
        public string actualEndTime { get; set; }
        public int createdById { get; set; }
        public bool isActive { get; set; }
        public string recordingSource { get; set; }
        public string appVersion { get; set; }
        public DateTime createdOnApp { get; set; }
        public DateTime? createdOnServer { get; set; }
        public DateTime? lastModifiedOnApp { get; set; }
        public DateTime? lastModifiedOnServer { get; set; }
        public string notes { get; set; }
        public int? assignedById { get; set; }
    }
}
