﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FrontlineApp.DataLayer.Entities;

namespace FrontlineApp.ServiceLayer.ServiceObjects
{
    public class ViewingSummaryResultSO
    {
        public List<ViewingSummary> viewingListDataModel { get; set; }
        public ViewingLookUpSO lookups { get; set; }
    }
}
