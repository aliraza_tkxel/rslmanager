﻿using FrontlineApp.DataLayer.Entities;
using System.Collections.Generic;

namespace FrontlineApp.ServiceLayer.ServiceObjects
{
    public class CustomerLookupSO
    {
        public List<AgencySupport> supportAgencies { get; set; }
        public List<AidsAndAdaption> adaptions { get; set; }
        public List<BankingFacility> bankingFacility { get; set; }
        public List<EmploymentStatus> employmentStatus { get; set; }
        public List<HealthProblem> disability { get; set; }
        public List<LandlordType> landlord { get; set; }
        public List<Nationality> nationality { get; set; }
        public List<ReferralHelpCategory> referralsHelpCategories { get; set; }
        public List<ReferralHelpSubCategory> referralsHelpSubCategories { get; set; }
        public List<ReferralStages> referralsStages { get; set; }        
        public List<CustomerTitle> customerTitles { get; set; }
        public List<Communication> communication { get; set; }
        public List<InternetAccess> internetAccess { get; set; }
        public List<PreferedContact> preferedContact { get; set; }
        public List<SexualOrientation> sexualOrientation { get; set; }
        public List<Benefit> benefits { get; set; }
        public List<Ethnicity> ethnicity { get; set; }
        public List<MaritalStatus> maritalStatus { get; set; }
        public List<Religion> religion { get; set; }
        public List<YesNo> yesNo { get; set; }
        public List<YesNoUnknown> yesNoUnkown { get; set; }
        public List<AddressType> addressType { get; set; }
        public List<HouseType> houseType { get; set; }
        public List<TerminationReason> terminationResaon { get; set; }
        public List<TerminationReasonCategory> terminationResaonCategory { get; set; }
        public List<Floor> floorType { get; set; }
        public List<RelationStatus> relationshipStatus { get; set; }

    }
}
