﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ServiceLayer.ServiceObjects
{
    public class CustomerLookupRequestSO
    {
        public PaginationSO pagination { get; set; }
        public string searchText { get; set; }
        public bool isLookUpRequired { get; set; }
    }
}
