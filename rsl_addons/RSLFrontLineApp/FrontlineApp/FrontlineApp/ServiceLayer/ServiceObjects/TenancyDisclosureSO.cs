﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ServiceLayer.ServiceObjects
{
    public class TenancyDisclosureSO
    {
        public int? disclosureId { get; set; }
        public bool? isEvicted { get; set; }
        public string evictedReason { get; set; }
        public bool? isSubjectToDebt { get; set; }
        public bool? isUnsatisfiedCcj { get; set; }
        public bool? hasCriminalOffense { get; set; }
        public int? tenancyId { get; set; }
    }
}
