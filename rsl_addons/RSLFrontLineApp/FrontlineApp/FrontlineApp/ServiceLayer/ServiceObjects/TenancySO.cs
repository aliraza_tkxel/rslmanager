﻿using System;
using System.Collections.Generic;

namespace FrontlineApp.ServiceLayer.ServiceObjects
{
    public class TenancySO
    {
        public int? tenancyId { get; set; }
        public string propertyId { get; set; }
        public int? customerId { get; set; }
        public EmploymentInfoSO employmentInfo { get; set; }
        public CustomerGeneralInfoSO customerGeneralInfo { get; set; }
        public EmergencyContactSO emergencyContact { get; set; }
        public JointTenantSO jointTenant { get; set; }
        public List<OccupantSO> occupants { get; set; }
        public ReferralSO referral { get; set; }
        public PropertyRentSO propertyRentInfo { get; set; }
        public TenancyDisclosureSO tenancyDisclosure { get; set; }
        public TenancyAddressSO tenancyAddress { get; set; }
        public TenantOnlineInfoSO tenantOnlineInfo { get; set; }
        public DateTime? tenancyStartDate { get; set; }
        public DateTime? tenancyEndDate { get; set; }
    }
}
