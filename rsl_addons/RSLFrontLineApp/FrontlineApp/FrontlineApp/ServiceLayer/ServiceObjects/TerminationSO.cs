﻿using System;

namespace FrontlineApp.ServiceLayer.ServiceObjects
{
    public class TerminationSO
    {
        public int? journalId { get; set; }
        public int? terminationHistoryId { get; set; }
        public int? reasonId { get; set; }
        public int? reasonCategoryId { get; set; }
        public string notes { get; set; }
        public int? terminatedBy { get; set; }
        public DateTimeOffset? terminationDate { get; set; }
        public int customerId { get; set; }
        public string AppVersion { get; set; }
        public ForwardingAddressSO forwardAddress { get; set; }
    }
}
