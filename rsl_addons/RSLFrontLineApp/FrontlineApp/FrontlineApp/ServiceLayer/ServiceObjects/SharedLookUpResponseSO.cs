﻿using FrontlineApp.DataLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ServiceLayer.ServiceObjects
{
    public class SharedLookUpResponseSO
    {
        public List<PropertyType> propertyType { get; set; }
        public List<CustomerType> customerType { get; set; }
        public List<AgencySupport> supportAgencies { get; set; }
        public List<AidsAndAdaption> adaptions { get; set; }
        public List<BankingFacility> bankingFacility { get; set; }
        public List<EmploymentStatus> employmentStatus { get; set; }
        public List<HealthProblem> disability { get; set; }
        public List<ReferralHelpCategory> referralsHelpCategories { get; set; }
        public List<ReferralHelpSubCategory> referralsHelpSubCategories { get; set; }
        public List<ReferralStages> referralsStages { get; set; }
        public List<TerminationReason> terminationResaon { get; set; }
        public List<TerminationReasonCategory> terminationResaonCategory { get; set; }
        public List<CustomerTitle> customerTitles { get; set; }
        public List<Benefit> benefits { get; set; }
        public List<YesNo> yesNo { get; set; }
        public List<YesNoUnknown> yesNoUnkown { get; set; }
        public List<AddressType> addressType { get; set; }
        public List<RelationStatus> relationshipStatus { get; set; }
        public List<ItemStatus> riskVulnerabilityStatus { get; set; }
        public List<Team> teams { get; set; }
        public List<Employee> employees { get; set; }
        public List<RiskCategory> riskCategory { get; set; }
        public List<RiskSubCategory> riskSubCategory { get; set; }
        public List<VulnerabilityCategory> vulnerabilityCategory { get; set; }
        public List<VulnerabilitySubCategory> vulnerabilitySubCategory { get; set; }
        public List<LetterAction> letterAction { get; set; }
        public List<ViewingCommentType> viewingCommentTypes { get; set; }

    }
}
