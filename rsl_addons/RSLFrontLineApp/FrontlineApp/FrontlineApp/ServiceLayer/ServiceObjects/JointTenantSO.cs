﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ServiceLayer.ServiceObjects
{
    public class JointTenantSO
    {
        public int? tenancyId { get; set; }
        public int? customerId { get; set; }
        public DateTimeOffset? startDate { get; set; }
        public int? isCorrespondingAddress { get; set; }
        public EmploymentInfoSO employmentInfo { get; set; }
        public CustomerGeneralInfoSO customerGeneralInfo { get; set; }
        public EmergencyContactSO emergencyContact { get; set; }
    }
}
