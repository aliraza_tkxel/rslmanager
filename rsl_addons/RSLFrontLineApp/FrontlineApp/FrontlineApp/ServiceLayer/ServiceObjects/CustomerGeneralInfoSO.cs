﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ServiceLayer.ServiceObjects
{
    public class CustomerGeneralInfoSO
    {
        public string benefits { get; set; }
        public string bankingFacilities { get; set; }
        public string bankingFacilityOther { get; set; }
        public string healthProblems { get; set; }
        public string healthProblemOthers { get; set; }
        public int? liveInCarer { get; set; }
        public int? wheelChair { get; set; }
        public string supportFromAgencies { get; set; }
        public string supportFromAgenciesOther { get; set; }
        public string aidsAndAdaptation { get; set; }
    }
}
