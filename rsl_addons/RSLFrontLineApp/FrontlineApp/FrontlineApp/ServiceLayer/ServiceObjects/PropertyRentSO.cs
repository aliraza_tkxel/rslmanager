﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ServiceLayer.ServiceObjects
{
    public class PropertyRentSO
    {
        public decimal? rentPayable { get; set; }
        public decimal? rent { get; set; }
        public decimal? services { get; set; }
        public decimal? councilTax { get; set; }
        public decimal? waterRates { get; set; }
        public decimal? ineligServ { get; set; }
        public decimal? supportedServices { get; set; }
        public decimal? garage { get; set; }
    }
}
