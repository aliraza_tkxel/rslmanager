﻿using FrontlineApp.DataLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ServiceLayer.ServiceObjects
{
    public class AddViewingCommentResponseSO
    {
        public bool isSuccess { get; set; }
        public string message { get; set; }
        public List<ViewingComment> allComments { get; set; }
    }
}
