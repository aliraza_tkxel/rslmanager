﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ServiceLayer.ServiceObjects
{
    public class CustomerViewingSO
    {
        public int viewingId { get; set; }
        public int viewingStatusId { get; set; }
        public DateTimeOffset? viewingDateTime { get; set; }
        public string viewingTime { get; set; }
        public int? housingOfficerId { get; set; }
        public string housingOfficerName { get; set; }
        public string propertyAddress { get; set; }
        public string propertyId { get; set; }
    }
}
