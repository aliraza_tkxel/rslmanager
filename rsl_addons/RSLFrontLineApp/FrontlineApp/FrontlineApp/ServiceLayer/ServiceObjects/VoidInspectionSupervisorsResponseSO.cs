﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ServiceLayer.ServiceObjects
{
    public class VoidInspectionSupervisorsResponseSO
    {
        public List<VoidInspectionSupervisorSO> Supervisors { get; set; }
    }
}
