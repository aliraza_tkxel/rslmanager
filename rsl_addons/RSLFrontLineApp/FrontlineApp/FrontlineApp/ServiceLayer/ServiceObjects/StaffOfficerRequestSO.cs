﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ServiceLayer.ServiceObjects
{
    public class StaffOfficerRequestSO
    {
        public DateTime viewingDate { get; set; }
        public string viewingTime { get; set; }
        public string searchText { get; set; }
    }
}
