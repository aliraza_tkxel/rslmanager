﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ServiceLayer.ServiceObjects
{
    public class ResultSO<T>
    {
        public string Error { get; set; }
        public string Message { get; set; }
        public bool Success { get; set; }
        public T Value { get; set; }

    }
}
