﻿using FrontlineApp.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ServiceLayer.ServiceObjects
{
    public class PropertyViewingsRequestSO
    {
        public PropertyViewingsRequestSO()
        {
            startDate = GeneralHelper.AddOrSubtractDaysToCurrentDate("-");
            endDate = GeneralHelper.AddOrSubtractDaysToCurrentDate("+");
        }
        public string propertyId { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
    }
}
