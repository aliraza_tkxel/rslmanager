﻿using FrontlineApp.DataLayer.Entities;
using System.Collections.Generic;

namespace FrontlineApp.ServiceLayer.ServiceObjects
{
    public class SearchCustomerResponseSO
    {
        public List<CustomerSO> customerList { get; set; }
        public CommonSO commonData { get; set; }
    }
}
