﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ServiceLayer.ServiceObjects
{
    public class CommonSO
    {
        public CustomerLookupSO lookups { get; set; }
        public string profileThumb { get; set; }
        public string cusProfile { get; set; }
    }
}
