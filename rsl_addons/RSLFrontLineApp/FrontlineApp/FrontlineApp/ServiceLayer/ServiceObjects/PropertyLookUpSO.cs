﻿using System.Collections.Generic;
using FrontlineApp.DataLayer.Entities;
namespace FrontlineApp.ServiceLayer.ServiceObjects
{
    public class PropertyLookUpSO
    {
        public List<PropertyScheme> schemeModel { get; set; }
    }
}
