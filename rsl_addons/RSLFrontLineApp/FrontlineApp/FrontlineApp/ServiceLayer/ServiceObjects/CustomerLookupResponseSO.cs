﻿using FrontlineApp.DataLayer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ServiceLayer.ServiceObjects
{
    public class CustomerLookupResponseSO
    {
        public List<CustomerLookup> customerList { get; set; }
    }
}
