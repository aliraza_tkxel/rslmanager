﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ServiceLayer.ServiceObjects
{
    public class ReferralSO
    {
        public int? referralHistoryId { get; set; }
        public int? journalId { get; set; }
        public int? customerId { get; set; }
        public string referrerName { get; set; }
        public bool? isTypeArrears { get; set; }
        public bool? isTypeDamage { get; set; }
        public bool? isTypeAsb { get; set; }
        public int? stageId { get; set; }
        public int? safetyIssueId { get; set; }
        public int? isConviction { get; set; }
        public int? isSubstanceMisuse { get; set; }
        public int? isSelfHarm { get; set; }
        public string yesNotes { get; set; }
        public string neglectNotes { get; set; }
        public string otherNotes { get; set; }
        public string miscNotes { get; set; }
        public int? isTenantAware { get; set; }
        public string title { get; set; }
        public string appVersion { get; set; }
        public DateTimeOffset? createdOnApp { get; set; }
        public DateTimeOffset? createdOnServer { get; set; }
        public DateTimeOffset? lastModifiedOnApp { get; set; }
        public DateTimeOffset? lastModifiedOnServer { get; set; }
        public string customerHelpSubCategories { get; set; }
        public int? lastActionUserId { get; set; }
    }
}
