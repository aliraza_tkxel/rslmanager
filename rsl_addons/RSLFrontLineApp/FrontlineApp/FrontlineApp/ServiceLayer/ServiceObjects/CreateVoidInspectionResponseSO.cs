﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ServiceLayer.ServiceObjects
{
    public class CreateVoidInspectionResponseSO
    {
        public bool IsSaved { get; set; }
        public int? AppointmentIdOut { get; set; }
        public string StatusMessage { get; set; }
    }
}
