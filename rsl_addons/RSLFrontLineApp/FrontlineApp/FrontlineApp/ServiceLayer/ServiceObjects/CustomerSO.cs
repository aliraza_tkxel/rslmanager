﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ServiceLayer.ServiceObjects
{
   public class CustomerSO
    {
        public int? customerId { get; set; }
        public string customerNumber { get; set; }
        public int? customerTypeId { get; set; }
        public int? tenancyCount { get; set; }
        public int? titleId { get; set; }
        public string firstName { get; set; }
        public string middleName { get; set; }
        public string lastName { get; set; }
        public int? preferedContactId { get; set; }
        public string internetAccess { get; set; }
        public DateTimeOffset? dob { get; set; }
        public string gender { get; set; }
        public int? maritalStatusId { get; set; }
        public int? ethnicityId { get; set; }
        public int? religionId { get; set; }
        public int? sexualOrientationId { get; set; }
        public string niNumber { get; set; }
        public int? nationalityId { get; set; }
        public string firstLanguage { get; set; }
        public int? isSubjectToImmigration { get; set; }
        public int? isPermanentUKResident { get; set; }
        public string communication { get; set; }
        public int? createdBy { get; set; }
        public int? children { get; set; }
        public int? adults { get; set; }
        public string appVersion { get; set; }
        public DateTimeOffset? createdOnApp { get; set; }
        public DateTimeOffset? createdOnServer { get; set; }
        public DateTimeOffset? lastModifiedOnApp { get; set; }
        public DateTimeOffset? lastModifiedOnServer { get; set; }
        public CurrentHomeSO currentHome { get; set; }
        public CustomerImageSO customerImage { get; set; }
        public TenancySO tenancy { get; set; }
        public List<CustomerAddressSO> customerAddress { get; set; }
        public EmploymentInfoSO employmentInfo { get; set; }
        public CustomerGeneralInfoSO customerGeneralInfo { get; set; }
    }
}
