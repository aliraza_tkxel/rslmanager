﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ServiceLayer.ServiceObjects
{
    public class UserSO
    {
        public string username { get; set; }
        public string password { get; set; }
    }
}
