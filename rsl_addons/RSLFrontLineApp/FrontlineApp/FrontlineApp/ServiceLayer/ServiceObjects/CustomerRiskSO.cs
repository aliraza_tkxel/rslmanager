﻿using System;

namespace FrontlineApp.ServiceLayer.ServiceObjects
{
    public class CustomerRiskSO
    {
        public int? riskHistoryId { get; set; }
        public int? customerId { get; set; }
        public string customerName { get; set; }
        public int? tenancyId { get; set; }
        public int? journalId { get; set; }
        public string title { get; set; }
        public int? teamId { get; set; }
        public int? assignedTo { get; set; }
        public DateTimeOffset? startDate { get; set; }
        public string customerRiskSubcategories { get; set; }
        public string notes { get; set; }
        public DateTimeOffset? reviewDate { get; set; }
        public bool? isCustomerAware { get; set; }
        public bool? isTenantOnline { get; set; }
        public int? actionId { get; set; }
        public int? letterId { get; set; }
        public int? lastActionUserId { get; set; }
        public DateTimeOffset? lastActionDate { get; set; }
        public int? statusId { get; set; }
    }
}
