﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ServiceLayer.ServiceObjects
{
    public class OccupantSO
    {
        public int? occupantId { get; set; }
        public int? customerId { get; set; }
        public int? titleId { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public DateTimeOffset? dateOfBirth { get; set; }
        public int? relationshipStatusId { get; set; }
        public string disabilities { get; set; }
        public string disabilitiesOthers { get; set; }
        public int? employmentStatusId { get; set; }
    }
}
