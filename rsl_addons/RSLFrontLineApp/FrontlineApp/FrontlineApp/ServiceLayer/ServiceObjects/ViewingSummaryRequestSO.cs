﻿using System;
using FrontlineApp.Utility;
namespace FrontlineApp.ServiceLayer.ServiceObjects
{
    public class ViewingSummaryRequestSO
    {
        public ViewingSummaryRequestSO()
        {
            housingOfficer = 0;
            isLookUpRequired = false;
            startDate = GeneralHelper.AddOrSubtractDaysToCurrentDate("-");
            endDate = GeneralHelper.AddOrSubtractDaysToCurrentDate("+");
        }
        public int housingOfficer { get; set; }
        public bool isLookUpRequired { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        
    }
}
