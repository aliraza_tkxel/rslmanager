﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ServiceLayer.ServiceObjects
{
    public class PaginationSO
    {
        public int pageSize { get; set; }
        public int pageNumber { get; set; }
        public int totalRows { get; set; }
        public int totalPages { get; set; }
    }
}
