﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.DataLayer.Entities;
using FrontlineApp.DataLayer.Repository;
using FrontlineApp.ServiceLayer.ServiceObjects;
using FrontlineApp.Utility;
using FrontlineApp.Utility.Constants;
using FrontlineApp.Utility.Helper.AppSession;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FrontlineApp.ServiceLayer.Services
{
    public class ViewingsService : ABaseService
    {
        #region Properties
        ViewingsRepository viewingRepo;
        #endregion

        #region "Constructor"
        public ViewingsService() : base()
        {
            viewingRepo = new ViewingsRepository();
        }
        #endregion

        #region "Get Staff Member List"

        public async Task<ResultModel<bool>> GetStaffMemberList(StaffOfficerRequestSO searchObj)
        {
            ResultSO<List<StaffMember>> response =  await PostHttp<StaffOfficerRequestSO, List<StaffMember>>(searchObj, UriTemplateConstants.GetStaffMember);

            // Save information in local database
            ResultModel<bool> result = new ResultModel<bool>();
            if (response.Success)
            {
                var memberList = response.Value as List<StaffMember>;
                if (memberList != null)
                {
                    result = viewingRepo.SaveStaffMember(memberList);
                }
            }
            else
            {
                result.Message = response.Message;
                result.Success = false;
            }

            return result;
        }
        #endregion

        #region "Get Customer LisT"
        public async Task<ResultModel<bool>> SearchCustomerLookUp(PaginationSO pageInfo, string searchText, bool isLookupRequired)
        {
            CustomerLookupRequestSO requestParam = new CustomerLookupRequestSO();
            requestParam.pagination = pageInfo;
            requestParam.searchText = searchText;
            requestParam.isLookUpRequired = isLookupRequired;

            ResultSO<CustomerLookupResponseSO> response = await PostHttp<CustomerLookupRequestSO, CustomerLookupResponseSO>(requestParam, UriTemplateConstants.SearchCustomer);
            ResultModel<bool> result = new ResultModel<bool>();
            if (response.Success)
            {
                result = viewingRepo.SaveCustomers(response.Value);
            }
            return result;

        }
        #endregion

        #region "Get Viewings List"
        public async Task<bool> GetAllViewings(int officerId, bool isLookUpRequired)
        {
            ViewingSummaryRequestSO RequestSO = new ViewingSummaryRequestSO();
            RequestSO.housingOfficer = officerId;
            RequestSO.isLookUpRequired = true;

            ResultSO<ViewingSummaryResultSO> ResultObj = await PostHttp<ViewingSummaryRequestSO, ViewingSummaryResultSO>(RequestSO, UriTemplateConstants.GetAllViewings);
            if (ResultObj.Success)
            {
                 InsertViewingsForResult(ResultObj.Value as ViewingSummaryResultSO);
                 InsertViewingStatusLookUps(ResultObj.Value as ViewingSummaryResultSO);
            }
            return ResultObj.Success;
        }
        public async Task<bool> GetViewingsDetailForViewing (ViewingSummaryModel Viewing)
        {
            IDictionary<string, string> paramDic = new Dictionary<string, string>();
            paramDic.Add("viewingId", Viewing.ViewingId.ToString());
            ResultSO<ViewingSummary> response = await GetHttp<ViewingSummary>(string.Empty, UriTemplateConstants.GetViewingDetail, paramDic);
            response.Value.id = Viewing.Id;
            if (response.Success == true && response.Value!=null)
            {
                viewingRepo.UpdateViewingSummary(response.Value);
                //UpdateViewingForDetails(response.Value);
            }
            return response.Success;
        }
        #endregion

        #region "Add Comment"
        public async Task<ResultSO<AddViewingCommentResponseSO>> AddViewingComment(ViewingCommentSO requestSO)
        {
            ResultSO<AddViewingCommentResponseSO> response = await PostHttp<ViewingCommentSO, AddViewingCommentResponseSO>(requestSO, UriTemplateConstants.AddCommentToViewing);
            return response;
        }
        #endregion

        #region "Edit or Arrange Viewing"
        public async Task<ResultSO<ViewingSummary>> ArrangeOrEditViewing(ViewingSummaryModel viewingObj, bool isChangingStatus)
        {
            ArrangeEditViewingRequestSO requestSO = MapModelToEditRequestSO(viewingObj, isChangingStatus);
            ResultSO<ViewingSummary> response = await PostHttp<ArrangeEditViewingRequestSO, ViewingSummary>(requestSO, UriTemplateConstants.ArrangeOrEditViewing);
            return response;
        } 
        
        private ArrangeEditViewingRequestSO MapModelToEditRequestSO(ViewingSummaryModel viewingObj, bool isChangingStatus)
        {
            ArrangeEditViewingRequestSO requestSO = new ArrangeEditViewingRequestSO();
            requestSO.customerId = viewingObj.CustomerId;
            requestSO.propertyId = viewingObj.PropertyId;
            requestSO.housingOfficerId = viewingObj.HousingOfficerId;
            requestSO.viewingStatusId = viewingObj.ViewingStatusId;
            if (viewingObj.ViewingStatusId == 0)
            {
                if (viewingObj.HousingOfficerId == AppSession.LoggedInUser.userId)
                {
                    requestSO.viewingStatusId = ApplicationConstants.ViewingStatusAccepted; //Because assigned to the same user
                }
                else
                {
                    requestSO.viewingStatusId = ApplicationConstants.ViewingStatusWaiting;
                }
                
            }
            if (!isChangingStatus)
            {
                if (viewingObj.ViewingStatusId == ApplicationConstants.ViewingStatusRejected)
                {
                    if (viewingObj.HousingOfficerId == AppSession.LoggedinUserId)
                    {
                        requestSO.viewingStatusId = ApplicationConstants.ViewingStatusAccepted;
                    }
                    else
                    {
                        requestSO.viewingStatusId = ApplicationConstants.ViewingStatusWaiting;
                    }
                }
                else if (viewingObj.ViewingStatusId == ApplicationConstants.ViewingStatusCancelled)
                {
                    if (viewingObj.HousingOfficerId == AppSession.LoggedinUserId)
                    {
                        requestSO.viewingStatusId = ApplicationConstants.ViewingStatusCancelled;
                    }
                    else
                    {
                        requestSO.viewingStatusId = ApplicationConstants.ViewingStatusWaiting;
                    }
                }
            }

            requestSO.viewingDate = GeneralHelper.GetCombinedDateTimeStringForViewing(viewingObj.ViewingDate, viewingObj.FormattedTime);//DateTime.Parse(viewingObj.ViewingDate).ToUniversalTime();
            requestSO.viewingTime = viewingObj.FormattedTime;
            requestSO.createdById = viewingObj.CreatedById;
            requestSO.createdOnServer = null;
            requestSO.notes = viewingObj.Notes;
            requestSO.assignedById = viewingObj.AssignedById;
            if (string.IsNullOrWhiteSpace(viewingObj.CreationDate))
            {
                requestSO.createdOnApp = DateTime.UtcNow;
                requestSO.lastModifiedOnApp = null;
                requestSO.lastModifiedOnServer = null;
            }
            else
            {
                requestSO.createdOnApp = DateTime.Parse(viewingObj.CreationDate).ToUniversalTime();
            }
            requestSO.viewingId = viewingObj.ViewingId;
            return requestSO;
        }
        #endregion

        #region "Viewings Insertion"
        
        public  bool InsertViewingsForResult(ViewingSummaryResultSO result)
        {
            Repository<ViewingSummary> myRepo = new Repository<ViewingSummary>();
            myRepo.InsertAll(result.viewingListDataModel);
            return true;
        }
        public  bool InsertViewingStatusLookUps(ViewingSummaryResultSO result)
        {
            Repository<ViewingStatus> myRepo = new Repository<ViewingStatus>();
            myRepo.InsertOrReplaceAll(result.lookups.viewingStatus);
            return true;
        }
        public bool UpdateViewingForDetails(ViewingSummary result)
        {
            Repository<ViewingSummary> myRepo = new Repository<ViewingSummary>();
            ViewingSummary oldObj =  myRepo.Get(result.id);
            oldObj.propertyId = result.propertyId;
            oldObj.rent = result.rent;
            oldObj.noOfBedRooms = result.noOfBedRooms;
            oldObj.propertyType = result.propertyType;
            oldObj.customerType = result.customerType;
            oldObj.mobileNo = result.mobileNo;
            oldObj.dob = result.dob;
            oldObj.createdOnApp = result.createdOnApp;
            oldObj.viewingNotes = result.viewingNotes;
            oldObj.viewingCreatedBy = result.viewingCreatedBy;
            oldObj.propertyImage = result.propertyImage;
            oldObj.outlookIdentifier = result.outlookIdentifier;
            oldObj.assignedById = result.assignedById;
            oldObj.assignedByName = result.assignedByName;
            myRepo.Update(oldObj);
            return true;
        }
        #endregion
    }
}
