﻿using AutoMapper;
using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.DataLayer.Entities;
using FrontlineApp.DataLayer.Repository;
using FrontlineApp.ServiceLayer.ServiceObjects;
using FrontlineApp.Utility.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ServiceLayer.Services
{

    public class AlertsService : ABaseService
    {
        #region Properties
        AlertsRepository AlertsRepo;
        #endregion

        #region "Constructor"
        public AlertsService() : base()
        {
            AlertsRepo = new AlertsRepository();
        }
        #endregion

        #region "Get Open Risks"
        public async Task<ResultModel<bool>> GetOpenRisks()
        {
            LookupRepository lookupRepo = new LookupRepository();
            int openStatusId = lookupRepo.GetItemStatusId(ApplicationConstants.OpenStatusDescription);

            IDictionary<string, string> paramDic = new Dictionary<string, string>();
            paramDic.Add("statusId", openStatusId.ToString());

            ResultSO<List<AlertRiskSO>> serverResponse = await GetHttp<List<AlertRiskSO>>(string.Empty, UriTemplateConstants.GetRisksList, paramDic);
            
            ResultModel<bool> result = new ResultModel<bool>();
            if (serverResponse.Success)
            {
                var openRiskList = serverResponse.Value as List<AlertRiskSO>;
                var list = Mapper.Map<List<AlertRiskSO>, List<AlertRisk>>(openRiskList);
                result = AlertsRepo.SaveRiskListing(list);
            }
            else
            {
                result.Success = false;
                result.Message = serverResponse.Message;
                result.Error = serverResponse.Error;
            }

            return result;
        }
        #endregion

        #region "Get Open Vulnerabilities"
        public async Task<ResultModel<bool>> GetOpenVulnerabilities()
        {
            LookupRepository lookupRepo = new LookupRepository();
            int openStatusId = lookupRepo.GetItemStatusId(ApplicationConstants.OpenStatusDescription);

            IDictionary<string, string> paramDic = new Dictionary<string, string>();
            paramDic.Add("statusId", openStatusId.ToString());

            ResultSO<List<AlertVulnerabilitySO>> serverResponse = await GetHttp<List<AlertVulnerabilitySO>>(string.Empty, UriTemplateConstants.GetVulnerabilitiesList, paramDic);

            ResultModel<bool> result = new ResultModel<bool>();
            if (serverResponse.Success)
            {
                var openvulnerabilityList = serverResponse.Value as List<AlertVulnerabilitySO>;
                var list = Mapper.Map<List<AlertVulnerabilitySO>, List<AlertVulnerability>>(openvulnerabilityList);
                result = AlertsRepo.SaveVulnerabilityListing(list);
            }
            else
            {
                result.Success = false;
                result.Message = serverResponse.Message;
                result.Error = serverResponse.Error;
            }

            return result;
        }
        #endregion

    }

}
