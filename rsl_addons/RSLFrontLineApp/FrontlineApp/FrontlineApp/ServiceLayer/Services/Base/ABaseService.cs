﻿using FrontlineApp.ServiceLayer.ServiceObjects;
using FrontlineApp.Utility.Helper.Extensions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using FrontlineApp.Utility.Constants;

namespace FrontlineApp.ServiceLayer.Services
{
    public abstract class ABaseService
    {

        protected HttpClient _client = null;

        public ABaseService()
        {
            string baseUrl = UriTemplateConstants.BaseURL;
            _client = new HttpClient();
            _client.DefaultRequestHeaders.Add("If-Modified-Since", DateTime.UtcNow.ToString("r")); //Disable caching

            _client.DefaultRequestHeaders
                             .Accept
                             .Add(new MediaTypeWithQualityHeaderValue("application/json"));


            if (baseUrl.EndsWith("/"))
                baseUrl = baseUrl.Substring(0, baseUrl.Length - 1);
            
            _client.BaseAddress = new Uri(baseUrl + "/" );
        }


        protected async Task<ResultSO<TOutbound>> PutHttp<TInbound, TOutbound>(TInbound data, string id, String controller)
        {

            var content = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");

            var result = await _client.PutAsync(controller + "/" + id, content);

            if (result.IsSuccessStatusCode)
            {
                return new ResultSO<TOutbound>()
                {
                    Success = true,
                    Value = JsonConvert.DeserializeObject<TOutbound>(await result.Content.ReadAsStringAsync())
                };
            }
            else
            {
                var error = new ResultSO<TOutbound>()
                {
                    Error = result.StatusCode.ToString(),
                    Message = result.ReasonPhrase,
                    Success = false
                };

                return error;
            }

        }


        protected async Task<ResultSO<TOutbound>> PostHttp<TInbound, TOutbound>(TInbound data, String controller)
        {

            var content = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");

            var result = await _client.PostAsync(controller, content);

            if (result.IsSuccessStatusCode)
            {
                try
                {
                    string json = await result.Content.ReadAsStringAsync();
                    ResultSO<TOutbound> response = new ResultSO<TOutbound>();
                    response.Success = true;
                    response.Value = JsonConvert.DeserializeObject<TOutbound>(json);
                    return response;
                }
                catch(Exception e)
                {
                    var error = new ResultSO<TOutbound>()
                    {
                        Error = result.StatusCode.ToString(),
                        Message = result.Content.ReadAsStringAsync().Result,
                        Success = false
                    };

                    return error;
                }
            }
            else
            {
                var error = new ResultSO<TOutbound>()
                {
                    Error = result.StatusCode.ToString(),
                    Message = result.Content.ReadAsStringAsync().Result,
                    Success = false
                };

                return error;
            }

        }

        protected async Task<ResultSO<IList<TOutbound>>> GetListHttp<TOutbound>(String controller, IDictionary<String, String> parameters = null)
        {

            HttpResponseMessage result = null;

            if (parameters == null)
                result = await _client.GetAsync(controller);
            else
                result = await _client.GetAsync(controller + "/" + parameters.ToQueryString());

            if (result.IsSuccessStatusCode)
            {
                return new ResultSO<IList<TOutbound>>()
                {
                    Success = true,
                    Value = JsonConvert.DeserializeObject<IList<TOutbound>>(await result.Content.ReadAsStringAsync())
                };
            }
            else
            {
                var error = new ResultSO<IList<TOutbound>>()
                {
                    Error = result.StatusCode.ToString(),
                    Message = result.Content.ReadAsStringAsync().Result,
                    Success = false
                };

                return error;
            }
        }


        protected async Task<ResultSO<TOutbound>> GetHttp<TOutbound>(String id, String controller, IDictionary<String, String> parameters = null)
        {
            HttpResponseMessage result = null;

            if (parameters == null)
                result = await _client.GetAsync(controller + "/" + id);
            else
                result = await _client.GetAsync(controller + "/" + id + parameters.ToQueryString());


            if (result.IsSuccessStatusCode)
            {
                ResultSO<TOutbound> res = new ResultSO<TOutbound>();
                try
                {
                    res.Success = true;
                    res.Value = JsonConvert.DeserializeObject<TOutbound>(await result.Content.ReadAsStringAsync());
                }
                catch(Exception e)
                {
                    var error = new ResultSO<TOutbound>();
                    error.Error = e.InnerException.ToString();
                    error.Message = e.Message+" - Failed to parse JSON";
                    error.Success = false;
                }
                return res;
            }
            else
            {
                var error = new ResultSO<TOutbound>()
                {
                    Error = result.StatusCode.ToString(),
                    Message = result.Content.ReadAsStringAsync().Result,
                    Success = false
                };

                return error;
            }
        }

        protected async Task<ResultSO<TOutbound>> DeleteHttp<TOutbound>(string id, String controller, IDictionary<String, String> parameters = null)
        {
            HttpResponseMessage result = null;

            if (parameters == null)
                result = await _client.DeleteAsync(controller + "/" + id.ToString());
            else
                result = await _client.DeleteAsync(controller + "/" + id.ToString() + parameters.ToQueryString());


            if (result.IsSuccessStatusCode)
            {
                return new ResultSO<TOutbound>()
                {
                    Success = true //No Content
                };
            }
            else
            {
                var error = new ResultSO<TOutbound>()
                {
                    Error = result.StatusCode.ToString(),
                    Message = result.ReasonPhrase,
                    Success = false
                };

                return error;
            }
        }



        public void InjectAuthorizationHeader(string token)
        {
            _client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
        }

    }
}
