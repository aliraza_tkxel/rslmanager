﻿using System.Threading.Tasks;
using FrontlineApp.ServiceLayer.ServiceObjects;
using FrontlineApp.DataLayer.Entities;
using System.Collections.Generic;
using FrontlineApp.Utility.Constants;
using FrontlineApp.DataLayer.Repository;
using FrontlineApp.Utility;

namespace FrontlineApp.ServiceLayer.Services
{
    public class AuthenticationService : ABaseService
    {
        public AuthenticationService(): base ()
        {

        }


        public async Task<ResultSO<HousingOfficer>> LoginUserWithParams(string userName, string password)
        {

            UserSO obj = new UserSO();
            obj.username = userName;
            obj.password = GeneralHelper.Base64Encode(password);
            HousingOfficer housingOfficerObj = null;
           
            
            ResultSO<HousingOfficer> result = await PostHttp<UserSO, HousingOfficer>(obj, UriTemplateConstants.AuthenticateUser);
            if (result.Success)
            {
                housingOfficerObj = result.Value as HousingOfficer;
                housingOfficerObj.IsLoggedIn = true;
                housingOfficerObj.Password = password;
                Repository<HousingOfficer> myRepo = new Repository<HousingOfficer>();
                HousingOfficer ReturnedObj =  myRepo.Get(housingOfficerObj.userId);
                if (ReturnedObj == null)
                {
                     myRepo.Insert(housingOfficerObj);
                }
                else
                {
                     myRepo.Update(housingOfficerObj);
                }
                
            }
            
            return result;

        }

        public async Task<bool> ForgotPasswordForEmail(string email)
        {
            
            IDictionary<string, string> paramDic = new Dictionary<string, string>();
            paramDic.Add("emailId", email);
            ResultSO<bool> result = await GetHttp<bool>(string.Empty, UriTemplateConstants.EmailForgottenPassword,paramDic);
            return result.Value;
        }
    }
}
