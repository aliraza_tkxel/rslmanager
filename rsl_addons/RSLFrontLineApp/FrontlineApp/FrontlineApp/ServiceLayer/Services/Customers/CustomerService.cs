﻿using AutoMapper;
using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.DataLayer.Entities;
using FrontlineApp.DataLayer.Repository;
using FrontlineApp.ServiceLayer.ServiceObjects;
using FrontlineApp.Utility.Constants;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FrontlineApp.ServiceLayer.Services
{
    public class CustomerService : ABaseService
    {
        #region Properties
        CustomerRepository CustomerRepo;
        #endregion

        #region "Constructor"
        public CustomerService() : base()
        {
            CustomerRepo = new CustomerRepository();
        }
        #endregion

        #region "Search Customer"
        public async Task<ResultModel<bool>> SearchCustomer(PaginationSO pageInfo, string searchText, bool isLookupRequired)
        {
            SearchCustomerRequestSO requestParam = new SearchCustomerRequestSO();
            requestParam.pagination = pageInfo;
            requestParam.isLookUpRequired = isLookupRequired;
            requestParam.searchText = searchText;

            // Get customer list from server side
            ResultSO<SearchCustomerResponseSO> response = await PostHttp<SearchCustomerRequestSO, SearchCustomerResponseSO>(requestParam, UriTemplateConstants.SearchCustomer);

            // Save information in local database
            ResultModel<bool> result = new ResultModel<bool>();
            if (response.Success)
            {
                var custResponse = response.Value as SearchCustomerResponseSO;
                if (custResponse != null)
                {
                    // Saving Customer Lookups 
                    LookupRepository lookupRepo = new LookupRepository();
                    result = lookupRepo.SaveCustomerlookup(custResponse.commonData.lookups);

                    // If lookups saved successfully save customer list
                    if (result.Success == true)
                    {
                        var list = Mapper.Map<List<CustomerSO>, List<Customer>>(custResponse.customerList);
                        result = CustomerRepo.SaveCustomerListing(list);
                    }
                }
                else
                {
                    result.Success = false;
                    result.Message = MessageConstants.FailedToLoadCustomerRecords;
                }
            }
            else
            {
                result.Success = false;
                result.Message = response.Message;
                result.Error = response.Error;
            }

            return result;
        }
        #endregion

        #region "Save Customer"
        /// <summary>
        //  0- Get Customer from Customer Repository using id
        //  1- Upload Image if any
        //  2- Map Customer to CustomerSO
        //  3- Send to server
        //  4- Update sync status of customer
        //  5- Delete customer recursivly using the id 
        //  6- Map CustomerSO to Customer
        //  7- Delete local Db record
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ResultModel<int?>> SaveCustomer(int? id)
        {
            ResultModel<int?> result = new ResultModel<int?>();
            var customerEntity = CustomerRepo.GetCustomerDetail(id);

            // Upload Customer Image
            var customerImageEntity = customerEntity.customerImage;
            var isImageSyncRequired = false;
            var isImageSyncSuccessful = false;

            if (customerImageEntity != null)
            {
                if (!string.IsNullOrEmpty(customerImageEntity.imageName)
                    && customerImageEntity.isSynced == false)
                {
                    isImageSyncRequired = true;
                    Uri sourceUri = new Uri(string.Format(PathConstants.LocalFolderPath + "{0}", customerImageEntity.imageName));
                    Uri destinationUri = new Uri(UriTemplateConstants.BaseURL + "/" + UriTemplateConstants.UploadCustomerProfilePic);
                    FileUploadingService service = new FileUploadingService();
                    var uploadResult = await service.UploadImage(sourceUri, destinationUri);
                    isImageSyncSuccessful = uploadResult.Success;
                }
            }

            if (isImageSyncRequired == true
                && isImageSyncSuccessful == false)
            {
                result.Success = false;
                result.Message = MessageConstants.FailedToUploadCustomerImage;
            }
            else
            {

                // Map Customer to CustomerSO
                var customerSO = Mapper.Map<Customer, CustomerSO>(customerEntity);
                var response = await PostHttp<CustomerSO, CustomerSO>(customerSO, UriTemplateConstants.AddEditcustomer);

                // Delete local db record            
                if (response.Success)
                {
                    var customerSoResponse = response.Value as CustomerSO;
                    if (customerSoResponse != null)
                    {
                        var customer = Mapper.Map<CustomerSO, Customer>(customerSoResponse);
                        result = CustomerRepo.SaveCustomerInfoFromServer(id, customer);
                    }
                }
                else
                {
                    result.Success = false;
                    result.Message = response.Message;
                }

            }

            return result;
        }
        #endregion

        #region "Get Customer Viewings"
        public async Task<ResultModel<bool>> GetCustomerViewings(CustomerModel customer)
        {
            ReferralRequestSO request = new ReferralRequestSO();
            request.customerId = (int)customer.CustomerId;
            request.startDate = DateTime.UtcNow.AddDays(ApplicationConstants.ViewingsMinDaysThreshold );
            request.endDate = DateTime.UtcNow.AddDays(ApplicationConstants.ViewingsMaxDaysThreshold);

            // Get customer viewings from server side
            ResultSO<List<CustomerViewingSO>> response = await PostHttp<ReferralRequestSO, List<CustomerViewingSO>>(request,UriTemplateConstants.GetCustomerViewings);

            // Save information in local database
            ResultModel<bool> result = new ResultModel<bool>();
            if (response.Success)
            {
                var custViewingsResponse = response.Value as List<CustomerViewingSO>;
                if (custViewingsResponse != null)
                {
                    var customerViewingEntity = Mapper.Map<List<CustomerViewingSO>, List<CustomerViewing>>(custViewingsResponse);
                    result = CustomerRepo.SaveCustomerViewing(customer,customerViewingEntity);
                }
                else
                {
                    result.Success = false;
                    result.Message = MessageConstants.FailedToLoadCustomerViewingsFromServer;
                }
            }
            else
            {
                result.Success = false;
                result.Message = response.Message;
                result.Error = response.Error;
            }

            return result;
        }
        #endregion

        #region "Lookups for Tenancy"
        public async Task<ResultModel<List<CustomerModel>>> GetCustomerLookUpList(PaginationSO pageInfo, string searchText, bool isLookupRequired)
        {
            SearchCustomerRequestSO requestParam = new SearchCustomerRequestSO();
            requestParam.pagination = pageInfo;
            requestParam.isLookUpRequired = isLookupRequired;
            requestParam.searchText = searchText;

            // Get customer list from server side
            ResultSO<SearchCustomerResponseSO> response = await PostHttp<SearchCustomerRequestSO, SearchCustomerResponseSO>(requestParam, UriTemplateConstants.SearchCustomer);

            // Save information in local database
            ResultModel<List<CustomerModel>> result = new ResultModel<List<CustomerModel>>();
            if (response.Success)
            {
                var custResponse = response.Value as SearchCustomerResponseSO;
                if (custResponse != null)
                {
                    // Saving Customer Lookups 
                    LookupRepository lookupRepo = new LookupRepository();
                    ResultModel<bool>result2 = lookupRepo.SaveCustomerlookup(custResponse.commonData.lookups);

                    // If lookups saved successfully save customer list
                    if (result2.Success == true)
                    {
                        var list = Mapper.Map<List<CustomerSO>, List<Customer>>(custResponse.customerList);
                        var list2 = Mapper.Map<List<Customer>, List<CustomerModel>>(list);
                        result.Value = list2;
                        result.Success = true;
                    }
                }
                else
                {
                    result.Success = false;
                    result.Message = MessageConstants.FailedToLoadCustomerRecords;
                }
            }
            else
            {
                result.Success = false;
                result.Message = response.Message;
                result.Error = response.Error;
            }

            return result;
        }

        public CustomerSO ConvertModelToSOForTenancy(CustomerModel model)
        {
            var customerSo = Mapper.Map<CustomerModel, CustomerSO>(model);
            return customerSo;
        }
        public async Task<ResultModel<CustomerModel>> GetCustomerDetailLookUp(int? id, CustomerSO customer)
        {
            IDictionary<string, string> paramDic = new Dictionary<string, string>();
            paramDic.Add("customerId", customer.customerId.ToString());

            // Get customer detail from server side
            ResultSO<CustomerSO> response = await GetHttp<CustomerSO>(string.Empty, UriTemplateConstants.GetCustomerDetail, paramDic);


            ResultModel<CustomerModel> result = new ResultModel<CustomerModel>();
            if (response.Success)
            {
                var custResponse = response.Value as CustomerSO;
                if (custResponse != null)
                {
                    var customerEntity = Mapper.Map<CustomerSO, Customer>(custResponse);
                    result.Value = Mapper.Map<Customer, CustomerModel>(customerEntity);
                    result.Success = true;
                }
                else
                {
                    result.Success = false;
                    result.Message = MessageConstants.FailedToLoadCustomerDetailFromServer;
                }
            }
            else
            {
                result.Success = false;
                result.Message = response.Message;
                result.Error = response.Error;
            }

            return result;
        }
        #endregion

        #region "Get Customer Detail"
        public async Task<ResultModel<int?>> GetCustomerDetail(int? id, CustomerSO customer)
        {
            IDictionary<string, string> paramDic = new Dictionary<string, string>();
            paramDic.Add("customerId", customer.customerId.ToString());

            // Get customer detail from server side
            ResultSO<CustomerSO> response = await GetHttp<CustomerSO>(string.Empty, UriTemplateConstants.GetCustomerDetail, paramDic);

            // Save information in local database
            ResultModel<int?> result = new ResultModel<int?>();
            if (response.Success)
            {
                var custResponse = response.Value as CustomerSO;
                if (custResponse != null)
                {
                    var customerEntity = Mapper.Map<CustomerSO, Customer>(custResponse);
                    result = CustomerRepo.SaveCustomerInfoFromServer(id, customerEntity);
                }
                else
                {
                    result.Success = false;
                    result.Message = MessageConstants.FailedToLoadCustomerDetailFromServer;
                }
            }
            else
            {
                result.Success = false;
                result.Message = response.Message;
                result.Error = response.Error;
            }

            return result;
        }
        #endregion

        #region "Get Customer Referral"
        public async Task<ResultModel<bool>> GetCustomerReferral(CustomerModel customer)
        {
            IDictionary<string, string> paramDic = new Dictionary<string, string>();
            paramDic.Add("customerId", customer.CustomerId.ToString());

            // Get customer referral from server side
            ResultSO<ReferralSO> response = await GetHttp<ReferralSO>(string.Empty, UriTemplateConstants.GetReferralDetails, paramDic);

            // Save information in local database
            ResultModel<bool> result = new ResultModel<bool>();
            if (response.Success)
            {
                var referralResponse = response.Value as ReferralSO;
                if (referralResponse != null)
                {
                    var customerReferralEntity = Mapper.Map<ReferralSO, CustomerReferral>(referralResponse);
                    result = CustomerRepo.SaveCustomerReferralFromServer(customer, customerReferralEntity);
                }
                
                result.Success = true;
            }
            else
            {
                result.Success = false;
                result.Message = response.Message;
                result.Error = response.Error;
            }

            return result;
        }
        #endregion

        #region "Tenancy Module"
        public async Task<ResultModel<TenancySO>> CreateTenancy(TenancyModel tenancy)
        {
            ResultModel<TenancySO> result = new ResultModel<TenancySO>();
            TenancySO requestSO = new TenancySO();
            requestSO = Mapper.Map<TenancyModel, TenancySO > (tenancy);
            ResultSO<TenancySO> response = await PostHttp<TenancySO, TenancySO>(requestSO, UriTemplateConstants.CreateTenancy);
            if (response.Success)
            {
                //var tenancyResult = response.Value as TenancySO;
                //result.Value = Mapper.Map<TenancySO, TenancyModel>(tenancyResult);
                result.Value = response.Value;
                result.Success = response.Success;
                result.Message = "";
            }
            else
            {
                result.Success = false;
                result.Message = response.Message;
                result.Error = response.Error;
            }
            return result;
        }
        public async Task<ResultModel<PropertyRentModel>> GetPropertyFinancialInfoForId(string propertyId)
        {
            IDictionary<string, string> paramDic = new Dictionary<string, string>();
            paramDic.Add("propertyId", propertyId);
            ResultModel<PropertyRentModel> result = new ResultModel<PropertyRentModel>();
            ResultSO<PropertyRentSO> response = await GetHttp<PropertyRentSO>(string.Empty, UriTemplateConstants.GetPropertyFinancialInfo, paramDic);
            if (response.Success)
            {
                var res = response.Value as PropertyRentSO;
                result.Success = true;
                result.Value = Mapper.Map<PropertyRentSO, PropertyRentModel>(res);
            }
            else
            {
                result.Success = false;
                result.Message = response.Message;
                result.Error = response.Error;
            }
            return result;
        }
        #endregion
		#region "Save Customer Referral"
        public async Task<ResultModel<bool>> SaveCustomerReferral(CustomerModel customer)
        {
            var customerReferralSO = Mapper.Map<ReferralModel, ReferralSO>(customer.Referral);

            // Save customer referral on server side
            ResultSO<ReferralSO> serviceResponse = await PostHttp<ReferralSO, ReferralSO>(customerReferralSO, UriTemplateConstants.SaveReferralDetails);

            // Save information in local database
            ResultModel<bool> result = new ResultModel<bool>();
            if (serviceResponse.Success)
            {
                var referralSO = serviceResponse.Value as ReferralSO;
                if (referralSO != null)
                {
                    var referralEntity = Mapper.Map<ReferralSO, CustomerReferral>(referralSO);
                    result = CustomerRepo.SaveCustomerReferralFromServer(customer, referralEntity);
                }

                result.Success = true;
            }
            else
            {
                result.Success = false;
                result.Message = serviceResponse.Message;
                result.Error = serviceResponse.Error;
            }

            return result;
        }
        #endregion

        #region "Close Risk Enquiry"
        public async Task<ResultModel<bool>> CloseRiskEnquiry(CustomerRiskModel risk)
        {
            var customerRiskSO = Mapper.Map<CustomerRiskModel, CustomerRiskSO>(risk);

           
            ResultSO<CustomerRiskSO> serviceResponse = await PostHttp<CustomerRiskSO, CustomerRiskSO>(customerRiskSO, UriTemplateConstants.AddUpdateRisk);

            ResultModel<bool> result = new ResultModel<bool>();
            if (serviceResponse.Success)
            {
                result.Success = true;
            }
            else
            {
                result.Success = false;
                result.Message = serviceResponse.Message;
                result.Error = serviceResponse.Error;
            }

            return result;
        }
        #endregion

        #region "Close Vulnerability Enquiry"
        public async Task<ResultModel<bool>> CloseVulnerabilityEnquiry(CustomerVulnerabilityModel vulnerability)
        {
            var customerVulnerabilitySO = Mapper.Map<CustomerVulnerabilityModel, CustomerVulnerabilitySO>(vulnerability);

            ResultSO<CustomerVulnerabilitySO> serviceResponse = await PostHttp<CustomerVulnerabilitySO, CustomerVulnerabilitySO>(customerVulnerabilitySO, UriTemplateConstants.AddUpdateVulnerability);

            ResultModel<bool> result = new ResultModel<bool>();
            if (serviceResponse.Success)
            {
                result.Success = true;
            }
            else
            {
                result.Success = false;
                result.Message = serviceResponse.Message;
                result.Error = serviceResponse.Error;
            }

            return result;
        }
        #endregion

        #region "Save Customer Referral"
        public async Task<ResultModel<TerminationSO>> TerminateTenancy(CustomerModel customer,TerminationModel terminationInfo)
        {
            var terminationRequestSO = Mapper.Map<TerminationModel, TerminationSO>(terminationInfo);

            ResultSO<TerminationSO> serviceResponse = await PostHttp<TerminationSO, TerminationSO>(terminationRequestSO, UriTemplateConstants.TerminateTenancy);

            ResultModel<TerminationSO> result = new ResultModel<TerminationSO>();
            if (serviceResponse.Success)
            {
                var terminationSO = serviceResponse.Value as TerminationSO;
                if (terminationSO != null)
                {
                    var tenancyEntity = Mapper.Map<TerminationSO, TenancyTermination>(terminationSO);
                    ResultModel<bool> dbResult = CustomerRepo.SaveTenancyTerminationInfoFromServer(customer, tenancyEntity);
                }

                result.Success = true;
                result.Value = terminationSO;
            }
            else
            {
                result.Success = false;
                result.Message = serviceResponse.Message;
                result.Error = serviceResponse.Error;
            }

            return result;
        }
        #endregion

        #region "Get Customer Risks"
        public async Task<ResultModel<bool>> GetCustomerRisks(CustomerModel customerModel)
        {
            IDictionary<string, string> paramDic = new Dictionary<string, string>();
            paramDic.Add("customerId", customerModel.CustomerId.ToString());

            ResultSO<List<CustomerRiskSO>> serverResponse = await GetHttp<List<CustomerRiskSO>>(string.Empty, UriTemplateConstants.GetCustomerRisksList, paramDic);

            ResultModel<bool> result = new ResultModel<bool>();
            if (serverResponse.Success)
            {
                var customerRiskList = serverResponse.Value as List<CustomerRiskSO>;
                var list = Mapper.Map<List<CustomerRiskSO>, List<CustomerRisk>>(customerRiskList);
                result = CustomerRepo.SaveCustomerRisks(customerModel,list);
            }
            else
            {
                result.Success = false;
                result.Message = serverResponse.Message;
                result.Error = serverResponse.Error;
            }

            return result;
        }
        #endregion

        #region "Get Customer Vulnerabilities"
        public async Task<ResultModel<bool>> GetCustomerVulnerabilities(CustomerModel customerModel)
        {
            IDictionary<string, string> paramDic = new Dictionary<string, string>();
            paramDic.Add("customerId", customerModel.CustomerId.ToString());

            ResultSO<List<CustomerVulnerabilitySO>> serverResponse = await GetHttp<List<CustomerVulnerabilitySO>>(string.Empty, UriTemplateConstants.GetCustomerVulnerabilitiesList, paramDic);

            ResultModel<bool> result = new ResultModel<bool>();
            if (serverResponse.Success)
            {
                var customerVulnerabilities = serverResponse.Value as List<CustomerVulnerabilitySO>;
                var list = Mapper.Map<List<CustomerVulnerabilitySO>, List<CustomerVulnerability>>(customerVulnerabilities);
                result = CustomerRepo.SaveCustomerVulnerabilities(customerModel, list);
            }
            else
            {
                result.Success = false;
                result.Message = serverResponse.Message;
                result.Error = serverResponse.Error;
            }

            return result;
        }
        #endregion

        #region "Void Inspection"
        public async Task<VoidInspectionSupervisorsResponseSO> GetVoidInspectionSupervisors(VoidInspectionSupervisorsRequestSO request)
        {
            ResultSO<VoidInspectionSupervisorsResponseSO> ResultObj = await PostHttp<VoidInspectionSupervisorsRequestSO, VoidInspectionSupervisorsResponseSO>(request, UriTemplateConstants.GetVoidInspectionSupervisors);
            return ResultObj.Value;
        }

        public async Task<VoidInspectionRootResponseSO> CreateVoidInspection(CreateVoidInspectionRequestSO request)
        {
            ResultSO<VoidInspectionRootResponseSO> ResultObj = await PostHttp<CreateVoidInspectionRequestSO, VoidInspectionRootResponseSO>(request, UriTemplateConstants.CreateVoidInspectionAppointment);
            return ResultObj.Value;
        }

        #endregion

    }
}
