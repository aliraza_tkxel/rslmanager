﻿using System.Threading.Tasks;
using FrontlineApp.ServiceLayer.ServiceObjects;
using FrontlineApp.DataLayer.Entities;
using FrontlineApp.Utility.Constants;
using System.Collections.Generic;
using FrontlineApp.DataLayer.Repository;
using FrontlineApp.ApplicationLayer.Models;

namespace FrontlineApp.ServiceLayer.Services
{
    public class SharedLookUpsService : ABaseService
    {
        #region Properties
        LookupRepository lookupRepo;
        #endregion

        #region Constructor
        public SharedLookUpsService() : base()
        {
            lookupRepo = new LookupRepository();
        }
        public async Task<bool> GetSharedLookUpTables()
        {
            ResultSO<SharedLookUpResponseSO> serviceResult = await GetHttp<SharedLookUpResponseSO>(string.Empty, UriTemplateConstants.GetSharedLookUps);
            ResultModel<bool> result = new ResultModel<bool>();

            if (serviceResult.Success)
            {
                result = lookupRepo.SaveSharedLookup(serviceResult.Value);
            }
            else
            {
                result.Success = false;
                result.Message = serviceResult.Message;
            }

            return result.Success;
        }
        #endregion


    }
}
