﻿using System.Threading.Tasks;
using FrontlineApp.ServiceLayer.ServiceObjects;
using FrontlineApp.DataLayer.Entities;
using FrontlineApp.Utility.Constants;
using System.Collections.Generic;
using FrontlineApp.DataLayer.Repository;
using System;
using FrontlineApp.ApplicationLayer.Models;

namespace FrontlineApp.ServiceLayer.Services
{
    public class PropertyService : ABaseService
    {

        #region Properties
        PropertyRepository propertyRepo;
        #endregion

        #region "Constructor"
        public PropertyService() : base()
        {
            propertyRepo = new PropertyRepository();
        }
        #endregion

        public async Task<bool> GetPropertyViewingsForId(string propertyId)
        {
            PropertyViewingsRequestSO reqParams = new PropertyViewingsRequestSO();
            reqParams.propertyId = propertyId;
            ResultSO<List<PropertyViewing>> result = await PostHttp<PropertyViewingsRequestSO, List<PropertyViewing>>(reqParams, UriTemplateConstants.GetPropertyViewingsList);
            if (result.Success)
            {
                 InsertOrUpdatePropertyViewings(result.Value);
            }
            return result.Success;
        }

        public async Task<bool> GetPropertyDetailsForId(string propertyId)
        {
            IDictionary<string, string> paramDic = new Dictionary<string, string>();
            paramDic.Add("propertyId", propertyId);

            ResultSO<PropertyDetails> result = await GetHttp<PropertyDetails>(string.Empty, UriTemplateConstants.GetPropertyDetails, paramDic);
            if (result.Success)
            {
                PropertyDetails detailedObj = result.Value as PropertyDetails;
                 InsertOrUpdatePropertyDetailsObject(detailedObj);
                return true;
            }
            return false;
        }
        public async Task<PropertySearchResultSO> GetAllProperties(bool isLookUpRequired)
        {
            IDictionary<string, string> paramDic = new Dictionary<string, string>();
            paramDic.Add("isLookUpRequired", isLookUpRequired.ToString());
            ResultSO<PropertySearchResultSO> result = await GetHttp<PropertySearchResultSO>(string.Empty, UriTemplateConstants.SearchProperty, paramDic);
            PropertySearchResultSO searchResults = null;

            if (result.Success)
            {
                searchResults = result.Value as PropertySearchResultSO;
            }

            if (searchResults != null)
            {
                InsertPropertyDataForResults(searchResults);
                InsertPropertySchemesForResults(searchResults);

            }
            return searchResults;

     }

     #region PropertyInsertion


     public  bool InsertPropertyDataForResults(PropertySearchResultSO searchResults)
     {
        
            Repository < Property> myRepo = new Repository<Property>();
            myRepo.InsertOrReplaceAll(searchResults.propertiesData);
            return true;
        }
        public  bool InsertOrUpdatePropertyDetailsObject(PropertyDetails destinationObj)
        {
            try
            {
                Repository<PropertyDetails> myRepo = new Repository<PropertyDetails>();
                PropertyDetails returnedObj =  myRepo.Get(destinationObj.propertyId);
                if (returnedObj == null)
                {
                     myRepo.Insert(destinationObj);
                }
                else
                {
                     myRepo.Update(destinationObj);
                }
            }
            catch (Exception e)
            {
                return false;
            }
            return true;

        }
        public  bool InsertOrUpdatePropertyObject(Property destinationObj)
        {
            try
            {
                Repository<Property> myRepo = new Repository<Property>();
                Property returnedObj =  myRepo.Get(destinationObj.propertyId);
                if (returnedObj == null)
                {
                     myRepo.Insert(destinationObj);
                }
                else
                { 
                     myRepo.Update(destinationObj);
                }
            }
            catch
            {

            }
            return true;
           
        }

        public  bool InsertOrUpdatePropertyViewings(List<PropertyViewing> result)
        {
            Repository<PropertyViewing> myRepo = new Repository<PropertyViewing>();
             myRepo.InsertOrReplaceAll(result);
            return true;
        }
        public  bool InsertPropertySchemesForResults(PropertySearchResultSO searchResults)
        {
            Repository<PropertyScheme> myRepo = new Repository<PropertyScheme>();
             myRepo.InsertOrReplaceAll(searchResults.lookups.schemeModel);
            return true;
        }
        #endregion
    }
}
