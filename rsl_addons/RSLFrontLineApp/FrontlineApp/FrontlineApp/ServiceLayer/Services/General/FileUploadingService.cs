﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.ServiceLayer.ServiceObjects;
using FrontlineApp.Utility.Constants;
using System;
using System.Threading.Tasks;
using Windows.Web.Http;
using Xamarin.Forms;

namespace FrontlineApp.ServiceLayer.Services
{
    public class FileUploadingService
    {

        public async Task<ResultSO<bool>> UploadImage(Uri sourceUri,Uri destinationUri)
        {
            ResultModel<bool> uploadResult = await DependencyService.Get<FileBase>().UploadImageFromPlatformSpecificLocation(sourceUri, destinationUri);

            if (uploadResult.Success)
            {
                return new ResultSO<bool>()
                {
                    Success = true,
                    Value = true
                };
            }
            else
            {
                var error = new ResultSO<bool>()
                {
                    Message = uploadResult.Message,
                    Success = false
                };

                return error;
            }

        }

    }
}
