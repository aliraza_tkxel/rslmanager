﻿using FrontlineApp.ApplicationLayer.Models;
using System;
using System.IO;
using System.Threading.Tasks;
using Windows.Web.Http;

namespace FrontlineApp.ServiceLayer.Services
{
    public abstract class FileBase
    {
        public abstract Task<ResultModel<bool>> UploadImageFromPlatformSpecificLocation(Uri sourceUri,Uri destinationUri);
    }
}
