﻿using System.Collections.Generic;

namespace FrontlineApp.DataLayer.Repository
{
    public interface IRepository<T> where T : class
    {
        int Count();

        List<T> GetAll();

        int Insert(T item);

        T Get(string idValue);
        T Get(int idValue);

        void InsertOrReplace(T item);

        void Delete(T item);

        void InsertAll(IEnumerable<T> items);

        void InsertOrReplaceAll(IEnumerable<T> items);

        void Update(T item);

        void UpdateAll(IEnumerable<T> items);

        void ClearTable();
    }
}