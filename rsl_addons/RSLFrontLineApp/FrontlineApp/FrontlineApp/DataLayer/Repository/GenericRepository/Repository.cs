﻿using FrontlineApp.DataLayer.Repository;
using SQLite.Net;
using SQLite.Net.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using AutoMapper;
using Xamarin.Forms;
using FrontlineApp.DataLayer.Database;
using System.Linq.Expressions;

namespace FrontlineApp.DataLayer.Repository
{
    public class Repository<T> : IRepository<T> where T : class, new()
    {
        protected readonly SQLiteConnection dbContext;

        public Repository()
        {
            this.dbContext = DependencyService.Get<FrontlineDatabase>().DBContext;
        }

        public Repository(SQLiteConnection ctx)
        {
            dbContext = ctx;
        }

        public int Count()
        {
            string query = String.Format("SELECT COUNT(*) AS Count FROM '{0}'", GetTableName());
            var result = dbContext.Query<TableCountMap>(query);
            var firstItem = result.First();
            return firstItem.NumberOfItems;
        }

        public int GetMaxId()
        {
            string query = String.Format("SELECT Max(id) AS MaxId FROM '{0}'", GetTableName());
            var result = dbContext.Query<TableMaxMap>(query);
            var firstItem = result.First();
            return firstItem.MaxId;
        }

        public List<T> GetAll()
        {
            return new List<T>(dbContext.Table<T>().AsEnumerable());
        }

        public List<T> Get<TValue>(Expression<Func<T, bool>> predicate = null, Expression<Func<T, TValue>> orderBy = null)
        {
            var query = dbContext.Table<T>();

            if (predicate != null)
                query = query.Where(predicate);

            if (orderBy != null)
                query = query.OrderBy<TValue>(orderBy);

            return query.ToList();
        }

        public int Insert(T item)
        {
            return dbContext.Insert(item);
        }

        public T Get(string idValue)
        {
            try
            {
                var query = String.Format("SELECT * FROM {0} WHERE {1} = '{2}'", GetTableName(), GetPrimaryKeys().First().Name, idValue);

                return dbContext.Query<T>(query).Single();
            }
            
             catch (Exception e)
            {
                return null;
            }
        }

        public T Get(int idValue)
        {
            try
            {
                var query = String.Format("SELECT * FROM {0} WHERE {1} = {2}", GetTableName(), GetPrimaryKeys().First().Name, idValue);
                return dbContext.Query<T>(query).FirstOrDefault();
            }
            catch(Exception e)
            {
                return null;
            }
        }

        public void InsertOrReplace(T item)
        {
            dbContext.InsertOrReplace(item);
        }

        public void Delete(T item)
        {
            dbContext.Delete(item);
        }

        public void InsertAll(IEnumerable<T> items)
        {
            dbContext.InsertAll(items);
        }

        public void InsertOrReplaceAll(IEnumerable<T> items)
        {
            dbContext.InsertOrReplaceAll(items);
        }

        public void ClearTable()
        {
            var query = String.Format("DELETE FROM {0}", GetTableName());
            dbContext.Execute(query);
        }

        public void Update(T item)
        {
            dbContext.Update(item);
        }

        public void UpdateAll(IEnumerable<T> items)
        {
            dbContext.UpdateAll(items);
        }

        public List<TModel> GetModelList<TModel>()
        {
            Mapper.CreateMap<T, TModel>();
            return Mapper.Map<IEnumerable<T>,List<TModel>> (dbContext.Table<T>().AsEnumerable());
        }


        public void ResetAndInsertAll(IEnumerable<T> items)
        {
            dbContext.DeleteAll<T>();
            if (items != null)
            {
                dbContext.InsertAll(items);
            }
        }

        private string GetTableName()
        {
            return typeof(T).Name;
            //var attributes = typeof(T).GetTypeInfo().GetCustomAttributes(false);
            //var columnMapping = attributes.FirstOrDefault(m => m.GetType() == typeof(TableAttribute));

            //var mapsTo = (TableAttribute)columnMapping;
            //return mapsTo.Name;
        }

        private IEnumerable<PropertyInfo> GetPrimaryKeys()
        {
            var primaryKeysProperty =
                typeof(T).GetTypeInfo()
                    .DeclaredProperties.Where(
                        m => m.GetCustomAttributes(false).Any(at => at.GetType() == typeof(PrimaryKeyAttribute)));

            return primaryKeysProperty;
        }

        internal class TableCountMap
        {
            public int NumberOfItems { get; set; }
        }
        internal class TableMaxMap
        {
            public int MaxId { get; set; }
        }
    }
}