﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.DataLayer.Database;
using FrontlineApp.DataLayer.Entities;
using FrontlineApp.ServiceLayer.ServiceObjects;
using SQLite.Net;
using Xamarin.Forms;
using System.Linq;
using System;
using FrontlineApp.Utility.Helper.AppSession;

namespace FrontlineApp.DataLayer.Repository
{
    public class HousingOfficerRepository
    {
        #region "Properties"
        private SQLiteConnection dbContext;
        #endregion

        #region "Constructors"
        public HousingOfficerRepository()
        {
            this.dbContext = DependencyService.Get<FrontlineDatabase>().DBContext;
        }
        public HousingOfficerRepository(SQLiteConnection ctx)
        {
            dbContext = ctx;
        }
        #endregion

        #region "Get Housing Officers"
        public List<HousingOfficer> GetHousingOfficers()
        {
            var repo = new Repository<HousingOfficer>(dbContext);
            return repo.GetModelList<HousingOfficer>().OrderBy(x => x.userId).ToList();
        }
        #endregion
    }
}
