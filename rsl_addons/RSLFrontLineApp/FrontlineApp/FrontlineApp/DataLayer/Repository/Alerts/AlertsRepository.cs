﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.DataLayer.Database;
using SQLite.Net;
using System;
using System.Collections.Generic;
using Xamarin.Forms;
using SQLiteNetExtensions.Extensions;
using FrontlineApp.DataLayer.Entities;

namespace FrontlineApp.DataLayer.Repository
{
    public class AlertsRepository
    {
        #region "Properties"
        private SQLiteConnection dbCtx;
        #endregion

        #region "Constructor"
        public AlertsRepository()
        {
            this.dbCtx = DependencyService.Get<FrontlineDatabase>().DBContext;
        }
        #endregion

        #region "Save Risk Listing"
        public ResultModel<bool> SaveRiskListing(List<AlertRisk> riskList)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            try
            {
                dbCtx.BeginTransaction();

                dbCtx.DeleteAll<AlertRisk>();

                if (riskList != null)
                {                                        
                    dbCtx.InsertAll(riskList);
                }

                dbCtx.Commit();
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Error = ex.Message;
                result.Message = ex.Message;
                dbCtx.Rollback();
            }

            return result;
        }
        #endregion

        #region "Save Vulnerability Listing"
        public ResultModel<bool> SaveVulnerabilityListing(List<AlertVulnerability> vulnerabilityList)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            try
            {
                dbCtx.BeginTransaction();

                dbCtx.DeleteAll<AlertVulnerability>();
                if (vulnerabilityList != null)
                {
                    dbCtx.InsertAll(vulnerabilityList);
                }

                dbCtx.Commit();
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Error = ex.Message;
                result.Message = ex.Message;
                dbCtx.Rollback();
            }

            return result;
        }
        #endregion

        #region "Get Open Vulnerability Counts"
        public ResultModel<int> GetOpenVulnerabilityCounts()
        {
            ResultModel<int> result = new ResultModel<int>();
            try
            {

                var vulnerabilityCount = dbCtx.Table<AlertVulnerability>().Count();
                result.Value = vulnerabilityCount;
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Error = ex.Message;
                result.Message = ex.Message;
            }

            return result;
        }
        #endregion

        #region "Get Open Risks Counts"
        public ResultModel<int> GetOpenRisksCounts()
        {
            ResultModel<int> result = new ResultModel<int>();
            try
            {

                var riskCount = dbCtx.Table<AlertRisk>().Count();
                result.Value = riskCount;
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Error = ex.Message;
                result.Message = ex.Message;
            }

            return result;
        }
        #endregion

    }

}
