﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.DataLayer.Database;
using FrontlineApp.DataLayer.Entities;
using FrontlineApp.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;
using SQLite.Net;

namespace FrontlineApp.DataLayer.Repository
{
    public class PropertyRepository
    {
        #region "Properties"
        private SQLiteConnection dbCtx;
        #endregion

        #region "Constructor"
        public PropertyRepository()
        {
            this.dbCtx = DependencyService.Get<FrontlineDatabase>().DBContext;
        }
        #endregion
        #region "Get Property Viewings List"
        public  List<PropertyViewingModel> GetPropertyViewingList(string propertyId)
        {
            var viewingsList =  dbCtx.Table<PropertyViewing>().ToList();
            var viewingStatusList = dbCtx.Table<ViewingStatus>().ToList();
            var query = from propView in viewingsList
                        where propView.propertyId == propertyId
                        join vStatus in viewingStatusList on propView.viewingStatusId equals vStatus.id
                        select new PropertyViewingModel()
                        {
                            PropertyId = propView.propertyId,
                            ViewingId = propView.viewingId,
                            ViewingStatusDescription = vStatus.description,
                            ViewingStatusId = propView.viewingStatusId,
                            ViewingDateFormatted = GeneralHelper.GetCompleteCreationDateStringFromServerDate(propView.viewingDate),
                            ViewingTimeFormatted = GeneralHelper.GetTimeStringFromServerDate(propView.viewingTime),
                            ViewingDate = DateTime.Parse(propView.viewingDate),
                            CustomerId = propView.customerId,
                            CustomerName = propView.customerName,
                            HousingOfficerId = propView.housingOfficerId,
                            HousingOfficerName = propView.housingOfficerName
                        };
            List<PropertyViewingModel> propList = new List<PropertyViewingModel>();
            if (query != null)
            {
                propList = query.ToList();
            }
            return propList;
        }
        #endregion

        #region "Get Property Detail"
        public PropertyDetailsModel GetPropertyDetailsWithId (string propertyId)
        {
            List<PropertyDetailsModel> prop = new List<PropertyDetailsModel>();
            if (!string.IsNullOrWhiteSpace(propertyId))
            {
                prop = GetPropertyDetailsForId(propertyId);
            }
            PropertyDetailsModel model = new PropertyDetailsModel();
            if (prop.Count > 0)
            {
                model = prop.ElementAt(0);
            }
            return model;
        }
        private List<PropertyDetailsModel> GetPropertyDetailsForId(string propertyId)
        {
            var properties = dbCtx.Table<PropertyDetails>().ToList();
            var propertySchemes = dbCtx.Table<PropertyScheme>().ToList();
            var propertyTypes = dbCtx.Table<PropertyType>().ToList();
            var query = from enti in properties
                        where enti.propertyId == propertyId
                        join pType in propertyTypes on enti.propertyTypeId equals pType.id
                        join pScheme in propertySchemes on enti.schemeId equals pScheme.id into pschemeGroup
                        from pScheme in pschemeGroup.DefaultIfEmpty()
                        select new PropertyDetailsModel()
                        {
                            propertyId = enti.propertyId,
                            propertyTypeId = enti.propertyTypeId,
                            developmentType = enti.developmentType,
                            schemeId = enti.schemeId,
                            phase = enti.phase,
                            block = enti.block,
                            houseNo = enti.houseNo,
                            address = enti.address,
                            townCity = enti.townCity,
                            county = enti.county,
                            schemeDescription = (pScheme == null) ? "N/A" : pScheme.description,
                            propertyTypeDescription = pType.description,
                            imageName = string.IsNullOrEmpty(enti.imageName) ? "" : enti.imageName,
                            postalCode = enti.postalCode,
                            rent = string.Format("£{0:0.00}", enti.rent),
                            status = enti.status,
                            rightToAcquire = enti.rightToAcquire,
                            assetType = enti.assetType,
                            floorArea = string.Format("{0} sqm", enti.floorArea),
                            maxPeople = enti.maxPeople,
                            numberOfLivingRooms = enti.numberOfLivingRooms,
                            numberOfBedRooms = enti.numberOfBedRooms,
                            numberOfBathRooms = enti.numberOfBathRooms,
                            numberOfkitchen = enti.numberOfkitchen,
                            parking = enti.parking,
                            garden = enti.garden,
                            garage = enti.garage,
                            heatingFuel = enti.heatingFuel,
                            boilerType = enti.boilerType,
                            wheelChairAccess = enti.wheelChairAccess

                        };
            List<PropertyDetailsModel> propList = new List<PropertyDetailsModel>();
            if (query != null)
            {
                propList = query.ToList();
            }
            return propList;
        }
        #endregion

        #region "Get Property Listing"
        public List<PropertyModel> GetPropertyList()
        {
            var properties =  dbCtx.Table<Property>().ToList();
            var propertySchemes =  dbCtx.Table<PropertyScheme>().ToList();
            var propertyTypes =  dbCtx.Table<PropertyType>().ToList();

            var query = from prop in properties
                        join pType in propertyTypes on prop.propertyTypeId equals pType.id
                        join pScheme in propertySchemes on prop.schemeId equals pScheme.id into pschemeGroup
                        from pScheme in pschemeGroup.DefaultIfEmpty()
                        select new PropertyModel()
                        {
                            propertyId = prop.propertyId,
                            address = prop.address,
                            scheme = (pScheme==null)? "N/A": pScheme.description,
                            propertyType = pType.description,
                            bedRooms = string.IsNullOrEmpty(prop.bedRooms) ? "0" : prop.bedRooms,
                            imageName = string.IsNullOrEmpty(prop.imageName) ? "" : prop.imageName,
                            status = prop.status,
                            subStatus = prop.subStatus,
                            projectedRent = string.IsNullOrEmpty(string.Format("{0:0.00}", Convert.ToDouble(prop.rent))) ? "£0.00" : "£" + string.Format("{0:0.00}", Convert.ToDouble(prop.rent)),
                            rentNumeric = Convert.ToDouble(prop.rent),
                            postalCode = string.IsNullOrEmpty(prop.postalCode) ? "" : prop.postalCode,
                            schemeId = prop.schemeId,
                            propertyTypeId = prop.propertyTypeId
                            
        };

            List<PropertyModel> propList = new List<PropertyModel>();
            if (query != null)
            {
                propList = query.ToList();               
            }

            return propList;

        }
        #endregion

        #region "Delete All"
        public ResultModel<bool> DeleteAllProeprties()
        {
            ResultModel<bool> result = new ResultModel<bool>();
            try
            {
                dbCtx.BeginTransaction();
                dbCtx.DeleteAll<Property>();
                dbCtx.DeleteAll<PropertyScheme>();
                dbCtx.DeleteAll<PropertyViewing>();
                dbCtx.Commit();
                result.Success = true;
            }
            catch (Exception ex)
            {
                dbCtx.Rollback();
                result.Success = false;
                result.Error = ex.Message;
            }

            return result;
        }
        public ResultModel<bool> DeleteAllPropertyViewings()
        {
            ResultModel<bool> result = new ResultModel<bool>();
            try
            {
                dbCtx.BeginTransaction();
                dbCtx.DeleteAll<PropertyViewing>();
                dbCtx.Commit();
                result.Success = true;
            }
            catch (Exception ex)
            {
                dbCtx.Rollback();
                result.Success = false;
                result.Error = ex.Message;
            }

            return result;
        }
        #endregion
    }
}
