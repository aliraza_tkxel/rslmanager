﻿using System.Collections.Generic;
using System.Threading.Tasks;
using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.DataLayer.Database;
using FrontlineApp.DataLayer.Entities;
using FrontlineApp.ServiceLayer.ServiceObjects;
using SQLite.Net;
using Xamarin.Forms;
using System.Linq;
using System;
using FrontlineApp.Utility.Helper.AppSession;

namespace FrontlineApp.DataLayer.Repository
{
    public class LookupRepository
    {
        #region "Properties"
        private SQLiteConnection dbContext;
        #endregion

        #region "Constructor"
        public LookupRepository()
        {
            this.dbContext = DependencyService.Get<FrontlineDatabase>().DBContext;
        }
        public LookupRepository(SQLiteConnection ctx)
        {
            dbContext = ctx;
        }


        #endregion

        #region "Methods"

        #region "Customer Lookups"        

        #region "Save Customer lookups"
        public ResultModel<bool> SaveCustomerlookup(CustomerLookupSO lookup)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            result.Success = true;
            if (lookup != null)
            {
                try
                {
                    dbContext.BeginTransaction();

                    
                    SaveLookup(lookup.houseType);
                    SaveLookup(lookup.landlord);
                    SaveLookup(lookup.nationality);
                    
                    
                    
                    SaveLookup(lookup.communication);
                    SaveLookup(lookup.internetAccess);
                    SaveLookup(lookup.preferedContact);
                    SaveLookup(lookup.sexualOrientation);
                    
                    SaveLookup(lookup.ethnicity);
                    SaveLookup(lookup.maritalStatus);
                    SaveLookup(lookup.religion);
                    
                    SaveLookup(lookup.floorType);
                    

                    dbContext.Commit();

                    // Set IsCustomerLookupFetched to true in AppSession
                    AppSession.IsCustomerLookupFetched = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Error = ex.Message;
                    dbContext.Rollback();
                }

            }
            return result;
        }
        #endregion

        #region "Save Customer Titles"
        public void SaveLookup<T>(List<T> list)
        {
            if (list == null)
            {
                throw new Exception(string.Format("{0} lookup is missing", typeof(T).Name));
            }
            else
            {
                dbContext.DeleteAll<T>();
                dbContext.InsertAll(list);
            }
        }
        #endregion

        #region "Get Customer Titles"
        public List<LookupModel> GetCustomerTitles()
        {
            var repo = new Repository<CustomerTitle>(dbContext);
            return repo.GetModelList<LookupModel>().OrderBy(x=> x.Description).ToList();
        }
        #endregion

        #region "Get Prefered Contacts"
        public List<LookupModel> GetPreferedContacts()
        {
            var repo = new Repository<PreferedContact>(dbContext);
            return repo.GetModelList<LookupModel>().OrderBy(x=> x.Description).ToList();
        }
        #endregion

        #region "Get Internet Access"
        public List<LookupModel> GetInternetAccess()
        {
            var repo = new Repository<InternetAccess>(dbContext);
            return repo.GetModelList<LookupModel>().OrderBy(x=> x.Description).ToList();
        }
        #endregion

        #region "Get Marital Statuses"
        public List<LookupModel> GetMaritalStatuses()
        {
            var repo = new Repository<MaritalStatus>(dbContext);
            return repo.GetModelList<LookupModel>().OrderBy(x=> x.Description).ToList();
        }
        #endregion

        #region "Get Ethnicity"
        public List<LookupModel> GetEthnicity()
        {
            var repo = new Repository<Ethnicity>(dbContext);
            return repo.GetModelList<LookupModel>().OrderBy(x=> x.Description).ToList();
        }
        #endregion

        #region "Get Religions"
        public List<LookupModel> GetReligion()
        {
            var repo = new Repository<Religion>(dbContext);
            return repo.GetModelList<LookupModel>().OrderBy(x=> x.Description).ToList();
        }
        #endregion

        #region "Get Sexual Orientation"
        public List<LookupModel> GetSexualOrientation()
        {
            var repo = new Repository<SexualOrientation>(dbContext);
            return repo.GetModelList<LookupModel>().OrderBy(x=> x.Description).ToList();
        }
        #endregion

        #region "Get Nationality"
        public List<LookupModel> GetNationality()
        {
            var repo = new Repository<Nationality>(dbContext);
            return repo.GetModelList<LookupModel>().OrderBy(x=> x.Description).ToList();
        }
        #endregion

        #region "Get YesNo"
        public List<LookupModel> GetYesNo()
        {
            var repo = new Repository<YesNo>(dbContext);
            return repo.GetModelList<LookupModel>();
        }
        #endregion

        #region "Get YesNoUnknown"
        public List<LookupModel> GetYesNoUnknown()
        {
            var repo = new Repository<YesNoUnknown>(dbContext);
            return repo.GetModelList<LookupModel>();
        }
        #endregion

        #region "Get LandlordType"
        public List<LookupModel> GetLandlordType()
        {
            var repo = new Repository<LandlordType>(dbContext);
            return repo.GetModelList<LookupModel>().OrderBy(x=> x.Description).ToList();
        }
        #endregion

        #region "Get House Types"
        public List<LookupModel> GetHouseTypes()
        {
            var repo = new Repository<HouseType>(dbContext);
            return repo.GetModelList<LookupModel>().OrderBy(x=> x.Description).ToList();
        }
        #endregion

        #region "Get Communication"
        public List<LookupModel> GetCommunication()
        {
            var repo = new Repository<Communication>(dbContext);
            return repo.GetModelList<LookupModel>().OrderBy(x=> x.Description).ToList();
        }
        #endregion

        #region "Get Customer Type"
        public List<LookupModel> GetCustomerTypes()
        {
            var repo = new Repository<CustomerType>(dbContext);
            return repo.GetModelList<LookupModel>().OrderBy(x=> x.Description).ToList();
        }
        #endregion

        #region "Get Referral Stages"
        public List<LookupModel> GetReferralStages()
        {
            var repo = new Repository<ReferralStages>(dbContext);
            return repo.GetModelList<LookupModel>().OrderBy(x=> x.Description).ToList();
        }
        #endregion

        #region "Get Referral Help Categories"
        public List<GroupInfoListModel> GetReferralHelpCategory()
        {
            List<ReferralHelpCategory> helpCategories = dbContext.Table<ReferralHelpCategory>().ToList();
            List<ReferralHelpSubCategory> helpSubCategories = dbContext.Table<ReferralHelpSubCategory>().ToList();

            var query = from cat in helpCategories
                        join subcat in helpSubCategories on cat.id equals subcat.subId
                        select new LookupModel()
                        {
                            GroupKey = cat.description,
                            Id = subcat.id,
                            Description = subcat.description
                        };

            // Group category list by Category
            List<LookupModel> categoryList = new List<LookupModel>();
            List<GroupInfoListModel> categoryGroups = new List<GroupInfoListModel>();
            if (query != null)
            {
                categoryList = query.ToList();

                if (categoryList.Count() > 0)
                {
                    var groupQuery = from item in categoryList
                                     group item by item.GroupKey into g
                                     orderby g.Key
                                     select new { GroupName = g.Key, Items = g };

                    foreach (var g in groupQuery)
                    {
                        GroupInfoListModel info = new GroupInfoListModel();
                        info.Key = g.GroupName;
                        foreach (var item in g.Items)
                        {
                            info.Add(item);
                        }
                        categoryGroups.Add(info);
                    }
                }

            }

            return categoryGroups;
        }

        public List<GroupInfoListModel> GetReferralHelpCategory(string selectedItems)
        {
            List<ReferralHelpCategory> helpCategories = dbContext.Table<ReferralHelpCategory>().ToList();
            List<ReferralHelpSubCategory> helpSubCategories = dbContext.Table<ReferralHelpSubCategory>().ToList();

            var query = from cat in helpCategories
                        join subcat in helpSubCategories on cat.id equals subcat.subId
                        select new LookupModel()
                        {
                            GroupKey = cat.description,
                            Id = subcat.id,
                            Description = subcat.description
                        };

            // Group category list by Category
            List<LookupModel> categoryList = new List<LookupModel>();
            List<GroupInfoListModel> categoryGroups = new List<GroupInfoListModel>();
            if (query != null)
            {
                categoryList = query.ToList();
                if (!string.IsNullOrWhiteSpace(selectedItems))
                {
                    int intItem = 0;
                    var intList = selectedItems.Split(',')
                                        .Select(m => { int.TryParse(m, out intItem); return intItem; })
                                        .Where(m => m != 0)
                                        .ToList();
                    foreach (LookupModel item in categoryList)
                    {
                        if (intList.Contains(item.Id))
                        {
                            item.IsSelected = true;
                        }
                    }
                }
                

                if (categoryList.Count() > 0)
                {
                    var groupQuery = from item in categoryList
                                     group item by item.GroupKey into g
                                     orderby g.Key
                                     select new { GroupName = g.Key, Items = g };

                    foreach (var g in groupQuery)
                    {
                        GroupInfoListModel info = new GroupInfoListModel();
                        info.Key = g.GroupName;
                        foreach (var item in g.Items)
                        {
                            info.Add(item);
                        }
                        categoryGroups.Add(info);
                    }
                }

            }

            return categoryGroups;
        }

        #endregion
        #region "Get Viewing Comment Types"
        public List<LookupModel> GetViewingCommentTypes()
        {
            var repo = new Repository<ViewingCommentType>(dbContext);
            return repo.GetModelList<LookupModel>().OrderBy(x => x.Description).ToList();
        }
        #endregion
        #region "Get Termination Reason"
        public List<LookupModel> GetTerminationReason()
        {
            var repo = new Repository<TerminationReason>(dbContext);
            return repo.GetModelList<LookupModel>().OrderBy(x=> x.Description).ToList();
        }
        #endregion

        #region "Get Termination Reason Category"
        public List<LookupModel> GetTerminationReasonCategory()
        {
            var repo = new Repository<TerminationReasonCategory>(dbContext);
            return repo.GetModelList<LookupModel>().OrderBy(x=> x.Description).ToList();
        }
        #endregion

        #region "Get Benefits"
        public List<LookupModel> GetBenefits()
        {
            var repo = new Repository<Benefit>(dbContext);
            return repo.GetModelList<LookupModel>().OrderBy(x=> x.Description).ToList();
        }
        #endregion

        #region "Get Banking Facilities"
        public List<LookupModel> GetBankingFacilities()
        {
            var repo = new Repository<BankingFacility>(dbContext);
            return repo.GetModelList<LookupModel>().OrderBy(x=> x.Description).ToList();
        }
        #endregion

        #region "Get Health Problems"
        public List<LookupModel> GetHealthProblems()
        {
            var repo = new Repository<HealthProblem>(dbContext);
            return repo.GetModelList<LookupModel>().OrderBy(x=> x.Description).ToList();
        }
        #endregion

        #region "Get Support From Agencies"
        public List<LookupModel> GetSupportFromAgencies()
        {
            var repo = new Repository<AgencySupport>(dbContext);
            return repo.GetModelList<LookupModel>().OrderBy(x=> x.Description).ToList();
        }
        #endregion

        #region "Get Aids And Adaption"
        public List<LookupModel> GetAidsAndAdaption()
        {
            var repo = new Repository<AidsAndAdaption>(dbContext);
            return repo.GetModelList<LookupModel>().OrderBy(x=> x.Description).ToList();
        }
        #endregion

        #region "Get Employment Status"
        public List<LookupModel> GetEmploymentStatus()
        {
            var repo = new Repository<EmploymentStatus>(dbContext);
            return repo.GetModelList<LookupModel>().OrderBy(x=> x.Description).ToList();
        }
        #endregion

        #region "Get Eviction Reason"
        public List<LookupModel> GetEvictionReason()
        {

            var reasonList = dbContext.Table<TerminationReason>().ToList();
            var reasonCategoryList = dbContext.Table<TerminationReasonCategory>().ToList();

            var evictedReasonQuery = from rcl in reasonCategoryList
                                     join rl in reasonList on rcl.subId equals rl.id
                                     where rl.description == "Evicted"
                                     select new LookupModel {
                                          Id = rcl.id,
                                          Description = rcl.description
                                     };
            List<LookupModel> list = new List<LookupModel>();
            if (evictedReasonQuery.Count() > 0)
            {
                list = evictedReasonQuery.ToList();
            }

            return list;
        }
        #endregion

        #region "Get Floor"
        public List<LookupModel> GetFloor()
        {
            var repo = new Repository<Floor>(dbContext);
            return repo.GetModelList<LookupModel>().OrderBy(x=> x.Description).ToList();
        }
        #endregion

        #region "Get Relationship Status"
        public List<LookupModel> GetRelationshipStatus()
        {
            var repo = new Repository<RelationStatus>(dbContext);
            return repo.GetModelList<LookupModel>().OrderBy(x=> x.Description).ToList();
        }
        #endregion

        #region "Get Item Statuses"
        public List<LookupModel> GetItemStatuses()
        {
            var repo = new Repository<ItemStatus>(dbContext);
            return repo.GetModelList<LookupModel>().OrderBy(x => x.Description).ToList();
        }
        #endregion

        #region "Get Address Type"
        public List<LookupModel> GetAddressType()
        {
            var repo = new Repository<AddressType>(dbContext);
            return repo.GetModelList<LookupModel>().OrderBy(x=> x.Description).ToList();
        }
        #endregion

        #region "Get YesNoUnknown description"
        public string GetYesNoUnknownDescription(int? id)
        {
            string description = null;
            var yesNoUnknown = dbContext.Table<YesNoUnknown>().ToList();

            if (id != null)
            {
                description = yesNoUnknown.Where(x => x.id == id)
                                   .Select(y => y.description)
                                   .FirstOrDefault();
            }

            return description;
        }
        #endregion

        #region "Get Stage description"
        public string GetStageDescription(int? id)
        {
            string description = null;
            var stages = dbContext.Table<ReferralStages>().ToList();

            if (id != null)
            {
                description = stages.Where(x => x.id == id)
                                   .Select(y => y.description)
                                   .FirstOrDefault();
            }

            return description;
        }
        #endregion

        #region "Get YesNo description"
        public string GetYesNoDescription(int? id)
        {
            string description = null;
            var yesNo = dbContext.Table<YesNo>().ToList();

            if (id != null)
            {
                description = yesNo.Where(x => x.id == id)
                                   .Select(y => y.description)
                                   .FirstOrDefault();
            }

            return description;
        }
        #endregion

        #region "Get Selected Help Subcategories"
        public List<string> GetSelectedHelpSubcategories(string category,string selectedItems)
        {
            List<string> referralSubCategories = new List<string>();

            if (!string.IsNullOrEmpty(selectedItems))
            {
                var referralHelpCategory = dbContext.Table<ReferralHelpCategory>().ToList();
                var referralHelpSubCategory = dbContext.Table<ReferralHelpSubCategory>().ToList();

                int intItem = 0;
                var intList = selectedItems.Split(',')
                                           .Select(m => { int.TryParse(m, out intItem); return intItem; })
                                           .Where(m => m != 0)
                                           .ToList();

                referralSubCategories = (from subCat in referralHelpSubCategory
                                          join cat in referralHelpCategory on subCat.subId equals cat.id
                                          where cat.description == category
                                                && intList.Contains(subCat.id)
                                          select subCat.description).ToList();
            }

            return referralSubCategories;


        }
        #endregion

        #endregion

        #region "Shared Lookups"

        #region "Save Shared lookup"
        public ResultModel<bool> SaveSharedLookup(SharedLookUpResponseSO lookup)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            result.Success = true;
            if (lookup != null)
            {
                try
                {
                    dbContext.BeginTransaction();
                    SaveLookup(lookup.addressType);
                    SaveLookup(lookup.relationshipStatus);
                    SaveLookup(lookup.customerType);
                    SaveLookup(lookup.propertyType);
                    SaveLookup(lookup.supportAgencies);
                    SaveLookup(lookup.adaptions);
                    SaveLookup(lookup.bankingFacility);
                    SaveLookup(lookup.employmentStatus); 
                    SaveLookup(lookup.disability);
                    SaveLookup(lookup.referralsHelpCategories);
                    SaveLookup(lookup.referralsHelpSubCategories);
                    SaveLookup(lookup.referralsStages);
                    SaveLookup(lookup.terminationResaon);
                    SaveLookup(lookup.terminationResaonCategory);
                    SaveLookup(lookup.customerTitles);
                    SaveLookup(lookup.benefits);
                    SaveLookup(lookup.yesNo);
                    SaveLookup(lookup.yesNoUnkown);
                    SaveLookup(lookup.riskVulnerabilityStatus);
                    SaveLookup(lookup.teams);
                    SaveLookup(lookup.employees);
                    SaveLookup(lookup.riskCategory);
                    SaveLookup(lookup.riskSubCategory);
                    SaveLookup(lookup.vulnerabilityCategory);
                    SaveLookup(lookup.vulnerabilitySubCategory);
                    SaveLookup(lookup.letterAction);
                    SaveLookup(lookup.viewingCommentTypes);
                    dbContext.Commit();

                    // Set IsSharedLookupsFetched to true in AppSession
                    AppSession.IsSharedLookupsFetched = true;
                }
                catch (Exception ex)
                {
                    result.Success = false;
                    result.Error = ex.Message;
                    dbContext.Rollback();
                }

            }
            return result;
        }
        #endregion

        #region "Get Item Status Id"
        public int GetItemStatusId(string description)
        {
            int itemStatusId = 0;
            var itemStatusList = dbContext.Table<ItemStatus>().ToList();

            itemStatusId = itemStatusList.Where(x => x.description.Equals(description))
                                   .Select(y => y.id)
                                   .FirstOrDefault();

            return itemStatusId;
        }
        #endregion

        #endregion

        #endregion

    }
}
