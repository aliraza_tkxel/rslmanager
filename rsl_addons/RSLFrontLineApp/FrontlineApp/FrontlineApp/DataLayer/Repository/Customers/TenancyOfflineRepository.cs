﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.DataLayer.Database;
using FrontlineApp.DataLayer.OfflineTenancyEntities;
using SQLite.Net;
using SQLiteNetExtensions.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace FrontlineApp.DataLayer.Repository.Customers
{
    public class TenancyOfflineRepository
    {
        #region "Properties"
        private SQLiteConnection dbCtx;
        #endregion
        #region "Constructor"
        public TenancyOfflineRepository()
        {
            this.dbCtx = DependencyService.Get<FrontlineDatabase>().DBContext;
        }
        #endregion
        #region "Save Tenancy Data"
        public ResultModel<bool> SaveTenancyOfflineData(TenancyRootEnt tenancyObj)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            try
            {
                dbCtx.BeginTransaction();

                if (tenancyObj != null)
                {
                    dbCtx.InsertWithChildren(tenancyObj, recursive: true);
                }

                dbCtx.Commit();
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Error = ex.Message;
                result.Message = ex.Message;
                dbCtx.Rollback();
            }

            return result;
        }
        #endregion

        #region "Delete Tenancy"
        public ResultModel<bool> DeleteTenancyDataFor(int? CustomerId, string PropertyId)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            try
            {
                var tenancyData = dbCtx.GetAllWithChildren<TenancyRootEnt>().ToList();

                var CustomerGeneralInfo = dbCtx.GetAllWithChildren<TenancyCustomerGeneralInfoEnt>().ToList();
                var TenancyEmploymentInfo = dbCtx.GetAllWithChildren<TenancyEmploymentInfoEnt>().ToList();
                var TenancyEmergencyContactInfo = dbCtx.GetAllWithChildren<TenancyEmergencyContactEnt>().ToList();

                if (CustomerId != null)
                {
                    
                    List<TenancyRootEnt> filteredData = tenancyData.Where(o => o.CustomerId == CustomerId).ToList();
                    if (filteredData.Count > 0)
                    {
                        TenancyRootEnt rootObj = filteredData.ElementAt(0);
                        if (rootObj.JointTenant != null)
                        {
                            var filteredCGI = CustomerGeneralInfo.Where(o => o.Id == rootObj.JointTenant.TenancyJTCustomerGeneralInfoId).ToList();
                            if (filteredCGI.Count > 0)
                            {
                                dbCtx.DeleteAll(filteredCGI, recursive: true);
                            }
                            var filteredEmpInfo = TenancyEmploymentInfo.Where(o => o.Id == rootObj.JointTenant.TenancyJTEmploymentInfoId).ToList();
                            if (filteredEmpInfo.Count > 0)
                            {
                                dbCtx.DeleteAll(filteredEmpInfo, recursive: true);
                            }

                            var filteredEmergCont = TenancyEmergencyContactInfo.Where(o => o.Id == rootObj.JointTenant.TenancyJTEmergencyContactId).ToList();
                            if (filteredEmergCont.Count > 0)
                            {
                                dbCtx.DeleteAll(filteredEmergCont, recursive: true);
                            }
                        }
                        
                        dbCtx.DeleteAll(filteredData, recursive: true);
                        result.Success = true;
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "No Results found";
                    }
                }
                else
                {
                    List<TenancyRootEnt> filteredData = tenancyData.Where(o => o.PropertyId == PropertyId).ToList();
                    if (filteredData.Count > 0)
                    {
                        TenancyRootEnt rootObj = filteredData.ElementAt(0);
                        if (rootObj.JointTenant != null)
                        {
                            var filteredCGI = CustomerGeneralInfo.Where(o => o.Id == rootObj.JointTenant.TenancyJTCustomerGeneralInfoId).ToList();
                            if (filteredCGI.Count > 0)
                            {
                                dbCtx.DeleteAll(filteredCGI, recursive: true);
                            }
                            var filteredEmpInfo = TenancyEmploymentInfo.Where(o => o.Id == rootObj.JointTenant.TenancyJTEmploymentInfoId).ToList();
                            if (filteredEmpInfo.Count > 0)
                            {
                                dbCtx.DeleteAll(filteredEmpInfo, recursive: true);
                            }

                            var filteredEmergCont = TenancyEmergencyContactInfo.Where(o => o.Id == rootObj.JointTenant.TenancyJTEmergencyContactId).ToList();
                            if (filteredEmergCont.Count > 0)
                            {
                                dbCtx.DeleteAll(filteredEmergCont, recursive: true);
                            }
                        }
                        
                        dbCtx.DeleteAll(filteredData, recursive: true);
                        result.Success = true;
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "No Results found";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Error = ex.Message;
                result.Message = ex.Message;
            }

            return result;
        }
        #endregion

        #region "Fetch Tenancy"

        public ResultModel<TenancyRootEnt> FetchTenancyDataFor(int? CustomerId, string PropertyId)
        {
            ResultModel<TenancyRootEnt> result = new ResultModel<TenancyRootEnt>();
            try
            {
                var tenancyData = dbCtx.GetAllWithChildren<TenancyRootEnt>().ToList();

                var CustomerGeneralInfo = dbCtx.GetAllWithChildren<TenancyCustomerGeneralInfoEnt>().ToList();
                var TenancyEmploymentInfo = dbCtx.GetAllWithChildren<TenancyEmploymentInfoEnt>().ToList();
                var TenancyEmergencyContactInfo = dbCtx.GetAllWithChildren<TenancyEmergencyContactEnt>().ToList();

                if (CustomerId != null)
                {
                    List<TenancyRootEnt> filteredData = tenancyData.Where(o => o.CustomerId == CustomerId).ToList();
                    if (filteredData.Count>0)
                    {
                        TenancyRootEnt rootObj = filteredData.ElementAt(0);
                        if (rootObj.JointTenant != null)
                        {
                            var filteredCGI = CustomerGeneralInfo.Where(o => o.Id == rootObj.JointTenant.TenancyJTCustomerGeneralInfoId).ToList();
                            if (filteredCGI.Count > 0)
                            {
                                rootObj.JointTenant.CustomerGeneralInfoJT = filteredCGI.ElementAt(0);
                            }

                            var filteredEmpInfo = TenancyEmploymentInfo.Where(o => o.Id == rootObj.JointTenant.TenancyJTEmploymentInfoId).ToList();
                            if (filteredEmpInfo.Count > 0)
                            {
                                rootObj.JointTenant.EmploymentInfoJT = filteredEmpInfo.ElementAt(0);
                            }

                            var filteredEmergCont = TenancyEmergencyContactInfo.Where(o => o.Id == rootObj.JointTenant.TenancyJTEmergencyContactId).ToList();
                            if (filteredEmergCont.Count > 0)
                            {
                                rootObj.JointTenant.EmergencyContactJT = filteredEmergCont.ElementAt(0);
                            }
                        }
                        result.Value = rootObj;
                        result.Success = true;
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "No Results found";
                    }
                }
                else
                {
                    List<TenancyRootEnt> filteredData = tenancyData.Where(o => o.PropertyId == PropertyId).ToList();
                    if (filteredData.Count > 0)
                    {
                        TenancyRootEnt rootObj = filteredData.ElementAt(0);
                        if (rootObj.JointTenant != null)
                        {
                            var filteredCGI = CustomerGeneralInfo.Where(o => o.Id == rootObj.JointTenant.TenancyJTCustomerGeneralInfoId).ToList();
                            if (filteredCGI.Count > 0)
                            {
                                rootObj.JointTenant.CustomerGeneralInfoJT = filteredCGI.ElementAt(0);
                            }

                            var filteredEmpInfo = TenancyEmploymentInfo.Where(o => o.Id == rootObj.JointTenant.TenancyJTEmploymentInfoId).ToList();
                            if (filteredEmpInfo.Count > 0)
                            {
                                rootObj.JointTenant.EmploymentInfoJT = filteredEmpInfo.ElementAt(0);
                            }

                            var filteredEmergCont = TenancyEmergencyContactInfo.Where(o => o.Id == rootObj.JointTenant.TenancyJTEmergencyContactId).ToList();
                            if (filteredEmergCont.Count > 0)
                            {
                                rootObj.JointTenant.EmergencyContactJT = filteredEmergCont.ElementAt(0);
                            }
                        }
                        result.Value = rootObj;
                        result.Success = true;
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = "No Results found";
                    }
                }
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Error = ex.Message;
                result.Message = ex.Message;
            }

            return result;
        }
        #endregion
    }
}
