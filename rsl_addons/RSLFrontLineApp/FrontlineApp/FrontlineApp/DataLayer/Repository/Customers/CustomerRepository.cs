﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.DataLayer.Database;
using FrontlineApp.DataLayer.Entities;
using SQLite.Net;
using System;
using System.Collections.Generic;
using Xamarin.Forms;
using System.Linq;
using SQLiteNetExtensions.Extensions;
using AutoMapper;
using FrontlineApp.Utility.Helper.Extensions;
using FrontlineApp.Utility.Constants;
using System.Text;
using FrontlineApp.Utility;
using FrontlineApp.DataLayer.OfflineTenancyEntities;

namespace FrontlineApp.DataLayer.Repository
{
    public class CustomerRepository
    {
        #region "Properties"
        private SQLiteConnection dbCtx;
        #endregion

        #region "Constructor"
        public CustomerRepository()
        {
            this.dbCtx = DependencyService.Get<FrontlineDatabase>().DBContext;            
        }
        #endregion

        #region "Save Customer Listing"
        public ResultModel<bool> SaveCustomerListing(List<Customer> customerList)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            try
            {
                dbCtx.BeginTransaction();

                if (customerList != null)
                {
                    // Delete All Customers including their child relationships
                    var allCustomers = dbCtx.GetAllWithChildren<Customer>(null,recursive:true);
                    if (allCustomers.Count > 0)
                    {
                        dbCtx.DeleteAll(allCustomers, recursive: true);
                    }
                    
                    // Insert updated customer list 
                    dbCtx.InsertAllWithChildren(customerList, recursive: true);
                }

                dbCtx.Commit();
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Error = ex.Message;
                result.Message = ex.Message;
                dbCtx.Rollback();
            }

            return result;
        }
        #endregion 

        #region "Save Customer Info From Server"
        public ResultModel<int?> SaveCustomerInfoFromServer(int? id, Customer customer)
        {
            ResultModel<int?> result = new ResultModel<int?>();
            int? newId = null;
            try
            {
                dbCtx.BeginTransaction();

                if (customer != null)
                {
                    // Delete All Customers including their child relationships
                    var customerOldRecord = dbCtx.GetWithChildren<Customer>(id, recursive: true);
                    if (customerOldRecord !=null)
                    {
                        dbCtx.Delete(customerOldRecord, recursive: true);
                    }

                    // Insert updated customer list 
                    dbCtx.InsertWithChildren(customer, recursive: true);
                }

                dbCtx.Commit();

                if (customer.id == null)
                {
                    Repository<Customer> custRepo = new Repository<Customer>();
                    newId = custRepo.GetMaxId();
                }
                else
                {
                    newId = customer.id;
                }

                result.Success = true;
                result.Value = newId;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Error = ex.Message;
                result.Message = ex.Message;
                dbCtx.Rollback();
            }

            return result;
        }
        #endregion

        #region "Save Customer Viewing"
        public ResultModel<bool> SaveCustomerViewing(CustomerModel customerModel,List<CustomerViewing> viewingList)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            try
            {
                dbCtx.BeginTransaction();

                var customerEntity = dbCtx.GetWithChildren<Customer>(customerModel.Id, recursive: true);
                if (customerEntity != null)
                {
                    customerEntity.CustomerViewings = viewingList;
                    dbCtx.InsertOrReplaceWithChildren(customerEntity, true);
                    dbCtx.Commit();
                    result.Success = true;
                }
                else
                {
                    result.Success = false;
                    result.Message = MessageConstants.CustomerRecordNotFound;
                }

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Error = ex.Message;
                result.Message = ex.Message;
                dbCtx.Rollback();
            }

            return result;
        }
        #endregion

        #region "Save Customer Risks"
        public ResultModel<bool> SaveCustomerRisks(CustomerModel customerModel, List<CustomerRisk> risks)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            try
            {
                dbCtx.BeginTransaction();

                var customerEntity = dbCtx.GetWithChildren<Customer>(customerModel.Id, recursive: true);
                if (customerEntity != null)
                {
                    if (customerEntity.tenancy != null)
                    {
                        if (customerEntity.tenancy.Risks != null)
                        {
                            var oldRisks = customerEntity.tenancy.Risks;
                            dbCtx.DeleteAll(oldRisks,false);
                        }

                        customerEntity.tenancy.Risks = risks;
                        dbCtx.InsertOrReplaceWithChildren(customerEntity, true);
                        dbCtx.Commit();
                        result.Success = true;
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = MessageConstants.TenancyRecordNotFound;
                    }
                }
                else
                {
                    result.Success = false;
                    result.Message = MessageConstants.CustomerRecordNotFound;
                }

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Error = ex.Message;
                result.Message = ex.Message;
                dbCtx.Rollback();
            }

            return result;
        }
        #endregion

        #region "Save Customer Vulnerabilities"
        public ResultModel<bool> SaveCustomerVulnerabilities(CustomerModel customerModel, List<CustomerVulnerability> vulnerabilities)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            try
            {
                dbCtx.BeginTransaction();

                var customerEntity = dbCtx.GetWithChildren<Customer>(customerModel.Id, recursive: true);
                if (customerEntity != null)
                {
                    if (customerEntity.tenancy != null)
                    {
                        if (customerEntity.tenancy.Vulnerabilities != null)
                        {
                            var oldRisks = customerEntity.tenancy.Risks;
                            dbCtx.DeleteAll(oldRisks, false);
                        }

                        customerEntity.tenancy.Vulnerabilities = vulnerabilities;
                        dbCtx.InsertOrReplaceWithChildren(customerEntity, true);
                        dbCtx.Commit();
                        result.Success = true;
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = MessageConstants.TenancyRecordNotFound;
                    }
                }
                else
                {
                    result.Success = false;
                    result.Message = MessageConstants.CustomerRecordNotFound;
                }

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Error = ex.Message;
                result.Message = ex.Message;
                dbCtx.Rollback();
            }

            return result;
        }
        #endregion

        #region "Get Grouped Customer Viewing List"
        public ResultModel<List<GroupInfoListModel>> GetGroupedCustomerViewingList(CustomerModel customer)
        {

            ResultModel<List<GroupInfoListModel>> result = new ResultModel<List<GroupInfoListModel>>();
            var customerEntity = dbCtx.GetWithChildren<Customer>(customer.Id, recursive: true);
            var viewingStatusList = dbCtx.Table<ViewingStatus>().ToList();
            if (customerEntity != null)
            {
                List<GroupInfoListModel> viewingGroups = new List<GroupInfoListModel>();
                var customerViewingList = customerEntity.CustomerViewings;
                if (customerViewingList.Count() > 0)
                {

                    var viewingQuery = from cv in customerViewingList
                                       where cv.viewingDateTime != null
                                       join vStatus in viewingStatusList on cv.viewingStatusId equals vStatus.id
                                       orderby cv.viewingDateTime descending
                                       select new CustomerViewingModel()
                                       {
                                           Key = cv.viewingDateTime.Value.Year < DateTimeOffset.Now.Year
                                                 ? "Past Viewings"
                                                 : string.Empty,
                                           ViewingId = cv.viewingId,
                                           ViewingStatusDescription = vStatus.description,
                                           ViewingTime = GeneralHelper.GetTimeStringFromServerDate(cv.viewingTime),
                                           ViewingStatusId = cv.viewingStatusId,
                                           ViewingDateTime = cv.viewingDateTime,
                                           HousingOfficerId = cv.housingOfficerId,
                                           HousingOfficerName = cv.housingOfficerName,
                                           PropertyAddress = cv.propertyAddress,
                                           PropertyId = cv.propertyId
                                       };

                    List<CustomerViewingModel> custViewingList = new List<CustomerViewingModel>();
                    
                    if (viewingQuery != null)
                    {
                        custViewingList = viewingQuery.ToList();

                        if (custViewingList.Count() > 0)
                        {
                            var groupQuery = from item in custViewingList
                                             group item by item.Key into g
                                             orderby g.Key
                                             select new { GroupName = g.Key, Items = g };

                            foreach (var g in groupQuery)
                            {
                                GroupInfoListModel info = new GroupInfoListModel();
                                info.Key = g.GroupName;
                                foreach (var item in g.Items)
                                {
                                    info.Add(item);
                                }
                                viewingGroups.Add(info);
                            }

                            result.Value = viewingGroups;                            
                        }
                        
                    }
                }
                result.Success = true;
                result.Value = viewingGroups;
            }
            else
            {
                result.Success = false;
                result.Message = MessageConstants.CustomerRecordNotFound;
            }

            return result;

        }
        #endregion

        #region "Get Grouped Customer Listing"
        public List<GroupInfoListModel> GetGroupedCustomerList()
        {
            var customers = dbCtx.GetAllWithChildren<Customer>().ToList();
            var customerTypes = dbCtx.Table<CustomerType>().ToList();
            var currentAddressTypeId = dbCtx.Table<AddressType>().Where(x => x.description == "Current").Select(y => y.id).FirstOrDefault();

            var query = from cust in customers
                        join cType in customerTypes on cust.customerTypeId equals cType.id
                        select new CustomerModel()
                        {
                            Id = cust.id,
                            CustomerId = cust.customerId,
                            GroupKey = string.IsNullOrEmpty(cust.firstName) ? string.Empty : cust.firstName[0].ToString(),
                            FirstName = string.IsNullOrEmpty(cust.firstName)?string.Empty : cust.firstName,
                            MiddleName = string.IsNullOrEmpty(cust.middleName) ? string.Empty : cust.middleName,
                            LastName = cust.lastName,
                            IsJointTenant = cust.tenancyCount == null 
                                            ? false 
                                            : cust.tenancyCount > 1 ? true : false ,
                            Gender = cust.gender != null ? cust.gender.ToPascalCase() : null,
                            CustomerType = cType.description,
                            Telephone = cust.CustomerAddresses != null
                                        ? cust.CustomerAddresses.Where(ad => ad.addressTypeId == currentAddressTypeId).Select(t => t.telephone).FirstOrDefault()
                                        : null,
                            Dob = cust.dob == null ? (DateTimeOffset?)null : (DateTimeOffset?)DateTimeOffset.Parse(cust.dob.ToString()).LocalDateTime,
                            CustomerImage = cust.customerImage != null 
                                            ? (new CustomerImageModel {
                                                        Id = cust.customerImage.id,
                                                        BaseAddress = cust.customerImage.isSynced == false
                                                                      ? PathConstants.LocalFolderPath
                                                                      : PathConstants.CustomerProfileThumb,
                                                        ImageName = cust.customerImage.imageName,
                                                        IsSynced = cust.customerImage.isSynced
                                                    }) 
                                            : null 
                        };

            // Group customer list by FirstName
            List<CustomerModel> custList = new List<CustomerModel>();
            List<GroupInfoListModel> customerGroups = new List<GroupInfoListModel>();
            if (query !=null)
            {
                custList = query.ToList();

                if (custList.Count() > 0)
                {
                    var groupQuery = from item in custList
                                     group item by item.GroupKey into g
                                     orderby g.Key
                                     select new { GroupName = g.Key, Items = g };

                    foreach (var g in groupQuery)
                    {
                        GroupInfoListModel info = new GroupInfoListModel();
                        info.Key = g.GroupName;
                        foreach (var item in g.Items)
                        {
                            info.Add(item);
                        }
                        customerGroups.Add(info);
                    }
                }
            }

            return customerGroups;

        }
        #endregion

        #region "Save Customer Info"
        public ResultModel<CustomerModel> SaveCustomerInfo(CustomerModel customerInfo)
        {
            ResultModel<CustomerModel> result = new ResultModel<CustomerModel>();
            dbCtx.BeginTransaction();

            try
            {

                Customer customer = new Customer();
                if (customerInfo.Id != null && customerInfo.Id > 0)
                {
                    var existCustomer = dbCtx.GetWithChildren<Customer>(customerInfo.Id);
                    if (existCustomer != null)
                    {
                        customer = existCustomer;
                    }
                }             

                customer.customerId = customerInfo.CustomerId;
                customer.customerNumber = customerInfo.CustomerNumber;
                customer.customerTypeId = customerInfo.CustomerTypeId;
                customer.tenancyCount = customerInfo.TenancyCount;
                customer.titleId = customerInfo.TitleId;
                customer.firstName = customerInfo.FirstName;
                customer.middleName = customerInfo.MiddleName;
                customer.lastName = customerInfo.LastName;
                customer.preferedContactId = customerInfo.PreferedContactId;
                customer.internetAccess = customerInfo.InternetAccess;
                customer.dob = customerInfo.Dob;
                customer.gender = customerInfo.Gender;
                customer.maritalStatusId = customerInfo.MaritalStatusId;
                customer.ethnicityId = customerInfo.EthnicityId;
                customer.religionId = customerInfo.ReligionId;
                customer.sexualOrientationId = customerInfo.SexualOrientationId;
                customer.niNumber = customerInfo.NiNumber;
                customer.nationalityId = customerInfo.NationalityId;
                customer.firstLanguage = customerInfo.FirstLanguage;
                customer.isSubjectToImmigration = customerInfo.IsSubjectToImmigration;
                customer.isPermanentUKResident = customerInfo.IsPermanentUKResident;
                customer.communication = customerInfo.Communication;
                customer.appVersion = customerInfo.AppVersion;
                customer.isSynced = customerInfo.IsSynced;
                customer.createdBy = customerInfo.CreatedBy;
                customer.createdOnApp = customerInfo.CreatedOnApp;
                customer.lastModifiedOnApp = customerInfo.LastModifiedOnApp;

                // Setting Relationships
                if (customerInfo.CustomerAddresses != null)
                    customer.CustomerAddresses = Mapper.Map<List<CustomerAddressModel>, List<CustomerAddress>>(customerInfo.CustomerAddresses);

                if (customerInfo.CustomerImage != null)
                    customer.customerImage = Mapper.Map<CustomerImageModel, CustomerImage>(customerInfo.CustomerImage);

                if (customerInfo.CurrentHome != null)
                    customer.currentHome = Mapper.Map<CurrentHomeModel, CurrentHome>(customerInfo.CurrentHome);

                dbCtx.InsertOrReplaceWithChildren(customer,recursive:true);                       
                dbCtx.Commit();

                if (customerInfo.Id == null)
                {
                    Repository<Customer> custRepo = new Repository<Customer>();                    
                    customerInfo.Id = custRepo.GetMaxId();
                }

                var updatedCustomer = dbCtx.GetWithChildren<Customer>(customerInfo.Id);

                result.Success = true;
                result.Value = Mapper.Map<Customer, CustomerModel>(updatedCustomer);

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Error = ex.Message;
                result.Message = ex.Message;
                dbCtx.Rollback();
            }

            return result;
        }
        #endregion

        #region "Get Customer Detail"
        public Customer GetCustomerDetail(int? id)
        {
            var customer = dbCtx.GetWithChildren<Customer>(id , recursive:true);
            return customer;
        }
        #endregion

        #region "Delete Customer"
        public ResultModel<bool> DeleteCustomer(int? id)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            dbCtx.BeginTransaction();

            try
            {
                var customer = dbCtx.GetWithChildren<Customer>(id, recursive: true);
                dbCtx.Delete(customer, true);             
                dbCtx.Commit();

                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Error = ex.Message;
                result.Message = ex.Message;
                dbCtx.Rollback();
            }

            return result;
        }
        #endregion

        #region "Get Un Synced Customers"
        public List<Customer> GetUnSyncedCustomers()
        {
            Repository<Customer> customerRepo = new Repository<Customer>();            
            return customerRepo.Get<Customer>(x => x.isSynced == false);             
        }
        #endregion

        #region "Get Customer General Info"
        public ResultModel<CustomerModel> GetCustomerGeneralInfo(int? id)
        {
            ResultModel<CustomerModel> result = new ResultModel<CustomerModel>();

            var customers = dbCtx.Table<Customer>().ToList();
            var customerTypes = dbCtx.Table<CustomerType>().ToList();
            var internetAccess = dbCtx.Table<InternetAccess>().ToList();
            var communication = dbCtx.Table<Communication>().ToList();
            var maritalStatus = dbCtx.Table<MaritalStatus>().ToList();
            var ethnicity = dbCtx.Table<Ethnicity>().ToList();
            var religion = dbCtx.Table<Religion>().ToList();
            var addressTypeList = dbCtx.Table<AddressType>().ToList();
            var sexualOrientation = dbCtx.Table<SexualOrientation>().ToList();
            var nationality = dbCtx.Table<Nationality>().ToList();
            var preferedContact = dbCtx.Table<PreferedContact>().ToList();
            var economicStatusList = dbCtx.Table<EmploymentStatus>().ToList();

            var customerModel = (from c in customers
                        join ct in customerTypes on c.customerTypeId equals ct.id
                        join ms in maritalStatus on c.maritalStatusId equals ms.id into custMarital
                        from cm in custMarital.DefaultIfEmpty()
                        join e in ethnicity on c.ethnicityId equals e.id into custEthnicity
                        from ce in custEthnicity.DefaultIfEmpty()
                        join r in religion on c.religionId equals r.id into custReligion
                        from cr in custReligion.DefaultIfEmpty()
                        join s in sexualOrientation on c.sexualOrientationId equals s.id into custSexualOrientation
                        from cs in custSexualOrientation.DefaultIfEmpty()
                        join n in nationality on c.nationalityId equals n.id into custNationality
                        from cn in custNationality.DefaultIfEmpty()
                        join p in preferedContact on c.preferedContactId equals p.id into custPreferedContact
                        from cp in custPreferedContact.DefaultIfEmpty()
                        where c.id == id
                        select new CustomerModel
                        {
                            Id = c.id,
                            CustomerId = c.customerId,
                            CustomerNumber = c.customerNumber,
                            CustomerType = ct == null ? null: ct.description,                            
                            CustomerTypeId = ct.id,
                            TenancyCount = c.tenancyCount,
                            TitleId = c.titleId,
                            FirstName = c.firstName,
                            MiddleName = c.middleName,
                            LastName = c.lastName,
                            PreferredContact = cp == null ? null : cp.description,
                            PreferedContactId = c.preferedContactId,
                            InternetAccess = c.internetAccess,
                            Dob = c.dob == null ? null : (DateTimeOffset?)DateTimeOffset.Parse(c.dob.ToString()).LocalDateTime,
                            Gender = c.gender != null ? c.gender.ToPascalCase() : null,
                            MaritalStatus = cm == null ? null : cm.description,
                            MaritalStatusId = c.maritalStatusId,
                            Ethnicity = ce == null ? null : ce.description,
                            EthnicityId = c.ethnicityId,
                            Religion = cr == null ? null : cr.description,
                            ReligionId = c.religionId,
                            SexualOrientation = cs == null ? null : cs.description,
                            SexualOrientationId = c.sexualOrientationId,
                            NiNumber = c.niNumber,
                            NationalityId = c.nationalityId,
                            Nationality = cn == null ? null : cn.description,
                            FirstLanguage = c.firstLanguage,
                            IsSubjectToImmigration = c.isSubjectToImmigration,
                            SubjectToImmigration = c.isSubjectToImmigration == null ? "N/A" : (c.isSubjectToImmigration == 1 ? "Yes" : "No"),
                            IsPermanentUKResident = c.isPermanentUKResident,
                            PermanentUKResident = c.isPermanentUKResident == null ? "N/A" : (c.isPermanentUKResident == 1 ? "Yes" : "No"),
                            ResidencyStatus = c.isPermanentUKResident == null ? "N/A" : (c.isPermanentUKResident == 1 ? "Yes" : "No"),
                            Communication = c.communication,
                            AppVersion = c.appVersion,
                            CreatedBy = c.createdBy,
                            CreatedOnApp = c.createdOnApp,
                            CreatedOnServer = c.createdOnServer,
                            LastModifiedOnApp = c.lastModifiedOnApp,
                            LastModifiedOnServer = c.lastModifiedOnServer,
                            IsSynced = c.isSynced,
                            Children = c.children,
                            Adults = c.adults
                        }
                        ).FirstOrDefault();

            if (customerModel != null)
            {
                var customer = dbCtx.GetWithChildren<Customer>(id, recursive: true);

                #region  "Setup Internet Access"

                if (!string.IsNullOrEmpty(customer.internetAccess))
                {
                    int internetAccessIds = 0;
                    var internetAccessIdList = customer.internetAccess.Split(',')
                                        .Select(m => { int.TryParse(m, out internetAccessIds); return internetAccessIds; })
                                        .Where(m => m != 0)
                                        .ToList();

                    var internetAccessQuery = from c in internetAccess
                                              where internetAccessIdList.Contains(c.id)
                                              select c.description;

                    if (internetAccessQuery.Count() > 0)
                    {
                        customerModel.InternetAccessDescription = string.Join(", ", internetAccessQuery.ToList());
                    }

                }

                #endregion

                #region "Setup Communication"
                if (!string.IsNullOrEmpty(customer.communication))
                {
                    int communicationIds = 0;
                    var communicationIdList = customer.communication.Split(',')
                                        .Select(m => { int.TryParse(m, out communicationIds); return communicationIds; })
                                        .Where(m => m != 0)
                                        .ToList();

                    var communicationQuery = from c in communication
                                             where communicationIdList.Contains(c.id)
                                             select c.description;

                    if (communicationQuery.Count() > 0)
                    {
                        customerModel.CommunicationList = communicationQuery.ToList();
                    }
                }
                #endregion
                
                #region "Setup Customer Addresses"
                if (customer.CustomerAddresses != null)
                {
                    customerModel.CustomerAddress = new CustomerAddressModel();
                    var customerAddressesModel = Mapper.Map<List<CustomerAddress>, List<CustomerAddressModel>>(customer.CustomerAddresses);
                    customerModel.CustomerAddresses = customerAddressesModel;

                    var adrsTypeId = addressTypeList.Where(x => x.description == "Current")
                                               .Select(y => y.id)
                                               .FirstOrDefault();
                    var adrs = customerAddressesModel.Where(x => x.AddressTypeId == adrsTypeId).FirstOrDefault();
                    if (adrs != null)
                    {
                        //var address = Mapper.Map<CustomerAddress, CustomerAddressModel>(adrs);
                        string[] adrsParams = new[] {
                                                adrs.HouseNumber,
                                                adrs.Address1,
                                                adrs.City,
                                                adrs.County,
                                                adrs.PostCode
                                              };

                        customerModel.CustomerAddress = adrs;
                        customerModel.CustomerAddress.CompleteAddress = string.Join(", ", adrsParams.Where(e => !string.IsNullOrWhiteSpace(e)));
                        if (string.IsNullOrWhiteSpace(customerModel.CustomerAddress.CompleteAddress))
                        {
                            customerModel.CustomerAddress.CompleteAddress = null;
                        }
                    }
                }
                #endregion

                #region "Setup Current Home"
                if (customer.currentHome != null)
                {
                    var customerCurrentHome = Mapper.Map<CurrentHome, CurrentHomeModel>(customer.currentHome);

                    if (customerCurrentHome.LandlordTypeId != null)
                    {
                        customerCurrentHome.LandlordType = dbCtx.Table<LandlordType>()
                                                                .Where(x => x.id == customerCurrentHome.LandlordTypeId)
                                                                .Select(y => y.description)
                                                                .FirstOrDefault();
                    }

                    if (customerCurrentHome.HouseTypeId != null)
                    {
                        customerCurrentHome.HouseType = dbCtx.Table<HouseType>()
                                                             .Where(x => x.id == customerCurrentHome.HouseTypeId)
                                                             .Select(y => y.description)
                                                             .FirstOrDefault();
                    }

                    if (customerCurrentHome.Floor != null)
                    {
                        customerCurrentHome.FloorDescription = dbCtx.Table<Floor>()
                                                                    .Where(x => x.id == customerCurrentHome.Floor)
                                                                    .Select(y => y.description)
                                                                    .FirstOrDefault();
                    }

                    if (customerCurrentHome.LivingWithFamilyId != null)
                    {
                        customerCurrentHome.LivingWithFamily = dbCtx.Table<YesNo>()
                                                                    .Where(x => x.id == customerCurrentHome.LivingWithFamilyId)
                                                                    .Select(y => y.description)
                                                                    .FirstOrDefault();
                    }

                    customerModel.CurrentHome = customerCurrentHome;
                }
                #endregion

                #region "Setup Customer Image"
                if (customer.customerImage != null)
                {
                    var customerImage = Mapper.Map<CustomerImage, CustomerImageModel>(customer.customerImage);
                    customerImage.BaseAddress = PathConstants.CustomerProfileThumb;
                    customerImage.IsSynced = true;
                    customerModel.CustomerImage = customerImage;
                }
                #endregion

                #region "Setup Employment Info"
                if (customer.employmentInfo != null)
                {
                    var customerEmploymentInfo = Mapper.Map<EmploymentInfo, EmploymentInfoModel>(customer.employmentInfo);
                    var matchList = economicStatusList.Where(x => x.id == customerEmploymentInfo.EmploymentStatusId).ToList().FirstOrDefault() ;
                    customerModel.EmploymentInfo = customerEmploymentInfo;
                    if (matchList != null)
                    {
                        customerModel.EmploymentInfo.EmploymentStatusDescription = matchList.description;
                    }
                    
                    
                }
                #endregion

                #region "Setup General Info"

                if (customer.customerGeneralInfo != null)
                {
                    var customerGeneralInfo = Mapper.Map<CustomerGeneralInfo, CustomerGeneralInfoModel>(customer.customerGeneralInfo);
                    customerModel.CustomerGeneralInfo = customerGeneralInfo;
                }

                #endregion

                #region "Setup Tenancy Info"

                if (customer.tenancy != null)
                {
                    var tenancy = Mapper.Map<Tenancy, TenancyModel>(customer.tenancy);
                    customerModel.Tenancy = tenancy;
                }
                
                #endregion
                #region "Set up Tenancy Address"
                if (customerModel.CustomerType.Equals("Tenant"))
                {
                    customerModel.IsTenant = true;
                    var tenancyAddress = Mapper.Map<TenancyAddress, TenancyAddressModel>(customer.tenancy.tenancyAddress);
                    if (tenancyAddress != null)
                    {
                        string[] adrsParams = new[] {
                                                tenancyAddress.HouseNumber,
                                                tenancyAddress.Address1,
                                                tenancyAddress.Address2,
                                                tenancyAddress.Address3,
                                                tenancyAddress.City,
                                                tenancyAddress.County,
                                                tenancyAddress.PostCode
                                              };
                        customerModel.Tenancy.TenancyAddress = tenancyAddress;
                        customerModel.Tenancy.TenancyAddress.CompleteAddress = string.Join(", ", adrsParams.Where(e => !string.IsNullOrWhiteSpace(e)));
                    }
                }
                else
                {
                    customerModel.IsTenant = false;
                }
                #endregion

                result.Success = true;
                result.Value = customerModel;

            }
            else
            {
                result.Success = false;
                result.Message = MessageConstants.CustomerRecordNotFound;
            }

            return result;

        }
        #endregion

        #region "Save Customer Referral from server"
        /// <summary>
        /// Get Tenancy Id based on customerId
        /// Get Tenancy entity including children
        /// Attach customer referral as its child
        /// Insert or Replace With children 
        /// </summary>
        /// <param name="customerModel"></param>
        /// <param name="referral"></param>
        /// <returns></returns>
        public ResultModel<bool> SaveCustomerReferralFromServer(CustomerModel customerModel , CustomerReferral referral)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            try
            {
                dbCtx.BeginTransaction();

                var customerEntity = dbCtx.GetWithChildren<Customer>(customerModel.Id, recursive: true);
                if (customerEntity != null)
                {
                    if (customerEntity.tenancy != null)
                    {
                        if (customerEntity.tenancy.customerReferral != null)
                        {
                            var oldRefId = customerEntity.tenancy.customerReferral.id;
                            dbCtx.Delete<CustomerReferral>(oldRefId);
                        }

                        customerEntity.tenancy.customerReferral = referral;
                        dbCtx.InsertOrReplaceWithChildren(customerEntity, true);
                        dbCtx.Commit();
                        result.Success = true;
                    }
                    else
                    {
                        result.Success = false;
                        result.Message = MessageConstants.TenancyRecordNotFound;
                    }
                }
                else
                {
                    result.Success = false;
                    result.Message = MessageConstants.CustomerRecordNotFound;
                }

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Error = ex.Message;
                result.Message = ex.Message;
                dbCtx.Rollback();
            }

            return result;
        }
        #endregion

        #region "Save Tenancy Termination Info From Server"
        /// <summary>
        /// Save Tenancy Termination Info From Server
        /// </summary>
        /// <param name="customerModel"></param>
        /// <param name="terminationInfo"></param>
        /// <returns></returns>
        public ResultModel<bool> SaveTenancyTerminationInfoFromServer(CustomerModel customerModel, TenancyTermination terminationInfo)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            try
            {
                dbCtx.BeginTransaction();

                var customerEntity = dbCtx.GetWithChildren<Customer>(customerModel.Id, recursive: true);
                if (customerEntity != null)
                {
                    dbCtx.Delete(customerEntity, recursive: true);
                    dbCtx.Commit();
                    result.Success = true;
                }
                else
                {
                    result.Success = false;
                    result.Message = MessageConstants.CustomerRecordNotFound;
                }

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Error = ex.Message;
                result.Message = ex.Message;
                dbCtx.Rollback();
            }

            return result;
        }
        #endregion

        #region "Get Customer Referral"
        public ResultModel<ReferralModel> GetCustomerReferral(CustomerModel customerModel)
        {
            ResultModel<ReferralModel> result = new ResultModel<ReferralModel>();
            var customer = dbCtx.GetWithChildren<Customer>(customerModel.Id, recursive: true);

            if (customer.tenancy != null)
            {        
                var referralModel = customer.tenancy.customerReferral == null ? null : Mapper.Map<CustomerReferral, ReferralModel>(customer.tenancy.customerReferral);

                if (referralModel != null)
                {
                    LookupRepository lookupRepo = new LookupRepository();

                    // Section A items
                    referralModel.Stage = lookupRepo.GetStageDescription(referralModel.StageId);
                    referralModel.SafetyIssue = lookupRepo.GetYesNoUnknownDescription(referralModel.SafetyIssueId);
                    referralModel.Conviction = lookupRepo.GetYesNoUnknownDescription(referralModel.IsConviction);
                    referralModel.SubstanceMisuse = lookupRepo.GetYesNoUnknownDescription(referralModel.IsSubstanceMisuse);
                    referralModel.SelfHarm = lookupRepo.GetYesNoUnknownDescription(referralModel.IsSelfHarm);
                    referralModel.TenantAware = lookupRepo.GetYesNoDescription(referralModel.IsTenantAware);
                    referralModel.AidAndAdaptions = GetAidsAndAdaption(referralModel);

                    // Section B items
                    referralModel.MoneyIssues = lookupRepo.GetSelectedHelpSubcategories(ApplicationConstants.MoneyIssueDescription, referralModel.CustomerHelpSubCategories);
                    referralModel.PersonalProblems = lookupRepo.GetSelectedHelpSubcategories(ApplicationConstants.PersonalProblemsDescription, referralModel.CustomerHelpSubCategories);
                    referralModel.WorkOrTrainings = lookupRepo.GetSelectedHelpSubcategories(ApplicationConstants.WorkOrTrainingDescription, referralModel.CustomerHelpSubCategories);
                    referralModel.PracticalProblems = lookupRepo.GetSelectedHelpSubcategories(ApplicationConstants.PracticalProblemsDescription, referralModel.CustomerHelpSubCategories);

                }

                result.Value = referralModel;
                result.Success = true;
            }
            else {
                result.Success = false;
                result.Message = MessageConstants.TenancyReferralNotFound;
            }

            return result;

        }
        #endregion

        #region "Get Aids And Adaption"
        private string GetAidsAndAdaption(ReferralModel referral)
        {
            string aidsAdaption = null;
            List<string> list = new List<string>();

            if (referral.IsTypeArrears == true)
            {
                list.Add(ApplicationConstants.AdaptionTypeArrears);
            }

            if (referral.IsTypeDamage == true)
            {
                list.Add(ApplicationConstants.AdaptionTypeDamage);
            }

            if (referral.IsTypeAsb == true)
            {
                list.Add(ApplicationConstants.AdaptionTypeASB);
            }

            if (list.Count() > 0)
            {
                aidsAdaption = string.Join(". ", list.Where(e => !string.IsNullOrWhiteSpace(e)));
            }

            return aidsAdaption;

        }
        #endregion

        #region "Get Customer Risks List"
        public ResultModel<List<CustomerRiskModel>> GetCustomerRisksList(CustomerModel customer)
        {

            ResultModel<List<CustomerRiskModel>> result = new ResultModel<List<CustomerRiskModel>>();
            var customerEntity = dbCtx.GetWithChildren<Customer>(customer.Id, recursive: true);
            if (customerEntity != null)
            {
                List<CustomerRiskModel> customerRisks = new List<CustomerRiskModel>();
                
                var risksList = customerEntity.tenancy.Risks;
                if (risksList.Count() > 0)
                {
                    var teams = dbCtx.Table<Team>().ToList();
                    var employees = dbCtx.Table<Employee>().ToList();
                    var letterActions = dbCtx.Table<LetterAction>().ToList();
                    var statuses = dbCtx.Table<ItemStatus>().ToList();
                    var categories = dbCtx.Table<RiskCategory>().ToList();
                    var subcategories = dbCtx.Table<RiskSubCategory>().ToList();

                    var riskSubCategories = (from subcategory in subcategories
                                             join category in categories on subcategory.subId equals category.id
                                             select new
                                             {
                                                 Id = subcategory.id,
                                                 Description = string.Format("{0} - {1}", category.description, subcategory.description)
                                             }).ToList();

                    var riskQuery =    from risk in risksList
                                       join team in teams on risk.teamId equals team.id into riskTeam
                                       from rt in riskTeam.DefaultIfEmpty()
                                       join emp in employees on risk.assignedTo equals emp.id
                                       join action in letterActions on risk.actionId equals action.id into riskAction
                                       from ra in riskAction.DefaultIfEmpty()                                       
                                       join status in statuses on risk.statusId equals status.id into riskStatus
                                       from rs in riskStatus.DefaultIfEmpty()
                                       orderby risk.startDate descending
                                       select new CustomerRiskModel
                                       {
                                           RiskHistoryId = risk.riskHistoryId,
                                           CustomerId = risk.customerId,
                                           CustomerName = risk.customerName,
                                           TenancyId = risk.tenancyId,
                                           JournalId = risk.journalId,
                                           Title = risk.title,
                                           TeamId = risk.teamId,
                                           Team = (rt == null) ? null : rt.description,
                                           AssignedTo = risk.assignedTo,
                                           Assignee = emp.description,
                                           StartDate = risk.startDate,
                                           CustomerRiskSubcategories = risk.customerRiskSubcategories,
                                           Notes = risk.notes,
                                           ReviewDate = risk.reviewDate,
                                           IsCustomerAware = risk.isCustomerAware,
                                           IsTenantOnline = risk.isTenantOnline,
                                           ActionId = risk.actionId,
                                           Action = (ra == null) ? null : ra.description,
                                           LastActionUserId = risk.lastActionUserId,
                                           LastActionDate = risk.lastActionDate,
                                           StatusId = risk.statusId,
                                           Status = (rs == null) ? null : rs.description
                                       };

                    if (riskQuery != null)
                    {
                        customerRisks = riskQuery.ToList();

                        #region "Customer Risk Category-Subcategories"

                        foreach (var risk in customerRisks)
                        {
                            if (!string.IsNullOrEmpty(risk.CustomerRiskSubcategories))
                            {
                                int subCatId = 0;
                                var subCategoryIdList = risk.CustomerRiskSubcategories.Split(',')
                                                    .Select(m => { int.TryParse(m, out subCatId); return subCatId; })
                                                    .Where(m => m != 0)
                                                    .ToList();

                                var subCategoryQuery = from r in riskSubCategories
                                                       where subCategoryIdList.Contains(r.Id)
                                                       select r.Description;

                                if (subCategoryQuery.Count() > 0)
                                {
                                    risk.RiskCategorySubCategoryList = subCategoryQuery.ToList();
                                }

                            }
                        }

                        #endregion

                    }
                }
                result.Success = true;
                result.Value = customerRisks;
            }
            else
            {
                result.Success = false;
                result.Message = MessageConstants.CustomerRecordNotFound;
            }

            return result;

        }
        #endregion

        #region "Get Customer Vulnerability List"
        public ResultModel<List<CustomerVulnerabilityModel>> GetCustomerVulnerabilityList(CustomerModel customer)
        {

            ResultModel<List<CustomerVulnerabilityModel>> result = new ResultModel<List<CustomerVulnerabilityModel>>();
            var customerEntity = dbCtx.GetWithChildren<Customer>(customer.Id, recursive: true);
            if (customerEntity != null)
            {
                List<CustomerVulnerabilityModel> customerVulnerabilities = new List<CustomerVulnerabilityModel>();

                var vulnerabilityList = customerEntity.tenancy.Vulnerabilities;
                if (vulnerabilityList.Count() > 0)
                {
                    var teams = dbCtx.Table<Team>().ToList();
                    var employees = dbCtx.Table<Employee>().ToList();
                    var letterActions = dbCtx.Table<LetterAction>().ToList();
                    var statuses = dbCtx.Table<ItemStatus>().ToList();
                    var categories = dbCtx.Table<VulnerabilityCategory>().ToList();
                    var subcategories = dbCtx.Table<VulnerabilitySubCategory>().ToList();

                    var vulnerabilitySubCategories = (from subcategory in subcategories
                                                     join category in categories on subcategory.subId equals category.id
                                                     select new
                                                     {
                                                         Id = subcategory.id,
                                                         Description = string.Format("{0} - {1}", category.description, subcategory.description)
                                                     }).ToList();

                    var vulnerabilityQuery = from vulnerability in vulnerabilityList
                                    join team in teams on vulnerability.teamId equals team.id into vulTeam
                                    from rt in vulTeam.DefaultIfEmpty()
                                    join emp in employees on vulnerability.assignedTo equals emp.id
                                    join action in letterActions on vulnerability.actionId equals action.id into vulAction
                                    from ra in vulAction.DefaultIfEmpty()
                                    join status in statuses on vulnerability.statusId equals status.id into vulStatus
                                    from rs in vulStatus.DefaultIfEmpty()
                                    orderby vulnerability.startDate descending
                                    select new CustomerVulnerabilityModel
                                    {
                                        VulnerabilityHistoryId = vulnerability.vulnerabilityHistoryId,
                                        CustomerId = vulnerability.customerId,
                                        CustomerName = vulnerability.customerName,
                                        TenancyId = vulnerability.tenancyId,
                                        JournalId = vulnerability.journalId,
                                        Title = vulnerability.title,
                                        TeamId = vulnerability.teamId,
                                        Team = (rt == null) ? null : rt.description,
                                        AssignedTo = vulnerability.assignedTo,
                                        Assignee = emp.description,
                                        StartDate = vulnerability.startDate,
                                        CustomerVulnerabilitySubcategories = vulnerability.customerVulnerabilitySubcategories,
                                        Notes = vulnerability.notes,
                                        ReviewDate = vulnerability.reviewDate,
                                        IsCustomerAware = vulnerability.isCustomerAware,
                                        IsTenantOnline = vulnerability.isTenantOnline,
                                        ActionId = vulnerability.actionId,
                                        Action = (ra == null) ? null : ra.description,
                                        LastActionUserId = vulnerability.lastActionUserId,
                                        LastActionDate = vulnerability.lastActionDate,
                                        StatusId = vulnerability.statusId,
                                        Status = (rs == null) ? null : rs.description
                                    };

                    if (vulnerabilityQuery != null)
                    {
                        customerVulnerabilities = vulnerabilityQuery.ToList();


                        #region "Customer Vulnerability Category-Subcategories"

                        foreach (var vulnerability in customerVulnerabilities)
                        {
                            if (!string.IsNullOrEmpty(vulnerability.CustomerVulnerabilitySubcategories))
                            {
                                int subCatId = 0;
                                var subCategoryIdList = vulnerability.CustomerVulnerabilitySubcategories.Split(',')
                                                    .Select(m => { int.TryParse(m, out subCatId); return subCatId; })
                                                    .Where(m => m != 0)
                                                    .ToList();

                                var subCategoryQuery = from r in vulnerabilitySubCategories
                                                       where subCategoryIdList.Contains(r.Id)
                                                       select r.Description;

                                if (subCategoryQuery.Count() > 0)
                                {
                                    vulnerability.VulnerabilityCategorySubCategoryList = subCategoryQuery.ToList();
                                }

                            }
                        }

                        #endregion

                    }
                }
                result.Success = true;
                result.Value = customerVulnerabilities;
            }
            else
            {
                result.Success = false;
                result.Message = MessageConstants.CustomerRecordNotFound;
            }

            return result;

        }
        #endregion
    }
}
