﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.DataLayer.Database;
using FrontlineApp.DataLayer.Entities;
using FrontlineApp.ServiceLayer.ServiceObjects;
using SQLite.Net;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Linq;
using FrontlineApp.Utility;
using SQLiteNetExtensions.Extensions;
using FrontlineApp.Utility.Helper.AppSession;
using FrontlineApp.Utility.Constants;

namespace FrontlineApp.DataLayer.Repository
{
    public class ViewingsRepository
    {

        #region "Properties"
        private SQLiteConnection dbCtx;
        #endregion

        #region "Constructor"
        public ViewingsRepository()
        {
            this.dbCtx = DependencyService.Get<FrontlineDatabase>().DBContext;
        }
        #endregion

        #region "Save Staff Members"
        public ResultModel<bool> SaveStaffMember(List<StaffMember> list)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            try
            {
                dbCtx.BeginTransaction();
                if (list != null)
                {
                    dbCtx.DeleteAll<StaffMember>();
                    dbCtx.InsertAll(list);
                }
                dbCtx.Commit();
                result.Success = true;
            }
            catch (Exception ex)
            {
                dbCtx.Rollback();
                result.Success = false;
                result.Error = ex.Message;
            }

            return result;
        }
        #endregion

        #region "Delete"

        public ResultModel<bool> DeleteAllViewings()
        {
            ResultModel<bool> result = new ResultModel<bool>();
            try
            {
                dbCtx.BeginTransaction();
                dbCtx.DeleteAll<ViewingSummary>();
                dbCtx.DeleteAll<ViewingComment>();
                dbCtx.Commit();
                result.Success = true;
            }
            catch (Exception ex)
            {
                dbCtx.Rollback();
                result.Success = false;
                result.Error = ex.Message;
            }

            return result;
        }

        public ResultModel<bool> DeleteViewingWithId(int ViewingId)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            try
            {
                List<ViewingSummary> dataSource = GetViewingEntities();
                var obj = dataSource.Where(p => p.viewingId == ViewingId).ToList();
                if (obj.Count > 0)
                {
                    ViewingSummary objEnt = obj.FirstOrDefault();
                    if (objEnt.commentsList != null)
                    {
                        if (objEnt.commentsList.Count > 0)
                        {
                            dbCtx.DeleteAll(objEnt.commentsList);
                        }
                        
                    }
                    dbCtx.Delete(objEnt,recursive:true);
                }
                result.Success = true;
            }
            catch (Exception ex)
            {
                dbCtx.Rollback();
                result.Success = false;
                result.Error = ex.Message;
            }

            return result;
        }

        #endregion
        #region "Save Customers"
        public ResultModel<bool> SaveCustomers(CustomerLookupResponseSO response)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            try
            {
                dbCtx.BeginTransaction();
                if (response != null)
                {
                    var customerList = dbCtx.GetAllWithChildren<CustomerLookup>(recursive: true);
                    if (customerList.Count > 0)
                    {
                        dbCtx.DeleteAll(customerList, true);
                    }

                    //TODO: This is a temporary fix to handle duplicate records in response. API side should fix this on their end to make sure 
                    //Multiple results do not return in the response. 

                    var distinctList = response.customerList.GroupBy(elem => elem.customerId).Select(group => group.First()).ToList();
                    if (distinctList.Count > 0)
                    {
                        dbCtx.InsertAllWithChildren(distinctList, recursive: true);
                    }
                    
                }
                dbCtx.Commit();
                result.Success = true;
            }
            catch (Exception ex)
            {
                dbCtx.Rollback();
                result.Success = false;
                result.Error = ex.Message;
            }

            return result;
        }
        #endregion

        #region "Get Staff Member List"
        public List<StaffMemberModel> GetStaffMembersList()
        {
            var staffMembers = dbCtx.Table<StaffMember>().ToList();

            var query = from ofc in staffMembers
                        select new StaffMemberModel()
                        {
                            EmployeeId = ofc.employeeId,
                            FirstName = ofc.firstName,
                            LastName = ofc.lastName,
                            Gender = ofc.gender,
                            ImagePath = ofc.imagePath,
                            Email = ofc.email,
                            Initials = ofc.firstName[0].ToString() + ofc.lastName[0].ToString()
                            
                        };

            List<StaffMemberModel> staffMemberList = new List<StaffMemberModel>();

            if (query != null)
            {
                staffMemberList = query.ToList();
            }

            return staffMemberList;

        }
        #endregion

        #region "Get Customer Lookup List"
        public List<GroupInfoListModel> GetCustomerLookUpList()
        {
            var customersList = dbCtx.GetAllWithChildren<CustomerLookup>().ToList();
            var customerTypes = dbCtx.Table<CustomerType>().ToList();
            var currentAddressTypeId = dbCtx.Table<AddressType>().Where(x => x.description == "Current").Select(y => y.id).FirstOrDefault();
            var query = from cust in customersList
                        join cType in customerTypes on cust.customerTypeId equals cType.id
                        select new CustomerLookUpListModel()
                        {
                            FirstName = cust.firstName,
                            LastName =  cust.lastName,
                            CustomerId = cust.customerId,
                            Dob = cust.dob,
                            CustomerGender =  cust.gender,
                            CustomerType = cType.description,
                            CustomerPhoneNumber = cust.customerAddress != null
                                        ? cust.customerAddress.Where(ad => ad.addressTypeId == currentAddressTypeId).Select(t => t.telephone).FirstOrDefault()
                                        : "N/A",
                            CustomerImage = cust.customerImage != null
                                            ? (new CustomerLookupImageModel
                                            {
                                                Id = cust.customerImage.id,
                                                BaseAddress = PathConstants.CustomerProfileThumb,
                                                ImageName = cust.customerImage.imageName
                                            })
                                            : null

                        };
            List<CustomerLookUpListModel> result = new List<CustomerLookUpListModel>();
            List<GroupInfoListModel> customerGroups = new List<GroupInfoListModel>();
            if (query != null)
            {
                try
                {
                    result = query.ToList();
                    if (result.Count() > 0)
                    {
                        var groupQuery = from item in result
                                         group item by item.Initials[0].ToString() into g
                                         orderby g.Key
                                         select new { GroupName = g.Key, Items = g };

                        foreach (var g in groupQuery)
                        {
                            GroupInfoListModel info = new GroupInfoListModel();
                            info.Key = g.GroupName;
                            foreach (var item in g.Items)
                            {
                                info.Add(item);
                            }
                            customerGroups.Add(info);
                        }
                    }
                }
                catch (Exception e)
                {
                    string error = e.Message;
                    
                }
                
            }
            return customerGroups;
        }

        public List<CustomerModel> GetCustomerLookUpListAsCustomerModel()
        {
            var customersList = dbCtx.GetAllWithChildren<CustomerLookup>().ToList();
            var customerTypes = dbCtx.Table<CustomerType>().ToList();
            var currentAddressTypeId = dbCtx.Table<AddressType>().Where(x => x.description == "Current").Select(y => y.id).FirstOrDefault();
            var query = from cust in customersList
                        join cType in customerTypes on cust.customerTypeId equals cType.id
                        select new CustomerModel()
                        {
                            FirstName = cust.firstName,
                            LastName = cust.lastName,
                            CustomerId = cust.customerId,
                            Dob = cust.dob,
                            Gender = cust.gender,
                            CustomerType = cType.description,
                            Telephone = cust.customerAddress != null
                                        ? cust.customerAddress.Where(ad => ad.addressTypeId == currentAddressTypeId).Select(t => t.telephone).FirstOrDefault()
                                        : "N/A",
                            CustomerImage = cust.customerImage != null
                                            ? (new CustomerImageModel
                                            {
                                                Id = cust.customerImage.id,
                                                BaseAddress = PathConstants.CustomerProfileThumb,
                                                ImageName = cust.customerImage.imageName
                                            })
                                            : null

                        };
            List<CustomerModel> result = new List<CustomerModel>();
            if (query != null)
            {
                try
                {
                    result = query.ToList();
                }
                catch (Exception e)
                {
                    string error = e.Message;

                }

            }
            return result;
        }

        #endregion

        #region "Update Viewing"

        public ResultModel<bool> UpdateViewingComments(List<ViewingComment> comments, ViewingSummaryModel summaryModel)
        {
            ResultModel<bool> resulto = new ResultModel<bool>();
            var allComments = dbCtx.Table<ViewingComment>().ToList();
            List<ViewingSummary> dataSource = GetViewingEntities();
            ViewingSummary summary = dataSource.Where(v => v.viewingId == summaryModel.ViewingId).ToList().FirstOrDefault();
            try
            {
                dbCtx.BeginTransaction();
                if (allComments.Count > 0)
                {
                    var filteredObj = allComments.Where(p => p.ViewingId == summary.viewingId).ToList();
                    if (filteredObj.Count > 0)
                    {
                        
                        dbCtx.DeleteAll(filteredObj);
                        
                    }
                    
                }
                summary.commentsList = comments;
                dbCtx.UpdateWithChildren(summary);
                dbCtx.Commit();
                resulto.Success = true;
            }
           
            
            
            catch (Exception ex)
            {
                dbCtx.Rollback();
                resulto.Success = false;
                resulto.Error = ex.Message;
            }

            return resulto;
        }

        public ResultModel<bool> UpdateViewingSummary(ViewingSummary result)
        {
            ResultModel<bool> resulto = new ResultModel<bool>();
            try
            {
                dbCtx.BeginTransaction();
                List<ViewingSummary> dataSource = GetViewingEntities();
                if (dataSource.Count > 0)
                {
                    var filteredObj = dataSource.Where(p => p.viewingId == result.viewingId).ToList();
                    if (filteredObj.Count > 0)
                    {
                        ViewingSummary oldObj = filteredObj.FirstOrDefault();
                        oldObj.id = 0;
                        oldObj.propertyId = result.propertyId;
                        oldObj.rent = result.rent;
                        oldObj.noOfBedRooms = result.noOfBedRooms;
                        oldObj.propertyType = result.propertyType;
                        oldObj.customerType = result.customerType;
                        oldObj.mobileNo = result.mobileNo;
                        oldObj.dob = result.dob;
                        oldObj.createdOnApp = result.createdOnApp;
                        oldObj.viewingNotes = result.viewingNotes;
                        oldObj.viewingCreatedBy = result.viewingCreatedBy;
                        oldObj.propertyImage = result.propertyImage;
                        oldObj.outlookIdentifier = result.outlookIdentifier;
                        oldObj.assignedById = result.assignedById;
                        oldObj.assignedByName = result.assignedByName;
                        oldObj.commentsList = result.commentsList;
                        DeleteViewingWithId(oldObj.viewingId);
                        dbCtx.InsertWithChildren(oldObj);
                    }
                }
                dbCtx.Commit();
                resulto.Success = true;
            }
            catch (Exception ex)
            {
                dbCtx.Rollback();
                resulto.Success = false;
                resulto.Error = ex.Message;
            }

            return resulto;
            
        }
        #endregion

        #region "Get Viewings Detail"
        public List<ViewingCommentModel> GetCommentsForViewing(ViewingSummaryModel model)
        {
            var comments = dbCtx.Table<ViewingComment>().ToList();
            var query = from vc in comments
                        where vc.ViewingId == model.ViewingId
                        select new ViewingCommentModel()
                        {
                            CommentId = vc.CommentId,
                            CommentText = vc.CommentText,
                            ViewingId = vc.ViewingId,
                            CommentedDate = vc.CommentedDate,
                            FormattedDate = string.Format("{0} - {1}", vc.CommentedDate.Value.ToString("MMMM dd,yyyy"), vc.CommentedDate.Value.ToString("HH:mm")),
                            CommentOwnerName = vc.CommentOwnerName,
                            CommentTypeId = vc.CommentTypeId,
                            CommentedBy = vc.CommentedBy

                        };
            List<ViewingCommentModel> modelList = new List<ViewingCommentModel>();
            if (query != null)
            {
                modelList = query.ToList();
            }
            return modelList;

        }
        public ViewingSummaryModel GetViewingModel(ViewingSummaryModel detailedObj)
        {
            List<ViewingSummaryModel> model = new List<ViewingSummaryModel>();
            model =  GetViewingSummaryForId(detailedObj.ViewingId);
            if (model.Count > 0)
            {
                detailedObj = model.ElementAt(0);
            }
            /*var query = model.Where(obj => obj.Id == viewingId).ToList();
            ViewingSummaryModel detailedObj = new ViewingSummaryModel();
            if (query.Count > 0)
            {
                detailedObj = query.ElementAt(0);
            }*/
            return detailedObj;
        }
        #endregion

        #region "Get Viewings List"

        

        public  List<ViewingSummary> GetViewingEntities()
        {
            var viewingsList =  dbCtx.GetAllWithChildren<ViewingSummary>().ToList();
            
            return viewingsList;
        }

        public  List<ViewingSummaryModel> GetViewingSummaryList()
        {
            var viewingsList =  dbCtx.Table<ViewingSummary>().ToList();
            var viewingStatusList =  dbCtx.Table<ViewingStatus>().ToList();
            var query = from ofc in viewingsList
                        join vStatus in viewingStatusList on ofc.viewingStatusId equals vStatus.id
                        select new ViewingSummaryModel()
                        {
                            Id = ofc.id,
                            ViewingId = ofc.viewingId,
                            ViewingDate = ofc.viewingDate,
                            FormattedDate = GeneralHelper.GetDateStringFromServerDate(ofc.viewingDate),
                            FormattedTime = GeneralHelper.GetTimeStringFromServerDate(ofc.viewingTime),
                            CompleteDateForDetails = GeneralHelper.GetCompleteDateStringFromServerDate(ofc.viewingDate),
                            FormattedDateTime = String.Format("{0} ({1})", GeneralHelper.GetDateStringFromServerDate(ofc.viewingDate), GeneralHelper.GetTimeStringFromServerDate(ofc.viewingTime)),
                            CustomerName = ofc.customerName,
                            CustomerGender = ofc.customerGender,
                            HousingOfficerGender = ofc.housingOfficerGender,
                            ViewingStatusId = ofc.viewingStatusId,
                            HousingOfficerId = ofc.housingOfficerId,
                            AssignedTo =  ofc.housingOfficerName,
                            CreatedById = ofc.createdById,
                            CreatorName = ofc.viewingCreatedBy,
                            Notes = "",//ofc.viewingNotes - To avoid confusion
                            AssignedByName = ofc.assignedByName,
                            AssignedById = ofc.assignedById,
                            ViewingStatusDescription = vStatus.description,
                            PropertyAddress = ofc.propertyAddress,
                            ActualDate = DateTime.Parse(ofc.viewingDate),
                            GroupKey = GeneralHelper.GetViewingGroupKeyFromDate(ofc.viewingDate),
                            FullCreationDateTime = String.Format("{0} ({1})", GeneralHelper.GetDateStringFromServerDate(ofc.createdOnApp), GeneralHelper.GetFormattedTimeFromTimeString(ofc.createdOnApp)),
                            FormattedCreationDate = GeneralHelper.GetCompleteCreationDateStringFromServerDate(ofc.createdOnApp),
                            FormattedCreationTime = GeneralHelper.GetFormattedTimeFromTimeString(ofc.createdOnApp),
                            PropertyId = ofc.propertyId,
                            PropertyRent = String.Format("£{0:0.00}/month", ofc.rent),
                            NoOfBedRooms = ofc.noOfBedRooms,
                            PropertyType = ofc.propertyType,
                            CustomerType = ofc.customerType,
                            MobileNumber = string.IsNullOrWhiteSpace(ofc.mobileNo) ? "NA" : ofc.mobileNo,
                            CustomerId = ofc.customerId,
                            CreationDate = ofc.createdOnApp,
                            CustomerDateOfBirth = string.IsNullOrEmpty(ofc.dob) ? "NA" : string.Format("{0: MMM dd, yyyy}", DateTime.Parse(ofc.dob)),
                            PropertyThumbnail = (string.IsNullOrWhiteSpace(ofc.propertyImage)) ? "http://i.imgur.com/LTWYX7t.png" : UriTemplateConstants.BaseURL.Replace("/RSLFrontLineApi", "") + "/PropertyImages/" + ofc.propertyId + "/Images/" + ofc.propertyImage
                        };
            List<ViewingSummaryModel> modelList = new List<ViewingSummaryModel>();
            if (query != null)
            {
                modelList = query.ToList();
            }
            return modelList;
        }

        public List<ViewingSummaryModel> GetViewingSummaryForId(int viewingId)
        {
            var viewingsList = dbCtx.Table<ViewingSummary>().ToList();
            var viewingStatusList = dbCtx.Table<ViewingStatus>().ToList();
            var viewingComments = dbCtx.Table<ViewingComment>().ToList();
            var viewingCommentsTypes = dbCtx.Table<ViewingCommentType>().ToList();
            int genericCommentId = 0;
            if (viewingCommentsTypes.Count > 0)
            {
                genericCommentId = viewingCommentsTypes.Where(x => x.description == ApplicationConstants.ViewingCommentTypeGeneral).ToList().FirstOrDefault().id;
            }
            


            var query = from ofc in viewingsList
                        where ofc.viewingId == viewingId
                        join vStatus in viewingStatusList on ofc.viewingStatusId equals vStatus.id
                        select new ViewingSummaryModel()
                        {
                            Id = ofc.id,
                            ViewingId = ofc.viewingId,
                            ViewingDate = ofc.viewingDate,
                            FormattedDate = GeneralHelper.GetDateStringFromServerDate(ofc.viewingDate),
                            FormattedTime = GeneralHelper.GetTimeStringFromServerDate(ofc.viewingTime),
                            CompleteDateForDetails = GeneralHelper.GetCompleteDateStringFromServerDate(ofc.viewingDate),
                            FormattedDateTime = String.Format("{0} ({1})", GeneralHelper.GetDateStringFromServerDate(ofc.viewingDate), GeneralHelper.GetTimeStringFromServerDate(ofc.viewingTime)),
                            CustomerName = ofc.customerName,
                            CustomerGender = ofc.customerGender,
                            HousingOfficerGender = ofc.housingOfficerGender,
                            ViewingStatusId = ofc.viewingStatusId,
                            HousingOfficerId = ofc.housingOfficerId,
                            HousingOfficerEmail = ofc.housingOfficerEmail,
                            AssignedTo =  ofc.housingOfficerName,
                            CreatedById = ofc.createdById,
                            AssignedById = ofc.assignedById,
                            AssignedByName = ofc.assignedByName,
                            CreatorName = (string.IsNullOrWhiteSpace(ofc.viewingCreatedBy))?"NA":ofc.viewingCreatedBy,
                            Notes = "",//ofc.viewingNotes - To avoid confusion
                            ViewingStatusDescription = vStatus.description,
                            PropertyAddress = ofc.propertyAddress,
                            ActualDate = DateTime.Parse(ofc.viewingDate),
                            GroupKey = GeneralHelper.GetViewingGroupKeyFromDate(ofc.viewingDate),
                            FullCreationDateTime = String.Format("{0} ({1})", GeneralHelper.GetDateStringFromServerDate(ofc.createdOnApp), GeneralHelper.GetFormattedTimeFromTimeString(ofc.createdOnApp)),
                            FormattedCreationDate = GeneralHelper.GetCompleteCreationDateStringFromServerDate(ofc.createdOnApp),
                            FormattedCreationTime = GeneralHelper.GetFormattedTimeFromTimeString(ofc.createdOnApp),
                            PropertyId = ofc.propertyId,
                            PropertyRent = String.Format("£{0:0.00}/month", ofc.rent),
                            NoOfBedRooms = ofc.noOfBedRooms,
                            PropertyType = ofc.propertyType,
                            CustomerType = ofc.customerType,
                            MobileNumber = string.IsNullOrWhiteSpace(ofc.mobileNo) ? "NA" : ofc.mobileNo,
                            CustomerId = ofc.customerId,
                            OutlookIdentifier = ofc.outlookIdentifier,
                            CreationDate = ofc.createdOnApp,
                            CustomerDateOfBirth = string.IsNullOrEmpty(ofc.dob) ? "NA" : string.Format("{0: MMM dd, yyyy}", DateTime.Parse(ofc.dob)),
                            PropertyThumbnail = (string.IsNullOrWhiteSpace(ofc.propertyImage)) ? "http://i.imgur.com/LTWYX7t.png" : UriTemplateConstants.BaseURL.Replace("/RSLFrontLineApi", "") + "/PropertyImages/" + ofc.propertyId + "/Images/" + ofc.propertyImage
                        };
            List<ViewingSummaryModel> modelList = new List<ViewingSummaryModel>();
            if (query != null)
            {
                modelList = query.ToList();
            }

            if (modelList.Count > 0)
            {
                foreach(ViewingSummaryModel vm in modelList)
                {
                    List<ViewingComment> filteredComments = viewingComments.Where(x => x.ViewingId == vm.ViewingId).ToList();
                    if (filteredComments.Count > 0)
                    {
                        filteredComments = filteredComments.Where(x => x.CommentTypeId == genericCommentId).ToList();
                        if (filteredComments.Count > 0)
                        {
                            var Comment = filteredComments.OrderByDescending(x => x.CommentedDate).ToList().FirstOrDefault() ;
                            vm.Notes = Comment.CommentText;
                        }
                        
                    }
                }
            }

            return modelList;
        }
        

        #endregion
    }
}
