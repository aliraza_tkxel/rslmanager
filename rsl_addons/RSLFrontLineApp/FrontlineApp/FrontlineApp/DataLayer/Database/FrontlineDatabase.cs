﻿using FrontlineApp.DataLayer.Entities;
using FrontlineApp.DataLayer.OfflineTenancyEntities;
using SQLite.Net;
using System.Threading.Tasks;

namespace FrontlineApp.DataLayer.Database
{
    public abstract class FrontlineDatabase
    {
        private static SQLiteConnection _dbContext = null;
        public SQLiteConnection DBContext
        {
            get
            {
                if (_dbContext == null)
                {
                    _dbContext = InitDBConnection();
                }
                return _dbContext;
            }
        }

        protected abstract SQLiteConnection InitDBConnection();

        public void DBInitializer()
        {
            SQLiteConnection tmp = this.DBContext;
            int result = 0;
            if (_dbContext != null)
            {
                result = _dbContext.CreateTable<AddressType>();
                result = _dbContext.CreateTable<AgencySupport>();
                result = _dbContext.CreateTable<AidsAndAdaption>();
                result = _dbContext.CreateTable<AlertRisk>();
                result = _dbContext.CreateTable<AlertVulnerability>();
                result = _dbContext.CreateTable<BankingFacility>();
                result = _dbContext.CreateTable<Benefit>();
                result = _dbContext.CreateTable<Communication>();
                result = _dbContext.CreateTable<CurrentHome>();
                result = _dbContext.CreateTable<Customer>();
                result = _dbContext.CreateTable<CustomerAddress>();
                result = _dbContext.CreateTable<CustomerAddressLookUp>();
                result = _dbContext.CreateTable<CustomerGeneralInfo>();
                result = _dbContext.CreateTable<CustomerImage>();
                result = _dbContext.CreateTable<CustomerLookUpImage>();
                result = _dbContext.CreateTable<CustomerLookup>();
                result = _dbContext.CreateTable<CustomerReferral>();
                result = _dbContext.CreateTable<CustomerRisk>();
                result = _dbContext.CreateTable<CustomerTitle>();
                result = _dbContext.CreateTable<CustomerType>();
                result = _dbContext.CreateTable<CustomerViewing>();
                result = _dbContext.CreateTable<CustomerVulnerability>();
                result = _dbContext.CreateTable<Employee>();
                result = _dbContext.CreateTable<EmploymentInfo>();
                result = _dbContext.CreateTable<EmploymentStatus>();
                result = _dbContext.CreateTable<Ethnicity>();
                result = _dbContext.CreateTable<Floor>();
                result = _dbContext.CreateTable<HealthProblem>();
                result = _dbContext.CreateTable<HouseType>();
                result = _dbContext.CreateTable<HousingOfficer>();
                result = _dbContext.CreateTable<InternetAccess>();
                result = _dbContext.CreateTable<ItemStatus>();
                result = _dbContext.CreateTable<JointTenant>();
                result = _dbContext.CreateTable<LandlordType>();
                result = _dbContext.CreateTable<LetterAction>();
                result = _dbContext.CreateTable<MaritalStatus>();
                result = _dbContext.CreateTable<Nationality>();
                result = _dbContext.CreateTable<Occupant>();
                result = _dbContext.CreateTable<PreferedContact>();
                result = _dbContext.CreateTable<Property>();
                result = _dbContext.CreateTable<PropertyDetails>();
                result = _dbContext.CreateTable<PropertyFinancial>();
                result = _dbContext.CreateTable<PropertyScheme>();
                result = _dbContext.CreateTable<PropertyType>();
                result = _dbContext.CreateTable<PropertyViewing>();
                result = _dbContext.CreateTable<ReferralHelpCategory>();
                result = _dbContext.CreateTable<ReferralHelpSubCategory>();
                result = _dbContext.CreateTable<ReferralStages>();
                result = _dbContext.CreateTable<RelationStatus>();
                result = _dbContext.CreateTable<Religion>();
                result = _dbContext.CreateTable<RiskCategory>();
                result = _dbContext.CreateTable<RiskSubCategory>();
                result = _dbContext.CreateTable<SexualOrientation>();
                result = _dbContext.CreateTable<StaffMember>();
                result = _dbContext.CreateTable<Team>();
                result = _dbContext.CreateTable<Tenancy>();
                result = _dbContext.CreateTable<TenancyAddress>();
                result = _dbContext.CreateTable<TenancyDisclosure>();
                result = _dbContext.CreateTable<TenancyTermination>();
                result = _dbContext.CreateTable<TenantOnline>();
                result = _dbContext.CreateTable<TerminationReason>();
                result = _dbContext.CreateTable<TerminationReasonCategory>();
                result = _dbContext.CreateTable<ViewingComment>();
                result = _dbContext.CreateTable<ViewingCommentType>();
                result = _dbContext.CreateTable<ViewingStatus>();
                result = _dbContext.CreateTable<ViewingSummary>();
                result = _dbContext.CreateTable<VulnerabilityCategory>();
                result = _dbContext.CreateTable<VulnerabilitySubCategory>();
                result = _dbContext.CreateTable<YesNo>();
                result = _dbContext.CreateTable<YesNoUnknown>();


                //Tenancy Offline
                result = _dbContext.CreateTable<TenancyAddressEnt>();
                result = _dbContext.CreateTable<TenancyCustomerEnt>();
                result = _dbContext.CreateTable<TenancyCustomerGeneralInfoEnt>();
                result = _dbContext.CreateTable<TenancyDisclosureEnt>();
                result = _dbContext.CreateTable<TenancyEmergencyContactEnt>();
                result = _dbContext.CreateTable<TenancyEmploymentInfoEnt>();
                result = _dbContext.CreateTable<TenancyJointTenantEnt>();
                result = _dbContext.CreateTable<TenancyOccupantEnt>();
                result = _dbContext.CreateTable<TenancyOnlineSetupEnt>();
                result = _dbContext.CreateTable<TenancyPropertyEnt>();
                result = _dbContext.CreateTable<TenancyReferralEnt>();
                result = _dbContext.CreateTable<TenancyRentEnt>();
                result = _dbContext.CreateTable<TenancyRootEnt>();

            }
        }
    }
}
