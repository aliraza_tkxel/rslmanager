﻿using FrontlineApp.DataLayer.Entities;
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;

namespace FrontlineApp.DataLayer.OfflineTenancyEntities
{
    public class TenancyReferralEnt
    {
        [PrimaryKey, AutoIncrement]
        public int? Id { get; set; }

        public int? CustomerId { get; set; }


        public int? ReferralHistoryId { get; set; }


        public int? JournalId { get; set; }


        public string Title { get; set; }


        public bool? IsTypeArrears { get; set; }


        public bool? IsTypeDamage { get; set; }


        public bool? IsTypeAsb { get; set; }


        public string Stage { get; set; }


        public int? StageId { get; set; }

        public string SafetyIssue { get; set; }

        public int? SafetyIssueId { get; set; }

        public int? IsConviction { get; set; }


        public string Conviction { get; set; }


        public int? IsSubstanceMisuse { get; set; }


        public string SubstanceMisuse { get; set; }


        public int? IsSelfHarm { get; set; }


        public string SelfHarm { get; set; }

        public string YesNotes { get; set; }

        public int? IsTenantAware { get; set; }


        public string TenantAware { get; set; }


        public string NeglectNotes { get; set; }


        public string OtherNotes { get; set; }


        public string MiscNotes { get; set; }


        public string CustomerHelpSubCategories { get; set; }


        public string MoneyIssues { get; set; }


        public string PersonalProblems { get; set; }


        public string WorkOrTrainings { get; set; }


        public string PracticalProblems { get; set; }



        public int? LastActionUserId { get; set; }


        public DateTimeOffset? LastModifiedOnServer { get; set; }


        public DateTimeOffset? LastModifiedOnApp { get; set; }


        public DateTimeOffset? CreatedOnApp { get; set; }


        public DateTimeOffset? CreatedOnServer { get; set; }


        public string AppVersion { get; set; }


        public string ReferrerName { get; set; }


        public string AidAndAdaptions { get; set; }
    }
}
