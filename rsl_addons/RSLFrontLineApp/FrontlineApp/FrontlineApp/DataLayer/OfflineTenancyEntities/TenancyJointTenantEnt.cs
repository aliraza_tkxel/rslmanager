﻿using FrontlineApp.DataLayer.Entities;
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;

namespace FrontlineApp.DataLayer.OfflineTenancyEntities
{
    public class TenancyJointTenantEnt
    {
        [PrimaryKey, AutoIncrement]
        public int? Id { get; set; }
        public int? TenancyId { get; set; }

        public int? CustomerId { get; set; }

        public DateTimeOffset? StartDate { get; set; }

        public bool IsCorrespondingAddress { get; set; }

        public bool IsJointTenantSelected { get; set; }
        public bool IsJointTenant { get; set; }

        public DateTimeOffset? JTDob { get; set; }
        public string JTFirstName { get; set; }
        public string JTLastName { get; set; }
        public string JTTelephone { get; set; }


        [OneToOne(foreignKey: "TenancyJointTenantId", inverseProperty: "JointTenant")]
        public TenancyRootEnt Tenancy { get; set; }

        public int? TenancyJTEmploymentInfoId { get; set; }
        [OneToOne(foreignKey: "TenancyJTEmploymentInfoId", inverseProperty: "JointTenant", CascadeOperations = CascadeOperation.All)]
        public TenancyEmploymentInfoEnt EmploymentInfoJT { get; set; }

        public int? TenancyJTCustomerGeneralInfoId { get; set; }
        [OneToOne(foreignKey: "TenancyJTCustomerGeneralInfoId", inverseProperty: "JointTenant", CascadeOperations = CascadeOperation.All)]
        public TenancyCustomerGeneralInfoEnt CustomerGeneralInfoJT { get; set; }

        public int? TenancyJTEmergencyContactId { get; set; }
        [OneToOne(foreignKey: "TenancyJTEmergencyContactId", inverseProperty: "JointTenant", CascadeOperations = CascadeOperation.All)]
        public TenancyEmergencyContactEnt EmergencyContactJT { get; set; }
    }
}
