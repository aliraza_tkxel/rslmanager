﻿using FrontlineApp.DataLayer.Entities;
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;

namespace FrontlineApp.DataLayer.OfflineTenancyEntities
{
    public class TenancyEmergencyContactEnt
    {
        [PrimaryKey, AutoIncrement]
        public int? Id { get; set; }
        public int? AddressId { get; set; }

        public int? AddressTypeId { get; set; }

        public string AddressType { get; set; }

        public string ContactName { get; set; }

        public string Telephone { get; set; }

        public string Mobile { get; set; }

        public string Email { get; set; }

        public string HouseNumber { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string Address3 { get; set; }

        public string City { get; set; }

        public string County { get; set; }

        public string PostCode { get; set; }

        public int? IsDefault { get; set; }

        public string CompleteAddress { get; set; }
        [OneToOne(foreignKey: "TenancyEmergencyContactId", inverseProperty: "EmergencyContact")]
        public TenancyRootEnt Tenancy { get; set; }

        [OneToOne(foreignKey: "TenancyJTEmergencyContactId", inverseProperty: "EmergencyContactJT")]
        public TenancyJointTenantEnt JointTenant { get; set; }
    }
}
