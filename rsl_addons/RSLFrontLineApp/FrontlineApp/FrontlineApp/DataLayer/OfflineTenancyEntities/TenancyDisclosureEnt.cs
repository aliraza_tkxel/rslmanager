﻿using FrontlineApp.DataLayer.Entities;
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
namespace FrontlineApp.DataLayer.OfflineTenancyEntities
{
    public class TenancyDisclosureEnt
    {
        [PrimaryKey, AutoIncrement]
        public int? Id { get; set; }
        public int? TenancyId { get; set; }

        public int? DisclosureId { get; set; }


        public bool? IsEvicted { get; set; }
        public bool? IsNotEvicted { get; set; }


        public string EvictedReason { get; set; }


        public bool? IsSubjectToDebt { get; set; }

        public bool? IsNotSubjectToDebt { get; set; }


        public bool? IsUnsatisfiedCcj { get; set; }

        public bool? IsNotUnsatisfiedCcj { get; set; }


        public bool? HasCriminalOffense { get; set; }
        public bool? HasNoCriminalOffense { get; set; }
    }
}
