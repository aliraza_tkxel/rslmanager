﻿using FrontlineApp.DataLayer.Entities;
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;

namespace FrontlineApp.DataLayer.OfflineTenancyEntities
{
    public class TenancyOccupantEnt
    {
        [PrimaryKey, AutoIncrement]
        public int? Id { get; set; }


        public int? OccupantId { get; set; }


        public int? CustomerId { get; set; }


        public int? TitleId { get; set; }


        public string FirstName { get; set; }

        public string Disabilities { get; set; }

        public string UIGuidId { get; set; }


        public string LastName { get; set; }


        public DateTimeOffset? DateOfBirth { get; set; }


        public int? RelationShipStatusId { get; set; }


        public string DisabilitiesOthers { get; set; }


        public int? EmploymentStatusId { get; set; }

        [ForeignKey(typeof(TenancyRootEnt))]
        public int tenancy { get; set; }

        [ManyToOne]
        public TenancyRootEnt Tenancy { get; set; }
    }
}
