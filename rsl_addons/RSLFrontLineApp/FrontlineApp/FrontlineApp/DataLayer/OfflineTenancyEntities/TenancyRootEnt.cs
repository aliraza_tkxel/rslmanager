﻿using FrontlineApp.DataLayer.Entities;
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;

namespace FrontlineApp.DataLayer.OfflineTenancyEntities
{
    public class TenancyRootEnt
    {
        #region "Properties"
        [PrimaryKey, AutoIncrement]
        public int? Id { get; set; }
        public int? TenancyId { get; set; }
        public int? CustomerId { get; set; }
        public string PropertyId { get; set; }
        public DateTime? TenancyStartDate { get; set; }
        public DateTime? TenancyEndDate { get; set; }
        public int? PausedOnStepId { get; set; }
        #endregion

        #region "One to One Relations"
        public int? CustomerIdFK { get; set; }
        [OneToOne(foreignKey: "CustomerIdFK", inverseProperty: "Tenancy", CascadeOperations = CascadeOperation.All)]
        public TenancyCustomerEnt Customer { get; set; }

        public int? PropertyIdFK { get; set; }
        [OneToOne(foreignKey: "PropertyIdFK", inverseProperty: "Tenancy", CascadeOperations = CascadeOperation.All)]
        public TenancyPropertyEnt Property { get; set; }

        public int? TenancyAddressId { get; set; }
        [OneToOne(foreignKey: "TenancyAddressId", inverseProperty: "Tenancy", CascadeOperations = CascadeOperation.All)]
        public TenancyAddressEnt TenancyAddress { get; set; }

        public int? TenancyEmploymentInfoId { get; set; }
        [OneToOne(foreignKey: "TenancyEmploymentInfoId", inverseProperty: "Tenancy", CascadeOperations = CascadeOperation.All)]
        public TenancyEmploymentInfoEnt EmploymentInfo { get; set; }

        public int? TenancyCustomerGeneralInfoId { get; set; }
        [OneToOne(foreignKey: "TenancyCustomerGeneralInfoId", inverseProperty: "Tenancy", CascadeOperations = CascadeOperation.All)]
        public TenancyCustomerGeneralInfoEnt CustomerGeneralInfo { get; set; }

        public int? TenancyEmergencyContactId { get; set; }
        [OneToOne(foreignKey: "TenancyEmergencyContactId", inverseProperty: "Tenancy", CascadeOperations = CascadeOperation.All)]
        public TenancyEmergencyContactEnt EmergencyContact { get; set; }

        public int? TenancyJointTenantId { get; set; }
        [OneToOne(foreignKey: "TenancyJointTenantId", inverseProperty: "Tenancy", CascadeOperations = CascadeOperation.All)]
        public TenancyJointTenantEnt JointTenant { get; set; }

        public int? TenancyReferralId { get; set; }
        [OneToOne(foreignKey: "TenancyReferralId", inverseProperty: "Tenancy", CascadeOperations = CascadeOperation.All)]
        public TenancyReferralEnt Referral { get; set; }

        public int? TenancyRentInfoId { get; set; }
        [OneToOne(foreignKey: "TenancyRentInfoId", inverseProperty: "Tenancy", CascadeOperations = CascadeOperation.All)]
        public TenancyRentEnt PropertyRentInfo { get; set; }

        public int? TenancyDisclosureId { get; set; }
        [OneToOne(foreignKey: "TenancyDisclosureId", inverseProperty: "Tenancy", CascadeOperations = CascadeOperation.All)]
        public TenancyDisclosureEnt TenancyDisclosure { get; set; }

        public int? TenancyOnlineInfoId { get; set; }
        [OneToOne(foreignKey: "TenancyOnlineInfoId", inverseProperty: "Tenancy", CascadeOperations = CascadeOperation.All)]
        public TenancyOnlineSetupEnt TenantOnlineInfo { get; set; }

        #endregion

        #region "One to Many Relations"

        [OneToMany(CascadeOperations = CascadeOperation.All)]
        public List<TenancyOccupantEnt> Occupants { get; set; }

        #endregion
    }
}
