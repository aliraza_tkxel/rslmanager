﻿using FrontlineApp.DataLayer.Entities;
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;

namespace FrontlineApp.DataLayer.OfflineTenancyEntities
{
    public class TenancyOnlineSetupEnt
    {
        [PrimaryKey, AutoIncrement]
        public int? Id { get; set; }


        public string UserName { get; set; }


        public string Password { get; set; }
    }
}
