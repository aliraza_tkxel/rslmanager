﻿using FrontlineApp.DataLayer.Entities;
using FrontlineApp.Utility.Constants;
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;

namespace FrontlineApp.DataLayer.OfflineTenancyEntities
{
    public class TenancyPropertyEnt
    {
        [PrimaryKey, AutoIncrement]
        public int? Id { get; set; }
        public string propertyId { get; set; }
        public string propertyType { get; set; }
        public int? propertyTypeId { get; set; }
        public string scheme { get; set; }
        public int? schemeId { get; set; }
        public string bedRooms { get; set; }
        public string projectedRent { get; set; }
        public double rentNumeric { get; set; }

        public string address { get; set; }
        public string status { get; set; }
        public string subStatus { get; set; }

        public string imageName { get; set; }
        public string postalCode { get; set; }
        [OneToOne(foreignKey: "PropertyIdFK", inverseProperty: "Property")]
        public TenancyRootEnt Tenancy { get; set; }
    }
}
