﻿using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;

namespace FrontlineApp.DataLayer.OfflineTenancyEntities
{
    public class TenancyCustomerEnt
    {
        [PrimaryKey, AutoIncrement]
        public int? Id { get; set; }

        public int? CustomerId { get; set; }

        public string CustomerNumber { get; set; }

        public string CustomerType { get; set; }

        public int? CustomerTypeId { get; set; }


        public int? TenancyCount { get; set; }


        public bool IsJointTenant { get; set; }



        public int? TitleId { get; set; }


        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public string PreferredContact { get; set; }

        public int? PreferedContactId { get; set; }


        public string InternetAccess { get; set; }


        public string InternetAccessDescription { get; set; }


        public DateTimeOffset? Dob { get; set; }


        public string Gender { get; set; }


        public string MaritalStatus { get; set; }


        public int? MaritalStatusId { get; set; }


        public string Ethnicity { get; set; }


        public int? EthnicityId { get; set; }


        public string Religion { get; set; }

        public int? ReligionId { get; set; }

        public string SexualOrientation { get; set; }

        public int? SexualOrientationId { get; set; }


        public string NiNumber { get; set; }


        public int? NationalityId { get; set; }


        public string Nationality { get; set; }


        public string FirstLanguage { get; set; }


        public int? IsSubjectToImmigration { get; set; }


        public string SubjectToImmigration { get; set; }


        public int? IsPermanentUKResident { get; set; }


        public string PermanentUKResident { get; set; }


        public string ResidencyStatus { get; set; }


        public string Communication { get; set; }


        public string GroupKey { get; set; }


        public string CommunicationList { get; set; }


        public string AppVersion { get; set; }


        public int? CreatedBy { get; set; }


        public bool IsTenant { get; set; }


        public DateTimeOffset? CreatedOnApp { get; set; }


        public DateTimeOffset? CreatedOnServer { get; set; }


        public DateTimeOffset? LastModifiedOnApp { get; set; }


        public DateTimeOffset? LastModifiedOnServer { get; set; }


        public bool? IsSynced { get; set; }


        public string Name { get; set; }

        public string Telephone { get; set; }

        public int? Children { get; set; }

        public int? Adults { get; set; }

        [OneToOne(foreignKey: "CustomerIdFK", inverseProperty: "Customer")]
        public TenancyRootEnt Tenancy { get; set; }
    }
}
