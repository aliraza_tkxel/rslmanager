﻿using FrontlineApp.DataLayer.Entities;
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;

namespace FrontlineApp.DataLayer.OfflineTenancyEntities
{
    public class TenancyEmploymentInfoEnt
    {
        [PrimaryKey, AutoIncrement]
        public int? Id { get; set; }

        public int? EmploymentStatusId { get; set; }
        public string EmploymentStatusDescription { get; set; }
        public string Occupation { get; set; }
        public string EmployerName { get; set; }
        public string EmployerAddress { get; set; }
        public int? TakeHomePay { get; set; }
        [OneToOne(foreignKey: "TenancyEmploymentInfoId", inverseProperty: "EmploymentInfo")]
        public TenancyRootEnt Tenancy { get; set; }

        [OneToOne(foreignKey: "TenancyJTEmploymentInfoId", inverseProperty: "EmploymentInfoJT")]
        public TenancyJointTenantEnt JointTenant { get; set; }
    }
}
