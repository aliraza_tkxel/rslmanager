﻿using FrontlineApp.DataLayer.Entities;
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;

namespace FrontlineApp.DataLayer.OfflineTenancyEntities
{
    public class TenancyCustomerGeneralInfoEnt
    {
        [PrimaryKey, AutoIncrement]
        public int? Id { get; set; }

        public string Benefits { get; set; }

        public string BankingFacilities { get; set; }

        public string BankingFacilityOther { get; set; }

        public string HealthProblems { get; set; }

        public string HealthProblemOthers { get; set; }

        public int? LiveInCarer { get; set; }

        public int? Wheelchair { get; set; }

        public string SupportFromAgencies { get; set; }

        public string SupportFromAgenciesOther { get; set; }


        public bool HasSupportFromAgencies { get; set; }


        public bool HasHealthProblems { get; set; }

        public bool DoesNotHaveHealthProblems { get; set; }

        public bool DoesNotHaveAgencySupport { get; set; }

        public string AidsAndAdaptation { get; set; }
        [OneToOne(foreignKey: "TenancyCustomerGeneralInfoId", inverseProperty: "CustomerGeneralInfo")]
        public TenancyRootEnt Tenancy { get; set; }

        [OneToOne(foreignKey: "TenancyJTCustomerGeneralInfoId", inverseProperty: "CustomerGeneralInfoJT")]
        public TenancyJointTenantEnt JointTenant { get; set; }
    }
}
