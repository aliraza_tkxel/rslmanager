﻿using FrontlineApp.DataLayer.Entities;
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;

namespace FrontlineApp.DataLayer.OfflineTenancyEntities
{
    public class TenancyRentEnt
    {
        [PrimaryKey, AutoIncrement]
        public int? Id { get; set; }


        public decimal? RentPayable { get; set; }


        public decimal? Rent { get; set; }


        public decimal? Services { get; set; }


        public decimal? CouncilTax { get; set; }


        public decimal? WaterRates { get; set; }


        public decimal? IneligServ { get; set; }


        public decimal? SupportedServices { get; set; }


        public decimal? Garage { get; set; }

        public decimal? PayableRentDynamic { get; set; }
    }
}
