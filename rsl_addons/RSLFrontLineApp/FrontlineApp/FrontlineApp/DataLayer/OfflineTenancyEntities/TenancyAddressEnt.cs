﻿using FrontlineApp.DataLayer.Entities;
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;

namespace FrontlineApp.DataLayer.OfflineTenancyEntities
{
    public class TenancyAddressEnt
    {
        [PrimaryKey, AutoIncrement]
        public int? id { get; set; }
        public string HouseNumber { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string PostCode { get; set; }
        public string County { get; set; }
        public string CompleteAddress { get; set; }
        [OneToOne(foreignKey: "TenancyAddressId", inverseProperty: "TenancyAddress")]
        public TenancyRootEnt Tenancy { get; set; }
    }
}
