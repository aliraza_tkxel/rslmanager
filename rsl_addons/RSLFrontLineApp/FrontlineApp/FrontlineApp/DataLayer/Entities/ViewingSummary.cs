﻿using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System.Collections.Generic;

namespace FrontlineApp.DataLayer.Entities
{
    public class ViewingSummary
    {
        public ViewingSummary()
        {

        }
        [PrimaryKey,AutoIncrement]
        public int id { get; set; }
        public int viewingId { get; set; }
        public string viewingDate { get; set; }
        public string viewingTime { get; set; }
        public string customerName { get; set; }
        public string customerGender { get; set; }
        public string housingOfficerGender { get; set; }
        public int viewingStatusId { get; set; }
        public int housingOfficerId { get; set; }
        public string housingOfficerName { get; set; }
        public string housingOfficerEmail { get; set; }
        public int createdById { get; set; }
        public string viewingCreatedBy { get; set; }
        public string propertyAddress { get; set; }
        public string createdOnApp { get; set; }
        public string propertyId { get; set; }
        public string viewingNotes { get; set; }
        public double rent { get; set; }
        public string noOfBedRooms { get; set; }
        public string propertyImage { get; set; }
        public string propertyType { get; set; }
        public string customerType { get; set; }
        public string mobileNo { get; set; }
        public string dob { get; set; }
        public int customerId { get; set; }
        public string outlookIdentifier { get; set; }
        public int? assignedById { get; set; }
        public string assignedByName { get; set; }
        #region "One to Many Relations"

        [OneToMany(CascadeOperations = CascadeOperation.All)]
        public List<ViewingComment> commentsList { get; set; }
        #endregion
    }
}
