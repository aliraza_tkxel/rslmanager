﻿using SQLite.Net.Attributes;
using System;

namespace FrontlineApp.DataLayer.Entities
{
    public class Viewing
    {
        public Viewing()
        {

        }
        [PrimaryKey,AutoIncrement]
        public int ID { get; set; } // Local Id
        public DateTime ScheduledDateTime { get; set; } /*When is the viewing scheduled for?*/
        public DateTime CreationDate { get; set; } // As name suggests 
        public int ViewingID { get; set; } /*Server Id*/
        public int HousingOfficerID { get; set; } /*FK to Housing Officer*/
        public int CustomerID { get; set; } /*FK to Customer*/
        public int PropertyID { get; set; } /* FK to Property*/
        public int ViewingStatus { get; set; } /*Will be assigned to enum. Accepted/Waiting/Rejected for Logged, 
        Waiting for Approval, NA for MyQueue*/
        public string FeedBack { get; set; } //Feedback recorded by Housing Officer
        public string AssignedBy { get; set; } /*Name of Officer who assigned this Viewing. Could be a FK to
        Housing Officer but using string instead to avoid over normalization*/
        public bool IsCancelled { get; set; } /*Is this Viewing Cancelled?*/
        public string CancellationReason { get; set; } /*The reason submitted for cancellation*/
    }
}
