﻿using SQLite.Net.Attributes;

namespace FrontlineApp.DataLayer.Entities
{
    public class TerminationReason
    {
        [PrimaryKey]
        public int id { get; set; }
        public string description { get; set; }
    }
}
