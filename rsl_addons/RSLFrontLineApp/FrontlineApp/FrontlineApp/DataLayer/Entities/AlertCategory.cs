﻿using System;
using SQLite.Net.Attributes;

namespace FrontlineApp.DataLayer.Entities
{
    public class AlertCategory
    {
        public AlertCategory()
        {

        }
        [PrimaryKey]
        public int AlertCategoryID { get; set; }
        public string AlertCategoryTitle { get; set; }
        public int AlertCategoryType { get; set; } /*Will be assigned to an enum. Risk and Vulnerability*/
    }
}
