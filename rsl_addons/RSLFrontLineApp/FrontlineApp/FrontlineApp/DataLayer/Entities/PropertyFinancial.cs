using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
public class PropertyFinancial
{
	[PrimaryKey, AutoIncrement]
    public int? id { get; set; }
    public decimal? rentPayable { get; set; }
    public decimal? rent { get; set; }
    public decimal? services { get; set; }
    public decimal? councilTax { get; set; }
    public decimal? waterRates { get; set; }
    public decimal? ineligServ { get; set; }
    public decimal? supportedServices { get; set; }
    public decimal? garage { get; set; }

    [OneToOne(foreignKey: "propertyFinancialId", inverseProperty: "propertyFinancial")]
    public Tenancy Tenancy { get; set; }

}
