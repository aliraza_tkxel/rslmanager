using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
public class TenancyDisclosure
{
	[PrimaryKey, AutoIncrement]
    public int? id { get; set; }
	public int? disclosureId { get; set; }
    public bool? isEvicted { get; set; }
    public string evictedReason { get; set; }
    public bool? isSubjectToDebt { get; set; }
    public bool? isUnsatisfiedCcj { get; set; }
    public bool? hasCriminalOffense { get; set; }
    public int? tenancyId { get; set; }

    [OneToOne(foreignKey: "tenancyDisclosureId", inverseProperty: "tenancyDisclosure")]
    public Tenancy Tenancy { get; set; }
}
