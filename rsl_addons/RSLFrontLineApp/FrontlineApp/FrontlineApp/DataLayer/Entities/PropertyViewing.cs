﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite.Net.Attributes;
namespace FrontlineApp.DataLayer.Entities
{
    public class PropertyViewing
    {
        public PropertyViewing()
        {

        }
        [PrimaryKey]
        public int viewingId { get; set; }
        public int viewingStatusId { get; set; }
        public string propertyId { get; set; }
        public string viewingDate { get; set; }
        public string viewingTime { get; set; }
        public int customerId { get; set; }
        public string customerName { get; set; }
        public int housingOfficerId { get; set; }
        public string housingOfficerName { get; set; }
    }
}
