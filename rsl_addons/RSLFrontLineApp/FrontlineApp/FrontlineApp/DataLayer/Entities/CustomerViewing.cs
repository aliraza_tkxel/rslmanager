using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;

public class CustomerViewing
{
	[PrimaryKey]
    public int viewingId { get; set; }
    public DateTimeOffset? viewingDateTime { get; set; }
    public string viewingTime { get; set; }
    public int viewingStatusId { get; set; }
    public int? housingOfficerId { get; set; }
    public string housingOfficerName { get; set; }
    public string propertyAddress { get; set; }
    public string propertyId { get; set; }
	
	[ForeignKey(typeof(Customer))]
    public int customer { get; set; }
	
	[ManyToOne]
    public Customer Customer { get; set; }
}
