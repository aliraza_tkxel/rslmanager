﻿using FrontlineApp.DataLayer.Entities;
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;

public class Tenancy
{
	[PrimaryKey, AutoIncrement]
    public int? id { get; set; }
    public int? tenancyId { get; set; }
	public int? customerId { get; set; }
    public string propertyId { get; set; }
    public DateTime? tenancyStartDate { get; set; }
    public DateTime? tenancyEndDate { get; set; }


    #region "One To One Relationships"

    [OneToOne(foreignKey: "tenancyInfoId", inverseProperty: "tenancy")]
    public Customer Customer { get; set; }


    public int? propertyFinancialId { get; set; }
    [OneToOne(foreignKey: "propertyFinancialId", inverseProperty: "Tenancy", CascadeOperations = CascadeOperation.All)]
    public PropertyFinancial propertyFinancial { get; set; }


    public int? tenancyDisclosureId { get; set; }    
    [OneToOne(foreignKey: "tenancyDisclosureId", inverseProperty: "Tenancy", CascadeOperations = CascadeOperation.All)]
    public TenancyDisclosure tenancyDisclosure { get; set; }


    public int? tenancyTerminationId { get; set; }
    [OneToOne(foreignKey: "tenancyTerminationId", inverseProperty: "Tenancy", CascadeOperations = CascadeOperation.All)]
    public TenancyTermination tenancyTermination { get; set; }


    public int? customerReferralId { get; set; }
    [OneToOne(foreignKey: "customerReferralId", inverseProperty: "Tenancy", CascadeOperations = CascadeOperation.All )]
	public CustomerReferral customerReferral { get; set; }


    public int? tenantOnlineId { get; set; }
    [OneToOne(foreignKey: "tenantOnlineId", inverseProperty: "Tenancy", CascadeOperations = CascadeOperation.All)]
    public TenantOnline tenantOnline { get; set; }

    public int? tenancyAddressId { get; set; }
    [OneToOne(foreignKey: "tenancyAddressId", inverseProperty: "Tenancy", CascadeOperations = CascadeOperation.All)]
    public TenancyAddress tenancyAddress { get; set; }

    #endregion


    #region "One To Many Relationships"

    [OneToMany(CascadeOperations = CascadeOperation.CascadeRead | CascadeOperation.CascadeDelete)]
    public List<JointTenant> JointTenants { get; set; }

    [OneToMany(CascadeOperations = CascadeOperation.CascadeRead | CascadeOperation.CascadeDelete)]
	public List<Occupant> Occupants { get; set; }

    [OneToMany(CascadeOperations = CascadeOperation.All)]
    public List<CustomerVulnerability> Vulnerabilities { get; set; }

    [OneToMany(CascadeOperations = CascadeOperation.All)]
    public List<CustomerRisk> Risks { get; set; }

    #endregion

}
