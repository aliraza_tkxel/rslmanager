﻿using FrontlineApp.DataLayer.Entities;
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;

namespace FrontlineApp.DataLayer.Entities
{
    public class TenancyAddress
    {
        [PrimaryKey, AutoIncrement]
        public int? id { get; set; }
        public string houseNumber { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string address3 { get; set; }
        public string city { get; set; }
        public string postCode { get; set; }
        public string county { get; set; }
        [OneToOne(foreignKey: "tenancyAddressId", inverseProperty: "tenancyAddress")]
        public Tenancy Tenancy { get; set; }
    }
}
