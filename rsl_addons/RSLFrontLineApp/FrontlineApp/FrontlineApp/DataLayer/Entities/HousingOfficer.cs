﻿using SQLite.Net.Attributes;

namespace FrontlineApp.DataLayer.Entities
{
    public class HousingOfficer
    {
        public HousingOfficer()
        {

        }
        [PrimaryKey]
        public int userId { get; set; }
        public string employeeFullName { get; set; }
        public int isActive { get; set; }
        public string userName { get; set; }
        public string Password { get; set; }
        public bool IsLoggedIn { get; set; } // If true, this user is the one logged one. 
        public int TeamID { get; set; } /*FK to Team*/
    }
}
