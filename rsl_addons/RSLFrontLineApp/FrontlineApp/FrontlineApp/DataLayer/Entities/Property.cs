﻿using System;
using SQLite.Net.Attributes;

namespace FrontlineApp.DataLayer.Entities
{
    public class Property
    {
        public Property()
        {
           
        }
        /*Property Basic Info*/
        [PrimaryKey]
        public string propertyId { get; set; }
        public double rent { get; set; }
        public string bedRooms { get; set; }
        public string address { get; set; }
        public string status { get; set; }
        public string subStatus { get; set; }
        public string imageName { get; set; } /*This one should be used*/
        public string postalCode { get; set; }
        public int? schemeId { get; set; }
        public int? propertyTypeId { get; set; }
    }
}
