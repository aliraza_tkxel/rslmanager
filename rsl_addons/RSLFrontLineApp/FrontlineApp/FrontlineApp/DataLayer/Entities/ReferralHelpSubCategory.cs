﻿using SQLite.Net.Attributes;

namespace FrontlineApp.DataLayer.Entities
{
    public class ReferralHelpSubCategory
    {
        [PrimaryKey]
        public int id { get; set; }
        public string description { get; set; }
        public int subId { get; set; }
    }
}
