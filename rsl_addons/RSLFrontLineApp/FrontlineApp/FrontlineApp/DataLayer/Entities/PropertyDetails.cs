﻿using System;
using SQLite.Net.Attributes;

namespace FrontlineApp.DataLayer.Entities
{
    public class PropertyDetails
    {
        [PrimaryKey]
        public string propertyId { get; set; }
        public int propertyTypeId { get; set; }
        public string developmentType { get; set; }
        public int? schemeId { get; set; }
        public string phase { get; set; }
        public string block { get; set; }
        public string houseNo { get; set; }
        public string address { get; set; }
        public string townCity { get; set; }
        public string county { get; set; }
        public string imageName { get; set; }
        public string postalCode { get; set; }
        public double rent { get; set; }
        public string status { get; set; }
        public string rightToAcquire { get; set; }
        public string assetType { get; set; }
        public string floorArea { get; set; }
        public string maxPeople { get; set; }
        public string numberOfLivingRooms { get; set; }
        public string numberOfBedRooms { get; set; }
        public string numberOfBathRooms { get; set; }
        public string numberOfkitchen { get; set; }
        public string parking { get; set; }
        public string garden { get; set; }
        public string garage { get; set; }
        public string heatingFuel { get; set; }
        public string boilerType { get; set; }
        public string wheelChairAccess { get; set; }
    }
}
