﻿using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;

public class Customer
{
	[PrimaryKey, AutoIncrement]
    public int? id { get; set; }
    public int? customerId { get; set; }
    public string customerNumber { get; set; }
    public int? customerTypeId { get; set; }
    public int? tenancyCount { get; set; }
    public int? titleId { get; set; }
    public string firstName { get; set; }
    public string middleName { get; set; }
    public string lastName { get; set; }
    public int? preferedContactId { get; set; }
    public string internetAccess { get; set; }
    public DateTimeOffset? dob { get; set; }
    public string gender { get; set; }
    public int? maritalStatusId { get; set; }
    public int? ethnicityId { get; set; }
    public int? religionId { get; set; }
    public int? sexualOrientationId { get; set; }
    public string niNumber { get; set; }
    public int? nationalityId { get; set; }
    public string firstLanguage { get; set; }
    public int? isSubjectToImmigration { get; set; }
    public int? isPermanentUKResident { get; set; }
    public string communication { get; set; }
    public int? createdBy { get; set; }
    public int? children { get; set; }
    public int? adults { get; set; }
    public string appVersion { get; set; }
    public DateTimeOffset? createdOnApp { get; set; }
	public DateTimeOffset? createdOnServer { get; set; }
    public DateTimeOffset? lastModifiedOnApp { get; set; }
    public DateTimeOffset? lastModifiedOnServer { get; set; }	
    public bool? isSynced { get; set; }

    #region "One To One Relationships"

    public int? homeId { get; set; }
    [OneToOne(foreignKey: "homeId", inverseProperty: "Customer", CascadeOperations = CascadeOperation.All)]
    public CurrentHome currentHome { get; set; }

    public int? employmentInfoId { get; set; }
    [OneToOne(foreignKey: "employmentInfoId", inverseProperty: "Customer", CascadeOperations = CascadeOperation.All)]
	public EmploymentInfo employmentInfo { get; set; }

    public int? customerGeneralInfoId { get; set; }
    [OneToOne(foreignKey: "customerGeneralInfoId", inverseProperty: "Customer", CascadeOperations = CascadeOperation.All)]
    public CustomerGeneralInfo customerGeneralInfo { get; set; }


    public int? customerImageId { get; set; }
    [OneToOne(foreignKey: "customerImageId", inverseProperty: "Customer", CascadeOperations = CascadeOperation.All)]
	public CustomerImage customerImage { get; set; }


    public int? tenancyInfoId { get; set; }
    [OneToOne(foreignKey: "tenancyInfoId", inverseProperty: "Customer", CascadeOperations = CascadeOperation.All)]
    public Tenancy tenancy { get; set; }

    #endregion

    #region "One To Many Relationships"
    [OneToMany(CascadeOperations = CascadeOperation.All)]
    public List<CustomerAddress> CustomerAddresses { get; set; }

    [OneToMany(CascadeOperations = CascadeOperation.All)]
	public List<CustomerViewing> CustomerViewings { get; set; }
    #endregion

}
