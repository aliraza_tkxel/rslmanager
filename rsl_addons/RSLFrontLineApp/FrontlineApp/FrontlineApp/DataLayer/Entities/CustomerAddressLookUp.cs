﻿using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;

namespace FrontlineApp.DataLayer.Entities
{
    public class CustomerAddressLookUp
    {
        [PrimaryKey]
        public int addressId { get; set; }
        public string houseNumber { get; set; }
        public string address1 { get; set; }
        public string address2 { get; set; }
        public string address3 { get; set; }
        public string city { get; set; }
        public string postCode { get; set; }
        public string county { get; set; }
        public string telephone { get; set; }
        public string mobile { get; set; }
        public string email { get; set; }
        public int? addressTypeId { get; set; }
        public int? isDefault { get; set; }
        public string contactName { get; set; }
        [ForeignKey(typeof(CustomerLookup))]
        public int customer { get; set; }

        [ManyToOne]
        public CustomerLookup Customer { get; set; }
    }
}
