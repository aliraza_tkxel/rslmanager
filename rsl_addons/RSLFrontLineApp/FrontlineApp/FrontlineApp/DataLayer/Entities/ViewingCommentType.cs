﻿using SQLite.Net.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.DataLayer.Entities
{
    public class ViewingCommentType
    {
        [PrimaryKey]
        public int id { get; set; }
        public string description { get; set; }
    }
}
