﻿using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.DataLayer.Entities
{
    public class CustomerLookUpImage
    {
        [PrimaryKey, AutoIncrement]
        public int? id { get; set; }
       
        public string imageName { get; set; }
        [OneToOne(foreignKey: "customerImageId", inverseProperty: "customerImage")]
        public CustomerLookup Customer { get; set; }
    }
}
