using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
public class Occupant
{
	[PrimaryKey, AutoIncrement]
    public int id { get; set; }
    public int occupantId { get; set; }
    public int customerId { get; set; }
    public string firstName { get; set; }
    public string lastName { get; set; }
    public string dateOfBirth { get; set; }
    public int relationshipStatusId { get; set; }
    public string disabilities { get; set; }
    public string disabilitiesOther { get; set; }
    public int employmentStatusId { get; set; }
	
	[ForeignKey(typeof(Tenancy))]
    public int tenancy { get; set; }
	
	[ManyToOne]
    public Tenancy Tenancy { get; set; }
}
