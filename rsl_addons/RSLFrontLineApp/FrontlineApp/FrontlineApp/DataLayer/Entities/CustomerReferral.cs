using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;

public class CustomerReferral
{
	[PrimaryKey, AutoIncrement]
    public int? id { get; set; }
    public int? referralHistoryId { get; set; }
    public int? journalId { get; set; }
    public string title { get; set; }
    public bool? isTypeArrears { get; set; }
    public bool? isTypeDamage { get; set; }
    public bool? isTypeAsb { get; set; }
    public int? stageId { get; set; }
    public int? safetyIssueId { get; set; }
    public int? isConviction { get; set; }
    public int? isSubstanceMisuse { get; set; }
    public int? isSelfHarm { get; set; }
    public string yesNotes { get; set; }
    public int? isTenantAware { get; set; }
    public string neglectNotes { get; set; }
    public string otherNotes { get; set; }
    public string miscNotes { get; set; }
    public string appVersion { get; set; }
    public DateTimeOffset? createdOnApp { get; set; }
    public DateTimeOffset? createdOnServer { get; set; }
    public DateTimeOffset? lastModifiedOnApp { get; set; }
    public DateTimeOffset? lastModifiedOnServer { get; set; }
    public int? lastActionUserId { get; set; }
    public string customerHelpSubCategories { get; set; }
	public int? customerId { get; set; }
    public string referrerName { get; set; }

    [OneToOne(foreignKey: "customerReferralId", inverseProperty: "customerReferral")]
    public Tenancy Tenancy { get; set; }
}
