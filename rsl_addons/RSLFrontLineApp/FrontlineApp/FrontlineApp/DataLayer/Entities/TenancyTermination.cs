using System;
using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
public class TenancyTermination
{
	[PrimaryKey, AutoIncrement]
    public int? id { get; set; }
    public int? terminationHistoryId { get; set; }
    public int? reasonId { get; set; }
    public int? customerId { get; set; }
    public int? reasonCategoryId { get; set; }
    public string notes { get; set; }
    public int? terminatedBy { get; set; }
    public string appVersion { get; set; }
    public DateTimeOffset? terminationDate { get; set; }

    [OneToOne(foreignKey: "tenancyTerminationId", inverseProperty: "tenancyTermination")]
    public Tenancy Tenancy { get; set; }

}
