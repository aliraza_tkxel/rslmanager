﻿using SQLite.Net.Attributes;

namespace FrontlineApp.DataLayer.Entities
{
    public class StaffMember
    {
        [PrimaryKey]
        public int employeeId { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string imagePath { get; set; }
        public string gender { get; set; }
        public string email { get; set; }
    }
}
