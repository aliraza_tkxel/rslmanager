﻿using SQLite.Net.Attributes;

namespace FrontlineApp.DataLayer.Entities
{
    public class CustomerType
    {
        [PrimaryKey]
        public int id { get; set; }
        public string description { get; set; }
    }
}
