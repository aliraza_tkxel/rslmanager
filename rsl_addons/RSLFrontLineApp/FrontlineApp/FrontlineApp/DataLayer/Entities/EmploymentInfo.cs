using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
public class EmploymentInfo
{
	[PrimaryKey, AutoIncrement]
    public int id { get; set; }
    public int employmentStatusId { get; set; }
    public string occupation { get; set; }
    public string employerName { get; set; }
    public string employerAddress { get; set; }
    public int? takeHomePay { get; set; }
    [OneToOne(foreignKey: "employmentInfoId", inverseProperty: "employmentInfo")]
    public Customer Customer { get; set; }

}
