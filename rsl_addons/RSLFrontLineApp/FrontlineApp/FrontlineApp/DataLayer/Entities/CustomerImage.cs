using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;

public class CustomerImage
{
	[PrimaryKey, AutoIncrement]
    public int? id { get; set; }
    public string imageName { get; set; }
    public bool? isSynced { get; set; }

    [OneToOne(foreignKey: "customerImageId", inverseProperty: "customerImage")]
    public Customer Customer { get; set; }
}
