﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQLite.Net.Attributes;

namespace FrontlineApp.DataLayer.Entities
{
    public class ItemStatus
    {
        [PrimaryKey]
        public int id { get; set; }
        public string description { get; set; }
    }
}
