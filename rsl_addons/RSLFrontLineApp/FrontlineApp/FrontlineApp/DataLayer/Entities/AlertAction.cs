﻿using SQLite.Net.Attributes;
using System;

namespace FrontlineApp.DataLayer.Entities
{
    public class AlertAction
    {
        public AlertAction()
        {

        }
        [PrimaryKey]
        public int AlertActionID { get; set; }
        public string AlertActionTitle { get; set; }
        public string AlertActionType { get; set; } /*Will be assigned to an enum. Risk or Vulnerability*/
    }
}
