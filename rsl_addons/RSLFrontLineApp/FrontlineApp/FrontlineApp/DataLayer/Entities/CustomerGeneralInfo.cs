using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
public class CustomerGeneralInfo
{
	[PrimaryKey, AutoIncrement]
    public int? id { get; set; }
    public string bankingFacilities { get; set; }
    public string bankingFacilityOther { get; set; }
    public string benefits { get; set; }
    public string healthProblems { get; set; }
    public string healthProblemsOther { get; set; }
    public int? liveInCarer { get; set; }
    public int? wheelChair { get; set; }
    public string supportFromAgencies { get; set; }
    public string supportFromAgenciesOther { get; set; }
	public string aidsAndAdaptation { get; set; }

    [OneToOne(foreignKey: "customerGeneralInfoId", inverseProperty: "customerGeneralInfo")]
    public Customer Customer { get; set; }

}
