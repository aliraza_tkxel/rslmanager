﻿using SQLite.Net.Attributes;
using System;
namespace FrontlineApp.DataLayer.Entities
{
    public class Alert
    {
        public Alert()
        {

        }
        [PrimaryKey]
        public int AlertID { get; set; }
        public int CustomerID { get; set; } /*FK to customer*/
        public string Title { get; set; }
        public int HousingOfficerID { get; set; } /*FK to Housing officer assigned to inquiry*/
        public DateTime StartDate { get; set; }

        public Array[] AlertCategoryId { get; set; } /*FK to AlertCategory. Can be zero to Many*/
        public Array[] AlertSubCategoryId { get; set; } /*FK to AlertSubcategory Can be Zero to Many*/

        public string Notes { get; set; }
        public DateTime ReviewDate { get; set; }

        public bool IsCustomerAware { get; set; }
        public bool IsTenantOnline { get; set; }

        public int AlertActionID { get; set; } /*FK to AlertAction*/
        public int AlertLetterID { get; set; } /*FK to Letter*/

        public int EnquiryStatus { get; set; } /*Will be assigned to an enum. Open, In progress, Completed*/
        public int AlertType { get; set; } /*Will be assigned to an enum. Alert or Vulnerability*/
    }
}
