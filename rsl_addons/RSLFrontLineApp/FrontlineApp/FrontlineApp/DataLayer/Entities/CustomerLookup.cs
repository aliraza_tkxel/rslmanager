﻿using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;

namespace FrontlineApp.DataLayer.Entities
{
    public class CustomerLookup
    {
        [PrimaryKey]
        public int customerId { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public DateTimeOffset? dob { get; set; }
        public string gender { get; set; }
        public int customerTypeId { get; set; }
        public int tenancyCount { get; set; }

        public int? customerImageId { get; set; }
        [OneToOne(foreignKey: "customerImageId", inverseProperty: "Customer", CascadeOperations = CascadeOperation.All)]
        public CustomerLookUpImage customerImage { get; set; }

        [OneToMany(CascadeOperations = CascadeOperation.All)]
        public List<CustomerAddressLookUp> customerAddress { get; set; }
    }
}
