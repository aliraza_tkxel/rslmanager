﻿using System;
using SQLite.Net.Attributes;

namespace FrontlineApp.DataLayer.Entities
{
    public class AlertSubCategory
    {
        public AlertSubCategory()
        {

        }
        [PrimaryKey]
        public int AlertSubCategoryID { get; set; }
        public string AlertSubCategoryTitle { get; set; }
        public int AlertCategoryID { get; set; } /*FK to Parent AlertCategory*/
    }
}
