﻿using SQLite.Net.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.DataLayer.Entities
{
    public class LetterAction
    {
        [PrimaryKey]
        public int id { get; set; }
        public string description { get; set; }
        public int? subId { get; set; }
    }
}
