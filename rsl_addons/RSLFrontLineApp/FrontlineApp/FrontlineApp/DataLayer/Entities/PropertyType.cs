﻿using SQLite.Net.Attributes;
using System;


namespace FrontlineApp.DataLayer.Entities
{
    public class PropertyType
    {
        public PropertyType()
        {

        }
        [PrimaryKey]
        public int id { get; set; }
        public string description { get; set; }
    }
}
