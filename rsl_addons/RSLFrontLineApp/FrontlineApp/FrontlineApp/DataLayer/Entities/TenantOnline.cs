using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
public class TenantOnline
{
	[PrimaryKey, AutoIncrement]
    public int? id { get; set; }
    public string username { get; set; }
    public string password { get; set; }

    [OneToOne(foreignKey: "tenantOnlineId", inverseProperty: "tenantOnline")]
    public Tenancy Tenancy { get; set; }
}




