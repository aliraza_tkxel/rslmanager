using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;

public class CurrentHome
{
    [PrimaryKey, AutoIncrement]
    public int? id { get; set; }
    public int? currentHomeId { get; set; }
    public int? landlordTypeId { get; set; }
    public string landlordName { get; set; }
    public int? houseTypeId { get; set; }
    public int? floor { get; set; }
    public int? noOfBedrooms { get; set; }
    public int? livingWithFamilyId { get; set; }
    public DateTimeOffset? tenancyStartDate { get; set; }
    public DateTimeOffset? tenancyEndDate { get; set; }
    public string previousLandlordName { get; set; }


    [OneToOne(foreignKey: "homeId", inverseProperty: "currentHome")]
    public Customer Customer { get; set; }

}
