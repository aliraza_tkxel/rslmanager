﻿using SQLite.Net.Attributes;

namespace FrontlineApp.DataLayer.Entities
{
    public class AidsAndAdaption
    {
        [PrimaryKey]
        public int id { get; set; }
        public string description { get; set; }
    }
}
