﻿using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.DataLayer.Entities
{
    public class ViewingComment
    {
        [PrimaryKey]
        public int CommentId { get; set; }
        public string CommentText { get; set; }
        public int? ViewingId { get; set; }
        public DateTime? CommentedDate { get; set; }
        public string CommentOwnerName { get; set; }
        public int? CommentTypeId { get; set; }
        public int? CommentedBy { get; set; }
        [ForeignKey(typeof(ViewingSummary))]
        public int viewingSerialId { get; set; }

        [ManyToOne]
        public ViewingSummary ViewingSummary { get; set; }
    }
}
