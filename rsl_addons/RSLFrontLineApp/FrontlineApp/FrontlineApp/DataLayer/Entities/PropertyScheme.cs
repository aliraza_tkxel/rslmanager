﻿using SQLite.Net.Attributes;
using System;
namespace FrontlineApp.DataLayer.Entities
{
    public class PropertyScheme
    {
        public PropertyScheme()
        {

        }
        [PrimaryKey]
        public int id { get; set; }
        public string description { get; set; }
    }
}
