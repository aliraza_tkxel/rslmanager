﻿using SQLite.Net.Attributes;

namespace FrontlineApp.DataLayer.Entities
{
    public class TerminationReasonCategory
    {
        [PrimaryKey, AutoIncrement]
        public int terminationReasonCategoryId { get; set; }
        public int id { get; set; }
        public string description { get; set; }
        public int subId { get; set; }
    }
}
