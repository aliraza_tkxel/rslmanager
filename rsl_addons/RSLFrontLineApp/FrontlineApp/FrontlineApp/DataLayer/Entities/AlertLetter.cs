﻿using SQLite.Net.Attributes;
using System;

namespace FrontlineApp.DataLayer.Entities
{
    class AlertLetter
    {
        public AlertLetter()
        {

        }
        [PrimaryKey]
        public int AlertLetterID { get; set; }
        public string AlertLetterTitle { get; set; }
        public string AlertLetterType { get; set; } /*Will be assigned to an enum. Risk or Vulnerability*/
        public string AlertLetterContent { get; set; } 
    }
}
