using SQLite.Net.Attributes;
using SQLiteNetExtensions.Attributes;
public class JointTenant
{
	[PrimaryKey, AutoIncrement]
    public int id { get; set; }
    public int tenancyId { get; set; }
	public int customerId { get; set; }
    public string startDate { get; set; }
    public bool isCorrespondingAddress { get; set; }
	
	[ForeignKey(typeof(Tenancy))]
    public int tenancy { get; set; }
	
	[ManyToOne]
    public Tenancy Tenancy { get; set; }
}
