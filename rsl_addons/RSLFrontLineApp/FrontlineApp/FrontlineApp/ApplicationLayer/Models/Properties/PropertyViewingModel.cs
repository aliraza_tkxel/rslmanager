﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ApplicationLayer.Models
{
    public class PropertyViewingModel :BaseModel
    {
        public PropertyViewingModel()
        {

        }
        private string viewingTime;
        public string ViewingTime
        {
            get { return viewingTime; }
            set { Set(() => ViewingTime, ref viewingTime, value); }
        }
        public string PropertyId { get; set; }
        public int ViewingId { get; set; }
        public int ViewingStatusId { get; set; }
        public string ViewingStatusDescription { get; set; }
        public string ViewingDateFormatted { get; set; }
        public string ViewingTimeFormatted { get; set; }
        public DateTime ViewingDate { get; set; }
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public int HousingOfficerId { get; set; }
        public string HousingOfficerName { get; set; }
    }
}
