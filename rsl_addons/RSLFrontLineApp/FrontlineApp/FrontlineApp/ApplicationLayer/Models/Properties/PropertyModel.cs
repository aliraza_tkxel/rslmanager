﻿using FrontlineApp.DataLayer.Entities;
using FrontlineApp.DataLayer.Repository;
using FrontlineApp.Utility.Constants;
using System;
using System.Threading.Tasks;

namespace FrontlineApp.ApplicationLayer.Models
{
    public class PropertyModel
    {
        public string propertyId { get; set; }
        public string propertyType { get; set; }
        public int? propertyTypeId { get; set; }
        public string scheme { get; set; }
        public int? schemeId { get; set; }
        public string bedRooms { get; set; }
        public string projectedRent { get; set; }
        public double rentNumeric { get; set; }

        public string address { get; set; }
        public string status { get; set; }
        public string subStatus { get; set; }
        public string imagePath
        {
            get
            {
                if (string.IsNullOrWhiteSpace(imageName))
                {
                    return "ms-appx:///Assets/1x/property_placeholder.png";
                }
                string baseUrl = UriTemplateConstants.BaseURL;
                baseUrl = baseUrl.Replace("/RSLFrontLineApi", "");
                return baseUrl+ "/PropertyImages/"+propertyId+ "/Images/"+imageName;
            }
        }

        public string imageName { get; set; }
        public string postalCode { get; set; }
        public PropertyModel()
        {
            
        }
       
    }
}
