﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ApplicationLayer.Models
{
    public class PropertyFilterModel
    {
        private string _postalCode;
        public string postalCode {
            get
            {
                return _postalCode;
            }
            set
            {
                _postalCode = value;
                if (string.IsNullOrEmpty(value) == false)
                {
                    isPostalCodeApplicable = true;
                }
            }
        }
        private int _schemeId;
        public int schemeId {
            get
            {
                return _schemeId;
            }
            set
            {
                _schemeId = value;
                if (value>0)
                {
                    isSchemeApplicable = true;
                }
            }
        }
        private int _propTypeId;
        public int propertyTypeId {
            get
            {
                return _propTypeId;
            }
            set
            {
                _propTypeId = value;
                if (value>0)
                {
                    isPropertyTypeApplicable = true;
                }
            }
        }
        private double _minRent;
        public double minRent {
            get
            {
                return _minRent;
            }
            set
            {
                _minRent = value;
                if (value > 0)
                {
                    isMinRentApplicable = true;
                }
            }
        }
        private double _maxRent;
        public double maxRent {
            get
            {
                return _maxRent;
            }
            set
            {
                _maxRent = value;
                if (value <10000)
                {
                    isMaxRentApplicable = true;
                }
            }
        }
        private int _bedRooms;
        public int bedRooms {
            get
            {
                return _bedRooms;
            }
            set
            {
                _bedRooms = value;
                if (value >0)
                {
                    isBedroomApplicable = true;
                }
            }
        }


        public bool isBedroomApplicable { get; set; }
        public bool isMaxRentApplicable { get; set; }
        public bool isMinRentApplicable { get; set; }
        public bool isSchemeApplicable { get; set; }
        public bool isPropertyTypeApplicable { get; set; }
        public bool isPostalCodeApplicable { get; set; }
        public bool shouldApplyFilter {
            get
            {
                if(isBedroomApplicable || isMaxRentApplicable || isMinRentApplicable || isSchemeApplicable || isPropertyTypeApplicable || isPostalCodeApplicable)
                {
                    return true;
                }
                return false;
            }
        }
        public PropertyFilterModel()
        {
            _postalCode = "";
            _schemeId = 0;
            _propTypeId = 0;
            _minRent = 0;
            _maxRent = 10000;
            _bedRooms = 0;

            isBedroomApplicable = false;
            isMaxRentApplicable = false;
            isMinRentApplicable = false;
            isSchemeApplicable = false;
            isPropertyTypeApplicable = false;
        }
    }
}
