﻿using System.Threading.Tasks;
using FrontlineApp.DataLayer.Entities;
using FrontlineApp.DataLayer.Repository;
using FrontlineApp.Utility.Constants;

namespace FrontlineApp.ApplicationLayer.Models
{
    public class PropertyDetailsModel: BaseModel
    {
        private string _propertyId;
        public string propertyId
        {
            get { return _propertyId; }
            set { Set(() => propertyId, ref _propertyId, value); }
        }

        private string _propertyTypeDescription;
        public string propertyTypeDescription
        {
            get { return _propertyTypeDescription; }
            set { Set(() => propertyTypeDescription, ref _propertyTypeDescription, value); }
        }

        private int _propertyTypeId;
        public int propertyTypeId
        {
            get { return _propertyTypeId; }
            set { Set(() => propertyTypeId, ref _propertyTypeId, value); }
        }


        private string _developmentType;
        public string developmentType
        {
            get { return _developmentType; }
            set { Set(() => developmentType, ref _developmentType, value); }
        }

        private string _schemeDescription;
        public string schemeDescription
        {
            get { return _schemeDescription; }
            set { Set(() => schemeDescription, ref _schemeDescription, value); }
        }

        private int? _schemeId;
        public int? schemeId
        {
            get { return _schemeId; }
            set { Set(() => schemeId, ref _schemeId, value); }
        }

        private string _phase;
        public string phase
        {
            get { return _phase; }
            set { Set(() => phase, ref _phase, value); }
        }

        private string _block;
        public string block
        {
            get { return _block; }
            set { Set(() => block, ref _block, value); }
        }

        private string _houseNo;
        public string houseNo
        {
            get { return _houseNo; }
            set { Set(() => houseNo, ref _houseNo, value); }
        }

        private string _address;
        public string address
        {
            get { return _address; }
            set { Set(() => address, ref _address, value); }
        }

        private string _townCity;
        public string townCity
        {
            get { return _townCity; }
            set { Set(() => townCity, ref _townCity, value); }
        }

        private string _county;
        public string county
        {
            get { return _county; }
            set { Set(() => county, ref _county, value); }
        }

        private string _postalCode;
        public string postalCode
        {
            get { return _postalCode; }
            set { Set(() => postalCode, ref _postalCode, value); }
        }

        private string _rent;
        public string rent
        {
            get { return _rent; }
            set { Set(() => rent, ref _rent, value); }
        }

        private string _status;
        public string status
        {
            get { return _status; }
            set { Set(() => status, ref _status, value); }
        }

        private string _rightToAcquire;
        public string rightToAcquire
        {
            get { return _rightToAcquire; }
            set { Set(() => rightToAcquire, ref _rightToAcquire, value); }
        }


        private string _assetType;
        public string assetType
        {
            get { return _assetType; }
            set { Set(() => assetType, ref _assetType, value); }
        }

        private string _floorArea;
        public string floorArea
        {
            get { return _floorArea; }
            set { Set(() => floorArea, ref _floorArea, value); }
        }

        private string _imagePath;
        public string imagePath
        {
            get { return _imagePath; }
            set { Set(() => imagePath, ref _imagePath, value); }
        }

        private string _imageName;
        public string imageName
        {
            get { return _imageName; }
            set { Set(() => imageName, ref _imageName, value); SetValueForImagePath(); }
        }

        private string _maxPeople;
        public string maxPeople
        {
            get { return _maxPeople; }
            set { Set(() => maxPeople, ref _maxPeople, value); }
        }

        private string _numberOfLivingRooms;
        public string numberOfLivingRooms
        {
            get { return _numberOfLivingRooms; }
            set { Set(() => numberOfLivingRooms, ref _numberOfLivingRooms, value); }
        }

        private string _numberOfBedrooms;
        public string numberOfBedRooms
        {
            get { return _numberOfBedrooms; }
            set { Set(() => numberOfBedRooms, ref _numberOfBedrooms, value); }
        }

        private string _numberOfBathrooms;
        public string numberOfBathRooms
        {
            get { return _numberOfBathrooms; }
            set { Set(() => numberOfBathRooms, ref _numberOfBathrooms, value); }
        }

        private string _numberOfKitchen;
        public string numberOfkitchen
        {
            get { return _numberOfKitchen; }
            set { Set(() => numberOfkitchen, ref _numberOfKitchen, value); }
        }

        private string _parking;
        public string parking
        {
            get { return _parking; }
            set { Set(() => parking, ref _parking, value); }
        }

        private string _garden;
        public string garden
        {
            get { return _garden; }
            set { Set(() => garden, ref _garden, value); }
        }

        private string _garage;
        public string garage
        {
            get { return _garage; }
            set { Set(() => garage, ref _garage, value); }
        }

        private string _heatingFuel;
        public string heatingFuel
        {
            get { return _heatingFuel; }
            set { Set(() => heatingFuel, ref _heatingFuel, value); }
        }

        private string _boilerType;
        public string boilerType
        {
            get { return _boilerType; }
            set { Set(() => boilerType, ref _boilerType, value); }
        }

        private string _wheelChairAccess;
        public string wheelChairAccess
        {
            get { return _wheelChairAccess; }
            set { Set(() => wheelChairAccess, ref _wheelChairAccess, value); }
        }

        private string _completeAddress;
        public string completeAddress
        {
            get { return houseNo + ", " + address + ", " + townCity + ", " + postalCode; }
            set { Set(() => completeAddress, ref _completeAddress, value); }
        }
        
        public PropertyDetailsModel()
        {

        }
        
        public void SetValueForImagePath()
        {
            if (string.IsNullOrWhiteSpace(imageName))
            {
                imagePath= "ms-appx:///Assets/2x/property_placeholder.png";
            }
            string baseUrl = UriTemplateConstants.BaseURL;
            baseUrl = baseUrl.Replace("/RSLFrontLineApi", "");
            imagePath = baseUrl + "/PropertyImages/" + propertyId + "/Images/" + imageName;
        }
    }
}
