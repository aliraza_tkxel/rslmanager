﻿
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

using System.Linq;

namespace FrontlineApp.ApplicationLayer.Models
{
    public class StaffMemberTempModel
    {
        private static Random random = new Random();

        #region Properties

        private string internetAccess;

        public string InternetAccess
        {
            get { return internetAccess; }
            set { internetAccess = value; }
        }

        private string ethnicity;

        public string Ethnicity
        {
            get { return ethnicity; }
            set { ethnicity = value; }
        }

        private string religion;

        public string Religion
        {
            get { return religion; }
            set { religion = value; }
        }

        private string sexualOrientation;

        public string SexualOrientation
        {
            get { return sexualOrientation; }
            set { sexualOrientation = value; }
        }

        private string niNumber;

        public string NiNumber
        {
            get { return niNumber; }
            set { niNumber = value; }
        }

        private string nationality;

        public string Nationality
        {
            get { return nationality; }
            set { nationality = value; }
        }

        private string subjectToImmigration;

        public string SubjectToImmigration
        {
            get { return subjectToImmigration; }
            set { subjectToImmigration = value; }
        }

        private string residencyStatus;

        public string ResidencyStatus
        {
            get { return residencyStatus; }
            set { residencyStatus = value; }
        }

        private string language;

        public string Language
        {
            get { return language; }
            set { language = value; }
        }

        private string preferredContact;

        public string PreferredContact
        {
            get { return preferredContact; }
            set { preferredContact = value; }
        }


        private string landlord;

        public string Landlord
        {
            get { return landlord; }
            set { landlord = value; }
        }

        private string landlordName;

        public string LandlordName
        {
            get { return landlordName; }
            set { landlordName = value; }
        }

        private string currentAddress;

        public string CurrentAddress
        {
            get { return currentAddress; }
            set { currentAddress = value; }
        }

        private string typeOfHouse;

        public string TypeOfHouse
        {
            get { return typeOfHouse; }
            set { typeOfHouse = value; }
        }

        private string livingWith;

        public string LivingWith
        {
            get { return livingWith; }
            set { livingWith = value; }
        }

        private string numberOfBedroom;

        public string NumberOfBedroom
        {
            get { return numberOfBedroom; }
            set { numberOfBedroom = value; }
        }

        private string currencyTenancyStart;

        public string CurrencyTenancyStart
        {
            get { return currencyTenancyStart; }
            set { currencyTenancyStart = value; }
        }

        private string currentTenancyEnd;

        public string CurrentTenancyEnd
        {
            get { return currentTenancyEnd; }
            set { currentTenancyEnd = value; }
        }

        private string currentTenancyStart;

        public string CurrentTenancyStart
        {
            get { return currentTenancyStart; }
            set { currentTenancyStart = value; }
        }


        private string previousLandlord;

        public string PreviousLandlord
        {
            get { return previousLandlord; }
            set { previousLandlord = value; }
        }


        private string email;

        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        private string telephone;

        public string Telephone
        {
            get { return telephone; }
            set { telephone = value; }
        }

        private string mobile;

        public string Mobile
        {
            get { return mobile; }
            set { mobile = value; }
        }

        private string gender;

        public string Gender
        {
            get { return gender; }
            set { gender = value; }
        }

        private string maritalStatus;

        public string MaritalStatus
        {
            get { return maritalStatus; }
            set { maritalStatus = value; }
        }


        private string address;

        public string Address
        {
            get { return address; }
            set { address = value; }
        }

        private string initials;
        public string Initials
        {
            get
            {
                if (initials == string.Empty && FirstName != string.Empty && LastName != string.Empty)
                {
                    initials = FirstName[0].ToString() + LastName[0].ToString();
                }
                return initials;
            }
        }
        private string name;
        public string Name
        {
            get
            {
                if (name == string.Empty && FirstName != string.Empty && LastName != string.Empty)
                {
                    name = FirstName + " " + LastName;
                }
                return name;
            }
        }

        private string lastName;
        public string LastName
        {
            get
            {
                return lastName;
            }
            set
            {
                lastName = value;
                initials = string.Empty; // force to recalculate the value 
                name = string.Empty; // force to recalculate the value 
            }
        }

        private string firstName;
        public string FirstName
        {
            get
            {
                return firstName;
            }
            set
            {
                firstName = value;
                initials = string.Empty; // force to recalculate the value 
                name = string.Empty; // force to recalculate the value 
            }
        }
        public string Position { get; set; }
        public string PhoneNumber { get; set; }
        public string Biography { get; set; }

        private DateTime dateOfBirth;

        public DateTime DateOfBirth
        {
            get { return dateOfBirth; }
            set { dateOfBirth = value; }
        }

        private string imageSource;

        public string ImageSource
        {
            get { return imageSource; }
            set { imageSource = value; }
        }

        private int myVar;

        public int MyProperty
        {
            get { return myVar; }
            set { myVar = value; }
        }



        #endregion

        public StaffMemberTempModel()
        {
            // default values for each property.
            initials = string.Empty;
            name = string.Empty;
            LastName = string.Empty;
            FirstName = string.Empty;
            Position = string.Empty;
            PhoneNumber = string.Empty;
            Biography = string.Empty;

        }

        #region Public Methods
        public static StaffMemberTempModel GetNewContact()
        {
            return new StaffMemberTempModel()
            {
                FirstName = GenerateFirstName(),
                LastName = GenerateLastName(),
                Biography = GetBiography(),
                PhoneNumber = GeneratePhoneNumber(),
                DateOfBirth = DateTime.Now,
                ImageSource = GenerateImage(),
                Telephone = "+447895654123",
                Mobile = "+447895654166",
                Email = "c.adams@gmail.com",
                InternetAccess = "At home, At work",
                Gender = "Male",
                MaritalStatus = "Married",
                Ethnicity = "White British",
                Religion = "Church of England",
                SexualOrientation = "Prefer not to say",
                NiNumber = "N/A",
                Nationality = "UK National",
                SubjectToImmigration = "Yes",
                ResidencyStatus = "Permanent UK Resident",
                Language = "British England",
                PreferredContact = "Email",
                Landlord = "Private",
                LandlordName = "Jaremy Barnes",
                CurrentAddress = "18, B-33, London, England",
                TypeOfHouse = "Flat",
                LivingWith = "Family",
                NumberOfBedroom = "4",
                CurrentTenancyStart = "July 15, 2015",
                CurrentTenancyEnd = "Apr 15, 2016",
                PreviousLandlord = "Yes"
            };
        }
        public static ObservableCollection<StaffMemberTempModel> GetContacts(int numberOfContacts)
        {
            ObservableCollection<StaffMemberTempModel> contacts = new ObservableCollection<StaffMemberTempModel>();

            for (int i = 0; i < numberOfContacts; i++)
            {
                contacts.Add(GetNewContact());
            }
            return contacts;
        }
        public static ObservableCollection<GroupInfoListModel> GetContactsGrouped(int numberOfContacts)
        {
            ObservableCollection<GroupInfoListModel> groups = new ObservableCollection<GroupInfoListModel>();

            var query = from item in GetContacts(numberOfContacts)
                        group item by item.FirstName[0].ToString() into g
                        orderby g.Key
                        select new { GroupName = g.Key, Items = g };

            foreach (var g in query)
            {
                GroupInfoListModel info = new GroupInfoListModel();
                info.Key = g.GroupName;
                foreach (var item in g.Items)
                {
                    info.Add(item);
                }
                groups.Add(info);
            }

            return groups;
        }

        public override string ToString()
        {
            return Name;
        }
        #endregion

        #region Helpers

        private static string GenerateImage()
        {
            List<string> positions = new List<string>() { "test1.jpg", "test2.jpg", "test3.jpg", null };
            return String.Format("ms-appx:///Assets/1x/{0}", positions[random.Next(0, positions.Count)]);
        }

        private static string GetBiography()
        {
            List<string> biographies = new List<string>()
            {
                @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer id facilisis lectus. Cras nec convallis ante, quis pulvinar tellus. Integer dictum accumsan pulvinar. Pellentesque eget enim sodales sapien vestibulum consequat.",
                @"Maecenas eu sapien ac urna aliquam dictum.",
                @"Nullam eget mattis metus. Donec pharetra, tellus in mattis tincidunt, magna ipsum gravida nibh, vitae lobortis ante odio vel quam.",
                @"Quisque accumsan pretium ligula in faucibus. Mauris sollicitudin augue vitae lorem cursus condimentum quis ac mauris. Pellentesque quis turpis non nunc pretium sagittis. Nulla facilisi. Maecenas eu lectus ante. Proin eleifend vel lectus non tincidunt. Fusce condimentum luctus nisi, in elementum ante tincidunt nec.",
                @"Aenean in nisl at elit venenatis blandit ut vitae lectus. Praesent in sollicitudin nunc. Pellentesque justo augue, pretium at sem lacinia, scelerisque semper erat. Ut cursus tortor at metus lacinia dapibus.",
                @"Ut consequat magna luctus justo egestas vehicula. Integer pharetra risus libero, et posuere justo mattis et.",
                @"Proin malesuada, libero vitae aliquam venenatis, diam est faucibus felis, vitae efficitur erat nunc non mauris. Suspendisse at sodales erat.",
                @"Aenean vulputate, turpis non tincidunt ornare, metus est sagittis erat, id lobortis orci odio eget quam. Suspendisse ex purus, lobortis quis suscipit a, volutpat vitae turpis.",
                @"Duis facilisis, quam ut laoreet commodo, elit ex aliquet massa, non varius tellus lectus et nunc. Donec vitae risus ut ante pretium semper. Phasellus consectetur volutpat orci, eu dapibus turpis. Fusce varius sapien eu mattis pharetra.",
                @"Nam vulputate eu erat ornare blandit. Proin eget lacinia erat. Praesent nisl lectus, pretium eget leo et, dapibus dapibus velit. Integer at bibendum mi, et fringilla sem."
            };
            return biographies[random.Next(0, biographies.Count)];
        }

        private static string GeneratePhoneNumber()
        {
            return string.Format("{0:(###)} {1:###}-{2:####}", random.Next(100, 999), random.Next(100, 999), random.Next(1000, 9999));
        }
        private static string GenerateFirstName()
        {
            List<string> names = new List<string>() { "Lilly", "Mukhtar", "Sophie", "Femke", "Abdul-Rafi'", "Chirag-ud-D...", "Mariana", "Aarif", "Sara", "Ibadah", "Fakhr", "Ilene", "Sardar", "Hanna", "Julie", "Iain", "Natalia", "Henrik", "Rasa", "Quentin", "Gadi", "Pernille", "Ishtar", "Jimme", "Justina", "Lale", "Elize", "Rand", "Roshanara", "Rajab", "Bijou", "Marcus", "Marcus", "Alima", "Francisco", "Thaqib", "Andreas", "Mariana", "Amalie", "Rodney", "Dena", "Fadl", "Ammar", "Anna", "Nasreen", "Reem", "Tomas", "Filipa", "Frank", "Bari'ah", "Parvaiz", "Jibran", "Tomas", "Elli", "Carlos", "Diego", "Henrik", "Aruna", "Vahid", "Eliana", "Roxane", "Amanda", "Ingrid", "Wander", "Malika", "Basim", "Eisa", "Alina", "Andreas", "Deeba", "Diya", "Parveen", "Bakr", "Celine", "Bakr", "Marcus", "Daniel", "Mathea", "Edmee", "Hedda", "Maria", "Maja", "Alhasan", "Alina", "Hedda", "Victor", "Aaftab", "Guilherme", "Maria", "Kai", "Sabien", "Abdel", "Fadl", "Bahaar", "Vasco", "Jibran", "Parsa", "Catalina", "Fouad", "Colette" };
            return names[random.Next(0, names.Count)];
        }
        private static string GenerateLastName()
        {
            List<string> lastnames = new List<string>() { "Carlson", "Attia", "Quint", "Hollenberg", "Khoury", "Araujo", "Hakimi", "Seegers", "Abadi", "al", "Krommenhoek", "Siavashi", "Kvistad", "Sjo", "Vanderslik", "Fernandes", "Dehli", "Sheibani", "Laamers", "Batlouni", "Lyngvær", "Oveisi", "Veenhuizen", "Gardenier", "Siavashi", "Mutlu", "Karzai", "Mousavi", "Natsheh", "Seegers", "Nevland", "Lægreid", "Bishara", "Cunha", "Hotaki", "Kyvik", "Cardoso", "Pilskog", "Pennekamp", "Nuijten", "Bettar", "Borsboom", "Skistad", "Asef", "Sayegh", "Sousa", "Medeiros", "Kregel", "Shamoun", "Behzadi", "Kuzbari", "Ferreira", "Van", "Barros", "Fernandes", "Formo", "Nolet", "Shahrestaani", "Correla", "Amiri", "Sousa", "Fretheim", "Van", "Hamade", "Baba", "Mustafa", "Bishara", "Formo", "Hemmati", "Nader", "Hatami", "Natsheh", "Langen", "Maloof", "Berger", "Ostrem", "Bardsen", "Kramer", "Bekken", "Salcedo", "Holter", "Nader", "Bettar", "Georgsen", "Cunha", "Zardooz", "Araujo", "Batalha", "Antunes", "Vanderhoorn", "Nader", "Abadi", "Siavashi", "Montes", "Sherzai", "Vanderschans", "Neves", "Sarraf", "Kuiters" };
            return lastnames[random.Next(0, lastnames.Count)];
        }
        #endregion

    }
}
