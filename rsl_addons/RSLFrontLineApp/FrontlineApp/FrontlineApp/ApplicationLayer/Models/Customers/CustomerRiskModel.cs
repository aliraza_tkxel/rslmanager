﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ApplicationLayer.Models

{
    public class CustomerRiskModel : BaseModel
    {
        private int? riskHistoryId;
        public int? RiskHistoryId
        {
            get { return riskHistoryId; }
            set { Set(() => RiskHistoryId, ref riskHistoryId, value); }
        }


        private int? customerId;
        public int? CustomerId
        {
            get { return customerId; }
            set { Set(() => CustomerId, ref customerId, value); }
        }


        private string customerName;
        public string CustomerName
        {
            get { return customerName; }
            set { Set(() => CustomerName, ref customerName, value); }
        }


        private int? tenancyId;
        public int? TenancyId
        {
            get { return tenancyId; }
            set { Set(() => TenancyId, ref tenancyId, value); }
        }


        private int? journalId;
        public int? JournalId
        {
            get { return journalId; }
            set { Set(() => JournalId, ref journalId, value); }
        }


        private string title;
        public string Title
        {
            get { return title; }
            set { Set(() => Title, ref title, value); }
        }


        private int? teamId;
        public int? TeamId
        {
            get { return teamId; }
            set { Set(() => TeamId, ref teamId, value); }
        }


        private string team;
        public string Team
        {
            get { return team; }
            set { Set(() => Team, ref team, value); }
        }


        private int? assignedTo;
        public int? AssignedTo
        {
            get { return assignedTo; }
            set { Set(() => AssignedTo, ref assignedTo, value); }
        }


        private string assignee;
        public string Assignee
        {
            get { return assignee; }
            set { Set(() => Assignee, ref assignee, value); }
        }


        private DateTimeOffset? startDate;
        public DateTimeOffset? StartDate
        {
            get { return startDate; }
            set { Set(() => StartDate, ref startDate, value); }
        }


        private string customerRiskSubcategories;
        public string CustomerRiskSubcategories
        {
            get { return customerRiskSubcategories; }
            set { Set(() => CustomerRiskSubcategories, ref customerRiskSubcategories, value); }
        }


        private List<string> riskCategorySubCategoryList;
        public List<string> RiskCategorySubCategoryList
        {
            get { return riskCategorySubCategoryList; }
            set { Set(() => RiskCategorySubCategoryList, ref riskCategorySubCategoryList, value); }
        }

        private string notes;
        public string Notes
        {
            get { return notes; }
            set { Set(() => Notes, ref notes, value); }
        }


        private DateTimeOffset? reviewDate;
        public DateTimeOffset? ReviewDate
        {
            get { return reviewDate; }
            set { Set(() => ReviewDate, ref reviewDate, value); }
        }


        private bool? isCustomerAware;
        public bool? IsCustomerAware
        {
            get { return isCustomerAware; }
            set { Set(() => IsCustomerAware, ref isCustomerAware, value); }
        }


        private bool? isTenantOnline;
        public bool? IsTenantOnline
        {
            get { return isTenantOnline; }
            set { Set(() => IsTenantOnline, ref isTenantOnline, value); }
        }


        private int? actionId;
        public int? ActionId
        {
            get { return actionId; }
            set { Set(() => ActionId, ref actionId, value); }
        }

        private string action;
        public string Action
        {
            get { return action; }
            set { Set(() => Action, ref action, value); }
        }


        private int? letterId;
        public int? LetterId
        {
            get { return letterId; }
            set { Set(() => LetterId, ref letterId, value); }
        }

        private string letter;
        public string Letter
        {
            get { return letter; }
            set { Set(() => Letter, ref letter, value); }
        }


        private int? lastActionUserId;
        public int? LastActionUserId
        {
            get { return lastActionUserId; }
            set { Set(() => LastActionUserId, ref lastActionUserId, value); }
        }


        private DateTimeOffset? lastActionDate;
        public DateTimeOffset? LastActionDate
        {
            get { return lastActionDate; }
            set { Set(() => LastActionDate, ref lastActionDate, value); }
        }


        private int? statusId;
        public int? StatusId
        {
            get { return statusId; }
            set { Set(() => StatusId, ref statusId, value); }
        }

        private string status;
        public string Status
        {
            get { return status; }
            set { Set(() => Status, ref status, value); }
        }
    }
}
