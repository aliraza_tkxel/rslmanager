﻿using System.Collections.Generic;

namespace FrontlineApp.ApplicationLayer.Models
{
    public class GroupInfoListModel : List<object>
    {
        public string Key { get; set; }
    }
}
