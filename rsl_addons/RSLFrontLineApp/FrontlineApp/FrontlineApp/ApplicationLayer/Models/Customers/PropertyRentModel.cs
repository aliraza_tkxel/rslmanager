﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ApplicationLayer.Models
{
    public class PropertyRentModel : BaseModel
    {

        private decimal? rentPayable;
        public decimal? RentPayable
        {
            get { return rentPayable; }
            set { Set(() => RentPayable, ref rentPayable, value); }
        }

        private decimal? rent;
        public decimal? Rent
        {
            get { return rent; }
            set { Set(() => Rent, ref rent, value); RentPayable = PayableRentDynamic; }
        }

        private decimal? services;
        public decimal? Services
        {
            get { return services; }
            set { Set(() => Services, ref services, value); RentPayable = PayableRentDynamic; }
        }

        private decimal? councilTax;
        public decimal? CouncilTax
        {
            get { return councilTax; }
            set { Set(() => CouncilTax, ref councilTax, value); RentPayable = PayableRentDynamic; }
        }

        private decimal? waterRates;
        public decimal? WaterRates
        {
            get { return waterRates; }
            set { Set(() => WaterRates, ref waterRates, value); RentPayable = PayableRentDynamic; }
        }

        private decimal? ineligServ;
        public decimal? IneligServ
{
            get { return ineligServ; }
            set { Set(() => IneligServ, ref ineligServ, value); RentPayable = PayableRentDynamic;}
        }

        private decimal? supportedServices;
        public decimal? SupportedServices
        {
            get { return supportedServices; }
            set { Set(() => SupportedServices, ref supportedServices, value); RentPayable = PayableRentDynamic; }
        }

        private decimal? garage;
        public decimal? Garage
        {
            get { return garage; }
            set { Set(() => Garage, ref garage, value); RentPayable = PayableRentDynamic; }
        }
        
        public decimal? PayableRentDynamic
        {
            get { return GetSafeDecimal(Garage) + GetSafeDecimal(SupportedServices) + GetSafeDecimal(IneligServ) + GetSafeDecimal(WaterRates)  + GetSafeDecimal(CouncilTax) + GetSafeDecimal(Services) + GetSafeDecimal(Rent) ; }
        }

        public decimal? GetSafeDecimal(decimal? dec)
        {
            decimal? safedec = (dec!=null) ? dec : 0;
            return safedec;
        }
        

    }
}
