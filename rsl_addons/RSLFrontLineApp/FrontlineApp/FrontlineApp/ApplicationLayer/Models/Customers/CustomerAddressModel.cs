﻿using MvvmValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FrontlineApp.ApplicationLayer.Models
{
    public class CustomerAddressModel : BaseModel
    {
        #region "Constructor"
        public CustomerAddressModel()
        {
            ConfigureValidationRules();
        }
        #endregion

        #region "Properties"

        private int? id;
        public int? Id
        {
            get { return id; }
            set { Set(() => Id, ref id, value); }
        }

        private int? addressId;
        public int? AddressId
        {
            get { return addressId; }
            set { Set(() => AddressId, ref addressId, value); }
        }

        private int? addressTypeId;
        public int? AddressTypeId
        {
            get { return addressTypeId; }
            set { Set(() => AddressTypeId, ref addressTypeId, value); }
        }

        private string addressType;
        public string AddressType
        {
            get { return addressType; }
            set { Set(() => AddressType, ref addressType, value); }
        }

        private string contactName;
        public string ContactName
        {
            get { return contactName; }
            set { Set(() => ContactName, ref contactName, value); }
        }

        private string telephone;
        public string Telephone
        {
            get { return telephone; }
            set { Set(() => Telephone, ref telephone, value); }
        }

        private string mobile;
        public string Mobile
        {
            get { return mobile; }
            set { Set(() => Mobile, ref mobile, value); }
        }

        private string email;
        public string Email
        {
            get { return email; }
            set { Set(() => Email, ref email, value); }
        }

        private string houseNumber;
        public string HouseNumber
        {
            get { return houseNumber; }
            set { Set(() => HouseNumber, ref houseNumber, value); }
        }

        private string address1;
        public string Address1
        {
            get { return address1; }
            set { Set(() => Address1, ref address1, value); }
        }

        private string address2;
        public string Address2
        {
            get { return address2; }
            set { Set(() => Address2, ref address2, value); }
        }

        private string address3;
        public string Address3
        {
            get { return address3; }
            set { Set(() => Address3, ref address3, value); }
        }

        private string city;
        public string City
        {
            get { return city; }
            set { Set(() => City, ref city, value); }
        }

        private string county;
        public string County
        {
            get { return county; }
            set { Set(() => County, ref county, value); }
        }

        private string postCode;
        public string PostCode
        {
            get { return postCode; }
            set { Set(() => PostCode, ref postCode, value); }
        }

        private int? isDefault;
        public int? IsDefault
        {
            get { return isDefault; }
            set { Set(() => IsDefault, ref isDefault, value); }
        }

        private string completeAddress;
        public string CompleteAddress
        {
            get { return completeAddress; }
            set { Set(() => CompleteAddress, ref completeAddress, value); }
        }

        #endregion

        #region "Configure Validation Rules"
        private void ConfigureValidationRules()
        {
            Validator.AddRule(nameof(Telephone),
                            () =>
                            {
                                if (
                                    (string.IsNullOrEmpty(Telephone) || string.IsNullOrWhiteSpace(Telephone)) &&
                                    (string.IsNullOrEmpty(Email) || string.IsNullOrWhiteSpace(Email)) &&
                                    (string.IsNullOrEmpty(Mobile) || string.IsNullOrWhiteSpace(Mobile))
                                    )
                                {
                                    return RuleResult.Invalid("Add atleast one contact detail");
                                }
                                else if (!string.IsNullOrEmpty(Telephone) && !string.IsNullOrWhiteSpace(Telephone))
                                {
                                    Regex noSpecialCharacterRegex = new Regex(@"^[0-9 ]*$");
                                    bool isValid = noSpecialCharacterRegex.IsMatch(Telephone);
                                    return isValid ? RuleResult.Valid() : RuleResult.Invalid("Telephone is invalid ");
                                }

                                return RuleResult.Valid();
                            });

            Validator.AddRule(nameof(Mobile),
                            () =>
                            {
                                if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrWhiteSpace(Mobile))
                                {
                                    Regex noSpecialCharacterRegex = new Regex(@"^[0-9 ]*$");
                                    bool isValid = noSpecialCharacterRegex.IsMatch(Mobile);
                                    return isValid ? RuleResult.Valid() : RuleResult.Invalid("Mobile is invalid");
                                }

                                return RuleResult.Valid();
                            });

            Validator.AddRule(nameof(Email),
                            () =>
                            {
                                if (!string.IsNullOrEmpty(Email) && !string.IsNullOrWhiteSpace(Email))
                                {
                                    Regex emailRegexPattern = new Regex(@"^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$");
                                    bool isValid = emailRegexPattern.IsMatch(Email);
                                    return isValid ? RuleResult.Valid() : RuleResult.Invalid("Email is invalid");
                                }
                                return RuleResult.Valid();
                            });

            Validator.AddRule(nameof(PostCode),
                () =>
                {
                    if (!string.IsNullOrEmpty(PostCode) && !string.IsNullOrWhiteSpace(PostCode))
                    {
                        
                        Regex postCodeRegex = new Regex(@"^[a-zA-Z]{1,2}[0-9][0-9A-Za-z]{0,1} {0,1}[0-9][A-Za-z]{2}$");
                        bool isValid = postCodeRegex.IsMatch(PostCode);
                        return isValid ? RuleResult.Valid() : RuleResult.Invalid("Postal code is invalid");
                    }

                    return RuleResult.Valid();
                });

        }

        #endregion

    }
}
