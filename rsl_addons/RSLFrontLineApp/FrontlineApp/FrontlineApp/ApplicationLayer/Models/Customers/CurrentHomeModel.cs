﻿using System;
using MvvmValidation;
using System.Text.RegularExpressions;
using FrontlineApp.BusinessLayer;
using System.Collections.Generic;
using System.Linq;
using FrontlineApp.Utility.Constants;

namespace FrontlineApp.ApplicationLayer.Models
{
    public class CurrentHomeModel :BaseModel
    {

        #region "Constructor"
        public CurrentHomeModel()
        {
            ConfigureValidationRules();
        }
        #endregion

        private int? id;
        public int? Id
        {
            get { return id; }
            set { Set(() => Id, ref id, value); }
        }

        private int? currentHomeId;
        public int? CurrentHomeId
        {
            get { return currentHomeId; }
            set { Set(() => CurrentHomeId, ref currentHomeId, value); }
        }

        private int? landlordTypeId;
        public int? LandlordTypeId
        {
            get { return landlordTypeId; }
            set { Set(() => LandlordTypeId, ref landlordTypeId, value); }
        }

        private string landlordType;
        public string LandlordType
        {
            get { return landlordType; }
            set { Set(() => LandlordType, ref landlordType, value); }
        }

        private string landlordName;
        public string LandlordName
        {
            get { return landlordName; }
            set { Set(() => LandlordName, ref landlordName, value); }
        }

        private int? houseTypeId;
        public int? HouseTypeId
        {
            get { return houseTypeId; }
            set {
                Set(() => HouseTypeId, ref houseTypeId, value);
                SetupFloorState();
            }
        }

        private string houseType;
        public string HouseType
        {
            get { return houseType; }
            set { Set(() => HouseType, ref houseType, value); }
        }

        private int? floor;
        public int? Floor
        {
            get { return floor; }
            set
            {
                Set(() => Floor, ref floor, value);
            }
        }

        private string floorDescription;
        public string FloorDescription
        {
            get { return floorDescription; }
            set { Set(() => FloorDescription, ref floorDescription, value); }
        }

        private bool? isFloorSelected=false;
        public bool? IsFloorSelected
        {
            get { return isFloorSelected; }
            set { Set(() => IsFloorSelected, ref isFloorSelected, value); }
        }

        private int? noOfBedrooms;
        public int? NoOfBedrooms
        {
            get { return noOfBedrooms; }
            set { Set(() => NoOfBedrooms, ref noOfBedrooms, value); }
        }

        private string livingWithFamily;
        public string LivingWithFamily
        {
            get { return livingWithFamily; }
            set { Set(() => LivingWithFamily, ref livingWithFamily, value); }
        }

        private int? livingWithFamilyId;
        public int? LivingWithFamilyId
        {
            get { return livingWithFamilyId; }
            set { Set(() => LivingWithFamilyId, ref livingWithFamilyId, value); }
        }

        private DateTimeOffset? tenancyStartDate;
        public DateTimeOffset? TenancyStartDate
        {
            get { return tenancyStartDate; }
            set { Set(() => TenancyStartDate, ref tenancyStartDate, value); }
        }

        private DateTimeOffset? tenancyEndDate;
        public DateTimeOffset? TenancyEndDate
        {
            get { return tenancyEndDate; }
            set { Set(() => TenancyEndDate, ref tenancyEndDate, value); }
        }

        private string previousLandlordName;
        public string PreviousLandlordName
        {
            get { return previousLandlordName; }
            set { Set(() => PreviousLandlordName, ref previousLandlordName, value); }
        }


        #region "Configure Validation Rules"
        private void ConfigureValidationRules()
        {
            Validator.AddRule(nameof(LandlordName),
                            () =>
                            {
                                if (!string.IsNullOrEmpty(LandlordName) && !string.IsNullOrWhiteSpace(LandlordName))
                                {
                                    Regex noSpecialCharacterRegex = new Regex(@"^[a-zA-Z0-9 _'.-]*$");
                                    bool isValid = noSpecialCharacterRegex.IsMatch(LandlordName);
                                    return isValid ? RuleResult.Valid() : RuleResult.Invalid("Special characters not allowed for Landlord Name");
                                }

                                return RuleResult.Valid();
                            });

            Validator.AddRule(nameof(TenancyEndDate),
                            () =>
                            {
                                if (TenancyStartDate == null && TenancyEndDate != null)
                                {
                                    return RuleResult.Invalid("Please add Tenancy Start Date");
                                }
                                else if (TenancyStartDate != null && TenancyEndDate != null)
                                {
                                    return TenancyStartDate > TenancyEndDate ? RuleResult.Invalid("Tenancy End Date must be greater than Tenancy Start Date") : RuleResult.Valid() ;
                                }

                                return RuleResult.Valid();
                            });

            Validator.AddRule(nameof(PreviousLandlordName),
                () =>
                {
                    if (!string.IsNullOrEmpty(PreviousLandlordName) && !string.IsNullOrWhiteSpace(PreviousLandlordName))
                    {
                        Regex noSpecialCharacterRegex = new Regex(@"^[a-zA-Z0-9 _'.-]*$");
                        bool isValid = noSpecialCharacterRegex.IsMatch(PreviousLandlordName);
                        return isValid ? RuleResult.Valid() : RuleResult.Invalid("Special characters not allowed for Previous Landlord Name");
                    }

                    return RuleResult.Valid();
                });
        }

        private void SetupFloorState()
        {
            LookupManager mngr = new LookupManager();
            List<LookupModel> houseList = mngr.GetHouseTypes();
            var floorHouseTypeId = houseList.Where(x => x.Description == ApplicationConstants.HouseTypeFlatTitle).Select(y => y.Id).FirstOrDefault();
            IsFloorSelected = floorHouseTypeId == HouseTypeId ? true : false;
            Floor = IsFloorSelected == false ? null : Floor;
        }

        #endregion
    }
}
