﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ApplicationLayer.Models
{
    public class TenantOnlineSetupModel : BaseModel
    {
        private string userName;
        public string UserName
        {
            get { return userName; }
            set { Set(() => UserName, ref userName, value); }
        }

        private string password;
        public string Password
        {
            get { return password; }
            set { Set(() => Password, ref password, value); }
        }
    }
}
