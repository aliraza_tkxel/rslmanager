﻿using System;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

namespace FrontlineApp.ApplicationLayer.Models
{
    public class PivotTitleModel: BaseModel
    {
        private string _title;
        public string title
        {
            get { return _title; }
            set { Set(() => title, ref _title, value); }
        }
        private int _index;
        public int index
        {
            get { return _index; }
            set { Set(() => index, ref _index, value); }
        }
        private int _selectedIndex;
        public int SelectedIndex
        {
            get { return _selectedIndex; }
            set
            {
                Set(() => SelectedIndex, ref _selectedIndex, value);
                IsSelected = (SelectedIndex == index) ? true : false;
            }
        }
        public BitmapImage imagePath
        {
            get { return ReconfigImagePathsFor(SelectedIndex, index); }
        }
        private bool _isSelected;
        public bool IsSelected
        {
            get { return _isSelected; }
            set { Set(() => IsSelected, ref _isSelected, value); }
        }
        public PivotTitleModel(string t, int i, int s)
        {
            title = t;
            index = i;
            SelectedIndex = s;
        }
        private BitmapImage ReconfigImagePathsFor(int selectedIndex, int ObjIndex)
        {
            var path = "ms-appx:///Assets/1x/tenancy_step_unfilled.png";
            if (selectedIndex == ObjIndex)
            {
                path = "ms-appx:///Assets/1x/tenancy_step_selected.png";
            }
            else if (selectedIndex > ObjIndex)
            {
                path = "ms-appx:///Assets/1x/tenancy_step_filled.png";
            }

            return new BitmapImage(new Uri(path, UriKind.Absolute));
        }
        
    }
}
