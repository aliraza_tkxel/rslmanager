﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ApplicationLayer.Models
{
    public class CustomerGeneralInfoModel : BaseModel
    {
        private int? id;
        public int? Id
        {
            get { return id; }
            set { Set(() => Id, ref id, value); }
        }

        private string benefits;
        public string Benefits
        {
            get { return benefits; }
            set { Set(() => Benefits, ref benefits, value); }
        }

        private string bankingFacilities;
        public string BankingFacilities
        {
            get { return bankingFacilities; }
            set { Set(() => BankingFacilities, ref bankingFacilities, value); }
        }

        private string bankingFacilityOther;
        public string BankingFacilityOther
        {
            get { return bankingFacilityOther; }
            set { Set(() => BankingFacilityOther, ref bankingFacilityOther, value); }
        }

        private string healthProblems;
        public string HealthProblems
        {
            get { return healthProblems; }
            set { Set(() => HealthProblems, ref healthProblems, value); }
        }

        private string healthProblemOthers;
        public string HealthProblemOthers
        {
            get { return healthProblemOthers; }
            set { Set(() => HealthProblemOthers, ref healthProblemOthers, value); }
        }

        private int? liveInCarer;
        public int? LiveInCarer
        {
            get { return liveInCarer; }
            set { Set(() => LiveInCarer, ref liveInCarer, value); }
        }

        private int? wheelchair;
        public int? Wheelchair
        {
            get { return wheelchair; }
            set { Set(() => Wheelchair, ref wheelchair, value); }
        }

        private string supportFromAgencies;
        public string SupportFromAgencies
        {
            get { return supportFromAgencies; }
            set { Set(() => SupportFromAgencies, ref supportFromAgencies, value); }
        }

        private string supportFromAgenciesOther;
        public string SupportFromAgenciesOther
        {
            get { return supportFromAgenciesOther; }
            set { Set(() => SupportFromAgenciesOther, ref supportFromAgenciesOther, value); }
        }

        private bool hasSupportFromAgencies;

        public bool HasSupportFromAgencies
        {
            get { return hasSupportFromAgencies; }
            set { Set(() => HasSupportFromAgencies, ref hasSupportFromAgencies, value); }
        }

        private bool hasHealthProblems;

        public bool HasHealthProblems
        {
            get { return hasHealthProblems; }
            set { Set(() => HasHealthProblems, ref hasHealthProblems, value); }
        }

        public bool DoesNotHaveHealthProblems
        {
            get
            {
                return !HasHealthProblems;
            }
        }

        public bool DoesNotHaveAgencySupport
        {
            get
            {
                return !HasSupportFromAgencies;
            }
        }

        

        private string aidsAndAdaptation;
        public string AidsAndAdaptation
        {
            get { return aidsAndAdaptation; }
            set { Set(() => AidsAndAdaptation, ref aidsAndAdaptation, value); }
        }

    }
}
