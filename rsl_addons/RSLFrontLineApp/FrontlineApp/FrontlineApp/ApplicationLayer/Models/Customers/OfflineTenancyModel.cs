﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.DataLayer.OfflineTenancyEntities;
using FrontlineApp.DataLayer.Repository.Customers;

namespace FrontlineApp.ApplicationLayer.Models
{
    public class OfflineTenancyModel: BaseModel
    {
        public PropertyModel Property { get; set; }
        public CustomerModel Customer { get; set; }

        public TenancyModel TenancyModel { get; set; }
    }
}
