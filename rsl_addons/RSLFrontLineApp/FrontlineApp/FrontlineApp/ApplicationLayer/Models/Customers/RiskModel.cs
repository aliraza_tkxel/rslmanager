﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ApplicationLayer.Models
{
    public class RiskModel
    {
        private static Random random = new Random();
        private DateTime riskDateTime;

        public DateTime RiskDateTime
        {
            get { return riskDateTime; }
            set { riskDateTime = value; }
        }

        private string team;

        public string Team
        {
            get { return team; }
            set { team = value; }
        }

        private string assignee;

        public string Assignee
        {
            get { return assignee; }
            set { assignee = value; }
        }

        private string status;

        public string Status
        {
            get { return status; }
            set { status = value; }
        }

        private string category;

        public string Category
        {
            get { return category; }
            set { category = value; }
        }

        private string subCategory;

        public string SubCategory
        {
            get { return subCategory; }
            set { subCategory = value; }
        }

        private string key;

        public string Key
        {
            get { return key; }
            set { key = value; }
        }


        public static RiskModel GetNewRisk()
        {
            DateTime riskTime = GenerateRandomDatetime();
            return new RiskModel()
            {
                RiskDateTime = riskTime,
                Assignee = GenerateHousingOfficer(),
                Team = "Frontline",
                Category = GenerateCategory(),
                SubCategory = GenerateSubCategory(),
                Status = GenerateStatus(),
                Key = riskTime > DateTime.Now ? "" : "Past Risks"
            };
        }

        public static ObservableCollection<RiskModel> GetRisks(int numberOfRisks)
        {
            ObservableCollection<RiskModel> risks = new ObservableCollection<RiskModel>();

            for (int i = 0; i < numberOfRisks; i++)
            {
                risks.Add(GetNewRisk());
            }
            return risks;
        }

        private static string GenerateStatus()
        {
            List<string> positions = new List<string>() { "Open", "In Progress"};
            return positions[random.Next(0, positions.Count)];
        }

        private static string GenerateHousingOfficer()
        {
            List<string> positions = new List<string>() { "Laura Weber", "Sean Green", "Amelia Dormer", "Natalia Johnson" };
            return positions[random.Next(0, positions.Count)];
        }

        private static string GenerateCategory()
        {
            List<string> positions = new List<string>() { "Act of Violence", "Threatening Behaviour" };
            return positions[random.Next(0, positions.Count)];
        }

        private static string GenerateSubCategory()
        {
            List<string> positions = new List<string>() { "Verbal Abuse", "Threatening another provider" };
            return positions[random.Next(0, positions.Count)];
        }

        private static DateTime GenerateRandomDatetime()
        {
            var list = new List<DateTime>();
            list.Add(new DateTime(2000, 5, 5));
            list.Add(new DateTime(2011, 10, 20));
            list.Add(new DateTime(2014, 1, 4));
            list.Add(new DateTime(2017, 6, 19));
            list.Add(new DateTime(2019, 1, 4));
            list.Add(new DateTime(2018, 6, 19));

            return list[random.Next(0, list.Count)];
        }

        public static ObservableCollection<GroupInfoListModel> GetRisksGrouped(int count)
        {
            ObservableCollection<GroupInfoListModel> groups = new ObservableCollection<GroupInfoListModel>();

            var query = from item in GetRisks(count)
                        group item by item.Key into g
                        orderby g.Key
                        select new { GroupName = g.Key, Items = g };

            foreach (var g in query)
            {
                GroupInfoListModel info = new GroupInfoListModel();
                info.Key = g.GroupName;
                foreach (var item in g.Items)
                {
                    info.Add(item);
                }
                groups.Add(info);
            }

            return groups;
        }

    }
}
