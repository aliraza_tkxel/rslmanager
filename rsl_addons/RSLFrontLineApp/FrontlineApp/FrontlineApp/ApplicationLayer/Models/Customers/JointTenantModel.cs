﻿using FrontlineApp.BusinessLayer;
using MvvmValidation;
using System;

namespace FrontlineApp.ApplicationLayer.Models
{
    public class JointTenantModel : BaseModel
    {
        public JointTenantModel()
        {
            EmergencyContact = new TenancyEmergencyContactModel();
            EmploymentInfo = new EmploymentInfoModel();
            StarteDate = DateTimeOffset.UtcNow;
            CustomerGeneralInfo = new CustomerGeneralInfoModel();
            ConfigureValidationRules();
            
        }

        public DateTimeOffset? JTDob { get; set; }
        public string JTFirstName { get; set; }
        public string JTLastName { get; set; }
        public string JTTelephone { get; set; }

        private int? tenancyId;
        public int? TenancyId
        {
            get { return tenancyId; }
            set { Set(() => TenancyId, ref tenancyId, value); }
        }

        private int? customerId;
        public int? CustomerId
        {
            get { return customerId; }
            set { Set(() => CustomerId, ref customerId, value); }
        }

        private DateTimeOffset? starteDate;
        public DateTimeOffset? StarteDate
        {
            get { return starteDate; }
            set { Set(() => StarteDate, ref starteDate, value); }
        }

        private bool isCorrespondingAddress;
        public bool IsCorrespondingAddress
        {
            get { return isCorrespondingAddress; }
            set { Set(() => IsCorrespondingAddress, ref isCorrespondingAddress, value); }
        }

        private bool isJointTenantSelected;
        public bool IsJointTenantSelected
        {
            get { return isJointTenantSelected; }
            set { Set(() => IsJointTenantSelected, ref isJointTenantSelected, value); }
        }

        private bool isJointTenant;
        public bool IsJointTenant
        {
            get { return isJointTenant; }
            set { Set(() => IsJointTenant, ref isJointTenant, value); }
        }

        private EmploymentInfoModel employmentInfo;
        public EmploymentInfoModel EmploymentInfo
        {
            get { return employmentInfo; }
            set { Set(() => EmploymentInfo, ref employmentInfo, value); }
        }

        private CustomerGeneralInfoModel customerGeneralInfo;
        public CustomerGeneralInfoModel CustomerGeneralInfo
        {
            get { return customerGeneralInfo; }
            set { Set(() => CustomerGeneralInfo, ref customerGeneralInfo, value); }
        }

        private TenancyEmergencyContactModel emergencyContact;
        public TenancyEmergencyContactModel EmergencyContact
        {
            get { return emergencyContact; }
            set { Set(() => EmergencyContact, ref emergencyContact, value); }
        }

        #region "Configure Validation Rules"
        private void ConfigureValidationRules()
        {
            Validator.AddRule(nameof(IsJointTenantSelected),
                           () =>
                           {
                               if (IsJointTenant && !IsJointTenantSelected )
                               {
                                   return RuleResult.Invalid("Please select a Joint Tenant");
                               }
                               return RuleResult.Valid();
                           });
            
            Validator.AddRule(nameof(EmergencyContact),
                            () =>
                            {
                                if (IsJointTenantSelected)
                                {
                                    ValidationResult emConRes = EmergencyContact.ValidateAll();
                                    if (!emConRes.IsValid)
                                    {
                                        return RuleResult.Invalid(emConRes.ToString());
                                    }
                                }
                                
                                
                                return RuleResult.Valid();
                            });

            Validator.AddRule(nameof(EmploymentInfo),
                            () =>
                            {
                                return RuleResult.Valid();
                            });

            Validator.AddRule(nameof(CustomerGeneralInfo),
                            () =>
                            {
                                if (IsJointTenantSelected)
                                {
                                    ResultModel<bool> res = ValidateGeneralInfo();
                                    if (!res.Success)
                                    {
                                        return RuleResult.Invalid(res.Message);
                                    }
                                }
                                
                                return RuleResult.Valid();
                                
                            });




        }
        public TenancyEmergencyContactModel GetSafeEmergencyContact()
        {
            if (EmergencyContact.CompleteAddress == null && EmergencyContact.ContactName == null
                && EmergencyContact.Telephone == null && EmergencyContact.Mobile == null
                && EmergencyContact.Email == null)
            {
                return null;
            }
            return EmergencyContact;
        }
        public EmploymentInfoModel GetSafeEmploymentInfo()
        {
            if (EmploymentInfo.EmploymentStatusId == null && EmploymentInfo.Occupation == null
                && EmploymentInfo.EmployerName == null && EmploymentInfo.EmployerAddress == null)
            {
                return null;
            }
            return EmploymentInfo;
        }
        public CustomerGeneralInfoModel GetSafeGeneralInfo()
        {
            if (CustomerGeneralInfo.Benefits == null && CustomerGeneralInfo.BankingFacilities == null
                && CustomerGeneralInfo.BankingFacilityOther == null && CustomerGeneralInfo.HealthProblems == null
                && CustomerGeneralInfo.HealthProblemOthers == null && CustomerGeneralInfo.LiveInCarer == null
                 && CustomerGeneralInfo.Wheelchair == null && CustomerGeneralInfo.SupportFromAgencies == null
                  && CustomerGeneralInfo.SupportFromAgenciesOther == null && CustomerGeneralInfo.AidsAndAdaptation == null)
            {
                return null;
            }
            return CustomerGeneralInfo;
        }
        public ResultModel<bool> ValidateGeneralInfo()
        {
            ResultModel<bool> result = new ResultModel<bool>();
            result.Success = true;
            result.Message = "";
            
            if (IsJointTenantSelected)
            {
                
                if (CustomerGeneralInfo.HasHealthProblems)
                {
                    if (string.IsNullOrEmpty(CustomerGeneralInfo.HealthProblems) && string.IsNullOrWhiteSpace(CustomerGeneralInfo.HealthProblemOthers))
                    {
                        result.Success = false;
                        result.Message = result.Message + "\r" +  "Please select health problems";
                    }
                    
                }
                if (CustomerGeneralInfo.HasSupportFromAgencies)
                {
                    if (string.IsNullOrEmpty(CustomerGeneralInfo.SupportFromAgencies) && string.IsNullOrWhiteSpace(CustomerGeneralInfo.SupportFromAgenciesOther))
                    {
                        result.Success = false;
                        result.Message = result.Message+"\r"+"Please select the agencies which support you";
                    }
                }
            }
            

            return result;
        }
        
        #endregion

    }
}
