﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ApplicationLayer.Models
{
    public class TenancyAddressModel:BaseModel
    {
        public string HouseNumber { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string PostCode { get; set; }
        public string County { get; set; }
        private string completeAddress;
        public string CompleteAddress
        {
            get { return completeAddress; }
            set { Set(() => CompleteAddress, ref completeAddress, value); }
        }
    }
}
