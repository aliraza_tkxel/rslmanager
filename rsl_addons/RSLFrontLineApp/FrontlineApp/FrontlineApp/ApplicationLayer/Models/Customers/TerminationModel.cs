﻿using MvvmValidation;
using System;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;

namespace FrontlineApp.ApplicationLayer.Models
{
    public class TerminationModel : BaseModel
    {
        #region "Properties"

        private bool willAddForwardingAddress;
        public bool WillAddForwardingAddress
        {
            get { return willAddForwardingAddress; }
            set { Set(() => WillAddForwardingAddress, ref willAddForwardingAddress, value); }
        }

        private int? terminationHistoryId;
        public int? TerminationHistoryId
        {
            get { return terminationHistoryId; }
            set { Set(() => TerminationHistoryId, ref terminationHistoryId, value); }
        }

        private int? reasonId;
        public int? ReasonId
        {
            get { return reasonId; }
            set { Set(() => ReasonId, ref reasonId, value); }
        }

        private int? customerId;
        public int? CustomerId
        {
            get { return customerId; }
            set { Set(() => CustomerId, ref customerId, value); }
        }

        private DateTimeOffset? terminationDate;
        public DateTimeOffset? TerminationDate
        {
            get { return terminationDate; }
            set { Set(() => TerminationDate, ref terminationDate, value); }
        }

        private int? reasonCategoryId;
        public int? ReasonCategoryId
        {
            get { return reasonCategoryId; }
            set { Set(() => ReasonCategoryId, ref reasonCategoryId, value); }
        }

        private string notes;
        public string Notes
        {
            get { return notes; }
            set { Set(() => Notes, ref notes, value); }
        }

        private int? terminatedBy;
        public int? TerminatedBy
        {
            get { return terminatedBy; }
            set { Set(() => TerminatedBy, ref terminatedBy, value); }
        }

        private string appVersion;
        public string AppVersion
        {
            get { return appVersion; }
            set { Set(() => AppVersion, ref appVersion, value); }
        }

        private ObservableCollection<LookupModel> filteredReasonCategoryList;
        public ObservableCollection<LookupModel> FilteredReasonCategoryList
        {
            get { return filteredReasonCategoryList; }
            set { Set(() => FilteredReasonCategoryList, ref filteredReasonCategoryList, value); }
        }

        private DateTimeOffset? tenancyStartDate;
        public DateTimeOffset? TenancyStartDate
        {
            get { return tenancyStartDate; }
            set { Set(() => TenancyStartDate, ref tenancyStartDate, value); }
        }

        private TenantForwardingAddress _forwardAddress;
        public TenantForwardingAddress ForwardAddress
        {
            get { return _forwardAddress; }
            set { Set(() => ForwardAddress, ref _forwardAddress, value); }
        }

        #endregion

        #region "Constructor"
        public TerminationModel()
        {
            ConfigureValidationRules();
            ForwardAddress = new TenantForwardingAddress();
        }
        #endregion

        #region "Configure Validation Rules"
        private void ConfigureValidationRules()
        {

            Validator.AddRule(nameof(ReasonId),
                            () =>
                            {
                                if (ReasonId == null)
                                {                                    
                                    return RuleResult.Invalid("Please select reason");
                                }

                                return RuleResult.Valid();
                            });

            Validator.AddRule(nameof(TerminationDate),
                            () =>
                            {
                                if (TerminationDate == null)
                                {
                                    return RuleResult.Invalid("Please add termination start date");
                                }
                                else if (TenancyStartDate > TerminationDate)
                                {
                                    return RuleResult.Invalid("Termination start date is before the start date of the tenancy. Please make sure the figure is greater than or equal to the tenancy start date.");
                                }
                                else
                                {
                                    return RuleResult.Valid();
                                }
                            });

            Validator.AddRule(nameof(ReasonCategoryId),
                           () =>
                           {
                               if (FilteredReasonCategoryList != null && FilteredReasonCategoryList.Count > 0 && ReasonCategoryId == null )
                               {
                                   return RuleResult.Invalid("Please select reason category");
                               }

                               return RuleResult.Valid();
                           });
            Validator.AddRule(nameof(ForwardAddress),
                           () =>
                           {
                               if (WillAddForwardingAddress)
                               {
                                   var x = ForwardAddress.ValidateAll();
                                   return (x.IsValid) ? RuleResult.Valid() : RuleResult.Invalid(x.ToString());
                               }
                               else
                               {
                                   return RuleResult.Valid();
                               }
                               
                           });


        }

        #endregion

    }
}
