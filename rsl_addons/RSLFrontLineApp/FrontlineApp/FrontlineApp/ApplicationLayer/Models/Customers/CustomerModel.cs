﻿using System;
using MvvmValidation;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Text;

namespace FrontlineApp.ApplicationLayer.Models
{
    public class CustomerModel : BaseModel
    {
        #region "Constructor"
        public CustomerModel()
        {
            ConfigureValidationRules();
        }
        #endregion

        #region "Configure Validation Rules"
        private void ConfigureValidationRules()
        {
            Validator.AddRule(nameof(FirstName),
                            () =>
                            {
                                if (string.IsNullOrEmpty(FirstName))
                                {
                                    return RuleResult.Invalid("First Name is required");
                                }
                                else if (string.IsNullOrWhiteSpace(FirstName.Trim()))
                                {
                                    return RuleResult.Invalid("First Name is required");
                                }
                                else
                                {
                                    Regex noSpecialCharacterRegex = new Regex(@"^[a-zA-Z0-9 _'.-]*$");
                                    bool isValid = noSpecialCharacterRegex.IsMatch(FirstName);
                                    return isValid ? RuleResult.Valid() : RuleResult.Invalid("Special characters not allowed for First Name");
                                }
                            });
            Validator.AddRule(nameof(MiddleName),
                            () =>
                            {
                                 if (!string.IsNullOrWhiteSpace(MiddleName))
                                {
                                    Regex noSpecialCharacterRegex = new Regex(@"^[a-zA-Z0-9 _'.-]*$");
                                    bool isValid = noSpecialCharacterRegex.IsMatch(MiddleName);
                                    return isValid ? RuleResult.Valid() : RuleResult.Invalid("Special characters not allowed for Middle Name");
                                }
                                return RuleResult.Valid();
                            });
            Validator.AddRule(nameof(LastName),
                            () =>
                            {
                                if (string.IsNullOrEmpty(LastName))
                                {
                                    return RuleResult.Invalid("Last Name is required");
                                }
                                else if (string.IsNullOrWhiteSpace(LastName.Trim()))
                                {
                                    return RuleResult.Invalid("Last Name is required");
                                }
                                else
                                {
                                    Regex noSpecialCharacterRegex = new Regex(@"^[a-zA-Z0-9 _'.-]*$");
                                    bool isValid = noSpecialCharacterRegex.IsMatch(LastName);
                                    return isValid ? RuleResult.Valid() : RuleResult.Invalid("Special characters not allowed for Last Name");
                                }
                            });

            Validator.AddRule(nameof(NiNumber),
                            () =>
                            {
                                if (!string.IsNullOrEmpty(NiNumber) && !string.IsNullOrWhiteSpace(NiNumber))
                                {
                                    Regex niNumberRegex = new Regex(@"^[A-CEGHJ-NOPR-TW-Z]{1}[A-CEGHJ-NPR-TW-Z]{1}[0-9]{6}[A-DFMI\s]{0,1}$");
                                    bool isValid = niNumberRegex.IsMatch(NiNumber);
                                    return isValid ? RuleResult.Valid() : RuleResult.Invalid("NI number is invalid ");
                                }

                                return RuleResult.Valid();
                            });
                       

        }

        #endregion

        #region "Properties"

        private int? id;
        public int? Id
        {
            get { return id; }
            set { Set(() => Id, ref id, value); }
        }

        private int? customerId;
        public int? CustomerId
        {
            get { return customerId; }
            set { Set(() => CustomerId, ref customerId, value); }
        }

        private string customerNumber;
        public string CustomerNumber
        {
            get { return customerNumber; }
            set { Set(() => CustomerNumber, ref customerNumber, value); }
        }

        private string customerType;
        public string CustomerType
        {
            get { return customerType; }
            set { Set(() => CustomerType, ref customerType, value); }
        }

        private int? customerTypeId;
        public int? CustomerTypeId
        {
            get { return customerTypeId; }
            set { Set(() => CustomerTypeId, ref customerTypeId, value); }
        }

        private int? tenancyCount;
        public int? TenancyCount
        {
            get { return tenancyCount; }
            set
            {
                Set(() => TenancyCount, ref tenancyCount, value);
            }
        }

        private bool isJointTenant;
        public bool IsJointTenant
        {
            get { return isJointTenant; }
            set { Set(() => IsJointTenant, ref isJointTenant, value);}
        }


        private int? titleId;
        public int? TitleId
        {
            get { return titleId; }
            set { Set(() => TitleId, ref titleId, value); }
        }

        private string firstName;
        public string FirstName
        {
            get
            {
                return firstName;
            }
            set
            {
                Set(() => FirstName, ref firstName, value);
                Set(() => Initials, ref initials, string.Empty);
                Set(() => Name, ref name, string.Empty);
            }
        }
        private string middleName;
        public string MiddleName
        {
            get {return middleName;}
            set {Set(() => MiddleName, ref middleName, value);}
        }
        private string lastName;
        public string LastName
        {
            get
            {
                return lastName;
            }
            set
            {
                Set(() => LastName, ref lastName, value);
                Set(() => Initials, ref initials, string.Empty);
                Set(() => Name, ref name, string.Empty);
            }
        }

        private string preferredContact;
        public string PreferredContact
        {
            get { return preferredContact; }
            set { Set(() => PreferredContact, ref preferredContact, value); }
        }

        private int? preferedContactId;
        public int? PreferedContactId
        {
            get { return preferedContactId; }
            set { Set(() => PreferedContactId, ref preferedContactId, value); }
        }

        private string internetAccess;
        public string InternetAccess
        {
            get { return internetAccess; }
            set { Set(() => InternetAccess, ref internetAccess, value); }
        }

        private string internetAccessDescription;
        public string InternetAccessDescription
        {
            get { return internetAccessDescription; }
            set { Set(() => InternetAccessDescription, ref internetAccessDescription, value); }
        }

        private DateTimeOffset? dob;
        public DateTimeOffset? Dob
        {
            get { return dob; }
            set { Set(() => Dob, ref dob, value); }
        }

        private string gender;
        public string Gender
        {
            get { return gender; }
            set { Set(() => Gender, ref gender, value); }
        }

        private string maritalStatus;
        public string MaritalStatus
        {
            get { return maritalStatus; }
            set { Set(() => MaritalStatus, ref maritalStatus, value); }
        }

        private int? maritalStatusId;
        public int? MaritalStatusId
        {
            get { return maritalStatusId; }
            set { Set(() => MaritalStatusId, ref maritalStatusId, value); }
        }

        private string ethnicity;
        public string Ethnicity
        {
            get { return ethnicity; }
            set { Set(() => Ethnicity, ref ethnicity, value); }
        }

        private int? ethnicityId;
        public int? EthnicityId
        {
            get { return ethnicityId; }
            set { Set(() => EthnicityId, ref ethnicityId, value); }
        }

        private string religion;
        public string Religion
        {
            get { return religion; }
            set { Set(() => Religion, ref religion, value); }
        }

        private int? religionId;
        public int? ReligionId
        {
            get { return religionId; }
            set { Set(() => ReligionId, ref religionId, value); }
        }

        private string sexualOrientation;
        public string SexualOrientation
        {
            get { return sexualOrientation; }
            set { Set(() => SexualOrientation, ref sexualOrientation, value); }
        }

        private int? sexualOrientationId;
        public int? SexualOrientationId
        {
            get { return sexualOrientationId; }
            set { Set(() => SexualOrientationId, ref sexualOrientationId, value); }
        }

        private string niNumber;
        public string NiNumber
        {
            get { return niNumber; }
            set { Set(() => NiNumber, ref niNumber, value); }
        }

        private int? nationalityId;
        public int? NationalityId
        {
            get { return nationalityId; }
            set { Set(() => NationalityId, ref nationalityId, value); }
        }

        private string nationality;
        public string Nationality
        {
            get { return nationality; }
            set { Set(() => Nationality, ref nationality, value); }
        }

        private string firstLanguage;
        public string FirstLanguage
        {
            get { return firstLanguage; }
            set { Set(() => FirstLanguage, ref firstLanguage, value); }
        }

        private int? isSubjectToImmigration;
        public int? IsSubjectToImmigration
        {
            get { return isSubjectToImmigration; }
            set { Set(() => IsSubjectToImmigration, ref isSubjectToImmigration, value); }
        }

        private string subjectToImmigration;
        public string SubjectToImmigration
        {
            get { return subjectToImmigration; }
            set { Set(() => SubjectToImmigration, ref subjectToImmigration, value); }
        }

        private int? isPermanentUKResident;
        public int? IsPermanentUKResident
        {
            get { return isPermanentUKResident; }
            set { Set(() => IsPermanentUKResident, ref isPermanentUKResident, value); }
        }

        private string permanentUKResident;
        public string PermanentUKResident
        {
            get { return permanentUKResident; }
            set { Set(() => PermanentUKResident, ref permanentUKResident, value); }
        }

        private string residencyStatus;
        public string ResidencyStatus
        {
            get { return residencyStatus; }
            set { Set(() => ResidencyStatus, ref residencyStatus, value); }
        }

        private string communication;
        public string Communication
        {
            get { return communication; }
            set { Set(() => Communication, ref communication, value); }
        }

        private string groupkey;
        public string GroupKey
        {
            get { return groupkey; }
            set { Set(() => GroupKey, ref groupkey, value); }
        }

        private List<string> communicationList;
        public List<string> CommunicationList
        {
            get { return communicationList; }
            set { Set(() => CommunicationList, ref communicationList, value); }
        }

        private string appVersion;
        public string AppVersion
        {
            get { return appVersion; }
            set { appVersion = value; }
        }

        private int? createdBy;
        public int? CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }

        private bool isTenant;
        public bool IsTenant
        {
            get { return isTenant; }
            set { isTenant = value; }
        }

        private DateTimeOffset? createdOnApp;
        public DateTimeOffset? CreatedOnApp
        {
            get { return createdOnApp; }
            set { createdOnApp = value; }
        }

        private DateTimeOffset? createdOnServer;
        public DateTimeOffset? CreatedOnServer
        {
            get { return createdOnServer; }
            set { createdOnServer = value; }
        }

        private DateTimeOffset? lastModifiedOnApp;
        public DateTimeOffset? LastModifiedOnApp
        {
            get { return lastModifiedOnApp; }
            set { lastModifiedOnApp = value; }
        }

        private DateTimeOffset? lastModifiedOnServer;
        public DateTimeOffset? LastModifiedOnServer
        {
            get { return lastModifiedOnServer; }
            set { lastModifiedOnServer = value; }
        }

        private bool? isSynced;
        public bool? IsSynced
        {
            get { return isSynced; }
            set { isSynced = value; }
        }

        private CurrentHomeModel currentHome;
        public CurrentHomeModel CurrentHome
        {
            get { return currentHome; }
            set { Set(() => CurrentHome, ref currentHome, value); }
        }

        private CustomerImageModel customerImage;
        public CustomerImageModel CustomerImage
        {
            get { return customerImage; }
            set { Set(() => CustomerImage, ref customerImage, value); }
        }

        private CustomerAddressModel customerAddress;
        public CustomerAddressModel CustomerAddress
        {
            get { return customerAddress; }
            set { Set(() => CustomerAddress, ref customerAddress, value); }
        }

        private List<CustomerAddressModel> customerAddresses;
        public List<CustomerAddressModel> CustomerAddresses
        {
            get { return customerAddresses; }
            set { Set(() => CustomerAddresses, ref customerAddresses, value); }
        }

        private EmploymentInfoModel employmentInfo;
        public EmploymentInfoModel EmploymentInfo
        {
            get { return employmentInfo; }
            set { Set(() => EmploymentInfo, ref employmentInfo, value); }
        }

        private ReferralModel referral;
        public ReferralModel Referral
        {
            get { return referral; }
            set { Set(() => Referral, ref referral, value); }
        }

        private TenancyModel tenancy;
        public TenancyModel Tenancy
        {
            get { return tenancy; }
            set { Set(() => Tenancy, ref tenancy, value);  }
        }

        private CustomerGeneralInfoModel customerGeneralInfo;
        public CustomerGeneralInfoModel CustomerGeneralInfo
        {
            get { return customerGeneralInfo; }
            set { Set(() => CustomerGeneralInfo, ref customerGeneralInfo, value); }
        }

        private string initials;
        public string Initials
        {
            get
            {

                StringBuilder sb = new StringBuilder();
                sb.Append(string.Empty);

                if (string.IsNullOrEmpty(FirstName) == false)
                {
                    sb.Append(FirstName[0].ToString());
                }

                if (string.IsNullOrEmpty(LastName) == false)
                {
                    sb.Append(LastName[0].ToString());
                }
                initials = sb.ToString();

                return initials;
            }
        }

        private string name;
        public string Name
        {
            get
            {
                if (name == string.Empty && FirstName != string.Empty && LastName != string.Empty)
                {
                    name = FirstName + " " + LastName;
                }
                return name;
            }
        }

        private string telephone;
        public string Telephone
        {
            get { return telephone; }
            set { Set(() => Telephone, ref telephone, value); }
        }

        /*Number of Children co-tenants*/
        private int? children;
        public int? Children
        {
            get { return children; }
            set { Set(() => Children, ref children, value); }
        }

        /*Number of Adult co-tenants*/
        private int? adults;
        public int? Adults
        {
            get { return adults; }
            set { Set(() => Adults, ref adults, value); }
        }

        #endregion
    }
}
