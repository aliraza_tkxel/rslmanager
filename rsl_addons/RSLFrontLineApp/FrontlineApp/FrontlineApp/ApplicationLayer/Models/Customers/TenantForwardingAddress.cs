﻿using MvvmValidation;
using System;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;

namespace FrontlineApp.ApplicationLayer.Models
{
    public class TenantForwardingAddress: BaseModel
    {
        #region "Properties"
        private string _number;
        public string Number
        {
            get { return _number; }
            set { Set(() => Number, ref _number, value); }
        }

        private string _address1;
        public string Address1
        {
            get { return _address1; }
            set { Set(() => Address1, ref _address1, value); }
        }

        private string _address2;
        public string Address2
        {
            get { return _address2; }
            set { Set(() => Address2, ref _address2, value); }
        }

        private string _address3;
        public string Address3
        {
            get { return _address3; }
            set { Set(() => Address3, ref _address3, value); }
        }

        private string _townCity;
        public string TownCity
        {
            get { return _townCity; }
            set { Set(() => TownCity, ref _townCity, value); }
        }

        private string _county;
        public string County
        {
            get { return _county; }
            set { Set(() => County, ref _county, value); }
        }

        private string _postCode;
        public string PostCode
        {
            get { return _postCode; }
            set { Set(() => PostCode, ref _postCode, value); }
        }

        private string _telephoneH;
        public string TelePhoneH
        {
            get { return _telephoneH; }
            set { Set(() => TelePhoneH, ref _telephoneH, value); }
        }

        private string _telephoneW;
        public string TelePhoneW
        {
            get { return _telephoneW; }
            set { Set(() => TelePhoneW, ref _telephoneW, value); }
        }

        private string _telephoneC;
        public string TelePhoneC
        {
            get { return _telephoneC; }
            set { Set(() => TelePhoneC, ref _telephoneC, value); }
        }

        private string _mobile1;
        public string Mobile1
        {
            get { return _mobile1; }
            set { Set(() => Mobile1, ref _mobile1, value); }
        }

        private string _mobile2;
        public string Mobile2
        {
            get { return _mobile2; }
            set { Set(() => Mobile2, ref _mobile2, value); }
        }

        private string _email;
        public string Email
        {
            get { return _email; }
            set { Set(() => Email, ref _email, value); }
        }

        private string _alternateContact;
        public string AlternateContact
        {
            get { return _alternateContact; }
            set { Set(() => AlternateContact, ref _alternateContact, value); }
        }

#endregion
        public TenantForwardingAddress()
        {
            ConfigureValidationRules();
        }
        public void ConfigureValidationRules()
        {
            Validator.AddRule(nameof(Number),
                           () =>
                           {
                               if (string.IsNullOrWhiteSpace(Number))
                               {
                                   return RuleResult.Invalid("Number is mandatory");
                               }
                               else
                               {
                                   Regex noSpecialCharacterRegex = new Regex(@"^[0-9 ]*$");
                                   bool isValid = noSpecialCharacterRegex.IsMatch(Number);
                                   return isValid ? RuleResult.Valid() : RuleResult.Invalid("Number is invalid ");
                               }
                           });
            Validator.AddRule(nameof(Address1),
                           () =>
                           {
                               if (string.IsNullOrWhiteSpace(Address1))
                               {
                                   return RuleResult.Invalid("Address1 is mandatory");
                               }
                               else
                               {
                                   return RuleResult.Valid();
                               }
                           });
            Validator.AddRule(nameof(TownCity),
                           () =>
                           {
                               if (string.IsNullOrWhiteSpace(TownCity))
                               {
                                   return RuleResult.Invalid("Town/City is mandatory");
                               }
                               else
                               {
                                   return RuleResult.Valid();
                               }
                           });
            Validator.AddRule(nameof(PostCode),
                           () =>
                           {
                               if (string.IsNullOrWhiteSpace(PostCode))
                               {
                                   return RuleResult.Invalid("Postal Code is mandatory");
                               }
                               else
                               {
                                   Regex noSpecialCharacterRegex = new Regex(@"^[a-zA-Z]{1,2}[0-9][0-9A-Za-z]{0,1} {0,1}[0-9][A-Za-z]{2}$");
                                   bool isValid = noSpecialCharacterRegex.IsMatch(PostCode);
                                   return isValid ? RuleResult.Valid() : RuleResult.Invalid("Postal Code is invalid ");
                               }
                           });
            Validator.AddRule(nameof(TelePhoneH),
                           () =>
                           {
                               if (!string.IsNullOrWhiteSpace(TelePhoneH))
                               {
                                   Regex noSpecialCharacterRegex = new Regex(@"^[0-9 ]*$");
                                   bool isValid = noSpecialCharacterRegex.IsMatch(TelePhoneH);
                                   return isValid ? RuleResult.Valid() : RuleResult.Invalid("Telephone(H) is invalid ");
                               }
                               return RuleResult.Valid();

                           });
            Validator.AddRule(nameof(TelePhoneW),
                          () =>
                          {
                              if (!string.IsNullOrWhiteSpace(TelePhoneW))
                              {
                                  Regex noSpecialCharacterRegex = new Regex(@"^[0-9 ]*$");
                                  bool isValid = noSpecialCharacterRegex.IsMatch(TelePhoneW);
                                  return isValid ? RuleResult.Valid() : RuleResult.Invalid("Telephone(W) is invalid ");
                              }
                              return RuleResult.Valid();

                          });
            Validator.AddRule(nameof(TelePhoneC),
                          () =>
                          {
                              if (!string.IsNullOrWhiteSpace(TelePhoneC))
                              {
                                  Regex noSpecialCharacterRegex = new Regex(@"^[0-9 ]*$");
                                  bool isValid = noSpecialCharacterRegex.IsMatch(TelePhoneC);
                                  return isValid ? RuleResult.Valid() : RuleResult.Invalid("Telephone(C) is invalid");
                              }
                              return RuleResult.Valid();

                          });
            Validator.AddRule(nameof(Mobile1),
                          () =>
                          {
                              if (!string.IsNullOrWhiteSpace(Mobile1))
                              {
                                  Regex noSpecialCharacterRegex = new Regex(@"^[0-9 ]*$");
                                  bool isValid = noSpecialCharacterRegex.IsMatch(Mobile1);
                                  return isValid ? RuleResult.Valid() : RuleResult.Invalid("Mobile 1 is invalid");
                              }
                              return RuleResult.Valid();

                          });
            Validator.AddRule(nameof(Mobile1),
                          () =>
                          {
                              if (!string.IsNullOrWhiteSpace(Mobile2))
                              {
                                  Regex noSpecialCharacterRegex = new Regex(@"^[0-9 ]*$");
                                  bool isValid = noSpecialCharacterRegex.IsMatch(Mobile2);
                                  return isValid ? RuleResult.Valid() : RuleResult.Invalid("Mobile 2 is invalid");
                              }
                              return RuleResult.Valid();

                          });
            Validator.AddRule(nameof(Email),
                         () =>
                         {
                             if (!string.IsNullOrWhiteSpace(Email))
                             {
                                 Regex noSpecialCharacterRegex = new Regex(@"^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$");
                                 bool isValid = noSpecialCharacterRegex.IsMatch(Email);
                                 return isValid ? RuleResult.Valid() : RuleResult.Invalid("Email is invalid");
                             }
                             return RuleResult.Valid();

                         });
        }
    }
}
