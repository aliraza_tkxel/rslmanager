﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ApplicationLayer.Models
{
    public class CustomerViewingModel : BaseModel
    {
        private int? id;
        public int? Id
        {
            get { return id; }
            set { Set(() => Id, ref id, value); }
        }

        private int? viewingId;
        public int? ViewingId
        {
            get { return viewingId; }
            set { Set(() => ViewingId, ref viewingId, value); }
        }

        private DateTimeOffset? viewingDateTime;
        public DateTimeOffset? ViewingDateTime
        {
            get { return viewingDateTime; }
            set { Set(() => ViewingDateTime, ref viewingDateTime, value); }
        }
        private int viewingStatusId;
        public int ViewingStatusId
        {
            get { return viewingStatusId; }
            set { Set(() => ViewingStatusId, ref viewingStatusId, value); }
        }
        private string viewingStatusDescription;
        public string ViewingStatusDescription
        {
            get { return viewingStatusDescription; }
            set { Set(() => ViewingStatusDescription, ref viewingStatusDescription, value); }
        }
        private string viewingTime;
        public string ViewingTime
        {
            get { return viewingTime; }
            set { Set(() => ViewingTime, ref viewingTime, value); }
        }
        private int? housingOfficerId;
        public int? HousingOfficerId
        {
            get { return housingOfficerId; }
            set { Set(() => HousingOfficerId, ref housingOfficerId, value); }
        }

        private string housingOfficerName;
        public string HousingOfficerName
        {
            get { return housingOfficerName; }
            set { Set(() => HousingOfficerName, ref housingOfficerName, value); }
        }

        private string propertyAddress;
        public string PropertyAddress
        {
            get { return propertyAddress; }
            set { Set(() => PropertyAddress, ref propertyAddress, value); }            
        }

        private string propertyId;
        public string PropertyId
        {
            get { return propertyId; }
            set { Set(() => PropertyId, ref propertyId, value); }
        }

        private string key;
        public string Key
        {
            get { return key; }
            set { Set(() => Key, ref key, value); }            
        }

    }
}
