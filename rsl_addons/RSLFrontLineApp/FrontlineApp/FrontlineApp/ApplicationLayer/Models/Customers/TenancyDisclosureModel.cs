﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ApplicationLayer.Models
{
    public class TenancyDisclosureModel : BaseModel
    {
        private int? tenancyId;
        public int? TenancyId
        {
            get { return tenancyId; }
            set { Set(() => TenancyId, ref tenancyId, value); }
        }

        private int? disclosureId;
        public int? DisclosureId
        {
            get { return disclosureId; }
            set { Set(() => DisclosureId, ref disclosureId, value); }
        }

        private bool? isEvicted;
        public bool? IsEvicted
        {
            get { return isEvicted; }
            set { Set(() => IsEvicted, ref isEvicted, value); }
        }
        public bool? IsNotEvicted
        {
            get { return !IsEvicted; }
        }

        private string evictedReason;
        public string EvictedReason
        {
            get { return evictedReason; }
            set { Set(() => EvictedReason, ref evictedReason, value); }
        }

        private bool? isSubjectToDebt;
        public bool? IsSubjectToDebt
        {
            get { return isSubjectToDebt; }
            set { Set(() => IsSubjectToDebt, ref isSubjectToDebt, value); }
        }
        public bool? IsNotSubjectToDebt
        {
            get { return !IsSubjectToDebt; }
        }
        private bool? isUnsatisfiedCcj;
        public bool? IsUnsatisfiedCcj
        {
            get { return isUnsatisfiedCcj; }
            set { Set(() => IsUnsatisfiedCcj, ref isUnsatisfiedCcj, value); }
        }
        public bool? IsNotUnsatisfiedCcj
        {
            get { return !IsUnsatisfiedCcj; }
        }

        private bool? hasCriminalOffense;
        public bool? HasCriminalOffense
        {
            get { return hasCriminalOffense; }
            set { Set(() => HasCriminalOffense, ref hasCriminalOffense, value); }
        }
        public bool? HasNoCriminalOffense
        {
            get { return !HasCriminalOffense; } 
        }

    }
}
