﻿using MvvmValidation;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FrontlineApp.ApplicationLayer.Models
{
    public class OccupantModel : BaseModel
    {
        public OccupantModel()
        {
            ConfigureValidationRules();
        }
        private ObservableCollection<LookupModel> titleList;
        public ObservableCollection<LookupModel> TitleList
        {
            get { return titleList; }
            set { Set(() => TitleList, ref titleList, value); }
        }

        private ObservableCollection<LookupModel> relationshipStatusList;
        public ObservableCollection<LookupModel> RelationshipStatusList
        {
            get { return relationshipStatusList; }
            set { Set(() => RelationshipStatusList, ref relationshipStatusList, value); }
        }

        private ObservableCollection<LookupModel> employmentStatusList;
        public ObservableCollection<LookupModel> EmploymentStatusList
        {
            get { return employmentStatusList; }
            set { Set(() => EmploymentStatusList, ref employmentStatusList, value); }
        }

        private ObservableCollection<LookupModel> disabilityList;
        public ObservableCollection<LookupModel> DisabilityList
        {
            get { return disabilityList; }
            set { Set(() => DisabilityList, ref disabilityList, value); }
        }

        private int? id;
        public int? Id
        {
            get { return id; }
            set { Set(() => OccupantId, ref id, value); }
        }

        private int? occupantId;
        public int? OccupantId
        {
            get { return occupantId; }
            set { Set(() => OccupantId, ref occupantId, value); }
        }

        private int? customerId;
        public int? CustomerId
        {
            get { return customerId; }
            set { Set(() => CustomerId, ref customerId, value); }
        }

        private int? titleId;
        public int? TitleId
        {
            get { return titleId; }
            set { Set(() => TitleId, ref titleId, value); }
        }

        private string firstName;
        public string FirstName
        {
            get { return firstName; }
            set { Set(() => FirstName, ref firstName, value); }
        }
        private string disabilities;
        public string Disabilities
        {
            get { return disabilities; }
            set { Set(() => Disabilities, ref disabilities, value); }
        }
        private string uiGuidId;
        public string UIGuidId
        {
            get { return uiGuidId; }
            set { Set(() => UIGuidId, ref uiGuidId, value); }
        }

        private string lastName;
        public string LastName
        {
            get { return lastName; }
            set { Set(() => LastName, ref lastName, value); }
        }

        private DateTimeOffset? dateOfBirth;
        public DateTimeOffset? DateOfBirth
        {
            get { return dateOfBirth; }
            set { Set(() => DateOfBirth, ref dateOfBirth, value); }
        }

        private int? relationShipStatusId;
        public int? RelationShipStatusId
        {
            get { return relationShipStatusId; }
            set { Set(() => RelationShipStatusId, ref relationShipStatusId, value); }
        }

        private string disabilitiesOthers;
        public string DisabilitiesOthers
        {
            get { return disabilitiesOthers; }
            set { Set(() => DisabilitiesOthers, ref disabilitiesOthers, value); }
        }

        private int? employmentStatusId;
        public int? EmploymentStatusId
        {
            get { return employmentStatusId; }
            set { Set(() => EmploymentStatusId, ref employmentStatusId, value); }
        }

        public OccupantModel Clone()
        {
            return (OccupantModel)this.MemberwiseClone();
        }

        #region "Configure Validation Rules"
        private void ConfigureValidationRules()
        {
            Validator.AddRule(nameof(FirstName),
                           () =>
                           {
                               if (string.IsNullOrWhiteSpace(FirstName))
                               {
                                   return RuleResult.Invalid("First Name is mandatory");
                               }
                               Regex noSpecialCharacterRegex = new Regex(@"^[a-zA-Z ]*$");
                               bool isValid = noSpecialCharacterRegex.IsMatch(FirstName);
                               return isValid ? RuleResult.Valid() : RuleResult.Invalid("First name can contain only alphabets and spaces");
                               
                           });

            Validator.AddRule(nameof(LastName),
                            () =>
                            {
                                if (string.IsNullOrWhiteSpace(LastName))
                                {
                                    return RuleResult.Invalid("Last Name is mandatory");
                                }
                                Regex noSpecialCharacterRegex = new Regex(@"^[a-zA-Z ]*$");
                                bool isValid = noSpecialCharacterRegex.IsMatch(LastName);
                                return isValid ? RuleResult.Valid() : RuleResult.Invalid("Last name can contain only alphabets and spaces");
                            });

            Validator.AddRule(nameof(RelationShipStatusId),
                            () =>
                            {
                                if (RelationShipStatusId==null)
                                {
                                    return RuleResult.Invalid("Relationship is mandatory");
                                }
                                return RuleResult.Valid();
                            });




        }
        #endregion

    }
}
