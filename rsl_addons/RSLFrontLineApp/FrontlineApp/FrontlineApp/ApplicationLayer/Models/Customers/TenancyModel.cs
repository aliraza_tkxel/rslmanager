﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ApplicationLayer.Models
{
    public class TenancyModel : BaseModel
    {
        private int? tenancyId;
        public int? TenancyId
        {
            get { return tenancyId; }
            set { Set(() => TenancyId, ref tenancyId, value); }
        }
        public int? PausedOnStepId { get; set; }
        private int? customerId;
        public int? CustomerId
        {
            get { return customerId; }
            set { Set(() => CustomerId, ref customerId, value); }
        }

        private string propertyId;
        public string PropertyId
        {
            get { return propertyId; }
            set { Set(() => PropertyId, ref propertyId, value); }
        }

        private DateTime? tenancyStartDate;
        public DateTime? TenancyStartDate
        {
            get { return tenancyStartDate; }
            set { Set(() => TenancyStartDate, ref tenancyStartDate, value); }
        }
        private DateTime? tenancyEndDate;
        public DateTime? TenancyEndDate
        {
            get { return tenancyEndDate; }
            set { Set(() => TenancyEndDate, ref tenancyEndDate, value); }
        }
        public TenancyAddressModel TenancyAddress { get; set; }
        public EmploymentInfoModel EmploymentInfo { get; set; }
        public CustomerGeneralInfoModel CustomerGeneralInfo { get; set; }
        public TenancyEmergencyContactModel EmergencyContact { get; set; }
        public JointTenantModel JointTenant { get; set; }
        public List<OccupantModel> Occupants { get; set; }
        public ReferralModel Referral { get; set; }
        public PropertyRentModel PropertyRentInfo { get; set; }
        public TenancyDisclosureModel TenancyDisclosure { get; set; }
        public TenantOnlineSetupModel TenantOnlineInfo { get; set; }

    }
}
