﻿using MvvmValidation;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace FrontlineApp.ApplicationLayer.Models
{
    public class ReferralModel : BaseModel
    {

        private int? id;
        public int? Id
        {
            get { return id; }
            set { Set(() => Id, ref id, value); }
        }

        private int? customerId;
        public int? CustomerId
        {
            get { return customerId; }
            set { Set(() => CustomerId, ref customerId, value); }
        }

        private int? referralHistoryId;
        public int? ReferralHistoryId
        {
            get { return referralHistoryId; }
            set { Set(() => ReferralHistoryId, ref referralHistoryId, value); }
        }

        private int? journalId;
        public int? JournalId
        {
            get { return journalId; }
            set { Set(() => JournalId, ref journalId, value); }
        }

        private string title;
        public string Title
        {
            get { return title; }
            set { Set(() => Title, ref title, value); }
        }

        private bool? isTypeArrears;
        public bool? IsTypeArrears
        {
            get { return isTypeArrears; }
            set { Set(() => IsTypeArrears, ref isTypeArrears, value); }
        }

        private bool? isTypeDamage;
        public bool? IsTypeDamage
        {
            get { return isTypeDamage; }
            set { Set(() => IsTypeDamage, ref isTypeDamage, value); }
        }

        private bool? isTypeAsb;
        public bool? IsTypeAsb
        {
            get { return isTypeAsb; }
            set { Set(() => IsTypeAsb, ref isTypeAsb, value); }
        }

        private string stage;
        public string Stage
        {
            get { return stage; }
            set { Set(() => Stage, ref stage, value); }
        }

        private int? stageId;
        public int? StageId
        {
            get { return stageId; }
            set { Set(() => StageId, ref stageId, value); }
        }

        private string safetyIssue;
        public string SafetyIssue
        {
            get { return safetyIssue; }
            set { Set(() => SafetyIssue, ref safetyIssue, value); }
        }

        private int? safetyIssueId;
        public int? SafetyIssueId
        {
            get { return safetyIssueId; }
            set { Set(() => SafetyIssueId, ref safetyIssueId, value); }
        }

        private int? isConviction;
        public int? IsConviction
        {
            get { return isConviction; }
            set { Set(() => IsConviction, ref isConviction, value); }
        }

        private string conviction;
        public string Conviction
        {
            get { return conviction; }
            set { Set(() => Conviction, ref conviction, value); }
        }

        private int? isSubstanceMisuse;
        public int? IsSubstanceMisuse
        {
            get { return isSubstanceMisuse; }
            set { Set(() => IsSubstanceMisuse, ref isSubstanceMisuse, value); }
        }

        private string substanceMisuse;
        public string SubstanceMisuse
        {
            get { return substanceMisuse; }
            set { Set(() => SubstanceMisuse, ref substanceMisuse, value); }
        }

        private int? isSelfHarm;
        public int? IsSelfHarm
        {
            get { return isSelfHarm; }
            set { Set(() => IsSelfHarm, ref isSelfHarm, value); }
        }

        private string selfHarm;
        public string SelfHarm
        {
            get { return selfHarm; }
            set { Set(() => SelfHarm, ref selfHarm, value); }
        }

        private string yesNotes;
        public string YesNotes
        {
            get { return yesNotes; }
            set { Set(() => YesNotes, ref yesNotes, value); }
        }

        private int? isTenantAware;
        public int? IsTenantAware
        {
            get { return isTenantAware; }
            set { Set(() => IsTenantAware, ref isTenantAware, value); }
        }

        private string tenantAware;
        public string TenantAware
        {
            get { return tenantAware; }
            set { Set(() => TenantAware, ref tenantAware, value); }
        }

        private string neglectNotes;
        public string NeglectNotes
        {
            get { return neglectNotes; }
            set { Set(() => NeglectNotes, ref neglectNotes, value); }
        }

        private string otherNotes;
        public string OtherNotes
        {
            get { return otherNotes; }
            set { Set(() => OtherNotes, ref otherNotes, value); }
        }

        private string miscNotes;
        public string MiscNotes
        {
            get { return miscNotes; }
            set { Set(() => MiscNotes, ref miscNotes, value); }
        }

        private string customerHelpSubCategories;
        public string CustomerHelpSubCategories
        {
            get { return customerHelpSubCategories; }
            set { Set(() => CustomerHelpSubCategories, ref customerHelpSubCategories, value); }
        }

        private List<string> moneyIssues;
        public List<string> MoneyIssues
        {
            get { return moneyIssues; }
            set { Set(() => MoneyIssues, ref moneyIssues, value); }
        }

        private List<string> personalProblems;
        public List<string> PersonalProblems
        {
            get { return personalProblems; }
            set { Set(() => PersonalProblems, ref personalProblems, value); }
        }

        private List<string> workOrTrainings;
        public List<string> WorkOrTrainings
        {
            get { return workOrTrainings; }
            set { Set(() => WorkOrTrainings, ref workOrTrainings, value); }
        }

        private List<string> practicalProblems;
        public List<string> PracticalProblems
        {
            get { return practicalProblems; }
            set { Set(() => PracticalProblems, ref practicalProblems, value); }
        }


        private int? lastActionUserId;
        public int? LastActionUserId
        {
            get { return lastActionUserId; }
            set { Set(() => LastActionUserId, ref lastActionUserId, value); }
        }

        private DateTimeOffset? lastModifiedOnServer;
        public DateTimeOffset? LastModifiedOnServer
        {
            get { return lastModifiedOnServer; }
            set { Set(() => LastModifiedOnServer, ref lastModifiedOnServer, value); }
        }

        private DateTimeOffset? lastModifiedOnApp;
        public DateTimeOffset? LastModifiedOnApp
        {
            get { return lastModifiedOnApp; }
            set { Set(() => LastModifiedOnApp, ref lastModifiedOnApp, value); }
        }

        private DateTimeOffset? createdOnApp;
        public DateTimeOffset? CreatedOnApp
        {
            get { return createdOnApp; }
            set { Set(() => CreatedOnApp, ref createdOnApp, value); }
        }

        private DateTimeOffset? createdOnServer;
        public DateTimeOffset? CreatedOnServer
        {
            get { return createdOnServer; }
            set { Set(() => CreatedOnServer, ref createdOnServer, value); }
        }

        private string appVersion;
        public string AppVersion
        {
            get { return appVersion; }
            set { Set(() => AppVersion, ref appVersion, value); }
        }

        private string referrerName;
        public string ReferrerName
        {
            get { return referrerName; }
            set { Set(() => ReferrerName, ref referrerName, value); }
        }

        private string aidAndAdaptions;
        public string AidAndAdaptions
        {
            get { return aidAndAdaptions; }
            set { Set(() => AidAndAdaptions, ref aidAndAdaptions, value); }
        }

        #region "Constructor"
        public ReferralModel()
        {
            ConfigureValidationRules();
        }
        #endregion

        #region "Configure Validation Rules"
        private void ConfigureValidationRules()
        {
            Validator.AddRule(nameof(Title),
                            () =>
                            {
                                if (string.IsNullOrEmpty(Title))
                                {
                                    return RuleResult.Invalid("Title is required");
                                }
                                else if (string.IsNullOrWhiteSpace(Title.Trim()))
                                {
                                    return RuleResult.Invalid("Title is required");
                                }
                                else
                                {
                                    Regex noSpecialCharacterRegex = new Regex(@"^[a-zA-Z0-9 _'.-]*$");
                                    bool isValid = noSpecialCharacterRegex.IsMatch(Title);
                                    return isValid ? RuleResult.Valid() : RuleResult.Invalid("Special characters are not allowed for Title");
                                }
                            });

            Validator.AddRule(nameof(IsTypeArrears),
                            () =>
                            {
                                if (IsTypeArrears == null && IsTypeAsb == null && IsTypeDamage == null)
                                {
                                    return RuleResult.Invalid("Please select atleast one type");
                                }

                                return RuleResult.Valid();
                            });

            Validator.AddRule(nameof(StageId),
                            () =>
                            {
                                if (StageId==null || StageId==0)
                                {
                                    return RuleResult.Invalid("Please select stage.");
                                }

                                return RuleResult.Valid();
                            });

            Validator.AddRule(nameof(SafetyIssueId),
                            () =>
                            {
                                if (SafetyIssueId == null || SafetyIssueId == 0)
                                {
                                    return RuleResult.Invalid("Please select value from safety issue radio button.");
                                }

                                return RuleResult.Valid();
                            });

            Validator.AddRule(nameof(IsConviction),
                            () =>
                            {
                                if (IsConviction == null || IsConviction == 0)
                                {
                                    return RuleResult.Invalid("Please select value from criminal conviction radio button.");
                                }

                                return RuleResult.Valid();
                            });

            Validator.AddRule(nameof(IsSubstanceMisuse),
                            () =>
                            {
                                if (IsSubstanceMisuse == null || IsSubstanceMisuse == 0)
                                {
                                    return RuleResult.Invalid("Please select value from substance misuse radio button.");
                                }

                                return RuleResult.Valid();
                            });

            Validator.AddRule(nameof(IsSelfHarm),
                            () =>
                            {
                                if (IsSelfHarm == null || IsSelfHarm == 0)
                                {
                                    return RuleResult.Invalid("Please select value from selfharm/suicide radio button.");
                                }

                                return RuleResult.Valid();
                            });

            Validator.AddRule(nameof(IsTenantAware),
                            () =>
                            {
                                if (IsTenantAware == null || IsTenantAware == 0)
                                {
                                    return RuleResult.Invalid("Please select value from Tenant Aware radio button.");
                                }

                                return RuleResult.Valid();
                            });


        }

        #endregion

    }
}
