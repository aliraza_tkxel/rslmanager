﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ApplicationLayer.Models
{
   public class EmploymentInfoModel : BaseModel
    {
        private int? id;
        public int? Id
        {
            get { return id; }
            set { Set(() => Id, ref id, value); }
        }

        private int? employmentStatusId;
        public int? EmploymentStatusId
        {
            get { return employmentStatusId; }
            set
            {
                if (value != null)
                {
                    Set(() => EmploymentStatusId, ref employmentStatusId, value);
                }
                

            }
        }
        private string employmentStatusDescription;
        public string EmploymentStatusDescription
        {
            get { return employmentStatusDescription; }
            set { Set(() => EmploymentStatusDescription, ref employmentStatusDescription, value); }
        }
        private string occupation;
        public string Occupation
        {
            get { return occupation; }
            set { Set(() => Occupation, ref occupation, value); }
        }

        private string employerName;
        public string EmployerName
        {
            get { return employerName; }
            set { Set(() => EmployerName, ref employerName, value); }
        }

        private string employerAddress;
        public string EmployerAddress
        {
            get { return employerAddress; }
            set { Set(() => EmployerAddress, ref employerAddress, value); }
        }

        private int? takeHomePay;
        public int? TakeHomePay
        {
            get { return takeHomePay; }
            set { Set(() => TakeHomePay, ref takeHomePay, value); }
        }
    }
}
