﻿namespace FrontlineApp.ApplicationLayer.Models
{
    public class ItemModel
    {
        public string Title { get; set; }
        public string NormalStateImage { get; set; }
        public string SelectedStateImage { get; set; }
    }
}
