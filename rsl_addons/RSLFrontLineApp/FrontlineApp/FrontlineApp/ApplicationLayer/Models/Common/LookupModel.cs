﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ApplicationLayer.Models
{
   public class LookupModel : BaseModel
    {
        private int id;

        public int Id
        {
            get { return id; }
            set
            {
                Set(() => Id, ref id, value);
            }
        }

        private string description;
        public string Description
        {
            get { return description; }
            set
            {
                Set(() => Description, ref description, value);
            }
        }

        private int subId;
        public int SubId
        {
            get { return subId; }
            set
            {
                Set(() => SubId, ref subId, value);
            }
        } 

        private bool isSelected;
        public bool IsSelected
        {
            get { return isSelected; }
            set
            {
                Set(() => IsSelected, ref isSelected, value);
            }
        }

        private string groupKey;
        public string GroupKey
        {
            get { return groupKey; }
            set
            {
                Set(() => GroupKey, ref groupKey, value);
            }
        }
    }
}
