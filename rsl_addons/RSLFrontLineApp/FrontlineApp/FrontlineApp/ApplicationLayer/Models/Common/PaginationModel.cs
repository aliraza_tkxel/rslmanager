﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ApplicationLayer.Models
{
    public class PaginationModel
    {
        public int pageSize { get; set; }
        public int pageNumber { get; set; }
        public int totalRows { get; set; }
        public int totalPages { get; set; }

        public PaginationModel()
        {
            pageSize = 300;
            pageNumber = 1;
            totalRows = 0;
            totalPages = 0;
        }
    }
}
