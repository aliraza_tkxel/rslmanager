﻿namespace FrontlineApp.ApplicationLayer.Models
{
    public class ManagerResponseModel
    {
        public ManagerResponseModel()
        {
            message = "";
            isSuccessful = false;
            responseObject = null;
        }
        public ManagerResponseModel(string msg, bool status, object response)
        {
            message = msg;
            isSuccessful = status;
            responseObject = response;
        }
        public string message;
        public bool isSuccessful;
        public object responseObject;
    }
}
