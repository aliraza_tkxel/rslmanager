﻿using System.Collections.Generic;

namespace FrontlineApp.ApplicationLayer.Models
{
    public class DummyDashboardModel
    {
        public string cardTitle { get; set; }
        public string cardValue { get; set; }
        public string cardIcon { get; set; }
    }
    public class DummyModelManager
    {
        public static List<DummyDashboardModel> GetViewingsData()
        {
            var DummyData = new List<DummyDashboardModel>();
            DummyData.Add(new DummyDashboardModel
            {
                cardTitle = "Today's Viewings",
                cardIcon = "/Assets/2x/viewings.png",
                cardValue = "3"
            });
            DummyData.Add(new DummyDashboardModel
            {
                cardTitle = "Logged",
                cardIcon = "/Assets/2x/logged.png",
                cardValue = "9"
            });
            DummyData.Add(new DummyDashboardModel
            {
                cardTitle = "Approvals",
                cardIcon = "/Assets/2x/approvals.png",
                cardValue = "0"
            });
            return DummyData;
        }
        public static List<DummyDashboardModel> GetAlertsData()
        {
            var DummyData = new List<DummyDashboardModel>();
            DummyData.Add(new DummyDashboardModel
            {
                cardTitle = "Open Risks",
                cardIcon = "/Assets/2x/viewings.png",
                cardValue = "0"
            });
            DummyData.Add(new DummyDashboardModel
            {
                cardTitle = "Open Vulnerabilities",
                cardIcon = "/Assets/2x/logged.png",
                cardValue = "11"
            });
            /*DummyData.Add(new DummyDashboardModel
            {
                cardTitle = "General Inquiry",
                cardIcon = "/Assets/2x/approvals.png",
                cardValue = "14"
            });*/
            return DummyData;
        }
        public static List<DummyDashboardModel> GetFaultsData()
        {
            var DummyData = new List<DummyDashboardModel>();
            DummyData.Add(new DummyDashboardModel
            {
                cardTitle = "Unassigned Faults",
                cardIcon = "/Assets/2x/unassigned_faults.png",
                cardValue = "9"
            });

            return DummyData;
        }
        public static List<DummyDashboardModel> GetIncomeManagementData()
        {
            var DummyData = new List<DummyDashboardModel>();
            DummyData.Add(new DummyDashboardModel
            {
                cardTitle = "My Case",
                cardIcon = "/Assets/2x/my_case.png",
                cardValue = "5"
            });
            DummyData.Add(new DummyDashboardModel
            {
                cardTitle = "Overdue Stages",
                cardIcon = "/Assets/2x/overdue0stages.png",
                cardValue = "32"
            });
            DummyData.Add(new DummyDashboardModel
            {
                cardTitle = "Initial Case Monitoring",
                cardIcon = "/Assets/2x/initial_case_monitoring.png",
                cardValue = "0"
            });
            DummyData.Add(new DummyDashboardModel
            {
                cardTitle = "NISPs Due to Expire",
                cardIcon = "/Assets/2x/nisps.png",
                cardValue = "9"
            });
            DummyData.Add(new DummyDashboardModel
            {
                cardTitle = "Overdue Actions",
                cardIcon = "/Assets/2x/overdue_actions.png",
                cardValue = "4"
            });
            return DummyData;
        }
    }
}
