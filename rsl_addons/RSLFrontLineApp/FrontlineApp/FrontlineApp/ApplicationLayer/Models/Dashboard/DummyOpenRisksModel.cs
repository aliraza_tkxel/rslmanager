﻿using System.Collections.Generic;

namespace FrontlineApp.ApplicationLayer.Models
{
    public class DummyOpenRisksModel
    {
        public string tenantName { get; set; }
        public string title { get; set; }
        public string teamName { get; set; }
        public string assignedOperativeName { get; set; }
        public string startDate { get; set; }
        public string riskCategory { get; set; }
        public string riskSubcategory { get; set; }
        public string riskNotes { get; set; }
        public string riskReviewDate { get; set; }
        public string riskStatus { get; set; }
        public string riskActions { get; set; }
        public string riskLetters { get; set; }
        public string enquiryStatus { get; set; } 
    }
    public class DummyOpenRisksManager
    {
        public static List<DummyOpenRisksModel> GetRisksData()
        {
            var DummyData = new List<DummyOpenRisksModel>();
            DummyData.Add(new DummyOpenRisksModel {
                tenantName = "Rebecca Crawley",
                title = "Rent Statement",
                teamName = "Frontline",
                assignedOperativeName = "FNAME1089 LNAME8927",
                startDate = "Jan 02, 2016",
                riskCategory = "Act of Violence",
                riskSubcategory = "Actual Attack",
                riskNotes = "The tenant has a history of psychological issues and is known to have psychotic episodes.",
                riskReviewDate = "Jan 10, 2016",
                riskStatus = "Customer Aware",
                riskActions = "NA",
                riskLetters = "Risk Notification",
                enquiryStatus = "In Progress"
            });
            DummyData.Add(new DummyOpenRisksModel
            {
                tenantName = "Eirc Mannstein",
                title = "Gas payment statement",
                teamName = "Frontline",
                assignedOperativeName = "FNAME1089 LNAME8927",
                startDate = "Jan 02, 2016",
                riskCategory = "Hate Crime",
                riskSubcategory = "Racism",
                riskNotes = "The tenant has been convicted of a hate crime before",
                riskReviewDate = "Jan 10, 2016",
                riskStatus = "Customer Aware",
                riskActions = "NA",
                riskLetters = "Risk Notification",
                enquiryStatus = "Open"
            });
            DummyData.Add(new DummyOpenRisksModel
            {
                tenantName = "Marvin Marshall",
                title = "Repairs Appointment",
                teamName = "Frontline",
                assignedOperativeName = "FNAME1089 LNAME8927",
                startDate = "Jan 02, 2016",
                riskCategory = "Threatening Behavior",
                riskSubcategory = "Harrasment",
                riskNotes = "The tenant has harrassed other operatives as well during the course of their duties",
                riskReviewDate = "Jan 10, 2016",
                riskStatus = "Customer Aware",
                riskActions = "Contact Exported",
                riskLetters = "Risk Notification",
                enquiryStatus = "Completed"
            });
            DummyData.Add(new DummyOpenRisksModel
            {
                tenantName = "Rebecca Crawley",
                title = "Rent Statement",
                teamName = "Frontline",
                assignedOperativeName = "FNAME1089 LNAME8927",
                startDate = "Jan 02, 2016",
                riskCategory = "Act of Violence",
                riskSubcategory = "Actual Attack",
                riskNotes = "The tenant has a history of psychological issues and is known to have psychotic episodes.",
                riskReviewDate = "Jan 10, 2016",
                riskStatus = "Customer Aware",
                riskActions = "Response Received",
                riskLetters = "Risk Notification",
                enquiryStatus = "In Progress"
            });
            DummyData.Add(new DummyOpenRisksModel
            {
                tenantName = "Eirc Mannstein",
                title = "Gas payment statement",
                teamName = "Frontline",
                assignedOperativeName = "FNAME1089 LNAME8927",
                startDate = "Jan 02, 2016",
                riskCategory = "Hate Crime",
                riskSubcategory = "Racism",
                riskNotes = "The tenant has been convicted of a hate crime before",
                riskReviewDate = "Jan 10, 2016",
                riskStatus = "Customer Aware",
                riskActions = "NA",
                riskLetters = "Risk Notification",
                enquiryStatus = "Open"
            });
            DummyData.Add(new DummyOpenRisksModel
            {
                tenantName = "Marvin Marshall",
                title = "Repairs Appointment",
                teamName = "Frontline",
                assignedOperativeName = "FNAME1089 LNAME8927",
                startDate = "Jan 02, 2016",
                riskCategory = "Threatening Behavior",
                riskSubcategory = "Harrasment",
                riskNotes = "The tenant has harrassed other operatives as well during the course of their duties",
                riskReviewDate = "Jan 10, 2016",
                riskStatus = "Customer Aware",
                riskActions = "NA",
                riskLetters = "Risk Notification",
                enquiryStatus = "Completed"
            });
            DummyData.Add(new DummyOpenRisksModel
            {
                tenantName = "Rebecca Crawley",
                title = "Rent Statement",
                teamName = "Frontline",
                assignedOperativeName = "FNAME1089 LNAME8927",
                startDate = "Jan 02, 2016",
                riskCategory = "Act of Violence",
                riskSubcategory = "Actual Attack",
                riskNotes = "The tenant has a history of psychological issues and is known to have psychotic episodes.",
                riskReviewDate = "Jan 10, 2016",
                riskStatus = "Customer Aware",
                riskActions = "NA",
                riskLetters = "Risk Notification",
                enquiryStatus = "In Progress"
            });
            DummyData.Add(new DummyOpenRisksModel
            {
                tenantName = "Eirc Mannstein",
                title = "Gas payment statement",
                teamName = "Frontline",
                assignedOperativeName = "FNAME1089 LNAME8927",
                startDate = "Jan 02, 2016",
                riskCategory = "Hate Crime",
                riskSubcategory = "Racism",
                riskNotes = "The tenant has been convicted of a hate crime before",
                riskReviewDate = "Jan 10, 2016",
                riskStatus = "Customer Aware",
                riskActions = "NA",
                riskLetters = "Risk Notification",
                enquiryStatus = "Open"
            });
            DummyData.Add(new DummyOpenRisksModel
            {
                tenantName = "Marvin Marshall",
                title = "Repairs Appointment",
                teamName = "Frontline",
                assignedOperativeName = "FNAME1089 LNAME8927",
                startDate = "Jan 02, 2016",
                riskCategory = "Threatening Behavior",
                riskSubcategory = "Harrasment",
                riskNotes = "The tenant has harrassed other operatives as well during the course of their duties",
                riskReviewDate = "Jan 10, 2016",
                riskStatus = "Customer Aware",
                riskActions = "NA",
                riskLetters = "Risk Notification",
                enquiryStatus = "Completed"
            });
            DummyData.Add(new DummyOpenRisksModel
            {
                tenantName = "Rebecca Crawley",
                title = "Rent Statement",
                teamName = "Frontline",
                assignedOperativeName = "FNAME1089 LNAME8927",
                startDate = "Jan 02, 2016",
                riskCategory = "Act of Violence",
                riskSubcategory = "Actual Attack",
                riskNotes = "The tenant has a history of psychological issues and is known to have psychotic episodes.",
                riskReviewDate = "Jan 10, 2016",
                riskStatus = "Customer Aware",
                riskActions = "NA",
                riskLetters = "Risk Notification",
                enquiryStatus = "In Progress"
            });
            DummyData.Add(new DummyOpenRisksModel
            {
                tenantName = "Eirc Mannstein",
                title = "Gas payment statement",
                teamName = "Frontline",
                assignedOperativeName = "FNAME1089 LNAME8927",
                startDate = "Jan 02, 2016",
                riskCategory = "Hate Crime",
                riskSubcategory = "Racism",
                riskNotes = "The tenant has been convicted of a hate crime before",
                riskReviewDate = "Jan 10, 2016",
                riskStatus = "Customer Aware",
                riskActions = "NA",
                riskLetters = "Risk Notification",
                enquiryStatus = "Open"
            });
            DummyData.Add(new DummyOpenRisksModel
            {
                tenantName = "Marvin Marshall",
                title = "Repairs Appointment",
                teamName = "Frontline",
                assignedOperativeName = "FNAME1089 LNAME8927",
                startDate = "Jan 02, 2016",
                riskCategory = "Threatening Behavior",
                riskSubcategory = "Harrasment",
                riskNotes = "The tenant has harrassed other operatives as well during the course of their duties",
                riskReviewDate = "Jan 10, 2016",
                riskStatus = "Customer Aware",
                riskActions = "NA",
                riskLetters = "Risk Notification",
                enquiryStatus = "Completed"
            });
            return DummyData;
        }

        public static List<DummyOpenRisksModel> GetVulnerabilitiesData()
        {
            var DummyData = new List<DummyOpenRisksModel>();
            DummyData.Add(new DummyOpenRisksModel
            {
                tenantName = "Rebecca Crawley",
                title = "Learning Disability",
                teamName = "Frontline",
                assignedOperativeName = "FNAME1089 LNAME8927",
                startDate = "Jan 02, 2016",
                riskCategory = "Disability",
                riskSubcategory = "Learning Disability",
                riskNotes = "The tenant has a history of being victim of domestic abuse",
                riskReviewDate = "Jan 10, 2016",
                riskStatus = "Customer Aware",
                riskActions = "NA",
                riskLetters = "Notification Letter",
                enquiryStatus = "In Progress"
            });
            DummyData.Add(new DummyOpenRisksModel
            {
                tenantName = "Eirc Mannstein",
                title = "Alcoholic",
                teamName = "Frontline",
                assignedOperativeName = "FNAME1089 LNAME8927",
                startDate = "Jan 02, 2016",
                riskCategory = "Dependency",
                riskSubcategory = "Alcohol Dependency",
                riskNotes = "The tenant has been convicted of a drunk driving",
                riskReviewDate = "Jan 10, 2016",
                riskStatus = "Customer Aware",
                riskActions = "NA",
                riskLetters = "Notification Letter",
                enquiryStatus = "Open"
            });
            DummyData.Add(new DummyOpenRisksModel
            {
                tenantName = "Marvin Marshall",
                title = "Handicapped Man",
                teamName = "Frontline",
                assignedOperativeName = "FNAME1089 LNAME8927",
                startDate = "Jan 02, 2016",
                riskCategory = "Disability",
                riskSubcategory = "Handicapped",
                riskNotes = "The tenant has lost the use of his legs",
                riskReviewDate = "Jan 10, 2016",
                riskStatus = "Customer Aware",
                riskActions = "Contact Exported",
                riskLetters = "Notification Letter",
                enquiryStatus = "Completed"
            });
            DummyData.Add(new DummyOpenRisksModel
            {
                tenantName = "Rebecca Crawley",
                title = "Visually impaired",
                teamName = "Frontline",
                assignedOperativeName = "FNAME1089 LNAME8927",
                startDate = "Jan 02, 2016",
                riskCategory = "Impairement",
                riskSubcategory = "Visually impaired",
                riskNotes = "She be blind, Son!",
                riskReviewDate = "Jan 10, 2016",
                riskStatus = "Customer Aware",
                riskActions = "Response Received",
                riskLetters = "Notification Letter",
                enquiryStatus = "In Progress"
            });
            DummyData.Add(new DummyOpenRisksModel
            {
                tenantName = "Eirc Mannstein",
                title = "Drug Dependency",
                teamName = "Frontline",
                assignedOperativeName = "FNAME1089 LNAME8927",
                startDate = "Jan 02, 2016",
                riskCategory = "Dependency",
                riskSubcategory = "Drug Dependency",
                riskNotes = "Known Junkie. This close to being a HOBO.",
                riskReviewDate = "Jan 10, 2016",
                riskStatus = "Customer Aware",
                riskActions = "NA",
                riskLetters = "Notification Letter",
                enquiryStatus = "Open"
            });
            DummyData.Add(new DummyOpenRisksModel
            {
                tenantName = "Marvin Marshall",
                title = "Mental Illness",
                teamName = "Frontline",
                assignedOperativeName = "FNAME1089 LNAME8927",
                startDate = "Jan 02, 2016",
                riskCategory = "Medical",
                riskSubcategory = "Mental Illness",
                riskNotes = "Off the boat Psycho. If you see him reaching into his jacket, Run.",
                riskReviewDate = "Jan 10, 2016",
                riskStatus = "Customer Aware",
                riskActions = "NA",
                riskLetters = "Notification Letter",
                enquiryStatus = "Completed"
            });
            DummyData.Add(new DummyOpenRisksModel
            {
                tenantName = "Rebecca Crawley",
                title = "Rent Statement",
                teamName = "Frontline",
                assignedOperativeName = "FNAME1089 LNAME8927",
                startDate = "Jan 02, 2016",
                riskCategory = "Medical",
                riskSubcategory = "Mental Illness",
                riskNotes = "The tenant has a history of psychological issues and is known to have psychotic episodes.",
                riskReviewDate = "Jan 10, 2016",
                riskStatus = "Customer Aware",
                riskActions = "NA",
                riskLetters = "Notification Letter",
                enquiryStatus = "In Progress"
            });
            DummyData.Add(new DummyOpenRisksModel
            {
                tenantName = "Rebecca Crawley",
                title = "Learning Disability",
                teamName = "Frontline",
                assignedOperativeName = "FNAME1089 LNAME8927",
                startDate = "Jan 02, 2016",
                riskCategory = "Disability",
                riskSubcategory = "Learning Disability",
                riskNotes = "The tenant has a history of being victim of domestic abuse",
                riskReviewDate = "Jan 10, 2016",
                riskStatus = "Customer Aware",
                riskActions = "NA",
                riskLetters = "Notification Letter",
                enquiryStatus = "In Progress"
            });
            DummyData.Add(new DummyOpenRisksModel
            {
                tenantName = "Eirc Mannstein",
                title = "Alcoholic",
                teamName = "Frontline",
                assignedOperativeName = "FNAME1089 LNAME8927",
                startDate = "Jan 02, 2016",
                riskCategory = "Dependency",
                riskSubcategory = "Alcohol Dependency",
                riskNotes = "The tenant has been convicted of a drunk driving",
                riskReviewDate = "Jan 10, 2016",
                riskStatus = "Customer Aware",
                riskActions = "NA",
                riskLetters = "Notification Letter",
                enquiryStatus = "Open"
            });
            DummyData.Add(new DummyOpenRisksModel
            {
                tenantName = "Marvin Marshall",
                title = "Handicapped Man",
                teamName = "Frontline",
                assignedOperativeName = "FNAME1089 LNAME8927",
                startDate = "Jan 02, 2016",
                riskCategory = "Disability",
                riskSubcategory = "Handicapped",
                riskNotes = "The tenant has lost the use of his legs",
                riskReviewDate = "Jan 10, 2016",
                riskStatus = "Customer Aware",
                riskActions = "Contact Exported",
                riskLetters = "Notification Letter",
                enquiryStatus = "Completed"
            });
            DummyData.Add(new DummyOpenRisksModel
            {
                tenantName = "Rebecca Crawley",
                title = "Visually impaired",
                teamName = "Frontline",
                assignedOperativeName = "FNAME1089 LNAME8927",
                startDate = "Jan 02, 2016",
                riskCategory = "Impairement",
                riskSubcategory = "Visually impaired",
                riskNotes = "She be blind, Son!",
                riskReviewDate = "Jan 10, 2016",
                riskStatus = "Customer Aware",
                riskActions = "Response Received",
                riskLetters = "Notification Letter",
                enquiryStatus = "In Progress"
            });
            DummyData.Add(new DummyOpenRisksModel
            {
                tenantName = "Eirc Mannstein",
                title = "Drug Dependency",
                teamName = "Frontline",
                assignedOperativeName = "FNAME1089 LNAME8927",
                startDate = "Jan 02, 2016",
                riskCategory = "Dependency",
                riskSubcategory = "Drug Dependency",
                riskNotes = "Known Junkie. This close to being a HOBO.",
                riskReviewDate = "Jan 10, 2016",
                riskStatus = "Customer Aware",
                riskActions = "NA",
                riskLetters = "Notification Letter",
                enquiryStatus = "Open"
            });
            DummyData.Add(new DummyOpenRisksModel
            {
                tenantName = "Marvin Marshall",
                title = "Mental Illness",
                teamName = "Frontline",
                assignedOperativeName = "FNAME1089 LNAME8927",
                startDate = "Jan 02, 2016",
                riskCategory = "Medical",
                riskSubcategory = "Mental Illness",
                riskNotes = "Off the boat Psycho. If you see him reaching into his jacket, Run.",
                riskReviewDate = "Jan 10, 2016",
                riskStatus = "Customer Aware",
                riskActions = "NA",
                riskLetters = "Notification Letter",
                enquiryStatus = "Completed"
            });
            DummyData.Add(new DummyOpenRisksModel
            {
                tenantName = "Rebecca Crawley",
                title = "Rent Statement",
                teamName = "Frontline",
                assignedOperativeName = "FNAME1089 LNAME8927",
                startDate = "Jan 02, 2016",
                riskCategory = "Medical",
                riskSubcategory = "Mental Illness",
                riskNotes = "The tenant has a history of psychological issues and is known to have psychotic episodes.",
                riskReviewDate = "Jan 10, 2016",
                riskStatus = "Customer Aware",
                riskActions = "NA",
                riskLetters = "Notification Letter",
                enquiryStatus = "In Progress"
            });
            return DummyData;
        }
    }
}
