﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ApplicationLayer.Models
{
    public enum ViewingsTypes
    {
        MyViewings,
        Today,
        Pending,
        All
    }
    public class DashboardGridModel
    {
        #region "Properties"
        public string cardTitle { get; set; }
        public string cardValue { get; set; }

        public string CardBg { get; set; }
        #endregion
        #region "Constants"
        #region "Viewings"

        private const string ImagePathBgBlue = "/Assets/2x/dashboard_card_bg_blue.png";
        private const string ImagePathBgRed = "/Assets/2x/dashboard_card_bg_red.png";

        private const string TitleMyViewings = "My Viewings";
        private const string TitleTodaysViewings = "Today's Viewings";
        private const string TitleApprovalViewings = "Pending Viewings";
        private const string TitleAllViewings = "All Viewings";
        #endregion
        #endregion
        #region "Constructor"
        public DashboardGridModel()
        {

        }
        public DashboardGridModel(ViewingsTypes type, int Value, bool isRedCard)
        {
            switch (type)
            {
                case ViewingsTypes.MyViewings:
                    cardTitle = TitleMyViewings;
                    break;
                case ViewingsTypes.Today:
                    cardTitle = TitleTodaysViewings;
                    break;
                case ViewingsTypes.Pending:
                    cardTitle = TitleApprovalViewings;
                    break;
                case ViewingsTypes.All:
                    cardTitle = TitleAllViewings;
                    break;
                default:
                    break;
            }
            CardBg = (isRedCard) ? ImagePathBgRed : ImagePathBgBlue;
            cardValue = Value.ToString();
        }
        #endregion
    }
}
