﻿using System.Collections.Generic;

namespace FrontlineApp.ApplicationLayer.Models
{
    public class DummyPropertyModel
    {
        public string propertyType  { get; set; }
        public string  developmentType { get; set; }
        public string scheme { get; set; }
        public string phase  { get; set; }
        public string block { get; set; }
        public string houseNumber { get; set; }
        public string address { get; set; }
        public string city { get; set; }
        public string country { get; set; }
        public string postalCode { get; set; }
        public string floorArea { get; set; }
        public string maximumPeople { get; set; }
        public string noOfLivingRooms { get; set; }
        public string noOfBedrooms { get; set; }
        public string noOfBathrooms { get; set; }
        public string  noOfKitchens { get; set; }
        public string parkingLocation  { get; set; }
        public string gardenLocation { get; set; }
        public string noOfGarages { get; set; }
        public string projectedRent { get; set; }
        public string primaryHeatingFuel { get; set; }
        public string boilerType { get; set; }
        public string wheelChairAccess { get; set; }
        public string propertyStatus { get; set; }
        public string rightToAcquire  { get; set; }
        public string assetType { get; set; }
        public string referenceNumber { get; set; }
    }
    public class DummyPropertyManager
    {
        public static List<DummyPropertyModel> GetPropertyList()
        {
            var DummyData = new List<DummyPropertyModel>();
            DummyData.Add(new DummyPropertyModel {
                propertyType = "House - Mid Terrace",
                developmentType = "Newly Built",
                scheme = "4 Middleton Crescent Costessey",
                phase = "2nd",
                block = "Middleton Crescent (4)",
                houseNumber = "N/A",
                address = "4 Middleton Crescent Costessey, Norwich",
                city = "Norwich",
                country = "England",
                postalCode = "34672",
                floorArea = "100 sqm",
                maximumPeople = "4",
                noOfLivingRooms = "1",
                noOfBedrooms = "3",
                noOfBathrooms = "3",
                noOfKitchens = "1",
                parkingLocation = "Front &amp; Back",
                gardenLocation = "Front",
                noOfGarages = "1",
                projectedRent = "$450",
                primaryHeatingFuel = "Main Gas",
                boilerType = "N/A",
                wheelChairAccess = "Yes",
                propertyStatus = "To Let",
                rightToAcquire = "N/A",
                assetType = "House - Mid Terrace",
                referenceNumber = "BHA334019235"
            });
            DummyData.Add(new DummyPropertyModel
            {
                propertyType = "House - No Terrace",
                developmentType = "Newly Built",
                scheme = "15 Hamilton Queen's Boulevard",
                phase = "7th",
                block = "Hamilton (15)",
                houseNumber = "N/A",
                address = "15 Hamilton Queen's Boulevard, Glasgow",
                city = "Glasgow",
                country = "Scotland",
                postalCode = "34632",
                floorArea = "200 sqm",
                maximumPeople = "8",
                noOfLivingRooms = "1",
                noOfBedrooms = "4",
                noOfBathrooms = "3",
                noOfKitchens = "1",
                parkingLocation = "Front &amp; Back",
                gardenLocation = "Front",
                noOfGarages = "1",
                projectedRent = "$550",
                primaryHeatingFuel = "Main Gas",
                boilerType = "N/A",
                wheelChairAccess = "Yes",
                propertyStatus = "Pending Termination",
                rightToAcquire = "N/A",
                assetType = "House - No Terrace",
                referenceNumber = "BHA000019235"
            });
            DummyData.Add(new DummyPropertyModel
            {
                propertyType = "Studio",
                developmentType = "Renovated",
                scheme = "5 Roterham Fauxville",
                phase = "N/A",
                block = "Roterham(5)",
                houseNumber = "N/A",
                address = "5 Roterham Fauxville, Dover",
                city = "Dover",
                country = "England",
                postalCode = "32342",
                floorArea = "124 sqm",
                maximumPeople = "5",
                noOfLivingRooms = "1",
                noOfBedrooms = "3",
                noOfBathrooms = "3",
                noOfKitchens = "1",
                parkingLocation = "Front",
                gardenLocation = "N/A",
                noOfGarages = "1",
                projectedRent = "$350",
                primaryHeatingFuel = "Main Gas",
                boilerType = "N/A",
                wheelChairAccess = "Yes",
                propertyStatus = "Leased",
                rightToAcquire = "N/A",
                assetType = "Studio",
                referenceNumber = "BHA0000132235"
            });
            DummyData.Add(new DummyPropertyModel
            {
                propertyType = "Apartment",
                developmentType = "Newly Built",
                scheme = "31-3 Burghoff Heights",
                phase = "N/A",
                block = "Burghoff Heights",
                houseNumber = "N/A",
                address = "31-3 Burghoff Heights, London",
                city = "London",
                country = "England",
                postalCode = "312322",
                floorArea = "89 sqm",
                maximumPeople = "1",
                noOfLivingRooms = "1",
                noOfBedrooms = "1",
                noOfBathrooms = "1",
                noOfKitchens = "1",
                parkingLocation = "Basement",
                gardenLocation = "N/A",
                noOfGarages = "N/A",
                projectedRent = "$150",
                primaryHeatingFuel = "Main Gas",
                boilerType = "N/A",
                wheelChairAccess = "N/A",
                propertyStatus = "Pending Termination",
                rightToAcquire = "N/A",
                assetType = "Apartment",
                referenceNumber = "BHA0323019235"
            });
            DummyData.Add(new DummyPropertyModel
            {
                propertyType = "Flat",
                developmentType = "Refurbished",
                scheme = "311-22 Rue De Liberte",
                phase = "NA",
                block = "Rue De Liberte",
                houseNumber = "N/A",
                address = "311-22 Rue De Liberte, Cardiff",
                city = "Cardiff",
                country = "Wales",
                postalCode = "312312",
                floorArea = "231 sqm",
                maximumPeople = "6",
                noOfLivingRooms = "1",
                noOfBedrooms = "4",
                noOfBathrooms = "4",
                noOfKitchens = "1",
                parkingLocation = "Basement",
                gardenLocation = "N/A",
                noOfGarages = "N/A",
                projectedRent = "$250",
                primaryHeatingFuel = "Main Gas",
                boilerType = "N/A",
                wheelChairAccess = "N/A",
                propertyStatus = "To Let",
                rightToAcquire = "N/A",
                assetType = "Flat",
                referenceNumber = "BHA233421235"
            });
            return DummyData;
        }
    }
}
