﻿using System;
using System.Collections.Generic;

namespace FrontlineApp.ApplicationLayer.Models
{
    public class DummyViewingsModel
    {
        public string time { get; set; }
        public string compactDate { get; set; }
        public string propertyAddress { get; set; }
        public Boolean isMale { get; set; }
        public string customerName { get; set; }
        public string assignedBy { get; set; }
        public string completeDate { get; set; }
        public string propertyReferenceNumber { get; set; }
        public string propertyRent { get; set; }
        public string propertyType { get; set; }
        public string propertyRooms { get; set; }
        public string customerContactNumber { get; set; }
        public string customerDateOfBirth { get; set; }
        public string dateCreated { get; set; }
        public string timeCreated { get; set; }
        public string propertyThumbnail { get; set; }
        public string viewingStatus { get; set; }
        public string housingOfficer { get; set; }
        public bool isWaitingForApproval { get; set; }
    }
    public class DummyViewingsManager
    {
        public static List<DummyViewingsModel> GetViewingsData()
        {
            var DummyData = new List<DummyViewingsModel>();
            DummyData.Add(new DummyViewingsModel
            {
                compactDate = "Today",
                time = "13:30 (2hrs later)",
                customerName = "Anastasia Ardvark",
                propertyAddress = "4 Middleton Crescent Cortessy",
                isMale = false,
                assignedBy = "Me",
                completeDate = "Mar 5,2016  (Today)",
                propertyReferenceNumber = "BHA0000000836ZA",
                propertyRent = "$500/month",
                propertyType = "Flat",
                propertyRooms = "2",
                customerContactNumber = "07345 7777892",
                customerDateOfBirth = "Jan 12, 1978",
                dateCreated = "Feb 2, 2016",
                timeCreated = "12:00",
                propertyThumbnail = "/Assets/dummy_property.png",
                viewingStatus = "Accepted",
                housingOfficer = "Rin Tizuka",
                isWaitingForApproval = false
            });
            DummyData.Add(new DummyViewingsModel
            {
               compactDate = "Today",
                time = "16:30 (4hrs later)",
                customerName = "Chris Adams",
                propertyAddress = "4 Middleton Crescent Cortessy",
                isMale = true,
                assignedBy = "Me",
                completeDate = "Mar 5,2016  (Today)",
                propertyReferenceNumber = "BHA0000000836ZA",
                propertyRent = "$500/month",
                propertyType = "Flat",
                propertyRooms = "2",
                customerContactNumber = "07345 7777892",
                customerDateOfBirth = "Jan 12, 1978",
                timeCreated = "12:00",
                propertyThumbnail = "/Assets/dummy_property.png",
                dateCreated = "Feb 2, 2016",
                viewingStatus = "Rejected",
                housingOfficer = "Hanako Ibarazaki",
                isWaitingForApproval = false

            });
            DummyData.Add(new DummyViewingsModel
            {
                compactDate = "Today",
                time = "16:30 (4hrs later)",
                customerName = "Chris Adams",
                propertyAddress = "4 Middleton Crescent Cortessy",
                isMale = true,
                assignedBy = "Me",
                completeDate = "Mar 5,2016  (Today)",
                propertyReferenceNumber = "BHA0000000836ZA",
                propertyRent = "$500/month",
                propertyType = "Flat",
                propertyRooms = "2",
                customerContactNumber = "07345 7777892",
                customerDateOfBirth = "Jan 12, 1978",
                timeCreated = "12:00",
                propertyThumbnail = "/Assets/dummy_property.png",
                dateCreated = "Feb 2, 2016",
                viewingStatus = "Accepted",
                housingOfficer = "Hisao Naiki",
                isWaitingForApproval = false

            });
            DummyData.Add(new DummyViewingsModel
            {
                compactDate = "Today",
                time = "16:30 (4hrs later)",
                customerName = "Chris Adams",
                propertyAddress = "4 Middleton Crescent Cortessy",
                isMale = true,
                assignedBy = "Me",
                completeDate = "Mar 5,2016  (Today)",
                propertyReferenceNumber = "BHA0000000836ZA",
                propertyRent = "$500/month",
                propertyType = "Flat",
                propertyRooms = "2",
                customerContactNumber = "07345 7777892",
                customerDateOfBirth = "Jan 12, 1978",
                timeCreated = "12:00",
                propertyThumbnail = "/Assets/dummy_property.png",
                dateCreated = "Feb 2, 2016",
                viewingStatus = "Rejected",
                housingOfficer = "Shizun Mitsubishi",
                isWaitingForApproval = false

            });
            DummyData.Add(new DummyViewingsModel
            {
                compactDate = "Today",
                time = "13:30 (2hrs later)",
                customerName = "Anastasia Ardvark",
                propertyAddress = "4 Middleton Crescent Cortessy",
                isMale = false,
                assignedBy = "Me",
                completeDate = "Mar 5,2016  (Today)",
                propertyReferenceNumber = "BHA0000000836ZA",
                propertyRent = "$500/month",
                propertyType = "Flat",
                propertyRooms = "2",
                customerContactNumber = "07345 7777892",
                customerDateOfBirth = "Jan 12, 1978",
                timeCreated = "12:00",
                propertyThumbnail = "/Assets/dummy_property.png",
                dateCreated = "Feb 2, 2016",
                viewingStatus = "Waiting",
                housingOfficer = "Erik Ferghaven",
                isWaitingForApproval = true
            });
            DummyData.Add(new DummyViewingsModel
            {
                compactDate = "Today",
                time = "13:30 (2hrs later)",
                customerName = "Fernando Trastamaria",
                propertyAddress = "4 Middleton Crescent Cortessy",
                isMale = true,
                assignedBy = "Me",
                completeDate = "Mar 5,2016  (Today)",
                propertyReferenceNumber = "BHA0000000836ZA",
                propertyRent = "$500/month",
                propertyType = "Flat",
                propertyRooms = "2",
                customerContactNumber = "07345 7777892",
                customerDateOfBirth = "Jan 12, 1978",
                timeCreated = "12:00",
                propertyThumbnail = "/Assets/dummy_property.png",
                dateCreated = "Feb 2, 2016",
                viewingStatus = "Waiting",
                housingOfficer = "Wilhelm Stauffenberg",
                isWaitingForApproval = true
            });
            DummyData.Add(new DummyViewingsModel
            {
                compactDate = "Tomorrow",
                time = "13:30 (2hrs later)",
                customerName = "Fernando Trastamaria",
                propertyAddress = "4 Middleton Crescent Cortessy",
                isMale = true,
                assignedBy = "Me",
                completeDate = "Mar 5,2016  (Today)",
                propertyReferenceNumber = "BHA0000000836ZA",
                propertyRent = "$500/month",
                propertyType = "Flat",
                propertyRooms = "2",
                customerContactNumber = "07345 7777892",
                customerDateOfBirth = "Jan 12, 1978",
                timeCreated = "12:00",
                propertyThumbnail = "/Assets/dummy_property.png",
                dateCreated = "Feb 2, 2016",
                viewingStatus = "Waiting",
                housingOfficer = "Wilhelm Stauffenberg",
                isWaitingForApproval = true
            });
            DummyData.Add(new DummyViewingsModel
            {
                compactDate = "Tomorrow",
                time = "13:30 (2hrs later)",
                customerName = "Fernando Trastamaria",
                propertyAddress = "4 Middleton Crescent Cortessy",
                isMale = true,
                assignedBy = "Me",
                completeDate = "Mar 5,2016  (Today)",
                propertyReferenceNumber = "BHA0000000836ZA",
                propertyRent = "$500/month",
                propertyType = "Flat",
                propertyRooms = "2",
                customerContactNumber = "07345 7777892",
                customerDateOfBirth = "Jan 12, 1978",
                timeCreated = "12:00",
                propertyThumbnail = "/Assets/dummy_property.png",
                dateCreated = "Feb 2, 2016",
                viewingStatus = "Waiting",
                housingOfficer = "Wilhelm Stauffenberg",
                isWaitingForApproval = true
            });
            DummyData.Add(new DummyViewingsModel
            {
                compactDate = "25 Mar",
                time = "13:30 (2hrs later)",
                customerName = "Fernando Trastamaria",
                propertyAddress = "4 Middleton Crescent Cortessy",
                isMale = true,
                assignedBy = "Me",
                completeDate = "Mar 5,2016  (Today)",
                propertyReferenceNumber = "BHA0000000836ZA",
                propertyRent = "$500/month",
                propertyType = "Flat",
                propertyRooms = "2",
                customerContactNumber = "07345 7777892",
                customerDateOfBirth = "Jan 12, 1978",
                timeCreated = "12:00",
                propertyThumbnail = "/Assets/dummy_property.png",
                dateCreated = "Feb 2, 2016",
                viewingStatus = "Waiting",
                housingOfficer = "Wilhelm Stauffenberg",
                isWaitingForApproval = true
            });
            DummyData.Add(new DummyViewingsModel
            {
                compactDate = "27 Mar",
                time = "13:30 (2hrs later)",
                customerName = "Fernando Trastamaria",
                propertyAddress = "4 Middleton Crescent Cortessy",
                isMale = true,
                assignedBy = "Me",
                completeDate = "Mar 5,2016  (Today)",
                propertyReferenceNumber = "BHA0000000836ZA",
                propertyRent = "$500/month",
                propertyType = "Flat",
                propertyRooms = "2",
                customerContactNumber = "07345 7777892",
                customerDateOfBirth = "Jan 12, 1978",
                timeCreated = "12:00",
                propertyThumbnail = "/Assets/dummy_property.png",
                dateCreated = "Feb 2, 2016",
                viewingStatus = "Waiting",
                housingOfficer = "Wilhelm Stauffenberg",
                isWaitingForApproval = true
            });
            DummyData.Add(new DummyViewingsModel
            {
                compactDate = "Past Viewings",
                time = "16:30 (4hrs later)",
                customerName = "Chris Adams",
                propertyAddress = "4 Middleton Crescent Cortessy",
                isMale = true,
                assignedBy = "Me",
                completeDate = "Mar 5,2016  (Today)",
                propertyReferenceNumber = "BHA0000000836ZA",
                propertyRent = "$500/month",
                propertyType = "Flat",
                propertyRooms = "2",
                customerContactNumber = "07345 7777892",
                customerDateOfBirth = "Jan 12, 1978",
                timeCreated = "12:00",
                propertyThumbnail = "/Assets/dummy_property.png",
                dateCreated = "Feb 2, 2016",
                viewingStatus = "Rejected",
                housingOfficer = "Shizun Mitsubishi",
                isWaitingForApproval = false

            });
            return DummyData;
        }
    }
}
