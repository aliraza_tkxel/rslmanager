﻿using System.Collections.Generic;

namespace FrontlineApp.ApplicationLayer.Models
{
    public class DummyPropertyViewingModel
    {
        public string dateAndTime { get; set; }
        public string customerName { get; set; }
        public string customerId { get; set; }
        public string housingOfficerName { get; set; }
    }
    public class DummyPropertyViewingManager
    {
        public static List<DummyPropertyViewingModel> GetCustomerViewingData()
        {
            var DummyData = new List<DummyPropertyViewingModel>();
            DummyData.Add(new DummyPropertyViewingModel
            {
                dateAndTime = "Feb 23 - 13:23",
                customerName = "Ayhan Ozturk",
                customerId = "CN00033212",
                housingOfficerName = "Jeff Atwood"
            });
            DummyData.Add(new DummyPropertyViewingModel
            {
                dateAndTime = "Feb 12 - 15:23",
                customerName = "Marvin Eriksen",
                customerId = "CN02332352",
                housingOfficerName = "Brian Adams"
            });
            DummyData.Add(new DummyPropertyViewingModel
            {
                dateAndTime = "Mar 23 - 23:23",
                customerName = "Lev Shamon",
                customerId = "CN00003332",
                housingOfficerName = "Jennifer Gustavo"
            });
            DummyData.Add(new DummyPropertyViewingModel
            {
                dateAndTime = "Jan 4 - 11:33",
                customerName = "Mark Piers",
                customerId = "CN000038652",
                housingOfficerName = "David Jefferson"
            });
            return DummyData;
        }
    }
}
