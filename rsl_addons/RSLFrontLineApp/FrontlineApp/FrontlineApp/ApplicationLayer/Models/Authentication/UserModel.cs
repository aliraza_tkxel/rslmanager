﻿namespace FrontlineApp.ApplicationLayer.Models
{
    public class UserModel:BaseModel
    {
        public int userId { get; set; }
        public string employeeFullName { get; set; }
        public int isActive { get; set; }
        private string _userName;
        public string UserName
        {
            get
            {
                return this._userName;
            }
            set
            {
                Set(() => UserName, ref _userName, value); 
            }
        }
        private string _email;
        public string Email
        {
            get
            {
                return this._email;
            }
            set
            {
                Set(() => Email, ref _email, value);
            }
        }
        private string _password;
        
        public string Password
        {
            get
            {
                return this._password;
            }
            set
            {
                Set(() => Password, ref _password, value);
            }
        }
        
        public bool IsLoggedIn { get; set; } 
        public int TeamID { get; set; }
        
        public UserModel()
        {
            userId = 0;
            employeeFullName = "";
            isActive = 0;
            IsLoggedIn = false;
            TeamID = 0;
            _email = "";
            Email = "";
            UserName = "";
            Password = "";
            _password = "";
            _userName = "";

            
        }
        
    }
}
