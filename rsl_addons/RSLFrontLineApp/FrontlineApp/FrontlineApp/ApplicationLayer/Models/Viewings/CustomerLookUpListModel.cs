﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ApplicationLayer.Models
{
    public class CustomerLookUpListModel:BaseModel
    {
        private string _firstName;
        public string FirstName
        {
            get { return _firstName; }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    _firstName = "";
                }
                else
                {
                    _firstName = value;
                    Initials = _firstName[0].ToString();
                }
                
            }
        }
        private string _lastName;
        public string LastName
        {
            get { return _lastName; }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    _lastName = "";
                }
                else
                {
                    _lastName = value;
                    Initials = _lastName[0].ToString();
                }

            }
        }
        public int CustomerId { get; set; }
        public string CustomerPhoneNumber { get; set; }
        public string CustomerGender { get; set; }
        public DateTimeOffset? Dob { get; set; }
        private CustomerLookupImageModel customerImage;
        public CustomerLookupImageModel CustomerImage
        {
            get { return customerImage; }
            set { Set(() => CustomerImage, ref customerImage, value); }
        }
        private string _initials;
        public string Initials
        {
            set
            {
                _initials = Initials+value;
                
            }

            get
            {
                return _initials;
            }

        }
        public string CustomerType { get; set; }

    }
}
