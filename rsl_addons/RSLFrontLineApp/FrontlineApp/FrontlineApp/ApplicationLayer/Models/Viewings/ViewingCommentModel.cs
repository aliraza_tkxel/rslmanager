﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ApplicationLayer.Models
{
    public class ViewingCommentModel : BaseModel
    {
        public int CommentId { get; set; }
        public string CommentText { get; set; }
        public int? ViewingId { get; set; }
        public DateTime? CommentedDate { get; set; }
        public string FormattedDate { get; set; }
        public string CommentOwnerName { get; set; }
        public int? CommentTypeId { get; set; }
        public int? CommentedBy { get; set; }
    }
}
