﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ApplicationLayer.Models
{
    public class CustomerLookupImageModel:BaseModel
    {
        private int? id;
        public int? Id
        {
            get { return id; }
            set { Set(() => Id, ref id, value); }
        }

        private string imageName;
        public string ImageName
        {
            get { return imageName; }
            set
            {
                Set(() => ImageName, ref imageName, value);
                ImageSource = string.Format("{0}{1}", BaseAddress, ImageName);
            }
        }

        private string baseAddress;
        public string BaseAddress
        {
            get { return baseAddress; }
            set
            {
                Set(() => BaseAddress, ref baseAddress, value);
                ImageSource = string.Format("{0}{1}", BaseAddress, ImageName);
            }
        }

        private string imageSource;
        public string ImageSource
        {
            get { return imageSource; }
            set { Set(() => ImageSource, ref imageSource, value); }
        }
    }
}
