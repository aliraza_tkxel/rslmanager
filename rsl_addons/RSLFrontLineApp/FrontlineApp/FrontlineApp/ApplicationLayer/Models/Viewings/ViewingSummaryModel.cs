﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ApplicationLayer.Models
{
    public class ViewingSummaryModel : BaseModel
    {
        #region "Properties"
        
        public int Id { get; set; }
        public int ViewingId { get; set; }
        public string ViewingDate { get; set; }
        public DateTime ActualDate { get; set; }
        public string FormattedDate { get; set; }
        public string CompleteDateForDetails { get; set; }
        public string FormattedDateTime { get; set; }
        public string FormattedTime { get; set; }
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerGender { get; set; }
        public string HousingOfficerGender { get; set; }
        public string HousingOfficerEmail { get; set; }
        public int ViewingStatusId { get; set; }
        public string ViewingStatusDescription { get; set; }
        public int HousingOfficerId { get; set; }
        public string AssignedTo { get; set; }
        public int CreatedById { get; set; }
        public string CreatorName { get; set; }
        public string PropertyAddress { get; set; }
        public string OutlookIdentifier { get; set; }
        private string propertyThumbnail;
        public string PropertyThumbnail
        {
            get { return propertyThumbnail; }
            set { Set(() => PropertyThumbnail, ref propertyThumbnail, value); }
        }
        public string AssignedByName { get; set; }
        public int? AssignedById { get; set; }

        public string CreationDate { get; set; }
        public string FormattedCreationDate { get; set; }
        public string FormattedCreationTime { get; set; }
        public string PropertyId { get; set; }
        public string PropertyRent { get; set; }
        public string NoOfBedRooms { get; set; }
        public string PropertyType { get; set; }
        public string CustomerType { get; set; }
        public string MobileNumber { get; set; }
        public string CustomerDateOfBirth { get; set; }
        public string GroupKey { get; set; }
        public string Notes { get; set; }
        public bool ShouldDisplayCancelledButton
        {
            get
            {
                if (ViewingStatusId < 4)
                {
                    return true;
                }
                return false;
            }
        }
        public string FullCreationDateTime
        {
            get;set;
        }
        #endregion
    }
}
