﻿namespace FrontlineApp.ApplicationLayer.Models
{
    public class StaffMemberModel
    {
        public int EmployeeId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ImagePath { get; set; }
        public string Gender { get; set; }
        public string Initials { get; set; }
        public string Email { get; set; }
    }
}
