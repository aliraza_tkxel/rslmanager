﻿using System.ComponentModel;

namespace FrontlineApp.ApplicationLayer.Enums
{
    public enum CustomerType
    {
        /// <summary>
        /// CustomerType - Prospective Tenant
        /// </summary>
        [Description("Prospective Tenant")]
        ProspectiveTenant = 1,

        /// <summary>
        /// CustomerType - Tenant
        /// </summary>
        [Description("Tenant")]
        Tenant,

        /// <summary>
        /// CustomerType - Previous Tenant
        /// </summary>
        [Description("Previous Tenant")]
        PreviousTenant,

        /// <summary>
        /// CustomerType - Stakeholder
        /// </summary>
        [Description("Stakeholder")]
        Stakeholder,

        /// <summary>
        /// CustomerType - General
        /// </summary>
        [Description("General")]
        General

    }
}
