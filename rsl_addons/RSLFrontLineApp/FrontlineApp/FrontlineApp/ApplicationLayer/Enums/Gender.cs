﻿using System.ComponentModel;

namespace FrontlineApp.ApplicationLayer.Enums
{
    public enum Gender
    {
        /// <summary>
        /// Gender - Male
        /// </summary>
        [Description("Male")]
        Male = 1,

        /// <summary>
        /// Gender - Female
        /// </summary>
        [Description("Female")]
        Female,

        /// <summary>
        /// Gender - Transgender
        /// </summary>
        [Description("Transgender")]
        Transgender
    }
}
