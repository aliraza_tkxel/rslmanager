﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.ApplicationLayer.Enums
{
    public enum TenantItems
    {
        GeneralInfo,
        Viewings,
        Referral,
        Risks,
        Vulnerabilities
    }
}
