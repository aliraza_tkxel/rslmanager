﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.DataLayer.Repository;
using FrontlineApp.ServiceLayer.Services;
using FrontlineApp.Utility.Constants;
using FrontlineApp.Utility.Helper.AppSession;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.ObjectModel;
using FrontlineApp.DataLayer.Entities;
using FrontlineApp.ServiceLayer.ServiceObjects;
using FrontlineApp.Utility;
using System;

namespace FrontlineApp.BusinessLayer.Viewings
{
    public class ViewingsManager
    {
        #region "Properties"
        ViewingsService viewingService;
        ViewingsRepository viewingRepository;
        PropertyRepository propertyRepo;
        #endregion

        #region "Constructor"
        public ViewingsManager()
        {
            viewingService = new ViewingsService();
            viewingRepository = new ViewingsRepository();

        }
        #endregion

        #region "Get Customer List"

        public async Task<ResultModel<List<GroupInfoListModel>>> GetCustomerLookUpList(PaginationSO pagination, string searchText)
        {
            var isLookupRequired = false;
            viewingService = new ViewingsService();
            var serviceResult = await viewingService.SearchCustomerLookUp(pagination, searchText, isLookupRequired);
            var result = new ResultModel<List<GroupInfoListModel>>();
            if (serviceResult.Success)
            {
                viewingRepository = new ViewingsRepository();
                result.Value = viewingRepository.GetCustomerLookUpList();
                result.Success = true;
            }
            return result;
        }

        public async Task<ResultModel<List<CustomerModel>>> GetCustomerLookUpListAsCustomerModel(PaginationSO pagination, string searchText)
        {
            var isLookupRequired = false;
            viewingService = new ViewingsService();
            var serviceResult = await viewingService.SearchCustomerLookUp(pagination, searchText, isLookupRequired);
            var result = new ResultModel<List<CustomerModel>>();
            if (serviceResult.Success)
            {
                viewingRepository = new ViewingsRepository();
                result.Value = viewingRepository.GetCustomerLookUpListAsCustomerModel();
                result.Success = true;
            }
            return result;
        }

        public async Task<ResultModel<List<GroupInfoListModel>>> GetCustomerList(PaginationSO pagination, string searchText)
        {
            var isLookupRequired = true;//AppSession.IsSharedLookupsFetched ? false : true;

            CustomerService customerService = new CustomerService();
            var serviceResult = await customerService.SearchCustomer(pagination, searchText, isLookupRequired);
            var result = new ResultModel<List<GroupInfoListModel>>();
            if (serviceResult.Success)
            {
                CustomerRepository customerRepository = new CustomerRepository();
                result.Value =  customerRepository.GetGroupedCustomerList();
                result.Success = true;
            }
            else
            {
                result.Success = false;
                result.Error = serviceResult.Error;
            }

            return result;
        }
        #endregion

        #region "Get Staff Members List"

        public async Task<ResultModel<List<StaffMemberModel>>> GetStaffMembersList(StaffOfficerRequestSO reqObj)
        {
            var serviceResult = await viewingService.GetStaffMemberList(reqObj);
            var result = new ResultModel<List<StaffMemberModel>>();
            if (serviceResult.Success)
            {
                result.Value = viewingRepository.GetStaffMembersList();
                result.Success = true;
            }
            else
            {
                result.Success = false;
                result.Message = serviceResult.Message;
            }

            return result;
        }

        #endregion

        #region "Get Property List"
        public async Task<ResultModel<List<PropertyModel>>> GetPropertyList(string searchText)
        {
            // Check Shared lookups already fetched
            var isLookupRequired = true;//AppSession.IsPropertyLookupFetched ? false : true;
            propertyRepo = new PropertyRepository();
            ResultModel<bool> delRes = propertyRepo.DeleteAllProeprties();
            var result = new ResultModel<List<PropertyModel>>();
            if (delRes.Success)
            {
                PropertyService propertyService = new PropertyService();
                var serviceResult = await propertyService.GetAllProperties(isLookupRequired);

                if (serviceResult != null)
                {
                    PropertyRepository propertyRepository = new PropertyRepository();
                    result.Value = propertyRepository.GetPropertyList();
                    result.Success = true;
                }
                else
                {
                    result.Success = false;
                    result.Error = MessageConstants.ProblemDataSaving;
                }
            }
           

            return result;
        }
        #endregion
        #region "Add Comment"

        public async Task<ResultModel<bool>> AddCommentToViewing (ViewingSummaryModel viewing, string comment, string commentType)
        {
            var result = new ResultModel<bool>();
            viewingRepository = new ViewingsRepository();
            viewingService = new ViewingsService();

            LookupManager manager = new LookupManager();
            var types = manager.GetViewingCommentsTypes();

            var selectedType = types.Where(p => p.Description == commentType).ToList().FirstOrDefault();

            ViewingCommentSO requestSO = new ViewingCommentSO();
            requestSO.CommentId = 0;
            requestSO.CommentText = comment;
            requestSO.ViewingId = viewing.ViewingId;
            requestSO.CommentTypeId = selectedType.Id;
            requestSO.CommentedBy = AppSession.LoggedinUserId;
            requestSO.CommentedDate = DateTime.UtcNow;
            requestSO.isActive = true;

            ResultSO<AddViewingCommentResponseSO> serviceResult = await viewingService.AddViewingComment(requestSO);
            if (serviceResult.Success)
            {
                var dbRes = viewingRepository.UpdateViewingComments(serviceResult.Value.allComments, viewing);
                if (dbRes.Success)
                {
                    result.Success = true;
                    result.Message = serviceResult.Value.message;
                }
                else
                {
                    result.Success = false;
                    result.Message = dbRes.Error;
                }
                
            }
            else
            {
                result.Success = false;
                result.Message = "Failed to add feedback";
            }

            return result;
        }

        #endregion
        #region "Edit or Arrange Viewing"

        public async Task<ResultModel<bool>> EditViewingStatus (ViewingSummaryModel viewing, int status)
        {
            var result = new ResultModel<bool>();
            viewingRepository = new ViewingsRepository();
            viewingService = new ViewingsService();
            viewing.ViewingStatusId = status;
            if (ValidateSummaryInput(viewing))
            {
                ResultSO<ViewingSummary> serviceResult = await viewingService.ArrangeOrEditViewing(viewing, true);
                if (serviceResult.Success)
                {
                    result.Value = true;
                    result.Success = true;
                }
                else
                {
                    result.Success = false;
                    result.Value = false;
                    result.Message = "Operation Failed";
                }
            }
            else
            {
                result.Success = false;
                result.Message = "Invalid Parameters: Please try again later";
            }
            return result;

        }

        public async Task<ResultModel<bool>> ArrangeOrUpdateViewing(ViewingSummaryModel viewing)
        {
            var result = new ResultModel<bool>();
            if (ValidateSummaryInput(viewing))
            {
                viewingRepository = new ViewingsRepository();
                viewingService = new ViewingsService();
                ResultSO<ViewingSummary> serviceResult = await viewingService.ArrangeOrEditViewing(viewing, false);
                if (serviceResult.Success)
                {
                    result.Value =  true;
                    result.Success = true;
                }
                else
                {
                    result.Value = false;
                    result.Success = false;
                    result.Message = serviceResult.Message;
                }
            }
            else
            {
                result.Success = false;
                result.Message = "Please select all required parameters";
            }
            return result;
        }
        private bool ValidateSummaryInput(ViewingSummaryModel summary)
        {
            if(summary.CustomerId==0 || string.IsNullOrWhiteSpace(summary.PropertyId) || summary.HousingOfficerId == 0)
            {
                return false;
            }
            if(string.IsNullOrWhiteSpace(summary.ViewingDate)|| string.IsNullOrWhiteSpace(summary.FormattedTime) || summary.CreatedById == 0)
            {
                return false;
            }

            return true;
        }
        #endregion
        #region "Get Viewings Detail"
        public async Task<ResultModel<ViewingSummaryModel>> FetchDetailsForViewing(ViewingSummaryModel model)
        {
            var result = new ResultModel<ViewingSummaryModel>();
            viewingRepository = new ViewingsRepository();
            viewingService = new ViewingsService();
            bool serviceResult = await viewingService.GetViewingsDetailForViewing(model);
            if (serviceResult == true)
            {
                result.Value =  viewingRepository.GetViewingModel(model);
                result.Success = true;
            }
            else
            {
                result.Success = false;
                result.Error = MessageConstants.ProblemDataSaving;
            }
            return result;
        }
        #endregion
        #region "Get Viewings Lists"
        public async Task<ResultModel<List<ViewingSummaryModel>>> GetViewingsList()
        {
            var result = new ResultModel<List<ViewingSummaryModel>>();
            viewingRepository = new ViewingsRepository();
            viewingService = new ViewingsService();
            if (GeneralHelper.HasInternetConnection())
            {
                var delResult = viewingRepository.DeleteAllViewings();
                if (delResult.Success == true)
                {
                    bool serviceResult = await viewingService.GetAllViewings(AppSession.LoggedInUser.userId, true);
                    if (serviceResult == true)
                    {

                        result.Value = viewingRepository.GetViewingSummaryList();
                        result.Success = true;
                    }
                    else
                    {
                        result.Success = false;
                        result.Error = MessageConstants.ProblemDataSaving;
                    }
                }
                else
                {
                    result.Success = false;
                    result.Error = "Couldn't Delete redundant enteries";
                }
                
            }
            else
            {
                result.Value = viewingRepository.GetViewingSummaryList();
                result.Success = true;

            }

            return result;
        }

        public async Task<ObservableCollection<GroupInfoListModel>> GetActiveViewings()
        {
            var result = new ResultModel<List<ViewingSummaryModel>>();
            viewingRepository = new ViewingsRepository();
            result = await GetViewingsList();
            var filteredList = new List<ViewingSummaryModel>();
            if (result.Success)
            {
                
                filteredList = result.Value.Where(
                                        o => o.ViewingStatusId == ApplicationConstants.ViewingStatusAccepted
                                        && o.HousingOfficerId == AppSession.LoggedInUser.userId
                                        ).ToList();

                filteredList = filteredList.OrderBy(w => w.ActualDate).ToList();
            }
            
            result.Value = filteredList;
            ObservableCollection<GroupInfoListModel> groups = new ObservableCollection<GroupInfoListModel>();
            groups = SortViewingListIntoGroups(result);
            return groups;
            
        }

        public async Task<ObservableCollection<GroupInfoListModel>> GetAllViewingsInGroupedForm()
        {
            var result = new ResultModel<List<ViewingSummaryModel>>();
            viewingRepository = new ViewingsRepository();
            result = await GetViewingsList();
            var filteredList = new List<ViewingSummaryModel>();
            if (result.Success)
            {
                filteredList = result.Value.OrderBy(w => w.ActualDate).ToList();
            }

            result.Value = filteredList;
            ObservableCollection<GroupInfoListModel> groups = new ObservableCollection<GroupInfoListModel>();
            groups = SortViewingListIntoGroups(result);
            return groups;

        }


        public async Task<List<ViewingSummaryModel>> GetMyQueueViewingsList()
        {
            var result = new ResultModel<List<ViewingSummaryModel>>();
            viewingRepository = new ViewingsRepository();
            result = await GetViewingsList();
            var filteredList = new List<ViewingSummaryModel>();
            if (result.Success)
            {

                filteredList = result.Value.Where(
                                        o => o.ViewingStatusId == ApplicationConstants.ViewingStatusAccepted
                                        && o.HousingOfficerId == AppSession.LoggedInUser.userId
                                        ).ToList();

                filteredList = filteredList.OrderBy(w => w.ActualDate).ToList();
            }
            
            return filteredList;
        }

        public List<ViewingCommentModel> GetViewingCommentsFor(ViewingSummaryModel model)
        {
            
            viewingRepository = new ViewingsRepository();
            var commentList = viewingRepository.GetCommentsForViewing(model);
            var filteredList = new List<ViewingCommentModel>();
            if (commentList.Count>0)
            {

                filteredList = commentList.OrderBy(w => w.CommentedDate).ToList();
            }

            return filteredList;
        }

        public async Task<List<ViewingSummaryModel>> GetLoggedViewings()
        {
            var result = new ResultModel<List<ViewingSummaryModel>>();
            result = await GetViewingsList();
            var filteredList = new List<ViewingSummaryModel>();
            if (result.Success)
            { 
                filteredList = result.Value.Where(o => o.CreatedById == AppSession.LoggedInUser.userId).ToList();
                filteredList = filteredList.OrderBy(w => w.ActualDate).ToList();
            }
            
            return filteredList;

        }
        public async Task<List<ViewingSummaryModel>> GetAllViewings()
        {
            var result = new ResultModel<List<ViewingSummaryModel>>();
            result = await GetViewingsList();
            var filteredList = new List<ViewingSummaryModel>();
            viewingRepository = new ViewingsRepository();
            if (result.Success)
            {
                filteredList = result.Value.OrderBy(w => w.ActualDate).ToList();
            }
            return filteredList;
        }
        public async Task<List<ViewingSummaryModel>> GetTodaysViewings()
        {
            var result = new ResultModel<List<ViewingSummaryModel>>();
            result = await GetViewingsList();
            var filteredList = new List<ViewingSummaryModel>();
            viewingRepository = new ViewingsRepository();
            if (result.Success)
            {
               
                var query = result.Value.Where(
                                        o => o.ViewingStatusId == ApplicationConstants.ViewingStatusAccepted
                                        && o.HousingOfficerId == AppSession.LoggedInUser.userId
                                        && o.FormattedDate == ApplicationConstants.ViewingGroupToday
                                        );
                if (query.Count() > 0)
                {
                    filteredList = query.ToList();
                }
                filteredList = filteredList.OrderBy(w => w.ActualDate).ToList();
            }
            
            
            return filteredList;
        }
        public async Task<List<ViewingSummaryModel>> GetPendingViewings()
        {
            var result = new ResultModel<List<ViewingSummaryModel>>();
            result = await GetViewingsList();
            var filteredList = new List<ViewingSummaryModel>();
            if (result.Success)
            {
                filteredList = result.Value.Where(
                                    o => o.HousingOfficerId == AppSession.LoggedInUser.userId
                                    && o.ViewingStatusId == ApplicationConstants.ViewingStatusWaiting
                                    ).ToList();
                filteredList = filteredList.OrderBy(w => w.ActualDate).ToList();
            }
            return filteredList;

        }

        public ObservableCollection<GroupInfoListModel> SortViewingListIntoGroups(ResultModel<List<ViewingSummaryModel>> result)
        {
            ObservableCollection<GroupInfoListModel> groups = new ObservableCollection<GroupInfoListModel>();
            var query = from item in result.Value
                        group item by item.GroupKey into g
                        orderby g.Key
                        select new { GroupName = g.Key, Items = g };
            foreach (var g in query)
            {
                GroupInfoListModel info = new GroupInfoListModel();
                info.Key = g.GroupName;
                foreach (var item in g.Items)
                {
                    info.Add(item);
                }
                groups.Add(info);
            }
            ObservableCollection<GroupInfoListModel> sortedGroups = new ObservableCollection<GroupInfoListModel>();
           
            var linqResults = groups.Where(f => f.Key == ApplicationConstants.ViewingGroupToday);
            if (linqResults != null)
            {
                if (linqResults.FirstOrDefault() != null)
                {
                    sortedGroups.Add(linqResults.FirstOrDefault());
                }
                
            }
            linqResults = groups.Where(f => f.Key == ApplicationConstants.ViewingGroupTomorrow);
            if (linqResults != null)
            {
                if (linqResults.FirstOrDefault() != null)
                {
                    sortedGroups.Add(linqResults.FirstOrDefault());
                }
                    
            }
            linqResults = groups.Where(f => f.Key != ApplicationConstants.ViewingGroupToday && f.Key != ApplicationConstants.ViewingGroupTomorrow && f.Key!= ApplicationConstants.ViewingGroupPast);
            if (linqResults != null)
            {
                foreach(var x in linqResults)
                {
                    sortedGroups.Add(x);
                }
            }
            linqResults = groups.Where(f => f.Key == ApplicationConstants.ViewingGroupPast);
            if (linqResults != null)
            {
                if (linqResults.FirstOrDefault() != null)
                {
                    sortedGroups.Add(linqResults.FirstOrDefault());
                }

            }

            return sortedGroups;
        }

        public ObservableCollection<GroupInfoListModel> SortPropertyListIntoGroups(ResultModel<List<PropertyModel>> result)
        {
            ObservableCollection<GroupInfoListModel> groups = new ObservableCollection<GroupInfoListModel>();
            var query = from item in result.Value
                        group item by item.scheme into g
                        orderby g.Key
                        select new { GroupName = g.Key, Items = g };
            foreach (var g in query)
            {
                GroupInfoListModel info = new GroupInfoListModel();
                info.Key = g.GroupName;
                foreach (var item in g.Items)
                {
                    info.Add(item);
                }
                groups.Add(info);
            }
            return groups;
        }

        public ObservableCollection<GroupInfoListModel> SortStaffOfficersForList(List<StaffMemberModel> result)
        {
            
            ObservableCollection<GroupInfoListModel> groups = new ObservableCollection<GroupInfoListModel>();
            var query = from item in result
                        group item by item.FirstName[0].ToString() into g
                        orderby g.Key
                        select new { GroupName = g.Key, Items = g };
            foreach (var g in query)
            {
                GroupInfoListModel info = new GroupInfoListModel();
                info.Key = g.GroupName;
                foreach (var item in g.Items)
                {
                    info.Add(item);
                }
                groups.Add(info);
            }
            return groups;
        }




        #endregion
    }
}
