﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FrontlineApp.BusinessLayer.Viewings;
using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.Utility.Constants;

namespace FrontlineApp.BusinessLayer.Dashboard
{
    public class DashboardManager
    {
        #region "Properties"
        private ViewingsManager ViewingsManager { get; set; }
        #endregion
        #region "Constructor"
        public DashboardManager()
        {
            ViewingsManager = new ViewingsManager();
        }
        #endregion
        #region "Methods"
        public async Task<List<DashboardGridModel>> GetViewingsStats()
        {
            List<DashboardGridModel> viewingsStats = new List<DashboardGridModel>();
            var todaysCount = await ViewingsManager.GetTodaysViewings();
            var myViewingsCount = await ViewingsManager.GetMyQueueViewingsList();
            var approvedCount = await ViewingsManager.GetPendingViewings();
            var allCount = await ViewingsManager.GetAllViewings();
            DashboardGridModel TodaysViewings = new DashboardGridModel(ViewingsTypes.Today,todaysCount.Count(),true);
            DashboardGridModel LoggedViewings = new DashboardGridModel(ViewingsTypes.MyViewings, myViewingsCount.Count(),false);
            DashboardGridModel ApprovalViewings = new DashboardGridModel(ViewingsTypes.Pending, approvedCount.Count(),false);
            DashboardGridModel AllViewings = new DashboardGridModel(ViewingsTypes.All, allCount.Count(), false);
            viewingsStats.Add(LoggedViewings);
            viewingsStats.Add(TodaysViewings);
            viewingsStats.Add(ApprovalViewings);
            viewingsStats.Add(AllViewings);
            return viewingsStats;
        }

        public async Task<List<DashboardGridModel>> GetAlertsStats()
        {
            AlertsManager alertManger = new AlertsManager();
            List<DashboardGridModel> alertsStats = new List<DashboardGridModel>();
            var openRiskCount = await alertManger.GetOpenRiskCount();
            var openVulnerabilityCount = await alertManger.GetOpenVulnerabilityCount();
            DashboardGridModel openRisks = new DashboardGridModel() { cardTitle=ApplicationConstants.TitleOpenRisks, cardValue= openRiskCount.ToString(), CardBg = "/Assets/2x/dashboard_card_bg_red.png" };
            DashboardGridModel openVulnerability = new DashboardGridModel() {  cardTitle = ApplicationConstants.TitleOpenVulnerabilities, cardValue = openVulnerabilityCount.ToString(), CardBg = "/Assets/2x/dashboard_card_bg_blue.png" };
            alertsStats.Add(openRisks);
            alertsStats.Add(openVulnerability);
            return alertsStats;
        }

        #endregion
    }
}
