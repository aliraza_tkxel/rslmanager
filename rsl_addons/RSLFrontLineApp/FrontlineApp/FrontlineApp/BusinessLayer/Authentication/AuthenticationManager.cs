﻿using System;
using System.Threading.Tasks;
using FrontlineApp.ServiceLayer.Services;
using FrontlineApp.DataLayer.Entities;
using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.Utility.Helper;
using FrontlineApp.Utility.Helper.AppSession;
using FrontlineApp.DataLayer.Repository;
using System.Collections.Generic;
using System.Linq;
using FrontlineApp.ServiceLayer.ServiceObjects;

namespace FrontlineApp.BusinessLayer.Authentication
{
    public class AuthenticationManager
    {
        SharedLookUpsService lookupSv;
        HousingOfficerRepository housingOfficerRepo;
        
        public AuthenticationManager()
        {
            lookupSv = new SharedLookUpsService();
            housingOfficerRepo = new HousingOfficerRepository();
        }
        public async Task<ManagerResponseModel> ResetPasswordForEmail(string email)
        {
            ManagerResponseModel responseObj = new ManagerResponseModel();
            InputVerifier inputVerifier = new InputVerifier();
            if (inputVerifier.IsValidEmail(email))
            {
                AuthenticationService AuthServ = new AuthenticationService();
               
                bool hasResetPassword = await AuthServ.ForgotPasswordForEmail(email);
                if (hasResetPassword == true)
                {
                    responseObj = new ManagerResponseModel("An email with instructions for resetting password has been sent to your email address", true, null);
                }
                else
                {
                    responseObj = new ManagerResponseModel("Your email address does not match any of our records, please try again.", false, null);
                }
            }
            else
            {
                responseObj = new ManagerResponseModel("Please enter a valid email", false, null);
            }
            return responseObj;

        }
        public async Task<ManagerResponseModel> GetSharedLookUps()
        {
            bool result = await lookupSv.GetSharedLookUpTables();
            ManagerResponseModel responseObj = new ManagerResponseModel();
            responseObj = new ManagerResponseModel(result?"":"Couldn't retrieve/write shared lookup tables", result, null);
            return responseObj;
        }
        public async Task<ManagerResponseModel> LoginUserForParams(string userName, string password)
        {
            ManagerResponseModel responseObj = new ManagerResponseModel();
            try
            {
                if (!string.IsNullOrWhiteSpace(userName) && !string.IsNullOrWhiteSpace(password))
                {
                    
                    AuthenticationService AuthServ = new AuthenticationService();
                    ResultSO<HousingOfficer> houseOff = await AuthServ.LoginUserWithParams(userName,password);
                    if (houseOff.Success==false)
                    {
                        responseObj = new ManagerResponseModel(houseOff.Message, false, null);
                    }
                    else
                    {
                        responseObj = new ManagerResponseModel("", true, houseOff);
                        AppSession.LoggedInUser = houseOff.Value;
                        AppSession.LoggedinUserId = houseOff.Value.userId;
                    }
                }
                else
                {
                    responseObj = new ManagerResponseModel("Please enter valid username and password", false, null);
                }
            }
            catch (Exception exception)
            {
                responseObj = new ManagerResponseModel(exception.Message, false, null);
            }
            return responseObj;
        }

        public ManagerResponseModel LoginUserOfflineForParams(string userName, string password)
        {
            ManagerResponseModel responseObj = new ManagerResponseModel();
            try
            {
                if (!string.IsNullOrWhiteSpace(userName) && !string.IsNullOrWhiteSpace(password))
                {
                    List<HousingOfficer> housingOfficersList = housingOfficerRepo.GetHousingOfficers();
                    HousingOfficer officer = housingOfficersList.Where(x => x.userName == userName && x.Password == password).FirstOrDefault();
                    if (officer == null)
                    {
                        responseObj = new ManagerResponseModel("Your username or password is incorrect", false, null);
                    }
                    else
                    {
                        responseObj = new ManagerResponseModel("", true, officer);
                        AppSession.LoggedInUser = officer;
                        AppSession.LoggedinUserId = officer.userId;
                    }
                }
                else
                {
                    responseObj = new ManagerResponseModel("Please enter valid username and password", false, null);
                }
            }
            catch (Exception exception)
            {
                responseObj = new ManagerResponseModel(exception.Message, false, null);
            }
            return responseObj;
        }

    }
}
