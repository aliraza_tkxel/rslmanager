﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.DataLayer.Repository;
using FrontlineApp.ServiceLayer.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.BusinessLayer
{
    public class AlertsManager
    {
        #region "Properties"
        AlertsService alertsService;
        AlertsRepository alertRepository;
        #endregion

        #region "Constructor"
        public AlertsManager()
        {
            alertsService = new AlertsService();
            alertRepository = new AlertsRepository();
        }
        #endregion

        #region "Get Open Risk Count"
        public async Task<int> GetOpenRiskCount()
        {

            ResultModel<int> result = new ResultModel<int>();
            result.Value = 0;
            var response = await alertsService.GetOpenRisks();

            if (response.Success)
            {
                result = alertRepository.GetOpenRisksCounts();
            }
            else
            {
                result.Success = false;
                result.Message = response.Message;
                result.Error = response.Error;
            }

            return result.Value;
        }
        #endregion

        #region "Get Open Vulnerability Count"
        public async Task<int> GetOpenVulnerabilityCount()
        {

            ResultModel<int> result = new ResultModel<int>();
            result.Value = 0;
            var response = await alertsService.GetOpenVulnerabilities();
            if (response.Success)
            {
                result = alertRepository.GetOpenVulnerabilityCounts();
            }
            else
            {
                result.Success = false;
                result.Message = response.Message;
                result.Error = response.Error;
            }

            return result.Value;
        }
        #endregion

    }
}
