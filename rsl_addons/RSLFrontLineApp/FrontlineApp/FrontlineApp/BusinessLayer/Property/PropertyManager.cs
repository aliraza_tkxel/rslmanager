﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FrontlineApp.ServiceLayer.Services;
using FrontlineApp.ServiceLayer.ServiceObjects;
using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.DataLayer.Entities;
using System.Collections.ObjectModel;
using FrontlineApp.DataLayer.Repository;
using FrontlineApp.Utility.Helper.AppSession;

namespace FrontlineApp.BusinessLayer.PropertyManager
{
    public class PropertyManager
    {
        PropertyRepository propRepositiory;
        PropertyService propService;
        public PropertyManager()
        {

        }
        public async Task<ManagerResponseModel> GetAllProperties()
        {
            propRepositiory = new PropertyRepository();
            ManagerResponseModel responseObj = new ManagerResponseModel();
            ResultModel<bool> delRes = propRepositiory.DeleteAllProeprties();
            List<PropertyModel> PropertyModelList = new List<PropertyModel>();
            if (delRes.Success == true)
            {
                // Check Property lookups already fetched
                var isLookupRequired = true;//AppSession.IsPropertyLookupFetched ? false : true;

                PropertyService propServ = new PropertyService();
                PropertySearchResultSO searchResult = await propServ.GetAllProperties(isLookupRequired);
                if (searchResult == null)
                {
                    responseObj = new ManagerResponseModel("Failed to retrieve properties from server", false, PropertyModelList);
                    return responseObj;
                }
                PropertyModelList = propRepositiory.GetPropertyList();
            }
           
            responseObj = new ManagerResponseModel("", true, PropertyModelList);
            return responseObj;
        }
        public ManagerResponseModel ApplyFilter(PropertyFilterModel FilterObj, List<PropertyModel> PropertyModelList)
        {
            ManagerResponseModel responseObj = new ManagerResponseModel();
            List<PropertyModel> q = new List<PropertyModel>(PropertyModelList);
            if (FilterObj.isBedroomApplicable)
            {
                q = q.Where(t => t.bedRooms == FilterObj.bedRooms.ToString()).ToList();
            }
            if (FilterObj.isMaxRentApplicable)
            {
                q = q.Where(t => t.rentNumeric <= FilterObj.maxRent).ToList();
            }
            if (FilterObj.isMinRentApplicable)
            {
                q = q.Where(t => t.rentNumeric >= FilterObj.minRent).ToList();
            }
            if (FilterObj.isSchemeApplicable)
            {
                q = q.Where(t => t.schemeId == FilterObj.schemeId).ToList();
            }
            if (FilterObj.isPropertyTypeApplicable)
            {
                q = q.Where(t => t.propertyTypeId == FilterObj.propertyTypeId).ToList();
            }
            if (FilterObj.isPostalCodeApplicable)
            {
                q = q.Where(t => t.postalCode == FilterObj.postalCode).ToList();
            }
            
            responseObj = new ManagerResponseModel("", true, q);
            return responseObj;
        }
        public  ManagerResponseModel SearchObjectsByCustomQuery(string args)
        {
            ManagerResponseModel responseObj = new ManagerResponseModel();
            List<PropertyModel> q =  propRepositiory.GetPropertyList();
            q = q
               .Where(c => (c.scheme.IndexOf(args, StringComparison.CurrentCultureIgnoreCase) > -1) || (c.address.IndexOf(args, StringComparison.CurrentCultureIgnoreCase) > -1)).ToList();

            responseObj = new ManagerResponseModel("", true, q);

            return responseObj;
        }
        public async Task<ManagerResponseModel> GetPropertyDetailsForId(string PropertyId)
        {
            ManagerResponseModel responseObj = new ManagerResponseModel();
            Repository<PropertyDetails> myRepo = new Repository<PropertyDetails>();
            PropertyDetails returnedObj;
            PropertyDetailsModel SelectedProperty =  new PropertyDetailsModel();
            PropertyService propServ = new PropertyService();
            bool isFetched = await propServ.GetPropertyDetailsForId(PropertyId);
            if (isFetched)
            {
                returnedObj =  myRepo.Get(PropertyId);
                SelectedProperty = MapPropertyObjectToModelFor(returnedObj);
                responseObj = new ManagerResponseModel("", true, SelectedProperty);

            }
            else
            {
                responseObj = new ManagerResponseModel("Failed to retrieve details from server", false, SelectedProperty);
            }
            return responseObj;
        }
        public async Task<List<PropertyViewingModel>> GetPropertyViewingsListForId(string propertyId)
        {
            var result = new List<PropertyViewingModel>();
            propRepositiory = new PropertyRepository();
            var delResult = propRepositiory.DeleteAllPropertyViewings();
            if (delResult.Success)
            {
                result = propRepositiory.GetPropertyViewingList(propertyId);
                if (result.Count == 0)
                {
                    propService = new PropertyService();
                    bool servResult = await propService.GetPropertyViewingsForId(propertyId);
                    result = propRepositiory.GetPropertyViewingList(propertyId);
                }
            }
            
            return result;
        }
        
        private PropertyDetailsModel MapPropertyObjectToModelFor(PropertyDetails propObj)
        {
            PropertyDetailsModel model = new PropertyDetailsModel();
            propRepositiory = new PropertyRepository();
            model = propRepositiory.GetPropertyDetailsWithId(propObj.propertyId);
            return model;
        }
    }
}
