﻿using AutoMapper;
using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.DataLayer.Repository;
using FrontlineApp.ServiceLayer.Services;
using FrontlineApp.Utility.Constants;
using FrontlineApp.Utility.Helper.AppSession;
using FrontlineApp.ServiceLayer.ServiceObjects;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FrontlineApp.Utility;
using FrontlineApp.BusinessLayer.Viewings;

namespace FrontlineApp.BusinessLayer
{
    public class CustomerManager
    {
        #region "Properties"
        CustomerService customerService;
        CustomerRepository customerRepository;
        #endregion

        #region "Constructor"
        public CustomerManager()
        {
            customerService = new CustomerService();
            customerRepository = new CustomerRepository();
        }
        #endregion

        #region "Search Customer"
        /// <summary>
        /// 1- Check Internet Availability      
        /// 2- Check Offline records and sync if any exists
        /// 3- Fetch customer list from server
        /// 4- Delete old local db records
        /// </summary>
        /// <param name="pagination"></param>
        /// <param name="searchText"></param>
        /// <returns></returns>
        public async Task<ResultModel<List<GroupInfoListModel>>> SearchCustomer(PaginationModel pagination, string searchText)
        {

            ResultModel<bool> result = new ResultModel<bool>();

            // Check Internet availability
            if (GeneralHelper.HasInternetConnection() == true)
            {
                // Sync offline records 
                result = await SyncOfflineCustomerRecords();

                // If sync successful fetch updated customer list from server
                if (result.Success == true)
                {
                    result = await FetchCustomerListingFromServer(pagination, searchText);
                }
            }
            // If internet not available fetch local db records 
            else
            {
                result.Success = true;
            }

            var dbResult = new ResultModel<List<GroupInfoListModel>>();
            // If records fetched successfully from server get records from local db
            if (result.Success)
            {
                dbResult.Value = customerRepository.GetGroupedCustomerList();
                dbResult.Success = true;
            }
            else
            {
                dbResult.Success = false;
                dbResult.Message = result.Message;
                dbResult.Error = result.Error;
            }

            return dbResult;
        }
        #endregion

        #region "Fetch Customer list from local database"
        public ResultModel<List<GroupInfoListModel>> FetchCustomerListFromLocalDb(PaginationModel pagination, string searchText)
        {
            var dbResult = new ResultModel<List<GroupInfoListModel>>();
            dbResult.Value = customerRepository.GetGroupedCustomerList();
            dbResult.Success = true;

            return dbResult;
        }
        #endregion

        #region "Sync Offline customer records"
        public async Task<ResultModel<bool>> SyncOfflineCustomerRecords()
        {
            ResultModel<bool> result = new ResultModel<bool>();

            // Check Offline record exist, If exists then sync to server
            List<Customer> unSyncedCustomers = customerRepository.GetUnSyncedCustomers();
            bool isSuccess = true;
            if (unSyncedCustomers.Count() > 0)
            {
                foreach (var customer in unSyncedCustomers)
                {
                    var serverResult = await customerService.SaveCustomer(customer.id);
                    if (serverResult.Success == false)
                    {
                        isSuccess = false;
                        break;
                    }
                }
            }

            if (isSuccess == false)
            {
                result.Success = false;
                result.Message = MessageConstants.FailedToSyncCustomer;
            }
            else
            {
                result.Success = true;
            }

            return result;
        }
        #endregion

        #region "Check offline customer records exists"
        public bool CheckOfflineCustomerRecordsExists()
        {
            // Check Offline record exist
            List<Customer> unSyncedCustomers = customerRepository.GetUnSyncedCustomers();
            bool exist = false;
            if (unSyncedCustomers.Count() > 0)
            {
                exist = true;
            }
            else
            {
                exist = false;
            }

            return exist;
        }
        #endregion

        #region "Fetch customer listing from server"
        public async Task<ResultModel<bool>> FetchCustomerListingFromServer(PaginationModel pagination, string searchText)
        {
            ResultModel<bool> result = new ResultModel<bool>();

            // If no Offline record exist then fetch results from server
            // Check Customer lookups already fetched
            var isLookupRequired = AppSession.IsCustomerLookupFetched ? false : true;

            // Fetch Customer list from server 
            var paginationSO = Mapper.Map<PaginationModel, PaginationSO>(pagination);
            var serviceResult = await customerService.SearchCustomer(paginationSO, searchText, isLookupRequired);

            return serviceResult;

        }

        public async Task<ResultModel<List<CustomerModel>>> FetchCustomerLookUpList(PaginationModel pagination, string searchText)
        {
            //ResultModel<List<CustomerModel>> result = new ResultModel<List<CustomerModel>>();

            //// If no Offline record exist then fetch results from server
            //// Check Customer lookups already fetched
            //var isLookupRequired = AppSession.IsCustomerLookupFetched ? false : true;

            //// Fetch Customer list from server 
            //var paginationSO = Mapper.Map<PaginationModel, PaginationSO>(pagination);
            //var serviceResult = await customerService.GetCustomerLookUpList(paginationSO, searchText, isLookupRequired);

            //return serviceResult;
            var paginationSO = Mapper.Map<PaginationModel, PaginationSO>(pagination);
            ViewingsManager manager = new ViewingsManager();
            ResultModel<List<CustomerModel>> result = await manager.GetCustomerLookUpListAsCustomerModel(paginationSO, searchText);
            return result;
        }

        #endregion

        #region "Save Customer"
        public async Task<ResultModel<CustomerModel>> SaveCustomer(CustomerModel customer)
        {
            // Save customer info in local db
            // If saved successfully check internet availability
            // else show message

            ResultModel<CustomerModel> result = new ResultModel<CustomerModel>();
            var dbResult = customerRepository.SaveCustomerInfo(customer);
            if (dbResult.Success)
            {
                // Check internet availability
                // If available save customer on server
                // Else show message
                if (GeneralHelper.HasInternetConnection() == true)
                {
                    var localcustomer = dbResult.Value as CustomerModel;
                    var serverResult = await customerService.SaveCustomer(localcustomer.Id);

                    if (serverResult.Success == false)
                    {
                        result.Message = MessageConstants.CustomerSavedOfflineFailedToSyncedWithServer;
                    }                    
                }
                else
                {
                    result.Message = MessageConstants.CustomerSavedOffline;
                }
                result.Success = true;
                result.Value = dbResult.Value;
            }
            else
            {
                result.Success = false;
                result.Message = dbResult.Message;
            }

            return result;
        }
        #endregion

        #region "Get customer detail"
        public async Task<ResultModel<CustomerModel>> GetCustomerDetail(CustomerModel customer)
        {

            ResultModel<CustomerModel> result = new ResultModel<CustomerModel>();

            var customerSo = Mapper.Map<CustomerModel, CustomerSO>(customer);
            var response = await customerService.GetCustomerDetail(customer.Id, customerSo);

            if (response.Success)
            {
                result =  customerRepository.GetCustomerGeneralInfo(response.Value);
            }
            else
            {
                result.Success = false;
                result.Message = response.Message;
                result.Error = response.Error;
            }

            return result;
        }
        #endregion

        #region "Get customer viewings"
        public async Task<ResultModel<List<GroupInfoListModel>>> GetCustomerViewings(CustomerModel customer)
        {

            ResultModel<List<GroupInfoListModel>> result = new ResultModel<List<GroupInfoListModel>>();
            var response = await customerService.GetCustomerViewings(customer);

            if (response.Success)
            {
                result = customerRepository.GetGroupedCustomerViewingList(customer);
            }
            else
            {
                result.Success = false;
                result.Message = response.Message;
                result.Error = response.Error;
            }

            return result;
        }
        #endregion

        #region "Get customer referral"
        public async Task<ResultModel<ReferralModel>> GetCustomerReferral(CustomerModel customer)
        {

            ResultModel<ReferralModel> result = new ResultModel<ReferralModel>();
            var response = await customerService.GetCustomerReferral(customer);

            if (response.Success)
            {
                result = customerRepository.GetCustomerReferral(customer);
            }
            else
            {
                result.Success = false;
                result.Message = response.Message;
                result.Error = response.Error;
            }

            return result;
        }
        #endregion

        #region "Save customer referral"
        public async Task<ResultModel<ReferralModel>> SaveCustomerReferral(CustomerModel customer)
        {

            ResultModel<ReferralModel> result = new ResultModel<ReferralModel>();
            var serviceResponse = await customerService.SaveCustomerReferral(customer);

            if (serviceResponse.Success)
            {
                result = customerRepository.GetCustomerReferral(customer);
            }
            else
            {
                result.Success = false;
                result.Message = serviceResponse.Message;
                result.Error = serviceResponse.Error;
            }

            return result;
        }
        #endregion

        #region "Close Risk Enquiry"
        public async Task<ResultModel<bool>> CloseRiskEnquiry(CustomerRiskModel risk)
        {
            return await customerService.CloseRiskEnquiry(risk); ;
        }
        #endregion

        #region "Close Vulnerability Enquiry"
        public async Task<ResultModel<bool>> CloseVulnerabilityEnquiry(CustomerVulnerabilityModel vulnerability)
        {
            return await customerService.CloseVulnerabilityEnquiry(vulnerability); ;
        }
        #endregion

        #region "Terminate Tenancy"
        public async Task<ResultModel<TerminationSO>> TerminateTenancy(CustomerModel customer,TerminationModel termination)
        {
            var serviceResponse = await customerService.TerminateTenancy(customer, termination);
            return serviceResponse;
        }
        #endregion

        #region "Void Inspection"
        public async Task<VoidInspectionSupervisorsResponseSO> FetchVoidSupervisors(VoidInspectionSupervisorsRequestSO request)
        {
            var serviceResponse = await customerService.GetVoidInspectionSupervisors(request);
            return serviceResponse;
        }

        public async Task<VoidInspectionRootResponseSO> CreateVoidInspection(CreateVoidInspectionRequestSO request)
        {
            var serviceResponse = await customerService.CreateVoidInspection(request);
            return serviceResponse;
        }

        #endregion

        #region "Get customer risks"
        public async Task<ResultModel<List<CustomerRiskModel>>> GetCustomerRisks(CustomerModel customer)
        {

            ResultModel<List<CustomerRiskModel>> result = new ResultModel<List<CustomerRiskModel>>();
            var response = await customerService.GetCustomerRisks(customer);

            if (response.Success)
            {
                result = customerRepository.GetCustomerRisksList(customer);
            }
            else
            {
                result.Success = false;
                result.Message = response.Message;
                result.Error = response.Error;
            }

            return result;
        }
        #endregion

        #region "Get customer vulnerabilities"
        public async Task<ResultModel<List<CustomerVulnerabilityModel>>> GetCustomerVulnerabilities(CustomerModel customer)
        {

            ResultModel<List<CustomerVulnerabilityModel>> result = new ResultModel<List<CustomerVulnerabilityModel>>();
            var response = await customerService.GetCustomerVulnerabilities(customer);

            if (response.Success)
            {
                result = customerRepository.GetCustomerVulnerabilityList(customer);
            }
            else
            {
                result.Success = false;
                result.Message = response.Message;
                result.Error = response.Error;
            }

            return result;
        }
        #endregion
    }
}
