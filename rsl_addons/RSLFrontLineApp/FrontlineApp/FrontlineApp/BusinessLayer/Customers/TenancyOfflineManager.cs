﻿using AutoMapper;
using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.DataLayer.OfflineTenancyEntities;
using FrontlineApp.DataLayer.Repository.Customers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.BusinessLayer
{
    public class TenancyOfflineManager
    {
        #region "Properties"
        TenancyOfflineRepository TenancyRepository {get;set;}
        public LookupManager LookupManager { get; set; }
        #endregion
        #region "Constructor"
        public TenancyOfflineManager()
        {
            TenancyRepository = new TenancyOfflineRepository();
            LookupManager = new LookupManager();
        }
        #endregion
        #region "Save Tenancy"
        public ResultModel<bool> SaveTenancyOfflineEntity(TenancyModel TenancyObj, TenancyRootEnt root, PropertyModel SelectedProperty, CustomerModel SelectedCustomer)
        {
            if (SelectedProperty != null)
            {
                TenancyPropertyEnt propertyobj = new TenancyPropertyEnt();
                propertyobj = Mapper.Map<PropertyModel, TenancyPropertyEnt>(SelectedProperty);
                root.Property = propertyobj;
            }
            /*No Auto Mapper for Customer*/
            #region "Configure Customer Manually"
            if (SelectedCustomer != null)
            {
                TenancyCustomerEnt custobj = new TenancyCustomerEnt();
                custobj.CustomerId = SelectedCustomer.CustomerId;
                custobj.CustomerNumber = SelectedCustomer.CustomerNumber;
                custobj.CustomerType = SelectedCustomer.CustomerType;
                custobj.CustomerTypeId = SelectedCustomer.CustomerTypeId;
                custobj.TenancyCount = SelectedCustomer.TenancyCount;
                custobj.IsJointTenant = SelectedCustomer.IsJointTenant;
                custobj.TitleId = SelectedCustomer.TitleId;
                custobj.FirstName = SelectedCustomer.FirstName;
                custobj.MiddleName = SelectedCustomer.MiddleName;
                custobj.LastName = SelectedCustomer.LastName;
                custobj.PreferredContact = SelectedCustomer.PreferredContact;
                custobj.PreferedContactId = SelectedCustomer.PreferedContactId;
                custobj.InternetAccess = SelectedCustomer.InternetAccess;
                custobj.InternetAccessDescription = SelectedCustomer.InternetAccessDescription;
                custobj.Dob = SelectedCustomer.Dob;
                custobj.Gender = SelectedCustomer.Gender;
                custobj.MaritalStatus = SelectedCustomer.MaritalStatus;
                custobj.MaritalStatusId = SelectedCustomer.MaritalStatusId;
                custobj.Ethnicity = SelectedCustomer.Ethnicity;
                custobj.EthnicityId = SelectedCustomer.EthnicityId;
                custobj.Religion = SelectedCustomer.Religion;
                custobj.ReligionId = SelectedCustomer.ReligionId;
                custobj.SexualOrientation = SelectedCustomer.SexualOrientation;
                custobj.SexualOrientationId = SelectedCustomer.SexualOrientationId;
                custobj.NiNumber = SelectedCustomer.NiNumber;
                custobj.Nationality = SelectedCustomer.Nationality;
                custobj.NationalityId = SelectedCustomer.NationalityId;
                custobj.FirstLanguage = SelectedCustomer.FirstLanguage;
                custobj.IsSubjectToImmigration = SelectedCustomer.IsSubjectToImmigration;
                custobj.SubjectToImmigration = SelectedCustomer.SubjectToImmigration;
                custobj.IsPermanentUKResident = SelectedCustomer.IsPermanentUKResident;
                custobj.PermanentUKResident = SelectedCustomer.PermanentUKResident;
                custobj.ResidencyStatus = SelectedCustomer.ResidencyStatus;
                custobj.Communication = SelectedCustomer.Communication;
                custobj.GroupKey = SelectedCustomer.GroupKey;
                //custobj.CommunicationList = (SelectedCustomer.CommunicationList.Count>0)?string.Join(",", SelectedCustomer.CommunicationList):"";
                custobj.AppVersion = SelectedCustomer.AppVersion;
                custobj.CreatedBy = SelectedCustomer.CreatedBy;
                custobj.IsTenant = SelectedCustomer.IsTenant;
                custobj.CreatedOnApp = SelectedCustomer.CreatedOnApp;
                custobj.CreatedOnServer = SelectedCustomer.CreatedOnServer;
                custobj.LastModifiedOnApp = SelectedCustomer.LastModifiedOnApp;
                custobj.LastModifiedOnServer = SelectedCustomer.LastModifiedOnServer;
                custobj.IsSynced = SelectedCustomer.IsSynced;
                custobj.Name = SelectedCustomer.Name;
                custobj.Telephone = SelectedCustomer.Telephone;
                custobj.Children = SelectedCustomer.Children;
                custobj.Adults = SelectedCustomer.Adults;
                
                root.Customer = custobj;
            }
            #endregion
            if (TenancyObj.TenancyAddress != null)
            {
                TenancyAddressEnt tenAddEnt = new TenancyAddressEnt();
                tenAddEnt = Mapper.Map<TenancyAddressModel, TenancyAddressEnt>(TenancyObj.TenancyAddress);
                root.TenancyAddress = tenAddEnt;
            }

            if (TenancyObj.EmploymentInfo != null)
            {
               
                TenancyEmploymentInfoEnt tenEmpInfo = new TenancyEmploymentInfoEnt();
                tenEmpInfo = Mapper.Map<EmploymentInfoModel, TenancyEmploymentInfoEnt>(TenancyObj.EmploymentInfo);
                if (string.IsNullOrWhiteSpace(tenEmpInfo.EmploymentStatusDescription))
                {
                    var EconomicStatusList = LookupManager.GetEmploymentStatus();
                    if (EconomicStatusList.Count > 0)
                    {
                        if (tenEmpInfo.EmploymentStatusId != null)
                        {
                            var filteredListo = EconomicStatusList.Where(o => o.Id == tenEmpInfo.EmploymentStatusId).ToList();
                            if (filteredListo.Count > 0)
                            {
                                LookupModel modelo = filteredListo.ElementAt(0);
                                tenEmpInfo.EmploymentStatusDescription = modelo.Description;
                            }
                        }
                    }
                }
                root.EmploymentInfo = tenEmpInfo;
            }


            if (TenancyObj.CustomerGeneralInfo != null)
            {
                TenancyCustomerGeneralInfoEnt tenCustGenInfo = new TenancyCustomerGeneralInfoEnt();
                tenCustGenInfo = Mapper.Map<CustomerGeneralInfoModel, TenancyCustomerGeneralInfoEnt>(TenancyObj.CustomerGeneralInfo);
                root.CustomerGeneralInfo = tenCustGenInfo;
            }
            

            if (TenancyObj.EmergencyContact != null)
            {
                TenancyEmergencyContactEnt tenEmerContEnt = new TenancyEmergencyContactEnt();
                tenEmerContEnt = Mapper.Map<TenancyEmergencyContactModel, TenancyEmergencyContactEnt>(TenancyObj.EmergencyContact);
                root.EmergencyContact = tenEmerContEnt;
            }
            

            if (TenancyObj.Referral != null)
            {
                TenancyReferralEnt tenRefEnt = new TenancyReferralEnt();
                tenRefEnt = Mapper.Map<ReferralModel, TenancyReferralEnt>(TenancyObj.Referral);
                root.Referral = tenRefEnt;
            }
            

            if (TenancyObj.PropertyRentInfo != null)
            {
                TenancyRentEnt tenRentInfo = new TenancyRentEnt();
                tenRentInfo = Mapper.Map<PropertyRentModel, TenancyRentEnt>(TenancyObj.PropertyRentInfo);
                root.PropertyRentInfo = tenRentInfo;
            }
            

            if (TenancyObj.TenancyDisclosure != null)
            {
                TenancyDisclosureEnt tenDiscInfo = new TenancyDisclosureEnt();
                tenDiscInfo = Mapper.Map<TenancyDisclosureModel, TenancyDisclosureEnt>(TenancyObj.TenancyDisclosure);
                root.TenancyDisclosure = tenDiscInfo;
            }
           

            if (TenancyObj.TenantOnlineInfo != null)
            {
                TenancyOnlineSetupEnt tenOnlineSetUp = new TenancyOnlineSetupEnt();
                tenOnlineSetUp = Mapper.Map<TenantOnlineSetupModel, TenancyOnlineSetupEnt>(TenancyObj.TenantOnlineInfo);
                root.TenantOnlineInfo = tenOnlineSetUp;
            }
            

            if (TenancyObj.Occupants != null)
            {
                List<TenancyOccupantEnt> occupantsEnt = new List<TenancyOccupantEnt>();
                foreach (OccupantModel model in TenancyObj.Occupants)
                {
                    TenancyOccupantEnt occupEnt = new TenancyOccupantEnt();
                    occupEnt = Mapper.Map<OccupantModel, TenancyOccupantEnt>(model);
                    occupantsEnt.Add(occupEnt);
                }
                root.Occupants = occupantsEnt;
            }
            TenancyJointTenantEnt tenJointTenEnt = new TenancyJointTenantEnt();
            /*No Auto Mapper for Joint Tenant*/
            #region "Configure Joint Tenant Manually"
            if (TenancyObj.JointTenant != null)
            {
                tenJointTenEnt.TenancyId = TenancyObj.JointTenant.TenancyId;
                tenJointTenEnt.CustomerId = TenancyObj.JointTenant.CustomerId;
                tenJointTenEnt.StartDate = TenancyObj.JointTenant.StarteDate;
                tenJointTenEnt.IsCorrespondingAddress = TenancyObj.JointTenant.IsCorrespondingAddress;
                tenJointTenEnt.IsJointTenant = TenancyObj.JointTenant.IsJointTenant;
                tenJointTenEnt.IsJointTenantSelected = TenancyObj.JointTenant.IsJointTenantSelected;
                tenJointTenEnt.IsJointTenantSelected = TenancyObj.JointTenant.IsJointTenantSelected;
                tenJointTenEnt.JTFirstName = TenancyObj.JointTenant.JTFirstName;
                tenJointTenEnt.JTLastName = TenancyObj.JointTenant.JTLastName;
                tenJointTenEnt.JTDob = TenancyObj.JointTenant.JTDob;
                tenJointTenEnt.JTTelephone = TenancyObj.JointTenant.JTTelephone;

                if (TenancyObj.EmploymentInfo != null)
                {
                    TenancyEmploymentInfoEnt tenEmpJTInfo = new TenancyEmploymentInfoEnt();
                    tenEmpJTInfo = Mapper.Map<EmploymentInfoModel, TenancyEmploymentInfoEnt>(TenancyObj.JointTenant.EmploymentInfo);
                    if (string.IsNullOrWhiteSpace(tenEmpJTInfo.EmploymentStatusDescription))
                    {
                        var EconomicStatusList = LookupManager.GetEmploymentStatus();
                        if (EconomicStatusList.Count > 0)
                        {
                            if (tenEmpJTInfo.EmploymentStatusId != null)
                            {
                                var filteredListo = EconomicStatusList.Where(o => o.Id == tenEmpJTInfo.EmploymentStatusId).ToList();
                                if (filteredListo.Count > 0)
                                {
                                    LookupModel modelo = filteredListo.ElementAt(0);
                                    tenEmpJTInfo.EmploymentStatusDescription = modelo.Description;
                                }
                            }
                        }
                    }
                    tenJointTenEnt.EmploymentInfoJT = tenEmpJTInfo;

                }


                if (TenancyObj.CustomerGeneralInfo != null)
                {
                    TenancyCustomerGeneralInfoEnt tenCustJTGenInfo = new TenancyCustomerGeneralInfoEnt();
                    tenCustJTGenInfo = Mapper.Map<CustomerGeneralInfoModel, TenancyCustomerGeneralInfoEnt>(TenancyObj.JointTenant.CustomerGeneralInfo);
                    tenJointTenEnt.CustomerGeneralInfoJT = tenCustJTGenInfo;
                }


                if (TenancyObj.EmergencyContact != null)
                {
                    TenancyEmergencyContactEnt tenEmerJTContEnt = new TenancyEmergencyContactEnt();
                    tenEmerJTContEnt = Mapper.Map<TenancyEmergencyContactModel, TenancyEmergencyContactEnt>(TenancyObj.JointTenant.EmergencyContact);
                    tenJointTenEnt.EmergencyContactJT = tenEmerJTContEnt;
                }

                root.JointTenant = tenJointTenEnt;

            }

            #endregion;

            ResultModel<bool> result = TenancyRepository.SaveTenancyOfflineData(root);
            return result;


        }
        #endregion

        #region "Delete Tenancy"
        public void DeleteTenancyDataFor(int? CustomerId, string PropertyId)
        {
            TenancyRepository.DeleteTenancyDataFor(CustomerId, PropertyId);
        }
        #endregion

        #region "Retrieve Tenancy"
        public ResultModel<OfflineTenancyModel> FetchExistingTenancyFor(int? CustomerId, string PropertyId)
        {
            ResultModel<TenancyRootEnt> result = TenancyRepository.FetchTenancyDataFor(CustomerId,PropertyId);
            ResultModel<OfflineTenancyModel> yieldModel = new ResultModel<OfflineTenancyModel>();
            if (result.Success == true)
            {
                OfflineTenancyModel root = new OfflineTenancyModel();

                root.TenancyModel = new TenancyModel();

                root.TenancyModel.TenancyId = result.Value.TenancyId;
                root.TenancyModel.CustomerId = result.Value.CustomerId;
                root.TenancyModel.PropertyId = result.Value.Property.propertyId;
                root.TenancyModel.TenancyStartDate = result.Value.TenancyStartDate;
                root.TenancyModel.TenancyEndDate = result.Value.TenancyEndDate;
                root.TenancyModel.PausedOnStepId = result.Value.PausedOnStepId;
                
                if (result.Value.Property != null)
                {
                    PropertyModel propertyobj = new PropertyModel();
                    propertyobj = Mapper.Map<TenancyPropertyEnt, PropertyModel>(result.Value.Property);
                    root.Property = propertyobj;
                }
                /*No Auto Mapper for Customer*/
                #region "Configure Customer Manually"
                if (result.Value.Customer != null)
                {
                    CustomerModel custobj = new CustomerModel();
                    custobj.CustomerId = result.Value.Customer.CustomerId;
                    custobj.CustomerNumber = result.Value.Customer.CustomerNumber;
                    custobj.CustomerType = result.Value.Customer.CustomerType;
                    custobj.CustomerTypeId = result.Value.Customer.CustomerTypeId;
                    custobj.TenancyCount = result.Value.Customer.TenancyCount;
                    custobj.IsJointTenant = result.Value.Customer.IsJointTenant;
                    custobj.TitleId = result.Value.Customer.TitleId;
                    custobj.FirstName = result.Value.Customer.FirstName;
                    custobj.MiddleName = result.Value.Customer.MiddleName;
                    custobj.LastName = result.Value.Customer.LastName;
                    custobj.PreferredContact = result.Value.Customer.PreferredContact;
                    custobj.PreferedContactId = result.Value.Customer.PreferedContactId;
                    custobj.InternetAccess = result.Value.Customer.InternetAccess;
                    custobj.InternetAccessDescription = result.Value.Customer.InternetAccessDescription;
                    custobj.Dob = result.Value.Customer.Dob;
                    custobj.Gender = result.Value.Customer.Gender;
                    custobj.MaritalStatus = result.Value.Customer.MaritalStatus;
                    custobj.MaritalStatusId = result.Value.Customer.MaritalStatusId;
                    custobj.Ethnicity = result.Value.Customer.Ethnicity;
                    custobj.EthnicityId = result.Value.Customer.EthnicityId;
                    custobj.Religion = result.Value.Customer.Religion;
                    custobj.ReligionId = result.Value.Customer.ReligionId;
                    custobj.SexualOrientation = result.Value.Customer.SexualOrientation;
                    custobj.SexualOrientationId = result.Value.Customer.SexualOrientationId;
                    custobj.NiNumber = result.Value.Customer.NiNumber;
                    custobj.Nationality = result.Value.Customer.Nationality;
                    custobj.NationalityId = result.Value.Customer.NationalityId;
                    custobj.FirstLanguage = result.Value.Customer.FirstLanguage;
                    custobj.IsSubjectToImmigration = result.Value.Customer.IsSubjectToImmigration;
                    custobj.SubjectToImmigration = result.Value.Customer.SubjectToImmigration;
                    custobj.IsPermanentUKResident = result.Value.Customer.IsPermanentUKResident;
                    custobj.PermanentUKResident = result.Value.Customer.PermanentUKResident;
                    custobj.ResidencyStatus = result.Value.Customer.ResidencyStatus;
                    custobj.Communication = result.Value.Customer.Communication;
                    custobj.GroupKey = result.Value.Customer.GroupKey;
                    //custobj.CommunicationList = (result.Value.Customer.CommunicationList.Count>0)?string.Join(",", result.Value.Customer.CommunicationList):"";
                    custobj.AppVersion = result.Value.Customer.AppVersion;
                    custobj.CreatedBy = result.Value.Customer.CreatedBy;
                    custobj.IsTenant = result.Value.Customer.IsTenant;
                    custobj.CreatedOnApp = result.Value.Customer.CreatedOnApp;
                    custobj.CreatedOnServer = result.Value.Customer.CreatedOnServer;
                    custobj.LastModifiedOnApp = result.Value.Customer.LastModifiedOnApp;
                    custobj.LastModifiedOnServer = result.Value.Customer.LastModifiedOnServer;
                    custobj.IsSynced = result.Value.Customer.IsSynced;
                    custobj.Telephone = result.Value.Customer.Telephone;
                    custobj.Children = result.Value.Customer.Children;
                    custobj.Adults = result.Value.Customer.Adults;
                    
                    root.Customer = custobj;
                }
                #endregion
                if (result.Value.TenancyAddress != null)
                {
                    TenancyAddressModel tenAddEnt = new TenancyAddressModel();
                    tenAddEnt = Mapper.Map<TenancyAddressEnt, TenancyAddressModel>(result.Value.TenancyAddress);
                    root.TenancyModel.TenancyAddress = tenAddEnt;
                }

                if (result.Value.EmploymentInfo != null)
                {
                    EmploymentInfoModel tenEmpInfo = new EmploymentInfoModel();
                    tenEmpInfo = Mapper.Map<TenancyEmploymentInfoEnt, EmploymentInfoModel>(result.Value.EmploymentInfo);
                    if (string.IsNullOrWhiteSpace(tenEmpInfo.EmploymentStatusDescription))
                    {
                        var EconomicStatusList = LookupManager.GetEmploymentStatus();
                        if (EconomicStatusList.Count > 0)
                        {
                            if (tenEmpInfo.EmploymentStatusId != null)
                            {
                                var filteredListo = EconomicStatusList.Where(o => o.Id == tenEmpInfo.EmploymentStatusId).ToList();
                                if (filteredListo.Count > 0)
                                {
                                    LookupModel modelo = filteredListo.ElementAt(0);
                                    tenEmpInfo.EmploymentStatusDescription = modelo.Description;
                                }
                            }
                        }
                    }
                    
                    root.TenancyModel.EmploymentInfo = tenEmpInfo;
                }


                if (result.Value.CustomerGeneralInfo != null)
                {
                    CustomerGeneralInfoModel tenCustGenInfo = new CustomerGeneralInfoModel();
                    tenCustGenInfo = Mapper.Map<TenancyCustomerGeneralInfoEnt, CustomerGeneralInfoModel>(result.Value.CustomerGeneralInfo);
                    root.TenancyModel.CustomerGeneralInfo = tenCustGenInfo;
                }


                if (result.Value.EmergencyContact != null)
                {
                    TenancyEmergencyContactModel tenEmerContEnt = new TenancyEmergencyContactModel();
                    tenEmerContEnt = Mapper.Map<TenancyEmergencyContactEnt, TenancyEmergencyContactModel>(result.Value.EmergencyContact);
                    root.TenancyModel.EmergencyContact = tenEmerContEnt;
                }


                if (result.Value.Referral != null)
                {
                    ReferralModel tenRefEnt = new ReferralModel();
                    tenRefEnt = Mapper.Map<TenancyReferralEnt, ReferralModel>(result.Value.Referral);
                    root.TenancyModel.Referral = tenRefEnt;
                }


                if (result.Value.PropertyRentInfo != null)
                {
                    PropertyRentModel tenRentInfo = new PropertyRentModel();
                    tenRentInfo = Mapper.Map<TenancyRentEnt, PropertyRentModel>(result.Value.PropertyRentInfo);
                    root.TenancyModel.PropertyRentInfo = tenRentInfo;
                }


                if (result.Value.TenancyDisclosure != null)
                {
                    TenancyDisclosureModel tenDiscInfo = new TenancyDisclosureModel();
                    tenDiscInfo = Mapper.Map<TenancyDisclosureEnt, TenancyDisclosureModel>(result.Value.TenancyDisclosure);
                    root.TenancyModel.TenancyDisclosure = tenDiscInfo;
                }


                if (result.Value.TenantOnlineInfo != null)
                {
                    TenantOnlineSetupModel tenOnlineSetUp = new TenantOnlineSetupModel();
                    tenOnlineSetUp = Mapper.Map<TenancyOnlineSetupEnt, TenantOnlineSetupModel>(result.Value.TenantOnlineInfo);
                    root.TenancyModel.TenantOnlineInfo = tenOnlineSetUp;
                }


                if (result.Value.Occupants != null)
                {
                    List<OccupantModel> occupantsEnt = new List<OccupantModel>();
                    foreach (TenancyOccupantEnt model in result.Value.Occupants)
                    {
                        OccupantModel occupEnt = new OccupantModel();
                        occupEnt = Mapper.Map<TenancyOccupantEnt, OccupantModel>(model);
                        occupantsEnt.Add(occupEnt);
                    }
                    root.TenancyModel.Occupants = occupantsEnt;
                }
                JointTenantModel tenJointTenEnt = new JointTenantModel();
                /*No Auto Mapper for Joint Tenant*/
                #region "Configure Joint Tenant Manually"
                if (result.Value.JointTenant != null)
                {
                    tenJointTenEnt.TenancyId = result.Value.JointTenant.TenancyId;
                    tenJointTenEnt.CustomerId = result.Value.JointTenant.CustomerId;
                    tenJointTenEnt.StarteDate = result.Value.JointTenant.StartDate;
                    tenJointTenEnt.IsCorrespondingAddress = result.Value.JointTenant.IsCorrespondingAddress;
                    tenJointTenEnt.IsJointTenant = result.Value.JointTenant.IsJointTenant;
                    tenJointTenEnt.IsJointTenantSelected = result.Value.JointTenant.IsJointTenantSelected;
                    tenJointTenEnt.JTFirstName = result.Value.JointTenant.JTFirstName;
                    tenJointTenEnt.JTLastName = result.Value.JointTenant.JTLastName;
                    tenJointTenEnt.JTDob = result.Value.JointTenant.JTDob;
                    tenJointTenEnt.JTTelephone = result.Value.JointTenant.JTTelephone;

                    if (result.Value.JointTenant.EmploymentInfoJT != null)
                    {
                        EmploymentInfoModel tenEmpJTInfo = new EmploymentInfoModel();
                        tenEmpJTInfo = Mapper.Map<TenancyEmploymentInfoEnt, EmploymentInfoModel>(result.Value.JointTenant.EmploymentInfoJT);
                        if (string.IsNullOrWhiteSpace(tenEmpJTInfo.EmploymentStatusDescription))
                        {
                            var EconomicStatusList = LookupManager.GetEmploymentStatus();
                            if (EconomicStatusList.Count > 0)
                            {
                                if (tenEmpJTInfo.EmploymentStatusId != null)
                                {
                                    var filteredListo = EconomicStatusList.Where(o => o.Id == tenEmpJTInfo.EmploymentStatusId).ToList();
                                    if (filteredListo.Count > 0)
                                    {
                                        LookupModel modelo = filteredListo.ElementAt(0);
                                        tenEmpJTInfo.EmploymentStatusDescription = modelo.Description;
                                    }
                                }
                            }
                        }
                        tenJointTenEnt.EmploymentInfo = tenEmpJTInfo;

                    }


                    if (result.Value.JointTenant.CustomerGeneralInfoJT != null)
                    {
                        CustomerGeneralInfoModel tenCustJTGenInfo = new CustomerGeneralInfoModel();
                        tenCustJTGenInfo = Mapper.Map<TenancyCustomerGeneralInfoEnt, CustomerGeneralInfoModel>(result.Value.JointTenant.CustomerGeneralInfoJT);
                        tenJointTenEnt.CustomerGeneralInfo = tenCustJTGenInfo;
                    }


                    if (result.Value.JointTenant.EmergencyContactJT != null)
                    {
                        TenancyEmergencyContactModel tenEmerJTContEnt = new TenancyEmergencyContactModel();
                        tenEmerJTContEnt = Mapper.Map<TenancyEmergencyContactEnt, TenancyEmergencyContactModel>(result.Value.JointTenant.EmergencyContactJT);
                        tenJointTenEnt.EmergencyContact = tenEmerJTContEnt;
                    }

                    root.TenancyModel.JointTenant = tenJointTenEnt;

                }
                #endregion
                yieldModel.Success = true;
                yieldModel.Value = root;
            }
            else
            {
                yieldModel.Success = false;
                yieldModel.Message = result.Message;
            }
            return yieldModel;
        }
        #endregion
    }
}
