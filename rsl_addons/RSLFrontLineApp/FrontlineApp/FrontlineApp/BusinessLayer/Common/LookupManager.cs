﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.DataLayer.Repository;
using System.Collections.ObjectModel;
using FrontlineApp.Utility.Helper.Extensions;

namespace FrontlineApp.BusinessLayer
{
    public class LookupManager
    {

        #region "Properties"
        LookupRepository repo;
        #endregion

        #region "Constructor"
        public LookupManager()
        {
            repo = new LookupRepository();
        }
        #endregion

        #region "Methods"

        #region "Customer Lookups"        

        #region "Get Customer Titles"
        public List<LookupModel> GetCustomerTitles()
        {
            return repo.GetCustomerTitles();
        }
        #endregion

        #region "Get Prefered Contacts"
        public List<LookupModel> GetPreferedContacts()
        {
            return repo.GetPreferedContacts();
        }
        #endregion

        #region "Get Internet Access"
        public List<LookupModel> GetInternetAccess()
        {            
            return repo.GetInternetAccess();
        }
        #endregion

        #region "Get Marital Statuses"
        public List<LookupModel> GetMaritalStatuses()
        {
            return repo.GetMaritalStatuses();
        }
        #endregion

        #region "Get Ethnicity"
        public List<LookupModel> GetEthnicity()
        {
            return repo.GetEthnicity();
        }
        #endregion

        #region "Get Religions"
        public List<LookupModel> GetReligion()
        {
            return repo.GetReligion();
        }
        #endregion

        #region "Get Sexual Orientation"
        public List<LookupModel> GetSexualOrientation()
        {
            return repo.GetSexualOrientation();
        }
        #endregion

        #region "Get Nationality"
        public List<LookupModel> GetNationality()
        {
            return repo.GetNationality();
        }
        #endregion

        #region "Get YesNo"
        public List<LookupModel> GetYesNo()
        {
            return repo.GetYesNo();
        }
        #endregion

        #region "Get YesNoUnknown"
        public List<LookupModel> GetYesNoUnknown()
        {
            return repo.GetYesNoUnknown();
        }
        #endregion

        #region "Get LandlordType"
        public List<LookupModel> GetLandlordType()
        {
            return repo.GetLandlordType();
        }
        #endregion

        #region "Get House Types"
        public List<LookupModel> GetHouseTypes()
        {
            return repo.GetHouseTypes();
        }
        #endregion

        #region "Get Communication"
        public List<LookupModel> GetCommunication()
        {
            return repo.GetCommunication();
        }
        #endregion

        #region "Get Customer Type"
        public List<LookupModel> GetCustomerTypes()
        {
            return repo.GetCustomerTypes();
        }
        #endregion

        #region "Get Referral Stages"
        public List<LookupModel> GetReferralStages()
        {
            return repo.GetReferralStages();
        }
        #endregion

        #region "Get Viewing Comment Types"
        public List<LookupModel> GetViewingCommentsTypes()
        {
            return repo.GetViewingCommentTypes();
        }
        #endregion

        #region "Get Referral Help Categories"
        public List<GroupInfoListModel> GetReferralHelpCategory()
        {
            return repo.GetReferralHelpCategory();
        }
        public List<GroupInfoListModel> GetReferralHelpCategory(string selectedItems)
        {
            return repo.GetReferralHelpCategory(selectedItems);
        }
        #endregion

        #region "Get Termination Reason"
        public List<LookupModel> GetTerminationReason()
        {
            return repo.GetTerminationReason();
        }
        #endregion

        #region "Get Termination Reason Category"
        public List<LookupModel> GetTerminationReasonCategory()
        {
            return repo.GetTerminationReasonCategory();
        }
        #endregion

        #region "Get Benefits"
        public List<LookupModel> GetBenefits()
        {
            return repo.GetBenefits();
        }
        #endregion

        #region "Get Banking Facilities"
        public List<LookupModel> GetBankingFacilities()
        {
            return repo.GetBankingFacilities();
        }
        #endregion

        #region "Get Health Problems"
        public List<LookupModel> GetHealthProblems()
        {
            return repo.GetHealthProblems();
        }
        #endregion

        #region "Get Support From Agencies"
        public List<LookupModel> GetSupportFromAgencies()
        {
            return repo.GetSupportFromAgencies();
        }
        #endregion

        #region "Get Aids And Adaption"
        public List<LookupModel> GetAidsAndAdaption()
        {
            return repo.GetAidsAndAdaption();
        }
        #endregion

        #region "Get Employment Status"
        public List<LookupModel> GetEmploymentStatus()
        {
            return repo.GetEmploymentStatus();
        }
        #endregion

        #region "Get Eviction Reason"
        public List<LookupModel> GetEvictionReason()
        {
            return repo.GetEvictionReason();
        }
        #endregion

        #region "Get Floor"
        public List<LookupModel> GetFloor()
        {
            return repo.GetFloor();
        }
        #endregion
        
        #region "Get Address Type"
        public List<LookupModel> GetAddressType()
        {
            return repo.GetAddressType();
        }
        #endregion

        #region "Get Relationship Status"
        public List<LookupModel> GetRelationshipStatus()
        {
            return repo.GetRelationshipStatus();
        }
        #endregion

        #region "Get Item Status"
        public List<LookupModel> GetItemStatus()
        {
            return repo.GetItemStatuses();
        }
        #endregion

        #region "Get Number Of Bedrooms"
        public List<LookupModel> GetNumberOfBedrooms()
        {
            return Enumerable.Range(1, 6).Select(x => new LookupModel { Id = x, Description = x.ToString() }).ToList() ;
        }
        #endregion

        #region "Get Gender List"
        public List<string> GetGenderList()
        {
            List<string> genderList = new List<string>();
            genderList.Add("Male");
            genderList.Add("Female");
            genderList.Add("Transgender");
            return genderList;
        }
        #endregion

        #region "Get Selected Items"
        public string GetSelectedItems(ObservableCollection<LookupModel> list)
        {
            var selectedList = list.Where(x => x.IsSelected == true).Select(y => y.Id).ToList();
            return selectedList.Count() > 0 ? selectedList.ToCommaSeparatedString() : null;
        }
        #endregion

        #endregion

        #endregion

    }
}
