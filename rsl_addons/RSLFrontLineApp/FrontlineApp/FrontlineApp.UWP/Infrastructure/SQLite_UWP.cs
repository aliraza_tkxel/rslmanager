﻿using System.IO;
using Xamarin.Forms;
using Windows.Storage;
using FrontlineApp.DataLayer.Database;
using SQLite.Net;
using FrontlineApp.UWP.Infrastructure;

[assembly: Dependency(typeof(SQLite_UWP))]

namespace FrontlineApp.UWP.Infrastructure
{
    public class SQLite_UWP : FrontlineDatabase
    {

        #region FrontlineDatabase implementation

        protected override SQLiteConnection InitDBConnection()
        {

            var sqliteFilename = "FrontlineAppSQLite.db3";
            string path = Path.Combine(ApplicationData.Current.LocalFolder.Path, sqliteFilename);
            return new SQLiteConnection(new SQLite.Net.Platform.WinRT.SQLitePlatformWinRT(), path, false);
        }
        #endregion
    }
}
