﻿using FrontlineApp.UWP.Infrastructure;
using Windows.Storage;
using Xamarin.Forms;
using System.Threading.Tasks;
using System;
using Windows.Storage.Streams;
using FrontlineApp.ServiceLayer.Services;
using System.IO;
using System.Threading;
using FrontlineApp.ApplicationLayer.Models;
using System.Text;
using System.Net.Http;
using System.Net.Http.Headers;

[assembly: Dependency(typeof(File_UWP))]
namespace FrontlineApp.UWP.Infrastructure
{
    public class File_UWP : FileBase
    {
        private Stream stream = new MemoryStream();

        private CancellationTokenSource cts;

        #region Get File Stream implementation
        public override async Task<ResultModel<bool>> UploadImageFromPlatformSpecificLocation(Uri sourceUri, Uri destinationUri)
        {
            cts = new CancellationTokenSource();

            ResultModel<bool> result = new ResultModel<bool>();
            string error = string.Empty;
            try
            {
                StorageFile file = await StorageFile.GetFileFromApplicationUriAsync(sourceUri);

                byte[] fileBytes = null;
                using (IRandomAccessStreamWithContentType stream = await file.OpenReadAsync())
                {
                    fileBytes = new byte[stream.Size];

                    using (DataReader reader = new DataReader(stream))
                    {
                        await reader.LoadAsync((uint)stream.Size);
                        reader.ReadBytes(fileBytes);
                    }
                }
                HttpRequestMessage request = new HttpRequestMessage(
                        System.Net.Http.HttpMethod.Post, destinationUri);

                string boundary = "---------------------------7dd36f1721dc0";
                request.Content = new ByteArrayContent(
                    BuildByteArray("source", file.Name, fileBytes, boundary));
                request.Content.Headers.ContentType = new MediaTypeHeaderValue("multipart/form-data");
                request.Content.Headers.ContentType.Parameters.Add(new NameValueHeaderValue(
                    "boundary",
                    boundary));

                HttpClient client = new HttpClient();
                var attachmentResponse = await client.SendAsync(request);

                if (attachmentResponse.IsSuccessStatusCode)
                {
                    result.Success = true;
                }
                else
                {
                    result.Success = false;
                }

            }
            catch (Exception ex)
            {
                result.Success = false;
                result.Message = ex.Message;
            }

            return result;
            //  return isUploaded;
        }
        #endregion



        private byte[] BuildByteArray(string name, string fileName, byte[] fileBytes, string boundary)
        {
            // Create multipart/form-data headers.
            byte[] firstBytes = Encoding.UTF8.GetBytes(String.Format(
                "--{0}\r\n" +
                "Content-Disposition: form-data; name=\"{1}\"; filename=\"{2}\"\r\n" +
                "\r\n",
                boundary,
                name,
                fileName));

            byte[] lastBytes = Encoding.UTF8.GetBytes(String.Format(
                "\r\n" +
                "--{0}--\r\n",
                boundary));

            int contentLength = firstBytes.Length + fileBytes.Length + lastBytes.Length;
            byte[] contentBytes = new byte[contentLength];

            // Join the 3 arrays into 1.
            Array.Copy(
                firstBytes,
                0,
                contentBytes,
                0,
                firstBytes.Length);
            Array.Copy(
                fileBytes,
                0,
                contentBytes,
                firstBytes.Length,
                fileBytes.Length);
            Array.Copy(
                lastBytes,
                0,
                contentBytes,
                firstBytes.Length + fileBytes.Length,
                lastBytes.Length);

            return contentBytes;
        }

    }
}
