﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using FrontlineApp.UWP.ViewModels;
using Windows.UI.Xaml.Navigation;
using FrontlineApp.ApplicationLayer.Models;
// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace FrontlineApp.UWP.Views.Authentication
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class LoginPage : Page
    {
       
        public LoginPage()
        {            
            this.InitializeComponent();            
        }
        private void ButtonForgetPassword_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(ForgotPasswordPage));
        }

        private void ButtonLogin_Click(object sender, RoutedEventArgs e)
        {
            AuthenticationViewModel vm = this.DataContext as AuthenticationViewModel;
            vm.LoginUser();
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            AuthenticationViewModel vm = this.DataContext as AuthenticationViewModel;
            vm.UserObject = new UserModel();
        }
    }
}
