﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using FrontlineApp.ServiceLayer.Services;
// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238
using FrontlineApp.UWP.ViewModels;
using Windows.UI.Xaml.Navigation;
using FrontlineApp.ApplicationLayer.Models;

namespace FrontlineApp.UWP.Views.Authentication
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ForgotPasswordPage : Page
    {
        
        public ForgotPasswordPage()
        {
            this.InitializeComponent();
            
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            ResetPasswordViewModel vm = this.DataContext as ResetPasswordViewModel;
            vm.UserObject = new UserModel();
        }

        private void ButtonLogin_Click(object sender, RoutedEventArgs e)
        {
            if (Frame.CanGoBack)
            {
                Frame.GoBack();
            }
        }

        private void ButtonSubmit_Click(object sender, RoutedEventArgs e)
        {
            ResetPasswordViewModel vm = this.DataContext as ResetPasswordViewModel;
            vm.ResetPassword();
        }
    }
}
