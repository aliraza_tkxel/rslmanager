﻿using System.Collections.Generic;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using FrontlineApp.UWP.Views.Viewings;
using FrontlineApp.ApplicationLayer.Models;
using Windows.UI.Xaml.Navigation;
// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace FrontlineApp.UWP.Views.Properties
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class PropertiesMasterPage : Page
    {
        public string searchQuery { get; set; }
        public PropertyFilterModel propertyFilter;
        public PropertiesMasterPage()
        {
            this.InitializeComponent();
            propertyFilter = new PropertyFilterModel();
            
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.Parameter != null)
            {
                Dictionary<string, string> dict = e.Parameter as Dictionary<string, string>;
                searchQuery = dict["searchQuery"];
            }
            propertyLoadingCompletedCallBack();
        }

        /*Gets call back from Property list page*/
        public void FilterButton_Click()
        {
            if (PaneFrame.CanGoForward)
            {
                PaneFrame.GoForward();
            }
            else
            {
                PaneFrame.Navigate(typeof(PropertyFilterPage), this);
            }
            GoToNoPropertySelectedPage();
        }
        /*General method. Also gets call back from Filter Page*/
        public void navigateToPropertyList()
        {
            if (PaneFrame.CanGoBack)
            {
                PaneFrame.GoBack();
            }
            else
            {
                PaneFrame.Navigate(typeof(PropertyListPage), this);
            }
            this.GoToNoPropertySelectedPage();
        }
         /*Gets call back from the selected list item*/
        
        public void GoToPropertyDetailsForPropertyId(string propertyId, string propertyAddress)
        {
            
            NavFrame.Navigate(typeof(PropertyDetailPage),propertyId);
            
        }
        
        public void propertyLoadingCompletedCallBack()
        {
            PaneFrame.Navigate(typeof(PropertyListPage), this);
            PaneFrame.BackStack.Clear();
            GoToNoPropertySelectedPage();
        }
        public void propertyDetailLoadingCompleted()
        {
            PaneFrame.BackStack.Clear();
        }
        /*General method to show default page*/
        public void GoToNoPropertySelectedPage()
        {
            
            NavFrame.Navigate(typeof(NoPropertySelectedPage));
           
        }
        
        public void ApplyFilter(PropertyFilterModel model)
        {
            propertyFilter = model;
            navigateToPropertyList();
        }

        
    }
}
