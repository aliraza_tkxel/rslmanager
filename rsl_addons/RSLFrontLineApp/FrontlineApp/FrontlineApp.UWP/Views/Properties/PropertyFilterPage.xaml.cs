﻿using System;
using System.Collections.Generic;
using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using FrontlineApp.DataLayer.Entities;
using FrontlineApp.ApplicationLayer.Models;
using System.Text.RegularExpressions;
using Windows.UI.Popups;
using FrontlineApp.DataLayer.Repository;
using FrontlineApp.UWP.ViewModels;
// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace FrontlineApp.UWP.Views.Properties
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class PropertyFilterPage : Page
    {
        public delegate void FilterCancellationDelegate();
        public delegate void FilterApplicationDelegate(PropertyFilterModel model);
        public FilterCancellationDelegate CancellationDelegate;
        public FilterApplicationDelegate ApplyFilterDelegate;
        public PropertyFilterPage()
        {
            this.InitializeComponent();
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.Parameter != null)
            {
                PropertiesMasterPage masterPage = e.Parameter as PropertiesMasterPage;
                CancellationDelegate = masterPage.navigateToPropertyList;
                ApplyFilterDelegate = masterPage.ApplyFilter;
            }
        }
        
        
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            PropertyFilterViewModel vm = this.DataContext as PropertyFilterViewModel;
            vm.FilterObj = new PropertyFilterObj();
            PropertyFilterModel model = vm.PopulateFilterModel();
            ApplyFilterDelegate(model);
            //CancellationDelegate();
        }

        private void ApplyButton_Click(object sender, RoutedEventArgs e)
        {
            PropertyFilterViewModel vm = this.DataContext as PropertyFilterViewModel;
            if (string.IsNullOrWhiteSpace(vm.StripNonNumerics(TextBoxBedrooms.Text)) && TextBoxBedrooms.Text.Length>0)
            {
                vm.showMessage("Error", "Please enter valid input for number of bedrooms");
                return;
            }
            PropertyFilterModel model = vm.PopulateFilterModel();
            ApplyFilterDelegate(model);
        }
        

        
        
        

        
        
    }
}
