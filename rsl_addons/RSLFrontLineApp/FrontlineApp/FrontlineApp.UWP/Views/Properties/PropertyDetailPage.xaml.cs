﻿using FrontlineApp.UWP.ViewModels;
using FrontlineApp.UWP.Views.Customers;
using FrontlineApp.UWP.Views.Viewings;
using Microsoft.Practices.ServiceLocation;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using FrontlineApp.UWP.UserControls.Property;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace FrontlineApp.UWP.Views.Properties
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class PropertyDetailPage : Page
    {
        
        
        private PropertyLocationControl LocationCtrl { get; set; }
        public PropertyDetailPage()
        {
            this.InitializeComponent();
           
        }

        private void LocationCtrl_RemoveFromGridAction()
        {
            PivotGrid.Children.Remove(LocationCtrl);
        }

        public PropertyDetailsViewModel GetDataContext()
        {
            return this.DataContext as PropertyDetailsViewModel;
        }

        private async void GetPropertyDetails(string propertyId)
        {
            await GetDataContext().GetPropertyDetailsForSelectedProperty(propertyId);
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
             string propertyId = e.Parameter as string;
             GetPropertyDetails(propertyId);
        }

        private void rootPivot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void CustomerViewingListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void CreateViewing_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(ArrangeNewViewingPage));
            GetDataContext().SendViewingMessage(RefreshData);
        }
        private void ViewLocation_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            if (LocationCtrl == null)
            {
                LocationCtrl = new PropertyLocationControl();
                
                LocationCtrl.RemoveFromGridAction += LocationCtrl_RemoveFromGridAction;
            }
            
            PivotGrid.Children.Add(LocationCtrl);
            LocationCtrl.setLocationTitle(GetDataContext().SelectedProperty.completeAddress);
        }

        #region "Delegate"
        public void RefreshData()
        {
            GetPropertyDetails(GetDataContext().SelectedProperty.propertyId);
            this.Frame.GoBack();
            this.Frame.BackStack.Clear();
        }
        #endregion

        private void StartTenancyButton_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            SetupAndAddTenancyViewModel aoccup = ServiceLocator.Current.GetInstance<SetupAndAddTenancyViewModel>();
            aoccup.ClearAllData();
            this.Frame.Navigate(typeof(SetupAndStartTenancyPage));
            GetDataContext().SendTenancyMessages();
        }

        
    }
}
