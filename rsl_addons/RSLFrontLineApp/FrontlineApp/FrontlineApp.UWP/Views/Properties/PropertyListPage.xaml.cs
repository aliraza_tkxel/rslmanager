﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.UWP.ViewModels;
// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace FrontlineApp.UWP.Views.Properties
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class PropertyListPage : Page
    {
        public PropertyViewModel ViewModel
        {
            get { return this.DataContext as PropertyViewModel; }
        }
        public delegate void PropertySelectionDelegate(string propertyId, string address);
        public delegate void FilterSelectionDelegate();
        public delegate void CustomSearchDelegate();
        public PropertySelectionDelegate PropertyListDelegate;
        public FilterSelectionDelegate FilterSelDelegate;
        public CustomSearchDelegate CustomSearchDel;
        public string searchQuery;
        public PropertyListPage()
        {
            InitializeComponent();

        }
        private void PropertyListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            PropertyModel selectedObj = PropertyListView.SelectedItem as PropertyModel;
            ViewModel.SelectedProperty = selectedObj;
            if(selectedObj!=null)
            {
                PropertyListDelegate(selectedObj.propertyId, selectedObj.address);
                foreach (var item in e.AddedItems)
                {
                    ListViewItem lvi = (sender as ListView).ContainerFromItem(item) as ListViewItem;
                    lvi.ContentTemplate = (DataTemplate)this.Resources["ListItemSelected"];
                }
                //Remove DataTemplate for unselected items
                foreach (var item in e.RemovedItems)
                {
                    ListViewItem lvi = (sender as ListView).ContainerFromItem(item) as ListViewItem;
                    lvi.ContentTemplate = (DataTemplate)this.Resources["ListItem"];
                }
            }
            
            
        }
   
        private void FilterButton_Click(object sender, RoutedEventArgs e)
        {
            FilterSelDelegate();
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.Parameter != null)
            {
                PropertiesMasterPage masterPage = e.Parameter as PropertiesMasterPage;
                PropertyListDelegate = masterPage.GoToPropertyDetailsForPropertyId;
                FilterSelDelegate = masterPage.FilterButton_Click;
                CustomSearchDel = masterPage.GoToNoPropertySelectedPage;

                //ViewModel.InitializeData();
                ViewModel.FilterObj = masterPage.propertyFilter;
                ViewModel.SearchQuery = masterPage.searchQuery;
                masterPage.searchQuery = "";
                if (!string.IsNullOrWhiteSpace(ViewModel.SearchQuery))
                {
                    PropertySearchBox.QueryText = ViewModel.SearchQuery;
                }
                ViewModel.GetPropertyList();
            }
        }

        private void SearchBox_QuerySubmitted(SearchBox sender, SearchBoxQuerySubmittedEventArgs args)
        {
            CustomSearchDel();
            ViewModel.SearchQuery = args.QueryText;
            ViewModel.GetPropertyList();
        }
    }
}
