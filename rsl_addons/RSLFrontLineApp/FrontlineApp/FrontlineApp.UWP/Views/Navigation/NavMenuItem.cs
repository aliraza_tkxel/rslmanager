﻿using FrontlineApp.Utility.Constants;
using System;
using System.ComponentModel;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace FrontlineApp.UWP.Models.Navigation
{
    public class NavMenuItem : INotifyPropertyChanged
    {
        public string Label { get; set; }

        public string SelectedStateImage { get; set; }

        private string _normalStateImage;
        public string NormalStateImage
        {
            get { return _normalStateImage; }
            set { _normalStateImage = value; }
        }

        private string _imageSource;
        public string ImageSource
        {
            get
            {
                return _imageSource ?? String.Format("{0}{1}", PathConstants.ImageBase_1x, NormalStateImage);
            }
            set
            {
                _imageSource = value;
                this.OnPropertyChanged("ImageSource");
            }
        }

        public Symbol Symbol { get; set; }
        public char SymbolAsChar
        {
            get
            {
                return (char)this.Symbol;
            }
        }

        private bool _isSelected;
        public bool IsSelected
        {
            get { return _isSelected; }
            set
            {
                _isSelected = value;
                SelectedVis = value ? Visibility.Visible : Visibility.Collapsed;
                ImageSource = String.Format("{0}{1}",PathConstants.ImageBase_1x, _isSelected ? SelectedStateImage : NormalStateImage);
                this.OnPropertyChanged("IsSelected");

            }
        }

        private Visibility _selectedVis = Visibility.Collapsed;
        public Visibility SelectedVis
        {
            get { return _selectedVis; }
            set
            {
                _selectedVis = value;
                this.OnPropertyChanged("SelectedVis");
            }
        }

        public Type DestPage { get; set; }
        public object Arguments { get; set; }

        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        public void OnPropertyChanged(string propertyName)
        {
            // Raise the PropertyChanged event, passing the name of the property whose value has changed.
            this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
