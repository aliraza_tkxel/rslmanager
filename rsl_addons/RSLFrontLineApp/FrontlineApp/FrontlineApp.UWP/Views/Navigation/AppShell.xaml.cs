﻿using FrontlineApp.UWP.CustomControls;
using FrontlineApp.UWP.Models.Navigation;
using FrontlineApp.UWP.Views.Customers;
using FrontlineApp.UWP.Views.Dashboard;
using FrontlineApp.UWP.Views.Viewings;
using System.Collections.ObjectModel;
using System.Linq;
using Windows.Foundation;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Automation;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using FrontlineApp.UWP.Views.Properties;
using FrontlineApp.UWP.Views.Authentication;
using FrontlineApp.UWP.Views.Alerts;
using FrontlineApp.Utility.Constants;
using FrontlineApp.Utility;
using System.ComponentModel;
using FrontlineApp.Utility.Helper.AppSession;
// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace FrontlineApp.UWP.Views.Navigation
{
    /// <summary>
    /// The "chrome" layer of the app that provides top-level navigation with
    /// proper keyboarding navigation.
    /// </summary>
    public sealed partial class AppShell : Page, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        // Declare the top level nav items
        private ObservableCollection<NavMenuItem> navlist = new ObservableCollection<NavMenuItem>(
            new[]
            {
                new NavMenuItem(){ SelectedStateImage = "dashboard_selected.png", NormalStateImage = "dashboard.png", Label = "Dashboard", DestPage = typeof(DashboardPage)},
                new NavMenuItem(){ SelectedStateImage = "viewings_selected.png", NormalStateImage = "viewings.png", Label = "Viewings", DestPage = typeof(ViewingsMasterPage)},
                new NavMenuItem(){ SelectedStateImage = "contact_selected.png", NormalStateImage = "contact.png", Label = "Customers", DestPage = typeof(CustomersMainPage)},
                new NavMenuItem(){ SelectedStateImage = "properties_selected.png", NormalStateImage = "properties.png", Label = "Properties", DestPage = typeof(PropertiesMasterPage)},
                //new NavMenuItem(){ SelectedStateImage = "alerts_selected.png", NormalStateImage = "alerts.png", Label = "Alerts", DestPage = typeof(AlertsMasterPage)},
                //new NavMenuItem(){ SelectedStateImage = "income_management_selected.png", NormalStateImage = "income_management.png", Label = "Income Management", DestPage = typeof(DashboardPage)},
                //new NavMenuItem(){ SelectedStateImage = "faults_selected.png", NormalStateImage = "faults.png", Label = "Unassigned Faults", DestPage = typeof(DashboardPage)},
                new NavMenuItem(){ SelectedStateImage = "logout_selected.png", NormalStateImage = "logout.png", Label = "Logout", DestPage = typeof(LoginPage)}
            });

        public static AppShell Current = null;
        public string _appVersion;
        public string AppVersion
        {
            get { return _appVersion; }
            set { _appVersion = value; NotifyPropertyChanged("AppVersion"); }
        }
        private string _loggedInUser;
        public string LoggedInUser
        {
            get { return _loggedInUser; }
            set { _loggedInUser = value;NotifyPropertyChanged("LoggedInUser"); }
        }
        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        /// <summary>
        /// Initializes a new instance of the AppShell, sets the static 'Current' reference,
        /// adds callbacks for Back requests and changes in the SplitView's DisplayMode, and
        /// provide the nav menu list with the data to display.
        /// </summary>
        public AppShell()
        {
            this.InitializeComponent();
            AppVersion = GetAppVersion();
            LoggedInUser = AppSession.LoggedInUser.employeeFullName;
            this.Loaded += (sender, args) =>
            {
                Current = this;
                this.CheckTogglePaneButtonSizeChanged();
            };

            this.RootSplitView.RegisterPropertyChangedCallback(
                SplitView.DisplayModeProperty,
                (s, a) =>
                {
                    // Ensure that we update the reported size of the TogglePaneButton when the SplitView's
                    // DisplayMode changes.
                    this.CheckTogglePaneButtonSizeChanged();
                });

            SystemNavigationManager.GetForCurrentView().BackRequested += SystemNavigationManager_BackRequested;
            SystemNavigationManager.GetForCurrentView().AppViewBackButtonVisibility = AppViewBackButtonVisibility.Visible;

            NavMenuList.ItemsSource = navlist;
        }
        private string GetAppVersion()
        {
            string version = "v" + GeneralHelper.GetAppVersion();
            string baseURL = PathConstants.BaseURL;
            if (string.Equals(baseURL, ApplicationConstants.ServerURLDev))
            {
                version = version + " (D)";
            }
            else if (string.Equals(baseURL, ApplicationConstants.ServerURLTest))
            {
                version = version + " (T)";
            }
            else if (string.Equals(baseURL, ApplicationConstants.ServerURLUAT))
            {
                version = version + " (U)";
            }
            else if (string.Equals(baseURL, ApplicationConstants.ServerURLLive))
            {
                version = version + " (L)";
            }
            return version;
        }
        public Frame AppFrame { get { return this.frame; } }

        #region BackRequested Handlers

        private void SystemNavigationManager_BackRequested(object sender, BackRequestedEventArgs e)
        {
            bool handled = e.Handled;
            this.BackRequested(ref handled);
            e.Handled = handled;
        }

        private void BackRequested(ref bool handled)
        {
            // Get a hold of the current frame so that we can inspect the app back stack.

            if (this.AppFrame == null)
                return;

            // Check to see if this is the top-most page on the app back stack.
            if (this.AppFrame.CanGoBack && !handled)
            {
                // If not, set the event to handled and go back to the previous page in the app.
                handled = true;
                this.AppFrame.GoBack();
            }
        }

        #endregion

        #region Navigation

        /// <summary>
        /// Navigate to the Page for the selected <paramref name="listViewItem"/>.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="listViewItem"></param>
        private void NavMenuList_ItemInvoked(object sender, ListViewItem listViewItem)
        {
            foreach (var i in navlist)
            {
                i.IsSelected = false;
            }

            var item = (NavMenuItem)((NavMenuListView)sender).ItemFromContainer(listViewItem);

            if (item != null)
            {
                item.IsSelected = true;
                if (item.DestPage != null &&
                    item.DestPage != this.AppFrame.CurrentSourcePageType)
                {
                    if (item.DestPage == typeof(LoginPage))
                    {
                        this.HidePaneVisibility();
                        this.AppFrame.Navigate(item.DestPage, item.Arguments);
                        this.AppFrame.BackStack.Clear();
                    }
                    else
                    {
                        this.AppFrame.Navigate(item.DestPage, item.Arguments);
                    }
                    
                }
            }

        }
        public void HidePaneVisibility()
        {
            RootSplitView.CompactPaneLength = 0;
            TogglePaneButton.Visibility = Visibility.Collapsed;
        }
        /// <summary>
        /// Ensures the nav menu reflects reality when navigation is triggered outside of
        /// the nav menu buttons.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnNavigatingToPage(object sender, NavigatingCancelEventArgs e)
        {
            if (e.NavigationMode == NavigationMode.Back)
            {
                var item = (from p in this.navlist where p.DestPage == e.SourcePageType select p).FirstOrDefault();
                if (item == null && this.AppFrame.BackStackDepth > 0)
                {
                    // In cases where a page drills into sub-pages then we'll highlight the most recent
                    // navigation menu item that appears in the BackStack
                    foreach (var entry in this.AppFrame.BackStack.Reverse())
                    {
                        item = (from p in this.navlist where p.DestPage == entry.SourcePageType select p).SingleOrDefault();
                        if (item != null)
                            break;
                    }
                }

                foreach (var i in navlist)
                {
                    i.IsSelected = false;
                }
                if (item != null)
                {
                    item.IsSelected = true;
                }

                var container = (ListViewItem)NavMenuList.ContainerFromItem(item);

                // While updating the selection state of the item prevent it from taking keyboard focus.  If a
                // user is invoking the back button via the keyboard causing the selected nav menu item to change
                // then focus will remain on the back button.
                if (container != null) container.IsTabStop = false;
                NavMenuList.SetSelectedItem(container);
                if (container != null) container.IsTabStop = true;
            }
        }

        #endregion

        public Rect TogglePaneButtonRect
        {
            get;
            private set;
        }

        /// <summary>
        /// Callback when the SplitView's Pane is toggled closed.  When the Pane is not visible
        /// then the floating hamburger may be occluding other content in the app unless it is aware.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TogglePaneButton_Unchecked(object sender, RoutedEventArgs e)
        {
            this.CheckTogglePaneButtonSizeChanged();
        }

        /// <summary>
        /// Callback when the SplitView's Pane is toggled opened.
        /// Restores divider's visibility and ensures that margins around the floating hamburger are correctly set.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TogglePaneButton_Checked(object sender, RoutedEventArgs e)
        {
            this.CheckTogglePaneButtonSizeChanged();
        }

        /// <summary>
        /// Check for the conditions where the navigation pane does not occupy the space under the floating
        /// hamburger button and trigger the event.
        /// </summary>
        private void CheckTogglePaneButtonSizeChanged()
        {
            if (this.RootSplitView.DisplayMode == SplitViewDisplayMode.Inline ||
                this.RootSplitView.DisplayMode == SplitViewDisplayMode.Overlay)
            {
                var transform = this.TogglePaneButton.TransformToVisual(this);
                var rect = transform.TransformBounds(new Rect(0, 0, this.TogglePaneButton.ActualWidth, this.TogglePaneButton.ActualHeight));
                this.TogglePaneButtonRect = rect;
            }
            else
            {
                this.TogglePaneButtonRect = new Rect();
            }
        }

        /// <summary>
        /// Enable accessibility on each nav menu item by setting the AutomationProperties.Name on each container
        /// using the associated Label of each item.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        private void NavMenuItemContainerContentChanging(ListViewBase sender, ContainerContentChangingEventArgs args)
        {
            if (!args.InRecycleQueue && args.Item != null && args.Item is NavMenuItem)
            {
                args.ItemContainer.SetValue(AutomationProperties.NameProperty, ((NavMenuItem)args.Item).Label);
            }
            else
            {
                args.ItemContainer.ClearValue(AutomationProperties.NameProperty);
            }
        }
    }
}
