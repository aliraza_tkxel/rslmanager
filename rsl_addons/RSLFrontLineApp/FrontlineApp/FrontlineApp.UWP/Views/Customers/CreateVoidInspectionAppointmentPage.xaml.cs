﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.UWP.ViewModels;
using FrontlineApp.UWP.Views.Navigation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace FrontlineApp.UWP.Views.Customers
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class CreateVoidInspectionAppointmentPage : Page
    {
        public CreateVoidInspectionAppointmentPage()
        {
            this.InitializeComponent();
        }

        public CreateVoidInspectionViewModel GetDataContext()
        {
            return this.DataContext as CreateVoidInspectionViewModel;
        }

        private void PopulateSupButton_Click(object sender, RoutedEventArgs e)
        {
            GetDataContext().FetchSupervisors();
        }

        private void btnDone_Click(object sender, RoutedEventArgs e)
        {
            CreateInspection();
        }

        public async void CreateInspection()
        {
            ResultModel<bool> res = await GetDataContext().CreateVoidInspection();
            if (res.Success)
            {
                GetDataContext().showMessage("Success", res.Message);
                RefreshcustomerListing();
            }
            else
            {
                if (!res.Message.Equals("VE")) //Validation Error
                {
                    GetDataContext().showMessage("Error", res.Message);
                }
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            RefreshcustomerListing();
        }

        private void RefreshcustomerListing()
        {
            this.Frame.BackStack.Clear();
            this.Frame.Navigate(typeof(NoCustomerSelectedPage));

            AppShell shell = Window.Current.Content as AppShell;
            shell.AppFrame.Navigate(typeof(CustomersMainPage), new SuppressNavigationTransitionInfo());
        }
    }
}
