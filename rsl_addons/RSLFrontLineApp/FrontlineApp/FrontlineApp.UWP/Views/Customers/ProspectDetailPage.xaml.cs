﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.Utility;
using FrontlineApp.UWP.UserControls.Customers;
using FrontlineApp.UWP.ViewModels;
using System.Threading.Tasks;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using System;
using FrontlineApp.Utility.Constants;
using FrontlineApp.UWP.Views.Viewings;
using System.Collections.Generic;
using Microsoft.Practices.ServiceLocation;
using FrontlineApp.ApplicationLayer.Enums;
using FrontlineApp.Utility.Helper.AppSession;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace FrontlineApp.UWP.Views.Customers
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ProspectDetailPage : Page
    {
        ProspectDetailViewModel viewmodel;

        public ProspectDetailPage()
        {
            this.InitializeComponent();
            viewmodel = this.DataContext as ProspectDetailViewModel;
        }

        private void rootPivot_PivotItemLoading(Pivot sender, PivotItemEventArgs args)
        {
            // Create the user control for the selected pivot item
            var pivotItemContentControl = CreateUserControlForPivotItem(((Pivot)sender).SelectedIndex, viewmodel.SelectedCustomer);

            // Update the content of the pivot item
            args.Item.Content = pivotItemContentControl;
        }

        private UserControl CreateUserControlForPivotItem(int selectedIndex, CustomerModel customer)
        {
            switch (selectedIndex)
            {
                case 1:
                    AppSession.SelectedProspectItem = ProspectItems.Viewings;
                    return new ViewingsControl(customer);
                default:
                    AppSession.SelectedProspectItem = ProspectItems.GeneralInfo;
                    var generalControl = new GeneralInfoControl(customer);
                    generalControl.OnNavigationToAddEditCustomer += GeneralControl_OnNavigationToAddEditCustomer;
                    return generalControl;
            }
        }

        private void GeneralControl_OnNavigationToAddEditCustomer(CustomerModel customer)
        {
            this.Frame.Navigate(typeof(AddEditCustomerPage), customer);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            var selectedCustomer = (CustomerModel)e.Parameter;
            viewmodel.SelectedCustomer = selectedCustomer;

            switch (AppSession.SelectedProspectItem)
            {
                case ProspectItems.GeneralInfo:
                    this.rootPivot.SelectedIndex = (int)ProspectItems.GeneralInfo;
                    break;

                case ProspectItems.Viewings:
                    this.rootPivot.SelectedIndex = (int)ProspectItems.Viewings;
                    break;                
            }

        }


        private async void btnSetupAndStartTenancy_Click(object sender, RoutedEventArgs e)
        {
            if (GeneralHelper.HasInternetConnection())
            {
                SetupAndAddTenancyViewModel aoccup = ServiceLocator.Current.GetInstance<SetupAndAddTenancyViewModel>();
                aoccup.ClearAllData();
                this.Frame.Navigate(typeof(SetupAndStartTenancyPage));
                ProspectDetailViewModel vm = this.DataContext as ProspectDetailViewModel;
                vm.SendTenancyMessages();
            }
            else
            {
                await ShowMessage(MessageConstants.WarningTitle, MessageConstants.TenancySetupOfflineError);
            }
        }

        private async void btnViewings_Click(object sender, RoutedEventArgs e)
        {
            if (GeneralHelper.HasInternetConnection())
            {
               
                this.Frame.Navigate(typeof(ArrangeNewViewingPage));
                viewmodel.SendViewingMessage();
            }
            else
            {
                await ShowMessage(MessageConstants.WarningTitle, MessageConstants.ArrangeViewingOfflineError);
            }
        }

        public async Task<bool> ShowMessage(string header, string message)
        {
            var dialog = new MessageDialog(message);
            dialog.Title = header;
            dialog.Commands.Add(new UICommand { Label = "OK", Id = 0 });
            await dialog.ShowAsync();
            return true;
        }
    }
}
