﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.UWP.UserControls.Customers;
using FrontlineApp.UWP.ViewModels;
using FrontlineApp.UWP.Views.Viewings;
using System.Threading.Tasks;
using Windows.UI.Popups;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using System;
using FrontlineApp.Utility;
using FrontlineApp.Utility.Constants;
using FrontlineApp.Utility.Helper.AppSession;
using FrontlineApp.ApplicationLayer.Enums;
using System.Collections.Generic;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace FrontlineApp.UWP.Views.Customers
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class TenantDetailPage : Page
    {
        TenantDetailViewModel viewmodel;

        public TenantDetailPage()
        {
            this.InitializeComponent();
            viewmodel = this.DataContext as TenantDetailViewModel;

        }

       private void rootPivot_PivotItemLoading(Pivot sender, PivotItemEventArgs args)
        {  
            // Create the user control for the selected pivot item
            var pivotItemContentControl = CreateUserControlForPivotItem(((Pivot)sender).SelectedIndex, viewmodel.SelectedCustomer);
            // Update the content of the pivot item
            args.Item.Content = pivotItemContentControl;
        }

        private UserControl CreateUserControlForPivotItem(int selectedIndex, CustomerModel customer)
        {
            switch (selectedIndex)
            {
                case 1:
                    AppSession.SelectedTenantItem = TenantItems.Viewings;
                    return new ViewingsControl(customer);                    
                case 2:
                    AppSession.SelectedTenantItem = TenantItems.Referral;
                    var referralControl = new ReferralDetailControl(customer);
                    referralControl.OnNavigationToAddEditReferral += ReferralControl_OnNavigationToAddEditReferral;
                    return referralControl;
                case 3:
                    AppSession.SelectedTenantItem = TenantItems.Risks;
                    var riskControl = new RiskControl(customer);
                    riskControl.OnNavigationToRiskDetail += RiskControl_OnNavigationToRiskDetail;
                    return riskControl;
                case 4:
                    AppSession.SelectedTenantItem = TenantItems.Vulnerabilities;
                    var vulnerabilityControl = new VulnerabilityControl(customer);
                    vulnerabilityControl.OnNavigationToVulnerabilityDetail += VulnerabilityControl_OnNavigationToVulnerabilityDetail; ;
                    return vulnerabilityControl;
                default:
                    AppSession.SelectedTenantItem = TenantItems.GeneralInfo;
                    var generalControl = new GeneralInfoControl(customer);
                    generalControl.OnNavigationToAddEditCustomer += GeneralControl_OnNavigationToAddEditCustomer;
                    return generalControl;
            }
        }

        private void VulnerabilityControl_OnNavigationToVulnerabilityDetail(CustomerVulnerabilityModel vulnerability)
        {
            this.Frame.Navigate(typeof(VulnerabilityDetailPage), vulnerability);
        }

        private void RiskControl_OnNavigationToRiskDetail(CustomerRiskModel risk)
        {
            this.Frame.Navigate(typeof(RiskDetailPage), risk);
        }

        private void GeneralControl_OnNavigationToAddEditCustomer(CustomerModel customer)
        {
            this.Frame.Navigate(typeof(AddEditCustomerPage), customer);
        }

        private void ReferralControl_OnNavigationToAddEditReferral(CustomerModel customer)
        {
            this.Frame.Navigate(typeof(AddEditReferralPage), customer);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {

            base.OnNavigatedTo(e);

            var selectedCustomer = (CustomerModel)e.Parameter;
            viewmodel.SelectedCustomer = selectedCustomer;

            switch (AppSession.SelectedTenantItem)
            {
                case TenantItems.GeneralInfo:
                    this.rootPivot.SelectedIndex = (int)TenantItems.GeneralInfo;
                    break;

                case TenantItems.Viewings:
                    this.rootPivot.SelectedIndex = (int)TenantItems.Viewings;
                    break;

                case TenantItems.Referral:
                    this.rootPivot.SelectedIndex = (int)TenantItems.Referral;
                    break;

                case TenantItems.Risks:
                    this.rootPivot.SelectedIndex = (int)TenantItems.Risks;
                    break;

                case TenantItems.Vulnerabilities:
                    this.rootPivot.SelectedIndex = (int)TenantItems.Vulnerabilities;
                    break;
            }


        }

        private async void btnTermination_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            if (GeneralHelper.HasInternetConnection())
            {
                this.Frame.Navigate(typeof(TerminationPage), viewmodel.SelectedCustomer);
            }
            else
            {
               await ShowMessage(MessageConstants.WarningTitle, MessageConstants.TenancyTerminationOfflineError);
            }
        }

        private async void btnViewings_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            if (GeneralHelper.HasInternetConnection())
            {
                this.Frame.Navigate(typeof(ArrangeNewViewingPage));
                viewmodel.SendViewingMessage();
            }
            else
            {
                await ShowMessage(MessageConstants.WarningTitle, MessageConstants.ArrangeViewingOfflineError);
            }
        }

        public async Task<bool> ShowMessage(string header, string message)
        {
            var dialog = new MessageDialog(message);
            dialog.Title = header;
            dialog.Commands.Add(new UICommand { Label = "OK", Id = 0 });
            await dialog.ShowAsync();
            return true;
        }

    }
}
