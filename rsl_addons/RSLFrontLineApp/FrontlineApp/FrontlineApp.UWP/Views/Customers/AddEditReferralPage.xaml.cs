﻿using System;
using System.Threading.Tasks;
using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.Utility.Constants;
using FrontlineApp.UWP.Popups;
using FrontlineApp.UWP.ViewModels;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using FrontlineApp.Utility;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace FrontlineApp.UWP.Views.Customers
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AddEditReferralPage : Page
    {

        AddEditReferralViewModel viewModel;

        public AddEditReferralPage()
        {
            this.InitializeComponent();
            viewModel = this.DataContext as AddEditReferralViewModel;
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            BusyIndicator busyIndicator = BusyIndicator.Start(MessageConstants.Loading);
            viewModel.SelectedCustomer = e.Parameter as CustomerModel;
            viewModel.InitializeData();
            busyIndicator.Close();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            if (this.Frame.CanGoBack)
            {
                this.Frame.GoBack();
            }
        }

        private async void btnDone_Click(object sender, RoutedEventArgs e)
        {
            if (GeneralHelper.HasInternetConnection())
            {
                BusyIndicator busyIndicator = BusyIndicator.Start(MessageConstants.SavingReferral);
                var result = await viewModel.Save();
                busyIndicator.Close();

                if (result.Success)
                {
                    await ShowMessage(MessageConstants.SuccessTitle, MessageConstants.ReferralSavedSuccess);
                    if (this.Frame.CanGoBack)
                    {
                        this.Frame.GoBack();
                    }
                }
                else
                {
                    await ShowMessage(MessageConstants.ErrorMessageBoxHeader, result.Message);
                }
            }
            else
            {
                await ShowMessage(MessageConstants.WarningTitle, MessageConstants.AddEditReferralOfflineError);
            }
        }

        public async Task<bool> ShowMessage(string header, string message)
        {
            var dialog = new MessageDialog(message);
            dialog.Title = header;
            dialog.Commands.Add(new UICommand { Label = "OK", Id = 0 });
            await dialog.ShowAsync();
            return true;
        }
    }
}
