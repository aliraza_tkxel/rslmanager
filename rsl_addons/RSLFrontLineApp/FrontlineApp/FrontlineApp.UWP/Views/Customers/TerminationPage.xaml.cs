﻿using FrontlineApp.UWP.ViewModels;
using System.Threading.Tasks;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using System;
using FrontlineApp.UWP.Popups;
using FrontlineApp.Utility.Constants;
using Windows.UI.Xaml.Navigation;
using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.Utility;
using FrontlineApp.UWP.Views.Navigation;
using Windows.UI.Xaml.Media.Animation;
using FrontlineApp.ServiceLayer.ServiceObjects;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace FrontlineApp.UWP.Views.Customers
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class TerminationPage : Page
    {

        TerminationViewModel viewModel;

        public TerminationPage()
        {
            this.InitializeComponent();
            viewModel = this.DataContext as TerminationViewModel;
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            if (this.Frame.CanGoBack)
            {
                this.Frame.GoBack();
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            BusyIndicator busyIndicator = BusyIndicator.Start(MessageConstants.Loading);
            viewModel.SelectedCustomer = e.Parameter as CustomerModel;
            viewModel.InitializeData();
            busyIndicator.Close();
        }

        private async void btnDone_Click(object sender, RoutedEventArgs e)
        {
            if (GeneralHelper.HasInternetConnection())
            {
                ResultModel<bool> validationResult = ValidateData();
                if(validationResult.Success == true)
                {
                    BusyIndicator busyIndicator = BusyIndicator.Start(MessageConstants.Processing);
                    var result = await viewModel.TerminateTenancy();
                    busyIndicator.Close();

                    if (result.Success)
                    {
                        if (viewModel.ShouldShowVoidPopup())
                        {
                            ShowVoidInspectionAlert();
                        }
                        else
                        {
                            await ShowMessage(MessageConstants.SuccessTitle, MessageConstants.TenancyTerminatedSuccessfully);
                            RefreshcustomerListing();
                        }
                    }
                    else
                    {
                        await ShowMessage(MessageConstants.ErrorMessageBoxHeader, result.Message);
                    }
                }
                else
                {
                    await ShowMessage(MessageConstants.AlertMessageBoxHeader, validationResult.Message);
                }
                
            }
            else
            {
                await ShowMessage(MessageConstants.WarningTitle, MessageConstants.TenancyTerminationOfflineError);
            }
        }

        public ResultModel<bool> ValidateData()
        {
            if (viewModel.SelectedCustomer.Tenancy != null)
            {
                viewModel.Termination.TenancyStartDate = viewModel.SelectedCustomer.Tenancy.TenancyStartDate;
            }
            viewModel.Termination.FilteredReasonCategoryList = viewModel.FilteredReasonCategoryList;

            var validationResult = viewModel.IsValid();
            return validationResult;
        }

        public async Task<bool> ShowMessage(string header, string message)
        {
            var dialog = new MessageDialog(message);
            dialog.Title = header;
            dialog.Commands.Add(new UICommand { Label = "OK", Id = 0 });
            await dialog.ShowAsync();
            return true;
        }


        public async void ShowVoidInspectionAlert()
        {
            var messageDialog = new MessageDialog("Would you like to arrange a 1st inspection?");
            messageDialog.Title = "Arrange 1st Inspection";
            // Add commands and set their callbacks; both buttons use the same callback function instead of inline event handlers
            messageDialog.Commands.Add(new UICommand(
                "Arrange 1st inspection Now", new UICommandInvokedHandler(YesCommandInvokedHandler)));
            messageDialog.Commands.Add(new UICommand(
                "Arrange 1st inspection Later", new UICommandInvokedHandler(NoCommandInvokedHandler)));

            // Set the command that will be invoked by default
            messageDialog.DefaultCommandIndex = 0;

            // Set the command to be invoked when escape is pressed
            messageDialog.CancelCommandIndex = 1;

            // Show the message dialog
            await messageDialog.ShowAsync();
        }


        private async void YesCommandInvokedHandler(IUICommand command)
        {
            if (viewModel.TerminationResposne.journalId != null)
            {
                this.Frame.BackStack.Clear();
                this.Frame.Navigate(typeof(CreateVoidInspectionAppointmentPage));
                viewModel.SendVoidInspectionMessage();
            }
            else
            {
                await ShowMessage("Error", MessageConstants.TenancyTerminatedJournalIdNull);
                RefreshcustomerListing();
            }
            
        }
        private async void NoCommandInvokedHandler(IUICommand command)
        {
            await ShowMessage(MessageConstants.SuccessTitle, MessageConstants.TenancyTerminatedSuccessfully);
            RefreshcustomerListing();
        }

        private void RefreshcustomerListing()
        {
            this.Frame.BackStack.Clear();
            this.Frame.Navigate(typeof(NoCustomerSelectedPage));

            AppShell shell = Window.Current.Content as AppShell;
            shell.AppFrame.Navigate(typeof(CustomersMainPage), new SuppressNavigationTransitionInfo());
        }

    }
}
