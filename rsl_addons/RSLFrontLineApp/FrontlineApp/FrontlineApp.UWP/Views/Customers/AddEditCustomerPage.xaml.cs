﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.Utility.Constants;
using FrontlineApp.UWP.Popups;
using FrontlineApp.UWP.ViewModels;
using Windows.UI.Popups;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using System;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using FrontlineApp.UWP.ViewModels.Messages;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace FrontlineApp.UWP.Views.Customers
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AddEditCustomerPage : Page
    {

        public AddEditCustomerPage() : base()
        {
            this.InitializeComponent();
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            BusyIndicator busyIndicator = BusyIndicator.Start(MessageConstants.Loading);

            AddEditCustomerViewModel vm = this.DataContext as AddEditCustomerViewModel;
            var customer = e.Parameter as CustomerModel;
            ResultModel<bool> result = vm.InitializeData(customer);
            
            busyIndicator.Close();
            if (result.Success == false)
            {
               await ShowMessage(MessageConstants.ErrorMessageBoxHeader, result.Message);
            }

        }

        private void btnCancel_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            if (this.Frame.CanGoBack)
            {
                this.Frame.GoBack();
            }
        }

        private async void btnDone_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            BusyIndicator busyIndicator = BusyIndicator.Start(MessageConstants.SavingCustomerRecord);

            AddEditCustomerViewModel vm = this.DataContext as AddEditCustomerViewModel;
            ResultModel<CustomerModel> result = await vm.Save();

            busyIndicator.Close();

            if (result.Success == true)
            {
                await ShowMessage(MessageConstants.SuccessTitle, MessageConstants.CustomerSavedSuccessFully);
                Messenger.Default.Send(new RefreshCustomerListMessage() { IsLocalList=true });
                this.Frame.BackStack.Clear();
                this.Frame.Navigate(typeof(NoCustomerSelectedPage));
            }
            else
            {
                await ShowMessage(MessageConstants.ErrorMessageBoxHeader, result.Message);
            }
        }

        public async Task<bool> ShowMessage(string header, string message)
        {
            var dialog = new MessageDialog(message);
            dialog.Title = header;
            dialog.Commands.Add(new UICommand { Label = "OK", Id = 0 });
            await dialog.ShowAsync();
            return true;
        }
    }
}
