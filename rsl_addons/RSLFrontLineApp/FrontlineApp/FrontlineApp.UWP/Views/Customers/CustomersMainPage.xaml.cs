﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.UWP.Popups;
using FrontlineApp.UWP.ViewModels;
using System.Threading.Tasks;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;
using System;
using FrontlineApp.Utility.Constants;
using FrontlineApp.Utility;
using FrontlineApp.Utility.Helper.AppSession;
using FrontlineApp.ApplicationLayer.Enums;
using System.Collections.Generic;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace FrontlineApp.UWP.Views.Customers
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class CustomersMainPage : Page
    {
        CustomerMainViewModel viewModel;
        public CustomersMainPage()
        {
            this.InitializeComponent();
            viewModel = this.DataContext as CustomerMainViewModel;
            LoadDefaultView();
        }

        private void LoadDefaultView()
        {
            CustomerDetailFrame.BackStack.Clear();
            CustomerDetailFrame.Navigate(typeof(NoCustomerSelectedPage));
        }

        private void OnItemClick(object sender, ItemClickEventArgs e)
        {
            bool isExist = viewModel.CheckOfflineRecordsExists();
            if (isExist)
            {
                SyncOfflineRecords();
            }
            else
            {
               var  selectedCustomer = e.ClickedItem as CustomerModel;
                if (selectedCustomer.CustomerType.Equals("Tenant"))
                {
                    AppSession.SelectedTenantItem = TenantItems.GeneralInfo;
                    CustomerDetailFrame.Navigate(typeof(TenantDetailPage), selectedCustomer, new DrillInNavigationTransitionInfo());
                }
                else
                {
                    AppSession.SelectedProspectItem = ProspectItems.GeneralInfo;
                    CustomerDetailFrame.Navigate(typeof(ProspectDetailPage), selectedCustomer, new DrillInNavigationTransitionInfo());
                }
            }

        }

        private void btnAddCustomer_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            CustomerDetailFrame.Navigate(typeof(AddEditCustomerPage), new DrillInNavigationTransitionInfo());
        }


        private void CustomerListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            foreach (var item in e.AddedItems)
            {
                ListViewItem lvi = (sender as ListView).ContainerFromItem(item) as ListViewItem;
                if(lvi!=null)
                    lvi.ContentTemplate = (DataTemplate)this.Resources["SelectedCustomerListViewTemplate"];
            }

            foreach (var item in e.RemovedItems)
            {
                ListViewItem lvi = (sender as ListView).ContainerFromItem(item) as ListViewItem;
                if (lvi != null)
                    lvi.ContentTemplate = (DataTemplate)this.Resources["CustomerListViewTemplate"];
            }
        }

        private void autoSuggestBoxSearchCustomers_QuerySubmitted(AutoSuggestBox sender, AutoSuggestBoxQuerySubmittedEventArgs args)
        {
            SearchText();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.viewModel.SearchText = string.Empty;
            if (e.Parameter != null)
            {
                Dictionary<string, string> dict = e.Parameter as Dictionary<string, string>;
                if (dict!=null)
                {
                    if (dict.ContainsKey("searchQuery"))
                    {
                        this.viewModel.SearchText = dict["searchQuery"];
                    }
                    else
                    {
                        this.viewModel.SearchText = string.Empty;
                    }
                }
                
                
            }
            SearchText();
        }

        private async void SearchText()
        {
            LoadDefaultView();
            this.viewModel.ResetcustomerList();

            bool isExist = viewModel.CheckOfflineRecordsExists();
            if (isExist)
            {
              SyncOfflineRecords();
            }
            else
            {

                if (GeneralHelper.HasInternetConnection())
                {
                    BusyIndicator serverIndicator = BusyIndicator.Start(MessageConstants.FetchingCustomerRecordsFromServer);
                    var serverResult = await viewModel.FetchRecordsFromServer();
                    serverIndicator.Close();

                    if (serverResult.Success == false)
                    {
                        await ShowMessage(MessageConstants.ErrorMessageBoxHeader, serverResult.Message);
                    }
                }
                else
                {
                    await ShowMessage(MessageConstants.WarningTitle, MessageConstants.InternetNotAvailableShowOffline);

                    BusyIndicator busyIndicator = BusyIndicator.Start(MessageConstants.LoadingCustomers);
                    viewModel.FetchRecordsFromLocalDb();
                    busyIndicator.Close();
                }
            }

        }


        private async void SyncOfflineRecords()
        {
            ContentDialog postDataDialog = new ContentDialog()
            {
                Title = MessageConstants.WarningTitle,
                Content = MessageConstants.OfflineCustomersExistWarning,
                PrimaryButtonText = "Post Data",
                SecondaryButtonText = "Cancel"
            };

            ContentDialogResult result = await postDataDialog.ShowAsync();
            if (result == ContentDialogResult.Primary)
            {
                // Check internet
                if (GeneralHelper.HasInternetConnection())
                {
                    // Sync offline records
                    BusyIndicator syncingIndicator = BusyIndicator.Start(MessageConstants.SyncingOfflineCustomerRecords);
                    var syncResult = await viewModel.SyncOfflineRecords();
                    syncingIndicator.Close();

                    if (syncResult.Success)
                    {
                        BusyIndicator serverIndicator = BusyIndicator.Start(MessageConstants.FetchingCustomerRecordsFromServer);
                        var serverResult = await viewModel.FetchRecordsFromServer();
                        serverIndicator.Close();

                        if (serverResult.Success == false)
                        {
                            await ShowMessage(MessageConstants.ErrorMessageBoxHeader, serverResult.Message);
                        }
                    }
                    else
                    {
                        await ShowMessage(MessageConstants.ErrorMessageBoxHeader, syncResult.Message);
                    }
                }
                else
                {
                    await ShowMessage(MessageConstants.WarningTitle, MessageConstants.InternetNotAvailableShowOffline);

                    BusyIndicator busyIndicator = BusyIndicator.Start(MessageConstants.LoadingCustomers);
                    viewModel.FetchRecordsFromLocalDb();
                    busyIndicator.Close();
                }
            }
            else
            {
                BusyIndicator busyIndicator = BusyIndicator.Start(MessageConstants.LoadingCustomers);
                viewModel.FetchRecordsFromLocalDb();
                busyIndicator.Close();
            }
        }

        public async Task<bool> ShowMessage(string header, string message)
        {
            var dialog = new MessageDialog(message);
            dialog.Title = header;
            dialog.Commands.Add(new UICommand { Label = "OK", Id = 0 });
            await dialog.ShowAsync();
            return true;
        }
    }
}
