﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.UWP.UserControls.Customers;
using FrontlineApp.UWP.ViewModels;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml;
using FrontlineApp.UWP.Views.Properties;
using FrontlineApp.UWP.Views.Navigation;
using Windows.UI.Xaml.Media.Animation;
using FrontlineApp.Utility;
using FrontlineApp.Utility.Constants;
using FrontlineApp.ServiceLayer.ServiceObjects;
using FrontlineApp.Utility.Helper.AppSession;
using System.Linq;
using System;
using Windows.UI.Popups;
using FrontlineApp.UWP.Popups;
using System.Collections.ObjectModel;
using System.Collections.Generic;
// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace FrontlineApp.UWP.Views.Customers
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    ///

    public sealed partial class SetupAndStartTenancyPage : Page
    {
        public SetupAndStartTenancyPage()
        {
            this.InitializeComponent();
            ConfigurePivotControls();
            
            
        }
        #region "UserControls"
        private PaythruPaymentControl PayThruPayMentCtrl { get; set; }
        private TenancyInfoControl TenancyInfoCtrl { get; set; }
        private JointTenantControl JointTenantCtrl { get; set; }
        private AddOccupantsControl AddOccuptantsCtrl { get; set; }
        private TenancyReferralControl TenancyReferralCtrl { get; set; }
        private StartTenancyControl StartTenancyCtrl { get; set; }
        private TenancyCustomerControl TenancyCustomerCtrl { get; set; }
        private TenancyPropertyControl TenancyPropertyCtrl { get; set; }
        private SelectTenancyCustomerControl SelectCustomerCtrl { get; set; }
        private SelectTenancyPropertyControl SelectPropertyCtrl { get; set; }
        #endregion

        #region "Validation"
        public bool CheckDataStatus(UserControl item)
        {
            if (item == TenancyPropertyCtrl)
            {
                if (string.IsNullOrWhiteSpace(GetContext().SelectedProperty.propertyId))
                {
                    GetContext().showMessage("Alert", "Select Property"+ "\r\r"+"Please select a property first");
                    return false;
                }
            }
            else if (item == TenancyCustomerCtrl)
            {
                if (GetContext().SelectedCustomer.CustomerId == null)
                {
                    GetContext().showMessage("Alert", "Select Customer" + "\r\r" + "Please select a customer first");
                    return false;
                }
            }
            else if(item == TenancyInfoCtrl)
            {
                ResultModel<bool> status = TenancyInfoCtrl.GetContext().IsValid();
                if (!status.Value)
                {
                    GetContext().showMessage("Alert", status.Message);
                }
                return status.Value;
            }
            else if (item == JointTenantCtrl)
            {
                ResultModel<bool> status = JointTenantCtrl.GetDataContext().IsValid();
                if (!status.Value)
                {
                    GetContext().showMessage("Alert", status.Message);
                }
                return status.Value;
            }
            else if(item == AddOccuptantsCtrl)
            {
                ResultModel<bool> status = AddOccuptantsCtrl.GetDataContext().IsValid();
                if (!status.Value)
                {
                    GetContext().showMessage("Alert", status.Message);
                }
                return status.Value;
            }

            else if (item == TenancyReferralCtrl)
            {
                ResultModel<bool> status = TenancyReferralCtrl.GetContext().IsValid();
                if (!status.Value)
                {
                    GetContext().showMessage("Alert", status.Message);
                }
                return status.Value;
            }
            else if (item == StartTenancyCtrl)
            {
                ResultModel<bool> status = StartTenancyCtrl.GetDataContext().IsValid();
                if (!status.Value)
                {
                    GetContext().showMessage("Alert", status.Message);
                }
                return status.Value;
            }

            return true;
        }

       

        
        #endregion

        #region "General Methods"

        private void ConfigurePivotControls()
        {
            TenancyInfoCtrl = new TenancyInfoControl();
            AddOccuptantsCtrl = new AddOccupantsControl();
            TenancyReferralCtrl = new TenancyReferralControl();

            StartTenancyCtrl = new StartTenancyControl();
            
            TenancyInfoCtrl = new TenancyInfoControl();

            TenancyPropertyCtrl = new TenancyPropertyControl();
            TenancyPropertyCtrl.NavigateToSelectProperty += Control_NavigateToSelectProperty;

            TenancyCustomerCtrl = new TenancyCustomerControl();
            TenancyCustomerCtrl.NavigateToSelectCustomer += Control_NavigateToSelectCustomer;

            JointTenantCtrl = new JointTenantControl();
            JointTenantCtrl.NavigateToSelectCustomer += JointTenantCtrl_NavigateToSelectCustomer;

            
        }

        

        private SetupAndAddTenancyViewModel GetContext()
        {
            return this.DataContext as SetupAndAddTenancyViewModel;
        }

        
        

        private void BtnDone_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            ResultModel<bool> result = StartTenancyCtrl.GetDataContext().IsValid();
            if (result.Value)
            {
                if (GeneralHelper.HasInternetConnection())
                {
                    GetContext().TenancyObj = new TenancyModel();
                    GetContext().TenancyObj.TenancyId = 0;
                    GetContext().TenancyObj.TenancyStartDate = StartTenancyCtrl.GetDataContext().TenancyStartDate;
                    GetContext().TenancyObj.CustomerId = GetContext().SelectedCustomer.CustomerId;
                    GetContext().TenancyObj.PropertyId = GetContext().SelectedProperty.propertyId;

                    GetContext().TenancyObj.EmploymentInfo = TenancyInfoCtrl.GetContext().GetSafeEmploymentInfo();

                    GetContext().TenancyObj.EmergencyContact = TenancyInfoCtrl.GetContext().GetSafeEmergencyContact();

                    GetContext().TenancyObj.CustomerGeneralInfo = TenancyInfoCtrl.GetContext().GetSafeGeneralInfo();

                    GetContext().TenancyObj.JointTenant = JointTenantCtrl.GetDataContext().GetSafeJointTenant();

                    GetContext().TenancyObj.Occupants = AddOccuptantsCtrl.GetDataContext().GetSafeOccupants();

                    GetContext().TenancyObj.Referral = TenancyReferralCtrl.GetContext().GetSafeRefferalModel();
                    if (GetContext().TenancyObj.Referral != null)
                    {
                        GetContext().TenancyObj.Referral.CustomerId = GetContext().TenancyObj.CustomerId;
                    }


                    GetContext().TenancyObj.PropertyRentInfo = StartTenancyCtrl.GetDataContext().GetSafeRentModel();

                    GetContext().TenancyObj.TenancyDisclosure = StartTenancyCtrl.GetDataContext().GetSafeDisclosure();

                    GetContext().TenancyObj.TenantOnlineInfo = StartTenancyCtrl.GetDataContext().GetSafeOnlineSetUp();
                    GetContext().WillAddPaymentDetails = StartTenancyCtrl.GetDataContext().WillAddPaymentDetails;

                    CreateTenancy();
                }
                else
                {
                    GetContext().showMessage("Error", MessageConstants.InternetNotAvailableError);
                }


            }
            else
            {
                GetContext().showMessage("Alert", result.Message);
            }

        }
        public async void CreateTenancy()
        {
            ResultModel<TenancySO> result = await GetContext().CreateTenancy();
            if (result.Success)
            {
                if (GetContext().WillAddPaymentDetails)
                {
                    ShowPayThruPaymentModule(result.Value);
                }
                else
                {
                    ExitView();
                }
            }
        }

        

        private void ShowPayThruPaymentModule(TenancySO tenancyInfo)
        {
            if (PayThruPayMentCtrl == null)
            {
                PayThruPayMentCtrl = new PaythruPaymentControl();

                PayThruPayMentCtrl.RemoveFromGridAction += PayThruPayMentCtrl_RemoveFromGridAction;
            }

            MainGrid.Children.Add(PayThruPayMentCtrl);
            PayThruPayMentCtrl.ShowUrlWith(tenancyInfo.tenancyId, tenancyInfo.customerId, AppSession.LoggedinUserId);
        }

        private void PayThruPayMentCtrl_RemoveFromGridAction()
        {
            MainGrid.Children.Remove(PayThruPayMentCtrl);
            ExitView();
        }
        private void ExitView()
        {
            if (GetContext().TenancySource == TenancySource.Property)
            {
                RefreshPropertyListing();
            }
            else
            {
                RefreshcustomerListing();
            }
        }
        private void RefreshPropertyListing()
        {
            GetContext().SendTenancyMessages();
            Frame.Navigate(typeof(NoPropertySelectedPage));
            Frame.BackStack.Clear();
        }
        private void RefreshcustomerListing()
        {
            Frame.Navigate(typeof(NoCustomerSelectedPage));
            Frame.BackStack.Clear();
            AppShell shell = Window.Current.Content as AppShell;
            shell.AppFrame.Navigate(typeof(CustomersMainPage), new SuppressNavigationTransitionInfo());
        }
        #endregion
        #region "Pivot Methods"
        private void rootPivot_PivotItemLoading(Pivot sender, PivotItemEventArgs args)
        {
            ConfigureButtonsForTab(sender.SelectedIndex);
            if (args.Item.Content != null)
            {
                return;
            }
            var pivotItemContentControl = CreateUserControlForPivotItem(sender.SelectedIndex);
            args.Item.Content = pivotItemContentControl;
            
        }
        

        private  UserControl CreateUserControlForPivotItem(int selectedIndex)
        {
            
            switch (selectedIndex)
            {
                case 0:
                    return CreateUserControlForFirstIndex();
                case 1:
                    if (TenancyInfoCtrl.GetContext().CustomerGeneralInfo==null)
                    {
                        TenancyInfoCtrl.GetContext().CustomerGeneralInfo = GetContext().SelectedCustomer.CustomerGeneralInfo;
                    }
                    if (TenancyInfoCtrl.GetContext().EmploymentInfo==null)
                    {
                        TenancyInfoCtrl.GetContext().EmploymentInfo = GetContext().SelectedCustomer.EmploymentInfo;
                    }
                    if(GetContext().IsEditingExistingRecord == false)
                    {
                        TenancyInfoCtrl.GetContext().SetSelectedItems();
                    }
                    TenancyInfoCtrl.GetContext().PopulateLookups();
                    return TenancyInfoCtrl;
                case 2:
                    return JointTenantCtrl;
                case 3:
                    return AddOccuptantsCtrl;
                case 4:
                    return TenancyReferralCtrl;
                case 5:
                    StartTenancyCtrl.GetDataContext().SelectedProperty = GetContext().SelectedProperty;
                    StartTenancyCtrl.GetDataContext().TenancyRent = new PropertyRentModel();
                    if (GetContext().SelectedCustomer != null)
                    {
                        if (GetContext().SelectedCustomer.CustomerAddress != null)
                        {
                            if (!string.IsNullOrWhiteSpace(GetContext().SelectedCustomer.CustomerAddress.Email))
                            {
                                StartTenancyCtrl.GetDataContext().TenantOnlineSetup.UserName = GetContext().SelectedCustomer.CustomerAddress.Email;
                            }
                            else
                            {
                                if (GetContext().SelectedCustomer.CustomerAddresses != null)
                                {
                                    var address = GetContext().SelectedCustomer.CustomerAddresses.Where(x => x.Email != null).ToList().FirstOrDefault();
                                    if (address != null)
                                    {
                                        StartTenancyCtrl.GetDataContext().TenantOnlineSetup.UserName = address.Email;
                                    }
                                }

                            }
                        }
                    }
                    
                   
                    if (GeneralHelper.HasInternetConnection())
                    {
                        StartTenancyCtrl.GetDataContext().GetRentInfo();
                    }
                    else
                    {
                        StartTenancyCtrl.GetDataContext().showMessage("Error", MessageConstants.InternetNotAvailableError);
                    }
                    return StartTenancyCtrl;
                default:
                    return null;

            }
        }

        public UserControl CreateUserControlForFirstIndex()
        {
            FetchExistingTenancyData();
            UserControl ctrl;
            if (GetContext().TenancySource == TenancySource.Customer)
            {
                TenancyPropertyCtrl.GetDataContext().SelectedProperty = GetContext().SelectedProperty;
                ctrl = TenancyPropertyCtrl;
            }
            else
            {
                TenancyCustomerCtrl.GetDataContext().SelectedCustomerObj = GetContext().SelectedCustomer;
                ctrl = TenancyCustomerCtrl;
            }

            return ctrl;
        }


        public void ConfigureButtonsForTab(int selectedIndex)
        {
            if (selectedIndex == 5)
            {
                BtnDone.Visibility = Visibility.Visible;
                NavigateForwardButton.Visibility = Visibility.Collapsed;
            }
            else
            {
                NavigateForwardButton.Visibility = Visibility.Visible;
                BtnDone.Visibility = Visibility.Collapsed;
            }
        }
        

        private void NavigateForwardButton_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            int index = rootPivot.SelectedIndex+1;
            if (index <= 5)
            {
                UserControl ctrl = CreateUserControlForPivotItem(index-1);
                bool isValid = CheckDataStatus(ctrl);
                if (isValid)
                {
                    rootPivot.SelectedIndex++;
                    SliderPivot.SelectedIndex = rootPivot.SelectedIndex;
                    GetContext().ResetImagePath(rootPivot.SelectedIndex);
                }
                

            }
        }

        private void NavigateBackButton_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            
            if (rootPivot.SelectedIndex > 0)
            {
                rootPivot.SelectedIndex--;
                SliderPivot.SelectedIndex = rootPivot.SelectedIndex;
                GetContext().ResetImagePath(rootPivot.SelectedIndex);

            }
        }
        #endregion
        
        
        #region "Control events"
        private void JointTenantCtrl_NavigateToSelectCustomer()
        {
            /*Dictionary<string, object> parameterDict = new Dictionary<string, object>
                    {
                        { "master", this},
                        {"source","TenancyJointTenant" }
                    };
            Frame.Navigate(typeof(SelectTenancyCustomer), parameterDict);*/
            Control_NavigateToSelectCustomer();
        }
        private void Control_NavigateToSelectCustomer()
        {
            /*Dictionary<string, object> parameterDict = new Dictionary<string, object>
                    {
                        { "master", this},
                        {"source","TenancyCustomer" }
                    };
            Frame.Navigate(typeof(SelectTenancyCustomer), parameterDict);*/
            SelectCustomerCtrl = new SelectTenancyCustomerControl();
            SelectCustomerCtrl.GetDataContext().SearchText = string.Empty;
            SelectCustomerCtrl.GoBack += SelectCustomerCtrl_GoBack;
            if (rootPivot.SelectedIndex == 0)
            {
                SelectCustomerCtrl.CustSelDel = CustomerSelected;
            }
            else
            {
                SelectCustomerCtrl.CustSelDel = JointTenantSelected ;
            }
            if (MainGrid.Children.Contains(SelectCustomerCtrl))
            {
                MainGrid.Children.Remove(SelectCustomerCtrl);
            }
            MainGrid.Children.Add(SelectCustomerCtrl);
        }
        private void Control_NavigateToSelectProperty()
        {
            /*Dictionary<string, object> parameterDict = new Dictionary<string, object>
                    {
                        { "master", this},
                        {"source","Tenancy" }
                    };
            Frame.Navigate(typeof(SelectPropertyPage),parameterDict);*/
            SelectPropertyCtrl = new SelectTenancyPropertyControl();
            SelectPropertyCtrl.GetDataContext().SearchText = string.Empty;
            SelectPropertyCtrl.GoBack += SelectPropertyCtrl_GoBack;
            SelectPropertyCtrl.PropertySelectionDel = PropertySelected;
            if (MainGrid.Children.Contains(SelectPropertyCtrl))
            {
                MainGrid.Children.Remove(SelectPropertyCtrl);
            }
            MainGrid.Children.Add(SelectPropertyCtrl);
        }

        private void SelectPropertyCtrl_GoBack()
        {
            MainGrid.Children.Remove(SelectPropertyCtrl);
        }

        private void SelectCustomerCtrl_GoBack()
        {
            MainGrid.Children.Remove(SelectCustomerCtrl);
        }
        #endregion
        #region "Delegates"
        public void PropertySelected (PropertyModel selProp)
        {
            GetContext().SelectedProperty = selProp;
            TenancyPropertyCtrl.GetDataContext().SelectedProperty = selProp;
        }
        public async void CustomerSelected(CustomerModel selCust)
        {
            if (GeneralHelper.HasInternetConnection())
            {
                ResultModel<CustomerModel> result = await GetContext().GetCustomerDetailsFor(selCust);
                if (result.Success)
                {
                    result.Value.Telephone = selCust.Telephone;
                    result.Value.Gender = selCust.Gender;
                    result.Value.CustomerType = selCust.CustomerType;
                    result.Value.CustomerTypeId = selCust.CustomerTypeId;
                    GetContext().SelectedCustomer = result.Value;
                    TenancyCustomerCtrl.GetDataContext().SelectedCustomerObj = result.Value;
                }
                else
                {
                    GetContext().showMessage("Error", result.Message);
                    TenancyCustomerCtrl.GetDataContext().SelectedCustomerObj = selCust;
                    GetContext().SelectedCustomer = selCust;
                }
            }
            else
            {
                GetContext().showMessage("Error", MessageConstants.InternetNotAvailableError);
            }
            

        }

        public async void JointTenantSelected(CustomerModel selCust)
        {
            if (GeneralHelper.HasInternetConnection())
            {
                ResultModel<CustomerModel> result = await GetContext().GetCustomerDetailsFor(selCust);
                if (result.Success)
                {
                    JointTenantCtrl.GetDataContext().JointTenantGeneralInfo = result.Value;

                }
                else
                {
                    GetContext().showMessage("Error", result.Message);
                    JointTenantCtrl.GetDataContext().JointTenantGeneralInfo = selCust;
                }
                JointTenantCtrl.GetDataContext().JointTenant.CustomerGeneralInfo = JointTenantCtrl.GetDataContext().JointTenantGeneralInfo.CustomerGeneralInfo;
                JointTenantCtrl.GetDataContext().JointTenant.CustomerId = selCust.CustomerId;
            }
            else
            {
                GetContext().showMessage("Error", MessageConstants.InternetNotAvailableError);
            }
            
        }




        #endregion
       
        public void ConfigureTenancyObject()
        {
            GetContext().TenancyObj = new TenancyModel();
            GetContext().TenancyObj.TenancyId = 0;
            if (StartTenancyCtrl.GetDataContext().TenancyStartDateOffset != null)
            {
                GetContext().TenancyObj.TenancyStartDate = StartTenancyCtrl.GetDataContext().TenancyStartDateOffset.Value.DateTime;
            }
            GetContext().TenancyObj.CustomerId = GetContext().SelectedCustomer.CustomerId;
            GetContext().TenancyObj.PropertyId = GetContext().SelectedProperty.propertyId;

            GetContext().TenancyObj.EmploymentInfo = TenancyInfoCtrl.GetContext().EmploymentInfo;

            GetContext().TenancyObj.EmergencyContact = TenancyInfoCtrl.GetContext().EmergencyContact;

            TenancyInfoCtrl.GetContext().SetSelectedItems();
            GetContext().TenancyObj.CustomerGeneralInfo = TenancyInfoCtrl.GetContext().CustomerGeneralInfo;

            JointTenantCtrl.GetDataContext().SetSelectedItems();
            GetContext().TenancyObj.JointTenant = JointTenantCtrl.GetDataContext().JointTenant;
            if (GetContext().TenancyObj.JointTenant != null)
            {
                GetContext().TenancyObj.JointTenant.JTFirstName = JointTenantCtrl.GetDataContext().JTFirstName;
                GetContext().TenancyObj.JointTenant.JTLastName = JointTenantCtrl.GetDataContext().JTLastName;
                GetContext().TenancyObj.JointTenant.JTDob = JointTenantCtrl.GetDataContext().JTDob;
                GetContext().TenancyObj.JointTenant.JTTelephone = JointTenantCtrl.GetDataContext().JTTelephone;
            }

            if (AddOccuptantsCtrl.GetDataContext().OccupantList != null)
            {
                GetContext().TenancyObj.Occupants = AddOccuptantsCtrl.GetDataContext().OccupantList.ToList();
                foreach(OccupantModel model in GetContext().TenancyObj.Occupants)
                {
                    model.Disabilities = AddOccuptantsCtrl.GetDataContext().SetSelectedItems(model);
                }
            }
            

            GetContext().TenancyObj.Referral = TenancyReferralCtrl.GetContext().Referral;
            if (GetContext().TenancyObj.Referral != null)
            {
                GetContext().TenancyObj.Referral.CustomerId = GetContext().TenancyObj.CustomerId;
                GetContext().TenancyObj.Referral.CustomerHelpSubCategories = TenancyReferralCtrl.GetContext().GetSelectedHelpCategories();
            }


            GetContext().TenancyObj.PropertyRentInfo = StartTenancyCtrl.GetDataContext().TenancyRent;

            GetContext().TenancyObj.TenancyDisclosure = StartTenancyCtrl.GetDataContext().TenancyDisclosure;

            GetContext().TenancyObj.TenantOnlineInfo = StartTenancyCtrl.GetDataContext().TenantOnlineSetup;
            GetContext().WillAddPaymentDetails = StartTenancyCtrl.GetDataContext().WillAddPaymentDetails;
        }
        #region "Pausing and Fetching Saved Data"
        private void PauseTenancyButton_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(GetContext().SelectedProperty.propertyId))
            {
                GetContext().showMessage("Alert", "Select Property" + "\r\r" + "Please select a property first");
            }
            else if (GetContext().SelectedCustomer.CustomerId == null)
            {
                GetContext().showMessage("Alert", "Select Customer" + "\r\r" + "Please select customer first");
            }
            else
            {
                ConfigureTenancyObject();
                GetContext().SaveTenancyDataForStep(rootPivot.SelectedIndex);
            }
            
        }

        public async void FetchExistingTenancyData()
        {
            BusyIndicator serverIndicator = BusyIndicator.Start(MessageConstants.FetchingOfflineTenancyData);
            ResultModel<OfflineTenancyModel> result = GetContext().TenancyOfflineMgr.FetchExistingTenancyFor(GetContext().SelectedCustomer.CustomerId, GetContext().SelectedProperty.propertyId);
            GetContext().OfflineModelObj = result.Value;
            serverIndicator.Close();
            if (result.Success == true)
            {
                var messageDialog = new MessageDialog("A tenancy record for this Customer/Property exists on the system. Do you wish to continue with it? Selecting No will delete the Record.");
                messageDialog.Title = "Confirm Action";
                // Add commands and set their callbacks; both buttons use the same callback function instead of inline event handlers
                messageDialog.Commands.Add(new UICommand(
                    "Yes", new UICommandInvokedHandler(YesCommandInvokedHandler)));
                messageDialog.Commands.Add(new UICommand(
                    "No", new UICommandInvokedHandler(NoCommandInvokedHandler)));

                // Set the command that will be invoked by default
                messageDialog.DefaultCommandIndex = 0;

                // Set the command to be invoked when escape is pressed
                messageDialog.CancelCommandIndex = 1;

                // Show the message dialog
                await messageDialog.ShowAsync();
            }

        }
        private void YesCommandInvokedHandler(IUICommand command)
        {
            GetContext().TenancyObj = GetContext().OfflineModelObj.TenancyModel;
            GetContext().IsEditingExistingRecord = true;
            if (GetContext().OfflineModelObj.Property != null)
            {
                GetContext().SelectedProperty = GetContext().OfflineModelObj.Property;
            }
            if (GetContext().OfflineModelObj.Customer != null)
            {
                GetContext().SelectedCustomer = GetContext().OfflineModelObj.Customer;
            }
            SetUpPreSavedData();
            GetContext().OfflineModelObj = new OfflineTenancyModel();
        }
        private void NoCommandInvokedHandler(IUICommand command)
        {
            GetContext().DeleteTenancyRecord();
        }
        private void SetUpPreSavedData()
        {
            if (GetContext().SelectedProperty != null)
            {
                TenancyPropertyCtrl.GetDataContext().SelectedProperty = GetContext().SelectedProperty;
            }
            
            if (GetContext().OfflineModelObj.Customer != null)
            {
                TenancyCustomerCtrl.GetDataContext().SelectedCustomerObj = GetContext().OfflineModelObj.Customer;
            }
            
            if (GetContext().TenancyObj.TenancyStartDate != null)
            {
                StartTenancyCtrl.GetDataContext().TenancyStartDate = GetContext().TenancyObj.TenancyStartDate;
            }
            
            if (GetContext().TenancyObj.EmploymentInfo != null)
            {
                TenancyInfoCtrl.GetContext().EmploymentInfo = GetContext().TenancyObj.EmploymentInfo;
            }

            

            if (GetContext().TenancyObj.EmergencyContact != null)
            {
                TenancyInfoCtrl.GetContext().EmergencyContact = GetContext().TenancyObj.EmergencyContact;
            }
            

            if (GetContext().TenancyObj.CustomerGeneralInfo != null)
            {
                TenancyInfoCtrl.GetContext().CustomerGeneralInfo = GetContext().TenancyObj.CustomerGeneralInfo;
            }
            

            if (GetContext().TenancyObj.JointTenant != null)
            {
                JointTenantCtrl.GetDataContext().JointTenant = GetContext().TenancyObj.JointTenant;

                CustomerModel model = new CustomerModel();
                model.CustomerId = GetContext().TenancyObj.JointTenant.CustomerId;
                model.CustomerGeneralInfo = GetContext().TenancyObj.JointTenant.CustomerGeneralInfo;
                model.EmploymentInfo = GetContext().TenancyObj.JointTenant.EmploymentInfo;
                model.FirstName = GetContext().TenancyObj.JointTenant.JTFirstName;
                model.LastName = GetContext().TenancyObj.JointTenant.JTLastName;
                model.Dob = GetContext().TenancyObj.JointTenant.JTDob;
                model.Telephone = GetContext().TenancyObj.JointTenant.JTTelephone;

                JointTenantCtrl.GetDataContext().JointTenantGeneralInfo = model;
                JointTenantCtrl.GetDataContext().PopulateLookups();
            }
            

            if (GetContext().TenancyObj.Occupants != null)
            {
                AddOccuptantsCtrl.GetDataContext().InitializeData();
                AddOccuptantsCtrl.GetDataContext().OccupantList = new ObservableCollection<OccupantModel>(GetContext().TenancyObj.Occupants);
                AddOccuptantsCtrl.GetDataContext().ConfigureDisabilityList();
            }
            

            if (GetContext().TenancyObj.Referral != null)
            {
                TenancyReferralCtrl.GetContext().Referral = GetContext().TenancyObj.Referral;
                TenancyReferralCtrl.GetContext().PopulateLookups();
            }
            

            if (GetContext().TenancyObj.PropertyRentInfo != null)
            {
                StartTenancyCtrl.GetDataContext().TenancyRent = GetContext().TenancyObj.PropertyRentInfo;
            }
            

            if (GetContext().TenancyObj.TenancyDisclosure != null)
            {
                StartTenancyCtrl.GetDataContext().TenancyDisclosure = GetContext().TenancyObj.TenancyDisclosure;
            }
            

            if (GetContext().TenancyObj.TenantOnlineInfo != null)
            {
                StartTenancyCtrl.GetDataContext().TenantOnlineSetup = GetContext().TenancyObj.TenantOnlineInfo;
                
            }
            StartTenancyCtrl.GetDataContext().TenantOnlineSetup = GetContext().TenancyObj.TenantOnlineInfo;
            if (GetContext().TenancyObj.TenancyStartDate != null)
            {
                StartTenancyCtrl.GetDataContext().TenancyStartDate = GetContext().TenancyObj.TenancyStartDate;
                StartTenancyCtrl.GetDataContext().TenancyStartDateOffset = DateTimeOffset.Parse(GetContext().TenancyObj.TenancyStartDate.ToString());
            }
            StartTenancyCtrl.GetDataContext().WillAddPaymentDetails = GetContext().WillAddPaymentDetails;

            GetContext().DeleteTenancyRecord();

        }
        #endregion

    }
}
