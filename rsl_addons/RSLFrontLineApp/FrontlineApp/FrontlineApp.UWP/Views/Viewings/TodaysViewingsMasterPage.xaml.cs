﻿using System.Collections.Generic;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.UWP.UserControls.Viewings;
using Windows.UI.Xaml.Navigation;
using FrontlineApp.UWP.ViewModels;
using FrontlineApp.UWP.Views.Viewings;
using Microsoft.Practices.ServiceLocation;
using FrontlineApp.UWP.Views.Customers;
// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace FrontlineApp.UWP.Views.Dashboard
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class TodaysViewingsMasterPage : Page
    {
        private TodaysViewingsDetailControl CtrlTodaysViewingsDetail;
        private NoViewingSelectedControl CtrlNoViewingSelected;
        private TodaysViewingsControl CtrlTodaysViewings;
        public TodaysViewingsMasterPage()
        {
            this.InitializeComponent();
            ShowTodaysViewings();
            DisplayNoSelectedPropertyControl();

        }

        public void DisplayNoSelectedPropertyControl()
        {
            NavGrid.Children.Clear();
            CtrlNoViewingSelected = new NoViewingSelectedControl();
            NavGrid.Children.Add(CtrlNoViewingSelected);
        }

        public void ShowTodaysViewings()
        {
            ListGrid.Children.Clear();
            CtrlTodaysViewings = new TodaysViewingsControl();
            CtrlTodaysViewings.ItemSelectionAction += CtrlTodaysViewings_ItemSelectionAction;
            ListGrid.Children.Add(CtrlTodaysViewings);
        }
        private TodaysViewingsListViewModel GetDataContext()
        {
            return this.DataContext as TodaysViewingsListViewModel;
        }
        public void ShowViewingsDetail(ViewingSummaryModel model)
        {
            NavFrame.Visibility = Visibility.Collapsed;
            NavGrid.Visibility = Visibility.Visible;
            NavGrid.Children.Clear();
            CtrlTodaysViewingsDetail = new TodaysViewingsDetailControl();
            CtrlTodaysViewingsDetail.CreateTenancyAction += CtrlTodaysViewingsDetail_CreateTenancyAction;
            NavGrid.Children.Add(CtrlTodaysViewingsDetail);
            CtrlTodaysViewingsDetail.ConfigureDataForViewing(model);
        }
        private void CtrlTodaysViewingsDetail_CreateTenancyAction(PropertyDetailsModel model)
        {
            NavFrame.Visibility = Visibility.Visible;
            NavGrid.Visibility = Visibility.Collapsed;
            SetupAndAddTenancyViewModel aoccup = ServiceLocator.Current.GetInstance<SetupAndAddTenancyViewModel>();
            aoccup.ClearAllData();
            NavFrame.Navigate(typeof(SetupAndStartTenancyPage));
            GetDataContext().SendTenancyMessages(model);

        }
        private void CtrlTodaysViewings_ItemSelectionAction(ViewingSummaryModel selectedItem)
        {
            if (selectedItem != null)
            {
                ShowViewingsDetail(selectedItem);
            }
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            
        }
        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.Frame.CanGoBack)
            {
                this.Frame.GoBack();
            }
        }
    }
}
