﻿using System.Collections.Generic;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using FrontlineApp.UWP.Views.Viewings;
using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.UWP.UserControls.Viewings;
using FrontlineApp.UWP.ViewModels;
using Windows.UI.Xaml.Navigation;
using FrontlineApp.Utility;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace FrontlineApp.UWP.Views.Viewings
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MyViewingsMasterPage : Page
    {
        private MyViewingsControl CtrlMyViewings;
        private MyViewingsDetailControl CtrlMyViewingsDetail;
        private NoViewingSelectedControl CtrlNoViewingSelected;
        public MyViewingsMasterPage()
        {
            this.InitializeComponent();
            RefreshData();
        }
        private MyViewingsMasterViewModel GetDataContext()
        {
            return this.DataContext as MyViewingsMasterViewModel;
        }
        public void DisplayNoSelectedViewing()
        {
            NavFrame.Visibility = Visibility.Collapsed;
            NavGrid.Visibility = Visibility.Visible;
            NavGrid.Children.Clear();
            CtrlNoViewingSelected = new NoViewingSelectedControl();
            NavGrid.Children.Add(CtrlNoViewingSelected);
        }

        public void DisplayViewingsList()
        {
            CtrlMyViewings = new MyViewingsControl();
            CtrlMyViewings.ItemSelectionAction += CtrlMyViewings_ItemSelectionAction;
            ListGrid.Children.Add(CtrlMyViewings);
        }
        public void CreateNewViewing()
        {
            NavFrame.Visibility = Visibility.Visible;
            NavGrid.Visibility = Visibility.Collapsed;
            GetDataContext().SelectedProperty = null;
            NavFrame.Navigate(typeof(ArrangeNewViewingPage));
            GetDataContext().SendViewingMessage(RefreshData);
        }

        public void RefreshData()
        {
            DisplayNoSelectedViewing();
            DisplayViewingsList();
        }
        

        public void EditExistingViewing(ViewingSummaryModel model)
        {
            if (GeneralHelper.HasInternetConnection())
            {
                NavFrame.Visibility = Visibility.Visible;
                NavGrid.Visibility = Visibility.Collapsed;
                GetDataContext().SelectedProperty = model;
                NavFrame.Navigate(typeof(ArrangeNewViewingPage));
                GetDataContext().SendViewingMessage(RefreshData);
            }
        }

        private void ButtonAddNew_Click(object sender, RoutedEventArgs e)
        {
            CreateNewViewing();
        }

        private void CtrlMyViewingsDetail_RearrangeViewingAction(ViewingSummaryModel model)
        {
            EditExistingViewing(model);
        }
        private void CtrlMyViewingsDetail_RefreshDataAction()
        {
            RefreshData();
        }

        private void CtrlMyViewings_ItemSelectionAction(ViewingSummaryModel selectedItem)
        {
            NavFrame.Visibility = Visibility.Collapsed;
            NavGrid.Visibility = Visibility.Visible;
            NavFrame.BackStack.Clear();
            GetDataContext().SelectedProperty = selectedItem;
            CtrlMyViewingsDetail = new MyViewingsDetailControl();
            CtrlMyViewingsDetail.RefreshDataAction += CtrlMyViewingsDetail_RefreshDataAction;
            CtrlMyViewingsDetail.RearrangeViewingAction += CtrlMyViewingsDetail_RearrangeViewingAction;
            NavGrid.Children.Add(CtrlMyViewingsDetail);
            CtrlMyViewingsDetail.ConfigureDataForViewing(selectedItem);
        }


    }
}
