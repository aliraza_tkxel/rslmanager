﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using FrontlineApp.UWP.ViewModels;
using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.Utility.Helper.AppSession;
using System;
using Windows.UI.Xaml.Navigation;
using System.Collections.Generic;
using Windows.Foundation;
using Windows.UI.Xaml.Media;
using Windows.ApplicationModel.Appointments;
using FrontlineApp.Utility;
using System.Threading.Tasks;
using Windows.UI.Popups;
// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace FrontlineApp.UWP.Views.Viewings
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ArrangeNewViewingPage : Page
    {
        public delegate void RefreshDelegate();
        public RefreshDelegate refreshDel;
        public ArrangeNewViewingPage()
        {
            this.InitializeComponent();
        }

        #region "Methods"
        private ArrangeNewViewingViewModel GetDataContext()
        {
            return this.DataContext as ArrangeNewViewingViewModel;
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
           
        }
        #endregion

        #region "Click Methods"
        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.Frame.CanGoBack)
            {
                this.Frame.GoBack();
            }
            else
            {
                GetDataContext().RefreshMethod?.Invoke();
            }
        }

        private void SelectPropertyButton_Click(object sender, RoutedEventArgs e)
        {
            Dictionary<string, object> parameterDict = new Dictionary<string, object>
                    {
                        { "master", this},
                        {"source","Viewing" }
                    };
            this.Frame.Navigate(typeof(SelectPropertyPage),parameterDict);
        }

        private void SelectCustomerButton_Click(object sender, RoutedEventArgs e)
        {
            Dictionary<string, object> parameterDict = new Dictionary<string, object>
                    {
                        { "master", this},
                        {"source","Viewing" }
                    };
            this.Frame.Navigate(typeof(SelectCustomerPage),parameterDict);
        }
        private void ConfirmButton_Click(object sender, RoutedEventArgs e)
        {
            CreateViewing();
        }
        private void AssignToMeButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                StaffMemberModel model = new StaffMemberModel();
                model.EmployeeId = AppSession.LoggedInUser.userId;
                model.FirstName = AppSession.LoggedInUser.employeeFullName;
                GetDataContext().SelectedStaffMember = model;
                GetDataContext().PresentationModel.AssignedTo = model.FirstName + " " + model.LastName;
            }
            catch(Exception exc)
            {
                GetDataContext().showMessage("Error", exc.Message);
            }
            
        }

        

        private void SelectHousingOfficerButton_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrWhiteSpace(GetDataContext().PresentationModel.ViewingDate) || string.IsNullOrWhiteSpace(GetDataContext().PresentationModel.FormattedTime))
            {
                GetDataContext().showMessage("Alert", "Please select viewing date and time first");
            }
            else
            {
                this.Frame.Navigate(typeof(SelectStaffMemberPage), this);
            }
            
        }

        private void DatePicker_DateChanged(CalendarDatePicker sender, CalendarDatePickerDateChangedEventArgs args)
        {
            if (GetDataContext().PresentationModel != null)
            {
                GetDataContext().PresentationModel.ViewingDate = sender.Date.ToString();
            }
        }

        private void TimePicker_TimeChanged(object sender, TimePickerValueChangedEventArgs e)
        {
            if (GetDataContext().PresentationModel != null)
            {
                GetDataContext().PresentationModel.FormattedTime = (sender as TimePicker).Time.ToString();
            }

        }
        #endregion
        #region "Delegates"
        public void CustomerSelected(CustomerLookUpListModel selectedObj)
        {
            GetDataContext().SelectedCustomer = selectedObj;
            GetDataContext().PresentationModel.CustomerName = selectedObj.FirstName + " " + selectedObj.LastName;
        }
        public void StaffMemberSelected (StaffMemberModel selectedObj)
        {
            GetDataContext().SelectedStaffMember = selectedObj;
            GetDataContext().PresentationModel.HousingOfficerId = selectedObj.EmployeeId;
            GetDataContext().PresentationModel.AssignedTo = selectedObj.FirstName + " " + selectedObj.LastName;
        }
        public void PropertySelected(PropertyModel selectedObj)
        {
            GetDataContext().SelectedProperty = selectedObj;
            GetDataContext().PresentationModel.PropertyAddress = selectedObj.address;
        }
        #endregion
        #region "Create Viewing"
        public async void CreateViewing()
        {
            
            ResultModel<bool> model = await GetDataContext().StartEditingViewing();
            if (model.Success == true)
            {
                bool result = await AddAppointmentToOutlookCalendar(GetDataContext().PresentationModel);
                if (GetDataContext().RefreshMethod != null)
                {
                    GetDataContext().RefreshMethod();
                }
                else
                {
                    GetDataContext().showMessage("Success", "Viewing has been created/edited successfully");
                    this.Frame.GoBack();
                }

            }

        }
        #endregion
        #region "Outlook Calendar Syncing"
        public static Rect GetElementRect(FrameworkElement element)
        {
            GeneralTransform transform = element.TransformToVisual(null);
            Point point = transform.TransformPoint(new Point());
            return new Rect(point, new Size(element.ActualWidth, element.ActualHeight));
        }
        private async Task<bool> AddAppointmentToOutlookCalendar(ViewingSummaryModel model)
        {
            //Add appointment if assigned to the same user
            var appointment = new Appointment();
            //Populate Viewing Data in appointment
            appointment.Subject = string.Format("Viewing at {0}", model.PropertyAddress);
            appointment.Location = (string.IsNullOrWhiteSpace(model.PropertyAddress)) ? "NA" : model.PropertyAddress;
            appointment.BusyStatus = AppointmentBusyStatus.Tentative;
            appointment.Sensitivity = AppointmentSensitivity.Public;
            appointment.AllDay = false;
            //var timeZoneOffset = TimeZoneInfo.Local.GetUtcOffset(DateTime.Now);
            var date = GeneralHelper.GetCombinedDateTimeStringForViewing(model.ViewingDate, model.FormattedTime);
            //var startTime = new DateTimeOffset(date.Year, date.Month, date.Day, date.Hour, date.Minute, 0, TimeZoneInfo.Local.BaseUtcOffset);
            appointment.StartTime = date;
            appointment.Details = string.Format("Customer: {0}", model.CustomerName) + "\r"
                + string.Format("Housing Officer: {0}", (string.IsNullOrWhiteSpace(model.AssignedTo)) ? "NA" : model.AssignedTo) + "\r"
                + string.Format("Address: {0}", model.PropertyAddress) + "\r"
                + string.Format("Created by: {0}", (string.IsNullOrWhiteSpace(model.CreatorName)) ? AppSession.LoggedInUser.employeeFullName : model.CreatorName);




            // Get the selection rect of the button pressed to add this appointment
            var rect = GetElementRect(this.Frame as FrameworkElement);
            string appointmentId = string.Empty;

            
            //Add Invitees



            if (GetDataContext().SelectedStaffMember.EmployeeId != AppSession.LoggedinUserId)
            {
                if (!string.IsNullOrWhiteSpace(GetDataContext().SelectedStaffMember.Email))
                {
                    var concernedOfficer = new AppointmentInvitee();
                    concernedOfficer.DisplayName = GetDataContext().SelectedStaffMember.FirstName + " " + GetDataContext().SelectedStaffMember.LastName;
                    concernedOfficer.Address = GetDataContext().SelectedStaffMember.Email;
                    concernedOfficer.Role = AppointmentParticipantRole.RequiredAttendee;
                    concernedOfficer.Response = AppointmentParticipantResponse.Tentative;
                    appointment.Invitees.Add(concernedOfficer);
                    appointment.Organizer = null;
                }
                else
                {
                    GetDataContext().showMessage("Alert", "Aborted adding appointment to Selected Officer's calendar due to lacking associated email address");
                    return false;
                }
                    
            }
            ResultModel<bool> result = ValidateAppointment(appointment);
            if(result.Success == true)
            {
                appointmentId = await AppointmentManager.ShowAddAppointmentAsync(appointment, rect, Placement.Default);
            }
            else
            {
                GetDataContext().showMessage("Failed to add appointment", result.Message);
            }
            
            return true;
        }

        private ResultModel<bool> ValidateAppointment(Appointment appointment)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            string errorMessage = string.Empty;
            if (appointment.Subject.Length > 255)
            {
                errorMessage = "The subject cannot be greater than 255 characters." + Environment.NewLine;
            }

            if (appointment.Location.Length > 32768)
            {
                errorMessage = "The location cannot be greater than 32,768 characters." + Environment.NewLine;
            }

            if (appointment.Details.Length > 1073741823)
            {
                errorMessage = "The details cannot be greater than 1,073,741,823 characters." + Environment.NewLine;
            }

            if (appointment.Organizer != null)
            {
                if (appointment.Organizer.Address.Length > 321)
                {
                    errorMessage = "The organizer address cannot be greater than 321 characters." + Environment.NewLine;
                }
                if (appointment.Organizer.Address.Length == 0)
                {
                    errorMessage = "The organizer address must be greater than 0 characters." + Environment.NewLine;
                }
            }

            if (appointment.HasInvitees)
            {
                try
                {
                    AppointmentInvitee invitee = appointment.Invitees[0];
                    if (invitee.DisplayName.Length > 256)
                    {
                        errorMessage = "The invitee display name cannot be greater than 256 characters." + Environment.NewLine;
                    }
                    if (invitee.Address.Length > 321)
                    {
                        errorMessage = "The invitee address cannot be greater than 321 characters." + Environment.NewLine;
                    }
                    if (invitee.Address.Length == 0)
                    {
                        errorMessage = "The invitee address must be greater than 0 characters." + Environment.NewLine;
                    }
                }
                catch (Exception e)
                {
                    errorMessage = e.InnerException.Message + Environment.NewLine;
                }
                
            }
            result.Message = errorMessage;
            result.Success = (string.IsNullOrWhiteSpace(errorMessage)) ? true : false;
            return result;
        }


        #endregion

    }
}
