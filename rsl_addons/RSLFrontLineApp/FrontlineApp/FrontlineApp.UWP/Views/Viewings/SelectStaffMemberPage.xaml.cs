﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.ServiceLayer.ServiceObjects;
using FrontlineApp.Utility;
using FrontlineApp.UWP.ViewModels;
using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace FrontlineApp.UWP.Views.Viewings
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SelectStaffMemberPage : Page
    {
        public delegate void StaffMemberSelectionDelegate(StaffMemberModel selectedObj);
        public StaffMemberSelectionDelegate StaffSelDel;
        public SelectStaffMemberPage()
        {
            this.InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.Parameter != null)
            {
                ArrangeNewViewingPage master = e.Parameter as ArrangeNewViewingPage;
                StaffSelDel = master.StaffMemberSelected;
                SelectStaffMemberViewModel ViewModel = this.DataContext as SelectStaffMemberViewModel;
                ViewModel.RequestModel = new StaffOfficerRequestSO();
                ViewModel.RequestModel.searchText = String.Empty;

                ArrangeNewViewingViewModel masterVm = master.DataContext as ArrangeNewViewingViewModel;

                ViewModel.RequestModel.viewingDate = GeneralHelper.GetCombinedDateTimeStringForViewing(masterVm.PresentationModel.ViewingDate, masterVm.PresentationModel.FormattedTime);
                ViewModel.RequestModel.viewingTime = masterVm.PresentationModel.FormattedTime;
                ViewModel.SearchText = String.Empty;
                ViewModel.GetStaffMemberList();
            }
        }
        private void SearchBox_QuerySubmitted(SearchBox sender, SearchBoxQuerySubmittedEventArgs args)
        {
            SelectStaffMemberViewModel ViewModel = this.DataContext as SelectStaffMemberViewModel;
            ViewModel.FilterListBySearchString(args.QueryText);
        }

        private void btnDone_Click(object sender, RoutedEventArgs e)
        {
            SelectStaffMemberViewModel ViewModel = this.DataContext as SelectStaffMemberViewModel;
            if (ViewModel.SelectedProperty != null)
            {
                if (ViewModel.SelectedProperty.EmployeeId == 0)
                {
                    ViewModel.showMessage("Error", "Please select a Staff member first");
                }
                else
                {
                    StaffSelDel(ViewModel.SelectedProperty);
                    this.Frame.GoBack();
                }
            }
            else
            {
                ViewModel.showMessage("Error", "Please select a Staff member first");
            }

        }
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.GoBack();
        }
    }
}
