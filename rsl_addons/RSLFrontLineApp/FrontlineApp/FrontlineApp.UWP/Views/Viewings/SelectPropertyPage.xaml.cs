﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.UWP.ViewModels;
using FrontlineApp.UWP.Views.Customers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace FrontlineApp.UWP.Views.Viewings
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SelectPropertyPage : Page
    {
        public delegate void PropertySelectionDelegate(PropertyModel selectedObj);
        public PropertySelectionDelegate PropertySelectionDel;
        public SelectPropertyPage()
        {
            this.InitializeComponent();
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.Parameter != null)
            {
                Dictionary<string, object> parameterDict = e.Parameter as Dictionary<string, object>;
                string source = parameterDict["source"] as string;
                if (string.Equals(source,"Viewing"))
                {
                    ArrangeNewViewingPage master = parameterDict["master"] as ArrangeNewViewingPage;
                    PropertySelectionDel = master.PropertySelected;
                    
                }
                else
                {
                    SetupAndStartTenancyPage master = parameterDict["master"] as SetupAndStartTenancyPage;
                    PropertySelectionDel = master.PropertySelected;
                }
                SelectPropertyViewModel ViewModel = this.DataContext as SelectPropertyViewModel;
                ViewModel.SearchText = String.Empty;
                ViewModel.GetPropertyList();
            }
        }
        private void SearchBox_QuerySubmitted(SearchBox sender, SearchBoxQuerySubmittedEventArgs args)
        {
            SelectPropertyViewModel ViewModel = this.DataContext as SelectPropertyViewModel;
            ViewModel.FilterListBySearchString(args.QueryText);
        }

        private void btnDone_Click(object sender, RoutedEventArgs e)
        {
            SelectPropertyViewModel ViewModel = this.DataContext as SelectPropertyViewModel;
            if (string.IsNullOrWhiteSpace(ViewModel.SelectedProperty.propertyId))
            {
                ViewModel.showMessage("Error", "Please select a property first");
            }
            else
            {
                PropertySelectionDel(ViewModel.SelectedProperty);
                this.Frame.GoBack();
            }
            
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.GoBack();
        }
    }
}
