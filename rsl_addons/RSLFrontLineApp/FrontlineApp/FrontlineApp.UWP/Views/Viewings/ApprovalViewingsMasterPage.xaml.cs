﻿using System.Collections.Generic;
using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using FrontlineApp.UWP.Views.Viewings;
using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.UWP.ViewModels;
using Windows.UI.Xaml.Navigation;
using FrontlineApp.UWP.UserControls.Viewings;
// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace FrontlineApp.UWP.Views.Dashboard
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ApprovalViewingsMasterPage : Page
    {

        private PendingViewingsControl CtrlPendingViewings;
        private PendingViewingsDetailControl CtrlPendingViewingsDetail;
        private NoViewingSelectedControl CtrlNoViewingSelected;
        public ApprovalViewingsMasterPage()
        {
            this.InitializeComponent();
            RefreshData();
        }
        public void DisplayViewingList()
        {
            ListGrid.Children.Clear();
            CtrlPendingViewings = new PendingViewingsControl();
            CtrlPendingViewings.ItemSelectionAction += CtrlPendingViewings_ItemSelectionAction;
            ListGrid.Children.Add(CtrlPendingViewings);
        }
        public void DisplayNoSelectedPropertyControl()
        {
            NavFrame.Visibility = Visibility.Collapsed;
            NavGrid.Visibility = Visibility.Visible;
            NavGrid.Children.Clear();
            CtrlNoViewingSelected = new NoViewingSelectedControl();
            NavGrid.Children.Add(CtrlNoViewingSelected);
        }

        private void CtrlPendingViewings_ItemSelectionAction(ViewingSummaryModel selectedItem)
        {
            if (selectedItem != null)
            {
                CtrlPendingViewingsDetail = new PendingViewingsDetailControl();
                CtrlPendingViewingsDetail.RefreshDataAction += CtrlPendingViewingsDetail_RefreshDataAction;
                NavGrid.Children.Add(CtrlPendingViewingsDetail);
                CtrlPendingViewingsDetail.ConfigureDataForViewing(selectedItem);
            }
        }
        private void CtrlPendingViewingsDetail_RefreshDataAction()
        {
            RefreshData();
        }
        public void RefreshData()
        {
            DisplayNoSelectedPropertyControl();
            DisplayViewingList();
        }
        private ApprovalViewingsListViewModel GetDataContext()
        {
            return this.DataContext as ApprovalViewingsListViewModel;
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            NavFrame.Visibility = Visibility.Visible;
            NavGrid.Visibility = Visibility.Collapsed;
            NavFrame.Navigate(typeof(ArrangeNewViewingPage));
            GetDataContext().SendViewingMessage(RefreshData);
        }
        
    }
}
