﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.UWP.ViewModels;
using System;
using System.Collections.Generic;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace FrontlineApp.UWP.Views.Viewings
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SelectCustomerPage : Page
    {
        public delegate void CustomerSelectionDelegate(CustomerLookUpListModel selectedObj);
        public CustomerSelectionDelegate CustSelDel;

        public SelectCustomerPage()
        {
            this.InitializeComponent();
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (e.Parameter != null)
            {
                Dictionary<string, object> parameterDict = e.Parameter as Dictionary<string, object>;
                string source = parameterDict["source"] as string;
                if (string.Equals(source, "Viewing"))
                {
                    ArrangeNewViewingPage master = parameterDict["master"] as ArrangeNewViewingPage;
                    if (master != null)
                    {
                        CustSelDel = master.CustomerSelected;
                    }
                    
                    
                }
                
                SelectCustomerViewModel ViewModel = this.DataContext as SelectCustomerViewModel;
                ViewModel.SearchText = String.Empty;
                ViewModel.GetCustomerList();

            }
        }
        private void SearchBox_QuerySubmitted(SearchBox sender, SearchBoxQuerySubmittedEventArgs args)
        {
            SelectCustomerViewModel ViewModel = this.DataContext as SelectCustomerViewModel;
            ViewModel.FilterListBySearchString(args.QueryText);
        }
        private void btnDone_Click(object sender, RoutedEventArgs e)
        {
            SelectCustomerViewModel ViewModel = this.DataContext as SelectCustomerViewModel;
            if (ViewModel.SelectedProperty.CustomerId==0)
            {
                ViewModel.showMessage("Error", "Please select a Customer first");
            }
            else
            {
                CustSelDel(ViewModel.SelectedProperty);
                this.Frame.GoBack();
            }

        }
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.GoBack();
        }
    }
}
