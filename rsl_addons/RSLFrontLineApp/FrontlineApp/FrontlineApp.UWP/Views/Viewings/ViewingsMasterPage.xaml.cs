﻿using System.Collections.Generic;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using FrontlineApp.UWP.Views.Dashboard;
using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.UWP.UserControls.Viewings;
using Windows.UI.Xaml.Navigation;
using FrontlineApp.UWP.ViewModels;
using FrontlineApp.Utility;
using Microsoft.Practices.ServiceLocation;
using FrontlineApp.UWP.Views.Customers;
// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace FrontlineApp.UWP.Views.Viewings
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ViewingsMasterPage : Page
    {
        #region "Properties and Constructor"
        enum SelectedTab
        {
            MyViewings,
            TodaysViewings,
            PendingViewings
        };

        

        private SelectedTab tabInView;
        private MyViewingsControl CtrlMyViewings;
        private TodaysViewingsControl CtrlTodaysViewings;
        private PendingViewingsControl CtrlPendingViewings;

        private MyViewingsDetailControl CtrlMyViewingsDetail;
        private TodaysViewingsDetailControl CtrlTodaysViewingsDetail;
        private PendingViewingsDetailControl CtrlPendingViewingsDetail;

        private NoViewingSelectedControl CtrlNoViewingSelected;
        public ViewingsMasterPage()
        {
            this.InitializeComponent();
            tabInView = SelectedTab.MyViewings;
            DisplayNoSelectedPropertyControl();
        }

        #endregion

        #region "User Control Generation"

        public void AddListControlForCurrentTab()
        {
            
            ListGrid.Children.Clear();
            if(tabInView == SelectedTab.MyViewings)
            {
                CtrlMyViewings = new MyViewingsControl();
                CtrlMyViewings.ItemSelectionAction += CtrlMyViewings_ItemSelectionAction;
                ListGrid.Children.Add(CtrlMyViewings);
            }
            else if (tabInView == SelectedTab.TodaysViewings)
            {
                CtrlTodaysViewings = new TodaysViewingsControl();
                CtrlTodaysViewings.ItemSelectionAction += CtrlTodaysViewings_ItemSelectionAction;
                ListGrid.Children.Add(CtrlTodaysViewings);
            }
            else if (tabInView == SelectedTab.PendingViewings)
            {
                CtrlPendingViewings = new PendingViewingsControl();
                CtrlPendingViewings.ItemSelectionAction += CtrlPendingViewings_ItemSelectionAction;
                ListGrid.Children.Add(CtrlPendingViewings);
            }
        }

        public void AddDetailControlForCurrentTab(ViewingSummaryModel model)
        {
            NavFrame.Visibility = Visibility.Collapsed;
            NavGrid.Visibility = Visibility.Visible;
            NavGrid.Children.Clear();
            switch (tabInView)
            {
                case SelectedTab.MyViewings:
                    CtrlMyViewingsDetail = new MyViewingsDetailControl();
                    CtrlMyViewingsDetail.RefreshDataAction += CtrlMyViewingsDetail_RefreshDataAction;
                    CtrlMyViewingsDetail.RearrangeViewingAction += CtrlMyViewingsDetail_RearrangeViewingAction;
                    NavGrid.Children.Add(CtrlMyViewingsDetail);
                    CtrlMyViewingsDetail.ConfigureDataForViewing(model);
                    break;
                case SelectedTab.TodaysViewings:
                    CtrlTodaysViewingsDetail = new TodaysViewingsDetailControl();
                    CtrlTodaysViewingsDetail.CreateTenancyAction += CtrlTodaysViewingsDetail_CreateTenancyAction;
                    NavGrid.Children.Add(CtrlTodaysViewingsDetail);
                    CtrlTodaysViewingsDetail.ConfigureDataForViewing(model);
                    break;
                case SelectedTab.PendingViewings:
                    CtrlPendingViewingsDetail = new PendingViewingsDetailControl();
                    CtrlPendingViewingsDetail.RefreshDataAction += CtrlPendingViewingsDetail_RefreshDataAction;
                    NavGrid.Children.Add(CtrlPendingViewingsDetail);
                    CtrlPendingViewingsDetail.ConfigureDataForViewing(model);
                    break;
                default:
                    break;
            }
            
        }

        private void CtrlTodaysViewingsDetail_CreateTenancyAction(PropertyDetailsModel model)
        {
            NavFrame.Visibility = Visibility.Visible;
            NavGrid.Visibility = Visibility.Collapsed;
            SetupAndAddTenancyViewModel aoccup = ServiceLocator.Current.GetInstance<SetupAndAddTenancyViewModel>();
            aoccup.ClearAllData();
            NavFrame.Navigate(typeof(SetupAndStartTenancyPage));
            GetDataContext().SendTenancyMessages(model);

        }



        #endregion
        #region "UserControl Call Backs"
        private void CtrlPendingViewingsDetail_RefreshDataAction()
        {
            RefreshData();
        }

        private void CtrlMyViewingsDetail_RearrangeViewingAction(ViewingSummaryModel model)
        {
            EditExistingViewing(model);
        }

        private void CtrlMyViewingsDetail_RefreshDataAction()
        {
            RefreshData();
        }
        private void CtrlPendingViewings_ItemSelectionAction(ViewingSummaryModel selectedItem)
        {
            ListSelectionCallBack(selectedItem);
        }

        private void CtrlTodaysViewings_ItemSelectionAction(ViewingSummaryModel selectedItem)
        {
            ListSelectionCallBack(selectedItem);
        }

        private void CtrlMyViewings_ItemSelectionAction(ViewingSummaryModel selectedItem)
        {
            ListSelectionCallBack(selectedItem);
        }
        #endregion

        #region "Operations"
        public void DisplayNoSelectedPropertyControl()
        {
            NavFrame.Visibility = Visibility.Collapsed;
            NavGrid.Visibility = Visibility.Visible;
            NavGrid.Children.Clear();
            CtrlNoViewingSelected = new NoViewingSelectedControl();
            NavGrid.Children.Add(CtrlNoViewingSelected);
        }
        public void RefreshData()
        {
            AddListControlForCurrentTab();
            DisplayNoSelectedPropertyControl();
        }
        public void ListSelectionCallBack(ViewingSummaryModel selectedObj)
        {
            if (selectedObj != null)
            {
                AddDetailControlForCurrentTab(selectedObj);
            }
            
        }

        private void rootPivot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (rootPivot.SelectedIndex)
            {
                case 0:
                    tabInView = SelectedTab.MyViewings;
                    break;
                case 1:
                    tabInView = SelectedTab.TodaysViewings;
                    break;
                case 2:
                    tabInView = SelectedTab.PendingViewings;
                    break;
                default:
                    break;
            }
            AddListControlForCurrentTab();
            DisplayNoSelectedPropertyControl();
        }

        private void CreateNewViewingButton_Click(object sender, RoutedEventArgs e)
        {
            if (GeneralHelper.HasInternetConnection())
            {
                this.SetUpUIForCreateNewViewing();
            }
            
        }
        public void SetUpUIForCreateNewViewing()
        {
            NavFrame.Visibility = Visibility.Visible;
            NavGrid.Visibility = Visibility.Collapsed;
            GetDataContext().SelectedProperty = null;
            NavFrame.Navigate(typeof(ArrangeNewViewingPage));
            GetDataContext().SendViewingMessage(RefreshData);
        }
        private ViewingsMasterViewModel GetDataContext()
        {
            return this.DataContext as ViewingsMasterViewModel;
        }
        public void DetailDelegateCallBack()
        {
            RefreshData();
            SetUpUIForCreateNewViewing();
        }

        public void EditExistingViewing(ViewingSummaryModel model)
        {
            if (GeneralHelper.HasInternetConnection())
            {
                NavFrame.Visibility = Visibility.Visible;
                NavGrid.Visibility = Visibility.Collapsed;
                GetDataContext().SelectedProperty = model;
                NavFrame.Navigate(typeof(ArrangeNewViewingPage));
                GetDataContext().SendViewingMessage(RefreshData);
            }
            
        }
        #endregion

    }
}
