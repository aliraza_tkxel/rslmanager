﻿using System.Collections.Generic;
using Windows.UI.Xaml.Controls;
using FrontlineApp.UWP.Views.Alerts;
using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.UWP.ViewModels;
using Windows.UI.Xaml.Navigation;
using FrontlineApp.UWP.Views.Viewings;
using FrontlineApp.UWP.Views.Properties;
using FrontlineApp.UWP.Views.Customers;
// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace FrontlineApp.UWP.Views.Dashboard
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class DashboardPage : Page
    {
        private List<DummyDashboardModel> AlertsData;
        private List<DummyDashboardModel> FaultsData;
        private List<DummyDashboardModel> IncomeMgtData;
        public DashboardPage()
        {
            this.InitializeComponent();
            AlertsData = DummyModelManager.GetAlertsData();
            FaultsData = DummyModelManager.GetFaultsData();
            IncomeMgtData = DummyModelManager.GetIncomeManagementData();
        }

        private void ViewingsGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (ViewingsGrid.SelectedIndex)
            {
                case 0:
                    Frame.Navigate(typeof(MyViewingsMasterPage));
                    break;
                case 1:
                    Frame.Navigate(typeof(TodaysViewingsMasterPage));
                    break;
                case 2:
                    Frame.Navigate(typeof(ApprovalViewingsMasterPage));
                    break;
                case 3:
                    Frame.Navigate(typeof(AllViewingsMasterPage));
                    break;
                default:
                    break;
            }

        }
        private DashboardViewModel GetDataContext()
        {
            return this.DataContext as DashboardViewModel;
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            GetDataContext().UpdateViewingsStats() ;
        }
        private void AlertsGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (AlertsGrid.SelectedIndex)
            {
                case 0:
                    Frame.Navigate(typeof(OpenRisksMasterPage));
                    break;
                case 1:
                    Frame.Navigate(typeof(VulnerabilityMasterPage));
                    break;
                default:
                    break;
            }
        }

        private void ViewingSearchBox_QuerySubmitted(SearchBox sender, SearchBoxQuerySubmittedEventArgs args)
        {
            Dictionary<string, string> paramsDic = new Dictionary<string, string>()
            {
                { "searchQuery", ViewingSearchBox.QueryText}
            };
            this.Frame.Navigate(typeof(PropertiesMasterPage), paramsDic);
        }

        private void CustomerSearchBox_QuerySubmitted(SearchBox sender, SearchBoxQuerySubmittedEventArgs args)
        {
            Dictionary<string, string> paramsDic = new Dictionary<string, string>()
            {
                { "searchQuery", CustomerSearchBox.QueryText}

            };
            this.Frame.Navigate(typeof(CustomersMainPage), paramsDic);
        }
    }
}
