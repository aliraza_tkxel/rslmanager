﻿using System.Collections.Generic;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using FrontlineApp.ApplicationLayer.Models;
// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace FrontlineApp.UWP.Views.Alerts
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class OpenRisksMasterPage : Page
    {
        private List<DummyOpenRisksModel> RisksData;
        public OpenRisksMasterPage()
        {
            this.InitializeComponent();
            RisksData = DummyOpenRisksManager.GetRisksData();
            NavFrame.Navigate(typeof(NoAlertSelectedPage));
        }


        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            if (this.Frame.CanGoBack)
            {
                this.Frame.GoBack();
            }
        }

        private void RisksList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DummyOpenRisksModel selectedObject = RisksData[RisksList.SelectedIndex];
            NavFrame.Navigate(typeof(OpenRisksDetailsPage), selectedObject);
            foreach (var item in e.AddedItems)
            {
                ListViewItem lvi = (sender as ListView).ContainerFromItem(item) as ListViewItem;
                lvi.ContentTemplate = (DataTemplate)this.Resources["ListItemSelected"];
            }
            //Remove DataTemplate for unselected items
            foreach (var item in e.RemovedItems)
            {
                ListViewItem lvi = (sender as ListView).ContainerFromItem(item) as ListViewItem;
                lvi.ContentTemplate = (DataTemplate)this.Resources["ListItem"];
            }
        }
    }
}
