﻿using Windows.UI.Xaml.Controls;
using FrontlineApp.ApplicationLayer.Models;
// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace FrontlineApp.UWP.Views.Alerts
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class AlertsMasterPage : Page
    {
        enum SelectedTab
        {
            Risks,
            Vulnerability,
            General
        };
        private SelectedTab tabInView;
        public AlertsMasterPage()
        {
            this.InitializeComponent();
            tabInView = SelectedTab.Risks;
            ListFrame.Navigate(typeof(OpenRisksTabPage), this);
            NavFrame.Navigate(typeof(NoAlertSelectedPage));
        }
        public void ListSelectionCallBack(DummyOpenRisksModel selectedObj)
        {
            switch (tabInView)
            {
                case SelectedTab.Risks:
                    NavFrame.Navigate(typeof(OpenRisksDetailsPage), selectedObj);
                    break;
                case SelectedTab.Vulnerability:
                    NavFrame.Navigate(typeof(VulnerabilityDetailPage), selectedObj);
                    break;
                case SelectedTab.General:
                    break;
                default:
                    break;
            }
        }
        private void rootPivot_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            switch (rootPivot.SelectedIndex)
            {
                case 0:
                    ListFrame.Navigate(typeof(OpenRisksTabPage), this);
                    tabInView = SelectedTab.Risks;
                    break;
                case 1:
                    ListFrame.Navigate(typeof(VulnerabilityTabPage), this);
                    tabInView = SelectedTab.Vulnerability;
                    break;
                case 2:
                    tabInView = SelectedTab.General;
                    break;
                default:
                    break;
            }
            NavFrame.Navigate(typeof(NoAlertSelectedPage));
        }
    }
}
