﻿using System.Collections.Generic;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using FrontlineApp.ApplicationLayer.Models;
// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace FrontlineApp.UWP.Views.Alerts
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class OpenRisksTabPage : Page
    {
        public delegate void AlertsMasterDelegate(DummyOpenRisksModel selectedObj);
        public AlertsMasterDelegate RisksDelegate;
        private List<DummyOpenRisksModel> RisksData;
        public OpenRisksTabPage()
        {
            this.InitializeComponent();
            RisksData = DummyOpenRisksManager.GetRisksData();
        }

        private void RisksList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (RisksDelegate != null)
            {
                DummyOpenRisksModel selectedObj = RisksList.SelectedItem as DummyOpenRisksModel;
                RisksDelegate(selectedObj);
            }
            foreach (var item in e.AddedItems)
            {
                ListViewItem lvi = (sender as ListView).ContainerFromItem(item) as ListViewItem;
                lvi.ContentTemplate = (DataTemplate)this.Resources["ListItemSelected"];
            }
            //Remove DataTemplate for unselected items
            foreach (var item in e.RemovedItems)
            {
                ListViewItem lvi = (sender as ListView).ContainerFromItem(item) as ListViewItem;
                lvi.ContentTemplate = (DataTemplate)this.Resources["ListItem"];
            }
        }
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            AlertsMasterPage masterPage = e.Parameter as AlertsMasterPage;
            this.RisksDelegate = masterPage.ListSelectionCallBack;
        }
    }
}
