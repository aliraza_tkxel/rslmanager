﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.BusinessLayer.Viewings;
using FrontlineApp.Utility;
using FrontlineApp.Utility.Constants;
using FrontlineApp.UWP.Popups;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace FrontlineApp.UWP.ViewModels
{
    public class MyViewingsControlViewModel :BaseViewModel
    {
        #region "Properties"
        private ViewingSummaryModel _selectedProperty;
        public ViewingSummaryModel SelectedProperty
        {
            get { return _selectedProperty; }
            set { Set(() => SelectedProperty, ref _selectedProperty, value); }

        }
        public ViewingsManager ViewingsManager { get; set; }
        private ObservableCollection<GroupInfoListModel> _dataSource;
        public ObservableCollection<GroupInfoListModel> MyQueueViewings
        {
            get { return _dataSource; }
            set { Set(() => MyQueueViewings, ref _dataSource, value); }
        }

        private CollectionViewSource _viewingCVS;
        public CollectionViewSource ViewingsCVS
        {
            get { return _viewingCVS; }
            set { Set(() => ViewingsCVS, ref _viewingCVS, value); }
        }
        private ICollectionView _groupedView;
        public ICollectionView GroupedView
        {
            get { return _groupedView; }
            set { Set(() => GroupedView, ref _groupedView, value); }
        }
        private bool _shouldShowNoPropertiesDisclaimer;
        public bool ShouldShowNoPropertiesDisclaimer
        {
            get { return _shouldShowNoPropertiesDisclaimer; }
            set { Set(() => ShouldShowNoPropertiesDisclaimer, ref _shouldShowNoPropertiesDisclaimer, value); }
        }
        #endregion

        #region "Constructor"
        public MyViewingsControlViewModel()
        {
            ViewingsCVS = new CollectionViewSource();
            ViewingsManager = new ViewingsManager();
            MyQueueViewings = new ObservableCollection<GroupInfoListModel>();
        }
        #endregion
        #region "Methods"
        public async void GetMyQueueViewings()
        {
            BusyIndicator serverIndicator = BusyIndicator.Start(MessageConstants.FetchingViewingsList);
            ViewingsCVS = new CollectionViewSource();
            MyQueueViewings = new ObservableCollection<GroupInfoListModel>();
            MyQueueViewings = await ViewingsManager.GetActiveViewings();
            ShouldShowNoPropertiesDisclaimer = MyQueueViewings.Count == 0 ? true : false;
            ConfigureCollectionView();
            serverIndicator.Close();
            if (GeneralHelper.HasInternetConnection() == false)
            {
                showMessage(MessageConstants.AppOfflineHeader, MessageConstants.AppOfflineFeatureUnavailable);
            }

        }

        private void ConfigureCollectionView()
        {
            ViewingsCVS.IsSourceGrouped = true;
            ViewingsCVS.Source = MyQueueViewings;
            GroupedView = ViewingsCVS.View;
        }
        #endregion
    }
}
