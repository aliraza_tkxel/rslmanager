﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.BusinessLayer.Viewings;
using FrontlineApp.Utility;
using FrontlineApp.Utility.Constants;
using FrontlineApp.UWP.Popups;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.UWP.ViewModels
{
    public class AllViewingsControlDetailViewModel: BaseViewModel
    {
        #region "Properties"
        private ViewingSummaryModel _selectedProperty;
        public ViewingSummaryModel SelectedProperty
        {
            get { return _selectedProperty; }
            set { Set(() => SelectedProperty, ref _selectedProperty, value); }

        }
        public ViewingsManager ViewingsManager { get; set; }

        #endregion

        #region "Constructor"
        public AllViewingsControlDetailViewModel()
        {
            ViewingsManager = new ViewingsManager();
            SelectedProperty = new ViewingSummaryModel();
        }
        #endregion

        #region "Methods"
        public async void GetMyViewingDetails()
        {

            if (GeneralHelper.HasInternetConnection())
            {
                BusyIndicator serverIndicator = BusyIndicator.Start(MessageConstants.FetchingViewingDetails);
                var result = await ViewingsManager.FetchDetailsForViewing(SelectedProperty);
                SelectedProperty = result.Value;
                serverIndicator.Close();
            }
            else
            {
                showMessage("Error", MessageConstants.InternetNotAvailableError);
            }


        }

        public async Task<ResultModel<bool>> CancelViewing()
        {
            ResultModel<bool> result = new ResultModel<bool>();
            result.Success = false;
            result.Value = false;
            result.Message = "";
            if (GeneralHelper.HasInternetConnection())
            {
                BusyIndicator serverIndicator = BusyIndicator.Start(MessageConstants.UpdatingViewingStatus);
                result = await ViewingsManager.EditViewingStatus(SelectedProperty, ApplicationConstants.ViewingStatusCancelled);
                if (result.Success)
                {
                    showMessage("Success", "Viewing cancelled successfully");
                }
                else
                {
                    showMessage("Error", result.Error);
                }
                serverIndicator.Close();
            }
            else
            {
                showMessage("Error", MessageConstants.InternetNotAvailableError);
            }



            return result;
        }
        public async Task<ResultModel<bool>> RecordFeedBack(string type)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            result.Success = false;
            result.Value = false;
            result.Message = "";
            if (GeneralHelper.HasInternetConnection())
            {
                if (!string.IsNullOrWhiteSpace(SelectedProperty.Notes))
                {
                    BusyIndicator serverIndicator = BusyIndicator.Start(MessageConstants.RecordingFeedBackForViewing);
                    result = await ViewingsManager.AddCommentToViewing(SelectedProperty, SelectedProperty.Notes, type);//await ViewingsManager.RecordFeedBackForViewing(SelectedProperty, SelectedProperty.Notes);
                    if (result.Success)
                    {
                        showMessage("Success", "Feedback recorded successfully");
                    }
                    else
                    {
                        showMessage("Error", result.Error);
                    }
                    serverIndicator.Close();
                }
                else
                {
                    showMessage("Error", "Please provide some Feedback first");
                }

            }
            else
            {
                showMessage("Error", MessageConstants.InternetNotAvailableError);
            }



            return result;
        }
        #endregion
    }
}
