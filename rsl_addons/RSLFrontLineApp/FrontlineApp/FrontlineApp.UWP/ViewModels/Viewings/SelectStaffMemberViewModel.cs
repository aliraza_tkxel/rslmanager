﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.BusinessLayer.Viewings;
using FrontlineApp.ServiceLayer.ServiceObjects;
using FrontlineApp.Utility;
using FrontlineApp.Utility.Constants;
using FrontlineApp.Utility.Helper.Extensions;
using FrontlineApp.UWP.Popups;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace FrontlineApp.UWP.ViewModels
{
    public class SelectStaffMemberViewModel: BaseViewModel
    {
        #region "Properties"
        private StaffMemberModel _selectedProperty;
        public StaffMemberModel SelectedProperty
        {
            get { return _selectedProperty; }
            set { Set(() => SelectedProperty, ref _selectedProperty, value); }
        }
        private ObservableCollection<GroupInfoListModel> _staffList;
        public ObservableCollection<GroupInfoListModel> StaffMemberList
        {
            get { return _staffList; }
            set { Set(() => StaffMemberList, ref _staffList, value); }
        }
        private string _searchText;
        public string SearchText
        {
            get { return _searchText; }
            set
            {
                Set(() => SearchText, ref _searchText, value);
                if (RequestModel != null)
                {
                    RequestModel.searchText = string.IsNullOrWhiteSpace(value) ? string.Empty : value;
                }
            }
        }

        private StaffOfficerRequestSO _requestModel;
        public StaffOfficerRequestSO RequestModel
        {
            get { return _requestModel; }
            set { Set(() => RequestModel, ref _requestModel, value); }
        }

        public ViewingsManager ViewingsManager { get; set; }
        private CollectionViewSource _viewingCVS;
        public CollectionViewSource ViewingsCVS
        {
            get { return _viewingCVS; }
            set { Set(() => ViewingsCVS, ref _viewingCVS, value); }
        }
        private ICollectionView _groupedView;
        public ICollectionView GroupedView
        {
            get { return _groupedView; }
            set { Set(() => GroupedView, ref _groupedView, value); }
        }
        #endregion

        #region "Constructor"
        public SelectStaffMemberViewModel()
        {
            ViewingsManager = new ViewingsManager();
            StaffMemberList = new ObservableCollection<GroupInfoListModel>();
            SearchText = string.Empty;
            ViewingsCVS = new CollectionViewSource();
            SelectedProperty = new StaffMemberModel();
            RequestModel = new StaffOfficerRequestSO();
        }
        #endregion

        #region "Methods"

        #region "Get Staff Member List"
        public async void GetStaffMemberList()
        {

            if (GeneralHelper.HasInternetConnection())
            {
                BusyIndicator serverIndicator = BusyIndicator.Start("Fetching Staff Member Records from Server....");
                try
                {
                    // Set VM property; updates View appropriately.
                    //Busy = true;

                    ViewingsCVS = new CollectionViewSource();
                    SearchText = string.IsNullOrWhiteSpace(SearchText) ? string.Empty : SearchText;
                    SelectedProperty = new StaffMemberModel();
                    ResultModel<List<StaffMemberModel>> result = await ViewingsManager.GetStaffMembersList(RequestModel);
                    if (result.Success)
                    {
                        List<StaffMemberModel> staffmemberList = result.Value;


                        if (staffmemberList == null || staffmemberList.Count() == 0)
                        {
                            showMessage("Alert", "No Staff members found");
                        }
                        StaffMemberList = ViewingsManager.SortStaffOfficersForList(staffmemberList);
                        ConfigureCollectionView();
                    }
                    else
                    {
                        showMessage("Error", result.Message);
                    }
                    
                }
                catch
                {
                    // Error = ex.Message;
                }
                finally
                {
                    // Busy = false;
                    serverIndicator.Close();
                }
            }
            else
            {
                showMessage("Error", MessageConstants.InternetNotAvailableError);
            }

            

        }
        #endregion
        private void ConfigureCollectionView()
        {
            ViewingsCVS.IsSourceGrouped = true;
            ViewingsCVS.Source = StaffMemberList;
            GroupedView = ViewingsCVS.View;
        }
        public void FilterListBySearchString(string query)
        {
            GetStaffMemberList();
        }
        #endregion
    }
}
