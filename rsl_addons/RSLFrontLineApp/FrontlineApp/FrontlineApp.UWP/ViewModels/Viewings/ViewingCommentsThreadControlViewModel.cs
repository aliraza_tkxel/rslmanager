﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.BusinessLayer.Viewings;
using FrontlineApp.Utility;
using FrontlineApp.Utility.Constants;
using FrontlineApp.UWP.Popups;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.UWP.ViewModels
{
    public class ViewingCommentsThreadControlViewModel :BaseViewModel
    {
        #region "Properties"
        public ViewingsManager ViewingsManager { get; set; }
        public ViewingSummaryModel viewingObj { get; set; }
        private List<ViewingCommentModel> _dataSource;
        public List<ViewingCommentModel> CommentsList
        {
            get { return _dataSource; }
            set
            {
                Set(() => CommentsList, ref _dataSource, value);
                if (value.Count > 0)
                {
                    ShouldShowNoCommentsDisclaimer = false;
                }
                else
                {
                    ShouldShowNoCommentsDisclaimer = true;
                }
            }

        }
        private bool _shouldShowNoCommentsDisclaimer;
        public bool ShouldShowNoCommentsDisclaimer
        {
            get { return _shouldShowNoCommentsDisclaimer; }
            set { Set(() => ShouldShowNoCommentsDisclaimer, ref _shouldShowNoCommentsDisclaimer, value); }
        }
        #endregion
        #region "Constructor"
        public ViewingCommentsThreadControlViewModel()
        {
            ViewingsManager = new ViewingsManager();
            CommentsList = new List<ViewingCommentModel>();
        }
        #endregion
    }
}
