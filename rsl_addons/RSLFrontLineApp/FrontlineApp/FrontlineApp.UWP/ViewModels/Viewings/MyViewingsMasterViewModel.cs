﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.BusinessLayer.Viewings;
using FrontlineApp.Utility;
using FrontlineApp.Utility.Constants;
using FrontlineApp.UWP.Popups;
using FrontlineApp.UWP.ViewModels.Messages;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace FrontlineApp.UWP.ViewModels
{
    public class MyViewingsMasterViewModel : BaseViewModel
    {
        #region "Properties"
        private ViewingSummaryModel _selectedProperty;
        public ViewingSummaryModel SelectedProperty
        {
            get { return _selectedProperty; }
            set { Set(() => SelectedProperty, ref _selectedProperty, value); }

        }
        public ViewingsManager ViewingsManager { get; set; }
        private List<ViewingSummaryModel> _dataSource;
        public List<ViewingSummaryModel> LoggedViewingsList
        {
            get { return _dataSource; }
            set { Set(() => LoggedViewingsList, ref _dataSource, value); }

        }
        private bool _shouldShowNoPropertiesDisclaimer;
        public bool ShouldShowNoPropertiesDisclaimer
        {
            get { return _shouldShowNoPropertiesDisclaimer; }
            set { Set(() => ShouldShowNoPropertiesDisclaimer, ref _shouldShowNoPropertiesDisclaimer, value); }
        }
        #endregion
        #region "Constructor"
        public MyViewingsMasterViewModel()
        {
            ViewingsManager = new ViewingsManager();
            LoggedViewingsList = new List<ViewingSummaryModel>();
        }
        #endregion
        public async void GetMyQueueViewings()
        {
            BusyIndicator serverIndicator = BusyIndicator.Start(MessageConstants.FetchingViewingsList);
            LoggedViewingsList = await ViewingsManager.GetMyQueueViewingsList();
            ShouldShowNoPropertiesDisclaimer = LoggedViewingsList.Count == 0 ? true : false;
            serverIndicator.Close();
            if (GeneralHelper.HasInternetConnection() == false)
            {
                showMessage(MessageConstants.AppOfflineHeader, MessageConstants.AppOfflineFeatureUnavailable);
            }

        }
        public void SendViewingMessage(Action method)
        {
            Messenger.Default.Send(new ArrangeViewingMessage()
            {
                Source = ApplicationConstants.ViewingSourceLoggedKey,
                RefreshMethod = method,
                SelectedObject = SelectedProperty
            }
            );
        }
    }
}
