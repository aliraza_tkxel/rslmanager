﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.BusinessLayer.Viewings;
using FrontlineApp.Utility.Constants;
using FrontlineApp.UWP.Popups;
using FrontlineApp.Utility;

namespace FrontlineApp.UWP.ViewModels
{
    public class PendingViewingsControlDetailViewModel: BaseViewModel
    {
        #region "Properties"
        private ViewingSummaryModel _selectedProperty;
        public ViewingSummaryModel SelectedProperty
        {
            get { return _selectedProperty; }
            set { Set(() => SelectedProperty, ref _selectedProperty, value); }

        }
        public ViewingsManager ViewingsManager { get; set; }

        #endregion

        #region "Constructor"
        public PendingViewingsControlDetailViewModel()
        {
            ViewingsManager = new ViewingsManager();
            SelectedProperty = new ViewingSummaryModel();
        }
        #endregion
        #region "Methods"
        public async void GetMyViewingDetails()
        {
            if (GeneralHelper.HasInternetConnection())
            {
                BusyIndicator serverIndicator = BusyIndicator.Start(MessageConstants.FetchingViewingDetails);
                var result = await ViewingsManager.FetchDetailsForViewing(SelectedProperty);
                SelectedProperty = result.Value;
                serverIndicator.Close();
            }
            else
            {
                showMessage("Error", MessageConstants.InternetNotAvailableError);
            }

        }
        public async Task<ResultModel<bool>> AcceptViewing()
        {
            ResultModel<bool> result = new ResultModel<bool>();
            result.Success = false;
            result.Value = false;
            result.Message = "";
            if (GeneralHelper.HasInternetConnection())
            {
                BusyIndicator serverIndicator = BusyIndicator.Start(MessageConstants.UpdatingViewingStatus);
                result = await ViewingsManager.EditViewingStatus(SelectedProperty, ApplicationConstants.ViewingStatusAccepted);
                if (result.Success)
                {
                    showMessage("Success", "Viewing accepted successfully");
                }
                else
                {
                    showMessage("Error", result.Error);
                }
                serverIndicator.Close();
            }
            else
            {
                showMessage("Error", MessageConstants.InternetNotAvailableError);
            }

            return result;
        }

        public async Task<ResultModel<bool>> RejectViewing()
        {
            ResultModel<bool> result = new ResultModel<bool>();
            result.Success = false;
            result.Value = false;
            result.Message = "";
            if (GeneralHelper.HasInternetConnection())
            {
                BusyIndicator serverIndicator = BusyIndicator.Start(MessageConstants.UpdatingViewingStatus);
                result = await ViewingsManager.EditViewingStatus(SelectedProperty, ApplicationConstants.ViewingStatusRejected);
                if (result.Success)
                {
                    showMessage("Success", "Viewing rejected successfully");
                }
                else
                {
                    showMessage("Error", result.Error);
                }
                serverIndicator.Close();

            }
            else
            {
                showMessage("Error", MessageConstants.InternetNotAvailableError);
            }
            return result;

        }


        #endregion
    }
}
