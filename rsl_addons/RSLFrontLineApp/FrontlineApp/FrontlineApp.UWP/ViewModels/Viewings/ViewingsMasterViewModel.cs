﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.BusinessLayer.Viewings;
using FrontlineApp.Utility.Helper.Extensions;
using System.Collections.ObjectModel;
using FrontlineApp.Utility.Constants;
using FrontlineApp.UWP.Popups;
using FrontlineApp.Utility;
using GalaSoft.MvvmLight.Messaging;
using FrontlineApp.UWP.ViewModels.Messages;

namespace FrontlineApp.UWP.ViewModels
{
    public class ViewingsMasterViewModel : BaseViewModel
    {
        #region "Properties"
        private ViewingSummaryModel _selectedProperty;
        public ViewingSummaryModel SelectedProperty
        {
            get { return _selectedProperty; }
            set { Set(() => SelectedProperty, ref _selectedProperty, value); }
        }
        public ViewingsManager ViewingsManager { get; set; }
       
        #endregion
        #region "Constructor"
        public ViewingsMasterViewModel()
        {
            ViewingsManager = new ViewingsManager();
            //GetViewingsList();
        }
        #endregion
        #region "Methods"
        public void SendTenancyMessages(PropertyDetailsModel model)
        {
            Messenger.Default.Send(new ArrangeTenancyMessage()
            {
                source = "PropertyTenancy",
                PropertyModel = model,
                CustomerModel = null
            }
            );
        }
        public async void GetViewingsList()
        {
            BusyIndicator serverIndicator = BusyIndicator.Start(MessageConstants.FetchingViewingsList);
            ResultModel<List<ViewingSummaryModel>> result = await ViewingsManager.GetViewingsList();
            if (result.Success == false)
            {
                showMessage("Error", result.Error);
            }
            serverIndicator.Close();
            if (GeneralHelper.HasInternetConnection()==false)
            {
                showMessage(MessageConstants.AppOfflineHeader, MessageConstants.AppOfflineFeatureUnavailable);
            }
        }
        public void SendViewingMessage(Action method)
        {
            Messenger.Default.Send(new ArrangeViewingMessage()
            {
                Source = ApplicationConstants.ViewingSourceNewOrEditKey,
                RefreshMethod = method,
                SelectedObject = SelectedProperty
            }
            );
        }
        #endregion
    }
}
