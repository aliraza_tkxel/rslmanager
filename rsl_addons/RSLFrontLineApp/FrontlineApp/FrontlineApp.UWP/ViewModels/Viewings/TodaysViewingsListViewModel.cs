﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.BusinessLayer.Viewings;
using FrontlineApp.Utility;
using FrontlineApp.Utility.Constants;
using FrontlineApp.UWP.Popups;
using FrontlineApp.UWP.ViewModels.Messages;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.UWP.ViewModels
{
    public class TodaysViewingsListViewModel: BaseViewModel
    {
        #region "Properties"
        private ViewingSummaryModel _selectedProperty;
        public ViewingSummaryModel SelectedProperty
        {
            get { return _selectedProperty; }
            set { Set(() => SelectedProperty, ref _selectedProperty, value); }

        }
        public ViewingsManager ViewingsManager { get; set; }
        private List<ViewingSummaryModel> _dataSource;
        public List<ViewingSummaryModel> TodaysViewingsList
        {
            get { return _dataSource; }
            set { Set(() => TodaysViewingsList, ref _dataSource, value); }

        }
        private bool _shouldShowNoPropertiesDisclaimer;
        public bool ShouldShowNoPropertiesDisclaimer
        {
            get { return _shouldShowNoPropertiesDisclaimer; }
            set { Set(() => ShouldShowNoPropertiesDisclaimer, ref _shouldShowNoPropertiesDisclaimer, value); }
        }
        #endregion
        #region "Constructor"
        public TodaysViewingsListViewModel()
        {
            ViewingsManager = new ViewingsManager();
            TodaysViewingsList = new List<ViewingSummaryModel>();
        }
        #endregion
        #region "Methods"
        public async void GetTodaysViewings()
        {
            BusyIndicator serverIndicator = BusyIndicator.Start(MessageConstants.FetchingViewingsList);
            TodaysViewingsList = await ViewingsManager.GetTodaysViewings();
            ShouldShowNoPropertiesDisclaimer = TodaysViewingsList.Count == 0 ? true : false;
            serverIndicator.Close();
            if (GeneralHelper.HasInternetConnection() == false)
            {
                showMessage(MessageConstants.AppOfflineHeader, MessageConstants.AppOfflineFeatureUnavailable);
            }

        }
        public void SendTenancyMessages(PropertyDetailsModel model)
        {
            Messenger.Default.Send(new ArrangeTenancyMessage()
            {
                source = "PropertyTenancy",
                PropertyModel = model,
                CustomerModel = null
            }
            );
        }


        #endregion
    }
}
