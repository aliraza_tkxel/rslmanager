﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.BusinessLayer.Viewings;
using FrontlineApp.Utility;
using FrontlineApp.Utility.Constants;
using FrontlineApp.Utility.Helper.Extensions;
using FrontlineApp.UWP.Popups;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Windows.UI.Xaml.Data;

namespace FrontlineApp.UWP.ViewModels
{
    public class SelectPropertyViewModel:BaseViewModel
    {
        #region "Properties"

        private PropertyModel _selectedProperty;
        public PropertyModel SelectedProperty
        {
            get { return _selectedProperty; }
            set { Set(() => SelectedProperty, ref _selectedProperty, value); }
        }
        private ObservableCollection<GroupInfoListModel> _propertyList;
        public ObservableCollection<GroupInfoListModel> PropertyList
        {
            get { return _propertyList; }
            set { Set(() => PropertyList, ref _propertyList, value); }
        }

        private string _searchText;
        public string SearchText
        {
            get { return _searchText; }
            set { Set(() => SearchText, ref _searchText, value); }
        }

        public ViewingsManager ViewingsManager { get; set; }
        private CollectionViewSource _viewingCVS;
        public CollectionViewSource ViewingsCVS
        {
            get { return _viewingCVS; }
            set { Set(() => ViewingsCVS, ref _viewingCVS, value); }
        }
        private ICollectionView _groupedView;
        public ICollectionView GroupedView
        {
            get { return _groupedView; }
            set { Set(() => GroupedView, ref _groupedView, value); }
        }
        #endregion

        #region "Constructor"
        public SelectPropertyViewModel()
        {
            ViewingsManager = new ViewingsManager();
            ViewingsCVS = new CollectionViewSource();
            SelectedProperty = new PropertyModel();
            PropertyList = new ObservableCollection<GroupInfoListModel>();
            SearchText = String.Empty;
        }
        #endregion

        #region "Methods"

        #region "Get Property List"
        public async void GetPropertyList()
        {
            if (GeneralHelper.HasInternetConnection())
            {
                BusyIndicator serverIndicator = BusyIndicator.Start(MessageConstants.FetchingPropertiesList);
                try
                {
                    // Set VM property; updates View appropriately.
                    //Busy = true;
                    ViewingsCVS = new CollectionViewSource();
                    SelectedProperty = new PropertyModel();

                    ResultModel<List<PropertyModel>> result = await ViewingsManager.GetPropertyList(SearchText);
                    List<PropertyModel> propertyList = result.Value;
                    if (!string.IsNullOrWhiteSpace(SearchText))
                    {
                        propertyList = propertyList
                  .Where(c => (c.scheme.IndexOf(SearchText, StringComparison.CurrentCultureIgnoreCase) > -1) || (c.address.IndexOf(SearchText, StringComparison.CurrentCultureIgnoreCase) > -1)).ToList();
                        result.Value = propertyList;
                    }

                    if (propertyList == null || propertyList.Count() == 0)
                    {
                        showMessage("Alert", "No properties found");
                    }
                    else
                    {
                        PropertyList = ViewingsManager.SortPropertyListIntoGroups(result);
                        ConfigureCollectionView();
                    }
                }
                catch
                {
                    // Error = ex.Message;
                }
                finally
                {
                    // Busy = false;
                    serverIndicator.Close();
                }
            }
            else
            {
                showMessage("Error", MessageConstants.InternetNotAvailableError);
            }

            

        }
        #endregion
        private void ConfigureCollectionView()
        {
            ViewingsCVS.IsSourceGrouped = true;
            ViewingsCVS.Source = PropertyList;
            GroupedView = ViewingsCVS.View;
        }
        public void FilterListBySearchString(string query)
        {
            GetPropertyList();
        }
        #endregion
    }
}
