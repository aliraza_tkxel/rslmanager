﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.BusinessLayer.Viewings;
using FrontlineApp.Utility;
using FrontlineApp.Utility.Constants;
using FrontlineApp.Utility.Helper.AppSession;
using FrontlineApp.UWP.Infrastructure;
using FrontlineApp.UWP.Popups;
using FrontlineApp.UWP.ViewModels.Messages;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Globalization;
using System.Threading.Tasks;

namespace FrontlineApp.UWP.ViewModels
{

    public class ArrangeNewViewingViewModel : BaseViewModel
    {
        #region "Properties"
        public Action RefreshMethod { get; set; }
        private string _titleString;
        public string TitleString
        {
            get { return _titleString; }
            set { Set(() => TitleString, ref _titleString, value); }
        }
        private DateTimeOffset? _viewingDateOffset;
        public DateTimeOffset? ViewingOffSet
        {
            set
            {
                Set(() => ViewingOffSet, ref _viewingDateOffset, value);
                ViewingDate = DateTime.Parse(_viewingDateOffset.ToString());
            }
            
            get
            {
                if (!string.IsNullOrEmpty(PresentationModel.ViewingDate))
                {
                    return new DateTime(ViewingDate.Year, ViewingDate.Month, ViewingDate.Day);
                }
                else
                {
                    ViewingDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                }
                return new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day); 
            }
        }
        private DateTime _viewingDate;
        public DateTime ViewingDate
        {
            get {return _viewingDate;}
            set
            {
                Set(() => ViewingDate, ref _viewingDate, value);
                PresentationModel.ViewingDate = _viewingDate.ToString();

                if (SelectedStaffMember != null)
                {
                    SelectedStaffMember = new StaffMemberModel();
                }
                if (PresentationModel != null)
                {
                    PresentationModel.AssignedTo = string.Empty;
                    PresentationModel.HousingOfficerId = 0;
                }
            }
        }
        private TimeSpan _viewingTime;
        public TimeSpan ViewingTime
        {
            get { return _viewingTime; }
            
            set
            {
                Set(() => ViewingTime, ref _viewingTime, value);
                PresentationModel.FormattedTime = _viewingTime.ToString();

                if (SelectedStaffMember != null)
                {
                    SelectedStaffMember = new StaffMemberModel();
                }
                if (PresentationModel != null)
                {
                    PresentationModel.AssignedTo = string.Empty;
                    PresentationModel.HousingOfficerId = 0;
                }
            }
        }

        private PropertyModel _selectedProperty;
        public PropertyModel SelectedProperty
        {
            get { return _selectedProperty; }
            set
            {
                Set(() => SelectedProperty, ref _selectedProperty, value);
                PresentationModel.PropertyId = value.propertyId;
                ShouldShowSelectedPropPanel = (string.IsNullOrEmpty(SelectedProperty.propertyId)) ? false : true;
                
            }
        }
        private StaffMemberModel _selectedStaffMember;
        public StaffMemberModel SelectedStaffMember
        {
            get { return _selectedStaffMember; }
            set
            {
                Set(() => SelectedStaffMember, ref _selectedStaffMember, value);
                PresentationModel.HousingOfficerId = value.EmployeeId;
                PresentationModel.HousingOfficerEmail = value.Email;
                ShouldShowSelectedStaffMemberPanel = (SelectedStaffMember.EmployeeId == 0) ? false : true;
            }
        }
        private CustomerLookUpListModel _selectedCustomer;
        public CustomerLookUpListModel SelectedCustomer
        {
            get { return _selectedCustomer; }
            set
            {
                Set(() => SelectedCustomer, ref _selectedCustomer, value);
                PresentationModel.CustomerId = value.CustomerId;
                ShouldShowSelectedCustomerPanel=(SelectedCustomer.CustomerId == 0) ? false : true;
            }
        }
        private ViewingSummaryModel _presentationEntity;
        public ViewingSummaryModel PresentationModel
        {
            get { return _presentationEntity; }
            set { Set(() => PresentationModel, ref _presentationEntity, value); }
        }
        
        public ViewingsManager ViewingsManager { get; set; }
        #endregion

        #region "Visibility"
        private bool _shouldShowSelectedPropPanel;
        public bool ShouldShowSelectedPropPanel
        {
            get { return _shouldShowSelectedPropPanel; }
            set { Set(() => ShouldShowSelectedPropPanel, ref _shouldShowSelectedPropPanel, value); }
        }
        private bool _shouldShowSelectedStaffMemberPanel;
        public bool ShouldShowSelectedStaffMemberPanel
        {
            get { return _shouldShowSelectedStaffMemberPanel; }
            set { Set(() => ShouldShowSelectedStaffMemberPanel, ref _shouldShowSelectedStaffMemberPanel, value); }
        }
        private bool _shouldShowSelectedCustomerPanel;
        public bool ShouldShowSelectedCustomerPanel
        {
            get { return _shouldShowSelectedCustomerPanel; }
            set { Set(() => ShouldShowSelectedCustomerPanel, ref _shouldShowSelectedCustomerPanel, value); }
        }
        #endregion
        #region "Constructor"
        public ArrangeNewViewingViewModel()
        {
            RegisterMessages(); 
        }
        #endregion
        #region "Methods"
        public async Task<ResultModel<bool>> StartEditingViewing()
        {
            ResultModel<bool> model = new ResultModel<bool>();
            
            if (GeneralHelper.HasInternetConnection())
            {
                BusyIndicator serverIndicator = BusyIndicator.Start(MessageConstants.CreatingNewViewing);
                 model = await ViewingsManager.ArrangeOrUpdateViewing(PresentationModel);
                if (model.Success == false)
                {
                    showMessage("Error", model.Message);
                }
                
                serverIndicator.Close();
            }
            else
            {
                showMessage("Error", MessageConstants.InternetNotAvailableError);
            }
           
            return model;
        }
        public void ResetValues()
        {
            ViewingsManager = new ViewingsManager();
            PresentationModel = new ViewingSummaryModel();
            SelectedCustomer = new CustomerLookUpListModel();
            SelectedStaffMember = new StaffMemberModel();
            SelectedProperty = new PropertyModel();
            PresentationModel.CreatedById = AppSession.LoggedInUser.userId;
            PresentationModel.AssignedById = AppSession.LoggedInUser.userId;
            ViewingDate = DateTime.Now;
            ViewingTime = DateTime.Now.TimeOfDay;
            RefreshMethod = null;
            TitleString = "Arrange Viewing";

        }
         public void ConfigureForEditing(ViewingSummaryModel model)
        {
            TitleString = "Edit Viewing";
            PropertyModel propMod = new PropertyModel();
            propMod.propertyId = model.PropertyId;
            propMod.address = model.PropertyAddress;
           
            StaffMemberModel smMod = new StaffMemberModel();
            smMod.EmployeeId = model.HousingOfficerId;
            smMod.Gender = model.HousingOfficerGender;
            smMod.FirstName = model.AssignedTo;
            smMod.Email = model.HousingOfficerEmail;

            CustomerLookUpListModel custMod = new CustomerLookUpListModel();
            custMod.FirstName = model.CustomerName;
            custMod.CustomerId = model.CustomerId;
            custMod.CustomerPhoneNumber = model.MobileNumber;
            custMod.CustomerGender = model.CustomerGender;
            custMod.Dob = (!string.IsNullOrWhiteSpace(model.CustomerDateOfBirth) && !string.Equals("NA",model.CustomerDateOfBirth))?DateTimeOffset.Parse(model.CustomerDateOfBirth):(DateTimeOffset?) null ;
            ViewingDate = (!string.IsNullOrWhiteSpace(model.ViewingDate))?DateTime.Parse(model.ViewingDate):DateTime.Now;
            ViewingTime = (!string.IsNullOrWhiteSpace(model.FormattedTime)) ? TimeSpan.Parse(model.FormattedTime):DateTime.Now.TimeOfDay;

            PresentationModel = model;
            SelectedCustomer = custMod;
            SelectedProperty = propMod;
            SelectedStaffMember = smMod;

        }

        #endregion
        #region "Messages"
        private void RegisterMessages()
        {
            Messenger.Default.Register<ArrangeViewingMessage>(this, (Msg) =>
            {
                ResetValues();
                if (Msg.SelectedObject != null)
                {
                    ConfigureForEditing(Msg.SelectedObject);
                }
                if (Msg.RefreshMethod != null)
                {
                    RefreshMethod = Msg.RefreshMethod;
                }
                if (Msg.SelectedProperty != null)
                {
                    SelectedProperty = Msg.SelectedProperty;
                    PresentationModel.PropertyAddress = SelectedProperty.address;
                }
                if (Msg.SelectedCustomer != null)
                {
                    CustomerLookUpListModel model = new CustomerLookUpListModel();
                    model.CustomerId = (int)Msg.SelectedCustomer.CustomerId;
                    model.FirstName = Msg.SelectedCustomer.FirstName;
                    model.LastName = Msg.SelectedCustomer.LastName;
                    PresentationModel.CustomerName = model.FirstName + " " + model.LastName;
                    model.Dob = (Msg.SelectedCustomer.Dob!=null)?(DateTimeOffset?)DateTimeOffset.Parse(Msg.SelectedCustomer.Dob.ToString()).LocalDateTime: null;
                    model.CustomerPhoneNumber = Msg.SelectedCustomer.Telephone;
                    model.CustomerGender = Msg.SelectedCustomer.Gender;
                    SelectedCustomer = model;
                }
                
            });
        }
        #endregion
    }
}
