﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.BusinessLayer.Viewings;
using FrontlineApp.ServiceLayer.ServiceObjects;
using FrontlineApp.Utility;
using FrontlineApp.Utility.Constants;
using FrontlineApp.Utility.Helper.Extensions;
using FrontlineApp.UWP.Popups;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Windows.UI.Xaml.Data;

namespace FrontlineApp.UWP.ViewModels
{
    public class SelectCustomerViewModel:BaseViewModel
    {
        #region "Properties"

        private CustomerLookUpListModel _selectedProperty;
        public CustomerLookUpListModel SelectedProperty
        {
            get { return _selectedProperty; }
            set { Set(() => SelectedProperty, ref _selectedProperty, value); }
        }
        private ObservableCollection<GroupInfoListModel> _customers;
        public ObservableCollection<GroupInfoListModel> Customers
        {
            get { return _customers; }
            set { Set(() => Customers, ref _customers, value); }
        }
        public PaginationSO Pagination { get; set; }
        private string _searchText;
        public string SearchText
        {
            get { return _searchText; }
            set { Set(() => SearchText, ref _searchText, value); }
        }

        public ViewingsManager ViewingsManager { get; set; }
        private CollectionViewSource _viewingCVS;
        public CollectionViewSource ViewingsCVS
        {
            get { return _viewingCVS; }
            set { Set(() => ViewingsCVS, ref _viewingCVS, value); }
        }
        private ICollectionView _groupedView;
        public ICollectionView GroupedView
        {
            get { return _groupedView; }
            set { Set(() => GroupedView, ref _groupedView, value); }
        }
        #endregion

        #region "Constructor"
        public SelectCustomerViewModel()
        {
            ViewingsManager = new ViewingsManager();
            Customers = new ObservableCollection<GroupInfoListModel>();
            Pagination = new PaginationSO();
            Pagination.pageSize = 300;
            Pagination.pageNumber = 1;
            SearchText = String.Empty;
            SelectedProperty = new CustomerLookUpListModel();
            ViewingsCVS = new CollectionViewSource();

        }
        #endregion

        #region "Methods"

        #region "Get Customer List"
        public async void GetCustomerList()
        {

            if (GeneralHelper.HasInternetConnection())
            {
                BusyIndicator serverIndicator = BusyIndicator.Start(MessageConstants.FetchingCustomerRecordsFromServer);
                try
                {
                    // Set VM property; updates View appropriately.
                    //Busy = true;
                    ViewingsCVS = new CollectionViewSource();
                    SelectedProperty = new CustomerLookUpListModel();

                    ResultModel<List<GroupInfoListModel>> result = await ViewingsManager.GetCustomerLookUpList(Pagination, SearchText);
                    List<GroupInfoListModel> groupedCustList = result.Value;

                    if (groupedCustList == null || groupedCustList.Count() == 0)
                    {
                        // Show Dialog Message
                        showMessage("Alert", "No Customers found");
                    }
                    else
                    {
                        Customers = groupedCustList.ToObservableCollection();
                        ConfigureCollectionView();
                    }
                }
                catch (Exception e)
                {
                    // Error = ex.Message;
                    showMessage("Error", e.Message);
                }
                finally
                {
                    // Busy = false;
                    serverIndicator.Close();
                }
            }
            else
            {
                showMessage("Error", MessageConstants.InternetNotAvailableError);
            }

            
            
        }
        #endregion
        private void ConfigureCollectionView()
        {
            ViewingsCVS.IsSourceGrouped = true;
            ViewingsCVS.Source = Customers;
            GroupedView = ViewingsCVS.View;
        }
        public void FilterListBySearchString(string query)
        {
            GetCustomerList();
        }
        #endregion
    }
}
