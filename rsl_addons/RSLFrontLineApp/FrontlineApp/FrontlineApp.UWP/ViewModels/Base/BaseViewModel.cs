﻿using GalaSoft.MvvmLight;
using System;
using Windows.UI.Popups;

namespace FrontlineApp.UWP.ViewModels
{
    public class BaseViewModel : ViewModelBase
    {
        public BaseViewModel()
        {
        }

        private bool isBusy;
        public bool IsBusy
        {
            get { return isBusy; }
            set { Set(() => IsBusy, ref isBusy, value); }
        }

        private string error;
        public string Error
        {
            get { return error; }
            set { Set(() => Error, ref error, value); }
        }

        public async void showMessage(string header, string message)
        {
            var dialog = new MessageDialog(message);
            dialog.Title = header;
            dialog.Commands.Add(new UICommand { Label = "OK", Id = 0 });
            await dialog.ShowAsync();
        }

    }
}
