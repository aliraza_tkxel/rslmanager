﻿using System;
using FrontlineApp.ApplicationLayer.Models;
using System.Collections.Generic;
using FrontlineApp.DataLayer.Entities;
using FrontlineApp.DataLayer.Repository;
using System.Linq;
using System.Text.RegularExpressions;
using System.ComponentModel;
using FrontlineApp.Utility.Constants;

namespace FrontlineApp.UWP.ViewModels
{
    public class PropertyFilterObj
    {
        public string PostalCode { get; set; }
        public string Bedrooms { get; set; }
        public double MinRentNum
        {
            get;set;
        }
        public double MaxRentNum
        {
            get;set;
        }
        public string Scheme { get; set; }
        public string PropertyType { get; set; }
        public PropertyFilterObj()
        {
            MinRentNum = 0;
            MaxRentNum = 10000;
            PostalCode = "";
            Bedrooms = "";
            Scheme = ApplicationConstants.SchemePlaceholder;
            PropertyType = ApplicationConstants.PropertyTypePlaceHolder;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        public void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
    public class PropertyFilterViewModel : BaseViewModel
    {
        #region "Properties"
        private List<PropertyType> _propertyTypes;
        public List<PropertyType> PropertyTypes
        {
            get { return _propertyTypes; }
            set { Set(() => PropertyTypes, ref _propertyTypes, value); }
        }
        private List<PropertyScheme> _schemes;
        public List<PropertyScheme> Schemes
        {
            get
            {
                return _schemes;
            }
            set { Set(() => Schemes, ref _schemes, value); }
        }
        private List<String> _schemeDataSource;
        public List<String> SchemeDataSource
        {
            get
            {
                return _schemeDataSource;
            }
            set { Set(() => SchemeDataSource, ref _schemeDataSource, value); }
        }
        private List<String> _propertyTypeDataSource;
        public List<String> PropertyTypeDataSource
        {
            get
            {
                return _propertyTypeDataSource;
            }
            set { Set(() => PropertyTypeDataSource, ref _propertyTypeDataSource, value); }
        }
        
        private PropertyFilterObj _filterObj;
        public PropertyFilterObj FilterObj
        {
            get
            {
                return _filterObj;
            }
            set { Set(() => FilterObj, ref _filterObj, value); }
        }
        #endregion
        #region "Constructor"
        public PropertyFilterViewModel()
        {
            PopulateDataSources();
        }
        #endregion
        #region "Methods"
        private void PopulateDataSources()
        {
            Schemes = new List<PropertyScheme>();
            PropertyTypes = new List<PropertyType>();
            FilterObj = new PropertyFilterObj();
            Repository<PropertyScheme> myRepo = new Repository<PropertyScheme>();
            Schemes =  myRepo.GetAll();
            var ds = from emp in Schemes
                     select emp.description;
            ds = ds.OrderBy(x => x).ToList();
            
            SchemeDataSource = ds as List<String>;
            SchemeDataSource.Insert(0, ApplicationConstants.SchemePlaceholder);
            Repository<PropertyType> propertyRep = new Repository<PropertyType>();
            PropertyTypes =  propertyRep.GetAll();
            var typeDs = from emp in PropertyTypes
                         select emp.description;
            typeDs = typeDs.OrderBy(x => x).ToList();
            PropertyTypeDataSource = typeDs as List<String>;
            PropertyTypeDataSource.Insert(0, ApplicationConstants.PropertyTypePlaceHolder);
        }
        public PropertyFilterModel PopulateFilterModel()
        {
            PropertyFilterModel model = new PropertyFilterModel();
            model.postalCode = string.IsNullOrWhiteSpace(FilterObj.PostalCode) ? "" : FilterObj.PostalCode;
            model.bedRooms = string.IsNullOrWhiteSpace(StripNonNumerics(FilterObj.Bedrooms)) ? 0 : Convert.ToInt32(FilterObj.Bedrooms);
            model.minRent = FilterObj.MinRentNum;
            model.maxRent = FilterObj.MaxRentNum;
            model.schemeId = GetSelectedSchemeId();
            model.propertyTypeId = GetSelectedPropertyId();
            FilterObj = new PropertyFilterObj();
            return model;
        }
        public int GetSelectedSchemeId()
        {
            if (string.IsNullOrWhiteSpace(FilterObj.Scheme))
            {
                return 0;
            }
            if (string.Equals(FilterObj.Scheme, ApplicationConstants.SchemePlaceholder))
            {
                return 0;
            }
            List<PropertyScheme> sch = Schemes.Where(o => o.description == FilterObj.Scheme).ToList();
            int id = sch.ElementAt(0).id;
            return id;
        }
        public int GetSelectedPropertyId()
        {
            if (string.IsNullOrWhiteSpace(FilterObj.PropertyType))
            {
                return 0;
            }
            if (string.Equals(FilterObj.PropertyType, ApplicationConstants.PropertyTypePlaceHolder))
            {
                return 0;
            }
            List<PropertyType> sch = PropertyTypes.Where(o => o.description == FilterObj.PropertyType).ToList();
            int id = sch.ElementAt(0).id;
            return id;
        }
        public string StripNonNumerics(string phone)
        {
            Regex digitsOnly = new Regex(@"[^\d]");
            return digitsOnly.Replace(phone, "");
        }
        #endregion
    }
}
