﻿using System;
using System.Threading.Tasks;
using System.ComponentModel;
using FrontlineApp.ApplicationLayer.Models;
using Windows.UI.Popups;

using FrontlineApp.BusinessLayer.PropertyManager;
using System.Collections.Generic;
using FrontlineApp.Utility.Constants;
using FrontlineApp.UWP.Popups;
using GalaSoft.MvvmLight.Messaging;
using FrontlineApp.UWP.ViewModels.Messages;
using FrontlineApp.Utility;

namespace FrontlineApp.UWP.ViewModels
{
    public class PropertyDetailsViewModel : BaseViewModel
    {
        #region "Properties"
        
        private List<PropertyViewingModel> _selectedPropertyViewingsList;
        public List<PropertyViewingModel> SelectedPropertyViewingsList
        {
            get { return _selectedPropertyViewingsList; }
            set { Set(() => SelectedPropertyViewingsList, ref _selectedPropertyViewingsList, value); }
        }
        private PropertyDetailsModel _selectedProperty;
        public PropertyDetailsModel SelectedProperty
        {
            get { return _selectedProperty; }
            set { Set(() => SelectedProperty, ref _selectedProperty, value); }
        }
        private bool hasViewings;
        public bool HasViewings
        {
            get { return hasViewings; }
            set { Set(() => HasViewings, ref hasViewings, value); }
        }

        public PropertyManager propertyManager { get; set; }
        #endregion

        public PropertyDetailsViewModel()
        {
            _selectedProperty = new PropertyDetailsModel();
            propertyManager = new PropertyManager();
        }
        
       
        
        

        public async Task<bool> GetPropertyDetailsForSelectedProperty(string selectedPropId)
        {
            if (GeneralHelper.HasInternetConnection())
            {
                BusyIndicator serverIndicator = BusyIndicator.Start(MessageConstants.FetchingPropertyDetails);
                ManagerResponseModel responseObj = await propertyManager.GetPropertyDetailsForId(selectedPropId); //TODO: Get image path here
                if (responseObj.isSuccessful)
                {
                    SelectedProperty = responseObj.responseObject as PropertyDetailsModel;
                }
                else
                {
                    showMessage("Error", responseObj.message);
                }
                SelectedPropertyViewingsList = new List<PropertyViewingModel>();
                SelectedPropertyViewingsList = await propertyManager.GetPropertyViewingsListForId(selectedPropId);
                HasViewings = (SelectedPropertyViewingsList.Count > 0) ? true : false;
                serverIndicator.Close();
            }
            else
            {
                showMessage("Error", MessageConstants.InternetNotAvailableError);
            }
            return true;
        }
        public void SendTenancyMessages()
        {
            Messenger.Default.Send(new ArrangeTenancyMessage()
            {
                source = "PropertyTenancy",
                PropertyModel = SelectedProperty,
                CustomerModel = null
            }
            );
        }
        public void SendViewingMessage(Action method)
        {
            Messenger.Default.Send(new ArrangeViewingMessage()
            {
                Source = ApplicationConstants.ViewingSourcePropertyKey,
                SelectedProperty = ConvertPropertyDetailsToPropertyModel(), 
                RefreshMethod = method
            }
            );
        }
        public PropertyModel ConvertPropertyDetailsToPropertyModel()
        {
            PropertyModel propMod = new PropertyModel();
            propMod.propertyId = SelectedProperty.propertyId;
            propMod.address = SelectedProperty.completeAddress;
            return propMod;
        }
    }
}
