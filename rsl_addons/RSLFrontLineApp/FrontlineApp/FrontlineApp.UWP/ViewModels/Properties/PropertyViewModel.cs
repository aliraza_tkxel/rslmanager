﻿using FrontlineApp.ApplicationLayer.Models;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using FrontlineApp.BusinessLayer.PropertyManager;
using FrontlineApp.UWP.Popups;
using FrontlineApp.Utility.Constants;
using GalaSoft.MvvmLight.Messaging;
using FrontlineApp.UWP.ViewModels.Messages;
using FrontlineApp.Utility.Helper.Extensions;
using FrontlineApp.Utility;

namespace FrontlineApp.UWP.ViewModels
{
    public class PropertyViewModel : BaseViewModel
    {
        private ObservableCollection<PropertyModel> _propertyModelList;
        public ObservableCollection<PropertyModel> PropertyModelList
        {
            get{return _propertyModelList;}
            set {Set(() => PropertyModelList, ref _propertyModelList, value); }
        }
        public PropertyManager propertyManager;
        private PropertyModel _selectedProperty;
        public PropertyModel SelectedProperty
        {
            get { return _selectedProperty; }
            set { Set(() => SelectedProperty, ref _selectedProperty, value); }
        }
        private PropertyFilterModel _filterModel;
        public PropertyFilterModel FilterObj
        {
            get{return _filterModel;}
            set { Set(() => FilterObj, ref _filterModel, value); }
        }
        private bool _shouldShowNoPropertiesDisclaimer;
        public bool ShouldShowNoPropertiesDisclaimer
        {
            get{return _shouldShowNoPropertiesDisclaimer;}
            set { Set(() => ShouldShowNoPropertiesDisclaimer, ref _shouldShowNoPropertiesDisclaimer, value); }
        }

        private string _searchQuery;
        public string SearchQuery
        {
            get { return _searchQuery; }
            set { Set(() => SearchQuery, ref _searchQuery, value); }
        }

        public PropertyViewModel()
        {
            RegisterMessages();
            InitializeData();
          
        }
        public void InitializeData()
        {
            PropertyModelList = new ObservableCollection<PropertyModel>();
            ShouldShowNoPropertiesDisclaimer = true;
            SelectedProperty = new PropertyModel();
            FilterObj = new PropertyFilterModel();
            propertyManager = new PropertyManager();
        }
        
        public async void GetPropertyList()
        {
            if (GeneralHelper.HasInternetConnection())
            {
                BusyIndicator serverIndicator = BusyIndicator.Start(MessageConstants.FetchingPropertiesList);
                ManagerResponseModel response = await propertyManager.GetAllProperties();
                if (response.isSuccessful == false)
                {
                    showMessage("Error", response.message);
                }
                else
                {
                    if (FilterObj.shouldApplyFilter)
                    {
                        response = propertyManager.ApplyFilter(FilterObj, response.responseObject as List<PropertyModel>);

                    }
                }
                ConvertListToCollection(response.responseObject as List<PropertyModel>);
                ShouldShowNoPropertiesDisclaimer = (PropertyModelList.Count == 0) ? true : false;
                serverIndicator.Close();
                if (!string.IsNullOrWhiteSpace(SearchQuery))
                {
                    FilterListBySearchQuery(SearchQuery);
                    SearchQuery = "";
                }
            }
            else
            {
                showMessage("Error", MessageConstants.InternetNotAvailableError);
            }
            
        }
        
        public void ConvertListToCollection(List<PropertyModel>q)
        {
           PropertyModelList = q.ToObservableCollection(); 
        }

        public void FilterListBySearchQuery(string args)
        {
            if (string.IsNullOrEmpty(args))
            {
                GetPropertyList();
            }
            else
            {
                ManagerResponseModel response =  propertyManager.SearchObjectsByCustomQuery(args);
                ConvertListToCollection(response.responseObject as List<PropertyModel>);
                ShouldShowNoPropertiesDisclaimer = (PropertyModelList.Count == 0) ? true : false;
            }
            
        }
        #region "Messages"
        private void RegisterMessages()
        {
            Messenger.Default.Register<RefreshAfterTenancyMessage>(this, (message) =>
            {
                if (message.IsForProperty)
                {
                    InitializeData();
                    GetPropertyList();
                }
            });
        }
        #endregion

    }
}
