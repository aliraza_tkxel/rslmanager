﻿using System.Collections.Generic;
using FrontlineApp.BusinessLayer.Dashboard;
using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.UWP.Popups;
using FrontlineApp.Utility.Constants;
using FrontlineApp.Utility;
using FrontlineApp.UWP.ViewModels.Messages;
using GalaSoft.MvvmLight.Messaging;

namespace FrontlineApp.UWP.ViewModels
{
    public class DashboardViewModel: BaseViewModel
    {
        #region "Properties"
        private DashboardManager DashboardMgr;
        private List<DashboardGridModel> _viewingsStats;
        public List<DashboardGridModel> ViewingsStats
        {
            get { return _viewingsStats; }
            set {Set(() => ViewingsStats, ref _viewingsStats, value);}
        }

        private List<DashboardGridModel> _alertsStats;
        public List<DashboardGridModel> AlertsStats
        {
            get { return _alertsStats; }
            set { Set(() => AlertsStats, ref _alertsStats, value);}
        }

        #endregion
        #region "Constructor"
        public DashboardViewModel()
        {
            DashboardMgr = new DashboardManager();
            ViewingsStats = new List<DashboardGridModel>();
            AlertsStats = new List<DashboardGridModel>();
        }
        #endregion
        #region "Methods"
        public async void UpdateViewingsStats()
        {
            BusyIndicator serverIndicator = BusyIndicator.Start(MessageConstants.UpdatingDashboardStats);
            ViewingsStats = await DashboardMgr.GetViewingsStats();
            if (!GeneralHelper.HasInternetConnection())
            {
                //AlertsStats = await DashboardMgr.GetAlertsStats();
                showMessage(MessageConstants.AppOfflineHeader, MessageConstants.AppOfflineFeatureUnavailable);
            }
            
            serverIndicator.Close();

        }

        

        #endregion
    }
}
