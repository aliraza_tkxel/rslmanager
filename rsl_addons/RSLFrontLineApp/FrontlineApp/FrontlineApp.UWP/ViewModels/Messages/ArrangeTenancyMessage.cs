﻿using FrontlineApp.ApplicationLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.UWP.ViewModels.Messages
{
    public class ArrangeTenancyMessage
    {
        public string source { get; set; }
        public CustomerModel CustomerModel { get; set; }
        public PropertyDetailsModel PropertyModel { get; set; }
    }
}
