﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.UWP.ViewModels.Messages
{
    public class RefreshCustomerListMessage
    {
        public bool IsLocalList { get; set; }
    }
}
