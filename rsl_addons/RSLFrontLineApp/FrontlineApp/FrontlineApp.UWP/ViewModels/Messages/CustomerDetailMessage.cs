﻿using FrontlineApp.ApplicationLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.UWP.ViewModels.Messages
{
    public class CustomerDetailMessage
    {
        public CustomerModel OldCustomerDetail { get; set; }
        public CustomerModel UpdatedCustomerDetail { get; set; }
    }
}
