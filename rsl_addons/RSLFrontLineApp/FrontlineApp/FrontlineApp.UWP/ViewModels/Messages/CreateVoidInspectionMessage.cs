﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.UWP.ViewModels.Messages
{
    public class CreateVoidInspectionMessage
    {
        public int? JournalId { get; set; }
    }
}
