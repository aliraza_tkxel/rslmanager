﻿using FrontlineApp.ApplicationLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.UWP.ViewModels.Messages
{
    class ArrangeViewingMessage
    {
        /*
         *Source can be:
         * ViewingsMasterPage - Done
         * LoggedViewingsMasterPage - Done
         * ApprovalViewingsMasterPage - Done
         * PropertyDetailPage - Done
         * Customer - Done     
        */
        public string Source;
        public ViewingSummaryModel SelectedObject { get; set; }
        public Action RefreshMethod { get;set; }
        public PropertyModel SelectedProperty { get; set; }
        public CustomerModel SelectedCustomer { get; set; }
    }
}
