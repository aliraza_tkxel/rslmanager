﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.BusinessLayer;
using FrontlineApp.Utility.Constants;
using FrontlineApp.UWP.ViewModels.Messages;
using GalaSoft.MvvmLight.Messaging;

namespace FrontlineApp.UWP.ViewModels
{
    public class TenantDetailViewModel : BaseViewModel
    {
        #region "Properties"
        public CustomerManager CustomerManager { get; set; }

        private CustomerModel selectedCustomer;
        public CustomerModel SelectedCustomer
        {
            get { return selectedCustomer; }
            set { Set(() => SelectedCustomer, ref selectedCustomer, value); }
        }

        #endregion

        #region "Constructor"
        public TenantDetailViewModel()
        {
            CustomerManager = new CustomerManager();
            RegisterMessages();            
        }
        #endregion

        #region "Commands"
        #endregion

        #region "Methods"

        #region "Register Messages"
        private void RegisterMessages()
        {

            Messenger.Default.Register<CustomerDetailMessage>(this, (message) =>
            {
                this.SelectedCustomer = message.UpdatedCustomerDetail;
            });

        }
        #endregion
        public void SendViewingMessage()
        {
            Messenger.Default.Send(new ArrangeViewingMessage()
            {
                Source = ApplicationConstants.ViewingSourceCustomerKey,
                SelectedCustomer = SelectedCustomer
            }
            );
        }
        #endregion
    }
}
