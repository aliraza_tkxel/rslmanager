﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.BusinessLayer;
using FrontlineApp.BusinessLayer.Viewings;
using FrontlineApp.ServiceLayer.ServiceObjects;
using FrontlineApp.Utility;
using FrontlineApp.Utility.Constants;
using FrontlineApp.Utility.Helper.AppSession;
using FrontlineApp.Utility.Helper.Extensions;
using FrontlineApp.UWP.Popups;
using FrontlineApp.UWP.ViewModels.Messages;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;


namespace FrontlineApp.UWP.ViewModels
{
    public class CreateVoidInspectionViewModel: BaseViewModel
    {
        #region "Constructor"
        public CreateVoidInspectionViewModel()
        {
            RegisterMessages();
        }
        public void InitializeModule()
        {
            RequestBody = new VoidInspectionSupervisorsRequestSO();
            InspectionTypeList = new ObservableCollection<LookupModel>();
            StaffMemberList = new ObservableCollection<VoidInspectionSupervisorSO>();
            VoidInspectionRequest = new CreateVoidInspectionRequestSO();
            CustomerMgr = new CustomerManager();

            StartDate = DateTime.Now;
            int minuteFactor = 0;
            if(DateTime.Now.TimeOfDay.Minutes>0 && DateTime.Now.TimeOfDay.Minutes <= 15)
            {
                minuteFactor = 0;
            }
            else if (DateTime.Now.TimeOfDay.Minutes > 15 && DateTime.Now.TimeOfDay.Minutes <= 30)
            {
                minuteFactor = 15;
            }
            else if (DateTime.Now.TimeOfDay.Minutes > 30 && DateTime.Now.TimeOfDay.Minutes <= 45)
            {
                minuteFactor = 45;
            }
            else if (DateTime.Now.TimeOfDay.Minutes > 45)
            {
                minuteFactor = 0;
            }
            StartTime = new TimeSpan(DateTime.Now.TimeOfDay.Hours+2, minuteFactor,0);
            EndTime = new TimeSpan(DateTime.Now.TimeOfDay.Hours + 2, minuteFactor, 0); ;

            var listp = new List<LookupModel>();
            LookupModel model = new LookupModel();
            model.Id = 1;
            model.Description = "Void Inspection";
            listp.Add(model);

            VoidInspectionType = 1;

            InspectionTypeList = listp.ToObservableCollection();
        }
        #endregion

        #region "Properties"

        private int _voidInspectionType;
        public int VoidInspectionType
        {
            get { return _voidInspectionType; }
            set { Set(() => VoidInspectionType, ref _voidInspectionType, value); }
        }

        public int JournalId { get; set; }

        private DateTimeOffset? _pickerDateOffSet;
        public DateTimeOffset? PickerDateOffSet
        {
            set
            {
                Set(() => PickerDateOffSet, ref _pickerDateOffSet, value);
                StartDate = DateTime.Parse(_pickerDateOffSet.ToString());
                StaffMemberList = new ObservableCollection<VoidInspectionSupervisorSO>();
            }

            get
            {
                return new DateTime(StartDate.Year, StartDate.Month, StartDate.Day);
            }
        }

        public CreateVoidInspectionRequestSO VoidInspectionRequest { get; set; }

        private bool _shouldAllowSelection;
        public bool ShouldAllowSelection
        {
            get { return _shouldAllowSelection; }
            set { Set(() => ShouldAllowSelection, ref _shouldAllowSelection, value); }
        }


        private DateTime _startDate;
        public DateTime StartDate
        {
            get { return _startDate; }
            set { Set(() => StartDate, ref _startDate, value); }
        }

        private TimeSpan _startTime;
        public TimeSpan StartTime
        {
            get { return _startTime; }
            set { Set(() => StartTime, ref _startTime, value); }
        }

        private TimeSpan _endTime;
        public TimeSpan EndTime
        {
            get { return _endTime; }
            set { Set(() => EndTime, ref _endTime, value); }
        }

        public CustomerManager CustomerMgr { get; set; }

        private VoidInspectionSupervisorSO _selectedSupervisor;
        public VoidInspectionSupervisorSO SelectedSupervisor
        {
            get { return _selectedSupervisor; }
            set { Set(() => SelectedSupervisor, ref _selectedSupervisor, value); }
        }

        private ObservableCollection<LookupModel> _inspectionTypeList;
        public ObservableCollection<LookupModel> InspectionTypeList
        {
            get { return _inspectionTypeList; }
            set { Set(() => InspectionTypeList, ref _inspectionTypeList, value); }
        }

        private ObservableCollection<VoidInspectionSupervisorSO> _staffList;
        public ObservableCollection<VoidInspectionSupervisorSO> StaffMemberList
        {
            get { return _staffList; }
            set
            {
                Set(() => StaffMemberList, ref _staffList, value);
                ShouldAllowSelection = (StaffMemberList.Count > 0) ? true : false;
            }
        }

        private VoidInspectionSupervisorsRequestSO _requestBody;
        public VoidInspectionSupervisorsRequestSO RequestBody
        {
            get { return _requestBody; }
            set { Set(() => RequestBody, ref _requestBody, value); }
        }

        #endregion

        #region "Methods"
        public async void FetchSupervisors()
        {
            if (GeneralHelper.HasInternetConnection())
            {
                BusyIndicator serverIndicator = BusyIndicator.Start(MessageConstants.FetchingVoidSupervisors);
                RequestBody.startDate = StartDate;
                var serviceResponse = await CustomerMgr.FetchVoidSupervisors(RequestBody);
                if (serviceResponse != null)
                {
                    if (serviceResponse.Supervisors != null)
                    {
                        StaffMemberList = serviceResponse.Supervisors.ToObservableCollection();
                    }
                }
                
                if (StaffMemberList == null)
                {
                    StaffMemberList = new ObservableCollection<VoidInspectionSupervisorSO>();
                }
                if (StaffMemberList.Count <= 0)
                {
                    showMessage("Alert", MessageConstants.NoVoidSupervisorsFound);
                }
                serverIndicator.Close();
            }
            else
            {
                showMessage("Error", MessageConstants.InternetNotAvailableError);
            }
            
        }

        public async Task<ResultModel<bool>> CreateVoidInspection()
        {
            ResultModel<bool> resMod = new ResultModel<bool>();
            resMod.Success = false;
            resMod.Message = string.Empty;
            if (GeneralHelper.HasInternetConnection())
            {

                
                bool isValid = ValidateData();
                if(isValid == true)
                {
                    /*Journal Id should be assigned via notif and Appointment Notes are Pre-binded*/

                    VoidInspectionRequest.AppointmentStartDate = StartDate;
                    VoidInspectionRequest.JournalId = JournalId;
                    VoidInspectionRequest.AppointmentEndDate = StartDate;
                    VoidInspectionRequest.StartTime = new DateTime(StartTime.Ticks).ToString("HH:mm"); 
                    VoidInspectionRequest.EndTime = new DateTime(EndTime.Ticks).ToString("HH:mm");
                    VoidInspectionRequest.UserId = AppSession.LoggedinUserId;
                    VoidInspectionRequest.OperativeId = SelectedSupervisor.EmployeeId;
                    VoidInspectionRequest.IsRearrange = false;
                    VoidInspectionRequest.Duration = (decimal)(EndTime - StartTime).TotalHours;

                    BusyIndicator serverIndicator = BusyIndicator.Start(MessageConstants.CreatingVoidInspection);
                    var serviceResponse = await CustomerMgr.CreateVoidInspection(VoidInspectionRequest);
                    serverIndicator.Close();
                    if (serviceResponse != null)
                    {
                        if (serviceResponse.Response != null)
                        {
                            resMod.Success = serviceResponse.Response.IsSaved;
                            resMod.Message = serviceResponse.Response.StatusMessage;
                        }
                        else
                        {
                            resMod.Success = false;
                            resMod.Message = MessageConstants.VoidInspectionServerError;
                        }
                        
                    }
                    else
                    {
                        resMod.Success = false;
                        resMod.Message = MessageConstants.VoidInspectionServerError;
                    }

                }
                else
                {
                    resMod.Success = false;
                    resMod.Message = "VE";
                }

            }
            else
            {
                resMod.Success = false;
                resMod.Message = MessageConstants.InternetNotAvailableError;
            }
            return resMod;
        }

        public bool ValidateData()
        {
            
            if (StartDate == null)
            {
                showMessage("Error", MessageConstants.VoidInspectionStartDateNull);
                return false;
            }
            if (StartDate.Date < DateTime.Today.Date)
            {
                showMessage("Error", MessageConstants.VoidInspectionStartDateInvalid);
                return false;
            }
            if(StartTime == null)
            {
                showMessage("Error", MessageConstants.VoidInspectionStartTimeNull);
                return false;
            }
            if(EndTime ==null )
            {
                showMessage("Error", MessageConstants.VoidInspectionEndTimeNull);
                return false;
            }

            if(SelectedSupervisor == null)
            {
                showMessage("Error", MessageConstants.VoidInspectionSupervisorNull);
                return false;
            }

            if (SelectedSupervisor.EmployeeId == 0)
            {
                showMessage("Error", MessageConstants.VoidInspectionSupervisorNull);
                return false;
            }
            if (StartTime > EndTime)
            {
                showMessage("Error", MessageConstants.VoidInspectionStartTimeInvalid);
                return false;
            }
            else if(StartTime.Hours == EndTime. Hours  && EndTime.Minutes == StartTime.Minutes)
            {
                showMessage("Error", MessageConstants.VoidInspectionStartTimeInvalid);
                return false;
            }
            return true;
        }

        #endregion

        #region "Messages"
        private void RegisterMessages()
        {
            Messenger.Default.Register<CreateVoidInspectionMessage>(this, (msg) =>
            {
                InitializeModule();
                JournalId = msg.JournalId.Value;
                

            });
        }
        #endregion
    }
}
