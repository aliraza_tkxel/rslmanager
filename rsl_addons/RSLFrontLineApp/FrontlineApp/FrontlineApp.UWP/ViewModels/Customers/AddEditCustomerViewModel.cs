﻿using System.Collections.ObjectModel;
using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.BusinessLayer;
using FrontlineApp.Utility.Helper.Extensions;
using System.Linq;
using FrontlineApp.UWP.Infrastructure;
using System;
using FrontlineApp.Utility.Constants;
using MvvmValidation;
using FrontlineApp.Utility.Helper.AppSession;
using Windows.Storage.Pickers;
using Windows.Storage;
using Windows.UI.Xaml.Media.Imaging;
using Windows.Storage.Streams;
using FrontlineApp.Utility;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FrontlineApp.UWP.ViewModels
{
    public class AddEditCustomerViewModel : BaseViewModel
    {

        #region "Properties"

        private ObservableCollection<LookupModel> customerTitleList;
        public ObservableCollection<LookupModel> CustomerTitleList
        {
            get { return customerTitleList; }
            set { Set(() => CustomerTitleList, ref customerTitleList, value); }
        }

        private ObservableCollection<LookupModel> customerTypeList;
        public ObservableCollection<LookupModel> CustomerTypeList
        {
            get { return customerTypeList; }
            set { Set(() => CustomerTypeList, ref customerTypeList, value); }
        }

        private ObservableCollection<LookupModel> preferredContactList;
        public ObservableCollection<LookupModel> PreferredContactList
        {
            get { return preferredContactList; }
            set { Set(() => PreferredContactList, ref preferredContactList, value); }
        }

        private ObservableCollection<LookupModel> maritalStatusList;
        public ObservableCollection<LookupModel> MaritalStatusList
        {
            get { return maritalStatusList; }
            set { Set(() => MaritalStatusList, ref maritalStatusList, value); }
        }

        private ObservableCollection<LookupModel> ethnicityList;
        public ObservableCollection<LookupModel> EthnicityList
        {
            get { return ethnicityList; }
            set { Set(() => EthnicityList, ref ethnicityList, value); }
        }

        private ObservableCollection<LookupModel> religionList;
        public ObservableCollection<LookupModel> ReligionList
        {
            get { return religionList; }
            set { Set(() => ReligionList, ref religionList, value); }
        }

        private ObservableCollection<LookupModel> sexualityList;
        public ObservableCollection<LookupModel> SexualityList
        {
            get { return sexualityList; }
            set { Set(() => SexualityList, ref sexualityList, value); }
        }

        private ObservableCollection<LookupModel> nationailityList;
        public ObservableCollection<LookupModel> NationailityList
        {
            get { return nationailityList; }
            set { Set(() => NationailityList, ref nationailityList, value); }
        }

        private ObservableCollection<LookupModel> yesNoList;
        public ObservableCollection<LookupModel> YesNoList
        {
            get { return yesNoList; }
            set { Set(() => YesNoList, ref yesNoList, value); }
        }

        private ObservableCollection<LookupModel> yesNoUnknownList;
        public ObservableCollection<LookupModel> YesNoUnknownList
        {
            get { return yesNoUnknownList; }
            set { Set(() => YesNoUnknownList, ref yesNoUnknownList, value); }
        }

        private ObservableCollection<LookupModel> landlordTypeList;
        public ObservableCollection<LookupModel> LandlordTypeList
        {
            get { return landlordTypeList; }
            set { Set(() => LandlordTypeList, ref landlordTypeList, value); }
        }

        private ObservableCollection<LookupModel> houseTypeList;
        public ObservableCollection<LookupModel> HouseTypeList
        {
            get { return houseTypeList; }
            set { Set(() => HouseTypeList, ref houseTypeList, value); }
        }

        private ObservableCollection<LookupModel> numberOfBedroomsList;
        public ObservableCollection<LookupModel> NumberOfBedroomsList
        {
            get { return numberOfBedroomsList; }
            set { Set(() => NumberOfBedroomsList, ref numberOfBedroomsList, value); }
        }

        private ObservableCollection<LookupModel> floorList;
        public ObservableCollection<LookupModel> FloorList
        {
            get { return floorList; }
            set { Set(() => FloorList, ref floorList, value); }
        }

        private ObservableCollection<string> genderList;
        public ObservableCollection<string> GenderList
        {
            get { return genderList; }
            set { Set(() => GenderList, ref genderList, value); }
        }

        private ObservableCollection<LookupModel> addressTypeList;
        public ObservableCollection<LookupModel> AddressTypeList
        {
            get { return addressTypeList; }
            set { Set(() => AddressTypeList, ref addressTypeList, value); }
        }

        private ObservableCollection<LookupModel> specialRequirementList;
        public ObservableCollection<LookupModel> SpecialRequirementList
        {
            get { return specialRequirementList; }
            set { Set(() => SpecialRequirementList, ref specialRequirementList, value); }
        }

        private ObservableCollection<LookupModel> internetAccessList;
        public ObservableCollection<LookupModel> InternetAccessList
        {
            get { return internetAccessList; }
            set { Set(() => InternetAccessList, ref internetAccessList, value); }
        }

        private CustomerModel customer;
        public CustomerModel Customer
        {
            get { return customer; }
            set { Set(() => Customer, ref customer, value); }
        }

        private CustomerAddressModel customerAddress;
        public CustomerAddressModel CustomerAddress
        {
            get { return customerAddress; }
            set { Set(() => CustomerAddress, ref customerAddress, value); }
        }

        private List<CustomerAddressModel> customerAddressList;
        public List<CustomerAddressModel> CustomerAddressList
        {
            get { return customerAddressList; }
            set { Set(() => CustomerAddressList, ref customerAddressList, value); }
        }

        private CustomerImageModel customerImage;
        public CustomerImageModel CustomerImage
        {
            get { return customerImage; }
            set { Set(() => CustomerImage, ref customerImage, value); }
        }

        private CurrentHomeModel customerCurrentHome;
        public CurrentHomeModel CustomerCurrentHome
        {
            get { return customerCurrentHome; }
            set { Set(() => CustomerCurrentHome, ref customerCurrentHome, value); }
        }

        public LookupManager LookupManager { get; set; }
        public CustomerManager CustomerManager { get; set; }

        private string title;
        public string Title
        {
            get { return title; }
            set { Set(() => Title, ref title, value); }
        }

        private string profilePicTitle;
        public string ProfilePicTitle
        {
            get { return profilePicTitle; }
            set { Set(() => ProfilePicTitle, ref profilePicTitle, value); }
        }

        private bool isNewCustomer;
        public bool IsNewCustomer
        {
            get { return isNewCustomer; }
            set { Set(() => IsNewCustomer, ref isNewCustomer, value); }
        }

        #endregion

        #region "Constructor"
        public AddEditCustomerViewModel()
        {
            LookupManager = new LookupManager();
            CustomerManager = new CustomerManager();
            SelectImageCommand = new DelegateCommand(SelectImage, () => true);
        }
        #endregion

        #region "Commands"

        #region "Save Command"

        public async Task<ResultModel<CustomerModel>> Save()
        {
            ResultModel<CustomerModel> result = new ResultModel<CustomerModel>();
            try
            {
                var validationResult = IsValid();
                if (validationResult.Success)
                {
                    Customer.InternetAccess = LookupManager.GetSelectedItems(InternetAccessList);
                    Customer.Communication = LookupManager.GetSelectedItems(SpecialRequirementList);

                    Customer.CustomerImage = GetCustomerImageModel();
                    Customer.CurrentHome = GetCurrentHomeModel();
                    Customer.CustomerAddresses = GetCustomerAddress();

                    Customer.AppVersion = GeneralHelper.GetAppVersion();
                    Customer.IsSynced = false;
                    if (isNewCustomer == true)
                    {
                        Customer.CreatedOnApp = DateTimeOffset.UtcNow;
                        Customer.CreatedBy = AppSession.LoggedinUserId;
                    }
                    else
                    {
                        Customer.LastModifiedOnApp = DateTimeOffset.Now;
                    }

                    result = await CustomerManager.SaveCustomer(Customer);
                }
                else
                {
                    result.Success = false;
                    result.Message = validationResult.Message;
                }

            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.Success = false;
            }

            return result;
        }

        #endregion

        #region "Select Image Command"

        public DelegateCommand SelectImageCommand { get; set; }
        private async void SelectImage()
        {

            FileOpenPicker openPicker = new FileOpenPicker();
            openPicker.ViewMode = PickerViewMode.Thumbnail;
            openPicker.SuggestedStartLocation = PickerLocationId.PicturesLibrary;
            openPicker.FileTypeFilter.Add(".jpg");
            openPicker.FileTypeFilter.Add(".png");

            StorageFile file = await openPicker.PickSingleFileAsync();

            if (file != null)
            {
                var fileInfo = await file.GetBasicPropertiesAsync();

                if (fileInfo.Size < ApplicationConstants.MaxProfileImageFileSizeInBytes)
                {
                    string fileName = string.Format("Customer_Profile_Image_{0}{1}", DateTime.UtcNow.ToString("yyyyMMHHddmmss"), file.FileType);
                    file = await file.CopyAsync(ApplicationData.Current.LocalFolder, fileName);

                    Uri uri = new Uri(string.Format("{0}{1}", PathConstants.LocalFolderPath, fileName));
                    StorageFile storedFile = await StorageFile.GetFileFromApplicationUriAsync(uri);

                    BitmapImage bitmapImage = new BitmapImage();
                    FileRandomAccessStream stream = (FileRandomAccessStream)await storedFile.OpenAsync(FileAccessMode.Read);
                    bitmapImage.SetSource(stream);
                    CustomerImage.ImageName = fileName;
                    CustomerImage.BaseAddress = PathConstants.LocalFolderPath;
                    CustomerImage.IsSynced = false;
                }
                else
                {
                    showMessage(MessageConstants.ErrorMessageBoxHeader, MessageConstants.ProfileImagesizeExceed);
                }
            }

        }

        #endregion

        #endregion

        #region "Methods"

        #region "Initialize Data"
        public ResultModel<bool> InitializeData(CustomerModel customer)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            try
            {
                PopulateLookups();
                SetupCustomer(customer);
                result.Success = true;
            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.Success = false;
            }

            return result;

        }
        #endregion

        #region "Populate lookups"
        private void PopulateLookups()
        {
            NumberOfBedroomsList = LookupManager.GetNumberOfBedrooms().ToObservableCollection();
            GenderList = LookupManager.GetGenderList().ToObservableCollection();
            CustomerTitleList = (LookupManager.GetCustomerTitles())?.ToObservableCollection();
            PreferredContactList = (LookupManager.GetPreferedContacts())?.ToObservableCollection();
            InternetAccessList = (LookupManager.GetInternetAccess())?.ToObservableCollection();
            MaritalStatusList = (LookupManager.GetMaritalStatuses())?.ToObservableCollection();
            EthnicityList = (LookupManager.GetEthnicity())?.ToObservableCollection();
            ReligionList = (LookupManager.GetReligion())?.ToObservableCollection();
            SexualityList = (LookupManager.GetSexualOrientation())?.ToObservableCollection();
            NationailityList = (LookupManager.GetNationality())?.ToObservableCollection();
            YesNoList = (LookupManager.GetYesNo())?.ToObservableCollection();
            YesNoUnknownList = (LookupManager.GetYesNoUnknown())?.ToObservableCollection();
            LandlordTypeList = (LookupManager.GetLandlordType())?.ToObservableCollection();
            HouseTypeList = (LookupManager.GetHouseTypes())?.ToObservableCollection();
            SpecialRequirementList = (LookupManager.GetCommunication())?.ToObservableCollection();
            AddressTypeList = (LookupManager.GetAddressType())?.ToObservableCollection();
            FloorList = (LookupManager.GetFloor())?.ToObservableCollection();
            CustomerTypeList = (LookupManager.GetCustomerTypes())?.ToObservableCollection();
        }
        #endregion

        #region "Setup Customer"
        private void SetupCustomer(CustomerModel customer)
        {
            if (customer == null)
            {
                IsNewCustomer = true;
                Customer = new CustomerModel();
                Customer.CustomerTypeId = CustomerTypeList.Where(x => x.Description == ApplicationConstants.CustomerTypeProspectiveTenantTitle)
                                                          .Select(y => y.Id)
                                                          .FirstOrDefault();
                Title = ApplicationConstants.AddCustomerTitle;
            }
            else
            {
                Customer = customer;
                IsNewCustomer = false;
                Title = ApplicationConstants.EditCustomerTitle;
                InternetAccessList = PopulateObservableCollection(InternetAccessList, Customer.InternetAccess);
                SpecialRequirementList = PopulateObservableCollection(SpecialRequirementList, Customer.Communication);
            }

            SetCustomerAddresses();
            CustomerCurrentHome = Customer.CurrentHome == null ? new CurrentHomeModel() : Customer.CurrentHome;
            CustomerImage = Customer.CustomerImage == null ? new CustomerImageModel() : Customer.CustomerImage;

        }
        #endregion

        #region "Populate Observable Collection"

        private ObservableCollection<LookupModel> PopulateObservableCollection(ObservableCollection<LookupModel> collection, string selectedString)
        {
            if (string.IsNullOrEmpty(selectedString))
            {
                return collection;
            }

            int intItem = 0;
            var intList = selectedString.Split(',')
                                .Select(m => { int.TryParse(m, out intItem); return intItem; })
                                .Where(m => m != 0)
                                .ToList();

            foreach (var item in collection)
            {
                if (intList.Contains(item.Id))
                {
                    item.IsSelected = true;
                }
            }

            return collection;
        }

        #endregion

        #region "Is Valid"
        private ResultModel<bool> IsValid()
        {
            if (CustomerAddress != null)
            {
                CustomerAddress.Telephone = string.IsNullOrWhiteSpace(CustomerAddress.Telephone) ? null : CustomerAddress.Telephone;
                CustomerAddress.Mobile = string.IsNullOrWhiteSpace(CustomerAddress.Mobile) ? null : CustomerAddress.Mobile;
                CustomerAddress.Email = string.IsNullOrWhiteSpace(CustomerAddress.Email) ? null : CustomerAddress.Email;
            }
            ResultModel<bool> result = new ResultModel<bool>();

            ValidationResult customerResult = Customer.ValidateAll();
            ValidationResult currentHomeResult = CustomerCurrentHome.ValidateAll();
            ValidationResult addressResult = CustomerAddress.ValidateAll();
            

            result.Message = customerResult.Combine(currentHomeResult).Combine(addressResult).ToString();
            result.Success = customerResult.IsValid && currentHomeResult.IsValid && addressResult.IsValid;

            return result;
        }
        #endregion

        #region "Get customer address"
        private List<CustomerAddressModel> GetCustomerAddress()
        {

            if (CustomerAddressList == null)
            {
                CustomerAddressList = new List<CustomerAddressModel>();
            }

            var adrs = CustomerAddressList.Where(x => x.Id == CustomerAddress.Id).FirstOrDefault();
            if (adrs != null)
            {
                adrs = CustomerAddress;
            }
            else
            {
                var adrsTypeId = AddressTypeList.Where(x => x.Description == "Current")
                                                .Select(y => y.Id)
                                                .FirstOrDefault();
                if (adrsTypeId > 0)
                {
                    CustomerAddress.AddressTypeId = adrsTypeId;
                }
                CustomerAddress.IsDefault = 0;

                CustomerAddressList.Add(CustomerAddress);
            }

            return CustomerAddressList;

        }
        #endregion

        #region "Setup customer address"
        private List<CustomerAddressModel> SetCustomerAddresses()
        {
            CustomerAddressList = Customer.CustomerAddresses == null ? new List<CustomerAddressModel>() : Customer.CustomerAddresses;

            var adrsTypeId = AddressTypeList.Where(x => x.Description == "Current")
                                                .Select(y => y.Id)
                                                .FirstOrDefault();

           var  adrs =  CustomerAddressList.Where(x => x.AddressTypeId == adrsTypeId).FirstOrDefault();
            if (adrs == null)
            {
                CustomerAddress = new CustomerAddressModel();
            }
            else
            {
                CustomerAddress = adrs;
            }

            return CustomerAddressList;

        }
        #endregion

        #region "Get Current Home"
        private CurrentHomeModel GetCurrentHomeModel()
        {
            if (CustomerCurrentHome.Floor == null
                && CustomerCurrentHome.HouseTypeId == null
                && (string.IsNullOrEmpty(CustomerCurrentHome.LandlordName) || string.IsNullOrWhiteSpace(CustomerCurrentHome.LandlordName))
                && CustomerCurrentHome.LandlordTypeId == null
                && CustomerCurrentHome.LivingWithFamilyId == null
                && CustomerCurrentHome.NoOfBedrooms == null
                && (string.IsNullOrEmpty(CustomerCurrentHome.PreviousLandlordName) || string.IsNullOrWhiteSpace(CustomerCurrentHome.PreviousLandlordName))
                && CustomerCurrentHome.TenancyEndDate == null
                && CustomerCurrentHome.TenancyStartDate == null)
            {
                return null;
            }
            else
            {
                return CustomerCurrentHome;
            }
        }
        #endregion

        #region "Get Customer Image"
        private CustomerImageModel GetCustomerImageModel()
        {
            if (CustomerImage == null)
                return null;

            if (CustomerImage.ImageName == null)
                return null;

            return CustomerImage;
        }
        #endregion

        #endregion
    }
}
