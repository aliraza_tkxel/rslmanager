﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.BusinessLayer;
using FrontlineApp.Utility.Helper.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace FrontlineApp.UWP.ViewModels
{
   public class CustomerRisksViewModel : BaseViewModel
    {
        #region "Properties"
        public CustomerManager CustomerManager { get; set; }

        private CustomerModel selectedCustomer;
        public CustomerModel SelectedCustomer
        {
            get { return selectedCustomer; }
            set { Set(() => SelectedCustomer, ref selectedCustomer, value); }
        }
    
        private bool isInternetAvailable;
        public bool IsInternetAvailable
        {
            get { return isInternetAvailable; }
            set { Set(() => IsInternetAvailable, ref isInternetAvailable, value); }
        }

        private bool noRecordFound;
        public bool NoRecordFound
        {
            get { return noRecordFound; }
            set { Set(() => NoRecordFound, ref noRecordFound, value); }
        }


        private ObservableCollection<CustomerRiskModel> customerRiskList;
        public ObservableCollection<CustomerRiskModel> CustomerRiskList
        {
            get { return customerRiskList; }
            set
            {
                Set(() => CustomerRiskList, ref customerRiskList, value);                
            }
        }

        #endregion

        #region "Constructor"
        public CustomerRisksViewModel()
        {
            CustomerManager = new CustomerManager();
        }
        #endregion

        #region "Commands"
        #endregion

        #region "Methods"

        #region "Fetch customer risks from server"

        public async Task<ResultModel<List<CustomerRiskModel>>> FetchCustomerRisksFromServer()
        {
            var result = new ResultModel<List<CustomerRiskModel>>();
            try
            {
                var serviceResult = await CustomerManager.GetCustomerRisks(SelectedCustomer);

                if (serviceResult.Success == true)
                {
                    var riskList = new ObservableCollection<CustomerRiskModel>();

                    if (serviceResult.Value != null)
                    {
                        riskList = serviceResult.Value.ToObservableCollection();
                    }

                    if (riskList.Count() == 0)
                    {
                        NoRecordFound = true;
                    }
                    else
                    {
                        NoRecordFound = false;
                        CustomerRiskList = riskList;
                    }
                    result.Success = true;
                }
                else
                {
                    result.Success = false;
                }

            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.Success = false;
            }

            return result;

        }
        #endregion

        #region "Reset risks list"
        public void ResetList()
        {
            CustomerRiskList = new ObservableCollection<CustomerRiskModel>();
        }
        #endregion

        #endregion
    }
}
