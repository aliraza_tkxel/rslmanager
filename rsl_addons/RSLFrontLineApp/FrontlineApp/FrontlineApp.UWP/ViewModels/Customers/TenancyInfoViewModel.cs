﻿using System.Collections.ObjectModel;
using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.BusinessLayer;
using FrontlineApp.Utility.Helper.Extensions;
using System.Linq;
using MvvmValidation;

namespace FrontlineApp.UWP.ViewModels
{
    public class TenancyInfoViewModel : BaseViewModel
    {
        
        #region "Properties"
        private ObservableCollection<LookupModel> _economicStatusList;
        public ObservableCollection<LookupModel> EconomicStatusList
        {
            get { return _economicStatusList; }
            set { Set(() => EconomicStatusList, ref _economicStatusList, value); }
        }
        private ObservableCollection<LookupModel> _benefitList;
        public ObservableCollection<LookupModel> BenefitsList
        {
            get { return _benefitList; }
            set { Set(() => BenefitsList, ref _benefitList, value); }
        }
        private ObservableCollection<LookupModel> _bankingFacilityList;
        public ObservableCollection<LookupModel> BankingFacilityList
        {
            get { return _bankingFacilityList; }
            set { Set(() => BankingFacilityList, ref _bankingFacilityList, value); }
        }

        public ObservableCollection<LookupModel> YesNoList { get; set; }
        public ObservableCollection<LookupModel> YesNoUnknownList { get; set; }

        private ObservableCollection<LookupModel> _healthProblemList;
        public ObservableCollection<LookupModel> HealthProblemList
        {
            get { return _healthProblemList; }
            set { Set(() => HealthProblemList, ref _healthProblemList, value); }
        }
        private ObservableCollection<LookupModel> _agencySupportList;
        public ObservableCollection<LookupModel> AgencySupportList
        {
            get { return _agencySupportList; }
            set { Set(() => AgencySupportList, ref _agencySupportList, value); }
        }

        private ObservableCollection<LookupModel> _aidsAndAdaptationList;
        public ObservableCollection<LookupModel> AidsAndAdaptationList
        {
            get { return _aidsAndAdaptationList; }
            set { Set(() => AidsAndAdaptationList, ref _aidsAndAdaptationList, value); }
        }

        public LookupManager LookupManager { get; set; }
        public CustomerManager CustomerManager { get; set; }

        private EmploymentInfoModel _employmentInfo;
        public EmploymentInfoModel EmploymentInfo
        {
            get { return _employmentInfo; }
            set { Set(() => EmploymentInfo, ref _employmentInfo, value); }
        }
        private CustomerGeneralInfoModel _customerGeneralInfo;
        public CustomerGeneralInfoModel CustomerGeneralInfo
        {
            get { return _customerGeneralInfo; }
            set { Set(() => CustomerGeneralInfo, ref _customerGeneralInfo, value);}
        }
        private TenancyEmergencyContactModel _emergencyContact;
        public TenancyEmergencyContactModel EmergencyContact
        {
            get { return _emergencyContact; }
            set { Set(() => EmergencyContact, ref _emergencyContact, value); }
        }
        #endregion

        #region "Constructor"
        public TenancyInfoViewModel()
        {
            LookupManager = new LookupManager();
            CustomerManager = new CustomerManager();
            EmploymentInfo = new EmploymentInfoModel();
            EmergencyContact = new TenancyEmergencyContactModel();
            CustomerGeneralInfo = new CustomerGeneralInfoModel();
            InitializeData();
        }
        
        #endregion
        #region "SAFE"
        public TenancyEmergencyContactModel GetSafeEmergencyContact()
        {
            if (EmergencyContact == null)
            {
                EmergencyContact = new TenancyEmergencyContactModel();
            }
            if (EmergencyContact.CompleteAddress==null && EmergencyContact.ContactName==null 
                && EmergencyContact.Telephone==null && EmergencyContact.Mobile==null
                && EmergencyContact.Email == null)
            {
                return null;
            }
            return EmergencyContact;
        }
        public EmploymentInfoModel GetSafeEmploymentInfo()
        {
            if (EmploymentInfo == null)
            {
                EmploymentInfo = new EmploymentInfoModel();
            }
            if (EmploymentInfo.EmploymentStatusId == null && EmploymentInfo.Occupation == null
                && EmploymentInfo.EmployerName == null && EmploymentInfo.EmployerAddress == null)
            {
                return null;
            }
            return EmploymentInfo;
        }
        public CustomerGeneralInfoModel GetSafeGeneralInfo()
        {
            if(CustomerGeneralInfo == null)
            {
                CustomerGeneralInfo = new CustomerGeneralInfoModel();
            }
            if (CustomerGeneralInfo.Benefits == null && CustomerGeneralInfo.BankingFacilities == null
                && CustomerGeneralInfo.BankingFacilityOther == null && CustomerGeneralInfo.HealthProblems == null
                && CustomerGeneralInfo.HealthProblemOthers == null && CustomerGeneralInfo.LiveInCarer == null
                 && CustomerGeneralInfo.Wheelchair == null && CustomerGeneralInfo.SupportFromAgencies == null
                  && CustomerGeneralInfo.SupportFromAgenciesOther == null && CustomerGeneralInfo.AidsAndAdaptation == null)
            {
                return null;
            }
            return CustomerGeneralInfo;
        }
        #endregion
        #region "Is Valid"
        public ResultModel<bool> IsValid()
        {
            ResultModel<bool> result = new ResultModel<bool>();
            result.Value = true;
            result.Message = "";

            ValidationResult emConRes = EmergencyContact.ValidateAll();
            result.Value = emConRes.IsValid;
            if (result.Value == false)
            {
                result.Message = emConRes.ToString();
                return result;
            }
            
            ValidationResult EmpInfoRes = EmploymentInfo.ValidateAll();
            result.Value = EmpInfoRes.IsValid;
            if (result.Value == false)
            {
                result.Message = EmpInfoRes.ToString();
                return result;
            }
            
            ResultModel<bool> CustInfoRes = ValidateGeneralInfo();
            result.Value = CustInfoRes.Value;
            if (result.Value == false)
            {
                result.Message = CustInfoRes.Message;
                return result;
            }
            

            return result;
        }

        public ResultModel<bool> ValidateGeneralInfo()
        {
            ResultModel<bool> result = new ResultModel<bool>();
            result.Value = true;
            result.Message = "";
            SetSelectedItems();
            if (CustomerGeneralInfo.HasHealthProblems )
            {
                if (string.IsNullOrEmpty(CustomerGeneralInfo.HealthProblems) && string.IsNullOrWhiteSpace(CustomerGeneralInfo.HealthProblemOthers))
                {
                    result.Value =  false;
                    result.Message =result.Message+"\r"+ "Please select health problems";
                }
            }
            if (CustomerGeneralInfo.HasSupportFromAgencies)
            {
                if (string.IsNullOrEmpty(CustomerGeneralInfo.SupportFromAgencies) && string.IsNullOrWhiteSpace(CustomerGeneralInfo.SupportFromAgenciesOther))
                {
                    result.Value = false;
                    result.Message = result.Message + "\r" + "Please select the agencies which support you";
                }
            }
            
            return result;
        }

        #endregion
        #region "Commands"
        #endregion

        #region "Methods"
        public void SetSelectedItems()
        {
            if (CustomerGeneralInfo == null)
            {
                CustomerGeneralInfo = new CustomerGeneralInfoModel();
            }
            CustomerGeneralInfo.AidsAndAdaptation = LookupManager.GetSelectedItems(AidsAndAdaptationList);
            CustomerGeneralInfo.Benefits = LookupManager.GetSelectedItems(BenefitsList);
            CustomerGeneralInfo.BankingFacilities = LookupManager.GetSelectedItems(BankingFacilityList);
            CustomerGeneralInfo.HealthProblems = LookupManager.GetSelectedItems(HealthProblemList);
            CustomerGeneralInfo.SupportFromAgencies = LookupManager.GetSelectedItems(AgencySupportList);
        }
        #region "Initialize Data"
        private void InitializeData()
        {
            
            PopulateLookups();
        }
        #endregion

        #region "Populate lookups"
        public void PopulateLookups()
        {
            if (CustomerGeneralInfo == null)
            {
                CustomerGeneralInfo = new CustomerGeneralInfoModel();
            }
            EconomicStatusList = (LookupManager.GetEmploymentStatus())?.ToObservableCollection();
            
            YesNoList = (LookupManager.GetYesNo())?.ToObservableCollection();
            YesNoUnknownList = (LookupManager.GetYesNoUnknown())?.ToObservableCollection();
            BenefitsList = PopulateObservableCollection((LookupManager.GetBenefits())?.ToObservableCollection(), CustomerGeneralInfo.Benefits);
            BankingFacilityList = PopulateObservableCollection((LookupManager.GetBankingFacilities())?.ToObservableCollection(), CustomerGeneralInfo.BankingFacilities);
            AidsAndAdaptationList = PopulateObservableCollection((LookupManager.GetAidsAndAdaption())?.ToObservableCollection(),CustomerGeneralInfo.AidsAndAdaptation);
            if (!string.IsNullOrWhiteSpace(CustomerGeneralInfo.SupportFromAgencies)|| !string.IsNullOrWhiteSpace(CustomerGeneralInfo.SupportFromAgenciesOther))
            {
                CustomerGeneralInfo.HasSupportFromAgencies = true;
            }
            AgencySupportList = PopulateObservableCollection((LookupManager.GetSupportFromAgencies())?.ToObservableCollection(), CustomerGeneralInfo.SupportFromAgencies);
            if ( !string.IsNullOrWhiteSpace(CustomerGeneralInfo.HealthProblems) || !string.IsNullOrWhiteSpace(CustomerGeneralInfo.HealthProblemOthers))
            {
                CustomerGeneralInfo.HasHealthProblems = true;
            }
            HealthProblemList = PopulateObservableCollection((LookupManager.GetHealthProblems())?.ToObservableCollection(), CustomerGeneralInfo.HealthProblems);
        }
        #endregion

        #region "Populate Observable Collection"

        private ObservableCollection<LookupModel> PopulateObservableCollection(ObservableCollection<LookupModel> collection, string selectedString)
        {
            if (string.IsNullOrEmpty(selectedString))
            {
                return collection;
            }

            int intItem = 0;
            var intList = selectedString.Split(',')
                                .Select(m => { int.TryParse(m, out intItem); return intItem; })
                                .Where(m => m != 0)
                                .ToList();

            foreach (LookupModel item in collection)
            {
                if (intList.Contains(item.Id))
                {
                    item.IsSelected = true;
                }
            }

            return collection;
        }

        #endregion

        #endregion
    }
}
