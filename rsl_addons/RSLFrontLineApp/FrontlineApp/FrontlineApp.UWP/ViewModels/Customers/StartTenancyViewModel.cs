﻿using System;
using System.Collections.ObjectModel;
using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.BusinessLayer;
using FrontlineApp.Utility.Helper.Extensions;
using System.Threading.Tasks;
using FrontlineApp.ServiceLayer.Services;
using FrontlineApp.UWP.Popups;

namespace FrontlineApp.UWP.ViewModels
{
    public class StartTenancyViewModel : BaseViewModel
    {
        
        #region "Properties"
        public CustomerService CustomerServiceObj { get; set; }
        public ObservableCollection<LookupModel> YesNoList { get; set; }
        public ObservableCollection<LookupModel> EvictionReasonList { get; set; }

        public LookupManager LookupManager { get; set; }
        public CustomerManager CustomerManager { get; set; }

        private bool willAddPaymentDetails;
        public bool WillAddPaymentDetails
        {
            get { return willAddPaymentDetails; }
            set { Set(() => WillAddPaymentDetails, ref willAddPaymentDetails, value); }
        }

        private DateTime? tenancyStartDate;
        public DateTime? TenancyStartDate
        {
            get { return tenancyStartDate; }
            set { Set(() => TenancyStartDate, ref tenancyStartDate, value); }
        }
        private DateTimeOffset? tenancyStartDateOffset;
        public DateTimeOffset? TenancyStartDateOffset
        {
            get { return tenancyStartDateOffset; }
            set { Set(() => TenancyStartDateOffset, ref tenancyStartDateOffset, value); }
        }
        private bool isReletRent;
        public bool IsReletRent
        {
            get { return isReletRent; }
            set { Set(() => IsReletRent, ref isReletRent, value); }
        }

        private PropertyRentModel tenancyRent;
        public PropertyRentModel TenancyRent
        {
            get { return tenancyRent; }
            set { Set(() => TenancyRent, ref tenancyRent, value); }
        }

        private TenantOnlineSetupModel tenantOnlineSetup;
        public TenantOnlineSetupModel TenantOnlineSetup
        {
            get { return tenantOnlineSetup; }
            set { Set(() => TenantOnlineSetup, ref tenantOnlineSetup, value); }
        }

        private TenancyDisclosureModel tenancyDisclosure;
        public TenancyDisclosureModel TenancyDisclosure
        {
            get { return tenancyDisclosure; }
            set { Set(() => TenancyDisclosure, ref tenancyDisclosure, value); }
        }

        private PropertyModel _propertySel;
        public PropertyModel SelectedProperty
        {
            get { return _propertySel; }
            set { Set(() => SelectedProperty, ref _propertySel, value); }
        }
        #endregion

        #region "Constructor"
        public StartTenancyViewModel()
        {
            
            InitializeData();
        }
        #endregion

        #region "SAFE"
        public PropertyRentModel GetSafeRentModel()
        {
            if(TenancyRent==null)
            {
                return null;
            }
            if (TenancyRent.Rent == null && TenancyRent.RentPayable == null && TenancyRent.Services == null
                && TenancyRent.CouncilTax == null && TenancyRent.WaterRates == null && TenancyRent.IneligServ == null
                && TenancyRent.SupportedServices == null && TenancyRent.Garage == null)
            {
                return null;
            }
            return TenancyRent;
        }
        public TenantOnlineSetupModel GetSafeOnlineSetUp()
        {
            if (TenantOnlineSetup.Password == null)
            {
                return null;
            }
            return TenantOnlineSetup;
        }
        public TenancyDisclosureModel GetSafeDisclosure()
        {
            if (TenancyDisclosure.DisclosureId == null && TenancyDisclosure.IsEvicted == null
                && TenancyDisclosure.EvictedReason == null && TenancyDisclosure.IsSubjectToDebt == null
                && TenancyDisclosure.IsUnsatisfiedCcj == null && TenancyDisclosure.HasCriminalOffense == null
                && TenancyDisclosure.TenancyId == null)
            {
                return null;
            }
            return TenancyDisclosure;
        }
        #endregion

        #region "Methods"
        
        #region "Initialize Data"
        private void InitializeData()
        {
            LookupManager = new LookupManager();
            CustomerManager = new CustomerManager();
            CustomerServiceObj = new CustomerService();
            TenancyDisclosure = new TenancyDisclosureModel();
            TenantOnlineSetup = new TenantOnlineSetupModel();
            TenancyRent = new PropertyRentModel();
            PopulateLookups();
            
        }
       
        #endregion

        #region "Populate lookups"
        private void PopulateLookups()
        {
            YesNoList = (LookupManager.GetYesNo())?.ToObservableCollection();
            EvictionReasonList = (LookupManager.GetEvictionReason())?.ToObservableCollection();
        }
        #endregion
        public async void  GetRentInfo()
        {
            await GetPropertyRentInfo();
        }
        public async Task<ResultModel<PropertyRentModel>> GetPropertyRentInfo()
        {
            ResultModel<PropertyRentModel> res = new ResultModel<PropertyRentModel>();
            if (SelectedProperty != null)
            {
                BusyIndicator serverIndicator = BusyIndicator.Start("Fetching Rent data.....");
                res = await CustomerServiceObj.GetPropertyFinancialInfoForId(SelectedProperty.propertyId);
                if (res.Success)
                {
                    TenancyRent = res.Value;
                }
                else
                {
                    showMessage("Error", res.Message);
                }
                serverIndicator.Close();
            }
            else
            {
                showMessage("Error", "Failed to access the selected Property");
            }
           
            return res;
        }
        public void GetSelectedValues()
        {
            TenancyDisclosure.EvictedReason = LookupManager.GetSelectedItems(EvictionReasonList);

        }
        public ResultModel<bool> IsValid()
        {
            ResultModel<bool> result = new ResultModel<bool>();
            result.Success = true;
            GetSelectedValues();
            result.Value = (TenancyStartDateOffset == null) ? false : true;
            if (TenancyStartDateOffset != null)
            {
                TenancyStartDate = TenancyStartDateOffset.Value.DateTime;
            }
            result.Message = "Please select Tenancy Start date";
            return result;
        }
        #endregion
    }
}
