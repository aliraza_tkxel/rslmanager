﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.BusinessLayer;
using FrontlineApp.DataLayer.OfflineTenancyEntities;
using FrontlineApp.ServiceLayer.ServiceObjects;
using FrontlineApp.ServiceLayer.Services;
using FrontlineApp.Utility.Constants;
using FrontlineApp.UWP.Popups;
using FrontlineApp.UWP.ViewModels.Messages;
using GalaSoft.MvvmLight.Messaging;
using System.Collections.Generic;
using System.Threading.Tasks;
using Windows.UI.Popups;
using System;

namespace FrontlineApp.UWP.ViewModels
{
    public enum TenancySource
    {
        Property,
        Customer
    }

    public class SetupAndAddTenancyViewModel: BaseViewModel
    {
      
        #region "Properties"
        public OfflineTenancyModel OfflineModelObj { get; set; }
        public CustomerService CustomerServiceObj { get; set; }
        public TenancyOfflineManager TenancyOfflineMgr { get; set; }
        public TenancyModel TenancyObj { get; set; }
        private PropertyModel _propertySel;
        public PropertyModel SelectedProperty
        {
            get { return _propertySel; }
            set { Set(() => SelectedProperty, ref _propertySel, value); TenancyObj.PropertyId = SelectedProperty.propertyId; }
        }

        private CustomerModel _selectedCustomer;
        public CustomerModel SelectedCustomer
        {
            get { return _selectedCustomer; }
            set
            { //Set(() => SelectedCustomer, ref _selectedCustomer, value); //TODO: Fix Crash here
                _selectedCustomer = value;
                TenancyObj.CustomerId = SelectedCustomer.CustomerId;
            }
        }

        #endregion

        #region "Properties"

        private bool isEditingExistingRecord;
        public bool IsEditingExistingRecord
        {
            get { return isEditingExistingRecord; }
            set { Set(() => IsEditingExistingRecord, ref isEditingExistingRecord, value); }

        }

        private bool willAddPaymentDetails;
        public bool WillAddPaymentDetails
        {
            get { return willAddPaymentDetails; }
            set { Set(() => WillAddPaymentDetails, ref willAddPaymentDetails, value); }
        }
        public string TempHeader { get; set; } /*TODO: To Be Removed Later on*/
        public TenancySource TenancySource { get; set; }
        
        
        private List<PivotTitleModel> _pivotItemList;
        public List<PivotTitleModel> PivotItemList
        {
            get { return _pivotItemList; }
            set { Set(() => PivotItemList, ref _pivotItemList, value); }
        }

        
        #endregion
       
        #region "Constructor"
        public SetupAndAddTenancyViewModel()
        {
            RegisterMessages();
        }
        public void ClearData()
        {
            TenancyObj = new TenancyModel();
            TenancyObj.TenancyId = 0;
            SelectedProperty = new PropertyModel();
            SelectedCustomer = new CustomerModel();
            TempHeader = "";
        }
        public void InitializeData()
        {
            CustomerServiceObj = new CustomerService();
            TenancyOfflineMgr = new TenancyOfflineManager();
            TenancyObj = new TenancyModel();
            TenancyObj.TenancyId = 0;
            SelectedProperty = new PropertyModel();
            SelectedCustomer = new CustomerModel();
            PivotItemList = new List<PivotTitleModel>();
            WillAddPaymentDetails = false;
            
        }
        #endregion
        
        #region "UI Methods"
        public void InitModule(PropertyModel PropertyObj, CustomerModel customer)
        {
            InitializeData();
            if (TenancySource == TenancySource.Property)
            {
                PivotItemList.Add(new PivotTitleModel("Customer", 0,0));
                SelectedProperty = PropertyObj;
                SelectedCustomer = new CustomerModel();
                TempHeader = "Customer";
            }
            else
            {
                PivotItemList.Add(new PivotTitleModel("Property", 0,0));
                SelectedProperty = new PropertyModel();
                SelectedCustomer = customer;
                TempHeader = "Property";
            }
            PivotItemList.Add(new PivotTitleModel("Tenancy Info.", 1,0));
            PivotItemList.Add(new PivotTitleModel("Joint Tenant", 2,0));
            PivotItemList.Add(new PivotTitleModel("Add Occupants", 3,0));
            PivotItemList.Add(new PivotTitleModel("Referrals", 4,0));
            PivotItemList.Add(new PivotTitleModel("Start Tenancy", 5,0));
        }
        public void ResetImagePath(int selectedIndex)
        {
            List<PivotTitleModel> model = new List<PivotTitleModel>();
            foreach(PivotTitleModel mod in PivotItemList)
            {
                mod.SelectedIndex = selectedIndex;
                model.Add(mod);
            }
            PivotItemList = model;
        }
        #endregion
        #region "Network Operations"
        
        public async Task<ResultModel<TenancySO>> CreateTenancy()
        {
            BusyIndicator serverIndicator = BusyIndicator.Start("Starting new tenancy");
            ResultModel<TenancySO> res = await CustomerServiceObj.CreateTenancy(TenancyObj);
            if (res.Success)
            {
                showMessage("Success", "Tenancy Created Successfully");
            }
            else
            {
                showMessage("Failure", res.Message);
            }
            serverIndicator.Close();
            return res;
        }
        
        public async Task<ResultModel<CustomerModel>> GetCustomerDetailsFor(CustomerModel selCust)
        {
            var customerSo = CustomerServiceObj.ConvertModelToSOForTenancy(selCust);
            BusyIndicator serverIndicator = BusyIndicator.Start(MessageConstants.FetchingCustomerDetailsFromServer);
            ResultModel<CustomerModel> result = await CustomerServiceObj.GetCustomerDetailLookUp(selCust.CustomerId, customerSo);

            serverIndicator.Close();
            return result;
        }
        #endregion

        #region "Messages"
        private void RegisterMessages()
        {
            Messenger.Default.Register<ArrangeTenancyMessage>(this, (message) =>
            {
                string source = message.source;
                if (string.Equals(source, "CustomerTenancy"))
                {
                    TenancySource = TenancySource.Customer;
                    CustomerModel model = message.CustomerModel;
                    InitModule( null, model);

                }
                else if (string.Equals(source, "PropertyTenancy"))
                {
                    TenancySource = TenancySource.Property;
                    PropertyDetailsModel model = message.PropertyModel;
                    PropertyModel PropMod = new PropertyModel();
                    PropMod.propertyId = model.propertyId;
                    PropMod.propertyTypeId = model.propertyTypeId;
                    PropMod.propertyType = model.propertyTypeDescription;
                    PropMod.address = model.address;
                    PropMod.projectedRent = model.rent;
                    InitModule( PropMod, null);
                }
            });
        }
        public void SendTenancyMessages()
        {
            if (TenancySource == TenancySource.Property)
            {
                Messenger.Default.Send(new RefreshAfterTenancyMessage() { IsForProperty = true });
            }
            
            
        }
        public void ClearAllData()
        {
            if (PivotItemList != null)
            {
                ResetImagePath(0);
            }
            
            ClearData();
            
        }

        #endregion

        #region "Offline Saving & Fetching"
        public void SaveTenancyDataForStep(int currentStepIndex)
        {
            BusyIndicator serverIndicator = BusyIndicator.Start(MessageConstants.SavingOfflineTenancyData);
            TenancyRootEnt root = new TenancyRootEnt();

            root.TenancyId = 0;
            if (SelectedCustomer != null)
            {
                root.CustomerId = SelectedCustomer.CustomerId;
            }
            if (SelectedProperty != null)
            {
                root.PropertyId = SelectedProperty.propertyId;
            }
            
            root.TenancyStartDate = TenancyObj.TenancyStartDate;
            root.TenancyEndDate = TenancyObj.TenancyEndDate;
            root.PausedOnStepId = currentStepIndex;

            ResultModel<bool> res  = TenancyOfflineMgr.SaveTenancyOfflineEntity(TenancyObj, root, SelectedProperty, SelectedCustomer);
            if (res.Success)
            {
                showMessage("Success", "Tenancy saved Successfully");
            }
            else
            {
                showMessage("Failure", res.Message);
            }
            serverIndicator.Close();

        }

        public void DeleteTenancyRecord()
        {
            TenancyOfflineMgr.DeleteTenancyDataFor(SelectedCustomer.CustomerId, SelectedProperty.propertyId);
        }

        
        #endregion
    }
}