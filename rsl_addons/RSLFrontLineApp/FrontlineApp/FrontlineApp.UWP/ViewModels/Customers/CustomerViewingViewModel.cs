﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.BusinessLayer;
using FrontlineApp.Utility;
using FrontlineApp.Utility.Constants;
using FrontlineApp.Utility.Helper.Extensions;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace FrontlineApp.UWP.ViewModels
{
    public class CustomerViewingViewModel : BaseViewModel
    {
        #region "Properties"
        public CustomerManager CustomerManager { get; set; }


        private CustomerModel selectedCustomer;
        public CustomerModel SelectedCustomer
        {
            get { return selectedCustomer; }
            set { Set(() => SelectedCustomer, ref selectedCustomer, value); }
        }


        private bool isInternetAvailable;
        public bool IsInternetAvailable
        {
            get { return isInternetAvailable; }
            set { Set(() => IsInternetAvailable, ref isInternetAvailable, value); }
        }

        private bool noRecordFound;
        public bool NoRecordFound
        {
            get { return noRecordFound; }
            set { Set(() => NoRecordFound, ref noRecordFound, value); }
        }


        private ObservableCollection<GroupInfoListModel> customerViewingList;
        public ObservableCollection<GroupInfoListModel> CustomerViewingList
        {
            get { return customerViewingList; }
            set
            {
                Set(() => CustomerViewingList, ref customerViewingList, value);
                CollectionViewSource cvs = new CollectionViewSource();
                cvs.IsSourceGrouped = true;
                cvs.Source = customerViewingList;
                ViewingsCVS = cvs;
            }
        }

        private CollectionViewSource viewingsCVS;
        public CollectionViewSource ViewingsCVS
        {
            get { return viewingsCVS; }
            set { Set(() => ViewingsCVS, ref viewingsCVS, value); }
        }

        #endregion

        #region "Constructor"
        public CustomerViewingViewModel()
        {
            CustomerManager = new CustomerManager();
        }
        #endregion

        #region "Commands"
        #endregion

        #region "Methods"

        #region "Fetch customer viewings from server"

        public async Task<ResultModel<List<GroupInfoListModel>>> FetchCustomerViewingsFromServer(CustomerModel customer)
        {
            var result = new ResultModel<List<GroupInfoListModel>>();
            try
            {
               var  serviceResult = await CustomerManager.GetCustomerViewings(customer);

                if (serviceResult.Success == true)
                {
                    var groupedViewingList = new ObservableCollection<GroupInfoListModel>();

                    if (serviceResult.Value != null)
                    {
                        groupedViewingList = serviceResult.Value.ToObservableCollection();
                    }

                    if (groupedViewingList.Count() == 0)
                    {
                        NoRecordFound = true;
                    }
                    else
                    {
                        NoRecordFound = false;
                        CustomerViewingList = groupedViewingList;
                    }
                    result.Success = true;
                }
                else
                {
                    result.Success = false;
                }

            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.Success = false;
            }

            return result;

        }
        #endregion

        #endregion
    }
}
