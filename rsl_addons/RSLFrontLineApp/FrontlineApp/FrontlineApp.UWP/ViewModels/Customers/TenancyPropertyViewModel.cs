﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.BusinessLayer;

namespace FrontlineApp.UWP.ViewModels
{
   public class TenancyPropertyViewModel : BaseViewModel
    {
        #region "Properties"
        public CustomerManager CustomerManager { get; set; }

        private PropertyModel property;
        public PropertyModel SelectedProperty
        {
            get { return property; }
            set
            {
                Set(() => SelectedProperty, ref property, value);
                if (!string.IsNullOrWhiteSpace(SelectedProperty.propertyId))
                {
                    IsPropertySelected = true;
                }
                else
                {
                    IsPropertySelected = false;
                }
            }
        }
        private bool _isPropertySelected;
        public bool IsPropertySelected
        {
            get
            {
                return _isPropertySelected;
            }
            set { Set(() => IsPropertySelected, ref _isPropertySelected, value); }
        }
        #endregion

        #region "Constructor"
        public TenancyPropertyViewModel()
        {
            CustomerManager = new CustomerManager();
            InitializeData();
        }
        #endregion

        #region "Commands"
        #endregion

        #region "Methods"

        #region "Initialize Data"
        private void InitializeData()
        {
            IsPropertySelected = false;
           // SelectedProperty = new PropertyModel();
        }
        
        #endregion

        #endregion
    }
}
