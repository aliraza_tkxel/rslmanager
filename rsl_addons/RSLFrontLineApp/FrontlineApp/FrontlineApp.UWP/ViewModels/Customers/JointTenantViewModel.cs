﻿using System.Collections.ObjectModel;
using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.BusinessLayer;
using FrontlineApp.Utility.Helper.Extensions;
using MvvmValidation;
using System.Linq;
using System;

namespace FrontlineApp.UWP.ViewModels
{
    public class JointTenantViewModel : BaseViewModel
    {
        #region "Properties"
        
        private ObservableCollection<LookupModel> _economicStatusList;
        public ObservableCollection<LookupModel> EconomicStatusList
        {
            get { return _economicStatusList; }
            set { Set(() => EconomicStatusList, ref _economicStatusList, value); }
        }
        private ObservableCollection<LookupModel> _benefitList;
        public ObservableCollection<LookupModel> BenefitsList
        {
            get { return _benefitList; }
            set { Set(() => BenefitsList, ref _benefitList, value); }
        }
        private ObservableCollection<LookupModel> _bankingFacilityList;
        public ObservableCollection<LookupModel> BankingFacilityList
        {
            get { return _bankingFacilityList; }
            set { Set(() => BankingFacilityList, ref _bankingFacilityList, value); }
        }
        public ObservableCollection<LookupModel> YesNoList { get; set; }
        public ObservableCollection<LookupModel> YesNoUnknownList { get; set; }
        private ObservableCollection<LookupModel> _healthProblemList;
        public ObservableCollection<LookupModel> HealthProblemList
        {
            get { return _healthProblemList; }
            set { Set(() => HealthProblemList, ref _healthProblemList, value); }
        }
        private ObservableCollection<LookupModel> _agencySupportList;
        public ObservableCollection<LookupModel> AgencySupportList
        {
            get { return _agencySupportList; }
            set { Set(() => AgencySupportList, ref _agencySupportList, value); }
        }

        private ObservableCollection<LookupModel> _aidsAndAdaptationList;
        public ObservableCollection<LookupModel> AidsAndAdaptationList
        {
            get { return _aidsAndAdaptationList; }
            set { Set(() => AidsAndAdaptationList, ref _aidsAndAdaptationList, value); }
        }

        public LookupManager LookupManager { get; set; }
        public CustomerManager CustomerManager { get; set; }

        private JointTenantModel jointTenant;
        public JointTenantModel JointTenant
        {
            get { return jointTenant; }
            set { Set(() => JointTenant, ref jointTenant, value); }
        }

        private bool isJointTenantSelected;
        public bool IsJointTenantSelected
        {
            get { return isJointTenantSelected; }
            set { Set(() => IsJointTenantSelected, ref isJointTenantSelected, value); }
        }

        public DateTimeOffset? JTDob { get; set; }
        public string JTFirstName { get; set; }
        public string JTLastName { get; set; }
        public string JTTelephone { get; set; }
        private CustomerModel jointTenantGeneralInfo;
        public CustomerModel JointTenantGeneralInfo
        {
            get { return jointTenantGeneralInfo; }
            set
            {
                Set(() => JointTenantGeneralInfo, ref jointTenantGeneralInfo, value);
                JTDob = jointTenantGeneralInfo.Dob;
                JTFirstName = JointTenantGeneralInfo.FirstName;
                JTLastName = JointTenantGeneralInfo.LastName;
                JTTelephone = jointTenantGeneralInfo.Telephone;
                if (jointTenantGeneralInfo != null)
                {
                    if (jointTenantGeneralInfo.CustomerGeneralInfo != null)
                    {
                        JointTenant.CustomerGeneralInfo = JointTenantGeneralInfo.CustomerGeneralInfo;
                    }
                    if (JointTenantGeneralInfo.EmploymentInfo != null)
                    {
                        JointTenant.EmploymentInfo = JointTenantGeneralInfo.EmploymentInfo;
                    }
                    
                    
                    if (JointTenantGeneralInfo.CustomerId != null)
                    {
                        IsJointTenantSelected = true;
                        JointTenant.IsJointTenantSelected = true;
                    }
                }
                
                
            }
        }

        #endregion

        #region "Constructor"
        public JointTenantViewModel()
        {
            LookupManager = new LookupManager();
            CustomerManager = new CustomerManager();
            JointTenant = new JointTenantModel();
            JointTenantGeneralInfo = new CustomerModel();
            InitializeData();
        }
        
        #endregion

        #region "SAFE"
        public JointTenantModel GetSafeJointTenant()
        {
            
            if (JointTenant.TenancyId == null && JointTenant.CustomerId==null
                && JointTenant.GetSafeEmergencyContact() == null && JointTenant.GetSafeEmploymentInfo() == null
                && JointTenant.GetSafeGeneralInfo() == null)
            {
                return null;
            }
            return JointTenant;
        }
        #endregion

        #region "Methods"
        public void SetSelectedItems()
        {
            JointTenant.CustomerGeneralInfo.AidsAndAdaptation = LookupManager.GetSelectedItems(AidsAndAdaptationList);
            JointTenant.CustomerGeneralInfo.Benefits = LookupManager.GetSelectedItems(BenefitsList);
            JointTenant.CustomerGeneralInfo.BankingFacilities = LookupManager.GetSelectedItems(BankingFacilityList);
            JointTenant.CustomerGeneralInfo.HealthProblems = LookupManager.GetSelectedItems(HealthProblemList);
            JointTenant.CustomerGeneralInfo.SupportFromAgencies = LookupManager.GetSelectedItems(AgencySupportList);
        }
        #region "Initialize Data"
        private void InitializeData()
        {
            PopulateLookups();
        }
        #endregion

        #region "Populate lookups"
        public void PopulateLookups()
        {
            EconomicStatusList = (LookupManager.GetEmploymentStatus())?.ToObservableCollection();
            YesNoList = (LookupManager.GetYesNo())?.ToObservableCollection();
            YesNoUnknownList = (LookupManager.GetYesNoUnknown())?.ToObservableCollection();
            BenefitsList = PopulateObservableCollection((LookupManager.GetBenefits())?.ToObservableCollection(), JointTenant.CustomerGeneralInfo.Benefits);
            BankingFacilityList = PopulateObservableCollection((LookupManager.GetBankingFacilities())?.ToObservableCollection(), JointTenant.CustomerGeneralInfo.BankingFacilities);
            AidsAndAdaptationList = PopulateObservableCollection((LookupManager.GetAidsAndAdaption())?.ToObservableCollection(), JointTenant.CustomerGeneralInfo.AidsAndAdaptation);
            AgencySupportList = PopulateObservableCollection((LookupManager.GetSupportFromAgencies())?.ToObservableCollection(), JointTenant.CustomerGeneralInfo.SupportFromAgencies);
            HealthProblemList = PopulateObservableCollection((LookupManager.GetHealthProblems())?.ToObservableCollection(), JointTenant.CustomerGeneralInfo.HealthProblems);
        }
        #endregion
        #region "Populate Observable Collection"

        private ObservableCollection<LookupModel> PopulateObservableCollection(ObservableCollection<LookupModel> collection, string selectedString)
        {
            if (string.IsNullOrEmpty(selectedString))
            {
                return collection;
            }

            int intItem = 0;
            var intList = selectedString.Split(',')
                                .Select(m => { int.TryParse(m, out intItem); return intItem; })
                                .Where(m => m != 0)
                                .ToList();

            foreach (LookupModel item in collection)
            {
                if (intList.Contains(item.Id))
                {
                    item.IsSelected = true;
                }
            }

            return collection;
        }
        #endregion
        #region "Is Valid"
        public ResultModel<bool> IsValid()
        {
            SetSelectedItems();
            ResultModel<bool> result = new ResultModel<bool>();
            result.Message = "";

            ValidationResult emplValRes = JointTenant.ValidateAll();
            result.Value = emplValRes.IsValid;
            result.Message = emplValRes.ToString();

            return result;
        }
        #endregion



        #endregion
        
    }
}
