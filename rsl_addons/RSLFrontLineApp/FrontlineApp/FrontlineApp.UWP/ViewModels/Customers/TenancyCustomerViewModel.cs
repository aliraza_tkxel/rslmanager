﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.BusinessLayer;

namespace FrontlineApp.UWP.ViewModels
{
    public class TenancyCustomerViewModel : BaseViewModel
    {
        #region "Properties"
        public CustomerManager CustomerManager { get; set; }

        private CustomerModel customer;
        public CustomerModel SelectedCustomerObj
        {
            get { return customer; }
            set
            {
                Set(() => SelectedCustomerObj, ref customer, value); 
                if (SelectedCustomerObj.CustomerId!=null)
                {
                    IsCustomerSelected = true;
                }
                else
                {
                    IsCustomerSelected = false;
                }
                
            }
        }
        private bool _isCustomerSelected;
        public bool IsCustomerSelected
        {
            get
            {
                return _isCustomerSelected;
            }
            set { Set(() => IsCustomerSelected, ref _isCustomerSelected, value); }
        }
        #endregion
        #region "Constructor"
        public TenancyCustomerViewModel()
        {
            
            InitializeData();
        }
        #endregion

        #region "Commands"
        #endregion

        #region "Methods"

        #region "Initialize Data"
        private void InitializeData()
        {
            SelectedCustomerObj = new CustomerModel();
            CustomerManager = new CustomerManager();
            IsCustomerSelected = false;
        }
        
        #endregion

        #endregion
    }
}
