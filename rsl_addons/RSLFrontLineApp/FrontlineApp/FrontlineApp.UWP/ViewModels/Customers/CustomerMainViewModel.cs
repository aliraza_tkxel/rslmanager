﻿using System;
using System.Linq;
using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.BusinessLayer;
using FrontlineApp.Utility.Constants;
using Windows.UI.Xaml.Data;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using FrontlineApp.UWP.ViewModels.Messages;
using System.Collections.ObjectModel;
using FrontlineApp.Utility.Helper.Extensions;

namespace FrontlineApp.UWP.ViewModels
{
    public class CustomerMainViewModel : BaseViewModel
    {
        #region "Properties"

        private ObservableCollection<GroupInfoListModel> customers;
        public ObservableCollection<GroupInfoListModel> Customers
        {
            get { return customers; }
            set
            {
                Set(() => Customers, ref customers, value);
                CollectionViewSource cvs = new CollectionViewSource();
                cvs.IsSourceGrouped = true;
                cvs.Source = customers;
                CustomersCVS = cvs;
            }
        }

        private CollectionViewSource customersCVS;

        public CollectionViewSource CustomersCVS
        {
            get { return customersCVS; }
            set { Set(() => CustomersCVS, ref customersCVS, value); }
        }

        CustomerManager CustomerManager;
        public PaginationModel Pagination { get; set; }

        private string searchText;
        public string SearchText
        {
            get { return searchText; }
            set { Set(() => SearchText, ref searchText, value); }
        }

        private bool noRecordFound;
        public bool NoRecordFound
        {
            get { return noRecordFound; }
            set { Set(() => NoRecordFound, ref noRecordFound, value); }
        }



        #endregion

        #region "Constructor"
        public CustomerMainViewModel()
        {
            Customers = new ObservableCollection<GroupInfoListModel>();
            CustomerManager = new CustomerManager();
            Pagination = new PaginationModel();
            SearchText = String.Empty;
            RegisterMessages();
        }
        #endregion

        #region "Methods"

        #region "Reset customer list"
        public void ResetcustomerList()
        {
            Customers = new ObservableCollection<GroupInfoListModel>();
        }
        #endregion

        #region "Search Customers"
        public async Task<ResultModel<bool>> SearchCustomer()
        {
            var result = new ResultModel<bool>();

            try
            {
                var searchResult = await CustomerManager.SearchCustomer(Pagination, SearchText);

                if (searchResult.Success == true)
                {
                    ObservableCollection<GroupInfoListModel> groupedCustList = searchResult.Value.ToObservableCollection();
                    if (groupedCustList.Count() == 0)
                    {
                        NoRecordFound = true;
                    }
                    else
                    {
                        NoRecordFound = false;
                        Customers = groupedCustList;
                    }
                    result.Success = true;
                }

            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.Success = false;
            }

            return result;

        }
        #endregion

        #region "Fetch records from local db"

        public void FetchRecordsFromLocalDb()
        {
            var result = CustomerManager.FetchCustomerListFromLocalDb(Pagination, SearchText);

            if (result.Success == true)
            {
                ObservableCollection<GroupInfoListModel> groupedCustList = result.Value.ToObservableCollection();
                if (groupedCustList.Count() == 0)
                {
                    NoRecordFound = true;
                    Error = MessageConstants.NoRecordFound;
                }
                else
                {
                    NoRecordFound = false;
                    Customers = groupedCustList;
                }
            }
            else
            {
                Error = result.Message;
            }

            if (!string.IsNullOrEmpty(Error))
            {
                showMessage(ApplicationConstants.ErrorTitle, Error);
            }
        }
        #endregion

        #region "Fetch records from server"

        public async Task<ResultModel<bool>> FetchRecordsFromServer()
        {
            var result = await CustomerManager.FetchCustomerListingFromServer(Pagination, SearchText);

            if (result.Success == true)
            {
                var dbResult = CustomerManager.FetchCustomerListFromLocalDb(Pagination, SearchText);

                if (dbResult.Success == true)
                {
                    ObservableCollection<GroupInfoListModel> groupedCustList = dbResult.Value.ToObservableCollection();
                    if (groupedCustList.Count() == 0)
                    {
                        NoRecordFound = true;
                    }
                    else
                    {
                        NoRecordFound = false;
                        Customers = groupedCustList;
                    }
                }
                else
                {
                    result.Success = false;
                    result.Message = dbResult.Message;
                }
            }

            return result;

        }
        #endregion

        #region "Check Offline Records Exists"
        public bool CheckOfflineRecordsExists()
        {
            bool isOfflineRecordsExist = false;
            isOfflineRecordsExist = CustomerManager.CheckOfflineCustomerRecordsExists();
            return isOfflineRecordsExist;
        }
        #endregion

        #region "Sync Offline Records"
        public async Task<ResultModel<bool>> SyncOfflineRecords()
        {
            return await CustomerManager.SyncOfflineCustomerRecords(); ;
        }
        #endregion

        #region "Register Messages"
        private void RegisterMessages()
        {
            // Refresh list from local db message
            Messenger.Default.Register<RefreshCustomerListMessage>(this, (message) =>
            {
                FetchRecordsFromLocalDb();
            });


            Messenger.Default.Register<CustomerDetailMessage>(this, (message) =>
            {
                UpdateCustomerList(message.OldCustomerDetail, message.UpdatedCustomerDetail);
            });

        }

        #endregion

        #region "Update Customer list"

        private void UpdateCustomerList(CustomerModel oldCustomerDetail, CustomerModel updatedCustomerDetail)
        {

            foreach (var grp in customers)
            {
                foreach (CustomerModel customer in grp)
                {
                    if (customer.Id == oldCustomerDetail.Id)
                    {
                        customer.Id = updatedCustomerDetail.Id;
                        break;
                    }
                }
            }

        }
        #endregion

        #endregion

    }
}
