﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.BusinessLayer;
using FrontlineApp.Utility.Constants;
using FrontlineApp.Utility.Helper.Extensions;
using FrontlineApp.UWP.Popups;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace FrontlineApp.UWP.ViewModels
{
    public class SelectTenancyCustomerViewModel:BaseViewModel
    {
        #region "Properties"
        public PaginationModel Pagination { get; set; }
        private CustomerModel _selectedProperty;
        public CustomerModel SelectedProperty
        {
            get { return _selectedProperty; }
            set { Set(() => SelectedProperty, ref _selectedProperty, value); }
        }
        private ObservableCollection<GroupInfoListModel> _customers;
        public ObservableCollection<GroupInfoListModel> Customers
        {
            get { return _customers; }
            set { Set(() => Customers, ref _customers, value); }
        }
        public CustomerManager CustomerManager { get; set; }
        private CollectionViewSource _viewingCVS;
        public CollectionViewSource ViewingsCVS
        {
            get { return _viewingCVS; }
            set { Set(() => ViewingsCVS, ref _viewingCVS, value); }
        }
        private ICollectionView _groupedView;
        public ICollectionView GroupedView
        {
            get { return _groupedView; }
            set { Set(() => GroupedView, ref _groupedView, value); }
        }
        private string _searchText;
        public string SearchText
        {
            get { return _searchText; }
            set { Set(() => SearchText, ref _searchText, value); }
        }
        #endregion

        #region "Constructor"
        public SelectTenancyCustomerViewModel()
        {
            CustomerManager = new CustomerManager();
            Customers = new ObservableCollection<GroupInfoListModel>();
            SelectedProperty = new CustomerModel();
            ViewingsCVS = new CollectionViewSource();
            Pagination = new PaginationModel();
        }
        
        #endregion
        public async void GetCustomerList()
        {
            BusyIndicator serverIndicator = BusyIndicator.Start(MessageConstants.FetchingCustomerRecordsFromServer);
            try
            {
                // Set VM property; updates View appropriately.
                //Busy = true;
                ViewingsCVS = new CollectionViewSource();
                SelectedProperty = new CustomerModel();
                string safeSearch = string.IsNullOrWhiteSpace(SearchText) ? "" : SearchText;
                ResultModel<List<CustomerModel>> result = await CustomerManager.FetchCustomerLookUpList(Pagination, safeSearch);
                List<GroupInfoListModel> groupedCustList = new List<GroupInfoListModel>();
                if (result.Value == null || result.Value.Count() == 0)
                {
                    // Show Dialog Message
                    showMessage("Alert", "No Customers found");
                }
                else
                {
                    var custList = FilterTenants(result.Value);
                    /*if (!string.IsNullOrWhiteSpace(SearchText))
                    {
                        List<CustomerModel> filteredList = custList.Where(c => (c.FirstName?.IndexOf(SearchText, StringComparison.CurrentCultureIgnoreCase) > -1) 
                                                                            || (c.LastName?.IndexOf(SearchText, StringComparison.CurrentCultureIgnoreCase) > -1))
                                                                            .ToList();
                        custList = filteredList;
                    }*/

                    
                    var groupQuery = from item in custList
                                     group item by item.Initials[0].ToString() into g
                                     orderby g.Key
                                     select new { GroupName = g.Key, Items = g };

                    foreach (var g in groupQuery)
                    {
                        GroupInfoListModel info = new GroupInfoListModel();
                        info.Key = g.GroupName;
                        foreach (var item in g.Items)
                        {
                            info.Add(item);
                        }
                        groupedCustList.Add(info);
                    }

                    Customers = groupedCustList.ToObservableCollection();
                    ConfigureCollectionView();
                }
            }
            catch (Exception e)
            {
                // Error = ex.Message;
                showMessage("Error", e.Message);
            }
            finally
            {
                // Busy = false;
                serverIndicator.Close();
            }

        }
        private List<CustomerModel> FilterTenants(List<CustomerModel> list)
        {
            LookupManager manager = new LookupManager();
            List<LookupModel> customerTypes = manager.GetCustomerTypes();
            LookupModel tenant = customerTypes.Where(ct => ct.Description.Equals(ApplicationConstants.CustomerTypeTenantTitle)).FirstOrDefault();
            var filtered = list.Where(o => o.CustomerTypeId != tenant.Id).ToList();
            return filtered;
        }
        private void ConfigureCollectionView()
        {
            ViewingsCVS.IsSourceGrouped = true;
            ViewingsCVS.Source = Customers;
            GroupedView = ViewingsCVS.View;
        }
    }
}
