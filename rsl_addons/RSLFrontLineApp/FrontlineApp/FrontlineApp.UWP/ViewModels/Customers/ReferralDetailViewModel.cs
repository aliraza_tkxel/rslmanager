﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.BusinessLayer;
using FrontlineApp.Utility;
using FrontlineApp.Utility.Constants;
using System;
using System.Threading.Tasks;

namespace FrontlineApp.UWP.ViewModels
{
    public class ReferralDetailViewModel : BaseViewModel 
    {
        #region "Properties"
        public CustomerManager CustomerManager { get; set; }

        private CustomerModel selectedCustomer;
        public CustomerModel SelectedCustomer
        {
            get { return selectedCustomer; }
            set { Set(() => SelectedCustomer, ref selectedCustomer, value); }
        }

        private ReferralModel customerReferral;
        public ReferralModel CustomerReferral
        {
            get { return customerReferral; }
            set { Set(() => CustomerReferral, ref customerReferral, value); }
        }

        private bool isInternetAvailable;
        public bool IsInternetAvailable
        {
            get { return isInternetAvailable; }
            set { Set(() => IsInternetAvailable, ref isInternetAvailable, value); }
        }

        private bool noRecordFound;
        public bool NoRecordFound
        {
            get { return noRecordFound; }
            set { Set(() => NoRecordFound, ref noRecordFound, value); }
        }

        #endregion

        #region "Constructor"
        public ReferralDetailViewModel()
        {
            CustomerManager = new CustomerManager();
        }
        #endregion

        #region "Commands"
        #endregion

        #region "Methods"

        #region "Fetch customer viewings from server"

        public async Task<ResultModel<CustomerReferral>> GetCustomerReferralFromServer(CustomerModel customer)
        {
            ResetInfo();

            var result = new ResultModel<CustomerReferral>();
            SelectedCustomer = customer;
            try
            {
                var serviceResult = await CustomerManager.GetCustomerReferral(customer);

                if (serviceResult.Success == true)
                {

                    if (serviceResult.Value != null)
                    {
                        NoRecordFound = false;
                        CustomerReferral = serviceResult.Value as ReferralModel;
                        SelectedCustomer.Referral = CustomerReferral;
                    }
                    else
                    {
                        CustomerReferral = new ReferralModel();
                        NoRecordFound = true;
                    }

                    result.Success = true;
                }
                else
                {
                    result.Success = false;
                }

            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.Success = false;
            }

            return result;

        }
        #endregion

        #region "Reset Info"
        private void ResetInfo()
        {
            SelectedCustomer = null;
            CustomerReferral = null;
        }
        #endregion


        #endregion
    }
}
