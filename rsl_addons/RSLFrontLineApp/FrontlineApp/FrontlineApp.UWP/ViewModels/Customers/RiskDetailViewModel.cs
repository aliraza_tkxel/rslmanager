﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.BusinessLayer;
using FrontlineApp.Utility.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FrontlineApp.UWP.ViewModels
{
    public class RiskDetailViewModel : BaseViewModel
    {
        #region "Properties"

        public CustomerManager CustomerManager { get; set; }

        private CustomerRiskModel selectedRisk;
        public CustomerRiskModel SelectedRisk
        {
            get { return selectedRisk; }
            set { Set(() => SelectedRisk, ref selectedRisk, value); }
        }

        private bool isClosed;
        public bool IsClosed
        {
            get { return isClosed; }
            set { Set(() => IsClosed, ref isClosed, value); }
        }

        #endregion

        #region "Constructor"
        public RiskDetailViewModel()
        {
            CustomerManager = new CustomerManager();
        }
        #endregion

        #region "Commands"

        #region "Close Enquiry"

        public async Task<ResultModel<bool>> CloseEnquiry()
        {

            ResultModel<bool> result = new ResultModel<bool>();
            try
            {
                LookupManager lookupManager = new LookupManager();
                var itemStatuses = lookupManager.GetItemStatus();
                var closeStatusId = itemStatuses.Where(x => x.Description.Equals(ApplicationConstants.CloseStatusTitle)).Select(y => y.Id).FirstOrDefault();

                if (closeStatusId > 0)
                {
                    SelectedRisk.StatusId = closeStatusId;
                    result = await CustomerManager.CloseRiskEnquiry(SelectedRisk);
                }
                else
                {
                    result.Success = false;
                    result.Message = MessageConstants.EnquiryClosedFailed;
                }

            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.Success = false;
            }

            return result;
        }

        #endregion

        #endregion

        #region "Methods"

        #region "Initialize Data"
        public void InitializeData()
        {
            CheckStatus();
        }
        #endregion

        #region "Check Status"
        private void CheckStatus()
        {
            LookupManager lookupManager = new LookupManager();
            var itemStatuses = lookupManager.GetItemStatus();
            var closeStatusId = itemStatuses.Where(x => x.Description.Equals(ApplicationConstants.CloseStatusTitle))
                                            .Select(y => y.Id)
                                            .FirstOrDefault();
            if (SelectedRisk.StatusId == closeStatusId)
            {
                IsClosed = true;
            }
            else
            {
                IsClosed = false;
            }
        }
        #endregion


        #endregion
    }
}
