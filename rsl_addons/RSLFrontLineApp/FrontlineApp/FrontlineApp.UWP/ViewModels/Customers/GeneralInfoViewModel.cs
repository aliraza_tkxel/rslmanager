﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.BusinessLayer;
using FrontlineApp.UWP.ViewModels.Messages;
using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace FrontlineApp.UWP.ViewModels
{
    public class GeneralInfoViewModel : BaseViewModel
    {
        #region "Properties"
        public CustomerManager CustomerManager { get; set; }

        private CustomerModel selectedCustomer;
        public CustomerModel SelectedCustomer
        {
            get { return selectedCustomer; }
            set { Set(() => SelectedCustomer, ref selectedCustomer, value); }
        }

        private CurrentHomeModel currentHome;
        public CurrentHomeModel CurrentHome
        {
            get { return currentHome; }
            set { Set(() => CurrentHome, ref currentHome, value); }
        }

        private bool isInternetAvailable;
        public bool IsInternetAvailable
        {
            get { return isInternetAvailable; }
            set { Set(() => IsInternetAvailable, ref isInternetAvailable, value); }
        }
        private bool hasSpecialRequirements;
        public bool HasSpecialRequirements
        {
            get { return hasSpecialRequirements; }
            set { Set(() => HasSpecialRequirements, ref hasSpecialRequirements, value); }
        }


        private ObservableCollection<string> communicationList;
        public ObservableCollection<string> CommunicationList
        {
            get { return communicationList; }
            set { Set(() => CommunicationList, ref communicationList, value); }
        }

        #endregion

        #region "Constructor"
        public GeneralInfoViewModel()
        {
            CustomerManager = new CustomerManager();
        }
        #endregion

        #region "Commands"
        #endregion

        #region "Methods"

        #region "Fetch records from server"

        public async Task<ResultModel<CustomerModel>> FetchCustomerDetailsFromServer(CustomerModel customer)
        {
            var result = new ResultModel<CustomerModel>();
            try
            {
                result = await CustomerManager.GetCustomerDetail(customer);

                if (result.Success == true)
                {
                    SelectedCustomer = result.Value;
                    if (SelectedCustomer.CommunicationList != null)
                    {
                        HasSpecialRequirements = (SelectedCustomer.CommunicationList.Count > 0) ? true : false;
                    }
                    else
                    {
                        HasSpecialRequirements = false;
                    }
                    CurrentHome = SelectedCustomer.CurrentHome == null ? new CurrentHomeModel() : SelectedCustomer.CurrentHome;
                    Messenger.Default.Send(new CustomerDetailMessage() { UpdatedCustomerDetail = SelectedCustomer,OldCustomerDetail = customer });
                }

            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.Success = false;
            }

            return result;

        }
        #endregion

        #endregion
    }
}
