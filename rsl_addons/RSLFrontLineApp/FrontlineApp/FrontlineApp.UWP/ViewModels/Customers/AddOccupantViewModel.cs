﻿using System.Collections.ObjectModel;
using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.BusinessLayer;
using FrontlineApp.Utility.Helper.Extensions;
using System;
using System.Linq;
using MvvmValidation;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace FrontlineApp.UWP.ViewModels
{
    public class AddOccupantViewModel : BaseViewModel
    {
        #region "Properties"

        public ObservableCollection<LookupModel> TitleList { get; set; }
        public ObservableCollection<LookupModel> RelationshipStatusList { get; set; }
        public ObservableCollection<LookupModel> DisabilitiesList { get; set; }
        public ObservableCollection<LookupModel> EmploymentStatusList { get; set; }


        public LookupManager LookupManager { get; set; }
        public CustomerManager CustomerManager { get; set; }

        public OccupantModel ProtoTypeOccupant { get; set; }

        private ObservableCollection<OccupantModel> occupantList;
        public ObservableCollection<OccupantModel> OccupantList
        {
            get { return occupantList; }
            set
            {
                Set(() => OccupantList, ref occupantList, value);
            }
        }

        #endregion

        #region "Constructor"
        public AddOccupantViewModel()
        {
            LookupManager = new LookupManager();
            CustomerManager = new CustomerManager();
            OccupantList = new ObservableCollection<OccupantModel>();
            InitializeData();
        }
        #endregion

        #region "Safe"
        public List<OccupantModel> GetSafeOccupants()
        {
            ResultModel<bool> DataExists = IsValid();
            if (DataExists.Value == false || OccupantList.Count==0)
            {
                return null;
            }
            return OccupantList.ToList();

        }
        #endregion

        #region "Methods"

        #region "Initialize Data"
        public void InitializeData()
        {
            PopulateLookups();
            SetupOccupantPrototype();
        }
       
        #endregion

        #region "Populate lookups"
        private void PopulateLookups()
        {
            TitleList = (LookupManager.GetCustomerTitles())?.ToObservableCollection();
            RelationshipStatusList = (LookupManager.GetRelationshipStatus())?.ToObservableCollection();
            DisabilitiesList = (LookupManager.GetHealthProblems())?.ToObservableCollection();
            EmploymentStatusList = (LookupManager.GetEmploymentStatus())?.ToObservableCollection();
         }
        #endregion

        #region "Setup Occupant Prototypes"
        private void SetupOccupantPrototype()
        {
            ProtoTypeOccupant = new OccupantModel();
            ProtoTypeOccupant.TitleList = TitleList;
            ProtoTypeOccupant.UIGuidId = Guid.NewGuid().ToString();
            ProtoTypeOccupant.RelationshipStatusList = RelationshipStatusList;
            ProtoTypeOccupant.EmploymentStatusList = EmploymentStatusList;
            //ProtoTypeOccupant.DisabilityList = (LookupManager.GetHealthProblems())?.ToObservableCollection(); 
        }
        #endregion

        #region "Add Occupant"
        public void AddOccupant()
        {
            OccupantModel model = ProtoTypeOccupant.Clone();
            model.DisabilityList = (LookupManager.GetHealthProblems())?.ToObservableCollection();
            OccupantList.Add(model);
        }

        public void ConfigureDisabilityList()
        {
            foreach (OccupantModel model in OccupantList)
            {
                model.TitleList = TitleList;
                model.RelationshipStatusList = RelationshipStatusList;
                model.EmploymentStatusList = EmploymentStatusList;
                model.DisabilityList = PopulateObservableCollection((LookupManager.GetHealthProblems())?.ToObservableCollection(),model.Disabilities);
            }
        }

        private ObservableCollection<LookupModel> PopulateObservableCollection(ObservableCollection<LookupModel> collection, string selectedString)
        {
            if (string.IsNullOrEmpty(selectedString))
            {
                return collection;
            }

            int intItem = 0;
            var intList = selectedString.Split(',')
                                .Select(m => { int.TryParse(m, out intItem); return intItem; })
                                .Where(m => m != 0)
                                .ToList();

            foreach (LookupModel item in collection)
            {
                if (intList.Contains(item.Id))
                {
                    item.IsSelected = true;
                }
            }

            return collection;
        }

        #endregion

        #region "Remove Occupant"
        public void RemoveOccupant(OccupantModel RemoveItem)
        {
            OccupantList.Remove(RemoveItem);
        }
        #endregion

        #endregion

        #region "IsValid"
        public ResultModel<bool> IsValid()
        {
            //SetSelectedItems();
            ResultModel<bool> result = new ResultModel<bool>();
            result.Message = "";
            result.Value = true;
            foreach(OccupantModel model in OccupantList)
            {
                ResultModel<bool> emplValRes = IsOccupantValid(model);
                if (emplValRes.Value == false)
                {
                    result = emplValRes;
                    break;
                }
                
            }
            return result;


        }
        public string SetSelectedItems(OccupantModel model)
        {
            return LookupManager.GetSelectedItems(model.DisabilityList);
        }
        //TODO: Add Rules in the model and make them work instead of manual validation

        private ResultModel<bool> IsOccupantValid(OccupantModel model)
        {
            ResultModel<bool> result = new ResultModel<bool>();
            result.Value = true;
            result.Message = "";
            model.Disabilities = SetSelectedItems(model);
            if (string.IsNullOrWhiteSpace(model.FirstName))
            {
                result.Value = false;
                result.Message = "First Name is mandatory";
                return result;
            }
            else
            {
                Regex noSpecialCharacterRegex = new Regex(@"^[a-zA-Z ]*$");
                result.Value = noSpecialCharacterRegex.IsMatch(model.FirstName);
                if (!result.Value)
                {
                    result.Message = " First name can contain only alphabets and spaces";
                    return result;
                }
            }

            if (string.IsNullOrWhiteSpace(model.LastName))
            {
                result.Value = false;
                result.Message = "Last Name is mandatory";
                return result;
            }
            else
            {
                Regex noSpecialCharacterRegex = new Regex(@"^[a-zA-Z ]*$");
                result.Value = noSpecialCharacterRegex.IsMatch(model.LastName);
                if (!result.Value)
                {
                    result.Message = " Last name can contain only alphabets and spaces";
                    return result;
                }
            }
            if (model.RelationShipStatusId==null)
            {
                result.Value = false;
                result.Message = "Relationship is mandatory";
            }

            return result;
            
            
        }
        #endregion
    }
}
