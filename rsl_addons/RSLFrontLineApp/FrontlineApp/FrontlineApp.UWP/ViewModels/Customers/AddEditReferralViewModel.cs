﻿using System.Collections.ObjectModel;
using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.BusinessLayer;
using FrontlineApp.Utility.Helper.Extensions;
using System;
using MvvmValidation;
using FrontlineApp.Utility.Constants;
using System.Linq;
using System.Collections.Generic;
using FrontlineApp.Utility.Helper.AppSession;
using FrontlineApp.Utility;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;

namespace FrontlineApp.UWP.ViewModels
{
    public class AddEditReferralViewModel : BaseViewModel
    {
        #region "Properties"

        private ObservableCollection<LookupModel> referralStagesList;
        public ObservableCollection<LookupModel> ReferralStagesList
        {
            get { return referralStagesList; }
            set { Set(() => ReferralStagesList, ref referralStagesList, value); }
        }

        private ObservableCollection<LookupModel> yesNoUnknownList;
        public ObservableCollection<LookupModel> YesNoUnknownList
        {
            get { return yesNoUnknownList; }
            set { Set(() => YesNoUnknownList, ref yesNoUnknownList, value); }
        }

        private ObservableCollection<LookupModel> yesNoList;
        public ObservableCollection<LookupModel> YesNoList
        {
            get { return yesNoList; }
            set { Set(() => YesNoList, ref yesNoList, value); }
        }

        //private ObservableCollection<GroupInfoListModel> helpCategoryList;
        //public ObservableCollection<GroupInfoListModel> HelpCategoryList
        //{
        //    get { return helpCategoryList; }
        //    set { Set(() => HelpCategoryList, ref helpCategoryList, value); }
        //}

        private ObservableCollection<GroupInfoListModel> helpCategoryList;
        public ObservableCollection<GroupInfoListModel> HelpCategoryList
        {
            get { return helpCategoryList; }
            set
            {
                Set(() => HelpCategoryList, ref helpCategoryList, value);
                CollectionViewSource cvs = new CollectionViewSource();
                cvs.IsSourceGrouped = true;
                cvs.Source = helpCategoryList;
                HelpCategoriesCvs = cvs;
            }
        }

        private CollectionViewSource helpCategoriesCvs;

        public CollectionViewSource HelpCategoriesCvs
        {
            get { return helpCategoriesCvs; }
            set { Set(() => HelpCategoriesCvs, ref helpCategoriesCvs, value); }
        }

        public LookupManager LookupManager { get; set; }
        public CustomerManager CustomerManager { get; set; }

        private ReferralModel referral;
        public ReferralModel Referral
        {
            get { return referral; }
            set { Set(() => Referral, ref referral, value); }
        }

        private CustomerModel selectedCustomer;
        public CustomerModel SelectedCustomer
        {
            get { return selectedCustomer; }
            set { Set(() => SelectedCustomer, ref selectedCustomer, value); }
        }

        private string title;
        public string Title
        {
            get { return title; }
            set { Set(() => Title, ref title, value); }
        }

        private string subTitle;
        public string SubTitle
        {
            get { return subTitle; }
            set { Set(() => SubTitle, ref subTitle, value); }
        }

        private bool isNewReferral;
        public bool IsNewReferral
        {
            get { return isNewReferral; }
            set { Set(() => IsNewReferral, ref isNewReferral, value); }
        }

        #endregion

        #region "Constructor"
        public AddEditReferralViewModel()
        {
            LookupManager = new LookupManager();
            CustomerManager = new CustomerManager();
        }
        #endregion

        #region "Commands"

        #region "Save Command"

        public async Task<ResultModel<ReferralModel>> Save()
        {
            ResultModel<ReferralModel> result = new ResultModel<ReferralModel>();
            try
            {
                var validationResult = IsValid();
                if (validationResult.Success)
                {
                    Referral.CustomerHelpSubCategories = GetSelectedHelpCategories();
                    Referral.AppVersion = GeneralHelper.GetAppVersion();
                    Referral.CustomerId = SelectedCustomer.CustomerId;
                    Referral.LastActionUserId = AppSession.LoggedinUserId;
                    if (IsNewReferral == true)
                    {
                        Referral.CreatedOnApp = DateTimeOffset.UtcNow;
                    }
                    else
                    {
                        Referral.LastModifiedOnApp = DateTimeOffset.UtcNow;
                    }

                    SelectedCustomer.Referral = Referral;

                   result = await CustomerManager.SaveCustomerReferral(SelectedCustomer);
                }
                else
                {
                    result.Success = false;
                    result.Message = validationResult.Message;
                }

            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.Success = false;
            }

            return result;
        }

        #endregion

        #endregion

        #region "Methods"

        #region "Initialize Data"
        public void InitializeData()
        {
            PopulateLookups();
            SetupReferral();
        }
        #endregion

        #region "Populate lookups"
        private void PopulateLookups()
        {
            YesNoList = (LookupManager.GetYesNo())?.ToObservableCollection();
            YesNoUnknownList = (LookupManager.GetYesNoUnknown())?.ToObservableCollection();
            ReferralStagesList = (LookupManager.GetReferralStages())?.ToObservableCollection();
            HelpCategoryList = (LookupManager.GetReferralHelpCategory())?.ToObservableCollection();            
        }
        #endregion

        #region "Is Valid"
        private ResultModel<bool> IsValid()
        {
            ResultModel<bool> result = new ResultModel<bool>();

            /*ValidationResult referralValidationResult = Referral.ValidateAll();            
            result.Success = referralValidationResult.IsValid;
            result.Message = referralValidationResult.ToString();*/
            result.Success = true;
            result.Message = "";

            return result;
        }
        #endregion

        #region "Get Selected Help Categories"
        private string GetSelectedHelpCategories()
        {
            List<string> selectedHelpCategories = new List<string>();

            foreach (var group in HelpCategoryList)
            {
                foreach (var item in group)
                {
                    LookupModel m = (LookupModel)item;
                    if (m.IsSelected)
                    {
                        selectedHelpCategories.Add(m.Id.ToString());
                    }
                }
            }

            return selectedHelpCategories.Count() > 0 ? String.Join(",", selectedHelpCategories) : string.Empty;
        }
        #endregion

        #region "Setup Referral"
        private void SetupReferral()
        {
            Referral = SelectedCustomer.Referral;

            if (Referral == null)
            {
                IsNewReferral = true;
                Referral = new ReferralModel();
                Title = ApplicationConstants.AddReferralTitle;
                SubTitle = ApplicationConstants.AddReferralSubTitle;
            }
            else
            {
                IsNewReferral = false;
                Title = ApplicationConstants.EditReferralTitle;
                SubTitle = ApplicationConstants.EditReferralSubTitle;
                HelpCategoryList = PopulateObservableCollection(HelpCategoryList, Referral.CustomerHelpSubCategories);
            }
        }
        #endregion

        #region "Populate Observable Collection"

        private ObservableCollection<GroupInfoListModel> PopulateObservableCollection(ObservableCollection<GroupInfoListModel> collection, string selectedString)
        {
            if (string.IsNullOrEmpty(selectedString))
            {
                return collection;
            }

            int intItem = 0;
            var intList = selectedString.Split(',')
                                .Select(m => { int.TryParse(m, out intItem); return intItem; })
                                .Where(m => m != 0)
                                .ToList();

            foreach (var grp in collection)
            {
                foreach (LookupModel item in grp)
                {
                    if (intList.Contains(item.Id))
                    {
                        item.IsSelected = true;
                    }
                }
            }

            return collection;
        }

        #endregion

        #endregion
    }
}
