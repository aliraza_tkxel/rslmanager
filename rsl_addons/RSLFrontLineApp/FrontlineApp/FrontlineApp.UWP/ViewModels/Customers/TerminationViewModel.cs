﻿using System.Collections.ObjectModel;
using System.Linq;
using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.BusinessLayer;
using FrontlineApp.Utility.Constants;
using FrontlineApp.Utility.Helper.Extensions;
using FrontlineApp.UWP.CustomControls.ContentDialogs;
using System;
using System.Collections.Generic;
using MvvmValidation;
using System.Threading.Tasks;
using FrontlineApp.Utility.Helper.AppSession;
using FrontlineApp.Utility;
using Windows.UI.Xaml.Controls;
using FrontlineApp.ServiceLayer.ServiceObjects;
using GalaSoft.MvvmLight.Messaging;
using FrontlineApp.UWP.ViewModels.Messages;

namespace FrontlineApp.UWP.ViewModels
{
    public class TerminationViewModel : BaseViewModel
    {
        #region "Properties"

        private ObservableCollection<LookupModel> reasonsList;
        public ObservableCollection<LookupModel> ReasonsList
        {
            get { return reasonsList; }
            set { Set(() => ReasonsList, ref reasonsList, value); }
        }

        private ObservableCollection<LookupModel> reasonCategoryList;
        public ObservableCollection<LookupModel> ReasonCategoryList
        {
            get { return reasonCategoryList; }
            set { Set(() => ReasonCategoryList, ref reasonCategoryList, value); }
        }

        private ObservableCollection<LookupModel> filteredReasonCategoryList;
        public ObservableCollection<LookupModel> FilteredReasonCategoryList
        {
            get { return filteredReasonCategoryList; }
            set { Set(() => FilteredReasonCategoryList, ref filteredReasonCategoryList, value); }
        }

        public LookupManager LookupManager { get; set; }
        public CustomerManager CustomerManager { get; set; }

        public TerminationSO TerminationResposne { get; set; }
        private TerminationModel termination;
        public TerminationModel Termination
        {
            get { return termination; }
            set { Set(() => Termination, ref termination, value); }
        }

        private CustomerModel selectedCustomer;
        public CustomerModel SelectedCustomer
        {
            get { return selectedCustomer; }
            set { Set(() => SelectedCustomer, ref selectedCustomer, value); }
        }

        private int? reasonId;
        public int? ReasonId
        {
            get { return reasonId; }
            set
            {
                Set(() => ReasonId, ref reasonId, value);
                ChangeReason();
            }
        }

        #endregion

        #region "Constructor"
        public TerminationViewModel()
        {
            LookupManager = new LookupManager();
            CustomerManager = new CustomerManager();
        }
        #endregion

        #region "Commands"
        #endregion

        #region "Methods"

        #region "Save Command"

        public async Task<ResultModel<TerminationSO>> TerminateTenancy()
        {
            ResultModel<TerminationSO> result = new ResultModel<TerminationSO>();
            try
            {
                if (SelectedCustomer.Tenancy != null)
                {
                    Termination.TenancyStartDate = SelectedCustomer.Tenancy.TenancyStartDate;
                }
                Termination.FilteredReasonCategoryList = FilteredReasonCategoryList;

                var validationResult = IsValid();
                if (validationResult.Success)
                {
                    Termination.CustomerId = SelectedCustomer.CustomerId;
                    Termination.TerminatedBy = AppSession.LoggedinUserId;
                    Termination.AppVersion = GeneralHelper.GetAppVersion();
                    if (Termination.WillAddForwardingAddress == false)
                    {
                        Termination.ForwardAddress = new TenantForwardingAddress();
                    }

                    result = await CustomerManager.TerminateTenancy(SelectedCustomer, Termination);
                    TerminationResposne = new TerminationSO();
                    TerminationResposne = result.Value;
                }
                else
                {
                    result.Success = false;
                    result.Message = validationResult.Message;
                }

            }
            catch (Exception ex)
            {
                result.Message = ex.Message;
                result.Success = false;
            }

            return result;
        }

        public bool ShouldShowVoidPopup()
        {
            if (ReasonId != null)
            {
                LookupModel model = ReasonsList.Where(e => e.Id == ReasonId).ToList().FirstOrDefault();
                if(model.Description.Equals(ApplicationConstants.TerminationReasonMutualExchangeBHA)
                  || model.Description.Equals(ApplicationConstants.TerminationReasonMutualExchangeLA)
                  || model.Description.Equals(ApplicationConstants.TerminationReasonMutualExchangeRP))
                {
                    return false;
                }
            }
            return true;
        }

        #endregion

        #region "Initialize Data"
        public void InitializeData()
        {
            PopulateLookups();
            SetupDefaults();
        }
        #endregion

        #region "Setup defaults"
        private void SetupDefaults()
        {
            Termination = new TerminationModel();

            ReasonId = null;
            FilteredReasonCategoryList = null;

            // Added default Date same way as on web when tenancy termination process is started.  
            Termination.TerminationDate = DateTimeOffset.UtcNow.AddDays(ApplicationConstants.TerminationDefaultDaysThreshold);
        }
        #endregion

        #region "Populate lookups"
        private void PopulateLookups()
        {
            ReasonsList = (LookupManager.GetTerminationReason())?.ToObservableCollection();
            ReasonCategoryList = (LookupManager.GetTerminationReasonCategory())?.ToObservableCollection();
        }
        #endregion

        #region "Get Customer Forwarding Address Count"
        private int GetCustomerForwardingAddressCount()
        {
            var customerAddresses = SelectedCustomer.CustomerAddresses;
            var fwdCount = 0;
            if (customerAddresses != null)
            {
                if (customerAddresses.Count() > 0)
                {
                    LookupManager lookupManager = new LookupManager();
                    var addressTypes = lookupManager.GetAddressType();
                    var forwardingAddressTypeId = addressTypes.Where(x => x.Description == "Forwarding Address")
                                                              .Select(y => y.Id)
                                                              .FirstOrDefault();
                    fwdCount = customerAddresses.Where(x => x.AddressTypeId == forwardingAddressTypeId)
                                                .Count();

                }
            }
            return fwdCount;
        }
        #endregion

        #region "Check Forwarding Address"
        private async void CheckForwardingAddress()
        {
            /*int customerFwdAdrsCount = GetCustomerForwardingAddressCount();
            int tenancyCount = SelectedCustomer.TenancyCount == null
                               ? 0
                               : (int)SelectedCustomer.TenancyCount;

            if (customerFwdAdrsCount == 0)
            {
                ContentDialog dialog = new ContentDialog()
                {
                    Title = MessageConstants.WarningTitle,
                    Content = tenancyCount > 0 ? MessageConstants.ForwardingAddressJointTenantError : MessageConstants.ForwardingAddressError,
                    PrimaryButtonText = "Ok"
                };

                await dialog.ShowAsync();
            }*/
            if (Termination.WillAddForwardingAddress==false)
            {
                int customerFwdAdrsCount = GetCustomerForwardingAddressCount();
                int tenancyCount = SelectedCustomer.TenancyCount == null
                                   ? 0
                                   : (int)SelectedCustomer.TenancyCount;
                ContentDialog dialog = new ContentDialog()
                {
                    Title = MessageConstants.WarningTitle,
                    Content = tenancyCount > 0 ? MessageConstants.ForwardingAddressJointTenantError : MessageConstants.ForwardingAddressError,
                    PrimaryButtonText = "OK"
                };

                await dialog.ShowAsync();
            }

        }
        #endregion

        #region "Change Reason"
        private void ChangeReason()
        {

            if (ReasonId != null)
            {
                // Check Forwarding Address
                CheckForwardingAddress();

                // Populate reason category list
                FilteredReasonCategoryList = ReasonCategoryList.Where(x => x.SubId == ReasonId)
                                                               .ToList()
                                                               .ToObservableCollection();
                Termination.ReasonCategoryId = null;

            }

            Termination.ReasonId = ReasonId;

        }
        #endregion

        #region "Is Valid"
        public  ResultModel<bool> IsValid()
        {
            ResultModel<bool> result = new ResultModel<bool>();
            ValidationResult validationResult = Termination.ValidateAll();
            result.Message = validationResult.ToString();
            result.Success = validationResult.IsValid;

            return result;
        }
        #endregion

        #region "Send Messages"
        public void SendVoidInspectionMessage()
        {
            Messenger.Default.Send(new CreateVoidInspectionMessage()
            {
                JournalId = TerminationResposne.journalId
            }
            );
        }
        #endregion

        #endregion
    }
}
