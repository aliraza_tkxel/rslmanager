﻿using System.Collections.ObjectModel;
using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.BusinessLayer;
using FrontlineApp.Utility.Helper.Extensions;
using Windows.UI.Xaml.Data;
using System.Linq;
using FrontlineApp.Utility.Constants;
using MvvmValidation;
using System.Collections.Generic;
using System;

namespace FrontlineApp.UWP.ViewModels
{
    public class TenancyReferralViewModel : BaseViewModel
    {
        
        #region "Properties"
        private CollectionViewSource helpCategoriesCvs;

        public CollectionViewSource HelpCategoriesCvs
        {
            get { return helpCategoriesCvs; }
            set { Set(() => HelpCategoriesCvs, ref helpCategoriesCvs, value); }
        }
        public ObservableCollection<LookupModel> ReferralStagesList { get; set; }
        public ObservableCollection<LookupModel> YesNoUnknownList { get; set; }
        public ObservableCollection<LookupModel> YesNoList { get; set; }
        private ObservableCollection<GroupInfoListModel> helpCategoryList;
        public ObservableCollection<GroupInfoListModel> HelpCategoryList
        {
            get { return helpCategoryList; }
            set
            {
                Set(() => HelpCategoryList, ref helpCategoryList, value);
                CollectionViewSource cvs = new CollectionViewSource();
                cvs.IsSourceGrouped = true;
                cvs.Source = helpCategoryList;
                HelpCategoriesCvs = cvs;
            }
        }

        public LookupManager LookupManager { get; set; }
        public CustomerManager CustomerManager { get; set; }
        private CustomerModel selectedCustomer;
        public CustomerModel SelectedCustomer
        {
            get { return selectedCustomer; }
            set { Set(() => SelectedCustomer, ref selectedCustomer, value); }
        }
        public ReferralModel Referral { get; set; }

        private string title;
        public string Title
        {
            get { return title; }
            set { Set(() => Title, ref title, value); }
        }
        private string subTitle;
        public string SubTitle
        {
            get { return subTitle; }
            set { Set(() => SubTitle, ref subTitle, value); }
        }
        private bool isNewReferral;
        public bool IsNewReferral
        {
            get { return isNewReferral; }
            set { Set(() => IsNewReferral, ref isNewReferral, value); }
        }

        #endregion

        #region "Constructor"
        public TenancyReferralViewModel()
        {
            LookupManager = new LookupManager();
            CustomerManager = new CustomerManager();
            InitializeData();
        }
        
        #endregion

        #region "SAFE"
        public ReferralModel GetSafeRefferalModel()
        {
            if (Referral.IsTypeArrears == null && Referral.IsTypeAsb == null && Referral.IsTypeDamage == null
                && Referral.StageId == null && Referral.IsTypeAsb == null && Referral.SafetyIssueId == null
                && Referral.IsConviction == null && Referral.IsSelfHarm == null && Referral.IsSubstanceMisuse == null
                && Referral.YesNotes == null && Referral.NeglectNotes == null && Referral.OtherNotes == null
                && Referral.MiscNotes == null && Referral.IsTenantAware == null && Referral.Title == null
                && Referral.CustomerHelpSubCategories == null)
            {
                return null;
            }
            return Referral;
        }
        #endregion

        #region "Methods"

        #region "Initialize Data"
        private void InitializeData()
        {
            Referral = new ReferralModel();
            PopulateLookups();
        }
        #endregion

        #region "Populate lookups"
        public void PopulateLookups()
        {
            YesNoList = (LookupManager.GetYesNo())?.ToObservableCollection();
            YesNoUnknownList = (LookupManager.GetYesNoUnknown())?.ToObservableCollection();
            ReferralStagesList = (LookupManager.GetReferralStages())?.ToObservableCollection();
            if (Referral != null)
            {
                HelpCategoryList = (LookupManager.GetReferralHelpCategory(Referral.CustomerHelpSubCategories))?.ToObservableCollection();
            }
            else
            {
                HelpCategoryList = (LookupManager.GetReferralHelpCategory())?.ToObservableCollection();
            }
            
        }
        #endregion


        #endregion

        #region "Validation"
        public string GetSelectedHelpCategories()
        {
            List<string> selectedHelpCategories = new List<string>();

            foreach (var group in HelpCategoryList)
            {
                foreach (var item in group)
                {
                    LookupModel m = (LookupModel)item;
                    if (m.IsSelected)
                    {
                        selectedHelpCategories.Add(m.Id.ToString());
                    }
                }
            }

            return selectedHelpCategories.Count() > 0 ? String.Join(",", selectedHelpCategories) : null;
        }

        public ResultModel<bool> IsValid()
        {
            ResultModel<bool> result = new ResultModel<bool>();
            result.Message = "";
            result.Value = true;
            Referral.CustomerHelpSubCategories = GetSelectedHelpCategories();
            if (Referral.JournalId == null)
            {
                Referral.JournalId = 0;
            }
            /*if (!IsRefferalNotModified())
            {
                ValidationResult emplValRes = Referral.ValidateAll();
                result.Value = emplValRes.IsValid;
                result.Message = emplValRes.ToString();
            }*/
           
            return result;
        }
        public bool IsRefferalNotModified()
        {
            if (string.IsNullOrWhiteSpace(Referral.Title) && Referral.IsTypeArrears==null && Referral.IsTypeAsb==null
                && Referral.IsTypeDamage == null && Referral.StageId==null&& Referral.SafetyIssueId==null
                && Referral.IsConviction==null && Referral.IsSubstanceMisuse==null&& Referral.IsSelfHarm==null
                && string.IsNullOrWhiteSpace(Referral.YesNotes) && Referral.IsTenantAware==null
                && Referral.MoneyIssues == null && Referral.PersonalProblems ==null && Referral.WorkOrTrainings == null
                && Referral.PracticalProblems == null && string.IsNullOrWhiteSpace(Referral.NeglectNotes)
                && string.IsNullOrWhiteSpace(Referral.OtherNotes) && string.IsNullOrWhiteSpace(Referral.MiscNotes))
            {
                return true;
            }
            return false;
        }
        #endregion
    }
}
