﻿using GalaSoft.MvvmLight.Ioc;
using Microsoft.Practices.ServiceLocation;

namespace FrontlineApp.UWP.ViewModels
{
    public class ViewModelLocator
    {
        static ViewModelLocator()
        {
            ServiceLocator.SetLocatorProvider(() => SimpleIoc.Default);

            
            // Customers
            SimpleIoc.Default.Register<AddEditCustomerViewModel>();
            SimpleIoc.Default.Register<AddEditReferralViewModel>();
            SimpleIoc.Default.Register<ReferralDetailViewModel>();
            SimpleIoc.Default.Register<AddOccupantViewModel>();
            SimpleIoc.Default.Register<CustomerMainViewModel>();
            SimpleIoc.Default.Register<JointTenantViewModel>();
            SimpleIoc.Default.Register<StartTenancyViewModel>();
            SimpleIoc.Default.Register<TenancyInfoViewModel>();
            SimpleIoc.Default.Register<TerminationViewModel>();
            SimpleIoc.Default.Register<TenancyPropertyViewModel>();
            SimpleIoc.Default.Register<TenancyReferralViewModel>();
            SimpleIoc.Default.Register<CustomerViewingViewModel>();
            SimpleIoc.Default.Register<ProspectDetailViewModel>();
            SimpleIoc.Default.Register<TenantDetailViewModel>();
            SimpleIoc.Default.Register<GeneralInfoViewModel>();
            SimpleIoc.Default.Register<SetupAndAddTenancyViewModel>();
            SimpleIoc.Default.Register<TenancyCustomerViewModel>();
            SimpleIoc.Default.Register<SelectTenancyCustomerViewModel>();
            SimpleIoc.Default.Register<CustomerRisksViewModel>();
            SimpleIoc.Default.Register<CustomerVulnerabilityViewModel>();
            SimpleIoc.Default.Register<VulnerabilityDetailViewModel>();
            SimpleIoc.Default.Register<RiskDetailViewModel>();
            SimpleIoc.Default.Register<CreateVoidInspectionViewModel>();

            // Properties
            SimpleIoc.Default.Register<PropertyDetailsViewModel>();
            SimpleIoc.Default.Register<PropertyViewModel>();
            SimpleIoc.Default.Register<PropertyFilterViewModel>();

            // Authentication
            SimpleIoc.Default.Register<AuthenticationViewModel>();
            SimpleIoc.Default.Register<ResetPasswordViewModel>();

            // Viewings
            SimpleIoc.Default.Register<SelectCustomerViewModel>();
            SimpleIoc.Default.Register<SelectPropertyViewModel>();
            SimpleIoc.Default.Register<SelectStaffMemberViewModel>();
            SimpleIoc.Default.Register<ViewingsMasterViewModel>();
            SimpleIoc.Default.Register<TodaysViewingsListViewModel>();
            SimpleIoc.Default.Register<ArrangeNewViewingViewModel>();
            SimpleIoc.Default.Register<MyViewingsMasterViewModel>();


            SimpleIoc.Default.Register<ApprovalViewingsListViewModel>();
            SimpleIoc.Default.Register<MyViewingsControlViewModel>();
            SimpleIoc.Default.Register<TodaysViewingsControlViewModel>();
            SimpleIoc.Default.Register<PendingViewingsControlViewModel>();
            SimpleIoc.Default.Register<MyViewingsControlDetailViewModel>();
            SimpleIoc.Default.Register<TodaysViewingsControlDetailViewModel>();
            SimpleIoc.Default.Register<PendingViewingsControlDetailViewModel>();
            SimpleIoc.Default.Register<AllViewingsMasterViewModel>();
            SimpleIoc.Default.Register<AllViewingsControlViewModel>();
            SimpleIoc.Default.Register<AllViewingsControlDetailViewModel>();
            SimpleIoc.Default.Register<ViewingCommentsThreadControlViewModel>();

            //Dashboard
            SimpleIoc.Default.Register<DashboardViewModel>();
        }

        #region "Dashboard"
        public DashboardViewModel DashboardPage
        {
            get
            {
                return ServiceLocator.Current.GetInstance<DashboardViewModel>();
            }
        }
        #endregion

        #region "Customers"
        public CreateVoidInspectionViewModel VoidInspectionViewModel
        {
            get
            {
                return ServiceLocator.Current.GetInstance<CreateVoidInspectionViewModel>();
            }
        }
        public VulnerabilityDetailViewModel VulnerabilityDetailPage
        {
            get
            {
                return ServiceLocator.Current.GetInstance<VulnerabilityDetailViewModel>();
            }
        }

        public RiskDetailViewModel RiskDetailPage
        {
            get
            {
                return ServiceLocator.Current.GetInstance<RiskDetailViewModel>();
            }
        }

        public CustomerRisksViewModel CustomerRisksPage
        {
            get
            {
                return ServiceLocator.Current.GetInstance<CustomerRisksViewModel>();
            }
        }

        public CustomerVulnerabilityViewModel CustomerVulnerabilityPage
        {
            get
            {
                return ServiceLocator.Current.GetInstance<CustomerVulnerabilityViewModel>();
            }
        }
        public SelectTenancyCustomerViewModel SelectTenantCustomerPage
        {
            get
            {
                return ServiceLocator.Current.GetInstance<SelectTenancyCustomerViewModel>();
            }
        }
        public TenancyCustomerViewModel TenancyCustomerPage
        {
            get
            {
                return ServiceLocator.Current.GetInstance<TenancyCustomerViewModel>();
            }
        }
        public ReferralDetailViewModel ReferralDetailPage
        {
            get
            {
                return ServiceLocator.Current.GetInstance<ReferralDetailViewModel>();
            }
        }

        public ProspectDetailViewModel ProspectDetailPage
        {
            get
            {
                return ServiceLocator.Current.GetInstance<ProspectDetailViewModel>();
            }
        }

        public GeneralInfoViewModel GeneralInfoPage
        {
            get
            {
                return ServiceLocator.Current.GetInstance<GeneralInfoViewModel>();
            }
        }

        public TenantDetailViewModel TenantDetailPage
        {
            get
            {
                return ServiceLocator.Current.GetInstance<TenantDetailViewModel>();
            }
        }
        public TenancyReferralViewModel TenancyReferralPage
        {
            get
            {
                return ServiceLocator.Current.GetInstance<TenancyReferralViewModel>();
            }
        }
        public SetupAndAddTenancyViewModel SetUpAndStartTenancyPage
        {
            get
            {
                return ServiceLocator.Current.GetInstance<SetupAndAddTenancyViewModel>();
            }
        }
        public CustomerViewingViewModel CustomerViewingPage
        {
            get
            {
                return ServiceLocator.Current.GetInstance<CustomerViewingViewModel>();
            }
        }
		
        public AddEditCustomerViewModel AddEditCustomerPage
        {
            get
            {
                return ServiceLocator.Current.GetInstance<AddEditCustomerViewModel>();
            }
        }

        public AddEditReferralViewModel AddEditReferralPage
        {
            get
            {
                return ServiceLocator.Current.GetInstance<AddEditReferralViewModel>();
            }
        }

        public AddOccupantViewModel AddOccupantPage
        {
            get
            {
                return ServiceLocator.Current.GetInstance<AddOccupantViewModel>();
            }
        }

        public CustomerMainViewModel CustomerMainPage
        {
            get
            {
                return ServiceLocator.Current.GetInstance<CustomerMainViewModel>();
            }
        }

        public JointTenantViewModel JointTenantPage
        {
            get
            {
                return ServiceLocator.Current.GetInstance<JointTenantViewModel>();
            }
        }

        public StartTenancyViewModel StartTenancyPage
        {
            get
            {
                return ServiceLocator.Current.GetInstance<StartTenancyViewModel>();
            }
        }

        public TenancyInfoViewModel TenancyInfoPage
        {
            get
            {
                return ServiceLocator.Current.GetInstance<TenancyInfoViewModel>();
            }
        }

        public TerminationViewModel TerminationPage
        {
            get
            {
                return ServiceLocator.Current.GetInstance<TerminationViewModel>();
            }
        }

        public TenancyPropertyViewModel TenancyPropertyPage
        {
            get
            {
                return ServiceLocator.Current.GetInstance<TenancyPropertyViewModel>();
            }
        }

        #endregion

        #region "Properties"
        public PropertyFilterViewModel PropertyFilterPage
        {
            get
            {
                return ServiceLocator.Current.GetInstance<PropertyFilterViewModel>();
            }
        }
        public PropertyDetailsViewModel PropertyDetailsPage
        {
            get
            {
                return ServiceLocator.Current.GetInstance<PropertyDetailsViewModel>();
            }
        }

        public PropertyViewModel PropertyPage
        {
            get
            {
                return ServiceLocator.Current.GetInstance<PropertyViewModel>();
            }
        }

        #endregion

        #region "Authentication"

        public AuthenticationViewModel AuthenticationPage
        {
            get
            {
                return ServiceLocator.Current.GetInstance<AuthenticationViewModel>();
            }
        }

        public ResetPasswordViewModel ResetPasswordPage
        {
            get
            {
                return ServiceLocator.Current.GetInstance<ResetPasswordViewModel>();
            }
        }

        #endregion

        #region "Viewings"
        public ViewingCommentsThreadControlViewModel ViewingCommentsThreadControlViewModel
        {
            get
            {
                return ServiceLocator.Current.GetInstance<ViewingCommentsThreadControlViewModel>();
            }
        }
        public AllViewingsControlDetailViewModel AllViewingsControlDetailViewModel
        {
            get
            {
                return ServiceLocator.Current.GetInstance<AllViewingsControlDetailViewModel>();
            }
        }
        public AllViewingsControlViewModel AllViewingsControlViewModel
        {
            get
            {
                return ServiceLocator.Current.GetInstance<AllViewingsControlViewModel>();
            }
        }
        public AllViewingsMasterViewModel AllViewingsMasterViewModel
        {
            get
            {
                return ServiceLocator.Current.GetInstance<AllViewingsMasterViewModel>();
            }
        }
        public PendingViewingsControlDetailViewModel PendingViewingsDetailControl
        {
            get
            {
                return ServiceLocator.Current.GetInstance<PendingViewingsControlDetailViewModel>();
            }
        }
        public TodaysViewingsControlDetailViewModel TodaysViewingsDetailControl
        {
            get
            {
                return ServiceLocator.Current.GetInstance<TodaysViewingsControlDetailViewModel>();
            }
        }
        public MyViewingsControlDetailViewModel MyViewingsDetailControl
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MyViewingsControlDetailViewModel>();
            }
        }

        public PendingViewingsControlViewModel PendingViewingsControl
        {
            get
            {
                return ServiceLocator.Current.GetInstance<PendingViewingsControlViewModel>();
            }
        }
        public TodaysViewingsControlViewModel TodaysViewingsControl
        {
            get
            {
                return ServiceLocator.Current.GetInstance<TodaysViewingsControlViewModel>();
            }
        }
        public MyViewingsControlViewModel MyViewingsControl
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MyViewingsControlViewModel>();
            }
        }
        public ApprovalViewingsListViewModel ApprovalViewingsList
        {
            get
            {
                return ServiceLocator.Current.GetInstance<ApprovalViewingsListViewModel>();
            }
        }
        public MyViewingsMasterViewModel MyViewingsMasterPageViewModel
        {
            get
            {
                return ServiceLocator.Current.GetInstance<MyViewingsMasterViewModel>();
            }
        }
        
        public ArrangeNewViewingViewModel ArrangeEditViewingPage
        {
            get
            {
                return ServiceLocator.Current.GetInstance<ArrangeNewViewingViewModel>();
            }
        }

        public ViewingsMasterViewModel ViewingsMasterPage
        {
            get
            {
                return ServiceLocator.Current.GetInstance<ViewingsMasterViewModel>();
            }
        }
       
       

        public SelectPropertyViewModel SelectPropertyPage
        {
            get
            {
                return ServiceLocator.Current.GetInstance<SelectPropertyViewModel>();
            }
        }
        public TodaysViewingsListViewModel TodaysViewingsListPage
        {
            get
            {
                return ServiceLocator.Current.GetInstance<TodaysViewingsListViewModel>();
            }
        }

        public SelectStaffMemberViewModel SelectStaffMemberPage
        {
            get
            {
                return ServiceLocator.Current.GetInstance<SelectStaffMemberViewModel>();
            }
        }

        public SelectCustomerViewModel SelectCustomerPage
        {
            get
            {
                return ServiceLocator.Current.GetInstance<SelectCustomerViewModel>();
            }
        }

        #endregion

    }
}
