﻿using System.ComponentModel;
using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.UWP.ViewModels;
using FrontlineApp.BusinessLayer.Authentication;
using FrontlineApp.UWP.Infrastructure;
using FrontlineApp.Utility;
using FrontlineApp.UWP.Popups;
using FrontlineApp.Utility.Constants;
using FrontlineApp.Utility.Helper;

namespace FrontlineApp.UWP.ViewModels
{
    public class ResetPasswordViewModel : BaseViewModel
    {
        
        
        private AuthenticationManager authManager;
        public UserModel UserObject { get; set; }

        public ResetPasswordViewModel() : base()
        {

            UserObject = new UserModel();
            
            authManager = new AuthenticationManager();
        }


        
        public async void ResetPassword()
        {
            InputVerifier inputVerifier = new InputVerifier();
            if (inputVerifier.IsValidEmail(UserObject.Email))
            {
                if (GeneralHelper.HasInternetConnection())
                {

                    BusyIndicator serverIndicator = BusyIndicator.Start(MessageConstants.ChangePassword);
                    ManagerResponseModel reponseObj = await authManager.ResetPasswordForEmail(UserObject.Email);

                    if (reponseObj.isSuccessful)
                    {
                        
                        showMessage("Success", reponseObj.message);
                    }
                    else
                    {
                        showMessage("Error", reponseObj.message);
                    }
                    serverIndicator.Close();
                }
                else
                {
                    showMessage("Error", MessageConstants.InternetNotAvailableError);
                }
            }
            else
            {
                showMessage("Alert","Please enter a valid email address");
            }


        }


    }
}
