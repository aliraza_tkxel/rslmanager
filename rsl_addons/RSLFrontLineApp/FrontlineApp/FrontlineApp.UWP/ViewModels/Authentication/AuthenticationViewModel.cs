﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.UWP.Views.Navigation;
using Windows.UI.Xaml;
using FrontlineApp.UWP.Views.Dashboard;
using Windows.UI.Xaml.Media.Animation;
using FrontlineApp.BusinessLayer.Authentication;
using FrontlineApp.UWP.Popups;
using FrontlineApp.Utility.Constants;
using FrontlineApp.Utility;
namespace FrontlineApp.UWP.ViewModels
{
    public class AuthenticationViewModel : BaseViewModel
    {
        private string _appVersion;
        public string AppVersion
        {
            get { return _appVersion; }
            set { Set(() => AppVersion, ref _appVersion, value); }

        }
        public AuthenticationManager authManager;
        public UserModel UserObject { get; set; }

        public AuthenticationViewModel() : base()
        {
            UserObject = new UserModel();
            authManager = new AuthenticationManager();
            AppVersion = GetAppVersion();
        }

        public async void LoginUser()
        {
            if (GeneralHelper.HasInternetConnection())
            {
                BusyIndicator serverIndicator = BusyIndicator.Start(MessageConstants.LoggingIn);
                //UserObject.UserName = "1014.1014";
                //UserObject.Password = "Broadland1014@!";
                //UserObject.UserName = "619.619";
                //UserObject.Password = "Broadland619@!";
                //UserObject.UserName = "734.734";
                //UserObject.Password = "Broadland734@!";
                ManagerResponseModel responseObj = await authManager.LoginUserForParams(UserObject.UserName, UserObject.Password);

                if (responseObj.isSuccessful)
                {

                    responseObj = await authManager.GetSharedLookUps();
                    if (responseObj.isSuccessful)
                    {
                        navigateToDashboard();
                        
                    }
                    else
                    {
                        showMessage("Error", responseObj.message);
                    }

                }
                else
                {
                    showMessage("Error", responseObj.message);
                    UserObject.Password = "";
                }
                serverIndicator.Close();
            }
            else
            {
                LoginUserOffline(UserObject.UserName, UserObject.Password);
            }
            

        }

        public void LoginUserOffline(string userName, string password)
        {
            BusyIndicator serverIndicator = BusyIndicator.Start(MessageConstants.LoggingIn);
            ManagerResponseModel responseObj = authManager.LoginUserOfflineForParams(userName, password);
            if (responseObj.isSuccessful) {
                navigateToDashboard();
            }
            else
            {
                showMessage("Error", responseObj.message);
                UserObject.Password = "";
            }
            serverIndicator.Close();

        }


        private string GetAppVersion()
        {
            string version = "App Version: "+GeneralHelper.GetAppVersion();
            string baseURL = PathConstants.BaseURL;
            if (string.Equals(baseURL, ApplicationConstants.ServerURLDev))
            {
                version = version + " (D)";
            }
            else if (string.Equals(baseURL, ApplicationConstants.ServerURLTest))
            {
                version = version + " (T)";
            }
            else if (string.Equals(baseURL, ApplicationConstants.ServerURLUAT))
            {
                version = version + " (U)";
            }
            else if (string.Equals(baseURL, ApplicationConstants.ServerURLLive))
            {
                version = version + " (L)";
            }
            return version;
        }
       

        private void navigateToDashboard()
        {
            UserObject = new UserModel();
            AppShell shell = new AppShell();
            Window.Current.Content = shell;
            shell.AppFrame.Navigate(typeof(DashboardPage), new SuppressNavigationTransitionInfo());
            Window.Current.Activate();


        }


    }

}
