﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Content Dialog item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace FrontlineApp.UWP.CustomControls.ContentDialogs
{
    public sealed partial class ThemeContentDialog : ContentDialog
    {
        public enum ThemeDialogResult
        {
            Ok,
            Cancel
        }

        public ThemeDialogResult Result { get; set; }

        public ThemeContentDialog()
        {
            this.InitializeComponent();
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            this.Result = ThemeDialogResult.Ok;
            this.Hide();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Result = ThemeDialogResult.Cancel;
            this.Hide();
        }
    }
}
