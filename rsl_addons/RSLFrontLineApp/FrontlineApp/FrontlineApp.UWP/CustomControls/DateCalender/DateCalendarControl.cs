﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace FrontlineApp.UWP.CustomControls
{
    public class DateCalendarControl : CalendarDatePicker
    {
        public static readonly DependencyProperty SelectedDateProperty
           = DependencyProperty.Register("SelectedDate"
                                           , typeof(DateTimeOffset)
                                           , typeof(DateCalendarControl)
                                           , new PropertyMetadata(null, (sender, e) =>
                                           {
                                               if (e.NewValue != null)
                                               {
                                                   ((DateCalendarControl)sender).Date = (DateTimeOffset)e.NewValue;
                                               }
                                               else
                                               {
                                                   ((DateCalendarControl)sender).Date = null;
                                               }
                                           }
                                                               )
                                       );

        public DateTimeOffset SelectedDate
        {
            get { return (DateTimeOffset)GetValue(SelectedDateProperty); }
            set { SetValue(SelectedDateProperty, value); }
        }

       

        protected override void OnApplyTemplate()
        {
            base.OnApplyTemplate();
            this.DateChanged +=
            ArrangeCalendarDatePicker_DateChanged;
        }

        private void ArrangeCalendarDatePicker_DateChanged(CalendarDatePicker sender, CalendarDatePickerDateChangedEventArgs args)
        {
            if (args.NewDate != args.OldDate)
            {
                if (args.NewDate != null && args.NewDate.HasValue)
                {
                    SelectedDate = args.NewDate.Value;
                }
                else if (args.OldDate != null && args.OldDate.HasValue)
                {
                    SelectedDate = args.OldDate.Value;
                    Date = args.OldDate;
                }
                else
                {
                    SelectedDate = DateTimeOffset.Now;
                }
            }
        }

    }
}
