﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;
using Windows.UI.Xaml.Markup;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Data;

namespace FrontlineApp.UWP.Converters
{
    class ViewingsBackgroundConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string culture)
        {
            var color = XamlBindingHelper.ConvertValue(typeof(Windows.UI.Color), "White");
            switch ((string)value)
            {
                case "Today":
                    color = XamlBindingHelper.ConvertValue(typeof(Windows.UI.Color), "#FAF0CD");
                    break;
                default:
                    color = XamlBindingHelper.ConvertValue(typeof(Windows.UI.Color), "#FFFFFF");
                    break;
            }
            SolidColorBrush brushVar = new SolidColorBrush((Windows.UI.Color)color);
            return brushVar;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string culture)
        {
            return null;
        }
    }
}
