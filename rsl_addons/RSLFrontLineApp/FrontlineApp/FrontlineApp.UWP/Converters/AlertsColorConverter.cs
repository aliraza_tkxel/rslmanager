﻿using System;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Markup;
using Windows.UI.Xaml.Media;
namespace FrontlineApp.UWP.Converters
{
    public class AlertsColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string culture)
        {
            var color = XamlBindingHelper.ConvertValue(typeof(Windows.UI.Color), "Black"); 
            if ((string)value == "Open")
            {
                color = XamlBindingHelper.ConvertValue(typeof(Windows.UI.Color), "#4A6BAE");
                
            }
            else if ((string)value == "Completed")
            {
                color = XamlBindingHelper.ConvertValue(typeof(Windows.UI.Color), "#60A62D");
                
            }
            else if((string)value=="In Progress")
            {
                color = XamlBindingHelper.ConvertValue(typeof(Windows.UI.Color), "#DDC830");
                
            }
            SolidColorBrush brushVar = new SolidColorBrush((Windows.UI.Color)color);
            return brushVar;

        }

        public object ConvertBack(object value, Type targetType, object parameter, string culture)
        {
            return null;
        }
        
    }
    
}
