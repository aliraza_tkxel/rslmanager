﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;

namespace FrontlineApp.UWP.Converters
{
    public class NormalGenderConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string culture)
        {
            var path = "ms-appx:///Assets/2x/female.png";
            switch ((string)value)
            {
                case "Male":
                    path = "ms-appx:///Assets/2x/male.png";
                    break;
                default:
                    break;
            }
            return new BitmapImage(new Uri(path, UriKind.Absolute));
        }

        public object ConvertBack(object value, Type targetType, object parameter, string culture)
        {
            return null;
        }
    }
}
