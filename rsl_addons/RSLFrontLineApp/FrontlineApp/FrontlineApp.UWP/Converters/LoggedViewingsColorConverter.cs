﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;
using System.Globalization;
using Windows.UI.Xaml.Markup;
using Windows.UI.Xaml.Media;

namespace FrontlineApp.UWP.Converters
{
    public class LoggedViewingsColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string culture)
        {
            var color = XamlBindingHelper.ConvertValue(typeof(Windows.UI.Color), "Black");
            switch ((string)value)
            {
                case "Rejected":
                    color = XamlBindingHelper.ConvertValue(typeof(Windows.UI.Color), "#BB3838");
                    break;
                case "Acceptance Pending":
                    color = XamlBindingHelper.ConvertValue(typeof(Windows.UI.Color), "#E4A701");
                    break;
                default:
                    color = XamlBindingHelper.ConvertValue(typeof(Windows.UI.Color), "#0BAD4C");
                    break;
            }
            SolidColorBrush brushVar = new SolidColorBrush((Windows.UI.Color)color);
            return brushVar;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string culture)
        {
            return null;
        }
    }
}
