﻿using System;
using Windows.UI.Xaml.Data;

namespace FrontlineApp.UWP.Converters
{
    public class DecmialConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value == null)
                return null;
            decimal result = (decimal)value;
            return result.ToString("0.##");
        }
        public object ConvertBack(object value, Type targetType, object parameter, string language)
       {
            if (value == null)
                return null;

            decimal result;
            bool isParsed = Decimal.TryParse(value.ToString(), out result);

            return isParsed==true ?(decimal?) result: null;
        }
    }
}
