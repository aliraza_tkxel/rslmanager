﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;
using System.Globalization;
using Windows.UI.Xaml.Markup;
using Windows.UI.Xaml.Media;
namespace FrontlineApp.UWP.Converters
{
    public class ViewingsHeaderConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string culture)
        {
            var color = XamlBindingHelper.ConvertValue(typeof(Windows.UI.Color), "Black");
            switch ((string)value)
            {
                case "Past Viewings":
                    color = XamlBindingHelper.ConvertValue(typeof(Windows.UI.Color), "#989898");
                    break;
                default:
                    color = XamlBindingHelper.ConvertValue(typeof(Windows.UI.Color), "#D8D8D8");
                    break;
            }
            SolidColorBrush brushVar = new SolidColorBrush((Windows.UI.Color)color);
            return brushVar;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string culture)
        {
            return null;
        }
    }
}
