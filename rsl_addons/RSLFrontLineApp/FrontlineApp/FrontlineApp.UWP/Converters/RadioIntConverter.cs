﻿using System;
using Windows.UI.Xaml.Data;

namespace FrontlineApp.UWP.Converters
{

    public class RadioIntConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value == null)
                return false;

            return value.ToString().Equals(parameter);
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            if (value == null)
                return null;

            return System.Convert.ToInt32(parameter);
        }
    }

}
