﻿using System;
using Windows.UI.Xaml.Data;

namespace FrontlineApp.UWP.Converters
{
    public class StringEmptyConverter: IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return string.IsNullOrEmpty((string)value) ? parameter : value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }

    }
}
