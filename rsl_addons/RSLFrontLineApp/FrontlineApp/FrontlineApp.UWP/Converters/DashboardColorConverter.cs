﻿using System;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Markup;
using Windows.UI.Xaml.Media;

namespace FrontlineApp.UWP.Converters
{

    public class DashboardColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string culture)
        {
            int numValue = Int32.Parse((string)value);
            var color = XamlBindingHelper.ConvertValue(typeof(Windows.UI.Color), "Black");
            switch (numValue)
            {
                case 0:
                    color = XamlBindingHelper.ConvertValue(typeof(Windows.UI.Color), "#50AEEC");
                    break;
                default:
                    color = XamlBindingHelper.ConvertValue(typeof(Windows.UI.Color), "#D42C1E");
                    break;
            }
            SolidColorBrush brushVar = new SolidColorBrush((Windows.UI.Color)color);
            return color;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string culture)
        {
            return null;
        }
        
    }
}
