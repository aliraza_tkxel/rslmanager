﻿using System;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Data;
using System.Text.RegularExpressions;
using FrontlineApp.Utility;

namespace FrontlineApp.UWP.Converters
{
    public class TypeToImageConverter : IValueConverter
    {

        public object Convert(object value, Type targetType, object parameter, string culture)
        {
            if (value ==null)
                return null;

            bool isSelected = System.Convert.ToBoolean(parameter);
            string enumTitle = Regex.Replace(value.ToString(), @"\s+", string.Empty);
            var item = GeneralHelper.GetEnumsInfo(enumTitle);
            var path = string.Empty;

            if (item != null)
            {
                path = String.Format("ms-appx:///Assets/1x/{0}", isSelected ? item.SelectedStateImage : item.NormalStateImage) ;
                return new BitmapImage(new Uri(path, UriKind.Absolute));
            }

            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string culture)
        {
            return null;
        }
    }
}
