﻿using System;
using Windows.UI.Xaml.Data;

namespace FrontlineApp.UWP.Converters
{
    public class RadioToBoolConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value == null)
                return false;

            return value.Equals(System.Convert.ToBoolean(parameter));
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            return System.Convert.ToBoolean(value) ? System.Convert.ToBoolean(parameter) : false;
        }
    }
}
