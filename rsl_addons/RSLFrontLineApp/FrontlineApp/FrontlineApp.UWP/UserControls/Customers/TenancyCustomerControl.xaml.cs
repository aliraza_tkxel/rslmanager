﻿using FrontlineApp.UWP.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace FrontlineApp.UWP.UserControls.Customers
{
    public sealed partial class TenancyCustomerControl : UserControl
    {
        public TenancyCustomerViewModel ViewModel { get; set; }
        public delegate void NavigationToTenancyHandler();
        public event NavigationToTenancyHandler NavigateToSelectCustomer;
        public TenancyCustomerControl()
        {
            this.InitializeComponent();
            ViewModel = new TenancyCustomerViewModel();
            this.DataContext = ViewModel;
        }

        private void SelectCustomerButton_Click(object sender, RoutedEventArgs e)
        {
            if (NavigateToSelectCustomer != null)
            {
                NavigateToSelectCustomer();
            }
        }
        public TenancyCustomerViewModel GetDataContext()
        {
           return ViewModel;
        }
    }
}
