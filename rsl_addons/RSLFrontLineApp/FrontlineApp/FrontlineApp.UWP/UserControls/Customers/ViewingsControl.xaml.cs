﻿using FrontlineApp.Utility;
using FrontlineApp.Utility.Constants;
using FrontlineApp.UWP.Popups;
using FrontlineApp.UWP.ViewModels;
using System.Threading.Tasks;
using Windows.UI.Popups;
using Windows.UI.Xaml.Controls;
using System;
using FrontlineApp.ApplicationLayer.Models;
using Windows.UI.Xaml;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace FrontlineApp.UWP.UserControls.Customers
{
    public sealed partial class ViewingsControl : UserControl
    {
        CustomerViewingViewModel viewModel;

        private CustomerModel selectedCustomer;
        public CustomerModel SelectedCustomer
        {
            get { return selectedCustomer; }
            set { selectedCustomer = value; }
        }

        private void ViewingsListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            foreach (var item in e.AddedItems)
            {
                ListViewItem lvi = (sender as ListView).ContainerFromItem(item) as ListViewItem;
                if (lvi != null)
                    lvi.ContentTemplate = (DataTemplate)this.Resources["SelectedViewingListViewTemplate"];
            }

            foreach (var item in e.RemovedItems)
            {
                ListViewItem lvi = (sender as ListView).ContainerFromItem(item) as ListViewItem;
                if (lvi != null)
                    lvi.ContentTemplate = (DataTemplate)this.Resources["ViewingListViewTemplate"];
            }
        }

        public ViewingsControl(CustomerModel customer)
        {
            SelectedCustomer = customer;
            this.InitializeComponent();
            viewModel = this.DataContext as CustomerViewingViewModel;
            InitializeData();
        }

        private async void InitializeData()
        {
            if (GeneralHelper.HasInternetConnection())
            {
                viewModel.IsInternetAvailable = true;
                BusyIndicator serverIndicator = BusyIndicator.Start(MessageConstants.FetchingCustomerViewingsFromServer);
                var result = await viewModel.FetchCustomerViewingsFromServer(SelectedCustomer);
                serverIndicator.Close();

                if (result.Success == false)
                {
                    await ShowMessage(MessageConstants.ErrorMessageBoxHeader, result.Message);
                }
            }
            else
            {
                viewModel.IsInternetAvailable = false;
            }
        }

        public async Task<bool> ShowMessage(string header, string message)
        {
            var dialog = new MessageDialog(message);
            dialog.Title = header;
            dialog.Commands.Add(new UICommand { Label = "OK", Id = 0 });
            await dialog.ShowAsync();
            return true;
        }

    }
}
