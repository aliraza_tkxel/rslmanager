﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.UWP.ViewModels;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace FrontlineApp.UWP.UserControls.Customers
{
    public sealed partial class AddOccupantsControl : UserControl
    {
        public AddOccupantViewModel ViewModel { get; set; }
        public AddOccupantsControl()
        {
            this.InitializeComponent();
            ViewModel = new AddOccupantViewModel();
            this.DataContext = ViewModel;
        }

        private void AddOccuptantsButton_Click(object sender, RoutedEventArgs e)
        {
            
            if (ViewModel != null)
            {
                ViewModel.AddOccupant();
            }
        }

        private void btnDelete_Click(object sender, RoutedEventArgs e)
        {
            
            if (ViewModel != null)
            {
                Button senderBtn = sender as Button;
                ViewModel.RemoveOccupant(senderBtn.DataContext as OccupantModel);
            }
        }
        public AddOccupantViewModel GetDataContext()
        {
            return ViewModel;
        }
    }
}
