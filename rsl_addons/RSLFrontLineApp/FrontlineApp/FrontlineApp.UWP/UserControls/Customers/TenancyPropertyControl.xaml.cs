﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.UWP.ViewModels;
using FrontlineApp.UWP.Views.Viewings;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace FrontlineApp.UWP.UserControls.Customers
{
    public sealed partial class TenancyPropertyControl : UserControl
    {
        public TenancyPropertyViewModel ViewModel { get; set; }
        public TenancyPropertyControl()
        {
            this.InitializeComponent();
            ViewModel = new TenancyPropertyViewModel();
            this.DataContext = ViewModel;
        }
        
        public delegate void NavigationToTenancyHandler();
        public event NavigationToTenancyHandler NavigateToSelectProperty;
        private void SelectPropertyButton_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            NavigateToSelectProperty?.Invoke();

        }
        public TenancyPropertyViewModel GetDataContext()
        {
            return ViewModel;
        }
    }
}
