﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.UWP.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace FrontlineApp.UWP.UserControls.Customers
{
    public sealed partial class JointTenantControl : UserControl
    {
        public JointTenantViewModel ViewModel { get; set; }
        public delegate void NavigationToTenancyHandler();
        public event NavigationToTenancyHandler NavigateToSelectCustomer;
        public JointTenantControl()
        {
            this.InitializeComponent();
            ViewModel = new JointTenantViewModel();
            this.DataContext = ViewModel;
        }

        
        public JointTenantViewModel GetDataContext()
        {
            return ViewModel;
        }

        private void SelectCustomer_Click(object sender, RoutedEventArgs e)
        {
            if (NavigateToSelectCustomer != null)
            {
                NavigateToSelectCustomer();
            }
        }

        private void LivesInCarerRadio_Checked(object sender, RoutedEventArgs e)
        {
            RadioButton btn = sender as RadioButton;
            List<LookupModel> listo = GetDataContext().YesNoList.ToList();
            var objList = listo.Where(o => o.Description == btn.Content as string).ToList();
            if (objList.Count > 0)
            {
                LookupModel selected = objList.ElementAt(0);
                GetDataContext().JointTenant.CustomerGeneralInfo.LiveInCarer = selected.Id;
            }
        }

        private void WheelChairRadio_Checked(object sender, RoutedEventArgs e)
        {
            RadioButton btn = sender as RadioButton;
            List<LookupModel> listo = GetDataContext().YesNoList.ToList();
            var objList = listo.Where(o => o.Description == btn.Content as string).ToList();
            if (objList.Count > 0)
            {
                LookupModel selected = objList.ElementAt(0);
                GetDataContext().JointTenant.CustomerGeneralInfo.Wheelchair = selected.Id;
            }
        }
    }
}
