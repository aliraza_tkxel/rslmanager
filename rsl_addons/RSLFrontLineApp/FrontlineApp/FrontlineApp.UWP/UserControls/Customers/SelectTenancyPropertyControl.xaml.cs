﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.Utility;
using FrontlineApp.Utility.Constants;
using FrontlineApp.UWP.ViewModels;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace FrontlineApp.UWP.UserControls.Customers
{
    public sealed partial class SelectTenancyPropertyControl : UserControl
    {
        public delegate void PropertySelectionDelegate(PropertyModel selectedObj);
        public PropertySelectionDelegate PropertySelectionDel;
        public delegate void NavigationToTenancyHandler();
        public event NavigationToTenancyHandler GoBack;
        public SelectTenancyPropertyControl()
        {
            this.InitializeComponent();
            SelectPropertyViewModel ViewModel = this.DataContext as SelectPropertyViewModel;
            if (GeneralHelper.HasInternetConnection())
            {
                ViewModel.GetPropertyList();
            }
            else
            {
                ViewModel.showMessage("Error", MessageConstants.InternetNotAvailableError);
            }
            
        }
        public SelectPropertyViewModel GetDataContext()
        {
            return this.DataContext as SelectPropertyViewModel;
        }
        private void btnDone_Click(object sender, RoutedEventArgs e)
        {
            SelectPropertyViewModel ViewModel = this.DataContext as SelectPropertyViewModel;
            if (string.IsNullOrWhiteSpace(ViewModel.SelectedProperty.propertyId))
            {
                ViewModel.showMessage("Error", "Please select a property first");
            }
            else
            {
                PropertySelectionDel(ViewModel.SelectedProperty);
                GoBack();
            }

        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            GoBack();
        }

        private void SearchBox_QuerySubmitted(SearchBox sender, SearchBoxQuerySubmittedEventArgs args)
        {
            SelectPropertyViewModel ViewModel = this.DataContext as SelectPropertyViewModel;
            ViewModel.FilterListBySearchString(args.QueryText);
        }
    }
}
