﻿using FrontlineApp.Utility;
using FrontlineApp.Utility.Constants;
using FrontlineApp.UWP.Popups;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace FrontlineApp.UWP.UserControls.Customers
{
    public sealed partial class PaythruPaymentControl : UserControl
    {
        public delegate void CancelButtonActionHandler();
        public event CancelButtonActionHandler RemoveFromGridAction;
        private string ShortenedURL;
        public PaythruPaymentControl()
        {
            this.InitializeComponent();
            PayThruWebView.NavigationStarting += PayThruWebView_NavigationStarting;
            PayThruWebView.NavigationFailed += PayThruWebView_NavigationFailed;
            PayThruWebView.ContentLoading += PayThruWebView_ContentLoading;
            PayThruWebView.DOMContentLoaded += PayThruWebView_DOMContentLoaded;
        }

        

        public void ShowUrlWith(int? TenancyId,int? CustomerId,int StaffMemberId)
        {
            if (GeneralHelper.HasInternetConnection())
            {
                if(TenancyId!=null && CustomerId != null)
                {
                    ShortenedURL = "";
                    string BaseURL = UriTemplateConstants.BaseURL;
                    BaseURL = BaseURL.Replace("/RSLFrontLineApi", "");
                    string PaythruURL = string.Format("{0}/PaythruWebLocal/Paythru.svc/geturl?tenancyId={1}&customerId={2}&applet=staff&employeeId={3}", BaseURL, TenancyId, CustomerId, StaffMemberId);
                    PayThruWebView.Navigate(new Uri(PaythruURL));

                    
                }
                else
                {
                    showMessage("Error", MessageConstants.PayThruInvalidParams);
                }

            }
            else
            {
                showMessage("Error", MessageConstants.InternetNotAvailableError);
            }
            
        }

        public async Task<string> ParseDomTree()
        {
            string html = await PayThruWebView.InvokeScriptAsync("eval", new string[] { "document.documentElement.outerHTML;" });
            return html;
        }

        private void PayThruWebView_NavigationFailed(object sender, WebViewNavigationFailedEventArgs e)
        {
            
            showMessage("Error", MessageConstants.PayThruConnectionFailure);

        }

        
        private void PayThruWebView_DOMContentLoaded(WebView sender, WebViewDOMContentLoadedEventArgs args)
        {
            
            NavigateToResponseString();
        }

        private async void NavigateToResponseString()
        {
            if (string.IsNullOrWhiteSpace(ShortenedURL))
            {
                ShortenedURL = await ParseDomTree();
                ShortenedURL = ShortenedURL.Replace("<string xmlns=\"http://schemas.microsoft.com/2003/10/Serialization/\">", "");
                ShortenedURL = ShortenedURL.Replace("</string>", "");
                if (!string.IsNullOrWhiteSpace(ShortenedURL))
                {
                    PayThruWebView.Navigate(new Uri(ShortenedURL));
                }
            }
            
        }

        private void PayThruWebView_ContentLoading(WebView sender, WebViewContentLoadingEventArgs args)
        {
            
        }
        private void PayThruWebView_NavigationStarting(WebView sender, WebViewNavigationStartingEventArgs args)
        {
            
        }
        public async void showMessage(string header, string message)
        {
            var dialog = new MessageDialog(message);
            dialog.Title = header;
            dialog.Commands.Add(new UICommand { Label = "OK", Id = 0 });
            await dialog.ShowAsync();
        }

        private void ExitView_Click(object sender, RoutedEventArgs e)
        {
            RemoveFromGridAction?.Invoke();
        }
    }
}
