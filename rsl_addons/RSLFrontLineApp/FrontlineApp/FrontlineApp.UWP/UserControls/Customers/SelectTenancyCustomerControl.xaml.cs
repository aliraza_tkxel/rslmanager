﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.Utility;
using FrontlineApp.Utility.Constants;
using FrontlineApp.UWP.ViewModels;
using System.Collections.Generic;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace FrontlineApp.UWP.UserControls.Customers
{
    public sealed partial class SelectTenancyCustomerControl : UserControl
    {
        public delegate void CustomerSelectionDelegate(CustomerModel selectedObj);
        public CustomerSelectionDelegate CustSelDel;
        public delegate void NavigationToTenancyHandler();
        public event NavigationToTenancyHandler GoBack;
        public SelectTenancyCustomerControl()
        {
            this.InitializeComponent();
            SelectTenancyCustomerViewModel ViewModel = this.DataContext as SelectTenancyCustomerViewModel;
            if (GeneralHelper.HasInternetConnection())
            {
                ViewModel.SearchText = null;
                ViewModel.GetCustomerList();
            }
            else
            {
                ViewModel.showMessage("Error", MessageConstants.InternetNotAvailableError);
            }
                
        }
        public SelectTenancyCustomerViewModel GetDataContext()
        {
            return this.DataContext as SelectTenancyCustomerViewModel; 
        }
        private void SearchBox_QuerySubmitted(SearchBox sender, SearchBoxQuerySubmittedEventArgs args)
        {
            SelectTenancyCustomerViewModel ViewModel = this.DataContext as SelectTenancyCustomerViewModel;
            ViewModel.GetCustomerList();
        }
        private void btnDone_Click(object sender, RoutedEventArgs e)
        {
            SelectTenancyCustomerViewModel ViewModel = this.DataContext as SelectTenancyCustomerViewModel;
            if (ViewModel.SelectedProperty.CustomerId == 0)
            {
                ViewModel.showMessage("Error", "Please select a Customer first");
            }
            else
            {
                CustSelDel(ViewModel.SelectedProperty);
                //this.Frame.GoBack();
                GoBack();
            }

        }
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            GoBack();
        }
    }
}
