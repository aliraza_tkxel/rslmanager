﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.Utility;
using FrontlineApp.Utility.Constants;
using FrontlineApp.UWP.Popups;
using FrontlineApp.UWP.ViewModels;
using System.Threading.Tasks;
using Windows.UI.Popups;
using Windows.UI.Xaml.Controls;
using System;
using FrontlineApp.UWP.Views.Customers;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace FrontlineApp.UWP.UserControls.Customers
{
    public sealed partial class ReferralDetailControl : UserControl
    {

        ReferralDetailViewModel viewModel;

        public delegate void NavigationToAddEditReferralHandler(CustomerModel customer);
        public event NavigationToAddEditReferralHandler OnNavigationToAddEditReferral;

        public ReferralDetailControl(CustomerModel customer)
        {
            this.InitializeComponent();
            viewModel = this.DataContext as ReferralDetailViewModel;
            InitializeData(customer);
        }

        private async void btnEditReferral_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            if (GeneralHelper.HasInternetConnection())
            {
                OnNavigationToAddEditReferral(viewModel.SelectedCustomer);                
            }
            else
            {
                await ShowMessage(MessageConstants.WarningTitle, MessageConstants.AddEditReferralOfflineError);
            }
        }

        private async void InitializeData(CustomerModel customer)
        {

            if (GeneralHelper.HasInternetConnection())
            {
                viewModel.IsInternetAvailable = true;
                BusyIndicator serverIndicator = BusyIndicator.Start(MessageConstants.FetchingCustomerReferralFromServer);
                var result = await viewModel.GetCustomerReferralFromServer(customer);
                serverIndicator.Close();

                if (result.Success == false)
                {
                    await ShowMessage(MessageConstants.ErrorMessageBoxHeader, result.Message);
                }
            }
            else
            {
                viewModel.IsInternetAvailable = false;
            }
        }

        public async Task<bool> ShowMessage(string header, string message)
        {
            var dialog = new MessageDialog(message);
            dialog.Title = header;
            dialog.Commands.Add(new UICommand { Label = "OK", Id = 0 });
            await dialog.ShowAsync();
            return true;
        }

        private async void btnAddReferral_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            if (GeneralHelper.HasInternetConnection())
            {
                OnNavigationToAddEditReferral(viewModel.SelectedCustomer);
            }
            else
            {
                await ShowMessage(MessageConstants.WarningTitle, MessageConstants.AddEditReferralOfflineError);
            }
        }
    }
}
