﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.Utility;
using FrontlineApp.Utility.Constants;
using FrontlineApp.UWP.Popups;
using FrontlineApp.UWP.ViewModels;
using System.Threading.Tasks;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using System;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace FrontlineApp.UWP.UserControls.Customers
{
    public sealed partial class RiskControl : UserControl
    {
        CustomerRisksViewModel viewModel;

        public delegate void NavigationToRiskDetailHandler(CustomerRiskModel risk);
        public event NavigationToRiskDetailHandler OnNavigationToRiskDetail;

        public RiskControl(CustomerModel customer)
        {
            this.InitializeComponent();
            viewModel = this.DataContext as CustomerRisksViewModel;
            InitializeData(customer);
        }


        private async void InitializeData(CustomerModel customer)
        {
            viewModel.ResetList();
            viewModel.SelectedCustomer = customer;

            if (GeneralHelper.HasInternetConnection())
            {
                viewModel.IsInternetAvailable = true;
                BusyIndicator serverIndicator = BusyIndicator.Start(MessageConstants.FetchingCustomerRisksFromServer);
                var result = await viewModel.FetchCustomerRisksFromServer();
                serverIndicator.Close();

                if (result.Success == false)
                {
                    await ShowMessage(MessageConstants.ErrorMessageBoxHeader, result.Message);
                }
            }
            else
            {
                viewModel.IsInternetAvailable = false;
            }
        }

        public async Task<bool> ShowMessage(string header, string message)
        {
            var dialog = new MessageDialog(message);
            dialog.Title = header;
            dialog.Commands.Add(new UICommand { Label = "OK", Id = 0 });
            await dialog.ShowAsync();
            return true;
        }

        private void RisksListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            foreach (var item in e.AddedItems)
            {
                ListViewItem lvi = (sender as ListView).ContainerFromItem(item) as ListViewItem;
                if (lvi != null)
                    lvi.ContentTemplate = (DataTemplate)this.Resources["SelectedRiskListViewTemplate"];
            }

            foreach (var item in e.RemovedItems)
            {
                ListViewItem lvi = (sender as ListView).ContainerFromItem(item) as ListViewItem;
                if (lvi != null)
                    lvi.ContentTemplate = (DataTemplate)this.Resources["RiskListViewTemplate"];
            }
        }

        private void OnItemClick(object sender, ItemClickEventArgs e)
        {
            var selectedRisk = e.ClickedItem as CustomerRiskModel;
            OnNavigationToRiskDetail(selectedRisk);
        }
    }
}
