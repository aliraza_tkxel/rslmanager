﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.Utility;
using FrontlineApp.Utility.Constants;
using FrontlineApp.UWP.Popups;
using FrontlineApp.UWP.ViewModels;
using System.Threading.Tasks;
using Windows.UI.Popups;
using Windows.UI.Xaml.Controls;
using System;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace FrontlineApp.UWP.UserControls.Customers
{
    public sealed partial class GeneralInfoControl : UserControl
    {

        GeneralInfoViewModel viewModel;

        private CustomerModel selectedCustomer;

        public CustomerModel SelectedCustomer
        {
            get { return selectedCustomer; }
            set { selectedCustomer = value; }
        }

        public delegate void NavigationToAddEditCustomerHandler(CustomerModel customer);
        public event NavigationToAddEditCustomerHandler OnNavigationToAddEditCustomer;

        public GeneralInfoControl(CustomerModel customer)
        {
            SelectedCustomer = customer;
            this.InitializeComponent();
            viewModel = this.DataContext as GeneralInfoViewModel;
            InitializeData();            
        }

        private async void InitializeData()
        {
            if (GeneralHelper.HasInternetConnection())
            {
                viewModel.IsInternetAvailable = true;
                BusyIndicator serverIndicator = BusyIndicator.Start(MessageConstants.FetchingCustomerDetailsFromServer);
                var result = await viewModel.FetchCustomerDetailsFromServer(SelectedCustomer);
                serverIndicator.Close();

                if (result.Success == false)
                {
                  await  ShowMessage(MessageConstants.ErrorMessageBoxHeader, result.Message);
                }

            }
            else
            {
                viewModel.IsInternetAvailable = false;
            }
        }

        public async Task<bool> ShowMessage(string header, string message)
        {
            var dialog = new MessageDialog(message);
            dialog.Title = header;
            dialog.Commands.Add(new UICommand { Label = "OK", Id = 0 });
            await dialog.ShowAsync();
            return true;
        }

        private async void btnEditCustomer_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            if (GeneralHelper.HasInternetConnection())
            {
                OnNavigationToAddEditCustomer(viewModel.SelectedCustomer);
            }
            else
            {
                await ShowMessage(MessageConstants.WarningTitle, MessageConstants.AddEditReferralOfflineError);
            }
        }
    }
}
