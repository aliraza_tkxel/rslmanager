﻿using FrontlineApp.UWP.ViewModels;
using Windows.UI.Xaml.Controls;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace FrontlineApp.UWP.UserControls.Customers
{
    public sealed partial class TenancyInfoControl : UserControl
    {
        public TenancyInfoViewModel ViewModel { get; set; }
        public TenancyInfoControl()
        {
            this.InitializeComponent();
            ViewModel = new TenancyInfoViewModel();
            this.DataContext = ViewModel;
        }
        public TenancyInfoViewModel GetContext()
        {
            return ViewModel;
        }
    }
}
