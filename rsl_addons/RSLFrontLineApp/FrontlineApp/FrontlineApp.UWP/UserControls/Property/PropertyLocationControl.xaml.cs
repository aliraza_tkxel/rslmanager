﻿using FrontlineApp.UWP.Popups;
using System;
using Windows.Devices.Geolocation;
using Windows.Services.Maps;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Maps;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace FrontlineApp.UWP.UserControls.Property
{
    public sealed partial class PropertyLocationControl : UserControl
    {
        public delegate void CancelButtonActionHandler();
        public event CancelButtonActionHandler RemoveFromGridAction;
        public MapIcon mapIcon;
        public string locationStr;
        public PropertyLocationControl()
        {
            this.InitializeComponent();
            LocationControl.Style = Windows.UI.Xaml.Controls.Maps.MapStyle.Road;
            
           
        }
        public void setLocationTitle(string location)
        {
            locationStr = location;
            ReverseGeoCode();
        }
        public async void ReverseGeoCode() {
            BusyIndicator serverIndicator = BusyIndicator.Start("Fetching Location...");
            BasicGeoposition location = new BasicGeoposition();  
            location.Latitude = 51.509865;
            location.Longitude = -0.118092;
            Geopoint hintPoint = new Geopoint(location);
            MapLocationFinderResult result = await MapLocationFinder.FindLocationsAsync(locationStr, hintPoint);

            if (result.Status == MapLocationFinderStatus.Success)
            {
                BasicGeoposition newLocation = new BasicGeoposition();
                newLocation.Latitude = result.Locations[0].Point.Position.Latitude;
                newLocation.Longitude = result.Locations[0].Point.Position.Longitude;
                Geopoint geoPoint = new Geopoint(newLocation);
                MapAddress mapAddress = result.Locations[0].Address;
                string address = mapAddress.FormattedAddress;
                AddMapIcon(geoPoint, address);
            }
            
            serverIndicator.Close();
        }
        private async void AddMapIcon(Geopoint geoPoint, String address)
        {
            BusyIndicator serverIndicator = BusyIndicator.Start("Adding Map Icon...");
            if (mapIcon != null)
            {
                LocationControl.MapElements.Remove(mapIcon);
            }
            mapIcon = new MapIcon();
            mapIcon.Location = geoPoint;
            mapIcon.Title = address;

           LocationControl.MapElements.Add(mapIcon);
           bool result = await LocationControl.TrySetViewAsync(geoPoint);
           serverIndicator.Close();
        }
        private void ExitView_Click(object sender, RoutedEventArgs e)
        {
            RemoveFromGridAction?.Invoke();
        }
    }
}
