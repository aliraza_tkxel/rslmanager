﻿using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.UWP.ViewModels;
using System.Collections.Generic;
using FrontlineApp.UWP.Views.Viewings;
using FrontlineApp.Utility;
using FrontlineApp.UWP.UserControls.Property;
using Windows.UI.Xaml;
using Windows.UI.Popups;
using System;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace FrontlineApp.UWP.UserControls.Viewings
{
    public sealed partial class PendingViewingsDetailControl : UserControl
    {
        public delegate void RefreshDataSourceHandler();
        public event RefreshDataSourceHandler RefreshDataAction;
        private PropertyLocationControl LocationCtrl { get; set; }
        public PendingViewingsDetailControl()
        {
            this.InitializeComponent();
        }
        public PendingViewingsControlDetailViewModel GetDataContext()
        {
            return this.DataContext as PendingViewingsControlDetailViewModel;
        }
        public void ConfigureDataForViewing(ViewingSummaryModel model)
        {
            GetDataContext().SelectedProperty = model;
            GetDataContext().GetMyViewingDetails();
        }
        private async void AcceptButton_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            var messageDialog = new MessageDialog("Are you sure you want to accept this Viewing?");
            messageDialog.Title = @"Confirm Action";
            // Add commands and set their callbacks; both buttons use the same callback function instead of inline event handlers
            messageDialog.Commands.Add(new UICommand(
                "Yes",
                new UICommandInvokedHandler(AcceptCommandInvokedHandler)));
            messageDialog.Commands.Add(new UICommand(
                "No"));

            // Set the command that will be invoked by default
            messageDialog.DefaultCommandIndex = 0;

            // Set the command to be invoked when escape is pressed
            messageDialog.CancelCommandIndex = 1;

            // Show the message dialog
            await messageDialog.ShowAsync();
        }

        private async void RejectButton_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            
            var messageDialog = new MessageDialog("Are you sure you want to reject this Viewing?");
            messageDialog.Title = @"Confirm Action";
            // Add commands and set their callbacks; both buttons use the same callback function instead of inline event handlers
            messageDialog.Commands.Add(new UICommand(
                "Yes",
                new UICommandInvokedHandler(RejectCommandInvokedHandler)));
            messageDialog.Commands.Add(new UICommand(
                "No"));

            // Set the command that will be invoked by default
            messageDialog.DefaultCommandIndex = 0;

            // Set the command to be invoked when escape is pressed
            messageDialog.CancelCommandIndex = 1;

            // Show the message dialog
            await messageDialog.ShowAsync();
        }
        private void RejectCommandInvokedHandler(IUICommand command)
        {
            RejectViewing();
        }
        private void AcceptCommandInvokedHandler(IUICommand command)
        {
            AcceptViewing();
        }
        private async void RejectViewing()
        {
            var res = await GetDataContext().RejectViewing();
            if (res.Success)
            {
                RefreshDataAction?.Invoke();
            }
        }
        private async void AcceptViewing()
        {
            var res = await GetDataContext().AcceptViewing();
            if (res.Success)
            {
                RefreshDataAction?.Invoke();
            }
        }
        private void ViewLocation_Click(object sender, RoutedEventArgs e)
        {
            if (LocationCtrl == null)
            {
                LocationCtrl = new PropertyLocationControl();

                LocationCtrl.RemoveFromGridAction += LocationCtrl_RemoveFromGridAction;
            }

            MainGrid.Children.Add(LocationCtrl);
            LocationCtrl.setLocationTitle(GetDataContext().SelectedProperty.PropertyAddress);
        }
        private void LocationCtrl_RemoveFromGridAction()
        {
            MainGrid.Children.Remove(LocationCtrl);
        }
    }
}
