﻿using FrontlineApp.UWP.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace FrontlineApp.UWP.UserControls.Viewings
{
    public sealed partial class CommentsThreadControl : UserControl
    {
        public delegate void CancelButtonActionHandler();
        public event CancelButtonActionHandler RemoveFromGridAction;
        public CommentsThreadControl()
        {
            this.InitializeComponent();
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            RemoveFromGridAction?.Invoke();
        }

        public ViewingCommentsThreadControlViewModel GetDataContext()
        {
            return this.DataContext as ViewingCommentsThreadControlViewModel;
        }
    }
}
