﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.Utility.Constants;
using FrontlineApp.UWP.UserControls.Property;
using FrontlineApp.UWP.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace FrontlineApp.UWP.UserControls.Viewings
{
    public sealed partial class AllViewingsDetailControl : UserControl
    {
        public delegate void RefreshDataSourceHandler();
        public event RefreshDataSourceHandler RefreshDataAction;
        private PropertyLocationControl LocationCtrl { get; set; }
        private CommentsThreadControl CommentsCtrl { get; set; }
        public delegate void RearrangeViewingHandler(ViewingSummaryModel model);
        public event RearrangeViewingHandler RearrangeViewingAction;
        public AllViewingsDetailControl()
        {
            this.InitializeComponent();
        }
        public AllViewingsControlDetailViewModel GetDataContext()
        {
            return this.DataContext as AllViewingsControlDetailViewModel;
        }
        public void ConfigureDataForViewing(ViewingSummaryModel model)
        {
            GetDataContext().SelectedProperty = model;
            GetDataContext().GetMyViewingDetails();
        }
        private async void CancelViewing()
        {
            ResultModel<bool> result = await GetDataContext().CancelViewing();
            if (result.Success)
            {
                RefreshDataAction?.Invoke();
            }

        }
        private async void BtnCancelViewing_Click(object sender, RoutedEventArgs e)
        {
            if (GetDataContext().SelectedProperty.ViewingStatusId == ApplicationConstants.ViewingStatusCancelled)
            {
                GetDataContext().showMessage("", "This viewing is already in cancelled state");
            }
            else
            {
                var messageDialog = new MessageDialog("Are you sure you want to cancel this Viewing?");
                messageDialog.Title = @"Confirm Action";
                // Add commands and set their callbacks; both buttons use the same callback function instead of inline event handlers
                messageDialog.Commands.Add(new UICommand(
                    "Yes",
                    new UICommandInvokedHandler(CommandInvokedHandler)));
                messageDialog.Commands.Add(new UICommand(
                    "No"));

                // Set the command that will be invoked by default
                messageDialog.DefaultCommandIndex = 0;

                // Set the command to be invoked when escape is pressed
                messageDialog.CancelCommandIndex = 1;

                // Show the message dialog
                await messageDialog.ShowAsync();
            }

            
        }
        private void CommandInvokedHandler(IUICommand command)
        {
            CancelViewing();
        }

        private async void RecordFeedback(string type)
        {
            ResultModel<bool> result = await GetDataContext().RecordFeedBack(type);
            if (type.Equals(ApplicationConstants.ViewingCommentTypeGeneral))
            {
                if (result.Success)
                {
                    RefreshDataAction?.Invoke();
                }
            }
            else if(type.Equals(ApplicationConstants.ViewingCommentTypeEdit))
            {
                if (GetDataContext().SelectedProperty.ViewingStatusId == ApplicationConstants.ViewingStatusCancelled)
                {
                    GetDataContext().showMessage("", "Cancelled Viewings cannot be edited.");
                }
                else
                {
                    RearrangeViewing();
                }
            }
            
        }

        private void RearrangeViewing()
        {
            //GetDataContext().SelectedProperty.ViewingDate = "";
            //GetDataContext().SelectedProperty.FormattedTime = "";
            
            FlyOutMenu.Hide();
            RearrangeViewingAction?.Invoke(GetDataContext().SelectedProperty);

        }

        private void ButtonSubmit_Click(object sender, RoutedEventArgs e)
        {

            RecordFeedback(ApplicationConstants.ViewingCommentTypeEdit);
        }

        private void BtnSubmitFeedback_Click(object sender, RoutedEventArgs e)
        {
            RecordFeedback(ApplicationConstants.ViewingCommentTypeGeneral);
        }

        private void ViewLocation_Click(object sender, RoutedEventArgs e)
        {
            if (LocationCtrl == null)
            {
                LocationCtrl = new PropertyLocationControl();

                LocationCtrl.RemoveFromGridAction += LocationCtrl_RemoveFromGridAction;
            }

            MainGrid.Children.Add(LocationCtrl);
            LocationCtrl.setLocationTitle(GetDataContext().SelectedProperty.PropertyAddress);
        }
        private void LocationCtrl_RemoveFromGridAction()
        {
            MainGrid.Children.Remove(LocationCtrl);
        }

        private void ViewAllCommentsBtn_Click(object sender, RoutedEventArgs e)
        {
            if (CommentsCtrl == null)
            {
                CommentsCtrl = new CommentsThreadControl();
                CommentsCtrl.GetDataContext().viewingObj = GetDataContext().SelectedProperty;
                CommentsCtrl.GetDataContext().CommentsList = CommentsCtrl.GetDataContext().ViewingsManager.GetViewingCommentsFor(CommentsCtrl.GetDataContext().viewingObj);
                CommentsCtrl.RemoveFromGridAction += CommentsCtrl_RemoveFromGridAction;
            }

            MainGrid.Children.Add(CommentsCtrl);
        }

        private void CommentsCtrl_RemoveFromGridAction()
        {
            MainGrid.Children.Remove(CommentsCtrl);
        }

    }
}
