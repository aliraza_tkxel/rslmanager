﻿using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.UWP.ViewModels;
using FrontlineApp.UWP.UserControls.Property;
using Windows.UI.Xaml;
using Microsoft.Practices.ServiceLocation;
using FrontlineApp.UWP.Views.Customers;
using FrontlineApp.Utility.Constants;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace FrontlineApp.UWP.UserControls.Viewings
{
    public sealed partial class TodaysViewingsDetailControl : UserControl
    {
        private PropertyLocationControl LocationCtrl { get; set; }
        private CommentsThreadControl CommentsCtrl { get; set; }
        public delegate void CreateTenancyActionHandler(PropertyDetailsModel model);
        public event CreateTenancyActionHandler CreateTenancyAction;
        public TodaysViewingsDetailControl()
        {
            this.InitializeComponent();
        }
        public TodaysViewingsControlDetailViewModel GetDataContext()
        {
            return this.DataContext as TodaysViewingsControlDetailViewModel;
        }
        public void ConfigureDataForViewing(ViewingSummaryModel model)
        {
            GetDataContext().SelectedProperty = model;
            GetDataContext().GetMyViewingDetails();
        }
        private void SubmitFeedback_Click(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            RecordFeedBack();
        }
        private async void RecordFeedBack()
        {
            await GetDataContext().RecordFeedBack(ApplicationConstants.ViewingCommentTypeGeneral);
        }
        private void ViewLocation_Click(object sender, RoutedEventArgs e)
        {
            if (LocationCtrl == null)
            {
                LocationCtrl = new PropertyLocationControl();

                LocationCtrl.RemoveFromGridAction += LocationCtrl_RemoveFromGridAction;
            }

            MainGrid.Children.Add(LocationCtrl);
            LocationCtrl.setLocationTitle(GetDataContext().SelectedProperty.PropertyAddress);
        }
        private void LocationCtrl_RemoveFromGridAction()
        {
            MainGrid.Children.Remove(LocationCtrl);
        }

        private void StartTenancyButton_Click(object sender, RoutedEventArgs e)
        {
            PropertyDetailsModel model = new PropertyDetailsModel();
            model.propertyId = GetDataContext().SelectedProperty.PropertyId;
            model.rent = GetDataContext().SelectedProperty.PropertyRent;
            model.address = GetDataContext().SelectedProperty.PropertyAddress;
            model.propertyTypeDescription = GetDataContext().SelectedProperty.PropertyType;
            CreateTenancyAction?.Invoke(model);
        }

        private void ViewAllCommentsBtn_Click(object sender, RoutedEventArgs e)
        {
            if (CommentsCtrl == null)
            {
                CommentsCtrl = new CommentsThreadControl();
                CommentsCtrl.GetDataContext().viewingObj = GetDataContext().SelectedProperty;
                CommentsCtrl.GetDataContext().CommentsList = CommentsCtrl.GetDataContext().ViewingsManager.GetViewingCommentsFor(CommentsCtrl.GetDataContext().viewingObj);
                CommentsCtrl.RemoveFromGridAction += CommentsCtrl_RemoveFromGridAction;
            }

            MainGrid.Children.Add(CommentsCtrl);
        }

        private void CommentsCtrl_RemoveFromGridAction()
        {
            MainGrid.Children.Remove(CommentsCtrl);
        }
    }
}
