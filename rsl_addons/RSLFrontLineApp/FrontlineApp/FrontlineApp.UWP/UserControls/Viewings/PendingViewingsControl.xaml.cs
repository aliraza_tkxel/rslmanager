﻿using System.Collections.Generic;
using System.Linq;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.UWP.ViewModels;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace FrontlineApp.UWP.UserControls.Viewings
{
    public sealed partial class PendingViewingsControl : UserControl
    {
        public delegate void SelectedItemHandler(ViewingSummaryModel selectedItem);
        public event SelectedItemHandler ItemSelectionAction;
        public PendingViewingsControl()
        {
            this.InitializeComponent();
            GetPendingViewings();
        }

        private void ViewingsList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            
            if (ViewingsList.SelectedItem != null)
            {
                ViewingSummaryModel selectedObj = ViewingsList.SelectedItem as ViewingSummaryModel;
                ItemSelectionAction?.Invoke(selectedObj);
                foreach (var item in e.AddedItems)
                {
                    ListViewItem lvi = (sender as ListView).ContainerFromItem(item) as ListViewItem;
                    lvi.ContentTemplate = (DataTemplate)this.Resources["ListItemSelected"];
                }
                //Remove DataTemplate for unselected items
                foreach (var item in e.RemovedItems)
                {
                    ListViewItem lvi = (sender as ListView).ContainerFromItem(item) as ListViewItem;
                    lvi.ContentTemplate = (DataTemplate)this.Resources["ListItem"];
                }
            }
        }

        public void GetPendingViewings()
        {
            PendingViewingsControlViewModel vm = this.DataContext as PendingViewingsControlViewModel;
            vm.GetPendingViewings();
        }

    }
}
