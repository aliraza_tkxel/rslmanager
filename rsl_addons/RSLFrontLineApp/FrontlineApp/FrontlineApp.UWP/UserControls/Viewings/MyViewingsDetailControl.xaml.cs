﻿using System.Collections.Generic;
using Windows.UI.Xaml;
using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.UWP.ViewModels;
using FrontlineApp.UWP.UserControls.Property;
using Windows.UI.Popups;
using FrontlineApp.Utility.Constants;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace FrontlineApp.UWP.UserControls.Viewings
{
    public sealed partial class MyViewingsDetailControl : UserControl
    {
        public delegate void RefreshDataSourceHandler();
        public event RefreshDataSourceHandler RefreshDataAction;
        private CommentsThreadControl CommentsCtrl { get; set; }
        private PropertyLocationControl LocationCtrl { get; set; }
        public delegate void RearrangeViewingHandler(ViewingSummaryModel model);
        public event RearrangeViewingHandler RearrangeViewingAction;

        public MyViewingsDetailControl()
        {
            this.InitializeComponent();
        }

        public void ConfigureDataForViewing(ViewingSummaryModel model)
        {
            GetDataContext().SelectedProperty = model;
            GetDataContext().GetMyViewingDetails();
        }

        public MyViewingsControlDetailViewModel GetDataContext()
        {
            return this.DataContext as MyViewingsControlDetailViewModel;
        }

        private void ButtonSubmit_Click(object sender, RoutedEventArgs e)
        {
            RecordFeedback(ApplicationConstants.ViewingCommentTypeRearrange);
            
        }
        private async void CancelViewing()
        {
            ResultModel<bool> result = await GetDataContext().CancelViewing();
            if (result.Success)
            {
                RefreshDataAction?.Invoke();
            }

        }

        private async void Button_Cancel_Click(object sender, RoutedEventArgs e)
        {
            
            var messageDialog = new MessageDialog("Are you sure you want to cancel this Viewing?");
            messageDialog.Title = @"Confirm Action";
            // Add commands and set their callbacks; both buttons use the same callback function instead of inline event handlers
            messageDialog.Commands.Add(new UICommand(
                "Yes",
                new UICommandInvokedHandler(CommandInvokedHandler)));
            messageDialog.Commands.Add(new UICommand(
                "No"));

            // Set the command that will be invoked by default
            messageDialog.DefaultCommandIndex = 0;

            // Set the command to be invoked when escape is pressed
            messageDialog.CancelCommandIndex = 1;

            // Show the message dialog
            await messageDialog.ShowAsync();
        }
        private void CommandInvokedHandler(IUICommand command)
        {
            CancelViewing();
        }
        private void BtnSubmitFeedback_Click(object sender, RoutedEventArgs e)
        {
            RecordFeedback(ApplicationConstants.ViewingCommentTypeGeneral);
        }
        private async void RecordFeedback(string type)
        {
            ResultModel<bool> result = await GetDataContext().RecordFeedBack(type);
            if(type.Equals(ApplicationConstants.ViewingCommentTypeGeneral))
            {
                if (result.Success)
                {
                    RefreshDataAction?.Invoke();
                }
            }
            else if (type.Equals(ApplicationConstants.ViewingCommentTypeRearrange))
            {
                RearrangeViewing();
            }
            
        }
        private void RearrangeViewing()
        {
            //GetDataContext().SelectedProperty.ViewingDate = "";
            //GetDataContext().SelectedProperty.FormattedTime = "";
            FlyOutMenu.Hide();
            RearrangeViewingAction?.Invoke(GetDataContext().SelectedProperty);

        }

        private void ViewLocation_Click(object sender, RoutedEventArgs e)
        {
            if (LocationCtrl == null)
            {
                LocationCtrl = new PropertyLocationControl();

                LocationCtrl.RemoveFromGridAction += LocationCtrl_RemoveFromGridAction;
            }

            MainGrid.Children.Add(LocationCtrl);
            LocationCtrl.setLocationTitle(GetDataContext().SelectedProperty.PropertyAddress);
        }
        private void LocationCtrl_RemoveFromGridAction()
        {
            MainGrid.Children.Remove(LocationCtrl);
        }

        private void ViewAllCommentsBtn_Click(object sender, RoutedEventArgs e)
        {
            if (CommentsCtrl == null)
            {
                CommentsCtrl = new CommentsThreadControl();
                CommentsCtrl.GetDataContext().viewingObj = GetDataContext().SelectedProperty;
                CommentsCtrl.GetDataContext().CommentsList = CommentsCtrl.GetDataContext().ViewingsManager.GetViewingCommentsFor(CommentsCtrl.GetDataContext().viewingObj);
                CommentsCtrl.RemoveFromGridAction += CommentsCtrl_RemoveFromGridAction;
            }

            MainGrid.Children.Add(CommentsCtrl);
        }

        private void CommentsCtrl_RemoveFromGridAction()
        {
            MainGrid.Children.Remove(CommentsCtrl);
        }

    }
}
