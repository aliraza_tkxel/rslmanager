﻿using FrontlineApp.ApplicationLayer.Models;
using FrontlineApp.UWP.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace FrontlineApp.UWP.UserControls.Viewings
{
    public sealed partial class AllViewingsControl : UserControl
    {
        public delegate void SelectedItemHandler(ViewingSummaryModel selectedItem);
        public event SelectedItemHandler ItemSelectionAction;
        public AllViewingsControl()
        {
            this.InitializeComponent();
            GetAllViewings();
        }

        private void ViewingsList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (ViewingsList.SelectedItem != null)
            {
                ViewingSummaryModel model = ViewingsList.SelectedItem as ViewingSummaryModel;
                ItemSelectionAction?.Invoke(model);
                foreach (var item in e.AddedItems)
                {
                    ListViewItem lvi = (sender as ListView).ContainerFromItem(item) as ListViewItem;
                    lvi.ContentTemplate = (DataTemplate)this.Resources["ListItemSelected"];
                }
                //Remove DataTemplate for unselected items
                foreach (var item in e.RemovedItems)
                {
                    ListViewItem lvi = (sender as ListView).ContainerFromItem(item) as ListViewItem;
                    lvi.ContentTemplate = (DataTemplate)this.Resources["ListItem"];
                }
            }
        }
        public void GetAllViewings()
        {
            AllViewingsControlViewModel vm = this.DataContext as AllViewingsControlViewModel;
            vm.GetAllViewings();
        }
    }
}
