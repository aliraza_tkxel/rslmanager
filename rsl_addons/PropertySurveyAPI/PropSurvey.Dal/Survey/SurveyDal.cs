﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PropSurvey.Dal.Base;
using PropSurvey.Contracts.Data;
using System.Transactions;
using PropSurvey.Entities;
using System.Data.Objects;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using PropSurvey.Utilities.Constants;
using System.Web.Script.Serialization;
using PropSurvey.Utilities.Helpers;
using System.Collections;
using System.ComponentModel;
using System.Reflection;

namespace PropSurvey.Dal.Survey
{
    public class SurveyDal : BaseDal
    {
        #region save Survey Form
        /// <summary>
        /// This function save the survey form. Data should be post at specific url. 
        /// </summary>
        /// <param name="survData">survey data object</param>
        /// <returns>it returns true or false</returns>
        public bool saveSurveyForm(SurveyData survData, PropertySurvey_Entities currentContext = null)
        {
            if (currentContext != null) this.context = currentContext;

            PS_Survey survey = new PS_Survey();
            bool success = false;
           
            survey = this.savePsSurvey(survey, survData);
            if (survData.surveyNotesDetail != null && survData.surveyNotesDetail != string.Empty && survData.surveyNotesDate != null)
            {
                if (survData.surveyNotesDetail != string.Empty)
                {
                    //save the survey in ps_survey_item_notes
                    this.savePsSurveyItemNotes(survey, survData);

                    //save survey notes in pa_property_item_notes
                    this.savePaSurveyItemNotes(survData);
                }
            }

            //save the survey fields
            this.saveSurveyFields(survey, survData);

            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            //trans.Complete();
            success = true;
            //}

            if (currentContext == null) context.AcceptAllChanges();

            return success;
        }
        #endregion

        #region "Save/Update Heating"
        /// <summary>
        /// Save/Update Heating
        /// </summary>
        private void saveHeatingInfo(SurveyData survData)
        {
            /// Note: Currently there is no functionality for the heating Item to be added or deleted from the app side
            /// that is why only heating update code is written. This function need to be updated if add or delete functionality will be required by client
            var surveyHeatingQuery = context.PA_HeatingMapping
                                            .Where(x => x.HeatingMappingId == survData.heatingId)
                                            .FirstOrDefault();

            if (surveyHeatingQuery != null)
            {
                surveyHeatingQuery.HeatingType = survData.fuelTypeId;
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            }
            
        }
        #endregion

        #region save Ps Survey
        /// <summary>
        /// This function will save the survey in ps_survey and ps_appointment2survey
        /// </summary>
        /// <param name="survey"></param>
        /// <param name="survData"></param>
        /// <returns></returns>
        private PS_Survey savePsSurvey(PS_Survey survey, SurveyData survData)
        {
            PS_Appointment2Survey savedSurvey = new PS_Appointment2Survey();

            //this will contain the existing /already saved survey id
            int existingSurveyId = 0;

            //Find the survey aginst give appointment id. 
            var appoint2survey = context.PS_Appointment2Survey.Where(app => app.AppointId == survData.appointmentId);

            //If survey already saved against the appointment id then get the existing survey id
            if (appoint2survey.Count() > 0)
            {
                //get the existing survey id
                savedSurvey = appoint2survey.First();
                existingSurveyId = savedSurvey.SurveyId;
            }

            if (existingSurveyId > 0)
            {
                survey.SurveyId = existingSurveyId;
            }


            survey.PROPERTYID = survData.propertyId;
            survey.CompletedBy = survData.completedBy;
            survey.SurveyDate = DateTime.Now;

            if (existingSurveyId > 0)
            {
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            }
            else
            {
                //save the data in ps_survey table
                context.AddToPS_Survey(survey);
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            }

            //if survey does not exist then entry will go to appointment2survey
            if (existingSurveyId == 0)
            {
                PS_Appointment2Survey appSurv = new PS_Appointment2Survey();
                appSurv.AppointId = survData.appointmentId;
                appSurv.SurveyId = survey.SurveyId;

                context.AddToPS_Appointment2Survey(appSurv);
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            }

            return survey;
        }
        #endregion

        #region Save Ps Survey Item Notes
        /// <summary>
        /// This function saves the notes of survey. Everytime new record will be inserted in PS_Survey_Item_Notes against ItemId 
        /// If item id already exist then previous record will be updated
        /// </summary>
        /// <param name="survey"></param>
        /// <param name="survData"></param>
        private void savePsSurveyItemNotes(PS_Survey survey, SurveyData survData)
        {
            var psNotes = context.PS_Survey_Item_Notes
                                 .Where(app => app.SurveyId == survey.SurveyId                                               
                                               && ((survData.heatingId != null && survData.heatingId == app.HeatingMappingId) 
                                                    || (survData.heatingId == null && app.HeatingMappingId == null))
                                               && app.ItemId == survData.itemId);

            PS_Survey_Item_Notes surveyNotes = new PS_Survey_Item_Notes();

            if (psNotes.Count() > 0)
            {
                surveyNotes = psNotes.First();
            }
            else
            {
                surveyNotes.ItemId = survData.itemId;                               
                surveyNotes.SurveyId = survey.SurveyId;
            }

            //save the notes in PS_Survey_Item_Notes            
            surveyNotes.Notes = survData.surveyNotesDetail;
            surveyNotes.NotesDate = survData.surveyNotesDate;

            if (surveyNotes.HeatingMappingId == null 
                && survData.heatingId !=null)
            {
                surveyNotes.HeatingMappingId = survData.heatingId; 
            }

            if (psNotes.Count() <= 0)
            {
                context.AddToPS_Survey_Item_Notes(surveyNotes);
            }
            //save the changes
            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);

        }
        #endregion

        #region save Pa Survey Item Notes
        /// <summary>
        /// This function will save the survey notes in pa_property_item_notes
        /// </summary>
        /// <param name="survData"></param>
        private void savePaSurveyItemNotes(SurveyData survData)
        {
            PA_PROPERTY_ITEM_NOTES paItemNotes = new PA_PROPERTY_ITEM_NOTES();

            //save the notes in PA_property_item_notes
            //search if previous notes already exist
            var paNotes = context.PA_PROPERTY_ITEM_NOTES
                                 .Where(prop => prop.ItemId == survData.itemId
                                     && ((survData.heatingId != null && survData.heatingId == prop.HeatingMappingId)
                                          || (survData.heatingId == null && prop.HeatingMappingId == null))
                                     && prop.PROPERTYID == survData.propertyId);

            if (paNotes.Count() > 0)
            {
                //fetch the already existed notes
                paItemNotes = paNotes.First();
            }
            else
            {
                //if record does not already exist the set the property id
                paItemNotes.PROPERTYID = survData.propertyId;

                //set the item id
                paItemNotes.ItemId = survData.itemId;                
            }

            paItemNotes.Notes = survData.surveyNotesDetail;
            paItemNotes.CreatedOn = survData.surveyNotesDate;
            paItemNotes.CreatedBy = survData.completedBy;

            if (paItemNotes.HeatingMappingId == null && survData.heatingId != null)
            {
                paItemNotes.HeatingMappingId = survData.heatingId;
            }

            //if previous notes does not already exist
            if (paNotes.Count() <= 0)
            {
                //insert the record
                context.AddToPA_PROPERTY_ITEM_NOTES(paItemNotes);
            }

            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
        }

        #endregion

        #region save Survey Fields
        /// <summary>
        /// This function saves the fields of survey.Every record will be inserted in ps_survey_parameters 
        /// and the record will be updated in pa_property attributes      
        /// </summary>
        /// <param name="survey"></param>
        /// <param name="survData"></param>
        private void saveSurveyFields(PS_Survey survey, SurveyData survData)
        {
            // reset shared variables 
            ApplicationConstants.plannedComponentId = 0;
            ApplicationConstants.previousComponentId = 0;

            //Variables for Condition Rating / Condition Works
            var componentIds = new List<int>();
            int attributeId = -1;
            int conditionRatingValueId = -1;
            string worksRequired = string.Empty;
            bool isUnsatisfactory = false;

            //Check if component is mapped with item
            var plannedComponentItem = context.PLANNED_COMPONENT_ITEM.Where(comp => comp.ITEMID == survData.itemId && comp.PARAMETERID == null && comp.VALUEID == null);
            if (plannedComponentItem.Count() > 0)
            {
                ApplicationConstants.plannedComponentId = plannedComponentItem.First().COMPONENTID;
                ApplicationConstants.previousComponentId = 0;
            }
            if (survData.surveyFields != null && survData.surveyFields.Count() > 0)
            {
                //save the survey fields value
                List<SurveyFieldsData> sortedSurveyFields = survData.surveyFields.OrderByDescending(s => s.controlType).ToList();
                // survData.surveyFields.Sort();            
                foreach (var item in sortedSurveyFields)
                {
                    #region itterate through all fields for an item and insert/update records accordingly

                    //if there is something in param item field
                    if (item.surveyParamItemField.Count() > 0)
                    {
                        foreach (var field in item.surveyParamItemField)
                        {
                            #region get plannedcomponentId by checking if  Component is mapped with item,parameter,parametervalue, subparameter and subparameter value

                            //Check if component is mapped with Item and parameter
                            var plannedComponentParameter = context.PLANNED_COMPONENT_ITEM.Where(comp => comp.ITEMID == survData.itemId 
                                                                                                        && comp.PARAMETERID == item.surveyParameterId 
                                                                                                        && comp.VALUEID == null);
                            if (plannedComponentParameter.Count() > 0)
                            {
                                ApplicationConstants.plannedComponentId = plannedComponentParameter.First().COMPONENTID;
                                ApplicationConstants.previousComponentId = 0;
                            }
                            else
                            {
                                //Check if component is mapped with Item , parameter and Value
                                var plannedComponentValue = context.PLANNED_COMPONENT_ITEM.Where(comp => comp.ITEMID == survData.itemId 
                                                                                                         && comp.PARAMETERID == item.surveyParameterId 
                                                                                                         && comp.VALUEID == field.surveyPramItemFieldId 
                                                                                                         && comp.SubParameter != null 
                                                                                                         && comp.SubValue != null);
                                var plannedComponentNextValue = context.PLANNED_COMPONENT_ITEM.Where(comp => comp.ITEMID == survData.itemId 
                                                                                                            && comp.SubParameter == item.surveyParameterId 
                                                                                                            && comp.SubValue == field.surveyPramItemFieldId);
                                if (plannedComponentNextValue.Count() > 0)
                                {
                                    ApplicationConstants.nextComponentId = field.surveyPramItemFieldId;
                                    ApplicationConstants.plannedComponentId = plannedComponentNextValue.First().COMPONENTID;
                                }
                                else if (plannedComponentValue.Count() > 0)
                                {
                                    if (ApplicationConstants.nextComponentId > 0)
                                    {
                                        plannedComponentValue = context.PLANNED_COMPONENT_ITEM.Where(comp => comp.ITEMID == survData.itemId 
                                                                                                            && comp.PARAMETERID == item.surveyParameterId 
                                                                                                            && comp.VALUEID == field.surveyPramItemFieldId 
                                                                                                            && comp.SubValue == ApplicationConstants.nextComponentId);

                                    }
                                    if (plannedComponentValue.Count() > 0)
                                    {
                                        ApplicationConstants.previousComponentId = (int)plannedComponentValue.First().VALUEID;
                                        ApplicationConstants.plannedComponentId = plannedComponentValue.First().COMPONENTID;
                                    }
                                }

                                else
                                {
                                    //Check if component is mapped with Item , parameter , ParameterValue, sub Parameter and sub value
                                    var plannedComponentSubValue = context.PLANNED_COMPONENT_ITEM.Where(comp => comp.ITEMID == survData.itemId 
                                                                                                                && comp.PARAMETERID == item.surveyParameterId 
                                                                                                                && comp.VALUEID == ApplicationConstants.previousComponentId 
                                                                                                                && comp.SubParameter != null 
                                                                                                                && comp.SubValue == field.surveyPramItemFieldId);
                                    if (plannedComponentSubValue.Count() > 0)
                                    {
                                        ApplicationConstants.plannedComponentId = plannedComponentSubValue.First().COMPONENTID;
                                        ApplicationConstants.previousComponentId = 0;
                                    }
                                }
                            }
                            #endregion

                            #region Add componentId to list, to add/update condition works records
                            if (ApplicationConstants.plannedComponentId > 0 && !(componentIds.Contains(ApplicationConstants.plannedComponentId)))
                                componentIds.Add(ApplicationConstants.plannedComponentId);
                            #endregion

                            //If user did not select the field value and left it empty then fieldId will be zero
                            //don't save it
                            if (item.controlType.ToUpper() == "Textbox".ToUpper() 
                                || item.controlType.ToUpper() == "TextArea".ToUpper() 
                                || item.controlType.ToUpper() == "CheckBoxes".ToUpper() 
                                || item.controlType.ToUpper() == "Radiobutton".ToUpper() 
                                || (item.controlType.ToUpper() == "Dropdown".ToUpper() && field.surveyPramItemFieldId > 0))
                            {
                                #region save data(survey data, i.e item, item parameters, and parameter values) in ps_survey_parameters

                                PS_Survey_Parameters surveyFields = new PS_Survey_Parameters();
                                PA_PROPERTY_ATTRIBUTES paAttribItem = new PA_PROPERTY_ATTRIBUTES();

                                IQueryable<PS_Survey_Parameters> oldSurveyFields;

                                //if field is checkbox then surveyParamItemField will have multiple values and 
                                //we will add one more and condition to match valueid 
                                if (item.surveyParamItemField.Count() > 1)
                                {
                                    oldSurveyFields = context.PS_Survey_Parameters
                                                             .Where(app => app.SurveyId == survey.SurveyId 
                                                                           && app.ItemParamId == item.surveyItemParamId 
                                                                           && app.ValueId == field.surveyPramItemFieldId
                                                                           && ((survData.heatingId != null && survData.heatingId == app.HeatingMappingId)
                                                                                || (survData.heatingId == null && app.HeatingMappingId == null))
                                                                           );
                                }
                                else
                                {
                                    //Find same survey which was saved previously i.e text box and drop down
                                    oldSurveyFields = context.PS_Survey_Parameters
                                                             .Where(app => app.SurveyId == survey.SurveyId
                                                                          && ((survData.heatingId != null && survData.heatingId == app.HeatingMappingId)
                                                                                || (survData.heatingId == null && app.HeatingMappingId == null))
                                                                           && app.ItemParamId == item.surveyItemParamId);
                                }

                                if (oldSurveyFields.Count() > 0)
                                {
                                    surveyFields = oldSurveyFields.First();
                                }
                                else
                                {
                                    surveyFields.SurveyId = survey.SurveyId;
                                    surveyFields.ItemParamId = item.surveyItemParamId;
                                }

                                if (surveyFields.HeatingMappingId == null && survData.heatingId != null)
                                {
                                    surveyFields.HeatingMappingId = survData.heatingId;
                                }


                                //Logger.Write(survData);
                                //if control type is not text box then this will not be selected
                                if (item.controlType.ToUpper() != "Textbox".ToUpper() && item.controlType.ToUpper() != "TextArea".ToUpper())
                                {
                                    surveyFields.ValueId = field.surveyPramItemFieldId;
                                }

                                //if the field has checkbox type then the isSeleccted will be Yes or No else it will be empty
                                if (field.isSelected != string.Empty)
                                {
                                    surveyFields.IsCheckBoxSelected = field.isSelected == "Yes" ? true : false;
                                }

                                surveyFields.PARAMETERVALUE = field.surveyParamItemFieldValue;                                

                                if (oldSurveyFields.Count() > 0)
                                {
                                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                                    context.Detach(surveyFields);
                                }
                                else
                                {
                                    context.AddToPS_Survey_Parameters(surveyFields);
                                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                                    context.Detach(surveyFields);
                                }
                                #endregion

                                #region save data(survey data, i.e item, item parameters, and parameter values) in PA_PROPERTY_ATTRIBUTES

                                //search if field value already exist in PA_PROPERTY_ATTRIBUTES
                                var paAttrib = context.PA_PROPERTY_ATTRIBUTES
                                                      .Where(prop => prop.PROPERTYID == survData.propertyId
                                                                    && ((survData.heatingId != null && survData.heatingId == prop.HeatingMappingId)
                                                                        || (survData.heatingId == null && prop.HeatingMappingId == null))
                                                                    && prop.ITEMPARAMID == item.surveyItemParamId);

                                //if field is checkbox then surveyParamItemField will have multiple values and 
                                //we will add one more and condition to match valueid, because we 'll have to update all checkbox values
                                if (item.surveyParamItemField.Count() > 1)
                                {
                                    paAttrib = context.PA_PROPERTY_ATTRIBUTES.Where(prop => prop.PROPERTYID == survData.propertyId && prop.ITEMPARAMID == item.surveyItemParamId && prop.VALUEID == field.surveyPramItemFieldId);
                                }

                                if (paAttrib.Count() > 0)
                                {
                                    //fetch the already existed record
                                    paAttribItem = paAttrib.First();
                                }
                                else
                                {
                                    //if record does not already exist the set the property id and itemparamid
                                    paAttribItem.PROPERTYID = survey.PROPERTYID;
                                    paAttribItem.ITEMPARAMID = item.surveyItemParamId;
                                }

                                //set the value id and value                          
                                //if control type is not text box then this will not be selected
                                if (item.controlType.ToUpper() != "Textbox".ToUpper() && item.controlType.ToUpper() != "TextArea".ToUpper())
                                {
                                    paAttribItem.VALUEID = field.surveyPramItemFieldId;
                                }

                                if (paAttribItem.HeatingMappingId == null && survData.heatingId != null)
                                {
                                    paAttribItem.HeatingMappingId = survData.heatingId;
                                }


                                paAttribItem.PARAMETERVALUE = field.surveyParamItemFieldValue;                                
                                paAttribItem.UPDATEDBY = survey.CompletedBy;
                                paAttribItem.UPDATEDON = survey.SurveyDate;
                                paAttribItem.IsUpdated = true;
                                //if the field has checkbox type then the isSeleccted will be Yes or No else it will be empty
                                if (field.isSelected != string.Empty)
                                {
                                    paAttribItem.IsCheckBoxSelected = field.isSelected == "1" ? true : false;
                                }

                                //if previous record does not already exist
                                if (paAttrib.Count() <= 0)
                                {
                                    //insert the record
                                    context.AddToPA_PROPERTY_ATTRIBUTES(paAttribItem);
                                }

                                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                                #endregion

                                #region Get attributeId, valueId and works required for condition works

                                if (item.controlType.ToLower() == "RadioButton".ToLower() && item.surveyParamName == "Condition Rating" && field.surveyParamItemFieldValue.Contains("Unsatisfactory"))
                                {
                                    attributeId = paAttribItem.ATTRIBUTEID;

                                    isUnsatisfactory = true;
                                    conditionRatingValueId = field.surveyPramItemFieldId;

                                }
                                if (item.controlType.ToUpper() == "TextArea".ToUpper() && item.surveyParamName == "Works Required")
                                    worksRequired = field.surveyParamItemFieldValue;

                                #endregion

                                context.Detach(paAttribItem);
                            }
                            //end condition:If FieldValueId is empty/zero && item.ControlType != date
                            else if (item.controlType.ToUpper() == "Date".ToUpper())
                            {
                                //save all type of date in PA_PROPERTY_ITEM_DATES table
                                this.saveSurveyReplacementDates(survey, survData, item, field);
                            }
                        }//end for : surveyParamItemField
                    }//end if: surveyParamItemField.Count                 

                    #endregion
                }//end for: survey fields
            }

            #region Check Unsatisfactory Condition and Save (Insert/Update) Condition Works

            if (isUnsatisfactory && attributeId > 0 )
            {
                saveConditionWorks(attributeId, (int)survey.CompletedBy, survey.SurveyDate, worksRequired, conditionRatingValueId, componentIds, survData);
            }

            #endregion

        }
        #endregion

        #region Save Survey Replacement Dates
        /// <summary>
        /// This function 'll save the 
        /// </summary>
        /// <param name="survey"></param>
        /// <param name="survData"></param>

        private void saveSurveyReplacementDates(PS_Survey survey, SurveyData survData, SurveyFieldsData survFieldsData, SurveyParamItemFieldsData survItemsFieldsData)
        {

            PA_PROPERTY_ITEM_DATES paItemDates = new PA_PROPERTY_ITEM_DATES();
            PS_Survey_Item_Dates psItemDates = new PS_Survey_Item_Dates();


            //The commented code has been taken to a common place
            DateTime surveyParamItemFieldValue = Convert.ToDateTime(survItemsFieldsData.surveyParamItemFieldValue);

            IQueryable<PA_PROPERTY_ITEM_DATES> paItemDateResult;

            if (survFieldsData.surveyParamName == JsonConstants.LastReplaced || survFieldsData.surveyParamName == JsonConstants.ReplacementDue)
            {
                paItemDateResult = context.PA_PROPERTY_ITEM_DATES
                                          .Where(app => app.ItemId == survData.itemId 
                                                        && app.PROPERTYID == survData.propertyId 
                                                        && app.ParameterId == null
                                                        && ((survData.heatingId != null && survData.heatingId == app.HeatingMappingId)
                                                             || (survData.heatingId == null && app.HeatingMappingId == null))
                                                        )
                                          .OrderBy(app => app.SID);

                if (paItemDateResult.Count() > 0)
                {
                    paItemDates = paItemDateResult.First();
                }
                if (survFieldsData.surveyParamName == JsonConstants.LastReplaced)
                {
                    paItemDates.LastDone = surveyParamItemFieldValue;
                }
                else if (survFieldsData.surveyParamName == JsonConstants.ReplacementDue)
                {
                    //if parameter name is Replacement Due
                    paItemDates.DueDate = surveyParamItemFieldValue;
                }

            }
            else if (survFieldsData.surveyParamName == JsonConstants.RewireDue || survFieldsData.surveyParamName == JsonConstants.LastRewired)
            {
                paItemDateResult = (from pad in context.PA_PROPERTY_ITEM_DATES
                                    join par in context.PA_PARAMETER on pad.ParameterId equals par.ParameterID
                                    where pad.PROPERTYID == survData.propertyId && par.ParameterName == JsonConstants.LastRewired && pad.ItemId == survData.itemId
                                            && ((survData.heatingId != null && survData.heatingId == pad.HeatingMappingId)
                                                 || (survData.heatingId == null && pad.HeatingMappingId == null))
                                    select pad);
                //paItemDateResult = context.PA_PROPERTY_ITEM_DATES.Where(app => app.ItemId == survData.itemId && app.PROPERTYID == survData.propertyId && app.ParameterId == survFieldsData.surveyParameterId).OrderBy(app => app.SID);
                if (paItemDateResult.Count() > 0)
                {
                    paItemDates = paItemDateResult.First();
                }
                if (survFieldsData.surveyParamName == JsonConstants.LastRewired)
                {
                    paItemDates.LastDone = surveyParamItemFieldValue;
                    paItemDates.ParameterId = survFieldsData.surveyParameterId;
                }
                else if (survFieldsData.surveyParamName == JsonConstants.RewireDue)
                {
                    //if parameter name is Replacement Due
                    paItemDates.DueDate = surveyParamItemFieldValue;
                    paItemDates.ParameterId = this.getParameterId(JsonConstants.LastRewired);
                }

            }
            else if (survFieldsData.surveyParamName == JsonConstants.CurDue || survFieldsData.surveyParamName == JsonConstants.CurLastReplaced)
            {
                paItemDateResult = (from pad in context.PA_PROPERTY_ITEM_DATES
                                    join par in context.PA_PARAMETER on pad.ParameterId equals par.ParameterID
                                    where pad.PROPERTYID == survData.propertyId && par.ParameterName == JsonConstants.CurLastReplaced && pad.ItemId == survData.itemId
                                    && ((survData.heatingId != null && survData.heatingId == pad.HeatingMappingId)
                                         || (survData.heatingId == null && pad.HeatingMappingId == null))
                                    select pad);
                //paItemDateResult = context.PA_PROPERTY_ITEM_DATES.Where(app => app.ItemId == survData.itemId && app.PROPERTYID == survData.propertyId && app.ParameterId == survFieldsData.surveyParameterId).OrderBy(app => app.SID);
                if (paItemDateResult.Count() > 0)
                {
                    paItemDates = paItemDateResult.First();
                }
                if (survFieldsData.surveyParamName == JsonConstants.CurLastReplaced)
                {
                    paItemDates.LastDone = surveyParamItemFieldValue;
                    paItemDates.ParameterId = survFieldsData.surveyParameterId;
                }
                else if (survFieldsData.surveyParamName == JsonConstants.CurDue)
                {
                    //if parameter name is Replacement Due
                    paItemDates.DueDate = surveyParamItemFieldValue;
                    paItemDates.ParameterId = this.getParameterId(JsonConstants.CurLastReplaced);
                }

            }
            else if (survFieldsData.surveyParamName == JsonConstants.UpgradeLastDone || survFieldsData.surveyParamName == JsonConstants.ElectricUpgradeDue)
            {
                paItemDateResult = (from pad in context.PA_PROPERTY_ITEM_DATES
                                    join par in context.PA_PARAMETER on pad.ParameterId equals par.ParameterID
                                    where pad.PROPERTYID == survData.propertyId && par.ParameterName == JsonConstants.UpgradeLastDone && pad.ItemId == survData.itemId
                                    && ((survData.heatingId != null && survData.heatingId == pad.HeatingMappingId)
                                         || (survData.heatingId == null && pad.HeatingMappingId == null))
                                    select pad);
                //context.PA_PROPERTY_ITEM_DATES.Where(app => app.ItemId == survData.itemId && app.PROPERTYID == survData.propertyId && app.ParameterId == survFieldsData.surveyParameterId).OrderBy(app => app.SID);
                if (paItemDateResult.Count() > 0)
                {
                    paItemDates = paItemDateResult.First();
                }
                if (survFieldsData.surveyParamName == JsonConstants.UpgradeLastDone)
                {
                    paItemDates.LastDone = surveyParamItemFieldValue;
                    paItemDates.ParameterId = survFieldsData.surveyParameterId;
                }
                else if (survFieldsData.surveyParamName == JsonConstants.ElectricUpgradeDue)
                {
                    //if parameter name is Replacement Due
                    paItemDates.DueDate = surveyParamItemFieldValue;
                    paItemDates.ParameterId = this.getParameterId(JsonConstants.UpgradeLastDone);
                }
            }
            else
            {
                //for others only propertyid + itemid will make the record unique
                paItemDateResult = context.PA_PROPERTY_ITEM_DATES
                                          .Where(app => app.ItemId == survData.itemId 
                                                        && app.PROPERTYID == survData.propertyId 
                                                        && app.ParameterId == survFieldsData.surveyParameterId
                                                        && ((survData.heatingId != null && survData.heatingId == app.HeatingMappingId)
                                                             || (survData.heatingId == null && app.HeatingMappingId == null))
                                                        )
                                          .OrderBy(app => app.SID);
                if (paItemDateResult.Count() > 0)
                {
                    paItemDates = paItemDateResult.First();
                }

                paItemDates.DueDate = surveyParamItemFieldValue;
                paItemDates.ParameterId = survFieldsData.surveyParameterId;
            }
            if (ApplicationConstants.plannedComponentId > 0)
            {
                paItemDates.PLANNED_COMPONENTID = (short)ApplicationConstants.plannedComponentId;
            }
            paItemDates.UPDATEDBY = survData.customerId;
            paItemDates.UPDATEDON = DateTime.Now;

            if (paItemDates.HeatingMappingId == null && survData.heatingId != null)
            {
                paItemDates.HeatingMappingId = survData.heatingId;
            }
            

            if (paItemDateResult.Count() <= 0)
            {
                paItemDates.PROPERTYID = survData.propertyId;
                paItemDates.ItemId = survData.itemId;

                context.AddToPA_PROPERTY_ITEM_DATES(paItemDates);
            }
            context.SaveChanges();
            context.Detach(paItemDates);

            //}

            IQueryable<PS_Survey_Item_Dates> psItemDateResult;

            //If these parameters are of electric then query will be different. Property , itemid and paramter id will make the unique record in this case
            //if (survFieldsData.surveyParamName == JsonConstants.LastReplaced || survFieldsData.surveyParamName == JsonConstants.ReplacementDue)
            //{
            //    psItemDateResult = context.PS_Survey_Item_Dates.Where(app => app.ItemId == survData.itemId && app.SurveyId == survey.SurveyId && app.ParameterId == survFieldsData.surveyParameterId).OrderBy(app => app.SurveyId);

            //    if (psItemDateResult.Count() > 0)
            //    {
            //        psItemDates = psItemDateResult.First();
            //    }
            //    else
            //    {
            //        psItemDates.SurveyId = survey.SurveyId;
            //        psItemDates.ItemId = survData.itemId;
            //        psItemDates.ParameterId = survFieldsData.surveyParameterId;
            //    }

            //    psItemDates.LastDone = surveyParamItemFieldValue;

            //    if (psItemDateResult.Count() == 0)
            //    {
            //        context.AddToPS_Survey_Item_Dates(psItemDates);
            //    }

            //    context.SaveChanges();
            //    context.Detach(psItemDates);
            //}
            //else
            //{
            //    //Added by Behroz - Start
            //psItemDateResult = context.PS_Survey_Item_Dates.Where(app => app.ItemId == survData.itemId && app.SurveyId == survey.SurveyId).OrderBy(app => app.SurveyId);
            if (survFieldsData.surveyParamName == JsonConstants.LastReplaced || survFieldsData.surveyParamName == JsonConstants.ReplacementDue)
            {
                psItemDateResult = context.PS_Survey_Item_Dates
                                          .Where(app => app.ItemId == survData.itemId 
                                                        && app.SurveyId == survey.SurveyId 
                                                        && app.ParameterId == null
                                                        && ((survData.heatingId != null && survData.heatingId == app.HeatingMappingId)
                                                             || (survData.heatingId == null && app.HeatingMappingId == null))
                                                        )
                                          .OrderBy(app => app.SurveyId);

            }
            else
            {
                psItemDateResult = context.PS_Survey_Item_Dates
                                          .Where(app => app.ItemId == survData.itemId 
                                                        && app.SurveyId == survey.SurveyId 
                                                        && app.ParameterId == survFieldsData.surveyParameterId
                                                        && ((survData.heatingId != null && survData.heatingId == app.HeatingMappingId)
                                                             || (survData.heatingId == null && app.HeatingMappingId == null))
                                                        )
                                          .OrderBy(app => app.SurveyId);
            }

            if (psItemDateResult.Count() > 0)
            {
                psItemDates = psItemDateResult.First();
            }
            else
            {
                psItemDates.SurveyId = survey.SurveyId;
                psItemDates.ItemId = survData.itemId;                
            }

            if (psItemDates.HeatingMappingId == null && survData.heatingId != null)
            {
                psItemDates.HeatingMappingId = survData.heatingId;
            }

            //if parameter name is Last Replaced
            //if (JsonConstants.LastReplaced == survFieldsData.surveyParamName)
            if (survFieldsData.surveyParamName == JsonConstants.LastReplaced)
            {
                psItemDates.LastDone = surveyParamItemFieldValue;
            }

            //else if (JsonConstants.ReplacementDue == survFieldsData.surveyParamName)
            else if (survFieldsData.surveyParamName == JsonConstants.ReplacementDue)
            {
                //if parameter name is Replacement Due
                psItemDates.DueDate = surveyParamItemFieldValue;
            }
            else
            {
                paItemDates.LastDone = surveyParamItemFieldValue;
                if (survFieldsData.surveyParameterId != 0)
                    psItemDates.ParameterId = survFieldsData.surveyParameterId;
            }

            if (psItemDateResult.Count() == 0)
            {
                context.AddToPS_Survey_Item_Dates(psItemDates);
            }

            context.SaveChanges();
            context.Detach(psItemDates);
            // }


        }
        #endregion

        #region Save Survey Item Dates

        //Added by Behroz - Start
        /// <summary>
        /// 
        /// </summary>
        /// <param name="survey"></param>
        /// <param name="survData"></param>
        /// <param name="survFieldsData"></param>
        /// <param name="survItemsFieldsData"></param>
        private void saveSurveyItemDates(PS_Survey survey, SurveyData survData, SurveyFieldsData survFieldsData, SurveyParamItemFieldsData survItemsFieldsData)
        {
            PS_Survey_Parameters surveyFields = new PS_Survey_Parameters();
            PA_PROPERTY_ATTRIBUTES paAttribItem = new PA_PROPERTY_ATTRIBUTES();

            //Convert date
            DateTime surveyParamItemFieldValue = Convert.ToDateTime(survItemsFieldsData.surveyParamItemFieldValue);

            IQueryable<PS_Survey_Parameters> oldSurveyFields;
            oldSurveyFields = context.PS_Survey_Parameters.Where(app => app.SurveyId == survey.SurveyId && app.ItemParamId == survFieldsData.surveyItemParamId);

            if (oldSurveyFields.Count() > 0)
            {
                surveyFields = oldSurveyFields.First();
            }
            else
            {
                surveyFields.SurveyId = survey.SurveyId;
                surveyFields.ItemParamId = survFieldsData.surveyItemParamId;
            }

            //surveyFields.PARAMETERVALUE = survItemsFieldsData.surveyParamItemFieldValue;
            surveyFields.PARAMETERVALUE = surveyParamItemFieldValue.ToString();

            if (oldSurveyFields.Count() <= 0)
            {
                context.AddToPS_Survey_Parameters(surveyFields);
            }

            context.SaveChanges();
            context.Detach(surveyFields);


            //search if field value already exist in PA_PROPERTY_ATTRIBUTES
            var paAttrib = context.PA_PROPERTY_ATTRIBUTES.Where(prop => prop.PROPERTYID == survData.propertyId && prop.ITEMPARAMID == survFieldsData.surveyItemParamId);

            //if field is checkbox then surveyParamItemField will have multiple values and 
            //we will add one more and condition to match valueid, because we 'll have to update all checkbox values
            if (survFieldsData.surveyParamItemField.Count() > 1)
            {
                paAttrib = context.PA_PROPERTY_ATTRIBUTES.Where(prop => prop.PROPERTYID == survData.propertyId && prop.ITEMPARAMID == survFieldsData.surveyItemParamId && prop.VALUEID == survItemsFieldsData.surveyPramItemFieldId);
            }

            if (paAttrib.Count() > 0)
            {
                //fetch the already existed record
                paAttribItem = paAttrib.First();
            }
            else
            {
                //if record does not already exist the set the property id and itemparamid
                paAttribItem.PROPERTYID = survey.PROPERTYID;
                paAttribItem.ITEMPARAMID = survFieldsData.surveyItemParamId;
            }

            //paAttribItem.PARAMETERVALUE = survItemsFieldsData.surveyParamItemFieldValue;
            paAttribItem.PARAMETERVALUE = surveyParamItemFieldValue.ToString();
            paAttribItem.UPDATEDBY = survey.CompletedBy;
            paAttribItem.UPDATEDON = survey.SurveyDate;

            //if previous record does not already exist
            if (paAttrib.Count() <= 0)
            {
                //insert the record
                context.AddToPA_PROPERTY_ATTRIBUTES(paAttribItem);
            }

            context.SaveChanges();
            context.Detach(paAttribItem);
        }
        //Added by Behroz - End
        #endregion

        #region Save Survey Replacement Dates
        /// <summary>
        /// This function 'll save the 
        /// </summary>
        /// <param name="survey"></param>
        /// <param name="survData"></param>

        //private void saveSurveyReplacementDates(PS_Survey survey, SurveyData survData, SurveyFieldsData survFieldsData, SurveyParamItemFieldsData survItemsFieldsData)
        //{
        //    DateTime? dueDate = new DateTime();
        //    DateTime? lastDone = new DateTime();

        //    PA_PROPERTY_ITEM_DATES paItemDates = new PA_PROPERTY_ITEM_DATES();
        //    PS_Survey_Item_Dates psItemDates = new PS_Survey_Item_Dates();


        //    //The commented code has been taken to a common place
        //    DateTime surveyParamItemFieldValue = Convert.ToDateTime(survItemsFieldsData.surveyParamItemFieldValue);// FileHelper.convertDate(survItemsFieldsData);

        //    //string dateString = survItemsFieldsData.surveyParamItemFieldValue.Replace("/Date(", "").Replace(")/", "");
        //    //int plusPosition = 0;
        //    //if (dateString.Contains("+"))
        //    //{
        //    //    plusPosition = dateString.IndexOf("+");
        //    //}
        //    //else if (dateString.Contains("-"))
        //    //{
        //    //    plusPosition = dateString.IndexOf("-");
        //    //}
        //    //double seconds = double.Parse(dateString.Substring(0, plusPosition)) / 1000;            
        //    //TimeZone zone = TimeZone.CurrentTimeZone;
        //    //// Get offset.
        //    //double offset = double.Parse(zone.GetUtcOffset(DateTime.Now).ToString().Substring(0, 2));
        //    //DateTime surveyParamItemFieldValue = new DateTime(1970, 1, 1, 0, 0, 0, 0).AddSeconds(seconds).AddHours(offset);
        //    //Added by Behroz - End

        //    dueDate = DateTime.Now;
        //    lastDone = DateTime.Now;

        //    IQueryable<PA_PROPERTY_ITEM_DATES> paItemDateResult;

        //    //If these parameters are of electric then query will be different. Property , itemid and paramter id will make the unique record in this case
        //    if (survFieldsData.surveyParameterId == JsonConstants.LastRewiredId || survFieldsData.surveyParameterId == JsonConstants.ConsumerUnitReplacementId)
        //    {
        //        paItemDateResult = context.PA_PROPERTY_ITEM_DATES.Where(app => app.ItemId == survData.itemId && app.PROPERTYID == survData.propertyId && app.ParameterId == survFieldsData.surveyParameterId).OrderBy(app => app.SID);

        //        if (paItemDateResult.Count() > 0)
        //        {
        //            paItemDates = paItemDateResult.First();
        //            //save due date in local variable, it will be used to store in ps_survey_item_dates
        //            dueDate = paItemDates.DueDate;

        //            paItemDates.LastDone = surveyParamItemFieldValue;
        //            paItemDates.UPDATEDBY = survData.completedBy;
        //            paItemDates.UPDATEDON = DateTime.Now;
        //            context.SaveChanges();
        //            context.Detach(paItemDates);
        //        }
        //    }
        //    else if (survFieldsData.surveyParameterId != JsonConstants.LastRewiredId || survFieldsData.surveyParameterId != JsonConstants.ConsumerUnitReplacementId)
        //    {
        //        //If the parameterid is among the values mentioned in the config file then use the following query
        //        if (survFieldsData.surveyParameterId == JsonConstants.WcLastReplacedId || survFieldsData.surveyParameterId == JsonConstants.WcReplacementDueId ||
        //            survFieldsData.surveyParameterId == JsonConstants.HandBasinLastReplacedId || survFieldsData.surveyParameterId == JsonConstants.HandBasinReplacementDueId ||
        //            survFieldsData.surveyParameterId == JsonConstants.MainDwellingRoofLastReplacedId || survFieldsData.surveyParameterId == JsonConstants.MainDwellingRoofReplacementDueId)
        //        {
        //            paItemDateResult = context.PA_PROPERTY_ITEM_DATES.Where(app => app.ItemId == survData.itemId && app.PROPERTYID == survData.propertyId && app.ParameterId == survFieldsData.surveyParameterId).OrderBy(app => app.SID);
        //        }
        //        else
        //        {
        //            //for others only propertyid + itemid will make the record unique
        //            paItemDateResult = context.PA_PROPERTY_ITEM_DATES.Where(app => app.ItemId == survData.itemId && app.PROPERTYID == survData.propertyId).OrderBy(app => app.SID);
        //        }

        //        if (paItemDateResult.Count() > 0)
        //        {
        //            paItemDates = paItemDateResult.First();
        //        }

        //        //temporarily save due date in local variable, it will be used to save in ps_survey_item_dates
        //        dueDate = paItemDates.DueDate;

        //        //if parameter name is Last Replaced
        //        //if (JsonConstants.LastReplaced == survFieldsData.surveyParamName)
        //        if (survFieldsData.surveyParamName.Contains(JsonConstants.LastReplaced))
        //        {
        //            paItemDates.LastDone = surveyParamItemFieldValue;
        //        }
        //        else if (survFieldsData.surveyParamName.Contains(JsonConstants.ReplacementDue))
        //        {
        //            //if parameter name is Replacement Due
        //            paItemDates.DueDate = surveyParamItemFieldValue;
        //        }

        //        paItemDates.UPDATEDBY = survData.completedBy;
        //        paItemDates.UPDATEDON = DateTime.Now;

        //        if (paItemDateResult.Count() <= 0)
        //        {
        //            paItemDates.PROPERTYID = survData.propertyId;
        //            paItemDates.ItemId = survData.itemId;
        //            paItemDates.ParameterId = survFieldsData.surveyParameterId;

        //            context.AddToPA_PROPERTY_ITEM_DATES(paItemDates);
        //        }
        //        context.SaveChanges();
        //        context.Detach(paItemDates);

        //        //else if (survData.itemId == JsonConstants.StructureId || survData.itemId == JsonConstants.CloakroomId || survData.itemId == JsonConstants.FasciasId || survData.itemId == JsonConstants.SurfaceId)
        //        //{                    
        //        //    paItemDates.PROPERTYID = survData.propertyId;
        //        //    paItemDates.ItemId = survData.itemId;
        //        //    paItemDates.UPDATEDON = DateTime.Now;
        //        //    paItemDates.UPDATEDBY = survData.completedBy;

        //        //    if (JsonConstants.LastReplaced == survFieldsData.surveyParamName)
        //        //    {
        //        //        paItemDates.LastDone = surveyParamItemFieldValue;
        //        //    }
        //        //    else if (JsonConstants.ReplacementDue == survFieldsData.surveyParamName)
        //        //    {
        //        //        //if parameter name is Replacement Due
        //        //        paItemDates.DueDate = surveyParamItemFieldValue;
        //        //    }

        //        //    context.AddToPA_PROPERTY_ITEM_DATES(paItemDates);
        //        //    context.SaveChanges();
        //        //    //context.Detach(paItemDates);
        //        //}

        //    }

        //    IQueryable<PS_Survey_Item_Dates> psItemDateResult;

        //    //If these parameters are of electric then query will be different. Property , itemid and paramter id will make the unique record in this case
        //    if (survFieldsData.surveyParameterId == JsonConstants.LastRewiredId || survFieldsData.surveyParameterId == JsonConstants.ConsumerUnitReplacementId)
        //    {
        //        psItemDateResult = context.PS_Survey_Item_Dates.Where(app => app.ItemId == survData.itemId && app.SurveyId == survey.SurveyId && app.ParameterId == survFieldsData.surveyParameterId).OrderBy(app => app.SurveyId);

        //        if (psItemDateResult.Count() > 0)
        //        {
        //            psItemDates = psItemDateResult.First();
        //        }
        //        else
        //        {
        //            psItemDates.SurveyId = survey.SurveyId;
        //            psItemDates.ItemId = survData.itemId;
        //            psItemDates.ParameterId = survFieldsData.surveyParameterId;
        //        }

        //        psItemDates.LastDone = surveyParamItemFieldValue;

        //        if (psItemDateResult.Count() == 0)
        //        {
        //            context.AddToPS_Survey_Item_Dates(psItemDates);
        //        }

        //        context.SaveChanges();
        //        context.Detach(psItemDates);
        //    }
        //    else
        //    {

        //        //psItemDateResult = context.PS_Survey_Item_Dates.Where(app => app.ItemId == survData.itemId && app.SurveyId == survey.SurveyId).OrderBy(app => app.SurveyId);
        //        if (survFieldsData.surveyParameterId == JsonConstants.WcLastReplacedId || survFieldsData.surveyParameterId == JsonConstants.WcReplacementDueId ||
        //            survFieldsData.surveyParameterId == JsonConstants.HandBasinLastReplacedId || survFieldsData.surveyParameterId == JsonConstants.HandBasinReplacementDueId ||
        //            survFieldsData.surveyParameterId == JsonConstants.MainDwellingRoofLastReplacedId || survFieldsData.surveyParameterId == JsonConstants.MainDwellingRoofReplacementDueId)
        //        {
        //            psItemDateResult = context.PS_Survey_Item_Dates.Where(app => app.ItemId == survData.itemId && app.SurveyId == survey.SurveyId && app.ParameterId == survFieldsData.surveyParameterId).OrderBy(app => app.SurveyId);
        //        }
        //        else
        //        {

        //            psItemDateResult = context.PS_Survey_Item_Dates.Where(app => app.ItemId == survData.itemId && app.SurveyId == survey.SurveyId).OrderBy(app => app.SurveyId);
        //        }

        //        if (psItemDateResult.Count() > 0)
        //        {
        //            psItemDates = psItemDateResult.First();
        //        }
        //        else
        //        {
        //            psItemDates.SurveyId = survey.SurveyId;
        //            psItemDates.ItemId = survData.itemId;

        //            if (survFieldsData.surveyParameterId != 0)
        //                psItemDates.ParameterId = survFieldsData.surveyParameterId;

        //        }


        //        //if parameter name is Last Replaced
        //        //if (JsonConstants.LastReplaced == survFieldsData.surveyParamName)
        //        if (survFieldsData.surveyParamName.Contains(JsonConstants.LastReplaced))
        //        {
        //            psItemDates.LastDone = surveyParamItemFieldValue;
        //        }

        //        //else if (JsonConstants.ReplacementDue == survFieldsData.surveyParamName)
        //        else if (survFieldsData.surveyParamName.Contains(JsonConstants.ReplacementDue))
        //        {
        //            //if parameter name is Replacement Due
        //            psItemDates.DueDate = surveyParamItemFieldValue;
        //        }

        //        if (psItemDateResult.Count() > 0)
        //        {
        //            context.SaveChanges();
        //            context.Detach(psItemDates);

        //        }
        //        else
        //        {
        //            context.AddToPS_Survey_Item_Dates(psItemDates);
        //            context.SaveChanges();
        //            context.Detach(psItemDates);
        //        }
        //    }


        //}
        #endregion
        
        #region is Appointment For Property Exist
        /// <summary>
        /// This function check that either specific appointment exists against the proerpty.
        /// </summary>
        /// <param name="propertyId">property id</param>
        /// <param name="customerId">customer id</param>
        /// <param name="appointmentId">appointment id</param>
        /// <returns>true or false</returns>
        public bool isAppointmentForPropertyExist(string propertyId, int appointmentId, PropertySurvey_Entities currentContext = null)
        {
            if (currentContext != null) this.context = currentContext;
            bool success = false;

            var apptRecord = (from appt in context.PS_Property2Appointment
                              where appt.PropertyId.ToLower() == propertyId.ToLower()
                                && appt.AppointId == appointmentId
                              select appt
                            );

            if (apptRecord.Count() > 0)
            {
                success = true;
            }
            else
            {
                success = false;
            }

            return success;

        }
        #endregion

        #region OLD Functions

        #region get All Saved Survey Forms
        /// <summary>
        /// this funciton returns all the saved survey forms against appointment id
        /// </summary>
        /// <param name="appointmentId">appointment id</param>        
        /// <returns>it returns the list of saved forms data of property survey form</returns>
        public List<SurveyData> getAllSavedSurveyForms(int appointmentId)
        {
            List<SurveyData> survData = new List<SurveyData>();
            List<SurveyData> allFormsData = new List<SurveyData>();
            return allFormsData;
        }
        #endregion

        #endregion

        #region Get ParameterId by ParameterName

        public int getParameterId(string parameterName)
        {
            return context.PA_PARAMETER.Where(item => item.ParameterName.ToLower() == parameterName.ToLower()).FirstOrDefault().ParameterID;
        }

        #endregion

        #region Get SurveyId by AppointmentId

        public int getSurveyId(int appointmentId, PropertySurvey_Entities currentContext = null)
        {
            if (currentContext != null) this.context = currentContext;
            var result = context.PS_Appointment2Survey.Where(app => app.AppointId == appointmentId);
            if (result.Count() > 0)
            {
                return result.FirstOrDefault().SurveyId;
            }
            else
            {
                return 0;
            }

        }

        #endregion

        #region Save Condition Works (Insert/Update)

        private void saveConditionWorks(int attributeId, int completedBy, DateTime? surveyDate, string worksRequired, int valueId, List<int> componentIds, SurveyData survData)
        {
            #region Get action id for condition works

            //Get Condition works actionId where action is "Recommended"
            int actionId = 0;

            var actions = from action in context.PLANNED_Action
                          where action.Title.Equals("Recommended")
                          select action;

            if (actions.Count() > 0)
                actionId = actions.First().ActionId;

            #endregion

            // As componentId is optional in database design, so we are using a null able variable
            int? componentId = null;

            // To Control iteration of do-while loop and get componentId at the current index i.e 1,2,3 etc.
            int componentsIndex = 0;

            //Loop to insert more than one component or add at least one condition work with null/no componentId
            do
            {
                //Get Current Component Id, as there can be more than one component, so we have to loop through these one by one
                if (componentIds != null && componentIds.Count > 0)
                    componentId = (int?)componentIds.ElementAt(componentsIndex);

                #region Add/Update Condition Works and Insert in Condition Works History

                #region Insert/Update Condition Works record

                PLANNED_CONDITIONWORKS conditionWorks;

                var conditionWorksList = from cw in context.PLANNED_CONDITIONWORKS
                                         where cw.AttributeId == attributeId && cw.ComponentId == componentId
                                                && ((survData.heatingId != null && survData.heatingId == cw.HeatingMappingId)
                                                || (survData.heatingId == null && cw.HeatingMappingId == null))

                                         select cw;
                if (conditionWorksList.Count() > 0)
                {
                    conditionWorks = conditionWorksList.First();

                    conditionWorks.ModifiedBy = completedBy; //survData.completedBy;
                    conditionWorks.ModifiedDate = surveyDate; //survData.surveyDate;
                }
                else
                {
                    conditionWorks = new PLANNED_CONDITIONWORKS();

                    conditionWorks.CreatedBy = completedBy; //survData.completedBy;
                    conditionWorks.CreatedDate = surveyDate ?? DateTime.Now;//survData.surveyDate ?? DateTime.Now;
                }

                if (conditionWorks.HeatingMappingId == null && survData.heatingId != null)
                {
                    conditionWorks.HeatingMappingId = survData.heatingId;
                }

                conditionWorks.WorksRequired = worksRequired??String.Empty;
                conditionWorks.AttributeId = attributeId;
                conditionWorks.ComponentId = (short?)componentId;
                conditionWorks.ConditionAction = actionId;

                if (!(conditionWorksList.Count() > 0))
                    context.AddToPLANNED_CONDITIONWORKS(conditionWorks);

                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);

                #endregion

                #region Insert in Condition Works History

                var history = new PLANNED_CONDITIONWORKS_HISTORY();
                history.AttributeId = conditionWorks.AttributeId;
                history.ComponentId = conditionWorks.ComponentId;
                history.ConditionAction = conditionWorks.ConditionAction;
                history.ConditionWorksId = conditionWorks.ConditionWorksId;
                history.CreatedBy = completedBy; //survData.completedBy;
                history.CreatedDate = surveyDate ?? DateTime.Now;
                history.JournalId = conditionWorks.JournalId;
                history.ValueId = valueId;
                history.WorksRequired = conditionWorks.WorksRequired;

                if (history.HeatingMappingId == null && survData.heatingId != null)
                {
                    history.HeatingMappingId = survData.heatingId;
                }

                context.AddToPLANNED_CONDITIONWORKS_HISTORY(history);

                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);

                #endregion

                #endregion

                //Increase the index to get next component or violate the condition and exit the loop.
                componentsIndex++;
            } while (componentsIndex < componentIds.Count);
        }

        #endregion
    }
}
