﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Transactions;
using PropSurvey.Contracts.Data;
using PropSurvey.Dal.Base;
using PropSurvey.Entities;
using System.IO;

namespace PropSurvey.Dal.Faults
{
    public class FaultsDal : BaseDal
    {
        #region get Faults Data
        /// <summary>
        /// This function returns the Faults data
        /// </summary>
        /// <returns>List of Faults data objects</returns>

        public List<FaultsData> getFaultsData(int AppointmentID)
        {
            var _var = (from fault in context.GS_GasFault
                        where fault.AppointmentID == AppointmentID
                        select new FaultsData
                       {
                           ID = fault.ID,
                           AppointmentID = fault.AppointmentID,
                           DefectDesc = fault.DefectDesc,
                           FaultCategory = fault.FaultCategory,
                           isAdviceNoteIssued = fault.isAdviceNoteIssued,
                           RemedialAction = fault.RemedialAction,
                           SerialNo = fault.SerialNo,
                           WarningTagFixed = fault.WarningTagFixed

                       });

            List<FaultsData> faults = new List<FaultsData>();
            if (_var.Count() > 0)
            {
                faults = _var.ToList();
            }

            return faults;
        }
        #endregion

        #region get Faults Data Gas

        /// <summary>
        /// This function returns the Faults data
        /// </summary>
        /// <returns>List of Faults data objects</returns>
        public List<FaultsDataGas> getFaultsDataGas(int applianceId, string propertyId)
        {
            var data = from defects in context.P_PROPERTY_APPLIANCE_DEFECTS
                       where defects.ApplianceId == applianceId && defects.PropertyId == propertyId
                       select new FaultsDataGas
                       {
                           RemedialAction = defects.ActionNotes,
                           ApplianceID = defects.ApplianceId,
                           FaultCategory = defects.CategoryId,
                           DefectDate = defects.DefectDate,
                           DefectDesc = defects.DefectNotes,
                           IsActionTaken = defects.IsActionTaken,
                           IsDefectIdentified = defects.IsDefectIdentified,
                           WarningTagFixed = defects.IsWarningFixed,
                           isAdviceNoteIssued = defects.IsWarningIssued,
                           JournalId = defects.JournalId,
                           ID = defects.PropertyDefectId,
                           propertyId = defects.PropertyId,
                           SerialNo = defects.SerialNumber
                       };


            List<FaultsDataGas> faults = new List<FaultsDataGas>();
            if (data.Count() > 0)
            {
                faults = data.ToList();
            }

            return faults;
        }



        #endregion

        #region fetch Faults Data Gas

        /// <summary>
        /// This function returns the Faults data
        /// </summary>
        /// <returns>List of Faults data objects</returns>
        public List<FaultsDataGas> fetchFaultsDataGas(int journalId)
        {
            var data = from defects in context.P_PROPERTY_APPLIANCE_DEFECTS
                       where defects.JournalId == journalId
                       select new FaultsDataGas
                       {
                           RemedialAction = defects.ActionNotes,
                           ApplianceID = defects.ApplianceId,
                           FaultCategory = defects.CategoryId,
                           DefectDate = defects.DefectDate,
                           DefectDesc = defects.DefectNotes,
                           IsActionTaken = defects.IsActionTaken,
                           IsDefectIdentified = defects.IsDefectIdentified,
                           WarningTagFixed = defects.IsWarningFixed,
                           isAdviceNoteIssued = defects.IsWarningIssued,
                           JournalId = defects.JournalId,
                           ID = defects.PropertyDefectId,
                           propertyId = defects.PropertyId,
                           SerialNo = defects.SerialNumber
                       };


            List<FaultsDataGas> faults = new List<FaultsDataGas>();
            if (data.Count() > 0)
            {
                faults = data.ToList();
            }

            return faults;
        }



        #endregion

        #region get detector fault list

        /// <summary>
        /// This function returns the Faults data
        /// </summary>
        /// <returns>List of Faults data objects</returns>
        public List<FaultsDataGas> getDetectorDefectList(int DetectorTypeId, string propertyId)
        {
            var data = from defects in context.P_PROPERTY_APPLIANCE_DEFECTS
                       where defects.DetectorTypeId == DetectorTypeId && defects.PropertyId == propertyId
                       select new FaultsDataGas
                       {
                           RemedialAction = defects.ActionNotes,
                           ApplianceID = defects.ApplianceId,
                           FaultCategory = defects.CategoryId,
                           DefectDate = defects.DefectDate,
                           DefectDesc = defects.DefectNotes,
                           IsActionTaken = defects.IsActionTaken,
                           IsDefectIdentified = defects.IsDefectIdentified,
                           WarningTagFixed = defects.IsWarningFixed,
                           isAdviceNoteIssued = defects.IsWarningIssued,
                           JournalId = defects.JournalId,
                           ID = defects.PropertyDefectId,
                           propertyId = defects.PropertyId,
                           SerialNo = defects.SerialNumber,
                           detectorTypeId = defects.DetectorTypeId
                       };


            List<FaultsDataGas> faults = new List<FaultsDataGas>();
            if (data.Count() > 0)
            {
                faults = data.ToList();
            }

            return faults;
        }



        #endregion

        #region fetch detector fault list

        /// <summary>
        /// This function returns the Faults data
        /// </summary>
        /// <returns>List of Faults data objects</returns>
        public List<FaultsDataGas> fetchDetectorDefectList(int journalId)
        {
            var data = from defects in context.P_PROPERTY_APPLIANCE_DEFECTS
                       join detector in context.AS_DetectorType on defects.DetectorTypeId equals detector.DetectorTypeId
                       where defects.JournalId == journalId
                       select new FaultsDataGas
                       {
                           RemedialAction = defects.ActionNotes,
                           ApplianceID = defects.ApplianceId,
                           FaultCategory = defects.CategoryId,
                           DefectDate = defects.DefectDate,
                           DefectDesc = defects.DefectNotes,
                           IsActionTaken = defects.IsActionTaken,
                           IsDefectIdentified = defects.IsDefectIdentified,
                           WarningTagFixed = defects.IsWarningFixed,
                           isAdviceNoteIssued = defects.IsWarningIssued,
                           JournalId = defects.JournalId,
                           ID = defects.PropertyDefectId,
                           propertyId = defects.PropertyId,
                           SerialNo = defects.SerialNumber,
                           detectorTypeId = defects.DetectorTypeId
                       };


            List<FaultsDataGas> faults = new List<FaultsDataGas>();
            if (data.Count() > 0)
            {
                faults = data.ToList();
            }

            return faults;
        }



        #endregion

        #region save Faults Data

        /// <summary>
        /// This function saves the Faults Data in the database
        /// </summary>
        /// <param name="appData">The object of Faults Data</param>
        /// <returns>the Faults id if successfully save otherwise 0 </returns>
        public int saveFaultsData(FaultsData faultData)
        {
            bool success = false;

            GS_GasFault fault = new GS_GasFault();
            using (TransactionScope trans = new TransactionScope())
            {
                fault.AppointmentID = faultData.AppointmentID;
                fault.WarningTagFixed = faultData.WarningTagFixed;
                fault.SerialNo = faultData.SerialNo;
                fault.RemedialAction = faultData.RemedialAction;
                fault.isAdviceNoteIssued = faultData.isAdviceNoteIssued;
                fault.FaultCategory = faultData.FaultCategory;
                fault.DefectDesc = faultData.DefectDesc;

                context.AddToGS_GasFault(fault);
                context.SaveChanges();
                trans.Complete();
                success = true;
            }

            if (success == true)
            {
                return fault.ID;
            }
            else
            {
                return 0;
            }
        }

        #endregion

        #region save Faults Data Gas
        /// <summary>
        /// This function saves the Faults Data in the database for gas
        /// </summary>
        /// <param name="appData">The object of Faults Data</param>
        /// <returns>the Faults id if successfully save otherwise 0 </returns>
        public int saveFaultsDataGas(FaultsDataGas faultData)
        {
            bool success = false;
            P_PROPERTY_APPLIANCE_DEFECTS fault = new P_PROPERTY_APPLIANCE_DEFECTS();

            using (TransactionScope trans = new TransactionScope())
            {
                fault.ActionNotes = faultData.RemedialAction;
                fault.ApplianceId = faultData.ApplianceID;
                fault.CategoryId = faultData.FaultCategory;
                fault.DefectDate = faultData.DefectDate;
                fault.DefectNotes = faultData.DefectDesc;
                fault.IsActionTaken = faultData.IsActionTaken;
                fault.IsDefectIdentified = faultData.IsDefectIdentified;
                fault.IsWarningFixed = faultData.WarningTagFixed;
                fault.IsWarningIssued = faultData.isAdviceNoteIssued;
                fault.JournalId = faultData.JournalId;
                fault.PropertyId = faultData.propertyId;
                fault.SerialNumber = faultData.SerialNo;
                fault.DetectorTypeId = faultData.detectorTypeId;
                context.AddToP_PROPERTY_APPLIANCE_DEFECTS(fault);
                context.SaveChanges();
                trans.Complete();
                success = true;
            }

            if (success == true)
            {
                return fault.PropertyDefectId;
            }
            else
            {
                return 0;
            }
        }


        #endregion

        #region update fault Data

        /// <summary>
        /// This function updates fault Data in the database
        /// </summary>
        /// <param name="appData">The object of fault Data</param>
        /// <returns>true if successfully save otherwise 0 </returns>
        public bool updateFaultData(FaultsData faultData)
        {
            bool success = false;
            using (TransactionScope trans = new TransactionScope())
            {
                var _var = (from ins in context.GS_GasFault
                            where ins.ID == faultData.ID
                            select ins);
                if (_var.Count() > 0)
                {
                    GS_GasFault fault = _var.FirstOrDefault();
                    fault.FaultCategory = faultData.FaultCategory;
                    fault.DefectDesc = faultData.DefectDesc;
                    fault.AppointmentID = faultData.AppointmentID;
                    fault.isAdviceNoteIssued = faultData.isAdviceNoteIssued;
                    fault.RemedialAction = faultData.RemedialAction;
                    fault.SerialNo = faultData.SerialNo;
                    fault.WarningTagFixed = faultData.WarningTagFixed;

                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    trans.Complete();
                    success = true;
                }
                else
                {
                    success = false;
                }

            }

            return success;
        }

        /// <summary>
        /// This function updates fault Data in the database for gas
        /// </summary>
        /// <param name="appData">The object of fault Data</param>
        /// <returns>true if successfully save otherwise 0 </returns>
        public int updateFaultDataGas(FaultsDataGas faultData, PropertySurvey_Entities currentContext = null)
        {
            if (currentContext != null) this.context = currentContext;

            bool success = false;
            P_PROPERTY_APPLIANCE_DEFECTS fault = new P_PROPERTY_APPLIANCE_DEFECTS();

            var _var = (from ins in context.P_PROPERTY_APPLIANCE_DEFECTS
                        where ins.PropertyDefectId == faultData.ID
                        select ins);

            //To update property appliance defect
            if (_var.Count() > 0)
            {
                fault = _var.FirstOrDefault();
                fault.ActionNotes = faultData.RemedialAction;
                fault.ApplianceId = faultData.ApplianceID;
                fault.CategoryId = faultData.FaultCategory;
                //Ticket # 6833
                //fault.DefectDate = faultData.DefectDate;
                fault.DefectNotes = faultData.DefectDesc;
                fault.IsActionTaken = faultData.IsActionTaken;
                fault.IsDefectIdentified = faultData.IsDefectIdentified;
                fault.IsWarningFixed = faultData.WarningTagFixed;
                fault.IsWarningIssued = faultData.isAdviceNoteIssued;
                fault.JournalId = faultData.JournalId;
                fault.PropertyId = faultData.propertyId;
                fault.SerialNumber = faultData.SerialNo;
                fault.DetectorTypeId = faultData.detectorTypeId;
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                success = true;
            }
            //To Add new property appliance defect
            else
            {
                fault.ActionNotes = faultData.RemedialAction;
                fault.ApplianceId = faultData.ApplianceID;
                fault.CategoryId = faultData.FaultCategory;
                fault.DefectDate = faultData.DefectDate;
                fault.DefectNotes = faultData.DefectDesc;
                fault.IsActionTaken = faultData.IsActionTaken;
                fault.IsDefectIdentified = faultData.IsDefectIdentified;
                fault.IsWarningFixed = faultData.WarningTagFixed;
                fault.IsWarningIssued = faultData.isAdviceNoteIssued;
                fault.JournalId = faultData.JournalId;
                fault.PropertyId = faultData.propertyId;
                fault.SerialNumber = faultData.SerialNo;
                fault.DetectorTypeId = faultData.detectorTypeId;
                context.AddToP_PROPERTY_APPLIANCE_DEFECTS(fault);
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                success = true;
            }

            if (currentContext == null) context.AcceptAllChanges();

            if (success == true)
            {
                return fault.PropertyDefectId;
            }
            else
            {
                return 0;
            }
        }

        #endregion

        #region get General Comments
        /// <summary>
        /// This function returns General Comments
        /// </summary>
        /// <returns>List of General Comments</returns>
        public List<FaultsData> getGeneralComments(int AppointmentID)
        {
            var _var = (from fault in context.GS_GasFault
                        where fault.AppointmentID == AppointmentID && fault.FaultCategory.ToLower() == "general comment"
                        select new FaultsData
                        {
                            ID = fault.ID,
                            AppointmentID = fault.AppointmentID,
                            DefectDesc = fault.DefectDesc,
                            FaultCategory = fault.FaultCategory,
                            isAdviceNoteIssued = fault.isAdviceNoteIssued,
                            RemedialAction = fault.RemedialAction,
                            SerialNo = fault.SerialNo,
                            WarningTagFixed = fault.WarningTagFixed

                        });

            List<FaultsData> faults = new List<FaultsData>();
            if (_var.Count() > 0)
            {
                faults = _var.ToList();
            }

            return faults;
        }

        //Change#21 - Behroz - 19/12/2012 - Start
        /// <summary>
        /// This function returns General Comments for gas
        /// </summary>
        /// <returns>List of General Comments</returns>
        //public List<FaultsDataGas> getGeneralCommentsGas(string PropertyID)
        //{
        //    var _var = (from fault in context.P_PROPERTY_APPLIANCE_DEFECTS
        //                where fault.PropertyId == PropertyID && fault.CategoryId == 1
        //                select new FaultsDataGas
        //                {
        //                    RemedialAction = fault.ActionNotes,
        //                    ApplianceID = fault.ApplianceId,
        //                    FaultCategory = fault.CategoryId,
        //                    DefectDate = fault.DefectDate,
        //                    DefectDesc = fault.DefectNotes,
        //                    IsActionTaken = fault.IsActionTaken,
        //                    IsDefectIdentified = fault.IsDefectIdentified,
        //                    WarningTagFixed = fault.IsWarningFixed,
        //                    isAdviceNoteIssued = fault.IsWarningIssued,
        //                    JournalId = fault.JournalId,
        //                    ID = fault.PropertyDefectId,
        //                    PropertyId = fault.PropertyId,
        //                    SerialNo = fault.SerialNumber                            
        //                });

        //    List<FaultsDataGas> faults = new List<FaultsDataGas>();
        //    if (_var.Count() > 0)
        //    {
        //        faults = _var.ToList();
        //    }

        //    return faults;
        //}
        public List<FaultsDataGas> getGeneralCommentsGas(int journalId)
        {
            var _var = (from fault in context.P_PROPERTY_APPLIANCE_DEFECTS
                        where fault.JournalId == journalId && fault.CategoryId == Utilities.Constants.MessageConstants.GeneralCommentCategoryId
                        select new FaultsDataGas
                        {
                            RemedialAction = fault.ActionNotes,
                            ApplianceID = fault.ApplianceId,
                            FaultCategory = fault.CategoryId,
                            DefectDate = fault.DefectDate,
                            DefectDesc = fault.DefectNotes,
                            IsActionTaken = fault.IsActionTaken,
                            IsDefectIdentified = fault.IsDefectIdentified,
                            WarningTagFixed = fault.IsWarningFixed,
                            isAdviceNoteIssued = fault.IsWarningIssued,
                            JournalId = fault.JournalId,
                            ID = fault.PropertyDefectId,
                            propertyId = fault.PropertyId,
                            SerialNo = fault.SerialNumber
                        });

            List<FaultsDataGas> faults = new List<FaultsDataGas>();
            if (_var.Count() > 0)
            {
                faults = _var.ToList();
            }

            return faults;
        }
        //Change#21 - Behroz - 19/12/2012 - End
        #endregion

        #region get Faults only.
        /// <summary>
        /// This function returns Faults only.
        /// </summary>
        /// <returns>List of Faults.</returns>

        public List<FaultsData> getFaultsOnly(int AppointmentID)
        {
            var _var = (from fault in context.GS_GasFault
                        where fault.AppointmentID == AppointmentID && fault.FaultCategory.ToLower() != "general comment"
                        select new FaultsData
                        {
                            ID = fault.ID,
                            AppointmentID = fault.AppointmentID,
                            DefectDesc = fault.DefectDesc,
                            FaultCategory = fault.FaultCategory,
                            isAdviceNoteIssued = fault.isAdviceNoteIssued,
                            RemedialAction = fault.RemedialAction,
                            SerialNo = fault.SerialNo,
                            WarningTagFixed = fault.WarningTagFixed

                        });

            List<FaultsData> faults = new List<FaultsData>();
            if (_var.Count() > 0)
            {
                faults = _var.ToList();
            }

            return faults;
        }

        //Change#22 - Behroz - 19/12/2012 - Start
        /// <summary>
        /// This function returns Faults only for gas
        /// </summary>
        /// <returns>List of Faults.</returns>
        //public List<FaultsDataGas> getFaultsOnlyGas(string PropertyID)
        //{
        //    var _var = (from fault in context.P_PROPERTY_APPLIANCE_DEFECTS
        //                where fault.PropertyId == PropertyID && fault.CategoryId != 1
        //                select new FaultsDataGas
        //                {
        //                    RemedialAction = fault.ActionNotes,
        //                    ApplianceID = fault.ApplianceId,
        //                    FaultCategory = fault.CategoryId,
        //                    DefectDate = fault.DefectDate,
        //                    DefectDesc = fault.DefectNotes,
        //                    IsActionTaken = fault.IsActionTaken,
        //                    IsDefectIdentified = fault.IsDefectIdentified,
        //                    WarningTagFixed = fault.IsWarningFixed,
        //                    isAdviceNoteIssued = fault.IsWarningIssued,
        //                    JournalId = fault.JournalId,
        //                    ID = fault.PropertyDefectId,
        //                    PropertyId = fault.PropertyId,
        //                    SerialNo = fault.SerialNumber
        //                });

        //    List<FaultsDataGas> faults = new List<FaultsDataGas>();
        //    if (_var.Count() > 0)
        //    {
        //        faults = _var.ToList();
        //    }

        //    return faults;
        //}
        //Change#22 - Behroz - 19/12/2012 - End
        public List<FaultsDataGas> getFaultsOnlyGas(int journalId)
        {
            var _var = (from fault in context.P_PROPERTY_APPLIANCE_DEFECTS
                        where fault.JournalId == journalId && fault.CategoryId != Utilities.Constants.MessageConstants.GeneralCommentCategoryId
                        select new FaultsDataGas
                        {
                            RemedialAction = fault.ActionNotes,
                            ApplianceID = fault.ApplianceId,
                            FaultCategory = fault.CategoryId,
                            DefectDate = fault.DefectDate,
                            DefectDesc = fault.DefectNotes,
                            IsActionTaken = fault.IsActionTaken,
                            IsDefectIdentified = fault.IsDefectIdentified,
                            WarningTagFixed = fault.IsWarningFixed,
                            isAdviceNoteIssued = fault.IsWarningIssued,
                            JournalId = fault.JournalId,
                            ID = fault.PropertyDefectId,
                            propertyId = fault.PropertyId,
                            SerialNo = fault.SerialNumber
                        });

            List<FaultsDataGas> faults = new List<FaultsDataGas>();
            if (_var.Count() > 0)
            {
                faults = _var.ToList();
            }

            return faults;
        }

        #endregion

        #region Get All Defect Categories

        /// <summary>
        /// This function returns all the defect categories
        /// </summary>
        /// <returns>List of DefectCategory data objects</returns>
        public List<DefectCategoryData> getAllDefectCategories()
        {
            var _var = (from cat in context.P_DEFECTS_CATEGORY
                        select new DefectCategoryData
                        {
                            CatId = cat.CategoryId,
                            CatDescription = cat.Description

                        });

            List<DefectCategoryData> faults = new List<DefectCategoryData>();
            if (_var.Count() > 0)
            {
                faults = _var.ToList();
            }

            return faults;
        }

        #endregion

        #region "Get Fault id"

        /// <summary>
        /// This function returns the fault id
        /// </summary>
        /// <param name="propertyId"></param>
        /// <returns></returns>
        public int getFaultId(string propertyId)
        {
            bool success = false;
            P_PROPERTY_APPLIANCE_DEFECTS fault = new P_PROPERTY_APPLIANCE_DEFECTS();

            using (TransactionScope trans = new TransactionScope())
            {
                fault.CategoryId = 1;
                fault.PropertyId = propertyId;

                context.AddToP_PROPERTY_APPLIANCE_DEFECTS(fault);
                context.SaveChanges();
                trans.Complete();
                success = true;
            }

            if (success == true)
            {
                return fault.PropertyDefectId;
            }
            else
            {
                return 0;
            }
        }

        #endregion

        #region "Delete Fault"

        /// <summary>
        /// This function deletes the fault from the database
        /// </summary>
        /// <param name="propertyId"></param>
        /// <returns></returns>
        public bool deleteFault(int faultId)
        {
            bool success = false;

            using (TransactionScope trans = new TransactionScope())
            {
                List<P_PROPERTY_APPLIANCE_DEFECTS_IMAGES> faultImages = context.P_PROPERTY_APPLIANCE_DEFECTS_IMAGES.Where(img => img.PropertyDefectId == faultId).ToList();

                foreach (var item in faultImages)
                {
                    context.P_PROPERTY_APPLIANCE_DEFECTS_IMAGES.DeleteObject(item);
                    context.SaveChanges();
                }


                P_PROPERTY_APPLIANCE_DEFECTS fault = context.P_PROPERTY_APPLIANCE_DEFECTS.Where(fau => fau.PropertyDefectId == faultId).First();
                context.P_PROPERTY_APPLIANCE_DEFECTS.DeleteObject(fault);
                context.SaveChanges();

                trans.Complete();
                success = true;
            }

            return success;
        }

        #endregion

        #region Save Defect Image

        /// <summary>
        /// This function saves the fault image data in database
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="faultId"></param>
        /// <returns></returns>
        public int saveFaultImageData(string fileName, int faultId, string imageIdentifier = "")
        {
            int success = 0;
            using (TransactionScope trans = new TransactionScope())
            {
                P_PROPERTY_APPLIANCE_DEFECTS_IMAGES appDefectImage = new P_PROPERTY_APPLIANCE_DEFECTS_IMAGES()
                {
                    PropertyDefectId = faultId,
                    ImageTitle = fileName,
                    ImagePath = Utilities.Helpers.FileHelper.getPropertyImageUploadPath(),
                    ImageIdentifier = imageIdentifier
                };

                context.AddToP_PROPERTY_APPLIANCE_DEFECTS_IMAGES(appDefectImage);
                context.SaveChanges();
                trans.Complete();
                success = appDefectImage.PropertyDefectImageId;
            }

            return success;
        }

        #endregion

        #region "Delete Fault"

        /// <summary>
        /// This function deletes the fault from the database
        /// </summary>
        /// <param name="propertyId"></param>
        /// <returns></returns>
        public FaultImagesData deleteFaultImage(int faultImageId)
        {

            P_PROPERTY_APPLIANCE_DEFECTS_IMAGES defectImage = new P_PROPERTY_APPLIANCE_DEFECTS_IMAGES();
            FaultImagesData flImageData = new FaultImagesData();
            var propPic = (from pric in context.P_PROPERTY_APPLIANCE_DEFECTS_IMAGES
                           where pric.PropertyDefectImageId == faultImageId
                           select new FaultImagesData
                           {
                               PropertyDefectImageId = pric.PropertyDefectImageId,
                               PropertyDefectId = pric.PropertyDefectId,
                               ImageTitle = pric.ImageTitle,
                               ImagePath = pric.ImagePath
                           }
                     );
            if (propPic.Count() > 0)
            {
                flImageData = propPic.First();
                defectImage.PropertyDefectImageId = faultImageId;
                context.P_PROPERTY_APPLIANCE_DEFECTS_IMAGES.Attach(defectImage);
                context.P_PROPERTY_APPLIANCE_DEFECTS_IMAGES.DeleteObject(defectImage);
                context.SaveChanges();
            }

            return flImageData;
        }

        #endregion

        #region Check Appointment

        public bool checkAppointmentID(int appointmentID)
        {
            var id = context.PS_Appointment.Where(app => app.AppointId == appointmentID);
            if (id.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool checkAppointmentIDGas(int journalId)
        {
            var id = context.AS_APPOINTMENTS.Where(app => app.JournalId == journalId);
            if (id.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region "Validate Category Id"

        public string validateFaultsCategories(int? CatId)
        {
            var id = context.P_DEFECTS_CATEGORY.Where(cat => cat.CategoryId == CatId);

            if (id.Count() > 0)
                return id.First().Description;
            else
                return string.Empty;
        }

        /// <summary>
        /// This function validates the fault id
        /// </summary>
        /// <param name="CatId"></param>
        /// <returns></returns>
        public bool validateFaultsId(int faultId)
        {
            var id = context.P_PROPERTY_APPLIANCE_DEFECTS.Where(fault => fault.PropertyDefectId == faultId);

            if (id.Count() > 0)
                return true;
            else
                return false;
        }

        #endregion

        #region get Fault Repair List

        /// <summary>
        /// This function returns the list of all fault repairs
        /// </summary>
        /// <returns>List of Fault Repair </returns>
        public List<FaultRepairData> getFaultRepairList()
        {
            var faultRepairList = (from faultRepair in context.FL_FAULT_REPAIR_LIST
                                   where faultRepair.RepairActive == true
                                   select new FaultRepairData
                                   {
                                       FaultRepairID = faultRepair.FaultRepairListID,
                                       Description = faultRepair.Description
                                   }
            );
            List<FaultRepairData> faultRepairDataList = new List<FaultRepairData>();
            if (faultRepairList.Count() > 0)
            {
                faultRepairDataList = faultRepairList.ToList();
            }

            return faultRepairDataList;

        }

        #endregion

        #region get pause reason list

        /// <summary>
        /// This function returns reason list for pausing a job
        /// </summary>
        /// <returns>List of reasons</returns>
        public List<string> getPauseReasonList()
        {
            var pauseReasonList = (from pauseReason in context.FL_PAUSED_REASON
                                   select pauseReason.Reason).ToList();
            return pauseReasonList;
        }
        #endregion

        #region get CO2 Detector is inspected

        /// <summary>
        /// This function returns CO2 detector data against propertyid and journalid
        /// </summary>
        /// <returns></returns>
        public bool isInspectedCO2Detector(string propertyID, int appointmentId)
        {
            var co2DetectorData = (from coIns in context.P_CO2_Inspection
                                   join jor in context.AS_JOURNAL on coIns.Journalid equals jor.JOURNALID
                                   join app in context.AS_APPOINTMENTS on jor.JOURNALID equals app.JournalId
                                   where coIns.PropertyId == propertyID && app.APPOINTMENTID == appointmentId

                                   select coIns);

            if (co2DetectorData.Count() > 0)
            {
                return (bool)co2DetectorData.First().IsInspected;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region get CO2 Detector Inspection

        /// <summary>
        /// This function returns CO2 detector data against propertyid and journalid
        /// </summary>
        /// <returns></returns>
        public DetectorInspectionData CO2DetectorInspection(string propertyID, int appointmentId)
        {
            DetectorInspectionData detectorInspectionData = new DetectorInspectionData();

            var co2DetectorData = (from coIns in context.P_CO2_Inspection
                                   join jor in context.AS_JOURNAL on coIns.Journalid equals jor.JOURNALID
                                   join app in context.AS_APPOINTMENTS on jor.JOURNALID equals app.JournalId
                                   where coIns.PropertyId == propertyID && app.APPOINTMENTID == appointmentId

                                   select new DetectorInspectionData
                                   {
                                       detectorTest = coIns.DetectorTest,
                                       inspectionDate = coIns.DateStamp,
                                       inspectionID = coIns.DectectorId

                                   });

            if (co2DetectorData.Count() > 0)
            {
                detectorInspectionData = co2DetectorData.First();
                return detectorInspectionData;
            }
            else
            {
                return detectorInspectionData;
            }
        }

        #endregion

        #region get Smoke Detector is inspected

        /// <summary>
        /// This function returns smoke detector data against propertyid and journalid
        /// </summary>
        /// <returns>Smoke Inspection Object</returns>
        public bool isInspectedSmokeDetector(string propertyID, int appointmentId)
        {
            var smokeDetectorData = (from smIns in context.P_Smoke_Inspection
                                     join jor in context.AS_JOURNAL on smIns.Journalid equals jor.JOURNALID
                                     join app in context.AS_APPOINTMENTS on jor.JOURNALID equals app.JournalId
                                     where smIns.PropertyId == propertyID && app.APPOINTMENTID == appointmentId
                                     select smIns);

            if (smokeDetectorData.Count() > 0)
            {
                return (bool)smokeDetectorData.First().IsInspected;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region get Smoke Detector inspection

        /// <summary>
        /// This function returns smoke detector data against propertyid and journalid
        /// </summary>
        /// <returns>Smoke Inspection Object</returns>
        public DetectorInspectionData smokeDetectorInspection(string propertyID, int appointmentId)
        {
            DetectorInspectionData detectorInspectionData = new DetectorInspectionData();
            var smokeDetectorData = (from smIns in context.P_Smoke_Inspection
                                     join jor in context.AS_JOURNAL on smIns.Journalid equals jor.JOURNALID
                                     join app in context.AS_APPOINTMENTS on jor.JOURNALID equals app.JournalId
                                     where smIns.PropertyId == propertyID && app.APPOINTMENTID == appointmentId
                                     select new DetectorInspectionData
                                     {
                                         detectorTest = smIns.DetectorTest,
                                         inspectionDate = smIns.DateStamp,
                                         inspectionID = smIns.DectectorId

                                     });

            if (smokeDetectorData.Count() > 0)
            {
                detectorInspectionData = smokeDetectorData.First();
                return detectorInspectionData;
            }
            else
            {
                return detectorInspectionData;
            }
        }

        #endregion

        #region Save Fault Repair Image

        /// <summary>
        /// This function saves the fault image data in database
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="faultId"></param>
        /// <returns></returns>
        public int saveFaultRepairImage(string PropertyId,int schemeId,int blockId, string jsNumber, string imageName, bool isBeforeImage, int createdBy, string imageIdentifier = "")
        {            
			int faultRepairImageId = 0;
            DateTime createdOn = DateTime.Now;
            using (TransactionScope trans = new TransactionScope())
            {
                FL_FAULT_REPAIR_IMAGES repairImages = new FL_FAULT_REPAIR_IMAGES()
                {
                    PropertyId = PropertyId,
                    JobSheetNumber = jsNumber,
                    ImageName = imageName,
                    IsBeforeImage = isBeforeImage,
                    CreatedBy = createdBy,
                    CreatedOn = createdOn,
                    SchemeId = schemeId,
                    BlockId = blockId,
					ImageIdentifier = imageIdentifier
                };

                context.AddToFL_FAULT_REPAIR_IMAGES(repairImages);
                context.SaveChanges();
                trans.Complete();
                faultRepairImageId = repairImages.FaultRepairImageId;
            }

            return faultRepairImageId;
        }

        #endregion

        #region is Fault Repair Image already exists

        /// <summary>
        /// check if fault repair Image already exists
        /// </summary>        
        /// <param name="imageIdentifier"></param>
        /// <param name="imageName"></param>
        /// <param name="faultRepairImageId"></param>
        public bool isFaultRepairImageAlreadyExists(string imageIdentifier, out string imageName, out int faultRepairImageId)
        {
            bool isFaultRepairImageAlreadyExists = false;
            imageName = string.Empty;
            faultRepairImageId = -1;

            if (imageIdentifier != string.Empty)
            {
                var faultRepairImagesList = from img in context.FL_FAULT_REPAIR_IMAGES
                                        where img.ImageIdentifier == imageIdentifier
                                        select img;
                if (faultRepairImagesList.Count() > 0)
                {
                    isFaultRepairImageAlreadyExists = true;
                    var imageData = faultRepairImagesList.First();
                    imageName = imageData.ImageName;
                    faultRepairImageId = imageData.FaultRepairImageId;
                }
                

            }
            return isFaultRepairImageAlreadyExists;
        }

        #endregion

        #region is Property Defect Image already exists

        /// <summary>
        /// check if property defect Image already exists
        /// </summary>        
        /// <param name="imageIdentifier"></param>
        /// <param name="imageName"></param>
        /// <param name="faultRepairImageId"></param>
        public bool ispropertyDefectImageAlreadyExists(string imageIdentifier, out string imageName, out int propertyDefectImageId)
        {
            bool isPropertyDefectAlreadyExists = false;
            imageName = string.Empty;
            propertyDefectImageId = -1;

            if (imageIdentifier != string.Empty)
            {
                var faultRepairImagesList = from img in context.P_PROPERTY_APPLIANCE_DEFECTS_IMAGES
                                            where img.ImageIdentifier == imageIdentifier
                                            select img;
                if (faultRepairImagesList.Count() > 0)
                {
                    isPropertyDefectAlreadyExists = true;
                    var imageData = faultRepairImagesList.First();
                    imageName = imageData.ImageTitle;
                    propertyDefectImageId = imageData.PropertyDefectImageId;
                }
            }
            return isPropertyDefectAlreadyExists;
        }

        #endregion
    }
}
