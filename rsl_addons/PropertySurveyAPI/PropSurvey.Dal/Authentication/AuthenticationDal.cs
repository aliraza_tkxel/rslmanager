﻿using System;
using System.Globalization;
using System.Linq;
using System.Transactions;
using PropSurvey.Contracts.Data;
using PropSurvey.Dal.Base;
using PropSurvey.Entities;
using System.Collections.Generic;
using System.Data.Objects;

namespace PropSurvey.Dal.Authentication
{
    public class AuthenticationDal:BaseDal
    {
        #region Find User 
        
        /// <summary>
        /// This function search the user name and password in database.
        /// </summary>
        /// <param name="userName">username of surveyour</param>
        /// <param name="password">password of surveyour</param>
        /// <returns>logged in Id</returns>

        public int findUser(string userName, string password, string deviceToken)
        {

            PropertySurvey_Entities conn = new PropertySurvey_Entities();
            var userData = conn.SP_NET_CHECKLOGIN(userName, password).ToList();

            int userId = 0;

            if (userData.Count() > 0)
            {
                var user = userData.First();
                userId = Convert.ToInt32(user.LOGINID);
                if (deviceToken != null && deviceToken != "")
                {
                    var userRecord = context.AC_LOGINS.Where(ulogin => ulogin.LOGINID == userId);
                    if (userRecord.Count() > 0)
                    {
                        AC_LOGINS loggedInUser = userRecord.First();
                        loggedInUser.DEVICETOKEN = deviceToken;
                        context.SaveChanges();
                    }
                }

            }

            return userId;
            
        }

        #endregion 


        #region Get User Info By Email

        /// <summary>
        /// This function get User Info by email.
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <returns></returns>

        public SP_GetUserInfoByEmail_Result getUserInfoByEmail(string emailAddress)
        {

            PropertySurvey_Entities conn = new PropertySurvey_Entities();
            var userInfolist = conn.SP_GetUserInfoByEmail(emailAddress).ToList();

            SP_GetUserInfoByEmail_Result  userInfo = null;
            if (userInfolist.Count > 0)
            {
                userInfo = userInfolist.First();
            }

            return userInfo;
        }

        #endregion  


        #region Update Password

        /// <summary>
        /// Update Password
        /// </summary>
        /// <param name="objUpdatePasswordParam"></param>
        /// <returns></returns>

        public int updatePassword(UpdatePasswordParam objUpdatePasswordParam)
        {
            ObjectParameter output = new ObjectParameter("retStatus", typeof(Int32));
            String username = objUpdatePasswordParam.username;
            String oldPassword = objUpdatePasswordParam.oldPassword;
            String newPassword = objUpdatePasswordParam.newPassword;
            PropertySurvey_Entities conn = new PropertySurvey_Entities();
            conn.SP_NET_UPDATELOGIN(username, oldPassword, newPassword, output);

            return Convert.ToInt32(output.Value);
        }

        #endregion  

        
        #region Set Login Threshold

        /// <summary>
        /// This function Set Login Threshold.
        /// </summary>
        /// <param name="userName">username of surveyour</param>
        /// <returns>logged in Id</returns>

        public SP_NET_SETLOGIN_THRESHOLD_Result setLoginThreshold(string userName)
        {
          
            PropertySurvey_Entities conn = new PropertySurvey_Entities();
            var thresholdlist = conn.SP_NET_SETLOGIN_THRESHOLD(userName).ToList();

            SP_NET_SETLOGIN_THRESHOLD_Result loginThreshold = null;
            if (thresholdlist.Count > 0) {
                loginThreshold = thresholdlist.First();
            }

            return loginThreshold;
        }

        #endregion  

        #region is User Exist

        /// <summary>
        /// this fuction varifies that either specific user exists in database or not
        /// </summary>
        /// <param name="userName">user name</param>
        /// <returns> true or false</returns>

        public int isUserExist(string userName)
        {
            int login = 0;

            var apptRecord = context.AC_LOGINS.Where(acc => acc.LOGIN.ToLower() == userName.ToLower() && acc.ACTIVE == 1);            

            if (apptRecord.Count() > 0)
            {
                login = apptRecord.First().LOGINID;                
            }
            else
            {
                login = 0;
            }
            return login;
        }

        //Change - Behroz  - 12/05/2012 - Start
        
        /// <summary>
        /// this fuction varifies that either specific user exists in database or not
        /// </summary>
        /// <param name="userName">user name</param>
        /// <returns> true or false</returns>
        public int? isUserExistGas(string userName)
        {
            int? empId = 0;

            var apptRecord = context.AC_LOGINS.Where(acc => acc.LOGIN.ToLower() == userName.ToLower());
            //var apptRecord = from login in context.AC_LOGINS
            //                 join
            //                     user in context.AS_USER on login.EMPLOYEEID equals user.EmployeeId
            //                     where login.LOGIN.ToLower() == userName.ToLower() && login.ACTIVE == 1 && user.IsActive == true
            //                 select user.UserId;


            if (apptRecord.Count() > 0)
            {
                empId = apptRecord.First().EMPLOYEEID;                
            }
            else
            {
                empId = 0;
            }
            return empId;
        }

        /// <summary>
        /// this fuction varifies that either specific user exists in database or not
        /// </summary>
        /// <param name="userName">user name</param>
        /// <returns> true or false</returns>

        //public string isUserExist(int userId)
        //{
        //    //var apptRecord = context.AC_LOGINS.Where(acc => acc.EMPLOYEEID == userId);
        //    //var apptRecord = from login in context.AC_LOGINS
        //    //                 join
        //    //                     user in context.AS_USER on login.EMPLOYEEID equals user.EmployeeId
        //    //                 where login.EMPLOYEEID == userId && login.ACTIVE == 1 && user.IsActive == true
        //    //                 select login.LOGIN;

        //    //if (apptRecord.Count() > 0)
        //    //{
        //    //    return apptRecord.First();
        //    //}
        //    //else
        //    //{
        //    //    return string.Empty;
        //    //}
        //}

        public int GetUserId(int userId)
        {
            //var apptRecord = context.AC_LOGINS.Where(acc => acc.EMPLOYEEID == userId);
            //var apptRecord = from user in context.AS_USER
            //                 where user.EmployeeId == userId && user.IsActive == true
            //                 select user.UserId;

            //if (apptRecord.Count() > 0)
            //{
            //    return apptRecord.First();
            //}
            //else
            //{
            //    return 0;
            //}
            //We are saving Employee id instead of Userid
            return userId;
        }

        //Change - Behroz  - 12/05/2012 - End

        //Change#16 - Behroz - 18/12/2012 - Start
        public bool isUserExistRights(int userId)
        {   
            var apptRecord = from user in context.AS_USER
                             where user.EmployeeId == userId && user.IsActive == true && (user.UserTypeID == 1 || user.UserTypeID == 3)
                             select user;

            if (apptRecord.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        //Change#16 - Behroz - 18/12/2012 - End


        #endregion

        #region get Last Logged In Date

        /// <summary>
        /// This function find the last logged in date of surveryour
        /// </summary>
        /// <param name="userId">login id of the user</param>
        /// <returns>last logged in date</returns>

        public UserData getLastLoggedInDate(UserData usrData )
        {
            var usrLog = (from usr in context.PS_UserLog
                           where usr.LoggedInUserId == usrData.userId
                           select new UserData
                           {
                               lastLoggedInDate = usr.LastLoggedInDate,
                               userId = usr.LoggedInUserId
                           }
                          );

            if (usrLog.Count() > 0)
            {
                return usrLog.First();
            }
            else
            {
                return usrData;
            }
            
            
        }
        #endregion

        #region update Last Logged In Date

        /// <summary>
        /// This function update the current date as last logged in date of the user/surveyour
        /// </summary>
        /// <param name="usrData">object of user data</param>        

        public void updateLastLoggedInDate(UserData usrData)
        {            
            var userRecord = context.PS_UserLog.Where(usr => usr.LoggedInUserId == usrData.userId);

            if (userRecord.Count() > 0)
            {
                PS_UserLog userLog = userRecord.First();
                userLog.LastLoggedInDate = usrData.lastLoggedInDate;
                userLog.LoggedInUserId = usrData.userId;
                context.SaveChanges();                
            }            
        }
        #endregion 

        #region Reset login threshold

        /// <summary>
        /// This function Reset login threshold
        /// </summary>
        /// <param name="usrData">object of user data</param>        

        public void resetLoginThreshold(String username)
        {
            var userRecord = context.AC_LOGINS.Where(usr => usr.LOGIN == username);

            if (userRecord.Count() > 0)
            {
                AC_LOGINS user = userRecord.First();
                user.THRESHOLD= 0;
                user.ISLOCKED = false ;
                context.SaveChanges();
            }
        }
        #endregion 

        #region insert Last Logged In Date

        /// <summary>
        /// This function insert the current date as last logged in date of the user/surveyour
        /// </summary>
        /// <param name="usrData">object of user data</param>        

        public void insertLastLoggedInDate(UserData usrData)
        {                         

            using (TransactionScope trans = new TransactionScope())
            {
                PS_UserLog userLog = new PS_UserLog();
                userLog.LoggedInUserId = usrData.userId;
                userLog.LastLoggedInDate = usrData.lastLoggedInDate;

                context.AddToPS_UserLog(userLog);
                context.SaveChanges();
                trans.Complete();
            }                        
        }
        #endregion

        #region get device token
        /// <summary>
        /// this fuction gets the device token of a specified user
        /// </summary>
        /// <param name="id">id</param>
        /// <returns> device token</returns>

        public string getDeviceToken(int userId)
        {
            string deviceToken = string.Empty;

            var userRecord = context.AC_LOGINS.Where(acc => acc.LOGINID == userId);

            if (userRecord.Count() > 0)
            {
                deviceToken = userRecord.First().DEVICETOKEN;                
            }
           
            return deviceToken;
        }
        #endregion

        #region get user Full Name
        /// <summary>
        /// this fuction gets user Full Name of a specified user
        /// </summary>
        /// <param name="id">id</param>
        /// <returns> Full Name</returns>

        public string getUserFullName(string loginId, string password)
        {
            string _var = (from log in context.AC_LOGINS
                        join emp in context.E__EMPLOYEE on log.EMPLOYEEID equals emp.EMPLOYEEID
                        where log.LOGIN == loginId && log.PASSWORD == password
                        select emp.FIRSTNAME + " " + emp.LASTNAME).FirstOrDefault();
            return _var;

        }
        #endregion

        #region get user Full Name
        /// <summary>
        /// this fuction gets user Full Name of a specified user
        /// </summary>
        /// <param name="id">id</param>
        /// <returns> Full Name</returns>

        public int getUserId(string loginId, string password)
        {
            int _var = (from log in context.AC_LOGINS
                           join emp in context.E__EMPLOYEE on log.EMPLOYEEID equals emp.EMPLOYEEID
                           where log.LOGIN == loginId && log.PASSWORD == password
                           select emp.EMPLOYEEID).FirstOrDefault();
            return _var;

        }
        #endregion

        #region is User Already LoggedIn

        /// <summary>
        /// this fuction verifies that either specific user is already logged in
        /// </summary>
        /// <param name="username">username</param>
        /// <returns> true or false</returns>

        public bool isUserLoggedIn(string username)
        {
            bool isLoggedIn = false;

            var userRecord = context.PS_LoggedInUser.Where(usr => usr.LoggedInUsername.ToLower() == username.ToLower());
            if (userRecord.Count() > 0)
            {
                PS_LoggedInUser userLog = userRecord.First();
                TimeSpan ts = DateTime.Now - (DateTime)userLog.LoggedInUserDateTime;
                if (ts.TotalMinutes >= 0 && ts.TotalMinutes <= 30   && userLog.LoggedInUserSalt != "0")
                {
                    isLoggedIn = true;
                }
            }
            
            return isLoggedIn;
        }
        #endregion

        #region Save Logged In user salt

        /// <summary>
        /// this fuction inserts the logged in user to database
        /// </summary>
        /// <param name="username">username</param>
        /// <returns>DateTime</returns>

        public DateTime? saveLoggedInUserSalt(string username, string salt)
        {
            PS_LoggedInUser userLog = new PS_LoggedInUser();
            using (TransactionScope trans = new TransactionScope())
            {
                var userRecord = context.PS_LoggedInUser.Where(usr => usr.LoggedInUsername.ToLower() == username.ToLower());
                
                if (userRecord.Count() > 0)
                {
                    userLog = userRecord.First();
                    userLog.LoggedInUserDateTime = DateTime.Now;
                    userLog.LoggedInUserSalt = salt;
                    context.SaveChanges();
                    trans.Complete();
                }
                else
                {
                    
                    userLog.LoggedInUsername = username;
                    userLog.LoggedInUserDateTime = DateTime.Now;
                    userLog.LoggedInUserSalt = salt;
                    context.AddToPS_LoggedInUser(userLog);
                    context.SaveChanges();
                    trans.Complete();
 
                }
            }
            return userLog.LoggedInUserDateTime;
        }
        #endregion

        #region Delete Logged In user

        /// <summary>
        /// this fuction deletes the logged in user to database
        /// </summary>
        /// <param name="userId">username</param>
        /// <returns></returns>

        public void deleteLoggedInUser(string username)
        {
            
            using (TransactionScope trans = new TransactionScope())
            {
                var userRecord = context.PS_LoggedInUser.Where(usr => usr.LoggedInUsername.ToLower() == username.ToLower());

                if (userRecord.Count() > 0)
                {
                    PS_LoggedInUser userLog = userRecord.First();
                    context.PS_LoggedInUser.DeleteObject(userLog);
                    context.SaveChanges();


                    trans.Complete();
                    
                }
            }
        }
        #endregion

        #region log out user

        /// <summary>
        /// this fuction logouts the user
        /// </summary>
        /// <param name="username">username</param>
        /// <returns></returns>

        public void LogoutUser(string username)
        {
            using (TransactionScope trans = new TransactionScope())
            {
                var userRecord = context.PS_LoggedInUser.Where(usr => usr.LoggedInUsername.ToLower() == username.ToLower());

                if (userRecord.Count() > 0)
                {
                    PS_LoggedInUser userLog = userRecord.First();
                    //userLog.LoggedInUserDateTime = DateTime.Now;
                    userLog.LoggedInUserSalt = "0";
                    context.SaveChanges();
                    trans.Complete();
                }
                
            }
        }
        #endregion

        #region check for Valid User session

        /// <summary>
        /// this fuction checks that the user session is a valid one
        /// </summary>
        /// <param name="username">username</param>
        /// <param name="salt">salt</param>
        /// <returns>true or false</returns>

        public bool checkForValidSession(string username, string salt)
        {
            bool result = false;
            if (salt != "0")
            {
                using (TransactionScope trans = new TransactionScope())
                {
                    var userRecord = context.PS_LoggedInUser.Where(usr => usr.LoggedInUsername.ToLower() == username.ToLower() && usr.LoggedInUserSalt == salt);

                    if (userRecord.Count() > 0)
                    {
                        PS_LoggedInUser userLog = userRecord.First();

                        TimeSpan ts = DateTime.Now - (DateTime)userLog.LoggedInUserDateTime;
                        if ( ts.TotalMinutes > 120 )
                        {
                            result = false;
                        }
                        else
                        {


                            //userLog.LoggedInUserDateTime = DateTime.Now;
                            //userLog.LoggedInUserDateTime = "0";
                            //context.SaveChanges();
                            trans.Complete();
                            result = true;
                        }
                    }

                }
            }
            return result;
        }
        #endregion

        public string getSalt(string userName)
        {
            var salt = (from user in context.PS_LoggedInUser where user.LoggedInUsername.ToLower() == userName.ToLower() select user.LoggedInUserSalt).FirstOrDefault();
            if (salt.Count() > 0)
            {
                return salt.ToString();
            }
            else
            {
                return "Not Found";
            }
        }
    }
}
