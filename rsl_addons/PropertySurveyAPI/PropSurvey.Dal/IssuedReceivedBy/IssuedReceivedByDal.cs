﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Transactions;
using PropSurvey.Contracts.Data;
using PropSurvey.Dal.Base;
using PropSurvey.Entities;
using PropSurvey.Utilities.Helpers;
using System.Text;
using System.IO;

namespace PropSurvey.Dal.IssuedReceivedBy
{
    public class IssuedReceivedByDal : BaseDal
    {
        #region get IssuedReceivedBy Data
        /// <summary>
        /// This function returns the IssuedReceivedBy data
        /// </summary>
        /// <returns>List of IssuedReceivedBy data objects</returns>

        public IssuedReceivedByData getIssuedReceivedByFormData(int AppointmentID)
        {
            IssuedReceivedByData _var = (from work in context.GS_IssuedReceivedBy
                                         where work.AppointmentID == AppointmentID
                                         select new IssuedReceivedByData
                                           {
                                               ID = work.ID,
                                               AppointmentID = work.AppointmentID,
                                               IssuedDate = work.IssuedDate,
                                               ReceivedDate = work.ReceivedDate,
                                               ReceivedOnBehalfOf = work.ReceivedOnBehalf,
                                               IssuedBy = work.IssuedBy,
                                               CP12No = work.CP12No
                                           }).FirstOrDefault();

            return _var;
        }

        /// <summary>
        /// This function returns the IssuedReceivedBy data for gas
        /// </summary>
        /// <returns>List of IssuedReceivedBy data objects</returns>
        public LGSRData getIssuedReceivedByFormDataGas(int journalId)
        {
            LGSRData _var = (from appointment in context.AS_APPOINTMENTS
                             join work in context.P_LGSR on appointment.JournalId equals work.JOURNALID into workLGSR
                             from lgsr in workLGSR.DefaultIfEmpty()
                             where appointment.JournalId == journalId
                             select new LGSRData
                             {
                                 LGSRID = lgsr.LGSRID,
                                 propertyId = lgsr.PROPERTYID,
                                 IssuedDate = lgsr.ISSUEDATE,
                                 ReceivedDate = lgsr.RECEVIEDDATE,
                                 ReceivedOnBehalfOf = lgsr.RECEVIEDONBEHALF,
                                 IssuedBy = lgsr.CP12ISSUEDBY,
                                 CP12Number = lgsr.CP12NUMBER,
                                 TenantHanded = lgsr.TENANTHANDED,
                                 InspectionCarried = lgsr.INSPECTIONCARRIED,
                                 JournalId = lgsr.JOURNALID,
                                 jsgNumber = appointment.JSGNUMBER
                             }).FirstOrDefault();

            return _var;
        }

        /// <summary>
        /// This function returns the IssuedReceivedBy data for gas
        /// </summary>
        /// <returns>List of IssuedReceivedBy data objects</returns>
        public LGSRData getIssuedReceivedByFormData(string propertyId)
        {

            P_LGSR var = (from lgsr in context.P_LGSR
                          where lgsr.PROPERTYID == propertyId //&& lgsr.IsAppointmentCompleted == false
                          select lgsr
                             ).FirstOrDefault();
            LGSRData _var = new LGSRData();
            if (var != null && !var.IsAppointmentCompleted)
            {
                _var.CP12Number = var.CP12NUMBER;
                _var.propertyId = var.PROPERTYID;
                _var.JournalId = var.JOURNALID;
                _var.LGSRID = var.LGSRID;
                
                _var.DTimeStamp = var.DTIMESTAMP;
                _var.DocumentType = var.DOCUMENTTYPE;
                _var.InspectionCarried = var.INSPECTIONCARRIED;
                _var.IssuedBy = var.CP12ISSUEDBY;
                _var.IssuedDate = var.ISSUEDATE;
                _var.Notes = var.NOTES;
                _var.ReceivedDate = var.RECEVIEDDATE;
                _var.ReceivedOnBehalfOf = var.RECEVIEDONBEHALF;
                _var.TenantHanded = var.TENANTHANDED;
                _var.isAppointmentCompleted = var.IsAppointmentCompleted;
            }
            else
            {
                _var.CP12Number = var.CP12NUMBER;
                _var.propertyId = var.PROPERTYID;
                _var.JournalId = var.JOURNALID;
                _var.LGSRID = var.LGSRID;
            }


            if (_var != null && _var.IssuedDate != null)
            {
                _var.IssuedDateString = _var.IssuedDate.Value.ToString("dddd, dd MMMM yyyy");
                _var.ReceivedDateString = _var.IssuedDate.Value.ToString("dddd, dd MMMM yyyy");
            }
            return _var;
        }
        #endregion

        #region save IssuedReceivedBy Data
        /// <summary>
        /// This function saves the IssuedReceivedBy Data in the database
        /// </summary>
        /// <param name="appData">The object of IssuedReceivedBy Data</param>
        /// <returns>the IssuedReceivedBy id if successfully save otherwise 0 </returns>

        public int saveIssuedReceivedBy(IssuedReceivedByData workData)
        {
            bool success = false;

            GS_IssuedReceivedBy issueby = new GS_IssuedReceivedBy();
            using (TransactionScope trans = new TransactionScope())
            {
                issueby.AppointmentID = workData.AppointmentID;
                issueby.IssuedDate = workData.IssuedDate;
                issueby.IssuedBy = workData.IssuedBy;
                issueby.ReceivedOnBehalf = workData.ReceivedOnBehalfOf.ToUpper();
                issueby.CP12No = workData.CP12No;
                issueby.ReceivedDate = workData.ReceivedDate;

                context.AddToGS_IssuedReceivedBy(issueby);
                context.SaveChanges();
                trans.Complete();
                success = true;
            }

            if (success == true)
            {
                return issueby.ID;
            }
            else
            {
                return 0;
            }
        }

        //Change#12 - Behroz - 12/13/2012 - Start
        /// <summary>
        /// This function saves the IssuedReceivedBy Data in the database
        /// </summary>
        /// <param name="appData">The object of IssuedReceivedBy Data</param>
        /// <returns>the IssuedReceivedBy id if successfully save otherwise 0 </returns>
        public int saveIssuedReceivedBy(LGSRData workData)
        {
            bool success = false;

            P_LGSR lgsr = new P_LGSR();
            P_LGSR_HISTORY lgsrHistory = new P_LGSR_HISTORY();

            var propertyAlreadyExist = from issue in context.P_LGSR
                                       where issue.PROPERTYID == workData.propertyId
                                       select issue;

            if (propertyAlreadyExist.Count() > 0)
            {
                #region "Update"

                //If the property Already exists then update the DB .. otherwise Insert

                lgsr = propertyAlreadyExist.FirstOrDefault();

                #region "Update P_LGSR table and Insert into P_LGSRHISTORY"

                using (TransactionScope trans = new TransactionScope())
                {
                    //lgsrHistory.LGSRID = workData.LGSRID;
                    lgsrHistory.LGSRID = lgsr.LGSRID;

                    if (workData.IssuedBy != null)
                    {
                        lgsr.CP12ISSUEDBY = workData.IssuedBy;
                        lgsrHistory.CP12ISSUEDBY = workData.IssuedBy;
                    }

                    // Code modified  - 06/06/2013 - START

                    if (workData.IssuedDateString != null)
                    {
                        DateTime issuedDate = FileHelper.convertDate(workData.IssuedDateString);
                        lgsr.ISSUEDATE = issuedDate;
                        lgsrHistory.ISSUEDATE = issuedDate;
                    }

                    // Code modified  - 06/06/2013 - END

                    if (workData.ReceivedOnBehalfOf != null)
                    {
                        lgsr.RECEVIEDONBEHALF = workData.ReceivedOnBehalfOf.ToUpper();
                        lgsrHistory.RECEVIEDONBEHALF = workData.ReceivedOnBehalfOf.ToUpper();
                    }

                    // Code modified  - 06/06/2013 - START

                    if (workData.ReceivedDateString != null)
                    {
                        DateTime receivedDate = FileHelper.convertDate(workData.ReceivedDateString);
                        lgsr.RECEVIEDDATE = receivedDate;
                        lgsrHistory.RECEVIEDDATE = receivedDate;
                    }

                    // Code modified  - 06/06/2013 - END

                    if (workData.propertyId != null)
                    {
                        lgsr.PROPERTYID = workData.propertyId;
                        lgsrHistory.PROPERTYID = workData.propertyId;
                    }

                    if (workData.CP12Number != null)
                    {
                        lgsr.CP12NUMBER = workData.CP12Number;
                        lgsrHistory.CP12NUMBER = workData.CP12Number;
                    }

                    if (workData.CP12Document != null)
                    {
                        lgsr.CP12DOCUMENT = Encoding.ASCII.GetBytes(workData.CP12Document);
                        lgsrHistory.CP12DOCUMENT = Encoding.ASCII.GetBytes(workData.CP12Document);
                    }

                    //New column added in the database. Save the values in the history and the main table.
                    if (workData.TenantHanded != null)
                    {
                        lgsr.TENANTHANDED = workData.TenantHanded;
                        lgsrHistory.TENANTHANDED = workData.TenantHanded;
                    }

                    //New column added
                    if (workData.InspectionCarried != null)
                    {
                        lgsr.INSPECTIONCARRIED = workData.InspectionCarried;
                        lgsrHistory.INSPECTIONCARRIED = workData.InspectionCarried;
                    }

                    if (workData.JournalId != null)
                    {
                        lgsr.JOURNALID = workData.JournalId;
                        lgsrHistory.JOURNALID = workData.JournalId;
                    }

                    context.AddToP_LGSR_HISTORY(lgsrHistory);

                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    trans.Complete();

                    success = true;
                }

                #endregion

                if (success == true)
                {
                    return lgsr.LGSRID;
                }
                else
                {
                    return 0;
                }

                #endregion
            }
            else
            {
                #region "Insert"

                #region "Insert P_LGSR table"

                using (TransactionScope trans = new TransactionScope())
                {
                    #region "Insert Into P_LGSR"

                    // Code added  - 06/06/2013 - START

                    DateTime issuedDate = DateTime.Now;
                    DateTime receivedDate = DateTime.Now;

                    if (workData.IssuedDateString != null)
                    {
                        issuedDate = FileHelper.convertDate(workData.IssuedDateString);
                    }

                    if (workData.ReceivedDateString != null)
                    {
                        receivedDate = FileHelper.convertDate(workData.ReceivedDateString);
                    }

                    // Code added  - 06/06/2013 - END

                    //Insert into P_LGSR
                    lgsr.PROPERTYID = workData.propertyId;
                    // Code modified  - 06/06/2013 - START
                    if (workData.IssuedDateString != null)
                    {
                        lgsr.ISSUEDATE = issuedDate;
                    }
                    // Code modified  - 06/06/2013 - END
                    lgsr.CP12ISSUEDBY = workData.IssuedBy;
                    lgsr.CP12NUMBER = workData.CP12Number;
                    lgsr.RECEVIEDONBEHALF = workData.ReceivedOnBehalfOf;
                    // Code modified  - 06/06/2013 - START
                    if (workData.ReceivedDateString != null)
                    {
                        lgsr.RECEVIEDDATE = receivedDate;
                    }
                    // Code modified  - 06/06/2013 - END
                    lgsr.DOCUMENTTYPE = "pdf";
                    lgsr.DTIMESTAMP = DateTime.Now;
                    lgsr.TENANTHANDED = workData.TenantHanded;
                    lgsr.INSPECTIONCARRIED = workData.InspectionCarried;
                    lgsr.JOURNALID = workData.JournalId;

                    context.AddToP_LGSR(lgsr);
                    context.SaveChanges();

                    #endregion

                    #region "Insert into P_LGSR_HISTORY"

                    //Insert into P_LGSR_History
                    lgsrHistory.LGSRID = lgsr.LGSRID;
                    lgsrHistory.PROPERTYID = workData.propertyId;
                    // Code modified  - 06/06/2013 - START
                    if (workData.IssuedDate != null)
                    {
                        lgsrHistory.ISSUEDATE = issuedDate;
                    }
                    // Code modified  - 06/06/2013 - END
                    lgsrHistory.CP12ISSUEDBY = workData.IssuedBy;
                    lgsrHistory.CP12NUMBER = workData.CP12Number;
                    lgsrHistory.RECEVIEDONBEHALF = workData.ReceivedOnBehalfOf;
                    // Code modified  - 06/06/2013 - START
                    if (workData.ReceivedDate != null)
                    {
                        lgsrHistory.RECEVIEDDATE = receivedDate;
                    }
                    // Code modified  - 06/06/2013 - END
                    lgsrHistory.DOCUMENTTYPE = "pdf";
                    lgsrHistory.DTIMESTAMP = DateTime.Now;
                    lgsrHistory.TENANTHANDED = workData.TenantHanded;
                    lgsrHistory.INSPECTIONCARRIED = workData.InspectionCarried;
                    lgsrHistory.JOURNALID = workData.JournalId;

                    context.AddToP_LGSR_HISTORY(lgsrHistory);

                    #endregion

                    trans.Complete();

                    success = true;
                }
                #endregion

                if (success == true)
                {
                    return lgsr.LGSRID;
                }
                else
                {
                    return 0;
                }

                #endregion
            }
        }
        //Change#12 - Behroz - 12/13/2012 - End
        #endregion

        #region update IssuedReceivedBy Data

        /// <summary>
        /// This function updates IssuedReceivedBy Data in the database
        /// </summary>
        /// <param name="appData">The object of IssuedReceivedBy Data</param>
        /// <returns>true if successfully save otherwise 0 </returns>
        public bool updateIssuedReceivedByData(IssuedReceivedByData insData)
        {
            bool success = false;
            using (TransactionScope trans = new TransactionScope())
            {
                var _var = (from ins in context.GS_IssuedReceivedBy
                            where ins.ID == insData.ID
                            select ins);
                if (_var.Count() > 0)
                {
                    GS_IssuedReceivedBy Ins = _var.FirstOrDefault();
                    Ins.IssuedBy = insData.IssuedBy;
                    Ins.IssuedDate = insData.IssuedDate;
                    Ins.ReceivedOnBehalf = insData.ReceivedOnBehalfOf.ToUpper();
                    Ins.ReceivedDate = insData.ReceivedDate;
                    Ins.AppointmentID = insData.AppointmentID;
                    Ins.CP12No = insData.CP12No;

                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    trans.Complete();
                    success = true;
                }
                else
                {
                    success = false;
                }

            }

            return success;
        }

        //Change#35 - Behroz - 12/17/2012 - Start
        /// <summary>
        /// This function updates IssuedReceivedBy Data in the database
        /// </summary>
        /// <param name="appData">The object of IssuedReceivedBy Data</param>
        /// <returns>true if successfully save otherwise 0 </returns>
        public bool updateIssuedReceivedByData(LGSRData lgsrData)
        {
            //Update IssuedReceivedByData and insert into History
            bool success = false;
            using (TransactionScope trans = new TransactionScope())
            {
                var _var = (from lgsr in context.P_LGSR
                            where lgsr.LGSRID == lgsrData.LGSRID
                            select lgsr);

                P_LGSR_HISTORY objlgsrHistory = new P_LGSR_HISTORY();

                if (_var.Count() > 0)
                {
                    #region "Update in P_LGSR and Insert in P_LGSRHISTORY"

                    P_LGSR objlgsr = _var.FirstOrDefault();

                    objlgsrHistory.LGSRID = lgsrData.LGSRID;

                    if (lgsrData.IssuedBy != null)
                    {
                        objlgsr.CP12ISSUEDBY = lgsrData.IssuedBy;
                        objlgsrHistory.CP12ISSUEDBY = lgsrData.IssuedBy;
                    }
                    // Code modified  - 06/06/2013 - START
                    if (lgsrData.IssuedDateString != null)
                    {
                        DateTime issueDate = FileHelper.convertDate(lgsrData.IssuedDateString);
                        objlgsr.ISSUEDATE = issueDate;
                        objlgsrHistory.ISSUEDATE = issueDate;
                    }
                    // Code modified  - 06/06/2013 - END
                    if (lgsrData.ReceivedOnBehalfOf != null)
                    {
                        objlgsr.RECEVIEDONBEHALF = lgsrData.ReceivedOnBehalfOf.ToUpper();
                        objlgsrHistory.RECEVIEDONBEHALF = lgsrData.ReceivedOnBehalfOf.ToUpper();
                    }
                    // Code modified  - 06/06/2013 - START
                    if (lgsrData.ReceivedDateString != null)
                    {
                        DateTime receviedDate = FileHelper.convertDate(lgsrData.ReceivedDateString);
                        objlgsr.RECEVIEDDATE = receviedDate;
                        objlgsrHistory.RECEVIEDDATE = receviedDate;
                    }
                    // Code modified  - 06/06/2013 - END
                    if (lgsrData.propertyId != null)
                    {
                        objlgsr.PROPERTYID = lgsrData.propertyId;
                        objlgsrHistory.PROPERTYID = lgsrData.propertyId;
                    }

                    if (lgsrData.CP12Number != null)
                    {
                        objlgsr.CP12NUMBER = lgsrData.CP12Number;
                        objlgsrHistory.CP12NUMBER = lgsrData.CP12Number;
                    }

                    if (lgsrData.CP12Document != null)
                    {
                        objlgsr.CP12DOCUMENT = Encoding.ASCII.GetBytes(lgsrData.CP12Document);
                        objlgsrHistory.CP12DOCUMENT = Encoding.ASCII.GetBytes(lgsrData.CP12Document);
                    }

                    //New column was added.
                    if (lgsrData.TenantHanded != null)
                    {
                        objlgsr.TENANTHANDED = lgsrData.TenantHanded;
                        objlgsrHistory.TENANTHANDED = lgsrData.TenantHanded;
                    }

                    //New column added
                    if (lgsrData.InspectionCarried != null)
                    {
                        objlgsr.INSPECTIONCARRIED = lgsrData.InspectionCarried;
                        objlgsrHistory.INSPECTIONCARRIED = lgsrData.InspectionCarried;
                    }

                    if (lgsrData.JournalId != null)
                    {
                        objlgsr.JOURNALID = lgsrData.JournalId;
                        objlgsrHistory.JOURNALID = lgsrData.JournalId;
                    }

                    context.AddToP_LGSR_HISTORY(objlgsrHistory);

                    ////If in a scenerio, the status is not Complete, then make it.
                    //AS_JOURNAL lobjJournal = new AS_JOURNAL();

                    //var getStatusId = context.AS_Status.Where(status => status.Title == Utilities.Constants.MessageConstants.CP12IssuedNameInAS_Status).FirstOrDefault();
                    //var JournalData = context.AS_JOURNAL.Where(jor => jor.PROPERTYID == lgsrData.PropertyID && jor.ISCURRENT == true).FirstOrDefault();

                    //if (JournalData.STATUSID != getStatusId.StatusId)
                    //{
                    //    JournalData.STATUSID = getStatusId.StatusId;
                    //    context.SaveChanges();
                    //}

                    #endregion

                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    trans.Complete();
                    success = true;
                }
                else
                {
                    success = false;
                }
            }

            return success;
        }

        /// <summary>
        /// This function saves the CP12 Document in the database
        /// </summary>
        /// <param name="documentStream"></param>
        /// <param name="LGSRID"></param>
        /// <returns></returns>
        public bool updateCP12Document(Stream documentStream, int LGSRID, bool tenantHanded)
        {
            P_LGSR objLgsr = new P_LGSR();
            P_LGSR_HISTORY objLgsrHistory = new P_LGSR_HISTORY();

            var existingIssue = context.P_LGSR.Where(lgsr => lgsr.LGSRID == LGSRID);

            if (existingIssue.Count() > 0)
            {
                using (TransactionScope trans = new TransactionScope())
                {
                    #region "Update P_LGSR Table"

                    objLgsr = existingIssue.FirstOrDefault();

                    byte[] lDocumentStream = ConvertStreamToByteArray(documentStream);
                    //Update P_LGSR
                    objLgsr.CP12DOCUMENT = lDocumentStream;
                    objLgsr.TENANTHANDED = tenantHanded;

                    //Insert into P_LGSR_HISTORY
                    objLgsrHistory.LGSRID = objLgsr.LGSRID;
                    objLgsrHistory.PROPERTYID = objLgsr.PROPERTYID;
                    objLgsrHistory.ISSUEDATE = objLgsr.ISSUEDATE;
                    objLgsrHistory.CP12NUMBER = objLgsr.CP12NUMBER;
                    objLgsrHistory.CP12ISSUEDBY = objLgsr.CP12ISSUEDBY;
                    objLgsrHistory.DOCUMENTTYPE = objLgsr.DOCUMENTTYPE;
                    objLgsrHistory.NOTES = objLgsr.NOTES;
                    objLgsrHistory.DTIMESTAMP = objLgsr.DTIMESTAMP;
                    objLgsrHistory.CP12DOCUMENT = lDocumentStream;
                    objLgsrHistory.RECEVIEDONBEHALF = objLgsr.RECEVIEDONBEHALF;
                    objLgsrHistory.RECEVIEDDATE = objLgsr.RECEVIEDDATE;
                    objLgsrHistory.TENANTHANDED = tenantHanded;
                    objLgsrHistory.INSPECTIONCARRIED = objLgsr.INSPECTIONCARRIED;
                    objLgsrHistory.JOURNALID = objLgsr.JOURNALID;

                    context.AddToP_LGSR_HISTORY(objLgsrHistory);
                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);

                    #endregion

                    #region "Update and Insert in Journal

                    UpdateAndInsertJournal(objLgsr);

                    #endregion

                    trans.Complete();
                }
                return true;
            }
            else
            {
                return false;
            }
        }
        //Change#35 - Behroz - 12/17/2012 - End
        #endregion

        #region "Check Appointment Id"

        public bool checkAppointmentID(int appointmentID)
        {
            var id = context.PS_Appointment.Where(app => app.AppointId == appointmentID);
            if (id.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        //Change#12 - Behroz - 19/12/2012 - Start
        public bool checkAppointmentIDGas(string PropertyID)
        {
            var data = from jor in context.AS_JOURNAL
                       join app in context.AS_APPOINTMENTS on jor.JOURNALID equals app.JournalId
                       where jor.PROPERTYID == PropertyID && jor.ISCURRENT == true && jor.INSPECTIONTYPEID == 1
                       select app.APPOINTMENTID;

            if (data.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        //Change#12 - Behroz - 19/12/2012 - End
        #endregion

        #region "Update And Insert Into Journal"

        //Change - Behroz - 29/12/2012 - Start
        /// <summary>
        /// This function updates the journal table, inserts in the journal history table. As per the comments of the client
        /// following implementation has been done.
        /// "If the appoinment does not exist against PropertyId (JournalId), then another appointment can be created. To create the appointment, we need to have a Journal Id. If the JournalId exists in AS_APPOINTMENTS, it suggests that appointment already exists. In order to create a new appointment for same property, we need to ensure that the JournalId is not repeated. In other words, one JournalId will only have one record in AS_APPOINTMENTS. The appliance servicing team stance is correct
        /// In terms of your 2nd question, hitting Save on screen 17 (means issuing certificate) will update the status of AS_journal to CP12 Issued. Also it will then make IsCurrent=0 and Insert a new reocord in AS_JOurnal with status 'Appointment to be Arranged' with isCurrent=1 for newly inserted record"
        /// </summary>
        /// <param name="workData"></param>
        private void UpdateAndInsertJournal(LGSRData workData)
        {
            #region "Update AS_JOURNAL Table"

            //Update the status in AS_JOURNAL
            //Insert in AS_JOURNALHISTORY            
            AS_JOURNALHISTORY lobjJournalHistory = new AS_JOURNALHISTORY();

            var getStatusId = context.AS_Status.Where(status => status.Title == Utilities.Constants.MessageConstants.CP12IssuedNameInAS_Status).FirstOrDefault();
            var JournalData = context.AS_JOURNAL.Where(jor => jor.PROPERTYID == workData.propertyId && jor.ISCURRENT == true).FirstOrDefault();
            var AppointmentData = context.AS_APPOINTMENTS.Where(app => app.JournalId == JournalData.JOURNALID).FirstOrDefault();

            JournalData.STATUSID = getStatusId.StatusId;
            JournalData.ISCURRENT = false;
            JournalData.CREATEDBY = workData.IssuedBy;
            JournalData.CREATIONDATE = DateTime.Now;

            context.SaveChanges();

            #endregion

            #region "Insert into AS_JOURNALHISTORY table"

            lobjJournalHistory.JOURNALID = JournalData.JOURNALID;
            lobjJournalHistory.PROPERTYID = JournalData.PROPERTYID;
            lobjJournalHistory.STATUSID = getStatusId.StatusId;
            lobjJournalHistory.ACTIONID = JournalData.ACTIONID;
            lobjJournalHistory.INSPECTIONTYPEID = JournalData.INSPECTIONTYPEID;
            lobjJournalHistory.CREATIONDATE = JournalData.CREATIONDATE;
            lobjJournalHistory.CREATEDBY = JournalData.CREATEDBY;
            lobjJournalHistory.NOTES = AppointmentData.NOTES;
            lobjJournalHistory.ISLETTERATTACHED = false;
            lobjJournalHistory.IsDocumentAttached = false;

            context.AddToAS_JOURNALHISTORY(lobjJournalHistory);
            context.SaveChanges();

            #endregion

            #region "Insert into AS_JOURNAL"

            //Insert in AS_JOURNAL and AS_JOURNALHISTORY
            AS_JOURNAL lobjNewJournal = new AS_JOURNAL();
            AS_JOURNALHISTORY lobjNewJournalHistory = new AS_JOURNALHISTORY();

            var lIdOfAppointmentToBeArrangedStatus = context.AS_Status.Where(status => status.Title == Utilities.Constants.MessageConstants.AppointmentToBeArrangedStatus).FirstOrDefault();

            lobjNewJournal.PROPERTYID = JournalData.PROPERTYID;
            lobjNewJournal.STATUSID = lIdOfAppointmentToBeArrangedStatus.StatusId;
            lobjNewJournal.ACTIONID = JournalData.ACTIONID;
            lobjNewJournal.INSPECTIONTYPEID = JournalData.INSPECTIONTYPEID;
            lobjNewJournal.CREATIONDATE = DateTime.Now;
            lobjNewJournal.CREATEDBY = workData.IssuedBy;
            lobjNewJournal.ISCURRENT = true;

            context.AddToAS_JOURNAL(lobjNewJournal);
            context.SaveChanges();

            #endregion

            #region "Insert into AS_JOURNALHISTORY"

            lobjNewJournalHistory.JOURNALID = lobjNewJournal.JOURNALID;
            lobjNewJournalHistory.PROPERTYID = lobjNewJournal.PROPERTYID;
            lobjNewJournalHistory.STATUSID = getStatusId.StatusId;
            lobjNewJournalHistory.ACTIONID = lobjNewJournal.ACTIONID;
            lobjNewJournalHistory.INSPECTIONTYPEID = lobjNewJournal.INSPECTIONTYPEID;
            lobjNewJournalHistory.CREATIONDATE = lobjNewJournal.CREATIONDATE;
            lobjNewJournalHistory.CREATEDBY = lobjNewJournal.CREATEDBY;
            lobjNewJournalHistory.NOTES = AppointmentData.NOTES;
            lobjNewJournalHistory.ISLETTERATTACHED = false;
            lobjNewJournalHistory.IsDocumentAttached = false;

            context.AddToAS_JOURNALHISTORY(lobjNewJournalHistory);

            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);

            #endregion
        }

        /// <summary>
        /// This function updates the journal table, inserts in the journal history table. As per the comments of the client
        /// following implementation has been done.
        /// "If the appoinment does not exist against PropertyId (JournalId), then another appointment can be created. To create the appointment, we need to have a Journal Id. If the JournalId exists in AS_APPOINTMENTS, it suggests that appointment already exists. In order to create a new appointment for same property, we need to ensure that the JournalId is not repeated. In other words, one JournalId will only have one record in AS_APPOINTMENTS. The appliance servicing team stance is correct
        /// In terms of your 2nd question, hitting Save on screen 17 (means issuing certificate) will update the status of AS_journal to CP12 Issued. Also it will then make IsCurrent=0 and Insert a new reocord in AS_JOurnal with status 'Appointment to be Arranged' with isCurrent=1 for newly inserted record"
        /// </summary>
        /// <param name="workData"></param>
        private void UpdateAndInsertJournal(P_LGSR workData)
        {
            #region "Update AS_JOURNAL Table"

            //Update the status in AS_JOURNAL
            //Insert in AS_JOURNALHISTORY            
            AS_JOURNALHISTORY lobjJournalHistory = new AS_JOURNALHISTORY();

            var getStatusId = context.AS_Status.Where(status => status.Title == Utilities.Constants.MessageConstants.CP12IssuedNameInAS_Status).FirstOrDefault();
            var JournalData = context.AS_JOURNAL.Where(jor => jor.PROPERTYID == workData.PROPERTYID && jor.ISCURRENT == true).FirstOrDefault();
            var AppointmentData = context.AS_APPOINTMENTS.Where(app => app.JournalId == JournalData.JOURNALID).FirstOrDefault();

            JournalData.STATUSID = getStatusId.StatusId;
            JournalData.ISCURRENT = false;
            JournalData.CREATEDBY = workData.CP12ISSUEDBY;
            JournalData.CREATIONDATE = DateTime.Now;

            context.SaveChanges();

            #endregion

            #region "Update Appointment"

            AppointmentData.APPOINTMENTSTATUS = AppointmentProgressStatus.Finished.ToString();
            context.SaveChanges();

            #endregion

            //#region "Insert Appointment History"

            //AS_APPOINTMENTSHISTORY apphis = new AS_APPOINTMENTSHISTORY()
            //{
            //    APPOINTMENTALERT = AppointmentData.APPOINTMENTALERT,
            //    APPOINTMENTCALENDER = AppointmentData.APPOINTMENTCALENDER,
            //    APPOINTMENTDATE = AppointmentData.APPOINTMENTDATE,
            //    APPOINTMENTENDTIME = AppointmentData.APPOINTMENTENDTIME,
            //    APPOINTMENTSTARTTIME = AppointmentData.APPOINTMENTSTARTTIME,
            //    APPOINTMENTSTATUS = AppointmentProgressStatus.Finished.ToString(),
            //    ASSIGNEDTO = AppointmentData.ASSIGNEDTO,
            //    CREATEDBY = AppointmentData.CREATEDBY,
            //    JournalId = AppointmentData.JournalId,
            //    JSGNUMBER = AppointmentData.JSGNUMBER.ToString(),
            //    APPOINTMENTID = AppointmentData.APPOINTMENTID,
            //    APPOINTMENTSHIFT = AppointmentData.APPOINTMENTSHIFT,
            //    JOURNALHISTORYID = AppointmentData.JOURNALHISTORYID,
            //    LOGGEDDATE = DateTime.Now,
            //    NOTES = AppointmentData.NOTES,
            //    TENANCYID = AppointmentData.TENANCYID,
            //    SURVEYOURSTATUS = AppointmentData.SURVEYOURSTATUS
            //};

            //context.AddToAS_APPOINTMENTSHISTORY(apphis);
            //context.SaveChanges();

            //#endregion

            #region "Insert into AS_JOURNALHISTORY table"

            lobjJournalHistory.JOURNALID = JournalData.JOURNALID;
            lobjJournalHistory.PROPERTYID = JournalData.PROPERTYID;
            lobjJournalHistory.STATUSID = getStatusId.StatusId;
            lobjJournalHistory.ACTIONID = null; //JournalData.ACTIONID; Changed  11-07-2013
            lobjJournalHistory.INSPECTIONTYPEID = JournalData.INSPECTIONTYPEID;
            lobjJournalHistory.CREATIONDATE = JournalData.CREATIONDATE;
            lobjJournalHistory.CREATEDBY = JournalData.CREATEDBY;
            lobjJournalHistory.NOTES = AppointmentData.NOTES;
            lobjJournalHistory.ISLETTERATTACHED = false;
            lobjJournalHistory.IsDocumentAttached = false;

            context.AddToAS_JOURNALHISTORY(lobjJournalHistory);
            context.SaveChanges();

            #endregion

            #region "Insert into AS_JOURNAL"

            //Insert in AS_JOURNAL and AS_JOURNALHISTORY
            AS_JOURNAL lobjNewJournal = new AS_JOURNAL();
            AS_JOURNALHISTORY lobjNewJournalHistory = new AS_JOURNALHISTORY();

            var lIdOfAppointmentToBeArrangedStatus = context.AS_Status.Where(status => status.Title == Utilities.Constants.MessageConstants.AppointmentToBeArrangedStatus).FirstOrDefault();

            lobjNewJournal.PROPERTYID = JournalData.PROPERTYID;
            lobjNewJournal.STATUSID = lIdOfAppointmentToBeArrangedStatus.StatusId;
            lobjNewJournal.ACTIONID = null; //JournalData.ACTIONID; Changed  14-05-2013
            lobjNewJournal.INSPECTIONTYPEID = JournalData.INSPECTIONTYPEID;
            lobjNewJournal.CREATIONDATE = DateTime.Now;
            lobjNewJournal.CREATEDBY = workData.CP12ISSUEDBY;
            lobjNewJournal.ISCURRENT = true;

            context.AddToAS_JOURNAL(lobjNewJournal);
            context.SaveChanges();

            #endregion

            #region "Insert into AS_JOURNALHISTORY"

            lobjNewJournalHistory.JOURNALID = lobjNewJournal.JOURNALID;
            lobjNewJournalHistory.PROPERTYID = lobjNewJournal.PROPERTYID;
            lobjNewJournalHistory.STATUSID = lIdOfAppointmentToBeArrangedStatus.StatusId;
            lobjNewJournalHistory.ACTIONID = lobjNewJournal.ACTIONID;
            lobjNewJournalHistory.INSPECTIONTYPEID = lobjNewJournal.INSPECTIONTYPEID;
            lobjNewJournalHistory.CREATIONDATE = lobjNewJournal.CREATIONDATE;
            lobjNewJournalHistory.CREATEDBY = lobjNewJournal.CREATEDBY;
            lobjNewJournalHistory.NOTES = AppointmentData.NOTES;
            lobjNewJournalHistory.ISLETTERATTACHED = false;
            lobjNewJournalHistory.IsDocumentAttached = false;

            context.AddToAS_JOURNALHISTORY(lobjNewJournalHistory);

            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);

            #endregion
        }

        //Change - Behroz - 29/12/2012 - End

        #endregion

        #region Utility Function to Convert to Byte Array

        /// <summary>
        /// This function converts Stream to byte[]
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static byte[] ConvertStreamToByteArray(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        #endregion
    }
}
