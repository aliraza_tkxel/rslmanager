﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PropSurvey.Dal.Base;
using PropSurvey.Entities;
using PropSurvey.Utilities.Constants;
using PropSurvey.Contracts.Data;
using PropSurvey.Contracts.Data.Detector;
using PropSurvey.Utilities.Helpers;
using PropSurvey.Contracts.Data.Appointment;

namespace PropSurvey.Dal.AppScreen
{
    public class AppScreenDal : BaseDal
    {
        string propertyId = string.Empty;
        int paramItemIdCount = 1;

        public string getApplicationScreens(string propertyId, int appointmentId)
        {
            try
            {

                if (propertyId != string.Empty)
                {
                    this.propertyId = propertyId;
                }

                string completeJson = string.Empty;

                string jsonOpen = string.Empty;
                string jsonClose = string.Empty;
                string innerBlock = string.Empty;

                StringBuilder jsonComplete = new StringBuilder();
                jsonComplete.Append("{\"response\":{\"Image\":true, \"NameAddressLabel\":true,\"Type\":\"Menu\",");


                var location = (from loc in context.PA_LOCATION
                                orderby loc.LocationSorder ascending
                                select loc).ToList();

                if (location.Count() > 0)
                {
                    jsonComplete.Append(getLocations(location));
                }
                //Adding Appointment Info
                var proprtyApp = (from app in context.PS_Appointment
                                  join p2a in context.PS_Property2Appointment on app.AppointId equals p2a.AppointId
                                  join ten in context.C_TENANCY on p2a.TenancyId equals ten.TENANCYID into tenancy
                                  from tenan in tenancy.DefaultIfEmpty()
                                  join pro in context.P__PROPERTY on p2a.PropertyId equals pro.PROPERTYID
                                  orderby app.AppointId
                                  where p2a.PropertyId == propertyId
                                  && p2a.AppointId == appointmentId
                                  select new { p2a.AppointId }).ToList();
                if (proprtyApp.Count() > 0)
                {
                    jsonComplete.Append(", \"appointmentinfo\":{\"appointmentId\":" + proprtyApp[0].AppointId.ToString() + ",\"propertyId\":\"" + propertyId + "\"}");
                }
                else
                {
                    jsonComplete.Append(", \"appointmentinfo\":{\"appointmentId\":\"null\",\"propertyId\":\"" + propertyId + "\"}");
                }

                #region "Property Detectors"                

                var propertyDetectors = getPropertyDetectors(propertyId);
                jsonComplete.Append(", \"propertyInfo\":{\"propertyDetectors\":" + propertyDetectors + "}");

                #endregion

                #region "Common"                

                var plannedComponents = getPlannedComponents();
                var heatingCertificates = getHeatingCertificates();
                var fuelDatasets = getHeatingFuelFieldDataset();

                jsonComplete.Append(", \"common\":{" +
                    "\"plannedComponents\":" + plannedComponents + "," +
                    "\"certificates\":" + heatingCertificates + "," +
                    "\"fuelFieldsDataset\":" + fuelDatasets +
                    "}");

                #endregion

                jsonComplete.Append("},");
                jsonComplete.Append(" \"status\":{\"code\":\"200\", \"message\":\"success\" }}");
                jsonComplete = jsonComplete.Replace(System.Environment.NewLine, " ");
                return jsonComplete.ToString();


            }
            catch (Exception)
            {
                throw;
            }
        }

        #region Get Property Detectors

        public string getPropertyDetectors(string propertyId)
        {
            List<PropertyDetector> detectorList = new List<PropertyDetector>();

            var detectorsQueryResult = from detector in context.P_DETECTOR
                                       join detectorType in context.AS_DetectorType on detector.DetectorTypeId equals detectorType.DetectorTypeId
                                       where detector.PropertyId == propertyId
                                       select new PropertyDetector
                                       {
                                           detectorId = detector.DetectorId,
                                           detectorType = detectorType.DetectorType,
                                           detectorTypeId = detector.DetectorTypeId,
                                           installedBy = detector.InstalledBy,
                                           installedDate = detector.InstalledDate,
                                           isLandlordsDetector = detector.IsLandlordsDetector,
                                           location = detector.Location,
                                           manufacturer = detector.Manufacturer,
                                           powerTypeId = detector.PowerSource,
                                           propertyId = detector.PropertyId,
                                           serialNumber = detector.SerialNumber
                                       };

            if (detectorsQueryResult.Count() > 0)
                detectorList = detectorsQueryResult.ToList();

            string detectorJson = JsonHelper.SerializerToString(detectorList);

            return detectorJson;
        }

        #endregion

        #region Get Planned Components

        public string getPlannedComponents()
        {
            List<PlannedComponentItemData> componentList = new List<PlannedComponentItemData>();

            var componentQueryResult = from cmpItm in context.PLANNED_COMPONENT_ITEM
                                       join comp in context.PLANNED_COMPONENT on cmpItm.COMPONENTID equals comp.COMPONENTID
                                       where cmpItm.ISACTIVE == true
                                       select new PlannedComponentItemData
                                       {
                                           componentId = cmpItm.COMPONENTID,
                                           componentName = comp.COMPONENTNAME,
                                           itemId = cmpItm.ITEMID,
                                           parameterId = cmpItm.PARAMETERID,
                                           valueId = cmpItm.VALUEID,
                                           subParameterId = cmpItm.SubParameter,
                                           subValueId = cmpItm.SubValue,
                                           cycle = comp.CYCLE,
                                           frequency = comp.FREQUENCY
                                           ,isComponentAccounting = comp.ISACCOUNTING
                                       };

            if (componentQueryResult.Count() > 0)
                componentList = componentQueryResult.ToList();

            string componentJson = JsonHelper.SerializerToString(componentList);

            return componentJson;
        }

        #endregion

        #region Get Heating Certificates

        public string getHeatingCertificates()
        {
            List<HeatingCertificate> certificateList = new List<HeatingCertificate>();

            var certQueryResult = from cert in context.P_LGSR_Certificates
                                       where cert.IsActive == true
                                       select new HeatingCertificate
                                       {
                                           certificateId = cert.CertificateId,
                                           certificateName = cert.Title,
                                           fuelTypeId = cert.HeatingType
                                       };

            if (certQueryResult.Count() > 0)
                certificateList = certQueryResult.ToList();

            string certJson = JsonHelper.SerializerToString(certificateList);

            return certJson;
        }

        #endregion

        public string getLocations(List<PropSurvey.Entities.PA_LOCATION> location)
        {
            string locationNames = string.Empty;
            string singleLocationFieldBlock = string.Empty;
            string allLocationFieldBlocks = string.Empty;
            string completeLocations = string.Empty;

            //convert the result into list
            var locationList = location.ToList();

            foreach (var loc in locationList)
            {
                //the below statement will produce string like this : "Main Construction","Externals","Internals",
                locationNames = locationNames + "\"" + loc.LocationName + "\",";


                var area = (from ar in context.PA_AREA
                            where ar.LocationId == loc.LocationID && ar.ShowInApp == true && ar.IsActive == true
                            orderby ar.AreaSorder ascending
                            select ar).ToList();

                if (area.Count() > 0)
                {
                    //The below statement will produce the statement something like this: "Main Construction":{.....},"Externals":{.....},"Internals":{.....},
                    singleLocationFieldBlock = singleLocationFieldBlock + getArea(area, loc.LocationName) + ",";
                }
            }

            //removing the last comma from string like this: "Main Construction","Externals","Internals",
            locationNames = locationNames.Substring(0, locationNames.Length - 1);

            //the below statement 'll look like this 
            //"Order": ["Main Construction","Externals","Internals"]
            locationNames = "\"Order\":[" + locationNames + "]";


            if (singleLocationFieldBlock.Length > 1)
            {
                //the below statement 'll look like this with comma in the last
                //"Order": ["Main Construction","Externals","Internals"],                
                locationNames = locationNames + ",";

                allLocationFieldBlocks = singleLocationFieldBlock.Substring(0, singleLocationFieldBlock.Length - 1);

                //the below statement will make the string like this: "Fields":{Main Construction":{.....},"Externals":{.....},"Internals":{.....}}
                allLocationFieldBlocks = "\"Fields\":{" + allLocationFieldBlocks + "}";
            }

            //the below statement will make the string like this: 
            //"Order": ["Main Construction","Externals","Internals"],"Fields":{Main Construction":{.....},"Externals":{.....},"Internals":{.....}}            
            completeLocations = locationNames + allLocationFieldBlocks;
            // Get Appointment Id against Property


            return completeLocations;
        }

        public string getArea(List<PropSurvey.Entities.PA_AREA> area, string locationName)
        {
            string completeAreas = string.Empty;
            string areaNames = string.Empty;
            string singleAreaFieldBlock = string.Empty;
            string allAreaFieldBlocks = string.Empty;

            //convert the result into list
            var areaList = area.ToList();

            foreach (var ar in areaList)
            {
                //the below statement will produce string like this : "Dwelling","Garage","Outbuilding","Communal Building","Apartment Block",
                areaNames = areaNames + "\"" + ar.AreaName + "\",";

                var item = (from it in context.PA_ITEM
                            where it.AreaID == ar.AreaID && it.IsActive == true && it.ParentItemId == null && it.ShowInApp == true
                            orderby it.ItemSorder ascending
                            select it).ToList();

                if (item.Count() > 0)
                {
                    //The below statement will produce the statement something like this: "Dwelling":{.....},"Garage":{.....},"Communal Building":{.....},"Apartment Block":{.....},
                    singleAreaFieldBlock = singleAreaFieldBlock + getItem(item, ar.AreaName) + ",";
                }
            }

            //removing the last comma from string like this: "Dwelling","Garage","Outbuilding","Communal Building","Apartment Block",
            areaNames = areaNames.Substring(0, areaNames.Length - 1);

            //"Order": ["Dwelling","Garage","Outbuilding","Communal Building","Apartment Block"],            
            areaNames = "\"Order\":[" + areaNames + "]";


            if (singleAreaFieldBlock.Length > 1)
            {
                //the below statement 'll look like this with comma in the last
                //"Order": ["Dwelling","Garage","Outbuilding","Communal Building","Apartment Block"],            
                areaNames = areaNames + ",";

                allAreaFieldBlocks = singleAreaFieldBlock.Substring(0, singleAreaFieldBlock.Length - 1);

                //the below statement will make the string like this: "Fields":{Dwelling":{.....},"Garage":{.....},"Outbuilding":{.....},"Communal Building":{.....},"Apartment Block":{.....}}
                allAreaFieldBlocks = "\"Fields\":{" + allAreaFieldBlocks + "}";

            }


            completeAreas = "\"" + locationName + "\":{\"Type\":\"Menu\",\"Image\":false, " + areaNames + allAreaFieldBlocks + "}";

            return completeAreas;
        }

        public string getItem(List<PA_ITEM> item, string areaName)
        {

            string completeItems = string.Empty;
            string itemNames = string.Empty;
            string singleItemFieldBlock = string.Empty;
            string allItemFieldBlocks = string.Empty;

            //convert the result into list
            var itemList = item.ToList();

            foreach (var it in itemList)
            {
                //the below statement will produce string like this : "Dwelling","Garage","Outbuilding","Communal Building","Apartment Block",
                itemNames = itemNames + "\"" + it.ItemName + "\",";

                var itemPrameter = (from itp in context.PA_ITEM_PARAMETER
                                    join par in context.PA_PARAMETER on itp.ParameterId equals par.ParameterID
                                    where itp.ItemId == it.ItemID && it.IsActive == true && itp.IsActive == true && par.IsActive == true
                                    orderby par.ParameterSorder ascending
                                    select itp).ToList();


                var subItem = (from subIt in context.PA_ITEM
                               where subIt.ParentItemId == it.ItemID && subIt.IsActive == true && subIt.ShowInApp == true
                               orderby subIt.ItemSorder ascending
                               select subIt).ToList();
                //Special requirement from client. Gas item will be picked up from P_LGSR table & it will be read only
                if (it.ItemName == "Heating")
                {
                    singleItemFieldBlock = singleItemFieldBlock + this.getHeatingValues(it.ItemName, it.ItemID);
                }
                else if (subItem.Count() > 0)
                {

                    if (itemPrameter.Count() > 0)
                    {
                        //The below statement will produce the statement something like this: "Dwelling":{.....},"Garage":{.....},"Communal Building":{.....},"Apartment Block":{.....},
                        singleItemFieldBlock = singleItemFieldBlock + getItemParameter(itemPrameter, it.ItemName, it.ItemID, true) + ",";
                        //itemNames = itemNames + "\"" + it.ItemName + "\",";
                    }
                    else
                    {
                        singleItemFieldBlock = singleItemFieldBlock + getSubItem(subItem, it.ItemName, false) + ",";
                    }

                    //{
                    //The below statement will produce the statement something like this: "Dwelling":{.....},"Garage":{.....},"Communal Building":{.....},"Apartment Block":{.....},

                    //}
                }
                else
                {
                    if (itemPrameter.Count() > 0)
                    {
                        //The below statement will produce the statement something like this: "Dwelling":{.....},"Garage":{.....},"Communal Building":{.....},"Apartment Block":{.....},
                        singleItemFieldBlock = singleItemFieldBlock + getItemParameter(itemPrameter, it.ItemName, it.ItemID, false) + ",";
                    }
                }



            }

            //removing the last comma from string like this: "Dwelling","Garage","Outbuilding","Communal Building","Apartment Block",
            itemNames = itemNames.Substring(0, itemNames.Length - 1);

            //the below statement 'll look like this 
            //"Order": ["Dwelling","Garage","Outbuilding","Communal Building","Apartment Block"],            
            itemNames = "\"Order\":[" + itemNames + "]";


            if (singleItemFieldBlock.Length > 1)
            {
                //the below statement 'll look like this with comma in the last
                //"Order": ["Dwelling","Garage","Outbuilding","Communal Building","Apartment Block"],            
                itemNames = itemNames + ",";

                allItemFieldBlocks = singleItemFieldBlock.Substring(0, singleItemFieldBlock.Length - 1);

                //the below statement will make the string like this: "Fields":{Dwelling":{.....},"Garage":{.....},"Outbuilding":{.....},"Communal Building":{.....},"Apartment Block":{.....}}
                allItemFieldBlocks = "\"Fields\":{" + allItemFieldBlocks + "}";
            }


            completeItems = "\"" + areaName + "\":{\"Type\":\"Menu\",\"Image\":false, " + itemNames + allItemFieldBlocks + "}";

            return completeItems;
        }

        public string getItemParameter(List<PA_ITEM_PARAMETER> itemParameter, string itemName, int itemId, bool isParentParameter)
        {

            string paramNames = string.Empty;
            string completeItemParam = string.Empty;
            string singleItemParamFieldBlock = string.Empty;
            string itemDatesBlock = string.Empty;
            string allItemParamFieldBlocks = string.Empty;

            //convert the result into list
            var itemParamList = itemParameter.ToList();
            if (isParentParameter == true)
            {
                var subItem = (from subIt in context.PA_ITEM
                               where subIt.ParentItemId == itemId && subIt.IsActive == true && subIt.ShowInApp == true
                               orderby subIt.ItemSorder ascending
                               select subIt).ToList();
                //convert the result into list
                var itemList = subItem.ToList();
                singleItemParamFieldBlock = singleItemParamFieldBlock + getSubItem(subItem, itemName, true) + ",";
                paramNames = paramNames + "\"" + itemList[0].ItemName.ToString() + "\"" + ","; // paramNames + getSubItemNames(subItem);
            }
            foreach (var itp in itemParamList)
            {

                var parameter = (from par in context.PA_PARAMETER
                                 where par.ParameterID == itp.ParameterId && itp.IsActive == true && par.ShowInApp == true
                                 orderby par.ParameterSorder ascending
                                 select par).ToList();

                if (parameter.Count() > 0)
                {
                    //The below statement will produce the statement something like this: "Wall Materials":{.....},"Wall Type":{.....},"Wall Insulation":{.....},"Ground Floor Materials":{.....},
                    singleItemParamFieldBlock = singleItemParamFieldBlock + getParameter(parameter, itp.ItemParamID) + ",";
                    paramNames = paramNames + getParameterNames(parameter);
                }
            }


            if (singleItemParamFieldBlock.Length > 1)
            {

                //get the replacement due date and last done date for this item
                if (this.propertyId != string.Empty)
                {
                    itemDatesBlock = getItemDates(itemId);
                }

                if (itemDatesBlock.Length > 1)
                {
                    singleItemParamFieldBlock = singleItemParamFieldBlock + itemDatesBlock + ",";
                    // +"\"" + JsonConstants.LastReplaced + "\", \"" + JsonConstants.ReplacementDue + "\",";
                }

                allItemParamFieldBlocks = singleItemParamFieldBlock.Substring(0, singleItemParamFieldBlock.Length - 1);

                //the below statement will make the string like this: "Fields":{id:1, "Wall Materials":{.....},"Wall Type":{.....},"Wall Insulation":{.....},"Ground Floor Materials":{.....}}
                allItemParamFieldBlocks = "\"Fields\":{" + allItemParamFieldBlocks + "}";
            }

            if (paramNames.Length > 1)
            {
                //remove the comma from the last
                paramNames = paramNames.Substring(0, paramNames.Length - 1);
            }


            //the below statement will make the string something like this: "Order": ["Wall Materials","Wall Type","Wall Insulation","Ground Floor Materials"]
            paramNames = " \"Order\":[" + paramNames + "]";



            completeItemParam = "\"" + itemName + "\":{\"Type\":\"Form\",\"itemId\":" + itemId + ',' + allItemParamFieldBlocks + "," + paramNames + "}";



            return completeItemParam;
        }

        public string getParameter(List<PA_PARAMETER> parameter, int itemParamId)
        {
            string paramNames = string.Empty;
            string singleParamBlock = string.Empty;
            string singleParamValuesBlock = string.Empty;
            string allPreviouslySelectedValuesBlocks = string.Empty;
            string allParamBlocks = string.Empty;

            //convert the result into list
            var paramList = parameter.ToList();

            foreach (var pl in paramList)
            {


                var paramValue = (from parv in context.PA_PARAMETER_VALUE
                                  where parv.ParameterID == pl.ParameterID && parv.IsActive == true
                                  orderby parv.Sorder ascending
                                  select parv).ToList();

                if (paramValue.Count() > 0)
                {
                    //The below statement will produce the statement something like this: "Dwelling":{.....},"Garage":{.....},"Communal Building":{.....},"Apartment Block":{.....},
                    singleParamValuesBlock = singleParamValuesBlock + getParameterValue(paramValue);
                }

                if (this.propertyId != string.Empty)
                {
                    allPreviouslySelectedValuesBlocks = this.previouslySelectedValues(pl.ParameterID, itemParamId, pl.ParameterName);
                }

                paramNames = "\"" + pl.ParameterName + "\":{\"itemParamId\":" + itemParamId + ", \"paramId\":" + pl.ParameterID + ", \"Type\":\"" + pl.ControlType + "\"," + allPreviouslySelectedValuesBlocks;

                if (singleParamValuesBlock.Length > 1)
                {


                    singleParamBlock = singleParamBlock + paramNames + "," + singleParamValuesBlock + "},";
                }
                else
                {
                    //the below statement will make the string like this: "Condition": {"Type": "YesNo"},
                    singleParamBlock = singleParamBlock + paramNames + "},";
                }

            }


            if (singleParamBlock.Length > 1)
            {


                allParamBlocks = singleParamBlock.Substring(0, singleParamBlock.Length - 1);

            }

            return allParamBlocks;
        }

        public string getParameterValue(List<PA_PARAMETER_VALUE> paramValue)
        {
            string values = string.Empty;
            //convert the result into list
            var paramValList = paramValue.ToList();
            var completeValuesBlock = string.Empty;

            foreach (var pvl in paramValList)
            {

                //below line will make the string like this: "Timber","Chipboard","Block and beam","Solid","Insulated",
                values = values + "\"" + pvl.ValueDetail + "\":\"" + pvl.ValueID + "\",";
            }

            if (values.Length > 1)
            {
                //remove the comma and string will look like this: "Timber","Chipboard","Block and beam","Solid","Insulated"
                values = values.Substring(0, values.Length - 1);

                //the value of below line will look like this:"Values:["Timber","Chipboard","Block and beam","Solid","Insulated"]"
                completeValuesBlock = "\"Values\":{" + values + "}";
            }

            return completeValuesBlock;
        }

        public string getParameterNames(List<PA_PARAMETER> parameter)
        {

            string paramNames = string.Empty;
            //convert the result into list
            var paramValList = parameter.ToList();

            foreach (var par in paramValList)
            {

                //below line will make the string like this: "Timber","Chipboard","Block and beam","Solid","Insulated",
                paramNames = "\"" + par.ParameterName + "\"" + ",";
            }

            return paramNames;

        }

        public string getFuelDatasetDefaultTemplateValue(string parameterName, int parameterValueId)
        {
            string singleDefaultValueBlock = string.Empty;
            string defaultValueBlocks = string.Empty;

            if (parameterName.Equals(JsonConstants.HeatingFuelParam))
            {
                string parameterValue = context.PA_PARAMETER_VALUE.Where(x => x.ValueID == parameterValueId)
                                               .First()
                                               .ValueDetail;
                singleDefaultValueBlock = singleDefaultValueBlock + "\"" + parameterValue + "\":{\"id\":\"" + parameterValueId.ToString() + "\",\"IsCheckBoxSelected\":\"" + string.Empty + "\"},";
            }

            if (singleDefaultValueBlock.Length > 1)
            {
                defaultValueBlocks = singleDefaultValueBlock.Substring(0, singleDefaultValueBlock.Length - 1);
            }

            defaultValueBlocks = "\"Selected\":{" + defaultValueBlocks + "}";

            return defaultValueBlocks;

        }

        public string previouslySelectedValues(int parameterId, int itemParamId, string parameterName = "")
        {
            string singleSelectedValueBlock = string.Empty;
            string allSelectedValueBlocks = string.Empty;
            PA_PARAMETER paramData = new PA_PARAMETER();
            PA_ITEM_PARAMETER paItemParam = new PA_ITEM_PARAMETER();
            paramData = context.PA_PARAMETER.Where(item => item.ParameterID == parameterId).FirstOrDefault();
            paItemParam = context.PA_ITEM_PARAMETER.Where(item => item.ItemParamID == itemParamId).FirstOrDefault();
            //   paramData=  context.PA_PARAMETER on itp.ParameterId equals par.ParameterID
            if (paramData.ControlType == "Date")
            {
                if (parameterId == JsonConstants.MainDwellingRoofLastReplacedId || parameterId == JsonConstants.MainDwellingRoofReplacementDueId)
                {
                    var datesRecord = (from pad in context.PA_PROPERTY_ITEM_DATES

                                       where pad.PROPERTYID == this.propertyId && pad.ParameterId == null && pad.ItemId == paItemParam.ItemId
                                       select new { pad.DueDate, pad.LastDone, pad.SID }).ToList();
                    if (datesRecord.Count() > 0)
                    {
                        var date = datesRecord.First();
                        if (parameterId == JsonConstants.MainDwellingRoofLastReplacedId)
                            singleSelectedValueBlock = singleSelectedValueBlock + "\"" + date.LastDone + "\":{\"id\":\"" + date.SID + "\",\"IsCheckBoxSelected\":\"\"},";
                        else if (parameterId == JsonConstants.MainDwellingRoofReplacementDueId)
                            singleSelectedValueBlock = singleSelectedValueBlock + "\"" + date.DueDate + "\":{\"id\":\"" + date.SID + "\",\"IsCheckBoxSelected\":\"\"},";


                    }
                }
                else if (paramData.ParameterName == JsonConstants.LastRewired || paramData.ParameterName == JsonConstants.RewireDue ||
                    paramData.ParameterName == JsonConstants.CurLastReplaced || paramData.ParameterName == JsonConstants.CurDue ||
                     paramData.ParameterName == JsonConstants.ElectricUpgradeDue || paramData.ParameterName == JsonConstants.UpgradeLastDone)
                //if (paramData.ParameterName.Contains("Last"))
                {
                    if (paramData.ParameterName == JsonConstants.LastRewired)
                    {
                        var datesRecord = (from pad in context.PA_PROPERTY_ITEM_DATES
                                           join par in context.PA_PARAMETER on pad.ParameterId equals par.ParameterID
                                           where pad.PROPERTYID == this.propertyId && par.ParameterName == JsonConstants.LastRewired && pad.ItemId == paItemParam.ItemId
                                           select new { pad.DueDate, pad.LastDone, pad.SID }).ToList();
                        if (datesRecord.Count() > 0)
                        {
                            var date = datesRecord.First();
                            singleSelectedValueBlock = singleSelectedValueBlock + "\"" + date.LastDone + "\":{\"id\":\"" + date.SID + "\",\"IsCheckBoxSelected\":\"\"},";

                        }
                        // join par in context.PA_PARAMETER on itp.ParameterId equals par.ParameterID
                    }
                    else if (paramData.ParameterName == JsonConstants.CurLastReplaced)
                    {
                        var datesRecord = (from pad in context.PA_PROPERTY_ITEM_DATES
                                           join par in context.PA_PARAMETER on pad.ParameterId equals par.ParameterID
                                           where pad.PROPERTYID == this.propertyId && par.ParameterName == JsonConstants.CurLastReplaced && pad.ItemId == paItemParam.ItemId
                                           select new { pad.DueDate, pad.LastDone, pad.SID }).ToList();
                        if (datesRecord.Count() > 0)
                        {
                            var date = datesRecord.First();
                            singleSelectedValueBlock = singleSelectedValueBlock + "\"" + date.LastDone + "\":{\"id\":\"" + date.SID + "\",\"IsCheckBoxSelected\":\"\"},";

                        }
                        // join par in context.PA_PARAMETER on itp.ParameterId equals par.ParameterID
                    }
                    else if (paramData.ParameterName == JsonConstants.UpgradeLastDone)
                    {
                        var datesRecord = (from pad in context.PA_PROPERTY_ITEM_DATES
                                           join par in context.PA_PARAMETER on pad.ParameterId equals par.ParameterID
                                           where pad.PROPERTYID == this.propertyId && par.ParameterName == JsonConstants.UpgradeLastDone && pad.ItemId == paItemParam.ItemId
                                           select new { pad.DueDate, pad.LastDone, pad.SID }).ToList();
                        if (datesRecord.Count() > 0)
                        {
                            var date = datesRecord.First();
                            singleSelectedValueBlock = singleSelectedValueBlock + "\"" + date.LastDone + "\":{\"id\":\"" + date.SID + "\",\"IsCheckBoxSelected\":\"\"},";

                        }
                        // join par in context.PA_PARAMETER on itp.ParameterId equals par.ParameterID
                    }
                    else if (paramData.ParameterName == JsonConstants.RewireDue)
                    {
                        var datesRecord = (from pad in context.PA_PROPERTY_ITEM_DATES
                                           join par in context.PA_PARAMETER on pad.ParameterId equals par.ParameterID
                                           where pad.PROPERTYID == this.propertyId && par.ParameterName == JsonConstants.LastRewired && pad.ItemId == paItemParam.ItemId
                                           select new { pad.DueDate, pad.LastDone, pad.SID }).ToList();
                        if (datesRecord.Count() > 0)
                        {
                            var date = datesRecord.First();
                            singleSelectedValueBlock = singleSelectedValueBlock + "\"" + date.DueDate + "\":{\"id\":\"" + date.SID + "\",\"IsCheckBoxSelected\":\"\"},";

                        }
                        // join par in context.PA_PARAMETER on itp.ParameterId equals par.ParameterID
                    }
                    else if (paramData.ParameterName == JsonConstants.CurDue)
                    {
                        var datesRecord = (from pad in context.PA_PROPERTY_ITEM_DATES
                                           join par in context.PA_PARAMETER on pad.ParameterId equals par.ParameterID
                                           where pad.PROPERTYID == this.propertyId && par.ParameterName == JsonConstants.CurLastReplaced && pad.ItemId == paItemParam.ItemId
                                           select new { pad.DueDate, pad.LastDone, pad.SID }).ToList();
                        if (datesRecord.Count() > 0)
                        {
                            var date = datesRecord.First();
                            singleSelectedValueBlock = singleSelectedValueBlock + "\"" + date.DueDate + "\":{\"id\":\"" + date.SID + "\",\"IsCheckBoxSelected\":\"\"},";

                        }
                        // join par in context.PA_PARAMETER on itp.ParameterId equals par.ParameterID
                    }
                    else if (paramData.ParameterName == JsonConstants.ElectricUpgradeDue)
                    {
                        var datesRecord = (from pad in context.PA_PROPERTY_ITEM_DATES
                                           join par in context.PA_PARAMETER on pad.ParameterId equals par.ParameterID
                                           where pad.PROPERTYID == this.propertyId && par.ParameterName == JsonConstants.UpgradeLastDone && pad.ItemId == paItemParam.ItemId
                                           select new { pad.DueDate, pad.LastDone, pad.SID }).ToList();
                        if (datesRecord.Count() > 0)
                        {
                            var date = datesRecord.First();
                            singleSelectedValueBlock = singleSelectedValueBlock + "\"" + date.DueDate + "\":{\"id\":\"" + date.SID + "\",\"IsCheckBoxSelected\":\"\"},";

                        }
                        // join par in context.PA_PARAMETER on itp.ParameterId equals par.ParameterID
                    }
                }
                else
                {
                    var datesRecord = (from pad in context.PA_PROPERTY_ITEM_DATES
                                       where pad.ParameterId == parameterId
                                       && pad.PROPERTYID == this.propertyId
                                       select new { pad.DueDate, pad.LastDone, pad.SID }).ToList();

                    if (datesRecord.Count() > 0)
                    {
                        var date = datesRecord.First();
                        singleSelectedValueBlock = singleSelectedValueBlock + "\"" + date.DueDate + "\":{\"id\":\"" + date.SID + "\",\"IsCheckBoxSelected\":\"\"},";
                    }
                }
            }
            else
            {
                var selectedValue = (from par in context.PA_PROPERTY_ATTRIBUTES
                                     where par.ITEMPARAMID == itemParamId
                                     && par.PROPERTYID == this.propertyId && (!(parameterName.ToLower().Contains("condition rating")))
                                     select par).ToList();

                if (selectedValue.Count() > 0)
                {
                    foreach (var item in selectedValue)
                    {
                        if (item.PARAMETERVALUE == null)
                        {
                            item.PARAMETERVALUE = "";
                        }
                        string parameterValue = item.PARAMETERVALUE.Replace("\n", " ");
                        singleSelectedValueBlock = singleSelectedValueBlock + "\"" + parameterValue + "\":{\"id\":\"" + item.VALUEID + "\",\"IsCheckBoxSelected\":\"" + item.IsCheckBoxSelected.ToString() + "\"},";
                    }
                    //end for each selectedValueCount
                }
                //end if: selectedValueCount.count >0
            }//end else

            if (singleSelectedValueBlock.Length > 1)
            {
                allSelectedValueBlocks = singleSelectedValueBlock.Substring(0, singleSelectedValueBlock.Length - 1);
            }


            allSelectedValueBlocks = "\"Selected\":{" + allSelectedValueBlocks + "}";



            return allSelectedValueBlocks;
        }

        #region "Heating Area"

        #region Get Heating Values
        public string getHeatingValues(string itemName, int itemId)
        {            
            string paramNames = string.Empty;
            string fieldBlock = string.Empty;
            string heatingBlock = string.Empty;

            var heatingList = (from     phm in context.PA_HeatingMapping
                               where    phm.IsActive == true && phm.PropertyId == this.propertyId
                               orderby  phm.HeatingMappingId ascending
                               select   phm).ToList();
            int counter = 0;
            foreach (var heating in heatingList)
            {
                counter++;
                var heatingName = string.Format("Heating Type {0}", counter);
                paramNames = paramNames + "\"" + heatingName + "\"" + ",";
                fieldBlock = fieldBlock + getHeatingItem(itemId, (int)heating.HeatingType, heatingName, heating.HeatingMappingId);
            }

            if (paramNames.Length > 0)
            {
                paramNames = paramNames.Substring(0, paramNames.Length - 1);
            }

            if (fieldBlock.Length > 0)
            {
                fieldBlock = fieldBlock.Substring(0, fieldBlock.Length - 1);
            }

            heatingBlock = "\"" + itemName + "\":{\"Type\":\"Menu\",\"Image\":false, \"Fields\": {" + fieldBlock + "}, \"Order\":[" + paramNames + "]},";
            return heatingBlock;
        }
        #endregion

        #region "Get Heating Item"

        public string getHeatingItem(int itemId,int fuelTypeId,string heatingName, int heatingId)
        {
            string issueDateBlock = string.Empty;
            string certificateNumberBlock = string.Empty;
            string certificateNameBlock = string.Empty;
            string certificateNameSelectedBlock = string.Empty;
            string expiryDateBlock = string.Empty;
            string certificateNumberSelectedBlock = string.Empty;
            string issueDateSelectedBlock = string.Empty;
            string renewalDateSelectedBlock = string.Empty;
            string renewalDateBlock = string.Empty;

            string singleItemSelectedValues = string.Empty;
            string singleItemFieldBlock = string.Empty;
            string singleItemValues = string.Empty;
            string paramNames = string.Empty;
            string fieldBlock = string.Empty;
            string gasBlock = string.Empty;


            var certificateName = context.P_LGSR_Certificates
                                             .Where(x => x.HeatingType == fuelTypeId)
                                             .Select(y => y.Title)
                                             .FirstOrDefault();

            if (certificateName != null)
            {
                
                var certificateQuery = (from plg in context.P_LGSR
                                        join ht in context.PA_HeatingMapping on plg.HeatingMappingId equals ht.HeatingMappingId
                                        where plg.PROPERTYID == this.propertyId && ht.HeatingType == fuelTypeId
                                        orderby plg.ISSUEDATE descending
                                        select plg);

                var certificateValues = certificateQuery.FirstOrDefault();

                if (certificateValues != null)
                {
                    certificateNameSelectedBlock = "\"Selected\":{\"" + (certificateName != null ? certificateName.ToString() : "") + "\":{\"id\":0, \"IsCheckBoxSelected\": \"\"}}";
                    issueDateSelectedBlock  = "\"Selected\":{\"" + (certificateValues.ISSUEDATE != null ? certificateValues.ISSUEDATE.ToString() : "") + "\":{\"id\":0, \"IsCheckBoxSelected\": \"\"}}";
                    certificateNumberSelectedBlock = "\"Selected\":{\"" + certificateValues.CP12NUMBER + "\":{\"id\":0, \"IsCheckBoxSelected\": \"\"}}";
                    renewalDateSelectedBlock = "\"Selected\":{\"" + (certificateValues.CP12Renewal != null ? certificateValues.CP12Renewal.ToString() : "") + "\":{\"id\":0, \"IsCheckBoxSelected\": \"\"}}";

                    certificateNameBlock = "\"Certificate Name\":{\"itemParamId\":0, \"paramId\":0, \"Type\":\"Textbox\", \"ReadOnly\": true, \"Values\":{\"" + (certificateName != null ? certificateName.ToString() : "") + "\":0}," + certificateNameSelectedBlock + "}";
                    issueDateBlock = "\"Certificate Issued\":{\"itemParamId\":0, \"paramId\":0, \"Type\":\"Date\", \"ReadOnly\": true, \"Values\":{\"" + (certificateValues.ISSUEDATE != null ? certificateValues.ISSUEDATE.ToString() : "") + "\":0}," + issueDateSelectedBlock + "}";
                    certificateNumberBlock = "\"Certificate Number\":{\"itemParamId\":0, \"paramId\":0, \"Type\":\"Textbox\", \"ReadOnly\": true, \"Values\":{\"" + certificateValues.CP12NUMBER + "\":0}," + certificateNumberSelectedBlock + "}";
                    renewalDateBlock = "\"Certificate Renewal\":{\"itemParamId\":0, \"paramId\":0, \"Type\":\"Date\", \"ReadOnly\": true, \"Values\":{\"" + (certificateValues.CP12Renewal != null ? certificateValues.CP12Renewal.ToString() : "") + "\":0}," + renewalDateSelectedBlock + "}";

                    fieldBlock = certificateNameBlock + "," + issueDateBlock + "," + certificateNumberBlock + "," + renewalDateBlock + ",";
                    paramNames = "\"Certificate Name\",\"Certificate Issued\", \"Certificate Number\", \"Certificate Renewal\",";
                }
                else {

                    certificateNameSelectedBlock = "\"Selected\":{\"" + (certificateName != null ? certificateName.ToString() : "") + "\":{\"id\":0, \"IsCheckBoxSelected\": \"\"}}";
                    certificateNumberSelectedBlock = "\"Selected\":{\"" + "" + "\":{\"id\":0, \"IsCheckBoxSelected\": \"\"}}";
                    issueDateSelectedBlock = "\"Selected\":{\"" + "" + "\":{\"id\":0, \"IsCheckBoxSelected\": \"\"}}";
                    renewalDateSelectedBlock = "\"Selected\":{\"" + "" + "\":{\"id\":0, \"IsCheckBoxSelected\": \"\"}}";

                    certificateNameBlock = "\"Certificate Name\":{\"itemParamId\":0, \"paramId\":0, \"Type\":\"Textbox\", \"ReadOnly\": true, \"Values\":{\"" + (certificateName != null ? certificateName.ToString() : "") + "\":0}," + certificateNameSelectedBlock + "}";
                    certificateNumberBlock = "\"Certificate Number\":{\"itemParamId\":0, \"paramId\":0, \"Type\":\"Textbox\", \"ReadOnly\": true, \"Values\":{\"" + "" + "\":0}," + certificateNumberSelectedBlock + "}";
                    issueDateBlock = "\"Certificate Issued\":{\"itemParamId\":0, \"paramId\":0, \"Type\":\"Date\", \"ReadOnly\": true, \"Values\":{\"" + "" + "\":0}," + issueDateSelectedBlock + "}";
                    renewalDateBlock = "\"Certificate Renewal\":{\"itemParamId\":0, \"paramId\":0, \"Type\":\"Date\", \"ReadOnly\": true, \"Values\":{\"" + "" + "\":0}," + renewalDateSelectedBlock + "}";

                    fieldBlock = certificateNameBlock + "," + issueDateBlock + "," + certificateNumberBlock + "," + renewalDateBlock + ",";
                    paramNames = "\"Certificate Name\",\"Certificate Issued\", \"Certificate Number\", \"Certificate Renewal\",";
                
                }
            }


            var itemList = (from it in context.PA_ITEM
                            join itp in context.PA_ITEM_PARAMETER on it.ItemID equals itp.ItemId
                            join par in context.PA_PARAMETER on itp.ParameterId equals par.ParameterID
                            where it.ItemID == itemId && it.IsActive == true 
                            && itp.IsActive == true && par.IsActive == true 
                            && it.ShowInApp == true && par.ShowInApp == true
                            && (itp.ParameterValueId == null || itp.ParameterValueId == fuelTypeId)
                            orderby par.ParameterSorder ascending
                            select new { it, itp, par }).ToList();

            foreach (var it in itemList)
            {
                if (this.propertyId != string.Empty)
                {
                    paramNames = paramNames + "\"" + it.par.ParameterName + "\"" + ",";

                    var isAlternativeHeating = this.isAlternativeHeating(fuelTypeId);
                    
                    var paramValue = (from parv in context.PA_PARAMETER_VALUE
                                      where parv.ParameterID == it.par.ParameterID
                                      orderby parv.Sorder ascending
                                      select parv).ToList();

                    // Condition for Manufacturer Parameter to show diplay values if Alternative Heating Type is selected
                    if (it.par.ParameterName.Equals(JsonConstants.ManufacturerParam))
                    {
                        var heatingManufacturerValues = paramValue.Where(x => isAlternativeHeating
                                                                               ? x.IsAlterNativeHeating == true
                                                                               : (x.IsAlterNativeHeating == null || x.IsAlterNativeHeating == false)
                                                                         ).ToList();
                        singleItemValues = getParameterValue(heatingManufacturerValues);
                    }
                    else
                    {
                        singleItemValues = getParameterValue(paramValue);
                    }

                    bool isReadOnly = isReadOnlyParameter(it.par.ParameterName);
                    
                    singleItemSelectedValues = getHeatingPreviouslySelectedValues(it.par.ParameterID,it.itp.ItemParamID,heatingId ,it.par.ParameterName);
                    if (singleItemValues == string.Empty)
                    {
                        singleItemFieldBlock = singleItemFieldBlock + "\"" + it.par.ParameterName + "\":{\"itemParamId\":" + it.itp.ItemParamID + ", \"paramId\":" + it.itp.ParameterId + ", \"Type\":\"" + it.par.ControlType + "\", \"ReadOnly\": " + isReadOnly.ToString().ToLower() + " " + singleItemValues + "," + singleItemSelectedValues + "},";
                    }
                    else
                    {
                        singleItemFieldBlock = singleItemFieldBlock + "\"" + it.par.ParameterName + "\":{\"itemParamId\":" + it.itp.ItemParamID + ", \"paramId\":" + it.itp.ParameterId + ", \"Type\":\"" + it.par.ControlType + "\", \"ReadOnly\": " + isReadOnly.ToString().ToLower() + " ," + singleItemValues + "," + singleItemSelectedValues + "},";
                    }
                }
            }

            if (singleItemFieldBlock.Length > 0)
            {
                singleItemFieldBlock = singleItemFieldBlock.Substring(0, singleItemFieldBlock.Length - 1);
                fieldBlock = fieldBlock + singleItemFieldBlock + ",";
            }

            if (paramNames.Length > 0)
            {
                paramNames = paramNames.Substring(0, paramNames.Length - 1);
            }

            if (fieldBlock.Length > 0)
            {
                fieldBlock = fieldBlock.Substring(0, fieldBlock.Length - 1);
            }

            gasBlock = "\"" + heatingName + "\":{\"Type\": \"Form\", \"itemId\":" + itemId +", \"heatingTypeId\":"+ heatingId +", \"Fields\": {" + fieldBlock + "}, \"Order\":[" + paramNames + "]},";
            return gasBlock;
        }
        #endregion

        #region "Is Alternative Heating"
        
        public bool isAlternativeHeating(int fuelTypeId)
        {
            var nonAlternative = new List<string>() { "Mains Gas", "Oil" };
            var alternativeHeatingList = (from ppv in context.PA_PARAMETER_VALUE
                                          join pp in context.PA_PARAMETER on ppv.ParameterID equals pp.ParameterID
                                          where pp.ParameterName == "Heating Fuel" && ppv.IsActive == true && !nonAlternative.Contains(ppv.ValueDetail)
                                          select ppv).ToList();
            return alternativeHeatingList.Where(x => x.ValueID == fuelTypeId).Any() ? true : false;
        }
        #endregion

        #region "Check parameter is read only"

        private bool isReadOnlyParameter(string parameterName)
        {
            string[] readOnlyParams = { "Heating Fuel", " due" };
            bool isReadOnly = readOnlyParams.Any(x => parameterName.ToLower().IndexOf(x.ToLower()) != -1);
            return isReadOnly;
        }

        #endregion

        #region "Get Heating Fuel Dataset"
        public string getHeatingFuelFieldDataset()
        {
            string fuelDatalist = string.Empty;

            var fuelTypes = (from ppv in context.PA_PARAMETER_VALUE
                              join pp in context.PA_PARAMETER on ppv.ParameterID equals pp.ParameterID
                              where pp.ParameterName == "Heating Fuel" && ppv.IsActive == true
                              orderby ppv.Sorder ascending
                              select ppv).ToList();

            foreach (var fuelType in fuelTypes)
            {
                var fuelDataset = getHeatingFuelFieldsTemplateById((int)fuelType.ValueID);
                fuelDatalist = fuelDatalist + fuelDataset;
            }

            if (fuelDatalist.Length > 0)
            {
                fuelDatalist = fuelDatalist.Substring(0, fuelDatalist.Length - 1);
            }

            fuelDatalist = "[" + fuelDatalist + "]";
            return fuelDatalist;

        }
        #endregion

        #region "Get Heating Fuel Fields Dataset By Id"

        public string getHeatingFuelFieldsTemplateById(int fuelTypeId)
        {
            string issueDateBlock = string.Empty;
            string certificateNumberBlock = string.Empty;
            string certificateNameBlock = string.Empty;
            string renewalDateBlock = string.Empty;
            string certificateNumberSelectedBlock = string.Empty;
            string issueDateSelectedBlock = string.Empty;
            string renewalDateSelectedBlock = string.Empty;
            string certificateNameSelectedBlock = string.Empty;

            string singleItemSelectedValues = string.Empty;
            string singleItemFieldBlock = string.Empty;
            string singleItemValues = string.Empty;
            string paramNames = string.Empty;
            string fieldBlock = string.Empty;
            string fuelDataset = string.Empty;

            var certificateName = context.P_LGSR_Certificates
                                             .Where(x => x.HeatingType == fuelTypeId)
                                             .Select(y => y.Title)
                                             .FirstOrDefault();

            if (certificateName != null)
            {

                var certificateQuery = (from plg in context.P_LGSR
                                        join ht in context.PA_HeatingMapping on plg.HeatingMappingId equals ht.HeatingMappingId
                                        where plg.PROPERTYID == this.propertyId && ht.HeatingType == fuelTypeId
                                        orderby plg.ISSUEDATE descending
                                        select plg);

                var certificateValues = certificateQuery.FirstOrDefault();

                if (certificateValues != null)
                {
                    certificateNameSelectedBlock = "\"Selected\":{\"" + (certificateName != null ? certificateName.ToString() : "") + "\":{\"id\":0, \"IsCheckBoxSelected\": \"\"}}";
                    issueDateSelectedBlock = "\"Selected\":{\"" + (certificateValues.ISSUEDATE != null ? certificateValues.ISSUEDATE.ToString() : "") + "\":{\"id\":0, \"IsCheckBoxSelected\": \"\"}}";
                    certificateNumberSelectedBlock = "\"Selected\":{\"" + certificateValues.CP12NUMBER + "\":{\"id\":0, \"IsCheckBoxSelected\": \"\"}}";
                    renewalDateSelectedBlock = "\"Selected\":{\"" + (certificateValues.CP12Renewal != null ? certificateValues.CP12Renewal.ToString() : "") + "\":{\"id\":0, \"IsCheckBoxSelected\": \"\"}}";

                    certificateNameBlock = "\"Certificate Name\":{\"itemParamId\":0, \"paramId\":0, \"Type\":\"Textbox\", \"ReadOnly\": true, \"Values\":{\"" + (certificateName != null ? certificateName.ToString() : "") + "\":0}," + certificateNameSelectedBlock + "}";
                    issueDateBlock = "\"Certificate Issued\":{\"itemParamId\":0, \"paramId\":0, \"Type\":\"Date\", \"ReadOnly\": true, \"Values\":{\"" + (certificateValues.ISSUEDATE != null ? certificateValues.ISSUEDATE.ToString() : "") + "\":0}," + issueDateSelectedBlock + "}";
                    certificateNumberBlock = "\"Certificate Number\":{\"itemParamId\":0, \"paramId\":0, \"Type\":\"Textbox\", \"ReadOnly\": true, \"Values\":{\"" + certificateValues.CP12NUMBER + "\":0}," + certificateNumberSelectedBlock + "}";
                    renewalDateBlock = "\"Certificate Renewal\":{\"itemParamId\":0, \"paramId\":0, \"Type\":\"Date\", \"ReadOnly\": true, \"Values\":{\"" + (certificateValues.CP12Renewal != null ? certificateValues.CP12Renewal.ToString() : "") + "\":0}," + renewalDateSelectedBlock + "}";

                    fieldBlock = certificateNameBlock + "," + issueDateBlock + "," + certificateNumberBlock + "," + renewalDateBlock + ",";
                    paramNames = "\"Certificate Name\",\"Certificate Issued\", \"Certificate Number\", \"Certificate Renewal\",";
                }
                else
                {

                    certificateNameSelectedBlock = "\"Selected\":{\"" + (certificateName != null ? certificateName.ToString() : "") + "\":{\"id\":0, \"IsCheckBoxSelected\": \"\"}}";
                    certificateNumberSelectedBlock = "\"Selected\":{\"" + "" + "\":{\"id\":0, \"IsCheckBoxSelected\": \"\"}}";
                    issueDateSelectedBlock = "\"Selected\":{\"" + "" + "\":{\"id\":0, \"IsCheckBoxSelected\": \"\"}}";
                    renewalDateSelectedBlock = "\"Selected\":{\"" + "" + "\":{\"id\":0, \"IsCheckBoxSelected\": \"\"}}";

                    certificateNameBlock = "\"Certificate Name\":{\"itemParamId\":0, \"paramId\":0, \"Type\":\"Textbox\", \"ReadOnly\": true, \"Values\":{\"" + (certificateName != null ? certificateName.ToString() : "") + "\":0}," + certificateNameSelectedBlock + "}";
                    certificateNumberBlock = "\"Certificate Number\":{\"itemParamId\":0, \"paramId\":0, \"Type\":\"Textbox\", \"ReadOnly\": true, \"Values\":{\"" + "" + "\":0}," + certificateNumberSelectedBlock + "}";
                    issueDateBlock = "\"Certificate Issued\":{\"itemParamId\":0, \"paramId\":0, \"Type\":\"Date\", \"ReadOnly\": true, \"Values\":{\"" + "" + "\":0}," + issueDateSelectedBlock + "}";
                    renewalDateBlock = "\"Certificate Renewal\":{\"itemParamId\":0, \"paramId\":0, \"Type\":\"Date\", \"ReadOnly\": true, \"Values\":{\"" + "" + "\":0}," + renewalDateSelectedBlock + "}";

                    fieldBlock = certificateNameBlock + "," + issueDateBlock + "," + certificateNumberBlock + "," + renewalDateBlock + ",";
                    paramNames = "\"Certificate Name\",\"Certificate Issued\", \"Certificate Number\", \"Certificate Renewal\",";

                }
            }


            var itemList = (from it in context.PA_ITEM
                            join itp in context.PA_ITEM_PARAMETER on it.ItemID equals itp.ItemId
                            join par in context.PA_PARAMETER on itp.ParameterId equals par.ParameterID
                            where it.ItemName == "Heating" 
                                  && it.IsActive == true && itp.IsActive == true 
                                  && par.IsActive == true && it.ShowInApp == true 
                                  && par.ShowInApp == true
                                  && (itp.ParameterValueId == null || itp.ParameterValueId == fuelTypeId)
                            orderby par.ParameterSorder ascending
                            select new { it, itp, par }).ToList();

            foreach (var it in itemList)
            {
                if (this.propertyId != string.Empty)
                {
                    paramNames = paramNames + "\"" + it.par.ParameterName + "\"" + ",";
                    var isAlternativeHeating = this.isAlternativeHeating(fuelTypeId);

                    var paramValue = (from parv in context.PA_PARAMETER_VALUE
                                      where parv.ParameterID == it.par.ParameterID
                                      orderby parv.Sorder ascending
                                      select parv).ToList();

                    // Condition for Manufacturer Parameter to show diplay values if Alternative Heating Type is selected
                    if (it.par.ParameterName.Equals(JsonConstants.ManufacturerParam))
                    {
                        var heatingManufacturerValues = paramValue.Where(x => isAlternativeHeating
                                                                               ? x.IsAlterNativeHeating == true
                                                                               : (x.IsAlterNativeHeating == null || x.IsAlterNativeHeating == false)
                                                                         ).ToList();
                        singleItemValues = getParameterValue(heatingManufacturerValues);
                    }
                    else
                    {
                        singleItemValues = getParameterValue(paramValue);
                    }
                                        
                    singleItemSelectedValues = getFuelDatasetDefaultTemplateValue(it.par.ParameterName,fuelTypeId);

                    string isReadOnly = "false";
                    if (it.par.ParameterName.ToLower().IndexOf(" due") != -1)
                    {
                        isReadOnly = "true";
                    }
                    
                    if (singleItemValues == string.Empty)
                    {
                        singleItemFieldBlock = singleItemFieldBlock + "\"" + it.par.ParameterName + "\":{\"itemParamId\":" + it.itp.ItemParamID + ", \"paramId\":" + it.itp.ParameterId + ", \"Type\":\"" + it.par.ControlType + "\", \"ReadOnly\": " + isReadOnly + " " + singleItemValues + "," + singleItemSelectedValues + "},";
                    }
                    else
                    {
                        singleItemFieldBlock = singleItemFieldBlock + "\"" + it.par.ParameterName + "\":{\"itemParamId\":" + it.itp.ItemParamID + ", \"paramId\":" + it.itp.ParameterId + ", \"Type\":\"" + it.par.ControlType + "\", \"ReadOnly\": " + isReadOnly + " ," + singleItemValues + "," + singleItemSelectedValues + "},";
                    }
                }
            }


            if (singleItemFieldBlock.Length > 0)
            {
                singleItemFieldBlock = singleItemFieldBlock.Substring(0, singleItemFieldBlock.Length - 1);
                fieldBlock = fieldBlock + singleItemFieldBlock + ",";
            }

            if (paramNames.Length > 0)
            {
                paramNames = paramNames.Substring(0, paramNames.Length - 1);
            }

            if (fieldBlock.Length > 0)
            {
                fieldBlock = fieldBlock.Substring(0, fieldBlock.Length - 1);
            }


            fuelDataset =  "{\"fuelTypeId\":" + fuelTypeId + ", \"Fields\": {" + fieldBlock + "}, \"Order\":[" + paramNames + "]},";

            return fuelDataset;
        }
        #endregion

        #region "Get Heating Previously Selected Values"
        public string getHeatingPreviouslySelectedValues(int parameterId, int itemParamId, int heatingId, string parameterName = "")
        {
            string singleSelectedValueBlock = string.Empty;
            string allSelectedValueBlocks = string.Empty;
            PA_PARAMETER paramData = new PA_PARAMETER();
            PA_ITEM_PARAMETER paItemParam = new PA_ITEM_PARAMETER();
            paramData = context.PA_PARAMETER.Where(item => item.ParameterID == parameterId).FirstOrDefault();
            paItemParam = context.PA_ITEM_PARAMETER.Where(item => item.ItemParamID == itemParamId).FirstOrDefault();
            //   paramData=  context.PA_PARAMETER on itp.ParameterId equals par.ParameterID
            if (paramData.ControlType == "Date")
            {
                if (parameterId == JsonConstants.MainDwellingRoofLastReplacedId || parameterId == JsonConstants.MainDwellingRoofReplacementDueId)
                {
                    var datesRecord = (from pad in context.PA_PROPERTY_ITEM_DATES
                                       where pad.PROPERTYID == this.propertyId && (pad.ParameterId == null || pad.ParameterId == 0)
                                                && pad.ItemId == paItemParam.ItemId && pad.HeatingMappingId == heatingId
                                       select new { pad.DueDate, pad.LastDone, pad.SID }).ToList();
                    if (datesRecord.Count() > 0)
                    {
                        var date = datesRecord.First();
                        if (parameterId == JsonConstants.MainDwellingRoofLastReplacedId)
                            singleSelectedValueBlock = singleSelectedValueBlock + "\"" + date.LastDone + "\":{\"id\":\"" + date.SID + "\",\"IsCheckBoxSelected\":\"\"},";
                        else if (parameterId == JsonConstants.MainDwellingRoofReplacementDueId)
                            singleSelectedValueBlock = singleSelectedValueBlock + "\"" + date.DueDate + "\":{\"id\":\"" + date.SID + "\",\"IsCheckBoxSelected\":\"\"},";
                    }
                }
                else if (paramData.ParameterName == JsonConstants.LastRewired || paramData.ParameterName == JsonConstants.RewireDue ||
                    paramData.ParameterName == JsonConstants.CurLastReplaced || paramData.ParameterName == JsonConstants.CurDue ||
                     paramData.ParameterName == JsonConstants.ElectricUpgradeDue || paramData.ParameterName == JsonConstants.UpgradeLastDone)
                //if (paramData.ParameterName.Contains("Last"))
                {
                    if (paramData.ParameterName == JsonConstants.LastRewired)
                    {
                        var datesRecord = (from pad in context.PA_PROPERTY_ITEM_DATES
                                           join par in context.PA_PARAMETER on pad.ParameterId equals par.ParameterID
                                           where pad.PROPERTYID == this.propertyId && par.ParameterName == JsonConstants.LastRewired
                                                && pad.ItemId == paItemParam.ItemId
                                                && pad.HeatingMappingId == heatingId
                                           select new { pad.DueDate, pad.LastDone, pad.SID }).ToList();
                        if (datesRecord.Count() > 0)
                        {
                            var date = datesRecord.First();
                            singleSelectedValueBlock = singleSelectedValueBlock + "\"" + date.LastDone + "\":{\"id\":\"" + date.SID + "\",\"IsCheckBoxSelected\":\"\"},";

                        }
                        // join par in context.PA_PARAMETER on itp.ParameterId equals par.ParameterID
                    }
                    else if (paramData.ParameterName == JsonConstants.CurLastReplaced)
                    {
                        var datesRecord = (from pad in context.PA_PROPERTY_ITEM_DATES
                                           join par in context.PA_PARAMETER on pad.ParameterId equals par.ParameterID
                                           where pad.PROPERTYID == this.propertyId && par.ParameterName == JsonConstants.CurLastReplaced
                                           && pad.ItemId == paItemParam.ItemId
                                           && pad.HeatingMappingId == heatingId
                                           select new { pad.DueDate, pad.LastDone, pad.SID }).ToList();
                        if (datesRecord.Count() > 0)
                        {
                            var date = datesRecord.First();
                            singleSelectedValueBlock = singleSelectedValueBlock + "\"" + date.LastDone + "\":{\"id\":\"" + date.SID + "\",\"IsCheckBoxSelected\":\"\"},";

                        }
                        // join par in context.PA_PARAMETER on itp.ParameterId equals par.ParameterID
                    }
                    else if (paramData.ParameterName == JsonConstants.UpgradeLastDone)
                    {
                        var datesRecord = (from pad in context.PA_PROPERTY_ITEM_DATES
                                           join par in context.PA_PARAMETER on pad.ParameterId equals par.ParameterID
                                           where pad.PROPERTYID == this.propertyId && par.ParameterName == JsonConstants.UpgradeLastDone
                                            && pad.ItemId == paItemParam.ItemId
                                            && pad.HeatingMappingId == heatingId
                                           select new { pad.DueDate, pad.LastDone, pad.SID }).ToList();
                        if (datesRecord.Count() > 0)
                        {
                            var date = datesRecord.First();
                            singleSelectedValueBlock = singleSelectedValueBlock + "\"" + date.LastDone + "\":{\"id\":\"" + date.SID + "\",\"IsCheckBoxSelected\":\"\"},";

                        }
                        // join par in context.PA_PARAMETER on itp.ParameterId equals par.ParameterID
                    }
                    else if (paramData.ParameterName == JsonConstants.RewireDue)
                    {
                        var datesRecord = (from pad in context.PA_PROPERTY_ITEM_DATES
                                           join par in context.PA_PARAMETER on pad.ParameterId equals par.ParameterID
                                           where pad.PROPERTYID == this.propertyId && par.ParameterName == JsonConstants.LastRewired
                                           && pad.ItemId == paItemParam.ItemId
                                           && pad.HeatingMappingId == heatingId
                                           select new { pad.DueDate, pad.LastDone, pad.SID }).ToList();
                        if (datesRecord.Count() > 0)
                        {
                            var date = datesRecord.First();
                            singleSelectedValueBlock = singleSelectedValueBlock + "\"" + date.DueDate + "\":{\"id\":\"" + date.SID + "\",\"IsCheckBoxSelected\":\"\"},";

                        }
                        // join par in context.PA_PARAMETER on itp.ParameterId equals par.ParameterID
                    }
                    else if (paramData.ParameterName == JsonConstants.CurDue)
                    {
                        var datesRecord = (from pad in context.PA_PROPERTY_ITEM_DATES
                                           join par in context.PA_PARAMETER on pad.ParameterId equals par.ParameterID
                                           where pad.PROPERTYID == this.propertyId && par.ParameterName == JsonConstants.CurLastReplaced
                                           && pad.ItemId == paItemParam.ItemId
                                           && pad.HeatingMappingId == heatingId
                                           select new { pad.DueDate, pad.LastDone, pad.SID }).ToList();
                        if (datesRecord.Count() > 0)
                        {
                            var date = datesRecord.First();
                            singleSelectedValueBlock = singleSelectedValueBlock + "\"" + date.DueDate + "\":{\"id\":\"" + date.SID + "\",\"IsCheckBoxSelected\":\"\"},";

                        }
                        // join par in context.PA_PARAMETER on itp.ParameterId equals par.ParameterID
                    }
                    else if (paramData.ParameterName == JsonConstants.ElectricUpgradeDue)
                    {
                        var datesRecord = (from pad in context.PA_PROPERTY_ITEM_DATES
                                           join par in context.PA_PARAMETER on pad.ParameterId equals par.ParameterID
                                           where pad.PROPERTYID == this.propertyId && par.ParameterName == JsonConstants.UpgradeLastDone
                                           && pad.ItemId == paItemParam.ItemId
                                           && pad.HeatingMappingId == heatingId
                                           select new { pad.DueDate, pad.LastDone, pad.SID }).ToList();
                        if (datesRecord.Count() > 0)
                        {
                            var date = datesRecord.First();
                            singleSelectedValueBlock = singleSelectedValueBlock + "\"" + date.DueDate + "\":{\"id\":\"" + date.SID + "\",\"IsCheckBoxSelected\":\"\"},";

                        }
                        // join par in context.PA_PARAMETER on itp.ParameterId equals par.ParameterID
                    }
                }
                else
                {
                    var datesRecord = (from pad in context.PA_PROPERTY_ITEM_DATES
                                       where (pad.ParameterId == parameterId || pad.ParameterId == 0 || pad.ParameterId == null)
                                       && pad.PROPERTYID == this.propertyId
                                       && pad.HeatingMappingId == heatingId
                                       select new { pad.DueDate, pad.LastDone, pad.SID }).ToList();

                    if (datesRecord.Count() > 0)
                    {
                        var date = datesRecord.First();
                        singleSelectedValueBlock = singleSelectedValueBlock + "\"" + date.DueDate + "\":{\"id\":\"" + date.SID + "\",\"IsCheckBoxSelected\":\"\"},";
                    }
                }
            }
            else
            {
                var selectedValue = (from par in context.PA_PROPERTY_ATTRIBUTES
                                     where par.ITEMPARAMID == itemParamId
                                     && par.PROPERTYID == this.propertyId && (!(parameterName.ToLower().Contains("condition rating")))
                                     && par.HeatingMappingId == heatingId
                                     select par).ToList();

                if (selectedValue.Count() > 0)
                {
                    foreach (var item in selectedValue)
                    {
                        if (item.PARAMETERVALUE == null)
                        {
                            item.PARAMETERVALUE = "";
                        }
                        string parameterValue = item.PARAMETERVALUE.Replace("\n", " ");
                        singleSelectedValueBlock = singleSelectedValueBlock + "\"" + parameterValue + "\":{\"id\":\"" + item.VALUEID + "\",\"IsCheckBoxSelected\":\"" + item.IsCheckBoxSelected.ToString() + "\"},";
                    }
                    //end for each selectedValueCount
                }
                //end if: selectedValueCount.count >0
            }//end else

            if (singleSelectedValueBlock.Length > 1)
            {
                allSelectedValueBlocks = singleSelectedValueBlock.Substring(0, singleSelectedValueBlock.Length - 1);
            }


            allSelectedValueBlocks = "\"Selected\":{" + allSelectedValueBlocks + "}";

            return allSelectedValueBlocks;
        }

        #endregion

        #endregion

        public string getItemDates(int itemId)
        {
            string lastReplacedBlock = string.Empty;
            string replacementDueBlock = string.Empty;
            string completeBlock = string.Empty;

            //if (itemId != JsonConstants.ElectricsId && itemId != JsonConstants.SeparateId)
            //{
            var datesRecord = (from pad in context.PA_PROPERTY_ITEM_DATES
                                   //from comp in context.PLANNED_COMPONENT
                               join pcomp in context.PLANNED_COMPONENT on pad.PLANNED_COMPONENTID equals pcomp.COMPONENTID into tempComponent
                               from comp in tempComponent.DefaultIfEmpty(null)
                               where pad.ItemId == itemId
                               && pad.PROPERTYID == this.propertyId //&& pad.PLANNED_COMPONENTID == comp.COMPONENTID
                               select new SurveyDatesData
                               {
                                   DueDate = pad.DueDate,
                                   LastDone = pad.LastDone,
                                   SID = pad.SID,
                                   cycle = comp.CYCLE
                               });

            if (datesRecord.Count() > 0)
            {
                int cycle = 0;
                var dates = datesRecord.First();
                string lastReplaced = dates.LastDone.ToString();
                string replacementDue = dates.DueDate.ToString();
                string sid = dates.SID.ToString();
                if (dates.cycle != null)
                    cycle = (int)dates.cycle;
                //These <"itemParamId\":"+paramItemId+",\"paramId\":"+paramItemId+"> parameters are added to support iOS application
                lastReplacedBlock = "\"" + JsonConstants.LastReplaced + "\":{\"itemParamId\":" + paramItemIdCount + ",\"paramId\":" + paramItemIdCount + ",\"cycle\":" + cycle.ToString() + ",\"Type\":\"Date\", \"Values\":[\"" + lastReplaced + "\"], \"Selected\":{\"" + lastReplaced + "\":{\"id\":" + sid + ", \"IsCheckBoxSelected\":\"\"}}}";
                paramItemIdCount++;
                //These <"itemParamId\":"+paramItemId+",\"paramId\":"+paramItemId+"> parameters are added to support iOS application
                replacementDueBlock = "\"" + JsonConstants.ReplacementDue + "\":{\"itemParamId\":" + paramItemIdCount + ",\"paramId\":" + paramItemIdCount + ",\"Type\":\"Date\", \"Values\":[\"" + replacementDue + "\"], \"Selected\":{\"" + replacementDue + "\":{\"id\":" + sid + ", \"IsCheckBoxSelected\":\"\"}}}";
                paramItemIdCount++;
                completeBlock = lastReplacedBlock + "," + replacementDueBlock;
            }

            //else if (itemId == JsonConstants.StructureId || itemId == JsonConstants.CloakroomId || itemId == JsonConstants.FasciasId || itemId == JsonConstants.SurfaceId)
            //{
            //    string lastReplaced = string.Empty;
            //    string replacementDue = string.Empty;
            //    string sid = string.Empty;
            //    int cycle = 0;
            //    //These <"itemParamId\":"+paramItemId+",\"paramId\":"+paramItemId+"> parameters are added to support iOS application
            //    lastReplacedBlock = "\"" + JsonConstants.LastReplaced + "\":{\"itemParamId\":" + paramItemIdCount + ",\"paramId\":" + paramItemIdCount + ",\"cycle\":" + cycle + ",\"Type\":\"Date\", \"Values\":[\"" + lastReplaced + "\"], \"Selected\":{\"" + lastReplaced + "\":{\"id\":\"\", \"IsCheckBoxSelected\":\"\"}}}";
            //    paramItemIdCount++;
            //    //These <"itemParamId\":"+paramItemId+",\"paramId\":"+paramItemId+"> parameters are added to support iOS application
            //    replacementDueBlock = "\"" + JsonConstants.ReplacementDue + "\":{\"itemParamId\":" + paramItemIdCount + ",\"paramId\":" + paramItemIdCount + ",\"Type\":\"Date\", \"Values\":[\"" + replacementDue + "\"], \"Selected\":{\"" + replacementDue + "\":{\"id\":\"\", \"IsCheckBoxSelected\":\"\"}}}";
            //    paramItemIdCount++;
            //    completeBlock = lastReplacedBlock + "," + replacementDueBlock;
            //}

            // }


            return completeBlock;
        }

        public string getSubItem(List<PA_ITEM> item, string areaName, bool isParentParameter)
        {

            string completeItems = string.Empty;
            string itemNames = string.Empty;
            string singleItemFieldBlock = string.Empty;
            string allItemFieldBlocks = string.Empty;
            //convert the result into list
            var itemList = item.ToList();
            string subItemName = itemList[0].ItemName;
            foreach (var it in itemList)
            {
                //the below statement will produce string like this : "Dwelling","Garage","Outbuilding","Communal Building","Apartment Block",
                itemNames = itemNames + "\"" + it.ItemName + "\",";

                var itemPrameter = (from itp in context.PA_ITEM_PARAMETER
                                    join par in context.PA_PARAMETER on itp.ParameterId equals par.ParameterID
                                    where itp.ItemId == it.ItemID && it.IsActive == true && itp.IsActive == true && par.IsActive == true
                                    orderby par.ParameterSorder ascending
                                    select itp).ToList();

                //Special requirement from client. Gas item will be picked up from P_LGSR table & it will be read only
                if (it.ItemName == "Heating")
                {
                    singleItemFieldBlock = singleItemFieldBlock + this.getHeatingValues(it.ItemName, it.ItemID);
                }
                else
                {
                    if (itemPrameter.Count() > 0)
                    {
                        //The below statement will produce the statement something like this: "Dwelling":{.....},"Garage":{.....},"Communal Building":{.....},"Apartment Block":{.....},
                        singleItemFieldBlock = singleItemFieldBlock + getItemParameter(itemPrameter, it.ItemName, it.ItemID, false) + ",";
                    }
                }



            }

            //removing the last comma from string like this: "Dwelling","Garage","Outbuilding","Communal Building","Apartment Block",
            itemNames = itemNames.Substring(0, itemNames.Length - 1);

            //the below statement 'll look like this 
            //"Order": ["Dwelling","Garage","Outbuilding","Communal Building","Apartment Block"],            
            itemNames = "\"Order\":[" + itemNames + "]";


            if (singleItemFieldBlock.Length > 1)
            {
                //the below statement 'll look like this with comma in the last
                //"Order": ["Dwelling","Garage","Outbuilding","Communal Building","Apartment Block"],            
                itemNames = itemNames + ",";

                allItemFieldBlocks = singleItemFieldBlock.Substring(0, singleItemFieldBlock.Length - 1);

                //the below statement will make the string like this: "Fields":{Dwelling":{.....},"Garage":{.....},"Outbuilding":{.....},"Communal Building":{.....},"Apartment Block":{.....}}
                completeItems = "\"Fields\":{" + allItemFieldBlocks + "}";
            }

            if (isParentParameter == true)
            {
                completeItems = "\"" + subItemName + "\":{\"Type\":\"Menu\",\"Image\":false, " + itemNames + completeItems + "}";
            }
            else
            {
                completeItems = "\"" + areaName + "\":{\"Type\":\"Menu\",\"Image\":false, " + itemNames + completeItems + "}";
            }

            return completeItems;


        }

        public string getSubItemNames(List<PA_ITEM> item)
        {

            string itemNames = string.Empty;
            //convert the result into list
            var itemList = item.ToList();

            foreach (var it in itemList)
            {

                //below line will make the string like this: "Timber","Chipboard","Block and beam","Solid","Insulated",
                itemNames = "\"" + it.ItemName + "\"" + ",";
            }

            return itemNames;

        }
    }


}
