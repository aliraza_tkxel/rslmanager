﻿using System;
using System.IO;
using System.Net;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Transactions;
using PropSurvey.Contracts.Data;
using PropSurvey.Dal.Base;
using PropSurvey.Dal.Customer;
using PropSurvey.Dal.Faults;
using PropSurvey.Dal.IssuedReceivedBy;
using PropSurvey.Dal.Property;
using PropSurvey.Entities;
using PropSurvey.Contracts.Data.Appointment;
using PropSurvey.Utilities.Helpers;
using PropSurvey.Utilities.Constants;
using PropSurvey.Utilities.ErrorSetter;
using System.Text;
using System.Data;
using PropSurvey.Dal.Survey;
using PropSurvey.Dal.Appliances;
using PropSurvey.Dal.InstallationPipework;
using System.Web;
using PropSurvey.Contracts.Fault;
using System.Data.Common;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using PropSurvey.Contracts.Data.Detector;

namespace PropSurvey.Dal.Appointment
{
    public class AppointmentDal : BaseDal
    {
        #region get All Fault Appointments Data

        /// <summary>
        /// This function returns all fault appointments by user
        /// </summary>
        /// <param name="operativeID">operative id</param>
        /// <returns>List of Jobs data objects</returns>
        public List<AllAppointmentsList> getAllFaultAppointmentsByUser(string userName, string startDate, string endDate)
        {
            //Create a list of strings from Enums as Linq do not support/Recognize ToString() funnction.
            List<string> completedStatusesList = new List<string>() {AppointmentCompleteStatus.Finished.ToString(),
                                        AppointmentCompleteStatus.Complete.ToString(),
                                        AppointmentCompleteStatus.Completed.ToString(),
                                        AppointmentCompleteStatus.NoEntry.ToString()};

            DateTime varStartTime = DateTime.Parse(startDate);
            DateTime varEndTime = DateTime.Parse(endDate);

            var _var = (from app in context.FL_CO_APPOINTMENT
                        join fltAppt in context.FL_FAULT_APPOINTMENT on app.AppointmentID equals fltAppt.AppointmentId
                        join fltl in context.FL_FAULT_LOG on fltAppt.FaultLogId equals fltl.FaultLogID
                        join flts in context.FL_FAULT_STATUS on fltl.StatusID equals flts.FaultStatusID
                        join ten in context.C_TENANCY on fltl.PROPERTYID equals ten.PROPERTYID into tenancy
                        from tenan in tenancy.Where(tenant => tenant.ENDDATE == null).DefaultIfEmpty()
                        join pro in context.P__PROPERTY on fltl.PROPERTYID equals pro.PROPERTYID
                        join creat in context.AC_LOGINS on fltl.UserId equals creat.EMPLOYEEID
                        join login in context.AC_LOGINS on app.OperativeID equals login.EMPLOYEEID
                        join emp in context.E__EMPLOYEE on login.EMPLOYEEID equals emp.EMPLOYEEID
                        join employee in context.E__EMPLOYEE on creat.EMPLOYEEID equals employee.EMPLOYEEID
                        join noentry in context.FL_FAULT_NOENTRY on fltl.FaultLogID equals noentry.FaultLogId into tempNoEntry
                        from noentry1 in tempNoEntry.DefaultIfEmpty()
                        orderby app.AppointmentID
                        where app.AppointmentDate >= varStartTime && app.AppointmentDate <= varEndTime
                            //&& cad.ISDEFAULT == 1
                                && login.LOGIN == userName
                                && fltAppt.FaultAppointmentId == (context.FL_FAULT_APPOINTMENT.Where(a => a.AppointmentId == app.AppointmentID).Min(a => a.FaultAppointmentId))
                                && flts.Description != "Cancelled"
                                && !(completedStatusesList.Contains(app.AppointmentStatus.Replace(" ", ""))) // Filter out completed appointments.
                        select new AllAppointmentsList
                        {
                            appointmentId = app.AppointmentID,
                            appointmentNotes = app.Notes,
                            appointmentDate = app.AppointmentDate,
                            appointmentEndDate = app.AppointmentEndDate ?? app.AppointmentDate,
                            appointmentStatus = app.AppointmentStatus,
                            appointmentType = "Fault",
                            surveyorUserName = login.LOGIN,
                            tenancyId = tenan.TENANCYID,
                            appointmentStartTimeString = app.Time,
                            appointmentEndTimeString = app.EndTime,
                            createdBy = fltl.UserId,
                            creationDate = app.LastActionDate,
                            loggedDate = app.LastActionDate,
                            createdByPerson = employee.FIRSTNAME + " " + employee.LASTNAME,
                            property = new PropertyData
                            {
                                propertyId = pro.PROPERTYID,
                                tenancyId = tenan.TENANCYID,
                                houseNumber = pro.HOUSENUMBER,
                                flatNumber = pro.FLATNUMBER,
                                address1 = pro.ADDRESS1,
                                address2 = pro.ADDRESS2,
                                address3 = pro.ADDRESS3,
                                townCity = pro.TOWNCITY,
                                postCode = pro.POSTCODE,
                                county = pro.COUNTY,
                                defaultPropertyPicId = pro.PropertyPicId
                            }
                        });

            List<AllAppointmentsList> faultAppointments = new List<AllAppointmentsList>();
            if (_var.Count() > 0)
            {
                faultAppointments = _var.ToList();
                foreach (AllAppointmentsList item in faultAppointments)
                {
                    DateTime dt = (DateTime)item.appointmentDate;
                    item.appointmentStartDateTime = DateTime.Parse(((DateTime)item.appointmentDate).ToString("dd/MM/yyyy") + " " + item.appointmentStartTimeString);
                    item.appointmentEndDateTime = DateTime.Parse(((DateTime)item.appointmentEndDate).ToString("dd/MM/yyyy") + " " + item.appointmentEndTimeString);
                    if (item.appointmentStatus == "Appointment Arranged")
                        item.appointmentStatus = "NotStarted";
                    setAdditionalDataFault(item);
                    item.scheme = null;
                }
            }

            return faultAppointments;
        }

        #endregion

        #region Get Job Status Name by StatusId

        /// <summary>
        /// Get Job Status Name by StatusId
        /// </summary>
        /// <param name="statusID"></param>
        /// <returns></returns>
        public string GetJobStatusString(int statusID)
        {
            var fltStatusList = context.FL_FAULT_STATUS.Where(fltSts => fltSts.FaultStatusID == statusID);
            if (fltStatusList.Count() == 0)
                return "";

            FL_FAULT_STATUS fltStatus = fltStatusList.First();

            return fltStatus.Description;
        }

        #endregion

        #region Get Job Status Name by StatusId

        /// <summary>
        /// Get Job Status Id By Status Name
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        public int GetJobStatusID(string status)
        {
            var fltStatusList = context.FL_FAULT_STATUS.Where(fltSts => fltSts.Description == status);
            if (fltStatusList.Count() == 0)
                return 0;

            FL_FAULT_STATUS fltStatus = fltStatusList.First();

            return fltStatus.FaultStatusID;
        }

        #endregion

        #region get All Stock Appointments

        /// <summary>
        /// This function returns all Stock appointments from particular start date
        /// </summary>
        /// <returns>List of appointment data objects</returns>
        public List<AllAppointmentsList> getAllStockAppointments(string userName, string startDate, string endDate, List<int> existingAppointmentIds)
        {
            //Create a list of strings from Enums as Linq do not support/Recognize ToString() funnction.
            List<string> completedStatusesList = new List<string>() {AppointmentCompleteStatus.Finished.ToString(),
                                        AppointmentCompleteStatus.Complete.ToString(),
                                        AppointmentCompleteStatus.Completed.ToString(),
                                        AppointmentCompleteStatus.NoEntry.ToString()};

            // Converting Date received from iOS
            DateTime varStartTime = DateTime.Parse(startDate);
            DateTime varEndDate = DateTime.Parse(endDate);
            var appointments = (from app in context.PS_Appointment
                                join p2a in context.PS_Property2Appointment on app.AppointId equals p2a.AppointId
                                join ten in context.C_TENANCY on p2a.TenancyId equals ten.TENANCYID into tenancy
                                from tenan in tenancy.DefaultIfEmpty()
                                join pro in context.P__PROPERTY on p2a.PropertyId equals pro.PROPERTYID
                                join login in context.AC_LOGINS on app.CreatedBy equals login.EMPLOYEEID
                                join emp in context.E__EMPLOYEE on login.EMPLOYEEID equals emp.EMPLOYEEID
                                orderby app.CreatedDate descending
                                where

                                app.SurveyourUserName == userName
                                && app.AppointStartDateTime >= varStartTime && app.AppointStartDateTime <= varEndDate
                                && !(completedStatusesList.Contains(app.AppointProgStatus))
                                && !(existingAppointmentIds.Contains(app.AppointId))

                                select new AllAppointmentsList
                                {
                                    appointmentId = app.AppointId,
                                    surveyourAvailability = app.SurveyourStatus,
                                    appointmentLocation = app.AppointLocation,
                                    appointmentStartDateTime = app.AppointStartDateTime,
                                    appointmentEndDateTime = app.AppointEndDateTime,
                                    appointmentNotes = app.AppointNotes,
                                    appointmentValidity = app.AppointValidity,
                                    appointmentType = app.AppointType,
                                    appointmentStatus = app.AppointProgStatus,
                                    surveyorUserName = app.SurveyourUserName,
                                    surveyorAlert = app.AppointmentAlert,
                                    surveyType = app.SurveyType,
                                    appointmentCalendar = app.AppointmentCalendar,
                                    tenancyId = tenan.TENANCYID,
                                    appointmentOverdue = 0,
                                    appointmentDate = app.AppointStartDateTime,
                                    createdBy = app.CreatedBy,
                                    addToCalendar = app.addToCalendar,
                                    creationDate = app.CreatedDate,
                                    loggedDate = app.CreatedDate,
                                    createdByPerson = emp.FIRSTNAME + " " + emp.LASTNAME,
                                    ////Get Property Data
                                    property = new PropertyData
                                    {
                                        propertyId = p2a.PropertyId,
                                        tenancyId = p2a.TenancyId,
                                        houseNumber = pro.HOUSENUMBER,

                                        flatNumber = pro.FLATNUMBER,
                                        address1 = pro.ADDRESS1,
                                        address2 = pro.ADDRESS2,
                                        address3 = pro.ADDRESS3,
                                        defaultPropertyPicId = pro.PropertyPicId,
                                        townCity = pro.TOWNCITY,
                                        postCode = pro.POSTCODE,
                                        county = pro.COUNTY,
                                        lastSurveyDate = p2a.LastSurveyDate
                                    }
                                });

            List<AllAppointmentsList> appointmentList = new List<AllAppointmentsList>();

            if (appointments.Count() > 0)
            {
                appointmentList = appointments.ToList();
                foreach (AllAppointmentsList item in appointmentList)
                {
                    setAdditionalDataStock(item);
                }
            }

            return appointmentList;
        }

        #endregion

        #region get All Planned Appointments Data

        /// <summary>
        /// This function returns all fault appointments by user
        /// </summary>
        /// <param name="operativeID">operative id</param>
        /// <returns>List of Jobs data objects</returns>
        public List<AllAppointmentsList> getAllPlannedAppointments(string userName, string startDate, string endDate)
        {
            //Create a list of strings from Enums as Linq do not support/Recognize ToString() funnction.
            List<string> completedStatusesList = new List<string>() {AppointmentCompleteStatus.Finished.ToString(),
                                        AppointmentCompleteStatus.Complete.ToString(),
                                        AppointmentCompleteStatus.Completed.ToString(),
                                        AppointmentCompleteStatus.NoEntry.ToString()};

            DateTime varStartTime = DateTime.Parse(startDate);
            DateTime varEndTime = DateTime.Parse(endDate);

            var _var = (from jor in context.PLANNED_JOURNAL
                        join app in context.PLANNED_APPOINTMENTS on jor.JOURNALID equals app.JournalId
                        join status in context.PLANNED_STATUS on jor.STATUSID equals status.STATUSID
                        // join pcomp in context.PLANNED_COMPONENT on jor.COMPONENTID equals pcomp.COMPONENTID
                        join pcomp in context.PLANNED_COMPONENT on jor.COMPONENTID equals pcomp.COMPONENTID into compo
                        from componentdetails in compo.DefaultIfEmpty()
                        join pro in context.P__PROPERTY on jor.PROPERTYID equals pro.PROPERTYID
                        //join ten in context.C_TENANCY on app.TENANCYID equals ten.TENANCYID //Changed from inner join to left join.
                        join tenant in context.C_TENANCY on app.TENANCYID equals tenant.TENANCYID into tenancy
                        from ten in tenancy.DefaultIfEmpty()
                        //join comTrade in context.PLANNED_COMPONENT_TRADE on app.COMPTRADEID equals comTrade.COMPTRADEID
                        join comTrade in context.PLANNED_COMPONENT_TRADE on app.COMPTRADEID equals comTrade.COMPTRADEID into compoTrade
                        from compoTradeDetails in compoTrade.DefaultIfEmpty()
                        //join trade in context.G_TRADE on comTrade.TRADEID equals trade.TradeId
                        join trade in context.G_TRADE on compoTradeDetails.TRADEID equals trade.TradeId into Trades
                        from tradeDetails in Trades.DefaultIfEmpty()
                        join creat in context.AC_LOGINS on app.CREATEDBY equals creat.EMPLOYEEID
                        join login in context.AC_LOGINS on app.ASSIGNEDTO equals login.EMPLOYEEID
                        join emp in context.E__EMPLOYEE on login.EMPLOYEEID equals emp.EMPLOYEEID
                        join employee in context.E__EMPLOYEE on creat.EMPLOYEEID equals employee.EMPLOYEEID
                        join noentry in context.PLANNED_NOENTRY on app.APPOINTMENTID equals noentry.APPOINTMENTID into tempNoEntry
                        from noentry1 in tempNoEntry.DefaultIfEmpty()
                        join jorsubstatus in context.PLANNED_SUBSTATUS on app.JOURNALSUBSTATUS equals jorsubstatus.SUBSTATUSID into tempSubStatus
                        from subStatus in tempSubStatus.DefaultIfEmpty()

                        join appointType in context.Planned_Appointment_Type on app.Planned_Appointment_TypeId equals appointType.Planned_Appointment_TypeId into appType
                        from appointmentType in appType.DefaultIfEmpty()
                        orderby app.APPOINTMENTID descending
                        where app.APPOINTMENTDATE >= varStartTime && app.APPOINTMENTDATE <= varEndTime
                                && login.LOGIN == userName
                                && app.APPOINTMENTSTATUS != "Canceled"
                               && (status.TITLE == "Arranged" || status.TITLE == "Condition Arranged" || status.TITLE == "In Progress")
                               && !(completedStatusesList.Contains(app.APPOINTMENTSTATUS.Replace(" ", ""))) // Filter out completed appointments.

                        select new AllAppointmentsList
                        {
                            appointmentId = app.APPOINTMENTID,
                            appointmentNotes = app.CUSTOMERNOTES,
                            appointmentDate = app.APPOINTMENTDATE,
                            appointmentEndDate = app.APPOINTMENTENDDATE,
                            appointmentStatus = app.APPOINTMENTSTATUS,
                            appointmentType = (appointmentType.Planned_Appointment_Type1 == null ? "Planned" : appointmentType.Planned_Appointment_Type1),
                            surveyorUserName = login.LOGIN,
                            tenancyId = ten.TENANCYID,
                            appointmentStartTimeString = app.APPOINTMENTSTARTTIME,
                            appointmentEndTimeString = app.APPOINTMENTENDTIME,
                            createdBy = app.CREATEDBY,
                            journalHistoryId = app.JOURNALHISTORYID,
                            journalId = app.JournalId,
                            surveyType = app.SURVEYTYPE,
                            surveyourAvailability = app.SURVEYOURSTATUS,
                            journalSubStatus = status.TITLE,
                            creationDate = app.LOGGEDDATE,
                            loggedDate = app.LOGGEDDATE,
                            isMiscAppointment = app.isMiscAppointment,
                            createdByPerson = employee.FIRSTNAME + " " + employee.LASTNAME,
                            property = new PropertyData
                            {
                                propertyId = pro.PROPERTYID,
                                tenancyId = ten.TENANCYID,
                                houseNumber = pro.HOUSENUMBER,
                                flatNumber = pro.FLATNUMBER,
                                address1 = pro.ADDRESS1,
                                address2 = pro.ADDRESS2,
                                address3 = pro.ADDRESS3,
                                townCity = pro.TOWNCITY,
                                postCode = pro.POSTCODE,
                                county = pro.COUNTY,
                                defaultPropertyPicId = pro.PropertyPicId
                            },

                            componentTrade = new PlannedComponentTrade
                            {
                                componentTradeId = compoTradeDetails.COMPTRADEID == null ? -1 : compoTradeDetails.COMPTRADEID,
                                componentId = compoTradeDetails.COMPONENTID,
                                componentName = componentdetails.COMPONENTNAME,
                                trade = tradeDetails.Description,
                                duration = app.DURATION,
                                tradeId = compoTradeDetails.TRADEID,
                                sorder = compoTradeDetails.sorder == null ? 0 : compoTradeDetails.sorder,
                                completionDate = app.APPOINTMENTENDDATE,
                                JSNNotes = app.APPOINTMENTNOTES,
                                reportedDate = app.LOGGEDDATE,
                                jobStatus = subStatus.TITLE
                            }
                        });

            List<AllAppointmentsList> faultAppointments = new List<AllAppointmentsList>();
            if (_var.Count() > 0)
            {
                faultAppointments = _var.ToList();
                foreach (AllAppointmentsList item in faultAppointments)
                {
                    DateTime dt = (DateTime)item.appointmentDate;
                    item.appointmentStartDateTime = DateTime.Parse(dt.ToString("dd/MM/yyyy") + " " + item.appointmentStartTimeString);
                    DateTime enddt = (DateTime)item.appointmentEndDate;
                    item.appointmentEndDateTime = DateTime.Parse(enddt.ToString("dd/MM/yyyy") + " " + item.appointmentEndTimeString);
                    item.componentTrade.PMO = "PMO" + item.journalId.ToString();
                    item.componentTrade.JSNumber = "JSN" + item.appointmentId;
                    setAdditionalDataPlanned(item);
                    if (item.isMiscAppointment == true || item.appointmentType == PropSurvey.Contracts.Data.Appointment.AppointmentTypes.Condition.ToString())
                    {
                        if (item.appointmentType == "Misc")
                        {
                            item.appointmentType = "Miscellaneous";
                            item.componentTrade.PMO = "MWO" + item.journalId.ToString();
                        }
                        if (item.appointmentType == PropSurvey.Contracts.Data.Appointment.AppointmentTypes.Condition.ToString())
                        {
                            var itemdetail = from cw in context.PLANNED_CONDITIONWORKS.Where(cw => cw.JournalId == item.journalId)
                                             join att in context.PA_PROPERTY_ATTRIBUTES on cw.AttributeId equals att.ATTRIBUTEID
                                             join ip in context.PA_ITEM_PARAMETER on att.ITEMPARAMID equals ip.ItemParamID
                                             join i in context.PA_ITEM on ip.ItemId equals i.ItemID
                                             select i;

                            //Set to default values
                            item.componentTrade.componentId = -1;
                            item.componentTrade.componentName = "N/A";

                            if (itemdetail.Count() > 0)
                            {
                                item.componentTrade.componentId = -1;
                                item.componentTrade.componentName = itemdetail.First().ItemName;
                            }
                        }
                        PlannedComponentMiscTrade miscTrade = this.getMiscAppointmentTrade(item.appointmentId);
                        item.componentTrade.tradeId = miscTrade.tradeId;
                        item.componentTrade.trade = miscTrade.trade;
                        item.componentTrade.duration = miscTrade.duration;
                        item.componentTrade.location = miscTrade.location;
                        item.componentTrade.adaptation = miscTrade.adaptation;
                        item.componentTrade.adaptationId = miscTrade.adaptationId;
                        item.componentTrade.locationId = miscTrade.locationId;
                    }
                    item.componentTrade.durationUnit = item.componentTrade.duration.ToString() + " Hour(s)";

                    //if appointment status=NotStarted and journal status =Arranged 
                    if ((item.journalSubStatus == "Arranged" || item.journalSubStatus == "Condition Arranged") && item.appointmentStatus == "NotStarted")
                    {
                        item.appointmentStatus = "NotStarted";
                        item.componentTrade.jobStatus = "NotStarted";
                    }
                    //PLANNED_APPOINTMENTS.JournalSubstatus in ('In Progress')
                    //PLANNED_APPOINTMENTS.AppointmentStatus = ‘InProgress’
                    else if (item.appointmentStatus.Replace(" ", "") == AppointmentCompleteStatus.InProgress.ToString() && item.componentTrade.jobStatus.Replace(" ", "") == AppointmentCompleteStatus.InProgress.ToString())
                    {
                        item.appointmentStatus = AppointmentCompleteStatus.InProgress.ToString();
                        int jobStatus = isPlannedJobStarted(item.appointmentId);
                        if (jobStatus == 0)
                            item.componentTrade.jobStatus = "NotStarted";
                        else
                            item.componentTrade.jobStatus = AppointmentCompleteStatus.InProgress.ToString();
                    }
                    //PLANNED_APPOINTMENTS.JournalSubstatus in (‘In Progress’,‘Paused’)
                    //PLANNED_APPOINTMENTS.AppointmentStatus = ‘InProgress’
                    else if ((item.componentTrade.jobStatus == "In Progress".ToString() || item.componentTrade.jobStatus == AppointmentCompleteStatus.Paused.ToString()) && item.appointmentStatus == AppointmentCompleteStatus.InProgress.ToString())
                    {
                        item.appointmentStatus = AppointmentCompleteStatus.InProgress.ToString();
                        item.componentTrade.jobStatus = item.componentTrade.jobStatus.Replace(" ", "");
                    }
                    else if ((item.componentTrade.jobStatus == AppointmentCompleteStatus.Completed.ToString() || item.componentTrade.jobStatus == "No Entry") && item.appointmentStatus == AppointmentCompleteStatus.Complete.ToString())
                    {
                        item.appointmentStatus = AppointmentCompleteStatus.Complete.ToString();
                        item.componentTrade.jobStatus = AppointmentCompleteStatus.Complete.ToString();
                    }
                }
            }

            return faultAppointments;
        }

        #endregion

        #region Get Misc Appointment Trade by appointmentId

        /// <summary>
        /// Get Misc Appointment Trade by appointmentId
        /// </summary>
        /// <param name="appointmentId"></param>
        /// <returns></returns>
        public PlannedComponentMiscTrade getMiscAppointmentTrade(int appointmentId)
        {
            var _var = (from jor in context.PLANNED_JOURNAL
                        join app in context.PLANNED_APPOINTMENTS on jor.JOURNALID equals app.JournalId
                        join trade in context.PLANNED_MISC_TRADE on app.APPOINTMENTID equals trade.AppointmentId
                        join tradeDetails in context.G_TRADE on trade.TradeId equals tradeDetails.TradeId
                        where trade.AppointmentId == appointmentId
                        select new PlannedComponentMiscTrade
                        {
                            tradeId = trade.TradeId,
                            trade = tradeDetails.Description,
                            duration = trade.Duration,
                            locationId = trade.ParameterId,
                            adaptationId = trade.ParameterValueId
                        });
            PlannedComponentMiscTrade miscTrade = new PlannedComponentMiscTrade();

            if (_var.Count() > 0)
            {
                miscTrade = _var.First();
                if (miscTrade.locationId != null && miscTrade.locationId > 0)
                {
                    List<PLANNED_GetLocations_Result> locationList = new List<PLANNED_GetLocations_Result>();
                    locationList = this.GetLocation();
                    DataTable locationDt = new DataTable();
                    DataTable filteredLocationDt = new DataTable();
                    locationDt = FileHelper.ConvertToDataTable(locationList);

                    DataView locationDv = new DataView();
                    locationDv = locationDt.AsDataView();
                    locationDv.RowFilter = "ItemID =  " + miscTrade.locationId.ToString();
                    filteredLocationDt = locationDv.ToTable();
                    miscTrade.location = filteredLocationDt.Rows[0]["ItemName"].ToString();
                    if (miscTrade.adaptationId > 0)
                    {
                        var paramValue = (from parv in context.PA_PARAMETER_VALUE
                                          where parv.ValueID == miscTrade.adaptationId
                                          select parv.ValueDetail);
                        if (paramValue.Count() > 0)
                        {
                            miscTrade.adaptation = paramValue.Single().ToString();
                        }
                    }
                }
            }
            return miscTrade;
        }

        #endregion

        #region Get Planned Locations

        public List<PLANNED_GetLocations_Result> GetLocation()
        {
            try
            {
                List<PLANNED_GetLocations_Result> list = new List<PLANNED_GetLocations_Result>();
                list = context.PLANNED_GetLocations().ToList();
                return list;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region get Stock Appointment by Id
        /// <summary>
        /// This function returns Stock appointments 
        /// </summary>
        /// <returns>List of appointment data objects</returns>
        public List<AllAppointmentsList> getStockAppointmentById(int appointmentId, int surveyId)
        {

            var appointments = (from app in context.PS_Appointment
                                join p2a in context.PS_Property2Appointment on app.AppointId equals p2a.AppointId
                                join ten in context.C_TENANCY on p2a.TenancyId equals ten.TENANCYID into tenancy
                                from tenan in tenancy.DefaultIfEmpty()
                                join pro in context.P__PROPERTY on p2a.PropertyId equals pro.PROPERTYID
                                join login in context.AC_LOGINS on app.CreatedBy equals login.EMPLOYEEID
                                join emp in context.E__EMPLOYEE on login.EMPLOYEEID equals emp.EMPLOYEEID
                                orderby app.AppointId
                                where

                                app.AppointId == appointmentId

                                select new AllAppointmentsList
                                {
                                    appointmentId = app.AppointId,
                                    surveyourAvailability = app.SurveyourStatus,
                                    appointmentLocation = app.AppointLocation,
                                    appointmentStartDateTime = app.AppointStartDateTime,
                                    appointmentEndDateTime = app.AppointEndDateTime,
                                    appointmentNotes = app.AppointNotes,
                                    appointmentValidity = app.AppointValidity,
                                    appointmentType = app.AppointType,
                                    appointmentStatus = app.AppointProgStatus,
                                    surveyorUserName = app.SurveyourUserName,
                                    surveyorAlert = app.AppointmentAlert,
                                    surveyType = app.SurveyType,
                                    appointmentCalendar = app.AppointmentCalendar,
                                    tenancyId = tenan.TENANCYID,
                                    appointmentOverdue = 0,
                                    appointmentDate = app.AppointStartDateTime,
                                    addToCalendar = app.addToCalendar,
                                    createdBy = app.CreatedBy,
                                    creationDate = app.CreatedDate,
                                    loggedDate = app.CreatedDate,
                                    createdByPerson = emp.FIRSTNAME + " " + emp.LASTNAME,
                                    //Get Property Data
                                    property = new PropertyData
                                    {
                                        propertyId = p2a.PropertyId,
                                        tenancyId = p2a.TenancyId,
                                        houseNumber = pro.HOUSENUMBER,
                                        flatNumber = pro.FLATNUMBER,
                                        address1 = pro.ADDRESS1,
                                        address2 = pro.ADDRESS2,
                                        address3 = pro.ADDRESS3,
                                        townCity = pro.TOWNCITY,
                                        postCode = pro.POSTCODE,
                                        county = pro.COUNTY,
                                        lastSurveyDate = p2a.LastSurveyDate,
                                        defaultPropertyPicId = pro.PropertyPicId
                                    }

                                });

            List<AllAppointmentsList> appointmentList = new List<AllAppointmentsList>();

            if (appointments.Count() > 0)
            {
                appointmentList = appointments.ToList();
                foreach (AllAppointmentsList item in appointmentList)
                {
                    setAdditionalDataStock(item, surveyId);
                }
            }

            return appointmentList;
        }

        #endregion

        #region get Stock Appointments between particular dates

        /// <summary>
        /// This function returns Stock appointments for 1 week (5 days) between particular dates
        /// </summary>
        /// <returns>List of appointment data objects</returns>
        public List<AllAppointmentsList> getStockAppointmentsByDates(string startDate, string endDate, string username, List<int> existingAppointmentIds)
        {
            //Create a list of strings from Enums as Linq do not support/Recognize ToString() funnction.
            List<string> completedStatusesList = new List<string>() {AppointmentCompleteStatus.Finished.ToString(),
                                        AppointmentCompleteStatus.Complete.ToString(),
                                        AppointmentCompleteStatus.Completed.ToString(),
                                        AppointmentCompleteStatus.NoEntry.ToString()};

            // Converting Dates received from iOS 
            DateTime varStartTime = DateTime.Parse(startDate);
            DateTime varEndTime = DateTime.Parse(endDate);
            //Getting Stock Appointment Information
            var appointments = (from app in context.PS_Appointment
                                join p2a in context.PS_Property2Appointment on app.AppointId equals p2a.AppointId
                                join ten in context.C_TENANCY on p2a.TenancyId equals ten.TENANCYID into tenancy
                                from tenan in tenancy.DefaultIfEmpty()
                                join pro in context.P__PROPERTY on p2a.PropertyId equals pro.PROPERTYID
                                join login in context.AC_LOGINS on app.CreatedBy equals login.EMPLOYEEID
                                join emp in context.E__EMPLOYEE on login.EMPLOYEEID equals emp.EMPLOYEEID
                                orderby app.AppointId
                                where app.SurveyourUserName == username
                                && app.AppointStartDateTime >= varStartTime
                                && app.AppointEndDateTime <= varEndTime
                                && !(completedStatusesList.Contains(app.AppointProgStatus))
                                && !(existingAppointmentIds.Contains(app.AppointId))

                                select new AllAppointmentsList
                                {
                                    appointmentId = app.AppointId,
                                    surveyourAvailability = app.SurveyourStatus,
                                    appointmentLocation = app.AppointLocation,
                                    appointmentStartDateTime = app.AppointStartDateTime,
                                    appointmentEndDateTime = app.AppointEndDateTime,
                                    appointmentNotes = app.AppointNotes,
                                    appointmentValidity = app.AppointValidity,
                                    appointmentType = app.AppointType,
                                    appointmentStatus = app.AppointProgStatus,
                                    surveyorUserName = app.SurveyourUserName,
                                    surveyorAlert = app.AppointmentAlert,
                                    surveyType = app.SurveyType,
                                    appointmentCalendar = app.AppointmentCalendar,
                                    tenancyId = tenan.TENANCYID,
                                    appointmentOverdue = 0,
                                    appointmentDate = app.AppointStartDateTime,
                                    createdBy = app.CreatedBy,
                                    addToCalendar = app.addToCalendar,
                                    creationDate = app.CreatedDate,
                                    loggedDate = app.CreatedDate,
                                    createdByPerson = emp.FIRSTNAME + " " + emp.LASTNAME,
                                    //Getting Property Data
                                    property = new PropertyData
                                    {
                                        propertyId = p2a.PropertyId,
                                        tenancyId = p2a.TenancyId,
                                        houseNumber = pro.HOUSENUMBER,

                                        flatNumber = pro.FLATNUMBER,
                                        address1 = pro.ADDRESS1,
                                        address2 = pro.ADDRESS2,
                                        address3 = pro.ADDRESS3,
                                        defaultPropertyPicId = pro.PropertyPicId,
                                        townCity = pro.TOWNCITY,
                                        postCode = pro.POSTCODE,
                                        county = pro.COUNTY,
                                        lastSurveyDate = p2a.LastSurveyDate
                                    }

                                });

            List<AllAppointmentsList> appointmentList = new List<AllAppointmentsList>();
            if (appointments.Count() > 0)
            {
                appointmentList = appointments.ToList();
                foreach (AllAppointmentsList item in appointmentList)
                {
                    setAdditionalDataStock(item);
                }
            }

            return appointmentList;
        }

        #endregion

        #region "Get Power Source Types"

        public List<PowerType> getPowerSourceTypes()
        {
            List<PowerType> powerTypes = new List<PowerType>();

            var powerTypesQuery = from pst in context.P_PowerSourceType
                                  orderby pst.sortOrder ascending
                                  select new PowerType
                                  {
                                      powerTypeId = pst.PowerTypeId,
                                      powerType = pst.PowerType
                                  };

            if (powerTypesQuery.Count() > 0) powerTypes = powerTypesQuery.ToList();

            return powerTypes;
        }
        #endregion

        #region Get Detector Types

        public List<DetectorType> getDetectorTypes()
        {
            List<DetectorType> detectorTypes = new List<DetectorType>();

            var types = from dt in context.AS_DetectorType
                        select new DetectorType
                        {
                            detectorTypeId = dt.DetectorTypeId,
                            detectorType = dt.DetectorType
                        };
            if (types.Count() > 0) detectorTypes = types.ToList();


            return detectorTypes;
        }


        #endregion

        #region get All Stock Overdue Appointments

        /// <summary>
        /// This function will return all over due appointments from particular start date
        /// </summary>
        /// <returns>List of appointment data objects</returns>
        public List<AllAppointmentsList> getAllOverdueStockAppointments(string startDate, string userName, List<int> existingAppointmentIds)
        {
            //Create a list of strings from Enums as Linq do not support/Recognize ToString() funnction.
            List<string> completedStatusesList = new List<string>() {AppointmentCompleteStatus.Finished.ToString(),
                                        AppointmentCompleteStatus.Complete.ToString(),
                                        AppointmentCompleteStatus.Completed.ToString(),
                                        AppointmentCompleteStatus.NoEntry.ToString()};

            DateTime varStartTime = DateTime.Parse(startDate);
            var appointments = (from app in context.PS_Appointment
                                join p2a in context.PS_Property2Appointment on app.AppointId equals p2a.AppointId
                                join ten in context.C_TENANCY on p2a.TenancyId equals ten.TENANCYID into tenancy
                                from tenan in tenancy.DefaultIfEmpty()
                                join pro in context.P__PROPERTY on p2a.PropertyId equals pro.PROPERTYID
                                join login in context.AC_LOGINS on app.CreatedBy equals login.EMPLOYEEID
                                join emp in context.E__EMPLOYEE on login.EMPLOYEEID equals emp.EMPLOYEEID
                                orderby app.AppointId
                                where app.SurveyourUserName == userName
                                && app.AppointEndDateTime <= varStartTime
                                && !(completedStatusesList.Contains(app.AppointProgStatus))
                                && !(existingAppointmentIds.Contains(app.AppointId))
                                select new AllAppointmentsList
                                {
                                    appointmentId = app.AppointId,
                                    surveyourAvailability = app.SurveyourStatus,
                                    appointmentLocation = app.AppointLocation,

                                    appointmentStartDateTime = app.AppointStartDateTime,
                                    appointmentEndDateTime = app.AppointEndDateTime,
                                    appointmentNotes = app.AppointNotes,
                                    createdByPerson = emp.FIRSTNAME + " " + emp.LASTNAME,
                                    appointmentValidity = app.AppointValidity,
                                    appointmentType = app.AppointType,
                                    appointmentStatus = app.AppointProgStatus,
                                    surveyorUserName = app.SurveyourUserName,
                                    surveyorAlert = app.AppointmentAlert,
                                    surveyType = app.SurveyType,
                                    appointmentCalendar = app.AppointmentCalendar,
                                    tenancyId = tenan.TENANCYID,
                                    appointmentOverdue = 1,
                                    appointmentDate = app.AppointStartDateTime,
                                    creationDate = app.CreatedDate,
                                    loggedDate = app.CreatedDate,
                                    //Get Property Data
                                    property = new PropertyData
                                    {
                                        propertyId = p2a.PropertyId,
                                        tenancyId = p2a.TenancyId,
                                        houseNumber = pro.HOUSENUMBER,

                                        flatNumber = pro.FLATNUMBER,
                                        address1 = pro.ADDRESS1,
                                        address2 = pro.ADDRESS2,
                                        address3 = pro.ADDRESS3,
                                        defaultPropertyPicId = pro.PropertyPicId,
                                        townCity = pro.TOWNCITY,
                                        postCode = pro.POSTCODE,
                                        county = pro.COUNTY,
                                        lastSurveyDate = p2a.LastSurveyDate
                                    }

                                });

            List<AllAppointmentsList> appointmentList = new List<AllAppointmentsList>();

            if (appointments.Count() > 0)
            {
                appointmentList = appointments.ToList();
                foreach (AllAppointmentsList item in appointmentList)
                {
                    setAdditionalDataStock(item);
                }
            }

            return appointmentList;
        }

        #endregion

        #region get Stock Overdue Appointments between particular dates

        /// <summary>
        /// This function will return 4 over due appointments from particular start date
        /// </summary>
        /// <returns>List of appointment data objects</returns>
        public List<AllAppointmentsList> getOverdueStockAppointmentsByDates(string startDate, string userName, List<int> existingAppointmentIds)
        {
            //Create a list of strings from Enums as Linq do not support/Recognize ToString() funnction.
            List<string> completedStatusesList = new List<string>() {AppointmentCompleteStatus.Finished.ToString(),
                                        AppointmentCompleteStatus.Complete.ToString(),
                                        AppointmentCompleteStatus.Completed.ToString(),
                                        AppointmentCompleteStatus.NoEntry.ToString()};

            DateTime varStartTime = DateTime.Parse(startDate);
            var appointments = (from app in context.PS_Appointment
                                join p2a in context.PS_Property2Appointment on app.AppointId equals p2a.AppointId
                                join ten in context.C_TENANCY on p2a.TenancyId equals ten.TENANCYID into tenancy
                                from tenan in tenancy.DefaultIfEmpty()
                                join pro in context.P__PROPERTY on p2a.PropertyId equals pro.PROPERTYID
                                join login in context.AC_LOGINS on app.CreatedBy equals login.EMPLOYEEID
                                join emp in context.E__EMPLOYEE on login.EMPLOYEEID equals emp.EMPLOYEEID
                                orderby app.AppointId
                                where app.SurveyourUserName == userName
                                && app.AppointEndDateTime <= varStartTime
                                && !(completedStatusesList.Contains(app.AppointProgStatus))
                                && !(existingAppointmentIds.Contains(app.AppointId))

                                select new AllAppointmentsList
                                {
                                    appointmentId = app.AppointId,
                                    surveyourAvailability = app.SurveyourStatus,
                                    appointmentLocation = app.AppointLocation,

                                    appointmentStartDateTime = app.AppointStartDateTime,
                                    appointmentEndDateTime = app.AppointEndDateTime,
                                    appointmentNotes = app.AppointNotes,

                                    appointmentValidity = app.AppointValidity,
                                    appointmentType = app.AppointType,
                                    appointmentStatus = app.AppointProgStatus,
                                    surveyorUserName = app.SurveyourUserName,
                                    surveyorAlert = app.AppointmentAlert,
                                    surveyType = app.SurveyType,
                                    appointmentCalendar = app.AppointmentCalendar,
                                    tenancyId = tenan.TENANCYID,
                                    appointmentOverdue = 1,
                                    appointmentDate = app.AppointStartDateTime,
                                    creationDate = app.CreatedDate,
                                    loggedDate = app.CreatedDate,
                                    createdByPerson = emp.FIRSTNAME + " " + emp.LASTNAME,
                                    //Get Property Data
                                    property = new PropertyData
                                    {
                                        propertyId = p2a.PropertyId,
                                        tenancyId = p2a.TenancyId,
                                        houseNumber = pro.HOUSENUMBER,

                                        flatNumber = pro.FLATNUMBER,
                                        address1 = pro.ADDRESS1,
                                        address2 = pro.ADDRESS2,
                                        address3 = pro.ADDRESS3,
                                        defaultPropertyPicId = pro.PropertyPicId,
                                        townCity = pro.TOWNCITY,
                                        postCode = pro.POSTCODE,
                                        county = pro.COUNTY,
                                        lastSurveyDate = p2a.LastSurveyDate
                                    }

                                }).Take(4);

            List<AllAppointmentsList> appointmentList = new List<AllAppointmentsList>();

            if (appointments.Count() > 0)
            {
                appointmentList = appointments.ToList();
                foreach (AllAppointmentsList item in appointmentList)
                {
                    setAdditionalDataStock(item);

                }

            }
            return appointmentList;
        }

        #endregion

        #region Get all gas appointments

        /// <summary>
        /// This function returns all the appointments
        /// </summary>
        /// <returns>List of appointment data objects</returns>
        public List<AllAppointmentsList> getAllAppointmentsGas(string username, string startDate, string endDate)
        {
            //Create a list of strings from Enums as Linq do not support/Recognize ToString() funnction.
            List<string> completedStatusesList = new List<string>() {AppointmentCompleteStatus.Finished.ToString(),
                                        AppointmentCompleteStatus.Complete.ToString(),
                                        AppointmentCompleteStatus.Completed.ToString(),
                                        AppointmentCompleteStatus.NoEntry.ToString()};


            DateTime varStartTime = DateTime.Parse(startDate);
            DateTime varEndTime = DateTime.Parse(endDate);
            var appointments = (from app in context.AS_APPOINTMENTS
                                join jor in context.AS_JOURNAL on app.JournalId equals jor.JOURNALID
                                join pro in context.P__PROPERTY on jor.PROPERTYID equals pro.PROPERTYID
                                join ten in context.C_TENANCY
                                        on new { TENANCYID = (Int32)app.TENANCYID, ENDDATE = (DateTime?)null }
                                        equals new { TENANCYID = ten.TENANCYID, ENDDATE = (DateTime?)ten.ENDDATE }
                                        into tenancy
                                from tenan in tenancy.DefaultIfEmpty()
                                join status in context.AS_Status on jor.STATUSID equals status.StatusId
                                join login in context.AC_LOGINS on app.ASSIGNEDTO equals login.EMPLOYEEID
                                join emp in context.E__EMPLOYEE on login.EMPLOYEEID equals emp.EMPLOYEEID
                                join creat in context.AC_LOGINS on app.CREATEDBY equals creat.EMPLOYEEID
                                join employee in context.E__EMPLOYEE on creat.EMPLOYEEID equals employee.EMPLOYEEID
                                join lgsr in context.P_LGSR on pro.PROPERTYID equals lgsr.PROPERTYID into templgsr
                                from lgsr in templgsr.DefaultIfEmpty()
                                orderby app.APPOINTMENTID
                                where login.LOGIN == username
                                && app.APPOINTMENTDATE >= varStartTime && app.APPOINTMENTDATE <= varEndTime
                                && !(completedStatusesList.Contains(app.APPOINTMENTSTATUS.Replace(" ", ""))) // Filter out completed appointments.

                                select new AllAppointmentsList
                                {

                                    appointmentId = app.APPOINTMENTID,
                                    jsgNumber = app.JSGNUMBER,
                                    tenancyId = tenan.TENANCYID,
                                    journalId = app.JournalId,
                                    journalHistoryId = app.JOURNALHISTORYID,
                                    appointmentDate = app.APPOINTMENTDATE,
                                    appointmentStartTimeString = app.APPOINTMENTSTARTTIME,
                                    appointmentEndTimeString = app.APPOINTMENTENDTIME,
                                    addToCalendar = app.addToCalendar,
                                    surveyorUserName = login.LOGIN,
                                    creationDate = app.LOGGEDDATE,
                                    loggedDate = app.LOGGEDDATE,
                                    createdBy = app.CREATEDBY,
                                    appointmentNotes = app.NOTES,
                                    appointmentStatus = (app.APPOINTMENTSTATUS == null ? "NotStarted" : app.APPOINTMENTSTATUS),
                                    appointmentCalendar = (app.APPOINTMENTCALENDER == null ? "default" : app.APPOINTMENTCALENDER),
                                    surveyorAlert = (app.APPOINTMENTALERT == null ? "15 minutes before" : app.APPOINTMENTALERT),
                                    surveyourAvailability = (app.SURVEYOURSTATUS == null ? "Free" : app.SURVEYOURSTATUS),
                                    appointmentType = "Gas",
                                    surveyType = app.SURVEYTYPE,
                                    createdByPerson = employee.FIRSTNAME + " " + employee.LASTNAME,
                                    journal = new JournalData
                                    {
                                        actionId = jor.ACTIONID,
                                        creationBy = jor.CREATEDBY,
                                        creationDate = jor.CREATIONDATE,
                                        inspectionTypeId = jor.INSPECTIONTYPEID,
                                        isCurrent = jor.ISCURRENT,
                                        journalId = jor.JOURNALID,
                                        propertyId = jor.PROPERTYID,
                                        statusId = jor.STATUSID
                                    },

                                    property = new PropertyData
                                    {
                                        propertyId = jor.PROPERTYID,
                                        tenancyId = tenan.TENANCYID,
                                        houseNumber = pro.HOUSENUMBER,

                                        flatNumber = pro.FLATNUMBER,
                                        address1 = pro.ADDRESS1,
                                        address2 = pro.ADDRESS2,
                                        address3 = pro.ADDRESS3,
                                        defaultPropertyPicId = pro.PropertyPicId,
                                        townCity = pro.TOWNCITY,
                                        postCode = pro.POSTCODE,
                                        county = pro.COUNTY,
                                        certificateExpiry = EntityFunctions.AddYears(lgsr.ISSUEDATE, 1)
                                    }

                                });

            List<AllAppointmentsList> appointmentList = new List<AllAppointmentsList>();

            if (appointments.Count() > 0)
            {
                appointmentList = appointments.ToList();
                foreach (AllAppointmentsList item in appointmentList)
                {
                    DateTime dt = (DateTime)item.appointmentDate;
                    item.appointmentStartDateTime = DateTime.Parse(dt.ToString("dd/MM/yyyy") + " " + item.appointmentStartTimeString);
                    //FileHelper.calculateEndDate(appointmentStartDateTime,item.)
                    item.appointmentEndDateTime = DateTime.Parse(dt.ToString("dd/MM/yyyy") + " " + item.appointmentEndTimeString);

                    //Set additional Gas data that is not possible in one linq query.
                    setAdditionalDataGas(item);
                    InstallationPipework.InstallationPipeworkDal insPipWorkDal = new InstallationPipework.InstallationPipeworkDal();
                    item.property.installationpipework = insPipWorkDal.getInstallationPipeworkFormDataGas((int)item.journal.journalId);
                    //In case cp12info is present, then set jsgNumber in it from app.jsgNumber
                    if (item.CP12Info != null)
                        item.CP12Info.jsgNumber = item.jsgNumber;
                }
            }

            return appointmentList;
        }

        #endregion

        #region Update Fault Appointment Progress Status
        /// <summary>
        /// This function update the status of fault appointment. This function use post method to accept data. 
        /// </summary>
        /// <param name="apptData">This fucntion accepts the AppointmentData's object </param>
        /// <param name="isAppointFinished">this flag represents the finished appointment flag </param>
        /// <param name="lastSurveyDate">The date of last survey which 'll be filled if status is finished</param>
        /// <returns>It returns true or false in update is successful</returns>
        public ResultBoolData updateFaultAppointmentProgressStatus(int appointmentID, string progressStatus, bool isAppointFinished)
        {
            ResultBoolData resultBoolData = new ResultBoolData();
            bool success = false;

            //if (progressStatus.Trim().Replace(" ", "") != FaultAppointmentProgressStatus.InProgress.ToString())
            //{
            //    resultBoolData.result = success;
            //    return resultBoolData;
            //}

            //using (TransactionScope trans = new TransactionScope())
            //{
            var appointment = context.FL_CO_APPOINTMENT.Where(app => app.AppointmentID == appointmentID).First();
            appointment.AppointmentStatus = progressStatus;

            success = true;

            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            //    trans.Complete();
            //}
            if (success == false)
            {
                resultBoolData.result = success;
                return resultBoolData;
            }

            if (isAppointFinished == true)
            {
                this.saveLastSurveyDateFault(appointmentID);
            }

            resultBoolData.result = success;
            return resultBoolData;
        }
        #endregion

        #region Update Fault Status

        /// <summary>
        /// This function update the status of all faults of an fault appointment. This function use post method to accept data. 
        /// </summary>
        /// <param name="apptData">This function accepts the AppointmentData's object </param>      
        /// <returns>It returns true or false in update is successful</returns>
        public Nullable<int> updateFaultStatusId(PropertySurvey_Entities context, int faultLogID, string progressStatus, int? operativeID, DateTime? submittedDate, DateTime? completedDate = null)
        {
            Nullable<int> faultLogHistoryId = null;

            FL_FAULT_LOG faultLog = context.FL_FAULT_LOG.Where(fltl => fltl.FaultLogID == faultLogID).First();
            var fltStatusList = context.FL_FAULT_STATUS.Where(fltSts => fltSts.Description.Replace(" ", "") == progressStatus);

            if (fltStatusList.Count() == 0)
                return faultLogHistoryId;

            FL_FAULT_STATUS fltStatus = fltStatusList.First();
            faultLog.StatusID = fltStatus.FaultStatusID;

            if (completedDate != null && progressStatus == FaultJobProgressStatus.Complete.ToString())
                faultLog.CompletedDate = completedDate;

            // insert record in fault log history
            faultLogHistoryId = saveFaultLogHistoryData(context, faultLog, operativeID, submittedDate);

            return faultLogHistoryId;
        }

        #endregion

        #region is Property N Customer Exists
        /// <summary>
        /// This function checks the property id against the customer, if that exists then it returns true otherwise false
        /// </summary>
        /// <param name="propertyId">property id </param>
        /// <param name="customerId">customer id</param>
        /// <returns>returns true or false</returns>

        public bool isPropertyNCustomerExists(string propertyId, int customerId)
        {
            bool success = false;
            var custRecord = (from cte in context.C_TENANCY
                              join prop in context.P__PROPERTY on cte.PROPERTYID equals prop.PROPERTYID
                              join cusp in context.C_CUSTOMERTENANCY on cte.TENANCYID equals cusp.TENANCYID
                              join cust in context.C__CUSTOMER on cusp.CUSTOMERID equals cust.CUSTOMERID
                              where cte.PROPERTYID.ToLower() == propertyId.ToLower() && cust.CUSTOMERID == customerId
                              select cte);

            if (custRecord.Count() > 0)
            {
                success = true;
            }
            else
            {
                success = false;
            }

            return success;
        }
        #endregion

        #region save Faults Log History Data

        /// <summary>
        /// This function saves the Faults Log History Data in the database
        /// </summary>
        /// <param name="appData">The object of Faults Log History Data</param>
        /// <returns>the Faults id if successfully save otherwise 0 </returns>
        public Nullable<int> saveFaultLogHistoryData(PropertySurvey_Entities context, FL_FAULT_LOG faultLog, int? operativeID, DateTime? submittedDate)
        {
            Nullable<int> faultLogHistoryId = null;
            FL_FAULT_LOG_HISTORY faultLogHistoryData = new FL_FAULT_LOG_HISTORY();

            var faultJournalList = context.FL_FAULT_JOURNAL.Where(jor => jor.FaultLogID == faultLog.FaultLogID);
            if (faultJournalList.Count() > 0)
            {
                FL_FAULT_JOURNAL faultJournal = faultJournalList.First();
                faultLogHistoryData.JournalID = faultJournal.JournalID;
                faultJournal.FaultStatusID = faultLog.StatusID;
            }
            faultLogHistoryData.FaultStatusID = faultLog.StatusID;
            // faultLogHistoryData.itemActionID;
            faultLogHistoryData.LastActionDate = submittedDate ?? DateTime.Now;
            faultLogHistoryData.LastActionUserID = operativeID;
            faultLogHistoryData.FaultLogID = faultLog.FaultLogID;
            faultLogHistoryData.ORGID = faultLog.ORGID;
            //faultLogHistoryData.scopeID;
            //faultLogHistoryData.title;
            faultLogHistoryData.Notes = faultLog.Notes;
            faultLogHistoryData.PROPERTYID = faultLog.PROPERTYID;
            faultLogHistoryData.ContractorID = faultLog.ContractorID;
            faultLogHistoryData.SchemeID = faultLog.SchemeId;
            faultLogHistoryData.BlockID = faultLog.BlockId;

            context.AddToFL_FAULT_LOG_HISTORY(faultLogHistoryData);
            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            faultLogHistoryId = faultLogHistoryData.FaultLogHistoryID;

            return faultLogHistoryId;
        }

        #endregion

        #region get misc appointment data
        public bool isFaultCancelled(int faultLogID)
        {
            int cancelledStatusID = GetJobStatusID("Cancelled");

            FL_FAULT_LOG faultLog = context.FL_FAULT_LOG.Where(fltl => fltl.FaultLogID == faultLogID).First();
            if (faultLog.StatusID == cancelledStatusID)
                return true;
            else
                return false;
        }

        public List<FL_FAULT_APPOINTMENT> getFaultAppointmentList(int appointmentID)
        {
            var fltApptList = (from fltAppt in context.FL_FAULT_APPOINTMENT
                               orderby fltAppt.FaultAppointmentId
                               where fltAppt.AppointmentId == appointmentID
                               select fltAppt);
            return fltApptList.ToList();
        }

        public List<FL_FAULT_LOG> getFaultLogList(int appointmentID)
        {
            var flLogList = (from fltl in context.FL_FAULT_LOG
                             join fltAppt in context.FL_FAULT_APPOINTMENT on fltl.FaultLogID equals fltAppt.FaultLogId
                             orderby fltl.FaultLogID
                             where fltAppt.AppointmentId == appointmentID
                             select fltl);
            return flLogList.ToList();
        }

        #endregion

        #region get And Set Appointment Progress Status


        /// <summary>
        /// This function get fault appointment progress status
        /// </summary>
        /// <param name="appointmentId">appointment id</param>
        /// <param name="userName">user name </param>
        /// <returns>return the fault appointment progress status</returns>

        public string getFaultJobStatus(int appointmentId)
        {
            string status = "";
            var fltList = (from fltl in context.FL_FAULT_LOG
                           join fltAppt in context.FL_FAULT_APPOINTMENT on fltl.FaultLogID equals fltAppt.FaultLogId
                           join fltStatus in context.FL_FAULT_STATUS on fltl.StatusID equals fltStatus.FaultStatusID
                           orderby fltl.FaultLogID
                           where fltAppt.AppointmentId == appointmentId
                           && fltAppt.FaultLogId == (context.FL_FAULT_APPOINTMENT.Where(ap => ap.AppointmentId == appointmentId).Min(ap => ap.FaultLogId))
                           select fltStatus.Description);

            if (fltList.Count() == 0)
                status = "{\"jobStatusNameKey\":\"\"}";

            status = fltList.First();



            if (status.Trim().Replace(" ", "").ToLower() == FaultAppointmentProgressStatus.AppointmentArranged.ToString().ToLower())
            {
                status = FaultJobProgressStatus.NotStarted.ToString();
            }
            else if (status.Trim().Replace(" ", "").ToLower() == FaultJobProgressStatus.InProgress.ToString().ToLower())
            {
                status = FaultJobProgressStatus.Started.ToString();
            }

            // Code added  - 12/06/2013 - END

            status = "{\"jobStatusNameKey\":\"" + status + "\"}";

            return status;
        }


        #endregion

        #region Check Bank Holiday
        /// <summary>
        /// This function check whether the selected appointment date exists in bank holiday.
        /// </summary>
        /// <param name="varAppointmentDataStock">The object of appointment</param>
        public bool checkBankHoliday(AppointmentDataStock varAppointmentDataStock)
        {
            bool exist = false;
            if (varAppointmentDataStock.appointmentStartDateTime != null)
            {
                var appointmentDate = (DateTime)varAppointmentDataStock.appointmentStartDateTime;

                var appData = context.G_BANKHOLIDAYS.Where(g => EntityFunctions.TruncateTime(g.BHDATE) == appointmentDate.Date).ToList();
                if (appData.Count() > 0)
                {
                    exist = true;
                }
            }

            return exist;

        }
        #endregion

        #region save Stock Appointment
        /// <summary>
        /// This function saves the Stock appointment in the database
        /// </summary>
        /// <param name="apptData">The object of appointment</param>
        /// <returns>the appointment id if successfully save otherwise 0 </returns>
        public int saveStockAppointment(AppointmentDataStock varAppointmentDataStock, out int surveyId)
        {
            bool success = false;

            var appData = context.PS_Appointment.Where(app => app.SurveyourUserName == varAppointmentDataStock.surveyorUserName
                                                        && ((app.AppointStartDateTime == varAppointmentDataStock.appointmentStartDateTime && app.AppointEndDateTime < varAppointmentDataStock.appointmentEndDateTime)
                                                        || (app.AppointStartDateTime < varAppointmentDataStock.appointmentStartDateTime && app.AppointEndDateTime == varAppointmentDataStock.appointmentEndDateTime)
                                                        || (app.AppointStartDateTime > varAppointmentDataStock.appointmentStartDateTime && app.AppointEndDateTime < varAppointmentDataStock.appointmentEndDateTime)
                                                        || (app.AppointStartDateTime < varAppointmentDataStock.appointmentStartDateTime && app.AppointEndDateTime > varAppointmentDataStock.appointmentEndDateTime)
                                                        || (app.AppointStartDateTime < varAppointmentDataStock.appointmentStartDateTime && app.AppointEndDateTime > varAppointmentDataStock.appointmentStartDateTime)
                                                        || (app.AppointStartDateTime < varAppointmentDataStock.appointmentEndDateTime && app.AppointEndDateTime > varAppointmentDataStock.appointmentEndDateTime))

                                                        );
            if (appData.Count() > 0)
            {
                ErrorFaultSetGet.setErrorFault(String.Format(MessageConstants.AppointmentAlreadyExistMsg, varAppointmentDataStock.surveyorUserName), true, MessageCodesConstants.AppointmentAlreadyExist);
                throw new ArgumentException(String.Format(MessageConstants.AppointmentAlreadyExistMsg, varAppointmentDataStock.surveyorUserName), "surveyorUserName");

            }

            PS_Appointment appointment = new PS_Appointment();
            using (TransactionScope trans = new TransactionScope())
            {

                appointment.AppointLocation = varAppointmentDataStock.appointmentLocation;
                appointment.AppointNotes = varAppointmentDataStock.appointmentNotes;
                appointment.AppointProgStatus = varAppointmentDataStock.appointmentStatus;

                appointment.AppointStartDateTime = varAppointmentDataStock.appointmentStartDateTime;
                appointment.AppointEndDateTime = varAppointmentDataStock.appointmentEndDateTime;
                appointment.AppointTitle = varAppointmentDataStock.appointmentTitle;
                appointment.AppointType = varAppointmentDataStock.appointmentType;

                appointment.AppointValidity = varAppointmentDataStock.appointmentValidity;
                appointment.CreatedBy = varAppointmentDataStock.createdBy;
                appointment.ModifiedBy = varAppointmentDataStock.createdBy;
                appointment.addToCalendar = varAppointmentDataStock.addToCalendar;
                appointment.SurveyourStatus = varAppointmentDataStock.surveyourAvailability;
                appointment.SurveyType = varAppointmentDataStock.surveyType;
                appointment.SurveyourUserName = varAppointmentDataStock.surveyorUserName;
                appointment.CreatedDate = varAppointmentDataStock.loggedDate;
                appointment.ModifiedDate = varAppointmentDataStock.loggedDate;

                if (varAppointmentDataStock.surveyorAlert != null)
                {
                    appointment.AppointmentAlert = varAppointmentDataStock.surveyorAlert;
                }
                else
                {
                    appointment.AppointmentAlert = "None";
                }
                if (varAppointmentDataStock.appointmentCalendar != null)
                {
                    appointment.AppointmentCalendar = varAppointmentDataStock.appointmentCalendar;
                }
                else
                {
                    appointment.AppointmentCalendar = "Property Survey";
                }

                context.AddToPS_Appointment(appointment);
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                PS_Property2Appointment property2Appointment = new PS_Property2Appointment();

                property2Appointment.AppointId = appointment.AppointId;

                if (varAppointmentDataStock.customer.customerId != null)
                {
                    property2Appointment.CustomerId = (int)varAppointmentDataStock.customer.customerId;
                }
                property2Appointment.PropertyId = varAppointmentDataStock.customer.property.propertyId;
                if (varAppointmentDataStock.customer.property.tenancyId != null)
                {
                    property2Appointment.TenancyId = (int)varAppointmentDataStock.customer.property.tenancyId;
                }

                context.PA_PROPERTY_ATTRIBUTES.Where(attrib => attrib.PROPERTYID == varAppointmentDataStock.customer.property.propertyId).ToList().ForEach(attrib => attrib.IsUpdated = false);
                context.AddToPS_Property2Appointment(property2Appointment);
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);

                // Add a new survey for above appointment.
                PS_Survey survey = new PS_Survey();

                survey.PROPERTYID = property2Appointment.PropertyId;
                survey.CompletedBy = varAppointmentDataStock.createdBy;
                survey.SurveyDate = varAppointmentDataStock.loggedDate;

                context.AddToPS_Survey(survey);
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                surveyId = survey.SurveyId;

                // Add a new record in PS_Appointment2Survey to relate survey to appointment.
                PS_Appointment2Survey appointment2Survey = new PS_Appointment2Survey();

                appointment2Survey.AppointId = appointment.AppointId;
                appointment2Survey.SurveyId = surveyId;

                context.AddToPS_Appointment2Survey(appointment2Survey);
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);

                trans.Complete();
                success = true;
            }

            if (success == true)
            {
                return appointment.AppointId;
            }
            else
            {
                return 0;
            }
        }
        #endregion

        #region save NoEntry
        /// <summary>
        /// save NoEntry For Gas
        /// </summary>
        /// <param name="noEntryData"></param>
        /// <returns></returns>
        public int saveNoEntryForGas(UpdateAppointmentData noEntryData)
        {
            bool success = false;
            AS_NoEntry noEntry = new AS_NoEntry();

            //using (TransactionScope trans = new TransactionScope())
            //{

            #region "Insert into No Entry"

            //Insert into no Entry
            noEntry.isCardLeft = noEntryData.appInfoData.isCardLeft;
            noEntry.RecordedBy = (int)noEntryData.createdBy;
            noEntry.RecordedDate = DateTime.Now;
            noEntry.JournalId = (int)noEntryData.journal.journalId;

            context.AddToAS_NoEntry(noEntry);
            #endregion

            #region "Update journal"
            //Update journal
            AS_JOURNAL jorData = context.AS_JOURNAL.Where(jor => jor.JOURNALID == noEntryData.journal.journalId).FirstOrDefault();
            var status = context.AS_Status.Where(s => s.Title == Utilities.Constants.MessageConstants.NoEntryInAS_Status).FirstOrDefault();

            jorData.STATUSID = status.StatusId;
            //jorData.STATUSID = 3;

            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            #endregion

            #region "Insert into Journal History"
            AS_APPOINTMENTS appData = context.AS_APPOINTMENTS.Where(app => app.JournalId == noEntryData.journal.journalId).FirstOrDefault();
            AS_JOURNALHISTORY jorHis = new AS_JOURNALHISTORY()
            {
                ACTIONID = jorData.ACTIONID,
                CREATEDBY = noEntryData.updatedBy,
                CREATIONDATE = DateTime.Now,
                NOTES = appData.NOTES,
                INSPECTIONTYPEID = jorData.INSPECTIONTYPEID,
                STATUSID = jorData.STATUSID,
                PROPERTYID = jorData.PROPERTYID,
                JOURNALID = jorData.JOURNALID,
                ISLETTERATTACHED = false,
                IsDocumentAttached = false
            };

            context.AddToAS_JOURNALHISTORY(jorHis);
            #endregion

            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            //trans.Complete();
            success = true;
            //}

            if (success == true)
            {
                return noEntry.NoEntryID;
            }
            else
            {
                return 0;
            }
        }
        #endregion

        #region Get Total No of NoEntries by PropertyID
        /// <summary>
        /// This function get appointment NoEntry from the database
        /// </summary>
        /// <param name="propertyID">PropertyID</param>
        /// <returns>the no of NoEntries</returns>

        public AppointmentInfoData getTotalNoEntries(string propertyID)
        {
            AppointmentInfoData noEntries = new AppointmentInfoData();

            var noEntriesData = (from entry in context.PS_NoEntry
                                 where entry.PropertyID == propertyID
                                 select entry);
            var totalAppointmentData = (from app in context.PS_Property2Appointment
                                        join a in context.PS_Appointment on app.AppointId equals a.AppointId
                                        where app.PropertyId == propertyID
                                        select a);
            noEntries.TotalNoEntries = noEntriesData.Count();
            noEntries.TotalAppointments = totalAppointmentData.Count();

            return noEntries;
        }

        /// <summary>
        /// This function get appointment NoEntry from the database related to GAS
        /// </summary>
        /// <param name="propertyID">PropertyID</param>
        /// <returns>the no of NoEntries</returns>
        public AppointmentInfoData getTotalNoEntriesGas(string propertyID, int journalId)
        {
            AppointmentInfoData noEntries = new AppointmentInfoData();

            noEntries.TotalNoEntries = 0;
            noEntries.TotalAppointments = 0;
            var isCP12Issued = (from app in context.AS_APPOINTMENTS
                                //join a in context.AS_APPOINTMENTSHISTORY on app.APPOINTMENTID equals a.APPOINTMENTID
                                join jor in context.AS_JOURNAL on app.JournalId equals jor.JOURNALID
                                where jor.PROPERTYID == propertyID && jor.ISCURRENT == true && jor.JOURNALID == journalId && (jor.STATUSID == (context.AS_Status.Where(status => status.Title == Utilities.Constants.MessageConstants.AppointmentArrangedInAS_Status).FirstOrDefault().StatusId) || jor.STATUSID == (context.AS_Status.Where(status => status.Title == Utilities.Constants.MessageConstants.NoEntryInAS_Status).FirstOrDefault().StatusId))
                                select app);

            if (isCP12Issued.Count() > 0)
            {
                var noEntriesData = (from history in context.AS_JOURNALHISTORY
                                     where history.PROPERTYID == propertyID && history.JOURNALID == journalId
                                     && history.STATUSID == (context.AS_Status.Where(status => status.Title == Utilities.Constants.MessageConstants.NoEntryInAS_Status).FirstOrDefault().StatusId)
                                     select history);

                var totalAppointmentData = (from history in context.AS_JOURNALHISTORY
                                            where history.PROPERTYID == propertyID && history.JOURNALID == journalId
                                            && history.STATUSID == (context.AS_Status.Where(status => status.Title == Utilities.Constants.MessageConstants.AppointmentArrangedInAS_Status).FirstOrDefault().StatusId)
                                            select history.JOURNALID);

                noEntries.TotalNoEntries = noEntriesData.Count();
                noEntries.TotalAppointments = totalAppointmentData.Count();
            }
            return noEntries;
        }

        #endregion

        #region Get JobSheetNumber List for all jobs of an AppointmentID
        /// <summary>
        /// This function returns the list of JobSheetNumber of all jobs of an appointment.
        /// </summary>
        /// <param name="appointmentId">appointment id</param>
        /// <returns>the list of JobSheetNumber Object</returns>

        public List<JobData> getJobDataList(int appointmentID)
        {
            var jobSheetNumbers = (from app in context.FL_CO_APPOINTMENT
                                   join fltAppt in context.FL_FAULT_APPOINTMENT on app.AppointmentID equals fltAppt.AppointmentId
                                   join fltl in context.FL_FAULT_LOG on fltAppt.FaultLogId equals fltl.FaultLogID
                                   join flts in context.FL_FAULT_STATUS on fltl.StatusID equals flts.FaultStatusID
                                   join flt in context.FL_FAULT on fltl.FaultID equals flt.FaultID
                                   join are in context.FL_AREA on flt.AREAID equals are.AreaID
                                   join pri in context.FL_FAULT_PRIORITY on flt.PriorityID equals pri.PriorityID
                                   orderby app.AppointmentID
                                   where app.AppointmentID == appointmentID
                                   select new JobData
                                   {
                                       JSNumber = fltl.JobSheetNumber,
                                       faultLogID = fltl.FaultLogID,
                                       JSNDescription = flt.Description,
                                       JSNNotes = fltl.Notes,
                                       JSNLocation = are.AreaName,
                                       duration = (double?)fltl.Duration ?? flt.duration ?? 0,
                                       reportedDate = fltl.SubmitDate,
                                       priority = pri.PriorityName,
                                       responseTime = (pri.ResponseTime == 24 ? "24 Hours" : (pri.ResponseTime == 1 ? "1 Day" : (pri.ResponseTime == 7 ? "7 Days" : "28 Days"))),
                                       jobStatus = flts.Description
                                   });

            List<JobData> jobDataList = new List<JobData>();

            if (jobSheetNumbers.Count() > 0)
            {
                jobDataList = jobSheetNumbers.ToList();

                // Adding fault repair list for complete faults
                foreach (JobData jobData in jobDataList)
                {
                    jobData.durationUnit = jobData.duration.ToString() + " Hour(s)";
                    jobData.repairNotes = GetRepairNotes(jobData.faultLogID);
                    if (jobData.jobStatus.Replace(" ", "") == FaultAppointmentProgressStatus.AppointmentArranged.ToString())
                    {
                        jobData.jobStatus = FaultJobProgressStatus.NotStarted.ToString();
                    }
                    else if (jobData.jobStatus.Replace(" ", "") == FaultJobProgressStatus.InProgress.ToString())
                    {
                        jobData.jobStatus = FaultJobProgressStatus.InProgress.ToString();
                    }

                    var completionDateData = context.C_REPAIR.Where(cr => cr.ITEMDETAILID == jobData.faultLogID);

                    if (completionDateData.Count() > 0)
                    {
                        jobData.completionDate = completionDateData.First().LASTACTIONDATE;
                    }
                    else
                    {
                        jobData.completionDate = null;
                    }

                    var followOnData = context.FL_FAULT_FOLLOWON.Where(fff => fff.FaultLogId == jobData.faultLogID);

                    if (followOnData.Count() > 0)
                    {
                        List<FL_FAULT_FOLLOWON> followOnList = followOnData.ToList();

                        jobData.followOnNotes = followOnList.Last().FollowOnNotes;
                    }
                    else
                    {
                        jobData.followOnNotes = null;
                    }

                    var faultRepair = (from flr in context.FL_CO_FAULTLOG_TO_REPAIR
                                       join frl in context.FL_FAULT_REPAIR_LIST on flr.FaultRepairListID equals frl.FaultRepairListID
                                       where flr.FaultLogID == jobData.faultLogID
                                       select new FaultRepairData
                                       {
                                           FaultRepairID = flr.FaultRepairListID,
                                           Description = frl.Description
                                       });

                    if (faultRepair.Count() > 0)
                    {
                        jobData.faultRepairList = faultRepair.ToList();
                    }
                    List<RepairImageData> propertyPicList = new List<RepairImageData>();
                    var faultRepairImages = (from fri in context.FL_FAULT_REPAIR_IMAGES
                                             where fri.JobSheetNumber == jobData.JSNumber
                                             select new RepairImageData
                                             {
                                                 faultRepairImageId = fri.FaultRepairImageId,
                                                 propertyId = fri.PropertyId,
                                                 jsNumber = fri.JobSheetNumber,
                                                 imageName = fri.ImageName,
                                                 isBeforeImage = fri.IsBeforeImage,
                                                 createdBy = fri.CreatedBy,
                                                 createdOn = fri.CreatedOn

                                             });

                    if (faultRepairImages.Count() > 0)
                    {
                        propertyPicList = faultRepairImages.ToList();
                        foreach (RepairImageData pic in propertyPicList)
                        {
                            pic.imagePath = FileHelper.getLogicalPropertyImagePath(pic.propertyId, pic.imageName);
                        }
                        jobData.repairImageList = propertyPicList;
                    }


                }


            }

            return jobDataList;
        }

        private string GetRepairNotes(int faultLogID)
        {
            var faultRepair = context.FL_CO_FAULTLOG_TO_REPAIR.Where(rep => rep.FaultLogID == faultLogID);

            string repairNotes = string.Empty;
            if (faultRepair != null && faultRepair.Count() > 0)
            {
                repairNotes = faultRepair.First().Notes;
            }
            return repairNotes;
        }
        #endregion

        #region get All Users
        /// <summary>
        /// This function returns all the users of rsl manager database
        /// </summary>        
        /// <returns>list of all users</returns>

        public List<SurveyorUserData> getAllUsers()
        {
            var users = (from app in context.AC_LOGINS
                         join emp in context.E__EMPLOYEE on app.EMPLOYEEID equals emp.EMPLOYEEID
                         where app.ACTIVE == 1
                         orderby app.LOGIN
                         select new SurveyorUserData
                         {
                             userId = app.LOGINID,
                             userName = app.LOGIN,
                             fullName = emp.FIRSTNAME + " " + emp.LASTNAME
                         });



            List<SurveyorUserData> userList = new List<SurveyorUserData>();
            if (users.Count() > 0)
            {
                userList = users.ToList();
            }

            return userList;
        }

        /// <summary>
        /// This function returns all the users of rsl manager database for Gas
        /// </summary>        
        /// <returns>list of all Gas users</returns>
        public List<SurveyorUserData> getAllUsersGas()
        {
            var users = from user in context.AS_USER
                        join login in context.AC_LOGINS on user.EmployeeId equals login.EMPLOYEEID
                        join emp in context.E__EMPLOYEE on user.EmployeeId equals emp.EMPLOYEEID
                        where user.IsActive == true
                        orderby login.LOGIN
                        select new SurveyorUserData
                        {
                            userId = login.LOGINID,
                            userName = login.LOGIN,
                            fullName = emp.FIRSTNAME + " " + emp.LASTNAME
                        };

            List<SurveyorUserData> userList = new List<SurveyorUserData>();
            if (users.Count() > 0)
            {
                userList = users.ToList();
            }

            return userList;
        }

        #endregion

        #region delete Appointment
        /// <summary>
        /// This function is used to delete the appointment from the server
        /// </summary>
        /// <param name="appointmentId">appointment id</param>
        /// <returns>returns true or false based on deleteion</returns>

        public bool deleteAppointment(int appointmentId)
        {
            bool success = false;

            using (TransactionScope trans = new TransactionScope())
            {

                var apptRecord = context.PS_Appointment.Where(app => app.AppointId == appointmentId);
                //Find the survey aginst give appointment id. 
                //var appoint2survey = context.PS_Appointment2Survey.Where(app => app.AppointId == appointmentId);
                //If appointment exist but survey is not saved against appointment then user can delete the appointment
                if (apptRecord.Count() > 0)
                {
                    PS_Appointment appointment = apptRecord.First();
                    context.PS_Survey.Join(context.PS_Appointment2Survey.Where(a2s => a2s.AppointId == appointmentId), s => s.SurveyId, a2s => a2s.SurveyId, (s, a2s) => s).ToList().ForEach(s => context.PS_Survey.DeleteObject(s));
                    context.PS_Appointment2Survey.Where(sur => sur.AppointId == appointmentId).ToList().ForEach(s => context.PS_Appointment2Survey.DeleteObject(s));
                    context.PS_Property2Appointment.Where(prop => prop.AppointId == appointmentId).ToList().ForEach(p => context.PS_Property2Appointment.DeleteObject(p));
                    context.PS_Appointment.DeleteObject(appointment);
                    context.SaveChanges();
                    trans.Complete();

                    success = true;
                }
            }
            return success;
        }
        #endregion

        #region is Appointment Exists against property
        /// <summary>
        /// This function checks the property id against a valid appointment, if that exists then it returns true otherwise false
        /// </summary>
        /// <param name="propertyId">property id </param>
        /// <param name="appointmentType">appoinmtment type</param>
        /// <returns>returns true or false</returns>
        public bool isAppointmentExistsAgainstProperty(string propertyId, string appointmentType, string appointmentStatus)
        {
            bool success = false;
            var apptRecord = (from pte in context.PS_Property2Appointment
                              join apt in context.PS_Appointment on pte.AppointId equals apt.AppointId
                              where pte.PropertyId == propertyId && apt.AppointType.ToLower() == appointmentType.ToLower()
                              && apt.AppointProgStatus.ToLower() == appointmentStatus
                              //&& apt.AppointStartDateTime >= DateTime.Now
                              select apt);

            if (apptRecord.Count() > 0)
            {
                success = true;
            }
            else
            {
                success = false;
            }

            return success;
        }
        #endregion

        #region save last survey date
        /// <summary>
        /// This function will save the last survey date
        /// </summary>
        /// <param name="appointmentId">appointment id</param>
        public void saveLastSurveyDate(int appointmentId)
        {
            var p2app = context.PS_Property2Appointment.Where(app => app.AppointId == appointmentId);
            PS_Property2Appointment property2appointment = p2app.First();
            property2appointment.LastSurveyDate = DateTime.Now;
            context.SaveChanges();
        }
        #endregion

        #region save last survey date of a fault appointment
        /// <summary>
        /// This function will save the last survey date of a fault appointment
        /// </summary>
        /// <param name="appointmentId">appointment id</param>
        public void saveLastSurveyDateFault(int appointmentId)
        {
            var apptlist = context.FL_CO_APPOINTMENT.Where(app => app.AppointmentID == appointmentId);
            FL_CO_APPOINTMENT apptFault = apptlist.First();
            apptFault.LastActionDate = DateTime.Now;
            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
        }
        #endregion

        #region get last survey date

        public DateTime? getLastSurveyDate(string propertyId)
        {
            var p2app = context.PS_Property2Appointment.Where(app => app.PropertyId == propertyId).OrderByDescending(app => app.LastSurveyDate);
            PS_Property2Appointment property2appointment = p2app.First();

            return property2appointment.LastSurveyDate;
        }
        #endregion

        #region Get All Status

        /// <summary>
        /// This method returns status from AS_Status table for gas only
        /// </summary>
        /// <returns></returns>
        public List<StatusData> getAllStatusData()
        {
            var statusData = context.AS_Status.Where(status => status.InspectionTypeID == 1);

            List<StatusData> statuslist = new List<StatusData>();

            if (statusData.Count() > 0)
            {
                List<AS_Status> ldata = statusData.ToList();
                foreach (var item in ldata)
                {
                    StatusData litem = new StatusData();
                    litem.createdBy = item.CreatedBy;
                    litem.createdDate = item.CreatedDate;
                    litem.inspectionTypeId = item.InspectionTypeID;
                    litem.isEditable = item.IsEditable;
                    litem.modifiedBy = item.ModifiedBy;
                    litem.modifiedDate = item.ModifiedDate;
                    litem.ranking = item.Ranking;
                    litem.statusId = item.StatusId;
                    litem.title = item.Title;

                    statuslist.Add(litem);
                }
            }

            return statuslist;
        }

        #endregion

        #region Get Customer List against a tenancy

        /// <summary>
        /// This method returns list of customers against a tenancy id
        /// </summary>
        /// <returns></returns>
        private List<CustomerData> getCustomersInfo(int? tenancyId)
        {
            var custData = (from ten in context.C_TENANCY
                            join cut in context.C_CUSTOMERTENANCY on ten.TENANCYID equals cut.TENANCYID
                            join cus in context.C__CUSTOMER on cut.CUSTOMERID equals cus.CUSTOMERID
                            join gtitle in context.G_TITLE on cus.TITLE equals gtitle.TITLEID into tempgti
                            from gti in tempgti.DefaultIfEmpty()
                            join cad in context.C_ADDRESS on cus.CUSTOMERID equals cad.CUSTOMERID
                            where cad.ISDEFAULT == 1 && ten.TENANCYID == tenancyId
                            select new CustomerData
                            {
                                customerId = cus.CUSTOMERID,
                                title = gti.DESCRIPTION,
                                firstName = cus.FIRSTNAME,
                                middleName = cus.MIDDLENAME,
                                lastName = cus.LASTNAME,
                                telephone = cad.TEL,
                                mobile = cad.MOBILE,
                                fax = cad.FAX,
                                email = cad.EMAIL
                            });

            if (custData.Count() > 0)
            {
                return custData.ToList();
            }
            else
            {
                return null;
            }
        }

        #endregion

        #region Set additional data for Fault Appointment

        /// <summary>
        /// This method sets additional data for Fault Appointment
        /// </summary>
        /// <returns></returns>
        private void setAdditionalDataFault(AllAppointmentsList alf)
        {
            /* Setting the Tenancy Information */

            if (alf.tenancyId != null)
            {
                alf.defaultCustomerId = (from cut in context.C_CUSTOMERTENANCY where alf.tenancyId == cut.TENANCYID select cut.CUSTOMERID).FirstOrDefault();

                alf.customerList = this.getCustomersInfo(alf.tenancyId);

                for (int i = 0; i < alf.customerList.Count(); i++)
                {
                    if (alf.customerList.ElementAt(i).customerId == alf.defaultCustomerId)
                    {
                        alf.defaultCustomerIndex = i;
                    }
                }
            }
            else
            {
                alf.defaultCustomerId = 0;
                alf.customerList = null;
            }

            /* Setting the Customer Vulnerability Information */

            if (alf.customerList != null)
            {
                CustomerDal custDal = new CustomerDal();
                List<PS_GetCustomerRisk_Result> custRiskList = new List<PS_GetCustomerRisk_Result>();
                List<CustomerVulnerabilityData> custVulData = new List<CustomerVulnerabilityData>();

                foreach (CustomerData custData in alf.customerList)
                {
                    int customerId = (int)custData.customerId; // Line changed  - 19/06/2013

                    //add customer vulnerability data
                    custVulData = custDal.getCustomerVulnerabilities(customerId);

                    custData.customerVulnerabilityData = custVulData;

                    //get custoemr risk list
                    custRiskList = custDal.getCustomerRisk(customerId);

                    for (int i = 0; i < custRiskList.Count; i++)
                    {
                        CustomerRiskData custRiskData = new CustomerRiskData();
                        custRiskData.riskCatDesc = custRiskList[i].CATDESC;
                        custRiskData.riskSubCatDesc = custRiskList[i].SUBCATDESC;

                        //add object of risk to  appointment data 
                        custData.customerRiskData.Add(custRiskData);
                    }

                    custData.Address = null;
                    custData.property = null;
                }
            }

            /* Setting the Property Asbestos Risk Information */

            //add property asbestos risk
            PropertyDal propDal = new PropertyDal();
            List<PropertyAsbestosData> propAsbListData = new List<PropertyAsbestosData>();
            string propertyId = alf.property.propertyId;

            //get property asbestos risk
            propAsbListData = propDal.getPropertyAsbestosRisk(propertyId);
            List<PropertyAsbestosData> propertyAsbestosData = new List<PropertyAsbestosData>();
            for (int j = 0; j < propAsbListData.Count; j++)
            {
                PropertyAsbestosData propAsbData = new PropertyAsbestosData();
                propAsbData.asbestosId = propAsbListData[j].asbestosId;
                propAsbData.asbRiskLevelDesc = propAsbListData[j].asbRiskLevelDesc;
                propAsbData.riskDesc = propAsbListData[j].riskDesc;

                //add object of property risk to  appointment data 
                propertyAsbestosData.Add(propAsbData);
            }
            alf.property.propertyAsbestosData = propertyAsbestosData;
            alf.property.propertyPicture = propDal.getAllPropertyImages(propertyId); ;

            /* Setting the Appointment Job Data Information */

            alf.jobDataList = getJobDataList(alf.appointmentId);
        }

        #endregion

        #region Set additional data for Stock Appointment
        /// <summary>
        /// This method sets additional data for Stock Appointment
        /// </summary>
        /// <returns></returns>
        private void setAdditionalDataStock(AllAppointmentsList varAppointmentListStock, int surveyId = -1)
        {
            /* Setting the Stock Appointment Specific Data Information */

            varAppointmentListStock.property.lastSurveyDate = this.getLastSurveyDate(varAppointmentListStock.property.propertyId);
            varAppointmentListStock.appInfoData = this.getTotalNoEntries(varAppointmentListStock.property.propertyId);
            varAppointmentListStock.CP12Info = null; //new IssuedReceivedByDal().getIssuedReceivedByFormData(varAppointmentListStock.appointmentId);
            //CP12 Information is not needed in stock.
            //varAppointmentListStock.CP12Info = null; new IssuedReceivedByDal().getIssuedReceivedByFormData(varAppointmentListStock.appointmentId);

            /* Setting Tenancy Data Information */
            if (varAppointmentListStock.tenancyId != null)
            {
                varAppointmentListStock.defaultCustomerId = (int)context.PS_Property2Appointment.Where(p2a => p2a.AppointId == varAppointmentListStock.appointmentId).FirstOrDefault().CustomerId;

                varAppointmentListStock.customerList = this.getCustomersInfo(varAppointmentListStock.tenancyId);

                for (int i = 0; i < varAppointmentListStock.customerList.Count(); i++)
                {
                    if (varAppointmentListStock.customerList.ElementAt(i).customerId == varAppointmentListStock.defaultCustomerId)
                    {
                        varAppointmentListStock.defaultCustomerIndex = i;
                    }
                }
            }
            else
            {
                varAppointmentListStock.defaultCustomerId = 0;
                varAppointmentListStock.customerList = null;
            }

            /* Setting the Customer Vulnerability Information */
            if (varAppointmentListStock.customerList != null)
            {
                CustomerDal custDal = new CustomerDal();
                List<PS_GetCustomerRisk_Result> custRiskList = new List<PS_GetCustomerRisk_Result>();
                List<CustomerVulnerabilityData> custVulData = new List<CustomerVulnerabilityData>();

                foreach (CustomerData custData in varAppointmentListStock.customerList)
                {
                    int customerId = (int)custData.customerId;

                    //add customer vulnerability data
                    custVulData = custDal.getCustomerVulnerabilities(customerId);

                    custData.customerVulnerabilityData = custVulData;

                    //get custoemr risk list
                    custRiskList = custDal.getCustomerRisk(customerId);

                    for (int i = 0; i < custRiskList.Count; i++)
                    {
                        CustomerRiskData custRiskData = new CustomerRiskData();
                        custRiskData.riskCatDesc = custRiskList[i].CATDESC;
                        custRiskData.riskSubCatDesc = custRiskList[i].SUBCATDESC;

                        //add object of risk to  appointment data 
                        custData.customerRiskData.Add(custRiskData);
                    }

                    custData.Address = null;
                    custData.property = null;
                } //End Customer Data Loop
            }// End Custome Check

            /* Setting the Property Asbestos Risk Information */
            //add property asbestos risk
            PropertyDal propDal = new PropertyDal();
            List<PropertyAsbestosData> propAsbListData = new List<PropertyAsbestosData>();
            //List<PropertyDimData> propDimDataList = new List<PropertyDimData>();
            string propertyId = varAppointmentListStock.property.propertyId;

            //get property asbestos risk
            propAsbListData = propDal.getPropertyAsbestosRisk(propertyId);
            //varAppointmentListStock.property.propertyAsbestosData = propAsbListData;
            PropertyAsbestosData propAsbData = new PropertyAsbestosData();

            //getting property Accommodations
            varAppointmentListStock.property.Accommodations = propDal.getPropertyDimensions(propertyId);

            for (int j = 0; j < propAsbListData.Count; j++)
            {

                propAsbData.asbRiskLevelDesc = propAsbListData[j].asbRiskLevelDesc;
                propAsbData.riskDesc = propAsbListData[j].riskDesc;

                //add object of property risk to  appointment data 
                if (varAppointmentListStock.property.propertyAsbestosData == null)
                {
                    varAppointmentListStock.property.propertyAsbestosData = new List<PropertyAsbestosData>();
                }
                varAppointmentListStock.property.propertyAsbestosData.Add(propAsbData);
            }

            // in case surveyId contains default value of -1, check for survey id from database.
            if (surveyId == -1)
            {
                var appointment2Survey = context.PS_Appointment2Survey.Where(a2s => a2s.AppointId == varAppointmentListStock.appointmentId);
                if (appointment2Survey.Count() > 0) surveyId = appointment2Survey.First().SurveyId;
            }

            varAppointmentListStock.property.propertyPicture = propDal.getAllStockSurveyImages(propertyId, surveyId);
        }
        #endregion

        #region Set additional data for Gas Appointment

        /// <summary>
        /// This method sets additional data for Gas Appointment
        /// </summary>
        /// <returns></returns>
        private void setAdditionalDataGas(AllAppointmentsList alg)
        {
            /* Setting the Stock Appointment Specific Data Information */

            alg.appInfoData = this.getTotalNoEntriesGas(alg.property.propertyId, alg.journalId);
            alg.CP12Info = new IssuedReceivedByDal().getIssuedReceivedByFormData(alg.property.propertyId);

            /* Setting Tenancy Data Information */

            if (alg.tenancyId != null)
            {
                alg.defaultCustomerId = (from cut in context.C_CUSTOMERTENANCY where alg.tenancyId == cut.TENANCYID select cut.CUSTOMERID).FirstOrDefault();

                alg.customerList = this.getCustomersInfo(alg.tenancyId);

                for (int i = 0; i < alg.customerList.Count(); i++)
                {
                    if (alg.customerList.ElementAt(i).customerId == alg.defaultCustomerId)
                    {
                        alg.defaultCustomerIndex = i;
                    }
                }
            }
            else
            {
                alg.defaultCustomerId = 0;
                alg.customerList = null;
            }

            /* Setting the Customer Vulnerability Information */

            if (alg.customerList != null)
            {
                CustomerDal custDal = new CustomerDal();
                List<PS_GetCustomerRisk_Result> custRiskList = new List<PS_GetCustomerRisk_Result>();
                List<CustomerVulnerabilityData> custVulData = new List<CustomerVulnerabilityData>();

                foreach (CustomerData custData in alg.customerList)
                {
                    int customerId = (int)custData.customerId;

                    //add customer vulnerability data
                    custVulData = custDal.getCustomerVulnerabilities(customerId);

                    custData.customerVulnerabilityData = custVulData;

                    //get custoemr risk list
                    custRiskList = custDal.getCustomerRisk(customerId);

                    for (int i = 0; i < custRiskList.Count; i++)
                    {
                        CustomerRiskData custRiskData = new CustomerRiskData();
                        custRiskData.riskCatDesc = custRiskList[i].CATDESC;
                        custRiskData.riskSubCatDesc = custRiskList[i].SUBCATDESC;

                        //add object of risk to  appointment data 
                        custData.customerRiskData.Add(custRiskData);
                    }

                    custData.Address = null;
                    custData.property = null;
                }
            }

            /* Setting the Property Asbestos Risk Information */

            //add property asbestos risk
            PropertyDal propDal = new PropertyDal();
            List<PropertyAsbestosData> propAsbListData = new List<PropertyAsbestosData>();
            string propertyId = alg.property.propertyId;

            //get property asbestos risk
            propAsbListData = propDal.getPropertyAsbestosRisk(propertyId);

            for (int j = 0; j < propAsbListData.Count; j++)
            {
                PropertyAsbestosData propAsbData = new PropertyAsbestosData();
                propAsbData.asbRiskLevelDesc = propAsbListData[j].asbRiskLevelDesc;
                propAsbData.riskDesc = propAsbListData[j].riskDesc;

                //Check for null reference if it is null initialize a new object.
                if (alg.property.propertyAsbestosData == null)
                    alg.property.propertyAsbestosData = new List<PropertyAsbestosData>();
                //add object of property risk to  appointment data 
                alg.property.propertyAsbestosData.Add(propAsbData);
            }

            alg.property.Accommodations = propDal.getPropertyDimensions(propertyId);

            alg.property.propertyPicture = propDal.getAllPropertyImages(propertyId);
        }

        #endregion

        #region Set additional data for Planned Appointment

        /// <summary>
        /// This method sets additional data for Planned Appointment
        /// </summary>
        /// <returns></returns>
        private void setAdditionalDataPlanned(AllAppointmentsList alf)
        {
            /* Setting the Tenancy Information */

            if (alf.tenancyId != null && alf.tenancyId > 0)
            {
                alf.defaultCustomerId = (from cut in context.C_CUSTOMERTENANCY where alf.tenancyId == cut.TENANCYID select cut.CUSTOMERID).FirstOrDefault();

                alf.customerList = this.getCustomersInfo(alf.tenancyId);

                if (alf.customerList != null)
                {
                    for (int i = 0; i < alf.customerList.Count(); i++)
                    {
                        if (alf.customerList.ElementAt(i).customerId == alf.defaultCustomerId)
                        {
                            alf.defaultCustomerIndex = i;
                        }
                    }
                }
                else
                {
                    alf.defaultCustomerId = 0;
                    alf.defaultCustomerIndex = -1;
                }

            }
            else
            {
                alf.defaultCustomerId = 0;
                alf.customerList = null;
                alf.defaultCustomerIndex = -1;
            }

            /* Setting the Customer Vulnerability Information */

            if (alf.customerList != null)
            {
                CustomerDal custDal = new CustomerDal();
                List<PS_GetCustomerRisk_Result> custRiskList = new List<PS_GetCustomerRisk_Result>();
                List<CustomerVulnerabilityData> custVulData = new List<CustomerVulnerabilityData>();

                foreach (CustomerData custData in alf.customerList)
                {
                    int customerId = (int)custData.customerId;

                    //add customer vulnerability data
                    custVulData = custDal.getCustomerVulnerabilities(customerId);

                    custData.customerVulnerabilityData = custVulData;

                    //get custoemr risk list
                    custRiskList = custDal.getCustomerRisk(customerId);

                    for (int i = 0; i < custRiskList.Count; i++)
                    {
                        CustomerRiskData custRiskData = new CustomerRiskData();
                        custRiskData.riskCatDesc = custRiskList[i].CATDESC;
                        custRiskData.riskSubCatDesc = custRiskList[i].SUBCATDESC;

                        //add object of risk to  appointment data 
                        custData.customerRiskData.Add(custRiskData);
                    }

                    custData.Address = null;
                    custData.property = null;
                }
            }

            /* Setting the Property Asbestos Risk Information */

            //add property asbestos risk
            PropertyDal propDal = new PropertyDal();
            List<PropertyAsbestosData> propAsbListData = new List<PropertyAsbestosData>();
            string propertyId = alf.property.propertyId;

            //get property asbestos risk
            propAsbListData = propDal.getPropertyAsbestosRisk(propertyId);
            List<PropertyAsbestosData> propertyAsbestosData = new List<PropertyAsbestosData>();
            for (int j = 0; j < propAsbListData.Count; j++)
            {
                PropertyAsbestosData propAsbData = new PropertyAsbestosData();
                propAsbData.asbestosId = propAsbListData[j].asbestosId;
                propAsbData.asbRiskLevelDesc = propAsbListData[j].asbRiskLevelDesc;
                propAsbData.riskDesc = propAsbListData[j].riskDesc;

                //add object of property risk to  appointment data 
                propertyAsbestosData.Add(propAsbData);
            }
            alf.property.propertyAsbestosData = propertyAsbestosData;
            alf.property.propertyPicture = propDal.getAllPropertyImages(propertyId); ;

            /* Setting the Appointment Job Data Information */

            // alf.jobDataList = getPlannedJobDataList(alf.appointmentId);
        }

        #endregion

        #region is Property Exists
        //Code added  - 11/07/2013 - START
        /// <summary>
        /// This function checks the property id, if that exists then it returns true otherwise false
        /// </summary>
        /// <param name="propertyId">property id </param>        
        /// <returns>returns true or false</returns>

        public bool isPropertyExists(string propertyId)
        {
            bool success = false;
            var propertyRecord = context.P__PROPERTY.Where(pro => pro.PROPERTYID.ToLower() == propertyId.ToLower());

            if (propertyRecord.Count() > 0)
            {
                success = true;
            }
            else
            {
                success = false;
            }

            return success;
        }
        //Code added  - 11/07/2013 - END
        #endregion

        #region Complete Appointment For Stock
        /// <summary>
        /// This function updates the appointment. This function use post method to accept data. 
        /// </summary>
        /// <param name="apptData">This fucntion accepts the AppointmentData's object </param>
        /// <returns>It returns true or false in update is successful</returns>
        public bool completeAppointmentForStock(UpdateAppointmentData apptData)
        {
            bool success = false;
            int apointmentId = apptData.appointmentId;
            //using (TransactionScope trans = new TransactionScope())
            //{
            var apptRecord = context.PS_Appointment.Where(app => app.AppointId == apointmentId);
            List<string> completedStatusesList = new List<string>() {AppointmentCompleteStatus.Finished.ToString(),
                                        AppointmentCompleteStatus.Complete.ToString(),
                                        AppointmentCompleteStatus.Completed.ToString(),
                                        AppointmentCompleteStatus.NoEntry.ToString()};

            if (apptRecord.Count() > 0)
            {
                PS_Appointment appointment = apptRecord.First();
                if (apptData.appointmentLocation != null)
                {
                    appointment.AppointLocation = apptData.appointmentLocation;
                }
                if (apptData.appointmentNotes != null)
                {
                    appointment.AppointNotes = apptData.appointmentNotes;
                }
                if (apptData.appointmentStatus != null)
                {
                    appointment.AppointProgStatus = apptData.appointmentStatus;
                }

                if (completedStatusesList.Contains(apptData.appointmentStatus))
                {
                    if (apptData.loggedDate != null)
                    {
                        appointment.AppointmentCompletionDate = apptData.loggedDate;
                    }

                    if (apptData.appointmentCurrentAppVersion != null)
                    {
                        appointment.AppointmentCurrentAppVersion = apptData.appointmentCurrentAppVersion;
                    }

                    if (apptData.appointmentCompletedAppVersion != null)
                    {
                        appointment.AppointmentCompletedAppVersion = apptData.appointmentCompletedAppVersion;
                    }
                }

                if (apptData.appointmentTitle != null)
                {
                    appointment.AppointTitle = apptData.appointmentTitle;
                }
                if (apptData.appointmentType != null)
                {
                    appointment.AppointType = apptData.appointmentType;
                }


                if (apptData.createdBy != null)
                {
                    appointment.CreatedBy = apptData.createdBy;
                }

                if (apptData.surveyourAvailability != null)
                {
                    appointment.SurveyourStatus = apptData.surveyourAvailability;
                }
                if (apptData.surveyType != null)
                {
                    appointment.SurveyType = apptData.surveyType;
                }
                if (apptData.surveyorUserName != null)
                {
                    appointment.SurveyourUserName = apptData.surveyorUserName;
                }

                // appointment.ModifiedDate = apptData.loggedDate;
                if (apptData.surveyorAlert != null)
                {
                    appointment.AppointmentAlert = apptData.surveyorAlert;
                }
                if (apptData.addToCalendar != null)
                {
                    appointment.addToCalendar = apptData.addToCalendar;
                }
                appointment.ModifiedDate = DateTime.Now;
                appointment.ModifiedBy = apptData.updatedBy;

                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);

                var apptPropertyRecord = context.PS_Property2Appointment.Where(prop => prop.AppointId == apointmentId);
                if (apptPropertyRecord.Count() > 0)
                {
                    PS_Property2Appointment property = apptPropertyRecord.First();

                    property.AppointId = apointmentId;
                    if ((apptData.customerList != null) & (apptData.customerList.Count > 0))
                    {

                        if (apptData.customerList[0].customerId != null)
                        {
                            property.CustomerId = (int)apptData.customerList[0].customerId;
                        }

                        if (apptData.property.propertyId != null)
                        {
                            property.PropertyId = apptData.property.propertyId;
                        }
                        if (apptData.property.tenancyId != null)
                        {
                            property.TenancyId = (int)apptData.property.tenancyId;
                        }
                    }
                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                }
                //trans.Complete();
                success = true;
            }
            //}
            return success;
        }

        #endregion

        #region Complete Appointment For Gas
        /// <summary>
        /// This function updates the appointment. This function use post method to accept data. 
        /// </summary>
        /// <param name="apptData">This fucntion accepts the AppointmentData's object </param>
        /// <returns>It returns true or false in update is successful</returns>
        public bool completeAppointmentForGas(UpdateAppointmentData apptData)
        {
            bool success = false;
            int apointmentId = apptData.appointmentId;
            //using (TransactionScope trans = new TransactionScope())
            //{

            var apptRecord = context.AS_APPOINTMENTS.Where(app => app.APPOINTMENTID == apointmentId);

            if (apptRecord.Count() > 0)
            {
                AS_APPOINTMENTS appointment = apptRecord.First();
                if (apptData.tenancyId != null)
                {
                    appointment.TENANCYID = apptData.tenancyId;
                }
                if (apptData.jsgNumber != null)
                {
                    appointment.JSGNUMBER = apptData.jsgNumber;
                }
                if (apptData.journalId != null)
                {
                    appointment.JournalId = (int)apptData.journalId;
                }
                if (apptData.journalHistoryId != null)
                {
                    appointment.JOURNALHISTORYID = (long)apptData.journalHistoryId;
                }

                if (apptData.appointmentShift != null)
                {
                    appointment.APPOINTMENTSHIFT = apptData.appointmentShift;
                }

                if (apptData.assignedTo != null)
                {
                    appointment.ASSIGNEDTO = (int)apptData.assignedTo;
                }

                if (apptData.createdBy != null)
                {
                    appointment.CREATEDBY = apptData.createdBy;
                }

                if (apptData.appointmentCalendar != null)
                {
                    appointment.APPOINTMENTCALENDER = apptData.appointmentCalendar;
                }
                if (apptData.appointmentStatus != null)
                {
                    appointment.APPOINTMENTSTATUS = apptData.appointmentStatus;
                }

                if (apptData.surveyorAlert != null)
                {
                    appointment.APPOINTMENTALERT = apptData.surveyorAlert;
                }
                if (apptData.surveyType != null)
                {
                    appointment.SURVEYTYPE = apptData.surveyType;
                }
                if (apptData.addToCalendar != null)
                {
                    appointment.addToCalendar = apptData.addToCalendar;
                }

                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);

                #region "Insert into Appointment history"

                //Insert into Appointment History
                AS_APPOINTMENTSHISTORY apphis = new AS_APPOINTMENTSHISTORY()
                {
                    APPOINTMENTALERT = appointment.APPOINTMENTALERT,
                    APPOINTMENTCALENDER = appointment.APPOINTMENTCALENDER,
                    APPOINTMENTDATE = appointment.APPOINTMENTDATE,
                    APPOINTMENTENDTIME = appointment.APPOINTMENTENDTIME,
                    APPOINTMENTSTARTTIME = appointment.APPOINTMENTSTARTTIME,
                    APPOINTMENTSTATUS = appointment.APPOINTMENTSTATUS,
                    ASSIGNEDTO = appointment.ASSIGNEDTO,
                    CREATEDBY = appointment.CREATEDBY,
                    JournalId = appointment.JournalId,
                    JSGNUMBER = appointment.JSGNUMBER.ToString(),
                    APPOINTMENTID = appointment.APPOINTMENTID,
                    APPOINTMENTSHIFT = appointment.APPOINTMENTSHIFT,
                    JOURNALHISTORYID = appointment.JOURNALHISTORYID,
                    LOGGEDDATE = apptData.loggedDate,
                    NOTES = appointment.NOTES,
                    TENANCYID = appointment.TENANCYID,
                    SURVEYOURSTATUS = appointment.SURVEYOURSTATUS
                };

                context.AddToAS_APPOINTMENTSHISTORY(apphis);
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                #endregion
                //trans.Complete();
                success = true;
                //}
            }


            return success;
        }

        #endregion

        #region Update Cp12 Info
        public bool updateLgsrData(UpdateAppointmentData apptData)
        {
            bool success = false;
            //using (TransactionScope trans = new TransactionScope())
            //{
            //Check lgsr exist against propertyId
            var lgsrRecord = context.P_LGSR.Where(lgsrData => lgsrData.PROPERTYID == apptData.property.propertyId);
            P_LGSR lgsr = new P_LGSR();
            if (lgsrRecord.Count() > 0)
            {
                lgsr = lgsrRecord.First();
            }
            if (apptData.CP12Info.IssuedBy != null)
            {
                lgsr.CP12ISSUEDBY = apptData.CP12Info.IssuedBy;
            }
            if (apptData.CP12Info.IssuedDate != null)
            {
                lgsr.ISSUEDATE = apptData.CP12Info.IssuedDate;
            }
            if (apptData.CP12Info.CP12Number != null)
            {
                lgsr.CP12NUMBER = apptData.CP12Info.CP12Number;
            }
            lgsr.DOCUMENTTYPE = "pdf";
            if (apptData.CP12Info.DocumentType != null)
            {
                lgsr.DOCUMENTTYPE = apptData.CP12Info.DocumentType;
            }

            lgsr.DTIMESTAMP = apptData.CP12Info.DTimeStamp;

            if (apptData.CP12Info.InspectionCarried != null)
            {
                lgsr.INSPECTIONCARRIED = apptData.CP12Info.InspectionCarried;
            }
            if (apptData.CP12Info.Notes != null)
            {
                lgsr.NOTES = apptData.CP12Info.Notes;
            }
            if (apptData.CP12Info.IssuedDate != null)
            {
                lgsr.RECEVIEDDATE = apptData.CP12Info.ReceivedDate;
            }
            if (apptData.CP12Info.ReceivedOnBehalfOf != null)
            {
                lgsr.RECEVIEDONBEHALF = apptData.CP12Info.ReceivedOnBehalfOf;
            }
            if (apptData.CP12Info.TenantHanded != null)
            {
                lgsr.TENANTHANDED = apptData.CP12Info.TenantHanded;
            }
            if (apptData.appointmentStatus == AppointmentCompleteStatus.Finished.ToString())
            {
                if (apptData.journalId != null)
                {
                    lgsr.JOURNALID = apptData.journalId;
                }
            }

            if (apptData.appointmentStatus.ToLower() == AppointmentCompleteStatus.Finished.ToString().ToLower()
                || apptData.appointmentStatus.ToLower() == AppointmentCompleteStatus.Complete.ToString().ToLower()
                || apptData.appointmentStatus.ToLower() == AppointmentCompleteStatus.Completed.ToString().ToLower())
            {
                lgsr.IsAppointmentCompleted = true;
            }
            else
            {
                lgsr.IsAppointmentCompleted = false;
            }


            if (lgsrRecord.Count() <= 0)
            {
                context.AddToP_LGSR(lgsr);
            }

            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            ////Insert data into P_LGSR_HISTORY
            int lgsrId = lgsr.LGSRID;
            P_LGSR_HISTORY lgsrHistory = new P_LGSR_HISTORY();
            if (apptData.CP12Info.IssuedBy != null)
            {
                lgsrHistory.CP12ISSUEDBY = apptData.CP12Info.IssuedBy;
            }
            if (apptData.CP12Info.IssuedDate != null)
            {
                lgsrHistory.ISSUEDATE = apptData.CP12Info.IssuedDate;
            }
            if (apptData.CP12Info.CP12Number != null)
            {
                lgsrHistory.CP12NUMBER = apptData.CP12Info.CP12Number;
            }
            if (apptData.CP12Info.DocumentType != null)
            {
                lgsrHistory.DOCUMENTTYPE = apptData.CP12Info.DocumentType;
            }
            if (apptData.CP12Info.DTimeStamp != null)
            {
                lgsrHistory.DTIMESTAMP = apptData.CP12Info.DTimeStamp;
            }
            if (apptData.CP12Info.InspectionCarried != null)
            {
                lgsrHistory.INSPECTIONCARRIED = apptData.CP12Info.InspectionCarried;
            }
            if (apptData.CP12Info.Notes != null)
            {
                lgsrHistory.NOTES = apptData.CP12Info.Notes;
            }
            if (apptData.CP12Info.ReceivedDate != null)
            {
                lgsrHistory.RECEVIEDDATE = apptData.CP12Info.ReceivedDate;
            }
            if (apptData.CP12Info.ReceivedOnBehalfOf != null)
            {
                lgsrHistory.RECEVIEDONBEHALF = apptData.CP12Info.ReceivedOnBehalfOf;
            }
            if (apptData.CP12Info.TenantHanded != null)
            {
                lgsrHistory.TENANTHANDED = apptData.CP12Info.TenantHanded;
            }
            if (apptData.journal.journalId != null)
            {
                lgsrHistory.JOURNALID = apptData.journal.journalId;
            }
            if (apptData.property.propertyId != null)
            {
                lgsrHistory.PROPERTYID = apptData.property.propertyId;
            }

            lgsrHistory.IsAppointmentCompleted = lgsr.IsAppointmentCompleted;
            lgsrHistory.LGSRID = lgsrId;

            context.AddToP_LGSR_HISTORY(lgsrHistory);

            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);

            //trans.Complete();
            success = true;
            //}
            return success;
        }

        #endregion

        #region "Update And Insert Into Journal"
        /// <summary>
        /// This function updates the journal table, inserts in the journal history table. As per the comments of the client
        /// following implementation has been done.
        /// "If the appoinment does not exist against PropertyId (JournalId), then another appointment can be created. To create the appointment, we need to have a Journal Id. If the JournalId exists in AS_APPOINTMENTS, it suggests that appointment already exists. In order to create a new appointment for same property, we need to ensure that the JournalId is not repeated. In other words, one JournalId will only have one record in AS_APPOINTMENTS. The appliance servicing team stance is correct
        /// In terms of your 2nd question, hitting Save on screen 17 (means issuing certificate) will update the status of AS_journal to CP12 Issued. Also it will then make IsCurrent=0 and Insert a new reocord in AS_JOurnal with status 'Appointment to be Arranged' with isCurrent=1 for newly inserted record"
        /// </summary>
        /// <param name="workData"></param>
        public bool UpdateAndInsertJournal(UpdateAppointmentData appointment)
        {
            bool result = false;
            //using (TransactionScope trans = new TransactionScope())
            //{
            #region "Update AS_JOURNAL Table"
            LGSRData workData = appointment.CP12Info;


            var JournalData = context.AS_JOURNAL.Where(jor => jor.PROPERTYID == appointment.property.propertyId && jor.JOURNALID == appointment.journal.journalId && jor.ISCURRENT == true).FirstOrDefault();

            if (JournalData != null)
            {
                #region Update Journal Data for CP12 Satus

                //Get Status for "CP12 issued" status form 
                var cp12Status = context.AS_Status.Where(status => status.Title == Utilities.Constants.MessageConstants.CP12IssuedNameInAS_Status).FirstOrDefault();

                int cp12StatusId = cp12Status.StatusId;

                var cp12Action = context.AS_Action.Where(action => action.Title == Utilities.Constants.MessageConstants.CP12IssuedNameInAS_Status && action.StatusId == cp12StatusId).FirstOrDefault();

                //Update the status in AS_JOURNAL
                JournalData.STATUSID = cp12Status.StatusId;
                JournalData.ACTIONID = cp12Action.ActionId;
                JournalData.ISCURRENT = false;
                //JournalData.CREATEDBY = workData.IssuedBy;
                //JournalData.CREATIONDATE = workData.IssuedDate;
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);

                #region Insert History record into AS_JOURNALHISTORY table for CP12 Status

                //Insert in AS_JOURNALHISTORY            
                AS_JOURNALHISTORY lobjJournalHistory = new AS_JOURNALHISTORY();
                lobjJournalHistory.JOURNALID = JournalData.JOURNALID;
                lobjJournalHistory.PROPERTYID = JournalData.PROPERTYID;
                lobjJournalHistory.STATUSID = JournalData.STATUSID;
                lobjJournalHistory.ACTIONID = JournalData.ACTIONID;
                lobjJournalHistory.INSPECTIONTYPEID = JournalData.INSPECTIONTYPEID;
                lobjJournalHistory.CREATIONDATE = DateTime.Now;
                lobjJournalHistory.CREATEDBY = appointment.updatedBy;
                //lobjJournalHistory.NOTES = AppointmentData.NOTES;
                lobjJournalHistory.ISLETTERATTACHED = false;
                lobjJournalHistory.IsDocumentAttached = false;

                context.AddToAS_JOURNALHISTORY(lobjJournalHistory);
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);

                #endregion

                #endregion

                #region Insert new record into AS_JOURNAL with Appoinetment To Be Arranged Status

                //Insert in AS_JOURNAL and AS_JOURNALHISTORY
                AS_JOURNAL lobjNewJournal = new AS_JOURNAL();
                AS_JOURNALHISTORY lobjNewJournalHistory = new AS_JOURNALHISTORY();

                var lIdOfAppointmentToBeArrangedStatus = context.AS_Status.Where(status => status.Title == Utilities.Constants.MessageConstants.AppointmentToBeArrangedStatus).FirstOrDefault();
                int appointmentToBeArrangedStatusId = lIdOfAppointmentToBeArrangedStatus.StatusId;

                var lATBAAction = context.AS_Action.Where(action => action.Title == Utilities.Constants.MessageConstants.AppointmentToBeArrangedStatus && action.StatusId == appointmentToBeArrangedStatusId).FirstOrDefault();

                lobjNewJournal.PROPERTYID = JournalData.PROPERTYID;
                lobjNewJournal.STATUSID = appointmentToBeArrangedStatusId;
                lobjNewJournal.ACTIONID = lATBAAction.ActionId;
                lobjNewJournal.INSPECTIONTYPEID = JournalData.INSPECTIONTYPEID;
                lobjNewJournal.CREATIONDATE = DateTime.Now;
                lobjNewJournal.CREATEDBY = workData.IssuedBy;
                lobjNewJournal.ISCURRENT = true;

                context.AddToAS_JOURNAL(lobjNewJournal);
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);

                #region Insert History record into AS_JOURNALHISTORY for new journal Record (above)

                lobjNewJournalHistory.JOURNALID = lobjNewJournal.JOURNALID;
                lobjNewJournalHistory.PROPERTYID = lobjNewJournal.PROPERTYID;
                lobjNewJournalHistory.STATUSID = lobjNewJournal.STATUSID;
                lobjNewJournalHistory.ACTIONID = lobjNewJournal.ACTIONID;
                lobjNewJournalHistory.INSPECTIONTYPEID = lobjNewJournal.INSPECTIONTYPEID;
                lobjNewJournalHistory.CREATIONDATE = lobjNewJournal.CREATIONDATE;
                lobjNewJournalHistory.CREATEDBY = lobjNewJournal.CREATEDBY;
                //lobjNewJournalHistory.NOTES = AppointmentData.NOTES;
                lobjNewJournalHistory.ISLETTERATTACHED = false;
                lobjNewJournalHistory.IsDocumentAttached = false;

                context.AddToAS_JOURNALHISTORY(lobjNewJournalHistory);

                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);

                #endregion

                #endregion

                result = true;
            }
            #endregion

            //trans.Complete();
            //}
            return result;
        }
        #endregion

        #region Save Fault paused
        public Nullable<int> saveFaultPaused(JobPauseHistoryData jobPauseData, int faultLogID, Nullable<int> faultLogHistoryId = null)
        {
            Nullable<int> pauseID = null;
            FL_FAULT_PAUSED faultPaused = new FL_FAULT_PAUSED();
            faultPaused.FaultLogId = faultLogID;
            faultPaused.PausedOn = jobPauseData.pauseDate;
            faultPaused.Notes = jobPauseData.pauseNote;
            faultPaused.Reason = jobPauseData.pauseReason;
            faultPaused.PausedBy = jobPauseData.pausedBy;
            faultPaused.FaultLogHistoryID = faultLogHistoryId;
            context.AddToFL_FAULT_PAUSED(faultPaused);
            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            pauseID = faultPaused.PauseID;
            return pauseID;
        }
        #endregion

        #region Save Fault Repair List
        public bool saveFaultRepairList(JobData jobData, int? userId)
        {
            bool success = false;

            if (jobData.faultRepairList.Count > 0)
            {
                StringBuilder sbRepairIds = new StringBuilder();
                foreach (FaultRepairData faultRepairData in jobData.faultRepairList)
                {
                    sbRepairIds.Append(faultRepairData.FaultRepairID);
                    sbRepairIds.Append(",");
                    // apptDal.SaveFaultPaused(jobPauseData, jobData.faultLogID);
                }
                string FaultRepairIDs = sbRepairIds.ToString().Substring(0, sbRepairIds.ToString().Length - 1);
                System.Data.Objects.ObjectParameter result = new ObjectParameter("result", typeof(int));
                context.FL_ADD_FL_CO_FAULTLOG_TO_REPAIR(jobData.faultLogID, FaultRepairIDs, userId, jobData.repairNotes, result);
                if (Convert.ToInt32(result.Value) == 1)
                    success = true;
                else
                    success = false;
            }
            return success;
        }
        #endregion

        #region Save NoEntry for Fault
        /// <summary>
        /// This function update the status of all jobs of an appointment to noEntry. This function use post method to accept data. 
        /// </summary>
        /// <param name="appointment">This fucntion accepts the FaultStatusData's object </param>
        /// <param name="userName">user name </param>
        /// <param name="salt">salt </param>
        /// <returns>It returns true or false in update is successful</returns>
        public ResultBoolData noEntryFaultAppointment(UpdateAppointmentData appointment)
        {
            if (appointment.repairCompletionDateTime == null)
                appointment.repairCompletionDateTime = DateTime.Now;

            ResultBoolData resultBoolData = new ResultBoolData();
            bool success = false;
            string status = FaultJobProgressStatus.NoEntry.ToString();

            List<FL_FAULT_APPOINTMENT> fltApptList = getFaultAppointmentList(appointment.appointmentId);

            // check if appointment has been cancelled from Web module
            if (fltApptList.Count() > 0)
            {
                var varfaultLogID = fltApptList.First().FaultLogId;
                if (isFaultCancelled(varfaultLogID))
                {
                    resultBoolData.result = success;
                    return resultBoolData;
                }
            }

            //using (TransactionScope trans = new TransactionScope())
            //{
            foreach (FL_FAULT_APPOINTMENT fltAppt in fltApptList)
            {
                int faultLogID = fltAppt.FaultLogId;

                // Get Job data from appointment, in case it is null create a new instance to avoid null reference exception, in function call below.
                JobData Jobdata = (JobData)(appointment.jobDataList.AsEnumerable<JobData>().Where(jobdata => jobdata.faultLogID == faultLogID).FirstOrDefault());
                Jobdata = Jobdata ?? new JobData();

                //1.) INSERT INTO FL_FAULT_NOENTRY                       
                FL_FAULT_NOENTRY faultNoEntry = new FL_FAULT_NOENTRY();
                faultNoEntry.FaultLogId = faultLogID;
                faultNoEntry.RecordedOn = Jobdata.completionDate ?? appointment.repairCompletionDateTime;
                faultNoEntry.Notes = appointment.noEntryNotes;
                context.AddToFL_FAULT_NOENTRY(faultNoEntry);

                //2.) INSERT INTO FL_FAULT_LOG_HISTORY (Status='No Entry')
                //3.) UPDATE FL_FAULT_LOG SET FaultStatusId='Completed'
                //4.) INSERT INTO FL_FAULT_LOG_HISTORY (Status='Completed')
                //5.) UPDATE FL_FAULT_JOURNAL SET FaultStatusId='Completed'                       
                this.updateFaultStatusId(context, faultLogID, FaultJobProgressStatus.NoEntry.ToString(), appointment.createdBy, Jobdata.completionDate);
                this.updateFaultStatusId(context, faultLogID, FaultJobProgressStatus.Complete.ToString(), appointment.createdBy, Jobdata.completionDate, Jobdata.completionDate);

                //6.) UPDATE FL_FAULT_JOBTIMESHEET SET JobEndDate/Time
                saveUpdateFaultTimeSheet(appointment.appointmentId, faultLogID, Jobdata.completionDate ?? appointment.repairCompletionDateTime, FaultJobProgressStatus.NoEntry.ToString());

                //7.) UPDATE FL_CO_APPOINTMENT SET AppointmentStatus='Completed'
                var flAppointment = context.FL_CO_APPOINTMENT.Where(app => app.AppointmentID == appointment.appointmentId).First();
                flAppointment.AppointmentStatus = FaultJobProgressStatus.Complete.ToString();
                flAppointment.LastActionDate = Jobdata.completionDate ?? appointment.repairCompletionDateTime;
                flAppointment.RepairCompletionDateTime = flAppointment.RepairCompletionDateTime ?? Jobdata.completionDate ?? appointment.repairCompletionDateTime;
                success = true;
            }

            if (success == true)
            {
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            }

            resultBoolData.result = success;
            return resultBoolData;
        }
        #endregion

        #region Complete Appointment For Fault
        public bool completeAppointmentForFault(JobData apptData, UpdateAppointmentData appointment)
        {
            if (appointment.appointmentStatus == AppointmentCompleteStatus.Complete.ToString())
            {
                if (apptData.completionDate == null)
                    apptData.completionDate = DateTime.Now;

                System.Data.Objects.ObjectParameter result1 = new ObjectParameter("result", typeof(int));
                context.FL_CO_COMPLETE_APPOINTMENT(appointment.appointmentId, apptData.faultLogID, apptData.followOnNotes, apptData.repairNotes,
                    apptData.completionDate, true, appointment.repairCompletionDateTime ?? apptData.completionDate, result1);
            }
            return true;
        }
        #endregion

        #region Paused Appointment For Fault

        public bool PausedAppointmentForFault(JobData jobData, UpdateAppointmentData appointment)
        {
            //2.) UPDATE FL_FAULT_LOG SET FaultStatusId=’Paused’
            //3.) INSERT INTO FL_FAULT_LOG_HISTORY (Status=’Paused)
            //4.) UPDATE FL_FAULT_JOURNAL SET FaultStatusId=’Paused’                      
            //TODO: Move faultlog update logic to a separate/independent locaiotn and update it for each status change occourance like (start --> pause --> resume(start again) --> complete etc.)
            if (jobData != null)
                this.updateFaultStatusId(context, jobData.faultLogID, jobData.jobStatus, appointment.createdBy, DateTime.Now);

            bool isAppointmentFinished = appointment.appointmentStatus == FaultAppointmentProgressStatus.Complete.ToString() ? true : false;

            this.updateFaultAppointmentProgressStatus(appointment.appointmentId, appointment.appointmentStatus, isAppointmentFinished);
            //faultJobTimeSheet.EndTime = apptData.reportedDate;
            this.saveLastSurveyDateFault(appointment.appointmentId);

            return true;
        }

        #endregion

        #region Save Planned appointment paused
        /// <summary>
        /// Save Planned appointment paused
        /// </summary>
        /// <param name="jobPauseData"></param>
        /// <param name="appointmentId"></param>
        /// <returns></returns>
        public Nullable<int> savePlannedPaused(JobPauseHistoryData jobPauseData, int appointmentId, Nullable<int> appointmentHistoryID = null)
        {
            Nullable<int> pauseID = null;
            PLANNED_PAUSED plannedPaused = new PLANNED_PAUSED();
            plannedPaused.APPOINTMENTID = appointmentId;
            plannedPaused.PausedOn = jobPauseData.pauseDate;
            plannedPaused.Notes = jobPauseData.pauseNote;
            plannedPaused.Reason = jobPauseData.pauseReason;
            plannedPaused.PausedBy = jobPauseData.pausedBy;
            plannedPaused.APPOINTMENTHISTORYID = appointmentHistoryID;
            context.AddToPLANNED_PAUSED(plannedPaused);
            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            pauseID = plannedPaused.PauseID;
            return pauseID;
        }
        #endregion

        #region Save NoEntry for Planned

        /// <summary>
        /// This function update the status of all jobs of an appointment to noEntry. This function use post method to accept data. 
        /// </summary>
        /// <param name="appointment"> </param>      
        /// <returns>It returns true or false in update is successful</returns>
        public ResultBoolData noEntryPlannedAppointment(UpdateAppointmentData appointment, bool callfromAPIV2 = false)
        {
            ResultBoolData resultBoolData = new ResultBoolData();
            bool success = false;

            // check if appointment has been cancelled from Web module

            int JournalId = (int)appointment.journalId;
            if (isPlannedCancelled(JournalId))
            {
                resultBoolData.result = success;
                return resultBoolData;
            }
            using (TransactionScope trans = new TransactionScope())
            {
                //1.) INSERT INTO PLANNED_NOENTRY                       
                PLANNED_NOENTRY plannedNoEntry = new PLANNED_NOENTRY();
                plannedNoEntry.APPOINTMENTID = appointment.appointmentId;
                plannedNoEntry.RecordedOn = appointment.appointmentEndDateTime;
                plannedNoEntry.Notes = appointment.noEntryNotes;
                if (appointment.noEntryNotes == null || appointment.noEntryNotes == string.Empty)
                    plannedNoEntry.Notes = "Job completed because of No Entry";
                context.AddToPLANNED_NOENTRY(plannedNoEntry);
                //2.) UPDATE PLANNED_JOBTIMESHEET SET JobEndDate/Time
                // 3.) UPDATE PLANNED_APPOINTMENT SET
                //AppointmentStatus=’Complete’, JournalSubstatus= ‘No Entry’, Notes=’Job
                //completed because of No Entry’
                //4.) INSERT into PLANNED_APPOINTMENTS_HISTORY
                //5.) Check if there are any other JSN’s or appointments which are (‘Not
                //Started’ or ‘InProgress’)
                //5a) If records found in point 5, then repeat steps 2,3 and 4 for each JSN
                if (callfromAPIV2) insertUpdatePlannedJobTimeSheet(appointment.appointmentId, appointment.repairCompletionDateTime ?? appointment.componentTrade.completionDate, AppointmentCompleteStatus.NoEntry.ToString());
                else saveUpdatePlannedJobTimeSheet(appointment.appointmentId, appointment.appointmentStartDateTime, appointment.appointmentEndDateTime);

                this.updatePlannedAppointment(appointment.appointmentId, appointment.appointmentNotes, appointment.componentTrade.JSNNotes, appointment.appointmentEndDateTime, AppointmentCompleteStatus.Complete.ToString(), "No Entry".ToString());
                List<PLANNED_APPOINTMENTS> appointmentList = new List<PLANNED_APPOINTMENTS>();
                appointmentList = checkPlannedAppointmentByJournalId(appointment.journalId);
                if (appointmentList.Count() > 0)
                {
                    foreach (PLANNED_APPOINTMENTS pAppointment in appointmentList)
                    {
                        if (callfromAPIV2) insertUpdatePlannedJobTimeSheet(appointment.appointmentId, appointment.repairCompletionDateTime ?? appointment.componentTrade.completionDate, AppointmentCompleteStatus.NoEntry.ToString());
                        else saveUpdatePlannedJobTimeSheet(pAppointment.APPOINTMENTID, appointment.appointmentStartDateTime, appointment.appointmentEndDateTime);

                        this.updatePlannedAppointment(pAppointment.APPOINTMENTID, appointment.appointmentNotes, appointment.componentTrade.JSNNotes, appointment.appointmentEndDateTime, AppointmentCompleteStatus.Complete.ToString(), "No Entry".ToString());
                    }
                }
                success = true;
                if (success == true)
                {
                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    trans.Complete();
                }
            }

            resultBoolData.result = success;
            return resultBoolData;
        }

        public bool isPlannedCancelled(int journalId)
        {
            int cancelledStatusID = GetPlannedStatusID("Cancelled");

            PLANNED_JOURNAL appointment = context.PLANNED_JOURNAL.Where(fltl => fltl.JOURNALID == journalId).First();
            if (appointment.STATUSID == cancelledStatusID)
                return true;
            else
                return false;
        }

        public int GetPlannedStatusID(string status)
        {
            var fltStatusList = context.PLANNED_STATUS.Where(fltSts => fltSts.TITLE.Replace(" ", "") == status.Replace(" ", ""));
            if (fltStatusList.Count() == 0)
                return 0;

            PLANNED_STATUS fltStatus = fltStatusList.First();

            return fltStatus.STATUSID;
        }

        public int GetPlannedSubStatusID(string status)
        {
            var fltStatusList = context.PLANNED_SUBSTATUS.Where(fltSts => fltSts.TITLE.Replace(" ", "") == status.Replace(" ", ""));
            if (fltStatusList.Count() == 0)
                return 0;

            PLANNED_SUBSTATUS fltStatus = fltStatusList.First();

            return fltStatus.SUBSTATUSID;
        }

        #region update planned appointment data

        /// <summary>
        /// This function saves the planned Log History Data in the database
        /// </summary>       
        /// <returns>the true if successfully save otherwise false</returns>
        public Nullable<int> updatePlannedAppointment(int appointmentId, string appointmentNotes, string customerNotes, DateTime? appointmentEndDateTime, string appStatus, string subStatus)
        {
            Nullable<int> appointmentHistoryID = null;

            int subStatusCode = GetPlannedSubStatusID(subStatus);
            var appData = context.PLANNED_APPOINTMENTS.Where(app => app.APPOINTMENTID == appointmentId);
            if (appData.Count() > 0)
            {
                PLANNED_APPOINTMENTS planned = appData.First();
                planned.APPOINTMENTSTATUS = appStatus;
                planned.JOURNALSUBSTATUS = subStatusCode;
                if (appStatus == AppointmentCompleteStatus.Complete.ToString())
                {
                    planned.CompletionNotes = customerNotes;
                }

                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                appointmentHistoryID = context.PLANNED_APPOINTMENTS_HISTORY.Where(appHistory => appHistory.APPOINTMENTID == appointmentId).Max(appHistory => appHistory.APPOINTMENTHISTORYID);
            }
            return appointmentHistoryID;
        }

        #endregion

        #region save Update Planned Job TimeSheet
        /// <summary>
        /// This function save or Update Planned Job TimeSheet in the database
        /// </summary>      
        /// <returns>the true if successfully save otherwise false </returns>
        public bool saveUpdatePlannedJobTimeSheet(int appointmentId, DateTime? startTime, DateTime? endTime)
        {
            bool result = false;

            //using (TransactionScope trans = new TransactionScope())
            //{
            var appData = context.PLANNED_JOBTIMESHEET.Where(app => app.APPOINTMENTID == appointmentId);
            if (appData.Count() > 0)
            {
                PLANNED_JOBTIMESHEET planned = appData.First();
                planned.EndTime = endTime;
            }
            else
            {
                PLANNED_JOBTIMESHEET planned = new PLANNED_JOBTIMESHEET();
                planned.APPOINTMENTID = appointmentId;
                planned.StartTime = startTime;
                planned.EndTime = endTime;
                context.AddToPLANNED_JOBTIMESHEET(planned);
            }
            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            //trans.Complete();
            result = true;
            //}
            return result;
        }
        #endregion

        #region save planned journal history
        /// <summary>
        /// save planned journal history
        /// </summary>
        /// <param name="journalId"></param>
        /// <returns></returns>
        public bool savePlannedJournalistoryData(int? updatedby, int? journalId, string status)
        {
            bool result = false;
            int plannedStatusId = GetPlannedStatusID(status);
            PLANNED_JOURNAL pjournal = new PLANNED_JOURNAL();
            var journalData = context.PLANNED_JOURNAL.Where(jor => jor.JOURNALID == journalId);
            if (journalData.Count() > 0)
            {
                //using (TransactionScope trans = new TransactionScope())
                //{
                pjournal = journalData.First();
                pjournal.STATUSID = (short)plannedStatusId;
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                PLANNED_JOURNAL_HISTORY pJournalHistory = new PLANNED_JOURNAL_HISTORY();
                pJournalHistory.JOURNALID = pjournal.JOURNALID;
                pJournalHistory.PROPERTYID = pjournal.PROPERTYID;
                pJournalHistory.COMPONENTID = pjournal.COMPONENTID;
                pJournalHistory.STATUSID = pjournal.STATUSID;
                pJournalHistory.ACTIONID = pjournal.ACTIONID;
                pJournalHistory.CREATEDBY = updatedby;
                pJournalHistory.CREATIONDATE = DateTime.Now;
                context.AddToPLANNED_JOURNAL_HISTORY(pJournalHistory);
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                //trans.Complete();
                result = true;
                //}
            }
            return result;

        }
        #endregion

        #region check planned job started or not
        /// <summary>
        /// This function return jobtime sheet id 
        /// </summary>      
        /// <returns>the TimeSheetID if exist otherwise 0 </returns>

        public int isPlannedJobStarted(int appointmentId)
        {
            int result = 0;

            var appData = context.PLANNED_JOBTIMESHEET.Where(app => app.APPOINTMENTID == appointmentId);
            if (appData.Count() > 0)
            {
                PLANNED_JOBTIMESHEET planned = appData.First();
                result = planned.TimeSheetID;

            }

            return result;
        }
        #endregion

        #region check Planned Appointment By PMO

        /// <summary>
        /// This function return appointment List 
        /// </summary>      
        /// <returns>the TimeSheetID if exist otherwise 0 </returns>
        public List<PLANNED_APPOINTMENTS> checkPlannedAppointmentByPMO(int? journalId)
        {
            var cancelledStatues = new List<string>(){                       
                         AppointmentCompleteStatus.Cancelled.ToString().ToLower()
                        ,AppointmentCompleteStatus.Cancel.ToString().ToLower()
                    };

            List<PLANNED_APPOINTMENTS> appointmentList = new List<PLANNED_APPOINTMENTS>();
            var appData = context.PLANNED_APPOINTMENTS.Where(app => app.JournalId == journalId &&
                            !(cancelledStatues.Contains(app.APPOINTMENTSTATUS.ToLower()))
                          );
            if (appData.Count() > 0)
            {
                appointmentList = appData.ToList();
            }

            return appointmentList;
        }

        #endregion

        #region check Planned Appointment By JournalId
        /// <summary>
        /// This function return appointment List 
        /// </summary>       

        public List<PLANNED_APPOINTMENTS> checkPlannedAppointmentByJournalId(int? journalId)
        {

            List<PLANNED_APPOINTMENTS> appointmentList = new List<PLANNED_APPOINTMENTS>();
            var appData = context.PLANNED_APPOINTMENTS.Where(app => app.JournalId == journalId && (app.APPOINTMENTSTATUS == "In Progress" || app.APPOINTMENTSTATUS == "NotStarted"));
            if (appData.Count() > 0)
            {
                appointmentList = appData.ToList();

            }

            return appointmentList;
        }
        #endregion

        #endregion

        #region Complete Appointment For Planned

        public bool completeAppointmentForPlanned(UpdateAppointmentData appointment, bool callfromAPIV2 = false)
        {
            if (appointment.appointmentStatus == AppointmentCompleteStatus.InProgress.ToString())
            {
                if (appointment.componentTrade.jobStatus == AppointmentCompleteStatus.InProgress.ToString() || appointment.componentTrade.jobStatus == AppointmentCompleteStatus.Paused.ToString())
                {
                    if (!callfromAPIV2) saveUpdatePlannedJobTimeSheet(appointment.appointmentId, appointment.appointmentStartDateTime, appointment.appointmentEndDateTime);
                }
                if (!callfromAPIV2) this.updatePlannedAppointment(appointment.appointmentId, appointment.appointmentNotes, appointment.componentTrade.JSNNotes, appointment.appointmentEndDateTime, AppointmentCompleteStatus.InProgress.ToString(), appointment.componentTrade.jobStatus);
            }

            else if (appointment.appointmentStatus == AppointmentCompleteStatus.Complete.ToString())
            {
                this.updatePlannedAppointment(appointment.appointmentId, appointment.appointmentNotes, appointment.componentTrade.JSNNotes, appointment.appointmentEndDateTime, AppointmentCompleteStatus.Complete.ToString(), AppointmentCompleteStatus.Complete.ToString());
                if (!callfromAPIV2) saveUpdatePlannedJobTimeSheet(appointment.appointmentId, appointment.appointmentStartDateTime, appointment.appointmentEndDateTime);
                List<PLANNED_APPOINTMENTS> appointmentList = new List<PLANNED_APPOINTMENTS>();
                appointmentList = checkPlannedAppointmentByPMO(appointment.journalId);

                int appointmentListCount = appointmentList.Count();
                if (appointmentListCount >= 1)
                {
                    int inCompleteStatusCount = 0;
                    int miscellaneousAppointmentCount = 0;
                    foreach (PLANNED_APPOINTMENTS plannedApp in appointmentList)
                    {
                        if (plannedApp.APPOINTMENTSTATUS != AppointmentCompleteStatus.Complete.ToString())
                            inCompleteStatusCount = inCompleteStatusCount + 1;
                        if (plannedApp.isMiscAppointment)
                            miscellaneousAppointmentCount = miscellaneousAppointmentCount + 1;
                    }
                    // Check whether there is one (or more) inComplete Appointments, before calling journal completion process.
                    if (inCompleteStatusCount == 0)
                    {
                        bool isMiscJournal = false;
                        if (miscellaneousAppointmentCount == appointmentListCount)
                        { isMiscJournal = true; }
                        completePlannedJournal(appointment, isMiscJournal);
                    }
                }

                if (appointment.appointmentType == PropSurvey.Contracts.Data.Appointment.AppointmentTypes.Adaptation.ToString())
                {
                    if (appointment.componentTrade.locationId != null && appointment.componentTrade.adaptationId != null
                        && appointment.componentTrade.locationId > 0 && appointment.componentTrade.adaptationId > 0)
                    {
                        int parameterId = (int)appointment.componentTrade.locationId;
                        int valueId = (int)appointment.componentTrade.adaptationId;
                        string adaptation = appointment.componentTrade.adaptation;
                        int createdBy = (int)appointment.createdBy;
                        updatePropertyAttributesArea(appointment.property.propertyId, parameterId, valueId, adaptation, createdBy);
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// This function gets the component id
        /// </summary>
        /// <returns></returns>
        public int getcycleByComponentId(int? componentId)
        {
            int cycle = 0;
            var component = context.PLANNED_COMPONENT.Where(item => item.COMPONENTID == componentId);
            if (component.Count() > 0)
            {
                PLANNED_COMPONENT comp = component.First();
                cycle = (int)comp.CYCLE;
                if (comp.FREQUENCY == "yrs")
                    cycle = (int)comp.CYCLE * 12;

            }
            return cycle;
        }

        /// <summary>
        /// update Planned Journal
        /// </summary>
        /// <param name="appointment"></param>
        /// <param name="isMiscJournal"></param>
        /// <returns></returns>
        public bool completePlannedJournal(UpdateAppointmentData appointment, bool isMiscJournal)
        {

            this.savePlannedJournalistoryData(appointment.updatedBy, appointment.journalId, AppointmentCompleteStatus.Completed.ToString());
            if (!isMiscJournal)
            {
                #region Update Last Replaced and Replacement Due for Planned Components based of Componenet Replacement Cycle
                int cycle = getcycleByComponentId(appointment.componentTrade.componentId);
                DateTime? replacementDue = appointment.componentTrade.completionDate.Value.AddMonths(cycle);
                PA_PROPERTY_ITEM_DATES itemDates = new PA_PROPERTY_ITEM_DATES();
                var appData = context.PA_PROPERTY_ITEM_DATES.Where(prop => prop.PROPERTYID == appointment.property.propertyId && prop.PLANNED_COMPONENTID == appointment.componentTrade.componentId);
                if (appData.Count() > 0)
                {
                    itemDates = appData.First();
                    itemDates.LastDone = appointment.componentTrade.completionDate;
                    itemDates.DueDate = replacementDue;
                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                }
                #endregion
            }

            #region Set Condition rating for the item/component to satisfactory

            int attributeId = -1;
            #region Get arrtibuteId for current Journal

            PLANNED_CONDITIONWORKS cw;

            var conditionWorks = context.PLANNED_CONDITIONWORKS.Where(cws => cws.JournalId == appointment.journalId);

            if (conditionWorks.Count() > 0)
            {
                cw = conditionWorks.First();
                attributeId = cw.AttributeId;
            }

            #endregion

            #region Set Attribute/Parameter value for given attribueId (Conditon Rating) to Satisfactory.
            //TODO: May need to check for case where one item is mapped to two or more components.
            if (attributeId > 0)
            {
                var PAs = from pa in context.PA_PROPERTY_ATTRIBUTES
                          join ip in context.PA_ITEM_PARAMETER on pa.ITEMPARAMID equals ip.ItemParamID
                          join pv in context.PA_PARAMETER_VALUE on ip.ParameterId equals pv.ParameterID
                          where pa.ATTRIBUTEID == attributeId && pv.ValueDetail.Equals("Satisfactory")
                          select new
                          {
                              pa,
                              pv
                          };
                if (PAs.Count() > 0)
                {
                    var PropAttrib = PAs.First();
                    PA_PROPERTY_ATTRIBUTES pa = PropAttrib.pa;
                    pa.VALUEID = PropAttrib.pv.ValueID;
                    pa.PARAMETERVALUE = PropAttrib.pv.ValueDetail;
                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                }
            }
            #endregion

            #endregion

            return true;
        }

        #endregion

        #region update Property Attributes Area
        /// <summary>
        /// update Property Attributes Area
        /// </summary>
        /// <param name="propertyId"></param>
        /// <param name="parameterId"></param>
        /// <param name="valueId"></param>
        /// <param name="adaptation"></param>
        /// <param name="createdBy"></param>
        public void updatePropertyAttributesArea(string propertyId, int parameterId, int valueId, string adaptation, int createdBy)
        {
            PA_PROPERTY_ATTRIBUTES paAttribItem = new PA_PROPERTY_ATTRIBUTES();
            var itemPrameter = (from itp in context.PA_ITEM_PARAMETER
                                join par in context.PA_PARAMETER on itp.ParameterId equals par.ParameterID
                                where par.ParameterID == parameterId && itp.IsActive == true && par.IsActive == true
                                orderby par.ParameterSorder ascending
                                select itp).ToList();

            if (itemPrameter.Count() > 0)
            {
                int itemParamId = itemPrameter.First().ItemParamID;

                var paAttrib = context.PA_PROPERTY_ATTRIBUTES.Where(prop => prop.PROPERTYID == propertyId && prop.ITEMPARAMID == itemParamId && prop.VALUEID == valueId);

                if (paAttrib.Count() > 0)
                {
                    //fetch the already existed record
                    paAttribItem = paAttrib.First();
                }
                else
                {
                    //if record does not already exist the set the property id and itemparamid
                    paAttribItem.PROPERTYID = propertyId;
                    paAttribItem.ITEMPARAMID = itemParamId;
                    paAttribItem.VALUEID = valueId;
                    paAttribItem.PARAMETERVALUE = adaptation;
                }
                paAttribItem.IsCheckBoxSelected = true;
                paAttribItem.UPDATEDBY = createdBy;
                paAttribItem.UPDATEDON = DateTime.Now;

                if (paAttrib.Count() <= 0)
                {
                    //insert the record
                    context.AddToPA_PROPERTY_ATTRIBUTES(paAttribItem);
                }

                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            }
        }
        #endregion

        #region New Complete Appointment Function(s)

        #region save Complete Appointment for gas V2
        /// <summary>
        /// this function calls the method for Gas. and update (appointment Data,SurveyData,Customer data,No entry data in case of "NoEntry" Status,
        /// Journal data,Lgsr data and populate CP12Document 
        /// </summary>
        /// <param name="appointment">The object of appointment</param>
        /// <returns>It returns true or false in update is successful</returns>
        public bool completeAppointmentForGasV2(UpdateAppointmentData appointment)
        {
            bool isSuccess = false;

            try
            {
                bool responseJournalUpdate = false;
                using (TransactionScope trans = new TransactionScope())
                {
                    FaultsDal faultDal = new FaultsDal();
                    //Update Appointment Data
                    completeAppointmentForGas(appointment);

                    if (appointment.property != null)
                    {
                        AppliancesDal appliancesDal = new AppliancesDal();
                        InstallationPipeworkDal insWorkDal = new InstallationPipeworkDal();

                        #region Add/Update Appliance(s), add appliance(s) inspection, add appliance(s) defects (if any)
                        foreach (ApplianceData appData in appointment.property.appliances)
                        {
                            #region Add/Update current Appliance

                            AppliancesDal appDal = new AppliancesDal();
                            if (appData.ApplianceType != null && appData.ApplianceType.ApplianceType != null)
                            {
                                appData.ApplianceType.ApplianceTypeID = appDal.saveApplianceType(appData.ApplianceType, context);
                            }
                            if (appData.ApplianceLocation != null && appData.ApplianceLocation.Location != null)
                            {
                                appData.ApplianceLocation.LocationID = appDal.saveApplianceLocation(appData.ApplianceLocation, context);
                            }
                            if (appData.ApplianceManufacturer != null && appData.ApplianceManufacturer.Manufacturer != null)
                            {
                                appData.ApplianceManufacturer.ManufacturerID = appDal.saveApplianceManufacturer(appData.ApplianceManufacturer, context);
                            }
                            if (appData.ApplianceModel != null && appData.ApplianceModel.ApplianceModel != null)
                            {
                                appData.ApplianceModel.ApplianceModelID = appDal.saveApplianceModel(appData.ApplianceModel, context);
                            }
                            appData.ApplianceID = appDal.saveAppliance(appData, context);

                            #endregion

                            #region Add Appliance Inspection current appliance

                            if (appData.ApplianceInspection != null && appData.ApplianceInspection.inspectionDate != null)
                            {
                                appData.ApplianceInspection.APPLIANCEID = appData.ApplianceID;
                                appData.ApplianceInspection.JOURNALID = appointment.journalId;
                                appliancesDal.saveApplianceInspectionGas(appData.ApplianceInspection, context);
                            }

                            #endregion

                            #region Add Appliance Defects current appliance

                            if (appData.ApplianceDefects != null && appData.ApplianceDefects.Count > 0)
                            {
                                foreach (FaultsDataGas appDefectData in appData.ApplianceDefects)
                                {
                                    appDefectData.ApplianceID = appData.ApplianceID;
                                    faultDal.updateFaultDataGas(appDefectData, context);
                                }
                            }

                            #endregion
                        }
                        #endregion

                        #region Update detector(s) Count, add detector(s) Inspection and add detector(s) defects
                        foreach (DetectorCountData detector in appointment.property.detectors)
                        {
                            appliancesDal.updateDetectorCount(appointment.property.propertyId, detector, appointment.updatedBy, context);
                            if (detector.detectorDefects != null && detector.detectorDefects.Count > 0)
                            {
                                foreach (FaultsDataGas appDefectData in detector.detectorDefects)
                                {
                                    faultDal.updateFaultDataGas(appDefectData, context);
                                }
                            }
                            if (detector.detectorInspection != null && detector.detectorTypeId > 0)
                            {
                                detector.detectorInspection.inspectionID = appliancesDal.addUpdateDetectorInfo(detector.detectorInspection
                                        , detector.detectorTypeId, detector.isInspected, appointment.journalId, appointment.property.propertyId, context).result;
                            }
                        }
                        #endregion

                        if (appointment.property.installationpipework != null && appointment.property.installationpipework.inspectionDate != null)
                            insWorkDal.saveInstallationPipeworkGas(appointment, context);
                    }
                    // Update property and customer details for this appointment
                    updatePropertyAndCustomerDetails(appointment);

                    if (appointment.appointmentStatus == "No Entry")
                    {
                        saveNoEntryForGas(appointment);
                    }
                    else
                    {
                        if (appointment.CP12Info != null)
                        {
                            updateLgsrData(appointment);
                        }
                    }
                    if (appointment.appointmentStatus == AppointmentCompleteStatus.Finished.ToString())
                    {
                        responseJournalUpdate = UpdateAndInsertJournal(appointment);
                    }
                    trans.Complete();
                }
                context.AcceptAllChanges();
                isSuccess = true;
                if (responseJournalUpdate)
                {
                    try
                    {
                        DocumentHelper.populateCp12Document(appointment.journal.propertyId, appointment.journal.journalId.ToString());
                    }
                    catch (Exception ex)
                    {
                        Logger.Write(ex.ToString());
                    }
                }

                return isSuccess;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                Logger.Write(entityexception.ToString());
                throw;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                Logger.Write(ex.ToString());
                throw ex;
            }
        }

        #endregion

        #region save Complete appointment for stock V2
        /// <summary>
        /// this function calls the method for Stock. and update (appointment Data,SurveyData and Customer data
        /// </summary>
        /// <param name="appointment">The object of appointment</param>
        /// <returns>It returns true or false in update is successful</returns>
        public bool completeAppointmentForStockV2(UpdateAppointmentData appointment)
        {
            bool isSuccessful = false;
            try
            {
                SurveyDal surveyDal = new SurveyDal();
                using (TransactionScope trans = new TransactionScope())
                {
                    //Update Appointment Data
                    completeAppointmentForStock(appointment);

                    SurveyData appSurveyData = new SurveyData();
                    if (appointment.Survey != null && appointment.Survey.surveyData != null)
                    {
                        foreach (SurveyData survData in appointment.Survey.surveyData)
                        {
                            survData.completedBy = appointment.updatedBy ?? 0;
                            string propertyId = survData.propertyId;
                            int customerId = survData.customerId;
                            int appointmentId = survData.appointmentId;
                            bool success = false;

                            bool isAppointmentExist = surveyDal.isAppointmentForPropertyExist(propertyId, appointmentId, context);

                            //if appointment does not exist then this is error
                            if (isAppointmentExist == false)
                            {
                                ErrorFaultSetGet.setErrorFault(MessageConstants.AppointmentIdDoesNotExistForPropertyMsg, true, MessageCodesConstants.PropertyOrCustomerOrAppointmentDoesNotExistCode);
                                throw new ArgumentException(String.Format(MessageConstants.AppointmentIdDoesNotExistForPropertyMsg, propertyId, customerId.ToString(), appointmentId.ToString()), "propertyId or customerId or appointmentId");
                            }
                            else
                            {
                                success = surveyDal.saveSurveyForm(survData, context);
                            }
                        }
                    }
                    // Update property and customer details for this appointment
                    updatePropertyAndCustomerDetails(appointment);
                    trans.Complete();
                }
                context.AcceptAllChanges();
                isSuccessful = true;

            }
            catch (EntityException entityexception)
            {
                Logger.Write(entityexception.ToString());
                ErrorFaultSetGet.setErrorFault(entityexception.Message, true, MessageCodesConstants.EntitiyExceptionCode);
                throw;
            }
            catch (Exception ex)
            {
                Logger.Write(ex.ToString());
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw;
            }
            return isSuccessful;
        }
        #endregion

        #region save complete Fault appointment V2

        /// <summary>
        /// save complete Fault appointment V2
        /// </summary>
        /// <param name="appointment"></param>
        /// <returns></returns>
        public bool completeAppointmentForFaultV2(UpdateAppointmentData appointment)
        {
            bool isSuccessful = false;
            try
            {
                using (TransactionScope trans = new TransactionScope())
                {
                    if (appointment.jobDataList != null)
                    {
                        foreach (JobData jobData in appointment.jobDataList)
                        {
                            //Check job pause data exist
                            if (jobData.jobPauseHistory.Count > 0)
                            {
                                jobData.jobPauseHistory = jobData.jobPauseHistory.OrderBy(i => i.pauseDate).ToList();
                                foreach (JobPauseHistoryData jobPauseData in jobData.jobPauseHistory)
                                {
                                    //Step 1
                                    /*
                                     * Apply condition to save in faultPause only in case of a pause.                                     
                                     * Update FaultLog Status and add an entry in FaultLogHistory with time stamp (pauseDate).                                     * and remove the above process for other code places like inside appointment update functions.
                                     * for example: move/remove from function PausedAppointmentForFault
                                     * save fault paused history                                     
                                     */
                                    Nullable<int> faultLogHistoryId = null;
                                    faultLogHistoryId = this.updateFaultStatusId(context, jobData.faultLogID, jobPauseData.actionType, appointment.createdBy, jobPauseData.pauseDate);

                                    //Step 2
                                    /*
                                     * Apply condition to save in faultPause only in case of a pause.                                     
                                     */
                                    Nullable<int> pauseID = null;
                                    if (jobPauseData.actionType == FaultJobProgressStatus.Paused.ToString())
                                        pauseID = saveFaultPaused(jobPauseData, jobData.faultLogID, faultLogHistoryId); // Exsisting implementation.                                    

                                    //Step 3
                                    /*
                                     * update FaultTIMESheet, in case of start/resume(restart) add new record for causes of pause/noentry/complete update latest previous record and update timesheet end time.
                                     * for noentry there may be a case, that the noentry is occoured at first step no data in faultTimeSheet will be recorded.
                                     * The process for the noentry will be implemented in noEntryFaultAppointment (at least for current process and service data).                                     
                                     */
                                    this.saveUpdateFaultTimeSheet(appointment.appointmentId, jobData.faultLogID, jobPauseData.pauseDate, jobPauseData.actionType, pauseID);

                                    //Step 4
                                    /*
                                     * For the case job is completed add repairs for current fault.
                                     * 
                                     */
                                    if (jobPauseData.actionType == FaultJobProgressStatus.Complete.ToString())
                                    {
                                        //check repair list exist
                                        if (jobData.faultRepairList.Count > 0)
                                        {
                                            saveFaultRepairList(jobData, appointment.assignedTo);
                                        }

                                        bool isAppointmentCompleted = false;
                                        if (appointment.appointmentStatus == AppointmentCompleteStatus.Complete.ToString())
                                        {
                                            isAppointmentCompleted = true;
                                        }
                                        if (jobData.completionDate == null)
                                            jobData.completionDate = DateTime.Now;

                                        System.Data.Objects.ObjectParameter result1 = new ObjectParameter("result", typeof(int));
                                        context.FL_CO_COMPLETE_APPOINTMENT(appointment.appointmentId, jobData.faultLogID, jobData.followOnNotes, jobData.repairNotes,
                                            jobData.completionDate, isAppointmentCompleted, jobData.completionDate, result1);
                                    }
                                }
                            }
                        }
                    }
                    if (appointment.appointmentStatus != "No Entry"
                                     || appointment.appointmentStatus.Replace(" ", "") != AppointmentCompleteStatus.NoEntry.ToString())
                    {
                        PausedAppointmentForFault(null, appointment);
                    }
                    else if (appointment.appointmentStatus == "No Entry")
                    {
                        noEntryFaultAppointment(appointment);
                    }
                    // Update property and customer details for this appointment
                    updatePropertyAndCustomerDetails(appointment);
                    context.AcceptAllChanges();
                    trans.Complete();
                    isSuccessful = true;
                }
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }

            return isSuccessful;
        }

        #endregion

        #region save complete Planned appointment V2

        public bool completeAppointmentForPlannedV2(UpdateAppointmentData appointment)
        {
            bool isSuccessful = false;
            try
            {
                using (TransactionScope trans = new TransactionScope())
                {
                    if (appointment.componentTrade != null)
                    {
                        if (appointment.componentTrade.jobPauseHistory != null && appointment.componentTrade.jobPauseHistory.Count > 0)
                        {
                            //Check job pause data exist
                            appointment.componentTrade.jobPauseHistory = appointment.componentTrade.jobPauseHistory.OrderBy(i => i.pauseDate).ToList();
                            foreach (JobPauseHistoryData jobPauseData in appointment.componentTrade.jobPauseHistory)
                            {
                                Nullable<int> appointmentHistoryID = null;

                                // update planned appointment with given data.
                                if (appointment.appointmentStatus == AppointmentCompleteStatus.Complete.ToString() && jobPauseData.actionType == AppointmentCompleteStatus.Complete.ToString()) completeAppointmentForPlanned(appointment, callfromAPIV2: true);
                                else appointmentHistoryID = updatePlannedAppointment(appointment.appointmentId, appointment.appointmentNotes, appointment.componentTrade.JSNNotes, appointment.appointmentEndDateTime, AppointmentCompleteStatus.InProgress.ToString(), jobPauseData.actionType);

                                // save fault paused history
                                Nullable<int> pauseID = null;
                                if (jobPauseData.actionType.ToLower() == AppointmentCompleteStatus.Paused.ToString().ToLower())
                                    pauseID = savePlannedPaused(jobPauseData, appointment.appointmentId, appointmentHistoryID);

                                // Insert update a record in Planned Job Sheet
                                insertUpdatePlannedJobTimeSheet(appointment.appointmentId, jobPauseData.pauseDate, jobPauseData.actionType, pauseID);
                            }
                        }
                        else
                        {
                            updatePlannedAppointment(appointment.appointmentId, appointment.appointmentNotes, appointment.componentTrade.JSNNotes, appointment.appointmentEndDateTime, appointment.appointmentStatus, AppointmentCompleteStatus.NotStarted.ToString());
                        }
                    }
                    if (appointment.appointmentStatus.Replace(" ", "") == AppointmentCompleteStatus.NoEntry.ToString())
                    {
                        noEntryPlannedAppointment(appointment, callfromAPIV2: true);
                    }
                    // Update property and customer details for this appointment
                    updatePropertyAndCustomerDetails(appointment);
                    trans.Complete();
                }
                context.AcceptAllChanges();
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                Logger.Write(entityexception.ToString());
                throw;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                Logger.Write(ex.ToString());
                throw;
            }
            isSuccessful = true;
            return isSuccessful;
        }

        #endregion

        #region update property and custumer details (for completeappointment call)
        /// <summary>
        /// update property and custumer details (for completeappointment call)
        /// </summary>
        /// <param name="appointment"></param>
        protected void updatePropertyAndCustomerDetails(UpdateAppointmentData appointment)
        {
            if (appointment.customerList != null)
            {
                foreach (CustomerData customer in appointment.customerList)
                {
                    CustomerDal custDal = new CustomerDal();
                    custDal.UpdateCustomerContactFromAppointment(customer, context);
                }
            }

            if (appointment.property != null)
            {
                PropertyDal propDal = new PropertyDal();

                #region Add/Update Property Accommodations

                if (appointment.property.Accommodations != null)
                {
                    propDal.updatePropertyDimensions(appointment.property.Accommodations, appointment.property.propertyId, context);
                }
                #endregion

                #region Add/Update Property Detectors

                if (appointment.property.propertyDetectors != null && appointment.appointmentStatus == AppointmentCompleteStatus.Complete.ToString())
                {
                    foreach (PropertyDetector detector in appointment.property.propertyDetectors)
                    {
                        saveDetector(detector, context);
                    }
                }

                #endregion

            }
        }
        #endregion

        #region Save (insert/update) detector

        public bool saveDetector(PropertyDetector detector, PropertySurvey_Entities currentContext = null)
        {
            bool saveStatus = false;
            bool addNewDetector = true;

            P_DETECTOR dbDetector = new P_DETECTOR();

            #region Check for existing detector, if exists get its context reference.

            if (detector.detectorId > 0)
            {
                var detctorsQueryResult = from d in context.P_DETECTOR
                                          where d.DetectorId == detector.detectorId
                                          select d;

                if (detctorsQueryResult.Count() > 0)
                {
                    dbDetector = detctorsQueryResult.First();
                    addNewDetector = false;
                }
            }

            #endregion

            #region Set/Update dector values for detector object

            if (detector.detectorTypeId != null)
                dbDetector.DetectorTypeId = (int)detector.detectorTypeId;
            if (detector.installedDate != null)
                dbDetector.InstalledDate = (DateTime)detector.installedDate;
            if (detector.isLandlordsDetector != null)
                dbDetector.IsLandlordsDetector = (bool)detector.isLandlordsDetector;

            dbDetector.Location = detector.location;
            dbDetector.Manufacturer = detector.manufacturer;
            if (detector.powerTypeId != null)
                dbDetector.PowerSource = (int)detector.powerTypeId;
            dbDetector.PropertyId = detector.propertyId;
            dbDetector.SerialNumber = detector.serialNumber;

            #endregion

            if (addNewDetector)
            {
                dbDetector.InstalledBy = detector.installedBy;
                context.P_DETECTOR.AddObject(dbDetector);
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            }

            context.SaveChanges();

            return saveStatus;
        }

        #endregion

        #region Update Fault Time Sheet

        /// <summary>
        /// Update Fault Time Sheet
        /// </summary>
        /// <param name="appointment"></param>
        private bool saveUpdateFaultTimeSheet(int appointmentId, int faultLogID, DateTime? pauseDate, string actionType, Nullable<int> pauseID = null)
        {
            FL_FAULT_JOBTIMESHEET faultJobTimeSheetData = new FL_FAULT_JOBTIMESHEET();
            if (actionType.ToLower() == FaultAppointmentProgressStatus.InProgress.ToString().ToLower())
            {
                faultJobTimeSheetData.APPOINTMENTID = appointmentId;
                faultJobTimeSheetData.FaultLogId = faultLogID;
                faultJobTimeSheetData.StartTime = pauseDate;
                context.AddToFL_FAULT_JOBTIMESHEET(faultJobTimeSheetData);
            }
            else
            {
                var faultJobTimeSheetList = context.FL_FAULT_JOBTIMESHEET.Where(flt => flt.FaultLogId == faultLogID && flt.EndTime == null).OrderByDescending(flt => flt.TimeSheetID);
                if (faultJobTimeSheetList.Count() > 0)
                {
                    faultJobTimeSheetData = faultJobTimeSheetList.First();
                    faultJobTimeSheetData.EndTime = pauseDate;
                    faultJobTimeSheetData.PauseID = pauseID;
                }
            }
            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            return true;
        }

        #endregion

        #region save Update Planned Job TimeSheet
        /// <summary>
        /// This function save or Update Planned Job TimeSheet in the database
        /// </summary>      
        /// <returns>the true if successfully save otherwise false </returns>
        public bool insertUpdatePlannedJobTimeSheet(int appointmentId, DateTime? actionDateTime, string actionType, Nullable<int> pauseID = null)
        {
            bool result = false;

            if (actionType.ToLower() == AppointmentCompleteStatus.InProgress.ToString().ToLower())
            {
                PLANNED_JOBTIMESHEET planned = new PLANNED_JOBTIMESHEET();
                planned.APPOINTMENTID = appointmentId;
                planned.StartTime = actionDateTime;
                planned.EndTime = null;
                context.AddToPLANNED_JOBTIMESHEET(planned);
            }
            else
            {
                var appData = context.PLANNED_JOBTIMESHEET.Where(app => app.APPOINTMENTID == appointmentId && app.EndTime == null).OrderByDescending(js => js.StartTime);
                if (appData.Count() > 0)
                {
                    PLANNED_JOBTIMESHEET planned = appData.First();
                    planned.EndTime = actionDateTime;
                    planned.PauseID = pauseID;
                }
            }
            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            result = true;

            return result;
        }
        #endregion

        #endregion

        #region Get All Scheme/Block Fault Appointments By User

        public List<AllAppointmentsList> getAllSchemeBlockFaultAppointmentsByUser(string userName, string startDate, string endDate)
        {
            //Create a list of strings from Enums as Linq do not support/Recognize ToString() funnction.
            List<string> completedStatusesList = new List<string>() {AppointmentCompleteStatus.Finished.ToString(),
                                        AppointmentCompleteStatus.Complete.ToString(),
                                        AppointmentCompleteStatus.Completed.ToString(),
                                        AppointmentCompleteStatus.NoEntry.ToString()};

            DateTime varStartTime = DateTime.Parse(startDate);
            DateTime varEndTime = DateTime.Parse(endDate);

            List<AllAppointmentsList> faultAppointments = new List<AllAppointmentsList>();

            var _var = (from app in context.FL_CO_APPOINTMENT
                        join fltAppt in context.FL_FAULT_APPOINTMENT on app.AppointmentID equals fltAppt.AppointmentId
                        join fltl in context.FL_FAULT_LOG on fltAppt.FaultLogId equals fltl.FaultLogID
                        join flts in context.FL_FAULT_STATUS on fltl.StatusID equals flts.FaultStatusID
                        join creat in context.AC_LOGINS on fltl.UserId equals creat.EMPLOYEEID
                        join login in context.AC_LOGINS on app.OperativeID equals login.EMPLOYEEID
                        join emp in context.E__EMPLOYEE on login.EMPLOYEEID equals emp.EMPLOYEEID
                        join employee in context.E__EMPLOYEE on creat.EMPLOYEEID equals employee.EMPLOYEEID
                        join schemes in context.P_SCHEME on fltl.SchemeId equals schemes.SCHEMEID
                        join blocks in context.P_BLOCK on fltl.BlockId equals blocks.BLOCKID into tempBlock
                        from blocks1 in tempBlock.DefaultIfEmpty()
                        join noentry in context.FL_FAULT_NOENTRY on fltl.FaultLogID equals noentry.FaultLogId into tempNoEntry
                        from noentry1 in tempNoEntry.DefaultIfEmpty()
                        orderby app.AppointmentID
                        where app.AppointmentDate >= varStartTime && app.AppointmentDate <= varEndTime
                                && login.LOGIN == userName
                                && fltAppt.FaultAppointmentId == (context.FL_FAULT_APPOINTMENT.Where(a => a.AppointmentId == app.AppointmentID).Min(a => a.FaultAppointmentId))
                                && flts.Description != "Cancelled"
                                && !(completedStatusesList.Contains(app.AppointmentStatus.Replace(" ", ""))) // Filter out completed appointments.
                        select new AllAppointmentsList
                        {
                            appointmentId = app.AppointmentID,
                            appointmentNotes = app.Notes,
                            appointmentDate = app.AppointmentDate,
                            //appointmentEndDate = app.a
                            appointmentStatus = app.AppointmentStatus,
                            appointmentType = "Fault",
                            surveyorUserName = login.LOGIN,
                            appointmentStartTimeString = app.Time,
                            appointmentEndTimeString = app.EndTime,
                            createdBy = fltl.UserId,
                            creationDate = app.LastActionDate,
                            loggedDate = app.LastActionDate,
                            createdByPerson = employee.FIRSTNAME + " " + employee.LASTNAME,
                            scheme = new SchemeBlockData
                            {
                                schemeId = schemes.SCHEMEID,
                                blockId = blocks1.BLOCKID,
                                schemeName = schemes.SCHEMENAME,
                                blockName = blocks1.BLOCKNAME,
                                address1 = blocks1.ADDRESS1,
                                address2 = blocks1.ADDRESS2,
                                address3 = blocks1.ADDRESS3,
                                towncity = blocks1.TOWNCITY,
                                postcode = blocks1.POSTCODE,
                                county = blocks1.COUNTY,
                            }

                        });

            if (_var.Count() > 0)
            {
                faultAppointments = _var.ToList();

                foreach (AllAppointmentsList item in faultAppointments)
                {
                    DateTime dt = (DateTime)item.appointmentDate;
                    item.appointmentStartDateTime = DateTime.Parse(dt.ToLongDateString() + " " + item.appointmentStartTimeString);
                    item.appointmentEndDateTime = DateTime.Parse(dt.ToLongDateString() + " " + item.appointmentEndTimeString);
                    if (item.appointmentStatus == "Appointment Arranged")
                        item.appointmentStatus = "NotStarted";
                    //setAdditionalDataFault(item);
                    item.jobDataList = getJobDataList(item.appointmentId);
                    item.property = null;
                }
            }

            return faultAppointments;
        }

        #endregion
    }
}
