﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PropSurvey.Dal.Base;
using PropSurvey.Contracts.Data;
using System.Transactions;
using PropSurvey.Entities;
using System.Data.Objects;

namespace PropSurvey.Dal.TestCase
{
    public class TestCaseDal : BaseDal
    {
        public int CheckUser(string Username, string Password)
        {
            try
            {
                var login = (from log in context.AC_LOGINS
                             where log.LOGIN == Username && log.PASSWORD == Password
                             select log);
                AC_LOGINS login_data = new AC_LOGINS();
                if (login.Count() > 0)
                {
                    login_data = login.First();
                }

                return login_data.LOGINID;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message,ex.InnerException);
            }
        }

        //public int CheckOrgID(string Username)
        //{

        //}

        //public int WritInLoggedInUser(string Username)
        //{

        //}

        //public int WriteInAppointments(string Username)
        //{

        //}

        
    }
}
