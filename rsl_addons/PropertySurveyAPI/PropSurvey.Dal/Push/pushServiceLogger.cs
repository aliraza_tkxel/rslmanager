﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PropSurvey.Utilities.Constants;
using PushSharp.Apple;
using PushSharp;
using PushSharp.Core;
using System.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging;

namespace PropSurvey.Dal.Push
{
    internal sealed class pushServiceLogger : ILogger
    {
        #region Singlton Implementation
        private static volatile pushServiceLogger instance;
        private static object syncRoot = new Object();
        public static pushServiceLogger Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new pushServiceLogger();
                    }
                }

                return instance;
            }
        }
        #endregion

        #region Logging Level Static Member

        public static readonly LogLevel Level = (LogLevel)int.Parse(ConfigurationManager.AppSettings.Get("PushServiceLogLevel"));

        #endregion

        #region Constructor(s)
        private pushServiceLogger() { }
        #endregion
        
        #region Logging Function(s)

        void ILogger.Debug(string format, params object[] objs)
        {
            if (((int)Level) >= ((int)LogLevel.Debug))
            {
                Logger.Write(String.Format("pushServiceLogger => DEBUG [" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "] " + format, objs), ApplicationConstants.PushService);
            }
        }

        void ILogger.Info(string format, params object[] objs)
        {
            if (((int)Level) >= ((int)LogLevel.Info))
            {
                Logger.Write(String.Format("pushServiceLogger => INFO [" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "] " + format, objs), ApplicationConstants.PushService);
            }
        }

        void ILogger.Warning(string format, params object[] objs)
        {
            Logger.Write(String.Format("pushServiceLogger => WARN [" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "] " + format, objs), ApplicationConstants.PushService);
        }

        void ILogger.Error(string format, params object[] objs)
        {
            if (((int)Level) >= ((int)LogLevel.Error))
            {
                Logger.Write(String.Format("pushServiceLogger => ERR  [" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "] " + format, objs), ApplicationConstants.PushService);
            }
        }

        #endregion
    }
}
