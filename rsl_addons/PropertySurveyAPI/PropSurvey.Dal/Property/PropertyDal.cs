﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Transactions;
using PropSurvey.Contracts.Data;
using PropSurvey.Dal.Base;
using PropSurvey.Entities;
using System.Web;
using PropSurvey.Utilities.Helpers;


namespace PropSurvey.Dal.Property
{
    public class PropertyDal : BaseDal
    {
        #region find Property

        /// <summary>
        /// This function returns the property against specified parameters
        /// </summary>
        /// <param name="skip">how many records client application want to skip. e.g 10,20</param>
        /// <param name="top">how many records client application wants to get</param>
        /// <param name="reference">reference number </param>
        /// <param name="houseNumber">house number of customer</param>
        /// <param name="street">street of customer</param>
        /// <param name="postCode">post code of customer</param>
        /// <param name="sureName">sure name of customer</param>
        /// <returns>It returns the list of customer data's object</returns>
        public PropertiesListData findProperty(PropertySearchParam varPropertySearchParam)
        {
            
            var proWithCustomer = (from cus in context.C__CUSTOMER
                                   join gtitle in context.G_TITLE on cus.TITLE equals gtitle.TITLEID into tempgti
                                   from gti in tempgti.DefaultIfEmpty()
                                   join cad in context.C_ADDRESS on cus.CUSTOMERID equals cad.CUSTOMERID
                                   join cut in context.C_CUSTOMERTENANCY on cus.CUSTOMERID equals cut.CUSTOMERID
                                   join ten in context.C_TENANCY on cut.TENANCYID equals ten.TENANCYID
                                   join pro in context.P__PROPERTY on ten.PROPERTYID equals pro.PROPERTYID
                                   join psta in context.P_STATUS on pro.STATUS equals psta.STATUSID
                                   join lgsrtemp in context.P_LGSR on pro.PROPERTYID equals lgsrtemp.PROPERTYID
                                        into tempjoin
                                   from lgsr in tempjoin.DefaultIfEmpty()
                                   where
                                   (ten.ENDDATE == null || ten.ENDDATE >= DateTime.Today.Date)
                                   && (cut.ENDDATE == null || cut.ENDDATE >= DateTime.Today.Date)
                                   && cad.ISDEFAULT == 1
                                   && psta.DESCRIPTION != "sold" && psta.DESCRIPTION != "demolished" && psta.DESCRIPTION != "transfer" && psta.DESCRIPTION != "other losses" 
                                   && cut.CUSTOMERTENANCYID == (context.C_CUSTOMERTENANCY.Where(a => a.TENANCYID == ten.TENANCYID && a.CUSTOMERID == cut.CUSTOMERID && (a.ENDDATE == null || a.ENDDATE >= DateTime.Today.Date)).Max(a => a.CUSTOMERTENANCYID))
                                   && (pro.PROPERTYID == varPropertySearchParam.searchstring
                                   || (pro.HOUSENUMBER.Contains(varPropertySearchParam.searchstring) 
                                   || ((pro.HOUSENUMBER == null ? string.Empty : (pro.HOUSENUMBER + " "))
                                        + (pro.ADDRESS1 == null ? string.Empty : (pro.ADDRESS1 + " ")) 
                                        + (pro.ADDRESS2 == null ? string.Empty : (pro.ADDRESS2 + " ")) 
                                        + (pro.ADDRESS3 == null ? string.Empty :  (pro.ADDRESS3 + " ")) 
                                        + (pro.POSTCODE == null ? string.Empty : pro.POSTCODE))
                                        .Contains(varPropertySearchParam.searchstring) 
                                   || pro.ADDRESS1.Contains(varPropertySearchParam.searchstring) 
                                   || pro.ADDRESS2.Contains(varPropertySearchParam.searchstring) 
                                   || pro.ADDRESS3.Contains(varPropertySearchParam.searchstring))
                                   || pro.POSTCODE == varPropertySearchParam.searchstring
                                       //|| pro.lastContains(varPropertySearchParam.surename)
                                   )
                                   select new CustomerData
                                   {
                                       customerId = cus.CUSTOMERID,
                                       title = gti.DESCRIPTION,
                                       firstName = cus.FIRSTNAME,
                                       middleName = cus.MIDDLENAME,
                                       lastName = cus.LASTNAME,
                                       property = new PropertyData
                                       {
                                           houseNumber = pro.HOUSENUMBER,
                                           flatNumber = pro.FLATNUMBER,
                                           address1 = pro.ADDRESS1,
                                           address2 = pro.ADDRESS2,
                                           address3 = pro.ADDRESS3,
                                           townCity = pro.TOWNCITY,
                                           postCode = pro.POSTCODE,
                                           county = pro.COUNTY,
                                           tenancyId = ten.TENANCYID,
                                           propertyId = pro.PROPERTYID,
                                           certificateExpiry = EntityFunctions.AddYears(lgsr.ISSUEDATE, 1)
                                       }
                                   });

            var proWithoutCustomer = (from pro in context.P__PROPERTY
                                      join psta in context.P_STATUS on pro.STATUS equals psta.STATUSID
                                      join lgsr in context.P_LGSR on pro.PROPERTYID equals lgsr.PROPERTYID
                                      where psta.DESCRIPTION != "Let"
                                      && psta.DESCRIPTION != "sold" && psta.DESCRIPTION != "demolished" && psta.DESCRIPTION != "transfer" && psta.DESCRIPTION != "other losses"
                                      && (pro.PROPERTYID == varPropertySearchParam.searchstring
                                   || (pro.HOUSENUMBER.Contains(varPropertySearchParam.searchstring)
                                   || ((pro.HOUSENUMBER == null ? string.Empty : (pro.HOUSENUMBER + " "))
                                        + (pro.ADDRESS1 == null ? string.Empty : (pro.ADDRESS1 + " "))
                                        + (pro.ADDRESS2 == null ? string.Empty : (pro.ADDRESS2 + " "))
                                        + (pro.ADDRESS3 == null ? string.Empty : (pro.ADDRESS3 + " "))
                                        + (pro.POSTCODE == null ? string.Empty : pro.POSTCODE))
                                        .Contains(varPropertySearchParam.searchstring) 
                                   || pro.ADDRESS1.Contains(varPropertySearchParam.searchstring)
                                   || pro.ADDRESS2.Contains(varPropertySearchParam.searchstring)
                                   || pro.ADDRESS3.Contains(varPropertySearchParam.searchstring))
                                   || pro.POSTCODE == varPropertySearchParam.searchstring
                                          //|| pro.lastContains(varPropertySearchParam.surename)
                                   )
                                      select new CustomerData
                                      {
                                          title = "N/A",
                                          firstName = "N/A",
                                          middleName = "N/A",
                                          lastName = "N/A",
                                          property = new PropertyData
                                          {
                                              houseNumber = pro.HOUSENUMBER,
                                              flatNumber = pro.FLATNUMBER,
                                              address1 = pro.ADDRESS1,
                                              address2 = pro.ADDRESS2,
                                              address3 = pro.ADDRESS3,
                                              townCity = pro.TOWNCITY,
                                              postCode = pro.POSTCODE,
                                              county = pro.COUNTY,
                                              propertyId = pro.PROPERTYID,
                                              certificateExpiry = EntityFunctions.AddYears(lgsr.ISSUEDATE, 1)
                                          }
                                      });

            List<CustomerData> propertyWithCustomer = proWithCustomer.ToList();
            List<CustomerData> propertyWithoutCustomer = proWithoutCustomer.ToList();

            IEnumerable<CustomerData> property = propertyWithCustomer.Union(propertyWithoutCustomer);

            PropertiesListData propertiesList = new PropertiesListData();

            propertiesList.PageInfo.PageNumber = varPropertySearchParam.pagenumber;
            propertiesList.PageInfo.PageSize = varPropertySearchParam.pagecount;


            if (varPropertySearchParam.pagenumber == 1)
            {
                int totalcount = property.Count();

                propertiesList.PageInfo.TotalCount = totalcount;
                propertiesList.PageInfo.TotalPages = (int)Math.Ceiling((double)totalcount / varPropertySearchParam.pagecount);
            }

            //From client sidee Page number should not be starting from 0 (Zero).
            property = property.Skip((varPropertySearchParam.pagenumber - 1) * varPropertySearchParam.pagecount).Take(varPropertySearchParam.pagecount);

            //List<CustomerData> propertyList = new List<CustomerData>();

            if (property.Count() > 0)
            {
                propertiesList.Properties = property.ToList();
                foreach (CustomerData prop in propertiesList.Properties)
                {
                    prop.property.propertyPicture = getAllPropertyImages(prop.property.propertyId);
                    prop.property.Accommodations = getPropertyDimensions(prop.property.propertyId);
                }
            }

            return propertiesList;
        }

        /// <summary>
        /// This function returns the property against specified parameters
        /// </summary>
        /// <param name="skip">how many records client application want to skip. e.g 10,20</param>
        /// <param name="top">how many records client application wants to get</param>
        /// <param name="reference">reference number </param>
        /// <param name="houseNumber">house number of customer</param>
        /// <param name="street">street of customer</param>
        /// <param name="postCode">post code of customer</param>
        /// <param name="surName">sur name of customer</param>
        /// <returns>It retruns the list of customer data's object</returns>
        public List<CustomerData> findPropertyGas(int skip, int top, string reference = "", string houseNumber = "", string street = "", string postCode = "", string surName = "")
        {
            var proWithCustomer = (from cus in context.C__CUSTOMER
                                   join gtitle in context.G_TITLE on cus.TITLE equals gtitle.TITLEID into tempgti
                                   from gti in tempgti.DefaultIfEmpty()
                                   join cad in context.C_ADDRESS on cus.CUSTOMERID equals cad.CUSTOMERID
                                   join cut in context.C_CUSTOMERTENANCY on cus.CUSTOMERID equals cut.CUSTOMERID
                                   join ten in context.C_TENANCY on cut.TENANCYID equals ten.TENANCYID
                                   join pro in context.P__PROPERTY on ten.PROPERTYID equals pro.PROPERTYID
                                   join psta in context.P_STATUS on pro.STATUS equals psta.STATUSID
                                   join jor in context.AS_JOURNAL on pro.PROPERTYID equals jor.PROPERTYID
                                   //Do join with P_LGSR to get the certificate expiry
                                   join lgsr in context.P_LGSR on jor.PROPERTYID equals lgsr.PROPERTYID into result
                                   from data in result.DefaultIfEmpty()

                                   //orderby data.RECEVIEDDATE ascending

                                   where ten.ENDDATE == null && cad.ISDEFAULT == 1 && jor.ISCURRENT == true && psta.DESCRIPTION == "Let" &&
                                   cut.ENDDATE == null && jor.INSPECTIONTYPEID == 1
                                   && cut.CUSTOMERTENANCYID == (context.C_CUSTOMERTENANCY.Where(a => a.TENANCYID == ten.TENANCYID).Min(a => a.CUSTOMERTENANCYID))
                                   select new CustomerData
                                   {
                                       customerId = cus.CUSTOMERID,
                                       title = gti.DESCRIPTION,
                                       firstName = cus.FIRSTNAME,
                                       middleName = cus.MIDDLENAME,
                                       lastName = cus.LASTNAME,
                                       property = new PropertyData
                                       {
                                           houseNumber = pro.HOUSENUMBER,
                                           flatNumber = pro.FLATNUMBER,
                                           address1 = pro.ADDRESS1,
                                           address2 = pro.ADDRESS2,
                                           address3 = pro.ADDRESS3,
                                           townCity = pro.TOWNCITY,
                                           postCode = pro.POSTCODE,
                                           county = pro.COUNTY,
                                           tenancyId = ten.TENANCYID,
                                           propertyId = pro.PROPERTYID,
                                           //show expiry date in the property
                                           certificateExpiry = data.RECEVIEDDATE
                                       }
                                   });

            var proWithoutCustomer = (from pro in context.P__PROPERTY
                                      join psta in context.P_STATUS on pro.STATUS equals psta.STATUSID
                                      join jor in context.AS_JOURNAL on pro.PROPERTYID equals jor.PROPERTYID
                                      //Do join with P_LGSR to get the certificate expiry
                                      join lgsr in context.P_LGSR on jor.PROPERTYID equals lgsr.PROPERTYID into result
                                      from data in result.DefaultIfEmpty()

                                      //orderby data.RECEVIEDDATE ascending

                                      where jor.ISCURRENT == true && psta.DESCRIPTION != "Let" && jor.INSPECTIONTYPEID == 1
                                      select new CustomerData
                                      {
                                          title = "N/A",
                                          firstName = "N/A",
                                          middleName = "N/A",
                                          lastName = "N/A",
                                          property = new PropertyData
                                          {
                                              houseNumber = pro.HOUSENUMBER,
                                              flatNumber = pro.FLATNUMBER,
                                              address1 = pro.ADDRESS1,
                                              address2 = pro.ADDRESS2,
                                              address3 = pro.ADDRESS3,
                                              townCity = pro.TOWNCITY,
                                              postCode = pro.POSTCODE,
                                              county = pro.COUNTY,
                                              propertyId = pro.PROPERTYID,
                                              //show expiry date in the property
                                              certificateExpiry = data.RECEVIEDDATE
                                          }
                                      });

            if (!string.IsNullOrEmpty(reference))
            {
                proWithCustomer = proWithCustomer.Where(p => p.property.propertyId == reference);
                proWithoutCustomer = proWithoutCustomer.Where(p => p.property.propertyId == reference);
            }

            if (!string.IsNullOrEmpty(houseNumber))
            {
                proWithCustomer = proWithCustomer.Where(p => p.property.houseNumber.ToLower() == houseNumber.ToLower());
                proWithoutCustomer = proWithoutCustomer.Where(p => p.property.houseNumber.ToLower() == houseNumber.ToLower());
            }

            if (!string.IsNullOrEmpty(street))
            {
                proWithCustomer = proWithCustomer.Where(p => p.property.address1.Contains(street) || p.property.address2.Contains(street) || p.property.address3.Contains(street));
                proWithoutCustomer = proWithoutCustomer.Where(p => p.property.address1.Contains(street) || p.property.address2.Contains(street) || p.property.address3.Contains(street));
            }

            if (!string.IsNullOrEmpty(postCode))
            {
                proWithCustomer = proWithCustomer.Where(p => p.property.postCode.ToLower() == postCode.ToLower());
                proWithoutCustomer = proWithoutCustomer.Where(p => p.property.postCode.ToLower() == postCode.ToLower());
            }

            if (!string.IsNullOrEmpty(surName))
            {
                proWithCustomer = proWithCustomer.Where(p => p.lastName.Contains(surName));
                proWithoutCustomer = proWithoutCustomer.Where(p => p.lastName.Contains(surName));
            }

            List<CustomerData> propertyWithCustomer = proWithCustomer.ToList();
            List<CustomerData> propertyWithoutCustomer = proWithoutCustomer.ToList();

            IEnumerable<CustomerData> property = propertyWithCustomer.Union(propertyWithoutCustomer);

            //The properties that have null certificate expiry should be on the end.
            //property = property.OrderBy(p => p.property.certificateExpiry).Skip(skip).Take(top);
            property = property.OrderByDescending(p => p.property.certificateExpiry.HasValue).ThenBy(p => p.property.certificateExpiry).Skip(skip).Take(top);

            List<CustomerData> propertyList = new List<CustomerData>();
            if (property.Count() > 0)
            {
                propertyList = property.ToList();
            }

            return propertyList;
        }

        #endregion

        #region get Property Images

        /// <summary>
        /// This function returns the list of images which were previously saved against property.
        /// </summary>
        /// <param name="propertyId">property id</param>
        /// <param name="itemId">item Id</param>
        /// <returns>returns the list of image names against property</returns>
        public List<PropertyPictureData> getPropertyImages(string propertyId, int itemId)
        {
            var propertyPic = (from prop in context.PA_PROPERTY_ITEM_IMAGES
                               orderby prop.PROPERTYID descending
                               where prop.PROPERTYID.ToLower() == propertyId.ToLower()
                               select new PropertyPictureData
                               {
                                   propertyPictureName = prop.ImageName,
                                   propertyPictureId = prop.SID,
                                   imagePath = prop.ImagePath,
                                   itemId = prop.ItemId
                               });

            List<PropertyPictureData> propertyPicList = new List<PropertyPictureData>();

            if (propertyPic.Count() > 0)
            {
                propertyPicList = propertyPic.ToList();
            }
            return propertyPicList;
        }

        #endregion

        #region get Property Images

        /// <summary>
        /// This function returns the list of images which were previously saved against property.
        /// </summary>
        /// <param name="propertyId">property id</param>
        /// <param name="itemId">item Id</param>
        /// <returns>returns the list of image names against property</returns>
        public List<PropertyPictureData> getAllPropertyImages(string propertyId)
        {
            var propertyPic = (from prop in context.PA_PROPERTY_ITEM_IMAGES
                               orderby prop.PROPERTYID descending
                               where prop.PROPERTYID.ToLower() == propertyId.ToLower()
                               select new PropertyPictureData
                               {
                                   propertyPictureName = prop.ImageName,
                                   propertyPictureId = prop.SID,
                                   imagePath = prop.ImagePath,
                                   itemId = prop.ItemId,
                                   propertyId = prop.PROPERTYID
                               });

            List<PropertyPictureData> propertyPicList = new List<PropertyPictureData>();
            propertyPicList = propertyPic.ToList();

            if (propertyPicList.Count() > 0)
            {
                var server = System.Web.HttpContext.Current.Server;
                var requestURL = HttpContext.Current.Request.Url;
                string imgFullPath = string.Empty;
                string serverRootPath = server.MapPath("/");
                serverRootPath = serverRootPath.Remove(serverRootPath.Length - 1);
                serverRootPath = serverRootPath.Remove(serverRootPath.LastIndexOf(@"\") - 1);

                foreach (PropertyPictureData pic in propertyPicList)
                {
                    pic.imagePath = FileHelper.getLogicalPropertyImagePath(pic.propertyId, pic.propertyPictureName);
                }
            }
            return propertyPicList;
        }

        #endregion        

        #region get All stock survey Images by survey Id

        /// <summary>
        /// This function returns the list of images which were previously saved against property.
        /// </summary>
        /// <param name="propertyId">property id</param>
        /// <param name="itemId">item Id</param>
        /// <returns>returns the list of image names against property</returns>
        public List<PropertyPictureData> getAllStockSurveyImages(string propertyId, int surveyId)
        {
            var propertyPic = (from prop in context.PS_Survey_Item_Images
                               where prop.SurveyId == surveyId
                               select new PropertyPictureData
                               {
                                   propertyPictureName = prop.ImageName,
                                   propertyPictureId = prop.ImagesId,
                                   imagePath = prop.ImagePath,
                                   itemId = prop.ItemId,
                                   propertyId = propertyId,
                                   createdOn = prop.ImageDate,
                                   surveyId = prop.SurveyId ?? 0
                               });

            List<PropertyPictureData> propertyPicList = new List<PropertyPictureData>();
            propertyPicList = propertyPic.ToList();

            if (propertyPicList.Count() > 0)
            {
                var server = System.Web.HttpContext.Current.Server;
                var requestURL = HttpContext.Current.Request.Url;
                string imgFullPath = string.Empty;
                string serverRootPath = server.MapPath("/");
                serverRootPath = serverRootPath.Remove(serverRootPath.Length - 1);
                serverRootPath = serverRootPath.Remove(serverRootPath.LastIndexOf(@"\") - 1);

                foreach (PropertyPictureData pic in propertyPicList)
                {
                    pic.imagePath = FileHelper.getLogicalPropertyImagePath(pic.propertyId, pic.propertyPictureName);
                }
            }
            return propertyPicList;
        }

        #endregion

        #region get Default Property Image Name

        /// <summary>
        /// This function returns the default image name which was saved against property
        /// </summary>
        /// <param name="propertyId">property id</param>
        /// <returns>returns the name of the image</returns>
        public string getDefaultPropertyImageName(string propertyId)
        {
            string imageName = String.Empty;

            //var property = (from pro in context.PA_PROPERTY_ITEM_IMAGES
            //                where pro.PROPERTYID.ToLower() == propertyId.ToLower() && pro.isDefault == true
            //                select pro.ImageName);

            //if (property.Count() > 0)
            //{
            //    imageName = property.Single().ToString();
            //}

            return imageName;
        }

        #endregion

        #region save Property Image

        /// <summary>
        /// This funciton save the property image in the database
        /// </summary>
        /// <param name="propPicData">property picture data</param>
        /// <param name="propertyId">property id</param>
        /// <returns>true or false upon successfull saving on disk and on db</returns>
        public int savePropertyImage(PropertyPictureData propPicData)
        {
            int success = 0;
            using (TransactionScope trans = new TransactionScope())
            {
                {
                    PS_Appointment2Survey savedSurvey = new PS_Appointment2Survey();
                    PS_Survey survey = new PS_Survey();

                    //this will contain the existing /already saved survey id
                    int existingSurveyId = 0;

                    //Find the survey aginst give appointment id. 
                    var appoint2survey = context.PS_Appointment2Survey.Where(app => app.AppointId == propPicData.appointmentId);

                    //If survey already saved against the appointment id then get the existing survey id
                    if (appoint2survey.Count() > 0)
                    {
                        //get the existing survey id
                        savedSurvey = appoint2survey.First();
                        existingSurveyId = savedSurvey.SurveyId;
                    }

                    //if survey already saved then save the property image against that survey id
                    if (existingSurveyId > 0)
                    {
                        //save the property image against exisiting survey id
                        this.savePsPropertyImage(propPicData, existingSurveyId);
                    }
                    else if (existingSurveyId == 0)
                    {
                        //save the new survey
                        survey.PROPERTYID = propPicData.propertyId;
                        survey.CompletedBy = propPicData.createdBy;
                        survey.SurveyDate = propPicData.createdOn;

                        context.AddToPS_Survey(survey);
                        context.SaveChanges();

                        //existing survey does not exist then entry will go to appointment2survey

                        PS_Appointment2Survey appSurv = new PS_Appointment2Survey();
                        appSurv.AppointId = propPicData.appointmentId;
                        appSurv.SurveyId = survey.SurveyId;

                        context.AddToPS_Appointment2Survey(appSurv);
                        context.SaveChanges();

                        //save the property image against new survey id
                        this.savePsPropertyImage(propPicData, survey.SurveyId);
                    }

                    //get the image from pa_property_item_images
                    PA_PROPERTY_ITEM_IMAGES paImage = new PA_PROPERTY_ITEM_IMAGES();

                    //prepare to insert the new one
                    // If ItemId is O then insert NULL for ItemId.
                    if (propPicData.itemId == 0)
                    { paImage.ItemId = null; }
                    else
                    { paImage.ItemId = propPicData.itemId; }

                    if (propPicData.heatingId  == 0)
                    { 
                        paImage.HeatingMappingId = null;
                    }
                    else
                    { 
                        paImage.HeatingMappingId = propPicData.heatingId; 
                    }

                    paImage.PROPERTYID = propPicData.propertyId;
                    paImage.ImageName = propPicData.propertyPictureName;
                    paImage.ImagePath = propPicData.imagePath;                    
                    paImage.CreatedBy = propPicData.createdBy;
                    paImage.CreatedOn = DateTime.Now;
                    paImage.ImageIdentifier = propPicData.ImageIdentifier;
                    //  paImage.IsDefault = propPicData.isDefault;
                    context.AddToPA_PROPERTY_ITEM_IMAGES(paImage);
                    context.SaveChanges();

                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    trans.Complete();

                    success = paImage.SID;
                }
            }

            return success;
        }

        /// <summary>
        /// This funciton save the property image in the database
        /// </summary>
        /// <param name="propPicData">property picture data</param>
        /// <param name="propertyId">property id</param>
        /// <returns>true or false upon successfull saving on disk and on db</returns>
        public int savePropertyImageGas(PropertyPictureData propPicData)
        {
            int propertyPicId = 0;
            using (TransactionScope trans = new TransactionScope())
            {
                //get the image from pa_property_item_images
                PA_PROPERTY_ITEM_IMAGES paImage = new PA_PROPERTY_ITEM_IMAGES();
                if (propPicData.itemId == 0)
                {
                    propPicData.itemId = null;
                }
                //prepare to insert the new one
                paImage.ItemId = propPicData.itemId;
                paImage.PROPERTYID = propPicData.propertyId;
                paImage.ImageName = propPicData.propertyPictureName;
                paImage.ImagePath = propPicData.imagePath;
                paImage.CreatedBy = propPicData.createdBy;
                paImage.CreatedOn = DateTime.Now;
                paImage.ImageIdentifier = propPicData.ImageIdentifier;
                //paImage.IsDefault = propPicData.isDefault;
                context.AddToPA_PROPERTY_ITEM_IMAGES(paImage);
                context.SaveChanges();

                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                trans.Complete();

                propertyPicId = paImage.SID;
            }

            return propertyPicId;
        }

        /// <summary>
        /// This function gets the item id against the text gas from the table PA_ITEM
        /// </summary>
        /// <returns></returns>
        public int getItemId()
        {
            var itemResult = context.PA_ITEM.Where(item => item.ItemName.ToLower() == Utilities.Constants.MessageConstants.GasItemName.ToLower()).FirstOrDefault();
            return itemResult == null ? 0 : itemResult.ItemID;
        }

        #endregion

        #region save ps property image

        /// <summary>
        /// This function always save the new image against survey id
        /// </summary>
        /// <param name="propPicData">Property Pic Data</param>
        /// <param name="surveyId">Survey Id</param>
        public void savePsPropertyImage(PropertyPictureData propPicData, int surveyId)
        {
            PS_Survey_Item_Images psImage = new PS_Survey_Item_Images();

            psImage.ItemId = propPicData.itemId;
            psImage.SurveyId = surveyId;

            psImage.ImageName = propPicData.propertyPictureName;
            psImage.ImagePath = propPicData.imagePath;
            psImage.ImageDate = DateTime.Now;
            psImage.ImageIdentifier = propPicData.ImageIdentifier;

            //insert the new one 
            context.AddToPS_Survey_Item_Images(psImage);
            context.SaveChanges();
        }

        #endregion

        #region delete Property Image
        /// <summary>
        /// This function is used to delete the image against the property from the server
        /// </summary>
        /// <param name="propertyPictureId">property picture id</param>
        /// <returns>property picture data which is deleted</returns>

        public PropertyPictureData deletePropertyImage(int propertyPictureId)
        {

            PA_PROPERTY_ITEM_IMAGES propDelPic = new PA_PROPERTY_ITEM_IMAGES();
            PropertyPictureData propPicData = new PropertyPictureData();
            string picName = string.Empty;

            var propPic = (from pric in context.PA_PROPERTY_ITEM_IMAGES
                           where pric.SID == propertyPictureId
                           select new PropertyPictureData
                           {
                               propertyId = pric.PROPERTYID,
                               propertyPictureName = pric.ImageName
                           }
                          );

            if (propPic.Count() > 0)
            {
                propPicData = propPic.First();
                propDelPic.SID = propertyPictureId;
                context.PA_PROPERTY_ITEM_IMAGES.Attach(propDelPic);
                context.PA_PROPERTY_ITEM_IMAGES.DeleteObject(propDelPic);
                context.SaveChanges();
                context.Dispose();
                //var propData = (from propImage in context.PA_PROPERTY_ITEM_IMAGES
                //                orderby propImage.SID descending
                //                where propImage.PROPERTYID == propPicData.propertyId
                //                select propImage);

                //if (propData.Count() > 0)
                //{
                //    PA_PROPERTY_ITEM_IMAGES propPicture = propData.First();
                //    propPicture.IsDefault = true;
                //    context.SaveChanges();
                //    context.Dispose();

                //}
            }
            return propPicData;

        }
        #endregion

        #region is Property Appointment Exist

        /// <summary>
        /// this fuction varifies that either specific property exists in database against any appointment
        /// </summary>
        /// <param name="userName">user name</param>
        /// <returns> true or false</returns>
        public bool isPropertyAppointmentExist(string propertyId)
        {
            bool success = false;

            var propRecord = context.PS_Property2Appointment.Where(acc => acc.PropertyId.ToLower() == propertyId.ToLower());

            if (propRecord.Count() > 0)
            {
                success = true;
            }
            else
            {
                success = false;
            }
            return success;
        }

        /// <summary>
        /// this fuction varifies that either specific property exists in database against any appointment
        /// </summary>
        /// <param name="userName">user name</param>
        /// <returns> true or false</returns>
        public bool isPropertyAppointmentExist(int appointmentId)
        {
            bool success = false;

            var appRecord = context.AS_APPOINTMENTS.Where(app => app.APPOINTMENTID == appointmentId);

            if (appRecord.Count() > 0)
            {
                success = true;
            }
            else
            {
                success = false;
            }
            return success;
        }

        #endregion

        #region get Property Dimensions

        /// <summary>
        /// This function returns ths property dimensions
        /// </summary>
        /// <param name="propertyId">property id</param>
        /// <returns>List of property dimension data object</returns>

        public List<PropertyDimData> getPropertyDimensions(string propertyId)
        {
            var propDim = (from prop in context.PA_PropertyDimension
                           where prop.PropertyId.ToLower() == propertyId.ToLower()
                           select new PropertyDimData
                           {
                               propertyDimId = prop.PropertyDimId,
                               propertyId = prop.PropertyId,
                               roomHeight = prop.RoomHeight,
                               roomLength = prop.RoomLength,
                               roomWidth = prop.RoomWidth,
                               roomName = prop.RoomName,
                               updatedBy = prop.UpdatedBy,
                               updatedOn = prop.UpdatedOn
                           });

            // return propDim.ToList();
            List<PropertyDimData> propDimDataList = new List<PropertyDimData>();

            if (propDim.Count() > 0)
            {
                propDimDataList = propDim.ToList();
                PropertyDimData propDimItem = new PropertyDimData();
                propDimItem = propDimDataList.Find(x => x.roomName.Equals("Living Room"));
                if (propDimItem == null)
                {
                    propDimItem = new PropertyDimData("Living Room");
                    propDimDataList.Insert(2, propDimItem);
                }

            }
            else
            {
                PropertyDimData propDimData = new PropertyDimData();
                propDimData = propDimData.getPropertyInitialValues("Kitchen/Diner");
                propDimDataList.Add(propDimData);

                propDimData = propDimData.getPropertyInitialValues("Dining Room");
                propDimDataList.Add(propDimData);

                propDimData = propDimData.getPropertyInitialValues("Living Room");
                propDimDataList.Add(propDimData);

                propDimData = propDimData.getPropertyInitialValues("Bathroom");
                propDimDataList.Add(propDimData);

                propDimData = propDimData.getPropertyInitialValues("Bedroom 1");
                propDimDataList.Add(propDimData);

                propDimData = propDimData.getPropertyInitialValues("Bedroom 2");
                propDimDataList.Add(propDimData);

                propDimData = propDimData.getPropertyInitialValues("Bedroom 3");
                propDimDataList.Add(propDimData);

                propDimData = propDimData.getPropertyInitialValues("Bedroom 4");
                propDimDataList.Add(propDimData);

            }
            return propDimDataList;
        }
        #endregion

        #region update Property Dimensions
        /// <summary>
        /// This function updates the property dimensions
        /// </summary>
        /// <param name="propDimData">This function accepts the list of property dimension data object</param>
        /// <returns>true on successful update and false on un successful update</returns>
        public bool updatePropertyDimensions(List<PropertyDimData> propDimData, string propertyId, PropertySurvey_Entities currentContext = null)
        {
            if (currentContext != null) this.context = currentContext;
            bool success = false;
            //using (TransactionScope trans = new TransactionScope())
            //{
            {
                if (propDimData.Count() > 0)
                {
                    //  string propertyId = propDimData[0].propertyId;
                    var dimRecord = context.PA_PropertyDimension.Where(app => app.PropertyId == propertyId);

                    if (dimRecord.Count() > 0)
                    {
                        foreach (var item in dimRecord)
                        {
                            context.PA_PropertyDimension.DeleteObject(item);
                        }
                    }
                    if (currentContext != null) context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    else context.SaveChanges();

                    foreach (var item in propDimData)
                    {
                        PA_PropertyDimension pad = new PA_PropertyDimension();
                        pad.PropertyId = propertyId;
                        pad.RoomName = item.roomName;
                        pad.RoomHeight = item.roomHeight;
                        pad.RoomLength = item.roomLength;
                        pad.RoomWidth = item.roomWidth;
                        pad.UpdatedBy = item.updatedBy;
                        pad.UpdatedOn = item.updatedOn;

                        context.AddToPA_PropertyDimension(pad);

                        context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                        context.Detach(pad);
                    }

                    //trans.Complete();                    
                    success = true;
                }
            }
            //}

            if (currentContext == null) context.AcceptAllChanges();

            return success;
        }
        #endregion

        #region get Property Asbestos Risk
        /// <summary>
        /// This function returns the list of asbestos risk against the property.
        /// </summary>
        /// <param name="propertyId">property id</param>
        /// <returns>the list of asbestos risks</returns>

        public List<PropertyAsbestosData> getPropertyAsbestosRisk(string propertyId)
        {
            var propertyAsbestos = (from pro in context.P__PROPERTY
                                    join par in context.P_PROPERTY_ASBESTOS_RISKLEVEL on pro.PROPERTYID equals par.PROPERTYID
                                    join pas in context.P_PROPERTY_ASBESTOS_RISK on par.PROPASBLEVELID equals pas.PROPASBLEVELID
                                    join asb in context.P_ASBESTOS on par.ASBESTOSID equals asb.ASBESTOSID
                                    join asl in context.P_ASBRISKLEVEL on par.ASBRISKLEVELID equals asl.ASBRISKLEVELID
                                    where pro.PROPERTYID.ToLower() == propertyId.ToLower() &&  (par.DateRemoved == null || par.DateRemoved >= DateTime.Now )

                                    select new PropertyAsbestosData
                                    {
                                        asbestosId = asb.ASBESTOSID,
                                        riskDesc = asb.RISKDESCRIPTION,
                                        asbRiskLevelDesc = asl.ASBRISKLEVELDESCRIPTION
                                    });
            List<PropertyAsbestosData> propAsbList = new List<PropertyAsbestosData>();
            if (propertyAsbestos.Count() > 0)
            {
                propAsbList = propertyAsbestos.ToList();
            }

            return propAsbList;
        }
        #endregion

        #region update Property

        /// <summary>
        /// This function updates the property 
        /// </summary>
        /// <param name="propDimData">This function accepts the list of property  data object</param>
        /// <returns>true on successful update and false on un successful update</returns>
        public bool updateProperty(PropertyData propData)
        {
            bool success = false;
            using (TransactionScope trans = new TransactionScope())
            {
                var propRecord = context.P__PROPERTY.Where(app => app.PROPERTYID == propData.propertyId);
                if (propRecord.Count() > 0)
                {
                    P__PROPERTY property = propRecord.First();
                    //if (propData.address1 != null)
                    //{
                    //    property.ADDRESS1 = propData.address1;
                    //}
                    //if (propData.address1 != null)
                    //{
                    //    property.ADDRESS1 = propData.address1;
                    //}
                    //if (propData.address2 != null)
                    //{
                    //    property.ADDRESS2 = propData.address2;
                    //}
                    //if (propData.address3 != null)
                    //{
                    //    property.ADDRESS3 = propData.address3;
                    //}
                    if (propData.county != null)
                    {
                        property.COUNTY = propData.county;
                    }
                    if (propData.flatNumber != null)
                    {
                        property.FLATNUMBER = propData.flatNumber;
                    }
                    if (propData.houseNumber != null)
                    {
                        property.HOUSENUMBER = propData.houseNumber;
                    }
                    if (propData.postCode != null)
                    {
                        property.POSTCODE = propData.postCode;
                    }
                    if (propData.townCity != null)
                    {
                        property.TOWNCITY = propData.townCity;
                    }

                    context.SaveChanges();
                    context.Detach(property);
                }

                trans.Complete();
                success = true;
            }

            return success;
        }

        #endregion

        #region Update Property Default Image

        /// <summary>
        /// This function updates the property default image
        /// </summary>
        /// <param name="propDimData">This function accepts the list of property  data object</param>
        /// <returns>true on successful update and false on un successful update</returns>
        public int updatePropertyDefaultImage(string PropertyId, int PropertyPicId)
        {
            int returnPropertyPicId = PropertyPicId;

            using (TransactionScope trans = new TransactionScope())
            {
                var propRecord = context.P__PROPERTY.Where(app => app.PROPERTYID == PropertyId);
                if (propRecord.Count() > 0)
                {
                    P__PROPERTY property = propRecord.First();

                    property.PropertyPicId = PropertyPicId;


                    context.SaveChanges();
                    context.Detach(property);
                }

                trans.Complete();
                //returnPropertyPicId = 1;
            }

            return returnPropertyPicId;
        }

        #endregion

        #region Get Picture Name By Property Picture Id

        public string GetPicNameByPropertyPicId(int propertyPicId)
        {
            string imageName = String.Empty;

            var property = (from pro in context.PA_PROPERTY_ITEM_IMAGES
                            where pro.SID == propertyPicId
                            select pro.ImageName);

            if (property.Count() > 0)
            {
                imageName = property.Single().ToString();
            }

            return imageName;
        }

        #endregion

        #region is Property Image already exists

        /// <summary>
        /// check if property Image already exists
        /// </summary>        
        /// <param name="imageIdentifier"></param>
        /// <param name="imageName"></param>
        /// <param name="propertyPicId"></param>
        public bool isPropertyImageAlreadyExists(string imageIdentifier, out string imageName, out int propertyPicId)
        {
            bool isPropertyImageAlreadyExists = false;
            imageName = string.Empty;
            propertyPicId = -1;

            if (imageIdentifier != string.Empty)
            {
                var imagesListProperty = from img in context.PA_PROPERTY_ITEM_IMAGES
                                         where img.ImageIdentifier == imageIdentifier
                                         select img;
                if (imagesListProperty.Count() > 0)
                {
                    isPropertyImageAlreadyExists = true;
                    var imageRecord = imagesListProperty.First();
                    imageName = imageRecord.ImageName;
                    propertyPicId = imageRecord.SID;
                }
                else
                {
                    var imagesListSurvey = from img in context.PS_Survey_Item_Images
                                           where img.ImageIdentifier == imageIdentifier
                                           select img;
                    if (imagesListSurvey.Count() > 0)
                    {
                        isPropertyImageAlreadyExists = true;
                        var imageRecord = imagesListSurvey.First();
                        imageName = imageRecord.ImageName;
                        propertyPicId = imageRecord.ImagesId;
                    }

                }
            }

            return isPropertyImageAlreadyExists;
        }

        #endregion
    }
}
