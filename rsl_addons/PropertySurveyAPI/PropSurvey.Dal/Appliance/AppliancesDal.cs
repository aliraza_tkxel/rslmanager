﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Transactions;
using PropSurvey.Contracts.Data;
using PropSurvey.Dal.Base;
using PropSurvey.Entities;
using PropSurvey.Utilities.Constants;
using PropSurvey.Dal.Faults;

namespace PropSurvey.Dal.Appliances
{
    public class AppliancesDal : BaseDal
    {
        #region get All Appliances
        /// <summary>
        /// This function returns all the appliances
        /// </summary>
        /// <returns>List of appliances data objects</returns>
        //public List<ApplianceData> getAllAppliancesByPropertyId(string propertyID, int appointmentID)
        //{
        //    var appliances = (from papp in context.GS_PROPERTY_APPLIANCE
        //                      join p2a in context.PS_Property2Appointment on papp.PROPERTYID equals p2a.PropertyId
        //                      join app in context.PS_Appointment on p2a.AppointId equals app.AppointId
        //                      join loc in context.GS_LOCATION on papp.LOCATIONID equals loc.LOCATIONID into tempLocation
        //                      from Locations in tempLocation.DefaultIfEmpty()
        //                      // Line modified  - 05/07/2013 - START
        //                      join manu in context.GS_MANUFACTURER on papp.MANUFACTURERID equals manu.MANUFACTURERID into tempManufacturer
        //                      from Manufacturer in tempManufacturer.DefaultIfEmpty()
        //                      // Line modified  - 05/07/2013 - END
        //                      join model in context.GS_ApplianceModel on papp.MODELID equals model.ModelID into tempModel
        //                      from Models in tempModel.DefaultIfEmpty()
        //                      //join isIns in context.GS_ApplianceInspection on app.AppointId equals isIns.APPOINTMENTID into Insp
        //                      //from isInspected in Insp.DefaultIfEmpty()
        //                      // Line modified  - 05/07/2013 - START
        //                      join type in context.GS_APPLIANCE_TYPE on papp.APPLIANCETYPEID equals type.APPLIANCETYPEID into tempType
        //                      from ApplianceType in tempType.DefaultIfEmpty()
        //                      // Line modified  - 05/07/2013 - END
        //                      where papp.PROPERTYID == propertyID && app.AppointId == appointmentID
        //                      select new ApplianceData
        //                      {
        //                          ApplianceID = papp.PROPERTYAPPLIANCEID,
        //                          //ApplianceOrgID = papp.ORGID,
        //                          FluType = papp.FLUETYPE,
        //                          InstalledDate = papp.DATEINSTALLED,
        //                          PropertyID = propertyID,
        //                          //isInspected = isInspected.APPLIANCEID != null,
        //                          isLandlordAppliance = papp.ISLANDLORDAPPLIANCE,
        //                          ReplacementDate = papp.REPLACEMENTDATE,
        //                          Model = papp.MODEL,
        //                          ApplianceLocation = new AppliancesLocationData
        //                          {
        //                              //Location = loc.LOCATION,
        //                              //LocationID = loc.LOCATIONID
        //                              Location = Locations.LOCATION != null ? Locations.LOCATION : "",
        //                              LocationID = Locations.LOCATIONID != null ? Locations.LOCATIONID : 0
        //                          },
        //                          ApplianceManufacturer = new ManufacturerData
        //                          {
        //                              // Line modified  - 05/07/2013 - START
        //                              Manufacturer = Manufacturer.MANUFACTURER != null ? Manufacturer.MANUFACTURER : "",
        //                              ManufacturerID = Manufacturer.MANUFACTURERID != null ? Manufacturer.MANUFACTURERID : 0
        //                              // Line modified  - 05/07/2013 - END
        //                          },
        //                          ApplianceModel = new ApplianceModelData
        //                          {
        //                              ApplianceModelID = Models.ModelID != null ? Models.ModelID : 0,
        //                              ApplianceModel = Models.Model != null ? Models.Model : ""
        //                          },
        //                          ApplianceType = new ApplianceTypeData
        //                          {
        //                              // Line modified  - 05/07/2013 - START
        //                              ApplianceType = ApplianceType.APPLIANCETYPE != null ? ApplianceType.APPLIANCETYPE : "",
        //                              ApplianceTypeID = ApplianceType.APPLIANCETYPEID != null ? ApplianceType.APPLIANCETYPEID : 0
        //                              // Line modified  - 05/07/2013 - END
        //                          }
        //                      });

        //    List<ApplianceData> appliancesList = new List<ApplianceData>();
        //    if (appliances.Count() > 0)
        //    {
        //        appliancesList = appliances.ToList();
        //        foreach (ApplianceData appl in appliancesList)
        //        {
        //            appl.isInspected = this.isInspected(appl.ApplianceID);
        //        }
        //    }

        //    return appliancesList;
        //}

        ////Change#31 - Behroz - 12/07/2012 - Start
        #endregion

        #region get All Appliances By GAS Appointment
        //<summary>
        //This function returns all the appliances
        //</summary>
        //<returns>List of appliances data objects</returns>
        public List<ApplianceData> getAllGasAppliancesByPropertyId(string propertyID, int? journalId)
        {

            var appliances = (from papp in context.GS_PROPERTY_APPLIANCE
                              join jor in context.AS_JOURNAL on papp.PROPERTYID equals jor.PROPERTYID
                              join loc in context.GS_LOCATION on papp.LOCATIONID equals loc.LOCATIONID into tempLocation
                              from Locations in tempLocation.DefaultIfEmpty()
                              join manu in context.GS_MANUFACTURER on papp.MANUFACTURERID equals manu.MANUFACTURERID into tempManufacturer
                              from Manufacturer in tempManufacturer.DefaultIfEmpty()
                              join model in context.GS_ApplianceModel on papp.MODELID equals model.ModelID into tempModel
                              from Models in tempModel.DefaultIfEmpty()
                              join type in context.GS_APPLIANCE_TYPE on papp.APPLIANCETYPEID equals type.APPLIANCETYPEID into tempType
                              from ApplianceType in tempType.DefaultIfEmpty()
                              //join isIns in context.P_APPLIANCE_INSPECTION on papp.PROPERTYAPPLIANCEID equals isIns.PROPERTYAPPLIANCEID into Insp
                              //from isInspected in Insp.DefaultIfEmpty()
                              where papp.PROPERTYID == propertyID
                               && jor.ISCURRENT == true
                              //&& jor.JOURNALID == journalId 

                              select new ApplianceData
                              {
                                  ApplianceID = papp.PROPERTYAPPLIANCEID,
                                  SerialNumber = papp.SerialNumber,
                                  FluType = papp.FLUETYPE,
                                  InstalledDate = papp.DATEINSTALLED,
                                  propertyId = propertyID,
                                  isLandlordAppliance = papp.ISLANDLORDAPPLIANCE,
                                  GCNumber = papp.GasCouncilNumber,
                                  Model = papp.MODEL,
                                  ReplacementDate = papp.REPLACEMENTDATE,

                                  // isInspected = isInspected.ISINSPECTED,
                                  ApplianceLocation = new AppliancesLocationData
                                  {
                                      Location = Locations.LOCATION,
                                      LocationID = Locations.LOCATIONID
                                  },
                                  ApplianceManufacturer = new ManufacturerData
                                  {
                                      Manufacturer = Manufacturer.MANUFACTURER,
                                      ManufacturerID = Manufacturer.MANUFACTURERID
                                  },
                                  ApplianceModel = new ApplianceModelData
                                  {
                                      ApplianceModelID = Models.ModelID,
                                      ApplianceModel = Models.Model
                                  },
                                  ApplianceType = new ApplianceTypeData
                                  {
                                      ApplianceType = ApplianceType.APPLIANCETYPE,
                                      ApplianceTypeID = ApplianceType.APPLIANCETYPEID
                                  }


                              });

            List<ApplianceData> appliancesList = new List<ApplianceData>();
            if (appliances.Count() > 0)
            {
                appliancesList = appliances.ToList();

                foreach (ApplianceData applData in appliancesList)
                {
                    applData.isInspected = false;
                    int count = getInspectedAppliancesGas(journalId, applData.ApplianceID);
                    if (count > 0)
                    {
                        applData.isInspected = true;
                    }
                    FaultsDal faultDal = new FaultsDal();
                    applData.ApplianceDefects = faultDal.getFaultsDataGas(applData.ApplianceID, applData.propertyId);
                    applData.ApplianceInspection = getApplianceInspectionByApplianceIDGas(applData.ApplianceID, (int)journalId);
                }
            }

            return appliancesList;
        }
        #endregion

        #region get All Appliances Type
        /// <summary>
        /// This function returns all the Appliances Type
        /// </summary>
        /// <returns>List of Appliances Type data objects</returns>
        public List<ApplianceTypeData> getAllApplianceTypes()
        {
            var applianceType = (from type in context.GS_APPLIANCE_TYPE
                                 select new ApplianceTypeData
                                 {
                                     ApplianceTypeID = type.APPLIANCETYPEID,
                                     ApplianceType = type.APPLIANCETYPE
                                 });



            List<ApplianceTypeData> appliancesTypeList = new List<ApplianceTypeData>();
            if (applianceType.Count() > 0)
            {
                appliancesTypeList = applianceType.ToList();
            }

            return appliancesTypeList;
        }
        #endregion

        #region get All Appliances Models
        /// <summary>
        /// This function returns all the Appliances Model
        /// </summary>
        /// <returns>List of Appliances Model data objects</returns>
        public List<ApplianceModelData> getAllApplianceModel()
        {
            var applianceType = (from model in context.GS_ApplianceModel
                                 select new ApplianceModelData
                                 {
                                     ApplianceModelID = model.ModelID,
                                     ApplianceModel = model.Model
                                 });

            List<ApplianceModelData> appliancesTypeList = new List<ApplianceModelData>();
            if (applianceType.Count() > 0)
            {
                appliancesTypeList = applianceType.ToList();
            }

            return appliancesTypeList;
        }
        #endregion

        #region get All Appliances Manufacturer
        /// <summary>
        /// This function returns all the Appliances Manufacturer
        /// </summary>
        /// <returns>List of Appliances Manufacturer data objects</returns>
        public List<ManufacturerData> getAllApplianceManufacturer()
        {
            var applianceType = (from manufactur in context.GS_MANUFACTURER
                                 select new ManufacturerData
                                 {
                                     ManufacturerID = manufactur.MANUFACTURERID,
                                     Manufacturer = manufactur.MANUFACTURER
                                 });

            List<ManufacturerData> appliancesTypeList = new List<ManufacturerData>();
            if (applianceType.Count() > 0)
            {
                appliancesTypeList = applianceType.ToList();
            }

            return appliancesTypeList;
        }
        #endregion

        #region get All Appliances Locations
        /// <summary>
        /// This function returns all the Appliances Location
        /// </summary>
        /// <returns>List of Appliances Location data objects</returns>
        public List<AppliancesLocationData> getAllApplianceLocations()
        {
            var applianceType = (from location in context.GS_LOCATION
                                 select new AppliancesLocationData
                                 {
                                     LocationID = location.LOCATIONID,
                                     Location = location.LOCATION
                                 });

            List<AppliancesLocationData> appliancesTypeList = new List<AppliancesLocationData>();
            if (applianceType.Count() > 0)
            {
                appliancesTypeList = applianceType.ToList();
            }

            return appliancesTypeList;
        }
        #endregion

        #region Save a new Appliance
        /// <summary>
        /// This function saves a new appliance
        /// </summary>
        /// <param name="appData">appliances data objects</param>
        /// <returns>ID in case of success</returns>
        public int saveAppliance(ApplianceData appData, PropertySurvey_Entities currentContext = null)
        {
            if (currentContext != null) this.context = currentContext;
            bool success = false;
            GS_PROPERTY_APPLIANCE appliance = new GS_PROPERTY_APPLIANCE();

            //using (TransactionScope trans = new TransactionScope())
            //{
            var appl = context.GS_PROPERTY_APPLIANCE.Where(app => app.PROPERTYAPPLIANCEID == appData.ApplianceID);
            if (appl.Count() > 0)
            {
                //fetch the already existed record
                appliance = appl.First();
            }
            if (appData.propertyId != null)
            {
                appliance.PROPERTYID = appData.propertyId;
            }
            appliance.DATEINSTALLED = appData.InstalledDate;
            appliance.FLUETYPE = appData.FluType;

            if (appData.ApplianceLocation != null)
            {
                appliance.LOCATIONID = appData.ApplianceLocation.LocationID;
            }

            if (appData.ApplianceModel != null)
            {
                appliance.MODELID = appData.ApplianceModel.ApplianceModelID;
                appliance.MODEL = appData.ApplianceModel.ApplianceModel;
            }

            if (appData.ApplianceManufacturer != null)
            {
                appliance.MANUFACTURERID = appData.ApplianceManufacturer.ManufacturerID;
            }

            if (appData.ApplianceType != null)
            {
                appliance.APPLIANCETYPEID = appData.ApplianceType.ApplianceTypeID;
            }

            appliance.ISLANDLORDAPPLIANCE = appData.isLandlordAppliance;
            appliance.REPLACEMENTDATE = appData.ReplacementDate;
            appliance.SerialNumber = appData.SerialNumber;
            appliance.GasCouncilNumber = appData.GCNumber;
            if (!(appl.Count() > 0))
            {
                context.AddToGS_PROPERTY_APPLIANCE(appliance);
            }
            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            //trans.Complete();
            success = true;
            //}
            if (currentContext == null) context.AcceptAllChanges();

            if (success == true)
            {
                return appliance.PROPERTYAPPLIANCEID;
            }
            else
            {
                return 0;
            }
        }
        #endregion

        #region Update a existing Appliance
        /// <summary>
        /// This function updates a existing appliance
        /// </summary>
        /// <param name="appData">appliances data objects</param>
        /// <returns>ID in case of success</returns>
        public bool updateAppliance(ApplianceData appData)
        {
            bool success = false;

            using (TransactionScope trans = new TransactionScope())
            {
                var appl = context.GS_PROPERTY_APPLIANCE.Where(app => app.PROPERTYAPPLIANCEID == appData.ApplianceID);
                if (appl.Count() > 0)
                {
                    GS_PROPERTY_APPLIANCE appliance = appl.First();

                    appliance.PROPERTYID = appData.propertyId;
                    appliance.DATEINSTALLED = appData.InstalledDate;
                    appliance.FLUETYPE = appData.FluType;

                    if (appData.ApplianceLocation != null)
                    {
                        appliance.LOCATIONID = appData.ApplianceLocation.LocationID;
                    }

                    if (appData.ApplianceModel != null)
                    {
                        appliance.MODELID = appData.ApplianceModel.ApplianceModelID;
                    }

                    if (appData.ApplianceManufacturer != null)
                    {
                        appliance.MANUFACTURERID = appData.ApplianceManufacturer.ManufacturerID;
                    }

                    if (appData.ApplianceType != null)
                    {
                        appliance.APPLIANCETYPEID = appData.ApplianceType.ApplianceTypeID;
                    }

                    //Change#27 - Behroz - 20/12/2012 - Start
                    appliance.MODEL = appData.Model;
                    appliance.ISLANDLORDAPPLIANCE = appData.isLandlordAppliance;
                    appliance.REPLACEMENTDATE = appData.ReplacementDate;
                    //Change#27 - Behroz - 20/12/2012 - End
                    // Code added  15/05/2013 - START
                    appliance.SerialNumber = appData.SerialNumber;
                    appliance.GasCouncilNumber = appData.GCNumber;
                    // Code added  15/05/2013 - END
                    //if (appData.ApplianceOrgID == 0)
                    //{
                    //    appliance.ORGID = null;
                    //}
                    //else
                    //{
                    //    appliance.ORGID = appData.ApplianceOrgID;
                    //}

                    context.SaveChanges();
                    trans.Complete();
                    success = true;
                }
            }
            return success;
        }
        #endregion

        #region save new Appliances Locations
        /// <summary>
        /// This function save new Appliances Location
        /// </summary>
        /// <returns>true for success</returns>
        public int saveApplianceLocation(AppliancesLocationData location, PropertySurvey_Entities currentContext = null)
        {
            if (currentContext != null) this.context = currentContext;
            bool success = false;
            GS_LOCATION app_location = new GS_LOCATION();

            //using (TransactionScope trans = new TransactionScope())
            //{
            var apptRecord = context.GS_LOCATION.Where(app => app.LOCATIONID == location.LocationID);
            if (apptRecord.Count() <= 0)
            {
                app_location.LOCATION = location.Location;
                context.AddToGS_LOCATION(app_location);
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                //trans.Complete();
                success = true;
            }

            if (currentContext == null) context.AcceptAllChanges();

            if (success == true)
            {
                return app_location.LOCATIONID;
            }
            else
            {
                return apptRecord.First().LOCATIONID;
            }
            //}
        }
        #endregion

        #region save new Appliances Type
        /// <summary>
        /// This function save new Appliances Type
        /// </summary>
        /// <returns>new inserted id in case of success</returns>
        public int saveApplianceType(ApplianceTypeData type, PropertySurvey_Entities currentContext = null)
        {
            if (currentContext != null) this.context = currentContext;

            bool success = false;
            GS_APPLIANCE_TYPE app_type = new GS_APPLIANCE_TYPE();

            //using (TransactionScope trans = new TransactionScope())
            //{
            var apptRecord = context.GS_APPLIANCE_TYPE.Where(app => app.APPLIANCETYPEID == type.ApplianceTypeID);
            if (apptRecord.Count() <= 0)
            {
                app_type.APPLIANCETYPE = type.ApplianceType;
                context.AddToGS_APPLIANCE_TYPE(app_type);
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                //trans.Complete();
                success = true;
            }

            if (currentContext == null) context.AcceptAllChanges();

            if (success == true)
            {
                return app_type.APPLIANCETYPEID;
            }
            else
            {
                return apptRecord.First().APPLIANCETYPEID;
            }
            //}
        }
        #endregion

        #region save new Appliances Model
        /// <summary>
        /// This function save new Appliances Model
        /// </summary>
        /// <returns>new inserted id in case of success</returns>
        public int saveApplianceModel(ApplianceModelData model, PropertySurvey_Entities currentContext = null)
        {
            if (currentContext != null) this.context = currentContext;

            bool success = false;
            GS_ApplianceModel app_model = new GS_ApplianceModel();

            //using (TransactionScope trans = new TransactionScope())
            //{
            var apptRecord = context.GS_ApplianceModel.Where(app => app.ModelID == model.ApplianceModelID);
            if (apptRecord.Count() <= 0)
            {
                app_model.Model = model.ApplianceModel;
                context.AddToGS_ApplianceModel(app_model);
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                //trans.Complete();
                success = true;
            }

            if (currentContext == null) context.AcceptAllChanges();

            if (success == true)
            {
                return app_model.ModelID;
            }
            else
            {
                return apptRecord.First().ModelID;
            }
            //}
        }
        #endregion

        #region save new Appliances manufacturer
        /// <summary>
        /// This function save new Appliances manufacturer
        /// </summary>
        /// <returns>new inserted id in case of success</returns>
        public int saveApplianceManufacturer(ManufacturerData manufacture, PropertySurvey_Entities currentContext = null)
        {
            if (currentContext != null) this.context = currentContext;
            bool success = false;

            GS_MANUFACTURER app_manufacture = new GS_MANUFACTURER();

            //using (TransactionScope trans = new TransactionScope())
            //{
            var apptRecord = context.GS_MANUFACTURER.Where(app => app.MANUFACTURERID == manufacture.ManufacturerID);
            if (apptRecord.Count() <= 0)
            {
                app_manufacture.MANUFACTURER = manufacture.Manufacturer;
                context.AddToGS_MANUFACTURER(app_manufacture);
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                //trans.Complete();
                success = true;
            }

            if (currentContext == null) context.AcceptAllChanges();

            if (success == true)
            {
                return app_manufacture.MANUFACTURERID;
            }
            else
            {
                return apptRecord.First().MANUFACTURERID;
            }
            //}
        }
        #endregion

        #region get number of inspected Appliances

        /// <summary>
        /// This function returns number of inspected Appliances
        /// </summary>
        /// <returns>number of inspected Appliances</returns>
        public int getInspectedAppliances(int appointmentID)
        {
            var count = (from app in context.GS_ApplianceInspection
                         where app.APPOINTMENTID == appointmentID
                         select app.APPLIANCEINSPECTIONID);
            return count.Count();
        }

        //Change#25 - Behroz - 20/12/2012 - Start
        /// <summary>
        /// This function returns number of inspected Appliances for gas
        /// </summary>
        /// <returns>number of inspected Appliances</returns>
        //public int getInspectedAppliances(string propertyID)
        //{
        //    var count = (from app in context.GS_PROPERTY_APPLIANCE
        //                 where app.PROPERTYID == propertyID
        //                 select app.PROPERTYAPPLIANCEID);

        //    return count.Count();
        //}
        public int getInspectedAppliancesGas(int? journalId, int applianceId)
        {
            var count = (from app in context.P_APPLIANCE_INSPECTION
                         where app.Journalid == journalId && app.PROPERTYAPPLIANCEID == applianceId
                         select app.PROPERTYAPPLIANCEID);

            return count.Count();
        }
        //Change#25 - Behroz - 20/12/2012 - End

        #endregion

        #region get ApplianceInspection by ApplianceID
        /// <summary>
        /// This function returns all the ApplianceInspection
        /// </summary>
        /// <returns>List of ApplianceInspection data objects</returns>
        public ApplianceInspectionData getApplianceInspectionByApplianceID(int ApplianceID, int appointmentID)
        {
            var appliances = (from AppIns in context.GS_ApplianceInspection
                              where AppIns.APPLIANCEID == ApplianceID && AppIns.APPOINTMENTID == appointmentID
                              select new ApplianceInspectionData
                              {
                                  adequateVentilation = AppIns.ADEQUATEVENTILATION,
                                  APPLIANCEID = AppIns.APPLIANCEID,
                                  inspectionID = AppIns.APPLIANCEINSPECTIONID,
                                  applianceSafeToUse = AppIns.APPLIANCESAFETOUSE,
                                  applianceServiced = AppIns.APPLIANCESERVICED,
                                  APPOINTMENTID = AppIns.APPOINTMENTID,
                                  combustionReading = AppIns.COMBUSTIONREADING,
                                  fluePerformanceChecks = AppIns.FLUEPERFORMANCECHECKS,
                                  flueVisualCondition = AppIns.FLUEVISUALCONDITION,
                                  inspectionDate = AppIns.INSPECTIONDATE,
                                  operatingPressure = AppIns.OPERATINGPRESSURE,
                                  //isInspected = AppIns.ISINSPECTED,
                                  safetyDeviceOperational = AppIns.SAFETYDEVICEOPERATIONAL,
                                  satisfactoryTermination = AppIns.SATISFACTORYTERMINATION,
                                  smokePellet = AppIns.SMOKEPELLET,
                                  spillageTest = AppIns.SPILLAGETEST
                              });

            ApplianceInspectionData appliancesList = new ApplianceInspectionData();
            if (appliances.Count() > 0)
            {
                appliancesList = appliances.First();
            }

            return appliancesList;
        }

        //Change#26 - Behroz - 20/12/2012 - Start
        /// <summary>
        /// This function returns all the ApplianceInspection for gas
        /// </summary>
        /// <returns>List of ApplianceInspection data objects</returns>
        //public ApplianceInspectionData getApplianceInspectionByApplianceID(int ApplianceID, string propertyID)
        //{
        //    var appliance = (from propApp in context.GS_PROPERTY_APPLIANCE
        //                     join AppIns in context.P_APPLIANCE_INSPECTION on
        //                     propApp.PROPERTYAPPLIANCEID equals AppIns.PROPERTYAPPLIANCEID
        //                     where propApp.PROPERTYAPPLIANCEID == ApplianceID && propApp.PROPERTYID == propertyID
        //                     select new ApplianceInspectionData {
        //                         ADEQUATEVENTILATION = AppIns.ADEQUATEVENTILATION,
        //                         APPLIANCEID = AppIns.PROPERTYAPPLIANCEID,
        //                         APPLIANCEINSPECTIONID = AppIns.APPLIANCEINSPECTIONID,
        //                         APPLIANCESAFETOUSE = AppIns.APPLIANCESAFETOUSE,
        //                         APPLIANCESERVICED = AppIns.APPLIANCESERVICED,                                 
        //                         COMBUSTIONREADING = AppIns.COMBUSTIONREADING,
        //                         FLUEPERFORMANCECHECKS = AppIns.FLUEPERFORMANCECHECKS,
        //                         FLUEVISUALCONDITION = AppIns.FLUEVISUALCONDITION,
        //                         INSPECTIONDATE = AppIns.INSPECTIONDATE,
        //                         OPERATINGPRESSURE = AppIns.OPERATINGPRESSURE,
        //                         ISINSPECTED = AppIns.ISINSPECTED,
        //                         SAFETYDEVICEOPERATIONAL = AppIns.SAFETYDEVICEOPERATIONAL,
        //                         SATISFACTORYTERMINATION = AppIns.SATISFACTORYTERMINATION,
        //                         SMOKEPELLET = AppIns.SMOKEPELLET,
        //                         SPILLAGETEST = AppIns.SPILLAGETEST,
        //                         INSPECTEDBY = AppIns.INSPECTEDBY.Value                                 
        //                     });            

        //    ApplianceInspectionData appliancesList = new ApplianceInspectionData();
        //    if (appliance.Count() > 0)
        //    {
        //        appliancesList = appliance.First();
        //    }

        //    return appliancesList;
        //}
        public ApplianceInspectionData getApplianceInspectionByApplianceIDGas(int ApplianceID, int journalId)
        {
            var appliance = (from AppIns in context.P_APPLIANCE_INSPECTION
                             where AppIns.PROPERTYAPPLIANCEID == ApplianceID && AppIns.Journalid == journalId
                             select new ApplianceInspectionData
                             {
                                 adequateVentilation = AppIns.ADEQUATEVENTILATION,
                                 APPLIANCEID = AppIns.PROPERTYAPPLIANCEID,
                                 inspectionID = AppIns.APPLIANCEINSPECTIONID,
                                 applianceSafeToUse = AppIns.APPLIANCESAFETOUSE,
                                 applianceServiced = AppIns.APPLIANCESERVICED,
                                 combustionReading = AppIns.COMBUSTIONREADING,
                                 fluePerformanceChecks = AppIns.FLUEPERFORMANCECHECKS,
                                 flueVisualCondition = AppIns.FLUEVISUALCONDITION,
                                 inspectionDate = AppIns.INSPECTIONDATE,
                                 operatingPressure = AppIns.OPERATINGPRESSURE,
                                 operatingPressureUnit = AppIns.OperatingPressureUnit,
                                 isInspected = AppIns.ISINSPECTED,
                                 safetyDeviceOperational = AppIns.SAFETYDEVICEOPERATIONAL,
                                 satisfactoryTermination = AppIns.SATISFACTORYTERMINATION,
                                 smokePellet = AppIns.SMOKEPELLET,
                                 spillageTest = AppIns.SPILLAGETEST,
                                 inspectedBy = AppIns.INSPECTEDBY.Value
                             });

            ApplianceInspectionData appliancesList = new ApplianceInspectionData();
            if (appliance.Count() > 0)
            {
                appliancesList = appliance.First();
            }

            return appliancesList;
        }
        //Change#26 - Behroz - 20/12/2012 - End

        #endregion

        #region get ApplianceInspection details by Appointment Id
        /// <summary>
        /// This function returns all the ApplianceInspection details
        /// </summary>
        /// <returns>List of ApplianceInspection data objects</returns>
        public List<ApplianceInspectionData> getApplianceInspectionDetails(int appointmentID)
        {
            var appliances = (from AppIns in context.GS_ApplianceInspection
                              where AppIns.APPOINTMENTID == appointmentID
                              select new ApplianceInspectionData
                              {
                                  adequateVentilation = AppIns.ADEQUATEVENTILATION,
                                  APPLIANCEID = AppIns.APPLIANCEID,
                                  inspectionID = AppIns.APPLIANCEINSPECTIONID,
                                  applianceSafeToUse = AppIns.APPLIANCESAFETOUSE,
                                  applianceServiced = AppIns.APPLIANCESERVICED,
                                  APPOINTMENTID = AppIns.APPOINTMENTID,
                                  combustionReading = AppIns.COMBUSTIONREADING,
                                  fluePerformanceChecks = AppIns.FLUEPERFORMANCECHECKS,
                                  flueVisualCondition = AppIns.FLUEVISUALCONDITION,
                                  inspectionDate = AppIns.INSPECTIONDATE,
                                  operatingPressure = AppIns.OPERATINGPRESSURE,
                                  isInspected = AppIns.ISINSPECTED,
                                  safetyDeviceOperational = AppIns.SAFETYDEVICEOPERATIONAL,
                                  satisfactoryTermination = AppIns.SATISFACTORYTERMINATION,
                                  smokePellet = AppIns.SMOKEPELLET,
                                  spillageTest = AppIns.SPILLAGETEST
                              });

            List<ApplianceInspectionData> appliancesList = new List<ApplianceInspectionData>();
            if (appliances.Count() > 0)
            {
                appliancesList = appliances.ToList();
            }

            return appliancesList;
        }

        //Change#40 - Behroz - 26/12/2012 - Start
        ///// <summary>
        ///// This function returns all the ApplianceInspection details
        ///// </summary>
        ///// <returns>List of ApplianceInspection data objects</returns>
        //public List<ApplianceInspectionData> getApplianceInspectionDetails(string propertyID)
        //{
        //    var appliances = (from propApp in context.GS_PROPERTY_APPLIANCE
        //                      join AppIns in context.P_APPLIANCE_INSPECTION on
        //                      propApp.PROPERTYAPPLIANCEID equals AppIns.PROPERTYAPPLIANCEID
        //                      where propApp.PROPERTYID == propertyID
        //                      select new ApplianceInspectionData
        //                      {
        //                          ADEQUATEVENTILATION = AppIns.ADEQUATEVENTILATION,
        //                          APPLIANCEID = AppIns.PROPERTYAPPLIANCEID,
        //                          APPLIANCEINSPECTIONID = AppIns.APPLIANCEINSPECTIONID,
        //                          APPLIANCESAFETOUSE = AppIns.APPLIANCESAFETOUSE,
        //                          APPLIANCESERVICED = AppIns.APPLIANCESERVICED,
        //                          COMBUSTIONREADING = AppIns.COMBUSTIONREADING,
        //                          FLUEPERFORMANCECHECKS = AppIns.FLUEPERFORMANCECHECKS,
        //                          FLUEVISUALCONDITION = AppIns.FLUEVISUALCONDITION,
        //                          INSPECTIONDATE = AppIns.INSPECTIONDATE,
        //                          OPERATINGPRESSURE = AppIns.OPERATINGPRESSURE,
        //                          ISINSPECTED = AppIns.ISINSPECTED,
        //                          SAFETYDEVICEOPERATIONAL = AppIns.SAFETYDEVICEOPERATIONAL,
        //                          SATISFACTORYTERMINATION = AppIns.SATISFACTORYTERMINATION,
        //                          SMOKEPELLET = AppIns.SMOKEPELLET,
        //                          SPILLAGETEST = AppIns.SPILLAGETEST,
        //                          INSPECTEDBY = AppIns.INSPECTEDBY.Value  
        //                      });

        //    List<ApplianceInspectionData> appliancesList = new List<ApplianceInspectionData>();
        //    if (appliances.Count() > 0)
        //    {
        //        appliancesList = appliances.ToList();
        //    }

        //    return appliancesList;
        //}
        //Change#40 - Behroz - 26/12/2012 - End
        /// <summary>
        /// This function returns all the ApplianceInspection details
        /// </summary>
        /// <returns>List of ApplianceInspection data objects</returns>
        public List<ApplianceInspectionData> getApplianceInspectionDetailsGas(int journalId)
        {
            var appliances = (from AppIns in context.P_APPLIANCE_INSPECTION
                              where AppIns.Journalid == journalId
                              select new ApplianceInspectionData
                              {
                                  adequateVentilation = AppIns.ADEQUATEVENTILATION,
                                  APPLIANCEID = AppIns.PROPERTYAPPLIANCEID,
                                  inspectionID = AppIns.APPLIANCEINSPECTIONID,
                                  applianceSafeToUse = AppIns.APPLIANCESAFETOUSE,
                                  applianceServiced = AppIns.APPLIANCESERVICED,
                                  combustionReading = AppIns.COMBUSTIONREADING,
                                  fluePerformanceChecks = AppIns.FLUEPERFORMANCECHECKS,
                                  flueVisualCondition = AppIns.FLUEVISUALCONDITION,
                                  inspectionDate = AppIns.INSPECTIONDATE,
                                  operatingPressure = AppIns.OPERATINGPRESSURE,
                                  isInspected = AppIns.ISINSPECTED,
                                  safetyDeviceOperational = AppIns.SAFETYDEVICEOPERATIONAL,
                                  satisfactoryTermination = AppIns.SATISFACTORYTERMINATION,
                                  smokePellet = AppIns.SMOKEPELLET,
                                  spillageTest = AppIns.SPILLAGETEST,
                                  inspectedBy = AppIns.INSPECTEDBY.Value,
                                  JOURNALID = AppIns.Journalid
                              });

            List<ApplianceInspectionData> appliancesList = new List<ApplianceInspectionData>();
            if (appliances.Count() > 0)
            {
                appliancesList = appliances.ToList();
            }

            return appliancesList;
        }
        #endregion

        #region Save a new ApplianceInspection
        /// <summary>
        /// This function saves a new ApplianceInspection
        /// </summary>
        /// <param name="appData">ApplianceInspection data objects</param>
        /// <returns>ID in case of success</returns>
        public int saveApplianceInspection(ApplianceInspectionData appData)
        {
            bool success = false;
            GS_ApplianceInspection appliance = new GS_ApplianceInspection();

            //using (TransactionScope trans = new TransactionScope())
            //{
            appliance.ADEQUATEVENTILATION = appData.adequateVentilation;
            appliance.APPLIANCEID = (int)appData.APPLIANCEID;
            //appliance.APPLIANCEINSPECTIONID = appData.APPLIANCEINSPECTIONID;
            appliance.APPLIANCESAFETOUSE = appData.applianceSafeToUse;
            appliance.APPLIANCESERVICED = appData.applianceServiced;
            appliance.APPOINTMENTID = appData.APPOINTMENTID;
            appliance.COMBUSTIONREADING = appData.combustionReading;
            appliance.FLUEPERFORMANCECHECKS = appData.fluePerformanceChecks;
            appliance.FLUEVISUALCONDITION = appData.flueVisualCondition;
            appliance.INSPECTIONDATE = appData.inspectionDate;
            appliance.OPERATINGPRESSURE = appData.operatingPressure;
            appliance.ISINSPECTED = (bool)appData.isInspected;
            appliance.SAFETYDEVICEOPERATIONAL = appData.safetyDeviceOperational;
            appliance.SATISFACTORYTERMINATION = appData.satisfactoryTermination;
            appliance.SMOKEPELLET = appData.smokePellet;
            appliance.SPILLAGETEST = appData.spillageTest;

            context.AddToGS_ApplianceInspection(appliance);
            context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
            //trans.Complete();
            success = true;
            //}

            if (success == true)
            {
                return appliance.APPLIANCEINSPECTIONID;
            }
            else
            {
                return 0;
            }
        }


        public int saveApplianceInspectionGas(ApplianceInspectionData appData, PropertySurvey_Entities currentContext = null)
        {
            if (currentContext != null) this.context = currentContext;
            bool success = false;
            P_APPLIANCE_INSPECTION appliance = new P_APPLIANCE_INSPECTION();

            //var PropertyApplianceExist = context.P_APPLIANCE_INSPECTION.Where(app => app.PROPERTYAPPLIANCEID == appData.APPLIANCEID);
            var PropertyApplianceExist = context.P_APPLIANCE_INSPECTION.Where(app => app.PROPERTYAPPLIANCEID == appData.APPLIANCEID && app.Journalid == appData.JOURNALID);

            if (PropertyApplianceExist.Count() > 0)
            {
                appliance = PropertyApplianceExist.First();

                #region "Update"

                //using (TransactionScope trans = new TransactionScope())
                //{
                appliance.ADEQUATEVENTILATION = appData.adequateVentilation;
                appliance.PROPERTYAPPLIANCEID = (int)appData.APPLIANCEID;
                appliance.APPLIANCESAFETOUSE = appData.applianceSafeToUse;
                appliance.APPLIANCESERVICED = appData.applianceServiced;
                appliance.COMBUSTIONREADING = appData.combustionReading;
                appliance.FLUEPERFORMANCECHECKS = appData.fluePerformanceChecks;
                appliance.FLUEVISUALCONDITION = appData.flueVisualCondition;
                appliance.INSPECTIONDATE = appData.inspectionDate;
                appliance.OPERATINGPRESSURE = appData.operatingPressure;
                appliance.ISINSPECTED = (bool)appData.isInspected;
                appliance.SAFETYDEVICEOPERATIONAL = appData.safetyDeviceOperational;
                appliance.SATISFACTORYTERMINATION = appData.satisfactoryTermination;
                appliance.SMOKEPELLET = appData.smokePellet;
                appliance.SPILLAGETEST = appData.spillageTest;
                appliance.INSPECTEDBY = appData.inspectedBy;
                appliance.Journalid = appData.JOURNALID;
                appliance.OperatingPressureUnit = appData.operatingPressureUnit;

                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                //trans.Complete();
                //}
                success = true;

                if (currentContext == null) context.AcceptAllChanges();

                if (success)
                    return 1;
                else return 0;

                #endregion
            }
            else
            {
                #region "Insert"

                //Insert if the Property Appliance Id does not exist
                using (TransactionScope trans = new TransactionScope())
                {
                    appliance.ADEQUATEVENTILATION = appData.adequateVentilation;
                    appliance.PROPERTYAPPLIANCEID = (int)appData.APPLIANCEID;
                    appliance.APPLIANCESAFETOUSE = appData.applianceSafeToUse;
                    appliance.APPLIANCESERVICED = appData.applianceServiced;
                    appliance.COMBUSTIONREADING = appData.combustionReading;
                    appliance.FLUEPERFORMANCECHECKS = appData.fluePerformanceChecks;
                    appliance.FLUEVISUALCONDITION = appData.flueVisualCondition;
                    appliance.INSPECTIONDATE = appData.inspectionDate;
                    appliance.OPERATINGPRESSURE = appData.operatingPressure;
                    appliance.ISINSPECTED = (bool)appData.isInspected;
                    appliance.SAFETYDEVICEOPERATIONAL = appData.safetyDeviceOperational;
                    appliance.SATISFACTORYTERMINATION = appData.satisfactoryTermination;
                    appliance.SMOKEPELLET = appData.smokePellet;
                    appliance.SPILLAGETEST = appData.spillageTest;
                    appliance.INSPECTEDBY = appData.inspectedBy;
                    appliance.Journalid = appData.JOURNALID;
                    appliance.OperatingPressureUnit = appData.operatingPressureUnit;

                    context.AddToP_APPLIANCE_INSPECTION(appliance);
                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    trans.Complete();
                    success = true;
                }

                if (success == true)
                {
                    return appliance.APPLIANCEINSPECTIONID;
                }
                else
                {
                    return 0;
                }

                #endregion
            }
        }
        #endregion

        #region "Get Appliance Count"

        //Change#50 - Behroz - 16/01/2013 - Start
        /// <summary>
        /// This function returns the appliance count 
        /// </summary>
        /// <param name="propertyId"></param>
        /// <returns></returns>
        public int getApplianceCount(string propertyId)
        {
            var appliances = context.GS_PROPERTY_APPLIANCE.Where(app => app.PROPERTYID == propertyId);
            return appliances.Count();
        }

        //Change#50 - Behroz - 16/01/2013 - End
        #endregion

        public bool isInspected(int applianceID) //, int appointmentID
        {
            var appId = (from appIns in context.GS_ApplianceInspection
                         where appIns.APPLIANCEID == applianceID //&& appIns.APPOINTMENTID == appointmentID
                         select appIns.APPLIANCEINSPECTIONID);
            if (appId.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool isInspectedGas(int propertyApplianceId, int journalId)
        {
            var appId = (from appIns in context.P_APPLIANCE_INSPECTION
                         where appIns.PROPERTYAPPLIANCEID == propertyApplianceId && appIns.Journalid == journalId
                         select appIns.APPLIANCEINSPECTIONID);
            if (appId.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #region get Detector Count
        /// <summary>
        /// This function returns detector count against a property
        /// </summary>
        /// <returns>Property Attributes Object</returns>
        public string getDetectorCount(string propertyID, string itemName)
        {
            string result = "0";

            var itemParam = (from ip in context.PA_ITEM_PARAMETER
                             join p in context.PA_PARAMETER on ip.ParameterId equals p.ParameterID
                             join i in context.PA_ITEM on ip.ItemId equals i.ItemID
                             where p.ParameterName == "Quantity"
                            && i.ItemName == itemName
                             select ip.ItemParamID);

            if (itemParam.Count() > 0)
            {
                int ItemParamID = itemParam.First();
                var detectorsCount = context.PA_PROPERTY_ATTRIBUTES.Where(patt => patt.PROPERTYID == propertyID && patt.ITEMPARAMID == ItemParamID);

                if (detectorsCount.Count() > 0)
                {
                    result = detectorsCount.First().PARAMETERVALUE;
                }

            }
            return result;
        }

        #endregion

        #region update Property Detector Count
        /// <summary>
        /// This function returns update Property Detector Count against a property
        /// </summary>
        /// <returns>Property Attributes Object</returns>
        public bool updatePropertyDetectorCount(string propertyID, string itemName, string quantity, int? updatedBy)
        {
            bool result = false;

            var itemParam = (from ip in context.PA_ITEM_PARAMETER
                             join p in context.PA_PARAMETER on ip.ParameterId equals p.ParameterID
                             join i in context.PA_ITEM on ip.ItemId equals i.ItemID
                             where p.ParameterName == "Quantity"
                            && i.ItemName == itemName
                             select ip.ItemParamID);

            if (itemParam.Count() > 0)
            {
                int ItemParamID = itemParam.First();
                var detectorsCount = context.PA_PROPERTY_ATTRIBUTES.Where(patt => patt.PROPERTYID == propertyID && patt.ITEMPARAMID == ItemParamID);

                if (detectorsCount.Count() > 0)
                {

                    PA_PROPERTY_ATTRIBUTES attributes = detectorsCount.First();
                    attributes.PARAMETERVALUE = quantity;
                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    result = true;
                }
                else
                {
                    PA_PROPERTY_ATTRIBUTES attributes = new PA_PROPERTY_ATTRIBUTES();
                    attributes.PROPERTYID = propertyID;
                    attributes.ITEMPARAMID = ItemParamID;
                    attributes.PARAMETERVALUE = quantity;
                    attributes.IsCheckBoxSelected = false;
                    attributes.UPDATEDON = DateTime.Now;
                    attributes.UPDATEDBY = updatedBy;
                    context.AddToPA_PROPERTY_ATTRIBUTES(attributes);
                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    result = true;
                }

            }
            return result;
        }

        #endregion

        #region Update Detector Count
        /// <summary>
        /// This function updates detector count against a property
        /// </summary>
        /// <param name="propertyId"></param>
        /// <param name="detectorCountData">DetectorCountData object</param>
        /// <returns>True in case of success</returns>  
        public ResultBoolData updateDetectorCount(string propertyId, DetectorCountData detectorCountData, int? updatedBy, PropertySurvey_Entities currentContext = null)
        {
            if (currentContext != null) this.context = currentContext;
            bool success = false;
            ResultBoolData resultData = new ResultBoolData();

            //using (TransactionScope trans = new TransactionScope())
            //{
            if (detectorCountData.detectorTypeId == 1)
            {
                success = updatePropertyDetectorCount(propertyId, "Smoke", detectorCountData.detectorCount.ToString(), updatedBy);

            }
            if (detectorCountData.detectorTypeId == 2)
            {
                success = updatePropertyDetectorCount(propertyId, "CO", detectorCountData.detectorCount.ToString(), updatedBy);
            }
            //trans.Complete();
            //}

            if (currentContext == null) context.AcceptAllChanges();

            resultData.result = success;
            return resultData;
        }
        #endregion

        #region Add Update Detector Info
        /// <summary>
        /// This function adds or updates detector info
        /// </summary>
        /// <param name="detectorType"></param>
        /// <param name="detectorInspectionData">DetectorInspectionData object</param>
        /// <returns>ID in case of success</returns>  
        public ResultIntData addUpdateDetectorInfo(DetectorInspectionData detectorInspectionData, int detectorTypeId, bool isInspected, int? journalId, string propertyId, PropertySurvey_Entities currentContext = null)
        {
            if (currentContext != null) this.context = currentContext;
            int detectorInspectionId = 0;
            ResultIntData resultData = new ResultIntData();

            //using (TransactionScope trans = new TransactionScope())
            //{
            if (detectorTypeId == 1)
            {
                var smokeDetectorInfo = context.P_Smoke_Inspection.Where(psi => psi.PropertyId == propertyId && psi.Journalid == journalId);

                if (smokeDetectorInfo.Count() > 0)
                {
                    P_Smoke_Inspection smokeIspectionObject = smokeDetectorInfo.First();
                    smokeIspectionObject.DateStamp = detectorInspectionData.inspectionDate;
                    smokeIspectionObject.DetectorTest = detectorInspectionData.detectorTest;
                    smokeIspectionObject.IsInspected = isInspected;
                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    //trans.Complete();
                    detectorInspectionId = smokeIspectionObject.DectectorId;
                }
                else
                {
                    P_Smoke_Inspection smokeIspectionObject = new P_Smoke_Inspection();
                    smokeIspectionObject.DateStamp = detectorInspectionData.inspectionDate;
                    smokeIspectionObject.DetectorTest = detectorInspectionData.detectorTest;
                    smokeIspectionObject.IsInspected = isInspected;
                    smokeIspectionObject.Journalid = journalId;
                    smokeIspectionObject.PropertyId = propertyId;
                    context.AddToP_Smoke_Inspection(smokeIspectionObject);
                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    //trans.Complete();
                    detectorInspectionId = smokeIspectionObject.DectectorId;
                }
            }
            else if (detectorTypeId == 2)
            {
                var coDetectorInfo = context.P_CO2_Inspection.Where(pci => pci.PropertyId == propertyId && pci.Journalid == journalId);

                if (coDetectorInfo.Count() > 0)
                {
                    P_CO2_Inspection coIspectionObject = coDetectorInfo.First();
                    coIspectionObject.DetectorTest = detectorInspectionData.detectorTest;
                    coIspectionObject.IsInspected = isInspected;
                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    //trans.Complete();
                    detectorInspectionId = coIspectionObject.DectectorId;
                }
                else
                {
                    P_CO2_Inspection coIspectionObject = new P_CO2_Inspection();
                    coIspectionObject.DateStamp = detectorInspectionData.inspectionDate;
                    coIspectionObject.DetectorTest = detectorInspectionData.detectorTest;
                    coIspectionObject.IsInspected = isInspected;
                    coIspectionObject.Journalid = journalId;
                    coIspectionObject.PropertyId = propertyId;
                    context.AddToP_CO2_Inspection(coIspectionObject);
                    context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                    //trans.Complete();
                    detectorInspectionId = coIspectionObject.DectectorId;
                }
            }
            //}

            if (currentContext == null) context.AcceptAllChanges();

            resultData.result = detectorInspectionId;

            return resultData;
        }
        #endregion

        #region Get Detector List

        public List<DetectorCountData> getDetectorList(string propertyId, int appointmentId)
        {

            List<DetectorCountData> detectorList = new List<DetectorCountData>();
            FaultsDal fltDal = new FaultsDal();
            var detectorsCount = context.P_ATTRIBUTES.Where(patt => patt.PROPERTYID == propertyId);

            //if (detectorsCount.Count() > 0)
            //{
            DetectorCountData smokeDetectorCount = new DetectorCountData();
            DetectorCountData CO2DetectorCount = new DetectorCountData();

            smokeDetectorCount.detectorType = MessageConstants.SmokeDetectorName;
            smokeDetectorCount.detectorTypeId = getDetectorId(MessageConstants.SmokeDetectorName);
            smokeDetectorCount.detectorCount = 0;

            smokeDetectorCount.detectorCount = Convert.ToInt32(getDetectorCount(propertyId, "Smoke"));
            smokeDetectorCount.propertyId = propertyId;
            smokeDetectorCount.isInspected = fltDal.isInspectedSmokeDetector(propertyId, appointmentId);
            smokeDetectorCount.detectorInspection = fltDal.smokeDetectorInspection(propertyId, appointmentId);

            CO2DetectorCount.detectorType = MessageConstants.CODetectorName;
            CO2DetectorCount.detectorTypeId = getDetectorId(MessageConstants.CODetectorName);
            CO2DetectorCount.detectorCount = 0;

            CO2DetectorCount.detectorCount = Convert.ToInt32(getDetectorCount(propertyId, "CO"));
            CO2DetectorCount.propertyId = propertyId;
            CO2DetectorCount.isInspected = fltDal.isInspectedCO2Detector(propertyId, appointmentId);
            CO2DetectorCount.detectorInspection = fltDal.CO2DetectorInspection(propertyId, appointmentId);
            detectorList.Add(smokeDetectorCount);
            detectorList.Add(CO2DetectorCount);

            //}
            return detectorList;
        }

        public int getDetectorId(string detectorType)
        {
            return context.AS_DetectorType.Where(item => item.DetectorType.ToLower() == detectorType).FirstOrDefault().DetectorTypeId;
        }
        #endregion

        #region Update appliance defect
        /// <summary>
        /// Update appliance defect
        /// </summary>
        /// <param name="appDefectData"></param>
        /// <returns></returns>
        public bool updateApplianceDefects(ApplianceDefectData appDefectData)
        {
            bool success = false;

            using (TransactionScope trans = new TransactionScope())
            {
                P_PROPERTY_APPLIANCE_DEFECTS appDefect = new P_PROPERTY_APPLIANCE_DEFECTS();
                var appl = context.P_PROPERTY_APPLIANCE_DEFECTS.Where(app => app.PropertyDefectId == appDefectData.PropertyDefectId);
                if (appl.Count() > 0)
                {
                    appDefect = appl.First();
                }
                if (appDefectData.ApplianceID != 0)
                {
                    appDefect.ApplianceId = appDefectData.ApplianceID;
                }
                if (appDefectData.CategoryID != null)
                {
                    appDefect.CategoryId = (int)appDefectData.CategoryID;
                }
                if (appDefectData.DefectDate != null)
                {
                    appDefect.DefectDate = appDefectData.DefectDate;
                }
                if (appDefectData.IsDefectIdentified != null)
                {
                    appDefect.IsDefectIdentified = appDefectData.IsDefectIdentified;
                }
                if (appDefectData.IsWarningFixed != null)
                {
                    appDefect.IsWarningFixed = appDefectData.IsWarningFixed;
                }
                if (appDefectData.IsWarningIssued != null)
                {
                    appDefect.IsWarningIssued = appDefectData.IsWarningIssued;
                }
                if (appDefectData.SerialNumber != null)
                {
                    appDefect.SerialNumber = appDefectData.SerialNumber;
                }
                if (appl.Count() <= 0)
                {
                    context.AddToP_PROPERTY_APPLIANCE_DEFECTS(appDefect);
                }
                context.SaveChanges();
                context.SaveChanges(SaveOptions.AcceptAllChangesAfterSave);
                trans.Complete();
                success = true;
            }

            return success;
        }
        #endregion
    }
}
