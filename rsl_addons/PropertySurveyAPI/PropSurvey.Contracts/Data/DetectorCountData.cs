﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class DetectorCountData
    {
        [DataMember]
        public int detectorCount { get; set; }

        [DataMember]
        public string detectorType { get; set; }

        [DataMember]
        public int detectorTypeId { get; set; }

        [DataMember]
        public bool isInspected { get; set; }

        [DataMember]
        public string propertyId { get; set; }
        [DataMember]
        public List<FaultsDataGas> detectorDefects = new List<FaultsDataGas>();

     
        [DataMember]
        public DetectorInspectionData detectorInspection = new DetectorInspectionData();
    }
}
