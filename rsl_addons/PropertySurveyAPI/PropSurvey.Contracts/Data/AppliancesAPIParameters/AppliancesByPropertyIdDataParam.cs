﻿using System;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class AppliancesByPropertyIdDataParam
    {
        [DataMember]
        public string propertyId { get; set; }
        [DataMember]
        public int appointmentid { get; set; } 
        [DataMember]
        public string username { get; set; }
        [DataMember]
        public int? journalId { get; set; } 
       

    }
}
