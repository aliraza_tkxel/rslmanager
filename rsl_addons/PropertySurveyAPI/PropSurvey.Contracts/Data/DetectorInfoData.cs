﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    //Class added  - 24/05/2013
    public class DetectorInfoData
    {
        [DataMember]
        public int detectorCount { get; set; }

        [DataMember]
        public DetectorInspectionData InspectionData = new DetectorInspectionData();
    }
}
