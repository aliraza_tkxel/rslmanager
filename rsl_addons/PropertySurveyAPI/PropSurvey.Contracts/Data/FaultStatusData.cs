﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class FaultStatusData
    {
        [DataMember]
        public int AppointmentId { get; set; }

        // Code added  - 11/06/2013 -START

        [DataMember]
        public int FaultLogID { get; set; }

        [DataMember]
        public bool IsAppointmentCompleted { get; set; }

        // Code added  - 11/06/2013 -START

        [DataMember]
        public int? UserID { get; set; }

        [DataMember]
        public string Notes { get; set; }

        // Change by Zeeshan Malik according to new requirment of single job status change - June 22 2013
        //[DataMember]
        //public List<FaultRepairListData> FaultRepairListData = new List<FaultRepairListData>();

        [DataMember]
        public string FaultRepairIDs { get; set; }

        [DataMember]
        public string FollowOnNotes { get; set; }

        // Change END

        [DataMember]
        public string Reason { get; set; }

        [DataMember]
        public DateTime? RepairCompletionDateTime { get; set; }

        [DataMember]
        public bool FollowOnRequired { get; set; }

        [DataMember]
        public string ProgressStatus { get; set; }

        public bool validateFaultJobProgressStatus(string status)
        {
            if (Enum.IsDefined(typeof(FaultJobProgressStatus), status) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public enum FaultJobProgressStatus
    {
        NotStarted,
        InProgress,
        Started,
        NoEntry,
        Paused,
        ReStarted,
        Complete
    }
}
