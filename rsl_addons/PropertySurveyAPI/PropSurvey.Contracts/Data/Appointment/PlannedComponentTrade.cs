﻿// -----------------------------------------------------------------------
// <copyright file="PlannedComponentTrade.cs" company="">
// this class will use to get and return  data for Planned component trade
// </copyright>
// -----------------------------------------------------------------------

namespace PropSurvey.Contracts.Data.Appointment
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Runtime.Serialization;

    [DataContract]
    [Serializable]
    public class PlannedComponentTrade
    {
        [DataMember]
        public int? componentTradeId { get; set; }
        [DataMember]
        public short? componentId { get; set; }
        [DataMember]
        public string componentName { get; set; }
        [DataMember]
        public int? tradeId { get; set; }
        [DataMember]
        public double? duration { get; set; }
        [DataMember]
        public string durationUnit { get; set; }
        [DataMember]
        public string trade { get; set; }
        [DataMember]
        public int? sorder { get; set; }

        [DataMember]
        public string location { get; set; }
        [DataMember]
        public string adaptation { get; set; }

        [DataMember]
        public int? locationId { get; set; }
        [DataMember]
        public int? adaptationId { get; set; }

        [DataMember]
        public string PMO { get; set; }
        [DataMember]
        public string JSNNotes { get; set; }
        [DataMember]
        public string jobStatus { get; set; }
        [DataMember]
        public string JSNumber { get; set; }
        [DataMember]
        public DateTime? reportedDate { get; set; }
        [DataMember]
        public DateTime? completionDate { get; set; }

        [DataMember]
        public List<JobPauseHistoryData> jobPauseHistory = new List<JobPauseHistoryData>();

    }
}
