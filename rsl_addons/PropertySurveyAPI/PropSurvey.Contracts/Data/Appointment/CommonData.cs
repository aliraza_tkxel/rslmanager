﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using PropSurvey.Contracts.Data.Detector;

namespace PropSurvey.Contracts.Data.Appointment
{
    [Serializable]
    [DataContract]
   public class CommonData
    {

        [DataMember]
        public List<DetectorType> detectorTypes = new List<DetectorType>(); 
                        
        [DataMember]
        public List<PowerType> powerTypes = new List<PowerType>();
        
    }    
}
