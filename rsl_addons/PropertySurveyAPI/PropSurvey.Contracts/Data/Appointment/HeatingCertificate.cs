﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PropSurvey.Contracts.Data.Appointment
{
    [DataContract]
    [Serializable]
    public class HeatingCertificate
    {
        [DataMember]
        public int certificateId { get; set; }
        [DataMember]
        public string certificateName { get; set; }
        [DataMember]
        public int? fuelTypeId { get; set; }
        
    }
}
