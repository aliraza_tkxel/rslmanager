﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data.Appointment
{

    [DataContract]
    [Serializable]
    public class SyncAppointmentDetail
    {
        [DataMember]
        public int appointmentId { get; set; }

        [DataMember]
        public string appointmentType { get; set; }

        [DataMember]
        public string propertyAddress { get; set; }
                
        [DataMember]
        public string failureReason { get; set; }

        public SyncAppointmentDetail(int appointmentId = -1, string appointmentType = "", String propertyAddress = "", string failureReason = "")
        {
            this.appointmentId = appointmentId;
            this.appointmentType = appointmentType;
            this.propertyAddress = propertyAddress;            
            this.failureReason = failureReason;
        }
    }
}
