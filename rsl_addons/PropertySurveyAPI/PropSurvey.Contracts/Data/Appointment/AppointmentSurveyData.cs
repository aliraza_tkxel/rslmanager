﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data.Appointment
{
    [DataContract]
    [Serializable]
    public class AppointmentSurveyData
    {
        [DataMember]
        public List<SurveyData> surveyData = new List<SurveyData>();
    }
}
