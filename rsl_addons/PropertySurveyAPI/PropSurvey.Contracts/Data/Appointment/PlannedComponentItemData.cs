﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace PropSurvey.Contracts.Data.Appointment
{
    [DataContract]
    [Serializable]
    public class PlannedComponentItemData
    {
        [DataMember]
        public short componentId { get; set; }
        [DataMember]
        public string componentName { get; set; }
        [DataMember]
        public int? itemId { get; set; }
        [DataMember]
        public int? parameterId { get; set; }
        [DataMember]
        public int? valueId { get; set; }
        [DataMember]
        public int? subParameterId { get; set; }
        [DataMember]
        public int? subValueId { get; set; }
        [DataMember]
        public int? cycle { get; set; }
        [DataMember]
        public string frequency { get; set; }
        [DataMember]
        public bool? isComponentAccounting { get; set; }

    }
}
