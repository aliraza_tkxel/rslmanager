﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace PropSurvey.Contracts.Data.Appointment
{
   public class AppointmentResponse
    {

       public CommonData commonData = new CommonData();
       public List<AllAppointmentsList> appointmentList = new List<AllAppointmentsList>();
    }
}
