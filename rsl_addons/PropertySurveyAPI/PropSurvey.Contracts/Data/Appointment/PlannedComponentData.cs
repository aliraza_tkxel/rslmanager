﻿// -----------------------------------------------------------------------
// <copyright file="PlannedComponentData.cs" company="Tkxel">
// this class will use to get and return  data for Planned component
// </copyright>
// -----------------------------------------------------------------------

namespace PropSurvey.Contracts.Data.Appointment
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Runtime.Serialization;


    [DataContract]
    [Serializable]
    public class PlannedComponentData
    {
        [DataMember]
        public short componentId { get; set; }
        [DataMember]
        public string componentName { get; set; }
        [DataMember]
        public bool? isAccounting { get; set; }
        [DataMember]
        public int? cycle { get; set; }
        [DataMember]
        public string frequency { get; set; }
        [DataMember]
        public double ? materialCost { get; set; }
        [DataMember]
        public double? labourCost { get; set; }
        [DataMember]
        public int? editedBy { get; set; }
        [DataMember]
        public DateTime? editedOn { get; set; }

    }
}
