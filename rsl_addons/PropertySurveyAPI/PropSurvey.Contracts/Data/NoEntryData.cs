﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class NoEntryData
    {
        [DataMember]
        public int NoEntyID { get; set; }

        [DataMember]
        public string propertyId { get; set; }

        [DataMember]
        public int AppointmentID { get; set; }

        [DataMember]
        public bool isCardLeft { get; set; }

        [DataMember]
        public bool isGasAvailable { get; set; }
        
        [DataMember]
        public bool isPropertyAccessible { get; set; }

        [DataMember]
        public DateTime NoEntryDate { get; set; }

        //Change#10 - Behroz - 19/12/2012 - Start
        [DataMember]
        public int JournalId { get; set; }

        [DataMember]
        public int RecordedBy { get; set; }
        //Change#10 - Behroz - 19/12/2012 - End
    }
}
