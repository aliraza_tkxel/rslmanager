﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class GasDefectsData
    {
        [DataMember]
        public int appliancesInspected { get; set; }
        [DataMember]
        public List<FaultsDataGas> defects = new List<FaultsDataGas>();
        [DataMember]
        public List<DefectCategoryData> defectCategories = new List<DefectCategoryData>();
        [DataMember]
        public List<FaultsDataGas> detectorDefects = new List<FaultsDataGas>();
        
    }
}
