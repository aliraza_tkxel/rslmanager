﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    //Class added  - 04/06/2013
    public class AppointmentListFault
    {
        [DataMember]
        public int appointmentId { get; set; }

        [DataMember]
        public string appointmentType { get; set; }

        [DataMember]
        public List<CustomerData> customerList = new List<CustomerData>();

        [DataMember]
        public int? tenancyId { get; set; } // type changed to int? from int  - 18/06/2013

        [DataMember]
        public PropertyData property = new PropertyData();

        [DataMember]
        public DateTime? appointmentDate { get; set; }

        [DataMember]
        public DateTime? repairCompletionDateTime { get; set; }

        [DataMember]
        public string appointmentStartTime { get; set; }

        [DataMember]
        public string appointmentEndTime { get; set; }

        [DataMember]
        public string appointmentStatus { get; set; }

        //[DataMember]
        //public string jobsStatus { get; set; } Commented out  - 11/06/2013

        [DataMember]
        public string appointmentNotes { get; set; }

        [DataMember]
        public string repairNotes { get; set; }

        [DataMember]
        public string noEntryNotes { get; set; }

        [DataMember]
        public string surveyorUserName { get; set; }

        // Code added  - 07/06/2013 - START

        [DataMember]
        public int defaultCustomerId { get; set; }

        [DataMember]
        public int defaultCustomerIndex { get; set; }

        // Code added  - 07/06/2013 - END

        [DataMember]
        public List<JobData> JobDataList = new List<JobData>();

        public bool validateFaultAppointmentProgressStatus(string status)
        {
            if (Enum.IsDefined(typeof(FaultAppointmentProgressStatus), status) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}

