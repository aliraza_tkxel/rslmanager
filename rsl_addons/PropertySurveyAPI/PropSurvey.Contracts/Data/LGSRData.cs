﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class LGSRData
    {

        [DataMember]
        public string CP12Document { get; set; }

        [DataMember]
        public string CP12Number { get; set; }
        
        [DataMember]
        public DateTime? DTimeStamp { get; set; }

        [DataMember]
        public string DocumentType { get; set; }

        [DataMember]
        public bool? InspectionCarried { get; set; }

        [DataMember]
        public int? IssuedBy { get; set; }

        [DataMember]
        public DateTime? IssuedDate { get; set; }
        
        [DataMember]
        public string IssuedDateString { get; set; }

        [DataMember]
        public int? JournalId { get; set; }

        [DataMember]
        public int? LGSRID { get; set; }
        
        [DataMember]
        public string Notes { get; set; }

        [DataMember]
        public string propertyId { get; set; }
        
        [DataMember]
        public DateTime? ReceivedDate { get; set; }

        [DataMember]
        public string ReceivedDateString { get; set; }

        [DataMember]
        public string ReceivedOnBehalfOf { get; set; }

        [DataMember]
        public bool? TenantHanded { get; set; }
        
        [DataMember]
        public int? jsgNumber { get; set; }

        [DataMember]
        public bool isAppointmentCompleted { get; set; }
        
        public bool validateReceiver(string option)
        {
            if (Enum.IsDefined(typeof(Receiver), option.ToUpper()) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public enum Receiver
        {
            TENANT,
            LANDLORD,
            AGENT,
            HOMEOWNER
        }
    }

}
