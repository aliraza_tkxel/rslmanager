﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class FaultsData
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public int AppointmentID { get; set; }

        [DataMember]
        public string FaultCategory { get; set; }

        [DataMember]
        public string DefectDesc { get; set; }

        [DataMember]
        public string RemedialAction { get; set; }

        [DataMember]
        public bool isAdviceNoteIssued { get; set; }

        [DataMember]
        public string SerialNo { get; set; }

        [DataMember]
        public bool WarningTagFixed { get; set; }


        public bool validateFaultsCategories(string option)
        {
            if (Enum.IsDefined(typeof(FaultsCategories), option.Replace(" ", string.Empty).ToUpper()) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public enum FaultsCategories
        {
            GENERALCOMMENT,
            RIDDOR,
            IMMEDIATELYDANGEROUS,
            ATRISK,
            NOTTOCURRENTSTANDARDS,
            OTHER
        }
    }

    /// <summary>
    /// Contract data for Defects Category
    /// </summary>
    [DataContract]
    [Serializable]
    public class DefectCategoryData
    {
        [DataMember]
        public int CatId { get; set; }

        [DataMember]
        public string CatDescription { get; set; }
    }
    /// <summary>
    /// Contract data for request Gas Defects 
    /// </summary>
    [DataContract]
    [Serializable]
    public class RequestGasDefectsData
    {
        [DataMember]
        public string username { get; set; }
        [DataMember]
        public string salt { get; set; }
        [DataMember]
        public int journalId { get; set; }

    }
    /// <summary>
    ///DataContract for  DeletepropertyImage request
    /// </summary>
    [DataContract]
    [Serializable]
    public class ResponseUploadDefectPictureData
    {
        [DataMember]
        public int? PropertyDefectImageId { get; set; }
        [DataMember]
        public string imagePath { get; set; }
    }

    /// <summary>
    ///DataContract for  DeletepropertyImage request
    /// </summary>
    [DataContract]
    [Serializable]
    public class ResponseSaveRepairPictureData
    {
        [DataMember]
        public int? FaultRepairImageId { get; set; }
        [DataMember]
        public string imagePath { get; set; }
    }
}

