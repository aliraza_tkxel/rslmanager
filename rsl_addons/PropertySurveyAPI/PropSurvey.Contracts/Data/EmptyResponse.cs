﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
namespace PropSurvey.Contracts.Data
{
    [DataContract]
    public class EmptyResponse
    {
        [DataMember]
        public string response = string.Empty;
    }
}
