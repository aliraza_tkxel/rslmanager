﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class FaultRepairData
    {
        [DataMember]
        public int? FaultRepairID { get; set; }

        [DataMember]
        public string Description { get; set; }
    }

/// <summary>
/// To get fault repair list
/// </summary>
    [DataContract]
    [Serializable]
    public class RequestFault
    {
        [DataMember]
        public string username { get; set; }
        [DataMember]
        public string salt { get; set; }
    }

    /// <summary>
    /// To get fault repair list
    /// </summary>
    [DataContract]
    [Serializable]
    public class ResponseFaultRepairData
    {
        [DataMember]
        public List<FaultRepairData> faultRepairList = new List<FaultRepairData>();
    }

    /// <summary>
    /// To get fault repair list
    /// </summary>
    [DataContract]
    [Serializable]
    public class ResponsePauseReasonList
    {
        [DataMember]
        public List<string> pauseReasonList = new List<string>();
    }
}
