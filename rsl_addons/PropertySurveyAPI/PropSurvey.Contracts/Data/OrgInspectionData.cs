﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class OrgInspectionData
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public int AppointmentID { get; set; }

        //[DataMember]
        //public int OrgID { get; set; }

        //[DataMember]
        //public string OrgName { get; set; }

        //[DataMember]
        //public string RegNo { get; set; }

        [DataMember]
        public int GasEngineerID { get; set; }
        
        [DataMember]
        public string GasEngineerName { get; set; }

        [DataMember]
        public string GSRENo { get; set; }

        //[DataMember]
        //public string Address { get; set; }

        //[DataMember]
        //public string City { get; set; }

        //[DataMember]
        //public string PostCode { get; set; }

        //[DataMember]
        //public string Telephone { get; set; }

        [DataMember]
        public DateTime InspectedDate { get; set; }

    }
}
