﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class IndexStatusData
    {
        [DataMember]
        public int AppointmentID { get; set; }

        [DataMember]
        public string ClientName { get; set; }

        [DataMember]
        public string ClientAddress { get; set; }

        [DataMember]
        public bool isOrgInspected { get; set; }

        [DataMember]
        public bool isAgentInspected { get; set; }

        [DataMember]
        public bool isApplianceInspected { get; set; }

        [DataMember]
        public bool isGIPWInspected { get; set; }

        [DataMember]
        public bool isFaultInspected { get; set; }

        [DataMember]
        public bool isIssuedReceivedInspected { get; set; }

    }
}
