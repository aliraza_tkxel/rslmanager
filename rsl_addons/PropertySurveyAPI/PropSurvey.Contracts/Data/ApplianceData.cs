﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class ApplianceData
    {
        [DataMember]
        public int ApplianceID { get; set; }

        [DataMember]
        public string propertyId { get; set; }

        [DataMember]
        public ApplianceTypeData ApplianceType = new ApplianceTypeData();

        [DataMember]
        public ManufacturerData ApplianceManufacturer = new ManufacturerData();

        [DataMember]
        public AppliancesLocationData ApplianceLocation = new AppliancesLocationData();

        [DataMember]
        public ApplianceModelData ApplianceModel = new ApplianceModelData();
        [DataMember]
        public ApplianceInspectionData ApplianceInspection = new ApplianceInspectionData();
        [DataMember]
        public List<FaultsDataGas> ApplianceDefects = new List<FaultsDataGas>();

       
   
        [DataMember]
        public string Model { get; set; }

        [DataMember]
        public string SerialNumber { get; set; }

        [DataMember]
        public string GCNumber { get; set; }

        [DataMember]
        public string FluType { get; set; }

        [DataMember]
        public DateTime? InstalledDate { get; set; }

        [DataMember]
        public bool? isInspected { get; set; }

        [DataMember]
        public bool? isLandlordAppliance { get; set; }

        [DataMember]
        public DateTime? ReplacementDate { get; set; }

        public bool validateApplianceFlueType(string FlueType)
        {
            if (Enum.IsDefined(typeof(ApplianceFlueType), FlueType) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public enum ApplianceFlueType
        {
            RS,
            OF,
            FL
        }
    }
}
