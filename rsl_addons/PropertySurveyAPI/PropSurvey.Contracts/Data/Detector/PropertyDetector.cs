﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;


namespace PropSurvey.Contracts.Data.Detector
{
    [DataContract]
    [Serializable]
    public class PropertyDetector
    {
        [DataMember]
        public int? detectorId { get; set; }

        [DataMember]
        public string detectorType { get; set; }

        [DataMember]
        public int? detectorTypeId { get; set; }

        [DataMember]
        public int? installedBy { get; set; }

        [DataMember]
        public DateTime? installedDate { get; set; }

        [DataMember]
        public bool? isLandlordsDetector { get; set; }

        [DataMember]
        public string location { get; set; }

        [DataMember]
        public string manufacturer { get; set; }

        [DataMember]
        public int? powerTypeId { get; set; }

        [DataMember]
        public string propertyId { get; set; }

        [DataMember]
        public string serialNumber { get; set; }
                    
    }
}
