﻿using System;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data.Detector
{
    [DataContract]
    [Serializable]
    public class PowerType
    {
        [DataMember]
        public int powerTypeId { get; set; }

        [DataMember]
        public string powerType { get; set; }

    }
}
