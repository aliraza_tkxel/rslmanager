﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class JournalData
    {
        [DataMember]
        public int? journalId { get; set; }

        [DataMember]
        public string propertyId { get; set; }

        [DataMember]
        public int? statusId { get; set; }

        [DataMember]
        public int? actionId { get; set; }

        [DataMember]
        public short? inspectionTypeId { get; set; }

        [DataMember]
        public DateTime? creationDate { get; set; }

        [DataMember]
        public int? creationBy { get; set; }

        [DataMember]
        public bool? isCurrent { get; set; }

        public JournalData()
        {
            creationDate = DateTime.Parse("01/01/1970");          
        }

        //[DataMember]
        //public StatusData status = new StatusData();
    }
}
