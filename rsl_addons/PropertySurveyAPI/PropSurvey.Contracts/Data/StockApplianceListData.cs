﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]

    public class StockApplianceListData
    {
        [DataMember]
        public List<ApplianceData> appliancesList = new List<ApplianceData>();
        [DataMember]
        public List<DetectorCountData> detectorsList = new List<DetectorCountData>(); 
    }
}
