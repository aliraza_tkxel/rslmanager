﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class PropertyDimData
    {
        [IgnoreDataMember]
        public int propertyDimId { get; set; }

        [DataMember]
        public string propertyId { get; set; }

        [DataMember]
        public string roomName { get; set; }

        [DataMember]
        public double? roomWidth { get; set; }

        [DataMember]
        public double? roomHeight { get; set; }
        
        [DataMember]
        public double? roomLength { get; set; }

        [DataMember]
        public DateTime? updatedOn { get; set; }

        [DataMember]
        public int? updatedBy { get; set; }

        public PropertyDimData()
        {
            this.roomWidth = 0.0D;
            this.roomHeight = 0.0D;
            this.roomLength = 0.0D;
        }

        public PropertyDimData(string roomName)
        {
            this.roomWidth = 0.0D;
            this.roomHeight = 0.0D;
            this.roomLength = 0.0D;
            this.roomName = roomName;

            updatedOn = DateTime.Parse("01/01/1970");
        }

        public PropertyDimData getPropertyInitialValues(string roomName)
        {
            PropertyDimData propDimData = new PropertyDimData(roomName);
            return propDimData;

        }
    }
}
