﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class SurveyDatesData
    {
       

        [DataMember]
        public int? cycle { get; set; }

        [DataMember]
        public int SID { get; set; }        

        [DataMember]
        public DateTime? LastDone { get; set; }                

        [DataMember]
        public DateTime? DueDate { get; set; }

    }
}
