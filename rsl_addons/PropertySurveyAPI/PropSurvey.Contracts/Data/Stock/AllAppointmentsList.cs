﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using PropSurvey.Contracts.Data.Appointment;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class AllAppointmentsList
    {
        [DataMember]
        public int appointmentId { get; set; }

        [DataMember]
        public String appointmentTitle { get; set; }

        [DataMember]
        public String appointmentType { get; set; }

        [DataMember]
        public String appointmentLocation { get; set; }

        [DataMember]
        public DateTime? appointmentStartDateTime { get; set; }

        [DataMember]
        public DateTime? appointmentEndDateTime { get; set; }

        [DataMember]
        public DateTime? creationDate { get; set; }

        [DataMember]
        public DateTime? loggedDate { get; set; }

        [DataMember]
        public String surveyourAvailability { get; set; }

        [DataMember]
        public String appointmentNotes { get; set; }

        [IgnoreDataMember]
        public Boolean? appointmentValidity { get; set; }

        [DataMember]
        public String appointmentStatus { get; set; }

        [IgnoreDataMember]
        public String journalSubStatus { get; set; }

        [DataMember]
        public String surveyType { get; set; }

        [DataMember]
        public int? createdBy { get; set; }
        [DataMember]
        public string createdByPerson { get; set; }
        [DataMember]
        public int defaultCustomerId { get; set; }

        [DataMember]
        public int defaultCustomerIndex { get; set; }

        [DataMember]
        public List<CustomerData> customerList = new List<CustomerData>();

        [DataMember]
        public int? tenancyId { get; set; }

        [DataMember]
        public PropertyData property = new PropertyData();

        [DataMember]
        public SchemeBlockData scheme {get; set;}

        [DataMember]
        public AppointmentInfoData appInfoData = new AppointmentInfoData();

        [DataMember]
        public LGSRData CP12Info = null;//new LGSRData();

        [DataMember]
        public string surveyorUserName { get; set; }

        [DataMember]
        public string surveyorAlert { get; set; }

        [DataMember]
        public string appointmentCalendar { get; set; }

        [DataMember]
        public byte appointmentOverdue { get; set; }

        [DataMember]
        public DateTime? appointmentDate { get; set; }

        [DataMember]
        public DateTime? appointmentEndDate { get; set; }

        [DataMember]
        public JournalData journal = new JournalData();

        [DataMember]
        public int? jsgNumber { get; set; }

        [DataMember]
        public int journalId { get; set; }

        [DataMember]
        public long? journalHistoryId { get; set; }

        [IgnoreDataMember]
        public String appointmentStartTimeString { get; set; }

        [IgnoreDataMember]
        public String appointmentEndTimeString { get; set; }

        [DataMember]
        public bool? addToCalendar { get; set; }

        [DataMember]
        public bool? isMiscAppointment { get; set; }

        [DataMember]
        public List<JobData> jobDataList = null;
     
        [DataMember]
        public PlannedComponentTrade componentTrade = null;

        public AllAppointmentsList()
        {
            appointmentStartDateTime = DateTime.Parse("01/01/1970");
            appointmentEndDateTime = DateTime.Parse("01/01/1970");
            appointmentDate = DateTime.Parse("01/01/1970");
            appointmentEndDate = DateTime.Parse("01/01/1970");
        }
        
        public bool validateAppointmentProgressStatus(string status)
        {
            if (Enum.IsDefined(typeof(AppointmentProgressStatus), status) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool validateSurveyourAvailabilityStatus(string status)
        {
            if (Enum.IsDefined(typeof(SurveyourAvailabilityStatus), status) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool validateAppointmentTypes(string status)
        {
            if (Enum.IsDefined(typeof(AppointmentTypes), status) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool validateSurveyTypes(string status)
        {
            if (Enum.IsDefined(typeof(SurveyTypes), status) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}