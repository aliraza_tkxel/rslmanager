﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class AppointmentDataStock
    {
        [DataMember]
        public int appointmentId { get; set; }

        //[DataMember]
        //public int customer { get; set; } //added 12-Nov-2013 due to user session expired issue

        [DataMember]
        public String appointmentTitle { get; set; }

        [DataMember]
        public String appointmentType { get; set; }

        [DataMember]
        public String appointmentLocation { get; set; }

        [DataMember]
        public DateTime? appointmentStartDateTime { get; set; }

        [DataMember]
        public DateTime? appointmentEndDateTime { get; set; }

        [DataMember]
        public DateTime? appointmentDate { get; set; }

        [DataMember]
        public String surveyourAvailability { get; set; }

        [DataMember]
        public String appointmentNotes { get; set; }

        [IgnoreDataMember]
        public Boolean? appointmentValidity { get; set; }

        [DataMember]
        public String appointmentStatus { get; set; }


        [DataMember]
        public String surveyType { get; set; }

        [DataMember]
        public int createdBy { get; set; }
        [DataMember]
        public DateTime? loggedDate { get; set; }
        [DataMember]
        public CustomerData customer = new CustomerData();

        [DataMember]
        public AppointmentInfoData appInfoData = new AppointmentInfoData();

        [DataMember]
        public IssuedReceivedByData CP12Info = new IssuedReceivedByData();

        [DataMember]
        public string surveyorUserName { get; set; }

        [DataMember]
        public string surveyorAlert { get; set; }

        [DataMember]
        public string appointmentCalendar { get; set; }

        [DataMember]
        public string username { get; set; }
        [DataMember]
        public string salt { get; set; }

        [DataMember]
        public bool? addToCalendar { get; set; }
        public bool validateAppointmentProgressStatus(string status)
        {
            if (Enum.IsDefined(typeof(AppointmentProgressStatus), status) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool validateSurveyourAvailabilityStatus(string status)
        {
            if (Enum.IsDefined(typeof(SurveyourAvailabilityStatus), status) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool validateAppointmentTypes(string status)
        {
            if (Enum.IsDefined(typeof(AppointmentTypes), status) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool validateSurveyTypes(string status)
        {
            if (Enum.IsDefined(typeof(SurveyTypes), status) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public enum AppointmentProgressStatus
    {
        NotStarted,
        InProgress,
        NoEntry,
        Finished
    }

    public enum SurveyourAvailabilityStatus
    {
        Busy,
        Available,
        Free,
        Tentative,
        OutOfOffice
    }

    public enum SurveyTypes
    {
        Appointments,
        ConditionSurveys,
        WorkOrders
    }


    public enum AppointmentTypes
    {
        Stock,
        Pre,
        Post,
        Void,
        Gas

    }

}
