﻿using System;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class SaveAppointmentData
    {
        [DataMember]
        public int appointmentId { get; set; }
      
    }
}
