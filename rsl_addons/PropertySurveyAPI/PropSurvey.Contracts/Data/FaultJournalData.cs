﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class FaultJournalData
    {
        [DataMember]
        public int? journalId { get; set; }

        [DataMember]
        public int? faultLogId { get; set; }

        [DataMember]
        public int? customerId { get; set; }

        [DataMember]
        public int? tenancyId { get; set; }        

        [DataMember]
        public string propertyId { get; set; }

        [DataMember]
        public int? itemId { get; set; }

        [DataMember]
        public int? itemNatureId { get; set; }

        [DataMember]
        public int? faultStatusId { get; set; }

        [DataMember]
        public int? letterAction { get; set; }      

        [DataMember]
        public DateTime? creationDate { get; set; }

        [DataMember]
        public string title { get; set; }

        [DataMember]
        public DateTime? lastActionDate { get; set; }

        [DataMember]
        public DateTime? nextActionDate { get; set; }
    }
}
