﻿using System;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class AppointmentParam
    {
        [DataMember]
        public string startdate { get; set; }

        [DataMember]
        public string enddate { get; set; }

        [DataMember]
        public byte includeoverdue { get; set; }

        [DataMember]
        public byte fetchall { get; set; }

        [DataMember]
        public string userinfo { get; set; }

        [DataMember]
        public List<int> appointmentIds { get; set; }
         
       
       public enum ApplicationType
        {
            Stock=0,
            Appliances=1,
            FaultLocator=2
        }

    }
}
