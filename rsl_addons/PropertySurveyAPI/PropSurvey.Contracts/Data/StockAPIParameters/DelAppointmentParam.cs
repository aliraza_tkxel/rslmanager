﻿using System;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class DelAppointmentParam
    {
        [DataMember]
        public int appointmentid { get; set; }
        [DataMember]
        public string username { get; set; }
        [DataMember]
        public string salt { get; set; }
      
       
    }
}
