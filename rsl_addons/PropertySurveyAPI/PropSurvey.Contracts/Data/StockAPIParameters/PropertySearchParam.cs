﻿using System;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class PropertySearchParam
    {
        [DataMember]
        public int pagenumber { get; set; }

        [DataMember]
        public int pagecount { get; set; }

        [DataMember]
        public string searchstring { get; set; } 
    }
}
