﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class PaymentTypeData
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public string PaymentType { get; set; }

    }
}
