﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class InstallationPipeworkData
    {
        [DataMember]
        public int ID { get; set; }

        [DataMember]
        public int AppointmentID { get; set; }

        [DataMember]
        public string emergencyControl { get; set; }

        [DataMember]
        public string visualInspection { get; set; }

        [DataMember]
        public string gasTightnessTest { get; set; }

        [DataMember]
        public string equipotentialBonding { get; set; }

        [DataMember]
        public DateTime? inspectionDate { get; set; }

        [DataMember]
        public bool? isInspected { get; set; }
       

        [DataMember]
        public string propertyId { get; set; }
   

        [DataMember]
        public int? JournalId { get; set; }

        public bool validatePipeworkOption(string option)
        {
            if (Enum.IsDefined(typeof(PipeworkOption), option.ToUpper()) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public enum PipeworkOption
        {
            YES,
            NO,
            NA
        }
    }
}
