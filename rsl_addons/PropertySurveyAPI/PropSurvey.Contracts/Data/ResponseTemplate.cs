﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{

    [DataContract]
    [Serializable]
    public class ResponseTemplate<T>
    {
        [DataMember]
        public MessageData  status { get; set; }

        [DataMember]
        public T response { get; set; }


    }
}
