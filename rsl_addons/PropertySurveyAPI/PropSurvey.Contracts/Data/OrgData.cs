﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class OrgData
    {
        [DataMember]
        public int ORGID { get; set; }

        [DataMember]
        public string ORGNAME { get; set; }

        [DataMember]
        public string BUILDINGNAME { get; set; }

        [DataMember]
        public string ADDRESS1 { get; set; }

        [DataMember]
        public string TOWNCITY { get; set; }

        [DataMember]
        public string POSTCODE { get; set; }

        [DataMember]
        public string COUNTY { get; set; }

        [DataMember]
        public string TELEPHONE1 { get; set; }

        [DataMember]
        public string CISCERTIFICATENUMBER { get; set; }

        [DataMember]
        public DateTime? CISEXPIRYDATE { get; set; }

        [DataMember]
        public string VATREGNUMBER { get; set; }

        [DataMember]
        public int? COMPANYTYPE { get; set; }

        [DataMember]
        public int SHOWDOCSONWHITEBOARD { get; set; }

        [DataMember]
        public int? ORGACTIVE { get; set; }

        [DataMember]
        public int? PAYMENTTERMS { get; set; }

        [DataMember]
        public int? PAYMENTTYPE { get; set; }

        
    }
}
