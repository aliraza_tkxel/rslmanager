﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class AppliancesAllData
    {
        //[DataMember]
        //public int ApplianceID { get; set; }

        //[DataMember]
        //public string PropertyID { get; set; }

        [DataMember]
        public List<ApplianceTypeData> ApplianceType = new List<ApplianceTypeData>();

        [DataMember]
        public List<ManufacturerData> ApplianceManufacturer = new List<ManufacturerData>();

        [DataMember]
        public List<AppliancesLocationData> ApplianceLocation = new List<AppliancesLocationData>();

        [DataMember]
        public List<ApplianceModelData> ApplianceModel = new List<ApplianceModelData>();

        [DataMember]
        public List<DefectCategoryData> defectCategories = new List<DefectCategoryData>();

        public bool validateApplianceFlueType(string FlueType)
        {
            if (Enum.IsDefined(typeof(ApplianceFlueType), FlueType) == true)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public enum ApplianceFlueType
        {
            RS,
            OF,
            FL
        }
    }
}
