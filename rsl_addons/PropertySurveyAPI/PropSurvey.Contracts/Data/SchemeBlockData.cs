﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class SchemeBlockData
    {
        [DataMember]
        public int? schemeId { get; set; }

        [DataMember]
        public int? blockId { get; set; } 

        [DataMember]
        public string schemeName { get; set; }

        [DataMember]
        public String blockName { get; set; }

        [DataMember]
        public String postcode { get; set; }

        [DataMember]
        public String address1 { get; set; }

        [DataMember]
        public String address2 { get; set; }

        [DataMember]
        public String address3 { get; set; }

        [DataMember]
        public String towncity { get; set; }

        [DataMember]
        public String county { get; set; }
    }
}
