﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ComponentModel;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class PropertyPictureData
    {
        [DataMember]
        public int propertyPictureId { get; set; }

        [DataMember]
        public int appointmentId { get; set; }

        [DataMember]
        public string propertyPictureName { get; set; }

        [DataMember]
        public int surveyId { get; set; }

        [DataMember]
        public bool? isDefault { get; set; }

        [DataMember]
        public string propertyId { get; set; }

        [DataMember]
        public int? itemId { get; set; }

        [DataMember]
        public int heatingId { get; set; }

        [DataMember]
        public DateTime? createdOn { get; set; }

        [DataMember]
        public int createdBy { get; set; }

        [DataMember]
        public string imagePath { get; set; }

        [DataMember(IsRequired = false, EmitDefaultValue = false)]
        [DefaultValue("")]
        public string ImageIdentifier { get; set; }

        public PropertyPictureData()
        {
            createdOn = DateTime.Parse("01/01/1970");
        }
    }

    /// <summary>
    ///DataContract for  DeletepropertyImage request
    /// </summary>
    [DataContract]
    [Serializable]
    public class DeletepropertyPictureData
    {
        [DataMember]
        public string username { get; set; }
        [DataMember]
        public string salt { get; set; }

        [DataMember]
        public int? propertyPictureId { get; set; }
    }
    /// <summary>
    ///DataContract for  DeletepropertyImage request
    /// </summary>
    [DataContract]
    [Serializable]
    public class ResponseUploadPictureData
    {
        [DataMember]
        public int? propertyPictureId { get; set; }
        [DataMember]
        public string imagePath { get; set; }


    }
}
