﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class ApplianceDefectData
    {
        [DataMember]
        public int ApplianceID { get; set; }
        [DataMember]
        public int PropertyDefectId { get; set; }
        [DataMember]
        public string propertyId { get; set; }

        [DataMember]
        public int? JournalID { get; set; }
       
        [DataMember]
        public string SerialNumber { get; set; }

        [DataMember]
        public int? CategoryID { get; set; }
   
        [DataMember]
        public bool? IsDefectIdentified { get; set; }

        [DataMember]
        public bool? IsWarningIssued { get; set; }
      
        [DataMember]
        public bool? IsWarningFixed { get; set; }      

        [DataMember]
        public DateTime? DefectDate { get; set; }

       
    }
}
