﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace PropSurvey.Contracts.Data
{
    [DataContract]
    [Serializable]
    public class PropertiesListData
    {
        [DataMember]
        public PageInfoData PageInfo = new PageInfoData();

        [DataMember]
        public List<CustomerData> Properties = new List<CustomerData>();

        
    }
}
