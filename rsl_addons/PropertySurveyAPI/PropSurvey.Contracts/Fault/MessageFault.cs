﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace PropSurvey.Contracts.Fault
{
    [DataContractAttribute]
    public class MessageFault
    {
        [DataMember]
        public string message { get; set; }

        [DataMember]
        public bool isErrorOccured { get; set; }
       
        [DataMember]
        public string errorCode { get; set; }
        
        public MessageFault()
        {

        }

        public MessageFault(string erroMsg, bool isError, string errorMsgCode)
        {
            message = erroMsg;
            isErrorOccured = isError;
            errorCode = errorMsgCode;
        }
    }
}
