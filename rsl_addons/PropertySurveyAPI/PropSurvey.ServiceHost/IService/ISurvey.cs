﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using System.ComponentModel;
using System.ServiceModel.Web;
using PropSurvey.Contracts.Data;
using PropSurvey.Utilities.Constants;

namespace PropSurvey.ServiceHost.IService
{
    // Start the service and browse to http://<machine_name>:<port>/survey/help to view the service's generated help page
    // NOTE: By default, a new instance of the service is created for each call; change the InstanceContextMode to Single if you want
    // a single instance of the service to process all calls.	
    [ServiceContract]
    // NOTE: If the service is renamed, remember to update the global.asax.cs file
    public interface ISurvey
    {
        #region get All Saved Survey Forms
        /// <summary>
        /// this funciton returns all the saved survey forms against appointment id
        /// </summary>
        /// <param name="appointmentId">appointment id</param>        
        /// <returns>it returns the list of saved forms data of property survey form</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetAllSavedSurveyForms)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "Post", UriTemplate = UriTemplateConstants.GetAllSavedSurveyForms)]
        List<SurveyData> getAllSavedSurveyForms(int appointmentId);
        #endregion        
    }
}
