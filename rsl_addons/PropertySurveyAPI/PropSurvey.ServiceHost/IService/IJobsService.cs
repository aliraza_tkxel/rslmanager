﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.ComponentModel;
using System.IO;
using PropSurvey.Utilities.Constants;
using PropSurvey.Contracts.Data;

namespace PropSurvey.ServiceHost.IService
{
     // Start the service and browse to http://<machine_name>:<port>/appointment/help to view the service's generated help page
    // NOTE: By default, a new instance of the service is created for each call; change the InstanceContextMode to Single if you want
    // a single instance of the service to process all calls.	
    [ServiceContract]
    // NOTE: If the service is renamed, remember to update the global.asax.cs file
    interface IJobsService
    {
        //#region get Jobs by Status ID
        ///// <summary>
        ///// This function returns all jobs of an operative by JobStatusID
        ///// </summary>
        ///// <returns>List of Jobs data objects</returns>
        //[OperationContract]
        //[Description(ServiceDescConstants.GetJobsByStatusID)]
        //[WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetJobsByStatusID)]
        //List<JobsData> getJobsDataByStatusID(int operativeID, int jobStatusID, string userName, string salt);
        //#endregion

        #region get all Jobs of an operative
        /// <summary>
        /// This function returns all jobs of an operative 
        /// </summary>
        /// <returns>List of Jobs data objects</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetAllJobs)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetAllJobs)]
        List<JobsData> getAllJobsData(int operativeID, string userName, string salt);
        #endregion

        #region get all Jobs of an operative in string
        /// <summary>
        /// This function returns all jobs of an operative 
        /// </summary>
        /// <returns>List of Jobs data objects</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetAllJobs)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetAllJobsString)]
        string getAllJobsDataString(int operativeID, string userName, string salt);
        #endregion
    }
}