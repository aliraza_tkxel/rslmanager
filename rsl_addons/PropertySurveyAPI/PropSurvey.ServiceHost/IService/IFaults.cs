﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ServiceModel;
using System.ServiceModel.Web;
using PropSurvey.Contracts.Data;
using PropSurvey.Utilities.Constants;
using System.IO;


namespace PropSurvey.ServiceHost.IService
{
    // Start the service and browse to http://<machine_name>:<port>/appliances/help to view the service's generated help page
    // NOTE: By default, a new instance of the service is created for each call; change the InstanceContextMode to Single if you want
    // a single instance of the service to process all calls.	
    [ServiceContract]
    // NOTE: If the service is renamed, remember to update the global.asax.cs file

    interface IFaults
    {

        #region get IFaults Data
        /// <summary>
        /// This function returns the IFaults data
        /// </summary>
        /// <returns>List of IFaults data objects</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetFaults)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetFaults)]
        List<FaultsData> getFaultsData(int AppointmentID, string userName, string salt);


        /// <summary>
        /// This function returns the IFaults data for gas
        /// </summary>
        /// <returns>List of IFaults data objects</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetFaultsGas)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.FetchGasDefects)]

        //[WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.FetchGasDefects)]
        ResponseTemplate<GasDefectsData> fetchGasDefects(RequestGasDefectsData requestData);
  
        #endregion

        #region get general comments Data
        /// <summary>
        /// This function returns general comments
        /// </summary>
        /// <returns>List of general comments objects</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetGeneralComments)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetGeneralComments)]
        List<FaultsData> getGeneralComments(int AppointmentID, string userName, string salt);

        //Change#21 - Behroz - 19/12/2012 - Start
        /// <summary>
        /// This function returns general comments for gas
        /// </summary>
        /// <returns>List of general comments objects</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetGeneralCommentsGas)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetGeneralCommentsGas)]
        List<FaultsDataGas> getGeneralCommentsGas(int journalId, string userName, string salt);
        //Change#21 - Behroz - 19/12/2012 - End

        #endregion

        #region get faults data only
        /// <summary>
        /// This function returns faults data
        /// </summary>
        /// <returns>List of faults data objects</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetFaultsOnly)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetFaultsOnly)]
        List<FaultsData> getFaultsOnly(int AppointmentID, string userName, string salt);

        //Change#22 - Behroz - 19/12/2012 - Start
        /// <summary>
        /// This function returns faults data for gas
        /// </summary>
        /// <returns>List of faults data objects</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetFaultsOnlyGas)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetFaultsOnlyGas)]
        List<FaultsDataGas> getFaultsOnlyGas(int journalId, string userName, string salt);
        //Change#22 - Behroz - 19/12/2012 - End

        #endregion

        #region save Faults Data
        /// <summary>
        /// This function saves the Faults Data in the database
        /// </summary>
        /// <param name="appData">The object of Faults Data</param>
        /// <returns>the Faults id if successfully save otherwise 0 </returns>

        [OperationContract]
        [Description(ServiceDescConstants.SaveFaults)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.SaveFaults)]
        int saveFaultsData(FaultsData faultData, string userName, string salt);


        /// <summary>
        /// This function saves the Faults Data in the database for gas
        /// </summary>
        /// <param name="appData">The object of Faults Data</param>
        /// <returns>the Faults id if successfully save otherwise 0 </returns>
        [OperationContract]
        [Description(ServiceDescConstants.SaveFaultsGas)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.SaveFaultsGas)]
        ResponseTemplate<FaultsDataGas> saveFaultsDataGas(RequestDefectDataGas faultData);

        #endregion

        #region update fault Data
        /// <summary>
        /// This function updates fault Data in the database
        /// </summary>
        /// <param name="appData">The object of fault Data</param>
        /// <returns>true if successfully save otherwise 0 </returns>

        [OperationContract]
        [Description(ServiceDescConstants.UpdateFaults)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.UpdateFaults)]
        bool updateFaultData(FaultsData faultData, string userName, string salt);


        /// <summary>
        /// This function updates fault Data in the database for gas
        /// </summary>
        /// <param name="appData">The object of fault Data</param>
        /// <returns>true if successfully save otherwise 0 </returns>
        //[OperationContract]
        //[Description(ServiceDescConstants.UpdateFaultsGas)]
        //[WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.UpdateFaultsGas)]
        //bool updateFaultDataGas(FaultsDataGas faultData, string userName, string salt);

      

        #endregion
                
        #region Get Fault Id
        
        /// <summary>
        /// This function returns a new fault id
        /// </summary>
        /// <returns>fault id</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetFaultId)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetFaultId)]
        int getFaultId(string propertyId, string userName, string salt);
        
        #endregion

        #region Delete Fault Image

        /// <summary>
        /// This function deletes the fault
        /// </summary>
        /// <returns>fault id</returns>
        [OperationContract]
        [Description(ServiceDescConstants.DeleteFault)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.DeleteFaultId)]
        ResponseTemplate<bool> deleteFaultImage(RequestDeleteDefectImageData deleteDefectImage);

        #endregion

        #region "Save Defect Image"

        /// <summary>
        /// This function saves the path of the image in database and the image on the server.
        /// </summary>
        /// <returns>fault id</returns>
        [OperationContract]
        [Description(ServiceDescConstants.SaveFaultImage)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.saveDefectImage)]
        ResponseTemplate<ResponseUploadDefectPictureData> saveDefectImage(Stream stream, int faultId, string fileExt, string propertyId, string userName, string salt,string imageIdentifier = "");
   
        #endregion

        #region get Fault Repair List

        /// <summary>
        /// This function returns the list of all fault repairs
        /// </summary>
        /// <returns>List of Fault Repair </returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetFaultRepairList)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.GetFaultRepairList)]
        ResponseTemplate<ResponseFaultRepairData> getFaultRepairList(RequestFault faultRepairData);

        #endregion


        #region get pause reason list

        /// <summary>
        /// This function returns reason list for pausing a job
        /// </summary>
        /// <returns>List of reasons</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetPauseReasonList)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.GetPauseReasonList)]
        ResponseTemplate<ResponsePauseReasonList> getPauseReasonList(RequestFault resuestFaultReason);

        #endregion

        #region Save fault repair Image

        /// <summary>
        /// This function saves the path of the image in database and the image on the server.
        /// </summary>
        /// <returns>fault id</returns>
        [OperationContract]
        [Description(ServiceDescConstants.SaveFaultRepairImage)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.SaveFaultRepairImage)]
        ResponseTemplate<ResponseSaveRepairPictureData> saveFaultRepairImage(Stream stream, string propertyId, int schemeId, int blockId, string jsNumber, string fileExt, int createdBy, bool isBeforeImage, string userName, string salt, string imageIdentifier = "");

        #endregion

    }
}
