﻿using System.ComponentModel;
using System.IO;
using System.ServiceModel;
using System.ServiceModel.Web;
using PropSurvey.Utilities.Constants;
using PropSurvey.Contracts.Data;


namespace PropSurvey.ServiceHost.IService
{
    // Start the service and browse to http://<machine_name>:<port>/survey/help to view the service's generated help page
    // NOTE: By default, a new instance of the service is created for each call; change the InstanceContextMode to Single if you want
    // a single instance of the service to process all calls.	
    [ServiceContract]
    // NOTE: If the service is renamed, remember to update the global.asax.cs file
    public interface IAppScreen
    {
        #region Return Forms

        /// <summary>
        /// This function will return the application screens
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="salt"></param>
        /// <returns></returns>

        [OperationContract]
        [Description(ServiceDescConstants.GetApplicaitonScreens)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.GetApplicaitonScreens)]
        Stream getApplicationScreens(StockSurveyParam varStockSurveyParam);
        
        #endregion


    }
}
