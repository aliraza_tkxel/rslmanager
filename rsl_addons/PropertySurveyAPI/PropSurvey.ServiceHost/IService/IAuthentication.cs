﻿using System;
using System.ComponentModel;
using System.ServiceModel;
using System.ServiceModel.Web;
using PropSurvey.Utilities.Constants;
using PropSurvey.Contracts.Data;

namespace PropSurvey.ServiceHost.IService
{
    [ServiceContract]
    public interface IAuthentication
    {
        #region Authenticate User
        /// <summary>
        /// This function will authenticate user from database.
        /// </summary>
        /// <param name="userName">username of surveyour</param>
        /// <param name="password">password of surveyour</param>
        /// <param name="deviceToken"> of surveyour</param>
        /// <returns>Last Logged In Data Time</returns>
        [OperationContract]
        [Description(ServiceDescConstants.AuthenticateUser)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json,Method="POST" , UriTemplate = UriTemplateConstants.AuthenticateUser)]
        ResponseTemplate<UserData> authenticateUser(UserAuthenticationParam varUserAuthParam);

        #endregion


        #region Email Forgotten Password
        /// <summary>
        /// This function will send email forgotten password to provided valid email address
        /// </summary>
        /// <param name="emailAddress">Email Address</param>
        /// <returns>Email send status</returns>
        [OperationContract]
        [Description(ServiceDescConstants.EmailForgottenPassword )]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.EmailForgottenPassword )]
        ResponseTemplate<MessageData> emailForgottenPassword(String emailAddress);

        #endregion

        #region Update Password
        /// <summary>
        /// This function will update password
        /// </summary>
        /// <param name="emailAddress">Email Address</param>
        /// <returns>Email send status</returns>
        [OperationContract]
        [Description(ServiceDescConstants.UpdatePassword)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.UpdatePassword)]
        ResponseTemplate<MessageData> updatePassword(UpdatePasswordParam objUpdatePasswordParam);

        #endregion

        //#region User Logout
        ///// <summary>
        ///// This function calls the business layer to logout the user from database.
        ///// </summary>
        ///// <param name="userName">username</param>
        ///// <param name="salt">salt</param>
        ///// <returns></returns>
        //[OperationContract]
        //[Description(ServiceDescConstants.LogoutUser)]
        //[WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.LogoutUser)]
        //void LogoutUser(string userName, string salt);

        
        //[OperationContract]
        //[Description(ServiceDescConstants.LogoutUser)]
        //[WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetSalt)]
        //string getSalt(string userName);

        //[OperationContract]
        //[Description(ServiceDescConstants.LogoutUser)]
        //[WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = "test?username={username}&password={password}")]
        //int checkUser(string userName,string password);
        //#endregion

        //#region Get Gas Tab Rights
        ////Change#16 - Behroz - 18/12/2012 - Start
        ///// <summary>
        ///// This services returns the rights on the user for the gas tab.
        ///// </summary>
        ///// <param name="userid"></param>
        ///// <param name="userName"></param>
        ///// <param name="salt"></param>
        ///// <returns></returns>
        //[OperationContract]
        //[Description(ServiceDescConstants.GetGasTabRights)]
        //[WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "GET", UriTemplate = UriTemplateConstants.GetGasTabRights)]
        //bool GasTabRights(int userid, string userName, string salt);
        ////Change#16 - Behroz - 18/12/2012 - End

        //#endregion
    }
}
