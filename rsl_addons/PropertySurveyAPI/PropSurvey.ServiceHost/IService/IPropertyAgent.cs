﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ServiceModel;
using System.ServiceModel.Web;
using PropSurvey.Contracts.Data;
using PropSurvey.Utilities.Constants;


namespace PropSurvey.ServiceHost.IService
{
    // Start the service and browse to http://<machine_name>:<port>/propertyagent/help to view the service's generated help page
    // NOTE: By default, a new instance of the service is created for each call; change the InstanceContextMode to Single if you want
    // a single instance of the service to process all calls.	
    [ServiceContract]
    // NOTE: If the service is renamed, remember to update the global.asax.cs file
    public interface IPropertyAgent
    {
        #region get All Property Agents

        /// <summary>
        /// This function returns all the agents
        /// </summary>
        /// <returns>List of agent data objects</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetAllPropertyAgents)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetAllAgents)]
        List<PropertyAgentData> getAllAgents(string userName, string salt);

        #endregion

        #region update property agent
        /// <summary>
        /// This function update the property agent. This function use post method to accept data. 
        /// </summary>
        /// <param name="agentData">This fucntion accepts the PropertyAgentData's object, username that updates the data and salt value for authentication </param>
        /// <returns>It returns true or false in update is successful</returns>
        [OperationContract]
        [Description(ServiceDescConstants.UpdatePropertyAgent)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.UpdateAgent)]
        bool updatePropertyAgent(PropertyAgentData agentData, string userName, string salt);

        #endregion

        #region save property agent
        /// <summary>
        /// This function save the property agent. This function use post method to accept data. 
        /// </summary>
        /// <param name="agentData">This fucntion accepts the PropertyAgentData's object, username that updates the data and salt value for authentication </param>
        /// <returns>It returns true or false in update is successful</returns>
        [OperationContract]
        [Description(ServiceDescConstants.SavePropertyAgent)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.SaveAgent)]
        bool savePropertyAgent(PropertyAgentData agentData, string userName, string salt);

        #endregion

        #region get Property Agent to Appointment
        /// <summary>
        /// This function returns Property Agent to Appointment
        /// </summary>
        /// <returns>It retruns Property Agent to Appointment's object</returns>
        
        [OperationContract]
        [Description(ServiceDescConstants.GetAgentToAppointment)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetAgentToAppointment)]
        PropertyAgentToAppointmentData getAgentToAppointment(int AppointmentID, string userName, string salt);
        #endregion

        #region get Property Agent Details
        /// <summary>
        /// This function returns Property Agent Details
        /// </summary>
        /// <returns>It retruns Property Agent Details</returns>

        [OperationContract]
        [Description(ServiceDescConstants.GetAgentDetails)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetAgentDetails)]
        PropertyAgentData getAgentDetails(int appoinmentId, string userName, string salt);
        #endregion

        #region Update a Property Agent to Appointment
        /// <summary>
        /// This function updates a Property Agent to Appointment
        /// </summary>
        /// <param name="proAgentData"> Property Agent to Appointment Data's object</param>
        /// <returns>It retruns the true on success else returns false</returns>
        [OperationContract]
        [Description(ServiceDescConstants.UpdateAgentToAppointment)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.UpdateAgentToAppointment)]
        bool UpdateAgentToAppointment(PropertyAgentToAppointmentData proAgentData, string userName, string salt);
        
        #endregion

        #region save a Property Agent to Appointment
        /// <summary>
        /// This function saves  Property Agent to Appointment
        /// </summary>
        /// <param name="proAgentData"> Property Agent to Appointment Data's object</param>
        /// <returns>It retruns the true on success else returns false</returns>
        [OperationContract]
        [Description(ServiceDescConstants.SaveAgentToAppointment)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.SaveAgentToAppointment)]
        int SaveAgentToAppointment(PropertyAgentToAppointmentData proAgentData, string userName, string salt);
        
        #endregion
    }
}
