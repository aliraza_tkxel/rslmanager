﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ServiceModel;
using System.ServiceModel.Web;
using PropSurvey.Contracts.Data;
using PropSurvey.Utilities.Constants;


namespace PropSurvey.ServiceHost.IService
{
    // Start the service and browse to http://<machine_name>:<port>/appliances/help to view the service's generated help page
    // NOTE: By default, a new instance of the service is created for each call; change the InstanceContextMode to Single if you want
    // a single instance of the service to process all calls.	
    [ServiceContract]
    // NOTE: If the service is renamed, remember to update the global.asax.cs file

    interface IInstallationPipework
    {

        #region get InstallationPipework Data
        /// <summary>
        /// This function returns the InstallationPipework data
        /// </summary>
        /// <returns>List of InstallationPipework data objects</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetInstallationPipework)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetInstallationPipework)]
        InstallationPipeworkData getInstallationPipeworkFormData(int AppointmentID, string userName, string salt);

        //Change#12 - Behroz - 12/14/2012 - Start
        /// <summary>
        /// This function returns the InstallationPipework data for gas
        /// </summary>
        /// <returns>List of InstallationPipework data objects</returns>
        [OperationContract]
        [Description(ServiceDescConstants.GetInstallationPipeworkGas)]
        [WebGet(ResponseFormat = WebMessageFormat.Json, UriTemplate = UriTemplateConstants.GetInstallationPipeworkGas)]
        //InstallationPipeworkData getInstallationPipeworkFormDataGas(string PropertyID, string userName, string salt);
        InstallationPipeworkData getInstallationPipeworkFormDataGas(int JournalId, string userName, string salt);
        //Change#12 - Behroz - 12/14/2012 - End
        #endregion

        #region save InstallationPipework Data

        /// <summary>
        /// This function saves the InstallationPipework Data in the database
        /// </summary>
        /// <param name="appData">The object of InstallationPipework Data</param>
        /// <returns>the InstallationPipework id if successfully save otherwise 0 </returns>
        [OperationContract]
        [Description(ServiceDescConstants.SaveInstallationPipework)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.SaveInstallationPipework)]
        int saveInstallationPipework(InstallationPipeworkData workData, string userName, string salt);

        //Change#12 - Behroz - 12/13/2012 - Start
        /// <summary>
        /// This function saves the InstallationPipework Data in the database for gas
        /// </summary>
        /// <param name="appData">The object of InstallationPipework Data</param>
        /// <returns>the InstallationPipework id if successfully save otherwise 0 </returns>
        //[OperationContract]
        //[Description(ServiceDescConstants.SaveInstallationPipeworkGas)]
        //[WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.SaveInstallationPipeworkGas)]
        //int saveInstallationPipeworkGas(InstallationPipeworkData workData, string userName, string salt);
        ////Change#12 - Behroz - 12/13/2012 - End
        #endregion

        #region update InstallationPipework Data
        /// <summary>
        /// This function updates theInstallationPipework Data in the database
        /// </summary>
        /// <param name="appData">The object of InstallationPipework Data</param>
        /// <returns>true if successfully save otherwise 0 </returns>
        
        [OperationContract]
        [Description(ServiceDescConstants.UpdateInstallationPipework)]
        [WebInvoke(ResponseFormat = WebMessageFormat.Json, Method = "POST", UriTemplate = UriTemplateConstants.UpdateInstallationPipework)]
        bool updateInstallationPipeworkData(InstallationPipeworkData insData, string userName, string salt);
        
        #endregion

    }
}
