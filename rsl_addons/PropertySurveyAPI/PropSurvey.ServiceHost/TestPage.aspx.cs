﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PropSurvey.BuisnessLayer.Appointment;
using PropSurvey.Contracts.Data;
using PropSurvey.Contracts.Fault;
using PropSurvey.ServiceHost.IService;
using PropSurvey.Utilities.Constants;
using PropSurvey.Utilities.ErrorSetter;
using PropSurvey.BuisnessLayer.Authentication;
using PropSurvey.Utilities.Helpers;
using System.IO;
using System.Text;

namespace PropSurvey.ServiceHost
{
    public partial class TestPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Button5_Click(object sender, EventArgs e)
        {
            try
            {
                FaultStatusData faultStatusData = new FaultStatusData();
                faultStatusData.AppointmentId = 34679;
                faultStatusData.FollowOnRequired = true;
                faultStatusData.Notes = "These are notes";
                faultStatusData.ProgressStatus = "Completed";


                faultStatusData.FaultLogID = 386;
                faultStatusData.FaultRepairIDs = "1,2,3";
                faultStatusData.FollowOnNotes = "These are follow on notes";

                //faultStatusData.FaultRepairListData = faultRepairList;
                string JSONFaultAppointment = Utilities.Helpers.JsonHelper.SerializerToString(faultStatusData);
                txtResponse.Text = JSONFaultAppointment;
            }
            catch (Exception ex)
            {
                txtResponse.Text = ex.InnerException.Message;
            }

        }

        protected void FetchAppointments(object sender, EventArgs e)
        {
            //string reponseJSON;
            AppointmentBl apptBl = new AppointmentBl();
            try
            {
                //reponseJSON = apptBl.getAllAppointmentsByUser(txtUserName.Text, "all");
                //reponseJSON = apptBl.getAllAppointmentsByAppType("29-07-2013", "04-08-2013", 0,"adnan.mirza,93090930000,0");
                //txtResponse.Text = reponseJSON;
            }
            catch (Exception ex)
            {
                txtResponse.Text = ex.Message;
            }
        }

        protected void StartAppointment(object sender, EventArgs e)
        {
            AppointmentBl apptBl = new AppointmentBl();
            ResultBoolData resultBoolData = new ResultBoolData();
            try
            {
                //resultBoolData = apptBl.updateFaultAppointmentProgressStatus(Convert.ToInt32(txtappointmentID.Text), "InProgress");
                txtResponse.Text = resultBoolData.result.ToString();
            }
            catch (Exception ex)
            {
                txtResponse.Text = ex.Message;
            }
        }

        protected void GetJobStatus(object sender, EventArgs e)
        {
            try
            {
                AppointmentBl apptBl = new AppointmentBl();
                int apptID = -1;
                string reponseJSON = "";
                apptID = Convert.ToInt32(txtappointmentID.Text);

                reponseJSON = apptBl.getFaultJobStatus(apptID, txtUserName.Text);
                txtResponse.Text = reponseJSON;
            }
            catch (Exception ex)
            {
                txtResponse.Text = ex.InnerException.Message;
            }
        }

        protected void UpdateJobStatus(object sender, EventArgs e)
        {
            try
            {
                AppointmentBl apptBl = new AppointmentBl();
                FaultStatusData faultStatusData = JsonHelper.JsonDeserialize<FaultStatusData>(txtResponse.Text);
                //ResultBoolData resultBoolData = apptBl.updateFaultJobStatus(faultStatusData, txtUserName.Text);

                //txtResponse.Text = resultBoolData.result.ToString();
            }
            catch (Exception ex)
            {
                txtResponse.Text = ex.InnerException.Message;
            }
        }
    }
}