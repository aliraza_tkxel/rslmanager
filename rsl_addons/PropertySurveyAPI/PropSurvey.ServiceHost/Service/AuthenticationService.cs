﻿using System;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling.Logging;
using PropSurvey.BuisnessLayer.Authentication;
using PropSurvey.Contracts.Data;
using PropSurvey.Contracts.Fault;
using PropSurvey.ServiceHost.IService;
using PropSurvey.Utilities.Helpers;

namespace PropSurvey.ServiceHost.Service
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class AuthenticationService:IAuthentication
    {
        /// <summary>
        /// This function calls the business layer to authenticate the function from database.
        /// </summary>
        /// <param name="userName">username of surveyor</param>
        /// <param name="password">password of surveyor</param>
        /// <returns>User Full Name, isActive, Last Logged In Data Time, Salt, UserId </returns>

        public ResponseTemplate<UserData> authenticateUser(UserAuthenticationParam varUserAuthParam)
        {
            try
            {
                //varUserAuthParam.username = SecurityEncryption.DecryptData(varUserAuthParam.username);
                //varUserAuthParam.password = SecurityEncryption.DecryptData(varUserAuthParam.password);
                AuthenticationBl authBl = new AuthenticationBl();
                //Calling business logic method           
                return authBl.findUser(varUserAuthParam.username, varUserAuthParam.password, varUserAuthParam.devicetoken);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
                //Instantiating classes objects
                MessageData objMessageData = new MessageData();
                //EmptyResponse objEmptyResponse = new EmptyResponse();
                ResponseTemplate<UserData> objResponseTemplate = new ResponseTemplate<UserData>();
                //Assigning values {error code and message} to MessageData object
                objMessageData.message = ErrorFault.message;
                objMessageData.code = ErrorFault.errorCode;
                //Assigning Message object to Response Template object
                objResponseTemplate.status = objMessageData;
                //objResponseTemplate.response = objEmptyResponse.response;
                return objResponseTemplate;
                //throw new WebFaultException<MessageData>(objResponseTemplate, HttpStatusCode.OK);
            }
        }


        public ResponseTemplate<MessageData> emailForgottenPassword(String emailAddress)
        {
            try
            {
                AuthenticationBl authBl = new AuthenticationBl();
                //Calling business logic method           
                return authBl.emailForgottenPassword(emailAddress);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
                MessageData objMessageData = new MessageData();
                ResponseTemplate<MessageData> objResponseTemplate = new ResponseTemplate<MessageData>();
                 objMessageData.message = ErrorFault.message;
                objMessageData.code = ErrorFault.errorCode;
                objResponseTemplate.status = objMessageData; 
                return objResponseTemplate;
                
            }
        }

        public ResponseTemplate<MessageData> updatePassword(UpdatePasswordParam objUpdatePasswordParam)
        {
            try
            {
                AuthenticationBl authBl = new AuthenticationBl();
                objUpdatePasswordParam.username = SecurityEncryption.DecryptData(objUpdatePasswordParam.username);
                objUpdatePasswordParam.oldPassword  = SecurityEncryption.DecryptData(objUpdatePasswordParam.oldPassword );
                objUpdatePasswordParam.newPassword = SecurityEncryption.DecryptData(objUpdatePasswordParam.newPassword);

                return authBl.updatePassword(objUpdatePasswordParam);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");
                MessageData objMessageData = new MessageData();
                ResponseTemplate<MessageData> objResponseTemplate = new ResponseTemplate<MessageData>();
                objMessageData.message = ErrorFault.message;
                objMessageData.code = ErrorFault.errorCode;
                objResponseTemplate.status = objMessageData;
                return objResponseTemplate;

            }
        }


        public void LogoutUser(string userName, string salt)
        {
            try
            {
                
                AuthenticationBl authBl = new AuthenticationBl();
                if (authBl.checkForValidSession(userName, salt))
                {
                    authBl.LogoutUser(SecurityEncryption.DecryptData(userName));
                }
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.NotFound);
            }
        }

        public string getSalt(string userName)
        {
        try
            { 
                AuthenticationBl authBl = new AuthenticationBl();
                return authBl.getSalt(userName);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.NotFound);
            }
        }

        public int checkUser(string userName, string password)
        {
            AuthenticationBl authBl = new AuthenticationBl();
            return authBl.CheckUser(userName, password);
        }

      
        #region "Can use access Gas Tab"
        /// <summary>
        /// This function returns a boolean indicating that user can access the gas tab or not.
        /// </summary>
        /// <returns></returns>
        public bool GasTabRights(int userid, string userName, string salt)
        {
            try
            {   
                AuthenticationBl authBl = new AuthenticationBl();
                if (authBl.checkForValidSession(userName, salt))
                {
                    return authBl.GetGasTabRights(userid);
                }

                return false;
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;
                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);

            }
        }

        #endregion
       
    }
}
