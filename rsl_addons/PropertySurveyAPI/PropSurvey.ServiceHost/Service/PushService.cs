﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel.Activation;
using System.ServiceModel;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PropSurvey.Contracts.Fault;
using System.ServiceModel.Web;
using System.Net;
using PropSurvey.BuisnessLayer.Push;
using PropSurvey.ServiceHost.IService;
using PropSurvey.Utilities.Helpers;
using PropSurvey.Utilities.Constants;
using Microsoft.Practices.EnterpriseLibrary.Logging;

namespace PropSurvey.ServiceHost.Service
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class PushService : IPush
    {

        #region send Push Notification for Fault Appointment
        /// <summary>
        /// This function will send push notification for fault appointment
        /// </summary>
        /// <returns>Success or failure</returns>
        public bool sendPushNotificationFault(int appointmentId, int type, bool isSchemeBlockAppointment = false)
        {
            try
            {
                string host = HttpContext.Current.Request.Url.Host.ToString();

                Logger.Write("Host: " + host);

                string serverinUse = String.Empty;

                if (host.StartsWith(ApplicationConstants.BHG_Live_Name))
                    serverinUse = ApplicationConstants.BHG_Live_Name;
                else if (host.StartsWith(ApplicationConstants.BHG_Test_Name))
                    serverinUse = ApplicationConstants.BHG_Test_Name;
                else if (host.StartsWith(ApplicationConstants.BHG_UAT_Name))
                    serverinUse = ApplicationConstants.BHG_UAT_Name;
                else if (host.StartsWith(ApplicationConstants.BHG_Dev_Name))
                    serverinUse = ApplicationConstants.BHG_Dev_Name;
                else //else case is only for local use.
                    serverinUse = ApplicationConstants.BHG_Test_Name;

                Logger.Write("Server in Use: " + serverinUse);

                Logger.Write("Push notification call appointmentId: " + appointmentId);

                PushNotificationBl pushNotificationBl = new PushNotificationBl();
                return pushNotificationBl.sendPushNotificationFault(appointmentId, type, serverinUse, isSchemeBlockAppointment);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }
        #endregion

        #region send Push Notification for Appliances and Planned Appointment(s)

        /// <summary>
        /// This function will send push notification for Appliances and Planned Appointment(s)
        /// </summary>
        /// <returns>Success or failure</returns>
        public bool sendPushNotificationApplianceAndPlanned(string appointmentMessage, int operatorId)
        {
            try
            {
                appointmentMessage = HttpUtility.UrlDecode(appointmentMessage).Replace(@"\\",@"\").Replace(@"\n",Environment.NewLine);

                string hostURL = HttpContext.Current.Request.Url.Host.ToString();
                Logger.Write("Host: " + hostURL);

                PushNotificationBl pushNotificationBl = new PushNotificationBl();
                return pushNotificationBl.sendPushNotification(appointmentMessage, operatorId, hostURL);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.BadRequest);
            }
        }
        #endregion
#region Send Push Notification to Operative having incomplete appointments

        public bool sendPushNotificationDocumentExpiry()
        {
            return new PushNotificationBl().sendPushNotificationDocumentExpiary();
        }

        #endregion
        #region Send Push Notification to Operative having incomplete appointments

        public bool sendOverduePushNotification()
        {
            return new PushNotificationBl().sendOverduePushNotification();
        }

        #endregion

    }
}