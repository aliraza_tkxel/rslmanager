﻿using System;
using System.Collections.Generic;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;
using PropSurvey.BuisnessLayer.IssuedReceivedBy;
using PropSurvey.Contracts.Data;
using PropSurvey.Contracts.Fault;
using PropSurvey.ServiceHost.IService;
using PropSurvey.Utilities.Constants;
using PropSurvey.Utilities.ErrorSetter;
using PropSurvey.BuisnessLayer.Authentication;
using PropSurvey.Utilities.Helpers;
using System.IO;

namespace PropSurvey.ServiceHost.Service
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall)]
    public class IssuedReceivedBy : IIssuedReceivedBy
    {

        #region get IssuedReceivedBy Data
        /// <summary>
        /// This function returns the IssuedReceivedBy data
        /// </summary>
        /// <returns>List of IssuedReceivedBy data objects</returns>
        public IssuedReceivedByData getIssuedReceivedByFormData(int AppointmentID, string userName, string salt)
        {
            try
            {
                IssuedReceivedByBl apptBl = new IssuedReceivedByBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                return apptBl.getIssuedReceivedByFormData(AppointmentID);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }

        //Change#34 - behroz - 12/17/2012 - Start
        /// <summary>
        /// This function returns the IssuedReceivedBy data
        /// </summary>
        /// <returns>List of IssuedReceivedBy data objects</returns>
        //public LGSRData getIssuedReceivedByFormDataGas(string PropertyID, string userName, string salt)
        //{
        //    try
        //    {
        //        IssuedReceivedByBl apptBl = new IssuedReceivedByBl();
        //        AuthenticationBl authBl = new AuthenticationBl();
        //        authBl.checkForValidSession(userName, salt);

        //        return apptBl.getIssuedReceivedByFormData(PropertyID);
        //    }
        //    catch (Exception ex)
        //    {
        //        ExceptionPolicy.HandleException(ex, "Exception Policy");

        //        MessageFault msgFault = new MessageFault();
        //        msgFault.message = ErrorFault.message;
        //        msgFault.isErrorOccured = true;
        //        msgFault.errorCode = ErrorFault.errorCode;

        //        throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
        //    }
        //}
        //Change#34 - behroz - 12/17/2012 - End
        public LGSRData getIssuedReceivedByFormDataGas(int journalId, string userName, string salt)
        {
            try
            {
                IssuedReceivedByBl apptBl = new IssuedReceivedByBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);

                return apptBl.getIssuedReceivedByFormDataGas(journalId);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }
        #endregion

        #region save IssuedReceivedBy Data
        /// <summary>
        /// This function saves the IssuedReceivedBy Data in the database
        /// </summary>
        /// <param name="appData">The object of IssuedReceivedBy Data</param>
        /// <returns>the IssuedReceivedBy id if successfully save otherwise 0 </returns>

        public int saveIssuedReceivedByData(IssuedReceivedByData workData, string userName, string salt)
        {
            try
            {
                IssuedReceivedByBl pptBl = new IssuedReceivedByBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                validateIssuedByData(workData);
                return pptBl.saveIssuedReceivedByData(workData);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }

        //Change#12 - Behroz - 12/13/2012 - Start
        /// <summary>
        /// This function saves the IssuedReceivedBy Data in the database
        /// </summary>
        /// <param name="appData">The object of IssuedReceivedBy Data</param>
        /// <returns>the IssuedReceivedBy id if successfully save otherwise 0 </returns>
        public int saveIssuedReceivedByDataGas(LGSRData workData, string userName, string salt)
        {
            try
            {
                IssuedReceivedByBl pptBl = new IssuedReceivedByBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);                
                validateIssuedByData(workData);
                return pptBl.saveIssuedReceivedByData(workData);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }
        //Change#12 - Behroz - 12/13/2012 - End
        
        #endregion

        #region update IssuedReceivedBy Data

        /// <summary>
        /// This function updates IssuedReceivedBy Data in the database
        /// </summary>
        /// <param name="appData">The object of IssuedReceivedBy Data</param>
        /// <returns>true if successfully save otherwise 0 </returns>
        public bool updateIssuedReceivedByData(IssuedReceivedByData insData, string userName, string salt)
        {
            try
            {
                IssuedReceivedByBl pptBl = new IssuedReceivedByBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                validateIssuedByData(insData);
                return pptBl.updateIssuedReceivedByData(insData);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }
                
        /// <summary>
        /// This function updates IssuedReceivedBy Data in the database for gas
        /// </summary>
        /// <param name="appData">The object of IssuedReceivedBy Data</param>
        /// <returns>true if successfully save otherwise 0 </returns>
        public bool updateIssuedReceivedByDataGas(LGSRData insData, string userName, string salt)
        {
            try
            {
                IssuedReceivedByBl pptBl = new IssuedReceivedByBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                validateIssuedByData(insData);
                return pptBl.updateIssuedReceivedByData(insData);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }

        /// <summary>
        /// This function saves the CP12 Document in database
        /// </summary>
        /// <param name="LGSRID"></param>
        /// <param name="documentStream"></param>
        /// <param name="userName"></param>
        /// <param name="salt"></param>
        /// <returns></returns>
        public bool updateCP12Document(Stream stream, int LGSRID, string userName, string salt, bool tenantHanded)
        {
            try
            {
                IssuedReceivedByBl pptBl = new IssuedReceivedByBl();
                AuthenticationBl authBl = new AuthenticationBl();
                authBl.checkForValidSession(userName, salt);
                return pptBl.updateCP12Document(stream, LGSRID, tenantHanded);
            }
            catch (Exception ex)
            {
                ExceptionPolicy.HandleException(ex, "Exception Policy");

                MessageFault msgFault = new MessageFault();
                msgFault.message = ErrorFault.message;
                msgFault.isErrorOccured = true;
                msgFault.errorCode = ErrorFault.errorCode;

                throw new WebFaultException<MessageFault>(msgFault, HttpStatusCode.InternalServerError);
            }
        }
        
        #endregion

        #region validate IssuedReceivedBy Data
        /// <summary>
        /// This function validate IssuedReceivedBy Data
        /// </summary>
        /// <param name="appData">The object of IssuedReceivedBy Data</param>
        /// <returns>true if successfully validate otherwise 0 </returns>

        public void validateIssuedByData(IssuedReceivedByData insData)
        {
            IssuedReceivedByData validate = new IssuedReceivedByData();
            if (!(validate.validateReceiver(insData.ReceivedOnBehalfOf)))
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.InvalidReceivedOnBehalfOf, true, MessageCodesConstants.InvalidReceivedOnBehalfOf);
                throw new ArgumentException(MessageConstants.InvalidReceivedOnBehalfOf);
            }
        }

        //Change#12 - Behroz - 12/17/2012 - Start
        /// <summary>
        /// This function validate IssuedReceivedBy Data
        /// </summary>
        /// <param name="appData">The object of IssuedReceivedBy Data</param>
        /// <returns>true if successfully validate otherwise 0 </returns>
        public void validateIssuedByData(LGSRData insData)
        {
            LGSRData validate = new LGSRData();
            if (!(validate.validateReceiver(insData.ReceivedOnBehalfOf)))
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.InvalidReceivedOnBehalfOf, true, MessageCodesConstants.InvalidReceivedOnBehalfOf);
                throw new ArgumentException(MessageConstants.InvalidReceivedOnBehalfOf);
            }
        }
        //Change#12 - Behroz - 12/17/2012 - End
        #endregion

    }
}