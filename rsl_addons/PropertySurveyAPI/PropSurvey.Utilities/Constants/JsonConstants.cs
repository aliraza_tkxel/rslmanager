﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PropSurvey.Utilities.Constants
{
    public static class JsonConstants
    {
        public const string LastReplaced = "Last Replaced";
        public const string ReplacementDue = "Replacement Due";
        public static int LastRewiredId = int.Parse(System.Configuration.ConfigurationManager.AppSettings["LastRewiredId"]);
        public static int ConsumerUnitReplacementId = int.Parse(System.Configuration.ConfigurationManager.AppSettings["ConsumerUnitReplacementId"]);
        public static int ElectricsId = int.Parse(System.Configuration.ConfigurationManager.AppSettings["ElectricsId"]);
        public static int WcLastReplacedId = int.Parse(System.Configuration.ConfigurationManager.AppSettings["Internal_Accomodation_Seperate_WC_LastReplaced_Id"]);
        public static int WcReplacementDueId = int.Parse(System.Configuration.ConfigurationManager.AppSettings["Internal_Accomodation_Seperate_WC_ReplacementDue_Id"]);
        public static int HandBasinLastReplacedId = int.Parse(System.Configuration.ConfigurationManager.AppSettings["Internal_Accomodation_Seperate_HandBasin_LastReplaced_Id"]);
        public static int HandBasinReplacementDueId = int.Parse(System.Configuration.ConfigurationManager.AppSettings["Internal_Accomodation_Seperate_HandBasin_ReplacementDue_Id"]);
        public static int MainDwellingRoofLastReplacedId = int.Parse(System.Configuration.ConfigurationManager.AppSettings["Main_Dwelling_Roof_LastReplaced_Id"]);
        public static int MainDwellingRoofReplacementDueId = int.Parse(System.Configuration.ConfigurationManager.AppSettings["Main_Dwelling_Roof_ReplacementDue_Id"]);
        public const string LastRewired = "Last Rewired";
        public const string RewireDue = "Rewire Due";
        public const string CurLastReplaced = "CUR Last Replaced";
        public const string CurDue = "Cur Due";
        public const string ElectricUpgradeDue = "Electric Upgrade Due";
        public const string UpgradeLastDone = "Electrical Upgrade Last Done";
        public const string ManufacturerParam = "Manufacturer";
        public const string HeatingFuelParam = "Heating Fuel";
 
        public static int StructureId = int.Parse(System.Configuration.ConfigurationManager.AppSettings["Main_Dwelling_Structure_Id"]);
        public static int CloakroomId = int.Parse(System.Configuration.ConfigurationManager.AppSettings["Internal_Accomodation_Cloakroom_Id"]);
        public static int FasciasId = int.Parse(System.Configuration.ConfigurationManager.AppSettings["External_Dwelling_Fascias_Id"]);
        public static int SurfaceId = int.Parse(System.Configuration.ConfigurationManager.AppSettings["External_Dewelling_Surface_Id"]);
        public static int SeparateId = int.Parse(System.Configuration.ConfigurationManager.AppSettings["Internal_Accomodation_Seperate_Id"]);

    }
}
