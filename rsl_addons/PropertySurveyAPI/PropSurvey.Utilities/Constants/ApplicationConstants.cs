﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PropSurvey.Utilities.Constants
{
    public class ApplicationConstants
    {
        /*************************************
         * Logging Categories
         */
        public const string OverDueFailure = "OverdueFailure";
        public const string PushService = "PushService";
        /**************************************/
        public static string BHG_Dev_Name = "devcrm";
        public static string BHG_Test_Name = "testcrm";
        public static string BHG_Live_Name = "crm";
        public static string BHG_UAT_Name = "uatcrm";
        public static int  plannedComponentId = 0;
        public static int previousComponentId = 0;
        public static int nextComponentId = 0;

    }
}
