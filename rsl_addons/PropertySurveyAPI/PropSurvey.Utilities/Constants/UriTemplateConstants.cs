﻿
namespace PropSurvey.Utilities.Constants
{
    public static class UriTemplateConstants
    {

        //property agent - embed propertyagent/ before the below URL
        public const string GetAllAgents = "all?username={username}&salt={salt}";
        public const string SaveAgent = "save?username={userName}&salt={salt}";
        public const string UpdateAgent = "update?username={userName}&salt={salt}";
        public const string GetAgentToAppointment = "agenttoappointment/get?appointmentid={appointmentId}&username={username}&salt={salt}";
        public const string GetAgentDetails = "agentdetails/get?appointmentid={appoinmentId}&username={username}&salt={salt}";
        public const string SaveAgentToAppointment = "agenttoappointment/save?username={userName}&salt={salt}";
        public const string UpdateAgentToAppointment = "agenttoappointment/update?username={userName}&salt={salt}";

        //Installation pipework - embed installationpipework/ before the below urls
        public const string GetInstallationPipework = "get?appointmentid={appointmentid}&username={username}&salt={salt}";
        public const string GetInstallationPipeworkGas = "get/gas?JournalId={JournalId}&username={username}&salt={salt}";
        public const string SaveInstallationPipework = "save?username={userName}&salt={salt}";
        public const string SaveInstallationPipeworkGas = "save/gas?username={userName}&salt={salt}";
        public const string UpdateIssuedReceivedGas = "update/gas?username={userName}&salt={salt}";
        public const string UpdateCP12DocumentGas = "update/gas/doc?id={id}&username={userName}&salt={salt}&tenantHanded={tenantHanded}";
        public const string UpdateInstallationPipework = "update?username={userName}&salt={salt}";

        //Faults - embed Faults/ before the below urls
        public const string GetFaults = "get?appointmentid={appointmentid}&username={username}&salt={salt}";
        public const string FetchGasDefects = "fetchGasDefects";
        // public const string GetDefectCategory = "get/defects?username={username}&salt={salt}";
        public const string GetFaultId = "get/id?propertyId={propertyId}&username={username}&salt={salt}";
        public const string DeleteFaultId = "deleteDefectImage";
        public const string saveDefectImage = "saveDefectImage?faultId={faultId}&fileExt={fileExt}&propertyId={propertyId}&username={username}&salt={salt}&imageIdentifier={imageIdentifier}";
        public const string SaveFaultRepairImage = "saveFaultRepairImage?PropertyId={PropertyId}&schemeId={schemeId}&blockId={blockId}&jsNumber={jsNumber}&fileExt={fileExt}&createdBy={createdBy}&isBeforeImage={isBeforeImage}&username={username}&salt={salt}&imageIdentifier={imageIdentifier}";
        //Stream stream, string propertyId, int appointmentId, int itemId, string fileName, int updatedBy, string fileSize, string pictureTag = "", bool isDefault = false        
        public const string GetFaultsOnly = "faults/get?appointmentid={appointmentid}&username={username}&salt={salt}";
        public const string GetFaultsOnlyGas = "faults/get/gas?journalid={journalId}&username={username}&salt={salt}";
        public const string GetGeneralComments = "comments/get?appointmentid={appointmentid}&username={username}&salt={salt}";
        public const string GetGeneralCommentsGas = "comments/get/gas?journalid={journalId}&username={username}&salt={salt}";
        public const string SaveFaults = "save?username={userName}&salt={salt}";
        public const string SaveFaultsGas = "saveFaultsGas";
        public const string UpdateFaults = "update?username={userName}&salt={salt}";
        public const string UpdateFaultsGas = "update/gas?username={userName}&salt={salt}";

        //IssuedReceivedBy - embed issuedreceivedby/ before the below urls
        public const string GetIssuedReceivedBy = "get?appointmentid={appointmentid}&username={username}&salt={salt}";
        public const string SaveIssuedReceivedBy = "save?username={userName}&salt={salt}";
        public const string UpdateIssuedReceivedBy = "update?username={userName}&salt={salt}";

        //user authentication - embed authenticate/ before the below urls
        //public const string AuthenticateUser = "user?username={userName}&password={password}&deviceToken={deviceToken}";
        public const string AuthenticateUser = "user";
        public const string EmailForgottenPassword = "emailForgottenPassword?emailAddress={emailAddress}";
        public const string UpdatePassword = "updatePassword";
        public const string LogoutUser = "logout?username={userName}&salt={salt}";
        public const string GetSalt = "salt?username={userName}";

        //customer - embed customer/ before the below urls
        public const string GetCustomerInfo = "info/{customerid}";
        public const string GetCustomerVulData = "vul/{customerid}";
        public const string GetEmployeeSignatureImage = "images/employeesignature/{employeeId}";

        //appointment - embed appointment/ before the below urls    

        //public const string GetAllAppointmentsByAppType = "allByAppType?startdate={startdate}&enddate={enddate}&exlcudeOverdue={exlcudeOverdue}&applicationType={applicationType}&appUserAgent={userName}&salt={salt}";
        public const string GetAllAppointmentsByAppType = "getAppointmentsByAppType";

        


        //public const string GetAllStockAppointments = "allStock?username={userName}&salt={salt}";
        public const string GetAllStockAppointments = "getStockAppointments";

        public const string GetAllAppointmentsByUser = "allByUser?username={userName}&filter={filter}&salt={salt}";
        public const string UpdateFaultAppointmentProgressStatus = "updateFaultAppntStatus?username={username}&appointmentId={appointmentId}&progressStatus={progressStatus}&salt={salt}";
        public const string GetFaultJobStatus = "faultStatus/Get?appointmentid={appointmentId}&username={userName}";
        public const string GetPauseReasonList = "fetchPauseReasonList";
        public const string UpdateFaultJobStatus = "faultStatus/Update?username={userName}&salt={salt}";
        public const string NoEntryAppointmentStatus = "appointmentStatus/NoEntry?username={userName}&salt={salt}";
        public const string GetFaultRepairList = "fetchFaultRepairList";
        public const string UpdateAppointmentProgressStatus = "update/status";
        public const string GetAndSetStockAppointmentProgressStatus = "progress/status?appointmentid={appointmentId}&username={userName}";

        public const string SaveAppointment = "saveStockAppointment";
        public const string GetAndSetGasAppointmentProgressStatus = "progress/gas/status?appointmentid={appointmentId}&userid={userId}";
        public const string SaveAppointmentGas = "save/gas?username={userName}&salt={salt}";
        public const string GetUserAppointmentsGas = "get/gas?username={userName}&filter={filter}&salt={salt}";
        public const string CompleteAppointment = "CompleteAppointment";
        public const string GetStatus = "get/status?username={userName}&salt={salt}";
        public const string saveNoEntryGas = "noentry/save/gas?username={userName}&salt={salt}";
        public const string GetStockAppointmentsByUser = "get?username={userName}&filter={filter}&salt={salt}";
        public const string GetAllUsers = "surveyors";
        public const string GetAllUsersGas = "users/all/gas?username={userName}&salt={salt}";
        public const string UpdateAppointment = "update?username={userName}&salt={salt}";
        public const string UpdateAppointmentGas = "update/gas?username={userName}&salt={salt}";

        public const string DeleteAppointment = "deleteAppointment";
        public const string GetAppointment = "getappointment?appointmentid={appointmentId}";
        public const string GetGasTabRights = "get/gas/rights?userid={userid}&username={userName}&salt={salt}";
        public const string getNoEntries = "appointmentInfo/get?propertyId={propertyId}&username={userName}&salt={salt}";
        public const string saveNoEntry = "noentry/save?username={userName}&salt={salt}";
        public const string GetIndexScreenInfo = "indexinfo/get?appointmentid={appointmentid}&username={username}&salt={salt}";
        public const string GetIndexScreenInfoGas = "indexinfo/get/gas?appointmentid={appointmentid}&username={username}&salt={salt}";

        public const string searchProperty = "searchProperty";
        public const string FindPropertyGas = "search/gas?skip={skip}&top={top}&reference={reference}&housenumber={housenumber}&street={street}&postcode={postcode}&surname={surname}";
        public const string GetPropertyImageNames = "images?itemId={itemId}&propertyId={propertyId}";
        public const string GetPropertyImage = "images/{propertyId}?imgname={imageName}";
        public const string GetDefaultPropertyImage = "images/default/{propertyId}";
        public const string UploadPropertyImage = "uploadPropertyImages?propertyId={propertyId}&appointmentId={appointmentId}&itemId={itemId}&updatedby={updatedBy}&fileExt={fileExt}&type={type}&username={username}&salt={salt}&propertyPicId={propertyPicId}&isDefault={isDefault}&imageIdentifier={imageIdentifier}&heatingId={heatingId}";
        //public const string UpdatePropertyDefaultImage = "updatePropertyDefaultImage?propertyId={propertyId}&appointmentId={appointmentId}&itemId={itemId}&updatedby={updatedBy}&fileExt={fileExt}&type={type}&username={username}&salt={salt}&isDefault={isDefault}";


        public const string UpdatePropertyDefaultImage = "updatePropertyDefaultImage?propertyId={propertyId}&propertyPicId={propertyPicId}&updatedBy={updatedBy}&fileExt={fileExt}";
        public const string DeletePropertyImage = "deletePropertyImage";
        public const string GetPropertyDimensions = "dimensions/{propertyId}";
        public const string UpdatePropertyDimensions = "dimensions/update";
        public const string GetPropertyAsbestosRisk = "get/asb/{propertyId}";

        //survey - embed survey/ before the below urls
        public const string SaveSurveyForm = "save?username={userName}&salt={salt}";
        public const string GetSavedSurveyForm = "get?appointmentid={appointmentId}&portionname={propertyPortionName}";
        // public const string GetAllSavedSurveyForms = "all?appointmentid={appointmentId}";
        public const string GetAllSavedSurveyForms = "getstocksurvey";
        public const string getSurveyorSurveyForms = "getsurveys?username={userName}";

        //appliances - embed appliances/ before the below urls        
        public const string GetAppliancesByPropertyIDGas = "get/gas?propertyId={propertyId}&journalid={journalId}&username={userName}&salt={salt}";
        public const string GetAppliancesData = "getAppliancesData";
        //public const string GetAppliancesByPropertyID = "get?propertyId={propertyId}&appointmentid={appointmentid}&username={userName}&salt={salt}";
        public const string GetAppliancesByPropertyID = "getApplianceByPropertyId";
        public const string GetApplianceTypeList = "type/get?username={userName}&salt={salt}";
        public const string GetApplianceLocationList = "location/get?username={userName}&salt={salt}";
        public const string GetApplianceModelList = "model/get?username={userName}&salt={salt}";
        public const string GetApplianceManufacturList = "manufacturer/get?username={userName}&salt={salt}";
        public const string SaveAnAppliance = "saveAnAppliance";
        public const string UpdateAnAppliance = "update?username={userName}&salt={salt}";
        public const string SaveAnApplianceType = "type/save?username={userName}&salt={salt}";
        public const string SaveApplianceLocation = "location/save?username={userName}&salt={salt}";
        public const string SaveAnApplianceModel = "model/save?username={userName}&salt={salt}";
        public const string SaveAnApplianceManufacturer = "manufacturer/save?username={userName}&salt={salt}";
        public const string GetApplianceInspectionByID = "inspection/get?applianceid={applianceid}&appointmentid={appointmentid}&username={userName}&salt={salt}";
        public const string GetApplianceInspectionByIDGas = "inspection/get/gas?applianceid={applianceid}&journald={journalId}&username={userName}&salt={salt}";
        public const string SaveApplianceInspection = "inspection/save?username={userName}&salt={salt}";
        public const string SaveApplianceInspectionGas = "inspection/save/gas?username={userName}&salt={salt}";
        public const string UpdateApplianceInspection = "inspection/update?username={userName}&salt={salt}";
        public const string GetApplianceCount = "get/count?propertyId={propertyId}&username={userName}&salt={salt}";
        public const string GetApplianceInspecedByAppointmentID = "inspection/getinspected?appointmentid={appointmentid}&username={userName}&salt={salt}";
        public const string GetApplianceInspecedByAppointmentIDGas = "inspection/getinspected/gas?journalid={journalId}&username={userName}&salt={salt}";
        public const string GetApplianceInspectionDetailsByAppointmentID = "inspection/details?appointmentid={appointmentid}&username={userName}&salt={salt}";
        public const string GetApplianceInspectionDetailsByPropertyIDGas = "inspection/gas/details?journalid={journalId}&username={userName}&salt={salt}";
        public const string UpdateDetectorCount = "updateDetectorCount";
        public const string AddUpdateDetectorInfo = "detector/addupdateinfo?detectortype={detectortype}&username={userName}&salt={salt}";

        //Org Inspection - embed orginspection/ before the below urls
        public const string SaveOrgInspection = "save?username={userName}&salt={salt}";
        public const string UpdateOrgInspection = "update?username={userName}&salt={salt}";
        public const string GetOrgInspection = "get?appointmentid={appointmentId}&username={userName}&salt={salt}";
        public const string GetGasEngineerList = "engineer/get?username={userName}&salt={salt}";
        public const string GetOrgList = "org/get?username={userName}&salt={salt}";
        public const string SaveAnOrg = "org/save?username={userName}&salt={salt}";
        public const string GetCompanyTypes = "companytype/get?username={userName}&salt={salt}";
        public const string GetPaymentTypes = "paymenttype/get?username={userName}&salt={salt}";

        //application screen - embed appscreen/ before the below urls
        //public const string GetApplicaitonScreens = "get?propertyId={propertyId}&username={userName}&salt={salt}";
        public const string GetApplicaitonScreens = "getSurveys";

        //Jobs - embed Jobs/ before the below urls
        public const string GetAllJobs = "getAllJobs?operativeID={operativeID}&username={username}&salt={salt}";
        public const string GetAllJobsString = "getAllJobsString?operativeID={operativeID}&username={username}&salt={salt}";
        public const string GetJobsByStatusID = "getJobsByStatusID?operativeID={operativeID}&jobstatusID={jobstatusID}&username={username}&salt={salt}";
        public const string UpdateCustomerContactFaultAppointment = "updateCusContact?username={userName}&salt={salt}";

        //Push Notification urls
        public const string SendPushNotificationFault = "notificationFault?appointmentId={appointmentId}&type={type}&isSchemeBlockAppointment={isSchemeBlockAppointment}";
        public const string sendPushNotificationApplianceAndPlanned = "notificationApplianceAndPlanned?appointmentMessage={appointmentMessage}&operatorId={operatorId}";
        public const string sendOverduePushNotifications = "sendOverduePushNotifications";
        public const string sendPushNotificationDocumentExpiry = "sendPushNotificationDocumentExpiry";
        public const string ServiceUriForCp12Document = "/CP12Upload/Default.aspx/populateCp12Document";
        public const string ServiceUriForPropertyRecordInspection = "/RSLPropertyStockInspection/Views/PDFCreator.aspx/populatePropertyRecordInspection";

        // public const string ServiceUriForCp12Document = "http://localhost:6980/Default.aspx/populateCp12Document";
        public const string PopulateStockInspection = "prepareStockInspectionDocument";

    }
}
