﻿using System;
using System.Web;
using System.Net;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PropSurvey.Utilities.Constants;

namespace PropSurvey.Utilities.Helpers
{
    public static class DocumentHelper
    {
        #region populate cp12Document
        /// <summary>
        /// Hit the cp12 service which generate pdf document and store in database 
        /// </summary>
        /// <param name="pid">propertyId</param>
        /// <param name="jid">JournalId</param>
        /// <returns>It returns success or failed </returns>
        public static string populateCp12Document(string propertyId, string journalId)
        {
            var requestURL = HttpContext.Current.Request.Url;
            Uri address = new Uri(requestURL.GetLeftPart(UriPartial.Authority) + UriTemplateConstants.ServiceUriForCp12Document);
            // Create the data we want to send
            StringBuilder jsonContent = new StringBuilder();
            jsonContent.Append("{\"pid\":\"" + propertyId + "\",");
            jsonContent.Append("\"jid\":" + journalId + "}");

            // Create a byte array of the data we want to send  
            byte[] byteData = UTF8Encoding.UTF8.GetBytes(jsonContent.ToString());


            //string jsonContent = "";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(address);
            request.Method = "POST";
            request.ContentLength = jsonContent.Length;
            request.ContentType = @"application/json";

            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            Byte[] byteArray = encoding.GetBytes(jsonContent.ToString());

            using (Stream dataStream = request.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
            }

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());
                return reader.ReadToEnd();
            }
        }
        #endregion

        #region populate Property Record Inspection
        /// <summary>
        /// Hit the Property Record Inspection service which generate pdf document and store in database 
        /// </summary>
        /// <param name="pid">propertyId</param>
        /// <param name="jid">surveyId</param>
        /// <param name="uid">createdBy</param>
        /// <returns>It returns success or failed </returns>
        public static string populatePropertyRecordInspection(string propertyId, string surveyId, string createdBy)
        {
            var requestURL = HttpContext.Current.Request.Url;
            Uri address = new Uri(requestURL.GetLeftPart(UriPartial.Authority) + UriTemplateConstants.ServiceUriForPropertyRecordInspection);            

            // Create the data we want to send
            StringBuilder jsonContent = new StringBuilder();
            jsonContent.Append("{\"pid\":\"" + propertyId + "\",");
            jsonContent.Append("\"sid\":\"" + surveyId + "\",");
            jsonContent.Append("\"uid\":\"" + createdBy + "\"}");

            // Create a byte array of the data we want to send  
            byte[] byteData = UTF8Encoding.UTF8.GetBytes(jsonContent.ToString());


            //string jsonContent = "";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(address);
            request.Method = "POST";
            request.ContentLength = jsonContent.Length;
            request.ContentType = @"application/json";

            System.Text.UTF8Encoding encoding = new System.Text.UTF8Encoding();
            Byte[] byteArray = encoding.GetBytes(jsonContent.ToString());

            using (Stream dataStream = request.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);
            }

            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());
                return reader.ReadToEnd();
            }
        }
        #endregion
    }
}
