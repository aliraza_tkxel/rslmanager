﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.IO;
using System.Security.Cryptography;


namespace PropSurvey.Utilities.Helpers
{
    public static class SecurityEncryption
    {
        public static string EncryptData(string input)
        {
            ////Set up the encryption objects
            //using (AesCryptoServiceProvider acsp = GetProvider(Encoding.Default.GetBytes("1234567891123456")))
            //{
            //    byte[] sourceBytes = Encoding.ASCII.GetBytes(input);
            //    ICryptoTransform ictE = acsp.CreateEncryptor();

            //    //Set up stream to contain the encryption
            //    MemoryStream msS = new MemoryStream();

            //    //Perform the encrpytion, storing output into the stream
            //    CryptoStream csS = new CryptoStream(msS, ictE, CryptoStreamMode.Write);
            //    csS.Write(sourceBytes, 0, sourceBytes.Length);
            //    csS.FlushFinalBlock();

            //    //sourceBytes are now encrypted as an array of secure bytes
            //    byte[] encryptedBytes = msS.ToArray(); //.ToArray() is important, don't mess with the buffer

            //    //return the encrypted bytes as a BASE64 encoded string
            //    return Convert.ToBase64String(encryptedBytes);
            //}



            //string key = "Tkxel";
            ////int key = 128;

            //List<char> encryptedtext = new List<char>();
            //int i = 0;
            //foreach (char c in input)
            //{
            //    int _c = (int)c ^ (int)key[i];
            //    encryptedtext.Add((char)_c);
            //    i++;
            //    if (i >= key.Length)
            //    {
            //        i = 0;
            //    }
            //}
            //return new string(encryptedtext.ToArray());

            return input;
        }

        public static string DecryptData(string input)
        {
            ////Set up the encryption objects
            //using (AesCryptoServiceProvider acsp = GetProvider(Encoding.Default.GetBytes("1234567891123456")))
            //{
            //    byte[] RawBytes = Convert.FromBase64String(input);
            //    ICryptoTransform ictD = acsp.CreateDecryptor();

            //    //RawBytes now contains original byte array, still in Encrypted state

            //    //Decrypt into stream
            //    MemoryStream msD = new MemoryStream(RawBytes, 0, RawBytes.Length);
            //    CryptoStream csD = new CryptoStream(msD, ictD, CryptoStreamMode.Read);
            //    //csD now contains original byte array, fully decrypted

            //    //return the content of msD as a regular string
            //    return (new StreamReader(csD)).ReadToEnd();
            //}


            //string key = "128";
            ////int key = 128;

            //List<char> encryptedtext = new List<char>();
            //int i = 0;
            //foreach (char c in input)
            //{
            //    int _c = (int)c ^ (int)key[i];
            //    encryptedtext.Add((char)_c);
            //    i++;
            //    if (i >= key.Length)
            //    {
            //        i = 0;
            //    }
            //}
            //return new string(encryptedtext.ToArray());
            return input;
        }


        private static AesCryptoServiceProvider GetProvider(byte[] key)
        {
            AesCryptoServiceProvider result = new AesCryptoServiceProvider();
            result.BlockSize = 128;
            result.KeySize = 128;
            result.Mode = CipherMode.CBC;
            result.Padding = PaddingMode.PKCS7;

            result.GenerateIV();
            result.IV = new byte[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

            byte[] RealKey = GetKey(key, result);
            result.Key = RealKey;
            // result.IV = RealKey;
            return result;
        }

        private static byte[] GetKey(byte[] suggestedKey, SymmetricAlgorithm p)
        {
            byte[] kRaw = suggestedKey;
            List<byte> kList = new List<byte>();

            for (int i = 0; i < p.LegalKeySizes[0].MinSize; i += 8)
            {
                kList.Add(kRaw[(i / 8) % kRaw.Length]);
            }
            byte[] k = kList.ToArray();
            return k;
        }

        public static string GenerateSalt()
        {
            string Rand1 = RandomString(12);

            // get 2nd random string 
            string Rand2 = RandomString(8);

            // creat full rand string
            return Rand1 + "-" + Rand2;

            //return "abcdef";
        }

        private static string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }

            return builder.ToString();
        }

    }
}









