﻿using System;
using System.Data;
using PropSurvey.Contracts.Data;
using PropSurvey.Contracts.Fault;
using PropSurvey.Dal.Authentication;
using PropSurvey.Dal.TestCase;
using PropSurvey.Utilities.Constants;
using PropSurvey.Utilities.ErrorSetter;
using PropSurvey.Utilities.Helpers;
using System.Collections.Generic;
using System.Text.RegularExpressions;


namespace PropSurvey.BuisnessLayer.Authentication
{
    public class AuthenticationBl
    {
        public ResponseTemplate<UserData> findUser(string userName, string password, string deviceToken)
        {

            try
            {
                //Access Data Access Layer
                AuthenticationDal authDal = new AuthenticationDal();
                //Instantiating classes {UserData and objMessageData} objects
                UserData objUserData = new UserData();
                ResponseTemplate<UserData> objResponseTemplate = new ResponseTemplate<UserData>();
                MessageData objMessageData = new MessageData();

                objUserData.userId = authDal.findUser(userName, password, deviceToken);
                string salt = string.Empty;
                //If user not found then return false
                if (objUserData.userId == 0)
                {

                    PropSurvey.Entities.SP_NET_SETLOGIN_THRESHOLD_Result loginThreshold = authDal.setLoginThreshold(userName);

                    if (loginThreshold != null)
                    {

                        if (loginThreshold.Locked == true)
                        {
                            ErrorFaultSetGet.setErrorFault(MessageConstants.AccessLockdownMsg, true, MessageCodesConstants.AccessLockdownCode);
                            throw new ArgumentException(MessageConstants.AccessLockdownMsg, "username or password");
                        }
                        else if (loginThreshold.Threshold == 4)
                        {
                            ErrorFaultSetGet.setErrorFault(MessageConstants.AccessLockdownWarningMsg, true, MessageCodesConstants.AccessLockdownWarningCode);
                            throw new ArgumentException(MessageConstants.AccessLockdownWarningMsg, "username or password");
                        }
                        else
                        {
                            ErrorFaultSetGet.setErrorFault(MessageConstants.IncorrectUsernamePasswordMsg, true, MessageCodesConstants.IncorrectUserNamePasswordCode);
                            throw new ArgumentException(MessageConstants.LoginSuccessMsg, "username or password");
                        }

                    }
                    else
                    {
                        ErrorFaultSetGet.setErrorFault(MessageConstants.LoginSuccessMsg, true, MessageCodesConstants.UserNamePasswordDoesNotExistCode);
                        throw new ArgumentException(MessageConstants.LoginSuccessMsg, "username or password");
                    }

                }
                else
                {
                    // Reset login threshold
                    authDal.resetLoginThreshold(userName);

                    //<User Information Start Here>
                    objUserData = authDal.getLastLoggedInDate(objUserData); //Get user last logged in date
                    if (objUserData.lastLoggedInDate != null)
                    {
                        authDal.updateLastLoggedInDate(objUserData);
                    }
                    else
                    {
                        objUserData.lastLoggedInDate = DateTime.Now;
                        authDal.insertLastLoggedInDate(objUserData);
                    }

                    salt = SecurityEncryption.GenerateSalt(); //Get salt info
                    objUserData.lastLoggedInDate = authDal.saveLoggedInUserSalt(userName, salt);

                    objUserData.salt = salt;
                    objUserData.userName = SecurityEncryption.EncryptData(userName);
                    objUserData.userId = authDal.getUserId(userName, password); //Get user id
                    objUserData.fullName = authDal.getUserFullName(userName, password); // Get username
                    objUserData.isActive = 1;
                    objResponseTemplate.response = objUserData;

                    //<Status Information Start Here>
                    objMessageData.code = MessageCodesConstants.successCode;
                    objMessageData.message = MessageConstants.success;
                    objResponseTemplate.status = objMessageData;


                }

                return objResponseTemplate;


            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == true)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
            }
        }


        #region Email Forgotten Password
        /// <summary>
        /// Email Forgotten Password
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        public ResponseTemplate<MessageData> emailForgottenPassword(String emailAddress)
        {
            try
            {
                AuthenticationDal authDal = new AuthenticationDal();
                PropSurvey.Entities.SP_GetUserInfoByEmail_Result userInfo = authDal.getUserInfoByEmail(emailAddress);
                ResponseTemplate<MessageData> objResponseTemplate = new ResponseTemplate<MessageData>();

                if (userInfo != null)
                {
                    var base64EncodedBytes = System.Convert.FromBase64String(userInfo.PASSWORD);
                    string decryptedPassword = System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
                    String body = "User Name: " + userInfo.LOGIN + "<br>Password: " + decryptedPassword;
                    EmailHelper.sendHtmlFormattedEmail(userInfo.WORKEMAIL, userInfo.WORKEMAIL, MessageConstants.LoginEmailSubjectMsg, body);
                    MessageData objMessageData = new MessageData();
                    MessageData objMsgData = new MessageData();
                    objMessageData.message = MessageConstants.LoginEmailSuccessMsg;
                    objMessageData.code = MessageCodesConstants.LoginEmailSuccessCode;
                    objMsgData.message = MessageConstants.success;
                    objMsgData.code = MessageCodesConstants.successCode;
                    objResponseTemplate.response = objMessageData;
                    objResponseTemplate.status = objMsgData;
                    return objResponseTemplate;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.InvalidEmailMsg, true, MessageCodesConstants.InvalidEmailCode);
                    throw new ArgumentException(MessageConstants.InvalidEmailMsg, "Invalid Email");
                }
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == true)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
            }

        }

        #endregion

        #region Update Password
        /// <summary>
        /// Update Password
        /// </summary>
        /// <param name="objUpdatePasswordParam"></param>
        /// <returns></returns>
        public ResponseTemplate<MessageData> updatePassword(UpdatePasswordParam objUpdatePasswordParam)
        {
            try
            {
                int resultStatus = -1;

                // Password Complexity Check
                string password = objUpdatePasswordParam.newPassword;

                Match match = Regex.Match(password, @"^.*(?=.{7,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).*$");

                if (password.Length < 7 ||  !match.Success)
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.PasswordComplexityMsg, true, MessageCodesConstants.PasswordComplexityCode);
                    throw new ArgumentException(MessageConstants.PasswordComplexityMsg, "Password Complexity Issue");
                }
                else
                {
                    AuthenticationDal authDal = new AuthenticationDal();
                    resultStatus = authDal.updatePassword(objUpdatePasswordParam);
                    ResponseTemplate<MessageData> objResponseTemplate = new ResponseTemplate<MessageData>();

                    if (resultStatus == 0)
                    {
                        MessageData objMessageData = new MessageData();
                        objMessageData.message = MessageConstants.PasswordUpdatedSuccessfullyMsg;
                        objMessageData.code = MessageCodesConstants.PasswordUpdatedSuccessfullyCode;
                        objResponseTemplate.response = objMessageData;
                        MessageData objMsgData = new MessageData();
                        objMsgData.message = MessageConstants.success;
                        objMsgData.code = MessageCodesConstants.successCode;
                        objResponseTemplate.status = objMsgData;

                        return objResponseTemplate;
                    }
                    else if (resultStatus == 10)
                    {
                        ErrorFaultSetGet.setErrorFault(MessageConstants.IncorrectUsernamePasswordMsg, true, MessageCodesConstants.IncorrectUserNamePasswordCode);
                        throw new ArgumentException(MessageConstants.IncorrectUsernamePasswordMsg, "Invalid UserName or Password");
                    }
                    else
                    {
                        ErrorFaultSetGet.setErrorFault(MessageConstants.PasswordExistedInHistoryMsg, true, MessageCodesConstants.PasswordExistedInHistoryCode);
                        throw new ArgumentException(MessageConstants.PasswordExistedInHistoryMsg, "Invalid UserName or Password");
                    }
                }
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == true)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
            }

        }

        #endregion

        #region log out user

        /// <summary>
        /// this fuction logouts the user
        /// </summary>
        /// <param name="username">username</param>
        /// <returns></returns>
        public void LogoutUser(string username)
        {

            try
            {
                AuthenticationDal authDal = new AuthenticationDal();
                authDal.LogoutUser(username);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == true)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
            }
        }
        #endregion

        #region check for Valid User session

        /// <summary>
        /// this fuction checks that the user session is a valid one
        /// </summary>
        /// <param name="username">username</param>
        /// <param name="salt">salt</param>
        /// <returns>true or false</returns>

        public bool checkForValidSession(string userName, string salt)
        {
            try
            {
                AuthenticationDal authDal = new AuthenticationDal();
                if (authDal.checkForValidSession(SecurityEncryption.DecryptData(userName), salt))
                {
                    return true;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.SessionExpiredMsg, true, MessageCodesConstants.UserNameSessionExpired);
                    throw new ArgumentException(MessageConstants.SessionExpiredMsg, "Your session has expired.");
                }
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == true)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
            }



        }
        #endregion

        public string getSalt(string userName)
        {
            try
            {
                AuthenticationDal authDal = new AuthenticationDal();
                return authDal.getSalt(userName);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == true)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
            }
        }

        public int CheckUser(string Username, string Password)
        {
            TestCaseDal testDal = new TestCaseDal();
            return testDal.CheckUser(Username, Password);
        }
        //Change#16 - Behroz - 18/12/2012 - Start
        /// <summary>
        /// This function return the rights of tab
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        public bool GetGasTabRights(int userid)
        {
            try
            {
                AuthenticationDal authDal = new AuthenticationDal();
                return authDal.isUserExistRights(userid);

            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == false)
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
                else
                {
                    throw ex;
                }
            }
        }
        //Change#16 - Behroz - 18/12/2012 - End

    }
}
