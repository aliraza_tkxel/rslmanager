﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PropSurvey.Contracts.Data;
using PropSurvey.Dal.Faults;
using System.Data;
using PropSurvey.Utilities.Constants;
using PropSurvey.Contracts.Fault;
using PropSurvey.Utilities.ErrorSetter;

namespace PropSurvey.BuisnessLayer.Faults
{
    public class JobsBl
    {
        #region get All Jobs Data
        /// <summary>
        /// This function returns all Jobs data for an operative
        /// </summary>
        /// <param name="operativeID">operativeID</param>
        /// <returns>List of Jobs data objects</returns>

        public List<JobsData> getAllJobsData(int operativeID)
        {
            try
            {
                JobsDal jobsDal = new JobsDal();
                List<JobsData> jobsData = new List<JobsData>();
                jobsData = jobsDal.getAllJobsData(operativeID);

                //add jobsheetnumbers to jobs Data
                this.addJobSheetNumbers(ref jobsData);
                return jobsData;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        //#region get Jobs Data
        ///// <summary>
        ///// This function returns the Jobs data
        ///// </summary>
        ///// <param name="appointmentId">appointment id</param>
        ///// <param name="faultStatusID">fault Status id</param>
        ///// <returns>List of Jobs data objects</returns>

        //public List<JobsData> getJobsDataByStatusID(int appointmentID, int faultStatusID)
        //{
        //    try
        //    {
        //        JobsDal jobsDal = new JobsDal();
        //        List<JobsData> jobsData = new List<JobsData>();
        //        jobsData = jobsDal.getJobsDataByStatusID(appointmentID, faultStatusID);

        //        //add jobsheetnumbers to jobs Data
        //        this.addJobSheetNumbersByStatusID(ref jobsData, faultStatusID);
        //        return jobsData;
        //    }
        //    catch (EntityException entityexception)
        //    {
        //        ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, ErrorCodesConstants.EntitiyExceptionCode);
        //        throw entityexception;
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, ErrorCodesConstants.GeneralExceptionCode);
        //        throw ex;
        //    }
        //}
        //#endregion

        #region add Job Sheet Number Data for an appointment
        /// <summary>
        /// This function adds Job Sheet Numbers list to Jobs data
        /// </summary>
        /// <param name="jobsData">jobs Data Object List</param>
        /// <param name="appointmentID">appointment id</param>
        /// <returns></returns>
        private void addJobSheetNumbers(ref List<JobsData> jobsData )
        {
            //JobsDal jobsDal = new JobsDal();
            //List<JobSheetNumberListData> JSNumberList = new List<JobSheetNumberListData>();
            //foreach (JobsData jobData in jobsData)
            //{
            //    jobData.JSNumberList = jobsDal.getJobSheetNumbersList(jobData.AppointmentID );
            //}
        }
        #endregion

        //#region add Job Sheet Number Data for an appointment by job StatusID
        ///// <summary>
        ///// This function adds Job Sheet Numbers list by job statusID to Jobs data
        ///// </summary>
        ///// <param name="jobsData">jobs Data Object List</param>
        ///// <param name="appointmentID">appointment id</param>
        ///// <param name="faultStatusID">job status id</param>
        ///// <returns></returns>
        //private void addJobSheetNumbersByStatusID(ref List<JobsData> jobsData, int faultStatusID)
        //{
        //    JobsDal jobsDal = new JobsDal();
        //    List<JobSheetNumberListData> JSNumberList = new List<JobSheetNumberListData>();
        //    foreach (JobsData jobData in jobsData)
        //    {
        //        jobData.JSNumberList = jobsDal.getJobSheetNumbersListByStatusID(jobData.AppointmentID , faultStatusID);
        //    }
        //}
        //#endregion
    }
}
