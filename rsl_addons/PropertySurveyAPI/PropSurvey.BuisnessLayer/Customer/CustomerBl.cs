﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using PropSurvey.Contracts.Data;
using PropSurvey.Dal.Customer;
using System.Data;
using PropSurvey.Utilities.Constants;
using PropSurvey.Contracts.Fault;
using PropSurvey.Utilities.ErrorSetter;

namespace PropSurvey.BuisnessLayer.Customer
{
    public class CustomerBl
    {
        #region Get Customer Info
        /// <summary>
        /// This function returns the customer record 
        /// </summary>
        /// <param name="customerId">customer id </param>
        /// <returns>It returns the customer data object</returns>
        public CustomerData getCustomerInformation(int customerId)
        {            
            try
            {
                CustomerDal custDal = new CustomerDal();
                CustomerData custData = new CustomerData();

                custData = custDal.getCustomerInfo(customerId);

                return custData;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }

        }
        #endregion 

        

        // Region added  30/04/2013 - START

        #region get Employee Signature Image Name

        /// <summary>
        /// This function returns the employee signature image name which was saved against employee
        /// </summary>
        /// <param name="employeeId">employee id</param>
        /// <returns>returns the name of the image</returns>

        public string getEmployeeSignatureImageName(int employeeId)
        {
            try
            {
                CustomerDal custDal = new CustomerDal();
                return custDal.getEmployeeSignatureImageName(employeeId);
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        // Region added  30/04/2013 - END

    }
}
