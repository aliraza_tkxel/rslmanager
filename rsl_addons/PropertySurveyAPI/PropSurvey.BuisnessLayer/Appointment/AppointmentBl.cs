﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using PropSurvey.Contracts.Data;
using PropSurvey.Contracts.Data.Stock;
using PropSurvey.Contracts.Fault;
using PropSurvey.Dal.Appointment;
using PropSurvey.Dal.Authentication;
using PropSurvey.Dal.Customer;
using PropSurvey.Dal.Property;
using PropSurvey.Entities;
using PropSurvey.Utilities.Constants;
using PropSurvey.Utilities.ErrorSetter;
using PropSurvey.Utilities.Helpers;
using PropSurvey.Contracts.Data.Appointment;
using PropSurvey.Dal.Survey;
using System.Text;
using System.Web;
using System.IO;
using PropSurvey.Dal.Appliances;
using PropSurvey.Dal.InstallationPipework;
using PropSurvey.Dal.Faults;
using PropSurvey.BuisnessLayer.Survey;
using PropSurvey.BuisnessLayer.Appliances;
using System.Transactions;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

namespace PropSurvey.BuisnessLayer.Appointment
{
    public class AppointmentBl
    {
        #region get All Appointments By Application Type
        /// <summary>
        /// This function returns Over Due and Next 5 days appointments along 
        /// with 'Property Info','Accomodations','Surveyors List' and 'Customer List'.
        /// Code at <01-Aug-2013>
        /// </summary>
        /// <returns>
        /// List of appointments,Property Info,Accomodations,Surveyors List and Customer List, data objects
        /// </returns>
        public ResponseTemplate<List<AllAppointmentsList>> getAllAppointmentsByAppType(AppointmentParam varAppointmentParam)
        {
            try
            {
                //Parsing User info {username,salt,application type}
                string[] arryUserInfo = varAppointmentParam.userinfo.Split(',');
                List<AllAppointmentsList> objAppointments = new List<AllAppointmentsList>();
                //Contain all appointments
                List<AllAppointmentsList> objAllAppointmentsList = new List<AllAppointmentsList>();
                //Response Template 
                ResponseTemplate<List<AllAppointmentsList>> objResponseTemplate = new ResponseTemplate<List<AllAppointmentsList>>();
                //Message Object
                MessageData objMessageData = new MessageData();
                //Checking Application Type {Stock}
                if (int.Parse(arryUserInfo[2]) == 0)
                {
                    List<int> existingAppointmentIds;
                    if (varAppointmentParam.appointmentIds != null)
                    {
                        existingAppointmentIds = varAppointmentParam.appointmentIds;
                    }
                    else
                    {
                        existingAppointmentIds = new List<int>();
                    }


                    // Getting all Stock appointments against userName
                    objAppointments = this.getStockAppointments(varAppointmentParam.startdate, varAppointmentParam.enddate, arryUserInfo[0], varAppointmentParam.fetchall, existingAppointmentIds);
                    //Getting Overdue Appointments
                    if (varAppointmentParam.includeoverdue == 1)
                    {
                        //Get Stock Overdue Appointments, and merging the Lists of Stock appointments{new and overdue} appointments
                        objAppointments = objAppointments.Union(this.getOverdueStockAppointments(varAppointmentParam.startdate, varAppointmentParam.enddate, arryUserInfo[0], varAppointmentParam.fetchall, existingAppointmentIds)).ToList();
                    }

                    objResponseTemplate.response = objAppointments;

                    //<Status Information Start Here>
                    objMessageData.code = MessageCodesConstants.successCode;
                    objMessageData.message = MessageConstants.success;
                    objResponseTemplate.status = objMessageData;

                }//End Check

                return objResponseTemplate;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(entityexception.InnerException.Message, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(ex.InnerException.Message, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        #region get All Stock Appointments
        /// <summary>
        /// This function returns Over Due and Next 5 days appointments along 
        /// with 'Property Info','Accomodations','Surveyors List' and 'Customer List'.
        /// Code at <31-March-2015>
        /// </summary>
        /// <returns>
        /// List of stock appointments,Property Info,Accomodations,Surveyors List and Customer List, data objects
        /// </returns>
        public ResponseTemplate<AppointmentResponse> getAllStockAppointments(AppointmentParam varAppointmentParam)
        {
            try
            {

                ResponseTemplate<AppointmentResponse> objresponse = new ResponseTemplate<AppointmentResponse>();

                //Parsing User info {username,salt,application type}
                string[] arryUserInfo = varAppointmentParam.userinfo.Split(',');

                //Checking Application Type {Stock}
                if (int.Parse(arryUserInfo[2]) == 0)
                {
                    List<int> existingAppointmentIds;
                    if (varAppointmentParam.appointmentIds != null)
                    {
                        existingAppointmentIds = varAppointmentParam.appointmentIds;
                    }
                    else
                    {
                        existingAppointmentIds = new List<int>();
                    }

                    List<AllAppointmentsList> objStockAppointments = new List<AllAppointmentsList>();
                    // Getting all Stock appointments against userName
                    objStockAppointments = this.getStockAppointments(varAppointmentParam.startdate, varAppointmentParam.enddate, arryUserInfo[0], varAppointmentParam.fetchall, existingAppointmentIds);
                    //Getting Overdue Appointments
                    if (varAppointmentParam.includeoverdue == 1)
                    {
                        //Get Stock Overdue Appointments, and merging the Lists of Stock appointments{new and overdue} appointments
                        objStockAppointments = objStockAppointments.Union(this.getOverdueStockAppointments(varAppointmentParam.startdate, varAppointmentParam.enddate, arryUserInfo[0], varAppointmentParam.fetchall, existingAppointmentIds)).ToList();
                    }

                    CommonData commonData = new CommonData();

                    AppointmentDal objAppointmentDal = new AppointmentDal();
                    commonData.powerTypes = objAppointmentDal.getPowerSourceTypes();
                    commonData.detectorTypes = objAppointmentDal.getDetectorTypes();

                    AppointmentResponse aptResponse = new AppointmentResponse();
                    aptResponse.appointmentList = objStockAppointments;
                    aptResponse.commonData = commonData;
                    objresponse.response = aptResponse;

                    //Setting status information
                    MessageData objMessageData = new MessageData();
                    objMessageData.code = MessageCodesConstants.successCode;
                    objMessageData.message = MessageConstants.success;
                    objresponse.status = objMessageData;
                }

                return objresponse;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(entityexception.Message, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(ex.InnerException.Message, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }
        #endregion

        #region add Customer Risk And Vul
        /// <summary>
        /// This function add customer risk & vulnerability & also property asbestos risk in appointment data
        /// </summary>
        /// <param name="apptData">appointment data inculding customer id & property id</param>
        public void addRiskVulAsbestos(ref List<AppointmentListFault> apptData)
        {
            CustomerDal custDal = new CustomerDal();
            List<PS_GetCustomerRisk_Result> custRiskList = new List<PS_GetCustomerRisk_Result>();
            List<CustomerVulnerabilityData> custVulData = new List<CustomerVulnerabilityData>();

            foreach (AppointmentListFault singleApptData in apptData)
            {
                if (singleApptData.customerList != null)
                {
                    foreach (CustomerData custData in singleApptData.customerList)
                    {
                        int customerId = (int)custData.customerId; // Line changed populateAppointmentToBeArrangedList - 19/06/2013

                        //add customer vulnerability data
                        custVulData = custDal.getCustomerVulnerabilities(customerId);

                        //for testing
                        //custVulData.vulCatDesc = "Vulnerability Category Description";
                        //custVulData.vulSubCatDesc = "Vulnerability Sub Category Description";

                        custData.customerVulnerabilityData = custVulData;

                        //get custoemr risk list
                        custRiskList = custDal.getCustomerRisk(customerId);

                        for (int i = 0; i < custRiskList.Count; i++)
                        {
                            CustomerRiskData custRiskData = new CustomerRiskData();
                            custRiskData.riskCatDesc = custRiskList[i].CATDESC;
                            custRiskData.riskSubCatDesc = custRiskList[i].SUBCATDESC;

                            //add object of risk to  appointment data 
                            custData.customerRiskData.Add(custRiskData);
                        }

                        custData.Address = null;
                        custData.property = null;
                    }
                }

                //add property asbestos risk
                PropertyDal propDal = new PropertyDal();
                List<PropertyAsbestosData> propAsbListData = new List<PropertyAsbestosData>();
                string propertyId = singleApptData.property.propertyId;

                //get property asbestos risk
                propAsbListData = propDal.getPropertyAsbestosRisk(propertyId);

                for (int j = 0; j < propAsbListData.Count; j++)
                {
                    PropertyAsbestosData propAsbData = new PropertyAsbestosData();
                    propAsbData.asbRiskLevelDesc = propAsbListData[j].asbRiskLevelDesc;
                    propAsbData.riskDesc = propAsbListData[j].riskDesc;

                    //add object of property risk to  appointment data 
                    singleApptData.property.propertyAsbestosData.Add(propAsbData);
                }

                singleApptData.property.propertyPicture = null;
            }
        }
        #endregion

        #region get And Set Appointment Progress Status

        /// <summary>
        /// This function get fault appointment progress status
        /// </summary>
        /// <param name="appointmentId">appointment id</param>
        /// <param name="userName">user name </param>
        /// <returns>return the fault appointment progress status</returns>
        public string getFaultJobStatus(int appointmentId, string userName)
        {
            try
            {
                AppointmentDal apptDal = new AppointmentDal();
                AuthenticationDal authDal = new AuthenticationDal();

                // if (authDal.isUserExist(userName) > 0)
                //{
                return apptDal.getFaultJobStatus(appointmentId);
                //}
                //else
                //{
                //    ErrorFaultSetGet.setErrorFault(String.Format(MessageConstants.SurveyourUsernameInvalidUserMsg, userName), true, MessageCodesConstants.SurveyourUserNameInvalidCode);
                //    throw new ArgumentException(String.Format(MessageConstants.SurveyourUsernameInvalidUserMsg, userName), "surveyorUserName");
                //}
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == false)
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
                else
                {
                    throw ex;
                }
            }
        }


        #endregion

        #region Check Bank Holiday
        /// <summary>
        /// This function check whether the selected appointment date exists in bank holiday.
        /// </summary>
        /// <param name="varAppointmentDataStock">The object of appointment</param>
        public bool checkBankHoliday(AppointmentDataStock varAppointmentDataStock)
        {
            AppointmentDal apptDal = new AppointmentDal();
            return apptDal.checkBankHoliday(varAppointmentDataStock);
        }
        #endregion


        #region save Stock Appointment

        /// <summary>
        /// This function saves the stock appointment in the database
        /// </summary>
        /// <param name="apptData">The object of appointment</param>
        /// <returns>the appointment id if successfully save otherwise return 0 </returns>

        public ResponseTemplate<List<AllAppointmentsList>> saveStockAppointment(AppointmentDataStock varAppointmentDataStock)
        {
            try
            {
                AppointmentDal apptDal = new AppointmentDal();
                int appointmentId = 0;
                ResponseTemplate<List<AllAppointmentsList>> objResponseTemplate = new ResponseTemplate<List<AllAppointmentsList>>();
                MessageData objMessageData = new MessageData();
                string propertyId = varAppointmentDataStock.customer.property.propertyId;

                if (varAppointmentDataStock.customer.customerId != null)
                {
                    int customerId = (int)varAppointmentDataStock.customer.customerId;

                    if (apptDal.isPropertyNCustomerExists(propertyId, customerId) == false)
                    {
                        ErrorFaultSetGet.setErrorFault(MessageConstants.PropertyIdOrCustomerIdDoesNotExistMsg, true, MessageCodesConstants.PropertyOrCustomerDoesNotExistCode);
                        throw new ArgumentException(String.Format(MessageConstants.PropertyIdOrCustomerIdDoesNotExistMsg, propertyId, customerId.ToString()), "propertyId or customer Id");
                    }
                    else if (apptDal.isAppointmentExistsAgainstProperty(propertyId, varAppointmentDataStock.appointmentType, AppointmentProgressStatus.InProgress.ToString()))
                    {
                        appointmentId = -1;

                    }
                }
                else
                {
                    if (apptDal.isPropertyExists(propertyId) == false)
                    {
                        ErrorFaultSetGet.setErrorFault(MessageConstants.PropertyDoesNotExistMsg, true, MessageCodesConstants.PropertyOrCustomerDoesNotExistCode);
                        throw new ArgumentException(String.Format(MessageConstants.PropertyDoesNotExistMsg, propertyId), "propertyId");
                    }
                    else if (apptDal.isAppointmentExistsAgainstProperty(propertyId, varAppointmentDataStock.appointmentType, AppointmentProgressStatus.InProgress.ToString()))
                    {
                        appointmentId = -1;

                    }
                }

                AuthenticationDal authDal = new AuthenticationDal();

                int loginId = authDal.isUserExist(varAppointmentDataStock.surveyorUserName);

                if (loginId > 0)
                {
                    //varAppointmentDataStock.createdBy = loginId;
                    int surveyId;
                    appointmentId = apptDal.saveStockAppointment(varAppointmentDataStock, out surveyId);

                    SendMessage.sendPushMessage(authDal.getDeviceToken(loginId), "An appointment has been added against your account in property survey application.");
                    //<Preparing the response>
                    //listSaveAppointmentData.Add(objSaveAppointmentData);
                    objResponseTemplate.response = apptDal.getStockAppointmentById(appointmentId, surveyId);

                    //<Status Information Start Here>
                    objMessageData.code = MessageCodesConstants.successCode;
                    objMessageData.message = MessageConstants.success;
                    objResponseTemplate.status = objMessageData;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(String.Format(MessageConstants.SurveyourUsernameInvalidUserMsg, varAppointmentDataStock.surveyorUserName), true, MessageCodesConstants.SurveyourUserNameInvalidCode);
                    throw new ArgumentException(String.Format(MessageConstants.SurveyourUsernameInvalidUserMsg, varAppointmentDataStock.surveyorUserName), "surveyorUserName");
                }

                return objResponseTemplate;

            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == true)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                    throw ex;
                }

            }

        }

        #endregion

        #region  get Stock Appointments
        /// <summary>
        /// This function Stock appointments information.
        /// Coded at <01-Aug-2013>
        /// </summary>
        /// <returns>
        /// List of appointments
        /// </returns>

        public List<AllAppointmentsList> getStockAppointments(string startDate, string endDate, string userName, byte fetchAll, List<int> existingAppointmentIds)
        {
            try
            {
                AppointmentDal objAppointmentDal = new AppointmentDal();

                if (fetchAll == 0) // Get Stock Appointments between particular dates
                {
                    return objAppointmentDal.getStockAppointmentsByDates(startDate, endDate, userName, existingAppointmentIds);
                }
                else // Get all Stock Appointments from particular start date
                {
                    return objAppointmentDal.getAllStockAppointments(userName, startDate, endDate, existingAppointmentIds);
                }
                //return appointmentData;
            }
            catch (EntityException entityException)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityException;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }

        #endregion

        #region  get Overdue Stock Appointments between particular dates
        /// <summary>
        /// This function returns 4 Over Due  and Next 5 days appointments.
        /// Coded at <01-Aug-2013>
        /// </summary>
        /// <returns>
        /// List of appointments
        /// </returns>

        public List<AllAppointmentsList> getOverdueStockAppointments(string startDate, string endDate, string userName, byte fetchAll, List<int> existingAppointmentIds)
        {
            try
            {
                AppointmentDal objAppointmentDal = new AppointmentDal();
                //List<AppointmentListStock> overdueAppointmentData = new List<AppointmentListStock>();
                if (fetchAll == 0)
                {
                    return objAppointmentDal.getOverdueStockAppointmentsByDates(startDate, userName, existingAppointmentIds);
                }
                else
                {
                    return objAppointmentDal.getAllOverdueStockAppointments(startDate, userName, existingAppointmentIds);
                }
                //return overdueAppointmentData;
            }
            catch (EntityException entityException)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityException;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }

        #endregion

        #region get All Users
        /// <summary>
        /// This function returns all the users of rsl manager database
        /// </summary>        
        /// <returns>list of all or Gas users</returns>

        public ResponseTemplate<List<SurveyorUserData>> getAllUsers(SurveyorParam varSurveyorParam)
        {
            try
            {
                AppointmentDal apptDal = new AppointmentDal();
                MessageData objMessageData = new MessageData();
                ResponseTemplate<List<SurveyorUserData>> objResponseTemplate = new ResponseTemplate<List<SurveyorUserData>>();
                //Adding Response Json
                if (varSurveyorParam.applicationtype == 1)
                {
                    objResponseTemplate.response = apptDal.getAllUsersGas();
                }
                else
                {
                    objResponseTemplate.response = apptDal.getAllUsers();
                }
                //<Status Information Start Here>
                objMessageData.code = MessageCodesConstants.successCode;
                objMessageData.message = MessageConstants.success;
                objResponseTemplate.status = objMessageData;
                return objResponseTemplate;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                throw ex;
            }
        }

        #endregion

        #region delete Stock Appointment
        /// <summary>
        /// This function is used to delete the appointment from the server
        /// </summary>
        /// <param name="appointmentId">appointment id</param>
        /// <returns>returns true or false based on delete action</returns>

        public ResponseTemplate<List<DelAppointmentData>> deleteStockAppointment(int appointmentId)
        {
            try
            {
                //<Declaring objects>
                AppointmentDal appointmentDal = new AppointmentDal();
                ResponseTemplate<List<DelAppointmentData>> objResponseTemplate = new ResponseTemplate<List<DelAppointmentData>>();
                DelAppointmentData objDelAppointmentData = new DelAppointmentData();
                List<DelAppointmentData> listDelAppointmentData = new List<DelAppointmentData>();
                MessageData objMessageData = new MessageData();
                //<Adding custom values in List>
                objDelAppointmentData.appointmentId = appointmentId;
                objDelAppointmentData.isDeleted = appointmentDal.deleteAppointment(appointmentId);
                listDelAppointmentData.Add(objDelAppointmentData);
                //<Assigning List to template>
                objResponseTemplate.response = listDelAppointmentData;
                //<Status Information Start Here>
                objMessageData.code = MessageCodesConstants.successCode;
                objMessageData.message = MessageConstants.success;
                objResponseTemplate.status = objMessageData;
                return objResponseTemplate;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == false)
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
                else
                {
                    throw ex;
                }

            }

        }
        #endregion

        #region save Complete Appointment for gas
        /// <summary>
        /// this function calls the method for Gas. and update (appointment Data,SurveyData,Customer data,No entry data in case of "NoEntry" Status,
        /// Journal data,Lgsr data and populate CP12Document 
        /// </summary>
        /// <param name="appointment">The object of appointment</param>
        /// <returns>It returns true or false in update is successful</returns>
        private bool completeAppointmentForGas(UpdateAppointmentData appointment)
        {
            try
            {
                AppointmentDal apptDal = new AppointmentDal();
                SurveyDal surveyDal = new SurveyDal();
                FaultsDal faultDal = new FaultsDal();
                //Update Appointment Data
                apptDal.completeAppointmentForGas(appointment);

                if (appointment.property != null)
                {
                    AppliancesDal appliancesDal = new AppliancesDal();
                    InstallationPipeworkDal insWorkDal = new InstallationPipeworkDal();

                    foreach (ApplianceData appData in appointment.property.appliances)
                    {
                        AppliancesBl appliancesBl = new Appliances.AppliancesBl();
                        appData.ApplianceID = appliancesBl.saveUpdateOfflineAppliance(appData);
                        //appliancesDal.saveAppliance(appData);
                        if (appData.ApplianceInspection != null && appData.ApplianceInspection.inspectionDate != null)
                        {
                            appData.ApplianceInspection.APPLIANCEID = appData.ApplianceID;
                            appData.ApplianceInspection.JOURNALID = appointment.journalId;
                            appliancesDal.saveApplianceInspectionGas(appData.ApplianceInspection);
                        }
                        if (appData.ApplianceDefects != null && appData.ApplianceDefects.Count > 0)
                        {
                            foreach (FaultsDataGas appDefectData in appData.ApplianceDefects)
                            {
                                appDefectData.ApplianceID = appData.ApplianceID;
                                faultDal.updateFaultDataGas(appDefectData);
                            }
                        }
                    }
                    foreach (DetectorCountData detector in appointment.property.detectors)
                    {

                        appliancesDal.updateDetectorCount(appointment.property.propertyId, detector, appointment.updatedBy);
                        if (detector.detectorDefects != null && detector.detectorDefects.Count > 0)
                        {
                            foreach (FaultsDataGas appDefectData in detector.detectorDefects)
                            {
                                faultDal.updateFaultDataGas(appDefectData);
                            }
                        }
                        if (detector.detectorInspection != null && detector.detectorTypeId > 0)
                        {
                            detector.detectorInspection.inspectionID = appliancesDal.addUpdateDetectorInfo(detector.detectorInspection, detector.detectorTypeId, detector.isInspected, appointment.journalId, appointment.property.propertyId).result;
                        }
                    }
                    if (appointment.property.installationpipework != null && appointment.property.installationpipework.inspectionDate != null)
                        insWorkDal.saveInstallationPipeworkGas(appointment);

                }
                if (appointment.appointmentStatus == "No Entry")
                {
                    apptDal.saveNoEntryForGas(appointment);
                }
                else
                {
                    if (appointment.CP12Info != null)
                    {
                        apptDal.updateLgsrData(appointment);
                    }

                    if (appointment.appointmentStatus == AppointmentCompleteStatus.Finished.ToString())
                    {
                        //  apptDal.updateJournalData(appointment, Utilities.Constants.MessageConstants.CP12IssuedNameInAS_Status);
                        bool response = apptDal.UpdateAndInsertJournal(appointment);
                        if (response)
                            DocumentHelper.populateCp12Document(appointment.journal.propertyId, appointment.journal.journalId.ToString());
                    }
                }
                return true;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == true)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                    throw ex;
                }

            }
        }
        #endregion

        #region save Complete appointment for stock
        /// <summary>
        /// this function calls the method for Stock. and update (appointment Data,SurveyData and Customer data
        /// </summary>
        /// <param name="appointment">The object of appointment</param>
        /// <returns>It returns true or false in update is successful</returns>
        private bool completeAppointmentForStock(UpdateAppointmentData appointment)
        {
            try
            {
                AppointmentDal apptDal = new AppointmentDal();
                SurveyDal surveyDal = new SurveyDal();
                SurveyBl survBl = new SurveyBl();
                //Update Appointment Data
                apptDal.completeAppointmentForStock(appointment);

                SurveyData appSurveyData = new SurveyData();
                if (appointment.Survey != null && appointment.Survey.surveyData != null)
                {
                    foreach (SurveyData surveylist in appointment.Survey.surveyData)
                    {
                        surveylist.completedBy = appointment.updatedBy ?? 0;
                        //surveyDal.saveSurveyForm(surveylist);
                        survBl.saveSurveyForm(surveylist);
                    }
                }
                if (appointment.appointmentStatus == AppointmentCompleteStatus.Complete.ToString())
                {
                    int surveyId = survBl.getSurveyId(appointment.appointmentId);
                    if (surveyId > 0)
                    {
                        try
                        {
                            string result = DocumentHelper.populatePropertyRecordInspection(appointment.property.propertyId, surveyId.ToString(), appointment.createdBy.ToString());
                            ErrorFaultSetGet.setErrorFault("+++Result++++++++++" + result + "++++++++++++++", true, MessageCodesConstants.GeneralExceptionCode);
                        }
                        catch (Exception ex)
                        {
                            ErrorFaultSetGet.setErrorFault("+++++Resultt++++++++" + ex.ToString() + "++++++++++++++", true, MessageCodesConstants.GeneralExceptionCode);
                        }
                    }
                }

                return true;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(entityexception.Message, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == true)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                    throw ex;
                }

            }
        }
        #endregion

        #region save Complete Appointment
        /// <summary>
        /// This function saves the stock appointment in the database
        /// </summary>
        /// <param name="apptData">The object of appointment</param>
        /// <returns>the appointment id if successfully save otherwise return 0 </returns>
        public ResponseTemplate<ReturnAppointmentStatus> completeAppointment(AppointmentContractData varUpdateAppointmentData)
        {
            try
            {
                //using (TransactionScope trans = new TransactionScope())
                //{
                ResponseTemplate<ReturnAppointmentStatus> objResponseTemplate = new ResponseTemplate<ReturnAppointmentStatus>();
                ReturnAppointmentStatus objUpdateCompleteAppointmentData = new ReturnAppointmentStatus();
                //ReturnAppointmentStatus listCompleteAppointmentData = new ReturnAppointmentStatus();
                MessageData objMessageData = new MessageData();
                objUpdateCompleteAppointmentData.isUpdated = false;
                foreach (UpdateAppointmentData appointment in varUpdateAppointmentData.appointments)
                {
                    AppointmentDal apptDal = new AppointmentDal();
                    SurveyDal surveyDal = new SurveyDal();
                    if (appointment.appointmentType == PropSurvey.Contracts.Data.Appointment.AppointmentTypes.Gas.ToString())
                    {
                        completeAppointmentForGas(appointment);
                    }
                    else if (appointment.appointmentType == PropSurvey.Contracts.Data.Appointment.AppointmentTypes.Stock.ToString())
                    {
                        completeAppointmentForStock(appointment);
                    }
                    else if (appointment.appointmentType == PropSurvey.Contracts.Data.Appointment.AppointmentTypes.Fault.ToString())
                    {
                        completeAppointmentForFault(appointment);
                    }
                    else if (appointment.appointmentType == PropSurvey.Contracts.Data.Appointment.AppointmentTypes.Planned.ToString()
                            || appointment.appointmentType == PropSurvey.Contracts.Data.Appointment.AppointmentTypes.Miscellaneous.ToString()
                            || appointment.appointmentType == PropSurvey.Contracts.Data.Appointment.AppointmentTypes.Adaptation.ToString()
                            || appointment.appointmentType == PropSurvey.Contracts.Data.Appointment.AppointmentTypes.Condition.ToString())
                    {
                        completeAppointmentForPlanned(appointment);
                    }
                    if (appointment.customerList != null)
                    {
                        foreach (CustomerData customer in appointment.customerList)
                        {
                            CustomerDal custDal = new CustomerDal();
                            custDal.UpdateCustomerContactFromAppointment(customer);
                        }
                    }

                    if (appointment.property != null)
                    {
                        PropertyDal propDal = new PropertyDal();
                        //propDal.updateProperty(appointment.property);
                        if (appointment.property.Accommodations != null)
                        {
                            propDal.updatePropertyDimensions(appointment.property.Accommodations, appointment.property.propertyId);
                        }
                    }
                    objUpdateCompleteAppointmentData.isUpdated = true;
                    if (varUpdateAppointmentData.respondWithData == true)
                    {
                        objUpdateCompleteAppointmentData.Appointment = appointment;
                    }
                    else
                    {
                        objUpdateCompleteAppointmentData.Appointment = null;
                    }
                }
                // listCompleteAppointmentData.Add(objUpdateCompleteAppointmentData);
                //<Assigning List to template>
                objResponseTemplate.response = objUpdateCompleteAppointmentData;
                //<Status Information Start Here>
                objMessageData.code = MessageCodesConstants.successCode;
                objMessageData.message = MessageConstants.success;
                objResponseTemplate.status = objMessageData;

                //trans.Complete(); 
                return objResponseTemplate;

                //}
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == true)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                    throw ex;
                }

            }

        }
        #endregion

        #region save complete Fault appointment
        private bool completeAppointmentForFault(UpdateAppointmentData appointment)
        {

            try
            {
                AppointmentDal apptDal = new AppointmentDal();
                if (appointment.jobDataList != null)
                {
                    foreach (JobData jobData in appointment.jobDataList)
                    {
                        //Check job pause data exist
                        if (jobData.jobPauseHistory.Count > 0)
                        {
                            foreach (JobPauseHistoryData jobPauseData in jobData.jobPauseHistory)
                            {
                                // save fault paused history
                                apptDal.saveFaultPaused(jobPauseData, jobData.faultLogID);

                            }
                        }
                        //check repair list exist
                        if (jobData.faultRepairList.Count > 0)
                        {
                            apptDal.saveFaultRepairList(jobData, appointment.createdBy);
                        }

                        if (appointment.appointmentStatus == AppointmentCompleteStatus.Complete.ToString())
                        {
                            apptDal.completeAppointmentForFault(jobData, appointment);

                        }
                        else if (appointment.appointmentStatus != "No Entry"
                                 || appointment.appointmentStatus.Replace(" ", "") != AppointmentCompleteStatus.NoEntry.ToString()
                                )
                        {
                            apptDal.PausedAppointmentForFault(jobData, appointment);
                        }
                    }
                }
                if (appointment.appointmentStatus == "No Entry")
                {
                    apptDal.noEntryFaultAppointment(appointment);
                }
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == true)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
            }
            return true;
        }
        #endregion

        #region save complete Planned appointment
        private bool completeAppointmentForPlanned(UpdateAppointmentData appointment)
        {

            try
            {
                AppointmentDal apptDal = new AppointmentDal();
                if (appointment.componentTrade != null)
                {
                    //Check job pause data exist
                    if (appointment.componentTrade.jobPauseHistory != null && appointment.componentTrade.jobPauseHistory.Count > 0)
                    {
                        foreach (JobPauseHistoryData jobPauseData in appointment.componentTrade.jobPauseHistory)
                        {
                            // save fault paused history
                            apptDal.savePlannedPaused(jobPauseData, appointment.appointmentId);
                        }

                    }

                }
                if (appointment.appointmentStatus == AppointmentCompleteStatus.Complete.ToString() || appointment.appointmentStatus == AppointmentCompleteStatus.InProgress.ToString())
                {
                    apptDal.completeAppointmentForPlanned(appointment);
                }
                if (appointment.appointmentStatus.Replace(" ", "") == AppointmentCompleteStatus.NoEntry.ToString())
                {
                    apptDal.noEntryPlannedAppointment(appointment);
                }
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == true)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
            }
            return true;
        }
        #endregion

        #region save Complete Appointment V2
        /// <summary>
        /// This function saves the stock appointment in the database
        /// </summary>
        /// <param name="apptData">The object of appointment</param>
        /// <returns>the appointment id if successfully save otherwise return 0 </returns>
        public ResponseTemplate<CompleteAppointmentsStatus> completeAppointmentV2(AppointmentContractData varUpdateAppointmentData)
        {
            ResponseTemplate<CompleteAppointmentsStatus> objResponseTemplate = new ResponseTemplate<CompleteAppointmentsStatus>();
            CompleteAppointmentsStatus objCompleteAppointmentsStatus = new CompleteAppointmentsStatus();
            objResponseTemplate.response = objCompleteAppointmentsStatus;
            try
            {
                AppointmentDal apptDal = new AppointmentDal();
                foreach (UpdateAppointmentData appointment in varUpdateAppointmentData.appointments)
                {
                    string address = string.Empty;


                    bool isSuccessFull = false;

                    if (appointment.property != null)
                    {
                        address = String.Format("{0}{1}{2}{3}", appointment.property.houseNumber ?? "", (" " + appointment.property.address1) ?? "", (" " + appointment.property.address2) ?? "", (" " + appointment.property.address3) ?? "").Trim();
                    }
                    else
                    {
                        address = String.Format("{0}{1}{2}", appointment.scheme.address1 ?? "", (" " + appointment.scheme.address2) ?? "", (" " + appointment.scheme.address3) ?? "").Trim();

                    }
                    SyncAppointmentDetail currentSyncappointment = new SyncAppointmentDetail(appointment.appointmentId, appointment.appointmentType, address);

                    try
                    {
                        if (appointment.appointmentType == PropSurvey.Contracts.Data.Appointment.AppointmentTypes.Gas.ToString())
                        {
                            isSuccessFull = apptDal.completeAppointmentForGasV2(appointment);
                        }
                        else if (appointment.appointmentType == PropSurvey.Contracts.Data.Appointment.AppointmentTypes.Stock.ToString())
                        {
                            isSuccessFull = apptDal.completeAppointmentForStockV2(appointment);
                        }
                        else if (appointment.appointmentType == PropSurvey.Contracts.Data.Appointment.AppointmentTypes.Fault.ToString())
                        {
                            isSuccessFull = apptDal.completeAppointmentForFaultV2(appointment);
                        }
                        else if (appointment.appointmentType == PropSurvey.Contracts.Data.Appointment.AppointmentTypes.Planned.ToString()
                                || appointment.appointmentType == PropSurvey.Contracts.Data.Appointment.AppointmentTypes.Miscellaneous.ToString()
                                || appointment.appointmentType == PropSurvey.Contracts.Data.Appointment.AppointmentTypes.Adaptation.ToString()
                                || appointment.appointmentType == PropSurvey.Contracts.Data.Appointment.AppointmentTypes.Condition.ToString())
                        {
                            isSuccessFull = apptDal.completeAppointmentForPlannedV2(appointment);
                        }
                    }
                    catch (TransactionAbortedException ex)
                    {
                        ExceptionPolicy.HandleException(ex, "Exception Policy");

                        isSuccessFull = false;
                        currentSyncappointment.failureReason = MessageConstants.DbConEntityErrorMsg;
                    }
                    catch (FormatException ex)
                    {
                        ExceptionPolicy.HandleException(ex, "Exception Policy");

                        isSuccessFull = false;
                        currentSyncappointment.failureReason = MessageConstants.InvalidData;
                    }
                    catch (EntityException ex)
                    {
                        ExceptionPolicy.HandleException(ex, "Exception Policy");

                        isSuccessFull = false;
                        currentSyncappointment.failureReason = MessageConstants.DbConEntityErrorMsg;
                    }
                    catch (Exception ex)
                    {
                        ExceptionPolicy.HandleException(ex, "Exception Policy");

                        isSuccessFull = false;
                        currentSyncappointment.failureReason = MessageConstants.DbConErrorMsg;
                        //Suggestion: If need more catch blocks add a bool type variable with name isError = false 
                        //and set isError = true in all catch block.
                        // and take following line in finally block and call with conditon isError = true.                        
                    }
                    finally
                    {
                        if (isSuccessFull)
                        {
                            currentSyncappointment.failureReason = null;
                            objCompleteAppointmentsStatus.savedAppointments.Add(currentSyncappointment);
                        }
                        else
                            objCompleteAppointmentsStatus.failedAppointments.Add(currentSyncappointment);
                    }
                }

                //<Assigning List to template>
                objResponseTemplate.response = objCompleteAppointmentsStatus;
                //<Status Information Start Here>
                MessageData objMessageData = new MessageData();
                objMessageData.code = MessageCodesConstants.successCode;
                objMessageData.message = MessageConstants.success;
                objResponseTemplate.status = objMessageData;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == true)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                    throw ex;
                }
            }
            return objResponseTemplate;
        }
        #endregion

        #region Populate Stock Inspection Documents.
        /// <summary>
        /// This function used to Populate Stock Inspection Documents.
        /// </summary>        
        /// <returns>string Message</returns>

        public string populateStockInspection(StockInspectionDocument stockInspectionDocList)
        {
            string message = string.Empty;
            int userId = stockInspectionDocList.userId;
            foreach (StockAppointmentDetail appointment in stockInspectionDocList.appointmentList)
            {
                SurveyBl survBl = new SurveyBl();
                int surveyId = survBl.getSurveyId(appointment.appointmentId);
                if (surveyId > 0)
                {
                    message = DocumentHelper.populatePropertyRecordInspection(appointment.propertyId, surveyId.ToString(), userId.ToString());
                }
            }

            return message;
        }
        #endregion
    }
}
