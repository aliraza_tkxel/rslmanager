﻿using System;
using System.Data;
using PropSurvey.Contracts.Fault;
using PropSurvey.Dal.AppScreen;
using PropSurvey.Utilities.Constants;
using PropSurvey.Utilities.ErrorSetter;

namespace PropSurvey.BuisnessLayer.AppScreen
{
    public class AppScreenBl
    {
        #region save Survey Form
        public string getApplicationScreens(string propertyId, int appointmentId)
        {
            try
            {
                AppScreenDal appScreenDal = new AppScreenDal();
                string appScreenJson = string.Empty;
                appScreenJson = appScreenDal.getApplicationScreens(propertyId, appointmentId);


                return appScreenJson;
            }
            catch (EntityException entityexception)
            {
                ErrorFaultSetGet.setErrorFault(MessageConstants.DbConEntityErrorMsg, true, MessageCodesConstants.EntitiyExceptionCode);
                throw entityexception;
            }
            catch (Exception ex)
            {
                if (ErrorFault.isErrorOccured == true)
                {
                    throw ex;
                }
                else
                {
                    ErrorFaultSetGet.setErrorFault(MessageConstants.DbConErrorMsg, true, MessageCodesConstants.GeneralExceptionCode);
                    throw ex;
                }

            }
        }
        #endregion
    }
}
