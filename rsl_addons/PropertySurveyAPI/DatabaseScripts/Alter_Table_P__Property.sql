
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[P__PROPERTY]') AND name = 'PropertyPicId' )
ALTER TABLE [dbo].[P__PROPERTY] DROP COLUMN [PropertyPicId]
GO

ALTER TABLE [dbo].[P__PROPERTY] ADD [PropertyPicId] Int Null
GO