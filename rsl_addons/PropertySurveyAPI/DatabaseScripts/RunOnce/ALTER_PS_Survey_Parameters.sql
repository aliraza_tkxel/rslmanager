/*
Author: Aamir Waheed
Date: 17 April 2015
*/

--=========================================================================
--====== ALTER PARAMETERVALUE Column in PS_Survey_Parameters
--=========================================================================
IF EXISTS
(
	SELECT
		*
	FROM
		INFORMATION_SCHEMA.COLUMNS
	WHERE
		TABLE_NAME = 'PS_Survey_Parameters'
		AND COLUMN_NAME = 'PARAMETERVALUE'
) BEGIN
PRINT 'Exists'

--==============================================================
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION

ALTER TABLE dbo.PS_Survey_Parameters
ALTER COLUMN PARAMETERVALUE NVARCHAR(1000)

ALTER TABLE dbo.PS_Survey_Parameters SET (LOCK_ESCALATION = TABLE)
COMMIT

SELECT
	HAS_PERMS_BY_NAME(N'dbo.PS_Survey_Parameters', 'Object', 'ALTER')			AS ALT_Per
	,HAS_PERMS_BY_NAME(N'dbo.PS_Survey_Parameters', 'Object', 'VIEW DEFINITION')	AS View_def_Per
	,HAS_PERMS_BY_NAME(N'dbo.PS_Survey_Parameters', 'Object', 'CONTROL')			AS Contr_Per

--==============================================================

	PRINT 'Column Altered successfully.'
END
ELSE
	PRINT 'Not Exists'
GO