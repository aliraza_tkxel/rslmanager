/*
Author: Aamir Waheed
Date: 25 Feb 2014
*/

--=========================================================================
--====== Add ImageIdentifier Column in FL_FAULT_REPAIR_IMAGES
--=========================================================================
IF NOT EXISTS
(
	SELECT
		*
	FROM
		INFORMATION_SCHEMA.COLUMNS
	WHERE
		TABLE_NAME = 'FL_FAULT_REPAIR_IMAGES'
		AND COLUMN_NAME = 'ImageIdentifier'
) BEGIN
PRINT 'Not Exists'

--==============================================================
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
ALTER TABLE dbo.FL_FAULT_REPAIR_IMAGES ADD
ImageIdentifier NVARCHAR(64) NULL
ALTER TABLE dbo.FL_FAULT_REPAIR_IMAGES SET (LOCK_ESCALATION = TABLE)
COMMIT
SELECT
	HAS_PERMS_BY_NAME(N'dbo.FL_FAULT_REPAIR_IMAGES', 'Object', 'ALTER')				AS ALT_Per
	,HAS_PERMS_BY_NAME(N'dbo.FL_FAULT_REPAIR_IMAGES', 'Object', 'VIEW DEFINITION')	AS View_def_Per
	,HAS_PERMS_BY_NAME(N'dbo.FL_FAULT_REPAIR_IMAGES', 'Object', 'CONTROL')			AS Contr_Per

--==============================================================

PRINT 'Column inserted successfully.'
END
ELSE
	PRINT 'Column already exists.'
GO