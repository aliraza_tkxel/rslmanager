/* ========================================================================================
 * A Special Variable to enable/disable Recharge process
 * This is a critical process, it creates sales invoice
 * and also adds an entry in event Rent Journal.
 * ======================================================================================== */
DECLARE @ExecuteRechargeProcess BIT = 1

SET NOCOUNT ON;

DECLARE @AppointmentId INT,
		@FaultLogID INT,
		@FollowOnNotes VARCHAR(500) = 'N/A',
		@RepairNotes VARCHAR(500),
		@RepairCompletionDateTime SmallDateTime,
		@IsAppointmentCompleted bit = 1,
		@FaultLogCompletedDate DATETIME = NULL	

DECLARE	@UserId INT,        
		@CustomerId INT,      
		@PropertyId nvarchar(20),      
		@TenancyId int,      
		@ItemnatureId INT,      
		@ItemId INT,        
		@JournalId INT,       
		@CJournalId int,        
		@repairDetail nvarchar(1000),        
		@CompleteStatusId INT,        
		@OrgID INT,         
		@QTY INT,      
		@RECHARGE INT,      
		@NetCost FLOAT,      
		@VatType INT,      
		@rVAT FLOAT,      
		@GrossCost FLOAT,      
		@FaultDescription NVARCHAR(100),      
		@SaleID INT,      
		@SALEITEMID INT,      
		@Success int,      
		@FaultRepairListId int,      
		@RentJournal_id BIGINT,  
		--For Scheme Block  
		@schemeId int,  
		@blockId int

SELECT @CompleteStatusId = FaultStatusID
FROM FL_FAULT_STATUS FS
WHERE FS.Description = 'Complete'

/* ========================================================================================
 * Get In Progress appointment, that have all jobs sheets completed.
 * ======================================================================================== */

SELECT DISTINCT A.AppointmentID
INTO #Appointments
FROM FL_CO_APPOINTMENT A
LEFT JOIN FL_FAULT_APPOINTMENT FA ON A.AppointmentID = FA.AppointmentId
LEFT JOIN FL_FAULT_LOG FL ON FA.FaultLogId = FL.FaultLogID AND FL.StatusID <> @CompleteStatusId
WHERE A.AppointmentStatus = 'InProgress' AND FL.FaultLogId IS NULL

/* ========================================================================================
* Create a cursor on Jobs Data to loop through each Job Data
* and fill (update/insert) data in relevant tables.
* ======================================================================================== */

DECLARE JobDataCursor CURSOR READ_ONLY FOR
SELECT FA.AppointmentId AS AppointmentId , FA.FaultLogID AS FaultLogID
, FL.CompletedDate AS RepairCompletionDateTime, FL.CompletedDate AS FaultLogCompletedDate
FROM FL_FAULT_APPOINTMENT FA
INNER JOIN #Appointments A ON FA.AppointmentId = A.AppointmentId
INNER JOIN FL_FAULT_LOG FL ON FA.FaultLogId = FL.FaultLogID

ORDER BY FL.CompletedDate ASC

OPEN JobDataCursor

/*
DECLARE @AppointmentId INT,
	@FaultLogID INT,
	@FollowOnNotes VARCHAR(500) = 'N/A', --Skipped and Always 'N/A'	
	@RepairCompletionDateTime SmallDateTime,
	@IsAppointmentCompleted bit = 1, --Skipped and Always 1 (true)
	@FaultLogCompletedDate DATETIME = NULL	
*/

FETCH NEXT FROM JobDataCursor INTO @AppointmentId, @FaultLogID, @RepairCompletionDateTime, @FaultLogCompletedDate

WHILE @@Fetch_Status = 0
BEGIN	
	BEGIN TRY    
		BEGIN TRANSACTION
				
		PRINT 'Now Updating => AppointmentId: ' + CONVERT(VARCHAR, @AppointmentId) + 
				', FaultLogID: ' + CONVERT(VARCHAR, @FaultLogID)
			
	    
	    /* ========================================================================================
		 * Get appointment and Job data from relevant tables		 
		 * ======================================================================================== */
	    
	    SELECT  
		 @UserId = FL_CO_APPOINTMENT.OperativeID,
		 @CustomerId = FL_FAULT_LOG.CustomerId,
		 @PropertyId = FL_FAULT_LOG.PROPERTYID,
		 @ItemnatureId = C_NATURE.ITEMNATUREID,  
		 @ItemId = C_NATURE.ITEMID,  
		 @orgId = FL_FAULT_LOG.ORGID,  
		 @schemeId = FL_FAULT_LOG.SchemeId,  
		 @blockId = FL_FAULT_LOG.BlockId  
		FROM FL_CO_APPOINTMENT
		INNER JOIN FL_FAULT_APPOINTMENT  
		 ON FL_FAULT_APPOINTMENT.AppointmentId = FL_CO_APPOINTMENT.AppointmentID  
		INNER JOIN FL_FAULT_LOG  
		 ON FL_FAULT_APPOINTMENT.FaultLogId = FL_FAULT_LOG.FaultLogID
		INNER JOIN FL_FAULT_JOURNAL  
		 ON FL_FAULT_LOG.FaultLogID = FL_FAULT_JOURNAL.FaultLogID  
		INNER JOIN C_NATURE  
		 ON FL_FAULT_JOURNAL.ITEMID = C_NATURE.ITEMID  
		WHERE FL_CO_APPOINTMENT.AppointmentID = @AppointmentId  
		AND C_NATURE.ITEMNATUREID = (SELECT  
		 ItemNatureID  
		FROM C_NATURE  
		WHERE DESCRIPTION = 'Reactive Repair')  
		AND FL_FAULT_APPOINTMENT.FaultAppointmentId = (SELECT  
		 MIN(FaultAppointmentId)  
		FROM FL_FAULT_APPOINTMENT  
		WHERE AppointmentID = @AppointmentId)
		
		SELECT @TenancyId = T.TENANCYID
		FROM FL_FAULT_LOG FL
		INNER JOIN C_CUSTOMERTENANCY CT ON FL.CustomerId = CT.CUSTOMERID
		INNER JOIN C_TENANCY T ON CT.TENANCYID = T.TENANCYID AND FL.PROPERTYID = T.PROPERTYID
		
		-- Get Fault Details
		SELECT  
		 @RECHARGE = FL.RECHARGE,  
		 @NetCost = NETCOST,  
		 @VatType = VATRATEID,  
		 @rVAT = VAT,  
		 @GrossCost = GROSS,  
		 @FaultDescription = F.DESCRIPTION  
		FROM dbo.FL_FAULT_LOG FL  
		INNER JOIN dbo.FL_FAULT F  
		 ON F.FaultID = FL.FaultID  
		WHERE FL.FaultLogID = @FaultLogId  
		
		-- Get Fault Log Journal Id		
		SELECT  
		 @JournalId = JOURNALID  
		FROM FL_FAULT_JOURNAL  
		WHERE FaultLogID = @FaultLogId
		
		-- Get Job Repair Notes
		SELECT @RepairNotes = FLTR.Notes
		FROM FL_CO_FAULTLOG_TO_REPAIR AS FLTR
		
		/* ========================================================================================
		 * Insert Record for each repair to customer tables
		 * ======================================================================================== */
		IF @CustomerId IS NOT NULL AND @PropertyId IS NOT NULL
		BEGIN 
			DECLARE c1 CURSOR READ_ONLY FOR SELECT  
			 FaultRepairListID  
			FROM FL_CO_FAULTLOG_TO_REPAIR  
			WHERE FaultLogID = @FaultLogId  
			  
			OPEN c1
			/*      
			Start Looping      
			*/		  
			FETCH NEXT FROM c1  
			INTO @FaultRepairListId  
			  
			WHILE @@FETCH_STATUS = 0 BEGIN
			  
			--Get repair detail      
				SELECT  
					@repairDetail = Description  
				FROM FL_FAULT_REPAIR_LIST  
				WHERE FaultRepairListID = @FaultRepairListId  
				
				--Insertion into C_Journal
				IF NOT EXISTS (SELECT 1 FROM C_JOURNAL CJ
					INNER JOIN C_REPAIR CR ON CR.JOURNALID = CJ.JOURNALID
				 WHERE CJ.CUSTOMERID = @CustomerId AND CJ.PROPERTYID = @PropertyId /* AND CJ.TENANCYID = @TenancyId */
					AND CJ.ITEMID = @ItemId AND CJ.ITEMNATUREID = @ItemnatureId
					AND CR.ITEMSTATUSID = 11 AND CR.ITEMACTIONID = 6 /* AND CR.LASTACTIONDATE = @RepairCompletionDateTime */ AND LASTACTIONUSER = @UserId 
					AND CR.ITEMDETAILID = @FaultLogID)
				BEGIN			
					INSERT INTO C_JOURNAL (CUSTOMERID, TENANCYID, PROPERTYID, ITEMID, ITEMNATUREID, CURRENTITEMSTATUSID, TITLE)
					 VALUES (@CustomerId, @TenancyId, @PropertyId, @ItemId, @ItemnatureId, 11, @repairDetail)  
				  
					--Get the latest CJournal ID  
					SELECT @CJournalId = SCOPE_IDENTITY()
					
					PRINT 'C_JOURNAL.JournalId: ' + CONVERT(VARCHAR, @CJournalId)
				  
					--Insertion into C_Repair      
					INSERT INTO C_REPAIR (JOURNALID, ITEMSTATUSID, ITEMACTIONID, LASTACTIONDATE, LASTACTIONUSER, ITEMDETAILID)					
					 VALUES (@CJournalId, 11, 6, @RepairCompletionDateTime, @UserId, @FaultLogId)
					 
					PRINT 'C_REPAIR.REPAIRHISTORYID: ' + CONVERT(VARCHAR, SCOPE_IDENTITY())
				END
				ELSE
				BEGIN
					SELECT @CJournalId = CJ.JOURNALID FROM C_JOURNAL CJ
					INNER JOIN C_REPAIR CR ON CR.JOURNALID = CJ.JOURNALID
					WHERE CJ.CUSTOMERID = @CustomerId /* AND CJ.TENANCYID = @TenancyId */ AND CJ.PROPERTYID = @PropertyId
					AND CJ.ITEMID = @ItemId AND CJ.ITEMNATUREID = @ItemnatureId
					AND CR.ITEMSTATUSID = 11 AND CR.ITEMACTIONID = 6  /* AND CR.LASTACTIONDATE = @RepairCompletionDateTime */ AND LASTACTIONUSER = @UserId
					AND CR.ITEMDETAILID = @FaultLogID
				END
				
				IF NOT EXISTS (SELECT 1 FROM FL_FAULTJOURNAL_TO_CJOURNAL WHERE FJOURNALID = @JournalId AND CJOURNALID = @CJournalId)
				BEGIN
					-- Insertion into FL_FAULTJOURNAL_TO_CJOURNAL      
					INSERT INTO FL_FAULTJOURNAL_TO_CJOURNAL (FJOURNALID, CJOURNALID)  
					 VALUES (@JournalId, @CJournalId)
					 
					PRINT 'FL_FAULTJOURNAL_TO_CJOURNAL.SID: ' + CONVERT(VARCHAR, SCOPE_IDENTITY())
				END
			
			FETCH NEXT FROM c1 INTO @FaultRepairListId
			END
			  
			CLOSE C1  
			DEALLOCATE C1
		END
		
		/* ========================================================================================
		 * Recharge Process	and create sales invoice
		 * ======================================================================================== */
  
		IF @RECHARGE = 1 AND @ExecuteRechargeProcess = 1
		BEGIN
		-- Insertion into C_RECHARGEABLE
			IF NOT EXISTS (SELECT 1 FROM C_RECHARGEABLE WHERE JOURNALID = @CJournalId)
			BEGIN 
				INSERT INTO C_RECHARGEABLE (JOURNALID)  
					VALUES (@CJournalId)
					
				PRINT 'C_RECHARGEABLE.JOURNALID: ' + CONVERT(VARCHAR, SCOPE_IDENTITY())
			END
			-- Insertion into F_SALESINVOICE
			
			IF NOT EXISTS (SELECT 1 FROM F_SALESINVOICE WHERE SONAME = 'TENANT RECHARGE' AND SODATE = @RepairCompletionDateTime
				AND SONOTES = 'Automated Tenant Recharge for: ' + @FaultDescription AND USERID = @UserId
				AND TENANCYID = @TenancyId AND ACTIVE = 1 AND SOTYPE = 1)
			BEGIN			
				INSERT INTO F_SALESINVOICE (SONAME, SODATE, SONOTES, USERID, TENANCYID, ACTIVE, SOTYPE, SOSTATUS)  
				 VALUES ('TENANT RECHARGE', @RepairCompletionDateTime, 'Automated Tenant Recharge for: ' + @FaultDescription, @UserID, @TenancyId, 1, 1, 1)  
				-- Get the latest Sales Invoice Id  
				SELECT @SaleID = SCOPE_IDENTITY()
				
				PRINT 'F_SALESINVOICE.SALEID: ' + CONVERT(VARCHAR, @SaleID)
				
				-- Insertion into F_RENTJOURNAL      
				INSERT INTO F_RENTJOURNAL (TENANCYID, TRANSACTIONDATE, ITEMTYPE, PAYMENTTYPE, AMOUNT, STATUSID)  
				 VALUES (@TenancyId, @RepairCompletionDateTime, 5, NULL, @GrossCost, 5)  
				
				SELECT @RentJournal_id = SCOPE_IDENTITY()
				
				PRINT 'F_RENTJOURNAL.JOURNALID: ' + CONVERT(VARCHAR, @RentJournal_id)
				 
				-- Insertion into F_SALESINVOICEITEM      
				INSERT INTO F_SALESINVOICEITEM (SALEID, SALESCATID, ITEMNAME, ITEMDESC, SIDATE, NETCOST, VATTYPE, VAT, GROSSCOST, USERID, ACTIVE, SITYPE, SISTATUS, RENTJOURNALID)  
				 VALUES (@SaleID, 1, @FaultDescription, @FaultDescription, GETDATE(), @NetCost, @VatType, @rVAT, @GrossCost, @UserID, 1, 1, 1, @RentJournal_id)  
				
				SELECT @SALEITEMID = SCOPE_IDENTITY()
				
				PRINT 'F_SALESINVOICEITEM.SALEITEMID: ' + CONVERT(VARCHAR, @SALEITEMID)
				
				-- Insertion into F_SALEITEM_TO_PURCHASEITEM      
				INSERT INTO F_SALEITEM_TO_PURCHASEITEM (SALEITEMID)  
				 VALUES (@SALEITEMID)
				 
				PRINT 'F_SALEITEM_TO_PURCHASEITEM.STPID: ' + CONVERT(VARCHAR, SCOPE_IDENTITY())
				
				-- Execute procedure for sales order      
				EXEC NL_SALESORDER @SaleID  
			END  
		END -- END RECHARGE PROCESS
		
		/* ========================================================================================
		 * Add follow on work
		 * ======================================================================================== */
		  
		IF ((NOT (@FollowOnNotes = 'N/A')) AND (LEN(@FollowOnNotes) > 0)) BEGIN  
			-- Insertion into FL_FAULT_FOLLOWON  
			INSERT INTO FL_FAULT_FOLLOWON (FaultLogId, RecordedOn, FollowOnNotes)  
				VALUES (@FaultLogID, ISNULL(@FaultLogCompletedDate,GETDATE()), @FollowOnNotes)
			Print 'FaultLogHistoryId: ' + CONVERT(NVARCHAR,SCOPE_IDENTITY())
		END
		
		/* ========================================================================================
		 * Update Fault Status in FL_FAULT_LOG  
		 * ======================================================================================== */
		
		UPDATE FL_FAULT_LOG
		SET StatusID = @CompleteStatusId
			,CompletedDate = ISNULL(@FaultLogCompletedDate,CompletedDate)
		WHERE FaultLogID = @FaultLogId
		
		/* ========================================================================================
		 * Update Fault Journal Status in FL_FAULT_JOURNAL
		 * ======================================================================================== */
		UPDATE FL_FAULT_JOURNAL  
			SET FaultStatusID = @CompleteStatusId,
				LastActionDate = ISNULL(@FaultLogCompletedDate,GETDATE())
		WHERE FaultLogID = @FaultLogId
		
		/* ========================================================================================
		 * Insertion into FL_FAULT_LOG_HISTORY for fault completion
		 * If an entry is not already inserted for fault completion
		 * ======================================================================================== */
		IF NOT EXISTS (SELECT FaultLogHistoryID FROM FL_FAULT_LOG_HISTORY WHERE FaultLogID = @FaultLogId AND FaultStatusID = @CompleteStatusId)
		BEGIN
		  INSERT INTO FL_FAULT_LOG_HISTORY (JournalID, FaultStatusID, LastActionDate, LastActionUserID, FaultLogID, PropertyID, SchemeId, BlockId)
			VALUES (@JOURNALID, @CompleteStatusId, ISNULL(@FaultLogCompletedDate,GETDATE()), @UserId, @FaultLogId, @PropertyId,@SchemeId,@BlockId)
			
		  Print 'FaultLogHistoryId: ' + CONVERT(NVARCHAR,SCOPE_IDENTITY())
		END
		
		/* ========================================================================================
		 * Update ENDTIME in FL_FAULT_JOBTIMESHEET
		 * Considering the case an end time is not already updated
		 * ======================================================================================== */
		UPDATE FL_FAULT_JOBTIMESHEET  
		SET EndTime = ISNULL(@FaultLogCompletedDate,GETDATE())  
		WHERE FaultLogId = @FaultLogId
		AND EndTime IS NULL
		AND TimeSheetID = (SELECT  
		 MAX(TimeSheetID)  
		FROM FL_FAULT_JOBTIMESHEET  
		WHERE FaultLogId = @FaultLogId)
		
		/* ========================================================================================
		 * Update AppointmentStatus,RepairNotes and LastActionDate in FL_CO_APPOINTMENT		 
		 * ======================================================================================== */		
		  
		IF @IsAppointmentCompleted = 1 BEGIN  
		UPDATE FL_CO_APPOINTMENT  
		SET AppointmentStatus = 'Complete',  
		 RepairNotes = @RepairNotes,  
		 LastActionDate = ISNULL(@FaultLogCompletedDate,GETDATE()),  
		 RepairCompletionDateTime = @RepairCompletionDateTime  
		WHERE AppointmentID = @AppointmentId  
		END ELSE BEGIN  
		UPDATE FL_CO_APPOINTMENT  
		SET RepairNotes = @RepairNotes,  
		 LastActionDate = ISNULL(@FaultLogCompletedDate,GETDATE()),  
		 RepairCompletionDateTime = @RepairCompletionDateTime  
		WHERE AppointmentID = @AppointmentId  
		END
		
		COMMIT
		
		PRINT 'Updated Successfully => AppointmentId: ' + CONVERT(VARCHAR, @AppointmentId) + 
			', FaultLogID: ' + CONVERT(VARCHAR, @FaultLogID)			
		PRINT '-----------------------------------------------------------------------------------'
	END TRY
	BEGIN CATCH
		ROLLBACK
		PRINT 'AppointmentId: ' + CONVERT(VARCHAR, @AppointmentId) + 
				', FaultLogID: ' + CONVERT(VARCHAR, @FaultLogID) + 
				', Error ' + CONVERT(VARCHAR, ERROR_NUMBER()) +
				', Severity ' + CONVERT(VARCHAR, ERROR_SEVERITY()) +
				', State ' + CONVERT(VARCHAR, ERROR_STATE()) + 
				', Line ' + CONVERT(VARCHAR, ERROR_LINE()) + 
				', Error Message ' + ERROR_MESSAGE()
		PRINT '-----------------------------------------------------------------------------------'
	 
	END CATCH

	
	/*
		DECLARE @AppointmentId INT,
			@FaultLogID INT,
			@FollowOnNotes VARCHAR(500) = 'N/A', --Skipped and Always 'N/A'			
			@RepairCompletionDateTime SmallDateTime,
			@IsAppointmentCompleted bit = 1, --Skipped and Always 1 (true)
			@FaultLogCompletedDate DATETIME = NULL	
	*/	
	FETCH NEXT FROM JobDataCursor INTO @AppointmentId, @FaultLogID, @RepairCompletionDateTime, @FaultLogCompletedDate
END
	
CLOSE JobDataCursor
DEALLOCATE JobDataCursor
	
DROP TABLE #Appointments