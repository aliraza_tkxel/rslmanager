﻿Imports System.Net.Mail

Namespace FLS_Utilities

    Public Class EmailHelper
        ''' <summary>
        ''' This function is to send html formatted email, in it caller send contents as string.
        ''' In this function no complex logic is needed in caller function.
        ''' </summary>
        ''' <param name="recepientName"></param>
        ''' <param name="recepientEmail"></param>
        ''' <param name="subject"></param>
        ''' <param name="body"></param>
        ''' <remarks></remarks>
        Public Shared Sub sendHtmlFormattedEmail(ByVal recepientName As String, ByVal recepientEmail As String, ByVal subject As String, ByVal body As String)

            Dim mailMessage As MailMessage = New MailMessage
            mailMessage.Subject = subject
            mailMessage.Body = body
            mailMessage.IsBodyHtml = True
            mailMessage.To.Add(New MailAddress(recepientEmail, recepientName))

            'The SmtpClient gets configuration from Web.Config
            Dim smtp As SmtpClient = New SmtpClient

            smtp.Send(mailMessage)

        End Sub
        ''' <summary>
        ''' This is simple function to send an email where the logic to create the mail message logic is at caller side.
        ''' </summary>
        ''' <param name="mailMessage"></param>
        ''' <remarks></remarks>
        Public Shared Sub sendEmail(ByRef mailMessage As MailMessage)

            'The SmtpClient gets configuration from Web.Config
            Dim smtp As SmtpClient = New SmtpClient

            smtp.Send(mailMessage)
        End Sub

        Public Shared Sub sendHtmlFormattedEmailWithAttachment(ByVal recepientName As String, ByVal recepientEmail As String, ByVal subject As String, ByVal body As String, ByVal filePath As String)

            Dim mailMessage As MailMessage = New MailMessage
            mailMessage.Subject = subject
            mailMessage.Body = body
            mailMessage.IsBodyHtml = True
            mailMessage.To.Add(New MailAddress(recepientEmail, recepientName))


            Dim iCalAttachement As Attachment = New Attachment(filePath)
            mailMessage.Attachments.Add(iCalAttachement)
            'The SmtpClient gets configuration from Web.Config
            Dim smtp As SmtpClient = New SmtpClient

            smtp.Send(mailMessage)
            iCalAttachement.Dispose()
        End Sub

    End Class

End Namespace


