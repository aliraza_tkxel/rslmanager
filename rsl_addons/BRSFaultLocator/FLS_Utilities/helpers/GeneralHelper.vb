﻿Imports System.Globalization
Imports System.Web.HttpContext
Imports System.Web
Imports System.Web.UI.Page
Imports System.IO
Imports System.Net
Imports System.Net.Mail
Imports System.Text
Imports FLS_BusinessObject
Imports System.Web.UI.WebControls
Imports System.Configuration

Namespace FLS_Utilities

    Public Class GeneralHelper

#Region "get Document Upload Path"
        Public Shared Function getDocumentUploadPath() As String
            Return ConfigurationManager.AppSettings("DocumentsUploadPath")
        End Function
#End Region

#Region "get Us Cultured Date Time"

        ''' <summary>
        ''' Returns the US cultured date time.
        ''' </summary>
        ''' <param name="date">date to be converted.</param>
        ''' <returns>US Cultured DateTime</returns>

        Public Shared Function getUsCulturedDateTime(ByVal [date] As String) As DateTime
            Dim infoUS As New CultureInfo("en-US")
            Return Convert.ToDateTime([date].ToString(), infoUS)
        End Function
#End Region

#Region "get Uk Cultured Date Time"

        ''' <summary>
        ''' Returns the UK cultured date time.
        ''' </summary>
        ''' <param name="date">date to be converted.</param>
        ''' <returns>UK Cultured DateTime</returns>

        Public Shared Function getUKCulturedDateTime(ByVal [date] As String) As DateTime
            Dim infoUK As New CultureInfo("en-GB")
            Return Convert.ToDateTime([date].ToString(), infoUK)
        End Function
#End Region

#Region "Populate Path Data"
        Public Shared Function populatePathsData() As Dictionary(Of String, String)
            Dim pathsData As Dictionary(Of String, String) = New Dictionary(Of String, String)
            pathsData.Add(PathConstants.SearchFaultSrcVal, PathConstants.SearchFault)
            pathsData.Add(PathConstants.SearchSbFaultSrcVal, PathConstants.SearchSbFault)
            pathsData.Add(PathConstants.OtherAppointmentSrcVal, PathConstants.OtherAppointmentPath)
            pathsData.Add(PathConstants.OtherSbAppointmentSrcVal, PathConstants.OtherSbAppointmentPath)
            pathsData.Add(PathConstants.MoreDetailsSrcVal, PathConstants.MoreDetails)
            pathsData.Add(PathConstants.MoreSbDetailsSrcVal, PathConstants.MoreSbDetails)
            pathsData.Add(PathConstants.RecallDetailsSrcVal, PathConstants.RecallDetails)
            pathsData.Add(PathConstants.RecallSbDetailsSrcVal, PathConstants.RecallSbDetails)
            pathsData.Add(PathConstants.FaultBasketSrcVal, PathConstants.FaultBasket)
            pathsData.Add(PathConstants.SbFaultBasketSrcVal, PathConstants.SbFaultBasket)
            pathsData.Add(PathConstants.AvailableAppointmentsSrcVal, PathConstants.AvailableAppointments)
            pathsData.Add(PathConstants.AvailableSbAppointmentsSrcVal, PathConstants.SbAvailableAppointments)
            pathsData.Add(PathConstants.AppointmentSummarySrcVal, PathConstants.AppointmentSummary)
            pathsData.Add(PathConstants.SbAppointmentSummarySrcVal, PathConstants.SbAppointmentSummary)
            pathsData.Add(PathConstants.ScheduleCalendarSrcVal, PathConstants.ScheduleCalender)
            pathsData.Add(PathConstants.SbScheduleCalendarSrcVal, PathConstants.SbScheduleCalender)
            pathsData.Add(PathConstants.RearrangeAppointmentSrcVal, PathConstants.RearrangCurrentFault)
            pathsData.Add(PathConstants.SbRearrangeAppointmentSrcVal, PathConstants.ReArrangingSbFault)
            pathsData.Add(PathConstants.CancelAppointmentSummarySrcVal, PathConstants.CancellationAppointmentSummary)
            pathsData.Add(PathConstants.CancelSbAppointmentSummarySrcVal, PathConstants.CancellationSbAppointmentSummary)
            pathsData.Add(PathConstants.CancelAppointmentSummaryForGasSrcVal, PathConstants.CancellationAppointmentSummaryForGas)
            pathsData.Add(PathConstants.ReportAreaSrcVal, PathConstants.ReportAreaPath)
            pathsData.Add(PathConstants.JobSheetSummarySubcontractorSrcVal, PathConstants.JobSheetSummarySubcontractor)
            Return pathsData
        End Function
#End Region

#Region "get Path Data Src"
        Public Shared Function getSrc(ByVal pathSrcVal As String)
            Dim pathsData As New Dictionary(Of String, String)
            pathsData = SessionManager.getPathsData()
            Dim srcPath As String = "?" + PathConstants.Src + "=" + ""
            If Not IsNothing(pathsData) AndAlso pathsData.ContainsKey(pathSrcVal) Then
                srcPath = "?" + PathConstants.Src + "=" + pathSrcVal
            End If
            Return srcPath
        End Function
#End Region

#Region "get Day Start Hour"
        Public Shared Function getDayStartHour() As String
            Return CType(ConfigurationManager.AppSettings("DayStartHour"), Double)
        End Function
#End Region

#Region "get Day End Hour"
        Public Shared Function getDayEndHour() As String
            Return CType(ConfigurationManager.AppSettings("DayEndHour"), Double)
        End Function
#End Region

#Region "get Unique Keys"
        Public Shared Function getUniqueKey(ByVal uniqueCounter As Integer) As String
            'this 'll be used to create the unique index of dictionary
            Dim uniqueKey As String = uniqueCounter.ToString() + SessionManager.getFaultSchedulingUserId().ToString() + DateTime.Now.ToString("ddMMyyyyhhmmssffff")
            Return uniqueKey
        End Function
#End Region

#Region "Convert Date In Seconds"
        Public Shared Function convertDateInSec(ByVal timeToConvert As Date) As Integer
            Dim calendarStartDate As New DateTime(1970, 1, 1)
            Dim timeInSec As Integer = (timeToConvert - calendarStartDate).TotalSeconds
            Return timeInSec
        End Function
#End Region

#Region " Unix time stamp is seconds past epoch"
        Public Shared Function unixTimeStampToDateTime(unixTimeStamp As Double) As DateTime
            ' Unix time stamp is seconds past epoch
            Dim dtDateTime As System.DateTime = New DateTime(1970, 1, 1, 0, 0, 0, _
             0, System.DateTimeKind.Utc)
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp)
            Return dtDateTime
        End Function
#End Region

#Region "Convert Date In Minute"
        Public Shared Function convertDateInMin(ByVal timeToConvert As Date) As Integer
            Dim calendarStartDate As New DateTime(1970, 1, 1)
            Dim timeInSec As Integer = (timeToConvert - calendarStartDate).TotalMinutes
            Return timeInSec
        End Function
#End Region

#Region "Get Push Notification Application Address"
        Public Shared Function getPushNotificationAddress(ByVal absoluteUri As String) As String
            Dim servername As String = absoluteUri.Substring(absoluteUri.IndexOf("//") + 2, Len(absoluteUri.Substring(absoluteUri.IndexOf("//") + 2, absoluteUri.Substring(absoluteUri.IndexOf("//") + 2).IndexOf("/"))))
            Return "https://" + servername + "/StockConditionOfflineAPI/push/notificationFault"
        End Function
#End Region

#Region "Get Today Start Hour"

        Public Shared Function getTodayStartHour(ByVal startDate As Date) As Double
            Return Math.Floor(CType(startDate.ToString("HH"), Double) + (CType(startDate.ToString("mm"), Double) / 60))
        End Function

#End Region

#Region "Populate Confirmation Email body"
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <param name="jobSheetNumberList"></param>
        ''' <param name="appointmentTime"></param>
        ''' <param name="appointmentDate"></param>
        ''' <param name="customerName"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function populateBodyAppointmentConfirmationEmail(ByVal jobSheetNumberList As String, ByVal appointmentTime As String, ByVal appointmentDate As Date, ByVal customerName As String) As AlternateView
            Dim body As String = String.Empty
            Dim server = HttpContext.Current.Server
            Dim emailtemplatepath = server.MapPath("~/Email/AppointmentConfirmation.htm")

            Dim mailMessage As New MailMessage()

            Dim reader As StreamReader = New StreamReader(emailtemplatepath)
            body = reader.ReadToEnd.ToString
            body = body.Replace("{CustomerName}", customerName)
            body = body.Replace("{JSN}", jobSheetNumberList)
            If CType(Left(appointmentTime, 2), Integer) < 12 Then
                body = body.Replace("{AppointmentTime}", "morning")
            Else
                body = body.Replace("{AppointmentTime}", "afternoon")
            End If

            body = body.Replace("{AppointmentDate}", appointmentDate.ToString("dddd, d") + GeneralHelper.getDaySuffix(appointmentDate) + appointmentDate.ToString(" MMMM yyyy"))

            Dim logo50Years As LinkedResource = New LinkedResource(server.MapPath("~/Images/50_Years.gif"))
            logo50Years.ContentId = "logo50Years_Id"

            body = body.Replace("{Logo_50_years}", String.Format("cid:{0}", logo50Years.ContentId))

            Dim logoBroadLandRepairs As LinkedResource = New LinkedResource(server.MapPath("~/Images/Braodland_Repairs.gif"))
            logoBroadLandRepairs.ContentId = "logoBroadLandRepairs_Id"

            body = body.Replace("{Logo_Broadland_Repairs}", String.Format("cid:{0}", logoBroadLandRepairs.ContentId))

            Dim mimeType As Mime.ContentType = New Mime.ContentType("text/html")

            Dim alternatevw As AlternateView = AlternateView.CreateAlternateViewFromString(body, mimeType)
            alternatevw.LinkedResources.Add(logo50Years)
            alternatevw.LinkedResources.Add(logoBroadLandRepairs)


            Return alternatevw
        End Function
#End Region

#Region "Populate SMS body"

        Public Shared Function populateBodyAppointmentConfirmationSMS(ByVal jobSheetNumberList As String, ByVal appointmentTime As String, ByVal appointmentDate As Date) As String
            Dim body As String = String.Empty
            body = ApplicationConstants.SMSmessage
            body = body.Replace("{JSN}", jobSheetNumberList)
            If CType(Left(appointmentTime, 2), Integer) < 12 Then
                body = body.Replace("{AppointmentTime}", "morning")
            Else
                body = body.Replace("{AppointmentTime}", "afternoon")
            End If

            body = body.Replace("{AppointmentDate}", appointmentDate.ToString("dddd, d") + GeneralHelper.getDaySuffix(appointmentDate) + appointmentDate.ToString(" MMMM yyyy"))

            Return body
        End Function
#End Region

#Region "Send SMS"
        Public Shared Sub sendSMS(ByVal body As String, ByVal recepientMobile As String)
            Dim request As WebRequest = WebRequest.Create(ApplicationConstants.SMSUrl)
            ' Set the Method property of the request to POST.
            request.Method = "POST"
            ' Create POST data and convert it to a byte array.

            Dim strdata As String

            strdata = "un=" & HttpUtility.UrlEncode(ApplicationConstants.SMSUserName) & "&pw=" & HttpUtility.UrlEncode(ApplicationConstants.SMSPassword) & "&call=" & HttpUtility.UrlEncode(ApplicationConstants.strSMS) & "&msisdn=" & HttpUtility.UrlEncode(recepientMobile) & "&message=" & HttpUtility.UrlEncode(body) & "&mo=" & HttpUtility.UrlEncode(ApplicationConstants.SMSCompany)

            Dim byteArray As Byte() = Encoding.UTF8.GetBytes(strdata)
            ' Set the ContentType property of the WebRequest.
            request.ContentType = "application/x-www-form-urlencoded"
            ' Set the ContentLength property of the WebRequest.
            request.ContentLength = byteArray.Length
            ' Get the request stream.
            Dim dataStream As Stream = request.GetRequestStream()
            ' Write the data to the request stream.
            dataStream.Write(byteArray, 0, byteArray.Length)
            ' Close the Stream object.
            dataStream.Close()
            ' Get the response.
            Dim response As WebResponse = request.GetResponse()
            ' Display the status.
            'Console.WriteLine(CType(response, HttpWebResponse).StatusDescription)
            ' Get the stream containing content returned by the server.f
            dataStream = response.GetResponseStream() ' Open the stream using a StreamReader for easy access.
            Dim reader As New StreamReader(dataStream) ' Read the content.
            Dim responseFromServer As String = reader.ReadToEnd()
            ' Display the content.
            'Console.WriteLine(responseFromServer)
            ' Clean up the streams.
            reader.Close()
            dataStream.Close()
            response.Close()
        End Sub
#End Region
#Region "Send SMS UPDATED API"
        Public Shared Sub sendSmsUpdatedAPI(ByVal body As String, ByVal recepientMobile As String)

            Dim result As String
            Dim apiKey As String = "resw5bAZdIs-fnUwznzIfzJM66WfRdWSQRXlFtR7V2"
            'string apiKey = "nVv8yYd3mYo-1UXMeTpnYJ4pXqspvYPrgC7HTsYH3s"; //rehan
            Dim username As String = "digitalengagementteam@broadlandgroup.org"
            Dim password As String = "Broadland@1"
            Dim numbers As String = recepientMobile ' in a comma seperated list
            Dim message As String = body
            Dim sender As String = " "

            Dim url As String = "https://api.txtlocal.com/send/?apiKey=" & apiKey & "&numbers=" & numbers & "&message=" & message & "&sender=" & sender
            'refer to parameters to complete correct url string

            Dim myWriter As StreamWriter = Nothing
            Dim objRequest As HttpWebRequest = CType(WebRequest.Create(url), HttpWebRequest)

            objRequest.Method = "POST"
            objRequest.ContentLength = Encoding.UTF8.GetByteCount(url)
            objRequest.ContentType = "application/x-www-form-urlencoded"
            Try
                myWriter = New StreamWriter(objRequest.GetRequestStream())
                myWriter.Write(url)
            Catch e As Exception
                'return e.Message;
            Finally
                myWriter.Close()
            End Try

            Dim objResponse As HttpWebResponse = CType(objRequest.GetResponse(), HttpWebResponse)
            Using sr As New StreamReader(objResponse.GetResponseStream())
                result = sr.ReadToEnd()
                ' Close and clean up the StreamReader
                sr.Close()
            End Using


        End Sub
#End Region
#Region "Get daysuffix Like th,st,nd,rd to show (1st, 2nd, 3rd etc.)"
        ''' <summary>
        ''' This function is to get the day suffix like st, nd, rd, th to show it with date like 1st, 2nd, 3rd, 4th etc.        ''' 
        ''' </summary>
        ''' <param name="dateparam">date to get suffix.</param>
        ''' <returns>It return the suffix one of these four, st, nd, rd, th</returns>
        ''' <remarks></remarks>
        Public Shared Function getDaySuffix(ByVal dateparam As Date) As String
            Dim daySuffix = "th"
            If (dateparam.Day = 1 OrElse dateparam.Day = 21 OrElse dateparam.Day = 31) Then
                daySuffix = "st"
            ElseIf (dateparam.Day = 2 OrElse dateparam.Day = 22) Then
                daySuffix = "nd"
            ElseIf (dateparam.Day = 3 OrElse dateparam.Day = 23) Then
                daySuffix = "rd"
            End If

            Return daySuffix
        End Function



#End Region

#Region "Set Grid View Pager"

        Public Shared Sub setGridViewPager(ByRef pnlPagination As Panel, ByVal objPageSortBO As PageSortBO)
            pnlPagination.Visible = False

            If objPageSortBO.TotalRecords >= 1 Then
                pnlPagination.Visible = True

                Dim pageNumber As Integer = objPageSortBO.PageNumber
                Dim pageSize As Integer = objPageSortBO.PageSize
                Dim TotalRecords As Integer = objPageSortBO.TotalRecords
                Dim totalPages As Integer = objPageSortBO.TotalPages

                Dim lnkbtnPagerFirst As LinkButton = DirectCast(pnlPagination.FindControl("lnkbtnPagerFirst"), LinkButton)
                Dim lnkbtnPagerPrev As LinkButton = DirectCast(pnlPagination.FindControl("lnkbtnPagerPrev"), LinkButton)
                Dim lnkbtnPagerNext As LinkButton = DirectCast(pnlPagination.FindControl("lnkbtnPagerNext"), LinkButton)
                Dim lnkbtnPagerLast As LinkButton = DirectCast(pnlPagination.FindControl("lnkbtnPagerLast"), LinkButton)

                lnkbtnPagerFirst.Enabled = (pageNumber <> 1)
                lnkbtnPagerFirst.Font.Underline = lnkbtnPagerFirst.Enabled

                lnkbtnPagerPrev.Enabled = lnkbtnPagerFirst.Enabled
                lnkbtnPagerPrev.Font.Underline = lnkbtnPagerPrev.Enabled

                lnkbtnPagerNext.Enabled = (pageNumber <> totalPages)
                lnkbtnPagerNext.Font.Underline = lnkbtnPagerNext.Enabled

                lnkbtnPagerLast.Enabled = lnkbtnPagerNext.Enabled
                lnkbtnPagerLast.Font.Underline = lnkbtnPagerLast.Enabled

                Dim lblPagerCurrentPage As Label = DirectCast(pnlPagination.FindControl("lblPagerCurrentPage"), Label)
                lblPagerCurrentPage.Text = pageNumber

                Dim lblPagerTotalPages As Label = DirectCast(pnlPagination.FindControl("lblPagerTotalPages"), Label)
                lblPagerTotalPages.Text = totalPages

                Dim lblPagerRecordStart As Label = DirectCast(pnlPagination.FindControl("lblPagerRecordStart"), Label)
                Dim pagerRecordStart As Integer = ((pageNumber - 1) * pageSize) + 1
                lblPagerRecordStart.Text = pagerRecordStart

                Dim lblPagerRecordEnd As Label = DirectCast(pnlPagination.FindControl("lblPagerRecordEnd"), Label)
                Dim pagerRecordEnd As Integer = pagerRecordStart + pageSize - 1
                lblPagerRecordEnd.Text = IIf(pagerRecordEnd < TotalRecords, pagerRecordEnd, TotalRecords)

                Dim lblPagerRecordTotal As Label = DirectCast(pnlPagination.FindControl("lblPagerRecordTotal"), Label)
                lblPagerRecordTotal.Text = TotalRecords

                'Set Range validator error Message and Maximum Value (Minimun value is 1).
                Dim rangevalidatorPageNumber As RangeValidator = pnlPagination.FindControl("rangevalidatorPageNumber")
                rangevalidatorPageNumber.ErrorMessage = "Enter a page between 1 and " + totalPages.ToString()
                rangevalidatorPageNumber.MaximumValue = totalPages
            End If
        End Sub

#End Region


#Region "Set Grid View Pager for Associated faults"

        Public Shared Sub setGridViewPagerAssociatedFaults(ByRef pnlPagination As Panel, ByVal objPageSortBO As PageSortBO)
            pnlPagination.Visible = False

            If objPageSortBO.TotalRecords >= 1 Then
                pnlPagination.Visible = True

                Dim pageNumber As Integer = objPageSortBO.PageNumber
                Dim pageSize As Integer = objPageSortBO.PageSize
                Dim TotalRecords As Integer = objPageSortBO.TotalRecords
                Dim totalPages As Integer = objPageSortBO.TotalPages

                Dim lnkbtnPagerFirst As LinkButton = DirectCast(pnlPagination.FindControl("lnkbtnPagerAssociatedFaultsFirst"), LinkButton)
                Dim lnkbtnPagerPrev As LinkButton = DirectCast(pnlPagination.FindControl("lnkbtnPagerAssociatedFaultsPrev"), LinkButton)
                Dim lnkbtnPagerNext As LinkButton = DirectCast(pnlPagination.FindControl("lnkbtnPagerAssociatedFaultsNext"), LinkButton)
                Dim lnkbtnPagerLast As LinkButton = DirectCast(pnlPagination.FindControl("lnkbtnPagerAssociatedFaultsLast"), LinkButton)

                lnkbtnPagerFirst.Enabled = (pageNumber <> 1)
                lnkbtnPagerFirst.Font.Underline = lnkbtnPagerFirst.Enabled

                lnkbtnPagerPrev.Enabled = lnkbtnPagerFirst.Enabled
                lnkbtnPagerPrev.Font.Underline = lnkbtnPagerPrev.Enabled

                lnkbtnPagerNext.Enabled = (pageNumber <> totalPages)
                lnkbtnPagerNext.Font.Underline = lnkbtnPagerNext.Enabled

                lnkbtnPagerLast.Enabled = lnkbtnPagerNext.Enabled
                lnkbtnPagerLast.Font.Underline = lnkbtnPagerLast.Enabled

                Dim lblPagerCurrentPage As Label = DirectCast(pnlPagination.FindControl("lblPagerAssociatedFaultsCurrentPage"), Label)
                lblPagerCurrentPage.Text = pageNumber

                Dim lblPagerTotalPages As Label = DirectCast(pnlPagination.FindControl("lblPagerAssociatedFaultsTotalPages"), Label)
                lblPagerTotalPages.Text = totalPages

                Dim lblPagerRecordStart As Label = DirectCast(pnlPagination.FindControl("lblPagerAssociatedFaultsRecordStart"), Label)
                Dim pagerRecordStart As Integer = ((pageNumber - 1) * pageSize) + 1
                lblPagerRecordStart.Text = pagerRecordStart

                Dim lblPagerRecordEnd As Label = DirectCast(pnlPagination.FindControl("lblPagerAssociatedFaultsRecordEnd"), Label)
                Dim pagerRecordEnd As Integer = pagerRecordStart + pageSize - 1
                lblPagerRecordEnd.Text = IIf(pagerRecordEnd < TotalRecords, pagerRecordEnd, TotalRecords)

                Dim lblPagerRecordTotal As Label = DirectCast(pnlPagination.FindControl("lblPagerAssociatedFaultsRecordTotal"), Label)
                lblPagerRecordTotal.Text = TotalRecords

                'Set Range validator error Message and Maximum Value (Minimun value is 1).
                'Dim rangevalidatorPageNumber As RangeValidator = pnlPagination.FindControl("rangevalidatorPageNumber")
                'rangevalidatorPageNumber.ErrorMessage = "Enter a page between 1 and " + totalPages.ToString()
                'rangevalidatorPageNumber.MaximumValue = totalPages
            End If
        End Sub

#End Region



#Region "Get Today Start Hour"

        Public Shared Function getMaximunLookAhead() As Double
            Return CType(ConfigurationManager.AppSettings("MaximunLookAhead"), Double)
        End Function

#End Region

#Region "Get Job Sheet Summary Address"

        Public Shared Function getJobSheetSummaryAddress() As String
            Return "../../../RSLJobSheetSummary/Views/JobSheets/JobSheetSummary.aspx?jsn="
        End Function

#End Region

#Region "Get Job Sheet Summary Address"

        Public Shared Function getSbJobSheetSummaryAddress() As String
            Return "../../../RSLJobSheetSummary/Views/JobSheets/SbJobSheetSummary.aspx?jsn="
        End Function

#End Region

#Region "Get Job Sheet Summary  for Voids"

        Public Shared Function getVoidJobSheetSummaryAddress() As String
            Return "../../../RSLJobSheetSummary/Views/JobSheets/JSVJobSheetSummary.aspx?jsn="
        End Function

#End Region

#Region "Get Job Sheet Summary for Planned"

        Public Shared Function getPlannedJobSheetSummaryAddress() As String
            Return "../../../RSLJobSheetSummary/Views/JobSheets/JobSheetSummaryPlanned.aspx?jsn="
        End Function

#End Region

#Region "Get Job Sheet Summary for Gas"

        Public Shared Function getGasJobSheetSummaryAddress() As String
            Return "../../../RSLJobSheetSummary/Views/JobSheets/ServicingJobSheetSummary.aspx?jsn="
        End Function

#End Region

#Region "Get Job Sheet Summary for Scheme/Block Gas"

        Public Shared Function getSchemeBlockGasJobSheetSummaryAddress() As String
            Return "../../../RSLJobSheetSummary/Views/JobSheets/ServicingJobSheetSummaryForSchemeBlock.aspx?jsn="
        End Function

#End Region

#Region "Get Job Sheet Summary for Gas"

        Public Shared Function getDefectsJobSheetSummaryAddress() As String
            Return "../../../RSLJobSheetSummary/Views/JobSheets/DefectsJobSheetSummary.aspx?jsn="
        End Function

#End Region
#Region "Get Job Sheet Summary for Cyclic Maintenance and "

        Public Shared Function getMEJobSheetSummary() As String
            Return "../../../RSLJobSheetSummary/Views/JobSheets/MaintenanceJobSheetSummary.aspx?jsn="
        End Function

#End Region

#Region "Get Is Multiple Days Appointment Allowed From configuration File"

        ''' <summary>
        ''' Get boolen value of isMultipleDaysAppointmentAllowed from configuration file i.e web.config 
        ''' </summary>
        ''' <returns>Boolen value (True/False) if Multiple Days Appointment Allowed is added in configuration file, otherwise false which is default in this function </returns>
        ''' <remarks></remarks>
        Public Shared Function isMultipleDaysAppointmentAllowed() As Boolean
            isMultipleDaysAppointmentAllowed = getBoolValueFromConfigurationFile("isMultipleDaysAppointmentAllowed", False)
        End Function

#End Region

#Region "Get max fault duration from configuration file"

        ''' <summary>
        ''' Get max fault duration from configuration file i.e web.config
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks>Decimal: Max fault duration from web.config or 9.5 as default.</remarks>
        Public Shared Function getMaxFaultDuration() As Decimal
            getMaxFaultDuration = getDecimalValueFromConfigurationFile("maxFaultDurationHours", 9.5)
        End Function

#End Region

#Region "Get Time Slots to Display"

        ''' <summary>
        ''' Get Time Slots to Display from configuration file i.e web.config
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks>Decimal: Get Time Slots to Display from web.config or 15 as default.</remarks>
        Public Shared Function getTimeSlotToDisplay() As Decimal
            getTimeSlotToDisplay = getDecimalValueFromConfigurationFile("timeSlotToDisplayIntelligenceScheduling", 15)
        End Function

#End Region

#Region "Get Bool value from Configuration file"

        ''' <summary>
        ''' Get bool value from file i.e web.config
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks>Boolen value (True/False): ,in case key/value not found it will return false(default) or provided default value.</remarks>
        Public Shared Function getBoolValueFromConfigurationFile(ByVal key As String, Optional ByVal defaultValue As Boolean = False)
            getBoolValueFromConfigurationFile = defaultValue
            Dim value As String = ConfigurationManager.AppSettings(key).ToString
            Dim tempParsedValue As Boolean = False
            If Not String.IsNullOrEmpty(value) AndAlso Boolean.TryParse(value, tempParsedValue) Then
                getBoolValueFromConfigurationFile = tempParsedValue
            End If
        End Function

#End Region

#Region "Get Decimal value from Configuration file"

        ''' <summary>
        ''' Get Decimal value from file i.e web.config
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks>Decimal value: in case key/value not found it will return 0.0(default) or provided default value.</remarks>
        Public Shared Function getDecimalValueFromConfigurationFile(ByVal key As String, Optional ByVal defaultValue As Decimal = 0.0) As Decimal
            getDecimalValueFromConfigurationFile = defaultValue
            Dim value As String = ConfigurationManager.AppSettings(key).ToString
            Dim tempParsedValue As Decimal = 0.0
            If Not String.IsNullOrEmpty(value) AndAlso Decimal.TryParse(value, tempParsedValue) Then
                getDecimalValueFromConfigurationFile = tempParsedValue
            End If
        End Function

#End Region

#Region "Get Integer value from Configuration file"

        ''' <summary>
        ''' Get Integer value from file i.e web.config
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks>Integer value: Value save in confituration file, Or  in case key/not found it will return 0(default) or provided default value.</remarks>
        Public Shared Function getIntegarlValueFromConfigurationFile(ByVal key As String, Optional ByVal defaultValue As Integer = 0) As Integer
            getIntegarlValueFromConfigurationFile = defaultValue
            Dim value As String = ConfigurationManager.AppSettings(key).ToString
            Dim tempParsedValue As Integer = 0.0
            If Not String.IsNullOrEmpty(value) AndAlso Integer.TryParse(value, tempParsedValue) Then
                getIntegarlValueFromConfigurationFile = tempParsedValue
            End If
        End Function

#End Region

#Region "Get U.K Financial/Fiscal Year from Date"

        Public Shared Function getFinancialYear_UK(ByVal givenDateTime As Date) As Integer
            Return If(givenDateTime.Month >= 4, givenDateTime.Year, givenDateTime.Year - 1)
        End Function

#End Region


#Region "fetch HEADID and EXPENDITUREID for assign to contractor"

        Public Shared Function GetHeadAndExpenditureId() As String
            Return CType(ConfigurationManager.AppSettings("ExpenditueType"), String)
        End Function

#End Region


#Region "Get Push Notification Application Address"
        Public Shared Function getPushNotificationAddressForNotes(ByVal absoluteUri As String) As String
            Dim servername As String = absoluteUri.Substring(absoluteUri.IndexOf("//") + 2, Len(absoluteUri.Substring(absoluteUri.IndexOf("//") + 2, absoluteUri.Substring(absoluteUri.IndexOf("//") + 2).IndexOf("/"))))
            Return "https://" + servername + "/RSLHRModuleApi/FaultNotes"
        End Function
#End Region
    End Class

End Namespace
