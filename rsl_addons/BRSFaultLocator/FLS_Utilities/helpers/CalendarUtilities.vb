﻿Imports System.Data
Imports System.Net
Imports System.IO
Imports System.Xml
Imports Microsoft.Practices.EnterpriseLibrary.ExceptionHandling
Imports Microsoft.Practices.EnterpriseLibrary.Logging
Imports System.Configuration
Imports System.Text
Imports System.Security.Cryptography

Namespace FLS_Utilities
    Public Class CalendarUtilities
        Dim el As New FLS_Utilities.ErrorLogger
#Region "Create Columns for Datatable "
        Public Sub createColumns(ByVal colName As String, ByRef varWeeklyDT As DataTable)
            Dim Name As DataColumn = New DataColumn(colName)
            Name.DataType = System.Type.GetType("System.String")
            varWeeklyDT.Columns.Add(Name)

        End Sub
#End Region

#Region " Read Google Api settings "

        Public ReadOnly Property GoogleApiEnabled() As Boolean
            Get
                Return CType(ConfigurationManager.AppSettings.Item("GoolgeApiEnabled"), Boolean)
            End Get
        End Property

#End Region

#Region "Calculate Distance"

        ''' <summary>
        '''  If isGoogleError= true system will add a new row in data table againt these properties
        ''' </summary>
        ''' <param name="propertyFrom"></param>
        ''' <param name="propertyTo"></param>
        ''' <param name="isGoogleError"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function CalculateDistance(ByVal propertyFrom As String, ByVal propertyTo As String, Optional ByRef isGoogleError As Boolean = False) As String
            'Pass request to google api with orgin and destination details

            Dim distance As String = "0"
            Dim key As String = "_Mk3W_etiTwvijZB441zy0E3iDU="

            Dim url As String = String.Empty

            Try

                Trace.WriteLine(String.Format("propertyFrom - {0}; propertyTo - {1}; {2}", propertyFrom, propertyTo, DateTime.Now.ToShortTimeString))

                url = String.Format("http://maps.googleapis.com/maps/api/distancematrix/xml?origins={0}&destinations={1}&mode=Car&units=imperial&sensor=false&client=gme-broadlandhousing", propertyFrom, propertyTo)

                Trace.WriteLine(String.Format("Unsigned url: {0}", url))

                url = Sign(url, key)

                Trace.WriteLine(String.Format("Signed url: {0}", url))

                Dim request As HttpWebRequest = DirectCast(WebRequest.Create(url), HttpWebRequest)

                Trace.WriteLine(request.RequestUri.OriginalString)

                Dim response As HttpWebResponse = DirectCast(request.GetResponse(), HttpWebResponse)
                'Get response as stream from httpwebresponse
                Dim resStream As New StreamReader(response.GetResponseStream())
                Dim distanceXml As New XmlDocument()
                distanceXml.LoadXml(resStream.ReadToEnd())

                'Get DistanceMatrixResponse element and its values
                Dim xnList As XmlNodeList = distanceXml.SelectNodes("/DistanceMatrixResponse")

                For Each xn As XmlNode In xnList

                    el.WriteToErrorLog(distanceXml.DocumentElement.InnerXml.ToString(), "XML Returned", "Calculate Distance")
                    If xn("status").InnerText.ToString() = "OK" Then
                        distance = distanceXml.DocumentElement.SelectSingleNode("/DistanceMatrixResponse/row/element/distance/text").InnerText
                        'below are the conditions to convert the distance into miles
                        If (distance.Contains("ft")) Then
                            distance = Convert.ToString(Math.Round(Val(distance.Replace("ft", "")) * 0.000189394, 2))
                        ElseIf (distance.Contains("km")) Then
                            distance = Convert.ToString(Math.Round(Val(distance.Replace("km", "")) * 0.621371, 2))
                        ElseIf (distance.Contains("mi")) Then
                            distance = Convert.ToString(Math.Round(Val(distance.Replace("mi", "")), 2))
                        Else
                            distance = "0"
                        End If
                        isGoogleError = True
                        ApplicationConstants.distanceError = True

                    ElseIf xn("status").InnerText.ToString() = "INVALID_REQUEST" Then
                        ApplicationConstants.distanceError = False
                        UserMessageConstants.DistanceError = UserMessageConstants.INVALIDREQUEST
                    ElseIf xn("status").InnerText.ToString() = "MAX_ELEMENTS_EXCEEDED" Then
                        ApplicationConstants.distanceError = False
                        UserMessageConstants.DistanceError = UserMessageConstants.MAXELEMENTSEXCEEDED
                    ElseIf xn("status").InnerText.ToString() = "OVER_QUERY_LIMIT" Then
                        ApplicationConstants.distanceError = False
                        UserMessageConstants.DistanceError = UserMessageConstants.OVERQUERYLIMIT
                    ElseIf xn("status").InnerText.ToString() = "REQUEST_DENIED" Then
                        ApplicationConstants.distanceError = False
                        UserMessageConstants.DistanceError = UserMessageConstants.REQUESTDENIED
                    ElseIf xn("status").InnerText.ToString() = "UNKNOWN_ERROR" Then
                        ApplicationConstants.distanceError = False
                        UserMessageConstants.DistanceError = UserMessageConstants.UNKNOWNERROR

                    End If
                Next

            Catch ex As Exception
                el.WriteToErrorLog(ex.ToString(), "exception Calculated Distance", "exception Calculate Distance")
                ExceptionPolicy.HandleException(ex, "Exception Policy")
                Trace.WriteLine(ex.ToString)
            End Try

            Trace.WriteLine(distance)
            Return distance.Replace("mi", "")

        End Function

        Private Function Sign(ByVal url As String, ByVal keyString As String) As String

            Dim encoding As ASCIIEncoding = New ASCIIEncoding()

            'URL-safe decoding
            Dim privateKeyBytes As Byte() = Convert.FromBase64String(keyString.Replace("-", "+").Replace("_", "/"))

            Dim objURI As Uri = New Uri(url)
            Dim encodedPathAndQueryBytes As Byte() = encoding.GetBytes(objURI.LocalPath & objURI.Query)

            'compute the hash
            Dim algorithm As HMACSHA1 = New HMACSHA1(privateKeyBytes)
            Dim hash As Byte() = algorithm.ComputeHash(encodedPathAndQueryBytes)

            'convert the bytes to string and make url-safe by replacing '+' and '/' characters
            Dim signature As String = Convert.ToBase64String(hash).Replace("+", "-").Replace("/", "_")

            'Add the signature to the existing URI.
            Return objURI.Scheme & "://" & objURI.Host & objURI.LocalPath & objURI.Query & "&signature=" & signature

        End Function

#End Region

#Region "Formating Date with suffix like 1st, 2nd, 3rd"
        Public Function FormatDayNumberSuffix(ByVal calendarDay As String) As String
            Select Case calendarDay
                Case 1, 21, 31
                    calendarDay = calendarDay + "st"
                Case 2, 22
                    calendarDay = calendarDay + "nd"
                Case 3, 23
                    calendarDay = calendarDay + "rd"
                Case Else
                    calendarDay = calendarDay + "th"
            End Select

            Return calendarDay
        End Function
#End Region

#Region "Calculate Last Monday Of The Month"
        Public Sub calculateMonday()
            For dayCount = 0 To 10
                If (DateAdd("d", -(dayCount), ApplicationConstants.startDate).ToString("dddd") = "Monday") Then
                    ApplicationConstants.startDate = DateAdd("d", -(dayCount), ApplicationConstants.startDate)
                    ApplicationConstants.weekDays = ApplicationConstants.weekDays + dayCount
                    dayCount = 10
                End If
            Next
        End Sub
#End Region

#Region "Calculate Last Sunday of the Month"
        Public Sub calculateSunday()
            For dayCount = 0 To 10
                If (DateAdd("d", dayCount, ApplicationConstants.endDate).ToString("dddd") = "Sunday") Then
                    ApplicationConstants.endDate = DateAdd("d", dayCount, ApplicationConstants.endDate)
                    ApplicationConstants.weekDays = ApplicationConstants.weekDays + dayCount
                    dayCount = 10
                End If
            Next
        End Sub
#End Region

#Region "Format Date Function"

        'this utility functions converts date from mm/dd/yyyy formate to dd/mm/yyyy
        Public Shared Function FormatDate(ByVal str As String) As String
            If Not str.Equals("") And Not (str Is Nothing) Then
                Dim strArr As String() = str.Split(" ")
                Return Convert.ToDateTime(strArr(0)).ToString("dd/MM/yyyy")
            Else
                Return ("")
            End If
        End Function

#End Region

    End Class
End Namespace