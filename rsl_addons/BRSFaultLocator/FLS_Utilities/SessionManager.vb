﻿Imports System
Imports System.Web.HttpContext
Imports System.Web
Imports FLS_BusinessObject

Namespace FLS_Utilities

    Public Class SessionManager

#Region "Login In User "

#Region "Set / Get  Fault Scheduling User Id"

#Region "Set  Fault Scheduling User Id"
        Public Shared Sub setFaultSchedulingUserId(ByRef faultSchedulingUserId As Integer)
            Current.Session(SessionConstants.FaultSchedulingUserId) = faultSchedulingUserId
        End Sub
#End Region

#Region "get Fault Scheduling User Id"
        Public Shared Function getFaultSchedulingUserId() As Integer

            If (IsNothing(Current.Session(SessionConstants.FaultSchedulingUserId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionConstants.FaultSchedulingUserId), Integer)
            End If
        End Function
#End Region

#Region "remove  Fault Scheduling User Id"

        Public Sub removeFaultSchedulingUserId()
            If (Not IsNothing(Current.Session(SessionConstants.FaultSchedulingUserId))) Then
                Current.Session.Remove(SessionConstants.FaultSchedulingUserId)
            End If

        End Sub

#End Region

#End Region

#Region "Set / Get User Full Name"

#Region "Set User Full Name"

        Public Shared Sub setUserFullName(ByRef userFullName As String)
            Current.Session(SessionConstants.UserFullName) = userFullName
        End Sub

#End Region

#Region "get User Full Name"

        Public Shared Function getUserFullName() As String
            If (IsNothing(Current.Session(SessionConstants.UserFullName))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionConstants.UserFullName), String)
            End If
        End Function

#End Region

#Region "remove User Full Name"

        Public Shared Sub removeUserFullName()
            If (Not IsNothing(Current.Session(SessionConstants.UserFullName))) Then
                Current.Session.Remove(SessionConstants.UserFullName)
            End If

        End Sub

#End Region

#End Region

#Region "Set / Get User Employee Id"

#Region "Set User Employee Id"

        Public Shared Sub setUserEmployeeId(ByRef employeeId As String)
            Current.Session(SessionConstants.EmployeeId) = employeeId
        End Sub

#End Region

#Region "get User Employee Id"

        Public Shared Function getUserEmployeeId() As Integer
            If (IsNothing(Current.Session(SessionConstants.EmployeeId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionConstants.EmployeeId), Integer)
            End If
        End Function

#End Region

#Region "remove User Employee Id"

        Public Shared Sub removeUserEmployeeId()
            If (Not IsNothing(Current.Session(SessionConstants.EmployeeId))) Then
                Current.Session.Remove(SessionConstants.EmployeeId)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get User Type"

#Region "Set Logged In User Type"

        Public Shared Sub setLoggedInUserType(ByRef userType As String)
            Current.Session(SessionConstants.LoggedInUserType) = userType
        End Sub

#End Region

#Region "get Logged In User Type"

        Public Shared Function getLoggedInUserType() As String
            If (IsNothing(Current.Session(SessionConstants.LoggedInUserType))) Then
                Return String.Empty
            Else
                Return CType(Current.Session(SessionConstants.LoggedInUserType), String)
            End If
        End Function

#End Region

#Region "remove Logged In User Type"

        Public Shared Sub removeLoggedInUserType()
            If (Not IsNothing(Current.Session(SessionConstants.LoggedInUserType))) Then
                Current.Session.Remove(SessionConstants.LoggedInUserType)
            End If
        End Sub

#End Region

#End Region

#End Region

#Region "Set / Get Property Id"

#Region "Set Property Id"

        Public Shared Sub setPropertyId(ByRef propertyId As String)
            Current.Session(SessionConstants.PropertyId) = propertyId
        End Sub

#End Region

#Region "get Property Id"

        Public Shared Function getPropertyId()
            If (IsNothing(Current.Session(SessionConstants.PropertyId))) Then
                Return String.Empty
            Else
                Return CType(Current.Session(SessionConstants.PropertyId), String)
            End If
        End Function

#End Region

#Region "remove Property Id"

        Public Shared Sub removePropertyId()
            If (Not IsNothing(Current.Session(SessionConstants.PropertyId))) Then
                Current.Session.Remove(SessionConstants.PropertyId)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Customer Id"

#Region "Set Customer Id"

        Public Shared Sub setCustomerId(ByRef customerId As Integer)
            Current.Session(SessionConstants.CustomerId) = customerId
        End Sub

#End Region

#Region "get Customer Id"

        Public Shared Function getCustomerId()
            If (IsNothing(Current.Session(SessionConstants.CustomerId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionConstants.CustomerId), Integer)
            End If
        End Function

#End Region

#Region "remove Customer Id"

        Public Shared Sub removeCustomerId()
            If (Not IsNothing(Current.Session(SessionConstants.CustomerId))) Then
                Current.Session.Remove(SessionConstants.CustomerId)
            End If

        End Sub

#End Region

#End Region

#Region "Set / Get Scheme Id"

#Region "Set Scheme Id"

        Public Shared Sub setSchemeId(ByRef schemeId As Integer)
            Current.Session(SessionConstants.SchemeId) = schemeId
        End Sub

#End Region

#Region "get Scheme Id"

        Public Shared Function getSchemeId()
            If (IsNothing(Current.Session(SessionConstants.SchemeId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionConstants.SchemeId), Integer)
            End If
        End Function

#End Region

#Region "remove Scheme Id"

        Public Shared Sub removeSchemeId()
            If (Not IsNothing(Current.Session(SessionConstants.SchemeId))) Then
                Current.Session.Remove(SessionConstants.SchemeId)
            End If

        End Sub

#End Region

#End Region

#Region "Set / Get Block Id"

#Region "Set Block Id"

        Public Shared Sub setBlockId(ByRef blockId As Integer)
            Current.Session(SessionConstants.BlockId) = blockId
        End Sub

#End Region

#Region "get Block Id"

        Public Shared Function getBlockId()
            If (IsNothing(Current.Session(SessionConstants.BlockId))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.BlockId), Integer)
            End If
        End Function

#End Region

#Region "remove Block Id"

        Public Shared Sub removeBlockId()
            If (Not IsNothing(Current.Session(SessionConstants.BlockId))) Then
                Current.Session.Remove(SessionConstants.BlockId)
            End If

        End Sub

#End Region

#End Region

#Region "Set / Get Scheme Block Data"

#Region "Set Scheme Block Data"

        Public Shared Sub setSbData(ByRef sbData As SchemeBlockBO)
            Current.Session(SessionConstants.SbData) = sbData
        End Sub

#End Region

#Region "get Scheme Block Data"

        Public Shared Function getSbData()
            If (IsNothing(Current.Session(SessionConstants.SbData))) Then
                Dim SbData As New SchemeBlockBO
                Return SbData
            Else
                Return CType(Current.Session(SessionConstants.SbData), SchemeBlockBO)
            End If
        End Function

#End Region

#Region "remove Scheme Block Data"

        Public Shared Sub removeSbData()
            If (Not IsNothing(Current.Session(SessionConstants.SbData))) Then
                Current.Session.Remove(SessionConstants.SbData)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Faults For Appointment Summary"

#Region "Set Faults For Appointment Summary"

        Public Shared Sub setFaultsForAppointmentSummary(ByRef faultList As ArrayList)
            Current.Session(SessionConstants.AppointmentSummaryFaultList) = faultList
        End Sub

#End Region

#Region "get Faults For Appointment Summary"

        Public Shared Function getFaultsForAppointmentSummary()
            If (IsNothing(Current.Session(SessionConstants.AppointmentSummaryFaultList))) Then
                Dim faultList As ArrayList = Nothing
                Return faultList
            Else
                Return CType(Current.Session(SessionConstants.AppointmentSummaryFaultList), String)
            End If
        End Function

#End Region

#Region "remove Faults For Appointment Summary"

        Public Shared Sub removeFaultsForAppointmentSummary()
            If (Not IsNothing(Current.Session(SessionConstants.AppointmentSummaryFaultList))) Then
                Current.Session.Remove(SessionConstants.AppointmentSummaryFaultList)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Appointment Data For Appointment Summary"

#Region "Set Appointment Data For Appointment Summary"

        Public Shared Sub setAppointmentDataForAppointmentSummary(ByRef appointmentData As AppointmentBO)
            Current.Session(SessionConstants.AppointmentData) = appointmentData
        End Sub

#End Region

#Region "get Appointment Data For Appointment Summary"

        Public Shared Function getAppointmentDataForAppointmentSummary()
            If (IsNothing(Current.Session(SessionConstants.AppointmentData))) Then
                Dim appointmentData As New AppointmentBO
                Return appointmentData
            Else
                Return CType(Current.Session(SessionConstants.AppointmentData), AppointmentBO)
            End If
        End Function

#End Region

#Region "remove Appointment Data For Appointment Summary"

        Public Shared Sub removeAppointmentDataForAppointmentSummary()
            If (Not IsNothing(Current.Session(SessionConstants.AppointmentData))) Then
                Current.Session.Remove(SessionConstants.AppointmentData)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Job Sheet Number"

#Region "Set Job Sheet Number"

        Public Shared Sub setJobSheetNumber(ByRef JobSheetNumber As String)
            Current.Session(SessionConstants.JobSheetNumber) = JobSheetNumber
        End Sub

#End Region

#Region "get Job Sheet Number"

        Public Shared Function getJobSheetNumber()
            If (IsNothing(Current.Session(SessionConstants.JobSheetNumber))) Then
                Return False
            Else
                Return CType(Current.Session(SessionConstants.JobSheetNumber), String)
            End If
        End Function

#End Region

#Region "remove Job Sheet Number"

        Public Shared Sub removeJobSheetNumber()
            If (Not IsNothing(Current.Session(SessionConstants.JobSheetNumber))) Then
                Current.Session.Remove(SessionConstants.JobSheetNumber)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Sb Job Sheet Number"

#Region "Set Sb Job Sheet Number"

        Public Shared Sub setSbJobSheetNumber(ByRef sbJobSheetNumber As String)
            Current.Session(SessionConstants.SbJobSheetNumber) = SbJobSheetNumber
        End Sub

#End Region

#Region "get Sb Job Sheet Number"

        Public Shared Function getSbJobSheetNumber()
            If (IsNothing(Current.Session(SessionConstants.SbJobSheetNumber))) Then
                Return False
            Else
                Return CType(Current.Session(SessionConstants.SbJobSheetNumber), String)
            End If
        End Function

#End Region

#Region "remove Sb Job Sheet Number"

        Public Shared Sub removeSbJobSheetNumber()
            If (Not IsNothing(Current.Session(SessionConstants.SbJobSheetNumber))) Then
                Current.Session.Remove(SessionConstants.SbJobSheetNumber)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Subcontractor Job Sheet Index"

#Region "Set Job Subcontractor Job Sheet Index"

        Public Shared Sub setSubcontractorJobSheetIndex(ByRef JobSheetIndex As Integer)
            Current.Session(SessionConstants.SubcontractorJobSheetIndex) = JobSheetIndex
        End Sub

#End Region

#Region "get Subcontractor Job Sheet Index"

        Public Shared Function getSubcontractorJobSheetIndex()
            If (IsNothing(Current.Session(SessionConstants.SubcontractorJobSheetIndex))) Then
                Return False
            Else
                Return CType(Current.Session(SessionConstants.SubcontractorJobSheetIndex), Integer)
            End If
        End Function

#End Region

#Region "remove Subcontractor Job Sheet Index"

        Public Shared Sub removeSubcontractorJobSheetIndex()
            If (Not IsNothing(Current.Session(SessionConstants.SubcontractorJobSheetIndex))) Then
                Current.Session.Remove(SessionConstants.SubcontractorJobSheetIndex)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Subcontractor Current Fault BO  "

#Region "Set JobSubcontractor Current Fault BO"

        Public Shared Sub setSubcontractorCurrentFaultBO(ByRef currentFaultBO As FaultBO)
            Current.Session(SessionConstants.SubcontractorCurrentFaultBO) = currentFaultBO
        End Sub

#End Region

#Region "get Subcontractor Current Fault BO"

        Public Shared Function getSubcontractorCurrentFaultBO()
            If (IsNothing(Current.Session(SessionConstants.SubcontractorCurrentFaultBO))) Then
                Return False
            Else
                Return CType(Current.Session(SessionConstants.SubcontractorCurrentFaultBO), FaultBO)
            End If
        End Function

#End Region

#Region "remove Subcontractor Current Fault BO"

        Public Shared Sub removeSubcontractorCurrentFaultBO()
            If (Not IsNothing(Current.Session(SessionConstants.SubcontractorCurrentFaultBO))) Then
                Current.Session.Remove(SessionConstants.SubcontractorCurrentFaultBO)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Temporary Fault Bo"

#Region "Set Temporary Fault BO"

        Public Shared Sub setTemporaryFaultBo(ByRef arrSelectedTempFaultBO As ArrayList)
            Current.Session(SessionConstants.TemporaryFaultBo) = arrSelectedTempFaultBO
        End Sub

#End Region

#Region "Get Temporary Fault bo"

        Public Shared Function getTemporaryFaultBo()
            If (IsNothing(Current.Session(SessionConstants.TemporaryFaultBo))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.TemporaryFaultBo), ArrayList)
            End If
        End Function

#End Region

#Region "remove Temporary Fault bo"

        Public Shared Sub removeTemporaryFaultBo()
            If (Not IsNothing(Current.Session(SessionConstants.TemporaryFaultBo))) Then
                Current.Session.Remove(SessionConstants.TemporaryFaultBo)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Fault Bo"

#Region "Set Fault BO"

        Public Shared Sub setFaultBo(ByRef objFaultBo As FaultBO)
            Current.Session(SessionConstants.FaultBo) = objFaultBo
        End Sub

#End Region

#Region "Get Fault bo"

        Public Shared Function getFaultBo()
            If (IsNothing(Current.Session(SessionConstants.FaultBo))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.FaultBo), FaultBO)
            End If
        End Function

#End Region

#Region "remove Fault bo"

        Public Shared Sub removeFaultBo()
            If (Not IsNothing(Current.Session(SessionConstants.FaultBo))) Then
                Current.Session.Remove(SessionConstants.FaultBo)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Customer Data"

#Region "Set Customer Data"

        Public Shared Sub setCustomerData(ByRef customerData As CustomerBO)
            Current.Session(SessionConstants.CustomerData) = customerData
        End Sub

#End Region

#Region "get Customer Data"

        Public Shared Function getCustomerData()
            If (IsNothing(Current.Session(SessionConstants.CustomerData))) Then
                Dim customerData As New CustomerBO
                Return customerData
            Else
                Return CType(Current.Session(SessionConstants.CustomerData), CustomerBO)
            End If
        End Function

#End Region

#Region "remove Customer Data"

        Public Shared Sub removeCustomerData()
            If (Not IsNothing(Current.Session(SessionConstants.CustomerData))) Then
                Current.Session.Remove(SessionConstants.CustomerData)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Selected Temp Fault Ids"

#Region "Set Selected TempFaultIds"

        Public Shared Sub setSelectedTempFaultIds(ByRef FaultLogIds As String)
            Current.Session(SessionConstants.TempFaultIds) = FaultLogIds
        End Sub

#End Region

#Region "Get Selected Temp Fault Ids"

        Public Shared Function getSelectedTempFaultIds()
            If (IsNothing(Current.Session(SessionConstants.TempFaultIds))) Then
                Return False
            Else
                Return CType(Current.Session(SessionConstants.TempFaultIds), String)
            End If
        End Function

#End Region

#Region "Remove Selected Temp Fault Ids"

        Public Shared Sub removeSelectedTempFaultIds()
            If (Not IsNothing(Current.Session(SessionConstants.TempFaultIds))) Then
                Current.Session.Remove(SessionConstants.TempFaultIds)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Follow On DataSet"

#Region "Set Follow On DataSet"

        Public Shared Sub setFollowOnDataSet(ByRef followOnDataSet As DataSet)
            Current.Session(SessionConstants.FollowOnDataSet) = followOnDataSet
        End Sub

#End Region

#Region "Get Follow On DataSet"

        Public Shared Function getFollowOnDataSet()
            If (IsNothing(Current.Session(SessionConstants.FollowOnDataSet))) Then
                Dim followOnDataSet As New DataSet
                Return followOnDataSet
            Else
                Return CType(Current.Session(SessionConstants.FollowOnDataSet), DataSet)
            End If
        End Function

#End Region

#Region "remove Follow On DataSet"

        Public Shared Sub removeFollowOnDataSet()
            If (Not IsNothing(Current.Session(SessionConstants.FollowOnDataSet))) Then
                Current.Session.Remove(SessionConstants.FollowOnDataSet)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get No Entry DataSet"

#Region "Set No Entry DataSet"

        Public Shared Sub setNoEntryDataSet(ByRef noEntryDataSet As DataSet)
            Current.Session(SessionConstants.NoEntryDataSet) = noEntryDataSet
        End Sub

#End Region

#Region "Get No Entry DataSet"

        Public Shared Function getNoEntryDataSet()
            If (IsNothing(Current.Session(SessionConstants.NoEntryDataSet))) Then
                Dim noEntryDataSet As New DataSet
                Return noEntryDataSet
            Else
                Return CType(Current.Session(SessionConstants.NoEntryDataSet), DataSet)
            End If
        End Function

#End Region

#Region "remove No Entry DataSet"

        Public Shared Sub removeNoEntryDataSet()
            If (Not IsNothing(Current.Session(SessionConstants.NoEntryDataSet))) Then
                Current.Session.Remove(SessionConstants.NoEntryDataSet)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Fault Log ID"

#Region "Set Fault Log ID"

        Public Shared Sub setFaultLogId(ByRef faultLogId As Integer)
            Current.Session(SessionConstants.FaultLogId) = faultLogId
        End Sub

#End Region

#Region "Get Fault Log ID"

        Public Shared Function getFaultLogId()
            If (IsNothing(Current.Session(SessionConstants.FaultLogId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionConstants.FaultLogId), Integer)
            End If
        End Function

#End Region

#Region "remove Fault Log ID"

        Public Shared Sub removeFaultLogId()
            If (Not IsNothing(Current.Session(SessionConstants.FaultLogId))) Then
                Current.Session.Remove(SessionConstants.FaultLogId)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Sb Fault Log ID"

#Region "Set Sb Fault Log ID"

        Public Shared Sub setSbFaultLogId(ByRef sbFaultLogId As Integer)
            Current.Session(SessionConstants.SbFaultLogId) = sbFaultLogId
        End Sub

#End Region

#Region "Get Sb Fault Log ID"

        Public Shared Function getSbFaultLogId()
            If (IsNothing(Current.Session(SessionConstants.SbFaultLogId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionConstants.SbFaultLogId), Integer)
            End If
        End Function

#End Region

#Region "remove b Fault Log ID"

        Public Shared Sub removeSbFaultLogId()
            If (Not IsNothing(Current.Session(SessionConstants.SbFaultLogId))) Then
                Current.Session.Remove(SessionConstants.FaultLogId)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Recall DataSet"

#Region "Set Recall DataSet"

        Public Shared Sub setRecallDataSet(ByRef recallDataSet As DataSet)
            Current.Session(SessionConstants.RecallDataSet) = recallDataSet
        End Sub

#End Region

#Region "Get Recall DataSet"

        Public Shared Function getRecallDataSet()
            If (IsNothing(Current.Session(SessionConstants.RecallDataSet))) Then
                Dim recallDataSet As New DataSet
                Return recallDataSet
            Else
                Return CType(Current.Session(SessionConstants.RecallDataSet), DataSet)
            End If
        End Function

#End Region

#Region "remove Recall DataSet"

        Public Shared Sub removeRecallDataSet()
            If (Not IsNothing(Current.Session(SessionConstants.RecallDataSet))) Then
                Current.Session.Remove(SessionConstants.RecallDataSet)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get IsRecall"

#Region "Set Is Recall"

        Public Shared Sub setIsRecall(ByRef isRecall As Boolean)
            Current.Session(SessionConstants.IsRecall) = isRecall
        End Sub

#End Region

#Region "Get Is Recall"

        Public Shared Function getIsRecall()
            If (IsNothing(Current.Session(SessionConstants.IsRecall))) Then
                Return False
            Else
                Return CType(Current.Session(SessionConstants.IsRecall), Boolean)
            End If
        End Function

#End Region

#Region "remove Is Recall"

        Public Shared Sub removeIsRecall()
            If (Not IsNothing(Current.Session(SessionConstants.IsRecall))) Then
                Current.Session.Remove(SessionConstants.IsRecall)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get IsSbRecall"

#Region "Set Is Sb Recall"

        Public Shared Sub setIsSbRecall(ByRef isSbRecall As Boolean)
            Current.Session(SessionConstants.IsSbRecall) = isSbRecall
        End Sub

#End Region

#Region "Get Is  Sb Recall"

        Public Shared Function getIsSbRecall()
            If (IsNothing(Current.Session(SessionConstants.IsSbRecall))) Then
                Return False
            Else
                Return CType(Current.Session(SessionConstants.IsSbRecall), Boolean)
            End If
        End Function

#End Region

#Region "remove Is  Sb Recall"

        Public Shared Sub removeIsSbRecall()
            If (Not IsNothing(Current.Session(SessionConstants.IsSbRecall))) Then
                Current.Session.Remove(SessionConstants.IsSbRecall)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Check for Redirect"

#Region "Set Is Recall"

        Public Shared Sub setRedirectCheck(ByRef redirectCheck As Boolean)
            Current.Session(SessionConstants.RedirectCheck) = redirectCheck
        End Sub

#End Region

#Region "Get Is Check for Redirect"

        Public Shared Function getRedirectCheck()
            If (IsNothing(Current.Session(SessionConstants.RedirectCheck))) Then
                Return False
            Else
                Return CType(Current.Session(SessionConstants.RedirectCheck), Boolean)
            End If
        End Function

#End Region

#Region "remove Is Check for Redirect"

        Public Shared Sub removeRedirectCheck()
            If (Not IsNothing(Current.Session(SessionConstants.RedirectCheck))) Then
                Current.Session.Remove(SessionConstants.RedirectCheck)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Sb Check for Redirect"

#Region "Set Sb Redirect Check"

        Public Shared Sub setSbRedirectCheck(ByRef sbRedirectCheck As Boolean)
            Current.Session(SessionConstants.SbRedirectCheck) = sbRedirectCheck
        End Sub

#End Region

#Region "Get Sb Redirect Check"

        Public Shared Function getSbRedirectCheck()
            If (IsNothing(Current.Session(SessionConstants.SbRedirectCheck))) Then
                Return False
            Else
                Return CType(Current.Session(SessionConstants.SbRedirectCheck), Boolean)
            End If
        End Function

#End Region

#Region "remove Sb Redirect Check"

        Public Shared Sub removeSbSbRedirectCheck()
            If (Not IsNothing(Current.Session(SessionConstants.SbRedirectCheck))) Then
                Current.Session.Remove(SessionConstants.SbRedirectCheck)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Fault ID"

#Region "Set Fault ID"

        Public Shared Sub setFaultId(ByRef faultId As Integer)
            Current.Session(SessionConstants.FaultId) = faultId
        End Sub

#End Region

#Region "Get Fault ID"

        Public Shared Function getFaultId()
            If (IsNothing(Current.Session(SessionConstants.FaultId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionConstants.FaultId), Integer)
            End If
        End Function

#End Region

#Region "remove Fault ID"

        Public Shared Sub removeFaultId()
            If (Not IsNothing(Current.Session(SessionConstants.FaultId))) Then
                Current.Session.Remove(SessionConstants.FaultId)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Sb Fault ID"

#Region "Set Sb Fault ID"

        Public Shared Sub setSbFaultId(ByRef sbFaultId As Integer)
            Current.Session(SessionConstants.SbFaultId) = sbFaultId
        End Sub

#End Region

#Region "Get Sb Fault ID"

        Public Shared Function getSbFaultId()
            If (IsNothing(Current.Session(SessionConstants.SbFaultId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionConstants.SbFaultId), Integer)
            End If
        End Function

#End Region

#Region "remove  Sb Fault ID"

        Public Shared Sub removeSbFaultId()
            If (Not IsNothing(Current.Session(SessionConstants.SbFaultId))) Then
                Current.Session.Remove(SessionConstants.SbFaultId)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Page Source"

#Region "Set Page Source"

        Public Shared Sub setPageSource(ByRef source As String)
            Current.Session(SessionConstants.Source) = source
        End Sub

#End Region

#Region "get Page Source"

        Public Shared Function getPageSource()
            If (IsNothing(Current.Session(SessionConstants.Source))) Then
                Return False
            Else
                Return CType(Current.Session(SessionConstants.Source), String)
            End If
        End Function

#End Region

#Region "remove Page Source"

        Public Shared Sub removePageSource()
            If (Not IsNothing(Current.Session(SessionConstants.Source))) Then
                Current.Session.Remove(SessionConstants.Source)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Sb Get Page Source"

#Region "Set Sb Page Source"

        Public Shared Sub setSbPageSource(ByRef source As String)
            Current.Session(SessionConstants.SbSource) = source
        End Sub

#End Region

#Region "get Sb Page Source"

        Public Shared Function getSbPageSource()
            If (IsNothing(Current.Session(SessionConstants.SbSource))) Then
                Return False
            Else
                Return CType(Current.Session(SessionConstants.SbSource), String)
            End If
        End Function

#End Region

#Region "remove Sb Page Source"

        Public Shared Sub removeSbPageSource()
            If (Not IsNothing(Current.Session(SessionConstants.SbSource))) Then
                Current.Session.Remove(SessionConstants.SbSource)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Follow On Page Sort Data"

#Region "Set Follow On Page Sort Data"

        Public Shared Sub setFollowOnPageSortData(ByRef followOnPageSortBO As PageSortBO)
            Current.Session(SessionConstants.FollowOnPageSortData) = followOnPageSortBO
        End Sub

#End Region

#Region "Get Follow On Page Sort Data"

        Public Shared Function getFollowOnPageSortData()
            If (IsNothing(Current.Session(SessionConstants.FollowOnPageSortData))) Then
                Dim followOnPageSortBO As New PageSortBO("DESC", "Recorded", 1, 30)
                Return followOnPageSortBO
            Else
                Return CType(Current.Session(SessionConstants.FollowOnPageSortData), PageSortBO)
            End If
        End Function

#End Region

#Region "Remove Follow On Page Sort Data"

        Public Shared Sub removeFollowOnPageSortData()
            If (Not IsNothing(Current.Session(SessionConstants.FollowOnPageSortData))) Then
                Current.Session.Remove(SessionConstants.FollowOnPageSortData)
            End If

        End Sub

#End Region

#End Region

#Region "Set / Get Group By"

#Region "Set Group By"

        Public Shared Sub setGroupBy(ByRef groupBy As Boolean)
            Current.Session(SessionConstants.GroupBy) = groupBy
        End Sub

#End Region

#Region "Get Group By"

        Public Shared Function getGroupBy()
            If (IsNothing(Current.Session(SessionConstants.GroupBy))) Then
                Return False
            Else
                Return CType(Current.Session(SessionConstants.GroupBy), Boolean)
            End If
        End Function

#End Region

#Region "remove Group By"

        Public Shared Sub removeGroupBy()
            If (Not IsNothing(Current.Session(SessionConstants.GroupBy))) Then
                Current.Session.Remove(SessionConstants.GroupBy)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get JSN List"

#Region "Set JSN List"

        Public Shared Sub setJsnId(ByRef JsnIds As String)
            Current.Session(SessionConstants.Jsn) = JsnIds
        End Sub

#End Region

#Region "Get JSN"

        Public Shared Function getJsnId()
            If (IsNothing(Current.Session(SessionConstants.Jsn))) Then
                Return String.Empty
            Else
                Return CType(Current.Session(SessionConstants.Jsn), String)
            End If
        End Function

#End Region

#Region "Remove JSN"

        Public Shared Sub removeJsnId()
            If (Not IsNothing(Current.Session(SessionConstants.Jsn))) Then
                Current.Session.Remove(SessionConstants.Jsn)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Temporary Faults Ds"

#Region "Set Temp Faults Ds"

        Public Shared Sub setTempFaultsDs(ByRef tempFaultDs As DataSet)
            Current.Session(SessionConstants.TempFaultDs) = tempFaultDs
        End Sub

#End Region

#Region "Get Temp Faults Ds"

        Public Shared Function getTempFaultsDs()
            If (IsNothing(Current.Session(SessionConstants.TempFaultDs))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.TempFaultDs), DataSet)
            End If
        End Function

#End Region

#Region "remove Temp Faults Ds"

        Public Shared Sub removesetTempFaultsDs()
            If (Not IsNothing(Current.Session(SessionConstants.TempFaultDs))) Then
                Current.Session.Remove(SessionConstants.TempFaultDs)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Avaialble Operative Ds"

#Region "Set  Avaialble Operative Ds"

        Public Shared Sub setAvailableOperativesDs(ByRef availableOperativesDs As DataSet)
            Current.Session(SessionConstants.AvailableOperativesDs) = availableOperativesDs
        End Sub

#End Region

#Region "Get  Avaialble Operative Ds"

        Public Shared Function getAvailableOperativesDs()
            If (IsNothing(Current.Session(SessionConstants.AvailableOperativesDs))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.AvailableOperativesDs), DataSet)
            End If
        End Function

#End Region

#Region "remove  Avaialble Operative Ds"

        Public Shared Sub removeAvailableOperativeDs()
            If (Not IsNothing(Current.Session(SessionConstants.AvailableOperativesDs))) Then
                Current.Session.Remove(SessionConstants.AvailableOperativesDs)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Selected Temporary Faults Dv"

#Region "Set Selected Temp Faults Dv"

        Public Shared Sub setSelectedTempFaultsDv(ByRef selectedTempFaultDv As DataView)
            Current.Session(SessionConstants.SelectedTempFaultDv) = selectedTempFaultDv
        End Sub

#End Region

#Region "Get Selected Temp Faults Dv"

        Public Shared Function getSelectedTempFaultsDv()
            If (IsNothing(Current.Session(SessionConstants.SelectedTempFaultDv))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.SelectedTempFaultDv), DataView)
            End If
        End Function

#End Region

#Region "remove Selected Temp Faults Dv"

        Public Shared Sub removeSelectedTempFaultsDv()
            If (Not IsNothing(Current.Session(SessionConstants.SelectedTempFaultDv))) Then
                Current.Session.Remove(SessionConstants.SelectedTempFaultDv)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Sb Temporary Faults Ds"

#Region "Set Temp Sb Faults Ds"

        Public Shared Sub setTempSbFaultsDs(ByRef tempSbFaultDs As DataSet)
            Current.Session(SessionConstants.TempSbFaultDs) = tempSbFaultDs
        End Sub

#End Region

#Region "Get Temp Sb Faults Ds"

        Public Shared Function getTempSbFaultsDs()
            If (IsNothing(Current.Session(SessionConstants.TempSbFaultDs))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.TempSbFaultDs), DataSet)
            End If
        End Function

#End Region

#Region "remove Temp  Sb Faults Ds"

        Public Shared Sub removeSbTempSbFaultsDs()
            If (Not IsNothing(Current.Session(SessionConstants.TempSbFaultDs))) Then
                Current.Session.Remove(SessionConstants.TempSbFaultDs)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Confirmed Faults"

#Region "Set Confirmed Faults"

        Public Shared Sub setConfirmedFaults(ByRef confirmedFaults As DataTable)
            Current.Session(SessionConstants.ConfirmedFaults) = confirmedFaults
        End Sub

#End Region

#Region "Get Confirmed Faults"

        Public Shared Function getConfirmedFaults() As DataTable
            If (IsNothing(Current.Session(SessionConstants.ConfirmedFaults))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.ConfirmedFaults), DataTable)
            End If
        End Function

#End Region

#Region "remove Confirmed Faults"

        Public Shared Sub removeConfirmedFaults()
            If (Not IsNothing(Current.Session(SessionConstants.ConfirmedFaults))) Then
                Current.Session.Remove(SessionConstants.ConfirmedFaults)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Confirmed Gas Appointments"

#Region "Set Confirmed Gas Appointments"

        Public Shared Sub setConfirmedGasAppointment(ByRef confirmedFaults As DataTable)
            Current.Session(SessionConstants.ConfirmedGasAppointment) = confirmedFaults
        End Sub

#End Region

#Region "Get Confirmed Gas Appointments"

        Public Shared Function getConfirmedGasAppointment() As DataTable
            If (IsNothing(Current.Session(SessionConstants.ConfirmedGasAppointment))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.ConfirmedGasAppointment), DataTable)
            End If
        End Function

#End Region

#Region "remove Confirmed Gas Appointment"

        Public Shared Sub removeConfirmedGasAppointment()
            If (Not IsNothing(Current.Session(SessionConstants.ConfirmedGasAppointment))) Then
                Current.Session.Remove(SessionConstants.ConfirmedGasAppointment)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Sb Confirmed Faults"

#Region "Set Sb Confirmed Faults"

        Public Shared Sub setSbConfirmedFaults(ByRef sbConfirmedFaults As DataTable)
            Current.Session(SessionConstants.SbConfirmedFaults) = sbConfirmedFaults
        End Sub

#End Region

#Region "Get Sb Confirmed Faults"

        Public Shared Function getSbConfirmedFaults()
            If (IsNothing(Current.Session(SessionConstants.SbConfirmedFaults))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.SbConfirmedFaults), DataTable)
            End If
        End Function

#End Region

#Region "remove Sb Confirmed Faults"

        Public Shared Sub removeSbConfirmedFaults()
            If (Not IsNothing(Current.Session(SessionConstants.SbConfirmedFaults))) Then
                Current.Session.Remove(SessionConstants.SbConfirmedFaults)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Confirmed Appointment Id List"

#Region "Set Confirmed Appointment Id List"

        Public Shared Sub setConfirmedAppointmentIdList(ByRef confirmedAppointmentIdList As List(Of AppointmentBO))
            Current.Session(SessionConstants.ConfirmedAppointmentIdList) = confirmedAppointmentIdList
        End Sub

#End Region

#Region "Set Confirmed Appointment Id List"

        Public Shared Function getConfirmedAppointmentIdList()
            If (IsNothing(Current.Session(SessionConstants.ConfirmedAppointmentIdList))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.ConfirmedAppointmentIdList), List(Of AppointmentBO))
            End If
        End Function

#End Region

#Region "remove Confirmed Appointment Id List"

        Public Shared Sub removeConfirmedAppointmentIdList()
            If (Not IsNothing(Current.Session(SessionConstants.ConfirmedAppointmentIdList))) Then
                Current.Session.Remove(SessionConstants.ConfirmedAppointmentIdList)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Hashtable"

#Region "Set Hashtable"
        Public Shared Sub setHashTable(ByRef hashTable As System.Collections.Hashtable)
            Current.Session(SessionConstants.HashTable) = hashTable
        End Sub
#End Region

#Region "Get Hashtable"
        Public Shared Function getHashTable()
            If (IsNothing(Current.Session(SessionConstants.HashTable))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.HashTable), System.Collections.Hashtable)
            End If
        End Function
#End Region

#Region "remove Hashtable"
        Public Shared Sub removeHashTable()
            If (Not IsNothing(Current.Session(SessionConstants.HashTable))) Then
                Current.Session.Remove(SessionConstants.HashTable)
            End If
        End Sub
#End Region

#End Region

#Region "Set / Get Appointment Id"

#Region "Set Appointment Id"

        Public Shared Sub setAppointmentId(ByRef appointmentId As Integer)
            Current.Session(SessionConstants.AppointmentId) = appointmentId
        End Sub

#End Region

#Region "get Appointment Id"

        Public Shared Function getAppointmentId()
            If (IsNothing(Current.Session(SessionConstants.AppointmentId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionConstants.AppointmentId), Integer)
            End If
        End Function

#End Region

#Region "remove Appointment Id"

        Public Shared Sub removeAppointmentId()
            If (Not IsNothing(Current.Session(SessionConstants.AppointmentId))) Then
                Current.Session.Remove(SessionConstants.AppointmentId)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Sb Appointment Id"

#Region "Set Sb Appointment Id"

        Public Shared Sub setSbAppointmentId(ByRef sbAppointmentId As Integer)
            Current.Session(SessionConstants.SbAppointmentId) = sbAppointmentId
        End Sub

#End Region

#Region "get Sb Appointment Id"

        Public Shared Function getSbAppointmentId()
            If (IsNothing(Current.Session(SessionConstants.SbAppointmentId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionConstants.SbAppointmentId), Integer)
            End If
        End Function

#End Region

#Region "remove Sb Appointment Id"

        Public Shared Sub removeSbAppointmentId()
            If (Not IsNothing(Current.Session(SessionConstants.SbAppointmentId))) Then
                Current.Session.Remove(SessionConstants.SbAppointmentId)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Original Fault Log ID"

#Region "Set Original Fault Log ID"

        Public Shared Sub setOriginalFaultLogId(ByRef originalFaultLogId As Integer)
            Current.Session(SessionConstants.OriginalFaultLogId) = originalFaultLogId
        End Sub

#End Region

#Region "Get Original Fault Log ID"

        Public Shared Function getOriginalFaultLogId()
            If (IsNothing(Current.Session(SessionConstants.OriginalFaultLogId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionConstants.OriginalFaultLogId), Integer)
            End If
        End Function

#End Region

#Region "remove Original Fault Log ID"

        Public Shared Sub removeOriginalFaultLogId()
            If (Not IsNothing(Current.Session(SessionConstants.OriginalFaultLogId))) Then
                Current.Session.Remove(SessionConstants.OriginalFaultLogId)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Original Sb Fault Log ID"

#Region "Set Original Sb Fault Log ID"

        Public Shared Sub setOriginalSbFaultLogId(ByRef originalSbFaultLogId As Integer)
            Current.Session(SessionConstants.OriginalSbFaultLogId) = originalSbFaultLogId
        End Sub

#End Region

#Region "Get Original Sb Fault Log ID"

        Public Shared Function getOriginalSbFaultLogId()
            If (IsNothing(Current.Session(SessionConstants.OriginalSbFaultLogId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionConstants.OriginalSbFaultLogId), Integer)
            End If
        End Function

#End Region

#Region "remove Original Sb Fault Log ID"

        Public Shared Sub removeOriginalSbFaultLogId()
            If (Not IsNothing(Current.Session(SessionConstants.OriginalSbFaultLogId))) Then
                Current.Session.Remove(SessionConstants.OriginalSbFaultLogId)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get PopupOpened"

#Region "Set PopupOpened"

        Public Shared Sub setPopupOpened(ByRef popupOpened As Boolean)
            Current.Session(SessionConstants.PopupOpened) = popupOpened
        End Sub

#End Region

#Region "Get PopupOpened"

        Public Shared Function getPopupOpened()
            If (IsNothing(Current.Session(SessionConstants.PopupOpened))) Then
                Return False
            Else
                Return CType(Current.Session(SessionConstants.PopupOpened), Boolean)
            End If
        End Function

#End Region

#Region "remove PopupOpened"

        Public Shared Sub removePopupOpened()
            If (Not IsNothing(Current.Session(SessionConstants.PopupOpened))) Then
                Current.Session.Remove(SessionConstants.PopupOpened)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get isSuccessAdded"

#Region "Set isSuccessAdded"

        Public Shared Sub setisSuccessAdded(ByRef isSuccessAdded As Boolean)
            Current.Session(SessionConstants.isSuccessAdded) = isSuccessAdded
        End Sub

#End Region

#Region "Get isSuccessAdded"

        Public Shared Function getisSuccessAdded()
            If (IsNothing(Current.Session(SessionConstants.isSuccessAdded))) Then
                Return False
            Else
                Return CType(Current.Session(SessionConstants.isSuccessAdded), Boolean)
            End If
        End Function

#End Region

#Region "remove isSuccessAdded"

        Public Shared Sub removeisSuccessAdded()
            If (Not IsNothing(Current.Session(SessionConstants.isSuccessAdded))) Then
                Current.Session.Remove(SessionConstants.isSuccessAdded)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Calendar Appointments"

#Region "Set Calendar Appointments"

        Public Shared Sub setCalendarAppointments(ByRef weeklyAptCalendar As DataTable)
            Current.Session(SessionConstants.weeklyAptCalendarDT) = weeklyAptCalendar
        End Sub

#End Region

#Region "Get Calendar Appointments"

        Public Shared Function getCalendarAppointments()
            If (IsNothing(Current.Session(SessionConstants.weeklyAptCalendarDT))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.weeklyAptCalendarDT), DataTable)
            End If
        End Function

#End Region

#Region "remove Calendar Appointments"

        Public Shared Sub removeCalendarAppointments()
            If (Not IsNothing(Current.Session(SessionConstants.weeklyAptCalendarDT))) Then
                Current.Session.Remove(SessionConstants.weeklyAptCalendarDT)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get is Appointment Rearrange"

#Region "Set Is Appointment Rearrange"

        Public Shared Sub setIsAppointmentRearrange(ByRef isAppointmentRearrange As Boolean)
            Current.Session(SessionConstants.IsAppointmentRearrange) = isAppointmentRearrange
        End Sub

#End Region

#Region "Get Is Appointment Rearrange"

        Public Shared Function getIsAppointmentRearrange()
            If (IsNothing(Current.Session(SessionConstants.IsAppointmentRearrange))) Then
                Return False
            Else
                Return CType(Current.Session(SessionConstants.IsAppointmentRearrange), Boolean)
            End If
        End Function

#End Region

#Region "remove Is Appointment Rearrange"

        Public Shared Sub removeIsAppointmentRearrange()
            If (Not IsNothing(Current.Session(SessionConstants.IsAppointmentRearrange))) Then
                Current.Session.Remove(SessionConstants.IsAppointmentRearrange)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get is Sb Appointment Rearrange"

#Region "Set Is Sb Appointment Rearrange"

        Public Shared Sub setIsSbAppointmentRearrange(ByRef isSbAppointmentRearrange As Boolean)
            Current.Session(SessionConstants.IsSbAppointmentRearrange) = isSbAppointmentRearrange
        End Sub

#End Region

#Region "Get Is Sb Appointment Rearrange"

        Public Shared Function getIsSbAppointmentRearrange()
            If (IsNothing(Current.Session(SessionConstants.IsSbAppointmentRearrange))) Then
                Return False
            Else
                Return CType(Current.Session(SessionConstants.IsSbAppointmentRearrange), Boolean)
            End If
        End Function

#End Region

#Region "remove Is Sb Appointment Rearrange"

        Public Shared Sub removeIsSbAppointmentRearrange()
            If (Not IsNothing(Current.Session(SessionConstants.IsSbAppointmentRearrange))) Then
                Current.Session.Remove(SessionConstants.IsSbAppointmentRearrange)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Paths Data"

#Region "Set Is Paths Data"

        Public Shared Sub setPathsData(ByRef pathsData As Dictionary(Of String, String))
            Current.Session(SessionConstants.PathsData) = pathsData
        End Sub

#End Region

#Region "Get Is Paths Data"

        Public Shared Function getPathsData()
            If (IsNothing(Current.Session(SessionConstants.PathsData))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.PathsData), Dictionary(Of String, String))
            End If
        End Function

#End Region

#Region "remove Is Paths Data"

        Public Shared Sub removePathsData()
            If (Not IsNothing(Current.Session(SessionConstants.PathsData))) Then
                Current.Session.Remove(SessionConstants.PathsData)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Dropdown List (Patch,Scheme and Trade) Values"

#Region "Set Dropdown List Values"

        Public Shared Sub setDDLData(ByRef ddlData As String)
            Current.Session(SessionConstants.ddlData) = ddlData
        End Sub

#End Region

#Region "get Dropdown List Values"

        Public Shared Function getDDLData() As String
            If (IsNothing(Current.Session(SessionConstants.ddlData))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionConstants.ddlData), String)
            End If
        End Function

#End Region

#Region "remove Dropdown List Values"

        Public Shared Sub removeDDLData()
            If (Not IsNothing(Current.Session(SessionConstants.ddlData))) Then
                Current.Session.Remove(SessionConstants.ddlData)
            End If

        End Sub

#End Region

#End Region

#Region "Set / Get Displayed Operatives"

#Region "Set Displayed Operatives"
        ''' <summary>
        ''' This function is especiall for screen 7,9a to implement the functionality of refresh list button.
        ''' session key should be in session constants but due to its dynamic behaviour and multiple datatables
        ''' we are passing session key as parameter.
        ''' </summary>
        ''' <param name="operativeDt"></param>
        ''' <param name="sessionKey"></param>
        ''' <remarks></remarks>
        Public Shared Sub setDisplayedOperatives(ByRef operativeDt As DataTable, ByRef sessionKey As Integer)
            Dim key As String = SessionConstants.OperativesBlock + sessionKey.ToString()
            Current.Session(key) = operativeDt
        End Sub

#End Region

#Region "Get Displayed Operatives"
        ''' <summary>
        ''' This function is especiall for screen 7,9a to implement the functionality of refresh list button.
        ''' session key should be in session constants but due to its dynamic behaviour and multiple datatables
        ''' we are passing session key as parameter.
        ''' </summary>
        ''' <param name="sessionKey"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getDisplayedOperatives(ByRef sessionKey As Integer)
            Dim key As String = SessionConstants.OperativesBlock + sessionKey.ToString()

            If (IsNothing(Current.Session(key))) Then
                Return Nothing
            Else
                Return CType(Current.Session(key), DataTable)
            End If
        End Function

#End Region

#Region "remove Displayed Operatives"
        ''' <summary>
        ''' This function is especiall for screen 7,9a to implement the functionality of refresh list button.
        ''' session key should be in session constants but due to its dynamic behaviour and multiple datatables
        ''' we are passing session key as parameter.
        ''' </summary>
        ''' <param name="sessionKey"></param>
        ''' <remarks></remarks>
        Public Shared Sub removeDisplayedOperatives(ByRef sessionKey As Integer)
            Dim key As String = SessionConstants.OperativesBlock + sessionKey.ToString()
            If (Not IsNothing(Current.Session(key))) Then
                Current.Session.Remove(key)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Previous Calculated Slots"

#Region "Set Previous Calculated Slots"
        ''' <summary>
        ''' This function is especiall for screen  15 to implement the functionality of refresh list button.        
        ''' </summary>
        ''' <param name="preCalculatedSlotsDt"></param>
        ''' <remarks></remarks>
        Public Shared Sub setPreCalculatedSlots(ByRef preCalculatedSlotsDt As DataTable)
            Current.Session(SessionConstants.CalculatedSlots) = preCalculatedSlotsDt
        End Sub

#End Region

#Region "Get Previous Calculated Slots"
        ''' <summary>
        ''' This function is especiall for screen 15 to implement the functionality of refresh list button.        
        ''' </summary>        
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getPreCalculatedSlots()

            If (IsNothing(Current.Session(SessionConstants.CalculatedSlots))) Then
                Return Nothing
            Else
                Return Current.Session(SessionConstants.CalculatedSlots)
            End If
        End Function

#End Region

#Region "remove Previous Calculated Slots"
        ''' <summary>
        ''' This function is especiall for screen 7,9a to implement the functionality of refresh list button.       
        ''' </summary>        
        ''' <remarks></remarks>
        Public Shared Sub removePreCalculatedSlots()
            If (Not IsNothing(Current.Session(SessionConstants.CalculatedSlots))) Then
                Current.Session.Remove(SessionConstants.CalculatedSlots)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Source For Scheduling Calendar"

#Region "Set Source For Scheduling Calendar"

        Public Shared Sub setSFSCalendar(ByRef source As String)
            Current.Session(SessionConstants.SFSCalendar) = source
        End Sub

#End Region

#Region "Get Source For Scheduling Calendar"

        Public Shared Function getSFSCalendar() As String
            If (IsNothing(Current.Session(SessionConstants.SFSCalendar))) Then
                Return String.Empty
            Else
                Return CType(Current.Session(SessionConstants.SFSCalendar), String)
            End If
        End Function

#End Region

#Region "Remove Source For Scheduling Calendar"

        Public Shared Sub removeSFSCalendar()
            If (Not IsNothing(Current.Session(SessionConstants.SFSCalendar))) Then
                Current.Session.Remove(SessionConstants.SFSCalendar)
            End If

        End Sub

#End Region

#End Region

#Region "Set / Get Temp Fault Apt Block List"

#Region "Set Temp Fault Apt Block List"

        Public Shared Sub setTempFaultAptBlockList(ByRef faultAptBlockList As List(Of TempFaultAppointmentBO))
            Current.Session(SessionConstants.TempFaultAptBlockList) = faultAptBlockList
        End Sub

#End Region

#Region "Get Temp Fault Apt Block List"

        Public Shared Function getTempFaultAptBlockList() As List(Of TempFaultAppointmentBO)
            If (IsNothing(Current.Session(SessionConstants.TempFaultAptBlockList))) Then
                Dim faultAptBlockList As List(Of TempFaultAppointmentBO) = New List(Of TempFaultAppointmentBO)
                Return faultAptBlockList
            Else
                Return CType(Current.Session(SessionConstants.TempFaultAptBlockList), List(Of TempFaultAppointmentBO))
            End If
        End Function

#End Region

#Region "Remove Temp Fault Apt Block List"

        Public Shared Sub removeTempFaultAptBlockList()
            If (Not IsNothing(Current.Session(SessionConstants.TempFaultAptBlockList))) Then
                Current.Session.Remove(SessionConstants.TempFaultAptBlockList)
            End If

        End Sub

#End Region

#End Region

#Region "Set / Get Temp Sb Fault Apt Block List"

#Region "Set Temp Sb Fault Apt Block List"

        Public Shared Sub setTempSbFaultAptBlockList(ByRef sbFaultAptBlockList As List(Of TempFaultAppointmentBO))
            Current.Session(SessionConstants.TempSbFaultAptBlockList) = sbFaultAptBlockList
        End Sub

#End Region

#Region "Get Temp Sb Fault Apt Block List"

        Public Shared Function getTempSbFaultAptBlockList() As List(Of TempFaultAppointmentBO)
            If (IsNothing(Current.Session(SessionConstants.TempSbFaultAptBlockList))) Then
                Dim faultAptBlockList As List(Of TempFaultAppointmentBO) = New List(Of TempFaultAppointmentBO)
                Return faultAptBlockList
            Else
                Return CType(Current.Session(SessionConstants.TempSbFaultAptBlockList), List(Of TempFaultAppointmentBO))
            End If
        End Function

#End Region

#Region "Remove Temp Sb Fault Apt Block List"

        Public Shared Sub removeTempSbFaultAptBlockList()
            If (Not IsNothing(Current.Session(SessionConstants.TempSbFaultAptBlockList))) Then
                Current.Session.Remove(SessionConstants.TempSbFaultAptBlockList)
            End If

        End Sub

#End Region

#End Region

#Region "Set / Get Confirm Fault Apt Block List"

#Region "Set Confirm Fault Apt Block List"

        Public Shared Sub setConfirmFaultAptBlockList(ByRef faultAptBlockList As List(Of ConfirmFaultAppointmentBO))
            Current.Session(SessionConstants.ConfirmFaultAptBlockList) = faultAptBlockList
        End Sub

#End Region

#Region "Get Confirm Fault Apt Block List"

        Public Shared Function getConfirmFaultAptBlockList() As List(Of ConfirmFaultAppointmentBO)
            If (IsNothing(Current.Session(SessionConstants.ConfirmFaultAptBlockList))) Then
                Dim faultAptBlockList As List(Of ConfirmFaultAppointmentBO) = New List(Of ConfirmFaultAppointmentBO)
                Return faultAptBlockList
            Else
                Return CType(Current.Session(SessionConstants.ConfirmFaultAptBlockList), List(Of ConfirmFaultAppointmentBO))
            End If
        End Function

#End Region

#Region "Remove Confirm Fault Apt Block List"

        Public Shared Sub removeConfirmFaultAptBlockList()
            If (Not IsNothing(Current.Session(SessionConstants.ConfirmFaultAptBlockList))) Then
                Current.Session.Remove(SessionConstants.ConfirmFaultAptBlockList)
            End If

        End Sub

#End Region

#End Region

#Region "Set / Get Confirm Sb Fault Apt Block List"

#Region "Set Confirm Sb Fault Apt Block List"

        Public Shared Sub setConfirmSbFaultAptBlockList(ByRef sbFaultAptBlockList As List(Of ConfirmFaultAppointmentBO))
            Current.Session(SessionConstants.ConfirmSbFaultAptBlockList) = sbFaultAptBlockList
        End Sub

#End Region

#Region "Get Confirm Sb Fault Apt Block List"

        Public Shared Function getConfirmSbFaultAptBlockList() As List(Of ConfirmFaultAppointmentBO)
            If (IsNothing(Current.Session(SessionConstants.ConfirmSbFaultAptBlockList))) Then
                Dim faultAptBlockList As List(Of ConfirmFaultAppointmentBO) = New List(Of ConfirmFaultAppointmentBO)
                Return faultAptBlockList
            Else
                Return CType(Current.Session(SessionConstants.ConfirmSbFaultAptBlockList), List(Of ConfirmFaultAppointmentBO))
            End If
        End Function

#End Region

#Region "Remove Confirm Sb Fault Apt Block List"

        Public Shared Sub removeConfirmSbFaultAptBlockList()
            If (Not IsNothing(Current.Session(SessionConstants.ConfirmSbFaultAptBlockList))) Then
                Current.Session.Remove(SessionConstants.ConfirmSbFaultAptBlockList)
            End If

        End Sub

#End Region

#End Region

#Region "Set / Get Property Distance"

#Region "Set Property Distance"

        Public Shared Sub setPropertyDistance(ByRef propertyDistance As Dictionary(Of String, PropertyDistanceBO))
            Current.Session(SessionConstants.PropertyDistance) = propertyDistance
        End Sub

#End Region

#Region "Get Property Distance"

        Public Shared Function getPropertyDistance() As Dictionary(Of String, PropertyDistanceBO)
            If (IsNothing(Current.Session(SessionConstants.PropertyDistance))) Then
                Dim propertyDistance As PropertyDistanceBO = New PropertyDistanceBO()
                Dim propertyDistanceDictionary As Dictionary(Of String, PropertyDistanceBO) = New Dictionary(Of String, PropertyDistanceBO)
                Return propertyDistanceDictionary
            Else
                Return CType(Current.Session(SessionConstants.PropertyDistance), Dictionary(Of String, PropertyDistanceBO))
            End If
        End Function

#End Region

#Region "Remove Property Distance"

        Public Shared Sub removePropertyDistance()
            If (Not IsNothing(Current.Session(SessionConstants.PropertyDistance))) Then
                Current.Session.Remove(SessionConstants.PropertyDistance)
            End If

        End Sub

#End Region

#End Region

#Region "Set / Get Patch Id"

#Region "Set Patch Id"

        Public Shared Sub setPatchId(ByVal patchId As Integer)
            Current.Session(SessionConstants.PatchId) = patchId
        End Sub

#End Region

#Region "get Patch Id"

        Public Shared Function getPatchId()
            If (IsNothing(Current.Session(SessionConstants.PatchId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionConstants.PatchId), Integer)
            End If
        End Function

#End Region

#Region "remove Patch Id"

        Public Shared Sub removePatchId()
            If (Not IsNothing(Current.Session(SessionConstants.PatchId))) Then
                Current.Session.Remove(SessionConstants.PatchId)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Sb Patch Id"

#Region "Set Sb Patch Id"

        Public Shared Sub setSbPatchId(ByVal patchId As Integer)
            Current.Session(SessionConstants.SbPatchId) = patchId
        End Sub

#End Region

#Region "get Sb Patch Id"

        Public Shared Function getSbPatchId()
            If (IsNothing(Current.Session(SessionConstants.SbPatchId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionConstants.SbPatchId), Integer)
            End If
        End Function

#End Region

#Region "remove Sb Patch Id"

        Public Shared Sub removeSbPatchId()
            If (Not IsNothing(Current.Session(SessionConstants.SbPatchId))) Then
                Current.Session.Remove(SessionConstants.SbPatchId)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Trade Id"

#Region "Set Trade Id"

        Public Shared Sub setTradeId(ByVal tradeId As Integer)
            Current.Session(SessionConstants.TradeId) = tradeId
        End Sub

#End Region

#Region "get Trade Id"

        Public Shared Function getTradeId()
            If (IsNothing(Current.Session(SessionConstants.TradeId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionConstants.TradeId), Integer)
            End If
        End Function

#End Region

#Region "remove Trade Id"

        Public Shared Sub removeTradeId()
            If (Not IsNothing(Current.Session(SessionConstants.TradeId))) Then
                Current.Session.Remove(SessionConstants.TradeId)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Sb Trade Id"

#Region "Set Sb Trade Id"

        Public Shared Sub setSbTradeId(ByVal sbTradeId As Integer)
            Current.Session(SessionConstants.SbTradeId) = sbTradeId
        End Sub

#End Region

#Region "get Sb Trade Id"

        Public Shared Function getSbTradeId()
            If (IsNothing(Current.Session(SessionConstants.SbTradeId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionConstants.SbTradeId), Integer)
            End If
        End Function

#End Region

#Region "remove Sb Trade Id"

        Public Shared Sub removeSbTradeId()
            If (Not IsNothing(Current.Session(SessionConstants.SbTradeId))) Then
                Current.Session.Remove(SessionConstants.SbTradeId)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get IsFollowOn"

#Region "Set isFollowOn"

        Public Shared Sub setIsFollowOn(ByVal isFollowOn As Boolean)
            Current.Session(SessionConstants.IsFollowOn) = isFollowOn
        End Sub

#End Region

#Region "Get isFollowOn"

        Public Shared Function getIsFollowOn() As Boolean
            Dim isFollowOn As Boolean = False
            If Not (IsNothing(Current.Session(SessionConstants.IsFollowOn))) Then
                isFollowOn = CType(Current.Session(SessionConstants.IsFollowOn), Boolean)
            End If
            Return isFollowOn
        End Function

#End Region

#Region "Remove IsFollowON"

        Public Shared Sub removeIsFollowOn()
            Current.Session.Remove(SessionConstants.IsFollowOn)
        End Sub

#End Region

#End Region

#Region "Set / Get IsSchmeBlockFollowOn"

#Region "Set isFollowOn"

        Public Shared Sub setIsSchmeBlockFollowOn(ByVal isSbFollowOn As Boolean)
            Current.Session(SessionConstants.IsSbFollowOn) = isSbFollowOn
        End Sub

#End Region

#Region "Get isSbFollowOn"

        Public Shared Function getIsSbFollowOn() As Boolean
            Dim isSbFollowOn As Boolean = False
            If Not (IsNothing(Current.Session(SessionConstants.IsSbFollowOn))) Then
                isSbFollowOn = CType(Current.Session(SessionConstants.IsSbFollowOn), Boolean)
            End If
            Return isSbFollowOn
        End Function

#End Region

#Region "Remove IsFollowON"

        Public Shared Sub removeIsSbFollowOn()
            Current.Session.Remove(SessionConstants.IsSbFollowOn)
        End Sub

#End Region

#End Region

#Region "Set / Get FollowOnFaultLogId"

#Region "Set FollowOnFaultLogId"

        Public Shared Sub setFollowOnFaultLogId(ByVal followOnFaultLogId As Integer)
            Current.Session(SessionConstants.FollowOnFaultLogId) = followOnFaultLogId
        End Sub

#End Region

#Region "Get FollowOnFaultLogId"

        Public Shared Function getFollowOnFaultLogId() As Integer
            Dim followOnFaultLogId As Integer = 0
            If Not (IsNothing(Current.Session(SessionConstants.FollowOnFaultLogId))) Then
                followOnFaultLogId = CType(Current.Session(SessionConstants.FollowOnFaultLogId), Integer)
            End If
            Return followOnFaultLogId
        End Function

#End Region

#Region "Remove FollowOnJSN"

        Public Shared Sub removeFollowOnFaultLogId()
            Current.Session.Remove(SessionConstants.FollowOnFaultLogId)
        End Sub

#End Region

#End Region

#Region "Set / Get Sb FollowOnFaultLogId"

#Region "Set SbFollowOnFaultLogId"

        Public Shared Sub setSbFollowOnFaultLogIdlockFollowOnFaultLogId(ByVal SbFollowOnFaultLogId As Integer)
            Current.Session(SessionConstants.SbFollowOnFaultLogId) = SbFollowOnFaultLogId
        End Sub

#End Region

#Region "Get SbFollowOnFaultLogId"

        Public Shared Function getSbFollowOnFaultLogId() As Integer
            Dim SbFollowOnFaultLogId As Integer = 0
            If Not (IsNothing(Current.Session(SessionConstants.SbFollowOnFaultLogId))) Then
                SbFollowOnFaultLogId = CType(Current.Session(SessionConstants.SbFollowOnFaultLogId), Integer)
            End If
            Return SbFollowOnFaultLogId
        End Function

#End Region

#Region "Remove SbFollowOnFaultLogId"

        Public Shared Sub removeSbFollowOnFaultLogId()
            Current.Session.Remove(SessionConstants.SbFollowOnFaultLogId)
        End Sub

#End Region

#End Region

#Region "Set / Get IsNoEntry"

#Region "Set IsNoEntry"

        Public Shared Sub setIsNoEntry(ByVal isNoEntry As Boolean)
            Current.Session(SessionConstants.IsNoEntry) = isNoEntry
        End Sub

#End Region

#Region "Get IsNoEntry"

        Public Shared Function getIsNoEntry() As Boolean
            Dim isFollowOn As Boolean = False
            If Not (IsNothing(Current.Session(SessionConstants.IsNoEntry))) Then
                isFollowOn = CType(Current.Session(SessionConstants.IsNoEntry), Boolean)
            End If
            Return isFollowOn
        End Function

#End Region

#Region "Remove IsNoEntry"

        Public Shared Sub removeIsNoEntry()
            Current.Session.Remove(SessionConstants.IsNoEntry)
        End Sub

#End Region

#End Region

#Region "Set / Get NoEntryFaultLogId"

#Region "Set NoEntryFaultLogId"

        Public Shared Sub setNoEntryFaultLogId(ByVal NoEntryFaultLogId As Integer)
            Current.Session(SessionConstants.NoEntryFaultLogId) = NoEntryFaultLogId
        End Sub

#End Region

#Region "Get NoEntryFaultLogId"

        Public Shared Function getNoEntryFaultLogId() As Integer
            Dim NoEntryFaultLogId As Integer = 0
            If Not (IsNothing(Current.Session(SessionConstants.NoEntryFaultLogId))) Then
                NoEntryFaultLogId = CType(Current.Session(SessionConstants.NoEntryFaultLogId), Integer)
            End If
            Return NoEntryFaultLogId
        End Function

#End Region

#Region "Remove NoEntryJSN"

        Public Shared Sub removeNoEntryFaultLogId()
            Current.Session.Remove(SessionConstants.NoEntryFaultLogId)
        End Sub

#End Region

#End Region

#Region "Repairs Data Table, Set and Remove Method/Function(s)"

#Region "Set Repairs Data Table"
        ''' <summary>
        ''' This function shall be used to save Trades Data Table in session
        ''' </summary>
        ''' <param name="RepairsDataSet"></param>
        ''' <remarks></remarks>
        Public Shared Sub setRepairsDataTable(ByRef RepairsDataSet As DataTable)
            Current.Session(SessionConstants.RepairDataTable) = RepairsDataSet
        End Sub

#End Region

#Region "Get Repairs Data Table"
        ''' <summary>
        ''' This function shall be used to get Repairs Data Table from session.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getRepairsDataTable() As DataTable
            If (IsNothing(Current.Session(SessionConstants.RepairDataTable))) Then
                Dim RepairsList As New DataTable
                Return RepairsList
            Else
                Return CType(Current.Session(SessionConstants.RepairDataTable), DataTable)
            End If
        End Function

#End Region

#Region "remove Repairs Data Table"
        ''' <summary>
        ''' This function shall be used to remove saved Repairs Data Table from session.
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeRepairsDataTable()
            If (Not IsNothing(Current.Session(SessionConstants.RepairDataTable))) Then
                Current.Session.Remove(SessionConstants.RepairDataTable)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get  Operative  working Hour Ds"

#Region "Set Operative working Hour Ds"

        Public Shared Sub setOperativeworkingHourDs(ByRef OperativeworkingHour As DataSet)
            Current.Session(SessionConstants.OperativeworkingHour) = OperativeworkingHour
        End Sub

#End Region

#Region "Get Operative working Hour Ds"

        Public Shared Function getOperativeworkingHourDs() As DataSet
            If (IsNothing(Current.Session(SessionConstants.OperativeworkingHour))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.OperativeworkingHour), DataSet)
            End If
        End Function

#End Region

#Region "remove Operative working Hour Ds"

        Public Shared Sub removeOperativeworkingHourDs()
            If (Not IsNothing(Current.Session(SessionConstants.OperativeworkingHour))) Then
                Current.Session.Remove(SessionConstants.OperativeworkingHour)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Page Access DataSet"
        ''' <summary>
        ''' This key will be used to save the Page Access DataSet
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared PageAccessDataSet As String = "PageAccessDataSet"

#Region "Set Page Access DataSet"
        ''' <summary>
        ''' This function shall be used Page Access DataSet in session
        ''' </summary>
        ''' <param name="ds"></param>
        ''' <remarks></remarks>
        Public Shared Sub setPageAccessDataSet(ByRef ds As DataSet)
            Current.Session(PageAccessDataSet) = ds
        End Sub

#End Region

#Region "Get Page Access DataSet"
        ''' <summary>
        ''' This function shall be used to get Page Access DataSet from session.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getPageAccessDataSet()
            If (IsNothing(Current.Session(PageAccessDataSet))) Then
                Return Nothing
            Else
                Return CType(Current.Session(PageAccessDataSet), DataSet)
            End If
        End Function

#End Region

#Region "Remove Page Access DataSet"
        ''' <summary>
        ''' This function shall be used to remove saved Core And Out Of Hours Detail dataset from session.
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removePageAccessDataSet()
            If (Not IsNothing(Current.Session(PageAccessDataSet))) Then
                Current.Session.Remove(PageAccessDataSet)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get ExistingAppointments For Scheduling Maps"

#Region "Get ExistingAppointments For Scheduling Map"
        Public Shared Function getExistingAppointmentsForAvailableAppointmentsMap() As DataTable
            Return CType(Current.Session(SessionConstants.existingAppointmentForAvailableAppoinetmentsMap), DataTable)
        End Function
#End Region

#Region "Set ExistingAppointments For Scheduling Maps"
        Public Shared Sub setExistingAppointmentsForAvailableAppointmentsMap(dataTable As DataTable)
            Current.Session(SessionConstants.existingAppointmentForAvailableAppoinetmentsMap) = dataTable
        End Sub
#End Region

#Region "Remove ExistingAppointments For Scheduling Map(s)"
        Public Shared Sub removeExistingAppointmentsForAvailableAppointmentsMap()
            Current.Session.Remove(SessionConstants.existingAppointmentForAvailableAppoinetmentsMap)
        End Sub
#End Region

#End Region

#Region "Set / Get Avaialble Operative Ds"

#Region "Set  Avaialble Operative Ds"

        Public Shared Sub setAvailableOperativesDsForRearrange(ByRef availableOperativesDs As DataSet)
            Current.Session(SessionConstants.AvailableOperativesDsForRearrange) = availableOperativesDs
        End Sub

#End Region

#Region "Get  Avaialble Operative Ds"

        Public Shared Function getAvailableOperativesDsForRearrange()
            If (IsNothing(Current.Session(SessionConstants.AvailableOperativesDsForRearrange))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.AvailableOperativesDsForRearrange), DataSet)
            End If
        End Function

#End Region

#Region "remove  Avaialble Operative Ds"

        Public Shared Sub removeAvailableOperativeDsForRearrange()
            If (Not IsNothing(Current.Session(SessionConstants.AvailableOperativesDsForRearrange))) Then
                Current.Session.Remove(SessionConstants.AvailableOperativesDsForRearrange)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get ExistingAppointments For Scheduling Maps"

#Region "Get ExistingAppointments For Scheduling Map"
        Public Shared Function getExistingAppointmentsForRearranggeAppointmentsMap() As DataTable
            Return CType(Current.Session(SessionConstants.existingAppointmentForRearrangeAppoinetmentsMap), DataTable)
        End Function
#End Region

#Region "Set ExistingAppointments For Scheduling Maps"
        Public Shared Sub setExistingAppointmentsForRearranggeAppointmentsMap(dataTable As DataTable)
            Current.Session(SessionConstants.existingAppointmentForRearrangeAppoinetmentsMap) = dataTable
        End Sub
#End Region

#Region "Remove ExistingAppointments For Scheduling Map(s)"
        Public Shared Sub removeExistingAppointmentsForRearranggeAppointmentsMap()
            Current.Session.Remove(SessionConstants.existingAppointmentForRearrangeAppoinetmentsMap)
        End Sub
#End Region

#End Region

#Region "Set / Get SbExistingAppointments For Scheduling Maps"

#Region "Get SbExistingAppointments For Scheduling Map"
        Public Shared Function getSbExistingAppointmentsForAvailableAppointmentsMap() As DataTable
            Return CType(Current.Session(SessionConstants.SbExistingAppointmentForAvailableAppoinetmentsMap), DataTable)
        End Function
#End Region

#Region "Set SbExistingAppointments For Scheduling Maps"
        Public Shared Sub setSbExistingAppointmentsForAvailableAppointmentsMap(ByVal dataTable As DataTable)
            Current.Session(SessionConstants.SbExistingAppointmentForAvailableAppoinetmentsMap) = dataTable
        End Sub
#End Region

#Region "Remove SbExistingAppointments For Scheduling Map(s)"
        Public Shared Sub removeSbExistingAppointmentsForAvailableAppointmentsMap()
            Current.Session.Remove(SessionConstants.SbExistingAppointmentForAvailableAppoinetmentsMap)
        End Sub
#End Region

#End Region

#Region "Set / Get Sb Temporary Fault Bo"

#Region "Set Temporary Fault BO"

        Public Shared Sub setSbTemporaryFaultBo(ByRef arrSelectedTempFaultBO As ArrayList)
            Current.Session(SessionConstants.SbTemporaryFaultBo) = arrSelectedTempFaultBO
        End Sub

#End Region

#Region "Get Temporary Fault bo"

        Public Shared Function getSbTemporaryFaultBo()
            If (IsNothing(Current.Session(SessionConstants.SbTemporaryFaultBo))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.SbTemporaryFaultBo), ArrayList)
            End If
        End Function

#End Region

#Region "remove Temporary Fault bo"

        Public Shared Sub removeSbTemporaryFaultBo()
            If (Not IsNothing(Current.Session(SessionConstants.SbTemporaryFaultBo))) Then
                Current.Session.Remove(SessionConstants.SbTemporaryFaultBo)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Common Address BO Data"

#Region "Set Common Address BO Data"

        Public Shared Sub setCommonAddressBO(ByRef customerData As CommonAddressBO)
            Current.Session(SessionConstants.CommonAddress) = customerData
        End Sub

#End Region

#Region "get Common Address BO Data"

        Public Shared Function getCommonAddressBO()
            If (IsNothing(Current.Session(SessionConstants.CommonAddress))) Then
                Dim customerData As New CommonAddressBO
                Return customerData
            Else
                Return CType(Current.Session(SessionConstants.CommonAddress), CommonAddressBO)
            End If
        End Function

#End Region

#Region "remove Common Address BO Data"

        Public Shared Sub removeCommonAddress()
            If (Not IsNothing(Current.Session(SessionConstants.CommonAddress))) Then
                Current.Session.Remove(SessionConstants.CommonAddress)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Avaialble Operative Ds for Scheme Block"

#Region "Set  Avaialble Operative Ds for Scheme Block"

        Public Shared Sub setSbAvailableOperativesDs(ByRef availableOperativesDs As DataSet)
            Current.Session(SessionConstants.SbAvailableOperativesDs) = availableOperativesDs
        End Sub

#End Region

#Region "Get  Avaialble Operative Ds for Scheme Block"

        Public Shared Function getSbAvailableOperativesDs()
            If (IsNothing(Current.Session(SessionConstants.SbAvailableOperativesDs))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.SbAvailableOperativesDs), DataSet)
            End If
        End Function

#End Region

#Region "remove  Avaialble Operative Ds for Scheme Block"

        Public Shared Sub removeSbAvailableOperativeDs()
            If (Not IsNothing(Current.Session(SessionConstants.SbAvailableOperativesDs))) Then
                Current.Session.Remove(SessionConstants.SbAvailableOperativesDs)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Sb Jsn List"

#Region "Set Sb Jsn List"

        Public Shared Sub setSbJsnId(ByRef JsnIds As String)
            Current.Session(SessionConstants.SbJsn) = JsnIds
        End Sub

#End Region

#Region "Get Sb Jsn"

        Public Shared Function getSbJsnId()
            If (IsNothing(Current.Session(SessionConstants.SbJsn))) Then
                Return String.Empty
            Else
                Return CType(Current.Session(SessionConstants.SbJsn), String)
            End If
        End Function

#End Region

#Region "Remove Sb Jsn"

        Public Shared Sub removeSbJsnId()
            If (Not IsNothing(Current.Session(SessionConstants.SbJsn))) Then
                Current.Session.Remove(SessionConstants.SbJsn)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Sb Hashtable"

#Region "Set Sb Hashtable"
        Public Shared Sub setSbHashTable(ByRef hashTable As System.Collections.Hashtable)
            Current.Session(SessionConstants.SbHashTable) = hashTable
        End Sub
#End Region

#Region "Get Sb Hashtable"
        Public Shared Function getSbHashTable()
            If (IsNothing(Current.Session(SessionConstants.SbHashTable))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.SbHashTable), System.Collections.Hashtable)
            End If
        End Function
#End Region

#Region "remove Sb Hashtable"
        Public Shared Sub removeSbHashTable()
            If (Not IsNothing(Current.Session(SessionConstants.SbHashTable))) Then
                Current.Session.Remove(SessionConstants.SbHashTable)
            End If
        End Sub
#End Region

#End Region

#Region "Set / Get Appointment Data For Sb Appointment Summary"

#Region "Set Appointment Data For Sb Appointment Summary"

        Public Shared Sub setAptDataForSbAptSummary(ByRef appointmentData As AppointmentBO)
            Current.Session(SessionConstants.SbAppointmentData) = appointmentData
        End Sub

#End Region

#Region "get Appointment Data For Sb Appointment Summary"

        Public Shared Function getAptDataForSbAptSummary()
            If (IsNothing(Current.Session(SessionConstants.SbAppointmentData))) Then
                Dim appointmentData As New AppointmentBO
                Return appointmentData
            Else
                Return CType(Current.Session(SessionConstants.SbAppointmentData), AppointmentBO)
            End If
        End Function

#End Region

#Region "remove Appointment Data For Sb Appointment Summary"

        Public Shared Sub removeAptDataForSbAptSummary()
            If (Not IsNothing(Current.Session(SessionConstants.SbAppointmentData))) Then
                Current.Session.Remove(SessionConstants.SbAppointmentData)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Sb Selected Temporary Faults Dv"

#Region "Set Selected Temp Faults Dv"

        Public Shared Sub setSbSelectedTempFaultDv(ByRef selectedTempFaultDv As DataView)
            Current.Session(SessionConstants.SbSelectedTempFaultDv) = selectedTempFaultDv
        End Sub

#End Region

#Region "Get Selected Temp Faults Dv"

        Public Shared Function getSbSelectedTempFaultDv()
            If (IsNothing(Current.Session(SessionConstants.SbSelectedTempFaultDv))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.SbSelectedTempFaultDv), DataView)
            End If
        End Function

#End Region

#Region "remove Selected Temp Faults Dv"

        Public Shared Sub removeSbSelectedTempFaultDv()
            If (Not IsNothing(Current.Session(SessionConstants.SbSelectedTempFaultDv))) Then
                Current.Session.Remove(SessionConstants.SbSelectedTempFaultDv)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Sb Confirmed Appointment Id List"

#Region "Set Confirmed Appointment Id List"

        Public Shared Sub setSbConfirmedAptIdList(ByRef confirmedAppointmentIdList As List(Of AppointmentBO))
            Current.Session(SessionConstants.SbConfirmedAptIdList) = confirmedAppointmentIdList
        End Sub

#End Region

#Region "Set Confirmed Appointment Id List"

        Public Shared Function getSbConfirmedAptIdList()
            If (IsNothing(Current.Session(SessionConstants.SbConfirmedAptIdList))) Then
                Return Nothing
            Else
                Return CType(Current.Session(SessionConstants.SbConfirmedAptIdList), List(Of AppointmentBO))
            End If
        End Function

#End Region

#Region "remove Confirmed Appointment Id List"

        Public Shared Sub removeSbConfirmedAptIdList()
            If (Not IsNothing(Current.Session(SessionConstants.SbConfirmedAptIdList))) Then
                Current.Session.Remove(SessionConstants.SbConfirmedAptIdList)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Initial DataTable"
        ''' <summary>
        ''' 
        ''' </summary>
        ''' <remarks></remarks>
        Public Const InitialDataTable As String = "InitialDataTable"
#Region "Set SelectedRowIndex"
        ''' <summary>
        ''' set the InitialDataTable
        ''' </summary>
        ''' <param name="InitialDataTable"></param>
        ''' <remarks></remarks>
        Public Shared Sub setInitialDataTable(ByRef InitialDataTable As DataTable)
            Current.Session(SessionManager.InitialDataTable) = InitialDataTable
        End Sub
#End Region

#Region "get Initial DataTable"
        ''' <summary>
        ''' get the InitialDataTable
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getInitialDataTable() As DataTable
            If (IsNothing(Current.Session(SessionManager.InitialDataTable))) Then
                Return New DataTable
            Else
                Return CType(Current.Session(SessionManager.InitialDataTable), DataTable)
            End If
        End Function
#End Region

#Region "remove SelectedRowIndex"
        ''' <summary>
        ''' remmove the InitialDataTable from Session
        ''' </summary>
        ''' <remarks></remarks>
        Public Shared Sub removeInitialDataTable()
            If (Not IsNothing(Current.Session(SessionManager.InitialDataTable))) Then
                Current.Session.Remove(SessionManager.InitialDataTable)
            End If

        End Sub
#End Region

#End Region

#Region "Set / Get  Expenditure BO List"

        ''' <summary>
        ''' This key is to manage List of Expenditure BOs in Session.
        ''' </summary>
        ''' <remarks></remarks>
        Public Const ExpenditureBOList As String = "ExpenditureBOList"
        ''' <summary>
        ''' To get Expenditure BO list, this is saved in view state to get LIMIT and Remaining LIMIT of the current employee.
        ''' That will be used to set a purchase/work required item in the queue for authorization.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getExpenditureBOList() As List(Of ExpenditureBO)
            Dim expenditureBOLst As List(Of ExpenditureBO) = TryCast(Current.Session(ExpenditureBOList), List(Of ExpenditureBO))
            If IsNothing(expenditureBOLst) Then
                expenditureBOLst = New List(Of ExpenditureBO)
            End If
            Return expenditureBOLst
        End Function

        Public Shared Sub removeExpenditureBOList()
            Current.Session.Remove(ExpenditureBOList)
        End Sub

        Public Shared Sub setExpenditureBOList(ByVal expenditureBOLst As List(Of ExpenditureBO))
            Current.Session(ExpenditureBOList) = expenditureBOLst
        End Sub

#End Region

#Region " Set / Get Vat BO List"

        ''' <summary>
        ''' This key is to manage List of Vat BOs in Session.
        ''' </summary>
        ''' <remarks></remarks>
        Public Const ContractorVatBoList As String = "ContractorVatBoList"

        ''' <summary>
        ''' To get Vat BO list, this is saved in view state to get Vat rate for the current vat item.
        ''' That will be used to set a purchase/work required item in the queue for authorization.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function getVatBOList() As List(Of ContractorVatBO)
            Dim VatBoLst As List(Of ContractorVatBO) = TryCast(Current.Session(ContractorVatBoList), List(Of ContractorVatBO))
            If IsNothing(VatBoLst) Then
                VatBoLst = New List(Of ContractorVatBO)
            End If
            Return VatBoLst
        End Function

        Public Shared Sub removeVatBOList()
            Current.Session.Remove(ContractorVatBoList)
        End Sub

        Public Shared Sub setVatBOList(ByRef VatBoLst As List(Of ContractorVatBO))
            Current.Session(ContractorVatBoList) = VatBoLst
        End Sub

#End Region

#Region "Set / Get Sb Subcontractor Current Fault BO  "

#Region "Set Sb JobSubcontractor Current Fault BO"

        Public Shared Sub setSbSubcontractorCurrentFaultBO(ByRef currentFaultBO As FaultBO)
            Current.Session(SessionConstants.SbSubcontractorCurrentFaultBO) = currentFaultBO
        End Sub

#End Region

#Region "get Subcontractor Current Fault BO"

        Public Shared Function getSbSubcontractorCurrentFaultBO()
            If (IsNothing(Current.Session(SessionConstants.SbSubcontractorCurrentFaultBO))) Then
                Return False
            Else
                Return CType(Current.Session(SessionConstants.SbSubcontractorCurrentFaultBO), FaultBO)
            End If
        End Function

#End Region

#Region "remove Sb Subcontractor Current Fault BO"

        Public Shared Sub removeSbSubcontractorCurrentFaultBO()
            If (Not IsNothing(Current.Session(SessionConstants.SbSubcontractorCurrentFaultBO))) Then
                Current.Session.Remove(SessionConstants.SbSubcontractorCurrentFaultBO)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get tradeType"

#Region "Set tradeType"

        Public Shared Sub setTradeType(ByVal TradeType As String)
            Current.Session(SessionConstants.TradeType) = TradeType
        End Sub

#End Region

#Region "Get TradeType"

        Public Shared Function getTradeType() As String
            Dim TradeType As String = String.Empty
            If Not (IsNothing(Current.Session(SessionConstants.TradeType))) Then
                TradeType = CType(Current.Session(SessionConstants.TradeType), String)
            End If
            Return TradeType
        End Function

#End Region

#Region "Remove TradeType"

        Public Shared Sub removeTradeType()
            Current.Session.Remove(SessionConstants.TradeType)
        End Sub

#End Region

#End Region

#Region "Set / Get Assign To Contractor BO Data"

#Region "Set  Assign To Contractor BO Data"

        Public Shared Sub setAssignToContractorBO(ByRef contractorBO As AssignToContractorBO)
            Current.Session(SessionConstants.AssignToContractorBo) = contractorBO
        End Sub

#End Region

#Region "get  Assign To Contractor BO Data"

        Public Shared Function getAssignToContractorBO()
            If (IsNothing(Current.Session(SessionConstants.AssignToContractorBo))) Then
                Dim contractorBO As New AssignToContractorBO
                Return contractorBO
            Else
                Return CType(Current.Session(SessionConstants.AssignToContractorBo), AssignToContractorBO)
            End If
        End Function

#End Region

#Region "remove Assign To Contractor BO Data"

        Public Shared Sub removeAssignToContractorBO()
            If (Not IsNothing(Current.Session(SessionConstants.AssignToContractorBo))) Then
                Current.Session.Remove(SessionConstants.AssignToContractorBo)
            End If
        End Sub

#End Region

#End Region

#Region "Set / Get Property Calendar Filters Values"

#Region "Set Property Calendar Filters Values"

        Public Shared Sub setPropertyCalendarFilters(ByRef propertyCalendarFilters As String)
            Current.Session(SessionConstants.PropertyCalendarFilters) = propertyCalendarFilters
        End Sub

#End Region

#Region "get Property Calendar Filters Values"

        Public Shared Function getPropertyCalendarFilters() As String
            If (IsNothing(Current.Session(SessionConstants.PropertyCalendarFilters))) Then
                Return "-1,-1,-1,-1,,,"
            Else
                Return CType(Current.Session(SessionConstants.PropertyCalendarFilters), String)
            End If
        End Function

#End Region

#Region "remove Property Calendar Filters Values"

        Public Shared Sub removePropertyCalendarFilters()
            If (Not IsNothing(Current.Session(SessionConstants.PropertyCalendarFilters))) Then
                Current.Session.Remove(SessionConstants.PropertyCalendarFilters)
            End If
        End Sub

#End Region

#End Region


#Region "Set / Get FaultRepairListId Values"

#Region "Set FaultRepairListId Values"
        Public Shared Sub setFaultRepairListId(ByRef FaultRepairListId As Integer)
            Current.Session(SessionConstants.FaultRepairListId) = FaultRepairListId
        End Sub
#End Region

#Region "get FaultRepairListId Values"
        Public Shared Function getFaultRepairListId() As Integer
            If (IsNothing(Current.Session(SessionConstants.FaultRepairListId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionConstants.FaultRepairListId), Integer)
            End If
        End Function
#End Region

#Region "remove FaultRepairListId Values"

        Public Shared Sub removeFaultRepairListId()
            If (Not IsNothing(Current.Session(SessionConstants.FaultRepairListId))) Then
                Current.Session.Remove(SessionConstants.FaultRepairListId)
            End If
        End Sub

#End Region
#End Region





#Region "Set / Get FollowOnWorkId Values"

#Region "Set FollowOnWorkId Values"
        Public Shared Sub setFollowOnWorkId(ByRef FollowOnWorkId As Integer)
            Current.Session(SessionConstants.FollowOnWorkId) = FollowOnWorkId
        End Sub
#End Region

#Region "get FollowOnWorkId Values"
        Public Shared Function getFollowOnWorkId() As Integer
            If (IsNothing(Current.Session(SessionConstants.FollowOnWorkId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionConstants.FollowOnWorkId), Integer)
            End If
        End Function
#End Region

#Region "remove FollowOnWorkId Values"

        Public Shared Sub removeFollowOnWorkId()
            If (Not IsNothing(Current.Session(SessionConstants.FollowOnWorkId))) Then
                Current.Session.Remove(SessionConstants.FollowOnWorkId)
            End If
        End Sub

#End Region
#End Region




#Region "Set / Get ExpenditureId Values"

#Region "Set ExpenditureId Values"
        Public Shared Sub setExpenditureId(ByRef ExpenditureId As Integer)
            Current.Session(SessionConstants.ExpenditureId) = ExpenditureId
        End Sub
#End Region

#Region "get ExpenditureId Values"
        Public Shared Function getExpenditureId() As Integer
            If (IsNothing(Current.Session(SessionConstants.ExpenditureId))) Then
                Return 0
            Else
                Return CType(Current.Session(SessionConstants.ExpenditureId), Integer)
            End If
        End Function
#End Region

#Region "remove ExpenditureId Values"

        Public Shared Sub removeExpenditureId()
            If (Not IsNothing(Current.Session(SessionConstants.ExpenditureId))) Then
                Current.Session.Remove(SessionConstants.ExpenditureId)
            End If
        End Sub

#End Region
#End Region




    End Class

End Namespace

