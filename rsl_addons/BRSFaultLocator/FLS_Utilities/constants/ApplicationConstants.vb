﻿Imports System
Imports System.Text
Imports System.Configuration
Namespace FLS_Utilities
    Public Class ApplicationConstants

#Region "General"
        Public Shared DropDownDefaultValue As String = "-1"
        Public Shared DefaultValue As Int32 = -1
        Public Shared Title As String = "Title"
        Public Shared LetterPrefix As String = "SL_"
        Public Shared DocPrefix As String = "Doc_"
        Public Shared LetterWord As String = "Letter"
        Public Shared DocumentWord As String = "Document"
        Public Shared DefaultDropDownDataValueField As String = "id"
        Public Shared DefaultDropDownDataTextField As String = "title"
        Public Shared Create As String = "Create"
        Public Shared NumberSymbol As String = "#"
        Public Shared AppointmentStartTime As String = " 8:00"
        Public Shared AppointmentStartHour As Double = 8
        Public Shared AppointmentEndHour As Double = 17.1
        Public Shared CancelledStatus As String = "Cancelled"
        Public Shared AssignedtoContractor As String = "Assigned to Contractor"

        'Appointment types are used for push notification
        ' "NewAppointmentType" means new appointment is going to be created
        ' "RearrangeAppointmentType" means push notification is for appointment rearrange
        Public Shared NewAppointmentType As Integer = 1
        Public Shared CancelAppointmentType As Integer = 2
        Public Shared RearrangeAppointmentType As Integer = 3

#End Region

#Region "Master Layout"

        Public Shared ConWebBackgroundColor As String = "#e6e6e6"
        Public Shared ConWebBackgroundProperty As String = "background-color"

        Public Shared MenuCol As String = "Menu"
        Public Shared UrlCol As String = "Url"
        Public Shared MenuIdCol As String = "MenuId"
        Public Shared PageFileNameCol As String = "PageFileName"
        Public Shared NoPageSelected As Integer = -1
        Public Shared PageFirstLevel As Integer = 1

        Public Shared ServicingMenu As String = "Servicing"
        Public Shared ResourcesMenu As String = "Resources"
        Public Shared PropertyModuleMenu As String = "Search"
        Public Shared RepairsMenu As String = "Repairs"
        Public Shared MultipleMenus As String = ""

        Public Shared AccessGrantedModulesDt As String = "AccessGrantedModulesDt"
        Public Shared AccessGrantedMenusDt As String = "AccessGrantedMenusDt"
        Public Shared AccessGrantedPagesDt As String = "AccessGrantedPagesDt"
        Public Shared RandomPageDt As String = "RandomPageDt"

        Public Shared GrantModulesModuleIdCol As String = "MODULEID"

        Public Shared GrantMenuModuleIdCol As String = "ModuleId"
        Public Shared GrantMenuMenuIdCol As String = "MenuId"
        Public Shared GrantMenuUrlCol As String = "Url"
        Public Shared GrantMenuDescriptionCol As String = "Menu"

        Public Shared GrantPageParentPageCol As String = "ParentPage"
        Public Shared GrantPageAccessLevelCol As String = "AccessLevel"
        Public Shared GrantPageQueryStringCol As String = "PageQueryString"
        Public Shared GrantPageFileNameCol As String = "PageFileName"
        Public Shared GrantPageUrlCol As String = "PageUrl"
        Public Shared GrantPageCoreUrlCol As String = "PageCoreUrl"
        Public Shared GrantPagePageNameCol As String = "PageName"
        Public Shared GrantPageIdCol As String = "PageId"
        Public Shared GrantPageMenuIdCol As String = "MenuId"
        Public Shared GrantPageModuleIdCol As String = "ModuleId"
        Public Shared GrantOrderTextCol As String = "ORDERTEXT"

        Public Shared RandomParentPageCol As String = "ParentPage"
        Public Shared RandomAccessLevelCol As String = "AccessLevel"
        Public Shared RandomQueryStringCol As String = "PageQueryString"
        Public Shared RandomFileNameCol As String = "PageFileName"
        Public Shared RandomPageNameCol As String = "PageName"
        Public Shared RandomPageIdCol As String = "PageId"

#End Region

#Region "Schedule Calendar"
        Public Shared daysArray() As String = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"}
        Public Shared appointmentTime() As String = {"00:00", "00:15", "00:30", "00:45", "01:00", "01:15", "01:30", "01:45", "02:00", "02:15", "02:30", "02:45", "03:00", "03:15", "03:30", "03:45", "04:00", "04:15", "04:30", "04:45", "05:00", "05:15", "05:30", "05:45", "06:00", "06:15", "06:30", "06:45", "07:00", "07:15", "07:30", "07:45", "08:00", "08:15", "08:30", "08:45", "09:00", "09:15", "09:30", "09:45", "10:00", "10:15", "10:30", "10:45", "11:00", "11:15", "11:30", "11:45", "12:00", "12:15", "12:30", "12:45", "13:00", "13:15", "13:30", "13:45", "14:00", "14:15", "14:30", "14:45", "15:00", "15:15", "15:30", "15:45", "16:00", "16:15", "16:30", "16:45", "17:00", "17:15", "17:30", "17:45", "18:00", "18:15", "18:30", "18:45", "19:00", "19:15", "19:30", "19:45", "20:00", "20:15", "20:30", "20:45", "21:00", "21:15", "21:30", "21:45", "22:00", "22:15", "22:30", "22:45", "23:00", "23:15", "23:30", "23:45"}
        Public Shared startDate As Date = Date.Today
        Public Shared endDate As Date = Date.Today
        Public Shared weekDays As Integer
        Public Shared isSuccessAdded As Boolean = False
        Public Shared isMoved As Boolean = False
        Public Shared isPopupOpened As Boolean = False
        Public Shared calendarAppointment As String = "calendarAppointment"
        Public Shared faultJSN As String = "faultJSN"


        'Appointment
        Public Shared AppointmentTypeColumn As String = "AppointmentType"
        Public Shared PlannedAppointment As String = "Planned"
        Public Shared FaultAppointment As String = "Fault"
        Public Shared MiscAppointment As String = "Misc"
        Public Shared ConditionAppointment As String = "Condition"
        Public Shared AdaptationAppointment As String = "Adaptation"
        Public Shared SbFaultAppointment As String = "SbFault"
        Public Shared CyclicMaintenanceAppointment As String = "Cyclic Maintenance"
        Public Shared MEServicingAppointment As String = "M&E Servicing"
        Public Shared GasAppointment As String = "GAS"

        Public Shared GasAppointmentForSchemeBlock As String = "GASSB"

        Public Shared DefectAppointment As String = "Appliance Defect"
#End Region

#Region "Page Sort"

        Public Shared Ascending As String = "ASC"
        Public Shared Descending As String = "DESC"
        Public Shared DefaultPageNumber As String = 1
        Public Shared DefaultPageSize As String = 5
        Public Shared PageSortBo As String = "PageSortBo"
#End Region

#Region "Page Names"
        'The value of these should be same as those are in database table AS_Pages
        Public Shared DashboardPage As String = "Dashboard"
        Public Shared ResourcesPage As String = "Resources"
        Public Shared SchedulingPage As String = "Scheduling"
        Public Shared UsersPage As String = "Users"
        Public Shared StatusPage As String = "Status"
        Public Shared LetterPage As String = "Letter"
        Public Shared AccessPage As String = "Access"
        Public Shared CalendarPage As String = "Calendar"
        Public Shared PropertyModulePage As String = "Property Module"
#End Region

#Region "Basket Selected Fault Ids"
        Public Shared faultIdsList As New ArrayList
        Public Shared TempFaultIds As New Int32
        Public Const ContactEmailDetailsDt As String = "ContactEmailDetails"

#End Region

#Region "Property distance "

        Public Shared propertyDistance As New DataTable
        Public Shared distanceError As Boolean = True

#End Region

#Region "Schedular Search"
        Public Shared JobSheetNumber As String = "JobSheetNumber"
#End Region

#Region "Fault Search/Manegement"

#Region "General Constants"
        Public Shared NewfaultId As Integer = -1
        Public Shared DefaultLocationRepairId As Integer = -1
        Public Shared NewFaultTitle As String = "Add a New Fault"
        Public Shared AmendFaultTitle As String = "Amend Fault"
#End Region

#Region "Paging and Sorting Constants"
        Public Shared faultManagementSortBy As String = "FaultID"
        Public Shared faultManagementResultPerPage As Integer = 25
        Public Shared faultManagementInitialPageIndex As Integer = 0
#End Region

#Region "Trades GridView\Table Column Name For Add Fault Page/Popup"
        Public Shared faultTradeIdColumn As String = "FaultTradeId"
        Public Shared tradeIdColumn As String = "TradeId"
        Public Shared tradeNameColumn As String = "TradeName"
        Public Shared isDeletedColumn As String = "IsDeleted"
#End Region

#Region "Fault Location Repairs GridView\Table Column Name For Add/Edit Fault Page"
        Public Shared faultRepairIdColumn As String = "faultRepairId"
        Public Shared areaIdColumn As String = "areaId"
        Public Shared areaColumn As String = "area"
        Public Shared repairIdColumn As String = "repairId"
        Public Shared repairColumn As String = "repair"
        Public Shared isRepairDeletedColumn As String = "isDeleted"
#End Region

#End Region

#Region "Job Sheet Summary Sub Contractor Update"

        'Table names
        Public Shared jobSheetSummaryDetailTable As String = "JobSheetSummaryDetailTable"
        Public Shared jobSheetSummaryAsbestosTable As String = "JobSheetSummaryAsbestosTable"

#End Region

#Region "Recall Details By Fault Log ID"

        'Table Names
        Public Shared PropertyDetailsDT As String = "PropertyDetailsDT"
        Public Shared OriginalFaultDetailsDT As String = "OriginalFaultDetailsDT"
        Public Shared RecallFaultsDetailsDT As String = "RecallFaultsDetailsDT"

#End Region

#Region "Intellegent Scheduling Principle"
        Public Shared EmergencyPriorityName As String = "Emergency"
        Public Shared UrgentPriorityName As String = "Urgent"
        Public Shared RoutinePriorityName As String = "Routine"
        Public Shared DailyPriorityName As String = "Daily"

        Public Shared StartDateCol As String = "StartDate"
        Public Shared EndDateCol As String = "EndDate"
        Public Shared StartTimeCol As String = "StartTime"
        Public Shared EndTimeCol As String = "EndTime"
        Public Shared WeekDayCol As String = "Name"
        Public Const CreationDateCol As String = "CreationDate"

        Public Shared MaximumDayWorkingHours As Double = 23.5
#End Region

#Region "Logged In User Type"
        Public Enum LoggedInUserTypes
            Manager = 1
            Scheduler = 2
        End Enum

#End Region

#Region "Appointment Confirmation Email"

        Public Shared SubjectAppointmentConfirmationEmail As String = "Appointment Confirmation - Broadland Repair Service"

#End Region

#Region "Report Area Heading"

        Public Shared FaultDatabaseReport As String = "Fault Database"
        Public Shared FaultManagementReport As String = "Fault Management Report"
        Public Shared FollowonWorksReport As String = "Follow on Works Report"
        Public Shared NoEntryReport As String = "No Entry Report"
        Public Shared PausedFaultsReport As String = "Paused Faults Report"
        Public Shared RecallsReport As String = "Recalls Report"
        Public Shared ContractorReport As String = "Contractor Report"
        Public Shared RepairDatabaseReport As String = "Repair Database"



#End Region

#Region "SMS"
        Public Shared SMSUrl As String = ConfigurationManager.AppSettings("SMSAddress")
        Public Shared SMSUserName As String = "adnan.mirza@broadlandgroup.org"
        Public Shared SMSPassword As String = "obp5+Ne"
        Public Shared strSMS As String = "sms"
        Public Shared SMSCompany As String = "Broadland"
        Public Shared SMSmessage As String = "Broadland Repairs Service will be carrying out your repair in the {AppointmentTime} of {AppointmentDate}. If you are unable to keep this appointment please call 0303 303 0003"
#End Region

#Region "Contractor report"
        Public Shared FaultRepairListID As String = "FaultRepairListID"
        Public Shared FaultRepairDescription As String = "Description"
#End Region

#Region "Scheduling Duration"

        Public Const EditImageButtonControlId As String = "imgBtnEdit"
        Public Const DoneImageButtonControlId As String = "imgBtnDone"
        Public Const DurationLabelControlId As String = "lblDuration"
        Public Const DurationDropdownControlId As String = "ddlDuration"
        Public Const DurationPanelControlId As String = "pnlDuration"
        Public Const DurationUserControlId As String = "ucDuration"

#End Region

#Region "String Formats"

        Public Const AppointmentDateTimeFormat As String = "HH:mm dddd d MMMM yyyy"
        Public Const CustomLongDateFormat As String = "dddd d MMMM yyyy"

#End Region

#Region "Assign Work To Contractor"

        Public Const EmailSubject As String = "Purchase Order for Reactive Repair Work"
        Public Const NoExpenditureSetup As String = "No Expenditures are Setup"
        Public Const WorksRequiredCol As String = "WorksRequired"
        Public Const FaultInfoDt As String = "FaultInfo"
        Public Const FaultCostInfoDt As String = "FaultCostInfo"

        Public Const EstateMaintenanceCostId As Int32 = 11

        Public Const WorkOrderedPoStatusId As Int32 = 1
        Public Const QueuedPoStatusId As Int32 = 0


#Region "Work Required Data Table column names"

        Public Const NetCostCol As String = "NetCost"
        Public Const VatTypeCol As String = "VatType"
        Public Const VatCol As String = "Vat"
        Public Const GrossCol As String = "Gross"
        Public Const PIStatusCol As String = "PIStatus"
        Public Const ExpenditureIdCol As String = "ExpenditureId"
        Public Const MaxStringLegthWordRequired As Integer = 4000
        Public Const TenantDetailsDt As String = "TenantDetails"
        Public Const BlockDetailsdt As String = "BlockDetailsdt"
        Public Const WorkRequireddt As String = "WorkRequireddt"
        Public Const TenantRiskDetailsDt As String = "TenantRiskDetails"
        Public Const TenantVulnerabilityDetailsDt As String = "TenantVulnerabilityDetails"
        Public Const OrderedByDetailsDt As String = "OrderedByDetails"
        Public Const ContractorDetailsDt As String = "ContractorDetails"
        Public Const FaultDetailsDt As String = "FaultDetails"
        Public Const AsbestosDetailsDt As String = "AsbestosDetails"

#End Region

#Region "Warranties Data Table column names"

        Public Const ExpiryCol As String = "EXPIRYDATE"
        Public Const WarrantyTypeCol As String = "WARRANTYTYPE"
        Public Const CategoryCol As String = "LOCATIONNAME"
        Public Const AreaCol As String = "AREANAME"
        Public Const ItemCol As String = "ITEMNAME"
        Public Const ContractorCol As String = "CONTRACTOR"

#End Region

#Region "Drop Down Lists"

        ''' <summary>
        ''' These two constant are use to add the "Please Select" Drop down item in a drop down list.
        ''' </summary>
        ''' <remarks>This is a good practice to avoid spelling mistakes etc.</remarks>

        Public Const DropdownContractorDefaultText As String = "-- Please select contractor --"
        Public Const DropdownContactDefaultText As String = "-- Please select contact --"
        Public Const DropDwonDefaultText As String = "Please Select"

        Public Const ddlDefaultDataTextField As String = "description"
        Public Const ddlDefaultDataValueField As String = "id"

        Public Const noContactFound = "No contact found"

#End Region

#End Region

#Region "Appointment Type Icons"

        Public Shared appointmentIcons As New Dictionary(Of String, String) From
                {{"Fault", "icon_repair.png"}, {"SbFault", "icon_repair.png"} _
                , {"", ""} _
                , {"Planned", "icon_planned.png"}, {"Adaptation", "icon_adaptations.png"}, {"Condition", "icon_crt.png"}, {"Misc", "icon_miscellaneous.png"} _
                , {"Stock", "icon_stock.png"} _
                , {"Cyclic Maintenance", "icon_cyclic_maintenance.png"} _
                , {"M&E Servicing", "icon_M_E_Servicing.png"} _
                , {"Default", "icon_miscellaneous.png"} _
                , {"Void", "icon_void.png"} _
                , {"GAS", "icon_gas.png"} _
                , {"VoidGAS", "icon_void_gas.png"} _
                , {"Appliance Defect", "icon_defect.png"}
                }

#End Region

#Region "Data Table Name"

        Public Shared RecentFaultsDtName As String = "RecentFaults"
        Public Shared CustomerDtName As String = "Customer"
        Public Shared PropertyDtName As String = "Property"
        Public Shared SchemeBlockDtName As String = "Scheme"
        Public Shared TempFaultDataTable As String = "TempFaultDataTable"
        Public Shared ConfirmFaultDataTable As String = "ConfirmFaultDataTable"
        Public Shared RecallFaultsDataTable As String = "RecallFaultsDataTable"
        Public Shared PlannedDataTable As String = "PlannedDataTable"
        Public Shared CurrentFaultsDataTable As String = "CurrentFaultsDataTable"
        Public Shared GasAppointmentssDataTable As String = "GasAppointmentssDataTable"
        Public Shared WorkingHourDataTable As String = "WorkingHourDataTable"

        Public Shared PropertyCalendarOperativesDt As String = "Operatives"
        Public Shared PropertyCalendarLeavesDt As String = "Leaves"
        Public Shared PropertyCalendarAppointmentsDt As String = "Appointments"
        Public Shared PropertyCalendarCoreWorkingHoursDt As String = "CoreWorkingHours"
        Public Shared FaultGeneralInfoDataTable As String = "FaultGeneralInfoDataTable"
        Public Shared FaultTradesDataTable As String = "FaultTradesDataTable"
        Public Shared FaultRepairsDataTable As String = "FaultRepairsDataTable"
        Public Shared RepairSearchResultDataTable As String = "RepairSearchResultDataTable"

#End Region

    End Class
End Namespace



