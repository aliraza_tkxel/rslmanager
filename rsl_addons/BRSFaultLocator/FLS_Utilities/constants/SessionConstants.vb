﻿Imports System

Namespace FLS_Utilities

    Public Class SessionConstants

#Region "User Session"

        Public Shared ConSessionUserId As String = "SESSION_USER_ID"
        Public Shared FaultSchedulingUserId As String = "AppServicingUserId"
        Public Shared UserFullName As String = "UserFullName"
        Public Shared EmployeeId As String = "UserEmployeeId"
        Public Shared LoggedInUserType As String = "LoggedInUserType"

#End Region

#Region "Property Session"

        Public Shared PropertyDistance As String = "PropertyDistance"
        Public Shared PatchId As String = "PatchId"
        Public Shared PropertyId As String = "PropertyId"
        Public Shared SbPatchId As String = "SbPatchId"
#End Region

#Region "Trade Session"

        Public Shared TradeId As String = "TradeId"
        Public Shared SbTradeId As String = "SbTradeId"

#End Region

#Region "Fault Session"

        Public Shared IsFollowOn As String = "IsFollowOn"
        Public Shared IsSbFollowOn As String = "IsSbFollowOn"
        Public Shared FollowOnFaultLogId As String = "FollowOnFaultLogId"
        Public Shared SbFollowOnFaultLogId As String = "SbFollowOnFaultLogId"
        Public Shared IsNoEntry As String = "IsNoEntry"
        Public Shared NoEntryFaultLogId As String = "NoEntryFaultLogId"

#End Region

#Region "Google Map"
        Public Shared existingAppointmentForAvailableAppoinetmentsMap As String = "existingAppointmentForAvailableAppoinetmentsMap"
        
	Public Shared existingAppointmentForRearrangeAppoinetmentsMap As String = "existingAppointmentForRearrangeAppoinetmentsMap"
Public Shared SbExistingAppointmentForAvailableAppoinetmentsMap As String = "existingAppointmentForAvailableAppoinetmentsMap"
#End Region

        Public Shared PreviousOperativeId As String = "PreviousOperativeId"
        Public Shared CustomerId As String = "CustomerId"
        Public Shared SchemeId As String = "SchemeId"
        Public Shared BlockId As String = "BlockId"
        Public Shared AppointmentSummaryFaultList As String = "AppointmentSummaryFaultList"
        Public Shared AppointmentData As String = "AppointmentData"
        Public Shared JobSheetNumber As String = "JobSheetNumber"
        Public Shared SbJobSheetNumber As String = "SbJobSheetNumber"
        Public Shared SubcontractorJobSheetIndex As String = "SubcontractorJobSheetIndex"
        Public Shared SubcontractorCurrentFaultBO As String = "SubcontractorCurrentFaultBO"
        Public Shared TemporaryFaultBo As String = "TemporaryFaultBo"
        Public Shared FaultBo As String = "FaultBo"
        Public Shared TemporaryFaultIds As String = "TemporaryFaultIds"
        Public Shared TempFaultIds As String = "TempFaultIds"
        Public Shared CustomerData As String = "CustomerData"
        Public Shared CommonAddress As String = "CommonAddress"
        Public Shared SbData As String = "SbData"
        Public Shared FollowOnDataSet As String = "FollowOnDataSet"
        Public Shared NoEntryDataSet As String = "NoEntryDataSet"
        Public Shared RecallDataSet As String = "RecallDataSet"
        Public Shared FaultLogId As String = "FaultLogId"
        Public Shared SbFaultLogId As String = "FaultLogId"
        Public Shared OriginalFaultLogId As String = "OriginalFaultLogId"
        Public Shared OriginalSbFaultLogId As String = "OriginalFaultLogId"
        Public Shared IsRecall As String = "isRecall"
        Public Shared IsSbRecall As String = "IsSbRecall"
        Public Shared RedirectCheck As String = "redirectCheck"
        Public Shared SbRedirectCheck As String = "sbredirectCheck"
        Public Shared FaultId As String = "FaultId"
        Public Shared SbFaultId As String = "SbFaultId"
        Public Shared Source As String = "source"
        Public Shared SbSource As String = "sbsource"
        Public Shared FollowOnPageSortData As String = "FollowOnPageSortData"
        Public Shared GroupBy As String = "groupBy"
        Public Shared TempFaultDs As String = "TempFaultDs"
        Public Shared TempSbFaultDs As String = "TempSbFaultDs"
        Public Shared AvailableOperativesDs As String = "AvailableOperativeDs"
        Public Shared SelectedTempFaultDv As String = "SelectedTempFaultDv"
        Public Shared ConfirmedFaults As String = "ConfirmedFaultsDv"
        Public Shared SbConfirmedFaults As String = "SbConfirmedFaultsDv"
        Public Shared ConfirmedGasAppointment As String = "ConfirmedGasAppointmentDv"
        Public Shared ConfirmedAppointmentIdList As String = "ConfirmedAppointmentIdList"
        Public Shared Jsn As String = "Jsn"
        Public Shared HashTable As String = "hashTable"
        Public Shared AppointmentId As String = "AppointmentId"
        Public Shared SbAppointmentId As String = "SbAppointmentId"
        Public Shared PopupOpened As String = "PopupOpened"
        Public Shared isSuccessAdded As String = "isSuccessAdded"
        Public Shared weeklyAptCalendarDT As String = "weeklyAptCalendarDT"
        Public Shared IsAppointmentRearrange As String = "IsAppointmentRearrange"
        Public Shared IsSbAppointmentRearrange As String = "IsSbAppointmentRearrange"
        Public Shared PathsData As String = "PathsData"
        Public Shared ddlData As String = "ddlData"
        Public Shared OperativesBlock As String = "operativesBlockScreen7"
        Public Shared CalculatedSlots As String = "CalculatedSlots"
        Public Shared SFSCalendar As String = "SFSCalendar"
        Public Shared TempFaultAptBlockList As String = "TempFaultAptBlockList"
        Public Shared TempSbFaultAptBlockList As String = "TempSbFaultAptBlockList"
        Public Shared ConfirmFaultAptBlockList As String = "ConfirmFaultAptBlockList"
        Public Shared ConfirmSbFaultAptBlockList As String = "ConfirmSbFaultAptBlockList"
        Public Shared RepairDataTable As String = "RepairDataTable"
        Public Shared OperativeworkingHour As String = "OperativeworkingHour"
        Public Shared AvailableOperativesDsForRearrange As String = "AvailableOperativeDsForRearrange"
        Public Shared AssignToContractorBo As String = "AssignToContractorBo"

        Public Shared SbTemporaryFaultBo As String = "SbTemporaryFaultBo"
        Public Shared SbAvailableOperativesDs As String = "SbAvailableOperativeDs"
        Public Shared SbJsn As String = "SbJsn"
        Public Shared SbHashTable As String = "SbHashTable"
        Public Shared SbAppointmentData As String = "SbAppointmentData"
        Public Shared SbSelectedTempFaultDv As String = "SbSelectedTempFaultDv"
        Public Shared SbConfirmedAptIdList As String = "SbConfirmedAptIdList"
        Public Shared SbSubcontractorCurrentFaultBO As String = "SbSubcontractorCurrentFaultBO"
        Public Shared TradeType As String = "TradeType"

        Public Shared PropertyCalendarFilters As String = "PropertyCalendarFilters"

        Public Shared FaultRepairListId As String = "FaultRepairListId"

        Public Shared FollowOnWorkId As String = "followOnWorkId"
        Public Shared ExpenditureId As String = "ExpenditureId"
    End Class

End Namespace


