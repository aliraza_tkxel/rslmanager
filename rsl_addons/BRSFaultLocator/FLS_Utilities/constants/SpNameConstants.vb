﻿Imports System
Namespace FLS_Utilities

    Public Class SpNameConstants

#Region "Master Page"

        Public Shared GetPropertyMenuList As String = "P_GetPropertyMenuList"
        Public Shared GetPropertyMenuPages As String = "P_GetPropertyMenuPages"

#End Region

#Region "Search Fault"
        Public Shared GetRecentFaults As String = "FL_GetRecentFaults"
        Public Shared GetSbRecentFaults As String = "FL_GetSbRecentFaults"
        Public Shared GetSchedularSearchFaults As String = "FL_GetSchedularSearchFaults"
#End Region

#Region "Avalialble Appointments & Faults"
        Public Shared GetTemporaryFaultBasket As String = "FL_GetTemporaryFaultBasket"
        Public Shared GetSbTemporaryFaultBasket As String = "FL_GetSbTemporaryFaultBasket"
        Public Shared GetNewCreatedAppointmentData As String = "FL_GetNewlyCreatedAppointmentData"
        Public Shared GetAvailableOperatives As String = "FL_GetAvailableOperatives"
        Public Shared GetAvailableOperativesForRearrange As String = "FL_GetAvailableOperativesForRearrangeAppointment"
        Public Shared GetOperativeWorkingHours As String = "FL_GetOperativeWorkingHours"
        Public Shared IsTimeSlotAvailable As String = "FL_isTimeSlotAvailable"
#End Region

#Region "Fault"
        Public Shared SaveFaultTemporary As String = "FL_SaveFaultTemporary"
        Public Shared SaveSbFaultTemporary As String = "FL_SaveSbFaultTemporary"
        Public Shared SaveFaultTemporaryRearrange As String = "FL_SaveFaultTemporaryReArrange"
        Public Shared RearrangeAppointment As String = "FL_RearrangeAppointment"
        Public Shared GetFaultBasketInfo As String = "FL_GetFaultBasketInfo"
        Public Shared GetFaultsAndAppointments As String = "FL_GetFaultsAndAppointments"
        Public Shared GetSbFaultsAndAppointments As String = "FL_GetSbFaultsAndAppointments"
        Public Shared GetFaultsList As String = "FL_GetFaultsList"
        Public Shared GetAppointmentData As String = "FL_GetAppointmentData"
        Public Shared GetGasAppointmentData As String = "AS_GetAppointmentDetail"
        Public Shared UpdateFaultNote As String = "FL_UpdateFaultNote"
        Public Shared UpdateGasNote As String = "AS_UpdateNotes"
        Public Shared SaveAppointmentDetails As String = "FL_SaveAppointmentDetails"
        Public Shared SaveSBAppointmentDetails As String = "FL_SaveSBAppointmentDetails"
        Public Shared GetAppointmentSummary As String = "FL_GetAppointmentSummary"
        Public Shared SaveFaultCancellation As String = "FL_SaveFaultCancellation"
        Public Shared SaveGasServicingCancellation As String = "AS_GasServicingCancellation"
        Public Shared GetMoreDetail As String = "FL_GetMoreDetails"
        Public Shared SaveSubcontractorFault As String = "FL_SaveSubcontractorAppointmentDetails"
        Public Shared DeleteAllTemporaryFault As String = "FL_DeleteAllTempFault"
        Public Shared ConfirmedAppointmentByJsn As String = "FL_GetConfirmedAppointmentByJsn"
        Public Shared GetJobSheetByAppointment As String = "FL_GetJobSheetByAppointment"
        Public Shared GetRSLModulesList As String = "P_GetRSLModulesList"
        Public Shared GetSbFaultBasketInfo As String = "FL_GetSbFaultBasketInfo"
        Public Shared GetSchemeBlockAddress As String = "FL_GetSchemeBlockAddress"
        Public Const GetOperativeInformation As String = "PLANNED_GetOperativeInfo"
        Public Const GetContactEmailDetail As String = "DF_GetContactEmailDetail"

#End Region

#Region "JobSheet"

        Public Shared GetJobSheetDetail As String = "FL_GetJobSheetDetail"
        Public Shared GetSbJobSheetDetail As String = "FL_GetSbJobSheetDetail"
        Public Shared GetSubcontractorJobSheetDetail As String = "FL_GetSubcontractorJobSheetDetail"
        Public Shared GetSbJobSheetSummaryByJSN As String = "FL_GetSbJobSheetSummaryByJSN"
        Public Shared GetSbSubcontractorJobSheetDetail As String = "FL_GetSbSubcontractorJobSheetDetail"

#End Region

#Region "FaultManagement"
        'Fault Management Search SP(s)
        Public Shared getFaultManagementSearchRowCount As String = "FL_GetFaultManagementSearchResultsCount"
        Public Shared getFaultManagementSerachRecords As String = "FL_GetFaultManagementSearchResults"

        'Get an Existing Fault's Values from Database (For Amend Fault Modal Popup
        Public Shared getFaultValuesByID As String = "FL_GetFaultValuesByID"

        'Get DropDown/Lookup Lists for Add and Amend Fault Popups
        Public Shared getAreaLookupAll As String = "FL_GetAreaLookUpAll"
        Public Shared getRepairsLookup As String = "FL_GetFaultRepairs"
        Public Shared getPriorityLookup As String = "FL_GetPriorityLookUpAll"
        Public Shared getTradeLookup As String = "FL_GetTradeLookUpAll"
        Public Shared getVatRateLookUp As String = "FL_GetVatLookUpAll"
        Public Shared getVatValuesByVatID As String = "FL_GetVatValuesByVatID"

        'Create A New Fault in Data Base
        Public Shared addFault As String = "FL_FaultManagementAddFault"

        'Create A Save Fault in Data Base
        Public Shared saveFault As String = "FL_FaultManagementSaveFault"
        'Update Fault Notes
        Public Shared updateFaultNotes As String = "FL_UpdateFaultNotes"
        'Amend an Existing Fault
        Public Shared amendFault As String = "FL_FaultManagementAmendFault"

        Public Shared checkFaultTradeInUse As String = "FL_CheckFaultTradeInUse"

        'TODO: After Confirmation,  Remove this unused stored procedure reference.
        Public Shared deleteAllFaultsByFaultID As String = "FL_FaultManagementDeleteAllTradesByFaultID"
        'Trades Add/Delete FL_FAULT_Trade Table
        Public Shared addFaultTradeRelation As String = "FL_FaultManagementAddFaultTrade"

        Public Shared SearchFaultRepair As String = "FL_SearchFaultRepair"

#End Region

#Region "Recall"
        Public Shared GetRecallDetail As String = "FL_GetRecallDetail"
#End Region

#Region "Customer"
        Public Shared AmendCustomerAddress As String = "FL_AmendCustomerAddress"
#End Region

#Region "Reports"
        Public Shared GetFollowOnList As String = "FL_GetFollowOnList"
        Public Shared GetSubcontractorList As String = "FL_GetSubcontractorList"
        Public Shared GetFollowOnWorksDetails As String = "FL_GetFollowOnWorksDetails"
        Public Shared GetNoEntryDetail As String = "FL_GetNoEntryDetail"
        Public Shared GetNoEntryList As String = "FL_GetNoEntryList"
        Public Shared GetOperativeList As String = "FL_GetOperativeList"
        Public Shared GetPausedFaultOperativeList As String = "FL_GetPausedFaultOperativeList"
        Public Shared GetRecallList As String = "FL_GetRecallList"
        Public Shared GetRecallDetailByFaultLogID As String = "FL_GetRecallDetailByFaultLogID"
        Public Shared GetPausedFaultList As String = "FL_GetPausedFaultList"
        Public Shared GetFaultRepairList As String = "FL_GETFAULTREPAIRLIST"
        Public Shared AddFaultRepairList As String = "FL_ADD_REPAIRSFORCONTRACTOR"
        Public Shared GetSchemeDropDown As String = "PDR_GetDevelopmentScheme"
        Public Shared GetBlockDropDownBySchemeId As String = "PDR_GetBlocks"
        Public Shared GetOperativesFilterByTrandeId As String = "FL_GetOperativesFilterByTradeId"
        Public Shared GetAppointmentTypes As String = "P_GetAppointmentTypes"
        Public Shared GetRepairList As String = "FL_GetRepairList"
        Public Shared AmendRepair As String = "FL_AmendRepair"
        Public Shared AddNewRepair As String = "FL_AddNewRepair"
        Public Shared DeleteFollowOnWork As String = "FL_DeleteFollowOnWork"
        Public Shared GetRepairListExportToExcel = "FL_GetRepairListExportToExcel"

        Public Shared AssociatedFaults As String = "FL_GetsAssociatedFaultsList"

        Public Shared GetWarrantyList As String = "FL_GetWarrantiesBeforeExpiry"
#End Region

#Region "ViewDetails"

        'Executed for Retrieving data for view screen
        Public Shared GetCustomerInfo As String = "TO_CUSTOMER_GETCUSTOMERINFO"


#End Region

#Region "Basket"

        'Executed for Retrieving data for view screen
        Public Shared FL_GetContractorsByTrade As String = "FL_GetContractorsByTrade"
        Public Shared FL_DeleteFaultFromBasket As String = "FL_DeleteFaultFromBasket"
        Public Shared FL_UpdateOrgId As String = "FL_UpdateOrgId"
        Public Shared FL_GetAppointmentDetail As String = "FL_GetAppointmentDetail"
        Public Shared GetFaultInfo As String = "FL_GetFaultInfo"

#End Region

#Region "Schedule Appointments"
        'TODO: After Confirmation,  Remove this unused stored procedure reference.
        Public Shared GetAppointmentToBeArranged As String = "AS_AppointmentsToBeArranged"
        'TODO: After Confirmation,  Remove this unused stored procedure reference.
        Public Shared GetAppointmentsArranged As String = "AS_AppointmentsArranged"
        'TODO: After Confirmation,  Remove this unused stored procedure reference.
        Public Shared GetSearchedValues As String = "AS_AppointmentArrangedSearchedValues"
        'TODO: After Confirmation,  Remove this unused stored procedure reference.
        Public Shared GetAppointmentEngineersInfo As String = "AS_AppointmentEngineerInfo"
        'TODO: After Confirmation,  Remove this unused stored procedure reference.
        Public Shared GetScheduledAppointmentsDetail As String = "AS_ScheduledAppointmentsDetail"
        'TODO: After Confirmation,  Remove this unused stored procedure reference.
        Public Shared SaveAppointment As String = "AS_SaveAppointment"
        'TODO: After Confirmation,  Remove this unused stored procedure reference.
        Public Shared AmendAppointment As String = "AS_AmendAppointment"
        'TODO: After Confirmation,  Remove this unused stored procedure reference.
        Public Shared GetSelectedAppointmentInfo As String = "AS_GetSelectedAppointmentInfo"
#End Region

#Region "Fault Appointments Calendar"
        Public Shared GetAppointmentsCalendarInfo As String = "FL_GetAppointmentsCalendarInfo"
        Public Shared GetFaultAppointmentEngineerInfo As String = "FL_FaultAppointmentEngineerInfo"
        Public Shared SearchedFaultAppointmentEngineerInfo As String = "FL_SearchedFaultAppointmentEngineerInfo"
        Public Shared GetEngineersLeaveInformation As String = "FL_GetEngineersLeaveInformation"
        Public Shared GetEmployeeCoreWorkingHours As String = "FL_GetEmployeeCoreWorkingHours"
        Public Shared GetPropertyPatches As String = "FL_GetPropertyPatches"
        Public Shared GetFaultTrades As String = "FL_GetFaultTrades"
        Public Shared SaveAppointments As String = "FL_SaveAppointments"
        'for scheme block appointment
        Public Shared SaveSbAppointments As String = "FL_SaveSbAppointments"
        Public Shared GetNewlyCreatedAppointmentInfo As String = "FL_GetNewlyCreatedAppointmentInfo"
        Public Shared GetJointTenantsInfoByTenancyID As String = "FL_GetJointTenantsInfoByTenancyID"
        Public Shared GetPidTidbyAppointmentID As String = "FL_GetPatchID_TradeID_ByAppointmentID"
        Public Shared GetPropertyCalendarDetails As String = "FL_GetPropertyCalendarDetails"
        Public Shared GetAppointmentNotes As String = "FL_GetAppointmentNotes"
#End Region

#Region "Scheme & Patch "
        Public Shared GetSchemeNames As String = "AS_SchemeNames"
        Public Shared GetPatchLocations As String = "AS_PatchLocation"
#End Region

#Region "Login"
        Public Shared GetEmployeeById As String = "FL_GetEmployeeById"
#End Region

#Region "Job Sheet Summary Sub Contractor Update"

        Public Shared getJobSheetSummaryByJSN As String = "FL_GetJobSheetSummaryByJSN"
        Public Shared getFaultStatusLookUpSubContractor As String = "FL_GetFaultStatusLookUpSubContractor"
        Public Shared setFaultStatusSubContractorUpdate As String = "FL_SetFaultStatusSubContractorUpdate"
        Public Shared cancelPuchaseOrder As String = "FL_CancelFaultPurchaseOrder"
        Public Shared cancelPuchaseOrderEmailDetail As String = "FL_GetContractorEmailDetail"


#End Region

#Region "Custom Duration"

        Public Shared UpdateTempFaultDuration As String = "FL_UpdateTempFaultDuration"

#End Region

#Region "Assign To contractor"

#Region "Drop Down Values"
        Public Const GetPlannedContractorDropDownValues As String = "PLANNED_GetPlannedContractorDropDownValues"
        Public Const GetCostCentreDropDownValues As String = "PLANNED_GetCostCentreDropDownValues"
        Public Const GetBudgetHeadDropDownValuesByCostCentreId As String = "PLANNED_GetBudgetHeadDropDownValuesByCostCentreId"
        Public Const GetExpenditureDropDownValuesByBudgetHeadId As String = "PLANNED_GetUserExpenditureDropDownValuesAndLimitsByHeadId"
        Public Const GetContactDropDownValuesbyContractorId As String = "PLANNED_GetContactDropDownValuesbyContractorId"
        Public Const GetRepairsContractorDropDownValues As String = "FL_GetRepairsContractorDropDownValues"
        Public Const GetVatDropDownValues As String = "PLANNED_GetVatDropDownValues"
        Public Shared GetBlockListBySchemeID As String = "PDR_GetBlockListBySchemeID"
#End Region

        Public Shared GetAttribute As String = "FL_GetAttribute"
        Public Shared GetArea As String = "FL_GetArea"
        Public Const AssignWorkToContractor As String = "FL_AssignWorkToContractor"
        Public Const AssignPropertyWorkToContractor As String = "FL_AssignPropertyWorkToContractor"
        Public Shared SaveSbSubcontractorFault As String = "FL_SaveSbSubcontractorAppointmentDetails"
        Public Shared SaveContractorAppointmentDetails As String = "FL_SaveContractorAppointmentDetails"
        Public Shared GetDetailforEmailToContractor As String = "FL_GetDetailforEmailToContractor"
        Public Shared GetContractorDetailforEmail As String = "FL_GetContractorDetailforEmail"
        Public Shared FL_GetContractorEmailDetail As String = "FL_GetContractorEmailDetail"
        Public Shared GetReplacementDueForAssignToContractor As String = "FL_GetReplacementDueForAssignToContractor"
        Public Shared GetBudgetHolderByOrderId As String = "FL_GetBudgetHolderByOrderId"

        Public Shared GetHeadAndExpenditureId As String = "FL_GetHeadIdAndExpenditureId"
#End Region

    End Class
End Namespace



