﻿Namespace FLS_Utilities


    Public Class PathConstants

#Region "URL Constants"
        Public Const LoginPath As String = "~/../BHAIntranet/Login.aspx"
        Public Const AccessDeniedPath As String = "~/AccessDenied.aspx"
        'Screen 1
        Public Const SearchFault As String = "~/Views/Search/SearchFault.aspx"
        'for scheme/ block (Sb)
        Public Const SearchSbFault As String = "~/Views/Search/SearchSbFault.aspx"
        'Screen3
        Public Const OtherAppointmentPath As String = "~/Views/Faults/OtherAppointments.aspx"
        'for scheme/ block (Sb)
        Public Const OtherSbAppointmentPath As String = "~/Views/Faults/OtherSbAppointments.aspx"
        'Screen 4
        Public Const RecallDetails As String = "~/Views/Faults/RecallDetails.aspx"
        Public Const RecallSbDetails As String = "~/Views/Faults/SbRecallDetails.aspx"
        Public Const RecallDetailsWithParameters As String = "~/Views/Search/SearchFault.aspx?isRecall=yes&jsn="
        Public Const RecallSbDetailsWithParameters As String = "~/Views/Search/SearchSbFault.aspx?isRecall=yes&jsn="
        'Screen 5
        Public Const MoreDetails As String = "~/Views/Faults/MoreDetails.aspx"
        'for scheme/ block (Sb)
        Public Const MoreSbDetails As String = "~/Views/Faults/MoreSbDetails.aspx"
        Public Const MoreDetailsFromStart As String = "~/Views/Faults/MoreDetails.aspx"
        'for scheme/ block (Sb)
        Public Const MoreSbDetailsFromStart As String = "~/Views/Faults/MoreSbDetails.aspx"
        'Screen 6
        Public Const FaultBasket As String = "~/Views/Faults/FaultBasket.aspx"
        Public Const SbFaultBasket As String = "~/Views/Faults/SbFaultBasket.aspx"
        Public Const FaultBasketPath As String = "~/Views/Faults/FaultBasket.aspx?src=moreDetails"
        'for scheme/ block (Sb)
        Public Const SbFaultBasketPath As String = "~/Views/Faults/SbFaultBasket.aspx"
        'Screen 7
        Public Const AvailableAppointments As String = "~/Views/Faults/AvailableAppointments.aspx"
        'for scheme/ block (Sb)
        Public Const SbAvailableAppointments As String = "~/Views/Faults/AvailableSbAppointments.aspx"

        'Screen 8
        Public Const AppointmentSummary As String = "~/Views/Faults/AppointmentSummary.aspx"
        Public Const SbAppointmentSummary As String = "~/Views/Faults/SBAppointmentSummary.aspx"
        'Screen 11a, 12, 11b, 14
        Public Const ScheduleCalender = "~/Views/FaultCalendar/SchedulingCalendar.aspx"
        'for scheme/ block (Sb)
        Public Const SbScheduleCalender = "~/Views/FaultCalendar/SbSchedulingCalendar.aspx"
        'Screen 15
        Public Const RearrangCurrentFault As String = "~/Views/Faults/ReArrangingCurrentFault.aspx"
        'for scheme/ block (Sb)
        Public Const ReArrangingSbFault As String = "~/Views/Faults/ReArrangingSbFault.aspx"
        'Screen 16,17
        Public Const SchedulersCalanderSearch As String = "~/Views/Faults/SchedulersCalanderSearch.aspx"
        'Read only Calendar Path for highlihting
        Public Const SchedulersCalanderSearchHighlight As String = "SchedulersCalanderSearch.aspx"
        'Screen 18
        Public Const CancellationAppointmentSummary As String = "~/Views/Faults/CancellationAppointmentSummary.aspx"
        Public Const CancellationAppointmentSummaryForGas As String = "~/Views/Faults/CancellationAppointmentSummaryForGas.aspx"
        Public Const CancellationSbAppointmentSummary As String = "~/Views/Faults/CancellationSbAppointmentSummary.aspx"
        Public Const JobSheetDetail As String = "~/Views/Faults/JobSheetDetail.aspx"
        Public Const JobSheetSummarySubcontractor As String = "~/Views/Faults/JobSheetSummarySubcontractor.aspx"
        'for scheme/ block (Sb)
        Public Const SbJobSheetSummarySubcontractor As String = "~/Views/Faults/SbJobSheetSummarySubcontractor.aspx"
        Public Const CustomerRecords As String = "/Customer/Crm.asp?CustomerId="
        'Reports area path
        Public Const ReportAreaPath As String = "~/Views/Reports/ReportsArea.aspx"
        Public Const ReportPathHighlight As String = "ReportsArea.aspx"
        'bridge file path
        Public Const Bridge As String = "~/Bridge.aspx"
        '''''''''''Urls for old report area        
        ''Pricing control
        'faultlocator/faultmanagement/fault_manager.aspx?option=pricing
        'Repair Management
        '/faultlocator/secure/faultlocator/repair_list_management.aspx
        'Priority Settings
        'faultlocator/faultmanagement/fault_manager.aspx?option=priority
        'Reported Faults
        '/faultlocator/secure/faultlocator/fault_locator.aspx
        'Pricing Control Url
        Public Const PricingControlReport As String = "/faultlocator/faultmanagement/fault_manager.aspx?option=pricing"
        'Repair Management Report Url
        Public Const RepairManagementReport As String = "/faultlocator/secure/faultlocator/repair_list_management.aspx"
        'Priority Settings Report Url
        Public Const PrioritySettingsReport As String = "/faultlocator/faultmanagement/fault_manager.aspx?option=priority"
        'Reported Faults Report Url
        Public Const ReportedFaultsReport As String = "~/../RepairsDashboard/Bridge.aspx?rpt=rf"
        'In Progress Faults
        Public Const InProgressFaultsReport As String = "~/../RepairsDashboard/Bridge.aspx?rpt=ip"
        'Overdue Faults
        Public Const OverDueFaultsReport As String = "~/../RepairsDashboard/Bridge.aspx?rpt=od"
        'Emergency Faults
        Public Const EmergencyFaultsReport As String = "~/../RepairsDashboard/Bridge.aspx?rpt=em"
        'Completed Faults
        Public Const CompletedFaultsReport As String = "~/../RepairsDashboard/Bridge.aspx?rpt=cmp"
        'Repair Dashboard Screen
        Public Const RepairsDashboardScreen As String = "~/../RepairsDashboard/Bridge.aspx"
        'Repair Dashboard Reports
        Public Const RepairsDashboardReports As String = "~/RepairsDashboard/Bridge.aspx?report=yes"

        Public Const SeparatorImagePath As String = "~/Images/PropertyModule/sep.jpg"

        Public Const ICalFilePath As String = "~/Attachment/FaultSchedule.ics"

        Public Const PropertyCalendarPath As String = "~/Views/Calendar/PropertyCalendar.aspx"
        Public Const FaultDetailPath As String = "~/Views/Faults/FaultDetail.aspx"

        Public Const FaultManagementReportPath As String = "~/Views/Reports/ReportsArea.aspx?rd=fm"
        Public Const SMSURL As String = "~/../Includes/SMS.asp"

#End Region

#Region "Query String Constants"

        Public Const GroupByQueryStringKey As String = "groupby"
        'QueryString Constants
        Public Const IsRecall As String = "isRecall"
        Public Const Jsn As String = "jsn"
        Public Const Rearrange As String = "rearrange"
        Public Const FaultsList As String = "faultsList"
        Public Const AppointmentId As String = "apt"
        Public Const openPopup As String = "openPopup"
        Public Const Yes As String = "yes"
        Public Const fBasket As String = "faultBasket"
        Public Const sbfBasket As String = "sbfaultBasket"
        Public Const Create As String = "Create"
        Public Const Src As String = "src"
        Public Const Cid As String = "cid"
        Public Const Pid As String = "pid"
        Public Const Menu As String = "mn"
        Public Const No As String = "no"
        Public Const CalView As String = "cal"
        Public Const ReportView As String = "report"
        Public Const RepairsDashboard As String = "rd"
        Public Const RejectedByContractor As String = "rbc"
        Public Const schedule As String = "schedule"
        Public Const AccessDenied As String = "accessDenied"
        Public Const isFollowOn As String = "isFollowOn"
        Public Const isNoEnty As String = "isNoEnty"
        Public Const Sid As String = "sid"
        Public Const Bid As String = "bid"
        Public Const isSb As String = "isSb"
        Public Const Page As String = "pg"
        Public Const PropertyCalendar As String = "propertyCal"
        Public Const FaultId As String = "fid"



#End Region

#Region "Src Keys"
        Public Const SearchFaultSrcVal As String = "searchFault"
        'for scheme/ block (Sb)
        Public Const SearchSbFaultSrcVal As String = "searchSbFault"
        Public Const OtherAppointmentSrcVal As String = "otherAppointments"
        'for scheme /block (Sb)
        Public Const OtherSbAppointmentSrcVal As String = "otherSbAppointments"
        Public Const MoreDetailsSrcVal As String = "moreDetails"
        'for scheme /block (Sb)
        Public Const MoreSbDetailsSrcVal As String = "moreSbDetails"
        Public Const RecallDetailsSrcVal As String = "recallDetails"
        Public Const RecallSbDetailsSrcVal As String = "recallSbDetails"
        Public Const FaultBasketSrcVal As String = "faultBasket"
        Public Const SbFaultBasketSrcVal As String = "sbfaultBasket"
        Public Const AvailableAppointmentsSrcVal As String = "availableAppointments"
        'for scheme /block (Sb)
        Public Const AvailableSbAppointmentsSrcVal As String = "sbAvailableAppointments"
        Public Const AppointmentSummarySrcVal As String = "appointmentSummary"
        'for scheme /block (Sb)
        Public Const SbAppointmentSummarySrcVal As String = "sbSppointmentSummary"
        Public Const ScheduleCalendarSrcVal As String = "scheduleCalendar"
        'for scheme /block (Sb)
        Public Const SbScheduleCalendarSrcVal As String = "sbScheduleCalendar"
        Public Const RearrangeAppointmentSrcVal As String = "rearrangeAppointment"
        'for scheme /block (Sb)
        Public Const SbRearrangeAppointmentSrcVal As String = "sbRearrangeAppointment"
        Public Const CancelAppointmentSummarySrcVal As String = "cancelAppointmentSummary"
        Public Const CancelAppointmentSummaryForGasSrcVal As String = "cancelAppointmentSummaryForGas"
        'for scheme /block (Sb)
        Public Const SbCancelAppointmentSummarySrcVal As String = "sbCancelAppointmentSummary"

        'for scheme /block (Sb)
        Public Const CancelSbAppointmentSummarySrcVal As String = "cancelSbAppointmentSummary"

        Public Const ReportAreaSrcVal As String = "reportArea"

        Public Const JobSheetSummarySubcontractorSrcVal As String = "jobSheetSummarySubcontractor"

#End Region

    End Class
End Namespace