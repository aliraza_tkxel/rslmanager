﻿Imports System

Namespace FLS_Utilities


    Public Class ViewStateConstants

#Region "Page Sort Constants"
        Public Shared SortDirection As String = "SortDirection"
        Public Shared SortExpression As String = "SortExpression"
        Public Shared Ascending As String = "Ascending"
        Public Shared Descending As String = "Descending"
        Public Shared DefaultPageNumber As String = "10"
        Public Shared DefaultPageSize As String = "10"
        Public Shared PageSortBo As String = "PageSortBo"
        Public Shared PageSortBoAssociatedFaults As String = "PageSortBoAssociatedFaults"
        Public Shared TotalPageNumbers As String = "TotalPageNumbers"
        Public Shared CurrentPageNumber As String = "CurrentPageNumber"
#End Region

#Region "General "

        Public Shared FaultLogId As String = "FaultLogId"
        Public Shared GridDataSet As String = "GridDataSet"
        Public Shared PropertyId As String = "PropertyId"
        Public Shared AppointmentId As String = "AppointmentId"
        Public Shared ApplianceId As String = "ApplianceId"
        Public Shared ResultDataSet As String = "ResultDataSet"
        Public Shared RepairsResultDataSet As String = "RepairsResultDataSet"
        Public Shared repairId As String = "repairID"
        Public Shared RepairList As String = "RepairList"

        Public Shared SlideIndex As String = "SlideIndex"
        Public Shared PopupSlideIndex As String = "PopupSlideIndex"
        Public Shared RedirectTempCount As String = "RedirectTempCount"
        Public Shared FollowOnDataSet As String = "FollowOnDataSet"
        Public Shared SubcontractorDataSet As String = "SubcontractorDataSet"
        Public Shared NoEntryDataSet As String = "NoEntryDataSet"
        Public Shared RecallDataSet As String = "RecallDataSet"
        Public Shared PausedDataSet As String = "PausedDataSet"
        Public Shared Search As String = "Search"
        Public Shared TotalCount As String = "TotalCount"
        Public Shared CustomerId As String = "CustomerId"
        Public Shared SchemeId As String = "SchemeId"
        Public Shared BlockId As String = "BlockId"
        Public Shared OperativeId As String = "OperativeId"
        Public Shared ConfirmedJsnList As String = "ConfirmedJsnList"
        Public Shared LastBlockId As String = "LastBlockId"

        Public Shared VirtualItemCount As String = "VirtualItemCount"

        Public Shared Email As String = "Email"
        Public Shared IsEmailSent As String = "IsEmailSent"

        Public Shared AppointmentData As String = "AppointmentData"

#End Region

#Region " Subcontractor JobSheet "
        Public Shared ArrayTempFaultBO As String = "ArrayTempFaultBO"
        Public Shared CurrentIndex As String = "CurrentIndex"
#End Region

#Region "Fault Search/Manegement"

#Region "View State Key Names"
        'Trades Grid View State
        Public Shared TradesGridViewState As String = "TradesViewState"
        Public Shared ExistingTradesViewState As String = "ExistingTradesViewState"
        Public Shared TradesToDeleteViewState As String = "TradesToDeleteViewState"
        Public Shared TradesToInsertViewState As String = "TradesToInsertViewState"
#End Region

#End Region
#Region "Assign to Contractor"
        Public Const WorkRequiredDT As String = "WorkRequiredDT"
        Public Const AssignToContractorWorkType As String = "AssignToContractorWorkType"
        Public Const IsSaved As String = "IsSaved"
#End Region
    End Class
End Namespace