﻿
Namespace FLS_Utilities


    Public Class UserMessageConstants

#Region "not categorized"
        Public Shared InvalidRecallJsnId As String = "Invalid JSN number."
        Public Shared InvalidPropertyCustomerId As String = "Invalid Property Id Or Customer Id in query string."
        Public Shared InvalidSbId As String = "Invalid Scheme Id Or Block Id in query string."
        Public Shared InvalidCustomerId As String = "Invalid Customer Id in session."
        Public Shared InvalidSubcontractorJobSheetIndex As String = "Invalid Subcontractor JobSheet index."
        Public Shared InvalidGroupByPropFaults As String = "Invalid input parameters (group by, property , faults)"
        Public Shared InvalidJobSheetNumber As String = "Invalid Job Sheet Number"
        Public Shared NoJobSheetFound As String = "No JobSheet found."
        Public Shared InvalidCustomerData As String = "Customer data not available."
        Public Shared InvalidInput As String = "Invalid selection or input."
        Public Shared RecordNotFound As String = "Record not found."
        Public Shared ProblemLoadingData As String = "Problem loading data."
        Public Shared ProblemLoadingCustomerData As String = "Problem loading customer data."
        Public Shared ProblemLoadingSelectedFaultData As String = "Problem loading selected fault data."
        Public Shared ProblemLoadingOperativeData As String = "Problem loading operative data."
        Public Shared ErrorDataBaseConnection As String = "Error while connecting with database."
        Public Shared NoRecordFound As String = "No record found."
        Public Shared InvalidUrl As String = "Url is invalid."
        Public Shared UserDoesNotExist As String = "You don't have access to appliance servicing. Please contant administrator."
        Public Shared UsersAccountDeactivated As String = "Your access to appliance servicing has been de-activiated. Please contant administrator."
        Public Shared LoginRslManager As String = "Please use the login from rsl manager."
        Public Shared ErrorDocumentUpload As String = "Error occured while uploading the document.Please try again."
        Public Shared EngNotFound As String = "Engineer not found."
        Public Shared EnterReason As String = "Please enter a reason for cancellation."
        Public Shared CancellationError As String = "You have already cancelled these faults."
        Public Shared ClearPanelMessage As String = ""
        Public Shared InvalidJsnList As String = "Invalid JSN List."
        Public Shared InvalidFaultData As String = "Invalid Fault Data."
        Public Shared InvalidFaultId As String = "Invalid Fault Data."
        Public Shared JobsNotSame As String = "The contractor jobs or the non contractor jobs first cannot be scheduled together. Both these types can not be scheduled together."
        Public Shared NoTempFaultData As String = "No temp fault data in session!"
        Public Shared InvalidTelephone As String = "Invalid Telephone number."
        Public Shared InvalidMobile As String = "Invalid Mobile number."
        Public Shared InvalidEmail As String = "Invalid Email address."
        Public Shared JSNNotAvailable As String = "Job Sheet Number not available."
        Public Shared DurationExceeded As String = "Total time required by faults fixation exceeds the operatives daily timing hours."
        Public Shared DeleteAllFault As String = "Something went wrong in the database. Delete failed in FL_DELETE_ALL_TEMP_FAULT."
        Public Shared AssignedToSubcontractor As String = "Fault successfully assigned to Subcontractor."
        Public Shared InvalidSrc As String = "Invalid Src"
        Public Shared SrcDoesNotExist As String = "Src does not exist"
        Public Shared OperativeNotFound As String = "Unable to find operative"
        Public Shared DurationError As String = "Duration of selected faults should not be greater than 9 hours."
        Public Shared AppointmentIdNotProvided As String = "Inavlid Appointment Id provided in query string."
        Public Shared PushNotificationFailed As String = "Appointment confirmed but failed to push appointment notification."
        Public Const AppointmentArrangedSavedEmailError As String = "Appoint arranged but unable to send email due to : "
#End Region

#Region "Generic Messages"
        Public Shared RecordNotExist As String = "No record exists"
        Public Shared DistanceError As String = "Unable to get distance."
        Public Shared INVALIDREQUEST As String = "Message from Google: The provided request was invalid."
        Public Shared MAXELEMENTSEXCEEDED As String = "Message from Google: The product of origins and destinations exceeds the per-query limit."
        Public Shared OVERQUERYLIMIT As String = "Message from Google: The service has received too many requests from your application within the allowed time period."
        Public Shared REQUESTDENIED As String = "Message from Google: The service denied use of the Distance Matrix service by your application."
        Public Shared UNKNOWNERROR As String = "Message from Google: The distance Matrix request could not be processed due to a server error. The request may succeed if you try again."
#End Region

#Region "Fault"
        Public Shared InvalidTempFaultId As String = "Invalid temp trade data"
        Public Shared FaultDoesNotExist As String = "Fault not found"
        Public Shared FaultNotCancelled As String = "Fault could not be cancelled"
        Public Shared FaultDeletedSuccessfuly As String = "Fault deleted successfully"
        Public Shared SelectContractor As String = "Please select contractor"
        Public Shared SelectFault As String = "Please select fault"
        Public Shared SelectOneFault As String = "The contractor jobs  cannot be scheduled Multiple fault At a time."
        Public Shared ErrorNoTradeExist As String = "No trade exist against this fault"
        Public Shared ErrorNoLocationExist As String = "No location exist against this fault, please add location from fault database area"

#End Region

#Region "Report"
        Public Shared FilterReportNotFound As String = "Record not found. Please select different scheme and block."
        Public Shared ReportNotFound As String = "Record not found. Please enter different search text."
        Public Shared RepairAlreadyAdded = "The Repair has already been added."
        Public Shared InvalidCompleteddate As String = "Please select current date or past date in Completed."
        Public Shared InvalidNoEntrydate As String = "Please select valid No Entry Date."
        Public Shared AddAtLeastRepair As String = "Please Select at least one repair."
        Public Shared RepairSavedSuccessfuly As String = "Repair Saved Successfully"
        Public Shared InvalidCompleteddatoeday As String = "Please select current or previous date for Completed date, and it must be greater order date."
        Public Shared InvalidNoEntryDateday As String = "Please select current or previous date for No Entry date, and it must be greater order date."
        Public Shared InvalidCompletedDateForNoEntry As String = "Completed date cannot be less than Order date.Please select valid date."

#End Region

#Region "Property"
        Public Shared InvalidPropertyId As String = "Property Id is invalid."
        Public Shared InvalidSchemeBlockId As String = "Scheme/Block Id is invalid."
#End Region

#Region "Calendar Popup"
        Public Shared SelectActionDate As String = "Please select date."
#End Region

#Region "Users"

        Public Shared UserSavedSuccessfuly As String = "User Saved Successfully"
        Public Shared userAlreadyExists As String = "User already exists"
        Public Shared UserNameNotValid As String = "User name is not valid"
        Public Shared ErrorUserUpdate As String = "Error Updating Record"
        Public Shared ErrorUserInsert As String = "Error Inserting Record"       
#End Region

#Region "Appointments"
        Public Shared SaveAppointmentFailed As String = "Some error occured in saving appointments"
        Public Shared AppointmentConfirmationFailed As String = "Some error occured in confirming appointments"
        Public Shared RearrangeAppointmentFailed As String = "Some error occured in Rearrange appointments"
        Public Shared AppointmentSavedEmailError As String = "Appointment saved but unable to send email due to : "
        Public Shared AppointmentSavedSMSError As String = "Appointment saved but unable to send SMS due to : "
        Public Shared NoOperativesExistWithInTime As String = "There are currently no operatives available to attend the reported fault within recommended response time. Either remove the Patch' and/or 'Due Date' filter to search for more operatives"
        Public Shared NoOperativesExistPatchUnchecked As String = "There are currently no operatives available to attend the reported fault within recommended response time. Either remove the 'Due Date' filter or assign the fault to a contractor by selecting the button below"
        Public Shared NoOperativesExistPatchDueDateUnchecked As String = "As there are currently no operatives available to attend the reported fault you have the option to assign the fault to a contractor by clicking on the 'Assign to Contractor' button below"
        Public Shared NoAppointmentRecord As String = "No appointment record found."
        Public Shared notValidDateFormat As String = "Please enter valid date."
        Public Shared DateNotValid As String = "Date should be greater then or equal to current date."
        Public Shared UncheckDueDate As String = "Date should be Less then Due Date or uncheck the due date filter."

#End Region

#Region "Add Appointment Popup"
        Public Shared SaveAppointmentSuccessfully As String = "Appointment is saved successfully."
        Public Shared SaveAppointmentSuccessfullyNotificationFailed As String = "Appointment is saved successfully but push notification failed."
        Public Shared RearrangeAppointmentSuccessfully As String = "Appointment is rearranged successfully."
        Public Shared RearrangeAppointmentSuccessfullyNotificationFailed As String = "Appointment is rearranged successfully but push notification failed."
        Public Shared SaveAmendAppointmentSuccessfully As String = "Appointment is amended successfully."
        Public Shared AppointmentTime As String = "Appointment start time shall always be less than end time."
        Public Shared duplicateAppointmentTime As String = "Another appointment already exist in selected Start/End time."
        Public Shared operativeNotAvailable As String = "Operative is not available in selected time slot."
#End Region

#Region "Customer Detail Custom Control Constants"
        Public Shared CustomControlErrorMsg As String = "Customer data does not exist."
#End Region

#Region "Scheme Block Detail Constant"
        Public Shared SchemeBlockHeaderControlErrorMsg As String = "Scheme &amp; Block data does not exist."
#End Region

#Region "Sub Contractor Report/ Update Popup(s)"

#Region "Sub Contractor Report"
        Public Shared FaultStatusUpdateSucessful As String = "Fault Status Updated Sucessfully."
#End Region

#Region "Save Notes Popup"

        'Headers Start

        Public Shared PopupHeaderSaveNotesCancelFault As String = "Cancel Fault"
        Public Shared PopupHeaderSaveNotesNoEntry As String = "No Entry"
        Public Shared PopupHeaderSaveNotesCompleteFault As String = "Confirm Repair Details"

        'Headers End

        'Heading Notes Start

        Public Shared PopupHeadingSaveNotesCancelFault As String = "Enter the reason for cancellation below:"
        Public Shared PopupHeadingSaveNotesNoEntry As String = "Enter the reason for no entry below:"
        Public Shared PopupHeadingSaveNotesCompleteFault As String = "Enter the notes for completion below:"

        Public Shared PopupSaveCancelFault As String = "Are you sure you want to Cancel the fault?"
        Public Shared PopupSaveNoEntryFault As String = "Are you sure you want to change status of this fault to No Entry?"
        Public Shared PopupSaveCompleteFault As String = "Are you sure you want to save the repair details?"
        Public Shared TimenotgreaterthancurrTime As String = "Time should not be greater than current time."
        'Heading Notes End

        'Save Message Notes Start

        Public Shared PopupSaveMsgSaveNotesCancelFault As String = "The fault has been canceled."
        Public Shared PopupSaveMsgSaveNotesNoEntry As String = "The fault has been set to no entry."
        Public Shared PopupSaveMsgSaveNotesCompleteFault As String = "The fault has been completed."

        'Save Message Notes End

        Public Shared PopupNotesErrorSaveNotes As String = "Please provide notes."

#End Region

#End Region

#Region "Fault Management Report / Page"

        Public Shared TradeAlreadyAdded = "The trade has already been added."
        Public Shared TradeDeletionError = "Selected trade can not be deleted, it is being used."
        Public Shared SelectLocation = "Please select location."
        Public Shared NoteLength = "Notes length should be less than 500"
        Public Shared SelectRepair = "Please select repair."
        Public Shared InvalidRepair = "Invalid repair."
        Public Shared SelectTrade = "Please select trade."
        Public Shared EnterFaultDescription = "Please enter description for the fault."
        Public Shared SpecialCharacterNotAllowed = "Special characters not allowed."
        Public Shared SelectFaultPriority = "Please select a priority from the list."
        Public Shared SelectFaultDuration = "Please select a duration from the list."
        Public Shared EnterNetAmount = "Please enter correct net amount or atleast 0."
        Public Shared EnterVatAmount = "Please enter correct vat amount or atleast 0."
        Public Shared EnterGrossAmount = "Please enter correct gross amount or atleast 0."
        Public Shared SelectVatRate = "Please select a Vat Rate."
        Public Shared SelectFaultRepair = "Please add repair."
        Public Shared AddFaultSuccess = "Fault has been saved successfully."
        Public Shared UpdateFaultNotesSuccess = "Fault info saved successfully."
        Public Shared AddFaultFailure = "Failed to save fault info."
        Public Shared FaultRepairAlreadyAdded = "The selected repair already exists against this fault."

#End Region

#Region "Job Sheet Summary Sub Contractor"
        Public Shared FaultSavedinvalidEmail As String = "Fault saved but unable to send email due to invalid email address"
        Public Shared FaultSavedEmailError As String = "Fault saved but unable to send email due to : "

#End Region

#Region " Tenant Information"

        Public Shared noTenantInformationFound As String = "No Tenant Information Found."

#End Region

#Region "Invalid Page Number"
        Public Shared InvalidPageNumber = "Kindly Provide a valid page number."
#End Region

#Region "Assign to Contractor"

        Public Const workRequiredCount = "Please add at least one work required."
        Public Const AssignedToContractor = "Work successfully assigned to Contractor."
        Public Const AssignedToContractorFailed = "Assign work to contractor has not successful."
        Public Const NoexpenditureError = "No expenditure setup for current fiscal year."
        Public Const EmailToContractor = "Email not sent to contractor."
        Public Const InvestigativeWorkInfo = "You have been issued a Purchase Order at zero value to carry out investigative works only in respect of the fault reported at the property. Once you have completed the works please submit your estimate to the supervisor at Broadland Repairs Service. Thank you"

#End Region

#Region "Add/Amned a repair"
        Public Const RepairAmendedSuccess = " Repair Amended Successfully."
        Public Const RepairAmendingException = "An Error occur while Amending a Repair."
        Public Const RepairDescriptionError = "Please enter description for the repair"
        Public Const RepairCostError = "Please enter net cost."
        Public Const RepairVatRateSelectionError = "Please select a Vat Rate."

        Public Const RepairAddingSuccess = " Repair Added Successfully."
        Public Const RepairAddingException = "An Error occur while Ading a Repair."
#End Region



    End Class
End Namespace

