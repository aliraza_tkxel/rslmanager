﻿Imports System
Imports FLS_BusinessObject
Imports FLS_DataAccess
Imports FLS_Utilities


Namespace FLS_BusinessLogic

    Public Class SchedularSearchBL
        Dim objSchedularSearchDAL As SchedularSearchDAL = New SchedularSearchDAL()

#Region "Functions"

#Region "Get Fault Basket Information"
        Public Sub getPatchesAndTrades(ByRef patchDataSet As DataSet, ByRef tradeDataSet As DataSet)

            objSchedularSearchDAL.GetPatchAndTradeInfo(patchDataSet, tradeDataSet)
            

        End Sub
#End Region


#Region "Get Schemes"
        Public Sub getSchemes(ByRef schemeDataSet As DataSet, ByVal patchId As Int32)

            objSchedularSearchDAL.GetSchemeInfo(schemeDataSet, patchId)
            
        End Sub
#End Region

#Region "Get Schedular Search Result"
        Public Function getSchedularSearchResult(ByRef resultDataSet As DataSet, ByVal objSchedularSearchBO As SchedularSearchBO, ByVal objPageSortBo As PageSortBO)

            Return objSchedularSearchDAL.GetSchedularSearchResult(resultDataSet, objSchedularSearchBO, objPageSortBo)

        End Function
#End Region

#End Region

    End Class

End Namespace