﻿Imports System
Imports FLS_BusinessObject
Imports FLS_DataAccess
Imports FLS_Utilities
Imports System.Collections.Specialized
Imports System.Web


Namespace FLS_BusinessLogic

    Public Class MasterPageBL

#Region "Get Property Page List"
        ''' <summary>
        ''' Get Property Page List
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="employeeId"></param>
        ''' <param name="selectedMenu"></param>
        ''' <remarks></remarks>
        Sub getPropertyPageList(ByRef resultDataSet As DataSet, ByVal employeeId As Integer, ByVal selectedMenu As String)
            Dim objMasterPageDAL As MasterPageDAL = New MasterPageDAL
            objMasterPageDAL.getPropertyPageList(resultDataSet, employeeId, selectedMenu)
        End Sub
#End Region

#Region "Check Page Access"
        ''' <summary>
        ''' Check Page Access
        ''' </summary>
        ''' <remarks></remarks>
        Function checkPageAccess(ByVal menu As String)

            Dim userId As Integer = SessionManager.getUserEmployeeId()
            Dim isAccessGranted As Boolean = False
            Dim pageId As Integer

            Dim resultDataset As DataSet = New DataSet
            getPropertyPageList(resultDataset, userId, menu)

            Dim accessGrantedModulesDt As DataTable = resultDataset.Tables(ApplicationConstants.AccessGrantedModulesDt)
            Dim accessGrantedMenusDt As DataTable = resultDataset.Tables(ApplicationConstants.AccessGrantedMenusDt)
            Dim accessGrantedPagesDt As DataTable = resultDataset.Tables(ApplicationConstants.AccessGrantedPagesDt)
            Dim randomPageDt As DataTable = resultDataset.Tables(ApplicationConstants.RandomPageDt)

            If (checkPageExist(accessGrantedPagesDt, pageId)) Then
                isAccessGranted = checkPageHierarchyAccessRights(accessGrantedModulesDt, accessGrantedMenusDt, accessGrantedPagesDt, pageId)
            ElseIf (checkPageExist(randomPageDt, pageId)) Then
                randomPageDt.Merge(accessGrantedPagesDt)
                isAccessGranted = checkPageHierarchyAccessRights(accessGrantedModulesDt, accessGrantedMenusDt, randomPageDt, pageId)
            End If

            Return isAccessGranted

        End Function

#End Region

#Region "Check Page Exist"
        ''' <summary>
        ''' Check Page Exist
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Function checkPageExist(ByVal pagesDt As DataTable, ByRef pageId As Integer)

            Dim pageFound As Boolean = False
            Dim pageFileName = HttpContext.Current.Request.Url.AbsolutePath.Substring(HttpContext.Current.Request.Url.AbsolutePath.LastIndexOf("/") + 1)
            Dim pageQueryString As NameValueCollection = HttpUtility.ParseQueryString(HttpContext.Current.Request.QueryString.ToString())

            Dim query = (From dataRow In pagesDt _
                       Where dataRow.Field(Of String)(ApplicationConstants.GrantPageFileNameCol).ToLower() = pageFileName.ToLower() _
                       Order By dataRow.Field(Of Integer)(ApplicationConstants.GrantPageAccessLevelCol) Descending _
                       Select dataRow)
            Dim resultPageDt As DataTable = pagesDt.Clone()


            If query.Count > 0 Then
                resultPageDt = query.CopyToDataTable()

                For Each row As DataRow In resultPageDt.Rows
                    Dim fileName As String = row(ApplicationConstants.GrantPageFileNameCol)
                    Dim url As String = row(ApplicationConstants.GrantPageQueryStringCol)
                    Dim queryString As NameValueCollection = HttpUtility.ParseQueryString(url)

                    If (queryString.Count = 0 And pageQueryString.Count = 0) Then
                        pageFound = True
                        pageId = row(ApplicationConstants.GrantPageIdCol)
                        Exit For
                    Else
                        Dim matchCount As Integer = 0
                        For Each key As String In queryString.AllKeys
                            If (queryString(key).Equals(pageQueryString(key))) Then
                                matchCount = matchCount + 1
                            End If
                        Next key

                        If (queryString.Count = matchCount) Then
                            pageId = row(ApplicationConstants.GrantPageIdCol)
                            pageFound = True
                            Exit For
                        End If

                    End If

                Next

            End If

            Return pageFound

        End Function

#End Region

#Region "Check Page Hierarchy Access Rights"
        ''' <summary>
        ''' Check Page Hierarchy Access Rights
        ''' First check in pages levels e.g (level 3 , level 2 , level 1)
        ''' Second check in menu
        ''' Third check in modules
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Function checkPageHierarchyAccessRights(ByVal modulesDt As DataTable, ByVal menusDt As DataTable, ByVal pagesDt As DataTable, ByVal pageId As Integer)

            Dim isAccessGranted As Boolean = False

            If (checkInPageLevelsAccessRights(pagesDt, pageId)) Then

                Dim pageExpression As String = ApplicationConstants.GrantPageIdCol + " = " + Convert.ToString(pageId)
                Dim pageRows() As DataRow = pagesDt.Select(pageExpression)
                Dim linkedMenuId As Integer = pageRows(0)(ApplicationConstants.GrantPageMenuIdCol)
                Dim linkedModuleId As Integer = pageRows(0)(ApplicationConstants.GrantPageModuleIdCol)

                Dim menuExpression As String = ApplicationConstants.GrantMenuMenuIdCol + " = " + Convert.ToString(linkedMenuId)
                Dim menuRows() As DataRow = menusDt.Select(menuExpression)

                If (menuRows.Count > 0) Then

                    Dim moduleExpression As String = ApplicationConstants.GrantModulesModuleIdCol + " = " + Convert.ToString(linkedModuleId)
                    Dim moduleRows() As DataRow = modulesDt.Select(moduleExpression)

                    If (moduleRows.Count > 0) Then
                        isAccessGranted = True
                    End If

                End If

            End If

            Return isAccessGranted
        End Function

#End Region

#Region "Check In Page Levels Access Rights"
        ''' <summary>
        ''' Check In Page Levels Access Rights from bottom to top
        ''' </summary>
        ''' <param name="pagesDt"></param>
        ''' <param name="pageId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Function checkInPageLevelsAccessRights(ByVal pagesDt As DataTable, ByVal pageId As Integer)

            Dim parentPageId As Integer
            Dim expression As String = ApplicationConstants.GrantPageIdCol + " = " + Convert.ToString(pageId)
            Dim rows() As DataRow = pagesDt.Select(expression)

            If (IsDBNull(rows(0)(ApplicationConstants.GrantPageParentPageCol)) Or _
                IsNothing(rows(0)(ApplicationConstants.GrantPageParentPageCol))) Then
                Return True
            Else
                parentPageId = rows(0)(ApplicationConstants.GrantPageParentPageCol)
                Dim parentExpression As String = ApplicationConstants.GrantPageIdCol + " = " + Convert.ToString(parentPageId)
                Dim parentRow() As DataRow = pagesDt.Select(parentExpression)

                If (parentRow.Count > 0) Then
                    Return checkInPageLevelsAccessRights(pagesDt, parentPageId)
                Else
                    Return False
                End If

            End If

        End Function

#End Region

    End Class

End Namespace
