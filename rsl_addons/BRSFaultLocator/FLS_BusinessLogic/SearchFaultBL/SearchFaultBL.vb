﻿Imports FLS_Utilities
Imports FLS_DataAccess

Namespace FLS_BusinessObject
    Public Class SearchFaultBL
        Public Sub getRecentFaults(ByRef resultDataSet As DataSet, ByRef propertyId As String, ByRef customerId As Integer, ByRef searchText As String, ByRef jsn As String)
            Dim objSerFaultDal As SearchFaultDAL = New SearchFaultDAL()            
            objSerFaultDal.getRecentFaults(resultDataSet, propertyId, customerId, searchText, jsn)            
        End Sub

        Public Sub getSbRecentFaults(ByRef resultDataSet As DataSet, ByRef schemeId As Integer, ByRef blockId As Integer, ByRef searchText As String, ByRef jsn As String)
            Dim objSerFaultDal As SearchFaultDAL = New SearchFaultDAL()
            objSerFaultDal.getSbRecentFaults(resultDataSet, schemeId, blockId, searchText, jsn)
        End Sub
    End Class
End Namespace