﻿Imports System
Imports FLS_BusinessObject
Imports System.Data.SqlClient
Imports FLS_DataAccess
Imports System.Web
Imports System.Drawing
Imports System.Web.UI.WebControls
Imports FLS_Utilities


Namespace FLS_BusinessLogic

    Public Class UsersBL
#Region "Attributes"
        Dim objUserDAL As UsersDAL = New UsersDAL()
#End Region

#Region "Functions"
        Function getEmployeeById(ByVal employeeId As Integer) As DataSet
            Return objUserDAL.getEmployeeById(employeeId)
        End Function
#End Region

    End Class

End Namespace
