﻿Imports System
Imports FLS_BusinessObject
Imports FLS_DataAccess
Imports FLS_Utilities


Namespace FLS_BusinessLogic



    Public Class RecallDetailsBL

#Region "Functions"

#Region "Get Recall Details"
        Public Sub getRecallDetails(ByRef resultDataSet As DataSet, ByVal faultId As Integer)

            Dim objFaultDAL As FaultDAL = New FaultDAL()
            objFaultDAL.GetRecallDetails(resultDataSet, faultId)


        End Sub
#End Region

#End Region



    End Class

End Namespace