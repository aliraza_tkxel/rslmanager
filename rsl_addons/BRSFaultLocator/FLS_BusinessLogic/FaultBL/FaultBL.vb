﻿Imports System
Imports FLS_BusinessObject
Imports FLS_DataAccess
Imports FLS_Utilities


Namespace FLS_BusinessLogic

    Public Class FaultBL
        Dim objFaultDAL As FaultDAL = New FaultDAL()
#Region "Functions"

#Region "Add More Fault Details"
        Public Function saveFaultInfo(ByVal objFaultBO As FaultBO)
            Return objFaultDAL.saveFaultInfo(objFaultBO)
        End Function
#End Region

#Region "Get Fault Basket Information"
        Public Sub getFaultBasket(ByVal objFaultBO As FaultBO, ByRef resultDataSet As DataSet)
            objFaultDAL.getFaultBasket(objFaultBO, resultDataSet)
        End Sub
#End Region

#Region "Get scheme/Block Fault Basket Information"
        Public Sub getSbFaultBasket(ByVal objFaultBO As FaultBO, ByRef resultDataSet As DataSet)

            objFaultDAL.getSbFaultBasket(objFaultBO, resultDataSet)


        End Sub
#End Region

#Region "Get Faults and Appointments"

        Public Function GetFaultsAndAppointments(ByVal customerId As Integer, ByVal propertyId As String) As DataSet
            Dim dsFaultsAndAppointments As DataSet = New DataSet()
            objFaultDAL.GetFaultsAndAppointments(dsFaultsAndAppointments, customerId, propertyId)
            Return dsFaultsAndAppointments

        End Function
#End Region

#Region "Get Warranties List"

        Public Function GetWarratiesList(ByVal propertyId As String) As DataSet
            Dim dsFaultsAndAppointments As DataSet = New DataSet()
            objFaultDAL.GetWarratiesList(dsFaultsAndAppointments, propertyId)
            Return dsFaultsAndAppointments

        End Function
#End Region

#Region "Get Faults and Appointments"

        Public Function GetFaultsAndAppointments(ByVal schemeId As Integer, ByVal blockId As Integer) As DataSet
            Dim dsFaultsAndAppointments As DataSet = New DataSet()
            objFaultDAL.GetFaultsAndAppointments(dsFaultsAndAppointments, schemeId, blockId)
            Return dsFaultsAndAppointments
        End Function
#End Region

#Region "Get Contractors By Trade"
        Public Sub getContractorsByTrade(ByVal objTradeType As String, ByRef resultDataSet As DataSet)

            objFaultDAL.GetContractorsByTrade(resultDataSet, objTradeType)


        End Sub
#End Region

#Region "Delete Fault from Basket"
        Public Function deleteFaultFromBasket(ByVal objTempFaultId As Integer)

            Return objFaultDAL.deleteFaultFromBasket(objTempFaultId)


        End Function
#End Region

#Region "Update Contractor in Basket"
        Public Sub updateContractor(ByVal faultID As Integer, ByVal orgID As Integer)

            objFaultDAL.updateContractorFromBasket(faultID, orgID)


        End Sub
#End Region

#Region "Get Appointment Detail"
        Public Function GetAppointmentDetail(ByVal faultLogId As Integer) As DataSet

            Dim dsAppointmentDetail As DataSet = New DataSet()
            objFaultDAL.getAppointmentDetail(dsAppointmentDetail, faultLogId)
            Return dsAppointmentDetail

        End Function
#End Region

#Region "Get Fault List"
        Public Sub getFaultsList(ByRef resultDataSet As DataSet, ByVal faultsListString As String)

            objFaultDAL.getFaultsList(resultDataSet, faultsListString)

        End Sub
#End Region

#Region "Get Appointment Data"
        Public Sub getAppointmentData(ByRef resultDataSet As DataSet, ByVal appointmentId As Integer)

            objFaultDAL.getAppointmentData(resultDataSet, appointmentId)

        End Sub
#End Region

#Region "Get Gas Appointment Data"
        Public Sub getGasAppointmentData(ByRef resultDataSet As DataSet, ByVal appointmentId As Integer)

            objFaultDAL.getGasAppointmentData(resultDataSet, appointmentId)

        End Sub
#End Region

#Region "Update Fault Notes"
        Public Function updateFaultNotes(ByVal text As String, ByVal accessNotes As String, ByVal appointmentId As Integer)

            Return objFaultDAL.updateFaultNotes(text, accessNotes, appointmentId)

        End Function
#End Region

#Region "Update Gas Notes"
        Public Function updateGasNotes(ByVal text As String, ByVal appointmentId As Integer)

            Return objFaultDAL.updateGasNotes(text, appointmentId)

        End Function
#End Region

#Region "Save Appointment Details"
        Public Sub saveAppointmentDetails(ByVal appointmentSummary As AppointmentSummaryBO, ByRef appointmentId As Integer, ByRef tempCount As Integer)

            objFaultDAL.saveAppointmentDetails(appointmentSummary, appointmentId, tempCount)

        End Sub
#End Region

#Region "Get Appointment Summary"
        Public Sub getAppointmentSummary(ByRef resultDataSet As DataSet, ByVal faultsList As String)

            objFaultDAL.getAppointmentSummary(resultDataSet, faultsList)

        End Sub
#End Region

#Region "Save Fault Cancellation"
        Public Function saveFaultCancellation(ByVal appointmentCancellation As AppointmentCancellationBO)

            Return objFaultDAL.saveFaultCancellation(appointmentCancellation)

        End Function
#End Region

#Region "Save Gas Servicing Cancellation"
        Public Function saveGasServicingCancellation(ByVal appointmentCancellation As AppointmentCancellationBO)

            Return objFaultDAL.saveGasServicingCancellation(appointmentCancellation)

        End Function
#End Region

#Region "Get Job Sheet Details"
        Public Sub getJobSheetDetails(ByRef resultDataSet As DataSet, ByVal jobSheetNumber As String)

            Dim objFaultDAL As FaultDAL = New FaultDAL()
            objFaultDAL.getJobSheetDetails(resultDataSet, jobSheetNumber)


        End Sub
#End Region

#Region "Get Subcontractor Job Sheet Details"
        Public Sub getSubcontractorJobSheetDetails(ByRef resultDataSet As DataSet, ByVal objTempFaultBO As TempFaultBO, ByVal propertyId As String)

            Dim objFaultDAL As FaultDAL = New FaultDAL()
            objFaultDAL.getSubcontractorJobSheetDetails(resultDataSet, objTempFaultBO, propertyId)

        End Sub
#End Region

#Region "Save Subsontractor fault Info"
        Public Function saveSubcontractorFaultInfo(ByVal objFaultBO As FaultBO, ByVal userId As Integer, ByVal tempfaultId As Integer)

            Dim objFaultDAL As FaultDAL = New FaultDAL()
            Return objFaultDAL.saveSubcontractorFault(objFaultBO, userId, tempfaultId)

        End Function
#End Region

#Region "Get More Details"
        Public Sub getMoreDetails(ByRef resultDataSet As DataSet, ByVal faultId As Integer)

            Dim objFaultDAL As FaultDAL = New FaultDAL()
            objFaultDAL.getMoreDetails(resultDataSet, faultId)


        End Sub
#End Region

#Region "Delete All Fault"
        Public Function deleteAllFault(ByVal tempFaultId As String) As Integer
            Return objFaultDAL.deleteAllFault(tempFaultId)
        End Function
#End Region

#Region "Get Job Sheed Summary Detail By JSN(Job Sheed Number)"

        Sub getJobSheetSummaryByJsn(ByRef ds As DataSet, ByRef jsn As String)
            objFaultDAL.getJobSheetSummaryByJsn(ds, jsn)
        End Sub

#End Region

#Region "Get Fault Stauts Look Up Values"

        Sub getFaultStatusLookUpSubContractor(ByRef LookUpList As List(Of LookUpBO))
            objFaultDAL.getFaultStatusLookUpSubContractor(LookUpList)
        End Sub

#End Region

#Region "Save/Set Fault Stauts in Database with Notes"

        Sub setFaultStatusSubContractorUpdate(ByRef faultBO As FaultBO)
            objFaultDAL.setFaultStatusSubContractorUpdate(faultBO)
        End Sub

#End Region

#Region "Cancel Fault Purchase Order"

        Public Function cancelFaultPurchaseOrder(ByRef faultBO As FaultBO)
            Return objFaultDAL.cancelFaultPurchaseOrder(faultBO)
        End Function

#End Region

#Region "Get Email Detail For Purchase Order"

        Sub getEmailDetail(ByRef faultBO As FaultBO, ByRef detailsForEmailDS As DataSet)
            objFaultDAL.getEmailDetail(faultBO, detailsForEmailDS)
        End Sub

#End Region

#Region "UpdateFaultNotes"
        Sub updateFaultNotes(ByVal tempFaultID As Integer, ByVal notes As String)
            objFaultDAL.updateFaultNotes(tempFaultID, notes)
        End Sub
#End Region
#Region "Save Fault Info for Rearrange"
        Public Function saveFaultInfoRearrange(ByVal objFaultBO As FaultBO)

            Return objFaultDAL.saveFaultInfoRearrange(objFaultBO)


        End Function

#End Region

#Region "Update Fault Info for Rearrange"

        Public Function updateFaultInfoRearrange(ByVal appointmentBO As AppointmentBO, ByVal faultLogidList As String)
            Return objFaultDAL.updateFaultInfoRearrange(appointmentBO, faultLogidList)
        End Function

#End Region

#Region "Get Confirmed fault data based on JSN"

        Public Sub getConfirmedFaultsByJSN(ByRef resultDataSet As DataSet, ByVal jsn As String)

            Dim objFaultDAL As FaultDAL = New FaultDAL()
            objFaultDAL.getConfirmedFaultsByJSN(resultDataSet, jsn)


        End Sub

#End Region

#Region "Get JobsheetNumber list by Appointment Id "
        Public Sub getJobsheetlistByAppointment(ByRef resultDataSet As DataSet, ByVal appointmentId As Integer)

            Dim objFaultDAL As FaultDAL = New FaultDAL()
            objFaultDAL.getJobsheetlistByAppointment(resultDataSet, appointmentId)

        End Sub
#End Region

#Region "Get RSLModules"
        ''' <summary>
        ''' Returns RSL Modules list
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="userId"></param>
        ''' <remarks></remarks>
        Public Sub getRSLModules(ByRef resultDataSet As DataSet, ByVal userId As String)
            objFaultDAL.getRSLModules(resultDataSet, Convert.ToInt32(userId))
        End Sub
#End Region


#Region "save SBAppointment Details"
        Sub saveSBAppointmentDetails(ByVal objappointmentSummaryBO As AppointmentSummaryBO, ByRef appointmentId As Integer, ByVal tempCount As Integer)
            objFaultDAL.saveSBAppointmentDetails(objappointmentSummaryBO, appointmentId, tempCount)
        End Sub
#End Region

#End Region
#Region "get SchemeBlock Address"
        Sub getSchemeBlockAddress(ByRef resultDataSet As DataSet, ByVal schemeId As Integer, ByVal blockId As Integer)
            objFaultDAL.getSchemeBlockAddress(resultDataSet, schemeId, blockId)
        End Sub

#End Region
      
#Region "Get Job Sheet Details"
        Public Sub getSbJobSheetDetails(ByRef resultDataSet As DataSet, ByVal jobSheetNumber As String)

            Dim objFaultDAL As FaultDAL = New FaultDAL()
            objFaultDAL.getSbJobSheetDetails(resultDataSet, jobSheetNumber)


        End Sub
#End Region

#Region "Get Sb Job Sheed Summary Detail By JSN(Job Sheed Number)"

        Sub getSbJobSheetSummaryByJsn(ByRef ds As DataSet, ByRef jsn As String)
            objFaultDAL.getJobSheetSummaryByJsn(ds, jsn)
        End Sub

#End Region

#Region "Get Sb Subcontractor Job Sheet Details"
        Public Sub getSubcontractorJobSheetDetails(ByRef resultDataSet As DataSet, ByVal objTempFaultBO As TempFaultBO)

            Dim objFaultDAL As FaultDAL = New FaultDAL()
            objFaultDAL.getSubcontractorJobSheetDetails(resultDataSet, objTempFaultBO)

        End Sub
#End Region

#Region "Save Sub contractor fault Info"
        Public Function saveSbSubcontractorFaultInfo(ByVal objFaultBO As FaultBO, ByVal userId As Integer, ByVal tempfaultId As Integer)

            Dim objFaultDAL As FaultDAL = New FaultDAL()
            Return objFaultDAL.saveSbSubcontractorFault(objFaultBO, userId, tempfaultId)

        End Function
#End Region

#Region "Save Sub contractor fault Info"
        Public Function saveSubcontractorFaultInfo(ByVal contractorId As Integer, ByVal userId As Integer, ByVal tempfaultId As Integer, ByRef jsn As String)

            Dim objFaultDAL As FaultDAL = New FaultDAL()
            Return objFaultDAL.saveSubcontractorFault(contractorId, userId, tempfaultId, jsn)

        End Function
#End Region

#Region "Get Operative Info"
        ''' <summary>
        ''' Get Operative Info
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="operaiveId"></param>
        ''' <remarks></remarks>
        Sub getOperativeInformation(ByRef resultDataSet As DataSet, ByVal operaiveId As Integer)
            Dim objFaultDAL As FaultDAL = New FaultDAL()
            objFaultDAL.getOperativeInformation(resultDataSet, operaiveId)
        End Sub
#End Region

#Region "Get Fault Assign To Contractor"

        Public Sub getFaultAssignToContractor(ByRef resultDataSet As DataSet, ByVal templFaultId As Int32)
            Dim objFaultDAL As FaultDAL = New FaultDAL()
            objFaultDAL.getFaultAssignToContractor(resultDataSet, templFaultId)
        End Sub

#End Region

    End Class

End Namespace

