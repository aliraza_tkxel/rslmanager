﻿Imports System
Imports FLS_BusinessObject
Imports FLS_DataAccess
Imports FLS_Utilities

Namespace FLS_BusinessLogic

    Public Class FaultManagementBL

#Region "Attributes"
        Dim faultManagementDAL As New FaultManagementDAL
#End Region

#Region "Fault Management Search"

        Public Function GetFaultSearchRowCount(ByVal search As String) As Integer
            Dim rowCount As Integer = 0

            rowCount = faultManagementDAL.getFaultManagementSearchRowCount(search)

            Return rowCount
        End Function

        Public Sub GetFaultSearchData(ByRef faultDS As DataSet, ByVal search As String, ByRef pageSortBO As PageSortBO)
            faultManagementDAL.GetFaultSearchData(faultDS, search, pageSortBO)
        End Sub

#End Region

#Region "Get Vat Values (For Add/Amend Fault)"

        Public Sub GetVatValues(ByRef vatBO As VatBO)
            faultManagementDAL.getVatValues(vatBO)
        End Sub

#End Region

#Region "Add New Fault"

        Public Sub AddNewFault(ByRef faultBO As FaultBO)
            faultManagementDAL.addNewFault(faultBO)
        End Sub

#End Region

#Region "Amend A Fault"

        Public Sub AmendFault(ByRef faultBO As FaultBO)
            faultManagementDAL.amendFault(faultBO)
        End Sub

#End Region

#Region "Save fault info"

        Public Sub saveFaultInfo(ByRef objFaultBO As FaultBO)
            faultManagementDAL.saveFaultInfo(objFaultBO)
        End Sub

#End Region

#Region "Get Repair Search Result"
        ''' <summary>
        ''' Get Repair Search Result
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="search"></param>
        ''' <remarks></remarks>
        Public Sub getRepairSearchResult(ByRef resultDataSet As DataSet, ByVal search As String)
            faultManagementDAL.getRepairSearchResult(resultDataSet, search)
        End Sub
#End Region

#Region "Get Fault Values"

        Sub GetFaultValues(ByRef faultBO As FaultBO)
            faultManagementDAL.getFaultValues(faultBO)
        End Sub

#End Region

#Region "Get Lookup Values"

        Public Sub getAreaLookUpValuesAll(ByRef lstLookup As List(Of LookUpBO))
            faultManagementDAL.getAreaLookUpValuesAll(lstLookup)
        End Sub

        Public Sub getRepairsLookUpValues(ByRef lstLookup As List(Of LookUpBO))
            faultManagementDAL.getRepairsLookUpValues(lstLookup)
        End Sub

        Public Sub getPriorityLookUpValuesAll(ByVal lstLookUp As List(Of LookUpBO))
            faultManagementDAL.getPriorityLookUpValuesAll(lstLookUp)
        End Sub

        Public Sub getTradeLookUpValuesAll(ByRef lstLookUp As List(Of LookUpBO))
            faultManagementDAL.getTradeLookUpValuesAll(lstLookUp)
        End Sub

        Public Sub getVatRateLookUpValuesAll(ByRef lstLookUp As List(Of LookUpBO))
            faultManagementDAL.getVatRateLookUpValuesAll(lstLookUp)
        End Sub

#End Region

#Region "Check fault trade in use"

        Function checkFaultTradeInUse(ByVal faultTradeId As Integer) As Boolean
            Return faultManagementDAL.checkFaultTradeInUse(faultTradeId)
        End Function

#End Region

    End Class

End Namespace
