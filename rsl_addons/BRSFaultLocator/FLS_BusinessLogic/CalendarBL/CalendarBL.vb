﻿Imports System
Imports FLS_BusinessObject
Imports FLS_DataAccess
Imports FLS_Utilities


Namespace FLS_BusinessLogic

    Public Class CalendarBL
        Dim objCalendarDAL As CalendarDAL = New CalendarDAL()

#Region "Functions"

#Region "get Engineers Info for Appointment"
        Public Sub getFaultEngineersForAppointment(ByVal objCalendarAppointmentsBO As CalendarAppointmentsBO, ByRef resultDataSet As DataSet)

            objCalendarDAL.getFaultEngineersForAppointment(objCalendarAppointmentsBO, resultDataSet)


        End Sub
#End Region

#Region "get Searched Engineers Info for Appointment"

        Public Sub getSearchedFaultEngineersForAppointment(ByVal objCalendarAppointmentsBO As CalendarAppointmentsBO, ByRef resultDataSet As DataSet)
            objCalendarDAL.getSearchedFaultEngineersForAppointment(objCalendarAppointmentsBO, resultDataSet)
        End Sub

#End Region

#Region "get Engineer Leaves Details"
        Public Sub getNewlyCreatedAppointmentDetail(ByVal objCalendarAppointmentsBO As CalendarAppointmentsBO, ByRef resultDataSet As DataSet)

            objCalendarDAL.getNewlyCreatedAppointmentDetail(objCalendarAppointmentsBO, resultDataSet)

        End Sub
#End Region

#Region "get Property Patches"
        Public Sub getPatchLocations(ByVal index As Int32, ByVal userTypeList As List(Of CalendarAppointmentsBO))
            objCalendarDAL.getPatchLocations(index, userTypeList)
        End Sub
#End Region

#Region "get Trades"

        Public Sub getTrades(ByVal index As Int32, ByVal userTypeList As List(Of CalendarAppointmentsBO))
            objCalendarDAL.getTrades(index, userTypeList)
        End Sub

#End Region

#Region "get Appointments Details for Calendar"

        Public Sub getFaultAppointmentsCalendarDetail(ByVal objCalendarAppointmentsBO As CalendarAppointmentsBO, ByRef resultDataSet As DataSet)
            objCalendarDAL.getFaultAppointmentsCalendarDetail(objCalendarAppointmentsBO, resultDataSet)
        End Sub

#End Region

#Region "get Employee Core Working Hours"
        Sub getEmployeeCoreWorkingHours(ByRef resultDataSet As DataSet, ByVal employeeIds As String)
            objCalendarDAL.getEmployeeCoreWorkingHours(resultDataSet, employeeIds)
        End Sub
#End Region

#Region "Save New Appointment From Calendar"
        Public Function saveAppointmentInformation(ByVal appointmentSummary As AppointmentSummaryBO) As Integer
            Return objCalendarDAL.saveAppointmentInformation(appointmentSummary)
        End Function
#End Region

#Region "Save New Appointment From Calendar"
        Public Function saveSbAppointmentInformation(ByVal appointmentSummary As AppointmentSummaryBO)

            Return objCalendarDAL.saveSbAppointmentInformation(appointmentSummary)

        End Function
#End Region

#Region "get Newly Created Appointment Details"
        Public Sub getEngineerLeavesDetail(ByVal objCalendarAppointmentsBO As CalendarAppointmentsBO, ByRef resultDataSet As DataSet)

            objCalendarDAL.getEngineerLeavesDetail(objCalendarAppointmentsBO, resultDataSet)

        End Sub
#End Region

#Region "Get Tenant(s) Info by TenancyId - Joint Tenancy"

        Sub GetJointTenantsInfoByTenancyID(ByRef dstenantsInfo As DataSet, ByRef tenancyID As Integer)
            objCalendarDAL.GetJointTenantsInfoByTenancyID(dstenantsInfo, tenancyID)
        End Sub

#End Region

#Region "Get PropertyID and TradeID of an Appointmet by giving AppointmetID"

        Public Sub getPidTidbyAppointmentID(ByVal currentAppointmentID As Integer, ByRef calendarAppointmentsBO As CalendarAppointmentsBO)
            objCalendarDAL.getPidTidbyAppointmentID(currentAppointmentID, calendarAppointmentsBO)
        End Sub

#End Region

#Region "Get Operatives filtered by trade"

        Public Sub getOperativeFilteredByTradeId(ByRef operatives As List(Of DropDownBO), Optional ByVal tradeId As Integer = -1)
            Dim calendarDal As New CalendarDAL()
            calendarDal.getOperativeFilteredByTradeId(operatives, tradeId)
        End Sub

#End Region


#Region "Get Operatives filtered by trade"

        Public Sub GetAppointmentType(ByRef appointmentType As List(Of DropDownBO))
            Dim calendarDal As New CalendarDAL()
            calendarDal.GetAppointmentType(appointmentType)
        End Sub

#End Region

#Region "Get Property Calendar Details"

        Public Sub getPropertyCalendarDetails(ByRef objCalendarAppointmentsBO As CalendarAppointmentsBO, ByRef calendarDetailsDataSet As DataSet)
            Dim calendarDal As New CalendarDAL()
            calendarDal.getPropertyCalendarDetails(objCalendarAppointmentsBO, calendarDetailsDataSet)
        End Sub

#End Region

#Region "Get Appointment Notes"

        Public Sub getAppointmentNotes(ByRef dsNotes As DataSet, ByVal appointmentId As Int32, ByVal aptType As String)
            objCalendarDAL.getAppointmentNotes(dsNotes, appointmentId, aptType)
        End Sub

#End Region

#End Region

    End Class

End Namespace

