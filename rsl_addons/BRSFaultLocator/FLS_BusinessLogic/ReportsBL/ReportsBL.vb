﻿Imports System
Imports FLS_Utilities
Imports FLS_DataAccess
Imports FLS_BusinessObject

Namespace FLS_BusinessLogic

    Public Class ReportsBL

#Region "Functions"

#Region "Get Follow On List"

        Public Function getFollowOnList(ByRef resultDataSet As DataSet, ByVal search As String, ByRef objPageSortBo As PageSortBO, ByVal schemeId As Integer, ByVal blockId As Integer, ByVal financialYear As Integer, ByVal followOnStatusId As Integer)
            Dim objReportsDAL As New ReportsDAL
            Return objReportsDAL.getFollowOnList(resultDataSet, search, objPageSortBo, schemeId, blockId, financialYear, followOnStatusId)
        End Function

#End Region


#Region "Delete Follow on work"
        Public Sub DeleteFollowOnWork(ByVal followOnId As Integer, ByVal faultLogId As Integer)
            Dim objReportsDAL As New ReportsDAL
            objReportsDAL.DeleteFollowOnWork(followOnId, faultLogId)
        End Sub
#End Region



#Region "Get Subcontractor List"

        Public Function getSubcontractorList(ByRef resultDataSet As DataSet, ByVal search As String, ByRef objPageSortBo As PageSortBO, ByVal statusId As Integer, ByVal schemeId As Integer, ByVal blockId As Integer, ByVal financialYear As Integer)
            Dim objReportsDAL As ReportsDAL = New ReportsDAL
            Return objReportsDAL.getSubcontractorList(resultDataSet, search, objPageSortBo, statusId, schemeId, blockId, financialYear)
        End Function

#End Region

#Region "Get FollowOn Work Detail"

        Public Sub getFollowOnWorkDetail(ByRef resultDataSet As DataSet, ByVal jobSheetNumber As String)
            Dim objReportsDAL As ReportsDAL = New ReportsDAL
            objReportsDAL.getFollowOnWorkDetail(resultDataSet, jobSheetNumber)
        End Sub

#End Region

#Region "Get No Entry List"

        Public Function getNoEntryList(ByRef resultDataSet As DataSet, ByVal search As String, ByRef objPageSortBo As PageSortBO, ByVal schemeId As Integer, ByVal blockId As Integer, ByVal financialYear As Integer)
            Dim objReportsDAL As ReportsDAL = New ReportsDAL
            Return objReportsDAL.getNoEntryList(resultDataSet, search, objPageSortBo, schemeId, blockId, financialYear)
        End Function

#End Region

#Region "Get No Entry Detail"

        Public Sub getNoEntryDetail(ByRef resultDataSet As DataSet, ByVal jobSheetNumber As String)
            Dim objReportsDAL As ReportsDAL = New ReportsDAL
            objReportsDAL.getNoEntryDetail(resultDataSet, jobSheetNumber)
        End Sub

#End Region

#Region "Get Operative List"

        Public Sub getOperativeList(ByRef resultDataSet As DataSet)
            Dim objReportsDAL As ReportsDAL = New ReportsDAL
            objReportsDAL.getOperativeList(resultDataSet)
        End Sub

#End Region

#Region "Get Paused Fault Operative List"

        Public Sub getPausedOperativeList(ByRef resultDataSet As DataSet)
            Dim objReportsDAL As ReportsDAL = New ReportsDAL
            objReportsDAL.getPausedOperativeList(resultDataSet)
        End Sub

#End Region

#Region "Get Recall List"

        Public Function getRecallList(ByRef resultDataSet As DataSet, ByVal search As String, ByVal operativeId As Integer, ByRef objPageSortBo As PageSortBO, ByVal schemeId As Integer, ByVal blockId As Integer, ByVal financialYear As Integer)
            Dim objReportsDAL As ReportsDAL = New ReportsDAL
            Return objReportsDAL.getRecallList(resultDataSet, search, operativeId, objPageSortBo, schemeId, blockId, financialYear)
        End Function

#End Region

#Region "Get Recall Detail By Original FaultLogID"

        Public Sub getRecallDetailByFaultLogID(ByRef resultDataset As DataSet, ByRef faultLogID As Integer)
            Dim objReportsDAL As ReportsDAL = New ReportsDAL
            objReportsDAL.getRecallDetailByFaultLogID(resultDataset, faultLogID)
        End Sub

#End Region

#Region "Get Paused Fault List"

        Public Function getPausedFaultList(ByRef resultDataSet As DataSet, ByVal search As String, ByVal operativeId As Integer, ByRef objPageSortBo As PageSortBO, ByVal schemeId As Integer, ByVal blockId As Integer, ByVal financialYear As Integer)
            Dim objReportsDAL As ReportsDAL = New ReportsDAL
            Return objReportsDAL.getPausedFaultList(resultDataSet, search, operativeId, objPageSortBo, schemeId, blockId, financialYear)
        End Function

#End Region

#Region "Get Repair Search Result"

        ''' <summary>
        ''' Get Property Search Result
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="search"></param>
        ''' <remarks></remarks>
        Public Sub getRepairSearchResult(ByRef resultDataSet As DataSet, ByVal search As String)
            Dim objReportsDAL As ReportsDAL = New ReportsDAL
            objReportsDAL.getRepairSearchResult(resultDataSet, search)
        End Sub

#End Region

#Region "Insert Repair List Data"

        ''' <summary>
        ''' Insert Repair List Data
        ''' </summary>
        ''' <param name="objRepairListBo"></param>
        ''' <remarks></remarks>
        Public Sub InsertRepairListData(ByRef objRepairListBo As RepairListBO)
            Dim objReportsDAL As ReportsDAL = New ReportsDAL
            objReportsDAL.InsertRepairListData(objRepairListBo)
        End Sub

#End Region

#Region "Get Schemes for Scheme Dropdown"

        Sub loadSchemeDropDown(ByRef schemeDataSet As DataSet)
            Dim objReportsDAL As ReportsDAL = New ReportsDAL()
            objReportsDAL.loadSchemeDropDown(schemeDataSet)
        End Sub

#End Region

#Region "Get blocks for a scheme by SchemeId"

        Sub loadBlockDropdownbySchemeId(ByRef blockDataSet As DataSet, ByVal schemeId As Integer)
            Dim objReportsDAL As ReportsDAL = New ReportsDAL()
            objReportsDAL.loadBlockDropdownbySchemeId(blockDataSet, schemeId)
        End Sub

#End Region

#Region "Get Reactive Repair List"

        Public Function getReactiveRepairList(ByRef resultDataSet As DataSet, ByVal search As String, ByRef objPageSortBo As PageSortBO, Optional ByRef isFullList As Boolean = False)
            Dim objReportsDAL As ReportsDAL = New ReportsDAL
            Return objReportsDAL.getReactiveRepairList(resultDataSet, search, objPageSortBo, isFullList)
        End Function

#End Region


#Region "Amend a Repair"

        Public Sub AmendRepair(ByRef objRepaisBo As RepairsBO)
            Dim objReportsDAL As ReportsDAL = New ReportsDAL
            objReportsDAL.AmendRepair(objRepaisBo)
        End Sub

#End Region

#Region "Add a New Repair"

        Public Sub AddNewRepair(ByRef objRepaisBo As RepairsBO)
            Dim objReportsDAL As ReportsDAL = New ReportsDAL
            objReportsDAL.AddNewRepair(objRepaisBo)
        End Sub

#End Region


#Region "Get Associated faults List"

        Public Function getAssociatedFaultList(ByRef resultDataSet As DataSet, ByVal repairID As Integer, ByRef objPageSortBo As PageSortBO)
            Dim objReportsDAL As ReportsDAL = New ReportsDAL
            Return objReportsDAL.getAssociatedFaultList(resultDataSet, repairID, objPageSortBo)
        End Function

#End Region

#Region "Get Reactive Repair List for Export to excel"

        Public Sub getReactiveRepairListExportToExcel(ByRef resultDataSet As DataSet, ByVal search As String)
            Dim objReportsDAL As ReportsDAL = New ReportsDAL
            objReportsDAL.getReactiveRepairListExportToExcel(resultDataSet, search)
        End Sub

#End Region
#End Region

    End Class

End Namespace
