﻿Imports FLS_Utilities
Imports FLS_DataAccess
Imports System.Text
Imports System.Globalization
Imports Microsoft.Practices.EnterpriseLibrary.Logging

Namespace FLS_BusinessObject
    Public Class FaultAppointmentBL

        Dim appointmentDueDate As Date


#Region "get Available Operatives"

        ''' <summary>
        ''' This function 'll get the available operatives against faults
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="tempFaultIds"></param>
        ''' <remarks></remarks>
        Public Sub getAvailableOperatives(ByRef resultDataSet As DataSet, ByRef tempFaultIds As StringBuilder, ByRef isRecall As Boolean)

            Dim objFaultAppointmentDal As FaultAppointmentDAL = New FaultAppointmentDAL()
            objFaultAppointmentDal.getAvailableOperatives(resultDataSet, tempFaultIds, isRecall)

        End Sub

        ''' <summary>
        ''' This function 'll get the available operatives against faults log id
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="FaultIds"></param>
        ''' <param name="propertyId"></param>
        ''' <remarks></remarks>
        Public Sub getAvailableOperativesForRearrange(ByRef resultDataSet As DataSet, ByRef faultIds As String, ByRef propertyId As String)

            Dim objFaultAppointmentDal As FaultAppointmentDAL = New FaultAppointmentDAL()
            objFaultAppointmentDal.getAvailableOperativesForRearrange(resultDataSet, faultIds, propertyId)

        End Sub

#End Region

#Region "Get Temporary Fault Basket"
        ''' <summary>
        ''' 'Get Temporary Fault Basket
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="tempFaultIds"></param>
        ''' <remarks></remarks>
        Public Sub getTemporaryFaultBasket(ByRef resultDataSet As DataSet, ByRef tempFaultIds As StringBuilder, ByRef tempFaultTradeIds As StringBuilder, ByRef propertyId As String, ByVal customerId As Integer, ByVal isRecall As Boolean)

            Dim objFaultAppointmentDal As FaultAppointmentDAL = New FaultAppointmentDAL()
            objFaultAppointmentDal.getTemporaryFaultBasket(resultDataSet, tempFaultIds, tempFaultTradeIds, propertyId, customerId, isRecall)

        End Sub
#End Region
#Region "Get Sb Temporary Fault Basket"
        ''' <summary>
        ''' 'Get Temporary Fault Basket
        ''' </summary>
        ''' <param name="resultDataSet"></param>
        ''' <param name="tempFaultIds"></param>
        ''' <remarks></remarks>

        Public Sub getTemporaryFaultBasket(ByRef resultDataSet As DataSet, ByRef tempFaultIds As StringBuilder, ByRef tempFaultTradeIds As StringBuilder, ByRef schemeId As Integer, ByVal blockId As Integer, ByVal isRecall As Boolean)

            Dim objFaultAppointmentDal As FaultAppointmentDAL = New FaultAppointmentDAL()
            objFaultAppointmentDal.getTemporaryFaultBasket(resultDataSet, tempFaultIds, tempFaultTradeIds, schemeId, blockId, isRecall)

        End Sub
#End Region

#Region "Get Newly Created Appointment"

        ''' <summary>
        ''' This function 'll get the newly created Appointments
        ''' </summary>
        ''' <param name="propertyId"></param>
        ''' <param name="customerId"></param>
        ''' <param name="appointmentId"></param>
        ''' <remarks></remarks>
        Public Sub getNewlyCreatedAppointment(ByRef resultDataSet As DataSet, ByVal propertyId As String, ByVal customerId As Integer, ByVal appointmentId As Integer)

            Dim objFaultAppointmentDal As FaultAppointmentDAL = New FaultAppointmentDAL()
            objFaultAppointmentDal.getNewlyCreatedAppointment(resultDataSet, propertyId, customerId, appointmentId)

        End Sub
#End Region

#Region "get Confirmed Faults Appointments"
        ''' <summary>
        ''' This function creates the data table of confirmed faults and their confirmed appointments
        ''' </summary>
        ''' <param name="tempFaultDs"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function getConfirmedFaultsAppointments(ByVal tempFaultDs As DataSet)
            'this object 'll contain the list of faults and appointment against them
            Dim faultAptBoList As List(Of ConfirmFaultAppointmentBO) = New List(Of ConfirmFaultAppointmentBO)
            'this flag 'll tell either this fault should be added in new list of faults
            Dim addFaultInList As Boolean = True
            'this flag 'll tell either this appointment should be added in new list of appointments
            Dim addAppointmentInList As Boolean = True
            'these two variables 'll help to identify either the fault belongs to same appointment or next appointment
            Dim nextAppointId As Integer = 0
            Dim currentAppointId As Integer = 0
            Dim rowCount As Integer = 0
            Dim faultAptBo As ConfirmFaultAppointmentBO = New ConfirmFaultAppointmentBO()

            For Each confrimFaultDr As DataRow In tempFaultDs.Tables(ApplicationConstants.ConfirmFaultDataTable).Rows
                'get the appointment id from the current row
                currentAppointId = Convert.ToInt32(confrimFaultDr.Item("AppointmentId"))
                'increase the row count to get the appointment id from next row
                rowCount = rowCount + 1
                'if its not the last row then get the appointment id of next row
                If (rowCount) <= tempFaultDs.Tables(ApplicationConstants.ConfirmFaultDataTable).Rows.Count() - 1 Then
                    'get the appointment id of next row
                    nextAppointId = tempFaultDs.Tables(ApplicationConstants.ConfirmFaultDataTable).Rows(rowCount).Item("AppointmentId")
                Else
                    'if its last row then next appointment id value 'll be same as current row's appointment id
                    nextAppointId = currentAppointId
                End If

                'Below mentioned IF BLOCK shows decides either to show operative or not based on APPOINTMENT ID                            
                If (nextAppointId = currentAppointId And rowCount = (tempFaultDs.Tables(ApplicationConstants.ConfirmFaultDataTable).Rows.Count())) Then
                    'If its last row then add fault of current row in new fault list
                    'and also add the appointment of current row in new appointment list
                    addFaultInList = True
                    addAppointmentInList = True
                ElseIf (nextAppointId = currentAppointId) Then
                    'If previous row appointment id is same as current row's appointmen id then add fault of current row in new fault list
                    'and do not add the appointment in new appointment list
                    addFaultInList = True
                    addAppointmentInList = False
                ElseIf (nextAppointId <> currentAppointId) Then
                    addFaultInList = True
                    addAppointmentInList = True
                End If

                If addFaultInList = True Then
                    faultAptBo.confirmFaultDtBo.jsn = "JS" + confrimFaultDr.Item("FaultLogId").ToString()
                    faultAptBo.confirmFaultDtBo.location = confrimFaultDr.Item("AreaName") + " > " + "<span><b>" + confrimFaultDr.Item("Description") + "</b></span>"
                    faultAptBo.confirmFaultDtBo.duration = IIf(IsDBNull(confrimFaultDr.Item("DurationString")) = True, "N/A", confrimFaultDr.Item("DurationString"))
                    faultAptBo.confirmFaultDtBo.trade = confrimFaultDr.Item("TradeName")
                    faultAptBo.confirmFaultDtBo.dueDate = confrimFaultDr.Item("DueDate").ToString()
                    faultAptBo.confirmFaultDtBo.addNewDataRow()
                End If

                If addAppointmentInList = True Then
                    faultAptBo.confirmAppointmentDtBo.operative = confrimFaultDr.Item("OperativeName")
                    faultAptBo.confirmAppointmentDtBo.appointmentStartDateTime = confrimFaultDr.Item("StartTime").ToString() + " " + CType(confrimFaultDr.Item("AppointmentDate"), Date).ToString(ApplicationConstants.CustomLongDateFormat, CultureInfo.CreateSpecificCulture("en-US"))
                    faultAptBo.confirmAppointmentDtBo.appointmentEndDateTime = confrimFaultDr.Item("EndTime").ToString() + " " + CType(confrimFaultDr.Item("AppointmentEndDate"), Date).ToString(ApplicationConstants.CustomLongDateFormat, CultureInfo.CreateSpecificCulture("en-US"))
                    faultAptBo.confirmAppointmentDtBo.lastColumn = "Confirmed"
                    faultAptBo.confirmAppointmentDtBo.addNewDataRow()
                    faultAptBoList.Add(faultAptBo)
                    'once appointment is added in list then create a new item 
                    'for new list of faults and appointment against them
                    faultAptBo = New ConfirmFaultAppointmentBO()
                End If
            Next

            Return faultAptBoList
        End Function
#End Region

#Region "Temporary Faults And Appointments"

#Region "add More Appointments"
        ''' <summary>
        ''' This function 'll do multiple tasks. It filters operatives and creates appointment slots based on leaves, appointments and due date
        ''' </summary>
        ''' <remarks></remarks>
        Public Sub addMoreAppointments(ByVal operativesDt As DataTable, ByVal leavesDt As DataTable, ByVal appointmentsDt As DataTable, ByVal customerBo As CommonAddressBO, ByRef tempFaultAptBo As TempFaultAppointmentBO)
            Dim filteredOperativesDt As DataTable = New DataTable()

            'If patch check box is selected this function will sort out the operatives who have same customer's property patch 
            'otherwise if patch is unchecked it will return all the operatives
            filteredOperativesDt = Me.applyPatchFilter(operativesDt, tempFaultAptBo, customerBo.PatchId)

            'Filter for operatives that have same trade as of current fault (operative_filter = fault_filter)
            'Provide the already filtered operatives for patchid to filter more for tradeid.
            'Note: Return result into same data table(variable) there is no need to use a data table(variable).
            filteredOperativesDt = applyTradeFilter(filteredOperativesDt, tempFaultAptBo.TradeId)

            'Filter for operatives that have IsGasSafe & IsOftec set as true in the operative detail screen
            filteredOperativesDt = applyGasSafeAndOftecFilter(filteredOperativesDt, tempFaultAptBo)

            ' This function 'll filtered the already created slots based on due date
            Me.applyDueDateFilter(tempFaultAptBo)

            'fetch the previous operative id so the operatives name should appear in order
            Dim previousOperativeId As Integer = 0
            Dim appointments = tempFaultAptBo.tempAppointmentDtBo.dt.AsEnumerable()
            Dim appResult = (From app In appointments Where app(TempAppointmentDtBO.lastAddedColName) = True Select app)
            If appResult.Count() > 0 Then
                previousOperativeId = appResult.LastOrDefault.Item(TempAppointmentDtBO.operativeIdColName)
            End If
            Dim startDate As DateTime = Convert.ToDateTime(tempFaultAptBo.FromDate)
            'This function 'll create the appointment slots based on operatives, appointments, leaves and due date 
            Me.createTempAppointmentSlots(filteredOperativesDt, appointmentsDt, leavesDt, tempFaultAptBo, customerBo, previousOperativeId, startDate)
        End Sub
#End Region

#Region "get Temp Faults Appointments"
        ''' <summary>
        ''' This function 'll group the faults by trade and 'll fill up the list of tempFaultAppointmentBO
        ''' </summary>
        ''' <param name="tempFaultDt"></param>
        ''' <param name="operativesDt"></param>
        ''' <param name="leavesDt"></param>
        ''' <param name="appointmentsDt"></param>
        ''' <param name="groupBy"></param>
        ''' <param name="customerBo"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function getTempFaultsAppointments(ByVal tempFaultDt As DataTable, ByVal operativesDt As DataTable, ByVal leavesDt As DataTable, ByVal appointmentsDt As DataTable, ByVal groupBy As Boolean, ByVal customerBo As CommonAddressBO)
            Dim objFaultAppointmentBl As FaultAppointmentBL = New FaultAppointmentBL()
            Dim tempFaultAptBo As TempFaultAppointmentBO = New TempFaultAppointmentBO()
            Dim tempFaultAptBoList As List(Of TempFaultAppointmentBO) = New List(Of TempFaultAppointmentBO)
            Dim currentFaultTradeId As Integer = 0
            'these two variables 'll help to identify either the fault belongs to same appointment or next appointment
            Dim currentTradeId As Integer = 0
            Dim nextTradeId As Integer = 0
            Dim faultCount As Integer = 0
            Dim isFaultGasSafe As Boolean = False
            Dim isFaultOftec As Boolean = False
            'this flag 'll tell either this fault should be added in new list of faults
            Dim addFaultInList As Boolean = True
            'this flag 'll tell either this appointment should be added in new list of appointments
            Dim addAppointmentInList As Boolean = True
            Dim faultDuration As Double = 0


            'Loop through faults           
            For Each tempFaultDr As DataRow In tempFaultDt.Rows
                'save fault count. This 'll help us to check either we should access next row or not
                faultCount = faultCount + 1
                currentTradeId = Convert.ToInt32(tempFaultDr.Item("TradeId"))
                currentFaultTradeId = tempFaultDr.Item("FaultTradeId")

                'Check that either fault is gas safe or not this 'll help us to determine the operatives.
                'In case of gas safe only operatives with gas safe status 'll be displayed
                If CType(tempFaultDr.Item("isGasSafe"), Boolean) = True Then
                    isFaultGasSafe = True
                End If
                'Check that either fault is oftec or not. This will help us to determine the operatives.
                'In case of oftec , only operatives with oftec status will be displayed
                If CType(tempFaultDr.Item("isOftec"), Boolean) = True Then
                    isFaultOftec = True
                End If

                'sum up the fault duration of all faults in one group
                faultDuration += IIf(IsDBNull(tempFaultDr.Item("Duration")) = True, 1, tempFaultDr.Item("Duration"))

                If (faultCount) <= tempFaultDt.Rows.Count() - 1 Then
                    nextTradeId = tempFaultDt.Rows(faultCount).Item("TradeId")
                Else
                    nextTradeId = currentTradeId
                End If

                'Below mentioned IF BLOCK shows decides either to show operative or not based on trade id                            
                If (groupBy = True And nextTradeId = currentTradeId) And (faultCount = tempFaultDt.Rows.Count()) Then
                    'If its last row then add fault of current row in new fault list
                    'and also add the appointment of current row in new appointment list
                    addFaultInList = True
                    addAppointmentInList = True
                ElseIf (groupBy = True And nextTradeId = currentTradeId) Then
                    addFaultInList = True
                    addAppointmentInList = False
                ElseIf (groupBy = True And nextTradeId <> currentTradeId) Then
                    addFaultInList = True
                    addAppointmentInList = True
                ElseIf groupBy = False Then
                    addFaultInList = True
                    addAppointmentInList = True
                End If

                If addFaultInList = True Then
                    tempFaultAptBo.tempFaultDtBo.location = tempFaultDr.Item("AreaName") + " > " + "<span><b>" + tempFaultDr.Item("Description") + "</b></span>"
                    tempFaultAptBo.tempFaultDtBo.durationString = IIf(IsDBNull(tempFaultDr.Item("DurationString")) = True, "N/A", tempFaultDr.Item("DurationString"))
                    tempFaultAptBo.tempFaultDtBo.trade = tempFaultDr.Item("TradeName")
                    tempFaultAptBo.tempFaultDtBo.dueDateString = tempFaultDr.Item("DueDate").ToString()
                    tempFaultAptBo.tempFaultDtBo.tempFaultId = tempFaultDr.Item("TempFaultId")
                    tempFaultAptBo.tempFaultDtBo.priorityName = tempFaultDr.Item("PriorityName")
                    tempFaultAptBo.tempFaultDtBo.completeDueDate = tempFaultDr.Item("CompleteDueDate")
                    tempFaultAptBo.tempFaultDtBo.tradeId = tempFaultDr.Item("TradeID")
                    tempFaultAptBo.tempFaultDtBo.addNewDataRow()

                End If

                If addAppointmentInList = True Then
                    tempFaultAptBo.FaultDuration = faultDuration
                    tempFaultAptBo.IsFaultGasSafe = isFaultGasSafe
                    tempFaultAptBo.IsFaultOftec = isFaultOftec
                    tempFaultAptBo.TradeId = currentTradeId
                    tempFaultAptBo.IsPatchSelected = True
                    tempFaultAptBo.IsDateSelected = True
                    tempFaultAptBo.DisplayCount = 5
                    tempFaultAptBo.CompleteDueDate = tempFaultDr.Item("CompleteDueDate").ToString()
                    tempFaultAptBo.FromDate = DateTime.Now
                    tempFaultAptBo.IsFromDateSelected = False
                    faultDuration = 0
                    isFaultGasSafe = False
                    isFaultOftec = False
                    Dim filteredOperativesDt As DataTable = New DataTable()
                    'If patch check box is selected this function 'll sort out the operatives who have same customer's property patch 
                    'otherwise if patch is unchecked it 'll return all the operatives
                    filteredOperativesDt = Me.applyPatchFilter(operativesDt, tempFaultAptBo, customerBo.PatchId)

                    'Filter for operatives that have same trade as of current fault (operative_filter = fault_filter)
                    'Provide the already filtered operatives for patchid to filter more for tradeid.
                    'Note: Return result into same data table(variable) there is no need to use a data table(variable).
                    filteredOperativesDt = applyTradeFilter(filteredOperativesDt, currentTradeId)

                    'Filter for operatives that have IsGasSafe & IsOftec set as true in the operative detail screen
                    filteredOperativesDt = applyGasSafeAndOftecFilter(filteredOperativesDt, tempFaultAptBo)

                    'this function will calculate/decide the due date of faults (if appointment has multiple faults) based on priority of faults
                    Me.calculateDueDate(tempFaultAptBo)

                    'this function will create the appointment slot based on leaves, appointments & due date
                    Me.createTempAppointmentSlots(filteredOperativesDt, leavesDt, appointmentsDt, tempFaultAptBo, customerBo)

                    'this function will sort the appointments by distance calculated between operative's last appointment and current calculated appointment 
                    orderAppointmentsByDistance(tempFaultAptBo)

                    'once appointment is added in list then create a new item 
                    'for new list of faults and appointment against them
                    tempFaultAptBoList.Add(tempFaultAptBo)
                    tempFaultAptBo = New TempFaultAppointmentBO()
                End If
            Next

            Return tempFaultAptBoList
        End Function
#End Region

#Region "apply Due Date Filter"
        ''' <summary>
        ''' This function 'll filtered the already created slots based on due date
        ''' </summary>
        ''' <param name="tempFaultAptBo"></param>
        ''' <remarks></remarks>
        Public Sub applyDueDateFilter(ByRef tempFaultAptBo As TempFaultAppointmentBO)

            If tempFaultAptBo.IsDateSelected = True Then
                Dim completeDueDate As Date = New Date()
                completeDueDate = CType(tempFaultAptBo.CompleteDueDate.ToLongDateString() + " " + "23:59", Date)
                Dim appointments = tempFaultAptBo.tempAppointmentDtBo.dt.AsEnumerable()
                Dim appResult = (From app In appointments Where app.Item(TempAppointmentDtBO.appointmentStartDateColName) <= completeDueDate Select app)

                If (appResult.Count() > 0) Then
                    tempFaultAptBo.tempAppointmentDtBo.dt = appResult.CopyToDataTable()
                Else
                    tempFaultAptBo.tempAppointmentDtBo.dt.Clear()
                End If

                'Dim tempOperativeDv As DataView = New DataView()
                'tempOperativeDv = tempFaultAptBo.tempAppointmentDtBo.dt.AsDataView()
                'tempOperativeDv.RowFilter = "[" + TempAppointmentDtBO.appointmentDateColName + "]" + "<= '" + completeDueDate + "'"
                'tempFaultAptBo.tempAppointmentDtBo.dt = tempOperativeDv.ToTable()
            End If
        End Sub
#End Region

#Region "apply Patch Filter"
        ''' <summary>
        ''' If patch check box is selected this function 'll sort out the operatives who have same customer's property patch 
        ''' otherwise if patch is unchecked it 'll return all the operatvies
        ''' </summary>
        ''' <param name="operativesDt"></param>
        ''' <param name="tempFaultAptBo"></param>
        ''' <param name="patchId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function applyPatchFilter(ByVal operativesDt As DataTable, ByRef tempFaultAptBo As TempFaultAppointmentBO, ByVal patchId As Integer)
            Dim filteredOperativesDt As DataTable = New DataTable()

            'convert operatives data table into dv            
            Dim operativesDv As DataView = New DataView()
            operativesDv = operativesDt.AsDataView()

            'if patch is selected then filter the operatives which have the same patch as customer's property patch
            If tempFaultAptBo.IsPatchSelected = True Then
                '(PatchId = -1 ) => Filter for operatives with all patches.
                operativesDv.RowFilter = String.Format("PatchId = {0} OR PatchId =  -1 ", patchId.ToString())
                filteredOperativesDt = operativesDv.ToTable()

                'get the ids of filtered operatives
                Dim employeeIds As String = String.Empty
                For Each dr As DataRow In filteredOperativesDt.Rows
                    employeeIds = employeeIds + dr.Item("EmployeeId").ToString() + ","
                Next
                If employeeIds.Length() > 1 Then
                    employeeIds = employeeIds.Substring(0, employeeIds.Length() - 1)

                    'select the appointments based on operatives which are filtered above
                    Dim tempAptDv As DataView = New DataView()
                    tempAptDv = tempFaultAptBo.tempAppointmentDtBo.dt.AsDataView()
                    tempAptDv.RowFilter = TempAppointmentDtBO.operativeIdColName + " IN (" + employeeIds + ")"
                    tempFaultAptBo.tempAppointmentDtBo.dt = tempAptDv.ToTable()
                Else
                    tempFaultAptBo.tempAppointmentDtBo.dt.Clear()
                End If
            Else
                filteredOperativesDt = operativesDv.ToTable()
            End If

            Return filteredOperativesDt
        End Function
#End Region

#Region "apply Trade Filter"

        ''' <summary>
        ''' This function is used to filter the available operate for a particular trade, that filter is applied based of tradeid provided.
        ''' In case the provided tradeid is not in operatives list it will return all records.
        ''' </summary>
        ''' <param name="operativesDt">Provide the data table to fiilter for trade, it must has a TradeID column</param>
        ''' <param name="tradeId">Provide Fault TradeId to filter Operative for that trade only</param>
        ''' <returns>The fuction returns filter operatives by trade as a datatable.</returns>
        ''' <remarks>Although this is not a generic function but with a little modification it can be changed to generic function</remarks>
        Public Function applyTradeFilter(ByVal operativesDt As DataTable, ByVal tradeId As Integer) As DataTable
            Dim filteredOperativesDt As DataTable = New DataTable()

            'convert operatives data table into dv            
            Dim operativesDv As DataView = New DataView()
            operativesDv = operativesDt.AsDataView()

            'Filter the operatives which have the same trade as temp faults trade.

            operativesDv.RowFilter = "TradeID =  " + tradeId.ToString()
            filteredOperativesDt = operativesDv.ToTable()

            Return filteredOperativesDt
        End Function

#End Region

#Region "apply gas safe and oftec filter"

        ''' <summary>
        ''' This function is used to filter the available operate who has isGasSafe and isOftec checked in there profile.        
        ''' </summary>
        ''' <returns>The fuction returns filter operatives by trade as a datatable.</returns>
        ''' <remarks>Although this is not a generic function but with a little modification it can be changed to generic function</remarks>
        Public Function applyGasSafeAndOftecFilter(ByVal operativesDt As DataTable, ByVal tempFaultAptBo As TempFaultAppointmentBO) As DataTable
            Dim filteredOperativesDt As DataTable = New DataTable()
            Dim filter As String = String.Empty

            If tempFaultAptBo.IsFaultGasSafe = True Then
                filter = "IsGasSafe = 1"
            End If

            If tempFaultAptBo.IsFaultGasSafe = True Then
                If String.IsNullOrEmpty(filter) Then
                    filter = "IsOftec = 1"
                Else
                    filter = filter + " And IsOftec = 1"
                End If

            End If

            'convert operatives data table into dv            
            Dim operativesDv As DataView = New DataView()
            operativesDv = operativesDt.AsDataView()

            'Filter the operatives which have the same trade as temp faults trade.
            If String.IsNullOrEmpty(filter) = False Then
                operativesDv.RowFilter = filter
            End If

            filteredOperativesDt = operativesDv.ToTable()

            Return filteredOperativesDt
        End Function

#End Region

#Region "create Temp Appointment Slots"

        ''' <summary>
        ''' 'This function 'll create the appointment slots based on operatives, appointments, leaves and due date 
        ''' </summary>
        ''' <param name="operativesDt"></param>
        ''' <param name="leavesDt"></param>
        ''' <param name="appointmentsDt"></param>
        ''' <param name="tempFaultAptBo"></param>
        ''' <param name="customerBo"></param>
        ''' <param name="previousOperativeId"></param>
        ''' <remarks></remarks>
        Public Sub createTempAppointmentSlots(ByVal operativesDt As DataTable, ByVal leavesDt As DataTable, ByVal appointmentsDt As DataTable, ByRef tempFaultAptBo As TempFaultAppointmentBO, ByVal customerBo As CommonAddressBO, Optional ByVal previousOperativeId As Integer = 0, Optional ByVal fromDate As DateTime = Nothing)
            Dim startDate As Date = getStartingDate()
            If Not fromDate = Nothing AndAlso tempFaultAptBo.tempAppointmentDtBo.dt.Rows.Count() = 0 Then
                startDate = fromDate
            End If
            Dim operativeId As Integer = 0
            Dim operativeName As String = String.Empty
            Dim operativePostCode As String = String.Empty
            Dim patchName As String = String.Empty
            Dim dayStartHour As Double = GeneralHelper.getTodayStartHour(startDate)
            Dim dayEndHour As Double = GeneralHelper.getDayEndHour()
            Dim displayedTimeSlotCount As Integer = 1
            Dim appointmentStartDateTime As Date
            Dim appointmentEndDate As Date
            Dim aptEndTime As New Date
            Dim isGasOftecOperativeCount As Integer = 0
            Dim dueDateCheckCount As Hashtable = New Hashtable()
            Dim timeSlotsToDisplay As Integer = GeneralHelper.getTimeSlotToDisplay
            Dim isMultipleDaysAppointmentAllowed As Boolean = GeneralHelper.isMultipleDaysAppointmentAllowed

            Dim operativeEligibility As New Dictionary(Of Integer, Boolean)
            Dim ofcCoreStartTime As String = DateTime.Parse(GeneralHelper.getDayStartHour().ToString() + ":00").ToString("HH:mm")
            Dim ofcCoreEndTime As String = DateTime.Parse(Convert.ToInt32(dayEndHour - dayEndHour Mod 1).ToString() + ":" + IIf(dayEndHour Mod 1 = 0, "00", Convert.ToInt32((dayEndHour Mod 1) * 60).ToString())).ToString("HH:mm")
            Dim skipOperative As Boolean

            'Just to initlize start date and end date.
            appointmentStartDateTime = startDate
            appointmentEndDate = appointmentStartDateTime

            'Calculate Time slots given in count
            ' Case: when fault duration is greater than MaximumDayWorkingHours (i.e 23.5 hours) then do not run following process
            While (displayedTimeSlotCount <= timeSlotsToDisplay AndAlso (tempFaultAptBo.FaultDuration <= ApplicationConstants.MaximumDayWorkingHours OrElse isMultipleDaysAppointmentAllowed))
                Dim uniqueCounter As Integer = 0
                'First Condition: If no operative found then display error and exit the loop

                If operativesDt.Rows.Count = 0 Then
                    Exit While
                End If

                For Each operativeDr As DataRow In operativesDt.Rows
                    skipOperative = False
                    uniqueCounter += 1
                    operativeId = Convert.ToString(operativeDr.Item("EmployeeId"))
                    operativeName = Convert.ToString(operativeDr.Item("FullName"))
                    operativePostCode = Convert.ToString(operativeDr.Item("PostCode"))
                    patchName = Convert.ToString(operativeDr.Item("PatchName"))
                    Dim operativeWorkingHourDs As DataSet = New DataSet()
                    Dim objFaultAppointmentDal As FaultAppointmentDAL = New FaultAppointmentDAL()
                    objFaultAppointmentDal.getOperativeWorkingHours(operativeWorkingHourDs, operativeId, ofcCoreStartTime, ofcCoreEndTime)
                    SessionManager.setOperativeworkingHourDs(operativeWorkingHourDs)

                    'this if block 'll be used when page 'll post back, 
                    'this will help to start creating appointment from the operative which was in the last row of previously displayed slots 
                    If (previousOperativeId <> 0) Then

                        If (operativeId = previousOperativeId) Then
                            previousOperativeId = 0
                            Continue For
                        End If

                        If (operativeId <> previousOperativeId) Then
                            Continue For
                        End If

                    End If

                    If tempFaultAptBo.IsDateSelected = True Then
                        appointmentDueDate = CType(tempFaultAptBo.CompleteDueDate.ToLongDateString() + " " + "23:59", Date)
                    Else
                        appointmentDueDate = CType(Date.Today.AddDays(GeneralHelper.getMaximunLookAhead()).ToLongDateString() + " " + "23:59", Date)
                    End If

                    'get the old time slots that have been displayed / added lately
                    Dim slots = tempFaultAptBo.tempAppointmentDtBo.dt.AsEnumerable()
                    Dim slotsResult = (From app In slots _
                                       Where app(TempAppointmentDtBO.operativeIdColName) = operativeId _
                                       Select app _
                                       Order By CType(app(TempAppointmentDtBO.appointmentEndDateColName), Date) Ascending)

                    'if the there's some old time slot that has been added lately get the new time slot
                    If slotsResult.Count() > 0 Then
                        'some old time slot that has been added lately get the new time slot based on old slot
                        appointmentStartDateTime = CType(slotsResult.Last.Item(TempAppointmentDtBO.appointmentEndDateColName), Date)
                    Else
                        'set the appointment starting hour e.g 8 (represents 8 o'clock in the morning)
                        appointmentStartDateTime = CType(startDate.ToLongDateString() + " " + dayStartHour.ToString() + ":00", Date)
                    End If

                    Me.setAppointmentStartEndTime(appointmentStartDateTime, appointmentEndDate, tempFaultAptBo.FaultDuration, skipOperative _
                                              , leavesDt, appointmentsDt, operativeId, uniqueCounter, dueDateCheckCount, tempFaultAptBo, isMultipleDaysAppointmentAllowed)

                    If (skipOperative = True) Then
                        'Requirement is to check, running date should not be greater than fault's due date (if due date check box is selected)
                        'If its true then we are adding entry in hash table and this (current) operative is not being processed any more for appointments
                        'this condition 'll check either due date check has been executed against all the operatives
                        If (dueDateCheckCount.Count() = operativesDt.Rows.Count) Then
                            Exit While
                        Else
                            Continue For
                        End If
                    End If


                    'if operative has any appointment prior to the running appointment slot then 
                    'find the distance between customer property's address and property address of operative's last arranged appointment on the same day
                    Dim distanceFrom As Double = getPropertyDistance(appointmentsDt, operativeId, appointmentStartDateTime, customerBo, operativePostCode)
                    Dim distanceTo As Double = getPropertyDistance(appointmentsDt, operativeId, appointmentEndDate, customerBo, operativePostCode, isToNext:=True)

                    'update the lastAdded column in tempfaultBO.dt
                    updateLastAdded(tempFaultAptBo)
                    'add this time slot in appointment list
                    tempFaultAptBo.addRowInAppointmentList(operativeId, operativeName, appointmentStartDateTime, appointmentEndDate, patchName, distanceFrom, distanceTo)

                    'increase the displayed time slot count
                    displayedTimeSlotCount = displayedTimeSlotCount + 1
                    If displayedTimeSlotCount > timeSlotsToDisplay Then
                        Exit While
                    End If
                Next
            End While 'Calculate Time slots loop ends here
        End Sub

#End Region

#Region "get starting date"
        ''' <summary>
        ''' This funciton 'll be used to reset the starting date.
        ''' </summary>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function getStartingDate()
            Return Date.Now.AddHours(1)
        End Function
#End Region

#Region "Calculate Due Date"
        ''' <summary>
        ''' If there are multiple jobs selected within an appointment e.g. see screen '7-1. 
        ''' Next available appointments' which has 'The tap is broken' and 'The shower is dripping' within one appointment. 
        ''' If the priorities set for these two fault types were Urgent' and 'Routine' respectively, 
        ''' the priority used for the 'Due Date' would be based on the 'Urgent' priority as oppose to the 'Routine'
        ''' </summary>
        ''' <param name="tempFaultAptBo"></param>
        ''' <remarks></remarks>
        Private Sub calculateDueDate(ByRef tempFaultAptBo As TempFaultAppointmentBO)
            Dim faults = tempFaultAptBo.tempFaultDtBo.dt.AsEnumerable()
            'if any of the fault is having priority as Emergency then due date 'll be according to emergency 
            Dim emFaultResult = (From fa In faults Where fa(TempFaultDtBO.priorityColName) = ApplicationConstants.EmergencyPriorityName Select fa)
            If emFaultResult.Count() > 0 Then
                tempFaultAptBo.CompleteDueDate = emFaultResult.First.Item(TempFaultDtBO.completeDueDateColName)
            Else
                'if any of the fault is having priority as daily then due date 'll be according to daily
                Dim dailyFaultResult = (From fa In faults Where fa(TempFaultDtBO.priorityColName) = ApplicationConstants.DailyPriorityName Select fa)
                If dailyFaultResult.Count() > 0 Then
                    tempFaultAptBo.CompleteDueDate = dailyFaultResult.First.Item(TempFaultDtBO.completeDueDateColName)
                Else
                    'if any of the fault is having priority as Urgent then due date 'll be according to urgent
                    Dim ugFaultResult = (From fa In faults Where fa(TempFaultDtBO.priorityColName) = ApplicationConstants.UrgentPriorityName Select fa)
                    If ugFaultResult.Count() > 0 Then
                        tempFaultAptBo.CompleteDueDate = ugFaultResult.First.Item(TempFaultDtBO.completeDueDateColName)
                    Else
                        'if any of the fault is having priority as Urgent then due date 'll be according to urgent
                        Dim routineFaultResult = (From fa In faults Where fa(TempFaultDtBO.priorityColName) = ApplicationConstants.RoutinePriorityName Select fa)
                        If routineFaultResult.Count() > 0 Then
                            tempFaultAptBo.CompleteDueDate = routineFaultResult.First.Item(TempFaultDtBO.completeDueDateColName)
                        End If
                    End If
                End If
            End If

        End Sub

#End Region

#Region "Order Temp Appointments By Distance"
        ''' <summary>
        ''' 'this function 'll sort the appointments by distance calculated between operative's last appointment and current calcualted appointment 
        ''' </summary>
        ''' <param name="tempFaultAptBo"></param>
        ''' <remarks></remarks>
        Public Sub orderAppointmentsByDistance(ByRef tempFaultAptBo As TempFaultAppointmentBO)
            Dim appointments = tempFaultAptBo.tempAppointmentDtBo.dt.AsEnumerable
            'Ordert The appointment, First By Distance then By Date and Then By Start Time.
            Dim appResult = (From app In appointments _
                             Select app _
                             Order By CType(app(TempAppointmentDtBO.distanceFromColName), Double) + CType(app(TempAppointmentDtBO.distanceToColName), Double) Ascending _
                             , CType(app(TempAppointmentDtBO.appointmentStartDateColName), Date) Ascending _
                             , app(TempAppointmentDtBO.startDateColName) Ascending)
            If appResult.Count() > 0 Then
                tempFaultAptBo.tempAppointmentDtBo.dt = appResult.CopyToDataTable()
            End If

        End Sub

#End Region

#Region "set appointment start end time"

        Private Sub setAppointmentStartEndTime(ByRef appointmentStartDate As Date, ByRef appointmentEndDate As Date _
                                               , ByVal faultDuration As Double, ByRef skipOperative As Boolean, ByVal leavesDt As DataTable _
                                               , ByVal appointmentsDt As DataTable, ByVal operativeId As Integer, ByRef uniqueCounter As Integer, _
                                               ByRef dueDateCheckCount As Hashtable, ByVal tempFaultAptBo As TempFaultAppointmentBO, Optional ByVal isMultipleDaysAppointmentAllowed As Boolean = False)

            Dim isAppointmentCreated As Boolean = False
            Dim isExtendedApptVerified As Boolean = False
            Dim loggingTime As Integer = 0
            'Get Operative core and extended working hours from session
            Dim operativeWorkingHourDs As DataSet = SessionManager.getOperativeworkingHourDs()
            Dim operativeWorkingHourDt As DataTable = operativeWorkingHourDs.Tables(0)

            Dim currentRunningDate As Date = appointmentStartDate


            'Do Until: appointment is created or the operative is skipped
            Do
                'Check Operative's Appointment Start Date for Due date and also for operatives availability (today and future)                
                If isOpertiveAvailable(operativeWorkingHourDt, appointmentStartDate, dueDateCheckCount, operativeId, uniqueCounter, skipOperative, tempFaultAptBo.IsFromDateSelected) Then
                    skipOperative = True
                Else

                    'Get operatives running day available time in ascending order of StartTime column
                    Dim operativeWorkingHourForCurrentDayDt As DataTable = getOperativeAvailableSlotsByDateTime(operativeWorkingHourDt, appointmentStartDate)

                    ' Check if there is a slot 
                    If (operativeWorkingHourForCurrentDayDt.Rows.Count > 0) Then
                        Dim nextBlockStartTime As String = operativeWorkingHourForCurrentDayDt.Rows(0)(ApplicationConstants.StartTimeCol)
                        Dim nextBlockStartDateTime As Date = CType(appointmentStartDate.ToLongDateString() + " " + nextBlockStartTime, Date)
                        If nextBlockStartDateTime > appointmentStartDate Then
                            appointmentStartDate = nextBlockStartDateTime
                        End If

                        'At the begining of slot creation set appointment end date time to same as start date time and then calculate
                        ' the end date time base on duration and operative working hours.
                        appointmentEndDate = appointmentStartDate

                        'Merge adjacent hours, treat each row as block
                        'operativeWorkingHourDt = Me.mergeAdjacentHours(operativeWorkingHourDt)

                        'This will actually work as day counter in the loop, every time entering the it will be increased by 1 mean set to 1 first time.
                        Dim dayCounter As Integer = 0
                        'Repeat For End Date Time Until the full remainingDuration is consumed to zero
                        Dim remainDuration As Double = faultDuration
                        While remainDuration > 0 AndAlso Not isAppointmentCreated
                            dayCounter += 1

                            ' Iterate through each working core/on call/extended to use it for appointment duration.
                            For Each operativeWorkingHoursBlock As DataRow In operativeWorkingHourForCurrentDayDt.Rows
                                nextBlockStartTime = operativeWorkingHoursBlock(ApplicationConstants.StartTimeCol)
                                nextBlockStartDateTime = CType(appointmentEndDate.ToLongDateString() + " " + nextBlockStartTime, Date)
                                loggingTime = operativeWorkingHoursBlock(ApplicationConstants.CreationDateCol)
                                'Set appointmentEndDate to start time of next block.
                                'At the begining of each block set appointment end date time to the start of next block's start date in case
                                ' in case it is less than next block start time.
                                If nextBlockStartDateTime > appointmentEndDate Then
                                    appointmentEndDate = nextBlockStartDateTime
                                End If

                                Dim blockEndTime As String = operativeWorkingHoursBlock(ApplicationConstants.EndTimeCol)
                                Dim blockEndDateTime As Date = CType(appointmentEndDate.ToLongDateString() + " " + blockEndTime, Date)

                                'calculate the remaining time/lapse of the block
                                Dim lapse As Double = blockEndDateTime.TimeOfDay.Subtract(appointmentEndDate.TimeOfDay).TotalHours
                                'subtract the total lapse/remaining time of the block from the remaining trade duration
                                Dim tDuration As Double = remainDuration - lapse

                                'if duration is less than zero than it means now we have to calculate the end hour of appointment otherwise we have to go to one day ahead to calculate the end time
                                If (tDuration <= 0) Then
                                    'Dim absoluteDuration As Double = Math.Abs(tDuration)
                                    appointmentEndDate = appointmentEndDate.AddHours(remainDuration)
                                    'In this case appointment is created successfully, exit the blocks loop and check for leaves and existing appointments
                                    isAppointmentCreated = True
                                ElseIf tDuration > 0 Then
                                    'we have to go to next block to calculate the end date/time, so setting appointment End Date = BlockEndDateTiem
                                    appointmentEndDate = blockEndDateTime
                                End If
                                remainDuration -= lapse
                                If isAppointmentCreated Then
                                    Exit For
                                End If
                            Next

                            'In case appointment is created or operative is skipped by reaching due date: exit while and apply next iteration or add operative to skip list.
                            If isAppointmentCreated OrElse isOpertiveAvailable(operativeWorkingHourDt, appointmentStartDate, dueDateCheckCount, operativeId, uniqueCounter, skipOperative, tempFaultAptBo.IsFromDateSelected) Then
                                Exit While
                            End If

                            'In case appointment is not created by above process change the day and set appointment end datetime at start of next day.
                            changeDay(appointmentEndDate, "00:00")

                            'In case appointment is not create and all slots (Core Working Hours, On Call hours and out of office hours) are 
                            'Check for case: is it is allowed to create multiple days appointment: If now exit while and start over for next day
                            ' in case due date is reached operative will be skipped by skipOperative Process.
                            If (Not isMultipleDaysAppointmentAllowed) Then
                                appointmentStartDate = appointmentEndDate
                                Exit While
                            End If

                            'Only in case remainDuration is more than zero then we need to appointment working hours for next day, other wise the loop will simply exit.                            
                            operativeWorkingHourForCurrentDayDt = If(remainDuration > 0, _
                                                                     getOperativeAvailableSlotsByDateTime(operativeWorkingHourDt, appointmentEndDate), Nothing)
                        End While

                    Else
                        'Change day 
                        changeDay(appointmentStartDate, "00:00")
                    End If
                End If

                If isAppointmentCreated AndAlso Not skipOperative Then
                    'check the leaves if exist or not
                    Dim leaveResult As EnumerableRowCollection(Of DataRow) = Me.isLeaveExist(leavesDt, operativeId, appointmentStartDate, appointmentEndDate)
                    'if leave exist get the new date time but still its possibility that leave will exist in the next date time so will also be checked again.
                    If leaveResult.Count() > 0 Then
                        appointmentStartDate = CType(leaveResult.Last().Item("EndDate"), Date)
                        appointmentStartDate = CType(appointmentStartDate.ToLongDateString() + " " + leaveResult.Last().Item("EndTime").ToString(), Date)
                        isAppointmentCreated = False
                    Else
                        'if appointment exist in this time slot get the new date time but still its possibility that appointment ''ll exist in the next time slot, so this while loop 'll run until
                        'this will be established that no new appointment actually exist in this time slot
                        Dim appResult As EnumerableRowCollection(Of DataRow) = Me.isAppointmentExist(appointmentsDt, operativeId, appointmentStartDate, appointmentEndDate)
                        If Not IsNothing(appResult) And appResult.Count() > 0 Then
                            'appointmentStartDate = CType(appResult.Last().Item("AppointmentDate"), Date)
                            'appointmentStartDate = GeneralHelper.unixTimeStampToDateTime(appResult.Last.Item("EndTimeInSec").ToString())
                            'isAppointmentCreated = False
                            If isExtendedApptVerified = False Then

                                If appResult.Last().Field(Of String)("AppointmentType") = "Planned Appointment" Then

                                    Dim isExtendedHoursExist As Boolean = isExtendedIncludedInAppointment(operativeWorkingHourDt, appointmentsDt, appointmentStartDate, appResult.Last().Field(Of Integer)("CreationDate"), faultDuration, operativeId, loggingTime, tempFaultAptBo.tempAppointmentDtBo)
                                    If isExtendedHoursExist = False Then
                                        appointmentStartDate = GeneralHelper.unixTimeStampToDateTime(Convert.ToDouble(appResult.Last().Field(Of Integer)("EndTimeInSec")))
                                    Else
                                        isExtendedApptVerified = True
                                    End If

                                Else
                                    appointmentStartDate = GeneralHelper.unixTimeStampToDateTime(Convert.ToDouble(appResult.Last().Field(Of Integer)("EndTimeInSec")))
                                End If
                                isAppointmentCreated = False

                            End If

                        End If
                    End If
                End If

            Loop Until (isAppointmentCreated OrElse skipOperative)
        End Sub

#End Region

#Region "Change day"

        ''' <summary>
        ''' This function will change day
        ''' </summary>
        ''' <remarks></remarks>
        Sub changeDay(ByRef runningDate As Date, ByVal nextDayStartTime As String)
            runningDate = runningDate.AddDays(1)
            runningDate = CType(runningDate.ToLongDateString() + " " + nextDayStartTime, Date)
        End Sub

#End Region

#Region "Merge adjacent hours"
        ''' <summary>
        ''' Merge adjacent hours, treat each row as block
        ''' </summary>
        ''' <remarks></remarks>
        Function mergeAdjacentHours(ByVal operativeWorkingHourDt As DataTable)

            Dim mergedAdjacentHoursDt As DataTable = operativeWorkingHourDt.Clone()
            ' This loop iterates through core and extended working hours of the day, merge adjacent hours
            ' to make list blocks. e.g Suppose "09:00-16:30" is core working hours and there is Extended working
            ' "16:30-18:00" as these are adjacent so they will be merged like "09:00-18:00"
            For Each row As DataRow In operativeWorkingHourDt.Rows
                Dim mergedDr As DataRow = mergedAdjacentHoursDt.Select(ApplicationConstants.EndTimeCol & " = '" & row(ApplicationConstants.StartTimeCol) & "'").FirstOrDefault()
                If IsNothing(mergedDr) Then
                    mergedAdjacentHoursDt.ImportRow(row)
                Else
                    mergedDr.Item(ApplicationConstants.EndTimeCol) = row(ApplicationConstants.EndTimeCol)
                End If
                mergedAdjacentHoursDt.AcceptChanges()
            Next
            Return mergedAdjacentHoursDt

        End Function

#End Region

#Region "Check operative availability"
        ''' <summary>
        ''' This function checks whether operative is fully absent in core and extended hours. If yes then this 
        ''' operative will be skipped.
        ''' </summary>
        ''' <param name="runningDate"></param>
        ''' <param name="operativeHoursDt"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function checkOperativeAvailability(ByVal runningDate As DateTime, ByVal operativeHoursDt As DataTable)

            Dim isAvailable As Boolean = True
            Dim workingHours = operativeHoursDt.AsEnumerable

            Dim coreWorkingHoursResult = (From app In workingHours _
                                            Where app(ApplicationConstants.StartDateCol) = "NA" _
                                            And app(ApplicationConstants.EndDateCol) = "NA" _
                                            And app(ApplicationConstants.StartTimeCol) <> "" _
                                            Select app)

            Dim extendedWorkingHoursResult = (From app In workingHours _
                                            Where app(ApplicationConstants.StartDateCol) <> "NA" _
                                            Select app)

            If (extendedWorkingHoursResult.Count > 0) Then
                extendedWorkingHoursResult = (From app In extendedWorkingHoursResult.CopyToDataTable().AsEnumerable() _
                                           Where Convert.ToDateTime(app(ApplicationConstants.StartDateCol)) >= runningDate _
                                           Select app)
            End If

            If coreWorkingHoursResult.Count = 0 And extendedWorkingHoursResult.Count = 0 Then
                isAvailable = False
            End If

            Return isAvailable

        End Function

#End Region

#Region "change Day If Time End"
        Private Function changeDayIfTimeEnd(ByRef runningDate As Date, ByRef runningHour As Double, ByVal dayEndHour As Double) As Boolean
            If (runningHour > dayEndHour) Then
                'go to next day because current day slots have finished
                'Also set the running hour to zero for the nex day 
                runningDate = runningDate.AddDays(1)
                runningHour = 0
                ' Me.skipWeekend(runningDate, runningHour)
                Return True
            Else
                Return False
            End If
        End Function
#End Region

#Region "Skip Weekend"
        ''' <summary>
        ''' This function will skip the weekend (Saturday or Sunday)
        ''' </summary>
        ''' <param name="runningDate"></param>
        ''' <param name="runningHour"></param>
        ''' <remarks></remarks>
        Private Sub skipWeekend(ByRef runningDate As Date, ByRef runningHour As Double)
            If runningDate.DayOfWeek = DayOfWeek.Saturday Then
                runningDate = runningDate.AddDays(2)
                runningHour = 0
            ElseIf runningDate.DayOfWeek = DayOfWeek.Sunday Then
                runningDate = runningDate.AddDays(1)
                runningHour = 0
            End If
        End Sub
#End Region

#Region "if Date Is Greater Than Due Date"

        ''' <summary>
        ''' This function 'll check if running date is greater than due date of fault. If its true then add the entry in hash table and this (current) operative 'll not process any more for appointments
        ''' The count of total entries in hash table 'll help us to check that all operatives has been processed against the fault due date
        ''' </summary>
        ''' <param name="uniqueCounter"></param>
        ''' <param name="dateToCheck"></param>
        ''' <param name="operativeId"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function checkDateIsGreaterThanDueDateAndAddToSkipList(ByRef uniqueCounter As Integer, ByRef dueDateCheckCount As Hashtable, ByVal dateToCheck As Date, ByVal operativeId As Integer, ByVal IsFromDateSelected As Boolean) As Boolean
            Dim operativeSkipped As Boolean = False

            'Check whether the due date is less than appointment date(s)(start/end), if yes then this operative will be skipped.
            If CType(dateToCheck.ToShortDateString(), Date) > appointmentDueDate Then
                operativeSkipped = True
                'this variable 'll check either due date check has been executed against all the operatives
                If Not (dueDateCheckCount.ContainsValue(operativeId)) Then
                    If dueDateCheckCount.ContainsKey(GeneralHelper.getUniqueKey(uniqueCounter)) = False Then
                        dueDateCheckCount.Add(GeneralHelper.getUniqueKey(uniqueCounter), operativeId)
                    Else
                        dueDateCheckCount.Add(GeneralHelper.getUniqueKey(uniqueCounter + 1), operativeId)
                    End If
                End If
            End If
            Return operativeSkipped
        End Function

#End Region

#Region "get Operative Distance From Property"
        ''' <summary>
        ''' This appointment 'll calculate the distance between operatives last appointment slot and running appointment slot (running means the slot that is being calculated at run time)
        ''' The rule would work like this
        ''' The distance will be calculated via the selected property and the last appointment (in order of time) prior to the available slot on the day 
        ''' Examples:
        ''' Case 1
        ''' Operative: Carl Roof
        ''' Free Appointment slot: 11:00 - 12:00
        ''' Lets assume that Carl had 1 appointment from 9:00 - 10:00 at Property 1. Similarly he has another appointment from 10:00 - 11:00 at Property 2. The distance ail be calculated between selected property and Property2. The reason we chose Property 2 is because Carl will be here prior to his free slot 11:00 - 12:00
        ''' Case2
        ''' Operative: Carl Roof
        ''' Free Appointment slot: 9:00 - 11:00
        ''' Lets assume that Carl had no appointments on this day. In case there is no prior booked appointment before the available appointment slot, we shall display 0 in the distance column
        ''' </summary>
        ''' <param name="appointmentsDt"></param>
        ''' <param name="operativeId"></param>
        ''' <param name="appointmentDate"></param>
        ''' <remarks></remarks>
        Private Function getPropertyDistance(ByVal appointmentsDt As DataTable, ByVal operativeId As Integer, ByVal appointmentDate As Date, ByVal customerBo As CommonAddressBO, ByVal operativePostCode As String, Optional ByVal isToNext As Boolean = False) As Double
            'Distance
            Dim distance As Double = 0
            Dim iscalculated As Boolean = False
            Dim isGoogleError As Boolean = False


            ''çonvert time into seconds for comparison
            Dim appointmentExist As Boolean = False
            Dim startDate As New DateTime(1970, 1, 1)
            Dim runningAptTimeInSec As Integer
            ' we are creating this variable because we want to compare the dates from db and from calcuated date with time as 00:00:00
            ' in order to do the right comparison
            Dim runningDateAlias As Date = New Date()

            runningAptTimeInSec = (appointmentDate - startDate).TotalSeconds
            'creating the alias for comparison with 00:00:00 time
            runningDateAlias = CType(appointmentDate.ToLongDateString() + " " + "00:00:00", Date)

            Dim fromPropertyAddress As String
            Dim toPropertyAddress As String

            Dim appointments = appointmentsDt.AsEnumerable()
            Dim appResult As OrderedEnumerableRowCollection(Of DataRow)

            If Not isToNext Then
                appResult = (From app In appointments Where app("AppointmentDate") = runningDateAlias AndAlso app("OperativeId") = operativeId AndAlso app("EndTimeInSec") <= runningAptTimeInSec Select app Order By app("AppointmentDate") Descending, CType(app("EndTimeInSec"), Integer) Descending)
            Else
                appResult = (From app In appointments Where app("AppointmentDate") = runningDateAlias AndAlso app("OperativeId") = operativeId AndAlso app("StartTimeInSec") >= runningAptTimeInSec Select app Order By app("AppointmentDate") Descending, CType(app("StartTimeInSec"), Integer) Ascending)
            End If

            'if operative has any appointment prior to the running appointment slot then 
            'find the distance between customer property address and property address of operatives last arranged appointment on the same day
            If appResult.Count() > 0 Then
                'get the property address of running time slot

                'the google distance matrix api requires that we should pass the distance separated with plus sign
                fromPropertyAddress = String.Join("+", New String() {appResult.First().Item("Address"),
                                                                     appResult.First().Item("TownCity"),
                                                                     appResult.First().Item("County"),
                                                                     appResult.First().Item("PostCode")})

                fromPropertyAddress = fromPropertyAddress.Replace(" ", "+")
                fromPropertyAddress = appResult.First().Item("PostCode").Replace(" ", "+")


                toPropertyAddress = String.Join("+", New String() {customerBo.Street,
                                                                   customerBo.City,
                                                                   customerBo.Area,
                                                                   customerBo.PostCode})
                toPropertyAddress = customerBo.PostCode.Replace(" ", "+")

                ''get the distance from session if this calcuation was done previously
                'Dim propertyAddressDictionary As Dictionary(Of String, PropertyDistanceBO) = New Dictionary(Of String, PropertyDistanceBO)
                ' propertyAddressDictionary = SessionManager.getPropertyDistance()


                Dim propertyDistance As PropertyDistanceBO = New PropertyDistanceBO()
                If ApplicationConstants.propertyDistance.Columns.Count = 0 Then

                    propertyDistance.createDataTable(ApplicationConstants.propertyDistance)

                Else
                    'Filter the data according to fromPropertyAddress and toPropertyAddress
                    Dim propertyResult = (From ps In ApplicationConstants.propertyDistance Where ps.Item(PropertyDistanceBO.fromAddressColName).ToString() = fromPropertyAddress And ps.Item(PropertyDistanceBO.toAddressColName).ToString() = toPropertyAddress Select ps)

                    If propertyResult.Count > 0 Then
                        distance = propertyResult.First.Item(PropertyDistanceBO.distanceStringColName)
                        iscalculated = True
                    End If

                End If
                If iscalculated = False Then
                    'we didnt' find the property distance from datatable then find it using map api
                    Dim calUtility As CalendarUtilities = New CalendarUtilities()
                    'distance = calUtility.CalculateDistance(fromPropertyAddress, toPropertyAddress, isGoogleError)
                    distance = calUtility.CalculateDistance(fromPropertyAddress, toPropertyAddress, isGoogleError)
                    If isGoogleError Then
                        'Save the property's distance in share datatable so that we can save map api call in future calculations
                        propertyDistance.fromAddress = fromPropertyAddress
                        propertyDistance.toAddress = toPropertyAddress
                        propertyDistance.distanceString = distance
                        'Add a new row in data table
                        propertyDistance.addNewDataRow(ApplicationConstants.propertyDistance)
                    End If
                End If

            ElseIf Not isToNext Then

                'get the property address of running time slot

                'the google distance matrix api requires that we should pass the distance separated with plus sign
                fromPropertyAddress = operativePostCode.Replace(" ", "+")

                If customerBo.PostCode.Length > 0 Then
                    toPropertyAddress = customerBo.PostCode.Replace(" ", "+")
                Else
                    toPropertyAddress = String.Join("+", New String() {customerBo.Street,
                                                                     customerBo.City,
                                                                     customerBo.Area,
                                                                     customerBo.PostCode})
                    toPropertyAddress = customerBo.PostCode.Replace(" ", "+")
                End If

                ''get the distance from session if this calcuation was done previously
                'Dim propertyAddressDictionary As Dictionary(Of String, PropertyDistanceBO) = New Dictionary(Of String, PropertyDistanceBO)
                ' propertyAddressDictionary = SessionManager.getPropertyDistance()

                Dim propertyDistance As PropertyDistanceBO = New PropertyDistanceBO()
                If ApplicationConstants.propertyDistance.Columns.Count = 0 Then

                    propertyDistance.createDataTable(ApplicationConstants.propertyDistance)

                Else
                    'Filter the data according to fromPropertyAddress and toPropertyAddress
                    Dim propertyResult = (From ps In ApplicationConstants.propertyDistance Where ps.Item(PropertyDistanceBO.fromAddressColName).ToString() = fromPropertyAddress And ps.Item(PropertyDistanceBO.toAddressColName).ToString() = toPropertyAddress Select ps)

                    If propertyResult.Count > 0 Then
                        distance = propertyResult.First.Item(PropertyDistanceBO.distanceStringColName)
                        iscalculated = True
                    End If

                End If
                If iscalculated = False Then
                    'we didnt' find the property distance from datatable then find it using map api
                    Dim calUtility As CalendarUtilities = New CalendarUtilities()
                    distance = calUtility.CalculateDistance(fromPropertyAddress, toPropertyAddress, isGoogleError)
                    If isGoogleError Then
                        'Save the property's distance in share datatable so that we can save map api call in future calculations
                        propertyDistance.fromAddress = fromPropertyAddress
                        propertyDistance.toAddress = toPropertyAddress
                        propertyDistance.distanceString = distance
                        'Add a new row in data table
                        propertyDistance.addNewDataRow(ApplicationConstants.propertyDistance)
                    End If
                End If
            ElseIf isToNext Then
                Dim appNextResult As OrderedEnumerableRowCollection(Of DataRow) = (From app In appointments Where app("AppointmentDate") = runningDateAlias AndAlso app("OperativeId") = operativeId AndAlso app("EndTimeInSec") <= runningAptTimeInSec Select app Order By app("AppointmentDate") Descending, CType(app("EndTimeInSec"), Integer) Descending)
                If (appNextResult.Count() > 0) Then
                    'get the property address of running time slot

                    'the google distance matrix api requires that we should pass the distance separated with plus sign
                    toPropertyAddress = operativePostCode.Replace(" ", "+")

                    fromPropertyAddress = String.Join("+", New String() {customerBo.Street,
                                                                       customerBo.City,
                                                                       customerBo.Area,
                                                                       customerBo.PostCode})
                    fromPropertyAddress = customerBo.PostCode.Replace(" ", "+")


                    ''get the distance from session if this calcuation was done previously
                    'Dim propertyAddressDictionary As Dictionary(Of String, PropertyDistanceBO) = New Dictionary(Of String, PropertyDistanceBO)
                    ' propertyAddressDictionary = SessionManager.getPropertyDistance()


                    Dim propertyDistance As PropertyDistanceBO = New PropertyDistanceBO()
                    If ApplicationConstants.propertyDistance.Columns.Count = 0 Then

                        propertyDistance.createDataTable(ApplicationConstants.propertyDistance)

                    Else
                        'Filter the data according to fromPropertyAddress and toPropertyAddress
                        Dim propertyResult = (From ps In ApplicationConstants.propertyDistance Where ps.Item(PropertyDistanceBO.fromAddressColName).ToString() = fromPropertyAddress And ps.Item(PropertyDistanceBO.toAddressColName).ToString() = toPropertyAddress Select ps)

                        If propertyResult.Count > 0 Then
                            distance = propertyResult.First.Item(PropertyDistanceBO.distanceStringColName)
                            iscalculated = True
                        End If

                    End If
                    If iscalculated = False Then
                        'we didnt' find the property distance from datatable then find it using map api
                        Dim calUtility As CalendarUtilities = New CalendarUtilities()
                        distance = calUtility.CalculateDistance(fromPropertyAddress, toPropertyAddress, isGoogleError)
                        If isGoogleError Then
                            'Save the property's distance in share datatable so that we can save map api call in future calculations
                            propertyDistance.fromAddress = fromPropertyAddress
                            propertyDistance.toAddress = toPropertyAddress
                            propertyDistance.distanceString = distance
                            'Add a new row in data table
                            propertyDistance.addNewDataRow(ApplicationConstants.propertyDistance)
                        End If
                    End If
                End If
            End If

            Return distance
        End Function
#End Region

#Region "is Leave Exist"
        ''' <summary>
        ''' This function checks either operative has leave in the given start time and end time
        ''' if leave exists then go to next time slot that 'll start from end time of leave
        ''' </summary>
        ''' <param name="leavesDt"></param>
        ''' <param name="operativeId"></param>
        ''' <param name="appointmentStartDateTime"></param>
        ''' <param name="appoitmentEndDateTime"></param>
        ''' <returns>true or false</returns>
        ''' <remarks></remarks>
        Public Function isLeaveExist(ByVal leavesDt As DataTable, ByVal operativeId As Integer, ByVal appointmentStartDateTime As Date, ByVal appoitmentEndDateTime As Date)

            ''çonvert time into seconds for comparison
            Dim leaveExist As Boolean = False
            Dim lStartTimeInMin As Integer
            Dim lEndTimeInMin As Integer

            lStartTimeInMin = GeneralHelper.convertDateInMin(appointmentStartDateTime)
            lEndTimeInMin = GeneralHelper.convertDateInMin(appoitmentEndDateTime)

            Dim leaves = leavesDt.AsEnumerable()
            Dim leaveResult = (From app In leaves Where app("OperativeId") = operativeId AndAlso ( _
                                                                                                    (lStartTimeInMin <= app("StartTimeInMin") And lEndTimeInMin > app("StartTimeInMin") And lEndTimeInMin <= app("EndTimeInMin")) _
                                                                                                    Or (lStartTimeInMin >= app("StartTimeInMin") And lEndTimeInMin <= app("EndTimeInMin")) _
                                                                                                    Or (lStartTimeInMin >= app("StartTimeInMin") And lStartTimeInMin < app("EndTimeInMin") And lEndTimeInMin >= app("EndTimeInMin")) _
                                                                                                    Or (lStartTimeInMin < app("StartTimeInMin") And lEndTimeInMin > app("EndTimeInMin"))) _
             Select app Order By app("EndTimeInMin") Ascending)

            Return leaveResult
        End Function
#End Region

#Region "is Appointment Exist"
        ''' <summary>
        ''' This function checks either operative has appointment in the given start time and end time
        ''' </summary>
        ''' <param name="appointmentsDt"></param>
        ''' <param name="operativeId"></param>
        ''' <param name="appointmentStartDateTime "></param>
        ''' <param name="appointmentEndDateTime "></param>
        ''' <returns>true or false</returns>
        ''' <remarks></remarks>
        Public Function isAppointmentExist(ByVal appointmentsDt As DataTable, ByVal operativeId As Integer, ByVal appointmentStartDateTime As Date, ByVal appointmentEndDateTime As Date)

            ''çonvert time into seconds for comparison
            Dim appointmentExist As Boolean = False
            Dim startTimeInSec As Integer
            Dim endTimeInSec As Integer

            startTimeInSec = GeneralHelper.convertDateInSec(appointmentStartDateTime)
            endTimeInSec = GeneralHelper.convertDateInSec(appointmentEndDateTime)

            Dim appointments = appointmentsDt.AsEnumerable()
            'Dim appResult = (From app In appointments Where app("AppointmentDate") = runningDateAlias AndAlso app("OperativeId") = operativeId AndAlso (app("StartTimeInSec") >= startTimeInSec Or endTimeInSec <= app("EndTimeInSec")) Select app)
            Dim appResult = (From app In appointments Where app("OperativeId") = operativeId _
                                                            AndAlso (1 = 0 _
                                                            OrElse (startTimeInSec = app("StartTimeInSec") AndAlso endTimeInSec < app("EndTimeInSec")) _
                                                            OrElse (startTimeInSec < app("EndTimeInSec") AndAlso endTimeInSec = app("EndTimeInSec")) _
                                                            OrElse (startTimeInSec > app("StartTimeInSec") AndAlso endTimeInSec < app("EndTimeInSec")) _
                                                            OrElse (startTimeInSec < app("StartTimeInSec") AndAlso endTimeInSec > app("EndTimeInSec")) _
                                                            OrElse (startTimeInSec < app("StartTimeInSec") AndAlso endTimeInSec > app("StartTimeInSec")) _
                                                            OrElse (startTimeInSec < app("EndTimeInSec") AndAlso endTimeInSec > app("EndTimeInSec"))) _
                            Select app Order By app("EndTimeInSec") Ascending)

            Return appResult
        End Function
#End Region

#Region "operative Duration Exist"
        ''' <summary>
        ''' check fault duration lies in Operative's core working hour and extended working hour  
        ''' </summary>
        ''' <param name="operativeWorkingHourDs"></param>
        ''' <param name="operativeId"></param>
        ''' <param name="runningDate"></param>
        ''' <param name="faultDuration"></param>
        ''' <returns>if duration exist return 0 else greater then 0</returns>
        ''' <remarks></remarks>
        Private Function operativeDurationExist(ByVal operativeWorkingHourDs As DataSet, ByVal operativeId As Integer, ByVal runningDate As Date, ByVal faultDuration As Double) As Integer
            Dim runningDateDay As String = runningDate.DayOfWeek.ToString
            Dim currentDate = Date.Today
            Dim coreStartTime As Date
            Dim coreEndTime As Date
            Dim coreHourResult As Integer = 1
            Dim coreHour = operativeWorkingHourDs.Tables(0).AsEnumerable
            ' get operative's core hour, extended hour and check weekend will be off or not for operative with respect to day.

            'Dim extendedHoursResult = (From app In coreHour Where app("Name").ToString = runningDateDay And app("StartDate") = runningDate.Date.ToString("dd/MM/yyyy") Or app("EndDate") = runningDate.Date.ToString("dd/MM/yyyy") Select app)

            If operativeWorkingHourDs.Tables(0).Rows.Count > 0 Then
                For Each operativeHourDr As DataRow In operativeWorkingHourDs.Tables(0).Rows
                    If operativeHourDr.Item("StartTime") <> "" Then
                        Dim dFrom As DateTime
                        Dim dTo As DateTime
                        Dim operativeCoreStartTime As String = operativeHourDr.Item("StartTime")
                        Dim operativeCoreEndTime As String = operativeHourDr.Item("EndTime")
                        Dim operativeCoreStartTimeArry As String() = operativeCoreStartTime.Split(":")
                        Dim operativeCoreEndTimeArry As String() = operativeCoreEndTime.Split(":")
                        coreStartTime = CType(currentDate.AddHours(Convert.ToDouble(operativeCoreStartTimeArry(0))).ToString("HH") + ":" + currentDate.AddMinutes(Convert.ToDouble(operativeCoreStartTimeArry(1))).ToString("mm"), Date)
                        coreEndTime = CType(currentDate.AddHours(Convert.ToDouble(operativeCoreEndTimeArry(0))).ToString("HH") + ":" + currentDate.AddMinutes(Convert.ToDouble(operativeCoreEndTimeArry(1))).ToString("mm"), Date)
                        If operativeHourDr.Item("StartDate") <> "NA" And operativeHourDr.Item("StartDate") <> "" Then
                            Dim extendedDate As DateTime = CType(operativeHourDr.Item("StartDate"), DateTime)
                            Dim extendedRunningDateDay As String = extendedDate.DayOfWeek.ToString

                            Dim coreHoursResult = (From app In coreHour Where app("Name").ToString = extendedRunningDateDay And app("StartDate") = "NA" And app("EndDate") = "NA" And app("StartTime") <> "" Select app)
                            If coreHoursResult.Count > 0 Then
                                Dim extendedStartTime As String = coreHoursResult.First().Item("StartTime")
                                Dim extendedEndTime As String = coreHoursResult.First().Item("EndTime")
                                Dim extstartTime As Date = CType(currentDate.ToLongDateString() + " " + extendedStartTime, Date)
                                Dim extEndTime As Date = CType(currentDate.ToLongDateString() + " " + extendedEndTime, Date)
                                If coreStartTime = extEndTime.ToString("T") Then
                                    coreStartTime = extstartTime

                                End If
                            End If
                        End If
                        If DateTime.TryParse(coreStartTime, dFrom) AndAlso DateTime.TryParse(coreEndTime, dTo) Then
                            Dim ts As TimeSpan = dTo - dFrom
                            Dim hour As Integer = ts.Hours
                            Dim mins As Integer = ts.Minutes
                            Dim secs As Integer = ts.Seconds
                            Dim totalTime As String = (hour.ToString("00"))
                            If faultDuration > totalTime And coreHourResult > 0 Then
                                coreHourResult = coreHourResult + 1
                            Else
                                coreHourResult = 0
                            End If

                        End If
                    End If
                Next
            Else
                ' Condition: If total fault is duration more than 9 hours then we cannot display operatives 
                'so display error and exit the loop
                If faultDuration > 9 Then
                    Return coreHourResult
                End If
                coreHourResult = 0
            End If
            Return coreHourResult
        End Function
#End Region

#Region "update Last added column in tempFaultAptBo datatable"
        ''' <summary>
        ''' 'update Last added column in tempFaultAptBo datatable 
        ''' </summary>
        ''' <param name="tempFaultAptBo"></param>
        ''' <remarks></remarks>
        Public Sub updateLastAdded(ByRef tempFaultAptBo As TempFaultAppointmentBO)
            Dim appointments = tempFaultAptBo.tempAppointmentDtBo.dt.AsEnumerable
            Dim appResult = (From app In appointments _
                           Where app(TempAppointmentDtBO.lastAddedColName) = True Select app)
            If appResult.Count() > 0 Then
                appResult.First.Item(TempAppointmentDtBO.lastAddedColName) = False

            End If

        End Sub

#End Region

#Region "Calculate Fault duration"
        Private Function calculateFaultDuration(ByVal startTime As Date, ByVal endTime As Date) As Double
            Dim dFrom As DateTime
            Dim dTo As DateTime
            If DateTime.TryParse(startTime, dFrom) AndAlso DateTime.TryParse(endTime, dTo) Then
                Dim ts As TimeSpan = dTo - dFrom
                Dim hour As Integer = ts.Hours
                Dim mins As Integer = ts.Minutes
                Dim secs As Integer = ts.Seconds
                Dim totalTime As String = (hour.ToString("00"))
                Return totalTime
            End If
            Return 0
        End Function
#End Region

#Region "Get operatives available time slots for given date/day in ascending order of StartTime column"

        Private Function getOperativeAvailableSlotsByDateTime(ByVal operativeWorkingHourDt As DataTable, ByVal dateToFilter As Date) As DataTable
            'Check one or more working hours slots from the operative data table.
            Dim dayFilterResult = (From optHour In operativeWorkingHourDt.AsEnumerable _
                                           Where optHour(ApplicationConstants.StartDateCol) = dateToFilter.ToString("dd/MM/yyyy") _
                                                 Or ((optHour(ApplicationConstants.WeekDayCol).ToString() = dateToFilter.DayOfWeek.ToString) _
                                                And optHour(ApplicationConstants.StartDateCol) = "NA" _
                                                And optHour(ApplicationConstants.EndDateCol) = "NA" _
                                                And optHour(ApplicationConstants.StartTimeCol) <> "" _
                                                ) _
                                           Select optHour _
                                           Order By CType(optHour(ApplicationConstants.StartTimeCol), Date) Ascending) _
                                            .Where(Function(optHour) If(optHour(ApplicationConstants.EndTimeCol) = "", False, TimeSpan.Parse(optHour(ApplicationConstants.EndTimeCol)) > dateToFilter.TimeOfDay))
            ''And If(optHour(ApplicationConstants.EndTimeCol) = "", False, CType(optHour(ApplicationConstants.EndTimeCol), TimeSpan) >= dateToFilter.TimeOfDay)

            ' Check if there is a slot then return the slots in data table otherwise return an empty data table with same schema 
            Dim operativeWorkingHourForCurrentDayDt = If(dayFilterResult.Any(), dayFilterResult.CopyToDataTable, operativeWorkingHourDt.Clone)
            Return operativeWorkingHourForCurrentDayDt
        End Function

#End Region

#Region "is Operative Available - Check operative availability from due date and workingHours"

        ''' <summary>
        ''' 1- Check whether the due date is less than appointment date(s)(start/end), if yes then this operative will be skipped.
        ''' 2- Check whether operative is completely not available in coming days. if yes then this operative will be skipped.
        ''' </summary>
        ''' <param name="operativeWorkingHourDt"></param>
        ''' <param name="dateToCheck"></param>
        ''' <param name="dueDateCheckCount"></param>
        ''' <param name="operativeId"></param>
        ''' <param name="uniqueCounter"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function isOpertiveAvailable(ByVal operativeWorkingHourDt As DataTable, ByVal dateToCheck As Date, ByRef dueDateCheckCount As Hashtable, ByVal operativeId As Integer, ByVal uniqueCounter As Integer, ByRef skipOperative As Boolean, ByVal IsFromDateSelected As Boolean) As Boolean

            '1- Check whether the due date less than appointment date(s)(start/end), if yes then this operative will be skipped.
            '2- Check whether operative is completely not available in coming days. if yes then this operative will be skipped.
            Dim operativeAvailable As Boolean = checkDateIsGreaterThanDueDateAndAddToSkipList(uniqueCounter, dueDateCheckCount, dateToCheck, operativeId, IsFromDateSelected) _
                                    OrElse Not checkOperativeAvailability(dateToCheck, operativeWorkingHourDt)

            Return operativeAvailable
        End Function

#End Region

#Region "Planned works do not allow operatives to show as available for call out"

#Region "Is Extended Hours included in Appointment"
        ''' <summary>
        ''' Is Extended Hours included in Appointment of Multiple days.
        ''' </summary>
        ''' <param name="operativeWorkingHourDt"></param>
        ''' <param name="appointmentsDt"></param>
        ''' <param name="dateToFilter"></param>
        ''' <param name="creationDateInSecond"></param>
        ''' <param name="workDuration"></param>
        ''' <param name="operativeId"></param>
        ''' <param name="loggingTime"></param>
        ''' <param name="tempTradeAptBo"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function isExtendedIncludedInAppointment(operativeWorkingHourDt As DataTable, ByVal appointmentsDt As DataTable, ByRef dateToFilter As DateTime, ByVal creationDateInSecond As Integer, _
                                                    ByVal workDuration As Double, ByVal operativeId As Integer, ByVal loggingTime As Integer, ByVal tempTradeAptBo As TempAppointmentDtBO)
            Dim appointmentStartDate As DateTime = dateToFilter
            Dim result As Boolean = False
            Dim dayFilterResult = (From optHour In operativeWorkingHourDt.AsEnumerable _
                                                          Where optHour(ApplicationConstants.StartDateCol) <> "NA" _
                                                               And optHour(ApplicationConstants.EndDateCol) <> "NA" _
                                                               And optHour(ApplicationConstants.StartTimeCol) <> "" _
                                                          Select optHour _
                                                          Order By CType(optHour(ApplicationConstants.StartTimeCol), Date) Ascending)
            Dim extendedDayFilterResult = (From optHour In dayFilterResult.AsEnumerable Where optHour(ApplicationConstants.StartDateCol) >= appointmentStartDate.ToString("dd/MM/yyyy") Select optHour Order By CType(optHour(ApplicationConstants.StartDateCol), Date) Ascending)

            Dim operativeWorkingHourForCurrentDayDt As DataTable = If(extendedDayFilterResult.Any(), extendedDayFilterResult.CopyToDataTable(), operativeWorkingHourDt.Clone())
            ' operativeWorkingHourForCurrentDayDt = operativeWorkingHourForCurrentDayDt.AsEnumerable.Where(p=> p.Field<string>("column_name")=="desire_value").CopyToDataTable()
            If (operativeWorkingHourForCurrentDayDt.Rows.Count > 0) Then

                For Each operativeWorkingHoursBlock As DataRow In operativeWorkingHourForCurrentDayDt.Rows
                    Dim extendedBlockStartTime As String = operativeWorkingHoursBlock(ApplicationConstants.StartTimeCol).ToString()
                    Dim extendedBlockStartDateTime As DateTime = Convert.ToDateTime(Convert.ToString(Convert.ToDateTime(operativeWorkingHoursBlock(ApplicationConstants.StartDateCol)).ToLongDateString() + " ") & extendedBlockStartTime)
                    dateToFilter = extendedBlockStartDateTime
                    result = True
                    ' get operative day end hour for specific day
                    Dim extendedBlockEndTime As String = operativeWorkingHoursBlock(ApplicationConstants.EndTimeCol).ToString()
                    Dim extendedBlockEndDateTime As DateTime = Convert.ToDateTime(Convert.ToString(Convert.ToDateTime(operativeWorkingHoursBlock(ApplicationConstants.EndDateCol)).ToLongDateString() + " ") & extendedBlockEndTime)

                    Dim isTimeSlotAlreadyAdded As Boolean = False
                    isTimeSlotAlreadyAdded = checkTimeSlotDisplayed(tempTradeAptBo, dateToFilter, extendedBlockEndDateTime, operativeId)


                    Dim apptEndDateTime As DateTime = dateToFilter.AddHours(workDuration)
                    Dim apptEndDateTimeInSec As Integer
                    Dim extendedBlockEndDateTimeInSec As Integer
                    apptEndDateTimeInSec = GeneralHelper.convertDateInSec(apptEndDateTime)
                    extendedBlockEndDateTimeInSec = GeneralHelper.convertDateInSec(extendedBlockEndDateTime)
                    If apptEndDateTimeInSec > extendedBlockEndDateTimeInSec Then
                        result = False
                        Continue For
                    Else

                        Dim isExtendedApptExist As Boolean = True
                        isExtendedApptExist = isExtendedAppointmentExist(appointmentsDt, operativeId, dateToFilter, apptEndDateTime, loggingTime)
                        If isExtendedApptExist = False Then
                            Exit For
                        End If
                    End If
                Next
            End If
            Return result

        End Function

#End Region


#Region "is Extended Appointment Exist"
        ''' <summary>
        ''' This function checks either operative has appointment in the given Extended start time and end time
        ''' </summary>
        ''' <param name="appointmentsDt"></param>
        ''' <param name="operativeId"></param>
        ''' <param name="appointmentStartDateTime"></param>
        ''' <param name="appointmentEndDateTime"></param>
        ''' <param name="loggingTime"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Private Function isExtendedAppointmentExist(ByVal appointmentsDt As DataTable, ByVal operativeId As Integer, ByRef appointmentStartDateTime As Date, ByVal appointmentEndDateTime As Date, ByVal loggingTime As Integer)

            ''çonvert time into seconds for comparison

            Dim startTimeInSec As Integer
            Dim endTimeInSec As Integer
            Dim result As Boolean = False
            startTimeInSec = GeneralHelper.convertDateInSec(appointmentStartDateTime)
            endTimeInSec = GeneralHelper.convertDateInSec(appointmentEndDateTime)

            Dim appointments = appointmentsDt.AsEnumerable()
            'Dim appResult = (From app In appointments Where app("AppointmentDate") = runningDateAlias AndAlso app("OperativeId") = operativeId AndAlso (app("StartTimeInSec") >= startTimeInSec Or endTimeInSec <= app("EndTimeInSec")) Select app)
            Dim appResult = (From app In appointments Where app("OperativeId") = operativeId _
                                                            AndAlso (1 = 0 _
                                                            OrElse (startTimeInSec = app("StartTimeInSec") AndAlso endTimeInSec < app("EndTimeInSec")) _
                                                            OrElse (startTimeInSec < app("EndTimeInSec") AndAlso endTimeInSec = app("EndTimeInSec")) _
                                                            OrElse (startTimeInSec > app("StartTimeInSec") AndAlso endTimeInSec < app("EndTimeInSec")) _
                                                            OrElse (startTimeInSec < app("StartTimeInSec") AndAlso endTimeInSec > app("EndTimeInSec")) _
                                                            OrElse (startTimeInSec < app("StartTimeInSec") AndAlso endTimeInSec > app("StartTimeInSec")) _
                                                            OrElse (startTimeInSec < app("EndTimeInSec") AndAlso endTimeInSec > app("EndTimeInSec"))) _
                                                        AndAlso app("AppointmentType") <> "Planned Appointment" _
                            Select app Order By app("EndTimeInSec") Ascending)
            If (appResult IsNot Nothing) And appResult.Count() > 0 Then
                appointmentStartDateTime = GeneralHelper.unixTimeStampToDateTime(Convert.ToDouble(appResult.Last().Field(Of Integer)("EndTimeInSec")))
                result = True
            End If

            Return result
        End Function
#End Region

#Region "Check Time Slot already displayed in Gridview"
        ''' <summary>
        ''' Check Time Slot already displayed in Gridview
        ''' </summary>
        ''' <param name="tempTradeAptBo"></param>
        ''' <remarks></remarks>
        Public Function checkTimeSlotDisplayed(ByVal tempTradeAptBo As TempAppointmentDtBO, ByRef appointmentStartDate As DateTime, ByVal extendedBlockEndDateTime As DateTime, operativeId As Integer)
            Dim result As Boolean = False
            Dim runningDate As DateTime = appointmentStartDate


            Dim appointments = tempTradeAptBo.dt.AsEnumerable
            Dim appResult = (From app In appointments _
                           Where app(TempAppointmentDtBO.startDateColName) >= runningDate _
                          AndAlso app(TempAppointmentDtBO.endDateColName) <= extendedBlockEndDateTime _
                           And app(TempAppointmentDtBO.operativeIdColName) = operativeId Select app Order By app(TempAppointmentDtBO.startDateColName) Ascending)
            If appResult.Count() > 0 Then
                appointmentStartDate = appResult.Last.Item(TempAppointmentDtBO.endDateColName)
                result = True
            End If
            Return result
        End Function

#End Region
#End Region

#End Region

#Region "Update temp fault Duration by tempFaultId"

        Sub updateTempFaultDuration(tempFaultId As Integer, duration As Decimal)
            Dim objFaultAppointmentDal As New FaultAppointmentDAL
            objFaultAppointmentDal.updateTempFaultDuration(tempFaultId, duration)
        End Sub

#End Region

#Region "is Time Slot Available"

        Function isTimeSlotAvailable(ByVal operativeId As Integer, ByVal startDateTime As Date, ByVal endDateTime As Date) As Boolean
            Dim appointmentDal As New FaultAppointmentDAL
            isTimeSlotAvailable = appointmentDal.isTimeSlotAvailable(operativeId, startDateTime, endDateTime)
        End Function

#End Region

    End Class
End Namespace
