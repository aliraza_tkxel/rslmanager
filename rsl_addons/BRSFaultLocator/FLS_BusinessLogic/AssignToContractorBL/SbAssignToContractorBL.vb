﻿Imports FLS_BusinessObject

Public Class SbAssignToContractorBL
#Region "Get Cost Centre Drop Down Values."

    Sub GetCostCentreDropDownVales(ByRef dropDownList As List(Of DropDownBO))
        Dim objAssignToContractorDAL = New SbAssignToContractorDAL
        objAssignToContractorDAL.GetCostCentreDropDownVales(dropDownList)
    End Sub

#End Region

#Region "Get Budget Head Drop Down Values by Cost Centre Id"

    Sub GetBudgetHeadDropDownValuesByCostCentreId(ByRef dropDownList As List(Of DropDownBO), ByRef CostCentreId As Integer)
        Dim objAssignToContractorDAL As New SbAssignToContractorDAL
        objAssignToContractorDAL.GetBudgetHeadDropDownValuesByCostCentreId(dropDownList, CostCentreId)
    End Sub

#End Region

#Region "Get Expenditure Drop Down Values by Budget Head Id"

    Sub GetExpenditureDropDownValuesByBudgetHeadId(ByRef expenditureBOList As List(Of ExpenditureBO), ByRef BudgetHeadId As Integer, ByRef EmployeeId As Integer)
        Dim objAssignToContractorDAL As New SbAssignToContractorDAL
        objAssignToContractorDAL.GetExpenditureDropDownValuesByBudgetHeadId(expenditureBOList, BudgetHeadId, EmployeeId)
    End Sub

#End Region

#Region "Get Contractor Having Planned Contract"

    Sub GetPlannedContractors(ByRef dropDownList As List(Of DropDownBO))
        Dim objAssignToContractorDAL As New SbAssignToContractorDAL
        objAssignToContractorDAL.GetPlannedContractors(dropDownList)
    End Sub

#End Region

#Region "Get Contact DropDown Values By ContractorId"

    Sub GetContactDropDownValuesbyContractorId(ByRef dropDownList As List(Of DropDownBO), ByRef ContractorId As Integer)
        Dim objAssignToContractorDAL As New SbAssignToContractorDAL
        objAssignToContractorDAL.GetContactDropDownValuesbyContractorId(dropDownList, ContractorId)
    End Sub

#End Region

#Region "Get Vat Drop down Values - Vat Rate as Value Field and Vat Name as text field."

    Sub getVatDropDownValues(ByRef vatBoList As List(Of ContractorVatBO))
        Dim objAssignToContractorDAL As New SbAssignToContractorDAL
        objAssignToContractorDAL.GetVatDropDownValues(vatBoList)
    End Sub

    Sub getVatDropDownValues(ByRef dropDownList As List(Of DropDownBO))
        Dim objAssignToContractorDAL As New SbAssignToContractorDAL
        objAssignToContractorDAL.GetVatDropDownValues(dropDownList)
    End Sub

#End Region

#Region "get Block List for dropdown List"
    ''' <summary>
    ''' get Block List for dropdown List
    ''' </summary>
    ''' <param name="schemeId"></param>
    ''' <returns></returns>
    Public Function getBlockListBySchemeId(schemeId As Integer) As DataSet
        Dim objAssignToContractorDAL = New SbAssignToContractorDAL
        Return objAssignToContractorDAL.getBlockListBySchemeId(schemeId)
    End Function
#End Region

#Region "get Area List "
    ''' <summary>
    ''' get Attribute By AreaId
    ''' </summary>
    ''' <returns></returns>
    Public Function getAreaList() As DataSet
        Dim objAssignToContractorDAL = New SbAssignToContractorDAL
        Return objAssignToContractorDAL.getAreaList()

    End Function
#End Region

#Region "get Attribute By AreaId"
    ''' <summary>
    ''' get Attribute By AreaId
    ''' </summary>
    ''' <param name="areaId"></param>
    ''' <returns></returns>
    Public Function getAttributeByAreaId(areaId As Integer) As DataSet
         Dim objAssignToContractorDAL = New SbAssignToContractorDAL
        Return objAssignToContractorDAL.getAttributeByAreaId(areaId)

    End Function
#End Region

#Region "Assign Work to Contractor"

    Function assignToContractor(ByRef assignToContractorBo As AssignToContractorBO) As Boolean
        Dim objAssignToContractorDAL As New SbAssignToContractorDAL
        Return objAssignToContractorDAL.assignToContractor(assignToContractorBo)
    End Function

#End Region

#Region "Get Details for email"

    ''' <summary>
    ''' This function is to get details for email, these details include contractor name, email and other details.
    ''' Contact details of customer
    ''' </summary>
    ''' <param name="detailsForEmailDS">Dataset to get details returned from Database.</param>
    ''' <param name="assignToContractorBo">Assign to Contractor Bo containing values used to get details</param>
    ''' <remarks></remarks>
    Sub getdetailsForEmail(ByRef assignToContractorBo As AssignToContractorBO, ByRef detailsForEmailDS As DataSet)
        Dim objAssignToContractorDAL As New SbAssignToContractorDAL
        objAssignToContractorDAL.getdetailsForEmail(assignToContractorBo, detailsForEmailDS)
    End Sub

#End Region

#Region "get Replacement Due For Assign To Contractor "
    ''' <summary>
    ''' get Replacement Due For Assign To Contractor
    ''' </summary>
    ''' <returns></returns>
    Public Function getReplacementDueForAssignToContractor(ByVal itemId As Integer, ByVal schemeId As Integer, ByVal blockId As Integer) As DataSet
         Dim objAssignToContractorDAL As New SbAssignToContractorDAL
        Return objAssignToContractorDAL.getReplacementDueForAssignToContractor(itemId, schemeId, blockId)

    End Function
#End Region
End Class
