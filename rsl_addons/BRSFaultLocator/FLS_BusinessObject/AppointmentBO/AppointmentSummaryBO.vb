﻿Namespace FLS_BusinessObject

    Public Class AppointmentSummaryBO : Inherits BaseBO

#Region "Attributes"


#End Region

#Region "Construtor"
        Public Sub New()
            OperativeId = 0
            FaultsList = String.Empty
            Duration = 0.0
            Time = String.Empty
            EndTime = String.Empty
            AppointmentStartDate = DateTime.Now
            FaultNotes = String.Empty
            AppointmentNotes = String.Empty
            PropertyId = String.Empty
            CustomerId = 0
            PropertyId = String.Empty
            IsRecall = False
            IsCalendarAppointment = False
            UserId = 0

            IsNoEntry = False
            NoEntryFaultLogId = 0
            AreaName = String.Empty

        End Sub
#End Region

#Region "Properties"
        ' Get / Set property for _operativeid
        Public Property OperativeId As Integer

        ' Get / Set property for _faultslist
        Public Property FaultsList As String

        ' Get / Set property for _duration
        Public Property Duration As Double

        ' Get / Set property for _time
        Public Property Time As String

        ' Get / Set property for _endtime
        Public Property EndTime As String

        ' Get / Set property for _appointmentdate
        Public Property AppointmentStartDate As DateTime

        ' Get / Set property for _faultnotes
        Public Property FaultNotes As String

        ' Get / Set property for _faultnotes
        Public Property AppointmentNotes As String

        ' Get / Set property for _propertyid
        Public Property PropertyId As String

        ' Get / Set property for _customerid
        Public Property CustomerId As Integer

        ' Get / Set property for _isRecall
        Public Property IsRecall As Boolean

        ' Get / Set property for _isCalendarAppointment
        Public Property IsCalendarAppointment As Boolean

        ' Get / Set property for _userId
        Public Property UserId As Integer

        ' Get / Set property for _isNoEntry
        Public Property IsNoEntry As Boolean

        ' Get / Set property for _NoEntryJSN
        Public Property NoEntryFaultLogId As Integer

        Public Property AppointmentEndDate As DateTime
        ' Get / Set property for _propertyid
        Public Property Schemeid As Integer

        ' Get / Set property for _customerid
        Public Property Blockid As Integer

        Public Property AreaName As String
#End Region

    End Class

End Namespace
