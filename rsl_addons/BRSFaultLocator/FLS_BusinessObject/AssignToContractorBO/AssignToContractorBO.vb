﻿Public Class AssignToContractorBO
#Region "Properties"

    Public Property ContactId As Integer
    Public Property ContractorId As Integer

    Public Property AreaId As Integer
    Public Property AttributeId As Integer
    Public Property UserId As Integer
    Public Property Estimate As Decimal
    Public Property EstimateRef As String
    Public Property AssignWorkType As Integer
    Public Property EmpolyeeId As Integer
    Public Property POStatus As Integer

    Public Property SchemeId As Integer
    Public Property BlockId As Integer
    Public Property FaultContractorId As Integer
    Public Property JournalId As Integer
    Public Property WorksRequired As DataTable
    Public Property PropertyId As String
    Public Property OrderId As Integer
    Public Property FaultId As Integer
    Public Property ReportedFault As String
    Public Property FaultWorksRequired As String
    Public Property DefaultNetCost As Double
    Public Property DefaultVatId As Integer
    Public Property DefaultVat As Double
    Public Property DefaultGross As Double
    Public Property TempFaultId As Integer
    Public Property jsn As String
    Public Property POCurrentStatus As Integer

#End Region
End Class
