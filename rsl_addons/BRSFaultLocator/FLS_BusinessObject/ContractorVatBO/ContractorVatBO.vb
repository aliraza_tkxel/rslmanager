﻿Public Class ContractorVatBO : Inherits DropDownBO

#Region "Constructor(s)"

    Sub New(ByVal id As Integer, ByVal name As String, ByVal vatRate As Decimal)
        MyBase.New(id, name)
        _vatRate = vatRate
    End Sub

#End Region

#Region "Properties"

    Private _vatRate As Decimal
    Public ReadOnly Property VatRate() As Decimal
        Get
            Return _vatRate
        End Get
    End Property

#End Region


End Class
