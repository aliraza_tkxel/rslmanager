﻿Imports System

Namespace FLS_BusinessObject
    Public Class CalendarAppointmentsBO : Inherits BaseBO

#Region "Construtor"

        Public Sub New(ByVal id As Integer, ByVal name As String)

            PatchId = id
            Description = name

        End Sub

        Public Sub New()
            PatchId = -1
            SchemeId = -1
            AppointmentType = String.Empty
            TradeId = -1
            Location = String.Empty
            Description = String.Empty
            AppointmentId = 0
        End Sub

#End Region

#Region "Properties"

#Region "Calendar Appointments Details"

        Public Property PatchId() As Integer

        Public Property SchemeId() As Integer

        Public Property AppointmentType() As String

        Public Property Location() As String

        Public Property TradeId() As Integer

        Public Property Description() As String

        Public Property StartDate() As DateTime

        Public Property EndDate() As DateTime

        Public Property AppointmentId() As Integer

        Public Property JSN() As String

        Public Property Tenant() As String

        Public Property Addresss() As String

#End Region

#Region "Get Employees Leaves Info"

        Public Property EmpolyeeIds() As String

        Public Property LeaveStartDate() As Date
            
        Public Property ReturnDate() As Date

        Public Property DocumentPath() As String

        Public Property OperativeId As Integer

#End Region

#End Region

    End Class
End Namespace
