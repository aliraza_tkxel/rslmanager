﻿
Namespace FLS_BusinessObject

    Public Class SchedularSearchBO : Inherits BaseBO

#Region "Attributes"
        Private _patchId As Integer
        Private _developmentId As Integer
        Private _tradeId As Integer
        Private _jsn As String
        Private _operative As String
        Private _tenant As String
        Private _address As String
        Private _pageIndex As Integer


#End Region

#Region "Construtor"
        Public Sub New()
            _patchId = 0
            _developmentId = 0
            _tradeId = 0
            _jsn = String.Empty
            _operative = String.Empty
            _tenant = String.Empty
            _address = String.Empty
            _pageIndex = 1
        End Sub
#End Region

#Region "Properties"

        ' Get / Set property for _pageIndex
        Public Property PageIndex() As Integer

            Get
                Return _pageIndex
            End Get

            Set(ByVal index As Integer)
                _pageIndex = index
            End Set

        End Property

        ' Get / Set property for _operative
        Public Property Operative() As String

            Get
                Return _operative
            End Get

            Set(ByVal value As String)
                _operative = value
            End Set

        End Property

        ' Get / Set property for _patchId
        Public Property Patch() As Integer

            Get
                Return _patchId
            End Get

            Set(ByVal value As Integer)
                _patchId = value
            End Set

        End Property
        ' Get / Set property for _developmentId
        Public Property Scheme() As Integer

            Get
                Return _developmentId
            End Get

            Set(ByVal value As Integer)
                _developmentId = value
            End Set

        End Property

        ' Get / Set property for _tradeId
        Public Property Trade() As Integer

            Get
                Return _tradeId
            End Get

            Set(ByVal value As Integer)
                _tradeId = value
            End Set

        End Property

        ' Get / Set property for _jsn
        Public Property JobSheetNumber() As String

            Get
                Return _jsn
            End Get

            Set(ByVal value As String)
                _jsn = value
            End Set

        End Property


        ' Get / Set property for _tenant
        Public Property Tenant() As String

            Get
                Return _tenant
            End Get

            Set(ByVal value As String)
                _tenant = value
            End Set

        End Property


        ' Get / Set property for _address
        Public Property Address() As String

            Get
                Return _address
            End Get

            Set(ByVal value As String)
                _address = value
            End Set

        End Property

#End Region

    End Class

End Namespace