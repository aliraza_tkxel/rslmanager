﻿Imports System

Public Class ExpenditureBO : Inherits DropDownBO


#Region "Attributes"

    Private _limit As Decimal
    Private _remaining As Decimal

#End Region

#Region "Constructor(s)"

    Sub New(ByVal id As Integer, ByVal name As String, ByVal limit As Decimal, ByVal remaining As Decimal)
        MyBase.New(id, name)
        _limit = limit
        _remaining = remaining
    End Sub

#End Region

#Region "Properties"

    Public ReadOnly Property Limit() As Decimal
        Get
            Return _limit
        End Get
    End Property

    Public ReadOnly Property Remaining() As Decimal
        Get
            Return _remaining
        End Get
    End Property

#End Region

End Class
