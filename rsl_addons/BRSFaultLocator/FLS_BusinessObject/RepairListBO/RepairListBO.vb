﻿Imports FLS_BusinessObject

Namespace FLS_BusinessObject
    ''' <summary>
    ''' This class will be used to insert repair data for contractor.
    ''' </summary>
    ''' <remarks></remarks>
    Public Class RepairListBO : Inherits BaseBO
#Region "Attribute"
        Private _faultLogId As Integer
        Private _faultRepairIdList As String
        Private _time As String
        Private _inspectionDate As DateTime
        Private _userId As Integer

#End Region

#Region "Properties"
        ' Get / Set property for _faultLogId
        Public Property FaultLogId() As Integer

            Get
                Return _faultLogId
            End Get

            Set(ByVal value As Integer)
                _faultLogId = value
            End Set

        End Property

        ' Get / Set property for _faultRepairIdList
        Public Property FaultRepairIdList() As String

            Get
                Return _faultRepairIdList
            End Get

            Set(ByVal value As String)
                _faultRepairIdList = value
            End Set

        End Property

        ' Get / Set property for _time
        Public Property Time() As String

            Get
                Return _time
            End Get

            Set(ByVal value As String)
                _time = value
            End Set

        End Property

        ' Get / Set property for _inspectionDate
        Public Property InspectionDate() As DateTime

            Get
                Return _inspectionDate
            End Get

            Set(ByVal value As DateTime)
                _inspectionDate = value
            End Set

        End Property

        ' Get / Set property for _userId
        Public Property UserId() As Integer

            Get
                Return _userId
            End Get

            Set(ByVal value As Integer)
                _userId = value
            End Set

        End Property

#End Region
    End Class
End Namespace
