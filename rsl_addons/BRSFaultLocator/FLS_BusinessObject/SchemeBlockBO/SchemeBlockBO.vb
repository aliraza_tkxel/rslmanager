﻿Namespace FLS_BusinessObject
    Public Class SchemeBlockBO : Inherits BaseBO
        Private _schemeId As Integer
        Public Property SchemeId() As Integer
            Get
                Return _schemeId
            End Get
            Set(ByVal value As Integer)
                _schemeId = value
            End Set
        End Property

        Private _blockId As Integer
        Public Property BlockId() As Integer
            Get
                Return _blockId
            End Get
            Set(ByVal value As Integer)
                _blockId = value
            End Set
        End Property

        Private _schemeName As String
        Public Property SchemeName() As String
            Get
                Return _schemeName
            End Get
            Set(ByVal value As String)
                _schemeName = value
            End Set
        End Property

        Private _blockName As String
        Public Property BlockName() As String
            Get
                Return _blockName
            End Get
            Set(ByVal value As String)
                _blockName = value
            End Set
        End Property


    End Class
End Namespace