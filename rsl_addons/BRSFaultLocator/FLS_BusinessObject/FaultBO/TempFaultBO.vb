﻿Namespace FLS_BusinessObject

    Public Class TempFaultBO
#Region "Attributes"
        Private _tempfaultid As Integer
        Private _tradeid As Integer
        Private _contractorIndex As Integer
#End Region

#Region "Construtor"
        Public Sub New()
            _tempfaultid = 0
            _tradeid = 0
            _contractorIndex = -1
        End Sub

#End Region

#Region "Properties"
        ' Get / Set property for _tempfaultid
        Public Property TempFaultId() As Integer

            Get
                Return _tempfaultid
            End Get

            Set(ByVal value As Integer)
                _tempfaultid = value
            End Set

        End Property
        ' Get / Set property for _tradeid
        Public Property Trade() As Integer

            Get
                Return _tradeid
            End Get

            Set(ByVal value As Integer)
                _tradeid = value
            End Set

        End Property

        ' Get / Set property for _orgid
        Public Property Contractor() As Integer

            Get
                Return _contractorIndex
            End Get

            Set(ByVal value As Integer)
                _contractorIndex = value
            End Set

        End Property

#End Region

    End Class
End Namespace

