﻿Imports System

Namespace FLS_BusinessObject
    Public Class FaultBO : Inherits BaseBO

#Region "Attributes"

        Private _faultid As Integer


        Private _elementID As Integer
        Private _priorityID As Integer
        Private _description As String
        Private _netCost As Double?
        Private _vat As Double?
        Private _gross As Double?
        Private _vatRateID As Integer?
        Private _effectFrom As String
        Private _recharge As Boolean
        Private _preInspection As Integer
        Private _postInspection As Integer
        Private _stockConditionItem As Integer
        Private _faultActive As Boolean
        Private _tenantsOnline As Boolean
        Private _faultAction As Integer
        Private _submitDate As String
        Private _noEntryDate As String

        Private _isInflative As Boolean
        Private _accepted As Boolean
        Private _areaid As Integer
        Private _isContractor As Boolean
        Private _duration As Double?
        Private _inflationValue As Double
        Private _locationID As Integer
        Private _vatRate As Double
        Private _vatName As String
        Private _isRecordRetrieved As Boolean
        Private _userId As Integer
        Private _repairId As Integer
        Private _isGasSafe As Boolean
        Private _isOFTEC As Boolean
        Private _trades As DataTable
        Private _faultRepairs As DataTable

        Private _customerid As Integer
        Private _quantity As Integer
        Private _propertyid As String
        Private _problemdays As Integer
        Private _recuringproblem As Boolean
        Private _communalproblem As Boolean
        Private _notes As String
        Private _itemActionId As Integer


        Private _orgid As Integer
        Private _statusId As Integer
        Private _dueDate As String
        Private _jobSheetNumber As String
        Private _isRecall As Boolean
        Private _isAppointmentConfirmed As Boolean
        Private _faultLogId As Integer
        Private _faulTradeId As Integer
        Private _originalFaultLogId As Integer
        Private _isRearrange As Boolean

        Private _tradesToDelete As List(Of Integer)
        Private _tradesToInsert As List(Of Integer)

        Private _isFollowOn As Boolean
        Private _followOnFaultLogID As Integer
        Private _followOnNotes As String
        Private _schemeId As Integer
        Private _blockId As Nullable(Of Integer)
        Private _isNewFault As Boolean

#End Region

#Region "Construtor"

        Public Sub New()
            _faultid = 0
            _elementID = -1
            _priorityID = -1
            _description = String.Empty
            _netCost = 0.0
            _vat = 0.0
            _gross = 0.0
            _vatRateID = Nothing
            _effectFrom = String.Empty
            _recharge = False
            _preInspection = False
            _postInspection = False
            _stockConditionItem = False
            _faultActive = False
            _tenantsOnline = False
            _faultAction = False
            _submitDate = Nothing
            _noEntryDate = Nothing
            _isInflative = False
            _accepted = False
            _areaid = -1
            _isContractor = False
            _duration = Nothing
            _inflationValue = -1
            _locationID = -1
            _vatRate = -1
            _vatName = Nothing
            _isRecordRetrieved = False
            _userId = -1
            _repairId = -1
            _trades = New DataTable
            CreateTradeColumns(_trades)
            _faultRepairs = New DataTable
            CreateFaultRepairColumns(_faultRepairs)
            _customerid = 0
            _quantity = 0
            _problemdays = 0
            _recuringproblem = False
            _communalproblem = False
            _notes = String.Empty
            _propertyid = String.Empty
            _itemActionId = 0

            _orgid = 0
            _statusId = 0
            _dueDate = String.Empty
            _jobSheetNumber = String.Empty
            _isRecall = False
            _isAppointmentConfirmed = False
            _faultLogId = 0
            _faulTradeId = -1
            _originalFaultLogId = 0
            _isRearrange = False

            _isFollowOn = False
            _followOnFaultLogID = 0

            _tradesToDelete = New List(Of Integer)
            _tradesToInsert = New List(Of Integer)
            _followOnNotes = String.Empty
            _schemeId = 0
            _blockId = Nothing
            _isNewFault = False

        End Sub

        Public Sub New(ByVal faultId As Integer, ByVal description As String)

            Me.FaultID = faultId
            Me.Description = description

            'Default Values for all other attributes
            _elementID = -1
            _priorityID = -1
            _netCost = 0.0
            _vat = 0.0
            _gross = 0.0
            _vatRateID = Nothing
            _effectFrom = String.Empty
            _recharge = False
            _preInspection = False
            _postInspection = False
            _stockConditionItem = False
            _faultActive = False
            _tenantsOnline = False
            _faultAction = False
            _submitDate = Nothing
            _noEntryDate = Nothing
            _isInflative = False
            _accepted = False
            _areaid = -1
            _isContractor = False
            _duration = Nothing
            _inflationValue = -1
            _locationID = -1
            _vatRate = -1
            _vatName = Nothing
            _isRecordRetrieved = False
            _userId = -1
            _repairId = -1
            _trades = New DataTable
            CreateTradeColumns(_trades)
            _faultRepairs = New DataTable
            CreateFaultRepairColumns(_faultRepairs)

            _customerid = 0
            _quantity = 0
            _problemdays = 0
            _recuringproblem = False
            _communalproblem = False
            _notes = String.Empty
            _propertyid = String.Empty
            _itemActionId = 0

            _orgid = 0
            _statusId = 0
            _dueDate = String.Empty
            _jobSheetNumber = String.Empty
            _isRecall = False
            _isAppointmentConfirmed = False
            _faultLogId = 0
            _faulTradeId = -1
            _originalFaultLogId = 0
            _isRearrange = False

            _isFollowOn = False
            _followOnFaultLogID = 0

            _tradesToDelete = New List(Of Integer)
            _tradesToInsert = New List(Of Integer)
            _followOnNotes = String.Empty
            _schemeId = 0
            _blockId = Nothing
            _isNewFault = False
        End Sub

#End Region

#Region "Properties"

        'Get/Set Property for _isNewFault
        Public Property IsNewFault() As Boolean
            Get
                Return _isNewFault
            End Get
            Set(ByVal value As Boolean)
                _isNewFault = value
            End Set
        End Property

        'Get/Set Property for _faulTradeId
        Public Property FaultTradeId() As Integer
            Get
                Return _faulTradeId
            End Get
            Set(ByVal value As Integer)
                _faulTradeId = value
            End Set
        End Property
        'Get/Set Property for _itemActionId
        Public Property ItemActionID() As Integer
            Get
                Return _itemActionId
            End Get
            Set(ByVal value As Integer)
                _itemActionId = value
            End Set
        End Property
        'Get/Set Property for _faultID
        Public Property FaultID() As Integer
            Get
                Return _faultid
            End Get
            Set(ByVal value As Integer)
                _faultid = value
            End Set
        End Property
        'Get/Set Property for _orgid
        Public Property OrgID() As Integer
            Get
                Return _orgid
            End Get
            Set(ByVal value As Integer)
                _orgid = value
            End Set
        End Property
        'Get/Set Property for _statusId
        Public Property StatusID() As Integer
            Get
                Return _statusId
            End Get
            Set(ByVal value As Integer)
                _statusId = value
            End Set
        End Property
        'Get/Set Property for _dueDate
        Public Property DueDate() As String
            Get
                Return _dueDate
            End Get
            Set(ByVal value As String)
                _dueDate = value
            End Set
        End Property
        'Get/Set Property for _jobSheetNumber
        Public Property JobSheetNumber() As String
            Get
                Return _jobSheetNumber
            End Get
            Set(ByVal value As String)
                _jobSheetNumber = value
            End Set
        End Property

        'Get/Set Property for _elementID
        Public Property ElementID() As Integer
            Get
                Return _elementID
            End Get
            Set(ByVal value As Integer)
                _elementID = value
            End Set
        End Property

        'Get/Set Property for _priorityID
        Public Property PriorityID() As Integer
            Get
                Return _priorityID
            End Get
            Set(ByVal value As Integer)
                _priorityID = value
            End Set
        End Property

        'Get/Set Property for _description
        Public Property Description() As String
            Get
                Return _description
            End Get
            Set(ByVal value As String)
                _description = value
            End Set
        End Property

        'Get/Set Property for _netCose
        Public Property NetCost() As Double?
            Get
                Return _netCost
            End Get
            Set(ByVal value As Double?)
                _netCost = value
            End Set
        End Property

        'Get/Set Property for _vat
        Public Property Vat() As Double?
            Get
                Return _vat
            End Get
            Set(ByVal value As Double?)
                _vat = value
            End Set
        End Property

        'Get/Set Property for _gross
        Public Property Gross() As Double?
            Get
                Return _gross
            End Get
            Set(ByVal value As Double?)
                _gross = value
            End Set
        End Property

        'Get/Set Property for _vatRateID
        Public Property VatRateID() As Integer?
            Get
                Return _vatRateID
            End Get
            Set(ByVal value As Integer?)
                _vatRateID = value
            End Set
        End Property

        'Get/Set Property for _effectFrom
        Public Property EffectFrom() As String
            Get
                Return _effectFrom
            End Get
            Set(ByVal value As String)
                _effectFrom = value
            End Set
        End Property

        'Get/Set Property for _recharge
        Public Property Recharge() As Boolean
            Get
                Return _recharge
            End Get
            Set(ByVal value As Boolean)
                _recharge = value
            End Set
        End Property

        'Get/Set Property for _preInspection
        Public Property PreInspection() As Integer
            Get
                Return _preInspection
            End Get
            Set(ByVal value As Integer)
                _preInspection = value
            End Set
        End Property

        'Get/Set Property for _postInspection
        Public Property PostInspection() As Integer
            Get
                Return _postInspection
            End Get
            Set(ByVal value As Integer)
                _postInspection = value
            End Set
        End Property

        'Get/Set Property for _stockConditionItem
        Public Property StockConditionItem() As Integer
            Get
                Return _stockConditionItem
            End Get
            Set(ByVal value As Integer)
                _stockConditionItem = value
            End Set
        End Property

        'Get/Set Property for _faultActive
        Public Property FaultActive() As Boolean
            Get
                Return _faultActive
            End Get
            Set(ByVal value As Boolean)
                _faultActive = value
            End Set
        End Property

        Public Property TenantsOnline() As Boolean
            Get
                Return _tenantsOnline
            End Get
            Set(ByVal value As Boolean)
                _tenantsOnline = value
            End Set
        End Property

        

        'Get/Set Property for _faultAction
        Public Property FaultAction() As Integer
            Get
                Return _faultAction
            End Get
            Set(ByVal value As Integer)
                _faultAction = value
            End Set
        End Property

        'Get/Set Property for _submitDate
        Public Property SubmitDate() As String
            Get
                Return _submitDate
            End Get
            Set(ByVal value As String)
                _submitDate = value
            End Set
        End Property

        'Get/Set Property for _noEntryDate
        Public Property NoEntryDate() As String
            Get
                Return _noEntryDate
            End Get
            Set(ByVal value As String)
                _noEntryDate = value
            End Set
        End Property

        'Get/Set Property for _isInflative
        Public Property IsInflative() As Boolean
            Get
                Return _isInflative
            End Get
            Set(ByVal value As Boolean)
                _isInflative = value
            End Set
        End Property

        'Get/Set Property for _accepted
        Public Property Accepted() As Boolean
            Get
                Return _accepted
            End Get
            Set(ByVal value As Boolean)
                _accepted = value
            End Set
        End Property

        'Get/Set Property for _areaID
        Public Property AreaID() As Integer
            Get
                Return _areaid
            End Get
            Set(ByVal value As Integer)
                _areaid = value
            End Set
        End Property

        'Get/Set Property for _isContractor
        Public Property IsContractor() As Boolean
            Get
                Return _isContractor
            End Get
            Set(ByVal value As Boolean)
                _isContractor = value
            End Set
        End Property

        'Get/Set Property for _duration
        Public Property Duration() As Double?
            Get
                Return _duration
            End Get
            Set(ByVal value As Double?)
                _duration = value
            End Set
        End Property

        'Get/Set Property for _inflationValue
        Public Property InflationValue() As Double
            Get
                Return _inflationValue
            End Get
            Set(ByVal value As Double)
                _inflationValue = value
            End Set
        End Property

        'Get/Set Property for _locationID
        Public Property LocationID() As Integer
            Get
                Return _locationID
            End Get
            Set(ByVal value As Integer)
                _locationID = value
            End Set
        End Property

        'Get/Set Property for _vatRate
        Public Property VatRate() As Double
            Get
                Return _vatRate
            End Get
            Set(ByVal value As Double)
                _vatRate = value
            End Set
        End Property

        'Get/Set Property for _VatName
        Public Property VatName() As String
            Get
                Return _vatName
            End Get
            Set(ByVal value As String)
                _vatName = value
            End Set
        End Property

        ' Get/Set property for _isRecordRetrieved
        Public Property IsRecordRetrieved() As Boolean

            Get
                Return _isRecordRetrieved
            End Get

            Set(ByVal value As Boolean)
                _isRecordRetrieved = value
            End Set

        End Property

        'Get/Set property for _userId
        Public Property UserID() As Integer
            Get
                Return _userId
            End Get
            Set(ByVal value As Integer)
                _userId = value
            End Set
        End Property

        'Get/Set property for _repairId
        Public Property RepairId() As Integer
            Get
                Return _repairId
            End Get
            Set(ByVal value As Integer)
                _repairId = value
            End Set
        End Property

        'Get/Set property for _gasSafe
        Public Property isGasSafe() As Boolean
            Get
                Return _isGasSafe
            End Get
            Set(ByVal value As Boolean)
                _isGasSafe = value
            End Set
        End Property

        'Get/Set property for _repairId
        Public Property isOFTEC() As Boolean
            Get
                Return _isOFTEC
            End Get
            Set(ByVal value As Boolean)
                _isOFTEC = value
            End Set
        End Property

        'Get/Set property for _Trades
        Public Property Trades As DataTable
            Get
                Return _trades
            End Get
            Set(ByVal value As DataTable)
                _trades = value
            End Set
        End Property

        'Get/Set property for _faultRepairs
        Public Property FaultRepairs As DataTable
            Get
                Return _faultRepairs
            End Get
            Set(ByVal value As DataTable)
                _faultRepairs = value
            End Set
        End Property

        ' Get / Set property for _customerid
        Public Property CustomerId() As Integer

            Get
                Return _customerid
            End Get

            Set(ByVal value As Integer)
                _customerid = value
            End Set

        End Property

        ' Get / Set property for _propertyid
        Public Property PropertyId() As String

            Get
                Return _propertyid
            End Get

            Set(ByVal value As String)
                _propertyid = value
            End Set

        End Property

        ' Get / Set property for _quantity
        Public Property Quantity() As Integer

            Get
                Return _quantity
            End Get

            Set(ByVal value As Integer)
                _quantity = value
            End Set

        End Property

        ' Get / Set property for _problemdays
        Public Property ProblemDays() As Integer
            Get
                Return _problemdays
            End Get
            Set(ByVal value As Integer)
                _problemdays = value
            End Set

        End Property

        ' Get / Set property for _recuringproblem
        Public Property RecuringProblem() As Boolean
            Get
                Return _recuringproblem
            End Get
            Set(ByVal value As Boolean)
                _recuringproblem = value
            End Set

        End Property

        ' Get / Set property for _communalproblem
        Public Property CommunalProblem() As Boolean
            Get
                Return _communalproblem
            End Get
            Set(ByVal value As Boolean)
                _communalproblem = value
            End Set

        End Property

        ' Get / Set property for _notes
        Public Property Notes() As String
            Get
                Return _notes
            End Get
            Set(ByVal value As String)
                _notes = value
            End Set
        End Property

        'Get/Set Property for _isRecall
        Public Property IsRecall() As Boolean
            Get
                Return _isRecall
            End Get
            Set(ByVal value As Boolean)
                _isRecall = value
            End Set
        End Property

        'Get/Set Property for _isAppointmentConfirmed
        Public Property IsAppointmentConfirmed() As Boolean
            Get
                Return _isAppointmentConfirmed
            End Get
            Set(ByVal value As Boolean)
                _isAppointmentConfirmed = value
            End Set
        End Property

        'Get/Set Property for _faultLogId
        Public Property FaultLogId() As Integer
            Get
                Return _faultLogId
            End Get
            Set(ByVal value As Integer)
                _faultLogId = value
            End Set
        End Property

        'Get/Set Property for _statusId
        Public Property OriginalFaultLogId() As Integer
            Get
                Return _originalFaultLogId
            End Get
            Set(ByVal value As Integer)
                _originalFaultLogId = value
            End Set
        End Property

        'Get/Set Property for _recharge
        Public Property IsRearrange() As Boolean
            Get
                Return _isRearrange
            End Get
            Set(ByVal value As Boolean)
                _isRearrange = value
            End Set
        End Property

        'Get/Set Property for _tradesToDelete
        Public Property TradesToDelete() As List(Of Integer)
            Get
                Return _tradesToDelete
            End Get
            Set(ByVal value As List(Of Integer))
                _tradesToDelete = value
            End Set
        End Property

        'Get/Set Property for _tradesToInsert
        Public Property TradesToInsert() As List(Of Integer)
            Get
                Return _tradesToInsert
            End Get
            Set(ByVal value As List(Of Integer))
                _tradesToInsert = value
            End Set
        End Property

        ' Get / Set property for _isFollowOn
        Public Property IsFollowOn() As Boolean
            Get
                Return _isFollowOn
            End Get
            Set(ByVal value As Boolean)
                _isFollowOn = value
            End Set
        End Property

        ' Get / Set property for _followOnJSN
        Public Property FollowOnFaultLogId() As Integer
            Get
                Return _followOnFaultLogID
            End Get
            Set(ByVal value As Integer)
                _followOnFaultLogID = value
            End Set
        End Property

        ' Get / Set property for _followOnJSN
        Public Property FollowOnNotes() As String
            Get
                Return _followOnNotes
            End Get
            Set(ByVal value As String)
                _followOnNotes = value
            End Set
        End Property

        ' Complete Date Property to record Job Sheet Completion date
        Public Property CompletedDate As DateTime?

        ' Get / Set property for _schemeId
        Public Property SchemeId() As Integer
            Get
                Return _schemeId
            End Get
            Set(ByVal value As Integer)
                _schemeId = value
            End Set
        End Property

        ' Get / Set property for _blockId
        Public Property BlockId() As Nullable(Of Integer)
            Get
                Return _blockId
            End Get
            Set(ByVal value As Nullable(Of Integer))
                _blockId = value
            End Set
        End Property
#End Region

#Region "Functions"

        Private Sub CreateTradeColumns(ByRef dtTrades As DataTable)
            dtTrades.Columns.Add("FaultTradeId", Type.GetType("System.Int32"))
            dtTrades.Columns.Add("TradeId", Type.GetType("System.Int32"))
            dtTrades.Columns.Add("TradeName", Type.GetType("System.String"))
            dtTrades.Columns.Add("IsDeleted", Type.GetType("System.Boolean"))
            Dim Primarykey(1) As DataColumn
            Primarykey(0) = dtTrades.Columns("TradeId")
            dtTrades.PrimaryKey = Primarykey
        End Sub

        Private Sub CreateFaultRepairColumns(ByRef dtLocationRepairs As DataTable)

            dtLocationRepairs.Columns.Add("faultRepairId", Type.GetType("System.Int32"))
            dtLocationRepairs.Columns.Add("repairId", Type.GetType("System.Int32"))
            dtLocationRepairs.Columns.Add("repair", Type.GetType("System.String"))
            dtLocationRepairs.Columns.Add("isDeleted", Type.GetType("System.Boolean"))

            Dim primarykey(1) As DataColumn
            primarykey(0) = FaultRepairs.Columns("faultRepairId")
            FaultRepairs.PrimaryKey = primarykey
        End Sub

#End Region

    End Class
End Namespace
