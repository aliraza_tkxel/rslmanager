﻿Imports System
Namespace FLS_BusinessObject
    <Serializable()> _
    Public Class FaultSearchBO : Inherits BaseBO

#Region "Attributes"
        '' Holds column name by which we are sorting 
        '' rowcount is number of results we want before querying database again
        '' pageindex is page number on gridview we will use it to calculate along 
        '' with rowcount to get results in between
        Private _sortBy As String
        Private _sortOrder As String
        Private _rowCount As Integer
        Private _pageIndex As Integer
        Private _locationID As String
        Private _areaID As String
        Private _elementID As String
        Private _priorityID As String
        Private _status As String
        Private _isSearch As Boolean
        Private _description As String

#End Region

#Region "Constructor"

        Public Sub New()
            _sortBy = String.Empty
            _sortOrder = String.Empty
            _rowCount = -1
            _pageIndex = -1
            _locationID = String.Empty
            _areaID = String.Empty
            _elementID = String.Empty
            _priorityID = String.Empty
            _status = String.Empty
            _isSearch = False
            _description = String.Empty

        End Sub

#End Region

#Region "Get/Set Properties"

        'Get/Set Property for _sortBy
        Public Property SortBy() As String
            Get
                Return _sortBy
            End Get
            Set(ByVal value As String)
                _sortBy = value
            End Set
        End Property

        'Get/Set Property for _sortOrder
        Public Property SortOrder() As String
            Get
                Return _sortOrder
            End Get
            Set(ByVal value As String)
                _sortOrder = value
            End Set
        End Property

        'Get/Set Property for _rowCount
        Public Property RowCount() As Integer
            Get
                Return _rowCount
            End Get
            Set(ByVal value As Integer)
                _rowCount = value
            End Set
        End Property

        'Get/Set Property for _pageIndex
        Public Property PageIndex() As Integer
            Get
                Return _pageIndex
            End Get
            Set(ByVal value As Integer)
                _pageIndex = value
            End Set
        End Property

        'Get/Set Property for _locationID
        Public Property LocationID() As String
            Get
                Return _locationID
            End Get
            Set(ByVal value As String)
                _locationID = value
            End Set
        End Property

        'Get/Set Property for _areaID
        Public Property AreaID() As String
            Get
                Return _areaID
            End Get
            Set(ByVal value As String)
                _areaID = value
            End Set
        End Property

        'Get/Set Property for _elementID
        Public Property ElementID() As String
            Get
                Return _elementID
            End Get
            Set(ByVal value As String)
                _elementID = value
            End Set
        End Property

        'Get/Set Property for _priorityID
        Public Property PriorityID() As String
            Get
                Return _priorityID
            End Get
            Set(ByVal value As String)
                _priorityID = value
            End Set
        End Property

        'Get/Set Property for _status
        Public Property Status() As String
            Get
                Return _status
            End Get
            Set(ByVal value As String)
                _status = value
            End Set
        End Property

        'Get/Set Property for _isSearch
        Public Property IsSearch() As Boolean
            Get
                Return _isSearch
            End Get
            Set(ByVal value As Boolean)
                _isSearch = value
            End Set
        End Property

        'Get/Set Property of _description
        Public Property Description() As String
            Get
                Return _description
            End Get
            Set(ByVal value As String)
                _description = value
            End Set
        End Property

#End Region

    End Class
End Namespace