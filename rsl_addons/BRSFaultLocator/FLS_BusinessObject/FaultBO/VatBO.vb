﻿Namespace FLS_BusinessObject

    <Serializable()> _
    Public Class VatBO : Inherits BaseBO

#Region "Attributes"

        Private _vatID As Int32
        Private _vatName As String
        Private _vatRate As Double
        Private _vatCode As Char

#End Region

#Region "Construcotr"

        Public Sub New()
            _vatID = -1
            _vatName = String.Empty
            _vatRate = -1.0
            _vatCode = String.Empty
        End Sub

#End Region

#Region "Properties"

        'Get/Set Property for _vatID
        Public Property VatID() As Int32
            Get
                Return _vatID
            End Get
            Set(ByVal value As Int32)
                _vatID = value
            End Set
        End Property


        'Get/Set Property for _vatName
        Public Property VatName() As String
            Get
                Return _vatName
            End Get
            Set(ByVal value As String)
                _vatName = value
            End Set
        End Property

        'Get/Set Property for _vatRate
        Public Property VatRate() As Double
            Get
                Return _vatRate
            End Get
            Set(ByVal value As Double)
                _vatRate = value
            End Set
        End Property

        'Get/Set Property for _vatCose
        Public Property VatCode() As Char
            Get
                Return _vatCode
            End Get
            Set(ByVal value As Char)
                _vatCode = value
            End Set
        End Property

#End Region

    End Class
End Namespace