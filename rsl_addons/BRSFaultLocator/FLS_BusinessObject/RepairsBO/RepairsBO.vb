﻿Imports FLS_BusinessObject

Namespace FLS_BusinessObject
    Public Class RepairsBO : Inherits BaseBO
#Region "Attribute"
        Private _RepairDescription As String
        Private _NetCost As Double
        Private _VatRateId As Integer
        Private _Vat As Double
        Private _GrossCost As Double
        Private _PostInspection As Boolean
        Private _StockConditionItem As Boolean
        Private _IsActive As Boolean
        Private _FaultRepairListId As Integer
#End Region


#Region "Properties"
        ' Get / Set property for _RepairDescription
        Public Property RepairDescription() As String

            Get
                Return _RepairDescription
            End Get

            Set(ByVal value As String)
                _RepairDescription = value
            End Set
        End Property

        ' Get / Set property for _NetCost
        Public Property NetCost() As Double

            Get
                Return _NetCost
            End Get

            Set(ByVal value As Double)
                _NetCost = value
            End Set
        End Property

        ' Get / Set property for _Vat
        Public Property Vat() As Double

            Get
                Return _Vat
            End Get

            Set(ByVal value As Double)
                _Vat = value
            End Set
        End Property

        ' Get / Set property for _GrossCost
        Public Property GrossCost() As Double
            Get
                Return _GrossCost
            End Get

            Set(ByVal value As Double)
                _GrossCost = value
            End Set
        End Property

        ' Get / Set property for _VatRateId
        Public Property VatRateId() As Integer

            Get
                Return _VatRateId
            End Get

            Set(ByVal value As Integer)
                _VatRateId = value
            End Set
        End Property

        ' Get / Set property for _PostInspection
        Public Property PostInspection() As Boolean

            Get
                Return _PostInspection
            End Get

            Set(ByVal value As Boolean)
                _PostInspection = value
            End Set
        End Property

        ' Get / Set property for _StockConditionItem
        Public Property StockConditionItem() As Boolean

            Get
                Return _StockConditionItem
            End Get

            Set(ByVal value As Boolean)
                _StockConditionItem = value
            End Set
        End Property

        ' Get / Set property for _IsActive
        Public Property IsActive() As Boolean

            Get
                Return _IsActive
            End Get

            Set(ByVal value As Boolean)
                _IsActive = value
            End Set
        End Property

        ' Get / Set property for _FaultRepairListId
        Public Property FaultRepairListId() As Integer

            Get
                Return _FaultRepairListId
            End Get

            Set(ByVal value As Integer)
                _FaultRepairListId = value
            End Set
        End Property


#End Region
    End Class

End Namespace
