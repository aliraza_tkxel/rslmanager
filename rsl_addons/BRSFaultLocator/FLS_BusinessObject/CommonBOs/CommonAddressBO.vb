﻿Imports FLS_BusinessObject

Public Class CommonAddressBO : Inherits BaseBO
   


    Private _street As String

    Public Property Street() As String
        Get
            Return _street
        End Get
        Set(ByVal value As String)
            _street = value
        End Set
    End Property

    Private _address As String

    Public Property Address() As String
        Get
            Return _address
        End Get
        Set(ByVal value As String)
            _address = value
        End Set
    End Property

    Private _area As String
    Public Property Area() As String
        Get
            Return _area
        End Get
        Set(ByVal value As String)
            _area = value
        End Set
    End Property

    Private _city As String
    Public Property City() As String
        Get
            Return _city
        End Get
        Set(ByVal value As String)
            _city = value
        End Set
    End Property
    Private _postCode As String
    Public Property PostCode() As String
        Get
            Return _postCode
        End Get
        Set(ByVal value As String)
            _postCode = value
        End Set
    End Property

    Private _patchId As String
    Public Property PatchId() As String
        Get
            Return _patchId
        End Get
        Set(ByVal value As String)
            _patchId = value
        End Set
    End Property

    Private _patchName As String
    Public Property PatchName() As String
        Get
            Return _patchName
        End Get
        Set(ByVal value As String)
            _patchName = value
        End Set
    End Property


#Region "Construtor"
    Public Sub New()
       
        _street = String.Empty
        _address = String.Empty
        _area = String.Empty
        _city = String.Empty
        _postCode = String.Empty
        _patchId = 0
        _patchName = String.Empty
    End Sub
#End Region
End Class
