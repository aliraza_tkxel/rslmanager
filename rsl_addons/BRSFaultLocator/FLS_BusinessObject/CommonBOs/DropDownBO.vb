﻿Imports System

Public Class DropDownBO

#Region "Attributes"

    Private _ID As Integer
    Private _description As String

#End Region

#Region "Construtor"

    Public Sub New(ByVal id As Integer, ByVal name As String)
        _ID = id
        _description = name
    End Sub

#End Region

#Region "Properties"


    Public Property ID() As Integer
        Get
            Return _ID
        End Get
        Set(ByVal value As Integer)
            _ID = value
        End Set
    End Property

    Public Property Description() As String
        Get
            Return _description
        End Get
        Set(ByVal value As String)
            _description = value
        End Set
    End Property

#End Region

#Region "Functions"

#End Region

End Class
