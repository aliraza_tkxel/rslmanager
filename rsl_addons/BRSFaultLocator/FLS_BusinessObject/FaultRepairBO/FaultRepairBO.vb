﻿Imports FLS_BusinessObject

Namespace FLS_BusinessObject
    Public Class FaultRepairBO : Inherits BaseBO

#Region "Attribute"
        Private _RepairId As Integer
        Private _FaultID As Integer
        Private _FaultDescription As String
        Private _RepairDescription As String
#End Region

#Region "Properties"
        ' Get / Set property for _RepairId
        Public Property RepairId() As Integer

            Get
                Return _RepairId
            End Get

            Set(ByVal value As Integer)
                _RepairId = value
            End Set
        End Property

        ' Get / Set property for _FaultID
        Public Property FaultID() As Integer

            Get
                Return _FaultID
            End Get

            Set(ByVal value As Integer)
                _FaultID = value
            End Set
        End Property

        ' Get / Set property for _FaultDescription
        Public Property FaultDescription() As String

            Get
                Return _FaultDescription
            End Get

            Set(ByVal value As String)
                _FaultDescription = value
            End Set
        End Property

        ' Get / Set property for _RepairDescription
        Public Property RepairDescription() As String

            Get
                Return _RepairDescription
            End Get

            Set(ByVal value As String)
                _RepairDescription = value
            End Set
        End Property

#End Region

    End Class
End Namespace


