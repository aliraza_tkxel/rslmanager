﻿Public Class ConfirmFaultDtBO
    Implements IDtBO

    Public dt As DataTable
    Public dr As DataRow

    Public jsn As String = String.Empty
    Public location As String = String.Empty
    Public duration As String = String.Empty
    Public trade As String = String.Empty
    Public dueDate As String = String.Empty

    Private Const jsnColName As String = "JSN:"
    Private Const locationColName As String = "Location:"
    Private Const durationColName As String = "Duration:"
    Private Const tradeColName As String = "Trade:"
    Private Const DueColName As String = "Due:"

    Sub New()
        Me.createDataTable()
    End Sub

    Public Sub addNewDataRow() Implements IDtBO.addNewDataRow
        dr = dt.NewRow()
        dr(jsnColName) = jsn
        dr(locationColName) = location
        dr(durationColName) = duration
        dr(tradeColName) = trade
        dr(DueColName) = dueDate
        dt.Rows.Add(dr)
    End Sub

    Public Sub createDataTable() Implements IDtBO.createDataTable
        dt = New DataTable()
        dt.Columns.Add(jsnColName)
        dt.Columns.Add(locationColName)
        dt.Columns.Add(durationColName)
        dt.Columns.Add(tradeColName)
        dt.Columns.Add(DueColName)
    End Sub

End Class

