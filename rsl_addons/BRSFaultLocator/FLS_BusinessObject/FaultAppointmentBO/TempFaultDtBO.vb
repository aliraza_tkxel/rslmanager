﻿Public Class TempFaultDtBO
    Implements IDtBO

    Public dt As DataTable
    Public dr As DataRow

    Public location As String = String.Empty
    Public durationString As String = String.Empty
    Public trade As String = String.Empty
    Public dueDateString As String = String.Empty
    Public tempFaultId As Integer = 0
    Public priorityName As String = String.Empty
    Public completeDueDate As Date
    Public tradeId As Integer = 0

    Public Const locationColName As String = "Location:"
    Public Const durationStringColName As String = "Duration:"
    Public Const tradeColName As String = "Trade:"
    Public Const dueDateStringColName As String = "Due:"
    Public Const tempFaultIdColName As String = "TempFaultId:"
    Public Const priorityColName As String = "PriorityName:"
    Public Const completeDueDateColName As String = "CompleteDueDate:"
    Public Const tradeIdColName As String = "TradeID:"

    Sub New()
        Me.createDataTable()
    End Sub

    Public Sub addNewDataRow() Implements IDtBO.addNewDataRow
        dr = dt.NewRow()
        dr(locationColName) = location
        dr(durationStringColName) = durationString
        dr(tradeColName) = trade
        dr(dueDateStringColName) = dueDateString
        dr(tempFaultIdColName) = tempFaultId
        dr(priorityColName) = priorityName
        dr(completeDueDateColName) = completeDueDate
        dr(tradeIdColName) = tradeId
        dt.Rows.Add(dr)
    End Sub

    Public Sub createDataTable() Implements IDtBO.createDataTable
        dt = New DataTable()
        dt.Columns.Add(locationColName)
        dt.Columns.Add(durationStringColName)
        dt.Columns.Add(tradeColName)
        dt.Columns.Add(dueDateStringColName)
        dt.Columns.Add(tempFaultIdColName)
        dt.Columns.Add(priorityColName)
        dt.Columns.Add(completeDueDateColName)
        dt.Columns.Add(tradeIdColName)
    End Sub
End Class


