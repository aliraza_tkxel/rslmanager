﻿Public Class ConfirmAppointmentDtBO
    Implements IDtBO

    Public operative As String = String.Empty
    Public appointmentStartDateTime As String = String.Empty
    Public appointmentEndDateTime As String = String.Empty
    Public lastColumn As String = String.Empty

    Public dt As DataTable
    Private dr As DataRow
    Private Const operativeColName As String = "Operative:"
    Private Const startDatetimeColName As String = "Start Date:"
    Private Const endDateTimeColName As String = "End Date:"
    Private Const lastColName As String = " "

    Sub New()
        Me.createDataTable()
    End Sub

    Public Sub createDataTable() Implements IDtBO.createDataTable
        dt = New DataTable()
        dt.Columns.Add(operativeColName)
        dt.Columns.Add(startDatetimeColName)
        dt.Columns.Add(endDateTimeColName)
        dt.Columns.Add(lastColName)
    End Sub

    Public Sub addNewDataRow() Implements IDtBO.addNewDataRow
        dr = dt.NewRow()
        dr(operativeColName) = operative
        dr(startDatetimeColName) = appointmentStartDateTime
        dr(endDateTimeColName) = appointmentEndDateTime
        dr(lastColName) = lastColumn
        dt.Rows.Add(dr)
    End Sub
End Class
