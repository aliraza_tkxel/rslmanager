﻿Public Class TempAppointmentDtBO
    Implements IDtBO

    Public Property operative As String = String.Empty
    Public Property patch As String = String.Empty
    Public Property startDateTime As String = String.Empty
    Public Property endDateTime As String = String.Empty
    Public Property distanceFrom As String = 0
    Public Property distanceTo As String = 0
    Public Property lastColumn As String = String.Empty
    Public Property operativeId As Integer = 0
    Public Property appointmentStartDate As Date
    Public Property appointmentEndDate As Date
    Public Property tradeId As Integer = 0
    Public Property lastAdded As Boolean = True

    Public dt As DataTable
    Private dr As DataRow

    Public Const operativeColName As String = "Operative:"
    Public Const patchName As String = "Patch:"
    Public Const startDateColName As String = "Start Date:"
    Public Const endDateColName As String = "End Date:"
    Public Const distanceFromColName As String = "From:"
    Public Const distanceToColName As String = "To:"
    Public Const lastColName As String = " "
    Public Const operativeIdColName As String = "Id"
    Public Const appointmentStartDateColName As String = "appointmentStartDate:"
    Public Const appointmentEndDateColName As String = "appointmentEndDateColName:"
    Public Const tradeIdColName As String = "TradeID:"
    Public Const lastAddedColName As String = "LastAdded:"

    Sub New()
        Me.createDataTable()
    End Sub

    Public Sub createDataTable() Implements IDtBO.createDataTable
        dt = New DataTable()
        dt.Columns.Add(operativeColName)
        dt.Columns.Add(patchName)
        dt.Columns.Add(startDateColName)
        dt.Columns.Add(endDateColName)
        dt.Columns.Add(distanceFromColName)
        dt.Columns.Add(distanceToColName)
        dt.Columns.Add(lastColName)
        dt.Columns.Add(operativeIdColName)
        dt.Columns.Add(appointmentStartDateColName)
        dt.Columns.Add(appointmentEndDateColName)
        dt.Columns.Add(tradeIdColName)
        dt.Columns.Add(lastAddedColName)
    End Sub

    Public Sub addNewDataRow() Implements IDtBO.addNewDataRow
        dr = dt.NewRow()
        dr(operativeColName) = operative
        dr(patchName) = patch
        dr(startDateColName) = startDateTime
        dr(endDateColName) = endDateTime
        dr(distanceFromColName) = distanceFrom
        dr(distanceToColName) = distanceTo
        dr(lastColName) = lastColumn
        dr(operativeIdColName) = operativeId
        dr(appointmentStartDateColName) = appointmentStartDate
        dr(appointmentEndDateColName) = appointmentEndDate
        dr(tradeIdColName) = tradeId
        dr(lastAddedColName) = lastAdded
        dt.Rows.Add(dr)
    End Sub
End Class
