﻿Imports System.Globalization

Public Class TempFaultAppointmentBO

    Public Property DueDate As Date

    Public Property FaultDuration As Double

    Public Property IsFaultGasSafe As Boolean

    Public Property IsFaultOftec As Boolean

    Public Property TradeId As Integer

    Public Property IsPatchSelected As Boolean

    Public Property IsDateSelected As Boolean

    Public Property DisplayCount As Integer

    Public Property CompleteDueDate As Date
    Public Property FromDate As Date
    Public Property IsFromDateSelected As Boolean
    Public tempFaultDtBo As TempFaultDtBO = New TempFaultDtBO
    Public tempAppointmentDtBo As TempAppointmentDtBO = New TempAppointmentDtBO

#Region "add Row In Appointment List"
    Public Sub addRowInAppointmentList(ByVal operativeId As Integer, ByVal operativeName As String, ByVal appointmentStartDateTime As Date, ByVal appointmentEndDateTime As Date, ByVal patch As String, ByVal distanceFrom As Double, ByVal distanceTo As Double)

        tempAppointmentDtBo.operativeId = operativeId
        tempAppointmentDtBo.operative = operativeName
        tempAppointmentDtBo.startDateTime = convertDateToCustomString(appointmentStartDateTime)
        tempAppointmentDtBo.patch = patch
        tempAppointmentDtBo.endDateTime = convertDateToCustomString(appointmentEndDateTime)
        tempAppointmentDtBo.appointmentStartDate = appointmentStartDateTime
        tempAppointmentDtBo.appointmentEndDate = appointmentEndDateTime
        tempAppointmentDtBo.distanceFrom = distanceFrom
        tempAppointmentDtBo.distanceTo = distanceTo
        tempAppointmentDtBo.lastColumn = "--"
        tempAppointmentDtBo.lastAdded = True
        tempAppointmentDtBo.addNewDataRow()

    End Sub
#End Region

#Region "convert Date Time In to Date Time String"
    ''' <summary>
    ''' This function 'll return the date in the following format in string (HH:mm dddd d MMMM yyyy)
    ''' </summary>
    ''' <param name="aDate"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function convertDateToCustomString(ByVal aDate As Date)
        Return (aDate.ToString("HH:mm dddd d MMMM yyyy", CultureInfo.CreateSpecificCulture("en-US")))
    End Function
#End Region

End Class
