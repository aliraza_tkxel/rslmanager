USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--EXEC FL_GetJobSheetByAppointment
--@appointmentId = 345
-- Author:		<Ahmed Mehmood>
-- Create date: <7/2/2013>
-- Description:	<Returns Job Sheet Numbers of specific appointment>
-- Webpage:AppointmentSummary.aspx
-- =============================================
CREATE PROCEDURE [dbo].[FL_GetJobSheetByAppointment] 
	-- Add the parameters for the stored procedure here
	(
	@appointmentId int
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

   SELECT DISTINCT FL_FAULT_LOG.JobSheetNumber 
   FROM FL_FAULT_APPOINTMENT 
		INNER JOIN FL_FAULT_LOG ON FL_FAULT_APPOINTMENT.FaultLogId = FL_FAULT_LOG.FaultLogID 
		
   WHERE  FL_FAULT_APPOINTMENT.AppointmentId=@appointmentId

				
END
