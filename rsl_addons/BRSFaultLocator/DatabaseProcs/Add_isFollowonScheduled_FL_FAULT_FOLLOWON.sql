IF NOT EXISTS (SELECT * 
FROM information_schema.COLUMNS 
WHERE
TABLE_NAME = 'FL_FAULT_FOLLOWON' 
AND COLUMN_NAME = 'isFollowonScheduled')
BEGIN
	ALTER TABLE FL_FAULT_FOLLOWON
	ADD isFollowonScheduled BIT NOT NULL DEFAULT(0)
	PRINT 'Column isFollowonScheduled Added Sucessfully'
END
ELSE
BEGIN
	PRINT 'Column "isFollowonScheduled" Alreaady Added, no Changes required.'
END