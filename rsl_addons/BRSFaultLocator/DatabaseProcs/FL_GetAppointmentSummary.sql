USE [RSLBHALive]
GO

/****** Object:  StoredProcedure [dbo].[FL_GetAppointmentSummary]    Script Date: 02/27/2013 10:59:30 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =================================================
--EXEC	[dbo].[FL_GetAppointmentSummary]
--		@faultsList = N'112,179'
-- Author:		<Muhammad Abdul Wahhab>
-- Create date: <4/2/2013>
-- Description:	<Get faults list>
-- Web Page: CancellationAppointmentSummary.aspx
-- =================================================
ALTER PROCEDURE [dbo].[FL_GetAppointmentSummary] 
	-- Add the parameters for the stored procedure here
	@faultsList nvarchar(500)
AS
BEGIN
DECLARE 
        @SelectClause varchar(1000),
        @fromClause   varchar(1000),
        @whereClause varchar(1000),
        @orderClause varchar(500)
                
	--Getting Faults List
	
	SET  @SelectClause=' SELECT FL_FAULT_LOG.FaultLogID as FaultLogID,
		FL_Fault.FaultId as FaultId, FL_FAULT_LOG.JobSheetNumber as JobSheetNumber,
		FL_AREA.AreaName as AreaName, FL_Fault.Description as Description,
		FL_Fault.Duration as Duration, G_TRADE.Description as Trade,
		FL_FAULT_LOG.ProblemDays as ProblemDays, 
		FL_FAULT_LOG.RecuringProblem as RecuringProblem, 
		FL_FAULT_LOG.CommunalProblem as CommunalProblem,
		DATENAME(D, FL_FAULT_LOG.DueDate)+'' ''+CONVERT(nvarchar(3), FL_FAULT_LOG.DueDate, 100) as Date '
	
	SET @fromClause=' From FL_FAULT_LOG 
		INNER JOIN FL_Fault ON FL_FAULT_LOG.FaultId = FL_Fault.FaultId
		INNER JOIN FL_Area ON FL_FAULT_LOG.AreaId = FL_Area.AreaId
		INNER JOIN FL_Fault_Trade ON FL_Fault.FaultId = FL_Fault_Trade.FaultId
		INNER JOIN G_TRADE ON FL_FAULT_TRADE.TradeId = G_TRADE.TradeId '
	
	SET @whereClause=' WHERE FL_FAULT_LOG.FaultLogID IN (' + @faultsList + ')'
	
	SET @orderClause=' ORDER BY FL_Fault_Trade.TradeId ASC '		
	
	EXEC (@selectClause + @fromClause + @whereClause + @orderClause )    
END



GO


