--Author: Behroz Sikander
--Date: 18/03/2013
--Description: To keep the record of original id of recall fault's.
ALTER TABLE FL_FAULT_RECALL ADD OriginalFaultLogId INT NULL
ALTER TABLE FL_FAULT_RECALL DROP COLUMN NoOfRecalls

