USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[FL_SaveAppointmentDetails]    Script Date: 01/05/2016 10:08:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================
-- EXEC [dbo].[FL_SaveAppointmentDetails]
--		@faultsList = N'283,5109',
--		@operativeId = 480,
--		@appointmentDate = N'2/4/2013',
--		@duration = 3.0,
--		@faultNotes = N'The tenant thinks that she has overtightened the hot tap',
--		@appointmentNotes = N'The tenant thinks that she has overtightened the hot tap',
--		@time = N'9:30 AM',
--		@endTime = N'10:30 AM',
--      @CustomerID = 11137,
--      @PropertyId = N'A720040007'
--		@isRecall = 0,
--		@appointmentID = @appointmentID OUTPUT,
--		@tempCount = @tempCount OUTPUT
-- Author:		<Muhammad Abdul Wahhab>
-- Create date: <1/2/2013>
-- Description:	<Saves all appointment information>
-- Web Page: AppointmentSummary.aspx
-- =====================================================
ALTER PROCEDURE [dbo].[FL_SaveAppointmentDetails]
	(
		@faultsList				NVARCHAR(500)
		,@operativeId			INT
		,@appointmentDate		DATETIME
		,@duration				FLOAT
		,@faultNotes			VARCHAR(500)
		,@appointmentNotes		VARCHAR(500)
		,@time					VARCHAR(50)
		,@endTime				VARCHAR(50)
		,@CustomerID			INT
		,@PropertyId			NVARCHAR(20)
		,@isRecall				BIT
		,@isCalendarAppointment	BIT
		,@userID				INT
		,@appointmentID			INT	OUTPUT
		,@tempCount				INT	OUTPUT
		,@appointmentEndDate	DATETIME = NULL
	)
AS
BEGIN

BEGIN TRANSACTION 
BEGIN TRY
	DECLARE @SelectStmt AS NVARCHAR(3000)
	DECLARE @Sql AS NVARCHAR(3200)

	DECLARE @FaultId AS INT
	DECLARE @Quantity AS INT
	DECLARE @ProblemDays AS INT
	DECLARE @RecuringProblem AS BIT
	DECLARE @CommunalProblem AS BIT
	DECLARE @Notes AS VARCHAR(4000)
	DECLARE @ORGID AS INT
	DECLARE @ItemActionId AS INT
	DECLARE @Recharge AS BIT
	DECLARE @StatusID AS INT
	DECLARE @IsReported AS BIT
	DECLARE @DueDate AS DATETIME
	DECLARE @SubmitDate AS DATETIME
	DECLARE @FaultLogID AS INT
	DECLARE @JournalID AS INT
	DECLARE @TempFaultID AS INT
	DECLARE @AreaId AS INT
	
	DECLARE @JSNumberPrefix AS VARCHAR(10)
	DECLARE @ResponseTime AS INT
	DECLARE @Days AS INT
	DECLARE @ItemID AS INT
	DECLARE @ItemNatureID AS INT
	DECLARE @TenancyID AS INT
	DECLARE @FaultTradeId AS INT
	DECLARE @OriginalFaultLogId AS INT
	DECLARE @isFollowon AS BIT
	DECLARE @FollowOnFaultLogID AS INT
	DECLARE @TempFaultDuration AS DECIMAL(9, 2)

SET @JSNumberPrefix = 'JS'
SET @SubmitDate = GETDATE()
SET @IsReported = 1

SELECT
	@StatusID = FaultStatusID
FROM
	FL_FAULT_STATUS
WHERE
	Description = 'Appointment Arranged'

--Get &  Set Tenancy Id 
SELECT
	@TenancyId = TENANCYID
FROM
	C_CUSTOMERTENANCY
WHERE
	CUSTOMERID = @CustomerID
	AND ENDDATE != NULL

--Get and set the Item id 
SET @ItemId =
(
	SELECT
		ItemID
	FROM
		C_ITEM
	WHERE
		DESCRIPTION = 'Property'
)

--Get and set the Item nature id 
SET @ItemNatureId =
(
	SELECT
		ItemNatureID
	FROM
		C_NATURE
	WHERE
		DESCRIPTION = 'Reactive Repair'
)
INSERT INTO FL_CO_APPOINTMENT (
				AppointmentDate
				,OperativeID
				,Time
				,EndTime
				,LastActionDate
				,AppointmentStatus
				,Notes
				,isCalendarAppointment
				,AppointmentEndDate
			)
	VALUES (
			@appointmentDate
			,@operativeId
			,@time
			,@endTime
			,@SubmitDate
			,'Appointment Arranged'
			,@appointmentNotes
			,@isCalendarAppointment
			,COALESCE(@appointmentEndDate, @appointmentDate)
		)

SET @appointmentID = SCOPE_IDENTITY()

INSERT INTO FL_CO_APPOINTMENT_HISTORY (
				[AppointmentId]
				,[JobSheetNumber]
				,[AppointmentDate]
				,[OperativeID]
				,[LastActionDate]
				,[Notes]
				,[Time]
				,[EndTime]
				,[isCalendarAppointment]
				,[AppointmentEndDate]
			)
	SELECT
		[AppointmentID]
		,[JobSheetNumber]
		,[AppointmentDate]
		,[OperativeID]
		,[LastActionDate]
		,[Notes]
		,[Time]
		,[EndTime]
		,[isCalendarAppointment]
		,[AppointmentEndDate]
	FROM
		FL_CO_APPOINTMENT
	WHERE
		AppointmentId = @appointmentID

-- this is a cursor variable and is needed so we can use dynamic-sql which we need to deal with the list of TempFaultIDs
DECLARE @FaultCursor CURSOR

SET @SelectStmt = 'SELECT FL_TEMP_FAULT.TempFaultID as TempFaultID,
		FL_TEMP_FAULT.FaultId as FaultId, FL_TEMP_FAULT.Quantity as Quantity,
		FL_TEMP_FAULT.ProblemDays as ProblemDays, FL_TEMP_FAULT.RecuringProblem as RecuringProblem,
		FL_TEMP_FAULT.CommunalProblem as CommunalProblem, FL_TEMP_FAULT.Notes as Notes,
		FL_TEMP_FAULT.ORGID as ORGID, FL_TEMP_FAULT.ItemActionId as ItemActionId,
		FL_TEMP_FAULT.Recharge as Recharge, FL_TEMP_FAULT.FaultTradeId as FaultTradeId,
		FL_TEMP_FAULT.OriginalFaultLogId, 
		FL_TEMP_FAULT.isFollowon, FL_TEMP_FAULT.FollowOnFaultLogID
		,FL_TEMP_FAULT.Duration
		,FL_TEMP_FAULT.areaId
		FROM FL_TEMP_FAULT 
		INNER JOIN FL_Fault ON FL_TEMP_FAULT.FaultId = FL_Fault.FaultId		
		INNER JOIN FL_FAULT_PRIORITY ON FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID
		WHERE FL_TEMP_FAULT.TempFaultID IN (' + @faultsList + ') And ISNULL(FL_TEMP_FAULT.ISRECALL,0) =' + CONVERT(VARCHAR(2), @isRecall)

SET @Sql = 'Set @FaultCursor = CURSOR FAST_FORWARD FOR ' + @SelectStmt + '; OPEN @FaultCursor'

EXEC sp_executesql	@Sql
					,N'@FaultCursor CURSOR OUTPUT'
					,@FaultCursor OUTPUT

FETCH NEXT FROM @FaultCursor INTO @TempFaultID, @FaultId, @Quantity, @ProblemDays,
@RecuringProblem, @CommunalProblem, @Notes, @ORGID, @ItemActionId, @Recharge, @FaultTradeId, @OriginalFaultLogId
, @isFollowon, @FollowOnFaultLogID, @TempFaultDuration, @AreaId

WHILE @@FETCH_STATUS = 0 BEGIN

SELECT
	@ResponseTime = ResponseTime
	,@Days = Days
FROM
	FL_FAULT_PRIORITY
		INNER JOIN FL_FAULT ON FL_FAULT_PRIORITY.PriorityID = FL_FAULT.PriorityID
WHERE
	FaultID = @FaultId

IF @Days = 0 SET @DueDate = DATEADD(HH, @ResponseTime, @SubmitDate) ELSE SET @DueDate = DATEADD(DD, @ResponseTime, @SubmitDate)

--Insert the record in fault log table
INSERT INTO FL_FAULT_LOG (
				FaultID
				,CustomerID
				,Quantity
				,ProblemDays
				,RecuringProblem
				,CommunalProblem
				,Notes
				,ORGID
				,Recharge
				,PROPERTYID
				,StatusID
				,IsReported
				,SubmitDate
				,DueDate
				,FaultTradeId
				,UserId
				,FollowOnFaultLogId
				,Duration
				,AreaId
			)
	VALUES (
			@FaultID
			,@CustomerID
			,@Quantity
			,@ProblemDays
			,@RecuringProblem
			,@CommunalProblem
			,@Notes
			,@ORGID
			,@Recharge
			,@PropertyId
			,@StatusID
			,@IsReported
			,@SubmitDate
			,@DueDate
			,@FaultTradeId
			,@userID
			,@FollowOnFaultLogID
			,@TempFaultDuration
			,@AreaId
		)

SET @FaultLogID = SCOPE_IDENTITY()

-- update the job sheet number
UPDATE
	FL_FAULT_LOG
SET
	JobSheetNumber = (@JSNumberPrefix + CONVERT(VARCHAR, @FaultLogID))
WHERE
	FaultLogId = @FaultLogID

--Insert the record in Fault Appointment Table
INSERT INTO FL_FAULT_APPOINTMENT (
				FaultLogId
				,AppointmentId
			)
	VALUES (
			@FaultLogID
			,@appointmentID
		)

--Insert the record in Journal Table
INSERT INTO FL_FAULT_JOURNAL (
				FaultLogID
				,CustomerID
				,PropertyID
				,TenancyID
				,ItemID
				,ItemNatureID
				,FaultStatusID
				,CreationDate
				,LastActionDate
			)
	VALUES (
			@FaultLogID
			,@CustomerID
			,@PropertyId
			,@TenancyID
			,@ItemID
			,@ItemNatureID
			,@StatusID
			,@SubmitDate
			,@SubmitDate
		)

SET @JournalID = SCOPE_IDENTITY()

--Insertion Fault Log History With Repair Reported Status
INSERT INTO FL_FAULT_LOG_HISTORY (
				JournalID
				,FaultLogID
				,FaultStatusID
				,Notes
				,ORGID
				,ItemActionID
				,PROPERTYID
				,LastActionDate
				,LastActionUserID
				,Duration
			)
	VALUES (
			@JournalID
			,@FaultLogID
			,@StatusID
			,@Notes
			,@ORGID
			,@ItemActionId
			,@PropertyId
			,@SubmitDate
			,@userID
			,@TempFaultDuration
		)

UPDATE
	FL_TEMP_FAULT
SET
	isAppointmentConfirmed = 1
	,FaultLogId = @FaultLogID
WHERE
	TempFaultID = @TempFaultID

--TODO:Change the Fault Recall count value
IF @isRecall = 1 BEGIN
INSERT INTO FL_FAULT_RECALL (
				FaultLogId
				,OriginalFaultLogId
			)
	VALUES (
			@FaultLogID
			,@OriginalFaultLogId
		)
END

--Update the record in FL_FAULT_FOLLOWON Table to set/show that the follow on is scheduled
IF (EXISTS
(
	SELECT
		FollowOnID
	FROM
		FL_FAULT_FOLLOWON
	WHERE
		FaultLogId = @FollowOnFaultLogID
))
AND (@isFollowon = 1
AND @FollowOnFaultLogID IS NOT NULL) BEGIN
UPDATE
	FL_FAULT_FOLLOWON
SET
	isFollowonScheduled = 1
WHERE
	FaultLogId = @FollowOnFaultLogID
END

FETCH NEXT FROM @FaultCursor INTO @TempFaultID, @FaultId, @Quantity, @ProblemDays,
@RecuringProblem, @CommunalProblem, @Notes, @ORGID, @ItemActionId, @Recharge, @FaultTradeId, @OriginalFaultLogId
, @isFollowon, @FollowOnFaultLogID, @TempFaultDuration , @AreaId
END

CLOSE @FaultCursor
DEALLOCATE @FaultCursor

SELECT
	@tempCount = COUNT(FL_TEMP_FAULT.TempFaultID)
FROM
	FL_TEMP_FAULT
WHERE
	FL_TEMP_FAULT.CustomerID = @CustomerID
	AND FL_TEMP_FAULT.PROPERTYID = @PropertyId
	AND FL_TEMP_FAULT.isAppointmentConfirmed IS NULL


END TRY BEGIN CATCH
IF @@TRANCOUNT > 0 BEGIN

ROLLBACK TRANSACTION;

END

DECLARE @ErrorMessage nvarchar(4000);
DECLARE @ErrorSeverity int;
DECLARE @ErrorState int;

SELECT
	@ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

-- Use RAISERROR inside the CATCH block to return 
-- error information about the original error that 
-- caused execution to jump to the CATCH block.
RAISERROR (@ErrorMessage, -- Message text.
@ErrorSeverity, -- Severity.
@ErrorState -- State.
);
END CATCH
  
	 
	IF @@TRANCOUNT >0
	BEGIN
		COMMIT TRANSACTION;
	END
	
END