USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
/* =================================================================================    
    Page Description: Checks whether selected faultTradeId is in use 
    Author: Ahmed Mehmood
    Creation Date: Dec-10-2015

    Change History:

    Version      Date             By                      Description
    =======     ============    ========           ===========================
    v1.0         Dec-10-2015   Ahmed Mehmood       Checks whether selected faultTradeId is in use 

  =================================================================================*/
--DECLARE	@return_value int,
--		@result bit

--EXEC	@return_value = [dbo].[FL_CheckFaultTradeInUse]
--		@faultTradeId = 1,
--		@result = @result OUTPUT
-- =============================================
CREATE PROCEDURE [dbo].[FL_CheckFaultTradeInUse]
-- Add the parameters for the stored procedure here
		@faultTradeId int = 0,
		@result bit = 0 output	
AS
BEGIN
	
		IF	EXISTS (SELECT TempFaultID FROM FL_TEMP_FAULT WHERE FaultTradeId = @faultTradeId)
				OR	EXISTS (SELECT FaultLogID FROM FL_FAULT_LOG WHERE FaultTradeId = @faultTradeId)
				BEGIN
					SET @result = 1 -- 1 means selected faultTradeId is not in use 				
				END
			ELSE
				BEGIN
					SET @result = 0	-- 0 means selected faultTradeId is in use
				END	    
		
END
