USE [RSLBHALive ]
GO
/****** Object:  StoredProcedure [dbo].[FL_AmendRepair]    Script Date: 01/12/2016 17:01:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Raja Aneeq
-- Create date: 30/12/2015
-- Description:	Edit a repair
-- Web Page:	BRSFaultLocator/Views/Reports/ReportsArea.aspx
-- =============================================
IF object_id('FL_AmendRepair') IS NULL
    EXEC ('create procedure dbo.FL_AmendRepair as select 1')
 GO
ALTER PROCEDURE [dbo].[FL_AmendRepair] 
	-- Add the parameters for the stored procedure here
	@FAULTREPAIRLISTID INT,
	@DESCRIPTION VARCHAR(500),
	@NETCOST FLOAT ,
	@VAT FLOAT,
	@GROSS FLOAT,
	@VATRATEID INT ,
	@POSTINSPECTION BIT,
	@STOCKCONDITIONITEM BIT,
	@REPAIRACTIVE BIT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	UPDATE FL_FAULT_REPAIR_LIST SET 
	DESCRIPTION =@DESCRIPTION, NETCOST=@NETCOST, VAT=@VAT,GROSS=@GROSS, VATRATEID=@VATRATEID, 
	POSTINSPECTION=@POSTINSPECTION,
	STOCKCONDITIONITEM=@STOCKCONDITIONITEM,RepairActive=@REPAIRACTIVE
	WHERE FaultRepairListID= @FAULTREPAIRLISTID

END
