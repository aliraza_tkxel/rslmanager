USE [RSLBHALive]
GO

/* ===========================================================================
--  Author:			Aamir Waheed
--  DATE CREATED:	12 March 2013
--  Webpage:		/Views/Resources/Resources.aspx (For Add Fault Modal Popup)(Appliance Servicing)
--  Description:	To Change Value 'Gas' to 'Appliance Servicing', 'Gas' Needed to be at InspectionTypeID = 1
					To Add a new Value 'Planed'
 '==============================================================================*/

UPDATE P_INSPECTIONTYPE
	SET Description = 'Appliance Servicing'
		WHERE InspectionTypeID = 1
GO

INSERT [dbo].[P_INSPECTIONTYPE] ([Description]) VALUES ('Planned')