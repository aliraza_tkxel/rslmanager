  
-- =============================================  
 --EXEC FL_GetFaultBasketInfo  
 -- @customerId = 5,  
 --@propertyId ='A010000018',@isRecall=0
-- Author:  <Ahmed Mehmood>  
-- Create date: <30/01/2013>  
-- Description: <Get Fault Basket Info FL_TEMP_FAULT table>  
-- Web Page: FaultBasket.aspx  
-- =============================================  
ALTER PROCEDURE [dbo].[FL_GetFaultBasketInfo]  
 (  
  @customerId  INT  
  ,@propertyId VARCHAR(100)  
  ,@isRecall  BIT  
 )  
AS  
SELECT  
 FL_FAULT.[Description]         AS [Description]  
 ,G_TRADE.[Description]         AS Trade  
 ,FL_AREA.AreaName          AS AreaName
 ,FL_TEMP_FAULT.Notes AS Notes  
 ,CASE  
  WHEN FL_FAULT_PRIORITY.Days = 1  
   THEN CONVERT(VARCHAR(5), FL_FAULT_PRIORITY.ResponseTime) + ' days'  
  ELSE CONVERT(VARCHAR(5), FL_FAULT_PRIORITY.ResponseTime) + ' hours'  
 END              AS ResponseTime  
 ,FL_TEMP_FAULT.TempFaultID        AS TempFaultID  
 ,COALESCE(FL_TEMP_FAULT.Duration, FL_FAULT.duration) AS duration  
 ,FL_FAULT.isContractor         AS isContractor  
 ,FL_TEMP_FAULT.FaultLogId        AS faultLogId  
 ,FL_TEMP_FAULT.IsAppointmentConfirmed     AS isAppointmentConfirmed  
 ,G_TRADE.TradeId          AS TradeId  
FROM  
 FL_TEMP_FAULT  
  INNER JOIN FL_FAULT_TRADE ON FL_TEMP_FAULT.FaultTradeId = FL_FAULT_TRADE.FaultTradeId  
  INNER JOIN FL_FAULT ON FL_FAULT_TRADE.FaultId = FL_FAULT.FaultID  
  INNER JOIN G_TRADE ON FL_FAULT_TRADE.TradeId = G_TRADE.TradeId  
  INNER JOIN FL_AREA ON FL_TEMP_FAULT.AREAID = FL_AREA.AreaID  
  INNER JOIN FL_FAULT_PRIORITY ON FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID  
WHERE  
 FL_TEMP_FAULT.CustomerID = @customerId  
 AND FL_TEMP_FAULT.PROPERTYID = @propertyId  
AND (FL_TEMP_FAULT.ISRECALL = @isRecall OR FL_TEMP_FAULT.ISRECALL IS NULL)