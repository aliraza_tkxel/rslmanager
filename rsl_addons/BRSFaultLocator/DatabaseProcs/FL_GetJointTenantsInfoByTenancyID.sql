USE [RSLBHALive1]
GO
/****** Object:  StoredProcedure [dbo].[FL_GetJointTenantsInfoByTenancyID]    Script Date: 07/09/2013 12:00:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/* =============================================
-- EXEC FL_GetJointTenantsInfoByTenancyID
			@tenancyid = 10219
		
-- Author:		<Muhammad Abdul Wahhab>
-- Create date: <08 July 2013>
-- Description:	<This stored procedure returns the tenants information of all tenants in a tenancy.>
-- Parameters:	
		--@tenancyid INT
-- ============================================= */
ALTER PROCEDURE [dbo].[FL_GetJointTenantsInfoByTenancyID] 
	-- Parameter(s) to get all customer(s) detail(s) for a tenancy.
	@tenancyID INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT C__CUSTOMER.CUSTOMERID,
	 ISNULL(G_Title.Description, '') + ISNULL(' ' +C__CUSTOMER.FirstName, '') + ISNULL(' ' + C__CUSTOMER.LASTNAME, '') as CustomerName,
	 ISNULL(NULLIF(LTRIM(C_ADDRESS.EMAIL), ''), 'N/A') AS EMAIL,
	 ISNULL(NULLIF(LTRIM(C_ADDRESS.TEL), '') , 'N/A') AS TEL,
	 ISNULL(NULLIF(LTRIM(C_ADDRESS.MOBILE), ''), 'N/A')AS MOBILE,
	 C_TENANCY.TENANCYID
						
FROM C_TENANCY
INNER JOIN C_CUSTOMERTENANCY ON C_CUSTOMERTENANCY.TENANCYID = C_TENANCY.TENANCYID AND (C_CUSTOMERTENANCY.ENDDATE IS NULL OR C_CUSTOMERTENANCY.ENDDATE>GETDATE())
INNER JOIN C__CUSTOMER ON C__CUSTOMER.CUSTOMERID = C_CUSTOMERTENANCY.CUSTOMERID 
LEFT JOIN G_Title ON C__CUSTOMER.Title = G_Title.TitleId 
INNER JOIN C_ADDRESS ON C_ADDRESS.CUSTOMERID = C__CUSTOMER.CUSTOMERID 
WHERE
 1=1 
 AND dbo.C_TENANCY.TENANCYID=@tenancyid
AND C_ADDRESS.ISDEFAULT = 1 
END
