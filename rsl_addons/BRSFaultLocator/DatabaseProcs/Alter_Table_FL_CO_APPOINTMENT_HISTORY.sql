-- Author:		<Muhammad Abdul Wahhab>
-- Create date: <27/02/2013>
-- Description:	<This script will create the new field in FL_CO_APPOINTMENT_HISTORY>

ALTER TABLE [dbo].[FL_CO_APPOINTMENT_HISTORY]	add [EndTime] [varchar](50) NULL
