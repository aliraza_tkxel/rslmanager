/* ===========================================================================
--  Author:			Ahmed Mehmood
--  DATE CREATED:	28 December 2015
--  Description:	Added areaId in FL_TEMP_FAULT, FL_FAULT_LOG
 '==============================================================================*/


--============================
-- ADD AREAID IN FL_TEMP_FAULT
--============================    
ALTER TABLE FL_TEMP_FAULT
ADD areaId INTEGER NULL
CONSTRAINT FK_FL_TEMP_FAULT_FL_AREA FOREIGN KEY REFERENCES FL_AREA(AREAID)

--============================
-- ADD AREAID IN FL_FAULT_LOG
--============================    
ALTER TABLE FL_FAULT_LOG
ADD areaId INTEGER NULL
CONSTRAINT FK_FL_FAULT_LOG_FL_AREA FOREIGN KEY REFERENCES FL_AREA(AREAID)
