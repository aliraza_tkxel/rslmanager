USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC FL_GetPatchID_TradeID_ByAppointmentID @AppointmentID = 64248
-- Author:		<Aamir Waheed>
-- Create date: <22/08/2013>
-- Description:	<Get PatchID and TradeID to show appropriate calendar for the current appointment.>
-- Webpage : SchedulingCalendar.aspx
-- =============================================
ALTER PROCEDURE [dbo].[FL_GetPatchID_TradeID_ByAppointmentID] 
@AppointmentID int

AS
BEGIN
SELECT DISTINCT
		ISNULL(P__PROPERTY.PATCH, 0) AS PatchID,
		ISNULL(FL_FAULT_TRADE.TradeId, 0) AS TradeID
		
FROM FL_CO_APPOINTMENT
INNER JOIN FL_FAULT_APPOINTMENT ON FL_CO_APPOINTMENT.AppointmentID = FL_FAULT_APPOINTMENT.AppointmentId
INNER JOIN FL_FAULT_LOG ON FL_FAULT_APPOINTMENT.FaultLogId = FL_FAULT_LOG.FaultLogID

--This join is for patch id
INNER JOIN P__PROPERTY ON FL_FAULT_LOG.PROPERTYID = P__PROPERTY.PROPERTYID

--This join is for trade id
INNER JOIN FL_FAULT_TRADE ON FL_FAULT_LOG.FaultTradeId = FL_FAULT_TRADE.FaultTradeId

WHERE FL_CO_APPOINTMENT.AppointmentID = @AppointmentID
END
