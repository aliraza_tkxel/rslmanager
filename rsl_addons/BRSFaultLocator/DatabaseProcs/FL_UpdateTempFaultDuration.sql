IF OBJECT_ID('dbo.FL_UpdateTempFaultDuration') IS NOT NULL
	SET NOEXEC ON
GO
CREATE PROCEDURE dbo.FL_UpdateTempFaultDuration
AS RETURN;
GO
SET NOEXEC OFF
GO
ALTER PROCEDURE dbo.FL_UpdateTempFaultDuration
(
	@tempFaultId INT
	,@Duration DECIMAL(9,2)
)
/* =================================================================================    
    Page Description: Available Appointments
 
    Author: Aamir Waheed
    Creation Date: Dec-19-2014

    Change History:

    Version      Date           By                  Description
    =======     ============    ========			===========================
    v1.0         Dec-19-2014    Aamir Waheed		Add Detail Page TProdCatlate Fire Fox 
  =================================================================================*/

-- WITH ENCRYPTION, RECOMPILE, EXECUTE AS CALLER|SELF|OWNER| 'user_name'
AS BEGIN
	SET NOCOUNT ON;
	
	BEGIN TRY
    	BEGIN TRANSACTION
    		UPDATE FL_TEMP_FAULT
    			SET Duration = ISNULL(@Duration,Duration)
    		WHERE TempFaultID = @tempFaultId
    	COMMIT TRANSACTION
    END TRY
    BEGIN CATCH
    	IF	@@TRANCOUNT > 0
		BEGIN
			ROLLBACK TRANSACTION;
		END
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT
			@ErrorMessage = ERROR_MESSAGE()
			,@ErrorSeverity = ERROR_SEVERITY()
			,@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
		@ErrorSeverity, -- Severity.
		@ErrorState -- State.
		);
    END CATCH;

	RETURN 0;
END
GO