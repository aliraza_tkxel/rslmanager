USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[FL_GetFaultsAndAppointments]    Script Date: 01/20/2015 01:22:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


-- =============================================
--EXEC FL_GetSbFaultsAndAppointments 
--  @schemeId = 1,
--	@blockId = 16
--  Author:		Noor Muhammad
-- Create date: 20/1/2015
-- Description:	<This store procedure shall provide the inform of other appointments that are already been arranged>
-- Web page: OtherSbAppointments.aspx

-- =============================================
ALTER PROCEDURE [dbo].[FL_GetSbFaultsAndAppointments]
	-- Add the parameters for the stored procedure here
	@schemeId		INT
	,@blockId	INT

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	----------------------------------------------------------------------------------------
	----------------------------------Current Faults ---------------------------------------	
	Declare @currentFaultSelectClause varchar(8000)
	Declare @currentFaultFromClause varchar(2000)
	Declare @currentFaultWhereClause varchar(2000)
	Declare @currentFaultOrderByClause varchar(2000)
	
	SET @currentFaultSelectClause = '	
		SELECT
			FL_CO_APPOINTMENT.AppointmentID AS AppointmentId
			,FL_FAULT_LOG.FaultLogID AS FaultLogId
			,FL_AREA.AreaName AS AreaName
			,FL_FAULT.Description AS Description
			,FL_FAULT_STATUS.Description AS FaultStatus
			,CONVERT(DATE, FL_FAULT_LOG.SubmitDate, 103) AS SubmitDate
			,CONVERT(CHAR(5), CAST(FL_CO_APPOINTMENT.Time AS DATETIME), 108) + '' '' + CONVERT(VARCHAR, FL_CO_APPOINTMENT.AppointmentDate, 103)	AppointmentDate
			,FL_FAULT.FaultId AS FaultId
			,FL_FAULT_LOG.JobSheetNumber AS JobSheetNumber
			,COALESCE(FL_FAULT_LOG.Duration, FL_FAULT.Duration) AS Duration
			,CASE
				WHEN COALESCE(FL_FAULT_LOG.Duration, FL_FAULT.Duration) IS NULL
					THEN ''N/A''
				WHEN COALESCE(FL_FAULT_LOG.Duration, FL_FAULT.Duration) <= 1
					THEN CONVERT(VARCHAR(10), COALESCE(FL_FAULT_LOG.Duration, FL_FAULT.Duration)) + '' hr''
				ELSE CONVERT(VARCHAR(10), COALESCE(FL_FAULT_LOG.Duration, FL_FAULT.Duration)) + '' hrs''
			END	AS DurationString
			,G_Trade.Description AS Trade
			,G_Trade.Description AS TradeName
			,FL_FAULT_LOG.ProblemDays
			,FL_FAULT_LOG.RecuringProblem
			,FL_FAULT_LOG.CommunalProblem
			,CASE
				days
				WHEN 1
					THEN CONVERT(CHAR(6), DATEADD(DAY, FL_Fault_Priority.ResponseTime, GETDATE()), 0)
				ELSE CONVERT(CHAR(6), DATEADD(HOUR, FL_Fault_Priority.ResponseTime, GETDATE()), 0)
			END AS DueDate
			,FL_FAULT_LOG.Notes
			,FL_FAULT.isGasSafe
			,FL_FAULT.isOftec
			,FL_FAULT_LOG.Quantity
			,FL_FAULT_LOG.Recharge
			,'''' AS OrgId
			,'''' AS IsAppointmentConfirmed
			,0 AS tempFaultId
			,FL_FAULT_TRADE.FaultTradeId
			,FL_FAULT_TRADE.TradeId
			,FL_Fault_Priority.PriorityName
			,(CASE
				days
				WHEN 1
					THEN CONVERT(CHAR(12), DATEADD(DAY, FL_Fault_Priority.ResponseTime, GETDATE()), 103)
				ELSE CONVERT(CHAR(12), DATEADD(HOUR, FL_Fault_Priority.ResponseTime, GETDATE()), 103)
			END) AS CompleteDueDate
			,(CASE
				WHEN FL_Fault_Priority.Days = 1
					THEN CONVERT(NVARCHAR(50), FL_Fault_Priority.ResponseTime) + '' days''
				ELSE CONVERT(NVARCHAR(50), FL_Fault_Priority.ResponseTime) + '' hours''
			END) AS Response'
		SET @currentFaultFromClause =' 
			FROM
			FL_FAULT
			INNER JOIN FL_FAULT_LOG ON FL_FAULT.FaultID = FL_FAULT_LOG.FaultID
			INNER JOIN FL_AREA ON FL_FAULT_LOG.AreaID = FL_AREA.AreaID
			INNER JOIN FL_FAULT_STATUS ON FL_FAULT_STATUS.FaultStatusID = FL_FAULT_LOG.StatusID
			LEFT JOIN FL_FAULT_APPOINTMENT ON FL_FAULT_LOG.FaultLogID = FL_FAULT_APPOINTMENT.FaultLogId
			LEFT JOIN FL_CO_APPOINTMENT ON FL_FAULT_APPOINTMENT.AppointmentId = FL_CO_APPOINTMENT.AppointmentID
			INNER JOIN FL_Fault_Priority ON FL_Fault.PriorityId = FL_Fault_Priority.PriorityId
			INNER JOIN FL_FAULT_TRADE ON FL_FAULT_TRADE.FaultTradeId = FL_FAULT_LOG.FaultTradeId
			INNER JOIN G_Trade ON FL_FAULT_TRADE.TradeId = G_Trade.TradeId'
		
		IF(@blockId !=0)
			BEGIN
				SET @currentFaultWhereClause = '
				WHERE				
				FL_FAULT_LOG.SchemeId = '+ convert(varchar(10), @schemeId) +'
				AND FL_FAULT_LOG.BlockId ='+ convert(varchar(10), @blockId) +'							
				AND FL_FAULT_STATUS.FaultStatusID != 17
				AND FL_FAULT_STATUS.FaultStatusID != 13'				
			END
		ELSE 
			BEGIN
				SET @currentFaultWhereClause = '
				WHERE								
				FL_FAULT_LOG.SchemeId = '+ convert(varchar(10), @schemeId )+'
				AND FL_FAULT_STATUS.FaultStatusID != 17
				AND FL_FAULT_STATUS.FaultStatusID != 13'
			END
					
		
		SET @currentFaultOrderByClause = 'ORDER BY SubmitDate DESC'
		EXEC(
				@currentFaultSelectClause +
				@currentFaultFromClause + 
				@currentFaultWhereClause +
				@currentFaultOrderByClause
			)				  		
	----------------------------------------------------------------------------------------  
	----------------------------------Completed Faults -------------------------------------   
	Declare @completeFaultSelectClause varchar(4000)
	Declare @completeFaultSubClause varchar(1000)
	Declare @completeFaultFromClause varchar(2000)
	Declare @completeFaultWhereClause varchar(2000)
	Declare @completeFaultOrderByClause varchar(2000)
	
	--Complete Fault Select Clause
	SET @completeFaultSelectClause = ' 
		SELECT
			FL_CO_APPOINTMENT.AppointmentID
			,FL_FAULT_LOG.FaultLogID AS FaultLogID
			,FL_Area.AreaName AS ElementName
			,FL_FAULT.Description AS FaultDetail
			,FL_FAULT_STATUS.Description AS FaultStatus
			,CONVERT(VARCHAR, FL_FAULT_LOG_HISTORY.LastActionDate, 103) AS SubmitDate
			,CONVERT(CHAR(5), CAST(FL_CO_APPOINTMENT.Time AS DATETIME), 108) + '' '' + CONVERT(VARCHAR, FL_CO_APPOINTMENT.AppointmentDate, 103)	AS AppointmentDate
			,FL_FAULT_LOG_HISTORY.LastActionDate'
	
	
	--IF USER HAS SELECTED THE SCHEME AND BLOCK					
	IF(@blockId !=0)				
		BEGIN
			--Complete Fault Where Clause if block id !=0
			SET @completeFaultWhereClause = ' 
			WHERE
			FL_FAULT_LOG.SchemeId = '+ convert(varchar(10), @schemeId) +'
			AND FL_FAULT_LOG.BlockId ='+ convert(varchar(10), @blockId) +'			
			AND FL_FAULT_STATUS.FaultStatusID = 17
			AND FL_FAULT_LOG_HISTORY.LastActionDate >= DATEADD(mm, -3, GETDATE())'
			
			--Complete Fault Sub Clause if block id !=0
			SET @completeFaultSubClause = '
				SELECT * FROM FL_FAULT_LOG_HISTORY 
				WHERE
				SchemeId = '+ convert(varchar(10), @schemeId) +'
				AND BlockId ='+ convert(varchar(10), @blockId) +'			
				AND FaultStatusID = 17'
		END
	ELSE
		BEGIN
			--IF USER SELECTED THE SCHEME AND DIDN'T SELECT THE BLOCK
			--Complete Fault Where Clause
			SET @completeFaultWhereClause = ' 
			WHERE		
			FL_FAULT_LOG.SchemeId = '+ convert(varchar(10), @schemeId) +'			
			AND FL_FAULT_STATUS.FaultStatusID = 17
			AND FL_FAULT_LOG_HISTORY.LastActionDate >= DATEADD(mm, -3, GETDATE())'
			
			--Complete Fault Sub Clause (if block id = 0)
			SET @completeFaultSubClause = '
				SELECT * FROM FL_FAULT_LOG_HISTORY 
				WHERE
				SchemeId = '+ convert(varchar(10), @schemeId) +'			
				AND FaultStatusID = 17'
		END	
	
	--Complete Fault From Clause
	SET @completeFaultFromClause = '		
		FROM
			FL_FAULT
				INNER JOIN FL_FAULT_LOG ON FL_FAULT.FaultID = FL_FAULT_LOG.FaultID
				INNER JOIN FL_Area ON FL_FAULT_LOG.AreaID = FL_Area.AreaId		
				INNER JOIN FL_FAULT_STATUS ON FL_FAULT_STATUS.FaultStatusID = FL_FAULT_LOG.StatusID
				LEFT JOIN FL_FAULT_APPOINTMENT ON FL_FAULT_LOG.FaultLogID = FL_FAULT_APPOINTMENT.FaultLogId
				LEFT JOIN FL_CO_APPOINTMENT ON FL_FAULT_APPOINTMENT.AppointmentId = FL_CO_APPOINTMENT.AppointmentID
				INNER JOIN
					(
						'+ @completeFaultSubClause +'	
					) AS FL_FAULT_LOG_HISTORY ON FL_FAULT_LOG_HISTORY.FaultLogID = FL_FAULT_LOG.FaultLogID'
		
			
	SET @completeFaultOrderByClause = '					
		ORDER BY FL_FAULT_LOG_HISTORY.LastActionDate DESC'
		
	EXEC(
			@completeFaultSelectClause +
			@completeFaultFromClause + 
			@completeFaultWhereClause +
			@completeFaultOrderByClause
		)				  		
	----------------------------------------------------------------------------------------      
	-----------------------------------Gas servicing appointments.--------------------------  
	
	--SELECT
	--	AS_APPOINTMENTS.APPOINTMENTID
	--	,CONVERT(CHAR(5), CAST(AS_APPOINTMENTS.APPOINTMENTSTARTTIME AS DATETIME), 108) + ' ' + CONVERT(VARCHAR, AS_APPOINTMENTS.APPOINTMENTDATE, 103)	AS AppointmentDate
	--	,AS_Status.Title																																AS STATUS
	--	,e.FIRSTNAME + ' ' + e.LASTNAME																													AS Operative
	--FROM
	--	AS_APPOINTMENTS
	--		INNER JOIN AS_JOURNAL ON AS_APPOINTMENTS.JournalId = AS_JOURNAL.JOURNALID
	--		INNER JOIN AS_Status ON AS_JOURNAL.STATUSID = AS_Status.StatusId
	--		INNER JOIN dbo.E__EMPLOYEE e ON e.EMPLOYEEID = AS_APPOINTMENTS.ASSIGNEDTO
	--WHERE
	--	AS_JOURNAL.PROPERTYID = @propertyId
	--	AND ISCURRENT = 1


	----------------------------------------------------------------------------------------      
	----------------------------------- Planned Appointments -------------------------------  
	--SELECT
	--	JOURNAL_PMO.PMO															AS PMO
	--	,APPOINTMENT_AID.JSN													AS JSN
	--	,ISNULL(PLANNED_COMPONENT.COMPONENTNAME, 'N/A')							AS Component
	--	,ISNULL(CASE
	--		WHEN PLANNED_APPOINTMENTS.isMiscAppointment = 1
	--			THEN MISCTRADE.Description
	--		ELSE COMPTRADE.Description
	--	END, 'N/A')																AS Trade
	--	,ISNULL(CONVERT(VARCHAR(3), DATENAME(WEEKDAY, PLANNED_APPOINTMENTS.APPOINTMENTDATE)) + ' ' +
	--	CONVERT(VARCHAR(11), PLANNED_APPOINTMENTS.APPOINTMENTDATE, 106), 'N/A')	AS AppointmentDate
	--	,PLANNED_APPOINTMENTS.APPOINTMENTSTARTTIME								AS AppointmentTime

	--FROM
	--	PLANNED_JOURNAL
	--		INNER JOIN PLANNED_APPOINTMENTS ON PLANNED_JOURNAL.JOURNALID = PLANNED_APPOINTMENTS.JournalId
	--		LEFT JOIN PLANNED_COMPONENT ON PLANNED_JOURNAL.COMPONENTID = PLANNED_COMPONENT.COMPONENTID
	--		INNER JOIN PLANNED_STATUS ON PLANNED_JOURNAL.STATUSID = PLANNED_STATUS.STATUSID

	--		LEFT JOIN PLANNED_COMPONENT_TRADE ON PLANNED_APPOINTMENTS.COMPTRADEID = PLANNED_COMPONENT_TRADE.COMPTRADEID
	--		LEFT JOIN PLANNED_MISC_TRADE ON PLANNED_APPOINTMENTS.APPOINTMENTID = PLANNED_MISC_TRADE.AppointmentId
	--		LEFT JOIN G_TRADE AS MISCTRADE ON PLANNED_MISC_TRADE.TradeId = MISCTRADE.TradeId
	--		LEFT JOIN G_TRADE AS COMPTRADE ON PLANNED_COMPONENT_TRADE.TradeId = COMPTRADE.TradeId

	--		INNER JOIN
	--			(
	--				SELECT
	--					PLANNED_JOURNAL.JOURNALID								AS JID
	--					,'PMO' + CONVERT(VARCHAR, PLANNED_JOURNAL.JOURNALID)	AS PMO
	--				FROM
	--					PLANNED_JOURNAL
	--			) JOURNAL_PMO ON PLANNED_JOURNAL.JOURNALID = JOURNAL_PMO.JID
	--		INNER JOIN
	--			(
	--				SELECT
	--					PLANNED_APPOINTMENTS.APPOINTMENTID													AS AID
	--					,'JSN' + RIGHT('00000' + CONVERT(VARCHAR, PLANNED_APPOINTMENTS.APPOINTMENTID), 5)	AS JSN
	--				FROM
	--					PLANNED_APPOINTMENTS
	--			) AS APPOINTMENT_AID ON PLANNED_APPOINTMENTS.APPOINTMENTID = APPOINTMENT_AID.AID

	--		INNER JOIN C_TENANCY ON PLANNED_APPOINTMENTS.TENANCYID = C_TENANCY.TENANCYID
	--		INNER JOIN P__PROPERTY ON C_TENANCY.PROPERTYID = P__PROPERTY.PROPERTYID
	--		INNER JOIN C_CUSTOMERTENANCY ON C_CUSTOMERTENANCY.TENANCYID = C_TENANCY.TENANCYID

	--WHERE
	--	PLANNED_STATUS.TITLE = 'Arranged'
	--	AND YEAR(PLANNED_APPOINTMENTS.APPOINTMENTDATE) = YEAR(GETDATE())
	--	AND C_TENANCY.PROPERTYID = @propertyId
	--	AND C_CUSTOMERTENANCY.CUSTOMERID = @customerId

END