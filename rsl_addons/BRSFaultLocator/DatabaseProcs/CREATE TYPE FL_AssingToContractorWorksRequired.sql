USE [RSLBHALive]
GO

CREATE TYPE [dbo].[FL_AssingToContractorWorksRequired] AS TABLE(
	[WorksRequired] [nvarchar](4000) NOT NULL,
	[NetCost] [smallmoney] NOT NULL,
	[VatType] [int] NOT NULL,
	[VAT] [smallmoney] NOT NULL,
	[GROSS] [smallmoney] NOT NULL,
	[PIStatus] [int] NOT NULL,
	[ExpenditureId] [int] NOT NULL
)
GO


