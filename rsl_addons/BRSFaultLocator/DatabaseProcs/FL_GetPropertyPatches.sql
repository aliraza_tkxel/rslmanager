SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC FL_GetPropertyPatches
-- Author:		<Aqib Javed>
-- Create date: <31/1/2013>
-- Description:	<This Stored Proceedure fetch the Patches>
-- WebPage: Resources.aspx
-- =============================================
CREATE PROCEDURE [dbo].[FL_GetPropertyPatches] 
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select * from E_PATCH
END
