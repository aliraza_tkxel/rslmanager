
-- Author:		<Noor Muhammad>
-- Create date: <09/03/2013>
-- Description:	<This script was being created to add FaultTradeId in FL_TEMP_FAULT, FL_FAULT_LOG>

ALTER TABLE [dbo].[FL_TEMP_FAULT] add [FaultTradeId] [int] NULL
ALTER TABLE [dbo].[FL_FAULT_LOG] add [FaultTradeId] [int] NULL

