USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[FL_FaultManagementAmendFault]
/* ===========================================================================
--	EXEC FL_FaultManagementAmendFault
		@FAULTID = 1,
		@AREAID = 1,
		@DESCRIPTION = 'Tap is Broken',
		@PRIORITYID = 1,
		@DURATION = 0.5,
		@RECHARGE = 1, -- Ture
		@NETCOST = 100,
		@VATRATEID = 1,
		@Vat = 20,
		@GROSS = 120,
		@ISCONTRACTOR = 1, --True
		@FAULTACTIVE = 1, --True
		@ISGASSAFE = 1, --True
		@ISOFTEC = 1, --True
		@USERID =65,
		@TradesToDelete = '1',
		@TradesToInsert '2, 3',
		@SUBMITDATE = '2013-03-08,
		@RESULT OUTPUT,
		@MESSAGE OUTPUT
--  Author:			Aamir Waheed
--  DATE CREATED:	8 March 2013, Modified (4 April 2013)
--  Description:	To UPDATE/Amend a fault with given values.
--  Webpage:		View/Reports/ReportArea.aspx (For Amend Fault Modal Popup)
 '==============================================================================*/

@FAULTID INT,
@AREAID INT,
@DESCRIPTION NVARCHAR(200),
@PRIORITYID INT,
@DURATION FLOAT,
@RECHARGE BIT,
@NETCOST FLOAT = NULL,
@VATRATEID INT = NULL,
@Vat FLOAT = NULL,
@GROSS FLOAT = NULL,
@ISCONTRACTOR BIT,
@FAULTACTIVE BIT,
@ISGASSAFE BIT,
@ISOFTEC BIT,
@USERID INT,
@SUBMITDATE DATETIME,
@TradesToDelete NVARCHAR(MAX) = NULL,
@TradesToInsert NVARCHAR(MAX) = NULL,
@RESULT  INT = 0 OUTPUT,
@MESSAGE NVARCHAR(200) = NULL OUTPUT

AS
BEGIN 

SET NOCOUNT ON
BEGIN TRAN


IF EXISTS (SELECT FaultID FROM FL_FAULT WHERE FAULTID=@FAULTID)
BEGIN

	DECLARE	@FaultTradeID INT = NULL,
			@TradeId INT = NULL,
			@TradeName NVARCHAR(100) = NULL
		

--===========================================================================================================

	-- Start -  Check if there are trades to delete and execute the checking and delete process
	IF @TradesToDelete IS NOT NULL AND NOT @TradesToDelete = ''
	BEGIN
		DECLARE @TradeToDeleteCursor CURSOR

		DECLARE @DeleteCursorSelectStmt NVARCHAR(MAX)
		Set @DeleteCursorSelectStmt = 'SELECT FaultTradeId, Description AS TradeName 
							FROM FL_FAULT_TRADE
							INNER JOIN G_TRADE ON FL_FAULT_TRADE.Tradeid = G_TRADE.Tradeid
							WHERE FaultID = ' + CONVERT(NVARCHAR, @FAULTID)   + ' AND FL_FAULT_TRADE.TradeId IN (' + @TradesToDelete + ')'

		DECLARE @SQL NVARCHAR(MAX)
			
		Set @SQL = 'Set @TradeToDeleteCursor = CURSOR FAST_FORWARD FOR ' + @DeleteCursorSelectStmt + '; OPEN @TradeToDeleteCursor'

		exec sp_executesql @Sql, N'@TradeToDeleteCursor CURSOR OUTPUT', @TradeToDeleteCursor OUTPUT

		-- Cursor @TradeToDeleteCursor itterations  starts here	

		FETCH NEXT FROM @TradeToDeleteCursor INTO @FaultTradeID, @TradeName

		WHILE @@FETCH_STATUS = 0
		BEGIN
			-- Check weather a particular trade for this fault (FaultTradeId) is being used, If used set an error and roll back any changes
			-- Other wise delete that trade for this fault (FaultTradeId)
			IF		EXISTS (SELECT TempFaultID FROM FL_TEMP_FAULT WHERE FaultTradeId = @FaultTradeID)
				OR	EXISTS (SELECT FaultLogID FROM FL_FAULT_LOG WHERE FaultTradeId = @FaultTradeID)
				BEGIN
					SET @MESSAGE = 'Trade "' + @TradeName + '" can not be deleted, it is being used'
					GOTO HANDLE_ERROR
				END
			ELSE
				BEGIN
					DELETE FROM FL_FAULT_TRADE WHERE FaultTradeId = @FaultTradeID
				END	    
			FETCH NEXT FROM @TradeToDeleteCursor INTO @FaultTradeID, @TradeName
		END

		CLOSE @TradeToDeleteCursor
		DEALLOCATE @TradeToDeleteCursor
		-- Cursor @TradeToDeleteCursor itterations  ends here
	END
	-- END - Check if there are trades to delete and execute the checking and delete process

--===========================================================================================================


--===========================================================================================================

-- Start - Save changes in FL_Fault Table if there is no error while deleting a trade.
-- This code will only execute if there is not trade to delete or the trades for this fault are deleted sucessfully.	

-- Start trade(s) inseration section

	IF @TradesToInsert IS NOT NULL AND NOT @TradesToInsert = ''
	BEGIN
		DECLARE @TradeToInsertCursor CURSOR

		DECLARE @TradeToInsertSelectStmt NVARCHAR(MAX)
		Set @TradeToInsertSelectStmt = 'SELECT TradeId
										FROM G_TRADE
										WHERE TradeId IN (' + @TradesToInsert + ')'
		
			
		Set @SQL = 'Set @TradeToInsertCursor = CURSOR FAST_FORWARD FOR ' + @TradeToInsertSelectStmt + '; OPEN @TradeToInsertCursor'

		exec sp_executesql @Sql, N'@TradeToInsertCursor CURSOR OUTPUT', @TradeToInsertCursor OUTPUT

		-- Cursor @TradeToInsertCursor itterations  starts here
		FETCH NEXT FROM @TradeToInsertCursor INTO @TradeId

		WHILE @@FETCH_STATUS = 0
		BEGIN
			-- Insert the Fault Trade Relation into table FL_FAULT_TRADE
			INSERT INTO FL_FAULT_TRADE (FaultId, TradeId)
					VALUES (@FAULTID, @TradeId)
			FETCH NEXT FROM @TradeToInsertCursor INTO @TradeId
		END

		CLOSE @TradeToInsertCursor
		DEALLOCATE @TradeToInsertCursor
		
	END
		-- Cursor @TradeToInsertCursor itterations  ends here
-- End trade(s) inseration section
--===========================================================================================================

--===========================================================================================================

	UPDATE FL_FAULT SET
		AREAID = @AREAID,
		Description = @DESCRIPTION,
		PRIORITYID = @PRIORITYID,
		duration = @DURATION,
		Recharge = @RECHARGE,
		NETCOST = @NETCOST,
		VATRATEID = @VATRATEID,
		VAT = @VAT,
		GROSS = @GROSS,
		isContractor = @ISCONTRACTOR,	
		FAULTACTIVE=@FAULTACTIVE,
		IsGasSafe = @ISGASSAFE,
		IsOftec = @ISOFTEC
	WHERE FAULTID=@FAULTID

-- End - Save changes in FL_Fault Table if there is no error while deleting a trade.	
--===========================================================================================================


	-- If insertion fails, goto HANDLE_ERROR block
	IF @@ERROR <> 0 GOTO HANDLE_ERROR

	COMMIT TRAN	

	exec FL_FAULT_TRANSACTIONLOG_ADD @FAULTID,@USERID,1,@SUBMITDATE
	SET @RESULT=1	
	
END


RETURN
END

/*'=================================*/

-- IF an error occurs roll back all changes in this stored procudure and set @Result to -1
HANDLE_ERROR:
	ROLLBACK TRAN
	SET @RESULT=-1
RETURN