USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =================================================
-- EXEC	[dbo].[FL_GetAppointmentData] 
--		@appointmentId = 162
-- Author:		<Muhammad Abdul Wahhab>
-- Create date: <5/2/2013>
-- Description:	<Get Appointment Data>
-- Web Page: CancellationAppointmentSummary.aspx
-- =================================================
ALTER PROCEDURE [dbo].[FL_GetAppointmentData] 
	@appointmentId int
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

SELECT
	E__EMPLOYEE.FIRSTNAME + ' ' + E__EMPLOYEE.LASTNAME		AS Operative
	,CONVERT(CHAR(5), CAST(Apt.Time AS DATETIME), 108)		AS AppointmentStartTime
	,Apt.AppointmentDate									AS AppointmentStartDate
	,Apt.Notes												AS Notes
	,CONVERT(CHAR(5), CAST(Apt.EndTime AS DATETIME), 108)	AS AppointmentEndTime
	,COALESCE(Apt.AppointmentEndDate, Apt.AppointmentDate)	AS AppointmentEndDate
FROM
	FL_CO_APPOINTMENT Apt
		INNER JOIN E__EMPLOYEE ON Apt.OperativeID = E__EMPLOYEE.EMPLOYEEID
WHERE
	Apt.AppointmentID = @appointmentId
END