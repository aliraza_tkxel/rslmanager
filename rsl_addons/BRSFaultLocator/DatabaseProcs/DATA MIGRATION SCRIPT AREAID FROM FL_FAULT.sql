
/* ======================================================================================
--  Author:			Ahmed Mehmood
--  DATE CREATED:	28 December 2015
--  Description:	Data migration for areaId from FL_FAULT TO FL_TEMP_FAULT, FL_FAULT_LOG
 '========================================================================================*/

--===============================================
-- MIGRATE AREAID FROM FL_FAULT TO FL_TEMP_FAULT
--===============================================
UPDATE	FL_TEMP_FAULT
SET		FL_TEMP_FAULT.areaId = FL_FAULT.AREAID
FROM	FL_TEMP_FAULT 
		INNER JOIN FL_FAULT ON FL_TEMP_FAULT.FaultID = 	FL_FAULT.FaultID
		
--===============================================
-- MIGRATE AREAID FROM FL_FAULT TO FL_FAULT_LOG
--===============================================
UPDATE	FL_FAULT_LOG
SET		FL_FAULT_LOG.areaId = FL_FAULT.AREAID
FROM	FL_FAULT_LOG 
		INNER JOIN FL_FAULT ON FL_FAULT_LOG.FaultID = 	FL_FAULT.FaultID 
		
		
