/*
   Wednesday, February 27, 20136:47:18 PM
   User: 
   Server: .
   Database: RSLBHALive
   Application: 
*/

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/

ALTER TABLE dbo.FL_FAULT ADD
	IsGasSafe smallint NULL,
	IsOftec smallint NULL

