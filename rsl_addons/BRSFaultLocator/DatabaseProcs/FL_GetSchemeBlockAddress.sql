-- Stored Procedure

-- =============================================
-- Author:		<Author,,Salman Nazir>
-- Create date: <Create Date,,21-Jan-20115>
-- Description:	<Description,,Get Scheme Block Address info on Faults AppointmentSummary Page>
-- =============================================
CREATE PROCEDURE [dbo].[FL_GetSchemeBlockAddress]
@schemeId int =-1,
@blockId int =-1
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Select ISNULL(P_SCHEME.SCHEMENAME,' ') as SchemeName, ISNULL(ADDRESS1,' ')+' '+ISNULL(ADDRESS2,' ')+' '+ISNULL(ADDRESS3,' ') as [Address],
ISNULL(TOWNCITY,'Town/City N/A') Town,ISNULL(CONVERT(NVARCHAR(15),PostCode), 'Post Code N/A') as POSTCODE,
ISNULL(COUNTY,'County N/A')as COUNTY
 from P_BLOCK
 INNER JOIN P_SCHEME on P_SCHEME.SCHEMEID = P_BLOCK.SCHEMEID

 Where P_SCHEME.SCHEMEID = @schemeId AND (@blockId = -1 or P_BLOCK.BLOCKID = @blockId )


END
GO