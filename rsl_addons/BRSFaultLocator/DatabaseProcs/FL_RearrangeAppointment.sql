USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[FL_RearrangeAppointment]    Script Date: 02/03/2015 14:38:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Behroz Sikander
-- Create date: 20/03/2013
-- Description:	This stored procedure updates the appointment data
-- WebPage: ReArrangingCurrentFault.aspx
--EXEC [FL_RearrangeAppointment] 
--@faultLogIdList = '35156',
--@operativeId = 480,
--@appointmentDate = '',
--@appointmentStartTime = '08:00',
--@appointmentEndTime = '11:00',
--@faultNotes ='notes',
--@result = 0
-- =============================================
ALTER PROCEDURE [dbo].[FL_RearrangeAppointment]  
	-- Add the parameters for the stored procedure here
	@faultLogIdList AS VARCHAR(3000),
	@operativeId AS INT,
	@appointmentDate AS Datetime,
	@appointmentStartTime AS varchar(50),
	@appointmentEndTime AS varchar(50),
	@faultNotes AS  varchar(1000),
	@isCalendarAppointment AS BIT,
	@faultLogList AS FL_FaultLogList READONLY,
	@result AS int OUTPUT,
	@appointmentEndDate AS DATETIME = NULL
AS
BEGIN TRAN
-- SET NOCOUNT ON added to prevent extra result sets from	
SET NOCOUNT ON;

UPDATE
	FL
SET
	FL.Duration = COALESCE(UFL.Duration, FL.Duration)
FROM
	FL_FAULT_LOG FL
		INNER JOIN @faultLogList UFL ON UFL.FaultLogId = FL.FaultLogID

--1.) UPDATE FL_FAULT_LOG SET FaultStatusId=’Appointment Arranged’ 
--2.) INSERT INTO FL_FAULT_LOG_HISTORY
--3.) UPDATE FL_FAULT_JOURNAL SET FaultStatusId=’Appointment Arranged’
--4.) UPDATE FL_CO_APPOINTMENT
--5.) INSERT INTO FL_CO_APPOINTMENT_HISTORY	

--1) UPDATE FL_FAULT_LOG SET FaultStatusId=’Appointment Arranged’

SELECT
	*
	INTO
		#FaultLogIds
FROM
	dbo.SPLIT_STRING(@faultLogIdList, ',')


UPDATE
	FL_FAULT_LOG
SET
	STATUSID = 4
	,SubmitDate = GETDATE()
	,Notes = CASE
				WHEN LTRIM(RTRIM(@faultNotes)) = ''
					THEN NOTES
				ELSE @faultNotes
			END
WHERE
	FaultLogID IN
	(
		SELECT
			COLUMN1
		FROM
			#FaultLogIds
	)


--2) INSERT INTO FL_FAULT_LOG_HISTORY	

INSERT INTO FL_FAULT_LOG_HISTORY (
				JournalId
				,FaultStatusId
				,LastActionDate
				,FaultLogId
				,OrgId
				,Notes
				,PropertyId
				,ContractorId
				,Duration
				,SchemeID
				,BlockID
			)
	SELECT
		FL_FAULT_JOURNAL.JOURNALID
		,FL_FAULT_LOG.StatusId
		,GETDATE()
		,FL_FAULT_LOG.FaultLogId
		,FL_FAULT_LOG.OrgId
		,FL_FAULT_LOG.Notes
		,FL_FAULT_LOG.PropertyID
		,FL_FAULT_LOG.ContractorID
		,FL_FAULT_LOG.Duration
		,FL_FAULT_LOG.SchemeId
		,FL_FAULT_LOG.BlockId
	FROM
		FL_FAULT_LOG
			INNER JOIN FL_FAULT_JOURNAL ON FL_FAULT_LOG.FaultLogId = FL_FAULT_JOURNAL.FaultLogId
			INNER JOIN #FaultLogIds FLL ON FLL.COLUMN1 = FL_FAULT_LOG.FaultLogId

--3) UPDATE FL_FAULT_JOURNAL SET FaultStatusId=’Appointment Arranged’

UPDATE
	FL_FAULT_JOURNAL
SET
	FaultStatusId = 4
WHERE
	FaultLogID IN
	(
		SELECT
			COLUMN1
		FROM
			#FaultLogIds
	)

--4) UPDATE FL_CO_APPOINTMENT		
--Get the appointmentid using the faultloglist
DECLARE @appointmentId AS INT

SELECT DISTINCT
	@appointmentId = AppointmentId
FROM
	FL_FAULT_APPOINTMENT A
		INNER JOIN #FaultLogIds FLL ON FLL.COLUMN1 = A.FaultLogId

UPDATE
	FL_CO_APPOINTMENT
SET
	AppointmentDate = @appointmentDate
	,OperativeId = @operativeId
	,LastActionDate = GETDATE()
	,[Time] = @appointmentStartTime
	,EndTime = @appointmentEndTime
	,AppointmentStatus = 'Appointment Arranged'
	,isCalendarAppointment = @isCalendarAppointment
	,AppointmentEndDate = COALESCE(@appointmentEndDate, @appointmentDate)
WHERE
	AppointmentId = @appointmentId


--5) INSERT INTO FL_CO_APPOINTMENT_HISTORY	
INSERT INTO FL_CO_APPOINTMENT_HISTORY (
			[AppointmentId]
           ,[JobSheetNumber]
           ,[AppointmentDate]
           ,[OperativeID]
           ,[LastActionDate]
           ,[Notes]
           ,[Time]           
           ,[EndTime]
           ,[isCalendarAppointment]
           ,[AppointmentEndDate]
			)
	SELECT
	  [AppointmentID]
      ,[JobSheetNumber]
      ,[AppointmentDate]
      ,[OperativeID]      
      ,[LastActionDate]
      ,[Notes]
      ,[Time]
      ,[EndTime]      
      ,[isCalendarAppointment]
      ,[AppointmentEndDate]
	FROM
		FL_CO_APPOINTMENT
	WHERE
		AppointmentId = @appointmentId


DROP TABLE #FaultLogIds

IF @@ERROR <> 0 BEGIN
ROLLBACK TRAN
SET @result = -1
RETURN @result
END

SET @result = @appointmentId
PRINT (@result)
COMMIT TRAN


