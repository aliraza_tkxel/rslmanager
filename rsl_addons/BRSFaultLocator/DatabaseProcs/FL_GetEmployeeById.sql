USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[AS_GetEmployeeById]    Script Date: 04/02/2013 12:51:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- EXEC FL_GetEmployeeById 	@employeeId = 615
-- Author:		<Noor Muhammad>
-- Create date: <02/04/2013>
-- Description:	<This Stored Proceedure get the Employee by id >
-- Web Page: Bridge.aspx
-- =============================================
ALTER PROCEDURE [dbo].[FL_GetEmployeeById](
	@employeeId int
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT E__EMPLOYEE.EMPLOYEEID as EmployeeId, E__EMPLOYEE.FirstName +' ' + E__EMPLOYEE.LastName as FullName, AC_LOGINS.Active as IsActive
	FROM E__EMPLOYEE 	
	INNER JOIN AC_LOGINS on E__EMPLOYEE.EmployeeId = AC_LOGINS.EmployeeId
	Where E__EMPLOYEE.EMPLOYEEID = @employeeId 
END
