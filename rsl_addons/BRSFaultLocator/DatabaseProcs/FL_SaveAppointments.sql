USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[FL_SaveAppointments]    Script Date: 02/18/2015 21:26:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =====================================================
 --EXEC [dbo].[FL_SaveAppointments]
	--	@faultsList = N'1028',
	--	@operativeId = 480,
	--	@appointmentDate = N'2/4/2013',
	--	@appointmentNotes = N'The tenant thinks that she has overtightened the hot tap',
	--	@time = N'9:30 AM',
	--	@endTime=N'12:30 PM',
	--	@PropertyId='A720040007',
	--	@customerId =2043,
	--	@isRecall =1,
	--	@result = 0
-- Author:		<Aqib Javed>
-- Create date: <20/2/2013>
-- Description:	<Create new appointment against a particular engineer>
-- Web Page: SchedulingCalendar.aspx
-- =====================================================
ALTER PROCEDURE [dbo].[FL_SaveAppointments]
( 
	@faultsList nvarchar(500),
	@operativeId int,
	@appointmentDate datetime,
	@appointmentNotes varchar(500),
	@time varchar(50),
	@endTime varchar(50),
	@PropertyId as nvarchar(20),
	@customerId int,
	@isRecall bit,
	@isCalendarAppointment BIT,
	@isNoEntry BIT,
	@NoEntryFaultLogId INT,
	@UserID INT,
	@appointmentEndDate datetime,
	@result int OUTPUT
)
AS
BEGIN
Declare @SelectStmt as nvarchar(3000)
Declare @Sql as nvarchar(3200)

Declare @FaultId as int
Declare @Quantity as int
Declare @ProblemDays as int
Declare @RecuringProblem as bit
Declare @CommunalProblem as bit
Declare @Notes as varchar(4000)
Declare @ORGId as int
Declare @ItemActionId as int
Declare @Recharge as bit
Declare @StatusId as int
Declare @IsReported as bit
Declare @DueDate as Date
Declare @FaultLogId as int
Declare @JournalId as int
Declare @TempFaultId as int
Declare @appointmentId as int
Declare @ItemID as int
Declare @ItemNatureID as int
Declare @TenancyID as int
Declare @ResponseTime as int
Declare @Days as int
Declare @SubmitDate as DateTime2 = GETDATE()
Declare @FaultTradeId as int
  
Declare @OriginalFaultLogId As Int
DECLARE @isFollowon AS BIT
DECLARE @FollowOnFaultLogID AS INT
DECLARE @TempFaultDuration AS DECIMAL(9, 2)



--Get &  Set Tenancy Id 
SELECT
	@TenancyId = TENANCYID
FROM
	C_CUSTOMERTENANCY
WHERE
	CUSTOMERID = @CustomerID
	AND ENDDATE != NULL

--Get and set the Item id 
SET @ItemId =
(
	SELECT
		ItemID
	FROM
		C_ITEM
	WHERE
		DESCRIPTION = 'Property'
)

--Get and set the Item nature id 
SET @ItemNatureId =
(
	SELECT
		ItemNatureID
	FROM
		C_NATURE
	WHERE
		DESCRIPTION = 'Reactive Repair'
)

-- Getting Status Id
SELECT
	@StatusId = FaultStatusID
FROM
	FL_FAULT_STATUS
WHERE
	[Description] = 'Appointment Arranged'
-- End Getting Status Id

--Creating New Appointment
INSERT INTO FL_CO_APPOINTMENT (
				AppointmentDate
				,Notes
				,OperativeID
				,[Time]
				,EndTime
				,AppointmentStatus
				,LastActionDate				
				,isCalendarAppointment
				,AppointmentEndDate
			)
	VALUES (
			@appointmentDate
			,@appointmentNotes
			,@operativeId
			,@time
			,@endTime
			,'Appointment Arranged'
			,@SubmitDate
			,@isCalendarAppointment
			,@appointmentEndDate
		)

SELECT
	@appointmentID = SCOPE_IDENTITY()

INSERT INTO FL_CO_APPOINTMENT_HISTORY (
				AppointmentDate
				,AppointmentId
				,Notes
				,OperativeID
				,[Time]
				,EndTime
				,LastActionDate
				,isCalendarAppointment
			)
	VALUES (
			@appointmentDate
			,@appointmentID
			,@appointmentNotes
			,@operativeId
			,@time
			,@endTime
			,@SubmitDate
			,@isCalendarAppointment
		)

-- End Creating New Appointment
-- Getting Information from Fl_Temp_Fault against Temp Fault Ids
DECLARE @FaultCursor CURSOR
SET @IsReported = 1

SET @SelectStmt = 'SELECT 
        FL_TEMP_FAULT.TempFaultID as TempFaultID
		,FL_TEMP_FAULT.FaultId as FaultId
		,FL_TEMP_FAULT.Quantity as Quantity
		,FL_TEMP_FAULT.ProblemDays as ProblemDays
		,FL_TEMP_FAULT.RecuringProblem as RecuringProblem
		,FL_TEMP_FAULT.CommunalProblem as CommunalProblem
		,FL_TEMP_FAULT.Notes as Notes
		,FL_TEMP_FAULT.ORGID as ORGID
		,FL_TEMP_FAULT.ItemActionId as ItemActionId
		,FL_TEMP_FAULT.Recharge as Recharge
		,FL_TEMP_FAULT.FaultTradeId as FaultTradeId
		,FL_TEMP_FAULT.OriginalFaultLogId
		,FL_TEMP_FAULT.isFollowon
		, FL_TEMP_FAULT.FollowOnFaultLogID
		,FL_TEMP_FAULT.Duration
		FROM FL_TEMP_FAULT 
		INNER JOIN FL_Fault ON FL_TEMP_FAULT.FaultId = FL_Fault.FaultId		
		INNER JOIN FL_FAULT_PRIORITY ON FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID
		WHERE FL_TEMP_FAULT.TempFaultID IN (' + @faultsList + ') And FL_TEMP_FAULT.ISRECALL =' + CONVERT(VARCHAR(2), @isRecall)

SET @Sql = 'Set @FaultCursor = CURSOR FAST_FORWARD FOR ' + @SelectStmt + '; OPEN @FaultCursor'

EXEC sp_executesql	@Sql
					,N'@FaultCursor CURSOR OUTPUT'
					,@FaultCursor OUTPUT

-- Cursor Code starts here
FETCH NEXT FROM @FaultCursor INTO @TempFaultID, @FaultId, @Quantity, @ProblemDays,
@RecuringProblem, @CommunalProblem, @Notes, @ORGId, @ItemActionId, @Recharge, @FaultTradeId, @OriginalFaultLogId
, @isFollowon, @FollowOnFaultLogID, @TempFaultDuration

WHILE @@FETCH_STATUS = 0 BEGIN

-- Calculating Due Date
SELECT
	@ResponseTime = ResponseTime
	,@Days = Days
FROM
	FL_FAULT_PRIORITY
		INNER JOIN FL_FAULT ON FL_FAULT_PRIORITY.PriorityID = FL_FAULT.PriorityID
WHERE
	FaultID = @FaultId
IF @Days = 0 SET @DueDate = DATEADD(HH, @ResponseTime, @SubmitDate) ELSE SET @DueDate = DATEADD(DD, @ResponseTime, @SubmitDate)
--End Calculating Due Date

--Insert the record in fault log table
INSERT INTO FL_FAULT_LOG (
				FaultID
				,CustomerID
				,Quantity
				,ProblemDays
				,RecuringProblem
				,CommunalProblem
				,Notes
				,ORGID
				,Recharge
				,PROPERTYID
				,StatusID
				,IsReported
				,SubmitDate
				,DueDate
				,FaultTradeId
				,FollowOnFaultLogId
				,Duration
				,UserId
			)
	VALUES (
			@FaultID
			,@customerId
			,@Quantity
			,@ProblemDays
			,@RecuringProblem
			,@CommunalProblem
			,@Notes
			,@ORGId
			,@Recharge
			,@PropertyId
			,@StatusID
			,@IsReported
			,@SubmitDate
			,@DueDate
			,@FaultTradeId
			,@FollowOnFaultLogID
			,@TempFaultDuration
			,@UserId
		)

-- get latest fault log id
SET @FaultLogId = SCOPE_IDENTITY()

--Insert JSN # in Fault Log Table
UPDATE
	FL_FAULT_LOG
SET
	JobSheetNumber = 'JS' + CONVERT(VARCHAR(20), @FaultLogId)
WHERE
	FaultLogID = @FaultLogId

--Insert the record in Fault Appointment Table
INSERT INTO FL_FAULT_APPOINTMENT (
				FaultLogId
				,AppointmentId
			)
	VALUES (
			@FaultLogId
			,@appointmentID
		)

--Insert the record in Journal Table
INSERT INTO FL_FAULT_JOURNAL (
				FAULTLOGID
				,CUSTOMERID
				,PROPERTYID
				,TenancyID
				,ItemID
				,ItemNatureID
				,FaultStatusID
				,CreationDate
				,LastActionDate				
			)
	VALUES (
			@FaultLogId
			,@CustomerID
			,@PropertyId
			,@TenancyID
			,@ItemID
			,@ItemNatureID
			,@StatusID
			,@SubmitDate
			,@SubmitDate
		)

--Get latest Journal id
SET @JournalID = SCOPE_IDENTITY()

--Insertion Fault Log History With Repair Reported Status
INSERT INTO FL_FAULT_LOG_HISTORY (
				JournalID
				,FaultLogID
				,FaultStatusID
				,Notes
				,ORGID
				,ItemActionID
				,PROPERTYID
				,LastActionDate
				,LastActionUserID
			)
	VALUES (
			@JournalID
			,@FaultLogId
			,@StatusID
			,@Notes
			,@ORGId
			,@ItemActionId
			,@PropertyId
			,@SubmitDate
			,@UserID
		)

IF (@isRecall = 1) BEGIN
INSERT INTO FL_FAULT_RECALL (
				FaultLogId
				,OriginalFaultLogId
			)
	VALUES (
			@FaultLogId
			,@OriginalFaultLogId
		)
END

--Update the record in FL_FAULT_FOLLOWON Table to set/show that the follow on is scheduled
IF (EXISTS
(
	SELECT
		FollowOnID
	FROM
		FL_FAULT_FOLLOWON
	WHERE
		FaultLogId = @FollowOnFaultLogID
))
AND (@isFollowon = 1
AND @FollowOnFaultLogID IS NOT NULL) BEGIN
UPDATE
	FL_FAULT_FOLLOWON
SET
	isFollowonScheduled = 1
WHERE
	FaultLogId = @FollowOnFaultLogID
END

FETCH NEXT FROM @FaultCursor INTO @TempFaultID, @FaultId, @Quantity, @ProblemDays,
@RecuringProblem, @CommunalProblem, @Notes, @ORGId, @ItemActionId, @Recharge, @FaultTradeId, @OriginalFaultLogId
, @isFollowon, @FollowOnFaultLogID, @TempFaultDuration
END

CLOSE @FaultCursor
DEALLOCATE @FaultCursor

--Set isNoEntry Scheduled in case an appointment is scheduled for a no entry.

IF @isNoEntry = 1 BEGIN
UPDATE
	FL_FAULT_NOENTRY
SET
	isNoEntryScheduled = 1
WHERE
	FaultLogId = @NoEntryFaultLogId
END

SET @Sql = 'Delete from FL_TEMP_FAULT
		WHERE FL_TEMP_FAULT.TempFaultID IN (' + @faultsList + ') '

EXEC sp_executesql @Sql

SELECT
	@result = @appointmentId
SELECT
	@result
END