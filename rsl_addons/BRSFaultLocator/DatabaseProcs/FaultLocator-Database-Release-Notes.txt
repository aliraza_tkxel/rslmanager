---------------------------------------Week 1-------------------------------------
Newly Created Storeprocedures
----------------------
1. FL_SaveFaultTemporary
2. FL_GetFaultsAndAppointments
3. FL_GetRecentFaults


---------------------------------------Week 2-------------------------------------
Newly Created Storeprocedures
----------------------
1. FL_GetRecallDetail
2. FL_AmendCustomerAddress
3. FL_DeleteFaultFromBasket
4. FL_GetContractorsByTrade
5. FL_GetFaultBasketInfo
6. FL_UpdateOrgId
7. FL_GetEngineersLeaveInformation
8. FL_GetAppointmentsCalendarInfo
9. FL_FaultAppointmentEngineerInfo

Alter Table Script
-------------------
1. Add_Duration_Field_FL_FAULT

Alter Storeprocedures
---------------------
1.FL_GetFaultsAndAppointments 

--------------------------------------Week3------------------------------------------
Newly Created Storeprocedures
------------------------------
1. FL_GetPropertyPatches
2. FL_EmptyFaultBasket
3. FL_GetAppointmentDetail
4. FL_SaveAppointmentDetails
5. FL_GetFaultsList
6. FL_GetFaultTrades


Alter Table Script
-------------------
1. Add-FaultLogId-FL_TEMP_FAULT


Alter Storeprocedures
---------------------
1. FL_GetFaultBasketInfo
2. FL_AmendCustomerAddress
3. FL_FaultAppointmentEngineerInfo

--------------------------------------Week4------------------------------------------
Newly Created Storeprocedures
------------------------------
1. FL_GetAppointmentSummary
2. FL_GetAppointmentData
3. FL_SaveFaultCancellation
4. FL_GetJobSheetDetail
5. FL_GetFollowOnList
6. FL_GetSchedularSearchFaults

--------------------------------------Week5------------------------------------------
Newly Created Storeprocedures
------------------------------
1. FL_GetFollowOnWorksDetails
2. FL_GetJobSheetAndCustomerDetail
3. FL_GetNoEntryDetail
4. FL_GetNoEntryList
5. FL_GetRecallList
6. FL_GetOperativeList
7. FL_GetAvailableOperatives
8. FL_SaveAppointments
9. FL_GetMoreDetails

Alter Storeprocedures
---------------------
1. FL_GetFollowOnList
2. FL_GetFaultsList
3. FL_GetAppointmentSummary
4. FL_GetTemporaryFaultBasket
5. FL_GetFaultsAndAppointments
6. FL_GetRecentFaults
7. FL_GetRecallDetail
8. FL_GetNoEntryList
9. FL_GetSchedularSearchFaults

Alter Table
------------
1. Alter_Table_FL_CO_APPOINTMENT

Create Table
-------------
1. Add_Table_FL_FAULT_RECALL


--------------------------------------Week6------------------------------------------

Newly Created Storeprocedures
------------------------------
1. FL_GetFaultManagementSearchResultsCount
2. FL_GetFaultManagementSearchResults


Alter Storeprocedures
---------------------
1. FL_GetJobSheetDetail
2. FL_GetFollowOnList
3. FL_GetNoEntryList
4. FL_GetRecallList
5. FL_GetFaultsList
6. FL_GetAppointmentSummary
7. FL_SaveAppointmentDetails
8. FL_GetAppointmentData
9. FL_GetFaultsAndAppointments
10.FL_GetMoreDetails
11.FL_GetRecentFaults
12.FL_GetFaultBasketInfo
13.FL_GetAppointmentDetail
14.FL_GetRecallDetail
15.FL_SaveAppointments
16.FL_GetAppointmentsCalendarInfo
17.FL_GetJobSheetAndCustomerDetail
18.FL_GetTemporaryFaultBasket
19.FL_GetRecentFaults
21.FL_GetSchedularSearchFaults

Alter Table
------------
1. Alter_Table_FL_CO_APPOINTMENT_HISTORY
2. Alter_Table_FL_FAULT

--------------------------------------Week7------------------------------------------

Newly Created Storeprocedures
------------------------------
1. FL_GetSubcontractorJobSheetDetail
2. FL_FaultManagementAddFault
3. FL_FaultManagementAddFaultTrade
4. FL_FaultManagementAmendFault
5. FL_FaultManagementDeleteAllTradesByFaultID
6. FL_GetAreaLookUpAll
7. FL_GetFaultValuesByID
8. FL_GetPriorityLookUpAll
9. FL_GetTradeLookUpAll
10. FL_GetVatLookUpAll
11. FL_GetVatValuesByVatID



Alter Storeprocedures
---------------------
1. FL_SaveAppointmentDetails
2. FL_GetTemporaryFaultBasket
3. FL_GetAppointmentDetail
4. FL_GetAvailableOperatives
5. FL_SaveAppointments
6. FL_GetAppointmentsCalendarInfo

Alter Table
------------
1. Alter_Table_FL_FAULT_20130311
2. Alter_Table_FL_FAULT


--------------------------------------Week8------------------------------------------

Newly Created Storeprocedures
------------------------------
1. FL_GetAvailableOperativesForRearrange
2. FL_GetNewlyCreatedAppointmentInfo
3. FL_SaveSubcontractorAppointmentDetails
4. FL_DeleteAllTempFault
5. FL_GetSubcontractorList
6. FL_GetNewlyCreatedAppointmentData
7. FL_GetFaultStatusLookUpSubContractor
8. FL_GetJobSheetSummaryByJSN
9. FL_SetFaultStatusSubContractorUpdate


Alter Storeprocedures
---------------------
1. FL_SaveFaultCancellation
2. FL_SaveFaultTemporary
3. FL_SaveAppointmentDetails
4. FL_GetTemporaryFaultBasket
5. FL_GetFaultBasketInfo
6. FL_GetOperativeList
7. FL_GetAppointmentDetail
8. FL_SaveAppointments
9. FL_GetTemporaryFaultBasket
10.FL_GetFaultsAndAppointments
11.FL_GetFollowOnList
12.FL_GetNoEntryList
13.FL_GetRecallList
14.FL_GetJobSheetDetail
15.FL_GetJobSheetAndCustomerDetail
16.FL_GetRecallDetail
17. FL_GetAvailableOperatives
18. FL_GetMoreDetails
19. FL_GetFaultsAndAppointments

Alter Table
-------------
1. ALTER_TABLE_FL_TEMP_FAULT_ADD_ISRECALL


Insert/Update Script
--------------------
1. Insert,Update-Script-P_INSPECTIONTYPE (For Appliance Servicing Web App)


--------------------------------------Week9---Already Executed on Server---------------------------------------

Newly Created Storeprocedures
------------------------------
1. FL_SaveFaultTemporaryReArrange
2. FL_RearrangeAppointment
3. FL_GetConfirmedAppointmentByJsn

Alter Storeprocedures
---------------------
1. FL_GetNewlyCreatedAppointmentInfo
2. FL_SaveAppointmentDetails
3. FL_SaveAppointments
4. FL_SaveFaultTemporary
5. FL_GetMoreDetails
6. FL_GetSubcontractorList
7. FL_GetJobSheetAndCustomerDetail
8. FL_GetJobSheetDetail
9. FL_GetRecallList
11. FL_GetFaultsAndAppointments
12. FL_GetRecentFaults
13. FL_GetTemporaryFaultBasket
14. FL_GetRecallDetail
15. FL_GetMoreDetails
16. FL_SaveAppointmentDetails
17. FL_SaveAppointments
18. FL_GetConfirmedAppointmentByJsn
19. FL_RearrangeAppointment
20.FL_GetFaultStatusLookUpSubContractor
21.FL_GetRecallList
22.FL_GetTemporaryFaultBasket
23.FL_GetAppointmentsCalendarInfo
24.FL_SaveSubcontractorAppointmentDetails
25.FL_GetContractorsByTrade
26.FL_GetJobSheetDetail
27.FL_GetJobSheetAndCustomerDetail
28.FL_GetFaultManagementSearchResults
29.FL_GetSubcontractorJobSheetDetail
30. FL_GetRecentFaults.sql
31. FL_GetTemporaryFaultBasket.sql
32.FL_GetFaultManagementSearchResults

Alter Table
-------------
1. ALTER_TABLE_TEMP_FAULT_ORIGINAL_FAULTLOGID
2. ALTER_TABLE_FL_FAULT_RECALL_ORIGINALFAULTLOGID
3. ALTER_TABLE_FL_TEMP_FAULT_ISREARRANGE
4. Alter_Table_FL_FAULT_RECALL_Drop_Columns

--------------------------------------Week9 ---------------------------------------------

Newly Created Storeprocedures
------------------------------


Alter Storeprocedures
---------------------
1. FL_GetFaultsAndAppointments
2. FL_GetJobSheetAndCustomerDetail
3. FL_GetJobSheetDetail
4. FL_GetRecallDetail
5. FL_SaveAppointmentDetails
6. FL_GetConfirmedAppointmentByJsn
7. FL_RearrangeAppointment.sql
8. FL_Fault_FullTextSearchIndex.sql
9. FL_GetEmployeeById.sql
10.FL_FaultManagementAmendFault
11.FL_GetAvailableOperatives.sql
12.FL_GetTemporaryFaultBasket.sql
13.FL_GetSubcontractorList
14.FL_GetNoEntryList
15.FL_GetRecallList
16.FL_GetFollowOnList
17.FL_GetNewlyCreatedAppointmentData
18.FL_GetNewlyCreatedAppointmentInfo
19.FL_SetFaultStatusSubContractorUpdate
20.FL_GetSchedularSearchFaults
21.FL_GetAppointmentsCalendarInfo

Alter Table
-------------
1.FL_SaveAppointments
2. Alter_Table_FL_FAULT_NOENTRY_Alter_Notes_length
3. Alter_Table_FL_FAULT_CANCELLED_Alter_Notes_Length

------------------------------------------------------------UAT--------------------------------------------------
Alter Storeprocedures
---------------------
1. FL_GetNewlyCreatedAppointmentInfo
2. FL_GetFaultsAndAppointments
3. FL_GetSchedularSearchFaults
4. FL_GetAppointmentsCalendarInfo
5. FL_GetJobSheetDetail
6. FL_GetJobSheetAndCustomerDetail
7. FL_GetMoreDetails
8. FL_GetRecallDetail
9. FL_GetRecallList.sql

Create Stored Procedure
-----------------------
1.FL_GetRecallDetailByFaultLogID.sql

-----------------------------------------Support Tasks------------------------------------------------------------

Alter Storeprocedures
---------------------
1. FL_GetSubcontractorJobSheetDetail

Alter Table
-------------
1- ALTER_TABLE_S_ORGANISATION_ADD_EMAIL

--------------------------------------------------Joint Tenancy CR------------------------------------------------

Alter Storeprocedure
--------------------
1. FL_GetAppointmentsCalendarInfo

Newly Created Storeprocedure
----------------------------
1. FL_GetJointTenantsInfoByTenancyID