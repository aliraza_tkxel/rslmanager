USE [RSLBHALive ]
GO
/****** Object:  StoredProcedure [dbo].[FL_AddNewRepair]    Script Date: 01/12/2016 17:00:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Raja Aneeq
-- Create date: 30/12/2015
-- Description:	Add a new repair
-- Web Page:	BRSFaultLocator/Views/Reports/ReportsArea.aspx
-- EXEC FL_AddEditRepair 349
-- =============================================
IF object_id('FL_AddNewRepair') IS NULL
    EXEC ('create procedure dbo.FL_AddNewRepair as select 1')
GO
ALTER PROCEDURE [dbo].[FL_AddNewRepair]
	-- Add the parameters for the stored procedure here
	@DESCRIPTION NVARCHAR(200),
	@NETCOST FLOAT ,
	@VAT FLOAT,
	@GROSS FLOAT,
	@VATRATEID INT ,
	@POSTINSPECTION BIT,
	@STOCKCONDITIONITEM BIT,
	@REPAIRACTIVE BIT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	INSERT INTO FL_FAULT_REPAIR_LIST
	(DESCRIPTION, NETCOST, VAT,GROSS, VATRATEID, POSTINSPECTION,STOCKCONDITIONITEM,REPAIRACTIVE, PMParameterID) 
	VALUES 	(@DESCRIPTION, @NETCOST, @VAT,@GROSS, @VATRATEID, @POSTINSPECTION,@STOCKCONDITIONITEM,@REPAIRACTIVE,Null) 
END
