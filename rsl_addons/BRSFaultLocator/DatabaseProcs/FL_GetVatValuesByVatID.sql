USE [RSLBHALive]
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[FL_GetVatValuesByVatID]
/* ===========================================================================
--	EXEC FL_GetVatValuesByVatID
		@VATID=1
--  Author:			Aamir Waheed
--  DATE CREATED:	8 March 2013
--  Description:	To Get List of AreaName and Area ID for LookUp/DropDown Lists
--  Webpage:		View/Reports/ReportArea.aspx (For Add/Amend Fault Modal Popup)
 '==============================================================================*/
@VATID int
AS
	SELECT *
	FROM F_VAT
	WHERE VATID=@VATID
	ORDER BY VATID ASC