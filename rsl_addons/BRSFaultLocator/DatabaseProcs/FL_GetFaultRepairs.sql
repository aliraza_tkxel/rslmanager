USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/* =================================================================================    
    Page Description: Get Fault Repairs
    Author: Ahmed Mehmood
    Creation Date: Dec-14-2015

    Change History:

    Version     Date           By              Description
    =======     ============   ========        ===========================
    v1.0         Dec-14-2015   Ahmed Mehmood   Get Fault Repairs
 ===========================================================================
--Exec FL_GetFaultRepairs
 '==============================================================================*/

-- Author:		<Author,,Ahmed Mehmood>
-- Create date: <Create Date,11/Dec/2013>
-- Description:	<Description,Get fault repair list>

-- =============================================
CREATE PROCEDURE [dbo].[FL_GetFaultRepairs]

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT	FaultRepairListID as id
			, [Description] as val
	FROM	FL_FAULT_REPAIR_LIST  
	WHERE	RepairActive = 1
	ORDER BY FL_FAULT_REPAIR_LIST.Description ASC
 
END
