--Author: Muhammad Abdul Wahhab
--Date: 21/03/2013
--Description: Remove "RecordedOn" and "Notes" columns from FL_FAULT_RECALL.

ALTER TABLE FL_FAULT_RECALL DROP COLUMN RecordedOn, Notes