USE [RSLBHALive]
GO

/****** Object:  UserDefinedTableType [dbo].[FL_FaultLogList]    Script Date: 02/03/2015 18:13:30 ******/
CREATE TYPE [dbo].[FL_FaultLogList] AS TABLE(
	[FaultLogId] [int] NOT NULL,
	[Duration] [decimal](9, 2) NULL DEFAULT ((0.0)),
	PRIMARY KEY CLUSTERED 
(
	[FaultLogId] ASC
)WITH (IGNORE_DUP_KEY = OFF)
)
GO


