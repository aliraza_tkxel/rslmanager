-- Stored Procedure

ALTER PROCEDURE [dbo].[FL_GetFaultManagementSearchResults]
/* ===========================================================================
--	EXEC FL_GetFaultManagementSearchResults
		@searchTerm = '',
		@noOfRows = 1000,
		@offSet = 0,
		@sortColumn = 'FaultID ',
		@sortOrder = 'DESC'
--  Author:			Aamir Waheed
--  DATE CREATED:	1 March 2013
--  Description:	To shortlist faults based on search criteria provided
--  Webpage:		View/Reports/ReportArea.aspx 
 '==============================================================================*/

	(	
		-- Quick find parameters
	    @searchTerm varchar(max) = NULL,

		-- Following Parameters used to limit and sort no. of records found
		@noOfRows  int = 10,
		@offSet   int = 0,

		-- column name on which sorting is performed
		@sortColumn varchar(50) = 'FaultID ',
		@sortOrder varchar (20) = 'DESC',

		-- Paging parameters if not passed will be calculated from @noOfRows and @offSet

		@startRow int = 0,
		@endRow int = 10			
	)


AS

	DECLARE --@selectClause varchar(8000),
	        --@fromClause   varchar(8000),
	        --@whereClause  varchar(8000),	        
	        @orderClause  varchar(500),
	        @query varchar(max),
	        -- used to add in conditions in WhereClause based on search criteria provided
	        @searchCriteria varchar(8000)

	--========================================================================================    
    -- Begin Calculating Start Row and End Row
    SET @startRow = (@noOfRows * @offSet) + 1
	SET	@endRow = @startRow + @noOfRows - 1    		
	 --End Calculating Start Row and End Row
    --========================================================================================*/	


    --========================================================================================
    -- Begin building SearchCriteria clause
    -- These conditions will be added into where clause based on search criteria provided

    SET @searchCriteria = '  1=1 '      


    IF @searchTerm IS NOT NULL AND @searchTerm != ''
       SET @searchCriteria = @searchCriteria + CHAR(10) + CHAR(9) + 
                             ' AND FL_FAULT.Description LIKE ''%'+ LTRIM(@searchTerm) + '%'''                            



    -- End building SearchCriteria clause   
    --========================================================================================*/

	-- Begin building OrderBy clause

	  IF @sortColumn != 'FaultID'       
		SET @sortColumn = @sortColumn + ' ' + @sortOrder + 
						' , FaultID'

		SET @orderClause =  @sortColumn + ' DESC'    


    -- End building OrderBy clause
    --========================================================================================*/      

SET @query = '
SELECT
		x2.*		
	FROM (	
		SELECT
					x1.*
					,ROW_NUMBER() OVER(ORDER BY '+ @orderClause + ') AS rowNumber
		FROM (

				SELECT	
					FL_FAULT.FaultID,
					FL_FAULT.Description AS Description,
					ISNULL(STUFF(
					(
						SELECT	'', '' + [G_TRADE].[Description] 
						FROM	FL_FAULT_TRADE
								JOIN G_TRADE ON FL_FAULT_TRADE.TradeId = G_TRADE.TradeId
						WHERE	FL_FAULT_TRADE.FaultId = FL_FAULT.FaultID
					FOR XML PATH('''')					
					)
					,1,1,''''), ''N/A'') AS Trade ,
					FL_FAULT_PRIORITY.PriorityName AS PriorityName,
					CASE WHEN FL_FAULT.duration IS NULL THEN ''N/A'' ELSE CONVERT(nvarchar, FL_FAULT.duration) +'' hr'' END AS duration,
					CASE WHEN FL_FAULT.NetCost IS NULL THEN ''N/A'' ELSE CONVERT(nvarchar, FL_FAULT.NetCost) END AS NetCost,
					CASE FL_FAULT.FaultActive when 0 then ''Inactive'' when 1 then ''Active'' end as FaultActive,
					CASE WHEN FL_FAULT.isContractor IS NULL THEN ''N/A'' WHEN FL_FAULT.isContractor = 1 THEN ''Yes'' ELSE ''No'' END as isContractor

				FROM 
					FL_FAULT 
					INNER JOIN FL_FAULT_PRIORITY ON FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID


				WHERE ( '+
					@searchCriteria +
					' )


		) AS x1

) AS x2
	WHERE 
		rowNumber BETWEEN ' +  CONVERT(varchar, @startRow) + ' AND ' + CONVERT(varchar, @endRow)
PRINT @query

EXEC (@query)
GO