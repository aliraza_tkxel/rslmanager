USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[FL_GetRecallDetail]    Script Date: 04/22/2013 17:29:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--EXEC FL_GetRecallDetail
--@faultLogID =20171
-- Author:		<Ahmed Mehmood>
-- Create date: <21/1/2013>
-- Description:	<Returns recall detail>
-- Webpage:RecallDetails.aspx
-- =============================================
ALTER PROCEDURE [dbo].[FL_GetRecallDetail] 
	-- Add the parameters for the stored procedure here
	(
	@faultLogID int
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Fault Log Info and Appointment Info

	Select	FL_FAULT_LOG.JobSheetNumber JSN,
			ISNULL(S_ORGANISATION.NAME,'N/A') ContractorName,
			ISNULL(E__EMPLOYEE.FIRSTNAME+' '+E__EMPLOYEE.LASTNAME,'-') as OperativeName,
			CASE WHEN FL_FAULT_PRIORITY.Days = 1 THEN
			convert(varchar(5),FL_FAULT_PRIORITY.ResponseTime)+' days' ELSE
			convert(varchar(5),FL_FAULT_PRIORITY.ResponseTime)+' hours' END as Priority,
			convert(VARCHAR(17), FL_CO_APPOINTMENT.LastActionDate,113)as Completion,
			convert(VARCHAR(17), FL_FAULT_LOG.SubmitDate,113)as OrderDate,
			FL_AREA.AreaName Location,
			FL_FAULT.Description Description,
			FL_FAULT_LOG.Notes Notes,
			convert(VARCHAR(20), FL_CO_APPOINTMENT.AppointmentDate,106)as AppointmentTimeDate,
			FL_CO_APPOINTMENT.Notes Notes ,
			ISNULL(FL_CO_APPOINTMENT.Time +' '+datename(weekday,FL_CO_APPOINTMENT.AppointmentDate),'N/A') Time,
			FL_FAULT.FaultId as FaultId	  	  
			
		  
	From    FL_FAULT_LOG
			left outer join FL_FAULT_APPOINTMENT on  FL_FAULT_LOG.FaultLogID=FL_FAULT_APPOINTMENT.FaultLogId
			left outer join FL_CO_APPOINTMENT on FL_FAULT_APPOINTMENT.AppointmentId = FL_CO_APPOINTMENT.AppointmentID
			inner join FL_FAULT on FL_FAULT_LOG.FaultID = FL_FAULT.FaultID
			inner join FL_FAULT_PRIORITY  on FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID
			inner join FL_AREA on FL_AREA.AreaID = FL_FAULT_LOG.AREAID 
			left outer join S_ORGANISATION on S_ORGANISATION.ORGID=FL_FAULT_LOG.ORGID 
		    left outer join E__EMPLOYEE on E__EMPLOYEE.EMPLOYEEID = FL_CO_APPOINTMENT.OperativeID
		    
	Where	FL_FAULT_LOG.FaultLogID = @faultLogID

	
-- Asbestos Info

	Select	P_PROPERTY_ASBESTOS_RISK.ASBRISKID AsbRiskID ,
			P_Asbestos.RISKDESCRIPTION Description
			
	From	FL_FAULT_LOG 
			INNER JOIN P_PROPERTY_ASBESTOS_RISKLEVEL on FL_FAULT_LOG.PROPERTYID = P_PROPERTY_ASBESTOS_RISKLEVEL.PROPERTYID
			INNER JOIN P_PROPERTY_ASBESTOS_RISK on P_PROPERTY_ASBESTOS_RISK.PROPASBLEVELID = P_PROPERTY_ASBESTOS_RISKLEVEL.PROPASBLEVELID
			INNER JOIN P_ASBESTOS on P_PROPERTY_ASBESTOS_RISKLEVEL.ASBESTOSID = P_ASBESTOS.ASBESTOSID
	
	Where	FL_FAULT_LOG.FaultLogID = @faultLogID

-- Repair Info

	Select	convert(date, FL_CO_APPOINTMENT.RepairCompletionDateTime,103) as RepairCompletionDateTime ,
			Ltrim(Rtrim(FL_FAULT_REPAIR_LIST.Description)) as Description  			  
	FROM	FL_CO_FAULTLOG_TO_REPAIR 
			INNER JOIN FL_FAULT_REPAIR_LIST on FL_CO_FAULTLOG_TO_REPAIR.FaultRepairListID=FL_FAULT_REPAIR_LIST.FaultRepairListID
			INNER JOIN FL_FAULT_LOG ON FL_FAULT_LOG.FaultLogId = FL_CO_FAULTLOG_TO_REPAIR.FaultLogId
			INNER JOIN FL_FAULT_APPOINTMENT ON FL_FAULT_LOG.FAULTLOGID = FL_FAULT_APPOINTMENT.FAULTLOGID 
			INNER JOIN FL_CO_APPOINTMENT ON FL_FAULT_APPOINTMENT.APPOINTMENTID = FL_CO_APPOINTMENT.APPOINTMENTID			
	Where	FL_CO_FAULTLOG_TO_REPAIR.FaultLogID=@faultLogID
				
END
