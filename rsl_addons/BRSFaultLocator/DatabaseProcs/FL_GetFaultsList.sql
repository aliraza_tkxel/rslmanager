USE [RSLBHALive]
GO

/****** Object:  StoredProcedure [dbo].[FL_GetFaultsList]    Script Date: 02/26/2013 19:09:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
--EXEC	[dbo].[FL_GetFaultsList]
--		@faultsList = N'283,5109'
-- Author:		<Muhammad Abdul Wahhab>
-- Create date: <30/1/2013>
-- Description:	<Get faults list>
-- Web Page: AppointmentSummary.aspx
-- =============================================
ALTER PROCEDURE [dbo].[FL_GetFaultsList] 
	-- Add the parameters for the stored procedure here
	@faultsList nvarchar(500)
AS
BEGIN
DECLARE 
        @SelectClause varchar(1000),
        @fromClause   varchar(1000),
        @whereClause varchar(1000),
        @orderClause varchar(500)        
	--Getting Faults List
	SET  @SelectClause=' SELECT FL_TEMP_FAULT.TempFaultId as TempFaultId, FL_Fault.FaultId as FaultId,
		FL_AREA.AreaName as AreaName, FL_Fault.Description as Description,
		FL_Fault.Duration as Duration,G_TRADE.Description as Trade,
		FL_TEMP_FAULT.ProblemDays as ProblemDays, FL_TEMP_FAULT.RecuringProblem as RecuringProblem,
		FL_TEMP_FAULT.CommunalProblem as CommunalProblem,
		DATENAME(D,(CASE WHEN FL_FAULT_PRIORITY.Days = 1 THEN
		DATEADD(DAY,FL_FAULT_PRIORITY.ResponseTime, GETDATE()) ELSE
		DATEADD(HOUR,FL_FAULT_PRIORITY.ResponseTime, GETDATE()) END)) + '' '' + 
		CONVERT(varchar(3), 
		(CASE WHEN FL_FAULT_PRIORITY.Days = 1 THEN
		DATEADD(DAY,FL_FAULT_PRIORITY.ResponseTime, GETDATE()) ELSE
		DATEADD(HOUR,FL_FAULT_PRIORITY.ResponseTime, GETDATE()) END), 100) as Date '
	
	SET @fromClause=' From FL_TEMP_FAULT 
		INNER JOIN FL_Fault ON FL_TEMP_FAULT.FaultId = FL_Fault.FaultId
		INNER JOIN FL_Area ON FL_TEMP_FAULT.AreaId = FL_Area.AreaId
		INNER JOIN FL_Fault_Trade ON FL_Fault.FaultId = FL_Fault_Trade.FaultId
		INNER JOIN G_TRADE ON FL_FAULT_TRADE.TradeId = G_TRADE.TradeId
		INNER JOIN FL_FAULT_PRIORITY ON FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID '
	
	SET @whereClause=' WHERE FL_TEMP_FAULT.TempFaultID IN (' + @faultsList + ')'
	
	SET @orderClause=' ORDER BY FL_Fault_Trade.TradeId ASC '		
	
	EXEC (@selectClause + @fromClause + @whereClause + @orderClause )    
END

GO


