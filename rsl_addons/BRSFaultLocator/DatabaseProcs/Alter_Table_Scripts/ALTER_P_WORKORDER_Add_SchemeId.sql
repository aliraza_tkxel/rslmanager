/* =================================================================================    
    
    Author: Aamir Waheed
    Creation Date:  19/06/2015
    Description: Alter P_WORKORDER to add (column) SchemeId to define a relation of work order
			, with scheme for the works assigned to contractor from reactive repair module

================================================================================= */


IF NOT EXISTS (SELECT Col.column_id FROM sys.columns AS Col WHERE Col.name = N'SchemeId' AND Col.object_id = OBJECT_ID(N'dbo.P_WORKORDER'))
BEGIN
	PRINT 'Not Exists, Adding'
	
--------------------------------------------------------------
	
	BEGIN TRANSACTION
	SET QUOTED_IDENTIFIER ON
	SET ARITHABORT ON
	SET NUMERIC_ROUNDABORT OFF
	SET CONCAT_NULL_YIELDS_NULL ON
	SET ANSI_NULLS ON
	SET ANSI_PADDING ON
	SET ANSI_WARNINGS ON
	COMMIT
	BEGIN TRANSACTION
	
	ALTER TABLE dbo.P_WORKORDER ADD
		SchemeId INT NULL
		
	COMMIT
	SELECT Has_Perms_By_Name(N'dbo.P_WORKORDER', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.P_WORKORDER', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.P_WORKORDER', 'Object', 'CONTROL') as Contr_Per 
	
--------------------------------------------------------------
	
END
ELSE
	PRINT 'Already Exists'