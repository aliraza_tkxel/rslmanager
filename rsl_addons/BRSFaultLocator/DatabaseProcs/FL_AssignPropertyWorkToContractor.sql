USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[FL_AssignPropertyWorkToContractor]    Script Date: 09/10/2015 07:57:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- Stored Procedure

-- =============================================
-- Author:		Ahmed Mehmood
-- Create date: 03/04/2015
-- Description:	Assign Work to Contractor, it can be either Repairs work. work or conditional work.
-- Updated 16/06/2015: Added "+ ' (JS' + CAST(FL_FAULT_LOG.FaultLogID AS VARCHAR) + ')'" to @FAULTDESCRIPTION 
--					   to allow some front end clarity on JS numbers/PO numbers
-- =============================================
ALTER PROCEDURE [dbo].[FL_AssignPropertyWorkToContractor] 
	-- Add the parameters for the stored procedure here
	@contactId INT,
	@contractorId INT,
	@userId int,
	@POStatus INT,
	@PropertyId varchar(20),
	@journalId INT ,
	@ContractorWorksDetail AS FL_AssingToContractorWorksRequired READONLY,
	@isSaved BIT = 0 OUTPUT,
	@journalIdOut INT OUTPUT,
	@faultContractorId INT OUTPUT,
	@POId int OUTPUT,
	@POStatusId INT OUTPUT

AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

BEGIN TRANSACTION
BEGIN TRY

-- =====================================================
-- General Purpose Variable
-- =====================================================

-- To save same time stamp in all records 
DECLARE @CurrentDateTime AS datetime2 = GETDATE()

--================================================================================
--Get Status Id for Status Title "Assigned To Contractor"
--In case (for first time) it does not exists Insert it and get Status Id.

-- Variables to get Status Id and Status History Id
DECLARE @newStatusHistoryId int = NULL
DECLARE @newStatusId int = NULL
-- =====================================================
-- Insert new Purchase Order
-- =====================================================

DECLARE @Active bit = 1
, @POTYPE int = (	SELECT	POTYPEID
					FROM F_POTYPE
					WHERE POTYPENAME = 'Repair') -- 2 = 'Repair'

, @purchaseOrderId int
, @FAULTDESCRIPTION VARCHAR(4000)
, @BlockId INT = NULL
, @SchemeId INT = NULL
, @DevelopmentId INT = NULL
, @CustomerId INT = NULL

SELECT	@FAULTDESCRIPTION = F.DESCRIPTION + ' (JS' + CAST(FL.FaultLogID AS VARCHAR(50)) + ')'
		--,@BlockId = FL.BlockId
		--,@SchemeId = FL.SchemeId
		--,@DevelopmentId = S.DEVELOPMENTID
		,@CustomerId = FL.CustomerId
		,@PropertyId = ISNULL(NULLIF(@PropertyId,'0'),FL.PROPERTYID)
FROM	FL_FAULT_JOURNAL FJ
		INNER JOIN FL_FAULT_LOG FL ON FJ.FAULTLOGID = FL.FAULTLOGID
		INNER JOIN FL_FAULT F ON FL.FAULTID = F.FAULTID
		LEFT JOIN P_SCHEME S ON FL.SchemeId = S.SCHEMEID
WHERE	FJ.JOURNALID = @journalId

INSERT INTO F_PURCHASEORDER (PONAME, PODATE, PONOTES, USERID, SUPPLIERID, ACTIVE,
POTYPE, POSTATUS, GASSERVICINGYESNO, POTIMESTAMP, DEVELOPMENTID, BLOCKID)
	VALUES (UPPER('Day to Day Repair Work Order'), @CurrentDateTime, @FAULTDESCRIPTION , @userId, @ContractorId, @ACTIVE, @POTYPE, @POSTATUS, 0, @CurrentDateTime, @DevelopmentId, @BlockId)

-- To get Identity Value of Purchase Order.
SET @purchaseOrderId = SCOPE_IDENTITY()

-- =====================================================
-- Old table(s)
-- Insert new P_WORKORDER
-- =====================================================
DECLARE @Title nvarchar(50) = 'Assigned To Contractor'
DECLARE @WOSTATUS INT = 2 -- 'Assigned To Contractor
If (@POStatus = 0) SET @WOSTATUS = 12 --Pending
DECLARE @BIRTH_NATURE INT = 2 --'Day To Day Repair'
DECLARE @BIRTH_ENTITY INT = 1 --'FOR Property'
DECLARE @BIRTH_MODULE INT = 1 --'Customer Module'

DECLARE @Today DATE = CAST(@CurrentDateTime AS DATE)
DECLARE @TenancyId INT
SELECT @TenancyId = MAX(TENANCYID) FROM C_TENANCY T
	WHERE T.PROPERTYID = @PropertyId AND (T.ENDDATE IS NULL OR T.ENDDATE >= @Today)



INSERT INTO P_WORKORDER (ORDERID, TITLE, CUSTOMERID, PROPERTYID, TENANCYID, WOSTATUS, CREATIONDATE, BIRTH_MODULE
						,	BIRTH_ENTITY, BIRTH_NATURE, DEVELOPMENTID, BLOCKID, GASSERVICINGYESNO, SchemeId)
		VALUES(@purchaseOrderId,@TITLE, @CustomerId, @PropertyId, @TenancyId,@WOSTATUS,@CurrentDateTime,@BIRTH_MODULE
				, @BIRTH_ENTITY, @BIRTH_NATURE, @DevelopmentId, @BlockId, 0, @SchemeId)

DECLARE @WOID INT = SCOPE_IDENTITY()

-- =====================================================
-- Insert new FL_CONTRACTOR_WORK
-- =====================================================

INSERT INTO FL_CONTRACTOR_WORK (JournalId, ContractorId, AssignedDate, AssignedBy
,ContactId,PurchaseORDERID)
	VALUES (@journalId,@ContractorId,@CurrentDateTime,@userId,@contactId,@PurchaseOrderId )

SET @faultContractorId = SCOPE_IDENTITY()


-- =====================================================
-- Declare a cursor to enter works requied,
--  loop through record and instert in table
-- =====================================================

DECLARE worksRequiredCursor CURSOR FOR SELECT
	*
FROM @ContractorWorksDetail
OPEN worksRequiredCursor

-- Declare Variable to use with cursor
DECLARE @WorksRequired nvarchar(4000),
@NetCost smallmoney,
@VatType int,
@VAT smallmoney,
@GROSS smallmoney,
@PIStatus int,
@ExpenditureId int

-- Variable used within loop
DECLARE @PurchaseItemTITLE nvarchar(4000) = @FAULTDESCRIPTION -- Title for Purchase Items, specially to inset in F_PurchaseItem

-- =====================================================
-- Loop (Start) through records and insert works required
-- =====================================================		
		-- Fetch record for First loop iteration.
		FETCH NEXT FROM worksRequiredCursor INTO @WorksRequired, @NetCost, @VatType, @VAT,
		@GROSS, @PIStatus, @ExpenditureId
		WHILE @@FETCH_STATUS = 0 BEGIN

		-- =====================================================
		-- Old table(s)
		-- INSERT VALUE IN C_JOURNAL FOR EACH PROPERTY SELECTED ON WORKORDER
		-- =====================================================

		DECLARE @ITEMID INT = 1 --'PROPERTY'
		DECLARE @STATUS INT = 2 --'ASSIGNED
		If (@POStatus = 0) SET @STATUS = 12 --Pending


		INSERT INTO C_JOURNAL ( CUSTOMERID, TENANCYID, PROPERTYID, ITEMID,ITEMNATUREID, CURRENTITEMSTATUSID, CREATIONDATE, TITLE)
		VALUES(@CustomerId, @TenancyId, @PROPERTYID, @ITEMID, @BIRTH_NATURE, @STATUS, @CurrentDateTime,@TITLE)

		DECLARE @CJOURNALID INT = SCOPE_IDENTITY()

		-- =====================================================
		-- Old table(s)
		--INSERT VALUE IN C_REPAIR FOR EACH PROPERTY SELECTED ON WORKORDER
		-- =====================================================

		DECLARE @ITEMACTIONID INT = 2 --'ASSIGNED TO CONTRACTOR'

		If (@POStatus = 0) SET @ITEMACTIONID = 12 --Pending

		DECLARE @PROPERTYADDRESS NVARCHAR(400)

		SELECT	@PROPERTYADDRESS = ISNULL(FLATNUMBER+',','')+ISNULL(HOUSENUMBER+',','')+ISNULL(ADDRESS1+',','')+ISNULL(ADDRESS2+',','')+ISNULL(ADDRESS3+',','')+ISNULL(TOWNCITY+',','')+ISNULL(POSTCODE,'')
		FROM	P__PROPERTY
		WHERE	PROPERTYID = @PROPERTYID

		--ITEMDETAILID WILL BE NULL BECAUSE THE COST OF WORKORDER DEPENDS ON PROGRAMME OF PLANNED MAINTENANCE 
		--AND THE SCOPEID WILL ALSO BE NULL BECAUSE IT IS NOT A GAS SERVICING CONTRACT

		INSERT INTO C_REPAIR (JOURNALID, ITEMSTATUSID, ITEMACTIONID, LASTACTIONDATE,LASTACTIONUSER, ITEMDETAILID,
							  CONTRACTORID, TITLE, NOTES,SCOPEID)	
		VALUES(@CJOURNALID, @STATUS, @ITEMACTIONID, @CurrentDateTime, @userId,NULL,@CONTRACTORID,'FOR'+@PROPERTYADDRESS,@TITLE,NULL)

		DECLARE @REPAIRHISTORYID INT = SCOPE_IDENTITY()

		-- =====================================================
		--Insert Values in F_PURCHASEITEM for each work required and get is identity value.
		-- =====================================================

		INSERT INTO F_PURCHASEITEM (ORDERID, EXPENDITUREID, ITEMNAME, ITEMDESC, PIDATE,
		NETCOST, VATTYPE, VAT, GROSSCOST, USERID, ACTIVE, PITYPE, PISTATUS)
			VALUES (@PurchaseOrderId, @ExpenditureId, @PurchaseItemTITLE, @WorksRequired, 
			@CurrentDateTime,  @NetCost, @VatType, @VAT, @GROSS, @userId, @ACTIVE, @POTYPE, @POSTATUS)

		DECLARE @ORDERITEMID int = SCOPE_IDENTITY()

		-- =====================================================
		--INSERT VALUE IN P_WOTOREPAIR for each work required
		-- =====================================================

		INSERT INTO P_WOTOREPAIR(WOID, JOURNALID, ORDERITEMID)
		VALUES(@WOID,@CJOURNALID,@ORDERITEMID)

		-- =====================================================
		-- Insert values in FL_CONTRACTOR_WORK_DETAIL for each work required
		-- =====================================================

		INSERT INTO FL_CONTRACTOR_WORK_DETAIL(FaultContractorId, WorkRequired, NetCost
			, VatId, Vat, Gross, ExpenditureId, PURCHASEORDERITEMID)
		VALUES(@faultContractorId, @WorksRequired, @NetCost, @VatType, @VAT, @GROSS
				, @ExpenditureId,@ORDERITEMID)

-- Fetch record for next loop iteration.
FETCH NEXT FROM worksRequiredCursor INTO @WorksRequired, @NetCost, @VatType, @VAT,
@GROSS, @PIStatus, @ExpenditureId
END

-- =====================================================
-- Loop (End) through records and insert works required
-- =====================================================

-- =====================================================
-- If PO is queue set work order status to 12 (queued) as set in Portfolio Work Order
-- =====================================================

IF @POStatus = 0
 BEGIN
  UPDATE P_WORKORDER SET WOSTATUS = 12 WHERE WOID = @WOID
  
 END
ELSE
 BEGIN
	-- - - -  - - - - - - -  -
	-- AUTO ACCEPT REPAIRS
	-- - ---- - - - -  - - - -
	-- we check to see if the org has an auto accept functionality =  1
	-- if so then we auto accept the repair
  EXEC C_REPAIR_AUTO_ACCEPT @ORDERID = @PurchaseOrderId, @SUPPLIERID =  @ContractorId
END


CLOSE worksRequiredCursor
DEALLOCATE worksRequiredCursor
Set @POId = @PurchaseOrderId
END TRY
BEGIN CATCH 
	IF @@TRANCOUNT > 0
	BEGIN     
		ROLLBACK TRANSACTION;   
		SET @isSaved = 0        
	END
	DECLARE @ErrorMessage NVARCHAR(4000);
	DECLARE @ErrorSeverity INT;
	DECLARE @ErrorState INT;

	SELECT @ErrorMessage = ERROR_MESSAGE(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE();

	-- Use RAISERROR inside the CATCH block to return 
	-- error information about the original error that 
	-- caused execution to jump to the CATCH block.
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
			);
END CATCH;

IF @@TRANCOUNT > 0
	BEGIN  
		COMMIT TRANSACTION;  
		SET @isSaved = 1
	END

SET @journalIdOut = @journalId
SET @POStatusId = @POStatus
END
