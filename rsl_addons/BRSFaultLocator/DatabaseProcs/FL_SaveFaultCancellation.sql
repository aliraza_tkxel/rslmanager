USE [RSLBHALive]
GO

/****** Object:  StoredProcedure [dbo].[FL_SaveFaultCancellation]    Script Date: 03/12/2013 19:29:45 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- =====================================================
-- EXEC [dbo].[FL_SaveFaultCancellation]
--		@faultsList = N'112,179',
--		@notes = N'The clent has fixed the problem',
--		@result = @result OUTPUT
-- Author:		<Muhammad Abdul Wahhab>
-- Create date: <6/2/2013>
-- Description:	<Saves fault cancellation information>
-- Web Page: CancellationAppointmentSummary.aspx
-- =====================================================
ALTER PROCEDURE [dbo].[FL_SaveFaultCancellation]
( 
	@faultsList nvarchar(500),	
	@notes nvarchar(100),	
	@result bit OUTPUT
)
AS
BEGIN
Declare @SelectStmt as nvarchar(3000)
Declare @Sql as nvarchar(3200)

Declare @FaultLogID as int
Declare @ORGID as int
Declare @FaultStatusID as int
Declare @FaultNotes as varchar(4000)
Declare @PROPERTYID as nvarchar(20)
Declare @ContractorID as int
Declare @JournalID as int

SELECT @FaultStatusID = FaultStatusID
FROM FL_FAULT_STATUS
WHERE Description = 'Cancelled'

-- this is a cursor variable and is needed so we can use dynamic-sql which we need to deal with the list of TempFaultIDs
Declare @FaultCursor CURSOR

Set @SelectStmt = 'SELECT FL_FAULT_LOG.FaultLogID as FaultLogID, FL_FAULT_LOG.ORGID as ORGID,
	FL_FAULT_LOG.Notes as FaultNotes, FL_FAULT_LOG.PROPERTYID as PROPERTYID,
	FL_FAULT_LOG.ContractorID as ContractorID
	From FL_FAULT_LOG
	WHERE FL_FAULT_LOG.FaultLogID IN (' + @faultsList + ') '

Set @Sql = 'Set @FaultCursor = CURSOR FAST_FORWARD FOR ' + @SelectStmt + '; OPEN @FaultCursor'

exec sp_executesql @Sql, N'@FaultCursor CURSOR OUTPUT', @FaultCursor OUTPUT

FETCH NEXT FROM @FaultCursor into @FaultLogID,@ORGID,@FaultNotes,@PROPERTYID,@ContractorID

WHILE @@FETCH_STATUS = 0
BEGIN	

	INSERT INTO FL_FAULT_CANCELLED (FaultLogId, Notes, RecordedOn)
	VALUES (@FaultLogID, @notes, GETDATE())
	
	UPDATE FL_FAULT_LOG
	SET StatusID = @FaultStatusID
	WHERE FL_FAULT_LOG.FaultLogID = @FaultLogID	
	
	UPDATE FL_FAULT_JOURNAL	
	SET FaultStatusID = @FaultStatusID
	WHERE FL_FAULT_JOURNAL.FaultLogID = @FaultLogID
	
	SELECT @JournalID = FL_FAULT_JOURNAL.JournalID
	FROM FL_FAULT_JOURNAL
	WHERE FL_FAULT_JOURNAL.FaultLogID = @FaultLogID
	
	INSERT INTO FL_FAULT_LOG_HISTORY (JournalID, FaultLogID, ORGID, FaultStatusID,
	Notes, PROPERTYID, ContractorID)
	VALUES (@JournalID, @FaultLogID, @ORGID, @FaultStatusID, @FaultNotes, @PROPERTYID,
	@ContractorID)	
	
	print 'Processing : ' + CAST(@@IDENTITY as varchar(20))
    
    FETCH NEXT FROM @FaultCursor into @FaultLogID,@ORGID,@FaultNotes,@PROPERTYID,
    @ContractorID
END

CLOSE @FaultCursor
DEALLOCATE @FaultCursor

SET @result = 1

END

GO


