USE [RSLBHALive]
GO
/****** Object:  StoredProcedure [dbo].[FL_GetJobSheetDetail]    Script Date: 09/27/2013 17:28:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
--EXEC FL_GetJobSheetDetail
--@jobSheetNumber ='JS5'
-- Author:		<Ahmed Mehmood>
-- Create date: <6/2/2013>
-- Description:	<Returns Job Sheet Summary>
-- Webpage:JobSheetDetail.aspx
-- =============================================
ALTER PROCEDURE [dbo].[FL_GetJobSheetDetail] 
	-- Add the parameters for the stored procedure here
	(
	@jobSheetNumber varchar(50)
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	-- Fault Log Info and Appointment Info

	Select	FL_FAULT_LOG.JobSheetNumber JSN,
			ISNULL(S_ORGANISATION.NAME,'N/A') ContractorName,
			ISNULL(E__EMPLOYEE.FIRSTNAME+' '+E__EMPLOYEE.LASTNAME,'N/A') as OperativeName,
			CASE WHEN FL_FAULT_PRIORITY.Days = 1 THEN
			convert(varchar(5),FL_FAULT_PRIORITY.ResponseTime)+' days' ELSE
			convert(varchar(5),FL_FAULT_PRIORITY.ResponseTime)+' hours' END as Priority,
			convert(VARCHAR, FL_FAULT_LOG.DueDate,106)+' '+ convert(VARCHAR(5), FL_FAULT_LOG.DueDate,108)as Completion,
			convert(VARCHAR, FL_FAULT_LOG.SubmitDate,106)+' '+convert(VARCHAR(5), FL_FAULT_LOG.SubmitDate,108)as OrderDate,
			FL_AREA.AreaName Location,
			FL_FAULT.Description Description,
			FL_FAULT_LOG.Notes Notes,
			convert(VARCHAR(20), FL_CO_APPOINTMENT.AppointmentDate,106)as AppointmentTimeDate,
			FL_CO_APPOINTMENT.Notes Notes,
			ISNULL(LEFT(CONVERT(TIME,FL_CO_APPOINTMENT.Time),5) + ' - ' +LEFT(CONVERT(TIME,FL_CO_APPOINTMENT.EndTime),5) + ' '+datename(weekday,FL_CO_APPOINTMENT.AppointmentDate),'N/A') Time ,
			G_TRADE.[Description] as Trade,
			FL_FAULT_STATUS.Description FaultStatus
		  
		  
	From    FL_FAULT_LOG
			left outer join FL_FAULT_APPOINTMENT on  FL_FAULT_LOG.FaultLogID=FL_FAULT_APPOINTMENT.FaultLogId
			left outer join FL_CO_APPOINTMENT on FL_FAULT_APPOINTMENT.AppointmentId = FL_CO_APPOINTMENT.AppointmentID
			inner join FL_FAULT on FL_FAULT_LOG.FaultID = FL_FAULT.FaultID
			inner join FL_FAULT_PRIORITY  on FL_FAULT.PriorityID = FL_FAULT_PRIORITY.PriorityID
			inner join FL_AREA on FL_FAULT_LOG.AREAID = FL_AREA.AreaID 
			left outer join S_ORGANISATION on S_ORGANISATION.ORGID=FL_FAULT_LOG.ORGID 
			left outer join E__EMPLOYEE on FL_CO_APPOINTMENT.OperativeID =E__EMPLOYEE.EMPLOYEEID
			inner join FL_FAULT_TRADE on FL_FAULT_LOG.FaultTradeId =FL_FAULT_TRADE.FaultTradeId 
			inner join G_TRADE on G_TRADE.TradeId = FL_FAULT_TRADE.TradeId
			inner join FL_FAULT_STATUS on FL_FAULT_LOG.StatusID = FL_FAULT_STATUS.FaultStatusID 
		   
	Where	FL_FAULT_LOG.JobSheetNumber = @jobSheetNumber

	
-- Asbestos Info

	Select	P_PROPERTY_ASBESTOS_RISK.ASBRISKID AsbRiskID ,
			P_Asbestos.RISKDESCRIPTION Description
			
	From	FL_FAULT_LOG INNER JOIN P_PROPERTY_ASBESTOS_RISKLEVEL on FL_FAULT_LOG.PROPERTYID = P_PROPERTY_ASBESTOS_RISKLEVEL.PROPERTYID
			INNER JOIN P_PROPERTY_ASBESTOS_RISK on P_PROPERTY_ASBESTOS_RISK.PROPASBLEVELID = 	P_PROPERTY_ASBESTOS_RISKLEVEL.PROPASBLEVELID
			INNER JOIN P_ASBESTOS on P_PROPERTY_ASBESTOS_RISKLEVEL.ASBESTOSID = P_ASBESTOS.ASBESTOSID
	
	Where	FL_FAULT_LOG.JobSheetNumber = @jobSheetNumber


-- Customer Info

SELECT	ISNULL(C__CUSTOMER.FirstName,'') +' '+ ISNULL(C__CUSTOMER.MiddleName,'') +' '+ ISNULL(C__CUSTOMER.LastName,'') as NAME
		,ISNULL(P__PROPERTY.HouseNumber,'') +' '+ ISNULL(P__PROPERTY.ADDRESS1,'') +' '+ ISNULL(P__PROPERTY.ADDRESS2,'') +' '+ ISNULL(P__PROPERTY.ADDRESS3,'')  AS Address
		,P__PROPERTY.TownCity as TOWNCITY
		,P__PROPERTY.PostCode as POSTCODE     
		,P__PROPERTY.County as COUNTY
		,C_ADDRESS.Tel as TEL
		,C_ADDRESS.MOBILE as MOBILE
		,C_ADDRESS.Email as EMAIL
		
FROM	FL_FAULT_LOG
		INNER JOIN P__PROPERTY ON FL_FAULT_LOG.PROPERTYID= P__PROPERTY.PROPERTYID 
		INNER JOIN C__CUSTOMER ON FL_FAULT_LOG.CustomerId = C__CUSTOMER.CUSTOMERID 
		INNER JOIN C_ADDRESS ON FL_FAULT_LOG.CustomerId = 	C_ADDRESS.CUSTOMERID 
		
WHERE	C_ADDRESS.IsDefault = 1 AND FL_FAULT_LOG.JobSheetNumber = @jobSheetNumber	

				
END
