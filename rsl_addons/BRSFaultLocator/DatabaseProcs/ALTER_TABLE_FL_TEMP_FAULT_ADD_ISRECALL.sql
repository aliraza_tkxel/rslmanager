--Author: Behroz Sikander
--Date: 12/03/2013
--This script adds a new column in FL_TEMP_FAULT table and inserts 0 in each row against recall column

--1. First select the below statement press F5
ALTER TABLE FL_TEMP_FAULT ADD ISRECALL BIT NULL

--2. Then select the below statement press F5
UPDATE FL_TEMP_FAULT SET isRecall = 0

