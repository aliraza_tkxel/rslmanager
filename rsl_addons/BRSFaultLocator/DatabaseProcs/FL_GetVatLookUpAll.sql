USE [RSLBHALive]
GO

SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

CREATE PROCEDURE [dbo].[FL_GetVatLookUpAll]
/* ===========================================================================
--	FL_GetVatLookUpAll
--  Author:			Aamir Waheed
--  DATE CREATED:	8 March 2013
--  Description:	To Get List of Vat Name and Vat ID for LookUp/DropDown Lists
--  Webpage:		View/Reports/ReportArea.aspx (For Add/Amend Fault Modal Popup)
 '==============================================================================*/
AS
	SELECT VATID AS id,VATNAME AS val
	FROM F_VAT
	ORDER BY VATID ASC