USE [RSLBHALive]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =====================================================================
--DECLARE	@return_value int

--EXEC	@return_value = [dbo].[FL_UpdateFaultNotes]
--		@@tempFaultID = 136652,
--		@@notes = N'Updated Text'
-- Author:		<Fakhar uz zaman>
-- Create date: <11/2/2016>
-- Description:	<Updates Notes in Fault>
-- Web Page: FaultBasket.aspx
-- =====================================================================
IF OBJECT_ID('dbo.FL_UpdateFaultNotes') IS NULL -- Check if SP Exists
 EXEC('CREATE PROCEDURE dbo.FL_UpdateFaultNotes AS SET NOCOUNT ON;') -- Create dummy/empty SP
GO

ALTER PROCEDURE [dbo].[FL_UpdateFaultNotes] 
	-- Add the parameters for the stored procedure here
	@tempFaultID int,
	@notes varchar(4000),
	@isUpdated int OUTPUT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
BEGIN TRANSACTION

	BEGIN TRY
	
	   Update FL_TEMP_FAULT 
	   set Notes = @Notes 
	   Where TempFaultID=@TempFaultID
	   
	END TRY
	
	BEGIN CATCH 
		IF @@TRANCOUNT > 0
		BEGIN     
			ROLLBACK TRANSACTION;   
			SET @isUpdated = 0        
		END
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT @ErrorMessage = ERROR_MESSAGE(),
		@ErrorSeverity = ERROR_SEVERITY(),
		@ErrorState = ERROR_STATE();

		-- Use RAISERROR inside the CATCH block to return 
		-- error information about the original error that 
		-- caused execution to jump to the CATCH block.
		RAISERROR (@ErrorMessage, -- Message text.
					@ErrorSeverity, -- Severity.
					@ErrorState -- State.
				);
	END CATCH;

	IF @@TRANCOUNT > 0
		BEGIN  
			COMMIT TRANSACTION;  
			SET @isUpdated = 1
		END
--END
  
END

GO