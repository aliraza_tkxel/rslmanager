USE [RSLBHALive]
GO

/****** Object:  Table [dbo].[FL_CONTRACTOR_WORK]    Script Date: 02/03/2015 18:04:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[FL_CONTRACTOR_WORK](
	[FaultContractorId] [int] IDENTITY(1,1) NOT NULL,
	[JournalId] [int] NOT NULL,
	[ContractorId] [int] NOT NULL,
	[AssignedDate] [datetime] NOT NULL,
	[AssignedBy] [int] NOT NULL,
	[Estimate] [money] NOT NULL,
	[EstimateRef] [nvarchar](400) NOT NULL,
	[AreaId] [int] NOT NULL,
	[ItemId] [int] NOT NULL,
	[ContactId] [int] NOT NULL,
	[PurchaseORDERID] [int] NULL,
 CONSTRAINT [PK_FL_CONTRACTOR_WORK] PRIMARY KEY CLUSTERED 
(
	[FaultContractorId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


