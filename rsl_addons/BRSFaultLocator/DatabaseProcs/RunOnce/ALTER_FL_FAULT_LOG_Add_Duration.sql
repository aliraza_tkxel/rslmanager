--=========================================================================
--====== Add Duration Column in FL_FAULT_LOG
--=========================================================================

IF NOT EXISTS
(
	SELECT
		*
	FROM
		INFORMATION_SCHEMA.COLUMNS
	WHERE
		TABLE_NAME = 'FL_FAULT_LOG'
		AND COLUMN_NAME = 'Duration'
) ALTER TABLE FL_FAULT_LOG Add Duration DECIMAL(9,2) NULL
GO