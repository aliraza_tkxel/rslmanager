/*
	Use this script to check and add a menu in AC_MENUS
	by setting the variable values in below section
*/

DECLARE	@Module NVARCHAR(100) = 'Property'
		,@MenuDescription NVARCHAR(200) = 'Calendar'
		,@Active INT = 1
		,@MenuPage NVARCHAR(600) = '~/../BRSFaultLocator/Bridge.aspx?pg=propertyCal'
		,@MenuWidth INT
		,@SubMenuWidth INT
		,@OrderText NVARCHAR(100) = 'Z'

BEGIN TRANSACTION
BEGIN TRY

	DECLARE	@ModuleId INT = NULL

	SELECT			
		@ModuleId = MD.MODULEID
	FROM
		AC_MODULES AS MD
	WHERE
		MD.DESCRIPTION = @Module
			
PRINT 'Module ID: ' + ISNULL(CONVERT(NVARCHAR,@ModuleId),'NULL')

IF @ModuleId IS NOT NULL
BEGIN
IF	NOT EXISTS
	(
		SELECT
			MN.MENUID
		FROM
			AC_MENUS MN
				INNER JOIN AC_MODULES MD ON MD.MODULEID = MN.MODULEID
					AND
					MD.DESCRIPTION = @Module
		WHERE
			MN.DESCRIPTION = @MenuDescription			
	)
BEGIN
		PRINT 'Menu: ' + @Module + ' > ' + '"' + @MenuDescription + '" do not exists, Inserting '
		
		INSERT INTO AC_MENUS (
						DESCRIPTION
						,MODULEID
						,ACTIVE						
						,PAGE
						,MENUWIDTH
						,SUBMENUWIDTH
						,ORDERTEXT
					)
			VALUES (
					@MenuDescription
					,@ModuleId
					,@Active					
					,@MenuPage
					,@MenuWidth
					,@SubMenuWidth
					,@OrderText
				)		
		PRINT 'Menu ' + @MenuDescription + ' added successfully'
END
ELSE
BEGIN
	PRINT 'Menu: ' + @Module + ' > ' + '"' + @MenuDescription + '" already exists, Updating '
	
	UPDATE MN
		SET MN.ACTIVE = @Active
			,MN.PAGE = @MenuPage
			,MN.MENUWIDTH = @MenuWidth
			,MN.SUBMENUWIDTH = @SubMenuWidth
			,MN.ORDERTEXT = @OrderText
	FROM
		AC_MENUS MN
			INNER JOIN AC_MODULES MD ON MD.MODULEID = MN.MODULEID
				AND
				MD.DESCRIPTION = @Module
	WHERE
		MN.DESCRIPTION = @MenuDescription		

END

END
ELSE
	PRINT 'Module "' + @Module + '" does not exists, please verify the module is correct.' 
	-- if successful - COMMIT the work
	COMMIT TRANSACTION
END TRY
BEGIN CATCH
	-- handle the error case (here by displaying the error)
	SELECT
		ERROR_NUMBER()		AS ErrorNumber
		,ERROR_SEVERITY()	AS ErrorSeverity
		,ERROR_STATE()		AS ErrorState
		,ERROR_PROCEDURE()	AS ErrorProcedure
		,ERROR_LINE()		AS ErrorLine
		,ERROR_MESSAGE()	AS ErrorMessage

	-- in case of an error, ROLLBACK the transaction    
	ROLLBACK TRANSACTION
END CATCH