--=========================================================================
--====== Add Duration Column in FL_FAULT_LOG_HISTORY
--=========================================================================

IF NOT EXISTS
(
	SELECT
		*
	FROM
		INFORMATION_SCHEMA.COLUMNS
	WHERE
		TABLE_NAME = 'FL_FAULT_LOG_HISTORY'
		AND COLUMN_NAME = 'Duration'
) ALTER TABLE FL_FAULT_LOG_HISTORY Add Duration DECIMAL(9,2) NULL
GO