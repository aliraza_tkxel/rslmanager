/*
Author: Aamir Waheed
Date: 27 Jan 2014
*/

--=========================================================================
--====== Add PauseID Column in FL_FAULT_JOBTIMESHEET
--=========================================================================

IF NOT EXISTS
(
	SELECT
		*
	FROM
		INFORMATION_SCHEMA.COLUMNS
	WHERE
		TABLE_NAME = 'FL_FAULT_JOBTIMESHEET'
		AND COLUMN_NAME = 'PauseID'
)
BEGIN
	PRINT 'Not Exists'
	
	--==============================================================
 /* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION 
ALTER TABLE dbo.FL_FAULT_PAUSED SET (LOCK_ESCALATION = TABLE) 
COMMIT
select Has_Perms_By_Name(N'dbo.FL_FAULT_PAUSED', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.FL_FAULT_PAUSED', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.FL_FAULT_PAUSED', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
 
ALTER TABLE dbo.FL_FAULT_JOBTIMESHEET ADD
	PauseID int NULL
 
ALTER TABLE dbo.FL_FAULT_JOBTIMESHEET ADD CONSTRAINT
	FK_FL_FAULT_JOBTIMESHEET_FL_FAULT_PAUSED FOREIGN KEY
	(
	PauseID
	) REFERENCES dbo.FL_FAULT_PAUSED
	(
	PauseID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION	
 
ALTER TABLE dbo.FL_FAULT_JOBTIMESHEET SET (LOCK_ESCALATION = TABLE)
 
COMMIT
select Has_Perms_By_Name(N'dbo.FL_FAULT_JOBTIMESHEET', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.FL_FAULT_JOBTIMESHEET', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.FL_FAULT_JOBTIMESHEET', 'Object', 'CONTROL') as Contr_Per 

--==============================================================
 PRINT 'Column inserted successfully.'
END
ELSE
	PRINT 'Column already exists.'
 