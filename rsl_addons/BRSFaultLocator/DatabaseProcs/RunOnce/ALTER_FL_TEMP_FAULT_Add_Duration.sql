--=========================================================================
--====== Add Duration Column in FL_TEMP_FAULT
--=========================================================================

IF NOT EXISTS
(
	SELECT
		*
	FROM
		INFORMATION_SCHEMA.COLUMNS
	WHERE
		TABLE_NAME = 'FL_TEMP_FAULT'
		AND COLUMN_NAME = 'Duration'
) ALTER TABLE FL_TEMP_FAULT Add Duration DECIMAL(9,2) NULL
GO