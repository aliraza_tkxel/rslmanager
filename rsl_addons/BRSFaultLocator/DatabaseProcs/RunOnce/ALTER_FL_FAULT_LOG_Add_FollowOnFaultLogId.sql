--=========================================================================
--====== Add FollowOnFaultLogId Column in FL_FAULT_LOG
--=========================================================================
IF NOT EXISTS
(
	SELECT
		*
	FROM
		INFORMATION_SCHEMA.COLUMNS
	WHERE
		TABLE_NAME = 'FL_FAULT_LOG'
		AND COLUMN_NAME = 'FollowOnFaultLogId'
) ALTER TABLE FL_FAULT_LOG Add FollowOnFaultLogId INT NULL
GO