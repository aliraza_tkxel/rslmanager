/*
Author: Aamir Waheed
Date: 27 Jan 2014
*/

--=========================================================================
--====== Add FaultLogHistoryID Column in FL_FAULT_PAUSED
--=========================================================================
IF NOT EXISTS
(
	SELECT
		*
	FROM
		INFORMATION_SCHEMA.COLUMNS
	WHERE
		TABLE_NAME = 'FL_FAULT_PAUSED'
		AND COLUMN_NAME = 'FaultLogHistoryID'
)
BEGIN
	PRINT 'Not Exists'
	
	--==============================================================
/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
	BEGIN TRANSACTION
	SET QUOTED_IDENTIFIER ON
	SET ARITHABORT ON
	SET NUMERIC_ROUNDABORT OFF
	SET CONCAT_NULL_YIELDS_NULL ON
	SET ANSI_NULLS ON
	SET ANSI_PADDING ON
	SET ANSI_WARNINGS ON
	COMMIT
	BEGIN TRANSACTION	
	ALTER TABLE dbo.FL_FAULT_LOG_HISTORY SET (LOCK_ESCALATION = TABLE)	
	COMMIT
	select Has_Perms_By_Name(N'dbo.FL_FAULT_LOG_HISTORY', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.FL_FAULT_LOG_HISTORY', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.FL_FAULT_LOG_HISTORY', 'Object', 'CONTROL') as Contr_Per BEGIN TRANSACTION
	
	ALTER TABLE dbo.FL_FAULT_PAUSED ADD
		FaultLogHistoryID int NULL	
	ALTER TABLE dbo.FL_FAULT_PAUSED ADD CONSTRAINT
	FK_FL_FAULT_PAUSED_FL_FAULT_LOG_HISTORY FOREIGN KEY
	(
	FaultLogHistoryID
	) REFERENCES dbo.FL_FAULT_LOG_HISTORY
	(
	FaultLogHistoryID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 	
	ALTER TABLE dbo.FL_FAULT_PAUSED SET (LOCK_ESCALATION = TABLE)	
	COMMIT
	select Has_Perms_By_Name(N'dbo.FL_FAULT_PAUSED', 'Object', 'ALTER') as ALT_Per, Has_Perms_By_Name(N'dbo.FL_FAULT_PAUSED', 'Object', 'VIEW DEFINITION') as View_def_Per, Has_Perms_By_Name(N'dbo.FL_FAULT_PAUSED', 'Object', 'CONTROL') as Contr_Per
	
	--==============================================================
	
	PRINT 'Column inserted successfully.'
END
ELSE
	PRINT 'Column already exists.'
GO